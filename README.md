# Welcome to the direforpythia_mg5_mes repository

## Introduction

This is a repository of Madgraph5_aMC@NLO-generated C++ matrix element code compatible with the Dire radiation cascades for particle physics. 

## Important warning
This repository is for development and testing. For stable and validated versions, see the Releases section below. 

## Releases

The code in this repository is compatible with Dire 2.003.
For documentation of Dire, please consult [dire.gitlab.io](https://dire.gitlab.io)

## Authors

The Madgraph5_aMC@NLO generation of C++ matrix element code for Dire has been developed by Valentin Hirschi.
Dire has been developed by Stefan Hoeche and Stefan Prestel. 
The latter will take all blame
regarding the Dire plugin for the Pythia 8 event generator, i.e. for the code in this repository.

## News:
- 2019/01/xx: First release of  direforpythia_mg5_mes 

