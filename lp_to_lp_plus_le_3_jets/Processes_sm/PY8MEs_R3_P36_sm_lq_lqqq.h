//==========================================================================
// This file has been automatically generated for Pythia 8
// MadGraph5_aMC@NLO v. 2.6.0, 2017-08-16
// By the MadGraph5_aMC@NLO Development Team
// Visit launchpad.net/madgraph5 and amcatnlo.web.cern.ch
//==========================================================================

#ifndef PY8MEs_R3_P36_sm_lq_lqqq_H
#define PY8MEs_R3_P36_sm_lq_lqqq_H

#include "Complex.h" 
#include <vector> 
#include <set> 
#include <exception> 
#include <iostream> 

#include "Parameters_sm.h"
#include "PY8MEs.h"

using namespace std; 

namespace PY8MEs_namespace 
{
//==========================================================================
// A class for calculating the matrix elements for
// Process: e- u > e- u u u~ WEIGHTED<=6 @3
// Process: e- c > e- c c c~ WEIGHTED<=6 @3
// Process: mu- u > mu- u u u~ WEIGHTED<=6 @3
// Process: mu- c > mu- c c c~ WEIGHTED<=6 @3
// Process: e- d > e- d d d~ WEIGHTED<=6 @3
// Process: e- s > e- s s s~ WEIGHTED<=6 @3
// Process: mu- d > mu- d d d~ WEIGHTED<=6 @3
// Process: mu- s > mu- s s s~ WEIGHTED<=6 @3
// Process: e- u~ > e- u u~ u~ WEIGHTED<=6 @3
// Process: e- c~ > e- c c~ c~ WEIGHTED<=6 @3
// Process: mu- u~ > mu- u u~ u~ WEIGHTED<=6 @3
// Process: mu- c~ > mu- c c~ c~ WEIGHTED<=6 @3
// Process: e- d~ > e- d d~ d~ WEIGHTED<=6 @3
// Process: e- s~ > e- s s~ s~ WEIGHTED<=6 @3
// Process: mu- d~ > mu- d d~ d~ WEIGHTED<=6 @3
// Process: mu- s~ > mu- s s~ s~ WEIGHTED<=6 @3
// Process: e+ u > e+ u u u~ WEIGHTED<=6 @3
// Process: e+ c > e+ c c c~ WEIGHTED<=6 @3
// Process: mu+ u > mu+ u u u~ WEIGHTED<=6 @3
// Process: mu+ c > mu+ c c c~ WEIGHTED<=6 @3
// Process: e+ d > e+ d d d~ WEIGHTED<=6 @3
// Process: e+ s > e+ s s s~ WEIGHTED<=6 @3
// Process: mu+ d > mu+ d d d~ WEIGHTED<=6 @3
// Process: mu+ s > mu+ s s s~ WEIGHTED<=6 @3
// Process: e+ u~ > e+ u u~ u~ WEIGHTED<=6 @3
// Process: e+ c~ > e+ c c~ c~ WEIGHTED<=6 @3
// Process: mu+ u~ > mu+ u u~ u~ WEIGHTED<=6 @3
// Process: mu+ c~ > mu+ c c~ c~ WEIGHTED<=6 @3
// Process: e+ d~ > e+ d d~ d~ WEIGHTED<=6 @3
// Process: e+ s~ > e+ s s~ s~ WEIGHTED<=6 @3
// Process: mu+ d~ > mu+ d d~ d~ WEIGHTED<=6 @3
// Process: mu+ s~ > mu+ s s~ s~ WEIGHTED<=6 @3
// Process: e- u > e- u c c~ WEIGHTED<=6 @3
// Process: e- c > e- c u u~ WEIGHTED<=6 @3
// Process: mu- u > mu- u c c~ WEIGHTED<=6 @3
// Process: mu- c > mu- c u u~ WEIGHTED<=6 @3
// Process: e- u > e- u d d~ WEIGHTED<=6 @3
// Process: e- u > e- u s s~ WEIGHTED<=6 @3
// Process: e- c > e- c d d~ WEIGHTED<=6 @3
// Process: e- c > e- c s s~ WEIGHTED<=6 @3
// Process: mu- u > mu- u d d~ WEIGHTED<=6 @3
// Process: mu- u > mu- u s s~ WEIGHTED<=6 @3
// Process: mu- c > mu- c d d~ WEIGHTED<=6 @3
// Process: mu- c > mu- c s s~ WEIGHTED<=6 @3
// Process: e- d > e- u d u~ WEIGHTED<=6 @3
// Process: e- d > e- c d c~ WEIGHTED<=6 @3
// Process: e- s > e- u s u~ WEIGHTED<=6 @3
// Process: e- s > e- c s c~ WEIGHTED<=6 @3
// Process: mu- d > mu- u d u~ WEIGHTED<=6 @3
// Process: mu- d > mu- c d c~ WEIGHTED<=6 @3
// Process: mu- s > mu- u s u~ WEIGHTED<=6 @3
// Process: mu- s > mu- c s c~ WEIGHTED<=6 @3
// Process: e- d > e- d s s~ WEIGHTED<=6 @3
// Process: e- s > e- s d d~ WEIGHTED<=6 @3
// Process: mu- d > mu- d s s~ WEIGHTED<=6 @3
// Process: mu- s > mu- s d d~ WEIGHTED<=6 @3
// Process: e- u~ > e- c u~ c~ WEIGHTED<=6 @3
// Process: e- c~ > e- u c~ u~ WEIGHTED<=6 @3
// Process: mu- u~ > mu- c u~ c~ WEIGHTED<=6 @3
// Process: mu- c~ > mu- u c~ u~ WEIGHTED<=6 @3
// Process: e- u~ > e- d u~ d~ WEIGHTED<=6 @3
// Process: e- u~ > e- s u~ s~ WEIGHTED<=6 @3
// Process: e- c~ > e- d c~ d~ WEIGHTED<=6 @3
// Process: e- c~ > e- s c~ s~ WEIGHTED<=6 @3
// Process: mu- u~ > mu- d u~ d~ WEIGHTED<=6 @3
// Process: mu- u~ > mu- s u~ s~ WEIGHTED<=6 @3
// Process: mu- c~ > mu- d c~ d~ WEIGHTED<=6 @3
// Process: mu- c~ > mu- s c~ s~ WEIGHTED<=6 @3
// Process: e- d~ > e- u u~ d~ WEIGHTED<=6 @3
// Process: e- d~ > e- c c~ d~ WEIGHTED<=6 @3
// Process: e- s~ > e- u u~ s~ WEIGHTED<=6 @3
// Process: e- s~ > e- c c~ s~ WEIGHTED<=6 @3
// Process: mu- d~ > mu- u u~ d~ WEIGHTED<=6 @3
// Process: mu- d~ > mu- c c~ d~ WEIGHTED<=6 @3
// Process: mu- s~ > mu- u u~ s~ WEIGHTED<=6 @3
// Process: mu- s~ > mu- c c~ s~ WEIGHTED<=6 @3
// Process: e- d~ > e- s d~ s~ WEIGHTED<=6 @3
// Process: e- s~ > e- d s~ d~ WEIGHTED<=6 @3
// Process: mu- d~ > mu- s d~ s~ WEIGHTED<=6 @3
// Process: mu- s~ > mu- d s~ d~ WEIGHTED<=6 @3
// Process: ve u > ve u u u~ WEIGHTED<=6 @3
// Process: ve c > ve c c c~ WEIGHTED<=6 @3
// Process: vm u > vm u u u~ WEIGHTED<=6 @3
// Process: vm c > vm c c c~ WEIGHTED<=6 @3
// Process: vt u > vt u u u~ WEIGHTED<=6 @3
// Process: vt c > vt c c c~ WEIGHTED<=6 @3
// Process: ve d > ve d d d~ WEIGHTED<=6 @3
// Process: ve s > ve s s s~ WEIGHTED<=6 @3
// Process: vm d > vm d d d~ WEIGHTED<=6 @3
// Process: vm s > vm s s s~ WEIGHTED<=6 @3
// Process: vt d > vt d d d~ WEIGHTED<=6 @3
// Process: vt s > vt s s s~ WEIGHTED<=6 @3
// Process: ve u~ > ve u u~ u~ WEIGHTED<=6 @3
// Process: ve c~ > ve c c~ c~ WEIGHTED<=6 @3
// Process: vm u~ > vm u u~ u~ WEIGHTED<=6 @3
// Process: vm c~ > vm c c~ c~ WEIGHTED<=6 @3
// Process: vt u~ > vt u u~ u~ WEIGHTED<=6 @3
// Process: vt c~ > vt c c~ c~ WEIGHTED<=6 @3
// Process: ve d~ > ve d d~ d~ WEIGHTED<=6 @3
// Process: ve s~ > ve s s~ s~ WEIGHTED<=6 @3
// Process: vm d~ > vm d d~ d~ WEIGHTED<=6 @3
// Process: vm s~ > vm s s~ s~ WEIGHTED<=6 @3
// Process: vt d~ > vt d d~ d~ WEIGHTED<=6 @3
// Process: vt s~ > vt s s~ s~ WEIGHTED<=6 @3
// Process: e+ u > e+ u c c~ WEIGHTED<=6 @3
// Process: e+ c > e+ c u u~ WEIGHTED<=6 @3
// Process: mu+ u > mu+ u c c~ WEIGHTED<=6 @3
// Process: mu+ c > mu+ c u u~ WEIGHTED<=6 @3
// Process: e+ u > e+ u d d~ WEIGHTED<=6 @3
// Process: e+ u > e+ u s s~ WEIGHTED<=6 @3
// Process: e+ c > e+ c d d~ WEIGHTED<=6 @3
// Process: e+ c > e+ c s s~ WEIGHTED<=6 @3
// Process: mu+ u > mu+ u d d~ WEIGHTED<=6 @3
// Process: mu+ u > mu+ u s s~ WEIGHTED<=6 @3
// Process: mu+ c > mu+ c d d~ WEIGHTED<=6 @3
// Process: mu+ c > mu+ c s s~ WEIGHTED<=6 @3
// Process: e+ d > e+ u d u~ WEIGHTED<=6 @3
// Process: e+ d > e+ c d c~ WEIGHTED<=6 @3
// Process: e+ s > e+ u s u~ WEIGHTED<=6 @3
// Process: e+ s > e+ c s c~ WEIGHTED<=6 @3
// Process: mu+ d > mu+ u d u~ WEIGHTED<=6 @3
// Process: mu+ d > mu+ c d c~ WEIGHTED<=6 @3
// Process: mu+ s > mu+ u s u~ WEIGHTED<=6 @3
// Process: mu+ s > mu+ c s c~ WEIGHTED<=6 @3
// Process: e+ d > e+ d s s~ WEIGHTED<=6 @3
// Process: e+ s > e+ s d d~ WEIGHTED<=6 @3
// Process: mu+ d > mu+ d s s~ WEIGHTED<=6 @3
// Process: mu+ s > mu+ s d d~ WEIGHTED<=6 @3
// Process: e+ u~ > e+ c u~ c~ WEIGHTED<=6 @3
// Process: e+ c~ > e+ u c~ u~ WEIGHTED<=6 @3
// Process: mu+ u~ > mu+ c u~ c~ WEIGHTED<=6 @3
// Process: mu+ c~ > mu+ u c~ u~ WEIGHTED<=6 @3
// Process: e+ u~ > e+ d u~ d~ WEIGHTED<=6 @3
// Process: e+ u~ > e+ s u~ s~ WEIGHTED<=6 @3
// Process: e+ c~ > e+ d c~ d~ WEIGHTED<=6 @3
// Process: e+ c~ > e+ s c~ s~ WEIGHTED<=6 @3
// Process: mu+ u~ > mu+ d u~ d~ WEIGHTED<=6 @3
// Process: mu+ u~ > mu+ s u~ s~ WEIGHTED<=6 @3
// Process: mu+ c~ > mu+ d c~ d~ WEIGHTED<=6 @3
// Process: mu+ c~ > mu+ s c~ s~ WEIGHTED<=6 @3
// Process: e+ d~ > e+ u u~ d~ WEIGHTED<=6 @3
// Process: e+ d~ > e+ c c~ d~ WEIGHTED<=6 @3
// Process: e+ s~ > e+ u u~ s~ WEIGHTED<=6 @3
// Process: e+ s~ > e+ c c~ s~ WEIGHTED<=6 @3
// Process: mu+ d~ > mu+ u u~ d~ WEIGHTED<=6 @3
// Process: mu+ d~ > mu+ c c~ d~ WEIGHTED<=6 @3
// Process: mu+ s~ > mu+ u u~ s~ WEIGHTED<=6 @3
// Process: mu+ s~ > mu+ c c~ s~ WEIGHTED<=6 @3
// Process: e+ d~ > e+ s d~ s~ WEIGHTED<=6 @3
// Process: e+ s~ > e+ d s~ d~ WEIGHTED<=6 @3
// Process: mu+ d~ > mu+ s d~ s~ WEIGHTED<=6 @3
// Process: mu+ s~ > mu+ d s~ d~ WEIGHTED<=6 @3
// Process: ve~ u > ve~ u u u~ WEIGHTED<=6 @3
// Process: ve~ c > ve~ c c c~ WEIGHTED<=6 @3
// Process: vm~ u > vm~ u u u~ WEIGHTED<=6 @3
// Process: vm~ c > vm~ c c c~ WEIGHTED<=6 @3
// Process: vt~ u > vt~ u u u~ WEIGHTED<=6 @3
// Process: vt~ c > vt~ c c c~ WEIGHTED<=6 @3
// Process: ve~ d > ve~ d d d~ WEIGHTED<=6 @3
// Process: ve~ s > ve~ s s s~ WEIGHTED<=6 @3
// Process: vm~ d > vm~ d d d~ WEIGHTED<=6 @3
// Process: vm~ s > vm~ s s s~ WEIGHTED<=6 @3
// Process: vt~ d > vt~ d d d~ WEIGHTED<=6 @3
// Process: vt~ s > vt~ s s s~ WEIGHTED<=6 @3
// Process: ve~ u~ > ve~ u u~ u~ WEIGHTED<=6 @3
// Process: ve~ c~ > ve~ c c~ c~ WEIGHTED<=6 @3
// Process: vm~ u~ > vm~ u u~ u~ WEIGHTED<=6 @3
// Process: vm~ c~ > vm~ c c~ c~ WEIGHTED<=6 @3
// Process: vt~ u~ > vt~ u u~ u~ WEIGHTED<=6 @3
// Process: vt~ c~ > vt~ c c~ c~ WEIGHTED<=6 @3
// Process: ve~ d~ > ve~ d d~ d~ WEIGHTED<=6 @3
// Process: ve~ s~ > ve~ s s~ s~ WEIGHTED<=6 @3
// Process: vm~ d~ > vm~ d d~ d~ WEIGHTED<=6 @3
// Process: vm~ s~ > vm~ s s~ s~ WEIGHTED<=6 @3
// Process: vt~ d~ > vt~ d d~ d~ WEIGHTED<=6 @3
// Process: vt~ s~ > vt~ s s~ s~ WEIGHTED<=6 @3
// Process: e- u > ve u d u~ WEIGHTED<=6 @3
// Process: e- c > ve c s c~ WEIGHTED<=6 @3
// Process: mu- u > vm u d u~ WEIGHTED<=6 @3
// Process: mu- c > vm c s c~ WEIGHTED<=6 @3
// Process: ve d > e- d u d~ WEIGHTED<=6 @3
// Process: ve s > e- s c s~ WEIGHTED<=6 @3
// Process: vm d > mu- d u d~ WEIGHTED<=6 @3
// Process: vm s > mu- s c s~ WEIGHTED<=6 @3
// Process: e- u > ve d d d~ WEIGHTED<=6 @3
// Process: e- c > ve s s s~ WEIGHTED<=6 @3
// Process: mu- u > vm d d d~ WEIGHTED<=6 @3
// Process: mu- c > vm s s s~ WEIGHTED<=6 @3
// Process: ve d > e- u u u~ WEIGHTED<=6 @3
// Process: ve s > e- c c c~ WEIGHTED<=6 @3
// Process: vm d > mu- u u u~ WEIGHTED<=6 @3
// Process: vm s > mu- c c c~ WEIGHTED<=6 @3
// Process: e- d > ve d d u~ WEIGHTED<=6 @3
// Process: e- s > ve s s c~ WEIGHTED<=6 @3
// Process: mu- d > vm d d u~ WEIGHTED<=6 @3
// Process: mu- s > vm s s c~ WEIGHTED<=6 @3
// Process: ve u > e- u u d~ WEIGHTED<=6 @3
// Process: ve c > e- c c s~ WEIGHTED<=6 @3
// Process: vm u > mu- u u d~ WEIGHTED<=6 @3
// Process: vm c > mu- c c s~ WEIGHTED<=6 @3
// Process: e- u~ > ve d u~ u~ WEIGHTED<=6 @3
// Process: e- c~ > ve s c~ c~ WEIGHTED<=6 @3
// Process: mu- u~ > vm d u~ u~ WEIGHTED<=6 @3
// Process: mu- c~ > vm s c~ c~ WEIGHTED<=6 @3
// Process: ve d~ > e- u d~ d~ WEIGHTED<=6 @3
// Process: ve s~ > e- c s~ s~ WEIGHTED<=6 @3
// Process: vm d~ > mu- u d~ d~ WEIGHTED<=6 @3
// Process: vm s~ > mu- c s~ s~ WEIGHTED<=6 @3
// Process: e- d~ > ve u u~ u~ WEIGHTED<=6 @3
// Process: e- s~ > ve c c~ c~ WEIGHTED<=6 @3
// Process: mu- d~ > vm u u~ u~ WEIGHTED<=6 @3
// Process: mu- s~ > vm c c~ c~ WEIGHTED<=6 @3
// Process: ve u~ > e- d d~ d~ WEIGHTED<=6 @3
// Process: ve c~ > e- s s~ s~ WEIGHTED<=6 @3
// Process: vm u~ > mu- d d~ d~ WEIGHTED<=6 @3
// Process: vm c~ > mu- s s~ s~ WEIGHTED<=6 @3
// Process: e- d~ > ve d u~ d~ WEIGHTED<=6 @3
// Process: e- s~ > ve s c~ s~ WEIGHTED<=6 @3
// Process: mu- d~ > vm d u~ d~ WEIGHTED<=6 @3
// Process: mu- s~ > vm s c~ s~ WEIGHTED<=6 @3
// Process: ve u~ > e- u d~ u~ WEIGHTED<=6 @3
// Process: ve c~ > e- c s~ c~ WEIGHTED<=6 @3
// Process: vm u~ > mu- u d~ u~ WEIGHTED<=6 @3
// Process: vm c~ > mu- c s~ c~ WEIGHTED<=6 @3
// Process: ve u > ve u c c~ WEIGHTED<=6 @3
// Process: ve c > ve c u u~ WEIGHTED<=6 @3
// Process: vm u > vm u c c~ WEIGHTED<=6 @3
// Process: vm c > vm c u u~ WEIGHTED<=6 @3
// Process: vt u > vt u c c~ WEIGHTED<=6 @3
// Process: vt c > vt c u u~ WEIGHTED<=6 @3
// Process: ve u > ve u d d~ WEIGHTED<=6 @3
// Process: ve u > ve u s s~ WEIGHTED<=6 @3
// Process: ve c > ve c d d~ WEIGHTED<=6 @3
// Process: ve c > ve c s s~ WEIGHTED<=6 @3
// Process: vm u > vm u d d~ WEIGHTED<=6 @3
// Process: vm u > vm u s s~ WEIGHTED<=6 @3
// Process: vm c > vm c d d~ WEIGHTED<=6 @3
// Process: vm c > vm c s s~ WEIGHTED<=6 @3
// Process: vt u > vt u d d~ WEIGHTED<=6 @3
// Process: vt u > vt u s s~ WEIGHTED<=6 @3
// Process: vt c > vt c d d~ WEIGHTED<=6 @3
// Process: vt c > vt c s s~ WEIGHTED<=6 @3
// Process: ve d > ve u d u~ WEIGHTED<=6 @3
// Process: ve d > ve c d c~ WEIGHTED<=6 @3
// Process: ve s > ve u s u~ WEIGHTED<=6 @3
// Process: ve s > ve c s c~ WEIGHTED<=6 @3
// Process: vm d > vm u d u~ WEIGHTED<=6 @3
// Process: vm d > vm c d c~ WEIGHTED<=6 @3
// Process: vm s > vm u s u~ WEIGHTED<=6 @3
// Process: vm s > vm c s c~ WEIGHTED<=6 @3
// Process: vt d > vt u d u~ WEIGHTED<=6 @3
// Process: vt d > vt c d c~ WEIGHTED<=6 @3
// Process: vt s > vt u s u~ WEIGHTED<=6 @3
// Process: vt s > vt c s c~ WEIGHTED<=6 @3
// Process: ve d > ve d s s~ WEIGHTED<=6 @3
// Process: ve s > ve s d d~ WEIGHTED<=6 @3
// Process: vm d > vm d s s~ WEIGHTED<=6 @3
// Process: vm s > vm s d d~ WEIGHTED<=6 @3
// Process: vt d > vt d s s~ WEIGHTED<=6 @3
// Process: vt s > vt s d d~ WEIGHTED<=6 @3
// Process: ve u~ > ve c u~ c~ WEIGHTED<=6 @3
// Process: ve c~ > ve u c~ u~ WEIGHTED<=6 @3
// Process: vm u~ > vm c u~ c~ WEIGHTED<=6 @3
// Process: vm c~ > vm u c~ u~ WEIGHTED<=6 @3
// Process: vt u~ > vt c u~ c~ WEIGHTED<=6 @3
// Process: vt c~ > vt u c~ u~ WEIGHTED<=6 @3
// Process: ve u~ > ve d u~ d~ WEIGHTED<=6 @3
// Process: ve u~ > ve s u~ s~ WEIGHTED<=6 @3
// Process: ve c~ > ve d c~ d~ WEIGHTED<=6 @3
// Process: ve c~ > ve s c~ s~ WEIGHTED<=6 @3
// Process: vm u~ > vm d u~ d~ WEIGHTED<=6 @3
// Process: vm u~ > vm s u~ s~ WEIGHTED<=6 @3
// Process: vm c~ > vm d c~ d~ WEIGHTED<=6 @3
// Process: vm c~ > vm s c~ s~ WEIGHTED<=6 @3
// Process: vt u~ > vt d u~ d~ WEIGHTED<=6 @3
// Process: vt u~ > vt s u~ s~ WEIGHTED<=6 @3
// Process: vt c~ > vt d c~ d~ WEIGHTED<=6 @3
// Process: vt c~ > vt s c~ s~ WEIGHTED<=6 @3
// Process: ve d~ > ve u u~ d~ WEIGHTED<=6 @3
// Process: ve d~ > ve c c~ d~ WEIGHTED<=6 @3
// Process: ve s~ > ve u u~ s~ WEIGHTED<=6 @3
// Process: ve s~ > ve c c~ s~ WEIGHTED<=6 @3
// Process: vm d~ > vm u u~ d~ WEIGHTED<=6 @3
// Process: vm d~ > vm c c~ d~ WEIGHTED<=6 @3
// Process: vm s~ > vm u u~ s~ WEIGHTED<=6 @3
// Process: vm s~ > vm c c~ s~ WEIGHTED<=6 @3
// Process: vt d~ > vt u u~ d~ WEIGHTED<=6 @3
// Process: vt d~ > vt c c~ d~ WEIGHTED<=6 @3
// Process: vt s~ > vt u u~ s~ WEIGHTED<=6 @3
// Process: vt s~ > vt c c~ s~ WEIGHTED<=6 @3
// Process: ve d~ > ve s d~ s~ WEIGHTED<=6 @3
// Process: ve s~ > ve d s~ d~ WEIGHTED<=6 @3
// Process: vm d~ > vm s d~ s~ WEIGHTED<=6 @3
// Process: vm s~ > vm d s~ d~ WEIGHTED<=6 @3
// Process: vt d~ > vt s d~ s~ WEIGHTED<=6 @3
// Process: vt s~ > vt d s~ d~ WEIGHTED<=6 @3
// Process: e+ u > ve~ u u d~ WEIGHTED<=6 @3
// Process: e+ c > ve~ c c s~ WEIGHTED<=6 @3
// Process: mu+ u > vm~ u u d~ WEIGHTED<=6 @3
// Process: mu+ c > vm~ c c s~ WEIGHTED<=6 @3
// Process: ve~ d > e+ d d u~ WEIGHTED<=6 @3
// Process: ve~ s > e+ s s c~ WEIGHTED<=6 @3
// Process: vm~ d > mu+ d d u~ WEIGHTED<=6 @3
// Process: vm~ s > mu+ s s c~ WEIGHTED<=6 @3
// Process: e+ d > ve~ u u u~ WEIGHTED<=6 @3
// Process: e+ s > ve~ c c c~ WEIGHTED<=6 @3
// Process: mu+ d > vm~ u u u~ WEIGHTED<=6 @3
// Process: mu+ s > vm~ c c c~ WEIGHTED<=6 @3
// Process: ve~ u > e+ d d d~ WEIGHTED<=6 @3
// Process: ve~ c > e+ s s s~ WEIGHTED<=6 @3
// Process: vm~ u > mu+ d d d~ WEIGHTED<=6 @3
// Process: vm~ c > mu+ s s s~ WEIGHTED<=6 @3
// Process: e+ d > ve~ u d d~ WEIGHTED<=6 @3
// Process: e+ s > ve~ c s s~ WEIGHTED<=6 @3
// Process: mu+ d > vm~ u d d~ WEIGHTED<=6 @3
// Process: mu+ s > vm~ c s s~ WEIGHTED<=6 @3
// Process: ve~ u > e+ d u u~ WEIGHTED<=6 @3
// Process: ve~ c > e+ s c c~ WEIGHTED<=6 @3
// Process: vm~ u > mu+ d u u~ WEIGHTED<=6 @3
// Process: vm~ c > mu+ s c c~ WEIGHTED<=6 @3
// Process: e+ u~ > ve~ u u~ d~ WEIGHTED<=6 @3
// Process: e+ c~ > ve~ c c~ s~ WEIGHTED<=6 @3
// Process: mu+ u~ > vm~ u u~ d~ WEIGHTED<=6 @3
// Process: mu+ c~ > vm~ c c~ s~ WEIGHTED<=6 @3
// Process: ve~ d~ > e+ d d~ u~ WEIGHTED<=6 @3
// Process: ve~ s~ > e+ s s~ c~ WEIGHTED<=6 @3
// Process: vm~ d~ > mu+ d d~ u~ WEIGHTED<=6 @3
// Process: vm~ s~ > mu+ s s~ c~ WEIGHTED<=6 @3
// Process: e+ u~ > ve~ d d~ d~ WEIGHTED<=6 @3
// Process: e+ c~ > ve~ s s~ s~ WEIGHTED<=6 @3
// Process: mu+ u~ > vm~ d d~ d~ WEIGHTED<=6 @3
// Process: mu+ c~ > vm~ s s~ s~ WEIGHTED<=6 @3
// Process: ve~ d~ > e+ u u~ u~ WEIGHTED<=6 @3
// Process: ve~ s~ > e+ c c~ c~ WEIGHTED<=6 @3
// Process: vm~ d~ > mu+ u u~ u~ WEIGHTED<=6 @3
// Process: vm~ s~ > mu+ c c~ c~ WEIGHTED<=6 @3
// Process: e+ d~ > ve~ u d~ d~ WEIGHTED<=6 @3
// Process: e+ s~ > ve~ c s~ s~ WEIGHTED<=6 @3
// Process: mu+ d~ > vm~ u d~ d~ WEIGHTED<=6 @3
// Process: mu+ s~ > vm~ c s~ s~ WEIGHTED<=6 @3
// Process: ve~ u~ > e+ d u~ u~ WEIGHTED<=6 @3
// Process: ve~ c~ > e+ s c~ c~ WEIGHTED<=6 @3
// Process: vm~ u~ > mu+ d u~ u~ WEIGHTED<=6 @3
// Process: vm~ c~ > mu+ s c~ c~ WEIGHTED<=6 @3
// Process: ve~ u > ve~ u c c~ WEIGHTED<=6 @3
// Process: ve~ c > ve~ c u u~ WEIGHTED<=6 @3
// Process: vm~ u > vm~ u c c~ WEIGHTED<=6 @3
// Process: vm~ c > vm~ c u u~ WEIGHTED<=6 @3
// Process: vt~ u > vt~ u c c~ WEIGHTED<=6 @3
// Process: vt~ c > vt~ c u u~ WEIGHTED<=6 @3
// Process: ve~ u > ve~ u d d~ WEIGHTED<=6 @3
// Process: ve~ u > ve~ u s s~ WEIGHTED<=6 @3
// Process: ve~ c > ve~ c d d~ WEIGHTED<=6 @3
// Process: ve~ c > ve~ c s s~ WEIGHTED<=6 @3
// Process: vm~ u > vm~ u d d~ WEIGHTED<=6 @3
// Process: vm~ u > vm~ u s s~ WEIGHTED<=6 @3
// Process: vm~ c > vm~ c d d~ WEIGHTED<=6 @3
// Process: vm~ c > vm~ c s s~ WEIGHTED<=6 @3
// Process: vt~ u > vt~ u d d~ WEIGHTED<=6 @3
// Process: vt~ u > vt~ u s s~ WEIGHTED<=6 @3
// Process: vt~ c > vt~ c d d~ WEIGHTED<=6 @3
// Process: vt~ c > vt~ c s s~ WEIGHTED<=6 @3
// Process: ve~ d > ve~ u d u~ WEIGHTED<=6 @3
// Process: ve~ d > ve~ c d c~ WEIGHTED<=6 @3
// Process: ve~ s > ve~ u s u~ WEIGHTED<=6 @3
// Process: ve~ s > ve~ c s c~ WEIGHTED<=6 @3
// Process: vm~ d > vm~ u d u~ WEIGHTED<=6 @3
// Process: vm~ d > vm~ c d c~ WEIGHTED<=6 @3
// Process: vm~ s > vm~ u s u~ WEIGHTED<=6 @3
// Process: vm~ s > vm~ c s c~ WEIGHTED<=6 @3
// Process: vt~ d > vt~ u d u~ WEIGHTED<=6 @3
// Process: vt~ d > vt~ c d c~ WEIGHTED<=6 @3
// Process: vt~ s > vt~ u s u~ WEIGHTED<=6 @3
// Process: vt~ s > vt~ c s c~ WEIGHTED<=6 @3
// Process: ve~ d > ve~ d s s~ WEIGHTED<=6 @3
// Process: ve~ s > ve~ s d d~ WEIGHTED<=6 @3
// Process: vm~ d > vm~ d s s~ WEIGHTED<=6 @3
// Process: vm~ s > vm~ s d d~ WEIGHTED<=6 @3
// Process: vt~ d > vt~ d s s~ WEIGHTED<=6 @3
// Process: vt~ s > vt~ s d d~ WEIGHTED<=6 @3
// Process: ve~ u~ > ve~ c u~ c~ WEIGHTED<=6 @3
// Process: ve~ c~ > ve~ u c~ u~ WEIGHTED<=6 @3
// Process: vm~ u~ > vm~ c u~ c~ WEIGHTED<=6 @3
// Process: vm~ c~ > vm~ u c~ u~ WEIGHTED<=6 @3
// Process: vt~ u~ > vt~ c u~ c~ WEIGHTED<=6 @3
// Process: vt~ c~ > vt~ u c~ u~ WEIGHTED<=6 @3
// Process: ve~ u~ > ve~ d u~ d~ WEIGHTED<=6 @3
// Process: ve~ u~ > ve~ s u~ s~ WEIGHTED<=6 @3
// Process: ve~ c~ > ve~ d c~ d~ WEIGHTED<=6 @3
// Process: ve~ c~ > ve~ s c~ s~ WEIGHTED<=6 @3
// Process: vm~ u~ > vm~ d u~ d~ WEIGHTED<=6 @3
// Process: vm~ u~ > vm~ s u~ s~ WEIGHTED<=6 @3
// Process: vm~ c~ > vm~ d c~ d~ WEIGHTED<=6 @3
// Process: vm~ c~ > vm~ s c~ s~ WEIGHTED<=6 @3
// Process: vt~ u~ > vt~ d u~ d~ WEIGHTED<=6 @3
// Process: vt~ u~ > vt~ s u~ s~ WEIGHTED<=6 @3
// Process: vt~ c~ > vt~ d c~ d~ WEIGHTED<=6 @3
// Process: vt~ c~ > vt~ s c~ s~ WEIGHTED<=6 @3
// Process: ve~ d~ > ve~ u u~ d~ WEIGHTED<=6 @3
// Process: ve~ d~ > ve~ c c~ d~ WEIGHTED<=6 @3
// Process: ve~ s~ > ve~ u u~ s~ WEIGHTED<=6 @3
// Process: ve~ s~ > ve~ c c~ s~ WEIGHTED<=6 @3
// Process: vm~ d~ > vm~ u u~ d~ WEIGHTED<=6 @3
// Process: vm~ d~ > vm~ c c~ d~ WEIGHTED<=6 @3
// Process: vm~ s~ > vm~ u u~ s~ WEIGHTED<=6 @3
// Process: vm~ s~ > vm~ c c~ s~ WEIGHTED<=6 @3
// Process: vt~ d~ > vt~ u u~ d~ WEIGHTED<=6 @3
// Process: vt~ d~ > vt~ c c~ d~ WEIGHTED<=6 @3
// Process: vt~ s~ > vt~ u u~ s~ WEIGHTED<=6 @3
// Process: vt~ s~ > vt~ c c~ s~ WEIGHTED<=6 @3
// Process: ve~ d~ > ve~ s d~ s~ WEIGHTED<=6 @3
// Process: ve~ s~ > ve~ d s~ d~ WEIGHTED<=6 @3
// Process: vm~ d~ > vm~ s d~ s~ WEIGHTED<=6 @3
// Process: vm~ s~ > vm~ d s~ d~ WEIGHTED<=6 @3
// Process: vt~ d~ > vt~ s d~ s~ WEIGHTED<=6 @3
// Process: vt~ s~ > vt~ d s~ d~ WEIGHTED<=6 @3
// Process: e- u > ve u s c~ WEIGHTED<=6 @3
// Process: e- c > ve c d u~ WEIGHTED<=6 @3
// Process: e- d > ve d s c~ WEIGHTED<=6 @3
// Process: e- s > ve s d u~ WEIGHTED<=6 @3
// Process: mu- u > vm u s c~ WEIGHTED<=6 @3
// Process: mu- c > vm c d u~ WEIGHTED<=6 @3
// Process: mu- d > vm d s c~ WEIGHTED<=6 @3
// Process: mu- s > vm s d u~ WEIGHTED<=6 @3
// Process: ve u > e- u c s~ WEIGHTED<=6 @3
// Process: ve c > e- c u d~ WEIGHTED<=6 @3
// Process: ve d > e- d c s~ WEIGHTED<=6 @3
// Process: ve s > e- s u d~ WEIGHTED<=6 @3
// Process: vm u > mu- u c s~ WEIGHTED<=6 @3
// Process: vm c > mu- c u d~ WEIGHTED<=6 @3
// Process: vm d > mu- d c s~ WEIGHTED<=6 @3
// Process: vm s > mu- s u d~ WEIGHTED<=6 @3
// Process: e- u > ve c d c~ WEIGHTED<=6 @3
// Process: e- u > ve s d s~ WEIGHTED<=6 @3
// Process: e- c > ve u s u~ WEIGHTED<=6 @3
// Process: e- c > ve d s d~ WEIGHTED<=6 @3
// Process: mu- u > vm c d c~ WEIGHTED<=6 @3
// Process: mu- u > vm s d s~ WEIGHTED<=6 @3
// Process: mu- c > vm u s u~ WEIGHTED<=6 @3
// Process: mu- c > vm d s d~ WEIGHTED<=6 @3
// Process: ve d > e- c u c~ WEIGHTED<=6 @3
// Process: ve d > e- s u s~ WEIGHTED<=6 @3
// Process: ve s > e- u c u~ WEIGHTED<=6 @3
// Process: ve s > e- d c d~ WEIGHTED<=6 @3
// Process: vm d > mu- c u c~ WEIGHTED<=6 @3
// Process: vm d > mu- s u s~ WEIGHTED<=6 @3
// Process: vm s > mu- u c u~ WEIGHTED<=6 @3
// Process: vm s > mu- d c d~ WEIGHTED<=6 @3
// Process: e- u~ > ve s u~ c~ WEIGHTED<=6 @3
// Process: e- c~ > ve d c~ u~ WEIGHTED<=6 @3
// Process: e- d~ > ve s d~ c~ WEIGHTED<=6 @3
// Process: e- s~ > ve d s~ u~ WEIGHTED<=6 @3
// Process: mu- u~ > vm s u~ c~ WEIGHTED<=6 @3
// Process: mu- c~ > vm d c~ u~ WEIGHTED<=6 @3
// Process: mu- d~ > vm s d~ c~ WEIGHTED<=6 @3
// Process: mu- s~ > vm d s~ u~ WEIGHTED<=6 @3
// Process: ve u~ > e- c u~ s~ WEIGHTED<=6 @3
// Process: ve c~ > e- u c~ d~ WEIGHTED<=6 @3
// Process: ve d~ > e- c d~ s~ WEIGHTED<=6 @3
// Process: ve s~ > e- u s~ d~ WEIGHTED<=6 @3
// Process: vm u~ > mu- c u~ s~ WEIGHTED<=6 @3
// Process: vm c~ > mu- u c~ d~ WEIGHTED<=6 @3
// Process: vm d~ > mu- c d~ s~ WEIGHTED<=6 @3
// Process: vm s~ > mu- u s~ d~ WEIGHTED<=6 @3
// Process: e- d~ > ve c u~ c~ WEIGHTED<=6 @3
// Process: e- d~ > ve s u~ s~ WEIGHTED<=6 @3
// Process: e- s~ > ve u c~ u~ WEIGHTED<=6 @3
// Process: e- s~ > ve d c~ d~ WEIGHTED<=6 @3
// Process: mu- d~ > vm c u~ c~ WEIGHTED<=6 @3
// Process: mu- d~ > vm s u~ s~ WEIGHTED<=6 @3
// Process: mu- s~ > vm u c~ u~ WEIGHTED<=6 @3
// Process: mu- s~ > vm d c~ d~ WEIGHTED<=6 @3
// Process: ve u~ > e- c d~ c~ WEIGHTED<=6 @3
// Process: ve u~ > e- s d~ s~ WEIGHTED<=6 @3
// Process: ve c~ > e- u s~ u~ WEIGHTED<=6 @3
// Process: ve c~ > e- d s~ d~ WEIGHTED<=6 @3
// Process: vm u~ > mu- c d~ c~ WEIGHTED<=6 @3
// Process: vm u~ > mu- s d~ s~ WEIGHTED<=6 @3
// Process: vm c~ > mu- u s~ u~ WEIGHTED<=6 @3
// Process: vm c~ > mu- d s~ d~ WEIGHTED<=6 @3
// Process: e+ u > ve~ u c s~ WEIGHTED<=6 @3
// Process: e+ c > ve~ c u d~ WEIGHTED<=6 @3
// Process: e+ d > ve~ d c s~ WEIGHTED<=6 @3
// Process: e+ s > ve~ s u d~ WEIGHTED<=6 @3
// Process: mu+ u > vm~ u c s~ WEIGHTED<=6 @3
// Process: mu+ c > vm~ c u d~ WEIGHTED<=6 @3
// Process: mu+ d > vm~ d c s~ WEIGHTED<=6 @3
// Process: mu+ s > vm~ s u d~ WEIGHTED<=6 @3
// Process: ve~ u > e+ u s c~ WEIGHTED<=6 @3
// Process: ve~ c > e+ c d u~ WEIGHTED<=6 @3
// Process: ve~ d > e+ d s c~ WEIGHTED<=6 @3
// Process: ve~ s > e+ s d u~ WEIGHTED<=6 @3
// Process: vm~ u > mu+ u s c~ WEIGHTED<=6 @3
// Process: vm~ c > mu+ c d u~ WEIGHTED<=6 @3
// Process: vm~ d > mu+ d s c~ WEIGHTED<=6 @3
// Process: vm~ s > mu+ s d u~ WEIGHTED<=6 @3
// Process: e+ d > ve~ u c c~ WEIGHTED<=6 @3
// Process: e+ d > ve~ u s s~ WEIGHTED<=6 @3
// Process: e+ s > ve~ c u u~ WEIGHTED<=6 @3
// Process: e+ s > ve~ c d d~ WEIGHTED<=6 @3
// Process: mu+ d > vm~ u c c~ WEIGHTED<=6 @3
// Process: mu+ d > vm~ u s s~ WEIGHTED<=6 @3
// Process: mu+ s > vm~ c u u~ WEIGHTED<=6 @3
// Process: mu+ s > vm~ c d d~ WEIGHTED<=6 @3
// Process: ve~ u > e+ d c c~ WEIGHTED<=6 @3
// Process: ve~ u > e+ d s s~ WEIGHTED<=6 @3
// Process: ve~ c > e+ s u u~ WEIGHTED<=6 @3
// Process: ve~ c > e+ s d d~ WEIGHTED<=6 @3
// Process: vm~ u > mu+ d c c~ WEIGHTED<=6 @3
// Process: vm~ u > mu+ d s s~ WEIGHTED<=6 @3
// Process: vm~ c > mu+ s u u~ WEIGHTED<=6 @3
// Process: vm~ c > mu+ s d d~ WEIGHTED<=6 @3
// Process: e+ u~ > ve~ c u~ s~ WEIGHTED<=6 @3
// Process: e+ c~ > ve~ u c~ d~ WEIGHTED<=6 @3
// Process: e+ d~ > ve~ c d~ s~ WEIGHTED<=6 @3
// Process: e+ s~ > ve~ u s~ d~ WEIGHTED<=6 @3
// Process: mu+ u~ > vm~ c u~ s~ WEIGHTED<=6 @3
// Process: mu+ c~ > vm~ u c~ d~ WEIGHTED<=6 @3
// Process: mu+ d~ > vm~ c d~ s~ WEIGHTED<=6 @3
// Process: mu+ s~ > vm~ u s~ d~ WEIGHTED<=6 @3
// Process: ve~ u~ > e+ s u~ c~ WEIGHTED<=6 @3
// Process: ve~ c~ > e+ d c~ u~ WEIGHTED<=6 @3
// Process: ve~ d~ > e+ s d~ c~ WEIGHTED<=6 @3
// Process: ve~ s~ > e+ d s~ u~ WEIGHTED<=6 @3
// Process: vm~ u~ > mu+ s u~ c~ WEIGHTED<=6 @3
// Process: vm~ c~ > mu+ d c~ u~ WEIGHTED<=6 @3
// Process: vm~ d~ > mu+ s d~ c~ WEIGHTED<=6 @3
// Process: vm~ s~ > mu+ d s~ u~ WEIGHTED<=6 @3
// Process: e+ u~ > ve~ c c~ d~ WEIGHTED<=6 @3
// Process: e+ u~ > ve~ s s~ d~ WEIGHTED<=6 @3
// Process: e+ c~ > ve~ u u~ s~ WEIGHTED<=6 @3
// Process: e+ c~ > ve~ d d~ s~ WEIGHTED<=6 @3
// Process: mu+ u~ > vm~ c c~ d~ WEIGHTED<=6 @3
// Process: mu+ u~ > vm~ s s~ d~ WEIGHTED<=6 @3
// Process: mu+ c~ > vm~ u u~ s~ WEIGHTED<=6 @3
// Process: mu+ c~ > vm~ d d~ s~ WEIGHTED<=6 @3
// Process: ve~ d~ > e+ c c~ u~ WEIGHTED<=6 @3
// Process: ve~ d~ > e+ s s~ u~ WEIGHTED<=6 @3
// Process: ve~ s~ > e+ u u~ c~ WEIGHTED<=6 @3
// Process: ve~ s~ > e+ d d~ c~ WEIGHTED<=6 @3
// Process: vm~ d~ > mu+ c c~ u~ WEIGHTED<=6 @3
// Process: vm~ d~ > mu+ s s~ u~ WEIGHTED<=6 @3
// Process: vm~ s~ > mu+ u u~ c~ WEIGHTED<=6 @3
// Process: vm~ s~ > mu+ d d~ c~ WEIGHTED<=6 @3
//--------------------------------------------------------------------------

typedef vector<double> vec_double; 
typedef vector < vec_double > vec_vec_double; 
typedef vector<int> vec_int; 
typedef vector<bool> vec_bool; 
typedef vector < vec_int > vec_vec_int; 

class PY8MEs_R3_P36_sm_lq_lqqq : public PY8ME
{
  public:

    // Check for the availability of the requested proces.
    // If available, this returns the corresponding permutation and Proc_ID  to
    // use.
    // If not available, this returns a negative Proc_ID.
    static pair < vector<int> , int > static_getPY8ME(vector<int> initial_pdgs,
        vector<int> final_pdgs, set<int> schannels = set<int> ());

    // Constructor.
    PY8MEs_R3_P36_sm_lq_lqqq(Parameters_sm * model) : pars(model) {initProc();}

    // Destructor.
    ~PY8MEs_R3_P36_sm_lq_lqqq(); 

    // Initialize process.
    virtual void initProc(); 

    // Calculate squared ME.
    virtual double sigmaKin(); 

    // Info on the subprocess.
    virtual string name() const {return "lq_lqqq (sm)";}

    virtual int code() const {return 10336;}

    virtual string inFlux() const {return "ff";}

    virtual vector<double> getMasses(); 

    virtual void setMasses(vec_double external_masses); 
    // Set all values of the external masses to an integer mode:
    // 0 : Mass taken from the model
    // 1 : Mass taken from p_i^2 if not massless to begin with
    // 2 : Mass always taken from p_i^2.
    virtual void setExternalMassesMode(int mode); 

    // Synchronize local variables of the process that depend on the model
    // parameters
    virtual void syncProcModelParams(); 

    // Tell Pythia that sigmaHat returns the ME^2
    virtual bool convertM2() const {return true;}

    // Access to getPY8ME with polymorphism from a non-static context
    virtual pair < vector<int> , int > getPY8ME(vector<int> initial_pdgs,
        vector<int> final_pdgs, set<int> schannels = set<int> ())
    {
      return static_getPY8ME(initial_pdgs, final_pdgs, schannels); 
    }

    // Set momenta
    virtual void setMomenta(vector < vec_double > momenta_picked); 

    // Set color configuration to use. An empty vector means sum over all.
    virtual void setColors(vector<int> colors_picked); 

    // Set the helicity configuration to use. Am empty vector means sum over
    // all.
    virtual void setHelicities(vector<int> helicities_picked); 

    // Set the permutation to use (will apply to momenta, colors and helicities)
    virtual void setPermutation(vector<int> perm_picked); 

    // Set the proc_ID to use
    virtual void setProcID(int procID_picked); 

    // Access to all the helicity and color configurations for a given process
    virtual vector < vec_int > getColorConfigs(int specify_proc_ID = -1,
        vector<int> permutation = vector<int> ());
    virtual vector < vec_int > getHelicityConfigs(vector<int> permutation =
        vector<int> ());

    // Maps of Helicity <-> hel_ID and ColorConfig <-> colorConfig_ID.
    virtual vector<int> getHelicityConfigForID(int hel_ID, vector<int>
        permutation = vector<int> ());
    virtual int getHelicityIDForConfig(vector<int> hel_config, vector<int>
        permutation = vector<int> ());
    virtual vector<int> getColorConfigForID(int color_ID, int specify_proc_ID =
        -1, vector<int> permutation = vector<int> ());
    virtual int getColorIDForConfig(vector<int> color_config, int
        specify_proc_ID = -1, vector<int> permutation = vector<int> ());
    virtual int getColorFlowRelativeNCPower(int color_flow_ID, int
        specify_proc_ID = -1);

    // Access previously computed results
    virtual vector < vec_double > getAllResults(int specify_proc_ID = -1); 
    virtual double getResult(int helicity_ID, int color_ID, int specify_proc_ID
        = -1);

    // Accessors
    Parameters_sm * getModel() {return pars;}
    void setModel(Parameters_sm * model) {pars = model;}

    // Invert the permutation mapping
    vector<int> invert_mapping(vector<int> mapping); 

    // Control whether to include the symmetry factors or not
    virtual void setIncludeSymmetryFactors(bool OnOff) 
    {
      include_symmetry_factors = OnOff; 
    }
    virtual bool getIncludeSymmetryFactors() {return include_symmetry_factors;}
    virtual int getSymmetryFactor() {return denom_iden[proc_ID];}

    // Control whether to include helicity averaging factors or not
    virtual void setIncludeHelicityAveragingFactors(bool OnOff) 
    {
      include_helicity_averaging_factors = OnOff; 
    }
    virtual bool getIncludeHelicityAveragingFactors() 
    {
      return include_helicity_averaging_factors; 
    }
    virtual int getHelicityAveragingFactor() {return denom_hels[proc_ID];}

    // Control whether to include color averaging factors or not
    virtual void setIncludeColorAveragingFactors(bool OnOff) 
    {
      include_color_averaging_factors = OnOff; 
    }
    virtual bool getIncludeColorAveragingFactors() 
    {
      return include_color_averaging_factors; 
    }
    virtual int getColorAveragingFactor() {return denom_colors[proc_ID];}

  private:

    // Private functions to calculate the matrix element for all subprocesses
    // Calculate wavefunctions
    void calculate_wavefunctions(const int hel[]); 
    static const int nwavefuncs = 109; 
    Complex<double> w[nwavefuncs][18]; 
    static const int namplitudes = 444; 
    Complex<double> amp[namplitudes]; 
    double matrix_3_emu_emuuux(); 
    double matrix_3_emd_emdddx(); 
    double matrix_3_emux_emuuxux(); 
    double matrix_3_emdx_emddxdx(); 
    double matrix_3_epu_epuuux(); 
    double matrix_3_epd_epdddx(); 
    double matrix_3_epux_epuuxux(); 
    double matrix_3_epdx_epddxdx(); 
    double matrix_3_emu_emuccx(); 
    double matrix_3_emu_emuddx(); 
    double matrix_3_emd_emudux(); 
    double matrix_3_emd_emdssx(); 
    double matrix_3_emux_emcuxcx(); 
    double matrix_3_emux_emduxdx(); 
    double matrix_3_emdx_emuuxdx(); 
    double matrix_3_emdx_emsdxsx(); 
    double matrix_3_veu_veuuux(); 
    double matrix_3_ved_vedddx(); 
    double matrix_3_veux_veuuxux(); 
    double matrix_3_vedx_veddxdx(); 
    double matrix_3_epu_epuccx(); 
    double matrix_3_epu_epuddx(); 
    double matrix_3_epd_epudux(); 
    double matrix_3_epd_epdssx(); 
    double matrix_3_epux_epcuxcx(); 
    double matrix_3_epux_epduxdx(); 
    double matrix_3_epdx_epuuxdx(); 
    double matrix_3_epdx_epsdxsx(); 
    double matrix_3_vexu_vexuuux(); 
    double matrix_3_vexd_vexdddx(); 
    double matrix_3_vexux_vexuuxux(); 
    double matrix_3_vexdx_vexddxdx(); 
    double matrix_3_emu_veudux(); 
    double matrix_3_emu_vedddx(); 
    double matrix_3_emd_veddux(); 
    double matrix_3_emux_veduxux(); 
    double matrix_3_emdx_veuuxux(); 
    double matrix_3_emdx_veduxdx(); 
    double matrix_3_veu_veuccx(); 
    double matrix_3_veu_veuddx(); 
    double matrix_3_ved_veudux(); 
    double matrix_3_ved_vedssx(); 
    double matrix_3_veux_vecuxcx(); 
    double matrix_3_veux_veduxdx(); 
    double matrix_3_vedx_veuuxdx(); 
    double matrix_3_vedx_vesdxsx(); 
    double matrix_3_epu_vexuudx(); 
    double matrix_3_epd_vexuuux(); 
    double matrix_3_epd_vexuddx(); 
    double matrix_3_epux_vexuuxdx(); 
    double matrix_3_epux_vexddxdx(); 
    double matrix_3_epdx_vexudxdx(); 
    double matrix_3_vexu_vexuccx(); 
    double matrix_3_vexu_vexuddx(); 
    double matrix_3_vexd_vexudux(); 
    double matrix_3_vexd_vexdssx(); 
    double matrix_3_vexux_vexcuxcx(); 
    double matrix_3_vexux_vexduxdx(); 
    double matrix_3_vexdx_vexuuxdx(); 
    double matrix_3_vexdx_vexsdxsx(); 
    double matrix_3_emu_veuscx(); 
    double matrix_3_emu_vecdcx(); 
    double matrix_3_emux_vesuxcx(); 
    double matrix_3_emdx_vecuxcx(); 
    double matrix_3_epu_vexucsx(); 
    double matrix_3_epd_vexuccx(); 
    double matrix_3_epux_vexcuxsx(); 
    double matrix_3_epux_vexccxdx(); 

    // Constants for array limits
    static const int nexternal = 6; 
    static const int ninitial = 2; 
    static const int nprocesses = 68; 
    static const int nreq_s_channels = 0; 
    static const int ncomb = 64; 

    // Helicities for the process
    static int helicities[ncomb][nexternal]; 

    // Normalization factors the various processes
    static int denom_colors[nprocesses]; 
    static int denom_hels[nprocesses]; 
    static int denom_iden[nprocesses]; 

    // Control whether to include symmetry factors or not
    bool include_symmetry_factors; 
    // Control whether to include helicity averaging factors or not
    bool include_helicity_averaging_factors; 
    // Control whether to include color averaging factors or not
    bool include_color_averaging_factors; 

    // Color flows, used when selecting color
    vector < vec_double > jamp2; 

    // Store individual results (for each color flow, helicity configurations
    // and proc_ID)
    // computed in the last call to sigmaKin().
    vector < vec_vec_double > all_results; 

    // required s-channels specified
    static std::set<int> s_channel_proc; 

    // vector with external particle masses
    vector<double> mME; 

    // vector with momenta (to be changed for each event)
    vector < double * > p; 

    // external particles permutation (to be changed for each event)
    vector<int> perm; 

    // vector with colors (to be changed for each event)
    vector<int> user_colors; 

    // vector with helicities (to be changed for each event)
    vector<int> user_helicities; 

    // Process ID (to be changed for each event)
    int proc_ID; 

    // All color configurations
    void initColorConfigs(); 
    vector < vec_vec_int > color_configs; 

    // Color flows relative N_c power (conventions are such that all elements
    // on the color matrix diagonal are identical).
    vector < vec_int > jamp_nc_relative_power; 

    // Model pointer to be used by this matrix element
    Parameters_sm * pars; 

}; 

}  // end namespace PY8MEs_namespace

#endif  // PY8MEs_R3_P36_sm_lq_lqqq_H

