//==========================================================================
// This file has been automatically generated for Pythia 8 by
// MadGraph5_aMC@NLO v. 2.6.0, 2017-08-16
// By the MadGraph5_aMC@NLO Development Team
// Visit launchpad.net/madgraph5 and amcatnlo.web.cern.ch
//==========================================================================

#include "PY8MEs_R4_P4_sm_lg_lggqq.h"
#include "HelAmps_sm.h"

using namespace Pythia8_sm; 

namespace PY8MEs_namespace 
{
//==========================================================================
// Class member functions for calculating the matrix elements for
// Process: e- g > e- g g u u~ WEIGHTED<=7 @4
// Process: e- g > e- g g c c~ WEIGHTED<=7 @4
// Process: mu- g > mu- g g u u~ WEIGHTED<=7 @4
// Process: mu- g > mu- g g c c~ WEIGHTED<=7 @4
// Process: e- g > e- g g d d~ WEIGHTED<=7 @4
// Process: e- g > e- g g s s~ WEIGHTED<=7 @4
// Process: mu- g > mu- g g d d~ WEIGHTED<=7 @4
// Process: mu- g > mu- g g s s~ WEIGHTED<=7 @4
// Process: e+ g > e+ g g u u~ WEIGHTED<=7 @4
// Process: e+ g > e+ g g c c~ WEIGHTED<=7 @4
// Process: mu+ g > mu+ g g u u~ WEIGHTED<=7 @4
// Process: mu+ g > mu+ g g c c~ WEIGHTED<=7 @4
// Process: e+ g > e+ g g d d~ WEIGHTED<=7 @4
// Process: e+ g > e+ g g s s~ WEIGHTED<=7 @4
// Process: mu+ g > mu+ g g d d~ WEIGHTED<=7 @4
// Process: mu+ g > mu+ g g s s~ WEIGHTED<=7 @4
// Process: e- g > ve g g d u~ WEIGHTED<=7 @4
// Process: e- g > ve g g s c~ WEIGHTED<=7 @4
// Process: mu- g > vm g g d u~ WEIGHTED<=7 @4
// Process: mu- g > vm g g s c~ WEIGHTED<=7 @4
// Process: ve g > e- g g u d~ WEIGHTED<=7 @4
// Process: ve g > e- g g c s~ WEIGHTED<=7 @4
// Process: vm g > mu- g g u d~ WEIGHTED<=7 @4
// Process: vm g > mu- g g c s~ WEIGHTED<=7 @4
// Process: ve g > ve g g u u~ WEIGHTED<=7 @4
// Process: ve g > ve g g c c~ WEIGHTED<=7 @4
// Process: vm g > vm g g u u~ WEIGHTED<=7 @4
// Process: vm g > vm g g c c~ WEIGHTED<=7 @4
// Process: vt g > vt g g u u~ WEIGHTED<=7 @4
// Process: vt g > vt g g c c~ WEIGHTED<=7 @4
// Process: ve g > ve g g d d~ WEIGHTED<=7 @4
// Process: ve g > ve g g s s~ WEIGHTED<=7 @4
// Process: vm g > vm g g d d~ WEIGHTED<=7 @4
// Process: vm g > vm g g s s~ WEIGHTED<=7 @4
// Process: vt g > vt g g d d~ WEIGHTED<=7 @4
// Process: vt g > vt g g s s~ WEIGHTED<=7 @4
// Process: e+ g > ve~ g g u d~ WEIGHTED<=7 @4
// Process: e+ g > ve~ g g c s~ WEIGHTED<=7 @4
// Process: mu+ g > vm~ g g u d~ WEIGHTED<=7 @4
// Process: mu+ g > vm~ g g c s~ WEIGHTED<=7 @4
// Process: ve~ g > e+ g g d u~ WEIGHTED<=7 @4
// Process: ve~ g > e+ g g s c~ WEIGHTED<=7 @4
// Process: vm~ g > mu+ g g d u~ WEIGHTED<=7 @4
// Process: vm~ g > mu+ g g s c~ WEIGHTED<=7 @4
// Process: ve~ g > ve~ g g u u~ WEIGHTED<=7 @4
// Process: ve~ g > ve~ g g c c~ WEIGHTED<=7 @4
// Process: vm~ g > vm~ g g u u~ WEIGHTED<=7 @4
// Process: vm~ g > vm~ g g c c~ WEIGHTED<=7 @4
// Process: vt~ g > vt~ g g u u~ WEIGHTED<=7 @4
// Process: vt~ g > vt~ g g c c~ WEIGHTED<=7 @4
// Process: ve~ g > ve~ g g d d~ WEIGHTED<=7 @4
// Process: ve~ g > ve~ g g s s~ WEIGHTED<=7 @4
// Process: vm~ g > vm~ g g d d~ WEIGHTED<=7 @4
// Process: vm~ g > vm~ g g s s~ WEIGHTED<=7 @4
// Process: vt~ g > vt~ g g d d~ WEIGHTED<=7 @4
// Process: vt~ g > vt~ g g s s~ WEIGHTED<=7 @4

// Exception class
class PY8MEs_R4_P4_sm_lg_lggqqException : public exception
{
  virtual const char * what() const throw()
  {
    return "Exception in class 'PY8MEs_R4_P4_sm_lg_lggqq'."; 
  }
}
PY8MEs_R4_P4_sm_lg_lggqq_exception; 

std::set<int> PY8MEs_R4_P4_sm_lg_lggqq::s_channel_proc = std::set<int>
    (createset<int> ());

int PY8MEs_R4_P4_sm_lg_lggqq::helicities[ncomb][nexternal] = {{-1, -1, -1, -1,
    -1, -1, -1}, {-1, -1, -1, -1, -1, -1, 1}, {-1, -1, -1, -1, -1, 1, -1}, {-1,
    -1, -1, -1, -1, 1, 1}, {-1, -1, -1, -1, 1, -1, -1}, {-1, -1, -1, -1, 1, -1,
    1}, {-1, -1, -1, -1, 1, 1, -1}, {-1, -1, -1, -1, 1, 1, 1}, {-1, -1, -1, 1,
    -1, -1, -1}, {-1, -1, -1, 1, -1, -1, 1}, {-1, -1, -1, 1, -1, 1, -1}, {-1,
    -1, -1, 1, -1, 1, 1}, {-1, -1, -1, 1, 1, -1, -1}, {-1, -1, -1, 1, 1, -1,
    1}, {-1, -1, -1, 1, 1, 1, -1}, {-1, -1, -1, 1, 1, 1, 1}, {-1, -1, 1, -1,
    -1, -1, -1}, {-1, -1, 1, -1, -1, -1, 1}, {-1, -1, 1, -1, -1, 1, -1}, {-1,
    -1, 1, -1, -1, 1, 1}, {-1, -1, 1, -1, 1, -1, -1}, {-1, -1, 1, -1, 1, -1,
    1}, {-1, -1, 1, -1, 1, 1, -1}, {-1, -1, 1, -1, 1, 1, 1}, {-1, -1, 1, 1, -1,
    -1, -1}, {-1, -1, 1, 1, -1, -1, 1}, {-1, -1, 1, 1, -1, 1, -1}, {-1, -1, 1,
    1, -1, 1, 1}, {-1, -1, 1, 1, 1, -1, -1}, {-1, -1, 1, 1, 1, -1, 1}, {-1, -1,
    1, 1, 1, 1, -1}, {-1, -1, 1, 1, 1, 1, 1}, {-1, 1, -1, -1, -1, -1, -1}, {-1,
    1, -1, -1, -1, -1, 1}, {-1, 1, -1, -1, -1, 1, -1}, {-1, 1, -1, -1, -1, 1,
    1}, {-1, 1, -1, -1, 1, -1, -1}, {-1, 1, -1, -1, 1, -1, 1}, {-1, 1, -1, -1,
    1, 1, -1}, {-1, 1, -1, -1, 1, 1, 1}, {-1, 1, -1, 1, -1, -1, -1}, {-1, 1,
    -1, 1, -1, -1, 1}, {-1, 1, -1, 1, -1, 1, -1}, {-1, 1, -1, 1, -1, 1, 1},
    {-1, 1, -1, 1, 1, -1, -1}, {-1, 1, -1, 1, 1, -1, 1}, {-1, 1, -1, 1, 1, 1,
    -1}, {-1, 1, -1, 1, 1, 1, 1}, {-1, 1, 1, -1, -1, -1, -1}, {-1, 1, 1, -1,
    -1, -1, 1}, {-1, 1, 1, -1, -1, 1, -1}, {-1, 1, 1, -1, -1, 1, 1}, {-1, 1, 1,
    -1, 1, -1, -1}, {-1, 1, 1, -1, 1, -1, 1}, {-1, 1, 1, -1, 1, 1, -1}, {-1, 1,
    1, -1, 1, 1, 1}, {-1, 1, 1, 1, -1, -1, -1}, {-1, 1, 1, 1, -1, -1, 1}, {-1,
    1, 1, 1, -1, 1, -1}, {-1, 1, 1, 1, -1, 1, 1}, {-1, 1, 1, 1, 1, -1, -1},
    {-1, 1, 1, 1, 1, -1, 1}, {-1, 1, 1, 1, 1, 1, -1}, {-1, 1, 1, 1, 1, 1, 1},
    {1, -1, -1, -1, -1, -1, -1}, {1, -1, -1, -1, -1, -1, 1}, {1, -1, -1, -1,
    -1, 1, -1}, {1, -1, -1, -1, -1, 1, 1}, {1, -1, -1, -1, 1, -1, -1}, {1, -1,
    -1, -1, 1, -1, 1}, {1, -1, -1, -1, 1, 1, -1}, {1, -1, -1, -1, 1, 1, 1}, {1,
    -1, -1, 1, -1, -1, -1}, {1, -1, -1, 1, -1, -1, 1}, {1, -1, -1, 1, -1, 1,
    -1}, {1, -1, -1, 1, -1, 1, 1}, {1, -1, -1, 1, 1, -1, -1}, {1, -1, -1, 1, 1,
    -1, 1}, {1, -1, -1, 1, 1, 1, -1}, {1, -1, -1, 1, 1, 1, 1}, {1, -1, 1, -1,
    -1, -1, -1}, {1, -1, 1, -1, -1, -1, 1}, {1, -1, 1, -1, -1, 1, -1}, {1, -1,
    1, -1, -1, 1, 1}, {1, -1, 1, -1, 1, -1, -1}, {1, -1, 1, -1, 1, -1, 1}, {1,
    -1, 1, -1, 1, 1, -1}, {1, -1, 1, -1, 1, 1, 1}, {1, -1, 1, 1, -1, -1, -1},
    {1, -1, 1, 1, -1, -1, 1}, {1, -1, 1, 1, -1, 1, -1}, {1, -1, 1, 1, -1, 1,
    1}, {1, -1, 1, 1, 1, -1, -1}, {1, -1, 1, 1, 1, -1, 1}, {1, -1, 1, 1, 1, 1,
    -1}, {1, -1, 1, 1, 1, 1, 1}, {1, 1, -1, -1, -1, -1, -1}, {1, 1, -1, -1, -1,
    -1, 1}, {1, 1, -1, -1, -1, 1, -1}, {1, 1, -1, -1, -1, 1, 1}, {1, 1, -1, -1,
    1, -1, -1}, {1, 1, -1, -1, 1, -1, 1}, {1, 1, -1, -1, 1, 1, -1}, {1, 1, -1,
    -1, 1, 1, 1}, {1, 1, -1, 1, -1, -1, -1}, {1, 1, -1, 1, -1, -1, 1}, {1, 1,
    -1, 1, -1, 1, -1}, {1, 1, -1, 1, -1, 1, 1}, {1, 1, -1, 1, 1, -1, -1}, {1,
    1, -1, 1, 1, -1, 1}, {1, 1, -1, 1, 1, 1, -1}, {1, 1, -1, 1, 1, 1, 1}, {1,
    1, 1, -1, -1, -1, -1}, {1, 1, 1, -1, -1, -1, 1}, {1, 1, 1, -1, -1, 1, -1},
    {1, 1, 1, -1, -1, 1, 1}, {1, 1, 1, -1, 1, -1, -1}, {1, 1, 1, -1, 1, -1, 1},
    {1, 1, 1, -1, 1, 1, -1}, {1, 1, 1, -1, 1, 1, 1}, {1, 1, 1, 1, -1, -1, -1},
    {1, 1, 1, 1, -1, -1, 1}, {1, 1, 1, 1, -1, 1, -1}, {1, 1, 1, 1, -1, 1, 1},
    {1, 1, 1, 1, 1, -1, -1}, {1, 1, 1, 1, 1, -1, 1}, {1, 1, 1, 1, 1, 1, -1},
    {1, 1, 1, 1, 1, 1, 1}};

// Normalization factors the various processes
// Denominators: spins, colors and identical particles
int PY8MEs_R4_P4_sm_lg_lggqq::denom_colors[nprocesses] = {8, 8, 8, 8, 8, 8, 8,
    8, 8, 8};
int PY8MEs_R4_P4_sm_lg_lggqq::denom_hels[nprocesses] = {4, 4, 4, 4, 4, 4, 4, 4,
    4, 4};
int PY8MEs_R4_P4_sm_lg_lggqq::denom_iden[nprocesses] = {2, 2, 2, 2, 2, 2, 2, 2,
    2, 2};

//--------------------------------------------------------------------------
// Color config initialization
void PY8MEs_R4_P4_sm_lg_lggqq::initColorConfigs() 
{
  color_configs = vector < vec_vec_int > (); 
  jamp_nc_relative_power = vector < vec_int > (); 

  // Color flows of process Process: e- g > e- g g u u~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[0].push_back(vec_int(createvector<int>
      (0)(0)(1)(2)(0)(0)(3)(2)(4)(3)(1)(0)(0)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #1
  color_configs[0].push_back(vec_int(createvector<int>
      (0)(0)(1)(2)(0)(0)(3)(4)(4)(2)(1)(0)(0)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #2
  color_configs[0].push_back(vec_int(createvector<int>
      (0)(0)(3)(2)(0)(0)(3)(1)(4)(2)(1)(0)(0)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #3
  color_configs[0].push_back(vec_int(createvector<int>
      (0)(0)(4)(2)(0)(0)(3)(1)(4)(3)(1)(0)(0)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #4
  color_configs[0].push_back(vec_int(createvector<int>
      (0)(0)(4)(2)(0)(0)(3)(2)(4)(1)(1)(0)(0)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #5
  color_configs[0].push_back(vec_int(createvector<int>
      (0)(0)(3)(2)(0)(0)(3)(4)(4)(1)(1)(0)(0)(2)));
  jamp_nc_relative_power[0].push_back(0); 

  // Color flows of process Process: e- g > e- g g d d~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[1].push_back(vec_int(createvector<int>
      (0)(0)(1)(2)(0)(0)(3)(2)(4)(3)(1)(0)(0)(4)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #1
  color_configs[1].push_back(vec_int(createvector<int>
      (0)(0)(1)(2)(0)(0)(3)(4)(4)(2)(1)(0)(0)(3)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #2
  color_configs[1].push_back(vec_int(createvector<int>
      (0)(0)(3)(2)(0)(0)(3)(1)(4)(2)(1)(0)(0)(4)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #3
  color_configs[1].push_back(vec_int(createvector<int>
      (0)(0)(4)(2)(0)(0)(3)(1)(4)(3)(1)(0)(0)(2)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #4
  color_configs[1].push_back(vec_int(createvector<int>
      (0)(0)(4)(2)(0)(0)(3)(2)(4)(1)(1)(0)(0)(3)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #5
  color_configs[1].push_back(vec_int(createvector<int>
      (0)(0)(3)(2)(0)(0)(3)(4)(4)(1)(1)(0)(0)(2)));
  jamp_nc_relative_power[1].push_back(0); 

  // Color flows of process Process: e+ g > e+ g g u u~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(0)(1)(2)(0)(0)(3)(2)(4)(3)(1)(0)(0)(4)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #1
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(0)(1)(2)(0)(0)(3)(4)(4)(2)(1)(0)(0)(3)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #2
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(0)(3)(2)(0)(0)(3)(1)(4)(2)(1)(0)(0)(4)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #3
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(0)(4)(2)(0)(0)(3)(1)(4)(3)(1)(0)(0)(2)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #4
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(0)(4)(2)(0)(0)(3)(2)(4)(1)(1)(0)(0)(3)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #5
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(0)(3)(2)(0)(0)(3)(4)(4)(1)(1)(0)(0)(2)));
  jamp_nc_relative_power[2].push_back(0); 

  // Color flows of process Process: e+ g > e+ g g d d~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[3].push_back(vec_int(createvector<int>
      (0)(0)(1)(2)(0)(0)(3)(2)(4)(3)(1)(0)(0)(4)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #1
  color_configs[3].push_back(vec_int(createvector<int>
      (0)(0)(1)(2)(0)(0)(3)(4)(4)(2)(1)(0)(0)(3)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #2
  color_configs[3].push_back(vec_int(createvector<int>
      (0)(0)(3)(2)(0)(0)(3)(1)(4)(2)(1)(0)(0)(4)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #3
  color_configs[3].push_back(vec_int(createvector<int>
      (0)(0)(4)(2)(0)(0)(3)(1)(4)(3)(1)(0)(0)(2)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #4
  color_configs[3].push_back(vec_int(createvector<int>
      (0)(0)(4)(2)(0)(0)(3)(2)(4)(1)(1)(0)(0)(3)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #5
  color_configs[3].push_back(vec_int(createvector<int>
      (0)(0)(3)(2)(0)(0)(3)(4)(4)(1)(1)(0)(0)(2)));
  jamp_nc_relative_power[3].push_back(0); 

  // Color flows of process Process: e- g > ve g g d u~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[4].push_back(vec_int(createvector<int>
      (0)(0)(1)(2)(0)(0)(3)(2)(4)(3)(1)(0)(0)(4)));
  jamp_nc_relative_power[4].push_back(0); 
  // JAMP #1
  color_configs[4].push_back(vec_int(createvector<int>
      (0)(0)(1)(2)(0)(0)(3)(4)(4)(2)(1)(0)(0)(3)));
  jamp_nc_relative_power[4].push_back(0); 
  // JAMP #2
  color_configs[4].push_back(vec_int(createvector<int>
      (0)(0)(3)(2)(0)(0)(3)(1)(4)(2)(1)(0)(0)(4)));
  jamp_nc_relative_power[4].push_back(0); 
  // JAMP #3
  color_configs[4].push_back(vec_int(createvector<int>
      (0)(0)(4)(2)(0)(0)(3)(1)(4)(3)(1)(0)(0)(2)));
  jamp_nc_relative_power[4].push_back(0); 
  // JAMP #4
  color_configs[4].push_back(vec_int(createvector<int>
      (0)(0)(4)(2)(0)(0)(3)(2)(4)(1)(1)(0)(0)(3)));
  jamp_nc_relative_power[4].push_back(0); 
  // JAMP #5
  color_configs[4].push_back(vec_int(createvector<int>
      (0)(0)(3)(2)(0)(0)(3)(4)(4)(1)(1)(0)(0)(2)));
  jamp_nc_relative_power[4].push_back(0); 

  // Color flows of process Process: ve g > ve g g u u~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[5].push_back(vec_int(createvector<int>
      (0)(0)(1)(2)(0)(0)(3)(2)(4)(3)(1)(0)(0)(4)));
  jamp_nc_relative_power[5].push_back(0); 
  // JAMP #1
  color_configs[5].push_back(vec_int(createvector<int>
      (0)(0)(1)(2)(0)(0)(3)(4)(4)(2)(1)(0)(0)(3)));
  jamp_nc_relative_power[5].push_back(0); 
  // JAMP #2
  color_configs[5].push_back(vec_int(createvector<int>
      (0)(0)(3)(2)(0)(0)(3)(1)(4)(2)(1)(0)(0)(4)));
  jamp_nc_relative_power[5].push_back(0); 
  // JAMP #3
  color_configs[5].push_back(vec_int(createvector<int>
      (0)(0)(4)(2)(0)(0)(3)(1)(4)(3)(1)(0)(0)(2)));
  jamp_nc_relative_power[5].push_back(0); 
  // JAMP #4
  color_configs[5].push_back(vec_int(createvector<int>
      (0)(0)(4)(2)(0)(0)(3)(2)(4)(1)(1)(0)(0)(3)));
  jamp_nc_relative_power[5].push_back(0); 
  // JAMP #5
  color_configs[5].push_back(vec_int(createvector<int>
      (0)(0)(3)(2)(0)(0)(3)(4)(4)(1)(1)(0)(0)(2)));
  jamp_nc_relative_power[5].push_back(0); 

  // Color flows of process Process: ve g > ve g g d d~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[6].push_back(vec_int(createvector<int>
      (0)(0)(1)(2)(0)(0)(3)(2)(4)(3)(1)(0)(0)(4)));
  jamp_nc_relative_power[6].push_back(0); 
  // JAMP #1
  color_configs[6].push_back(vec_int(createvector<int>
      (0)(0)(1)(2)(0)(0)(3)(4)(4)(2)(1)(0)(0)(3)));
  jamp_nc_relative_power[6].push_back(0); 
  // JAMP #2
  color_configs[6].push_back(vec_int(createvector<int>
      (0)(0)(3)(2)(0)(0)(3)(1)(4)(2)(1)(0)(0)(4)));
  jamp_nc_relative_power[6].push_back(0); 
  // JAMP #3
  color_configs[6].push_back(vec_int(createvector<int>
      (0)(0)(4)(2)(0)(0)(3)(1)(4)(3)(1)(0)(0)(2)));
  jamp_nc_relative_power[6].push_back(0); 
  // JAMP #4
  color_configs[6].push_back(vec_int(createvector<int>
      (0)(0)(4)(2)(0)(0)(3)(2)(4)(1)(1)(0)(0)(3)));
  jamp_nc_relative_power[6].push_back(0); 
  // JAMP #5
  color_configs[6].push_back(vec_int(createvector<int>
      (0)(0)(3)(2)(0)(0)(3)(4)(4)(1)(1)(0)(0)(2)));
  jamp_nc_relative_power[6].push_back(0); 

  // Color flows of process Process: e+ g > ve~ g g u d~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[7].push_back(vec_int(createvector<int>
      (0)(0)(1)(2)(0)(0)(3)(2)(4)(3)(1)(0)(0)(4)));
  jamp_nc_relative_power[7].push_back(0); 
  // JAMP #1
  color_configs[7].push_back(vec_int(createvector<int>
      (0)(0)(1)(2)(0)(0)(3)(4)(4)(2)(1)(0)(0)(3)));
  jamp_nc_relative_power[7].push_back(0); 
  // JAMP #2
  color_configs[7].push_back(vec_int(createvector<int>
      (0)(0)(3)(2)(0)(0)(3)(1)(4)(2)(1)(0)(0)(4)));
  jamp_nc_relative_power[7].push_back(0); 
  // JAMP #3
  color_configs[7].push_back(vec_int(createvector<int>
      (0)(0)(4)(2)(0)(0)(3)(1)(4)(3)(1)(0)(0)(2)));
  jamp_nc_relative_power[7].push_back(0); 
  // JAMP #4
  color_configs[7].push_back(vec_int(createvector<int>
      (0)(0)(4)(2)(0)(0)(3)(2)(4)(1)(1)(0)(0)(3)));
  jamp_nc_relative_power[7].push_back(0); 
  // JAMP #5
  color_configs[7].push_back(vec_int(createvector<int>
      (0)(0)(3)(2)(0)(0)(3)(4)(4)(1)(1)(0)(0)(2)));
  jamp_nc_relative_power[7].push_back(0); 

  // Color flows of process Process: ve~ g > ve~ g g u u~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[8].push_back(vec_int(createvector<int>
      (0)(0)(1)(2)(0)(0)(3)(2)(4)(3)(1)(0)(0)(4)));
  jamp_nc_relative_power[8].push_back(0); 
  // JAMP #1
  color_configs[8].push_back(vec_int(createvector<int>
      (0)(0)(1)(2)(0)(0)(3)(4)(4)(2)(1)(0)(0)(3)));
  jamp_nc_relative_power[8].push_back(0); 
  // JAMP #2
  color_configs[8].push_back(vec_int(createvector<int>
      (0)(0)(3)(2)(0)(0)(3)(1)(4)(2)(1)(0)(0)(4)));
  jamp_nc_relative_power[8].push_back(0); 
  // JAMP #3
  color_configs[8].push_back(vec_int(createvector<int>
      (0)(0)(4)(2)(0)(0)(3)(1)(4)(3)(1)(0)(0)(2)));
  jamp_nc_relative_power[8].push_back(0); 
  // JAMP #4
  color_configs[8].push_back(vec_int(createvector<int>
      (0)(0)(4)(2)(0)(0)(3)(2)(4)(1)(1)(0)(0)(3)));
  jamp_nc_relative_power[8].push_back(0); 
  // JAMP #5
  color_configs[8].push_back(vec_int(createvector<int>
      (0)(0)(3)(2)(0)(0)(3)(4)(4)(1)(1)(0)(0)(2)));
  jamp_nc_relative_power[8].push_back(0); 

  // Color flows of process Process: ve~ g > ve~ g g d d~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[9].push_back(vec_int(createvector<int>
      (0)(0)(1)(2)(0)(0)(3)(2)(4)(3)(1)(0)(0)(4)));
  jamp_nc_relative_power[9].push_back(0); 
  // JAMP #1
  color_configs[9].push_back(vec_int(createvector<int>
      (0)(0)(1)(2)(0)(0)(3)(4)(4)(2)(1)(0)(0)(3)));
  jamp_nc_relative_power[9].push_back(0); 
  // JAMP #2
  color_configs[9].push_back(vec_int(createvector<int>
      (0)(0)(3)(2)(0)(0)(3)(1)(4)(2)(1)(0)(0)(4)));
  jamp_nc_relative_power[9].push_back(0); 
  // JAMP #3
  color_configs[9].push_back(vec_int(createvector<int>
      (0)(0)(4)(2)(0)(0)(3)(1)(4)(3)(1)(0)(0)(2)));
  jamp_nc_relative_power[9].push_back(0); 
  // JAMP #4
  color_configs[9].push_back(vec_int(createvector<int>
      (0)(0)(4)(2)(0)(0)(3)(2)(4)(1)(1)(0)(0)(3)));
  jamp_nc_relative_power[9].push_back(0); 
  // JAMP #5
  color_configs[9].push_back(vec_int(createvector<int>
      (0)(0)(3)(2)(0)(0)(3)(4)(4)(1)(1)(0)(0)(2)));
  jamp_nc_relative_power[9].push_back(0); 
}

//--------------------------------------------------------------------------
// Destructor.
PY8MEs_R4_P4_sm_lg_lggqq::~PY8MEs_R4_P4_sm_lg_lggqq() 
{
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    delete[] p[i]; 
    p[i] = NULL; 
  }
}

//--------------------------------------------------------------------------
// Invert the permutation mapping
vector<int> PY8MEs_R4_P4_sm_lg_lggqq::invert_mapping(vector<int> mapping) 
{
  vector<int> inverted_mapping; 
  for (unsigned int i = 0; i < mapping.size(); i++ )
  {
    for (unsigned int j = 0; j < mapping.size(); j++ )
    {
      if (mapping[j] == ((int)i))
      {
        inverted_mapping.push_back(j); 
        break; 
      }
    }
  }
  return inverted_mapping; 
}

//--------------------------------------------------------------------------
// Return the list of possible helicity configurations
vector < vec_int > PY8MEs_R4_P4_sm_lg_lggqq::getHelicityConfigs(vector<int>
    permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(ncomb, vector<int> (nexternal, 0)); 
  for (unsigned int ihel = 0; ihel < ncomb; ihel++ )
  {
    for(unsigned int j = 0; j < nexternal; j++ )
    {
      res[ihel][chosenPerm[j]] = helicities[ihel][j]; 
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Return the list of possible color configurations
vector < vec_int > PY8MEs_R4_P4_sm_lg_lggqq::getColorConfigs(int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(color_configs[chosenProcID].size(), vector<int>
      (nexternal * 2, 0));
  for (unsigned int icol = 0; icol < color_configs[chosenProcID].size(); icol++
      )
  {
    for(unsigned int j = 0; j < (2 * nexternal); j++ )
    {
      res[icol][chosenPerm[j/2] * 2 + j%2] =
          color_configs[chosenProcID][icol][j];
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Get JAMP relative N_c power
int PY8MEs_R4_P4_sm_lg_lggqq::getColorFlowRelativeNCPower(int color_flow_ID,
    int specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return jamp_nc_relative_power[chosenProcID][color_flow_ID]; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity ID -> Helicity Config
vector<int> PY8MEs_R4_P4_sm_lg_lggqq::getHelicityConfigForID(int hel_ID,
    vector<int> permutation)
{
  if (hel_ID < 0 || hel_ID >= ncomb)
  {
    cerr <<  "Error in function 'getHelicityConfigForID' of class" << 
    " 'PY8MEs_R4_P4_sm_lg_lggqq': Specified helicity ID '" << 
    hel_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R4_P4_sm_lg_lggqq_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(nexternal, 0); 
  for (unsigned int j = 0; j < nexternal; j++ )
  {
    res[chosenPerm[j]] = helicities[hel_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity Config -> Helicity ID
int PY8MEs_R4_P4_sm_lg_lggqq::getHelicityIDForConfig(vector<int> hel_config,
    vector<int> permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  int user_ihel = -1; 
  if (hel_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < ncomb; i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < nexternal; j++ )
      {
        if (helicities[i][chosenPerm[j]] != hel_config[j])
        {
          found = false; 
          break; 
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_ihel = i; 
        break; 
      }
    }
    if (user_ihel == -1)
    {
      cerr <<  "Error in function 'getHelicityIDForConfig' of class" << 
      " 'PY8MEs_R4_P4_sm_lg_lggqq': Specified helicity" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R4_P4_sm_lg_lggqq_exception; 
    }
  }
  return user_ihel; 
}


//--------------------------------------------------------------------------
// Implements the map Color ID -> Color Config
vector<int> PY8MEs_R4_P4_sm_lg_lggqq::getColorConfigForID(int color_ID, int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < 0 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getColorConfigForID' of class" << 
    " 'PY8MEs_R4_P4_sm_lg_lggqq': Specified color ID '" << 
    color_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R4_P4_sm_lg_lggqq_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(color_configs[chosenProcID][color_ID].size(), 0); 
  for (unsigned int j = 0; j < (2 * nexternal); j++ )
  {
    res[chosenPerm[j/2] * 2 + j%2] = color_configs[chosenProcID][color_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Color Config -> Color ID
int PY8MEs_R4_P4_sm_lg_lggqq::getColorIDForConfig(vector<int> color_config, int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = -1; 
  if (color_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < color_configs[chosenProcID].size(); i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < (nexternal * 2); j++ )
      {

        // If colorless then make sure it matches
        // The little arithmetics in the color index is just
        // the permutation applies on the particle list which is
        // twice smaller since each particle can have two color indices.
        if (color_config[j] == 0)
        {
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] != 0)
          {
            found = false; 
            break; 
          }
          // Otherwise check that the color linked position matches
        }
        else
        {
          int color_linked_pos = -1; 
          // Find the other end of the line in the user color config
          for (unsigned int k = 0; k < (nexternal * 2); k++ )
          {
            if (k == j)
              continue; 
            if (color_config[j] == color_config[k])
            {
              color_linked_pos = k; 
              break; 
            }
          }
          if (color_linked_pos == -1)
          {
            cerr <<  "Error in function 'getColorIDForConfig' of class" << 
            " 'PY8MEs_R4_P4_sm_lg_lggqq': A color line could " << 
            " not be closed." << endl; 
            throw PY8MEs_R4_P4_sm_lg_lggqq_exception; 
          }
          // Now check whether the color line matches
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] !=
              color_configs[chosenProcID][i][chosenPerm[color_linked_pos/2] * 2
              + color_linked_pos%2])
          {
            found = false; 
            break; 
          }
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_icol = i; 
        break; 
      }
    }

    if (user_icol == -1)
    {
      cerr <<  "Error in function 'getColorIDForConfig' of class" << 
      " 'PY8MEs_R4_P4_sm_lg_lggqq': Specified color" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R4_P4_sm_lg_lggqq_exception; 
    }
  }
  return user_icol; 
}

//--------------------------------------------------------------------------
// Returns all result previously computed in SigmaKin
vector < vec_double > PY8MEs_R4_P4_sm_lg_lggqq::getAllResults(int
    specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return all_results[chosenProcID]; 
}

//--------------------------------------------------------------------------
// Returns a result previously computed in SigmaKin for a specific helicity
// and color ID. -1 means avg and summed over that characteristic.
double PY8MEs_R4_P4_sm_lg_lggqq::getResult(int helicity_ID, int color_ID, int
    specify_proc_ID)
{
  if (helicity_ID < - 1 || helicity_ID >= ncomb)
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R4_P4_sm_lg_lggqq': Specified helicity ID '" << 
    helicity_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R4_P4_sm_lg_lggqq_exception; 
  }
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < - 1 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R4_P4_sm_lg_lggqq': Specified color ID '" << 
    color_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R4_P4_sm_lg_lggqq_exception; 
  }
  return all_results[chosenProcID][helicity_ID + 1][color_ID + 1]; 
}

//--------------------------------------------------------------------------
// Check for the availability of the requested process and if available,
// If available, this returns the corresponding permutation and Proc_ID to use.
// If not available, this returns a negative Proc_ID.
pair < vector<int> , int > PY8MEs_R4_P4_sm_lg_lggqq::static_getPY8ME(vector<int> initial_pdgs, vector<int> final_pdgs, set<int> schannels) 
{

  // Not available return value
  pair < vector<int> , int > NA(vector<int> (), -1); 

  // Check if s-channel requirements match
  if (nreq_s_channels > 0)
  {
    if (schannels != s_channel_proc)
      return NA; 
  }
  else
  {
    if (schannels.size() != 0)
      return NA; 
  }

  // Check number of final state particles
  if (final_pdgs.size() != (nexternal - ninitial))
    return NA; 

  // Check number of initial state particles
  if (initial_pdgs.size() != ninitial)
    return NA; 

  // List of processes available in this class
  const int nprocs = 56; 
  const int proc_IDS[nprocs] = {0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3,
      4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 6, 6, 6, 6, 6, 6, 7, 7, 7, 7,
      7, 7, 7, 7, 8, 8, 8, 8, 8, 8, 9, 9, 9, 9, 9, 9};
  const int in_pdgs[nprocs][ninitial] = {{11, 21}, {11, 21}, {13, 21}, {13,
      21}, {11, 21}, {11, 21}, {13, 21}, {13, 21}, {-11, 21}, {-11, 21}, {-13,
      21}, {-13, 21}, {-11, 21}, {-11, 21}, {-13, 21}, {-13, 21}, {11, 21},
      {11, 21}, {13, 21}, {13, 21}, {12, 21}, {12, 21}, {14, 21}, {14, 21},
      {12, 21}, {12, 21}, {14, 21}, {14, 21}, {16, 21}, {16, 21}, {12, 21},
      {12, 21}, {14, 21}, {14, 21}, {16, 21}, {16, 21}, {-11, 21}, {-11, 21},
      {-13, 21}, {-13, 21}, {-12, 21}, {-12, 21}, {-14, 21}, {-14, 21}, {-12,
      21}, {-12, 21}, {-14, 21}, {-14, 21}, {-16, 21}, {-16, 21}, {-12, 21},
      {-12, 21}, {-14, 21}, {-14, 21}, {-16, 21}, {-16, 21}};
  const int out_pdgs[nprocs][nexternal - ninitial] = {{11, 21, 21, 2, -2}, {11,
      21, 21, 4, -4}, {13, 21, 21, 2, -2}, {13, 21, 21, 4, -4}, {11, 21, 21, 1,
      -1}, {11, 21, 21, 3, -3}, {13, 21, 21, 1, -1}, {13, 21, 21, 3, -3}, {-11,
      21, 21, 2, -2}, {-11, 21, 21, 4, -4}, {-13, 21, 21, 2, -2}, {-13, 21, 21,
      4, -4}, {-11, 21, 21, 1, -1}, {-11, 21, 21, 3, -3}, {-13, 21, 21, 1, -1},
      {-13, 21, 21, 3, -3}, {12, 21, 21, 1, -2}, {12, 21, 21, 3, -4}, {14, 21,
      21, 1, -2}, {14, 21, 21, 3, -4}, {11, 21, 21, 2, -1}, {11, 21, 21, 4,
      -3}, {13, 21, 21, 2, -1}, {13, 21, 21, 4, -3}, {12, 21, 21, 2, -2}, {12,
      21, 21, 4, -4}, {14, 21, 21, 2, -2}, {14, 21, 21, 4, -4}, {16, 21, 21, 2,
      -2}, {16, 21, 21, 4, -4}, {12, 21, 21, 1, -1}, {12, 21, 21, 3, -3}, {14,
      21, 21, 1, -1}, {14, 21, 21, 3, -3}, {16, 21, 21, 1, -1}, {16, 21, 21, 3,
      -3}, {-12, 21, 21, 2, -1}, {-12, 21, 21, 4, -3}, {-14, 21, 21, 2, -1},
      {-14, 21, 21, 4, -3}, {-11, 21, 21, 1, -2}, {-11, 21, 21, 3, -4}, {-13,
      21, 21, 1, -2}, {-13, 21, 21, 3, -4}, {-12, 21, 21, 2, -2}, {-12, 21, 21,
      4, -4}, {-14, 21, 21, 2, -2}, {-14, 21, 21, 4, -4}, {-16, 21, 21, 2, -2},
      {-16, 21, 21, 4, -4}, {-12, 21, 21, 1, -1}, {-12, 21, 21, 3, -3}, {-14,
      21, 21, 1, -1}, {-14, 21, 21, 3, -3}, {-16, 21, 21, 1, -1}, {-16, 21, 21,
      3, -3}};

  bool in_pdgs_used[ninitial]; 
  bool out_pdgs_used[nexternal - ninitial]; 
  for(unsigned int i = 0; i < nprocs; i++ )
  {
    int permutations[nexternal]; 

    // Reinitialize initial state look-up variables
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      in_pdgs_used[j] = false; 
      permutations[j] = -1; 
    }
    // Look for initial state matches
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      for(unsigned int k = 0; k < ninitial; k++ )
      {
        // Make sure it has not been used already
        if (in_pdgs_used[k])
          continue; 
        if (initial_pdgs[k] == in_pdgs[i][j])
        {
          permutations[j] = k; 
          in_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[ninitial - 1] == -1)
      continue; 

    // Reinitialize final state look-up variables
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      out_pdgs_used[j] = false; 
      permutations[ninitial + j] = -1; 
    }
    // Look for final state matches
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      for(unsigned int k = 0; k < (nexternal - ninitial); k++ )
      {
        // Make sure it has not been used already
        if (out_pdgs_used[k])
          continue; 
        if (final_pdgs[k] == out_pdgs[i][j])
        {
          permutations[ninitial + j] = ninitial + k; 
          out_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[ninitial + j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[nexternal - 1] == -1)
      continue; 

    // Return process found
    return pair < vector<int> , int > (vector<int> (permutations, permutations
        + nexternal), proc_IDS[i]);
  }

  // No process found
  return NA; 
}

//--------------------------------------------------------------------------
// Set momenta
void PY8MEs_R4_P4_sm_lg_lggqq::setMomenta(vector < vec_double > momenta_picked)
{
  if (momenta_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setMomenta' of class" << 
    " 'PY8MEs_R4_P4_sm_lg_lggqq': Incorrect number of" << 
    " momenta specified." << endl; 
    throw PY8MEs_R4_P4_sm_lg_lggqq_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    if (momenta_picked[i].size() != 4)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R4_P4_sm_lg_lggqq': Incorrect number of" << 
      " momenta components specified." << endl; 
      throw PY8MEs_R4_P4_sm_lg_lggqq_exception; 
    }
    if (momenta_picked[i][0] < 0.0)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R4_P4_sm_lg_lggqq': A momentum was specified" << 
      " with negative energy. Check conventions." << endl; 
      throw PY8MEs_R4_P4_sm_lg_lggqq_exception; 
    }
    for (unsigned int j = 0; j < 4; j++ )
    {
      p[i][j] = momenta_picked[i][j]; 
    }
  }
}

//--------------------------------------------------------------------------
// Set color configuration to use. An empty vector means sum over all.
void PY8MEs_R4_P4_sm_lg_lggqq::setColors(vector<int> colors_picked)
{
  if (colors_picked.size() == 0)
  {
    user_colors = vector<int> (); 
    return; 
  }
  if (colors_picked.size() != (2 * nexternal))
  {
    cerr <<  "Error in function 'setColors' of class" << 
    " 'PY8MEs_R4_P4_sm_lg_lggqq': Incorrect number" << 
    " of colors specified." << endl; 
    throw PY8MEs_R4_P4_sm_lg_lggqq_exception; 
  }
  user_colors = vector<int> ((2 * nexternal), 0); 
  for(unsigned int i = 0; i < (2 * nexternal); i++ )
  {
    user_colors[i] = colors_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the helicity configuration to use. Am empty vector means sum over all.
void PY8MEs_R4_P4_sm_lg_lggqq::setHelicities(vector<int> helicities_picked) 
{
  if (helicities_picked.size() != nexternal)
  {
    if (helicities_picked.size() == 0)
    {
      user_helicities = vector<int> (); 
      return; 
    }
    cerr <<  "Error in function 'setHelicities' of class" << 
    " 'PY8MEs_R4_P4_sm_lg_lggqq': Incorrect number" << 
    " of helicities specified." << endl; 
    throw PY8MEs_R4_P4_sm_lg_lggqq_exception; 
  }
  user_helicities = vector<int> (nexternal, 0); 
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    user_helicities[i] = helicities_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the permutation to use (will apply to momenta, colors and helicities)
void PY8MEs_R4_P4_sm_lg_lggqq::setPermutation(vector<int> perm_picked) 
{
  if (perm_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setPermutations' of class" << 
    " 'PY8MEs_R4_P4_sm_lg_lggqq': Incorrect number" << 
    " of permutations specified." << endl; 
    throw PY8MEs_R4_P4_sm_lg_lggqq_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    perm[i] = perm_picked[i]; 
  }
}

// Set the proc_ID to use
void PY8MEs_R4_P4_sm_lg_lggqq::setProcID(int procID_picked) 
{
  proc_ID = procID_picked; 
}

//--------------------------------------------------------------------------
// Initialize process.

void PY8MEs_R4_P4_sm_lg_lggqq::initProc() 
{

  // Initialize flags
  include_symmetry_factors = true; 
  include_helicity_averaging_factors = true; 
  include_color_averaging_factors = true; 

  // Initialize vectors.
  perm = vector<int> (nexternal, 0); 
  user_colors = vector<int> (2 * nexternal, 0); 
  user_helicities = vector<int> (nexternal, 0); 
  p = vector < double * > (); 
  for (unsigned int i = 0; i < nexternal; i++ )
  {
    p.push_back(new double[4]); 
  }
  initColorConfigs(); 
  // Synchronize local variables dependent on the model with the active model.
  mME = vector<double> (nexternal, 0.); 
  syncProcModelParams(); 
  jamp2 = vector < vec_double > (10); 
  jamp2[0] = vector<double> (6, 0.); 
  jamp2[1] = vector<double> (6, 0.); 
  jamp2[2] = vector<double> (6, 0.); 
  jamp2[3] = vector<double> (6, 0.); 
  jamp2[4] = vector<double> (6, 0.); 
  jamp2[5] = vector<double> (6, 0.); 
  jamp2[6] = vector<double> (6, 0.); 
  jamp2[7] = vector<double> (6, 0.); 
  jamp2[8] = vector<double> (6, 0.); 
  jamp2[9] = vector<double> (6, 0.); 
  all_results = vector < vec_vec_double > (10); 
  // The first entry is always the color or helicity avg/summed matrix element.
  all_results[0] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[1] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[2] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[3] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[4] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[5] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[6] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[7] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[8] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[9] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
}

// Synchronize local variables of the process that depend on the model
// parameters
void PY8MEs_R4_P4_sm_lg_lggqq::syncProcModelParams() 
{

  // Instantiate the model class and set parameters that stay fixed during run
  mME[0] = pars->ZERO; 
  mME[1] = pars->ZERO; 
  mME[2] = pars->ZERO; 
  mME[3] = pars->ZERO; 
  mME[4] = pars->ZERO; 
  mME[5] = pars->ZERO; 
  mME[6] = pars->ZERO; 
}

//--------------------------------------------------------------------------
// Setter allowing to force particular values for the external masses
void PY8MEs_R4_P4_sm_lg_lggqq::setMasses(vec_double external_masses) 
{

  if (external_masses.size() != mME.size())
  {
    cerr <<  "Error in function 'setMasses' of class" << 
    " 'PY8MEs_R4_P4_sm_lg_lggqq': Incorrect number of" << 
    " masses specified." << endl; 
    throw PY8MEs_R4_P4_sm_lg_lggqq_exception; 
  }
  for (unsigned int j = 0; j < mME.size(); j++ )
  {
    mME[j] = external_masses[perm[j]]; 
  }
}

//--------------------------------------------------------------------------
// Getter accessing external masses with the correct ordering
vector<double> PY8MEs_R4_P4_sm_lg_lggqq::getMasses() 
{

  vec_double external_masses; 
  vector<int> invertedPerm; 
  invertedPerm = invert_mapping(perm); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    external_masses.push_back(mME[invertedPerm[i]]); 
  }
  return external_masses; 

}


// Set all values of the external masses to float(-mode) where mode can be
// 0 : Mass taken from the model
// 1 : Mass taken from p_i^2 if not massless to begin with
// 2 : Mass always taken from p_i^2.
void PY8MEs_R4_P4_sm_lg_lggqq::setExternalMassesMode(int mode) 
{
  if (mode != 0 && mode != 1 && mode != 2)
  {
    cerr <<  "Error in function 'setExternalMassesMode' of class" << 
    " 'PY8MEs_R4_P4_sm_lg_lggqq': Incorrect mode selected :" << mode << 
    ". It must be either 0, 1 or 2" << endl; 
    throw PY8MEs_R4_P4_sm_lg_lggqq_exception; 
  }
  if (mode == 0)
  {
    syncProcModelParams(); 
  }
  else if (mode == 1)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      if (mME[j] != pars->ZERO)
      {
        mME[j] = -1.0; 
      }
    }
  }
  else if (mode == 2)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      mME[j] = -1.0; 
    }
  }
}

//--------------------------------------------------------------------------
// Evaluate the squared matrix element.

double PY8MEs_R4_P4_sm_lg_lggqq::sigmaKin() 
{
  // Set the parameters which change event by event
  pars->setDependentParameters(); 
  pars->setDependentCouplings(); 
  // Reset color flows
  for(int i = 0; i < 6; i++ )
    jamp2[0][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[1][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[2][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[3][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[4][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[5][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[6][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[7][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[8][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[9][i] = 0.; 

  // Save previous values of mME
  vector<double> saved_mME(mME.size(), 0.0); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (mME[i] < 0.0)
    {
      saved_mME[i] = mME[i]; 
      mME[i] = sqrt(abs(pow(p[perm[i]][0], 2) - 
      (pow(p[perm[i]][1], 2) + pow(p[perm[i]][2], 2) + pow(p[perm[i]][3],
          2))));
    }
  }

  // Local variables and constants
  const int max_tries = 10; 
  vector < vec_bool > goodhel(nprocesses, vec_bool(ncomb, false)); 
  vec_int ntry(nprocesses, 0); 
  double t = 0.; 
  double result = 0.; 

  if (ntry[proc_ID] <= max_tries)
    ntry[proc_ID] = ntry[proc_ID] + 1; 

  // Find which helicity configuration is asked for
  // -1 indicates one wants to sum over helicities
  int user_ihel = getHelicityIDForConfig(user_helicities); 

  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = getColorIDForConfig(user_colors); 

  // Reset the list of results that will be recomputed here
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      all_results[proc_ID][ihel + 1][icolor + 1] = 0.; 
    }
  }

  // Calculate the matrix element for all helicities
  // unless already detected as vanishing
  for(int ihel = 0; ihel < ncomb; ihel++ )
  {
    // Skip helicity if already detected as vanishing
    if ((ntry[proc_ID] >= max_tries) && !goodhel[proc_ID][ihel])
      continue; 

    // Also skip helicity if user asks for a specific one
    if ((ntry[proc_ID] >= max_tries) && user_ihel != -1 && user_ihel != ihel)
      continue; 

    calculate_wavefunctions(helicities[ihel]); 

    // Reset locally computed color flows
    for(int i = 0; i < 6; i++ )
      jamp2[0][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[1][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[2][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[3][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[4][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[5][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[6][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[7][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[8][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[9][i] = 0.; 

    if (proc_ID == 0)
      t = matrix_4_emg_emgguux(); 
    if (proc_ID == 1)
      t = matrix_4_emg_emggddx(); 
    if (proc_ID == 2)
      t = matrix_4_epg_epgguux(); 
    if (proc_ID == 3)
      t = matrix_4_epg_epggddx(); 
    if (proc_ID == 4)
      t = matrix_4_emg_veggdux(); 
    if (proc_ID == 5)
      t = matrix_4_veg_vegguux(); 
    if (proc_ID == 6)
      t = matrix_4_veg_veggddx(); 
    if (proc_ID == 7)
      t = matrix_4_epg_vexggudx(); 
    if (proc_ID == 8)
      t = matrix_4_vexg_vexgguux(); 
    if (proc_ID == 9)
      t = matrix_4_vexg_vexggddx(); 

    // Store which helicities give non-zero result
    if ((ntry[proc_ID] < max_tries) && t != 0. && !goodhel[proc_ID][ihel])
      goodhel[proc_ID][ihel] = true; 

    // Aggregate results
    if (user_ihel == -1 || user_ihel == ihel)
    {
      if (user_icol == -1)
      {
        result = result + t; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][0] += t; 
          for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
          {
            all_results[proc_ID][0][i + 1] += jamp2[proc_ID][i]; 
          }
        }
        all_results[proc_ID][ihel + 1][0] += t; 
        for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
        {
          all_results[proc_ID][ihel + 1][i + 1] += jamp2[proc_ID][i]; 
        }
      }
      else
      {
        result = result + jamp2[proc_ID][user_icol]; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][user_icol + 1] += jamp2[proc_ID][user_icol]; 
        }
        all_results[proc_ID][ihel + 1][user_icol + 1] +=
            jamp2[proc_ID][user_icol];
      }
    }

  }

  // Normalize results with the identical particle factor
  if (include_symmetry_factors)
  {
    result = result/denom_iden[proc_ID]; 
  }
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      if (include_symmetry_factors)
      {
        all_results[proc_ID][ihel + 1][icolor + 1] /= denom_iden[proc_ID]; 
      }
    }
  }


  // Normalize when when summing+averaging over helicity configurations
  if (user_ihel == -1 && include_helicity_averaging_factors)
  {
    result /= denom_hels[proc_ID]; 
    if (user_icol == -1)
    {
      all_results[proc_ID][0][0] /= denom_hels[proc_ID]; 
      for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
      {
        all_results[proc_ID][0][i + 1] /= denom_hels[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][0][user_icol + 1] /= denom_hels[proc_ID]; 
    }
  }

  // Normalize when summing+averaging over color configurations
  if (user_icol == -1 && include_color_averaging_factors)
  {
    result /= denom_colors[proc_ID]; 
    if (user_ihel == -1)
    {
      all_results[proc_ID][0][0] /= denom_colors[proc_ID]; 
      for (unsigned int i = 0; i < ncomb; i++ )
      {
        all_results[proc_ID][i + 1][0] /= denom_colors[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][user_ihel + 1][0] /= denom_colors[proc_ID]; 
    }
  }

  // Reinstate previous values of mME
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (saved_mME[i] < 0.0)
    {
      mME[i] = saved_mME[i]; 
    }
  }

  // Finally return it
  return result; 
}

//==========================================================================
// Private class member functions

//--------------------------------------------------------------------------
// Evaluate |M|^2 for each subprocess

void PY8MEs_R4_P4_sm_lg_lggqq::calculate_wavefunctions(const int hel[])
{
  // Calculate wavefunctions for all processes
  // Calculate all wavefunctions
  ixxxxx(p[perm[0]], mME[0], hel[0], +1, w[0]); 
  vxxxxx(p[perm[1]], mME[1], hel[1], -1, w[1]); 
  oxxxxx(p[perm[2]], mME[2], hel[2], +1, w[2]); 
  vxxxxx(p[perm[3]], mME[3], hel[3], +1, w[3]); 
  vxxxxx(p[perm[4]], mME[4], hel[4], +1, w[4]); 
  oxxxxx(p[perm[5]], mME[5], hel[5], +1, w[5]); 
  ixxxxx(p[perm[6]], mME[6], hel[6], -1, w[6]); 
  FFV1P0_3(w[0], w[2], pars->GC_3, pars->ZERO, pars->ZERO, w[7]); 
  VVV1P0_1(w[1], w[3], pars->GC_10, pars->ZERO, pars->ZERO, w[8]); 
  FFV1_1(w[5], w[7], pars->GC_2, pars->ZERO, pars->ZERO, w[9]); 
  VVV1P0_1(w[8], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[10]); 
  FFV1_2(w[6], w[8], pars->GC_11, pars->ZERO, pars->ZERO, w[11]); 
  FFV1_2(w[6], w[7], pars->GC_2, pars->ZERO, pars->ZERO, w[12]); 
  FFV1_1(w[5], w[8], pars->GC_11, pars->ZERO, pars->ZERO, w[13]); 
  FFV2_4_3(w[0], w[2], pars->GC_50, pars->GC_59, pars->mdl_MZ, pars->mdl_WZ,
      w[14]);
  FFV2_5_1(w[5], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[15]);
  FFV2_5_2(w[6], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[16]);
  FFV1_1(w[5], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[17]); 
  FFV1_1(w[17], w[7], pars->GC_2, pars->ZERO, pars->ZERO, w[18]); 
  FFV2_5_1(w[17], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[19]);
  FFV1_2(w[6], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[20]); 
  FFV1_2(w[20], w[7], pars->GC_2, pars->ZERO, pars->ZERO, w[21]); 
  FFV2_5_2(w[20], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[22]);
  VVV1P0_1(w[1], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[23]); 
  VVV1P0_1(w[23], w[3], pars->GC_10, pars->ZERO, pars->ZERO, w[24]); 
  FFV1_2(w[6], w[23], pars->GC_11, pars->ZERO, pars->ZERO, w[25]); 
  FFV1_1(w[5], w[23], pars->GC_11, pars->ZERO, pars->ZERO, w[26]); 
  FFV1_1(w[5], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[27]); 
  FFV1_1(w[27], w[7], pars->GC_2, pars->ZERO, pars->ZERO, w[28]); 
  FFV2_5_1(w[27], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[29]);
  FFV1_2(w[6], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[30]); 
  FFV1_2(w[30], w[7], pars->GC_2, pars->ZERO, pars->ZERO, w[31]); 
  FFV2_5_2(w[30], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[32]);
  FFV1_1(w[5], w[1], pars->GC_11, pars->ZERO, pars->ZERO, w[33]); 
  FFV1_1(w[33], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[34]); 
  FFV1_1(w[33], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[35]); 
  VVV1P0_1(w[3], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[36]); 
  FFV1_1(w[33], w[7], pars->GC_2, pars->ZERO, pars->ZERO, w[37]); 
  FFV2_5_1(w[33], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[38]);
  FFV1_2(w[6], w[1], pars->GC_11, pars->ZERO, pars->ZERO, w[39]); 
  FFV1_2(w[39], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[40]); 
  FFV1_2(w[39], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[41]); 
  FFV1_2(w[39], w[7], pars->GC_2, pars->ZERO, pars->ZERO, w[42]); 
  FFV2_5_2(w[39], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[43]);
  VVV1P0_1(w[1], w[36], pars->GC_10, pars->ZERO, pars->ZERO, w[44]); 
  FFV1_2(w[6], w[36], pars->GC_11, pars->ZERO, pars->ZERO, w[45]); 
  FFV1_1(w[5], w[36], pars->GC_11, pars->ZERO, pars->ZERO, w[46]); 
  FFV1_1(w[27], w[1], pars->GC_11, pars->ZERO, pars->ZERO, w[47]); 
  FFV1_1(w[27], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[48]); 
  FFV1_2(w[30], w[1], pars->GC_11, pars->ZERO, pars->ZERO, w[49]); 
  FFV1_2(w[30], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[50]); 
  FFV1_1(w[17], w[1], pars->GC_11, pars->ZERO, pars->ZERO, w[51]); 
  FFV1_1(w[17], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[52]); 
  FFV1_2(w[20], w[1], pars->GC_11, pars->ZERO, pars->ZERO, w[53]); 
  FFV1_2(w[20], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[54]); 
  VVVV1P0_1(w[1], w[3], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[55]); 
  VVVV3P0_1(w[1], w[3], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[56]); 
  VVVV4P0_1(w[1], w[3], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[57]); 
  FFV1_1(w[5], w[7], pars->GC_1, pars->ZERO, pars->ZERO, w[58]); 
  FFV1_2(w[6], w[7], pars->GC_1, pars->ZERO, pars->ZERO, w[59]); 
  FFV2_3_1(w[5], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[60]);
  FFV2_3_2(w[6], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[61]);
  FFV1_1(w[17], w[7], pars->GC_1, pars->ZERO, pars->ZERO, w[62]); 
  FFV2_3_1(w[17], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[63]);
  FFV1_2(w[20], w[7], pars->GC_1, pars->ZERO, pars->ZERO, w[64]); 
  FFV2_3_2(w[20], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[65]);
  FFV1_1(w[27], w[7], pars->GC_1, pars->ZERO, pars->ZERO, w[66]); 
  FFV2_3_1(w[27], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[67]);
  FFV1_2(w[30], w[7], pars->GC_1, pars->ZERO, pars->ZERO, w[68]); 
  FFV2_3_2(w[30], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[69]);
  FFV1_1(w[33], w[7], pars->GC_1, pars->ZERO, pars->ZERO, w[70]); 
  FFV2_3_1(w[33], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[71]);
  FFV1_2(w[39], w[7], pars->GC_1, pars->ZERO, pars->ZERO, w[72]); 
  FFV2_3_2(w[39], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[73]);
  oxxxxx(p[perm[0]], mME[0], hel[0], -1, w[74]); 
  ixxxxx(p[perm[2]], mME[2], hel[2], -1, w[75]); 
  FFV1P0_3(w[75], w[74], pars->GC_3, pars->ZERO, pars->ZERO, w[76]); 
  FFV1_1(w[5], w[76], pars->GC_2, pars->ZERO, pars->ZERO, w[77]); 
  FFV1_2(w[6], w[76], pars->GC_2, pars->ZERO, pars->ZERO, w[78]); 
  FFV2_4_3(w[75], w[74], pars->GC_50, pars->GC_59, pars->mdl_MZ, pars->mdl_WZ,
      w[79]);
  FFV2_5_1(w[5], w[79], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[80]);
  FFV2_5_2(w[6], w[79], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[81]);
  FFV1_1(w[17], w[76], pars->GC_2, pars->ZERO, pars->ZERO, w[82]); 
  FFV2_5_1(w[17], w[79], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[83]);
  FFV1_2(w[20], w[76], pars->GC_2, pars->ZERO, pars->ZERO, w[84]); 
  FFV2_5_2(w[20], w[79], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[85]);
  FFV1_1(w[27], w[76], pars->GC_2, pars->ZERO, pars->ZERO, w[86]); 
  FFV2_5_1(w[27], w[79], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[87]);
  FFV1_2(w[30], w[76], pars->GC_2, pars->ZERO, pars->ZERO, w[88]); 
  FFV2_5_2(w[30], w[79], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[89]);
  FFV1_1(w[33], w[76], pars->GC_2, pars->ZERO, pars->ZERO, w[90]); 
  FFV2_5_1(w[33], w[79], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[91]);
  FFV1_2(w[39], w[76], pars->GC_2, pars->ZERO, pars->ZERO, w[92]); 
  FFV2_5_2(w[39], w[79], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[93]);
  FFV1_1(w[5], w[76], pars->GC_1, pars->ZERO, pars->ZERO, w[94]); 
  FFV1_2(w[6], w[76], pars->GC_1, pars->ZERO, pars->ZERO, w[95]); 
  FFV2_3_1(w[5], w[79], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[96]);
  FFV2_3_2(w[6], w[79], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[97]);
  FFV1_1(w[17], w[76], pars->GC_1, pars->ZERO, pars->ZERO, w[98]); 
  FFV2_3_1(w[17], w[79], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[99]);
  FFV1_2(w[20], w[76], pars->GC_1, pars->ZERO, pars->ZERO, w[100]); 
  FFV2_3_2(w[20], w[79], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[101]);
  FFV1_1(w[27], w[76], pars->GC_1, pars->ZERO, pars->ZERO, w[102]); 
  FFV2_3_1(w[27], w[79], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[103]);
  FFV1_2(w[30], w[76], pars->GC_1, pars->ZERO, pars->ZERO, w[104]); 
  FFV2_3_2(w[30], w[79], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[105]);
  FFV1_1(w[33], w[76], pars->GC_1, pars->ZERO, pars->ZERO, w[106]); 
  FFV2_3_1(w[33], w[79], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[107]);
  FFV1_2(w[39], w[76], pars->GC_1, pars->ZERO, pars->ZERO, w[108]); 
  FFV2_3_2(w[39], w[79], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[109]);
  FFV2_3(w[0], w[2], pars->GC_100, pars->mdl_MW, pars->mdl_WW, w[110]); 
  FFV2_1(w[5], w[110], pars->GC_100, pars->ZERO, pars->ZERO, w[111]); 
  FFV2_2(w[6], w[110], pars->GC_100, pars->ZERO, pars->ZERO, w[112]); 
  FFV2_1(w[17], w[110], pars->GC_100, pars->ZERO, pars->ZERO, w[113]); 
  FFV2_2(w[20], w[110], pars->GC_100, pars->ZERO, pars->ZERO, w[114]); 
  FFV2_1(w[27], w[110], pars->GC_100, pars->ZERO, pars->ZERO, w[115]); 
  FFV2_2(w[30], w[110], pars->GC_100, pars->ZERO, pars->ZERO, w[116]); 
  FFV2_1(w[33], w[110], pars->GC_100, pars->ZERO, pars->ZERO, w[117]); 
  FFV2_2(w[39], w[110], pars->GC_100, pars->ZERO, pars->ZERO, w[118]); 
  FFV2_3(w[0], w[2], pars->GC_62, pars->mdl_MZ, pars->mdl_WZ, w[119]); 
  FFV2_5_1(w[5], w[119], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[120]);
  FFV2_5_2(w[6], w[119], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[121]);
  FFV2_5_1(w[17], w[119], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[122]);
  FFV2_5_2(w[20], w[119], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[123]);
  FFV2_5_1(w[27], w[119], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[124]);
  FFV2_5_2(w[30], w[119], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[125]);
  FFV2_5_1(w[33], w[119], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[126]);
  FFV2_5_2(w[39], w[119], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[127]);
  FFV2_3_1(w[5], w[119], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[128]);
  FFV2_3_2(w[6], w[119], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[129]);
  FFV2_3_1(w[17], w[119], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[130]);
  FFV2_3_2(w[20], w[119], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[131]);
  FFV2_3_1(w[27], w[119], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[132]);
  FFV2_3_2(w[30], w[119], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[133]);
  FFV2_3_1(w[33], w[119], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[134]);
  FFV2_3_2(w[39], w[119], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[135]);
  FFV2_3(w[75], w[74], pars->GC_100, pars->mdl_MW, pars->mdl_WW, w[136]); 
  FFV2_1(w[5], w[136], pars->GC_100, pars->ZERO, pars->ZERO, w[137]); 
  FFV2_2(w[6], w[136], pars->GC_100, pars->ZERO, pars->ZERO, w[138]); 
  FFV2_1(w[17], w[136], pars->GC_100, pars->ZERO, pars->ZERO, w[139]); 
  FFV2_2(w[20], w[136], pars->GC_100, pars->ZERO, pars->ZERO, w[140]); 
  FFV2_1(w[27], w[136], pars->GC_100, pars->ZERO, pars->ZERO, w[141]); 
  FFV2_2(w[30], w[136], pars->GC_100, pars->ZERO, pars->ZERO, w[142]); 
  FFV2_1(w[33], w[136], pars->GC_100, pars->ZERO, pars->ZERO, w[143]); 
  FFV2_2(w[39], w[136], pars->GC_100, pars->ZERO, pars->ZERO, w[144]); 
  FFV2_3(w[75], w[74], pars->GC_62, pars->mdl_MZ, pars->mdl_WZ, w[145]); 
  FFV2_5_1(w[5], w[145], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[146]);
  FFV2_5_2(w[6], w[145], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[147]);
  FFV2_5_1(w[17], w[145], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[148]);
  FFV2_5_2(w[20], w[145], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[149]);
  FFV2_5_1(w[27], w[145], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[150]);
  FFV2_5_2(w[30], w[145], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[151]);
  FFV2_5_1(w[33], w[145], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[152]);
  FFV2_5_2(w[39], w[145], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[153]);
  FFV2_3_1(w[5], w[145], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[154]);
  FFV2_3_2(w[6], w[145], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[155]);
  FFV2_3_1(w[17], w[145], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[156]);
  FFV2_3_2(w[20], w[145], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[157]);
  FFV2_3_1(w[27], w[145], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[158]);
  FFV2_3_2(w[30], w[145], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[159]);
  FFV2_3_1(w[33], w[145], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[160]);
  FFV2_3_2(w[39], w[145], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[161]);

  // Calculate all amplitudes
  // Amplitude(s) for diagram number 0
  FFV1_0(w[6], w[9], w[10], pars->GC_11, amp[0]); 
  FFV1_0(w[11], w[9], w[4], pars->GC_11, amp[1]); 
  FFV1_0(w[12], w[5], w[10], pars->GC_11, amp[2]); 
  FFV1_0(w[12], w[13], w[4], pars->GC_11, amp[3]); 
  FFV1_0(w[6], w[15], w[10], pars->GC_11, amp[4]); 
  FFV1_0(w[11], w[15], w[4], pars->GC_11, amp[5]); 
  FFV1_0(w[16], w[5], w[10], pars->GC_11, amp[6]); 
  FFV1_0(w[16], w[13], w[4], pars->GC_11, amp[7]); 
  FFV1_0(w[6], w[18], w[8], pars->GC_11, amp[8]); 
  FFV1_0(w[12], w[17], w[8], pars->GC_11, amp[9]); 
  FFV1_0(w[6], w[19], w[8], pars->GC_11, amp[10]); 
  FFV1_0(w[16], w[17], w[8], pars->GC_11, amp[11]); 
  FFV1_0(w[21], w[5], w[8], pars->GC_11, amp[12]); 
  FFV1_0(w[20], w[9], w[8], pars->GC_11, amp[13]); 
  FFV1_0(w[22], w[5], w[8], pars->GC_11, amp[14]); 
  FFV1_0(w[20], w[15], w[8], pars->GC_11, amp[15]); 
  FFV1_0(w[6], w[9], w[24], pars->GC_11, amp[16]); 
  FFV1_0(w[25], w[9], w[3], pars->GC_11, amp[17]); 
  FFV1_0(w[12], w[5], w[24], pars->GC_11, amp[18]); 
  FFV1_0(w[12], w[26], w[3], pars->GC_11, amp[19]); 
  FFV1_0(w[6], w[15], w[24], pars->GC_11, amp[20]); 
  FFV1_0(w[25], w[15], w[3], pars->GC_11, amp[21]); 
  FFV1_0(w[16], w[5], w[24], pars->GC_11, amp[22]); 
  FFV1_0(w[16], w[26], w[3], pars->GC_11, amp[23]); 
  FFV1_0(w[6], w[28], w[23], pars->GC_11, amp[24]); 
  FFV1_0(w[12], w[27], w[23], pars->GC_11, amp[25]); 
  FFV1_0(w[6], w[29], w[23], pars->GC_11, amp[26]); 
  FFV1_0(w[16], w[27], w[23], pars->GC_11, amp[27]); 
  FFV1_0(w[31], w[5], w[23], pars->GC_11, amp[28]); 
  FFV1_0(w[30], w[9], w[23], pars->GC_11, amp[29]); 
  FFV1_0(w[32], w[5], w[23], pars->GC_11, amp[30]); 
  FFV1_0(w[30], w[15], w[23], pars->GC_11, amp[31]); 
  FFV1_0(w[12], w[34], w[4], pars->GC_11, amp[32]); 
  FFV1_0(w[12], w[35], w[3], pars->GC_11, amp[33]); 
  FFV1_0(w[16], w[34], w[4], pars->GC_11, amp[34]); 
  FFV1_0(w[16], w[35], w[3], pars->GC_11, amp[35]); 
  FFV1_0(w[6], w[37], w[36], pars->GC_11, amp[36]); 
  FFV1_0(w[12], w[33], w[36], pars->GC_11, amp[37]); 
  FFV1_0(w[6], w[38], w[36], pars->GC_11, amp[38]); 
  FFV1_0(w[16], w[33], w[36], pars->GC_11, amp[39]); 
  FFV1_0(w[30], w[37], w[4], pars->GC_11, amp[40]); 
  FFV1_0(w[31], w[33], w[4], pars->GC_11, amp[41]); 
  FFV1_0(w[30], w[38], w[4], pars->GC_11, amp[42]); 
  FFV1_0(w[32], w[33], w[4], pars->GC_11, amp[43]); 
  FFV1_0(w[20], w[37], w[3], pars->GC_11, amp[44]); 
  FFV1_0(w[21], w[33], w[3], pars->GC_11, amp[45]); 
  FFV1_0(w[20], w[38], w[3], pars->GC_11, amp[46]); 
  FFV1_0(w[22], w[33], w[3], pars->GC_11, amp[47]); 
  FFV1_0(w[40], w[9], w[4], pars->GC_11, amp[48]); 
  FFV1_0(w[41], w[9], w[3], pars->GC_11, amp[49]); 
  FFV1_0(w[40], w[15], w[4], pars->GC_11, amp[50]); 
  FFV1_0(w[41], w[15], w[3], pars->GC_11, amp[51]); 
  FFV1_0(w[42], w[5], w[36], pars->GC_11, amp[52]); 
  FFV1_0(w[39], w[9], w[36], pars->GC_11, amp[53]); 
  FFV1_0(w[43], w[5], w[36], pars->GC_11, amp[54]); 
  FFV1_0(w[39], w[15], w[36], pars->GC_11, amp[55]); 
  FFV1_0(w[42], w[27], w[4], pars->GC_11, amp[56]); 
  FFV1_0(w[39], w[28], w[4], pars->GC_11, amp[57]); 
  FFV1_0(w[43], w[27], w[4], pars->GC_11, amp[58]); 
  FFV1_0(w[39], w[29], w[4], pars->GC_11, amp[59]); 
  FFV1_0(w[42], w[17], w[3], pars->GC_11, amp[60]); 
  FFV1_0(w[39], w[18], w[3], pars->GC_11, amp[61]); 
  FFV1_0(w[43], w[17], w[3], pars->GC_11, amp[62]); 
  FFV1_0(w[39], w[19], w[3], pars->GC_11, amp[63]); 
  FFV1_0(w[6], w[9], w[44], pars->GC_11, amp[64]); 
  FFV1_0(w[45], w[9], w[1], pars->GC_11, amp[65]); 
  FFV1_0(w[12], w[5], w[44], pars->GC_11, amp[66]); 
  FFV1_0(w[12], w[46], w[1], pars->GC_11, amp[67]); 
  FFV1_0(w[6], w[15], w[44], pars->GC_11, amp[68]); 
  FFV1_0(w[45], w[15], w[1], pars->GC_11, amp[69]); 
  FFV1_0(w[16], w[5], w[44], pars->GC_11, amp[70]); 
  FFV1_0(w[16], w[46], w[1], pars->GC_11, amp[71]); 
  FFV1_0(w[12], w[47], w[4], pars->GC_11, amp[72]); 
  FFV1_0(w[12], w[48], w[1], pars->GC_11, amp[73]); 
  FFV1_0(w[16], w[47], w[4], pars->GC_11, amp[74]); 
  FFV1_0(w[16], w[48], w[1], pars->GC_11, amp[75]); 
  FFV1_0(w[20], w[28], w[1], pars->GC_11, amp[76]); 
  FFV1_0(w[21], w[27], w[1], pars->GC_11, amp[77]); 
  FFV1_0(w[20], w[29], w[1], pars->GC_11, amp[78]); 
  FFV1_0(w[22], w[27], w[1], pars->GC_11, amp[79]); 
  FFV1_0(w[49], w[9], w[4], pars->GC_11, amp[80]); 
  FFV1_0(w[50], w[9], w[1], pars->GC_11, amp[81]); 
  FFV1_0(w[49], w[15], w[4], pars->GC_11, amp[82]); 
  FFV1_0(w[50], w[15], w[1], pars->GC_11, amp[83]); 
  FFV1_0(w[31], w[17], w[1], pars->GC_11, amp[84]); 
  FFV1_0(w[30], w[18], w[1], pars->GC_11, amp[85]); 
  FFV1_0(w[32], w[17], w[1], pars->GC_11, amp[86]); 
  FFV1_0(w[30], w[19], w[1], pars->GC_11, amp[87]); 
  FFV1_0(w[12], w[51], w[3], pars->GC_11, amp[88]); 
  FFV1_0(w[12], w[52], w[1], pars->GC_11, amp[89]); 
  FFV1_0(w[16], w[51], w[3], pars->GC_11, amp[90]); 
  FFV1_0(w[16], w[52], w[1], pars->GC_11, amp[91]); 
  FFV1_0(w[53], w[9], w[3], pars->GC_11, amp[92]); 
  FFV1_0(w[54], w[9], w[1], pars->GC_11, amp[93]); 
  FFV1_0(w[53], w[15], w[3], pars->GC_11, amp[94]); 
  FFV1_0(w[54], w[15], w[1], pars->GC_11, amp[95]); 
  FFV1_0(w[6], w[9], w[55], pars->GC_11, amp[96]); 
  FFV1_0(w[6], w[9], w[56], pars->GC_11, amp[97]); 
  FFV1_0(w[6], w[9], w[57], pars->GC_11, amp[98]); 
  FFV1_0(w[12], w[5], w[55], pars->GC_11, amp[99]); 
  FFV1_0(w[12], w[5], w[56], pars->GC_11, amp[100]); 
  FFV1_0(w[12], w[5], w[57], pars->GC_11, amp[101]); 
  FFV1_0(w[6], w[15], w[55], pars->GC_11, amp[102]); 
  FFV1_0(w[6], w[15], w[56], pars->GC_11, amp[103]); 
  FFV1_0(w[6], w[15], w[57], pars->GC_11, amp[104]); 
  FFV1_0(w[16], w[5], w[55], pars->GC_11, amp[105]); 
  FFV1_0(w[16], w[5], w[56], pars->GC_11, amp[106]); 
  FFV1_0(w[16], w[5], w[57], pars->GC_11, amp[107]); 
  FFV1_0(w[6], w[58], w[10], pars->GC_11, amp[108]); 
  FFV1_0(w[11], w[58], w[4], pars->GC_11, amp[109]); 
  FFV1_0(w[59], w[5], w[10], pars->GC_11, amp[110]); 
  FFV1_0(w[59], w[13], w[4], pars->GC_11, amp[111]); 
  FFV1_0(w[6], w[60], w[10], pars->GC_11, amp[112]); 
  FFV1_0(w[11], w[60], w[4], pars->GC_11, amp[113]); 
  FFV1_0(w[61], w[5], w[10], pars->GC_11, amp[114]); 
  FFV1_0(w[61], w[13], w[4], pars->GC_11, amp[115]); 
  FFV1_0(w[6], w[62], w[8], pars->GC_11, amp[116]); 
  FFV1_0(w[59], w[17], w[8], pars->GC_11, amp[117]); 
  FFV1_0(w[6], w[63], w[8], pars->GC_11, amp[118]); 
  FFV1_0(w[61], w[17], w[8], pars->GC_11, amp[119]); 
  FFV1_0(w[64], w[5], w[8], pars->GC_11, amp[120]); 
  FFV1_0(w[20], w[58], w[8], pars->GC_11, amp[121]); 
  FFV1_0(w[65], w[5], w[8], pars->GC_11, amp[122]); 
  FFV1_0(w[20], w[60], w[8], pars->GC_11, amp[123]); 
  FFV1_0(w[6], w[58], w[24], pars->GC_11, amp[124]); 
  FFV1_0(w[25], w[58], w[3], pars->GC_11, amp[125]); 
  FFV1_0(w[59], w[5], w[24], pars->GC_11, amp[126]); 
  FFV1_0(w[59], w[26], w[3], pars->GC_11, amp[127]); 
  FFV1_0(w[6], w[60], w[24], pars->GC_11, amp[128]); 
  FFV1_0(w[25], w[60], w[3], pars->GC_11, amp[129]); 
  FFV1_0(w[61], w[5], w[24], pars->GC_11, amp[130]); 
  FFV1_0(w[61], w[26], w[3], pars->GC_11, amp[131]); 
  FFV1_0(w[6], w[66], w[23], pars->GC_11, amp[132]); 
  FFV1_0(w[59], w[27], w[23], pars->GC_11, amp[133]); 
  FFV1_0(w[6], w[67], w[23], pars->GC_11, amp[134]); 
  FFV1_0(w[61], w[27], w[23], pars->GC_11, amp[135]); 
  FFV1_0(w[68], w[5], w[23], pars->GC_11, amp[136]); 
  FFV1_0(w[30], w[58], w[23], pars->GC_11, amp[137]); 
  FFV1_0(w[69], w[5], w[23], pars->GC_11, amp[138]); 
  FFV1_0(w[30], w[60], w[23], pars->GC_11, amp[139]); 
  FFV1_0(w[59], w[34], w[4], pars->GC_11, amp[140]); 
  FFV1_0(w[59], w[35], w[3], pars->GC_11, amp[141]); 
  FFV1_0(w[61], w[34], w[4], pars->GC_11, amp[142]); 
  FFV1_0(w[61], w[35], w[3], pars->GC_11, amp[143]); 
  FFV1_0(w[6], w[70], w[36], pars->GC_11, amp[144]); 
  FFV1_0(w[59], w[33], w[36], pars->GC_11, amp[145]); 
  FFV1_0(w[6], w[71], w[36], pars->GC_11, amp[146]); 
  FFV1_0(w[61], w[33], w[36], pars->GC_11, amp[147]); 
  FFV1_0(w[30], w[70], w[4], pars->GC_11, amp[148]); 
  FFV1_0(w[68], w[33], w[4], pars->GC_11, amp[149]); 
  FFV1_0(w[30], w[71], w[4], pars->GC_11, amp[150]); 
  FFV1_0(w[69], w[33], w[4], pars->GC_11, amp[151]); 
  FFV1_0(w[20], w[70], w[3], pars->GC_11, amp[152]); 
  FFV1_0(w[64], w[33], w[3], pars->GC_11, amp[153]); 
  FFV1_0(w[20], w[71], w[3], pars->GC_11, amp[154]); 
  FFV1_0(w[65], w[33], w[3], pars->GC_11, amp[155]); 
  FFV1_0(w[40], w[58], w[4], pars->GC_11, amp[156]); 
  FFV1_0(w[41], w[58], w[3], pars->GC_11, amp[157]); 
  FFV1_0(w[40], w[60], w[4], pars->GC_11, amp[158]); 
  FFV1_0(w[41], w[60], w[3], pars->GC_11, amp[159]); 
  FFV1_0(w[72], w[5], w[36], pars->GC_11, amp[160]); 
  FFV1_0(w[39], w[58], w[36], pars->GC_11, amp[161]); 
  FFV1_0(w[73], w[5], w[36], pars->GC_11, amp[162]); 
  FFV1_0(w[39], w[60], w[36], pars->GC_11, amp[163]); 
  FFV1_0(w[72], w[27], w[4], pars->GC_11, amp[164]); 
  FFV1_0(w[39], w[66], w[4], pars->GC_11, amp[165]); 
  FFV1_0(w[73], w[27], w[4], pars->GC_11, amp[166]); 
  FFV1_0(w[39], w[67], w[4], pars->GC_11, amp[167]); 
  FFV1_0(w[72], w[17], w[3], pars->GC_11, amp[168]); 
  FFV1_0(w[39], w[62], w[3], pars->GC_11, amp[169]); 
  FFV1_0(w[73], w[17], w[3], pars->GC_11, amp[170]); 
  FFV1_0(w[39], w[63], w[3], pars->GC_11, amp[171]); 
  FFV1_0(w[6], w[58], w[44], pars->GC_11, amp[172]); 
  FFV1_0(w[45], w[58], w[1], pars->GC_11, amp[173]); 
  FFV1_0(w[59], w[5], w[44], pars->GC_11, amp[174]); 
  FFV1_0(w[59], w[46], w[1], pars->GC_11, amp[175]); 
  FFV1_0(w[6], w[60], w[44], pars->GC_11, amp[176]); 
  FFV1_0(w[45], w[60], w[1], pars->GC_11, amp[177]); 
  FFV1_0(w[61], w[5], w[44], pars->GC_11, amp[178]); 
  FFV1_0(w[61], w[46], w[1], pars->GC_11, amp[179]); 
  FFV1_0(w[59], w[47], w[4], pars->GC_11, amp[180]); 
  FFV1_0(w[59], w[48], w[1], pars->GC_11, amp[181]); 
  FFV1_0(w[61], w[47], w[4], pars->GC_11, amp[182]); 
  FFV1_0(w[61], w[48], w[1], pars->GC_11, amp[183]); 
  FFV1_0(w[20], w[66], w[1], pars->GC_11, amp[184]); 
  FFV1_0(w[64], w[27], w[1], pars->GC_11, amp[185]); 
  FFV1_0(w[20], w[67], w[1], pars->GC_11, amp[186]); 
  FFV1_0(w[65], w[27], w[1], pars->GC_11, amp[187]); 
  FFV1_0(w[49], w[58], w[4], pars->GC_11, amp[188]); 
  FFV1_0(w[50], w[58], w[1], pars->GC_11, amp[189]); 
  FFV1_0(w[49], w[60], w[4], pars->GC_11, amp[190]); 
  FFV1_0(w[50], w[60], w[1], pars->GC_11, amp[191]); 
  FFV1_0(w[68], w[17], w[1], pars->GC_11, amp[192]); 
  FFV1_0(w[30], w[62], w[1], pars->GC_11, amp[193]); 
  FFV1_0(w[69], w[17], w[1], pars->GC_11, amp[194]); 
  FFV1_0(w[30], w[63], w[1], pars->GC_11, amp[195]); 
  FFV1_0(w[59], w[51], w[3], pars->GC_11, amp[196]); 
  FFV1_0(w[59], w[52], w[1], pars->GC_11, amp[197]); 
  FFV1_0(w[61], w[51], w[3], pars->GC_11, amp[198]); 
  FFV1_0(w[61], w[52], w[1], pars->GC_11, amp[199]); 
  FFV1_0(w[53], w[58], w[3], pars->GC_11, amp[200]); 
  FFV1_0(w[54], w[58], w[1], pars->GC_11, amp[201]); 
  FFV1_0(w[53], w[60], w[3], pars->GC_11, amp[202]); 
  FFV1_0(w[54], w[60], w[1], pars->GC_11, amp[203]); 
  FFV1_0(w[6], w[58], w[55], pars->GC_11, amp[204]); 
  FFV1_0(w[6], w[58], w[56], pars->GC_11, amp[205]); 
  FFV1_0(w[6], w[58], w[57], pars->GC_11, amp[206]); 
  FFV1_0(w[59], w[5], w[55], pars->GC_11, amp[207]); 
  FFV1_0(w[59], w[5], w[56], pars->GC_11, amp[208]); 
  FFV1_0(w[59], w[5], w[57], pars->GC_11, amp[209]); 
  FFV1_0(w[6], w[60], w[55], pars->GC_11, amp[210]); 
  FFV1_0(w[6], w[60], w[56], pars->GC_11, amp[211]); 
  FFV1_0(w[6], w[60], w[57], pars->GC_11, amp[212]); 
  FFV1_0(w[61], w[5], w[55], pars->GC_11, amp[213]); 
  FFV1_0(w[61], w[5], w[56], pars->GC_11, amp[214]); 
  FFV1_0(w[61], w[5], w[57], pars->GC_11, amp[215]); 
  FFV1_0(w[6], w[77], w[10], pars->GC_11, amp[216]); 
  FFV1_0(w[11], w[77], w[4], pars->GC_11, amp[217]); 
  FFV1_0(w[78], w[5], w[10], pars->GC_11, amp[218]); 
  FFV1_0(w[78], w[13], w[4], pars->GC_11, amp[219]); 
  FFV1_0(w[6], w[80], w[10], pars->GC_11, amp[220]); 
  FFV1_0(w[11], w[80], w[4], pars->GC_11, amp[221]); 
  FFV1_0(w[81], w[5], w[10], pars->GC_11, amp[222]); 
  FFV1_0(w[81], w[13], w[4], pars->GC_11, amp[223]); 
  FFV1_0(w[6], w[82], w[8], pars->GC_11, amp[224]); 
  FFV1_0(w[78], w[17], w[8], pars->GC_11, amp[225]); 
  FFV1_0(w[6], w[83], w[8], pars->GC_11, amp[226]); 
  FFV1_0(w[81], w[17], w[8], pars->GC_11, amp[227]); 
  FFV1_0(w[84], w[5], w[8], pars->GC_11, amp[228]); 
  FFV1_0(w[20], w[77], w[8], pars->GC_11, amp[229]); 
  FFV1_0(w[85], w[5], w[8], pars->GC_11, amp[230]); 
  FFV1_0(w[20], w[80], w[8], pars->GC_11, amp[231]); 
  FFV1_0(w[6], w[77], w[24], pars->GC_11, amp[232]); 
  FFV1_0(w[25], w[77], w[3], pars->GC_11, amp[233]); 
  FFV1_0(w[78], w[5], w[24], pars->GC_11, amp[234]); 
  FFV1_0(w[78], w[26], w[3], pars->GC_11, amp[235]); 
  FFV1_0(w[6], w[80], w[24], pars->GC_11, amp[236]); 
  FFV1_0(w[25], w[80], w[3], pars->GC_11, amp[237]); 
  FFV1_0(w[81], w[5], w[24], pars->GC_11, amp[238]); 
  FFV1_0(w[81], w[26], w[3], pars->GC_11, amp[239]); 
  FFV1_0(w[6], w[86], w[23], pars->GC_11, amp[240]); 
  FFV1_0(w[78], w[27], w[23], pars->GC_11, amp[241]); 
  FFV1_0(w[6], w[87], w[23], pars->GC_11, amp[242]); 
  FFV1_0(w[81], w[27], w[23], pars->GC_11, amp[243]); 
  FFV1_0(w[88], w[5], w[23], pars->GC_11, amp[244]); 
  FFV1_0(w[30], w[77], w[23], pars->GC_11, amp[245]); 
  FFV1_0(w[89], w[5], w[23], pars->GC_11, amp[246]); 
  FFV1_0(w[30], w[80], w[23], pars->GC_11, amp[247]); 
  FFV1_0(w[78], w[34], w[4], pars->GC_11, amp[248]); 
  FFV1_0(w[78], w[35], w[3], pars->GC_11, amp[249]); 
  FFV1_0(w[81], w[34], w[4], pars->GC_11, amp[250]); 
  FFV1_0(w[81], w[35], w[3], pars->GC_11, amp[251]); 
  FFV1_0(w[6], w[90], w[36], pars->GC_11, amp[252]); 
  FFV1_0(w[78], w[33], w[36], pars->GC_11, amp[253]); 
  FFV1_0(w[6], w[91], w[36], pars->GC_11, amp[254]); 
  FFV1_0(w[81], w[33], w[36], pars->GC_11, amp[255]); 
  FFV1_0(w[30], w[90], w[4], pars->GC_11, amp[256]); 
  FFV1_0(w[88], w[33], w[4], pars->GC_11, amp[257]); 
  FFV1_0(w[30], w[91], w[4], pars->GC_11, amp[258]); 
  FFV1_0(w[89], w[33], w[4], pars->GC_11, amp[259]); 
  FFV1_0(w[20], w[90], w[3], pars->GC_11, amp[260]); 
  FFV1_0(w[84], w[33], w[3], pars->GC_11, amp[261]); 
  FFV1_0(w[20], w[91], w[3], pars->GC_11, amp[262]); 
  FFV1_0(w[85], w[33], w[3], pars->GC_11, amp[263]); 
  FFV1_0(w[40], w[77], w[4], pars->GC_11, amp[264]); 
  FFV1_0(w[41], w[77], w[3], pars->GC_11, amp[265]); 
  FFV1_0(w[40], w[80], w[4], pars->GC_11, amp[266]); 
  FFV1_0(w[41], w[80], w[3], pars->GC_11, amp[267]); 
  FFV1_0(w[92], w[5], w[36], pars->GC_11, amp[268]); 
  FFV1_0(w[39], w[77], w[36], pars->GC_11, amp[269]); 
  FFV1_0(w[93], w[5], w[36], pars->GC_11, amp[270]); 
  FFV1_0(w[39], w[80], w[36], pars->GC_11, amp[271]); 
  FFV1_0(w[92], w[27], w[4], pars->GC_11, amp[272]); 
  FFV1_0(w[39], w[86], w[4], pars->GC_11, amp[273]); 
  FFV1_0(w[93], w[27], w[4], pars->GC_11, amp[274]); 
  FFV1_0(w[39], w[87], w[4], pars->GC_11, amp[275]); 
  FFV1_0(w[92], w[17], w[3], pars->GC_11, amp[276]); 
  FFV1_0(w[39], w[82], w[3], pars->GC_11, amp[277]); 
  FFV1_0(w[93], w[17], w[3], pars->GC_11, amp[278]); 
  FFV1_0(w[39], w[83], w[3], pars->GC_11, amp[279]); 
  FFV1_0(w[6], w[77], w[44], pars->GC_11, amp[280]); 
  FFV1_0(w[45], w[77], w[1], pars->GC_11, amp[281]); 
  FFV1_0(w[78], w[5], w[44], pars->GC_11, amp[282]); 
  FFV1_0(w[78], w[46], w[1], pars->GC_11, amp[283]); 
  FFV1_0(w[6], w[80], w[44], pars->GC_11, amp[284]); 
  FFV1_0(w[45], w[80], w[1], pars->GC_11, amp[285]); 
  FFV1_0(w[81], w[5], w[44], pars->GC_11, amp[286]); 
  FFV1_0(w[81], w[46], w[1], pars->GC_11, amp[287]); 
  FFV1_0(w[78], w[47], w[4], pars->GC_11, amp[288]); 
  FFV1_0(w[78], w[48], w[1], pars->GC_11, amp[289]); 
  FFV1_0(w[81], w[47], w[4], pars->GC_11, amp[290]); 
  FFV1_0(w[81], w[48], w[1], pars->GC_11, amp[291]); 
  FFV1_0(w[20], w[86], w[1], pars->GC_11, amp[292]); 
  FFV1_0(w[84], w[27], w[1], pars->GC_11, amp[293]); 
  FFV1_0(w[20], w[87], w[1], pars->GC_11, amp[294]); 
  FFV1_0(w[85], w[27], w[1], pars->GC_11, amp[295]); 
  FFV1_0(w[49], w[77], w[4], pars->GC_11, amp[296]); 
  FFV1_0(w[50], w[77], w[1], pars->GC_11, amp[297]); 
  FFV1_0(w[49], w[80], w[4], pars->GC_11, amp[298]); 
  FFV1_0(w[50], w[80], w[1], pars->GC_11, amp[299]); 
  FFV1_0(w[88], w[17], w[1], pars->GC_11, amp[300]); 
  FFV1_0(w[30], w[82], w[1], pars->GC_11, amp[301]); 
  FFV1_0(w[89], w[17], w[1], pars->GC_11, amp[302]); 
  FFV1_0(w[30], w[83], w[1], pars->GC_11, amp[303]); 
  FFV1_0(w[78], w[51], w[3], pars->GC_11, amp[304]); 
  FFV1_0(w[78], w[52], w[1], pars->GC_11, amp[305]); 
  FFV1_0(w[81], w[51], w[3], pars->GC_11, amp[306]); 
  FFV1_0(w[81], w[52], w[1], pars->GC_11, amp[307]); 
  FFV1_0(w[53], w[77], w[3], pars->GC_11, amp[308]); 
  FFV1_0(w[54], w[77], w[1], pars->GC_11, amp[309]); 
  FFV1_0(w[53], w[80], w[3], pars->GC_11, amp[310]); 
  FFV1_0(w[54], w[80], w[1], pars->GC_11, amp[311]); 
  FFV1_0(w[6], w[77], w[55], pars->GC_11, amp[312]); 
  FFV1_0(w[6], w[77], w[56], pars->GC_11, amp[313]); 
  FFV1_0(w[6], w[77], w[57], pars->GC_11, amp[314]); 
  FFV1_0(w[78], w[5], w[55], pars->GC_11, amp[315]); 
  FFV1_0(w[78], w[5], w[56], pars->GC_11, amp[316]); 
  FFV1_0(w[78], w[5], w[57], pars->GC_11, amp[317]); 
  FFV1_0(w[6], w[80], w[55], pars->GC_11, amp[318]); 
  FFV1_0(w[6], w[80], w[56], pars->GC_11, amp[319]); 
  FFV1_0(w[6], w[80], w[57], pars->GC_11, amp[320]); 
  FFV1_0(w[81], w[5], w[55], pars->GC_11, amp[321]); 
  FFV1_0(w[81], w[5], w[56], pars->GC_11, amp[322]); 
  FFV1_0(w[81], w[5], w[57], pars->GC_11, amp[323]); 
  FFV1_0(w[6], w[94], w[10], pars->GC_11, amp[324]); 
  FFV1_0(w[11], w[94], w[4], pars->GC_11, amp[325]); 
  FFV1_0(w[95], w[5], w[10], pars->GC_11, amp[326]); 
  FFV1_0(w[95], w[13], w[4], pars->GC_11, amp[327]); 
  FFV1_0(w[6], w[96], w[10], pars->GC_11, amp[328]); 
  FFV1_0(w[11], w[96], w[4], pars->GC_11, amp[329]); 
  FFV1_0(w[97], w[5], w[10], pars->GC_11, amp[330]); 
  FFV1_0(w[97], w[13], w[4], pars->GC_11, amp[331]); 
  FFV1_0(w[6], w[98], w[8], pars->GC_11, amp[332]); 
  FFV1_0(w[95], w[17], w[8], pars->GC_11, amp[333]); 
  FFV1_0(w[6], w[99], w[8], pars->GC_11, amp[334]); 
  FFV1_0(w[97], w[17], w[8], pars->GC_11, amp[335]); 
  FFV1_0(w[100], w[5], w[8], pars->GC_11, amp[336]); 
  FFV1_0(w[20], w[94], w[8], pars->GC_11, amp[337]); 
  FFV1_0(w[101], w[5], w[8], pars->GC_11, amp[338]); 
  FFV1_0(w[20], w[96], w[8], pars->GC_11, amp[339]); 
  FFV1_0(w[6], w[94], w[24], pars->GC_11, amp[340]); 
  FFV1_0(w[25], w[94], w[3], pars->GC_11, amp[341]); 
  FFV1_0(w[95], w[5], w[24], pars->GC_11, amp[342]); 
  FFV1_0(w[95], w[26], w[3], pars->GC_11, amp[343]); 
  FFV1_0(w[6], w[96], w[24], pars->GC_11, amp[344]); 
  FFV1_0(w[25], w[96], w[3], pars->GC_11, amp[345]); 
  FFV1_0(w[97], w[5], w[24], pars->GC_11, amp[346]); 
  FFV1_0(w[97], w[26], w[3], pars->GC_11, amp[347]); 
  FFV1_0(w[6], w[102], w[23], pars->GC_11, amp[348]); 
  FFV1_0(w[95], w[27], w[23], pars->GC_11, amp[349]); 
  FFV1_0(w[6], w[103], w[23], pars->GC_11, amp[350]); 
  FFV1_0(w[97], w[27], w[23], pars->GC_11, amp[351]); 
  FFV1_0(w[104], w[5], w[23], pars->GC_11, amp[352]); 
  FFV1_0(w[30], w[94], w[23], pars->GC_11, amp[353]); 
  FFV1_0(w[105], w[5], w[23], pars->GC_11, amp[354]); 
  FFV1_0(w[30], w[96], w[23], pars->GC_11, amp[355]); 
  FFV1_0(w[95], w[34], w[4], pars->GC_11, amp[356]); 
  FFV1_0(w[95], w[35], w[3], pars->GC_11, amp[357]); 
  FFV1_0(w[97], w[34], w[4], pars->GC_11, amp[358]); 
  FFV1_0(w[97], w[35], w[3], pars->GC_11, amp[359]); 
  FFV1_0(w[6], w[106], w[36], pars->GC_11, amp[360]); 
  FFV1_0(w[95], w[33], w[36], pars->GC_11, amp[361]); 
  FFV1_0(w[6], w[107], w[36], pars->GC_11, amp[362]); 
  FFV1_0(w[97], w[33], w[36], pars->GC_11, amp[363]); 
  FFV1_0(w[30], w[106], w[4], pars->GC_11, amp[364]); 
  FFV1_0(w[104], w[33], w[4], pars->GC_11, amp[365]); 
  FFV1_0(w[30], w[107], w[4], pars->GC_11, amp[366]); 
  FFV1_0(w[105], w[33], w[4], pars->GC_11, amp[367]); 
  FFV1_0(w[20], w[106], w[3], pars->GC_11, amp[368]); 
  FFV1_0(w[100], w[33], w[3], pars->GC_11, amp[369]); 
  FFV1_0(w[20], w[107], w[3], pars->GC_11, amp[370]); 
  FFV1_0(w[101], w[33], w[3], pars->GC_11, amp[371]); 
  FFV1_0(w[40], w[94], w[4], pars->GC_11, amp[372]); 
  FFV1_0(w[41], w[94], w[3], pars->GC_11, amp[373]); 
  FFV1_0(w[40], w[96], w[4], pars->GC_11, amp[374]); 
  FFV1_0(w[41], w[96], w[3], pars->GC_11, amp[375]); 
  FFV1_0(w[108], w[5], w[36], pars->GC_11, amp[376]); 
  FFV1_0(w[39], w[94], w[36], pars->GC_11, amp[377]); 
  FFV1_0(w[109], w[5], w[36], pars->GC_11, amp[378]); 
  FFV1_0(w[39], w[96], w[36], pars->GC_11, amp[379]); 
  FFV1_0(w[108], w[27], w[4], pars->GC_11, amp[380]); 
  FFV1_0(w[39], w[102], w[4], pars->GC_11, amp[381]); 
  FFV1_0(w[109], w[27], w[4], pars->GC_11, amp[382]); 
  FFV1_0(w[39], w[103], w[4], pars->GC_11, amp[383]); 
  FFV1_0(w[108], w[17], w[3], pars->GC_11, amp[384]); 
  FFV1_0(w[39], w[98], w[3], pars->GC_11, amp[385]); 
  FFV1_0(w[109], w[17], w[3], pars->GC_11, amp[386]); 
  FFV1_0(w[39], w[99], w[3], pars->GC_11, amp[387]); 
  FFV1_0(w[6], w[94], w[44], pars->GC_11, amp[388]); 
  FFV1_0(w[45], w[94], w[1], pars->GC_11, amp[389]); 
  FFV1_0(w[95], w[5], w[44], pars->GC_11, amp[390]); 
  FFV1_0(w[95], w[46], w[1], pars->GC_11, amp[391]); 
  FFV1_0(w[6], w[96], w[44], pars->GC_11, amp[392]); 
  FFV1_0(w[45], w[96], w[1], pars->GC_11, amp[393]); 
  FFV1_0(w[97], w[5], w[44], pars->GC_11, amp[394]); 
  FFV1_0(w[97], w[46], w[1], pars->GC_11, amp[395]); 
  FFV1_0(w[95], w[47], w[4], pars->GC_11, amp[396]); 
  FFV1_0(w[95], w[48], w[1], pars->GC_11, amp[397]); 
  FFV1_0(w[97], w[47], w[4], pars->GC_11, amp[398]); 
  FFV1_0(w[97], w[48], w[1], pars->GC_11, amp[399]); 
  FFV1_0(w[20], w[102], w[1], pars->GC_11, amp[400]); 
  FFV1_0(w[100], w[27], w[1], pars->GC_11, amp[401]); 
  FFV1_0(w[20], w[103], w[1], pars->GC_11, amp[402]); 
  FFV1_0(w[101], w[27], w[1], pars->GC_11, amp[403]); 
  FFV1_0(w[49], w[94], w[4], pars->GC_11, amp[404]); 
  FFV1_0(w[50], w[94], w[1], pars->GC_11, amp[405]); 
  FFV1_0(w[49], w[96], w[4], pars->GC_11, amp[406]); 
  FFV1_0(w[50], w[96], w[1], pars->GC_11, amp[407]); 
  FFV1_0(w[104], w[17], w[1], pars->GC_11, amp[408]); 
  FFV1_0(w[30], w[98], w[1], pars->GC_11, amp[409]); 
  FFV1_0(w[105], w[17], w[1], pars->GC_11, amp[410]); 
  FFV1_0(w[30], w[99], w[1], pars->GC_11, amp[411]); 
  FFV1_0(w[95], w[51], w[3], pars->GC_11, amp[412]); 
  FFV1_0(w[95], w[52], w[1], pars->GC_11, amp[413]); 
  FFV1_0(w[97], w[51], w[3], pars->GC_11, amp[414]); 
  FFV1_0(w[97], w[52], w[1], pars->GC_11, amp[415]); 
  FFV1_0(w[53], w[94], w[3], pars->GC_11, amp[416]); 
  FFV1_0(w[54], w[94], w[1], pars->GC_11, amp[417]); 
  FFV1_0(w[53], w[96], w[3], pars->GC_11, amp[418]); 
  FFV1_0(w[54], w[96], w[1], pars->GC_11, amp[419]); 
  FFV1_0(w[6], w[94], w[55], pars->GC_11, amp[420]); 
  FFV1_0(w[6], w[94], w[56], pars->GC_11, amp[421]); 
  FFV1_0(w[6], w[94], w[57], pars->GC_11, amp[422]); 
  FFV1_0(w[95], w[5], w[55], pars->GC_11, amp[423]); 
  FFV1_0(w[95], w[5], w[56], pars->GC_11, amp[424]); 
  FFV1_0(w[95], w[5], w[57], pars->GC_11, amp[425]); 
  FFV1_0(w[6], w[96], w[55], pars->GC_11, amp[426]); 
  FFV1_0(w[6], w[96], w[56], pars->GC_11, amp[427]); 
  FFV1_0(w[6], w[96], w[57], pars->GC_11, amp[428]); 
  FFV1_0(w[97], w[5], w[55], pars->GC_11, amp[429]); 
  FFV1_0(w[97], w[5], w[56], pars->GC_11, amp[430]); 
  FFV1_0(w[97], w[5], w[57], pars->GC_11, amp[431]); 
  FFV1_0(w[6], w[111], w[10], pars->GC_11, amp[432]); 
  FFV1_0(w[11], w[111], w[4], pars->GC_11, amp[433]); 
  FFV1_0(w[112], w[5], w[10], pars->GC_11, amp[434]); 
  FFV1_0(w[112], w[13], w[4], pars->GC_11, amp[435]); 
  FFV1_0(w[6], w[113], w[8], pars->GC_11, amp[436]); 
  FFV1_0(w[112], w[17], w[8], pars->GC_11, amp[437]); 
  FFV1_0(w[114], w[5], w[8], pars->GC_11, amp[438]); 
  FFV1_0(w[20], w[111], w[8], pars->GC_11, amp[439]); 
  FFV1_0(w[6], w[111], w[24], pars->GC_11, amp[440]); 
  FFV1_0(w[25], w[111], w[3], pars->GC_11, amp[441]); 
  FFV1_0(w[112], w[5], w[24], pars->GC_11, amp[442]); 
  FFV1_0(w[112], w[26], w[3], pars->GC_11, amp[443]); 
  FFV1_0(w[6], w[115], w[23], pars->GC_11, amp[444]); 
  FFV1_0(w[112], w[27], w[23], pars->GC_11, amp[445]); 
  FFV1_0(w[116], w[5], w[23], pars->GC_11, amp[446]); 
  FFV1_0(w[30], w[111], w[23], pars->GC_11, amp[447]); 
  FFV1_0(w[112], w[34], w[4], pars->GC_11, amp[448]); 
  FFV1_0(w[112], w[35], w[3], pars->GC_11, amp[449]); 
  FFV1_0(w[6], w[117], w[36], pars->GC_11, amp[450]); 
  FFV1_0(w[112], w[33], w[36], pars->GC_11, amp[451]); 
  FFV1_0(w[30], w[117], w[4], pars->GC_11, amp[452]); 
  FFV1_0(w[116], w[33], w[4], pars->GC_11, amp[453]); 
  FFV1_0(w[20], w[117], w[3], pars->GC_11, amp[454]); 
  FFV1_0(w[114], w[33], w[3], pars->GC_11, amp[455]); 
  FFV1_0(w[40], w[111], w[4], pars->GC_11, amp[456]); 
  FFV1_0(w[41], w[111], w[3], pars->GC_11, amp[457]); 
  FFV1_0(w[118], w[5], w[36], pars->GC_11, amp[458]); 
  FFV1_0(w[39], w[111], w[36], pars->GC_11, amp[459]); 
  FFV1_0(w[118], w[27], w[4], pars->GC_11, amp[460]); 
  FFV1_0(w[39], w[115], w[4], pars->GC_11, amp[461]); 
  FFV1_0(w[118], w[17], w[3], pars->GC_11, amp[462]); 
  FFV1_0(w[39], w[113], w[3], pars->GC_11, amp[463]); 
  FFV1_0(w[6], w[111], w[44], pars->GC_11, amp[464]); 
  FFV1_0(w[45], w[111], w[1], pars->GC_11, amp[465]); 
  FFV1_0(w[112], w[5], w[44], pars->GC_11, amp[466]); 
  FFV1_0(w[112], w[46], w[1], pars->GC_11, amp[467]); 
  FFV1_0(w[112], w[47], w[4], pars->GC_11, amp[468]); 
  FFV1_0(w[112], w[48], w[1], pars->GC_11, amp[469]); 
  FFV1_0(w[20], w[115], w[1], pars->GC_11, amp[470]); 
  FFV1_0(w[114], w[27], w[1], pars->GC_11, amp[471]); 
  FFV1_0(w[49], w[111], w[4], pars->GC_11, amp[472]); 
  FFV1_0(w[50], w[111], w[1], pars->GC_11, amp[473]); 
  FFV1_0(w[116], w[17], w[1], pars->GC_11, amp[474]); 
  FFV1_0(w[30], w[113], w[1], pars->GC_11, amp[475]); 
  FFV1_0(w[112], w[51], w[3], pars->GC_11, amp[476]); 
  FFV1_0(w[112], w[52], w[1], pars->GC_11, amp[477]); 
  FFV1_0(w[53], w[111], w[3], pars->GC_11, amp[478]); 
  FFV1_0(w[54], w[111], w[1], pars->GC_11, amp[479]); 
  FFV1_0(w[6], w[111], w[55], pars->GC_11, amp[480]); 
  FFV1_0(w[6], w[111], w[56], pars->GC_11, amp[481]); 
  FFV1_0(w[6], w[111], w[57], pars->GC_11, amp[482]); 
  FFV1_0(w[112], w[5], w[55], pars->GC_11, amp[483]); 
  FFV1_0(w[112], w[5], w[56], pars->GC_11, amp[484]); 
  FFV1_0(w[112], w[5], w[57], pars->GC_11, amp[485]); 
  FFV1_0(w[6], w[120], w[10], pars->GC_11, amp[486]); 
  FFV1_0(w[11], w[120], w[4], pars->GC_11, amp[487]); 
  FFV1_0(w[121], w[5], w[10], pars->GC_11, amp[488]); 
  FFV1_0(w[121], w[13], w[4], pars->GC_11, amp[489]); 
  FFV1_0(w[6], w[122], w[8], pars->GC_11, amp[490]); 
  FFV1_0(w[121], w[17], w[8], pars->GC_11, amp[491]); 
  FFV1_0(w[123], w[5], w[8], pars->GC_11, amp[492]); 
  FFV1_0(w[20], w[120], w[8], pars->GC_11, amp[493]); 
  FFV1_0(w[6], w[120], w[24], pars->GC_11, amp[494]); 
  FFV1_0(w[25], w[120], w[3], pars->GC_11, amp[495]); 
  FFV1_0(w[121], w[5], w[24], pars->GC_11, amp[496]); 
  FFV1_0(w[121], w[26], w[3], pars->GC_11, amp[497]); 
  FFV1_0(w[6], w[124], w[23], pars->GC_11, amp[498]); 
  FFV1_0(w[121], w[27], w[23], pars->GC_11, amp[499]); 
  FFV1_0(w[125], w[5], w[23], pars->GC_11, amp[500]); 
  FFV1_0(w[30], w[120], w[23], pars->GC_11, amp[501]); 
  FFV1_0(w[121], w[34], w[4], pars->GC_11, amp[502]); 
  FFV1_0(w[121], w[35], w[3], pars->GC_11, amp[503]); 
  FFV1_0(w[6], w[126], w[36], pars->GC_11, amp[504]); 
  FFV1_0(w[121], w[33], w[36], pars->GC_11, amp[505]); 
  FFV1_0(w[30], w[126], w[4], pars->GC_11, amp[506]); 
  FFV1_0(w[125], w[33], w[4], pars->GC_11, amp[507]); 
  FFV1_0(w[20], w[126], w[3], pars->GC_11, amp[508]); 
  FFV1_0(w[123], w[33], w[3], pars->GC_11, amp[509]); 
  FFV1_0(w[40], w[120], w[4], pars->GC_11, amp[510]); 
  FFV1_0(w[41], w[120], w[3], pars->GC_11, amp[511]); 
  FFV1_0(w[127], w[5], w[36], pars->GC_11, amp[512]); 
  FFV1_0(w[39], w[120], w[36], pars->GC_11, amp[513]); 
  FFV1_0(w[127], w[27], w[4], pars->GC_11, amp[514]); 
  FFV1_0(w[39], w[124], w[4], pars->GC_11, amp[515]); 
  FFV1_0(w[127], w[17], w[3], pars->GC_11, amp[516]); 
  FFV1_0(w[39], w[122], w[3], pars->GC_11, amp[517]); 
  FFV1_0(w[6], w[120], w[44], pars->GC_11, amp[518]); 
  FFV1_0(w[45], w[120], w[1], pars->GC_11, amp[519]); 
  FFV1_0(w[121], w[5], w[44], pars->GC_11, amp[520]); 
  FFV1_0(w[121], w[46], w[1], pars->GC_11, amp[521]); 
  FFV1_0(w[121], w[47], w[4], pars->GC_11, amp[522]); 
  FFV1_0(w[121], w[48], w[1], pars->GC_11, amp[523]); 
  FFV1_0(w[20], w[124], w[1], pars->GC_11, amp[524]); 
  FFV1_0(w[123], w[27], w[1], pars->GC_11, amp[525]); 
  FFV1_0(w[49], w[120], w[4], pars->GC_11, amp[526]); 
  FFV1_0(w[50], w[120], w[1], pars->GC_11, amp[527]); 
  FFV1_0(w[125], w[17], w[1], pars->GC_11, amp[528]); 
  FFV1_0(w[30], w[122], w[1], pars->GC_11, amp[529]); 
  FFV1_0(w[121], w[51], w[3], pars->GC_11, amp[530]); 
  FFV1_0(w[121], w[52], w[1], pars->GC_11, amp[531]); 
  FFV1_0(w[53], w[120], w[3], pars->GC_11, amp[532]); 
  FFV1_0(w[54], w[120], w[1], pars->GC_11, amp[533]); 
  FFV1_0(w[6], w[120], w[55], pars->GC_11, amp[534]); 
  FFV1_0(w[6], w[120], w[56], pars->GC_11, amp[535]); 
  FFV1_0(w[6], w[120], w[57], pars->GC_11, amp[536]); 
  FFV1_0(w[121], w[5], w[55], pars->GC_11, amp[537]); 
  FFV1_0(w[121], w[5], w[56], pars->GC_11, amp[538]); 
  FFV1_0(w[121], w[5], w[57], pars->GC_11, amp[539]); 
  FFV1_0(w[6], w[128], w[10], pars->GC_11, amp[540]); 
  FFV1_0(w[11], w[128], w[4], pars->GC_11, amp[541]); 
  FFV1_0(w[129], w[5], w[10], pars->GC_11, amp[542]); 
  FFV1_0(w[129], w[13], w[4], pars->GC_11, amp[543]); 
  FFV1_0(w[6], w[130], w[8], pars->GC_11, amp[544]); 
  FFV1_0(w[129], w[17], w[8], pars->GC_11, amp[545]); 
  FFV1_0(w[131], w[5], w[8], pars->GC_11, amp[546]); 
  FFV1_0(w[20], w[128], w[8], pars->GC_11, amp[547]); 
  FFV1_0(w[6], w[128], w[24], pars->GC_11, amp[548]); 
  FFV1_0(w[25], w[128], w[3], pars->GC_11, amp[549]); 
  FFV1_0(w[129], w[5], w[24], pars->GC_11, amp[550]); 
  FFV1_0(w[129], w[26], w[3], pars->GC_11, amp[551]); 
  FFV1_0(w[6], w[132], w[23], pars->GC_11, amp[552]); 
  FFV1_0(w[129], w[27], w[23], pars->GC_11, amp[553]); 
  FFV1_0(w[133], w[5], w[23], pars->GC_11, amp[554]); 
  FFV1_0(w[30], w[128], w[23], pars->GC_11, amp[555]); 
  FFV1_0(w[129], w[34], w[4], pars->GC_11, amp[556]); 
  FFV1_0(w[129], w[35], w[3], pars->GC_11, amp[557]); 
  FFV1_0(w[6], w[134], w[36], pars->GC_11, amp[558]); 
  FFV1_0(w[129], w[33], w[36], pars->GC_11, amp[559]); 
  FFV1_0(w[30], w[134], w[4], pars->GC_11, amp[560]); 
  FFV1_0(w[133], w[33], w[4], pars->GC_11, amp[561]); 
  FFV1_0(w[20], w[134], w[3], pars->GC_11, amp[562]); 
  FFV1_0(w[131], w[33], w[3], pars->GC_11, amp[563]); 
  FFV1_0(w[40], w[128], w[4], pars->GC_11, amp[564]); 
  FFV1_0(w[41], w[128], w[3], pars->GC_11, amp[565]); 
  FFV1_0(w[135], w[5], w[36], pars->GC_11, amp[566]); 
  FFV1_0(w[39], w[128], w[36], pars->GC_11, amp[567]); 
  FFV1_0(w[135], w[27], w[4], pars->GC_11, amp[568]); 
  FFV1_0(w[39], w[132], w[4], pars->GC_11, amp[569]); 
  FFV1_0(w[135], w[17], w[3], pars->GC_11, amp[570]); 
  FFV1_0(w[39], w[130], w[3], pars->GC_11, amp[571]); 
  FFV1_0(w[6], w[128], w[44], pars->GC_11, amp[572]); 
  FFV1_0(w[45], w[128], w[1], pars->GC_11, amp[573]); 
  FFV1_0(w[129], w[5], w[44], pars->GC_11, amp[574]); 
  FFV1_0(w[129], w[46], w[1], pars->GC_11, amp[575]); 
  FFV1_0(w[129], w[47], w[4], pars->GC_11, amp[576]); 
  FFV1_0(w[129], w[48], w[1], pars->GC_11, amp[577]); 
  FFV1_0(w[20], w[132], w[1], pars->GC_11, amp[578]); 
  FFV1_0(w[131], w[27], w[1], pars->GC_11, amp[579]); 
  FFV1_0(w[49], w[128], w[4], pars->GC_11, amp[580]); 
  FFV1_0(w[50], w[128], w[1], pars->GC_11, amp[581]); 
  FFV1_0(w[133], w[17], w[1], pars->GC_11, amp[582]); 
  FFV1_0(w[30], w[130], w[1], pars->GC_11, amp[583]); 
  FFV1_0(w[129], w[51], w[3], pars->GC_11, amp[584]); 
  FFV1_0(w[129], w[52], w[1], pars->GC_11, amp[585]); 
  FFV1_0(w[53], w[128], w[3], pars->GC_11, amp[586]); 
  FFV1_0(w[54], w[128], w[1], pars->GC_11, amp[587]); 
  FFV1_0(w[6], w[128], w[55], pars->GC_11, amp[588]); 
  FFV1_0(w[6], w[128], w[56], pars->GC_11, amp[589]); 
  FFV1_0(w[6], w[128], w[57], pars->GC_11, amp[590]); 
  FFV1_0(w[129], w[5], w[55], pars->GC_11, amp[591]); 
  FFV1_0(w[129], w[5], w[56], pars->GC_11, amp[592]); 
  FFV1_0(w[129], w[5], w[57], pars->GC_11, amp[593]); 
  FFV1_0(w[6], w[137], w[10], pars->GC_11, amp[594]); 
  FFV1_0(w[11], w[137], w[4], pars->GC_11, amp[595]); 
  FFV1_0(w[138], w[5], w[10], pars->GC_11, amp[596]); 
  FFV1_0(w[138], w[13], w[4], pars->GC_11, amp[597]); 
  FFV1_0(w[6], w[139], w[8], pars->GC_11, amp[598]); 
  FFV1_0(w[138], w[17], w[8], pars->GC_11, amp[599]); 
  FFV1_0(w[140], w[5], w[8], pars->GC_11, amp[600]); 
  FFV1_0(w[20], w[137], w[8], pars->GC_11, amp[601]); 
  FFV1_0(w[6], w[137], w[24], pars->GC_11, amp[602]); 
  FFV1_0(w[25], w[137], w[3], pars->GC_11, amp[603]); 
  FFV1_0(w[138], w[5], w[24], pars->GC_11, amp[604]); 
  FFV1_0(w[138], w[26], w[3], pars->GC_11, amp[605]); 
  FFV1_0(w[6], w[141], w[23], pars->GC_11, amp[606]); 
  FFV1_0(w[138], w[27], w[23], pars->GC_11, amp[607]); 
  FFV1_0(w[142], w[5], w[23], pars->GC_11, amp[608]); 
  FFV1_0(w[30], w[137], w[23], pars->GC_11, amp[609]); 
  FFV1_0(w[138], w[34], w[4], pars->GC_11, amp[610]); 
  FFV1_0(w[138], w[35], w[3], pars->GC_11, amp[611]); 
  FFV1_0(w[6], w[143], w[36], pars->GC_11, amp[612]); 
  FFV1_0(w[138], w[33], w[36], pars->GC_11, amp[613]); 
  FFV1_0(w[30], w[143], w[4], pars->GC_11, amp[614]); 
  FFV1_0(w[142], w[33], w[4], pars->GC_11, amp[615]); 
  FFV1_0(w[20], w[143], w[3], pars->GC_11, amp[616]); 
  FFV1_0(w[140], w[33], w[3], pars->GC_11, amp[617]); 
  FFV1_0(w[40], w[137], w[4], pars->GC_11, amp[618]); 
  FFV1_0(w[41], w[137], w[3], pars->GC_11, amp[619]); 
  FFV1_0(w[144], w[5], w[36], pars->GC_11, amp[620]); 
  FFV1_0(w[39], w[137], w[36], pars->GC_11, amp[621]); 
  FFV1_0(w[144], w[27], w[4], pars->GC_11, amp[622]); 
  FFV1_0(w[39], w[141], w[4], pars->GC_11, amp[623]); 
  FFV1_0(w[144], w[17], w[3], pars->GC_11, amp[624]); 
  FFV1_0(w[39], w[139], w[3], pars->GC_11, amp[625]); 
  FFV1_0(w[6], w[137], w[44], pars->GC_11, amp[626]); 
  FFV1_0(w[45], w[137], w[1], pars->GC_11, amp[627]); 
  FFV1_0(w[138], w[5], w[44], pars->GC_11, amp[628]); 
  FFV1_0(w[138], w[46], w[1], pars->GC_11, amp[629]); 
  FFV1_0(w[138], w[47], w[4], pars->GC_11, amp[630]); 
  FFV1_0(w[138], w[48], w[1], pars->GC_11, amp[631]); 
  FFV1_0(w[20], w[141], w[1], pars->GC_11, amp[632]); 
  FFV1_0(w[140], w[27], w[1], pars->GC_11, amp[633]); 
  FFV1_0(w[49], w[137], w[4], pars->GC_11, amp[634]); 
  FFV1_0(w[50], w[137], w[1], pars->GC_11, amp[635]); 
  FFV1_0(w[142], w[17], w[1], pars->GC_11, amp[636]); 
  FFV1_0(w[30], w[139], w[1], pars->GC_11, amp[637]); 
  FFV1_0(w[138], w[51], w[3], pars->GC_11, amp[638]); 
  FFV1_0(w[138], w[52], w[1], pars->GC_11, amp[639]); 
  FFV1_0(w[53], w[137], w[3], pars->GC_11, amp[640]); 
  FFV1_0(w[54], w[137], w[1], pars->GC_11, amp[641]); 
  FFV1_0(w[6], w[137], w[55], pars->GC_11, amp[642]); 
  FFV1_0(w[6], w[137], w[56], pars->GC_11, amp[643]); 
  FFV1_0(w[6], w[137], w[57], pars->GC_11, amp[644]); 
  FFV1_0(w[138], w[5], w[55], pars->GC_11, amp[645]); 
  FFV1_0(w[138], w[5], w[56], pars->GC_11, amp[646]); 
  FFV1_0(w[138], w[5], w[57], pars->GC_11, amp[647]); 
  FFV1_0(w[6], w[146], w[10], pars->GC_11, amp[648]); 
  FFV1_0(w[11], w[146], w[4], pars->GC_11, amp[649]); 
  FFV1_0(w[147], w[5], w[10], pars->GC_11, amp[650]); 
  FFV1_0(w[147], w[13], w[4], pars->GC_11, amp[651]); 
  FFV1_0(w[6], w[148], w[8], pars->GC_11, amp[652]); 
  FFV1_0(w[147], w[17], w[8], pars->GC_11, amp[653]); 
  FFV1_0(w[149], w[5], w[8], pars->GC_11, amp[654]); 
  FFV1_0(w[20], w[146], w[8], pars->GC_11, amp[655]); 
  FFV1_0(w[6], w[146], w[24], pars->GC_11, amp[656]); 
  FFV1_0(w[25], w[146], w[3], pars->GC_11, amp[657]); 
  FFV1_0(w[147], w[5], w[24], pars->GC_11, amp[658]); 
  FFV1_0(w[147], w[26], w[3], pars->GC_11, amp[659]); 
  FFV1_0(w[6], w[150], w[23], pars->GC_11, amp[660]); 
  FFV1_0(w[147], w[27], w[23], pars->GC_11, amp[661]); 
  FFV1_0(w[151], w[5], w[23], pars->GC_11, amp[662]); 
  FFV1_0(w[30], w[146], w[23], pars->GC_11, amp[663]); 
  FFV1_0(w[147], w[34], w[4], pars->GC_11, amp[664]); 
  FFV1_0(w[147], w[35], w[3], pars->GC_11, amp[665]); 
  FFV1_0(w[6], w[152], w[36], pars->GC_11, amp[666]); 
  FFV1_0(w[147], w[33], w[36], pars->GC_11, amp[667]); 
  FFV1_0(w[30], w[152], w[4], pars->GC_11, amp[668]); 
  FFV1_0(w[151], w[33], w[4], pars->GC_11, amp[669]); 
  FFV1_0(w[20], w[152], w[3], pars->GC_11, amp[670]); 
  FFV1_0(w[149], w[33], w[3], pars->GC_11, amp[671]); 
  FFV1_0(w[40], w[146], w[4], pars->GC_11, amp[672]); 
  FFV1_0(w[41], w[146], w[3], pars->GC_11, amp[673]); 
  FFV1_0(w[153], w[5], w[36], pars->GC_11, amp[674]); 
  FFV1_0(w[39], w[146], w[36], pars->GC_11, amp[675]); 
  FFV1_0(w[153], w[27], w[4], pars->GC_11, amp[676]); 
  FFV1_0(w[39], w[150], w[4], pars->GC_11, amp[677]); 
  FFV1_0(w[153], w[17], w[3], pars->GC_11, amp[678]); 
  FFV1_0(w[39], w[148], w[3], pars->GC_11, amp[679]); 
  FFV1_0(w[6], w[146], w[44], pars->GC_11, amp[680]); 
  FFV1_0(w[45], w[146], w[1], pars->GC_11, amp[681]); 
  FFV1_0(w[147], w[5], w[44], pars->GC_11, amp[682]); 
  FFV1_0(w[147], w[46], w[1], pars->GC_11, amp[683]); 
  FFV1_0(w[147], w[47], w[4], pars->GC_11, amp[684]); 
  FFV1_0(w[147], w[48], w[1], pars->GC_11, amp[685]); 
  FFV1_0(w[20], w[150], w[1], pars->GC_11, amp[686]); 
  FFV1_0(w[149], w[27], w[1], pars->GC_11, amp[687]); 
  FFV1_0(w[49], w[146], w[4], pars->GC_11, amp[688]); 
  FFV1_0(w[50], w[146], w[1], pars->GC_11, amp[689]); 
  FFV1_0(w[151], w[17], w[1], pars->GC_11, amp[690]); 
  FFV1_0(w[30], w[148], w[1], pars->GC_11, amp[691]); 
  FFV1_0(w[147], w[51], w[3], pars->GC_11, amp[692]); 
  FFV1_0(w[147], w[52], w[1], pars->GC_11, amp[693]); 
  FFV1_0(w[53], w[146], w[3], pars->GC_11, amp[694]); 
  FFV1_0(w[54], w[146], w[1], pars->GC_11, amp[695]); 
  FFV1_0(w[6], w[146], w[55], pars->GC_11, amp[696]); 
  FFV1_0(w[6], w[146], w[56], pars->GC_11, amp[697]); 
  FFV1_0(w[6], w[146], w[57], pars->GC_11, amp[698]); 
  FFV1_0(w[147], w[5], w[55], pars->GC_11, amp[699]); 
  FFV1_0(w[147], w[5], w[56], pars->GC_11, amp[700]); 
  FFV1_0(w[147], w[5], w[57], pars->GC_11, amp[701]); 
  FFV1_0(w[6], w[154], w[10], pars->GC_11, amp[702]); 
  FFV1_0(w[11], w[154], w[4], pars->GC_11, amp[703]); 
  FFV1_0(w[155], w[5], w[10], pars->GC_11, amp[704]); 
  FFV1_0(w[155], w[13], w[4], pars->GC_11, amp[705]); 
  FFV1_0(w[6], w[156], w[8], pars->GC_11, amp[706]); 
  FFV1_0(w[155], w[17], w[8], pars->GC_11, amp[707]); 
  FFV1_0(w[157], w[5], w[8], pars->GC_11, amp[708]); 
  FFV1_0(w[20], w[154], w[8], pars->GC_11, amp[709]); 
  FFV1_0(w[6], w[154], w[24], pars->GC_11, amp[710]); 
  FFV1_0(w[25], w[154], w[3], pars->GC_11, amp[711]); 
  FFV1_0(w[155], w[5], w[24], pars->GC_11, amp[712]); 
  FFV1_0(w[155], w[26], w[3], pars->GC_11, amp[713]); 
  FFV1_0(w[6], w[158], w[23], pars->GC_11, amp[714]); 
  FFV1_0(w[155], w[27], w[23], pars->GC_11, amp[715]); 
  FFV1_0(w[159], w[5], w[23], pars->GC_11, amp[716]); 
  FFV1_0(w[30], w[154], w[23], pars->GC_11, amp[717]); 
  FFV1_0(w[155], w[34], w[4], pars->GC_11, amp[718]); 
  FFV1_0(w[155], w[35], w[3], pars->GC_11, amp[719]); 
  FFV1_0(w[6], w[160], w[36], pars->GC_11, amp[720]); 
  FFV1_0(w[155], w[33], w[36], pars->GC_11, amp[721]); 
  FFV1_0(w[30], w[160], w[4], pars->GC_11, amp[722]); 
  FFV1_0(w[159], w[33], w[4], pars->GC_11, amp[723]); 
  FFV1_0(w[20], w[160], w[3], pars->GC_11, amp[724]); 
  FFV1_0(w[157], w[33], w[3], pars->GC_11, amp[725]); 
  FFV1_0(w[40], w[154], w[4], pars->GC_11, amp[726]); 
  FFV1_0(w[41], w[154], w[3], pars->GC_11, amp[727]); 
  FFV1_0(w[161], w[5], w[36], pars->GC_11, amp[728]); 
  FFV1_0(w[39], w[154], w[36], pars->GC_11, amp[729]); 
  FFV1_0(w[161], w[27], w[4], pars->GC_11, amp[730]); 
  FFV1_0(w[39], w[158], w[4], pars->GC_11, amp[731]); 
  FFV1_0(w[161], w[17], w[3], pars->GC_11, amp[732]); 
  FFV1_0(w[39], w[156], w[3], pars->GC_11, amp[733]); 
  FFV1_0(w[6], w[154], w[44], pars->GC_11, amp[734]); 
  FFV1_0(w[45], w[154], w[1], pars->GC_11, amp[735]); 
  FFV1_0(w[155], w[5], w[44], pars->GC_11, amp[736]); 
  FFV1_0(w[155], w[46], w[1], pars->GC_11, amp[737]); 
  FFV1_0(w[155], w[47], w[4], pars->GC_11, amp[738]); 
  FFV1_0(w[155], w[48], w[1], pars->GC_11, amp[739]); 
  FFV1_0(w[20], w[158], w[1], pars->GC_11, amp[740]); 
  FFV1_0(w[157], w[27], w[1], pars->GC_11, amp[741]); 
  FFV1_0(w[49], w[154], w[4], pars->GC_11, amp[742]); 
  FFV1_0(w[50], w[154], w[1], pars->GC_11, amp[743]); 
  FFV1_0(w[159], w[17], w[1], pars->GC_11, amp[744]); 
  FFV1_0(w[30], w[156], w[1], pars->GC_11, amp[745]); 
  FFV1_0(w[155], w[51], w[3], pars->GC_11, amp[746]); 
  FFV1_0(w[155], w[52], w[1], pars->GC_11, amp[747]); 
  FFV1_0(w[53], w[154], w[3], pars->GC_11, amp[748]); 
  FFV1_0(w[54], w[154], w[1], pars->GC_11, amp[749]); 
  FFV1_0(w[6], w[154], w[55], pars->GC_11, amp[750]); 
  FFV1_0(w[6], w[154], w[56], pars->GC_11, amp[751]); 
  FFV1_0(w[6], w[154], w[57], pars->GC_11, amp[752]); 
  FFV1_0(w[155], w[5], w[55], pars->GC_11, amp[753]); 
  FFV1_0(w[155], w[5], w[56], pars->GC_11, amp[754]); 
  FFV1_0(w[155], w[5], w[57], pars->GC_11, amp[755]); 


}
double PY8MEs_R4_P4_sm_lg_lggqq::matrix_4_emg_emgguux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 108;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = +amp[0] + amp[2] + Complex<double> (0, 1) * amp[3] + amp[4] +
      amp[6] + Complex<double> (0, 1) * amp[7] + Complex<double> (0, 1) *
      amp[12] + Complex<double> (0, 1) * amp[13] + Complex<double> (0, 1) *
      amp[14] + Complex<double> (0, 1) * amp[15] - amp[32] - amp[34] +
      Complex<double> (0, 1) * amp[36] + Complex<double> (0, 1) * amp[37] +
      Complex<double> (0, 1) * amp[38] + Complex<double> (0, 1) * amp[39] -
      amp[44] - amp[45] - amp[46] - amp[47] + amp[64] + Complex<double> (0, 1)
      * amp[65] + amp[66] + amp[68] + Complex<double> (0, 1) * amp[69] +
      amp[70] - amp[93] - amp[95] - amp[98] + amp[96] - amp[101] + amp[99] -
      amp[104] + amp[102] - amp[107] + amp[105];
  jamp[1] = +amp[16] + amp[18] + Complex<double> (0, 1) * amp[19] + amp[20] +
      amp[22] + Complex<double> (0, 1) * amp[23] + Complex<double> (0, 1) *
      amp[28] + Complex<double> (0, 1) * amp[29] + Complex<double> (0, 1) *
      amp[30] + Complex<double> (0, 1) * amp[31] - amp[33] - amp[35] -
      Complex<double> (0, 1) * amp[36] - Complex<double> (0, 1) * amp[37] -
      Complex<double> (0, 1) * amp[38] - Complex<double> (0, 1) * amp[39] -
      amp[40] - amp[41] - amp[42] - amp[43] - amp[64] - Complex<double> (0, 1)
      * amp[65] - amp[66] - amp[68] - Complex<double> (0, 1) * amp[69] -
      amp[70] - amp[81] - amp[83] - amp[96] - amp[97] - amp[99] - amp[100] -
      amp[102] - amp[103] - amp[105] - amp[106];
  jamp[2] = -amp[0] - amp[2] - Complex<double> (0, 1) * amp[3] - amp[4] -
      amp[6] - Complex<double> (0, 1) * amp[7] - Complex<double> (0, 1) *
      amp[12] - Complex<double> (0, 1) * amp[13] - Complex<double> (0, 1) *
      amp[14] - Complex<double> (0, 1) * amp[15] - amp[16] + Complex<double>
      (0, 1) * amp[17] - amp[18] - amp[20] + Complex<double> (0, 1) * amp[21] -
      amp[22] + Complex<double> (0, 1) * amp[24] + Complex<double> (0, 1) *
      amp[25] + Complex<double> (0, 1) * amp[26] + Complex<double> (0, 1) *
      amp[27] - amp[72] - amp[74] - amp[76] - amp[77] - amp[78] - amp[79] -
      amp[92] - amp[94] + amp[98] + amp[97] + amp[101] + amp[100] + amp[104] +
      amp[103] + amp[107] + amp[106];
  jamp[3] = +amp[16] - Complex<double> (0, 1) * amp[17] + amp[18] + amp[20] -
      Complex<double> (0, 1) * amp[21] + amp[22] - Complex<double> (0, 1) *
      amp[24] - Complex<double> (0, 1) * amp[25] - Complex<double> (0, 1) *
      amp[26] - Complex<double> (0, 1) * amp[27] - amp[49] - amp[51] +
      Complex<double> (0, 1) * amp[52] + Complex<double> (0, 1) * amp[53] +
      Complex<double> (0, 1) * amp[54] + Complex<double> (0, 1) * amp[55] -
      amp[56] - amp[57] - amp[58] - amp[59] - amp[64] - amp[66] +
      Complex<double> (0, 1) * amp[67] - amp[68] - amp[70] + Complex<double>
      (0, 1) * amp[71] - amp[73] - amp[75] - amp[96] - amp[97] - amp[99] -
      amp[100] - amp[102] - amp[103] - amp[105] - amp[106];
  jamp[4] = -amp[0] + Complex<double> (0, 1) * amp[1] - amp[2] - amp[4] +
      Complex<double> (0, 1) * amp[5] - amp[6] + Complex<double> (0, 1) *
      amp[8] + Complex<double> (0, 1) * amp[9] + Complex<double> (0, 1) *
      amp[10] + Complex<double> (0, 1) * amp[11] - amp[16] - amp[18] -
      Complex<double> (0, 1) * amp[19] - amp[20] - amp[22] - Complex<double>
      (0, 1) * amp[23] - Complex<double> (0, 1) * amp[28] - Complex<double> (0,
      1) * amp[29] - Complex<double> (0, 1) * amp[30] - Complex<double> (0, 1)
      * amp[31] - amp[80] - amp[82] - amp[84] - amp[85] - amp[86] - amp[87] -
      amp[88] - amp[90] + amp[98] + amp[97] + amp[101] + amp[100] + amp[104] +
      amp[103] + amp[107] + amp[106];
  jamp[5] = +amp[0] - Complex<double> (0, 1) * amp[1] + amp[2] + amp[4] -
      Complex<double> (0, 1) * amp[5] + amp[6] - Complex<double> (0, 1) *
      amp[8] - Complex<double> (0, 1) * amp[9] - Complex<double> (0, 1) *
      amp[10] - Complex<double> (0, 1) * amp[11] - amp[48] - amp[50] -
      Complex<double> (0, 1) * amp[52] - Complex<double> (0, 1) * amp[53] -
      Complex<double> (0, 1) * amp[54] - Complex<double> (0, 1) * amp[55] -
      amp[60] - amp[61] - amp[62] - amp[63] + amp[64] + amp[66] -
      Complex<double> (0, 1) * amp[67] + amp[68] + amp[70] - Complex<double>
      (0, 1) * amp[71] - amp[89] - amp[91] - amp[98] + amp[96] - amp[101] +
      amp[99] - amp[104] + amp[102] - amp[107] + amp[105];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[0][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P4_sm_lg_lggqq::matrix_4_emg_emggddx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 108;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = +amp[108] + amp[110] + Complex<double> (0, 1) * amp[111] + amp[112]
      + amp[114] + Complex<double> (0, 1) * amp[115] + Complex<double> (0, 1) *
      amp[120] + Complex<double> (0, 1) * amp[121] + Complex<double> (0, 1) *
      amp[122] + Complex<double> (0, 1) * amp[123] - amp[140] - amp[142] +
      Complex<double> (0, 1) * amp[144] + Complex<double> (0, 1) * amp[145] +
      Complex<double> (0, 1) * amp[146] + Complex<double> (0, 1) * amp[147] -
      amp[152] - amp[153] - amp[154] - amp[155] + amp[172] + Complex<double>
      (0, 1) * amp[173] + amp[174] + amp[176] + Complex<double> (0, 1) *
      amp[177] + amp[178] - amp[201] - amp[203] - amp[206] + amp[204] -
      amp[209] + amp[207] - amp[212] + amp[210] - amp[215] + amp[213];
  jamp[1] = +amp[124] + amp[126] + Complex<double> (0, 1) * amp[127] + amp[128]
      + amp[130] + Complex<double> (0, 1) * amp[131] + Complex<double> (0, 1) *
      amp[136] + Complex<double> (0, 1) * amp[137] + Complex<double> (0, 1) *
      amp[138] + Complex<double> (0, 1) * amp[139] - amp[141] - amp[143] -
      Complex<double> (0, 1) * amp[144] - Complex<double> (0, 1) * amp[145] -
      Complex<double> (0, 1) * amp[146] - Complex<double> (0, 1) * amp[147] -
      amp[148] - amp[149] - amp[150] - amp[151] - amp[172] - Complex<double>
      (0, 1) * amp[173] - amp[174] - amp[176] - Complex<double> (0, 1) *
      amp[177] - amp[178] - amp[189] - amp[191] - amp[204] - amp[205] -
      amp[207] - amp[208] - amp[210] - amp[211] - amp[213] - amp[214];
  jamp[2] = -amp[108] - amp[110] - Complex<double> (0, 1) * amp[111] - amp[112]
      - amp[114] - Complex<double> (0, 1) * amp[115] - Complex<double> (0, 1) *
      amp[120] - Complex<double> (0, 1) * amp[121] - Complex<double> (0, 1) *
      amp[122] - Complex<double> (0, 1) * amp[123] - amp[124] + Complex<double>
      (0, 1) * amp[125] - amp[126] - amp[128] + Complex<double> (0, 1) *
      amp[129] - amp[130] + Complex<double> (0, 1) * amp[132] + Complex<double>
      (0, 1) * amp[133] + Complex<double> (0, 1) * amp[134] + Complex<double>
      (0, 1) * amp[135] - amp[180] - amp[182] - amp[184] - amp[185] - amp[186]
      - amp[187] - amp[200] - amp[202] + amp[206] + amp[205] + amp[209] +
      amp[208] + amp[212] + amp[211] + amp[215] + amp[214];
  jamp[3] = +amp[124] - Complex<double> (0, 1) * amp[125] + amp[126] + amp[128]
      - Complex<double> (0, 1) * amp[129] + amp[130] - Complex<double> (0, 1) *
      amp[132] - Complex<double> (0, 1) * amp[133] - Complex<double> (0, 1) *
      amp[134] - Complex<double> (0, 1) * amp[135] - amp[157] - amp[159] +
      Complex<double> (0, 1) * amp[160] + Complex<double> (0, 1) * amp[161] +
      Complex<double> (0, 1) * amp[162] + Complex<double> (0, 1) * amp[163] -
      amp[164] - amp[165] - amp[166] - amp[167] - amp[172] - amp[174] +
      Complex<double> (0, 1) * amp[175] - amp[176] - amp[178] + Complex<double>
      (0, 1) * amp[179] - amp[181] - amp[183] - amp[204] - amp[205] - amp[207]
      - amp[208] - amp[210] - amp[211] - amp[213] - amp[214];
  jamp[4] = -amp[108] + Complex<double> (0, 1) * amp[109] - amp[110] - amp[112]
      + Complex<double> (0, 1) * amp[113] - amp[114] + Complex<double> (0, 1) *
      amp[116] + Complex<double> (0, 1) * amp[117] + Complex<double> (0, 1) *
      amp[118] + Complex<double> (0, 1) * amp[119] - amp[124] - amp[126] -
      Complex<double> (0, 1) * amp[127] - amp[128] - amp[130] - Complex<double>
      (0, 1) * amp[131] - Complex<double> (0, 1) * amp[136] - Complex<double>
      (0, 1) * amp[137] - Complex<double> (0, 1) * amp[138] - Complex<double>
      (0, 1) * amp[139] - amp[188] - amp[190] - amp[192] - amp[193] - amp[194]
      - amp[195] - amp[196] - amp[198] + amp[206] + amp[205] + amp[209] +
      amp[208] + amp[212] + amp[211] + amp[215] + amp[214];
  jamp[5] = +amp[108] - Complex<double> (0, 1) * amp[109] + amp[110] + amp[112]
      - Complex<double> (0, 1) * amp[113] + amp[114] - Complex<double> (0, 1) *
      amp[116] - Complex<double> (0, 1) * amp[117] - Complex<double> (0, 1) *
      amp[118] - Complex<double> (0, 1) * amp[119] - amp[156] - amp[158] -
      Complex<double> (0, 1) * amp[160] - Complex<double> (0, 1) * amp[161] -
      Complex<double> (0, 1) * amp[162] - Complex<double> (0, 1) * amp[163] -
      amp[168] - amp[169] - amp[170] - amp[171] + amp[172] + amp[174] -
      Complex<double> (0, 1) * amp[175] + amp[176] + amp[178] - Complex<double>
      (0, 1) * amp[179] - amp[197] - amp[199] - amp[206] + amp[204] - amp[209]
      + amp[207] - amp[212] + amp[210] - amp[215] + amp[213];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[1][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P4_sm_lg_lggqq::matrix_4_epg_epgguux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 108;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = -amp[216] - amp[218] - Complex<double> (0, 1) * amp[219] - amp[220]
      - amp[222] - Complex<double> (0, 1) * amp[223] - Complex<double> (0, 1) *
      amp[228] - Complex<double> (0, 1) * amp[229] - Complex<double> (0, 1) *
      amp[230] - Complex<double> (0, 1) * amp[231] + amp[248] + amp[250] -
      Complex<double> (0, 1) * amp[252] - Complex<double> (0, 1) * amp[253] -
      Complex<double> (0, 1) * amp[254] - Complex<double> (0, 1) * amp[255] +
      amp[260] + amp[261] + amp[262] + amp[263] - amp[280] - Complex<double>
      (0, 1) * amp[281] - amp[282] - amp[284] - Complex<double> (0, 1) *
      amp[285] - amp[286] + amp[309] + amp[311] + amp[314] - amp[312] +
      amp[317] - amp[315] + amp[320] - amp[318] + amp[323] - amp[321];
  jamp[1] = -amp[232] - amp[234] - Complex<double> (0, 1) * amp[235] - amp[236]
      - amp[238] - Complex<double> (0, 1) * amp[239] - Complex<double> (0, 1) *
      amp[244] - Complex<double> (0, 1) * amp[245] - Complex<double> (0, 1) *
      amp[246] - Complex<double> (0, 1) * amp[247] + amp[249] + amp[251] +
      Complex<double> (0, 1) * amp[252] + Complex<double> (0, 1) * amp[253] +
      Complex<double> (0, 1) * amp[254] + Complex<double> (0, 1) * amp[255] +
      amp[256] + amp[257] + amp[258] + amp[259] + amp[280] + Complex<double>
      (0, 1) * amp[281] + amp[282] + amp[284] + Complex<double> (0, 1) *
      amp[285] + amp[286] + amp[297] + amp[299] + amp[312] + amp[313] +
      amp[315] + amp[316] + amp[318] + amp[319] + amp[321] + amp[322];
  jamp[2] = +amp[216] + amp[218] + Complex<double> (0, 1) * amp[219] + amp[220]
      + amp[222] + Complex<double> (0, 1) * amp[223] + Complex<double> (0, 1) *
      amp[228] + Complex<double> (0, 1) * amp[229] + Complex<double> (0, 1) *
      amp[230] + Complex<double> (0, 1) * amp[231] + amp[232] - Complex<double>
      (0, 1) * amp[233] + amp[234] + amp[236] - Complex<double> (0, 1) *
      amp[237] + amp[238] - Complex<double> (0, 1) * amp[240] - Complex<double>
      (0, 1) * amp[241] - Complex<double> (0, 1) * amp[242] - Complex<double>
      (0, 1) * amp[243] + amp[288] + amp[290] + amp[292] + amp[293] + amp[294]
      + amp[295] + amp[308] + amp[310] - amp[314] - amp[313] - amp[317] -
      amp[316] - amp[320] - amp[319] - amp[323] - amp[322];
  jamp[3] = -amp[232] + Complex<double> (0, 1) * amp[233] - amp[234] - amp[236]
      + Complex<double> (0, 1) * amp[237] - amp[238] + Complex<double> (0, 1) *
      amp[240] + Complex<double> (0, 1) * amp[241] + Complex<double> (0, 1) *
      amp[242] + Complex<double> (0, 1) * amp[243] + amp[265] + amp[267] -
      Complex<double> (0, 1) * amp[268] - Complex<double> (0, 1) * amp[269] -
      Complex<double> (0, 1) * amp[270] - Complex<double> (0, 1) * amp[271] +
      amp[272] + amp[273] + amp[274] + amp[275] + amp[280] + amp[282] -
      Complex<double> (0, 1) * amp[283] + amp[284] + amp[286] - Complex<double>
      (0, 1) * amp[287] + amp[289] + amp[291] + amp[312] + amp[313] + amp[315]
      + amp[316] + amp[318] + amp[319] + amp[321] + amp[322];
  jamp[4] = +amp[216] - Complex<double> (0, 1) * amp[217] + amp[218] + amp[220]
      - Complex<double> (0, 1) * amp[221] + amp[222] - Complex<double> (0, 1) *
      amp[224] - Complex<double> (0, 1) * amp[225] - Complex<double> (0, 1) *
      amp[226] - Complex<double> (0, 1) * amp[227] + amp[232] + amp[234] +
      Complex<double> (0, 1) * amp[235] + amp[236] + amp[238] + Complex<double>
      (0, 1) * amp[239] + Complex<double> (0, 1) * amp[244] + Complex<double>
      (0, 1) * amp[245] + Complex<double> (0, 1) * amp[246] + Complex<double>
      (0, 1) * amp[247] + amp[296] + amp[298] + amp[300] + amp[301] + amp[302]
      + amp[303] + amp[304] + amp[306] - amp[314] - amp[313] - amp[317] -
      amp[316] - amp[320] - amp[319] - amp[323] - amp[322];
  jamp[5] = -amp[216] + Complex<double> (0, 1) * amp[217] - amp[218] - amp[220]
      + Complex<double> (0, 1) * amp[221] - amp[222] + Complex<double> (0, 1) *
      amp[224] + Complex<double> (0, 1) * amp[225] + Complex<double> (0, 1) *
      amp[226] + Complex<double> (0, 1) * amp[227] + amp[264] + amp[266] +
      Complex<double> (0, 1) * amp[268] + Complex<double> (0, 1) * amp[269] +
      Complex<double> (0, 1) * amp[270] + Complex<double> (0, 1) * amp[271] +
      amp[276] + amp[277] + amp[278] + amp[279] - amp[280] - amp[282] +
      Complex<double> (0, 1) * amp[283] - amp[284] - amp[286] + Complex<double>
      (0, 1) * amp[287] + amp[305] + amp[307] + amp[314] - amp[312] + amp[317]
      - amp[315] + amp[320] - amp[318] + amp[323] - amp[321];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[2][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P4_sm_lg_lggqq::matrix_4_epg_epggddx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 108;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = -amp[324] - amp[326] - Complex<double> (0, 1) * amp[327] - amp[328]
      - amp[330] - Complex<double> (0, 1) * amp[331] - Complex<double> (0, 1) *
      amp[336] - Complex<double> (0, 1) * amp[337] - Complex<double> (0, 1) *
      amp[338] - Complex<double> (0, 1) * amp[339] + amp[356] + amp[358] -
      Complex<double> (0, 1) * amp[360] - Complex<double> (0, 1) * amp[361] -
      Complex<double> (0, 1) * amp[362] - Complex<double> (0, 1) * amp[363] +
      amp[368] + amp[369] + amp[370] + amp[371] - amp[388] - Complex<double>
      (0, 1) * amp[389] - amp[390] - amp[392] - Complex<double> (0, 1) *
      amp[393] - amp[394] + amp[417] + amp[419] + amp[422] - amp[420] +
      amp[425] - amp[423] + amp[428] - amp[426] + amp[431] - amp[429];
  jamp[1] = -amp[340] - amp[342] - Complex<double> (0, 1) * amp[343] - amp[344]
      - amp[346] - Complex<double> (0, 1) * amp[347] - Complex<double> (0, 1) *
      amp[352] - Complex<double> (0, 1) * amp[353] - Complex<double> (0, 1) *
      amp[354] - Complex<double> (0, 1) * amp[355] + amp[357] + amp[359] +
      Complex<double> (0, 1) * amp[360] + Complex<double> (0, 1) * amp[361] +
      Complex<double> (0, 1) * amp[362] + Complex<double> (0, 1) * amp[363] +
      amp[364] + amp[365] + amp[366] + amp[367] + amp[388] + Complex<double>
      (0, 1) * amp[389] + amp[390] + amp[392] + Complex<double> (0, 1) *
      amp[393] + amp[394] + amp[405] + amp[407] + amp[420] + amp[421] +
      amp[423] + amp[424] + amp[426] + amp[427] + amp[429] + amp[430];
  jamp[2] = +amp[324] + amp[326] + Complex<double> (0, 1) * amp[327] + amp[328]
      + amp[330] + Complex<double> (0, 1) * amp[331] + Complex<double> (0, 1) *
      amp[336] + Complex<double> (0, 1) * amp[337] + Complex<double> (0, 1) *
      amp[338] + Complex<double> (0, 1) * amp[339] + amp[340] - Complex<double>
      (0, 1) * amp[341] + amp[342] + amp[344] - Complex<double> (0, 1) *
      amp[345] + amp[346] - Complex<double> (0, 1) * amp[348] - Complex<double>
      (0, 1) * amp[349] - Complex<double> (0, 1) * amp[350] - Complex<double>
      (0, 1) * amp[351] + amp[396] + amp[398] + amp[400] + amp[401] + amp[402]
      + amp[403] + amp[416] + amp[418] - amp[422] - amp[421] - amp[425] -
      amp[424] - amp[428] - amp[427] - amp[431] - amp[430];
  jamp[3] = -amp[340] + Complex<double> (0, 1) * amp[341] - amp[342] - amp[344]
      + Complex<double> (0, 1) * amp[345] - amp[346] + Complex<double> (0, 1) *
      amp[348] + Complex<double> (0, 1) * amp[349] + Complex<double> (0, 1) *
      amp[350] + Complex<double> (0, 1) * amp[351] + amp[373] + amp[375] -
      Complex<double> (0, 1) * amp[376] - Complex<double> (0, 1) * amp[377] -
      Complex<double> (0, 1) * amp[378] - Complex<double> (0, 1) * amp[379] +
      amp[380] + amp[381] + amp[382] + amp[383] + amp[388] + amp[390] -
      Complex<double> (0, 1) * amp[391] + amp[392] + amp[394] - Complex<double>
      (0, 1) * amp[395] + amp[397] + amp[399] + amp[420] + amp[421] + amp[423]
      + amp[424] + amp[426] + amp[427] + amp[429] + amp[430];
  jamp[4] = +amp[324] - Complex<double> (0, 1) * amp[325] + amp[326] + amp[328]
      - Complex<double> (0, 1) * amp[329] + amp[330] - Complex<double> (0, 1) *
      amp[332] - Complex<double> (0, 1) * amp[333] - Complex<double> (0, 1) *
      amp[334] - Complex<double> (0, 1) * amp[335] + amp[340] + amp[342] +
      Complex<double> (0, 1) * amp[343] + amp[344] + amp[346] + Complex<double>
      (0, 1) * amp[347] + Complex<double> (0, 1) * amp[352] + Complex<double>
      (0, 1) * amp[353] + Complex<double> (0, 1) * amp[354] + Complex<double>
      (0, 1) * amp[355] + amp[404] + amp[406] + amp[408] + amp[409] + amp[410]
      + amp[411] + amp[412] + amp[414] - amp[422] - amp[421] - amp[425] -
      amp[424] - amp[428] - amp[427] - amp[431] - amp[430];
  jamp[5] = -amp[324] + Complex<double> (0, 1) * amp[325] - amp[326] - amp[328]
      + Complex<double> (0, 1) * amp[329] - amp[330] + Complex<double> (0, 1) *
      amp[332] + Complex<double> (0, 1) * amp[333] + Complex<double> (0, 1) *
      amp[334] + Complex<double> (0, 1) * amp[335] + amp[372] + amp[374] +
      Complex<double> (0, 1) * amp[376] + Complex<double> (0, 1) * amp[377] +
      Complex<double> (0, 1) * amp[378] + Complex<double> (0, 1) * amp[379] +
      amp[384] + amp[385] + amp[386] + amp[387] - amp[388] - amp[390] +
      Complex<double> (0, 1) * amp[391] - amp[392] - amp[394] + Complex<double>
      (0, 1) * amp[395] + amp[413] + amp[415] + amp[422] - amp[420] + amp[425]
      - amp[423] + amp[428] - amp[426] + amp[431] - amp[429];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[3][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P4_sm_lg_lggqq::matrix_4_emg_veggdux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 54;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = +amp[432] + amp[434] + Complex<double> (0, 1) * amp[435] +
      Complex<double> (0, 1) * amp[438] + Complex<double> (0, 1) * amp[439] -
      amp[448] + Complex<double> (0, 1) * amp[450] + Complex<double> (0, 1) *
      amp[451] - amp[454] - amp[455] + amp[464] + Complex<double> (0, 1) *
      amp[465] + amp[466] - amp[479] - amp[482] + amp[480] - amp[485] +
      amp[483];
  jamp[1] = +amp[440] + amp[442] + Complex<double> (0, 1) * amp[443] +
      Complex<double> (0, 1) * amp[446] + Complex<double> (0, 1) * amp[447] -
      amp[449] - Complex<double> (0, 1) * amp[450] - Complex<double> (0, 1) *
      amp[451] - amp[452] - amp[453] - amp[464] - Complex<double> (0, 1) *
      amp[465] - amp[466] - amp[473] - amp[480] - amp[481] - amp[483] -
      amp[484];
  jamp[2] = -amp[432] - amp[434] - Complex<double> (0, 1) * amp[435] -
      Complex<double> (0, 1) * amp[438] - Complex<double> (0, 1) * amp[439] -
      amp[440] + Complex<double> (0, 1) * amp[441] - amp[442] + Complex<double>
      (0, 1) * amp[444] + Complex<double> (0, 1) * amp[445] - amp[468] -
      amp[470] - amp[471] - amp[478] + amp[482] + amp[481] + amp[485] +
      amp[484];
  jamp[3] = +amp[440] - Complex<double> (0, 1) * amp[441] + amp[442] -
      Complex<double> (0, 1) * amp[444] - Complex<double> (0, 1) * amp[445] -
      amp[457] + Complex<double> (0, 1) * amp[458] + Complex<double> (0, 1) *
      amp[459] - amp[460] - amp[461] - amp[464] - amp[466] + Complex<double>
      (0, 1) * amp[467] - amp[469] - amp[480] - amp[481] - amp[483] - amp[484];
  jamp[4] = -amp[432] + Complex<double> (0, 1) * amp[433] - amp[434] +
      Complex<double> (0, 1) * amp[436] + Complex<double> (0, 1) * amp[437] -
      amp[440] - amp[442] - Complex<double> (0, 1) * amp[443] - Complex<double>
      (0, 1) * amp[446] - Complex<double> (0, 1) * amp[447] - amp[472] -
      amp[474] - amp[475] - amp[476] + amp[482] + amp[481] + amp[485] +
      amp[484];
  jamp[5] = +amp[432] - Complex<double> (0, 1) * amp[433] + amp[434] -
      Complex<double> (0, 1) * amp[436] - Complex<double> (0, 1) * amp[437] -
      amp[456] - Complex<double> (0, 1) * amp[458] - Complex<double> (0, 1) *
      amp[459] - amp[462] - amp[463] + amp[464] + amp[466] - Complex<double>
      (0, 1) * amp[467] - amp[477] - amp[482] + amp[480] - amp[485] + amp[483];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[4][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P4_sm_lg_lggqq::matrix_4_veg_vegguux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 54;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = +amp[486] + amp[488] + Complex<double> (0, 1) * amp[489] +
      Complex<double> (0, 1) * amp[492] + Complex<double> (0, 1) * amp[493] -
      amp[502] + Complex<double> (0, 1) * amp[504] + Complex<double> (0, 1) *
      amp[505] - amp[508] - amp[509] + amp[518] + Complex<double> (0, 1) *
      amp[519] + amp[520] - amp[533] - amp[536] + amp[534] - amp[539] +
      amp[537];
  jamp[1] = +amp[494] + amp[496] + Complex<double> (0, 1) * amp[497] +
      Complex<double> (0, 1) * amp[500] + Complex<double> (0, 1) * amp[501] -
      amp[503] - Complex<double> (0, 1) * amp[504] - Complex<double> (0, 1) *
      amp[505] - amp[506] - amp[507] - amp[518] - Complex<double> (0, 1) *
      amp[519] - amp[520] - amp[527] - amp[534] - amp[535] - amp[537] -
      amp[538];
  jamp[2] = -amp[486] - amp[488] - Complex<double> (0, 1) * amp[489] -
      Complex<double> (0, 1) * amp[492] - Complex<double> (0, 1) * amp[493] -
      amp[494] + Complex<double> (0, 1) * amp[495] - amp[496] + Complex<double>
      (0, 1) * amp[498] + Complex<double> (0, 1) * amp[499] - amp[522] -
      amp[524] - amp[525] - amp[532] + amp[536] + amp[535] + amp[539] +
      amp[538];
  jamp[3] = +amp[494] - Complex<double> (0, 1) * amp[495] + amp[496] -
      Complex<double> (0, 1) * amp[498] - Complex<double> (0, 1) * amp[499] -
      amp[511] + Complex<double> (0, 1) * amp[512] + Complex<double> (0, 1) *
      amp[513] - amp[514] - amp[515] - amp[518] - amp[520] + Complex<double>
      (0, 1) * amp[521] - amp[523] - amp[534] - amp[535] - amp[537] - amp[538];
  jamp[4] = -amp[486] + Complex<double> (0, 1) * amp[487] - amp[488] +
      Complex<double> (0, 1) * amp[490] + Complex<double> (0, 1) * amp[491] -
      amp[494] - amp[496] - Complex<double> (0, 1) * amp[497] - Complex<double>
      (0, 1) * amp[500] - Complex<double> (0, 1) * amp[501] - amp[526] -
      amp[528] - amp[529] - amp[530] + amp[536] + amp[535] + amp[539] +
      amp[538];
  jamp[5] = +amp[486] - Complex<double> (0, 1) * amp[487] + amp[488] -
      Complex<double> (0, 1) * amp[490] - Complex<double> (0, 1) * amp[491] -
      amp[510] - Complex<double> (0, 1) * amp[512] - Complex<double> (0, 1) *
      amp[513] - amp[516] - amp[517] + amp[518] + amp[520] - Complex<double>
      (0, 1) * amp[521] - amp[531] - amp[536] + amp[534] - amp[539] + amp[537];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[5][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P4_sm_lg_lggqq::matrix_4_veg_veggddx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 54;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = +amp[540] + amp[542] + Complex<double> (0, 1) * amp[543] +
      Complex<double> (0, 1) * amp[546] + Complex<double> (0, 1) * amp[547] -
      amp[556] + Complex<double> (0, 1) * amp[558] + Complex<double> (0, 1) *
      amp[559] - amp[562] - amp[563] + amp[572] + Complex<double> (0, 1) *
      amp[573] + amp[574] - amp[587] - amp[590] + amp[588] - amp[593] +
      amp[591];
  jamp[1] = +amp[548] + amp[550] + Complex<double> (0, 1) * amp[551] +
      Complex<double> (0, 1) * amp[554] + Complex<double> (0, 1) * amp[555] -
      amp[557] - Complex<double> (0, 1) * amp[558] - Complex<double> (0, 1) *
      amp[559] - amp[560] - amp[561] - amp[572] - Complex<double> (0, 1) *
      amp[573] - amp[574] - amp[581] - amp[588] - amp[589] - amp[591] -
      amp[592];
  jamp[2] = -amp[540] - amp[542] - Complex<double> (0, 1) * amp[543] -
      Complex<double> (0, 1) * amp[546] - Complex<double> (0, 1) * amp[547] -
      amp[548] + Complex<double> (0, 1) * amp[549] - amp[550] + Complex<double>
      (0, 1) * amp[552] + Complex<double> (0, 1) * amp[553] - amp[576] -
      amp[578] - amp[579] - amp[586] + amp[590] + amp[589] + amp[593] +
      amp[592];
  jamp[3] = +amp[548] - Complex<double> (0, 1) * amp[549] + amp[550] -
      Complex<double> (0, 1) * amp[552] - Complex<double> (0, 1) * amp[553] -
      amp[565] + Complex<double> (0, 1) * amp[566] + Complex<double> (0, 1) *
      amp[567] - amp[568] - amp[569] - amp[572] - amp[574] + Complex<double>
      (0, 1) * amp[575] - amp[577] - amp[588] - amp[589] - amp[591] - amp[592];
  jamp[4] = -amp[540] + Complex<double> (0, 1) * amp[541] - amp[542] +
      Complex<double> (0, 1) * amp[544] + Complex<double> (0, 1) * amp[545] -
      amp[548] - amp[550] - Complex<double> (0, 1) * amp[551] - Complex<double>
      (0, 1) * amp[554] - Complex<double> (0, 1) * amp[555] - amp[580] -
      amp[582] - amp[583] - amp[584] + amp[590] + amp[589] + amp[593] +
      amp[592];
  jamp[5] = +amp[540] - Complex<double> (0, 1) * amp[541] + amp[542] -
      Complex<double> (0, 1) * amp[544] - Complex<double> (0, 1) * amp[545] -
      amp[564] - Complex<double> (0, 1) * amp[566] - Complex<double> (0, 1) *
      amp[567] - amp[570] - amp[571] + amp[572] + amp[574] - Complex<double>
      (0, 1) * amp[575] - amp[585] - amp[590] + amp[588] - amp[593] + amp[591];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[6][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P4_sm_lg_lggqq::matrix_4_epg_vexggudx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 54;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = -amp[594] - amp[596] - Complex<double> (0, 1) * amp[597] -
      Complex<double> (0, 1) * amp[600] - Complex<double> (0, 1) * amp[601] +
      amp[610] - Complex<double> (0, 1) * amp[612] - Complex<double> (0, 1) *
      amp[613] + amp[616] + amp[617] - amp[626] - Complex<double> (0, 1) *
      amp[627] - amp[628] + amp[641] + amp[644] - amp[642] + amp[647] -
      amp[645];
  jamp[1] = -amp[602] - amp[604] - Complex<double> (0, 1) * amp[605] -
      Complex<double> (0, 1) * amp[608] - Complex<double> (0, 1) * amp[609] +
      amp[611] + Complex<double> (0, 1) * amp[612] + Complex<double> (0, 1) *
      amp[613] + amp[614] + amp[615] + amp[626] + Complex<double> (0, 1) *
      amp[627] + amp[628] + amp[635] + amp[642] + amp[643] + amp[645] +
      amp[646];
  jamp[2] = +amp[594] + amp[596] + Complex<double> (0, 1) * amp[597] +
      Complex<double> (0, 1) * amp[600] + Complex<double> (0, 1) * amp[601] +
      amp[602] - Complex<double> (0, 1) * amp[603] + amp[604] - Complex<double>
      (0, 1) * amp[606] - Complex<double> (0, 1) * amp[607] + amp[630] +
      amp[632] + amp[633] + amp[640] - amp[644] - amp[643] - amp[647] -
      amp[646];
  jamp[3] = -amp[602] + Complex<double> (0, 1) * amp[603] - amp[604] +
      Complex<double> (0, 1) * amp[606] + Complex<double> (0, 1) * amp[607] +
      amp[619] - Complex<double> (0, 1) * amp[620] - Complex<double> (0, 1) *
      amp[621] + amp[622] + amp[623] + amp[626] + amp[628] - Complex<double>
      (0, 1) * amp[629] + amp[631] + amp[642] + amp[643] + amp[645] + amp[646];
  jamp[4] = +amp[594] - Complex<double> (0, 1) * amp[595] + amp[596] -
      Complex<double> (0, 1) * amp[598] - Complex<double> (0, 1) * amp[599] +
      amp[602] + amp[604] + Complex<double> (0, 1) * amp[605] + Complex<double>
      (0, 1) * amp[608] + Complex<double> (0, 1) * amp[609] + amp[634] +
      amp[636] + amp[637] + amp[638] - amp[644] - amp[643] - amp[647] -
      amp[646];
  jamp[5] = -amp[594] + Complex<double> (0, 1) * amp[595] - amp[596] +
      Complex<double> (0, 1) * amp[598] + Complex<double> (0, 1) * amp[599] +
      amp[618] + Complex<double> (0, 1) * amp[620] + Complex<double> (0, 1) *
      amp[621] + amp[624] + amp[625] - amp[626] - amp[628] + Complex<double>
      (0, 1) * amp[629] + amp[639] + amp[644] - amp[642] + amp[647] - amp[645];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[7][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P4_sm_lg_lggqq::matrix_4_vexg_vexgguux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 54;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = -amp[648] - amp[650] - Complex<double> (0, 1) * amp[651] -
      Complex<double> (0, 1) * amp[654] - Complex<double> (0, 1) * amp[655] +
      amp[664] - Complex<double> (0, 1) * amp[666] - Complex<double> (0, 1) *
      amp[667] + amp[670] + amp[671] - amp[680] - Complex<double> (0, 1) *
      amp[681] - amp[682] + amp[695] + amp[698] - amp[696] + amp[701] -
      amp[699];
  jamp[1] = -amp[656] - amp[658] - Complex<double> (0, 1) * amp[659] -
      Complex<double> (0, 1) * amp[662] - Complex<double> (0, 1) * amp[663] +
      amp[665] + Complex<double> (0, 1) * amp[666] + Complex<double> (0, 1) *
      amp[667] + amp[668] + amp[669] + amp[680] + Complex<double> (0, 1) *
      amp[681] + amp[682] + amp[689] + amp[696] + amp[697] + amp[699] +
      amp[700];
  jamp[2] = +amp[648] + amp[650] + Complex<double> (0, 1) * amp[651] +
      Complex<double> (0, 1) * amp[654] + Complex<double> (0, 1) * amp[655] +
      amp[656] - Complex<double> (0, 1) * amp[657] + amp[658] - Complex<double>
      (0, 1) * amp[660] - Complex<double> (0, 1) * amp[661] + amp[684] +
      amp[686] + amp[687] + amp[694] - amp[698] - amp[697] - amp[701] -
      amp[700];
  jamp[3] = -amp[656] + Complex<double> (0, 1) * amp[657] - amp[658] +
      Complex<double> (0, 1) * amp[660] + Complex<double> (0, 1) * amp[661] +
      amp[673] - Complex<double> (0, 1) * amp[674] - Complex<double> (0, 1) *
      amp[675] + amp[676] + amp[677] + amp[680] + amp[682] - Complex<double>
      (0, 1) * amp[683] + amp[685] + amp[696] + amp[697] + amp[699] + amp[700];
  jamp[4] = +amp[648] - Complex<double> (0, 1) * amp[649] + amp[650] -
      Complex<double> (0, 1) * amp[652] - Complex<double> (0, 1) * amp[653] +
      amp[656] + amp[658] + Complex<double> (0, 1) * amp[659] + Complex<double>
      (0, 1) * amp[662] + Complex<double> (0, 1) * amp[663] + amp[688] +
      amp[690] + amp[691] + amp[692] - amp[698] - amp[697] - amp[701] -
      amp[700];
  jamp[5] = -amp[648] + Complex<double> (0, 1) * amp[649] - amp[650] +
      Complex<double> (0, 1) * amp[652] + Complex<double> (0, 1) * amp[653] +
      amp[672] + Complex<double> (0, 1) * amp[674] + Complex<double> (0, 1) *
      amp[675] + amp[678] + amp[679] - amp[680] - amp[682] + Complex<double>
      (0, 1) * amp[683] + amp[693] + amp[698] - amp[696] + amp[701] - amp[699];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[8][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P4_sm_lg_lggqq::matrix_4_vexg_vexggddx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 54;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = -amp[702] - amp[704] - Complex<double> (0, 1) * amp[705] -
      Complex<double> (0, 1) * amp[708] - Complex<double> (0, 1) * amp[709] +
      amp[718] - Complex<double> (0, 1) * amp[720] - Complex<double> (0, 1) *
      amp[721] + amp[724] + amp[725] - amp[734] - Complex<double> (0, 1) *
      amp[735] - amp[736] + amp[749] + amp[752] - amp[750] + amp[755] -
      amp[753];
  jamp[1] = -amp[710] - amp[712] - Complex<double> (0, 1) * amp[713] -
      Complex<double> (0, 1) * amp[716] - Complex<double> (0, 1) * amp[717] +
      amp[719] + Complex<double> (0, 1) * amp[720] + Complex<double> (0, 1) *
      amp[721] + amp[722] + amp[723] + amp[734] + Complex<double> (0, 1) *
      amp[735] + amp[736] + amp[743] + amp[750] + amp[751] + amp[753] +
      amp[754];
  jamp[2] = +amp[702] + amp[704] + Complex<double> (0, 1) * amp[705] +
      Complex<double> (0, 1) * amp[708] + Complex<double> (0, 1) * amp[709] +
      amp[710] - Complex<double> (0, 1) * amp[711] + amp[712] - Complex<double>
      (0, 1) * amp[714] - Complex<double> (0, 1) * amp[715] + amp[738] +
      amp[740] + amp[741] + amp[748] - amp[752] - amp[751] - amp[755] -
      amp[754];
  jamp[3] = -amp[710] + Complex<double> (0, 1) * amp[711] - amp[712] +
      Complex<double> (0, 1) * amp[714] + Complex<double> (0, 1) * amp[715] +
      amp[727] - Complex<double> (0, 1) * amp[728] - Complex<double> (0, 1) *
      amp[729] + amp[730] + amp[731] + amp[734] + amp[736] - Complex<double>
      (0, 1) * amp[737] + amp[739] + amp[750] + amp[751] + amp[753] + amp[754];
  jamp[4] = +amp[702] - Complex<double> (0, 1) * amp[703] + amp[704] -
      Complex<double> (0, 1) * amp[706] - Complex<double> (0, 1) * amp[707] +
      amp[710] + amp[712] + Complex<double> (0, 1) * amp[713] + Complex<double>
      (0, 1) * amp[716] + Complex<double> (0, 1) * amp[717] + amp[742] +
      amp[744] + amp[745] + amp[746] - amp[752] - amp[751] - amp[755] -
      amp[754];
  jamp[5] = -amp[702] + Complex<double> (0, 1) * amp[703] - amp[704] +
      Complex<double> (0, 1) * amp[706] + Complex<double> (0, 1) * amp[707] +
      amp[726] + Complex<double> (0, 1) * amp[728] + Complex<double> (0, 1) *
      amp[729] + amp[732] + amp[733] - amp[734] - amp[736] + Complex<double>
      (0, 1) * amp[737] + amp[747] + amp[752] - amp[750] + amp[755] - amp[753];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[9][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}


}  // end namespace PY8MEs_namespace

