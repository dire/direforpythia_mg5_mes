//==========================================================================
// This file has been automatically generated for Pythia 8 by
// MadGraph5_aMC@NLO v. 2.6.0, 2017-08-16
// By the MadGraph5_aMC@NLO Development Team
// Visit launchpad.net/madgraph5 and amcatnlo.web.cern.ch
//==========================================================================

#include "PY8MEs_R3_P35_sm_lq_lggq.h"
#include "HelAmps_sm.h"

using namespace Pythia8_sm; 

namespace PY8MEs_namespace 
{
//==========================================================================
// Class member functions for calculating the matrix elements for
// Process: e- u > e- g g u WEIGHTED<=6 @3
// Process: e- c > e- g g c WEIGHTED<=6 @3
// Process: mu- u > mu- g g u WEIGHTED<=6 @3
// Process: mu- c > mu- g g c WEIGHTED<=6 @3
// Process: e- d > e- g g d WEIGHTED<=6 @3
// Process: e- s > e- g g s WEIGHTED<=6 @3
// Process: mu- d > mu- g g d WEIGHTED<=6 @3
// Process: mu- s > mu- g g s WEIGHTED<=6 @3
// Process: e- u~ > e- g g u~ WEIGHTED<=6 @3
// Process: e- c~ > e- g g c~ WEIGHTED<=6 @3
// Process: mu- u~ > mu- g g u~ WEIGHTED<=6 @3
// Process: mu- c~ > mu- g g c~ WEIGHTED<=6 @3
// Process: e- d~ > e- g g d~ WEIGHTED<=6 @3
// Process: e- s~ > e- g g s~ WEIGHTED<=6 @3
// Process: mu- d~ > mu- g g d~ WEIGHTED<=6 @3
// Process: mu- s~ > mu- g g s~ WEIGHTED<=6 @3
// Process: e+ u > e+ g g u WEIGHTED<=6 @3
// Process: e+ c > e+ g g c WEIGHTED<=6 @3
// Process: mu+ u > mu+ g g u WEIGHTED<=6 @3
// Process: mu+ c > mu+ g g c WEIGHTED<=6 @3
// Process: e+ d > e+ g g d WEIGHTED<=6 @3
// Process: e+ s > e+ g g s WEIGHTED<=6 @3
// Process: mu+ d > mu+ g g d WEIGHTED<=6 @3
// Process: mu+ s > mu+ g g s WEIGHTED<=6 @3
// Process: e+ u~ > e+ g g u~ WEIGHTED<=6 @3
// Process: e+ c~ > e+ g g c~ WEIGHTED<=6 @3
// Process: mu+ u~ > mu+ g g u~ WEIGHTED<=6 @3
// Process: mu+ c~ > mu+ g g c~ WEIGHTED<=6 @3
// Process: e+ d~ > e+ g g d~ WEIGHTED<=6 @3
// Process: e+ s~ > e+ g g s~ WEIGHTED<=6 @3
// Process: mu+ d~ > mu+ g g d~ WEIGHTED<=6 @3
// Process: mu+ s~ > mu+ g g s~ WEIGHTED<=6 @3
// Process: e- u > ve g g d WEIGHTED<=6 @3
// Process: e- c > ve g g s WEIGHTED<=6 @3
// Process: mu- u > vm g g d WEIGHTED<=6 @3
// Process: mu- c > vm g g s WEIGHTED<=6 @3
// Process: ve d > e- g g u WEIGHTED<=6 @3
// Process: ve s > e- g g c WEIGHTED<=6 @3
// Process: vm d > mu- g g u WEIGHTED<=6 @3
// Process: vm s > mu- g g c WEIGHTED<=6 @3
// Process: e- d~ > ve g g u~ WEIGHTED<=6 @3
// Process: e- s~ > ve g g c~ WEIGHTED<=6 @3
// Process: mu- d~ > vm g g u~ WEIGHTED<=6 @3
// Process: mu- s~ > vm g g c~ WEIGHTED<=6 @3
// Process: ve u~ > e- g g d~ WEIGHTED<=6 @3
// Process: ve c~ > e- g g s~ WEIGHTED<=6 @3
// Process: vm u~ > mu- g g d~ WEIGHTED<=6 @3
// Process: vm c~ > mu- g g s~ WEIGHTED<=6 @3
// Process: ve u > ve g g u WEIGHTED<=6 @3
// Process: ve c > ve g g c WEIGHTED<=6 @3
// Process: vm u > vm g g u WEIGHTED<=6 @3
// Process: vm c > vm g g c WEIGHTED<=6 @3
// Process: vt u > vt g g u WEIGHTED<=6 @3
// Process: vt c > vt g g c WEIGHTED<=6 @3
// Process: ve d > ve g g d WEIGHTED<=6 @3
// Process: ve s > ve g g s WEIGHTED<=6 @3
// Process: vm d > vm g g d WEIGHTED<=6 @3
// Process: vm s > vm g g s WEIGHTED<=6 @3
// Process: vt d > vt g g d WEIGHTED<=6 @3
// Process: vt s > vt g g s WEIGHTED<=6 @3
// Process: ve u~ > ve g g u~ WEIGHTED<=6 @3
// Process: ve c~ > ve g g c~ WEIGHTED<=6 @3
// Process: vm u~ > vm g g u~ WEIGHTED<=6 @3
// Process: vm c~ > vm g g c~ WEIGHTED<=6 @3
// Process: vt u~ > vt g g u~ WEIGHTED<=6 @3
// Process: vt c~ > vt g g c~ WEIGHTED<=6 @3
// Process: ve d~ > ve g g d~ WEIGHTED<=6 @3
// Process: ve s~ > ve g g s~ WEIGHTED<=6 @3
// Process: vm d~ > vm g g d~ WEIGHTED<=6 @3
// Process: vm s~ > vm g g s~ WEIGHTED<=6 @3
// Process: vt d~ > vt g g d~ WEIGHTED<=6 @3
// Process: vt s~ > vt g g s~ WEIGHTED<=6 @3
// Process: e+ d > ve~ g g u WEIGHTED<=6 @3
// Process: e+ s > ve~ g g c WEIGHTED<=6 @3
// Process: mu+ d > vm~ g g u WEIGHTED<=6 @3
// Process: mu+ s > vm~ g g c WEIGHTED<=6 @3
// Process: ve~ u > e+ g g d WEIGHTED<=6 @3
// Process: ve~ c > e+ g g s WEIGHTED<=6 @3
// Process: vm~ u > mu+ g g d WEIGHTED<=6 @3
// Process: vm~ c > mu+ g g s WEIGHTED<=6 @3
// Process: e+ u~ > ve~ g g d~ WEIGHTED<=6 @3
// Process: e+ c~ > ve~ g g s~ WEIGHTED<=6 @3
// Process: mu+ u~ > vm~ g g d~ WEIGHTED<=6 @3
// Process: mu+ c~ > vm~ g g s~ WEIGHTED<=6 @3
// Process: ve~ d~ > e+ g g u~ WEIGHTED<=6 @3
// Process: ve~ s~ > e+ g g c~ WEIGHTED<=6 @3
// Process: vm~ d~ > mu+ g g u~ WEIGHTED<=6 @3
// Process: vm~ s~ > mu+ g g c~ WEIGHTED<=6 @3
// Process: ve~ u > ve~ g g u WEIGHTED<=6 @3
// Process: ve~ c > ve~ g g c WEIGHTED<=6 @3
// Process: vm~ u > vm~ g g u WEIGHTED<=6 @3
// Process: vm~ c > vm~ g g c WEIGHTED<=6 @3
// Process: vt~ u > vt~ g g u WEIGHTED<=6 @3
// Process: vt~ c > vt~ g g c WEIGHTED<=6 @3
// Process: ve~ d > ve~ g g d WEIGHTED<=6 @3
// Process: ve~ s > ve~ g g s WEIGHTED<=6 @3
// Process: vm~ d > vm~ g g d WEIGHTED<=6 @3
// Process: vm~ s > vm~ g g s WEIGHTED<=6 @3
// Process: vt~ d > vt~ g g d WEIGHTED<=6 @3
// Process: vt~ s > vt~ g g s WEIGHTED<=6 @3
// Process: ve~ u~ > ve~ g g u~ WEIGHTED<=6 @3
// Process: ve~ c~ > ve~ g g c~ WEIGHTED<=6 @3
// Process: vm~ u~ > vm~ g g u~ WEIGHTED<=6 @3
// Process: vm~ c~ > vm~ g g c~ WEIGHTED<=6 @3
// Process: vt~ u~ > vt~ g g u~ WEIGHTED<=6 @3
// Process: vt~ c~ > vt~ g g c~ WEIGHTED<=6 @3
// Process: ve~ d~ > ve~ g g d~ WEIGHTED<=6 @3
// Process: ve~ s~ > ve~ g g s~ WEIGHTED<=6 @3
// Process: vm~ d~ > vm~ g g d~ WEIGHTED<=6 @3
// Process: vm~ s~ > vm~ g g s~ WEIGHTED<=6 @3
// Process: vt~ d~ > vt~ g g d~ WEIGHTED<=6 @3
// Process: vt~ s~ > vt~ g g s~ WEIGHTED<=6 @3

// Exception class
class PY8MEs_R3_P35_sm_lq_lggqException : public exception
{
  virtual const char * what() const throw()
  {
    return "Exception in class 'PY8MEs_R3_P35_sm_lq_lggq'."; 
  }
}
PY8MEs_R3_P35_sm_lq_lggq_exception; 

std::set<int> PY8MEs_R3_P35_sm_lq_lggq::s_channel_proc = std::set<int>
    (createset<int> ());

int PY8MEs_R3_P35_sm_lq_lggq::helicities[ncomb][nexternal] = {{-1, -1, -1, -1,
    -1, -1}, {-1, -1, -1, -1, -1, 1}, {-1, -1, -1, -1, 1, -1}, {-1, -1, -1, -1,
    1, 1}, {-1, -1, -1, 1, -1, -1}, {-1, -1, -1, 1, -1, 1}, {-1, -1, -1, 1, 1,
    -1}, {-1, -1, -1, 1, 1, 1}, {-1, -1, 1, -1, -1, -1}, {-1, -1, 1, -1, -1,
    1}, {-1, -1, 1, -1, 1, -1}, {-1, -1, 1, -1, 1, 1}, {-1, -1, 1, 1, -1, -1},
    {-1, -1, 1, 1, -1, 1}, {-1, -1, 1, 1, 1, -1}, {-1, -1, 1, 1, 1, 1}, {-1, 1,
    -1, -1, -1, -1}, {-1, 1, -1, -1, -1, 1}, {-1, 1, -1, -1, 1, -1}, {-1, 1,
    -1, -1, 1, 1}, {-1, 1, -1, 1, -1, -1}, {-1, 1, -1, 1, -1, 1}, {-1, 1, -1,
    1, 1, -1}, {-1, 1, -1, 1, 1, 1}, {-1, 1, 1, -1, -1, -1}, {-1, 1, 1, -1, -1,
    1}, {-1, 1, 1, -1, 1, -1}, {-1, 1, 1, -1, 1, 1}, {-1, 1, 1, 1, -1, -1},
    {-1, 1, 1, 1, -1, 1}, {-1, 1, 1, 1, 1, -1}, {-1, 1, 1, 1, 1, 1}, {1, -1,
    -1, -1, -1, -1}, {1, -1, -1, -1, -1, 1}, {1, -1, -1, -1, 1, -1}, {1, -1,
    -1, -1, 1, 1}, {1, -1, -1, 1, -1, -1}, {1, -1, -1, 1, -1, 1}, {1, -1, -1,
    1, 1, -1}, {1, -1, -1, 1, 1, 1}, {1, -1, 1, -1, -1, -1}, {1, -1, 1, -1, -1,
    1}, {1, -1, 1, -1, 1, -1}, {1, -1, 1, -1, 1, 1}, {1, -1, 1, 1, -1, -1}, {1,
    -1, 1, 1, -1, 1}, {1, -1, 1, 1, 1, -1}, {1, -1, 1, 1, 1, 1}, {1, 1, -1, -1,
    -1, -1}, {1, 1, -1, -1, -1, 1}, {1, 1, -1, -1, 1, -1}, {1, 1, -1, -1, 1,
    1}, {1, 1, -1, 1, -1, -1}, {1, 1, -1, 1, -1, 1}, {1, 1, -1, 1, 1, -1}, {1,
    1, -1, 1, 1, 1}, {1, 1, 1, -1, -1, -1}, {1, 1, 1, -1, -1, 1}, {1, 1, 1, -1,
    1, -1}, {1, 1, 1, -1, 1, 1}, {1, 1, 1, 1, -1, -1}, {1, 1, 1, 1, -1, 1}, {1,
    1, 1, 1, 1, -1}, {1, 1, 1, 1, 1, 1}};

// Normalization factors the various processes
// Denominators: spins, colors and identical particles
int PY8MEs_R3_P35_sm_lq_lggq::denom_colors[nprocesses] = {3, 3, 3, 3, 3, 3, 3,
    3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3};
int PY8MEs_R3_P35_sm_lq_lggq::denom_hels[nprocesses] = {4, 4, 4, 4, 4, 4, 4, 4,
    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4};
int PY8MEs_R3_P35_sm_lq_lggq::denom_iden[nprocesses] = {2, 2, 2, 2, 2, 2, 2, 2,
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2};

//--------------------------------------------------------------------------
// Color config initialization
void PY8MEs_R3_P35_sm_lq_lggq::initColorConfigs() 
{
  color_configs = vector < vec_vec_int > (); 
  jamp_nc_relative_power = vector < vec_int > (); 

  // Color flows of process Process: e- u > e- g g u WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[0].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(2)(1)(3)(2)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #1
  color_configs[0].push_back(vec_int(createvector<int>
      (0)(0)(2)(0)(0)(0)(2)(3)(3)(1)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 

  // Color flows of process Process: e- d > e- g g d WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[1].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(2)(1)(3)(2)(1)(0)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #1
  color_configs[1].push_back(vec_int(createvector<int>
      (0)(0)(2)(0)(0)(0)(2)(3)(3)(1)(1)(0)));
  jamp_nc_relative_power[1].push_back(0); 

  // Color flows of process Process: e- u~ > e- g g u~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(1)(3)(2)(0)(3)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #1
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(3)(3)(1)(0)(2)));
  jamp_nc_relative_power[2].push_back(0); 

  // Color flows of process Process: e- d~ > e- g g d~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[3].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(1)(3)(2)(0)(3)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #1
  color_configs[3].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(3)(3)(1)(0)(2)));
  jamp_nc_relative_power[3].push_back(0); 

  // Color flows of process Process: e+ u > e+ g g u WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[4].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(2)(1)(3)(2)(1)(0)));
  jamp_nc_relative_power[4].push_back(0); 
  // JAMP #1
  color_configs[4].push_back(vec_int(createvector<int>
      (0)(0)(2)(0)(0)(0)(2)(3)(3)(1)(1)(0)));
  jamp_nc_relative_power[4].push_back(0); 

  // Color flows of process Process: e+ d > e+ g g d WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[5].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(2)(1)(3)(2)(1)(0)));
  jamp_nc_relative_power[5].push_back(0); 
  // JAMP #1
  color_configs[5].push_back(vec_int(createvector<int>
      (0)(0)(2)(0)(0)(0)(2)(3)(3)(1)(1)(0)));
  jamp_nc_relative_power[5].push_back(0); 

  // Color flows of process Process: e+ u~ > e+ g g u~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[6].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(1)(3)(2)(0)(3)));
  jamp_nc_relative_power[6].push_back(0); 
  // JAMP #1
  color_configs[6].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(3)(3)(1)(0)(2)));
  jamp_nc_relative_power[6].push_back(0); 

  // Color flows of process Process: e+ d~ > e+ g g d~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[7].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(1)(3)(2)(0)(3)));
  jamp_nc_relative_power[7].push_back(0); 
  // JAMP #1
  color_configs[7].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(3)(3)(1)(0)(2)));
  jamp_nc_relative_power[7].push_back(0); 

  // Color flows of process Process: e- u > ve g g d WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[8].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(2)(1)(3)(2)(1)(0)));
  jamp_nc_relative_power[8].push_back(0); 
  // JAMP #1
  color_configs[8].push_back(vec_int(createvector<int>
      (0)(0)(2)(0)(0)(0)(2)(3)(3)(1)(1)(0)));
  jamp_nc_relative_power[8].push_back(0); 

  // Color flows of process Process: e- d~ > ve g g u~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[9].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(1)(3)(2)(0)(3)));
  jamp_nc_relative_power[9].push_back(0); 
  // JAMP #1
  color_configs[9].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(3)(3)(1)(0)(2)));
  jamp_nc_relative_power[9].push_back(0); 

  // Color flows of process Process: ve u > ve g g u WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[10].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(2)(1)(3)(2)(1)(0)));
  jamp_nc_relative_power[10].push_back(0); 
  // JAMP #1
  color_configs[10].push_back(vec_int(createvector<int>
      (0)(0)(2)(0)(0)(0)(2)(3)(3)(1)(1)(0)));
  jamp_nc_relative_power[10].push_back(0); 

  // Color flows of process Process: ve d > ve g g d WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[11].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(2)(1)(3)(2)(1)(0)));
  jamp_nc_relative_power[11].push_back(0); 
  // JAMP #1
  color_configs[11].push_back(vec_int(createvector<int>
      (0)(0)(2)(0)(0)(0)(2)(3)(3)(1)(1)(0)));
  jamp_nc_relative_power[11].push_back(0); 

  // Color flows of process Process: ve u~ > ve g g u~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[12].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(1)(3)(2)(0)(3)));
  jamp_nc_relative_power[12].push_back(0); 
  // JAMP #1
  color_configs[12].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(3)(3)(1)(0)(2)));
  jamp_nc_relative_power[12].push_back(0); 

  // Color flows of process Process: ve d~ > ve g g d~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[13].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(1)(3)(2)(0)(3)));
  jamp_nc_relative_power[13].push_back(0); 
  // JAMP #1
  color_configs[13].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(3)(3)(1)(0)(2)));
  jamp_nc_relative_power[13].push_back(0); 

  // Color flows of process Process: e+ d > ve~ g g u WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[14].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(2)(1)(3)(2)(1)(0)));
  jamp_nc_relative_power[14].push_back(0); 
  // JAMP #1
  color_configs[14].push_back(vec_int(createvector<int>
      (0)(0)(2)(0)(0)(0)(2)(3)(3)(1)(1)(0)));
  jamp_nc_relative_power[14].push_back(0); 

  // Color flows of process Process: e+ u~ > ve~ g g d~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[15].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(1)(3)(2)(0)(3)));
  jamp_nc_relative_power[15].push_back(0); 
  // JAMP #1
  color_configs[15].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(3)(3)(1)(0)(2)));
  jamp_nc_relative_power[15].push_back(0); 

  // Color flows of process Process: ve~ u > ve~ g g u WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[16].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(2)(1)(3)(2)(1)(0)));
  jamp_nc_relative_power[16].push_back(0); 
  // JAMP #1
  color_configs[16].push_back(vec_int(createvector<int>
      (0)(0)(2)(0)(0)(0)(2)(3)(3)(1)(1)(0)));
  jamp_nc_relative_power[16].push_back(0); 

  // Color flows of process Process: ve~ d > ve~ g g d WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[17].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(2)(1)(3)(2)(1)(0)));
  jamp_nc_relative_power[17].push_back(0); 
  // JAMP #1
  color_configs[17].push_back(vec_int(createvector<int>
      (0)(0)(2)(0)(0)(0)(2)(3)(3)(1)(1)(0)));
  jamp_nc_relative_power[17].push_back(0); 

  // Color flows of process Process: ve~ u~ > ve~ g g u~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[18].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(1)(3)(2)(0)(3)));
  jamp_nc_relative_power[18].push_back(0); 
  // JAMP #1
  color_configs[18].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(3)(3)(1)(0)(2)));
  jamp_nc_relative_power[18].push_back(0); 

  // Color flows of process Process: ve~ d~ > ve~ g g d~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[19].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(1)(3)(2)(0)(3)));
  jamp_nc_relative_power[19].push_back(0); 
  // JAMP #1
  color_configs[19].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(3)(3)(1)(0)(2)));
  jamp_nc_relative_power[19].push_back(0); 
}

//--------------------------------------------------------------------------
// Destructor.
PY8MEs_R3_P35_sm_lq_lggq::~PY8MEs_R3_P35_sm_lq_lggq() 
{
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    delete[] p[i]; 
    p[i] = NULL; 
  }
}

//--------------------------------------------------------------------------
// Invert the permutation mapping
vector<int> PY8MEs_R3_P35_sm_lq_lggq::invert_mapping(vector<int> mapping) 
{
  vector<int> inverted_mapping; 
  for (unsigned int i = 0; i < mapping.size(); i++ )
  {
    for (unsigned int j = 0; j < mapping.size(); j++ )
    {
      if (mapping[j] == ((int)i))
      {
        inverted_mapping.push_back(j); 
        break; 
      }
    }
  }
  return inverted_mapping; 
}

//--------------------------------------------------------------------------
// Return the list of possible helicity configurations
vector < vec_int > PY8MEs_R3_P35_sm_lq_lggq::getHelicityConfigs(vector<int>
    permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(ncomb, vector<int> (nexternal, 0)); 
  for (unsigned int ihel = 0; ihel < ncomb; ihel++ )
  {
    for(unsigned int j = 0; j < nexternal; j++ )
    {
      res[ihel][chosenPerm[j]] = helicities[ihel][j]; 
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Return the list of possible color configurations
vector < vec_int > PY8MEs_R3_P35_sm_lq_lggq::getColorConfigs(int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(color_configs[chosenProcID].size(), vector<int>
      (nexternal * 2, 0));
  for (unsigned int icol = 0; icol < color_configs[chosenProcID].size(); icol++
      )
  {
    for(unsigned int j = 0; j < (2 * nexternal); j++ )
    {
      res[icol][chosenPerm[j/2] * 2 + j%2] =
          color_configs[chosenProcID][icol][j];
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Get JAMP relative N_c power
int PY8MEs_R3_P35_sm_lq_lggq::getColorFlowRelativeNCPower(int color_flow_ID,
    int specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return jamp_nc_relative_power[chosenProcID][color_flow_ID]; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity ID -> Helicity Config
vector<int> PY8MEs_R3_P35_sm_lq_lggq::getHelicityConfigForID(int hel_ID,
    vector<int> permutation)
{
  if (hel_ID < 0 || hel_ID >= ncomb)
  {
    cerr <<  "Error in function 'getHelicityConfigForID' of class" << 
    " 'PY8MEs_R3_P35_sm_lq_lggq': Specified helicity ID '" << 
    hel_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R3_P35_sm_lq_lggq_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(nexternal, 0); 
  for (unsigned int j = 0; j < nexternal; j++ )
  {
    res[chosenPerm[j]] = helicities[hel_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity Config -> Helicity ID
int PY8MEs_R3_P35_sm_lq_lggq::getHelicityIDForConfig(vector<int> hel_config,
    vector<int> permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  int user_ihel = -1; 
  if (hel_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < ncomb; i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < nexternal; j++ )
      {
        if (helicities[i][chosenPerm[j]] != hel_config[j])
        {
          found = false; 
          break; 
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_ihel = i; 
        break; 
      }
    }
    if (user_ihel == -1)
    {
      cerr <<  "Error in function 'getHelicityIDForConfig' of class" << 
      " 'PY8MEs_R3_P35_sm_lq_lggq': Specified helicity" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R3_P35_sm_lq_lggq_exception; 
    }
  }
  return user_ihel; 
}


//--------------------------------------------------------------------------
// Implements the map Color ID -> Color Config
vector<int> PY8MEs_R3_P35_sm_lq_lggq::getColorConfigForID(int color_ID, int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < 0 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getColorConfigForID' of class" << 
    " 'PY8MEs_R3_P35_sm_lq_lggq': Specified color ID '" << 
    color_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R3_P35_sm_lq_lggq_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(color_configs[chosenProcID][color_ID].size(), 0); 
  for (unsigned int j = 0; j < (2 * nexternal); j++ )
  {
    res[chosenPerm[j/2] * 2 + j%2] = color_configs[chosenProcID][color_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Color Config -> Color ID
int PY8MEs_R3_P35_sm_lq_lggq::getColorIDForConfig(vector<int> color_config, int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = -1; 
  if (color_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < color_configs[chosenProcID].size(); i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < (nexternal * 2); j++ )
      {

        // If colorless then make sure it matches
        // The little arithmetics in the color index is just
        // the permutation applies on the particle list which is
        // twice smaller since each particle can have two color indices.
        if (color_config[j] == 0)
        {
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] != 0)
          {
            found = false; 
            break; 
          }
          // Otherwise check that the color linked position matches
        }
        else
        {
          int color_linked_pos = -1; 
          // Find the other end of the line in the user color config
          for (unsigned int k = 0; k < (nexternal * 2); k++ )
          {
            if (k == j)
              continue; 
            if (color_config[j] == color_config[k])
            {
              color_linked_pos = k; 
              break; 
            }
          }
          if (color_linked_pos == -1)
          {
            cerr <<  "Error in function 'getColorIDForConfig' of class" << 
            " 'PY8MEs_R3_P35_sm_lq_lggq': A color line could " << 
            " not be closed." << endl; 
            throw PY8MEs_R3_P35_sm_lq_lggq_exception; 
          }
          // Now check whether the color line matches
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] !=
              color_configs[chosenProcID][i][chosenPerm[color_linked_pos/2] * 2
              + color_linked_pos%2])
          {
            found = false; 
            break; 
          }
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_icol = i; 
        break; 
      }
    }

    if (user_icol == -1)
    {
      cerr <<  "Error in function 'getColorIDForConfig' of class" << 
      " 'PY8MEs_R3_P35_sm_lq_lggq': Specified color" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R3_P35_sm_lq_lggq_exception; 
    }
  }
  return user_icol; 
}

//--------------------------------------------------------------------------
// Returns all result previously computed in SigmaKin
vector < vec_double > PY8MEs_R3_P35_sm_lq_lggq::getAllResults(int
    specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return all_results[chosenProcID]; 
}

//--------------------------------------------------------------------------
// Returns a result previously computed in SigmaKin for a specific helicity
// and color ID. -1 means avg and summed over that characteristic.
double PY8MEs_R3_P35_sm_lq_lggq::getResult(int helicity_ID, int color_ID, int
    specify_proc_ID)
{
  if (helicity_ID < - 1 || helicity_ID >= ncomb)
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R3_P35_sm_lq_lggq': Specified helicity ID '" << 
    helicity_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R3_P35_sm_lq_lggq_exception; 
  }
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < - 1 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R3_P35_sm_lq_lggq': Specified color ID '" << 
    color_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R3_P35_sm_lq_lggq_exception; 
  }
  return all_results[chosenProcID][helicity_ID + 1][color_ID + 1]; 
}

//--------------------------------------------------------------------------
// Check for the availability of the requested process and if available,
// If available, this returns the corresponding permutation and Proc_ID to use.
// If not available, this returns a negative Proc_ID.
pair < vector<int> , int > PY8MEs_R3_P35_sm_lq_lggq::static_getPY8ME(vector<int> initial_pdgs, vector<int> final_pdgs, set<int> schannels) 
{

  // Not available return value
  pair < vector<int> , int > NA(vector<int> (), -1); 

  // Check if s-channel requirements match
  if (nreq_s_channels > 0)
  {
    if (schannels != s_channel_proc)
      return NA; 
  }
  else
  {
    if (schannels.size() != 0)
      return NA; 
  }

  // Check number of final state particles
  if (final_pdgs.size() != (nexternal - ninitial))
    return NA; 

  // Check number of initial state particles
  if (initial_pdgs.size() != ninitial)
    return NA; 

  // List of processes available in this class
  const int nprocs = 112; 
  const int proc_IDS[nprocs] = {0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3,
      4, 4, 4, 4, 5, 5, 5, 5, 6, 6, 6, 6, 7, 7, 7, 7, 8, 8, 8, 8, 8, 8, 8, 8,
      9, 9, 9, 9, 9, 9, 9, 9, 10, 10, 10, 10, 10, 10, 11, 11, 11, 11, 11, 11,
      12, 12, 12, 12, 12, 12, 13, 13, 13, 13, 13, 13, 14, 14, 14, 14, 14, 14,
      14, 14, 15, 15, 15, 15, 15, 15, 15, 15, 16, 16, 16, 16, 16, 16, 17, 17,
      17, 17, 17, 17, 18, 18, 18, 18, 18, 18, 19, 19, 19, 19, 19, 19};
  const int in_pdgs[nprocs][ninitial] = {{11, 2}, {11, 4}, {13, 2}, {13, 4},
      {11, 1}, {11, 3}, {13, 1}, {13, 3}, {11, -2}, {11, -4}, {13, -2}, {13,
      -4}, {11, -1}, {11, -3}, {13, -1}, {13, -3}, {-11, 2}, {-11, 4}, {-13,
      2}, {-13, 4}, {-11, 1}, {-11, 3}, {-13, 1}, {-13, 3}, {-11, -2}, {-11,
      -4}, {-13, -2}, {-13, -4}, {-11, -1}, {-11, -3}, {-13, -1}, {-13, -3},
      {11, 2}, {11, 4}, {13, 2}, {13, 4}, {12, 1}, {12, 3}, {14, 1}, {14, 3},
      {11, -1}, {11, -3}, {13, -1}, {13, -3}, {12, -2}, {12, -4}, {14, -2},
      {14, -4}, {12, 2}, {12, 4}, {14, 2}, {14, 4}, {16, 2}, {16, 4}, {12, 1},
      {12, 3}, {14, 1}, {14, 3}, {16, 1}, {16, 3}, {12, -2}, {12, -4}, {14,
      -2}, {14, -4}, {16, -2}, {16, -4}, {12, -1}, {12, -3}, {14, -1}, {14,
      -3}, {16, -1}, {16, -3}, {-11, 1}, {-11, 3}, {-13, 1}, {-13, 3}, {-12,
      2}, {-12, 4}, {-14, 2}, {-14, 4}, {-11, -2}, {-11, -4}, {-13, -2}, {-13,
      -4}, {-12, -1}, {-12, -3}, {-14, -1}, {-14, -3}, {-12, 2}, {-12, 4},
      {-14, 2}, {-14, 4}, {-16, 2}, {-16, 4}, {-12, 1}, {-12, 3}, {-14, 1},
      {-14, 3}, {-16, 1}, {-16, 3}, {-12, -2}, {-12, -4}, {-14, -2}, {-14, -4},
      {-16, -2}, {-16, -4}, {-12, -1}, {-12, -3}, {-14, -1}, {-14, -3}, {-16,
      -1}, {-16, -3}};
  const int out_pdgs[nprocs][nexternal - ninitial] = {{11, 21, 21, 2}, {11, 21,
      21, 4}, {13, 21, 21, 2}, {13, 21, 21, 4}, {11, 21, 21, 1}, {11, 21, 21,
      3}, {13, 21, 21, 1}, {13, 21, 21, 3}, {11, 21, 21, -2}, {11, 21, 21, -4},
      {13, 21, 21, -2}, {13, 21, 21, -4}, {11, 21, 21, -1}, {11, 21, 21, -3},
      {13, 21, 21, -1}, {13, 21, 21, -3}, {-11, 21, 21, 2}, {-11, 21, 21, 4},
      {-13, 21, 21, 2}, {-13, 21, 21, 4}, {-11, 21, 21, 1}, {-11, 21, 21, 3},
      {-13, 21, 21, 1}, {-13, 21, 21, 3}, {-11, 21, 21, -2}, {-11, 21, 21, -4},
      {-13, 21, 21, -2}, {-13, 21, 21, -4}, {-11, 21, 21, -1}, {-11, 21, 21,
      -3}, {-13, 21, 21, -1}, {-13, 21, 21, -3}, {12, 21, 21, 1}, {12, 21, 21,
      3}, {14, 21, 21, 1}, {14, 21, 21, 3}, {11, 21, 21, 2}, {11, 21, 21, 4},
      {13, 21, 21, 2}, {13, 21, 21, 4}, {12, 21, 21, -2}, {12, 21, 21, -4},
      {14, 21, 21, -2}, {14, 21, 21, -4}, {11, 21, 21, -1}, {11, 21, 21, -3},
      {13, 21, 21, -1}, {13, 21, 21, -3}, {12, 21, 21, 2}, {12, 21, 21, 4},
      {14, 21, 21, 2}, {14, 21, 21, 4}, {16, 21, 21, 2}, {16, 21, 21, 4}, {12,
      21, 21, 1}, {12, 21, 21, 3}, {14, 21, 21, 1}, {14, 21, 21, 3}, {16, 21,
      21, 1}, {16, 21, 21, 3}, {12, 21, 21, -2}, {12, 21, 21, -4}, {14, 21, 21,
      -2}, {14, 21, 21, -4}, {16, 21, 21, -2}, {16, 21, 21, -4}, {12, 21, 21,
      -1}, {12, 21, 21, -3}, {14, 21, 21, -1}, {14, 21, 21, -3}, {16, 21, 21,
      -1}, {16, 21, 21, -3}, {-12, 21, 21, 2}, {-12, 21, 21, 4}, {-14, 21, 21,
      2}, {-14, 21, 21, 4}, {-11, 21, 21, 1}, {-11, 21, 21, 3}, {-13, 21, 21,
      1}, {-13, 21, 21, 3}, {-12, 21, 21, -1}, {-12, 21, 21, -3}, {-14, 21, 21,
      -1}, {-14, 21, 21, -3}, {-11, 21, 21, -2}, {-11, 21, 21, -4}, {-13, 21,
      21, -2}, {-13, 21, 21, -4}, {-12, 21, 21, 2}, {-12, 21, 21, 4}, {-14, 21,
      21, 2}, {-14, 21, 21, 4}, {-16, 21, 21, 2}, {-16, 21, 21, 4}, {-12, 21,
      21, 1}, {-12, 21, 21, 3}, {-14, 21, 21, 1}, {-14, 21, 21, 3}, {-16, 21,
      21, 1}, {-16, 21, 21, 3}, {-12, 21, 21, -2}, {-12, 21, 21, -4}, {-14, 21,
      21, -2}, {-14, 21, 21, -4}, {-16, 21, 21, -2}, {-16, 21, 21, -4}, {-12,
      21, 21, -1}, {-12, 21, 21, -3}, {-14, 21, 21, -1}, {-14, 21, 21, -3},
      {-16, 21, 21, -1}, {-16, 21, 21, -3}};

  bool in_pdgs_used[ninitial]; 
  bool out_pdgs_used[nexternal - ninitial]; 
  for(unsigned int i = 0; i < nprocs; i++ )
  {
    int permutations[nexternal]; 

    // Reinitialize initial state look-up variables
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      in_pdgs_used[j] = false; 
      permutations[j] = -1; 
    }
    // Look for initial state matches
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      for(unsigned int k = 0; k < ninitial; k++ )
      {
        // Make sure it has not been used already
        if (in_pdgs_used[k])
          continue; 
        if (initial_pdgs[k] == in_pdgs[i][j])
        {
          permutations[j] = k; 
          in_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[ninitial - 1] == -1)
      continue; 

    // Reinitialize final state look-up variables
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      out_pdgs_used[j] = false; 
      permutations[ninitial + j] = -1; 
    }
    // Look for final state matches
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      for(unsigned int k = 0; k < (nexternal - ninitial); k++ )
      {
        // Make sure it has not been used already
        if (out_pdgs_used[k])
          continue; 
        if (final_pdgs[k] == out_pdgs[i][j])
        {
          permutations[ninitial + j] = ninitial + k; 
          out_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[ninitial + j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[nexternal - 1] == -1)
      continue; 

    // Return process found
    return pair < vector<int> , int > (vector<int> (permutations, permutations
        + nexternal), proc_IDS[i]);
  }

  // No process found
  return NA; 
}

//--------------------------------------------------------------------------
// Set momenta
void PY8MEs_R3_P35_sm_lq_lggq::setMomenta(vector < vec_double > momenta_picked)
{
  if (momenta_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setMomenta' of class" << 
    " 'PY8MEs_R3_P35_sm_lq_lggq': Incorrect number of" << 
    " momenta specified." << endl; 
    throw PY8MEs_R3_P35_sm_lq_lggq_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    if (momenta_picked[i].size() != 4)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R3_P35_sm_lq_lggq': Incorrect number of" << 
      " momenta components specified." << endl; 
      throw PY8MEs_R3_P35_sm_lq_lggq_exception; 
    }
    if (momenta_picked[i][0] < 0.0)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R3_P35_sm_lq_lggq': A momentum was specified" << 
      " with negative energy. Check conventions." << endl; 
      throw PY8MEs_R3_P35_sm_lq_lggq_exception; 
    }
    for (unsigned int j = 0; j < 4; j++ )
    {
      p[i][j] = momenta_picked[i][j]; 
    }
  }
}

//--------------------------------------------------------------------------
// Set color configuration to use. An empty vector means sum over all.
void PY8MEs_R3_P35_sm_lq_lggq::setColors(vector<int> colors_picked)
{
  if (colors_picked.size() == 0)
  {
    user_colors = vector<int> (); 
    return; 
  }
  if (colors_picked.size() != (2 * nexternal))
  {
    cerr <<  "Error in function 'setColors' of class" << 
    " 'PY8MEs_R3_P35_sm_lq_lggq': Incorrect number" << 
    " of colors specified." << endl; 
    throw PY8MEs_R3_P35_sm_lq_lggq_exception; 
  }
  user_colors = vector<int> ((2 * nexternal), 0); 
  for(unsigned int i = 0; i < (2 * nexternal); i++ )
  {
    user_colors[i] = colors_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the helicity configuration to use. Am empty vector means sum over all.
void PY8MEs_R3_P35_sm_lq_lggq::setHelicities(vector<int> helicities_picked) 
{
  if (helicities_picked.size() != nexternal)
  {
    if (helicities_picked.size() == 0)
    {
      user_helicities = vector<int> (); 
      return; 
    }
    cerr <<  "Error in function 'setHelicities' of class" << 
    " 'PY8MEs_R3_P35_sm_lq_lggq': Incorrect number" << 
    " of helicities specified." << endl; 
    throw PY8MEs_R3_P35_sm_lq_lggq_exception; 
  }
  user_helicities = vector<int> (nexternal, 0); 
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    user_helicities[i] = helicities_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the permutation to use (will apply to momenta, colors and helicities)
void PY8MEs_R3_P35_sm_lq_lggq::setPermutation(vector<int> perm_picked) 
{
  if (perm_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setPermutations' of class" << 
    " 'PY8MEs_R3_P35_sm_lq_lggq': Incorrect number" << 
    " of permutations specified." << endl; 
    throw PY8MEs_R3_P35_sm_lq_lggq_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    perm[i] = perm_picked[i]; 
  }
}

// Set the proc_ID to use
void PY8MEs_R3_P35_sm_lq_lggq::setProcID(int procID_picked) 
{
  proc_ID = procID_picked; 
}

//--------------------------------------------------------------------------
// Initialize process.

void PY8MEs_R3_P35_sm_lq_lggq::initProc() 
{

  // Initialize flags
  include_symmetry_factors = true; 
  include_helicity_averaging_factors = true; 
  include_color_averaging_factors = true; 

  // Initialize vectors.
  perm = vector<int> (nexternal, 0); 
  user_colors = vector<int> (2 * nexternal, 0); 
  user_helicities = vector<int> (nexternal, 0); 
  p = vector < double * > (); 
  for (unsigned int i = 0; i < nexternal; i++ )
  {
    p.push_back(new double[4]); 
  }
  initColorConfigs(); 
  // Synchronize local variables dependent on the model with the active model.
  mME = vector<double> (nexternal, 0.); 
  syncProcModelParams(); 
  jamp2 = vector < vec_double > (20); 
  jamp2[0] = vector<double> (2, 0.); 
  jamp2[1] = vector<double> (2, 0.); 
  jamp2[2] = vector<double> (2, 0.); 
  jamp2[3] = vector<double> (2, 0.); 
  jamp2[4] = vector<double> (2, 0.); 
  jamp2[5] = vector<double> (2, 0.); 
  jamp2[6] = vector<double> (2, 0.); 
  jamp2[7] = vector<double> (2, 0.); 
  jamp2[8] = vector<double> (2, 0.); 
  jamp2[9] = vector<double> (2, 0.); 
  jamp2[10] = vector<double> (2, 0.); 
  jamp2[11] = vector<double> (2, 0.); 
  jamp2[12] = vector<double> (2, 0.); 
  jamp2[13] = vector<double> (2, 0.); 
  jamp2[14] = vector<double> (2, 0.); 
  jamp2[15] = vector<double> (2, 0.); 
  jamp2[16] = vector<double> (2, 0.); 
  jamp2[17] = vector<double> (2, 0.); 
  jamp2[18] = vector<double> (2, 0.); 
  jamp2[19] = vector<double> (2, 0.); 
  all_results = vector < vec_vec_double > (20); 
  // The first entry is always the color or helicity avg/summed matrix element.
  all_results[0] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[1] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[2] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[3] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[4] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[5] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[6] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[7] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[8] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[9] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[10] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[11] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[12] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[13] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[14] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[15] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[16] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[17] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[18] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[19] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
}

// Synchronize local variables of the process that depend on the model
// parameters
void PY8MEs_R3_P35_sm_lq_lggq::syncProcModelParams() 
{

  // Instantiate the model class and set parameters that stay fixed during run
  mME[0] = pars->ZERO; 
  mME[1] = pars->ZERO; 
  mME[2] = pars->ZERO; 
  mME[3] = pars->ZERO; 
  mME[4] = pars->ZERO; 
  mME[5] = pars->ZERO; 
}

//--------------------------------------------------------------------------
// Setter allowing to force particular values for the external masses
void PY8MEs_R3_P35_sm_lq_lggq::setMasses(vec_double external_masses) 
{

  if (external_masses.size() != mME.size())
  {
    cerr <<  "Error in function 'setMasses' of class" << 
    " 'PY8MEs_R3_P35_sm_lq_lggq': Incorrect number of" << 
    " masses specified." << endl; 
    throw PY8MEs_R3_P35_sm_lq_lggq_exception; 
  }
  for (unsigned int j = 0; j < mME.size(); j++ )
  {
    mME[j] = external_masses[perm[j]]; 
  }
}

//--------------------------------------------------------------------------
// Getter accessing external masses with the correct ordering
vector<double> PY8MEs_R3_P35_sm_lq_lggq::getMasses() 
{

  vec_double external_masses; 
  vector<int> invertedPerm; 
  invertedPerm = invert_mapping(perm); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    external_masses.push_back(mME[invertedPerm[i]]); 
  }
  return external_masses; 

}


// Set all values of the external masses to float(-mode) where mode can be
// 0 : Mass taken from the model
// 1 : Mass taken from p_i^2 if not massless to begin with
// 2 : Mass always taken from p_i^2.
void PY8MEs_R3_P35_sm_lq_lggq::setExternalMassesMode(int mode) 
{
  if (mode != 0 && mode != 1 && mode != 2)
  {
    cerr <<  "Error in function 'setExternalMassesMode' of class" << 
    " 'PY8MEs_R3_P35_sm_lq_lggq': Incorrect mode selected :" << mode << 
    ". It must be either 0, 1 or 2" << endl; 
    throw PY8MEs_R3_P35_sm_lq_lggq_exception; 
  }
  if (mode == 0)
  {
    syncProcModelParams(); 
  }
  else if (mode == 1)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      if (mME[j] != pars->ZERO)
      {
        mME[j] = -1.0; 
      }
    }
  }
  else if (mode == 2)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      mME[j] = -1.0; 
    }
  }
}

//--------------------------------------------------------------------------
// Evaluate the squared matrix element.

double PY8MEs_R3_P35_sm_lq_lggq::sigmaKin() 
{
  // Set the parameters which change event by event
  pars->setDependentParameters(); 
  pars->setDependentCouplings(); 
  // Reset color flows
  for(int i = 0; i < 2; i++ )
    jamp2[0][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[1][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[2][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[3][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[4][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[5][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[6][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[7][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[8][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[9][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[10][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[11][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[12][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[13][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[14][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[15][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[16][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[17][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[18][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[19][i] = 0.; 

  // Save previous values of mME
  vector<double> saved_mME(mME.size(), 0.0); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (mME[i] < 0.0)
    {
      saved_mME[i] = mME[i]; 
      mME[i] = sqrt(abs(pow(p[perm[i]][0], 2) - 
      (pow(p[perm[i]][1], 2) + pow(p[perm[i]][2], 2) + pow(p[perm[i]][3],
          2))));
    }
  }

  // Local variables and constants
  const int max_tries = 10; 
  vector < vec_bool > goodhel(nprocesses, vec_bool(ncomb, false)); 
  vec_int ntry(nprocesses, 0); 
  double t = 0.; 
  double result = 0.; 

  if (ntry[proc_ID] <= max_tries)
    ntry[proc_ID] = ntry[proc_ID] + 1; 

  // Find which helicity configuration is asked for
  // -1 indicates one wants to sum over helicities
  int user_ihel = getHelicityIDForConfig(user_helicities); 

  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = getColorIDForConfig(user_colors); 

  // Reset the list of results that will be recomputed here
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      all_results[proc_ID][ihel + 1][icolor + 1] = 0.; 
    }
  }

  // Calculate the matrix element for all helicities
  // unless already detected as vanishing
  for(int ihel = 0; ihel < ncomb; ihel++ )
  {
    // Skip helicity if already detected as vanishing
    if ((ntry[proc_ID] >= max_tries) && !goodhel[proc_ID][ihel])
      continue; 

    // Also skip helicity if user asks for a specific one
    if ((ntry[proc_ID] >= max_tries) && user_ihel != -1 && user_ihel != ihel)
      continue; 

    calculate_wavefunctions(helicities[ihel]); 

    // Reset locally computed color flows
    for(int i = 0; i < 2; i++ )
      jamp2[0][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[1][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[2][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[3][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[4][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[5][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[6][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[7][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[8][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[9][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[10][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[11][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[12][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[13][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[14][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[15][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[16][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[17][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[18][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[19][i] = 0.; 

    if (proc_ID == 0)
      t = matrix_3_emu_emggu(); 
    if (proc_ID == 1)
      t = matrix_3_emd_emggd(); 
    if (proc_ID == 2)
      t = matrix_3_emux_emggux(); 
    if (proc_ID == 3)
      t = matrix_3_emdx_emggdx(); 
    if (proc_ID == 4)
      t = matrix_3_epu_epggu(); 
    if (proc_ID == 5)
      t = matrix_3_epd_epggd(); 
    if (proc_ID == 6)
      t = matrix_3_epux_epggux(); 
    if (proc_ID == 7)
      t = matrix_3_epdx_epggdx(); 
    if (proc_ID == 8)
      t = matrix_3_emu_veggd(); 
    if (proc_ID == 9)
      t = matrix_3_emdx_veggux(); 
    if (proc_ID == 10)
      t = matrix_3_veu_veggu(); 
    if (proc_ID == 11)
      t = matrix_3_ved_veggd(); 
    if (proc_ID == 12)
      t = matrix_3_veux_veggux(); 
    if (proc_ID == 13)
      t = matrix_3_vedx_veggdx(); 
    if (proc_ID == 14)
      t = matrix_3_epd_vexggu(); 
    if (proc_ID == 15)
      t = matrix_3_epux_vexggdx(); 
    if (proc_ID == 16)
      t = matrix_3_vexu_vexggu(); 
    if (proc_ID == 17)
      t = matrix_3_vexd_vexggd(); 
    if (proc_ID == 18)
      t = matrix_3_vexux_vexggux(); 
    if (proc_ID == 19)
      t = matrix_3_vexdx_vexggdx(); 

    // Store which helicities give non-zero result
    if ((ntry[proc_ID] < max_tries) && t != 0. && !goodhel[proc_ID][ihel])
      goodhel[proc_ID][ihel] = true; 

    // Aggregate results
    if (user_ihel == -1 || user_ihel == ihel)
    {
      if (user_icol == -1)
      {
        result = result + t; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][0] += t; 
          for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
          {
            all_results[proc_ID][0][i + 1] += jamp2[proc_ID][i]; 
          }
        }
        all_results[proc_ID][ihel + 1][0] += t; 
        for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
        {
          all_results[proc_ID][ihel + 1][i + 1] += jamp2[proc_ID][i]; 
        }
      }
      else
      {
        result = result + jamp2[proc_ID][user_icol]; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][user_icol + 1] += jamp2[proc_ID][user_icol]; 
        }
        all_results[proc_ID][ihel + 1][user_icol + 1] +=
            jamp2[proc_ID][user_icol];
      }
    }

  }

  // Normalize results with the identical particle factor
  if (include_symmetry_factors)
  {
    result = result/denom_iden[proc_ID]; 
  }
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      if (include_symmetry_factors)
      {
        all_results[proc_ID][ihel + 1][icolor + 1] /= denom_iden[proc_ID]; 
      }
    }
  }


  // Normalize when when summing+averaging over helicity configurations
  if (user_ihel == -1 && include_helicity_averaging_factors)
  {
    result /= denom_hels[proc_ID]; 
    if (user_icol == -1)
    {
      all_results[proc_ID][0][0] /= denom_hels[proc_ID]; 
      for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
      {
        all_results[proc_ID][0][i + 1] /= denom_hels[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][0][user_icol + 1] /= denom_hels[proc_ID]; 
    }
  }

  // Normalize when summing+averaging over color configurations
  if (user_icol == -1 && include_color_averaging_factors)
  {
    result /= denom_colors[proc_ID]; 
    if (user_ihel == -1)
    {
      all_results[proc_ID][0][0] /= denom_colors[proc_ID]; 
      for (unsigned int i = 0; i < ncomb; i++ )
      {
        all_results[proc_ID][i + 1][0] /= denom_colors[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][user_ihel + 1][0] /= denom_colors[proc_ID]; 
    }
  }

  // Reinstate previous values of mME
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (saved_mME[i] < 0.0)
    {
      mME[i] = saved_mME[i]; 
    }
  }

  // Finally return it
  return result; 
}

//==========================================================================
// Private class member functions

//--------------------------------------------------------------------------
// Evaluate |M|^2 for each subprocess

void PY8MEs_R3_P35_sm_lq_lggq::calculate_wavefunctions(const int hel[])
{
  // Calculate wavefunctions for all processes
  // Calculate all wavefunctions
  ixxxxx(p[perm[0]], mME[0], hel[0], +1, w[0]); 
  ixxxxx(p[perm[1]], mME[1], hel[1], +1, w[1]); 
  oxxxxx(p[perm[2]], mME[2], hel[2], +1, w[2]); 
  vxxxxx(p[perm[3]], mME[3], hel[3], +1, w[3]); 
  vxxxxx(p[perm[4]], mME[4], hel[4], +1, w[4]); 
  oxxxxx(p[perm[5]], mME[5], hel[5], +1, w[5]); 
  FFV1P0_3(w[0], w[2], pars->GC_3, pars->ZERO, pars->ZERO, w[6]); 
  VVV1P0_1(w[3], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[7]); 
  FFV1_1(w[5], w[6], pars->GC_2, pars->ZERO, pars->ZERO, w[8]); 
  FFV1_2(w[1], w[6], pars->GC_2, pars->ZERO, pars->ZERO, w[9]); 
  FFV2_4_3(w[0], w[2], pars->GC_50, pars->GC_59, pars->mdl_MZ, pars->mdl_WZ,
      w[10]);
  FFV2_5_1(w[5], w[10], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[11]);
  FFV2_5_2(w[1], w[10], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[12]);
  FFV1_1(w[5], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[13]); 
  FFV1_2(w[1], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[14]); 
  FFV1_2(w[1], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[15]); 
  FFV1_1(w[5], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[16]); 
  FFV1_1(w[5], w[6], pars->GC_1, pars->ZERO, pars->ZERO, w[17]); 
  FFV1_2(w[1], w[6], pars->GC_1, pars->ZERO, pars->ZERO, w[18]); 
  FFV2_3_1(w[5], w[10], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[19]);
  FFV2_3_2(w[1], w[10], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[20]);
  oxxxxx(p[perm[1]], mME[1], hel[1], -1, w[21]); 
  ixxxxx(p[perm[5]], mME[5], hel[5], -1, w[22]); 
  FFV1_1(w[21], w[6], pars->GC_2, pars->ZERO, pars->ZERO, w[23]); 
  FFV1_2(w[22], w[6], pars->GC_2, pars->ZERO, pars->ZERO, w[24]); 
  FFV2_5_1(w[21], w[10], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[25]);
  FFV2_5_2(w[22], w[10], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[26]);
  FFV1_1(w[21], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[27]); 
  FFV1_2(w[22], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[28]); 
  FFV1_2(w[22], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[29]); 
  FFV1_1(w[21], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[30]); 
  FFV1_1(w[21], w[6], pars->GC_1, pars->ZERO, pars->ZERO, w[31]); 
  FFV1_2(w[22], w[6], pars->GC_1, pars->ZERO, pars->ZERO, w[32]); 
  FFV2_3_1(w[21], w[10], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[33]);
  FFV2_3_2(w[22], w[10], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[34]);
  oxxxxx(p[perm[0]], mME[0], hel[0], -1, w[35]); 
  ixxxxx(p[perm[2]], mME[2], hel[2], -1, w[36]); 
  FFV1P0_3(w[36], w[35], pars->GC_3, pars->ZERO, pars->ZERO, w[37]); 
  FFV1_1(w[5], w[37], pars->GC_2, pars->ZERO, pars->ZERO, w[38]); 
  FFV1_2(w[1], w[37], pars->GC_2, pars->ZERO, pars->ZERO, w[39]); 
  FFV2_4_3(w[36], w[35], pars->GC_50, pars->GC_59, pars->mdl_MZ, pars->mdl_WZ,
      w[40]);
  FFV2_5_1(w[5], w[40], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[41]);
  FFV2_5_2(w[1], w[40], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[42]);
  FFV1_1(w[5], w[37], pars->GC_1, pars->ZERO, pars->ZERO, w[43]); 
  FFV1_2(w[1], w[37], pars->GC_1, pars->ZERO, pars->ZERO, w[44]); 
  FFV2_3_1(w[5], w[40], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[45]);
  FFV2_3_2(w[1], w[40], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[46]);
  FFV1_1(w[21], w[37], pars->GC_2, pars->ZERO, pars->ZERO, w[47]); 
  FFV1_2(w[22], w[37], pars->GC_2, pars->ZERO, pars->ZERO, w[48]); 
  FFV2_5_1(w[21], w[40], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[49]);
  FFV2_5_2(w[22], w[40], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[50]);
  FFV1_1(w[21], w[37], pars->GC_1, pars->ZERO, pars->ZERO, w[51]); 
  FFV1_2(w[22], w[37], pars->GC_1, pars->ZERO, pars->ZERO, w[52]); 
  FFV2_3_1(w[21], w[40], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[53]);
  FFV2_3_2(w[22], w[40], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[54]);
  FFV2_3(w[0], w[2], pars->GC_100, pars->mdl_MW, pars->mdl_WW, w[55]); 
  FFV2_1(w[5], w[55], pars->GC_100, pars->ZERO, pars->ZERO, w[56]); 
  FFV2_2(w[1], w[55], pars->GC_100, pars->ZERO, pars->ZERO, w[57]); 
  FFV2_1(w[21], w[55], pars->GC_100, pars->ZERO, pars->ZERO, w[58]); 
  FFV2_2(w[22], w[55], pars->GC_100, pars->ZERO, pars->ZERO, w[59]); 
  FFV2_3(w[0], w[2], pars->GC_62, pars->mdl_MZ, pars->mdl_WZ, w[60]); 
  FFV2_5_1(w[5], w[60], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[61]);
  FFV2_5_2(w[1], w[60], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[62]);
  FFV2_3_1(w[5], w[60], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[63]);
  FFV2_3_2(w[1], w[60], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[64]);
  FFV2_5_1(w[21], w[60], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[65]);
  FFV2_5_2(w[22], w[60], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[66]);
  FFV2_3_1(w[21], w[60], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[67]);
  FFV2_3_2(w[22], w[60], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[68]);
  FFV2_3(w[36], w[35], pars->GC_100, pars->mdl_MW, pars->mdl_WW, w[69]); 
  FFV2_1(w[5], w[69], pars->GC_100, pars->ZERO, pars->ZERO, w[70]); 
  FFV2_2(w[1], w[69], pars->GC_100, pars->ZERO, pars->ZERO, w[71]); 
  FFV2_1(w[21], w[69], pars->GC_100, pars->ZERO, pars->ZERO, w[72]); 
  FFV2_2(w[22], w[69], pars->GC_100, pars->ZERO, pars->ZERO, w[73]); 
  FFV2_3(w[36], w[35], pars->GC_62, pars->mdl_MZ, pars->mdl_WZ, w[74]); 
  FFV2_5_1(w[5], w[74], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[75]);
  FFV2_5_2(w[1], w[74], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[76]);
  FFV2_3_1(w[5], w[74], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[77]);
  FFV2_3_2(w[1], w[74], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[78]);
  FFV2_5_1(w[21], w[74], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[79]);
  FFV2_5_2(w[22], w[74], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[80]);
  FFV2_3_1(w[21], w[74], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[81]);
  FFV2_3_2(w[22], w[74], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[82]);

  // Calculate all amplitudes
  // Amplitude(s) for diagram number 0
  FFV1_0(w[1], w[8], w[7], pars->GC_11, amp[0]); 
  FFV1_0(w[9], w[5], w[7], pars->GC_11, amp[1]); 
  FFV1_0(w[1], w[11], w[7], pars->GC_11, amp[2]); 
  FFV1_0(w[12], w[5], w[7], pars->GC_11, amp[3]); 
  FFV1_0(w[9], w[13], w[4], pars->GC_11, amp[4]); 
  FFV1_0(w[12], w[13], w[4], pars->GC_11, amp[5]); 
  FFV1_0(w[14], w[13], w[6], pars->GC_2, amp[6]); 
  FFV2_5_0(w[14], w[13], w[10], pars->GC_51, pars->GC_58, amp[7]); 
  FFV1_0(w[15], w[8], w[4], pars->GC_11, amp[8]); 
  FFV1_0(w[15], w[11], w[4], pars->GC_11, amp[9]); 
  FFV1_0(w[15], w[16], w[6], pars->GC_2, amp[10]); 
  FFV2_5_0(w[15], w[16], w[10], pars->GC_51, pars->GC_58, amp[11]); 
  FFV1_0(w[9], w[16], w[3], pars->GC_11, amp[12]); 
  FFV1_0(w[12], w[16], w[3], pars->GC_11, amp[13]); 
  FFV1_0(w[14], w[8], w[3], pars->GC_11, amp[14]); 
  FFV1_0(w[14], w[11], w[3], pars->GC_11, amp[15]); 
  FFV1_0(w[1], w[17], w[7], pars->GC_11, amp[16]); 
  FFV1_0(w[18], w[5], w[7], pars->GC_11, amp[17]); 
  FFV1_0(w[1], w[19], w[7], pars->GC_11, amp[18]); 
  FFV1_0(w[20], w[5], w[7], pars->GC_11, amp[19]); 
  FFV1_0(w[18], w[13], w[4], pars->GC_11, amp[20]); 
  FFV1_0(w[20], w[13], w[4], pars->GC_11, amp[21]); 
  FFV1_0(w[14], w[13], w[6], pars->GC_1, amp[22]); 
  FFV2_3_0(w[14], w[13], w[10], pars->GC_50, pars->GC_58, amp[23]); 
  FFV1_0(w[15], w[17], w[4], pars->GC_11, amp[24]); 
  FFV1_0(w[15], w[19], w[4], pars->GC_11, amp[25]); 
  FFV1_0(w[15], w[16], w[6], pars->GC_1, amp[26]); 
  FFV2_3_0(w[15], w[16], w[10], pars->GC_50, pars->GC_58, amp[27]); 
  FFV1_0(w[18], w[16], w[3], pars->GC_11, amp[28]); 
  FFV1_0(w[20], w[16], w[3], pars->GC_11, amp[29]); 
  FFV1_0(w[14], w[17], w[3], pars->GC_11, amp[30]); 
  FFV1_0(w[14], w[19], w[3], pars->GC_11, amp[31]); 
  FFV1_0(w[22], w[23], w[7], pars->GC_11, amp[32]); 
  FFV1_0(w[24], w[21], w[7], pars->GC_11, amp[33]); 
  FFV1_0(w[22], w[25], w[7], pars->GC_11, amp[34]); 
  FFV1_0(w[26], w[21], w[7], pars->GC_11, amp[35]); 
  FFV1_0(w[24], w[27], w[4], pars->GC_11, amp[36]); 
  FFV1_0(w[26], w[27], w[4], pars->GC_11, amp[37]); 
  FFV1_0(w[28], w[27], w[6], pars->GC_2, amp[38]); 
  FFV2_5_0(w[28], w[27], w[10], pars->GC_51, pars->GC_58, amp[39]); 
  FFV1_0(w[29], w[23], w[4], pars->GC_11, amp[40]); 
  FFV1_0(w[29], w[25], w[4], pars->GC_11, amp[41]); 
  FFV1_0(w[29], w[30], w[6], pars->GC_2, amp[42]); 
  FFV2_5_0(w[29], w[30], w[10], pars->GC_51, pars->GC_58, amp[43]); 
  FFV1_0(w[24], w[30], w[3], pars->GC_11, amp[44]); 
  FFV1_0(w[26], w[30], w[3], pars->GC_11, amp[45]); 
  FFV1_0(w[28], w[23], w[3], pars->GC_11, amp[46]); 
  FFV1_0(w[28], w[25], w[3], pars->GC_11, amp[47]); 
  FFV1_0(w[22], w[31], w[7], pars->GC_11, amp[48]); 
  FFV1_0(w[32], w[21], w[7], pars->GC_11, amp[49]); 
  FFV1_0(w[22], w[33], w[7], pars->GC_11, amp[50]); 
  FFV1_0(w[34], w[21], w[7], pars->GC_11, amp[51]); 
  FFV1_0(w[32], w[27], w[4], pars->GC_11, amp[52]); 
  FFV1_0(w[34], w[27], w[4], pars->GC_11, amp[53]); 
  FFV1_0(w[28], w[27], w[6], pars->GC_1, amp[54]); 
  FFV2_3_0(w[28], w[27], w[10], pars->GC_50, pars->GC_58, amp[55]); 
  FFV1_0(w[29], w[31], w[4], pars->GC_11, amp[56]); 
  FFV1_0(w[29], w[33], w[4], pars->GC_11, amp[57]); 
  FFV1_0(w[29], w[30], w[6], pars->GC_1, amp[58]); 
  FFV2_3_0(w[29], w[30], w[10], pars->GC_50, pars->GC_58, amp[59]); 
  FFV1_0(w[32], w[30], w[3], pars->GC_11, amp[60]); 
  FFV1_0(w[34], w[30], w[3], pars->GC_11, amp[61]); 
  FFV1_0(w[28], w[31], w[3], pars->GC_11, amp[62]); 
  FFV1_0(w[28], w[33], w[3], pars->GC_11, amp[63]); 
  FFV1_0(w[1], w[38], w[7], pars->GC_11, amp[64]); 
  FFV1_0(w[39], w[5], w[7], pars->GC_11, amp[65]); 
  FFV1_0(w[1], w[41], w[7], pars->GC_11, amp[66]); 
  FFV1_0(w[42], w[5], w[7], pars->GC_11, amp[67]); 
  FFV1_0(w[39], w[13], w[4], pars->GC_11, amp[68]); 
  FFV1_0(w[42], w[13], w[4], pars->GC_11, amp[69]); 
  FFV1_0(w[14], w[13], w[37], pars->GC_2, amp[70]); 
  FFV2_5_0(w[14], w[13], w[40], pars->GC_51, pars->GC_58, amp[71]); 
  FFV1_0(w[15], w[38], w[4], pars->GC_11, amp[72]); 
  FFV1_0(w[15], w[41], w[4], pars->GC_11, amp[73]); 
  FFV1_0(w[15], w[16], w[37], pars->GC_2, amp[74]); 
  FFV2_5_0(w[15], w[16], w[40], pars->GC_51, pars->GC_58, amp[75]); 
  FFV1_0(w[39], w[16], w[3], pars->GC_11, amp[76]); 
  FFV1_0(w[42], w[16], w[3], pars->GC_11, amp[77]); 
  FFV1_0(w[14], w[38], w[3], pars->GC_11, amp[78]); 
  FFV1_0(w[14], w[41], w[3], pars->GC_11, amp[79]); 
  FFV1_0(w[1], w[43], w[7], pars->GC_11, amp[80]); 
  FFV1_0(w[44], w[5], w[7], pars->GC_11, amp[81]); 
  FFV1_0(w[1], w[45], w[7], pars->GC_11, amp[82]); 
  FFV1_0(w[46], w[5], w[7], pars->GC_11, amp[83]); 
  FFV1_0(w[44], w[13], w[4], pars->GC_11, amp[84]); 
  FFV1_0(w[46], w[13], w[4], pars->GC_11, amp[85]); 
  FFV1_0(w[14], w[13], w[37], pars->GC_1, amp[86]); 
  FFV2_3_0(w[14], w[13], w[40], pars->GC_50, pars->GC_58, amp[87]); 
  FFV1_0(w[15], w[43], w[4], pars->GC_11, amp[88]); 
  FFV1_0(w[15], w[45], w[4], pars->GC_11, amp[89]); 
  FFV1_0(w[15], w[16], w[37], pars->GC_1, amp[90]); 
  FFV2_3_0(w[15], w[16], w[40], pars->GC_50, pars->GC_58, amp[91]); 
  FFV1_0(w[44], w[16], w[3], pars->GC_11, amp[92]); 
  FFV1_0(w[46], w[16], w[3], pars->GC_11, amp[93]); 
  FFV1_0(w[14], w[43], w[3], pars->GC_11, amp[94]); 
  FFV1_0(w[14], w[45], w[3], pars->GC_11, amp[95]); 
  FFV1_0(w[22], w[47], w[7], pars->GC_11, amp[96]); 
  FFV1_0(w[48], w[21], w[7], pars->GC_11, amp[97]); 
  FFV1_0(w[22], w[49], w[7], pars->GC_11, amp[98]); 
  FFV1_0(w[50], w[21], w[7], pars->GC_11, amp[99]); 
  FFV1_0(w[48], w[27], w[4], pars->GC_11, amp[100]); 
  FFV1_0(w[50], w[27], w[4], pars->GC_11, amp[101]); 
  FFV1_0(w[28], w[27], w[37], pars->GC_2, amp[102]); 
  FFV2_5_0(w[28], w[27], w[40], pars->GC_51, pars->GC_58, amp[103]); 
  FFV1_0(w[29], w[47], w[4], pars->GC_11, amp[104]); 
  FFV1_0(w[29], w[49], w[4], pars->GC_11, amp[105]); 
  FFV1_0(w[29], w[30], w[37], pars->GC_2, amp[106]); 
  FFV2_5_0(w[29], w[30], w[40], pars->GC_51, pars->GC_58, amp[107]); 
  FFV1_0(w[48], w[30], w[3], pars->GC_11, amp[108]); 
  FFV1_0(w[50], w[30], w[3], pars->GC_11, amp[109]); 
  FFV1_0(w[28], w[47], w[3], pars->GC_11, amp[110]); 
  FFV1_0(w[28], w[49], w[3], pars->GC_11, amp[111]); 
  FFV1_0(w[22], w[51], w[7], pars->GC_11, amp[112]); 
  FFV1_0(w[52], w[21], w[7], pars->GC_11, amp[113]); 
  FFV1_0(w[22], w[53], w[7], pars->GC_11, amp[114]); 
  FFV1_0(w[54], w[21], w[7], pars->GC_11, amp[115]); 
  FFV1_0(w[52], w[27], w[4], pars->GC_11, amp[116]); 
  FFV1_0(w[54], w[27], w[4], pars->GC_11, amp[117]); 
  FFV1_0(w[28], w[27], w[37], pars->GC_1, amp[118]); 
  FFV2_3_0(w[28], w[27], w[40], pars->GC_50, pars->GC_58, amp[119]); 
  FFV1_0(w[29], w[51], w[4], pars->GC_11, amp[120]); 
  FFV1_0(w[29], w[53], w[4], pars->GC_11, amp[121]); 
  FFV1_0(w[29], w[30], w[37], pars->GC_1, amp[122]); 
  FFV2_3_0(w[29], w[30], w[40], pars->GC_50, pars->GC_58, amp[123]); 
  FFV1_0(w[52], w[30], w[3], pars->GC_11, amp[124]); 
  FFV1_0(w[54], w[30], w[3], pars->GC_11, amp[125]); 
  FFV1_0(w[28], w[51], w[3], pars->GC_11, amp[126]); 
  FFV1_0(w[28], w[53], w[3], pars->GC_11, amp[127]); 
  FFV1_0(w[1], w[56], w[7], pars->GC_11, amp[128]); 
  FFV1_0(w[57], w[5], w[7], pars->GC_11, amp[129]); 
  FFV1_0(w[57], w[13], w[4], pars->GC_11, amp[130]); 
  FFV2_0(w[14], w[13], w[55], pars->GC_100, amp[131]); 
  FFV1_0(w[15], w[56], w[4], pars->GC_11, amp[132]); 
  FFV2_0(w[15], w[16], w[55], pars->GC_100, amp[133]); 
  FFV1_0(w[57], w[16], w[3], pars->GC_11, amp[134]); 
  FFV1_0(w[14], w[56], w[3], pars->GC_11, amp[135]); 
  FFV1_0(w[22], w[58], w[7], pars->GC_11, amp[136]); 
  FFV1_0(w[59], w[21], w[7], pars->GC_11, amp[137]); 
  FFV1_0(w[59], w[27], w[4], pars->GC_11, amp[138]); 
  FFV2_0(w[28], w[27], w[55], pars->GC_100, amp[139]); 
  FFV1_0(w[29], w[58], w[4], pars->GC_11, amp[140]); 
  FFV2_0(w[29], w[30], w[55], pars->GC_100, amp[141]); 
  FFV1_0(w[59], w[30], w[3], pars->GC_11, amp[142]); 
  FFV1_0(w[28], w[58], w[3], pars->GC_11, amp[143]); 
  FFV1_0(w[1], w[61], w[7], pars->GC_11, amp[144]); 
  FFV1_0(w[62], w[5], w[7], pars->GC_11, amp[145]); 
  FFV1_0(w[62], w[13], w[4], pars->GC_11, amp[146]); 
  FFV2_5_0(w[14], w[13], w[60], pars->GC_51, pars->GC_58, amp[147]); 
  FFV1_0(w[15], w[61], w[4], pars->GC_11, amp[148]); 
  FFV2_5_0(w[15], w[16], w[60], pars->GC_51, pars->GC_58, amp[149]); 
  FFV1_0(w[62], w[16], w[3], pars->GC_11, amp[150]); 
  FFV1_0(w[14], w[61], w[3], pars->GC_11, amp[151]); 
  FFV1_0(w[1], w[63], w[7], pars->GC_11, amp[152]); 
  FFV1_0(w[64], w[5], w[7], pars->GC_11, amp[153]); 
  FFV1_0(w[64], w[13], w[4], pars->GC_11, amp[154]); 
  FFV2_3_0(w[14], w[13], w[60], pars->GC_50, pars->GC_58, amp[155]); 
  FFV1_0(w[15], w[63], w[4], pars->GC_11, amp[156]); 
  FFV2_3_0(w[15], w[16], w[60], pars->GC_50, pars->GC_58, amp[157]); 
  FFV1_0(w[64], w[16], w[3], pars->GC_11, amp[158]); 
  FFV1_0(w[14], w[63], w[3], pars->GC_11, amp[159]); 
  FFV1_0(w[22], w[65], w[7], pars->GC_11, amp[160]); 
  FFV1_0(w[66], w[21], w[7], pars->GC_11, amp[161]); 
  FFV1_0(w[66], w[27], w[4], pars->GC_11, amp[162]); 
  FFV2_5_0(w[28], w[27], w[60], pars->GC_51, pars->GC_58, amp[163]); 
  FFV1_0(w[29], w[65], w[4], pars->GC_11, amp[164]); 
  FFV2_5_0(w[29], w[30], w[60], pars->GC_51, pars->GC_58, amp[165]); 
  FFV1_0(w[66], w[30], w[3], pars->GC_11, amp[166]); 
  FFV1_0(w[28], w[65], w[3], pars->GC_11, amp[167]); 
  FFV1_0(w[22], w[67], w[7], pars->GC_11, amp[168]); 
  FFV1_0(w[68], w[21], w[7], pars->GC_11, amp[169]); 
  FFV1_0(w[68], w[27], w[4], pars->GC_11, amp[170]); 
  FFV2_3_0(w[28], w[27], w[60], pars->GC_50, pars->GC_58, amp[171]); 
  FFV1_0(w[29], w[67], w[4], pars->GC_11, amp[172]); 
  FFV2_3_0(w[29], w[30], w[60], pars->GC_50, pars->GC_58, amp[173]); 
  FFV1_0(w[68], w[30], w[3], pars->GC_11, amp[174]); 
  FFV1_0(w[28], w[67], w[3], pars->GC_11, amp[175]); 
  FFV1_0(w[1], w[70], w[7], pars->GC_11, amp[176]); 
  FFV1_0(w[71], w[5], w[7], pars->GC_11, amp[177]); 
  FFV1_0(w[71], w[13], w[4], pars->GC_11, amp[178]); 
  FFV2_0(w[14], w[13], w[69], pars->GC_100, amp[179]); 
  FFV1_0(w[15], w[70], w[4], pars->GC_11, amp[180]); 
  FFV2_0(w[15], w[16], w[69], pars->GC_100, amp[181]); 
  FFV1_0(w[71], w[16], w[3], pars->GC_11, amp[182]); 
  FFV1_0(w[14], w[70], w[3], pars->GC_11, amp[183]); 
  FFV1_0(w[22], w[72], w[7], pars->GC_11, amp[184]); 
  FFV1_0(w[73], w[21], w[7], pars->GC_11, amp[185]); 
  FFV1_0(w[73], w[27], w[4], pars->GC_11, amp[186]); 
  FFV2_0(w[28], w[27], w[69], pars->GC_100, amp[187]); 
  FFV1_0(w[29], w[72], w[4], pars->GC_11, amp[188]); 
  FFV2_0(w[29], w[30], w[69], pars->GC_100, amp[189]); 
  FFV1_0(w[73], w[30], w[3], pars->GC_11, amp[190]); 
  FFV1_0(w[28], w[72], w[3], pars->GC_11, amp[191]); 
  FFV1_0(w[1], w[75], w[7], pars->GC_11, amp[192]); 
  FFV1_0(w[76], w[5], w[7], pars->GC_11, amp[193]); 
  FFV1_0(w[76], w[13], w[4], pars->GC_11, amp[194]); 
  FFV2_5_0(w[14], w[13], w[74], pars->GC_51, pars->GC_58, amp[195]); 
  FFV1_0(w[15], w[75], w[4], pars->GC_11, amp[196]); 
  FFV2_5_0(w[15], w[16], w[74], pars->GC_51, pars->GC_58, amp[197]); 
  FFV1_0(w[76], w[16], w[3], pars->GC_11, amp[198]); 
  FFV1_0(w[14], w[75], w[3], pars->GC_11, amp[199]); 
  FFV1_0(w[1], w[77], w[7], pars->GC_11, amp[200]); 
  FFV1_0(w[78], w[5], w[7], pars->GC_11, amp[201]); 
  FFV1_0(w[78], w[13], w[4], pars->GC_11, amp[202]); 
  FFV2_3_0(w[14], w[13], w[74], pars->GC_50, pars->GC_58, amp[203]); 
  FFV1_0(w[15], w[77], w[4], pars->GC_11, amp[204]); 
  FFV2_3_0(w[15], w[16], w[74], pars->GC_50, pars->GC_58, amp[205]); 
  FFV1_0(w[78], w[16], w[3], pars->GC_11, amp[206]); 
  FFV1_0(w[14], w[77], w[3], pars->GC_11, amp[207]); 
  FFV1_0(w[22], w[79], w[7], pars->GC_11, amp[208]); 
  FFV1_0(w[80], w[21], w[7], pars->GC_11, amp[209]); 
  FFV1_0(w[80], w[27], w[4], pars->GC_11, amp[210]); 
  FFV2_5_0(w[28], w[27], w[74], pars->GC_51, pars->GC_58, amp[211]); 
  FFV1_0(w[29], w[79], w[4], pars->GC_11, amp[212]); 
  FFV2_5_0(w[29], w[30], w[74], pars->GC_51, pars->GC_58, amp[213]); 
  FFV1_0(w[80], w[30], w[3], pars->GC_11, amp[214]); 
  FFV1_0(w[28], w[79], w[3], pars->GC_11, amp[215]); 
  FFV1_0(w[22], w[81], w[7], pars->GC_11, amp[216]); 
  FFV1_0(w[82], w[21], w[7], pars->GC_11, amp[217]); 
  FFV1_0(w[82], w[27], w[4], pars->GC_11, amp[218]); 
  FFV2_3_0(w[28], w[27], w[74], pars->GC_50, pars->GC_58, amp[219]); 
  FFV1_0(w[29], w[81], w[4], pars->GC_11, amp[220]); 
  FFV2_3_0(w[29], w[30], w[74], pars->GC_50, pars->GC_58, amp[221]); 
  FFV1_0(w[82], w[30], w[3], pars->GC_11, amp[222]); 
  FFV1_0(w[28], w[81], w[3], pars->GC_11, amp[223]); 


}
double PY8MEs_R3_P35_sm_lq_lggq::matrix_3_emu_emggu() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 16;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {3, 3}; 
  static const double cf[ncolor][ncolor] = {{16, -2}, {-2, 16}}; 

  // Calculate color flows
  jamp[0] = +Complex<double> (0, 1) * amp[0] + Complex<double> (0, 1) * amp[1]
      + Complex<double> (0, 1) * amp[2] + Complex<double> (0, 1) * amp[3] -
      amp[4] - amp[5] - amp[6] - amp[7] - amp[14] - amp[15];
  jamp[1] = -Complex<double> (0, 1) * amp[0] - Complex<double> (0, 1) * amp[1]
      - Complex<double> (0, 1) * amp[2] - Complex<double> (0, 1) * amp[3] -
      amp[8] - amp[9] - amp[10] - amp[11] - amp[12] - amp[13];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[0][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P35_sm_lq_lggq::matrix_3_emd_emggd() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 16;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {3, 3}; 
  static const double cf[ncolor][ncolor] = {{16, -2}, {-2, 16}}; 

  // Calculate color flows
  jamp[0] = +Complex<double> (0, 1) * amp[16] + Complex<double> (0, 1) *
      amp[17] + Complex<double> (0, 1) * amp[18] + Complex<double> (0, 1) *
      amp[19] - amp[20] - amp[21] - amp[22] - amp[23] - amp[30] - amp[31];
  jamp[1] = -Complex<double> (0, 1) * amp[16] - Complex<double> (0, 1) *
      amp[17] - Complex<double> (0, 1) * amp[18] - Complex<double> (0, 1) *
      amp[19] - amp[24] - amp[25] - amp[26] - amp[27] - amp[28] - amp[29];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[1][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P35_sm_lq_lggq::matrix_3_emux_emggux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 16;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {3, 3}; 
  static const double cf[ncolor][ncolor] = {{16, -2}, {-2, 16}}; 

  // Calculate color flows
  jamp[0] = -Complex<double> (0, 1) * amp[32] - Complex<double> (0, 1) *
      amp[33] - Complex<double> (0, 1) * amp[34] - Complex<double> (0, 1) *
      amp[35] + amp[36] + amp[37] + amp[38] + amp[39] + amp[46] + amp[47];
  jamp[1] = +Complex<double> (0, 1) * amp[32] + Complex<double> (0, 1) *
      amp[33] + Complex<double> (0, 1) * amp[34] + Complex<double> (0, 1) *
      amp[35] + amp[40] + amp[41] + amp[42] + amp[43] + amp[44] + amp[45];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[2][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P35_sm_lq_lggq::matrix_3_emdx_emggdx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 16;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {3, 3}; 
  static const double cf[ncolor][ncolor] = {{16, -2}, {-2, 16}}; 

  // Calculate color flows
  jamp[0] = -Complex<double> (0, 1) * amp[48] - Complex<double> (0, 1) *
      amp[49] - Complex<double> (0, 1) * amp[50] - Complex<double> (0, 1) *
      amp[51] + amp[52] + amp[53] + amp[54] + amp[55] + amp[62] + amp[63];
  jamp[1] = +Complex<double> (0, 1) * amp[48] + Complex<double> (0, 1) *
      amp[49] + Complex<double> (0, 1) * amp[50] + Complex<double> (0, 1) *
      amp[51] + amp[56] + amp[57] + amp[58] + amp[59] + amp[60] + amp[61];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[3][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P35_sm_lq_lggq::matrix_3_epu_epggu() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 16;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {3, 3}; 
  static const double cf[ncolor][ncolor] = {{16, -2}, {-2, 16}}; 

  // Calculate color flows
  jamp[0] = -Complex<double> (0, 1) * amp[64] - Complex<double> (0, 1) *
      amp[65] - Complex<double> (0, 1) * amp[66] - Complex<double> (0, 1) *
      amp[67] + amp[68] + amp[69] + amp[70] + amp[71] + amp[78] + amp[79];
  jamp[1] = +Complex<double> (0, 1) * amp[64] + Complex<double> (0, 1) *
      amp[65] + Complex<double> (0, 1) * amp[66] + Complex<double> (0, 1) *
      amp[67] + amp[72] + amp[73] + amp[74] + amp[75] + amp[76] + amp[77];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[4][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P35_sm_lq_lggq::matrix_3_epd_epggd() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 16;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {3, 3}; 
  static const double cf[ncolor][ncolor] = {{16, -2}, {-2, 16}}; 

  // Calculate color flows
  jamp[0] = -Complex<double> (0, 1) * amp[80] - Complex<double> (0, 1) *
      amp[81] - Complex<double> (0, 1) * amp[82] - Complex<double> (0, 1) *
      amp[83] + amp[84] + amp[85] + amp[86] + amp[87] + amp[94] + amp[95];
  jamp[1] = +Complex<double> (0, 1) * amp[80] + Complex<double> (0, 1) *
      amp[81] + Complex<double> (0, 1) * amp[82] + Complex<double> (0, 1) *
      amp[83] + amp[88] + amp[89] + amp[90] + amp[91] + amp[92] + amp[93];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[5][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P35_sm_lq_lggq::matrix_3_epux_epggux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 16;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {3, 3}; 
  static const double cf[ncolor][ncolor] = {{16, -2}, {-2, 16}}; 

  // Calculate color flows
  jamp[0] = +Complex<double> (0, 1) * amp[96] + Complex<double> (0, 1) *
      amp[97] + Complex<double> (0, 1) * amp[98] + Complex<double> (0, 1) *
      amp[99] - amp[100] - amp[101] - amp[102] - amp[103] - amp[110] - amp[111];
  jamp[1] = -Complex<double> (0, 1) * amp[96] - Complex<double> (0, 1) *
      amp[97] - Complex<double> (0, 1) * amp[98] - Complex<double> (0, 1) *
      amp[99] - amp[104] - amp[105] - amp[106] - amp[107] - amp[108] - amp[109];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[6][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P35_sm_lq_lggq::matrix_3_epdx_epggdx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 16;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {3, 3}; 
  static const double cf[ncolor][ncolor] = {{16, -2}, {-2, 16}}; 

  // Calculate color flows
  jamp[0] = +Complex<double> (0, 1) * amp[112] + Complex<double> (0, 1) *
      amp[113] + Complex<double> (0, 1) * amp[114] + Complex<double> (0, 1) *
      amp[115] - amp[116] - amp[117] - amp[118] - amp[119] - amp[126] -
      amp[127];
  jamp[1] = -Complex<double> (0, 1) * amp[112] - Complex<double> (0, 1) *
      amp[113] - Complex<double> (0, 1) * amp[114] - Complex<double> (0, 1) *
      amp[115] - amp[120] - amp[121] - amp[122] - amp[123] - amp[124] -
      amp[125];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[7][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P35_sm_lq_lggq::matrix_3_emu_veggd() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 8;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {3, 3}; 
  static const double cf[ncolor][ncolor] = {{16, -2}, {-2, 16}}; 

  // Calculate color flows
  jamp[0] = +Complex<double> (0, 1) * amp[128] + Complex<double> (0, 1) *
      amp[129] - amp[130] - amp[131] - amp[135];
  jamp[1] = -Complex<double> (0, 1) * amp[128] - Complex<double> (0, 1) *
      amp[129] - amp[132] - amp[133] - amp[134];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[8][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P35_sm_lq_lggq::matrix_3_emdx_veggux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 8;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {3, 3}; 
  static const double cf[ncolor][ncolor] = {{16, -2}, {-2, 16}}; 

  // Calculate color flows
  jamp[0] = -Complex<double> (0, 1) * amp[136] - Complex<double> (0, 1) *
      amp[137] + amp[138] + amp[139] + amp[143];
  jamp[1] = +Complex<double> (0, 1) * amp[136] + Complex<double> (0, 1) *
      amp[137] + amp[140] + amp[141] + amp[142];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[9][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P35_sm_lq_lggq::matrix_3_veu_veggu() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 8;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {3, 3}; 
  static const double cf[ncolor][ncolor] = {{16, -2}, {-2, 16}}; 

  // Calculate color flows
  jamp[0] = +Complex<double> (0, 1) * amp[144] + Complex<double> (0, 1) *
      amp[145] - amp[146] - amp[147] - amp[151];
  jamp[1] = -Complex<double> (0, 1) * amp[144] - Complex<double> (0, 1) *
      amp[145] - amp[148] - amp[149] - amp[150];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[10][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P35_sm_lq_lggq::matrix_3_ved_veggd() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 8;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {3, 3}; 
  static const double cf[ncolor][ncolor] = {{16, -2}, {-2, 16}}; 

  // Calculate color flows
  jamp[0] = +Complex<double> (0, 1) * amp[152] + Complex<double> (0, 1) *
      amp[153] - amp[154] - amp[155] - amp[159];
  jamp[1] = -Complex<double> (0, 1) * amp[152] - Complex<double> (0, 1) *
      amp[153] - amp[156] - amp[157] - amp[158];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[11][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P35_sm_lq_lggq::matrix_3_veux_veggux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 8;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {3, 3}; 
  static const double cf[ncolor][ncolor] = {{16, -2}, {-2, 16}}; 

  // Calculate color flows
  jamp[0] = -Complex<double> (0, 1) * amp[160] - Complex<double> (0, 1) *
      amp[161] + amp[162] + amp[163] + amp[167];
  jamp[1] = +Complex<double> (0, 1) * amp[160] + Complex<double> (0, 1) *
      amp[161] + amp[164] + amp[165] + amp[166];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[12][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P35_sm_lq_lggq::matrix_3_vedx_veggdx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 8;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {3, 3}; 
  static const double cf[ncolor][ncolor] = {{16, -2}, {-2, 16}}; 

  // Calculate color flows
  jamp[0] = -Complex<double> (0, 1) * amp[168] - Complex<double> (0, 1) *
      amp[169] + amp[170] + amp[171] + amp[175];
  jamp[1] = +Complex<double> (0, 1) * amp[168] + Complex<double> (0, 1) *
      amp[169] + amp[172] + amp[173] + amp[174];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[13][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P35_sm_lq_lggq::matrix_3_epd_vexggu() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 8;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {3, 3}; 
  static const double cf[ncolor][ncolor] = {{16, -2}, {-2, 16}}; 

  // Calculate color flows
  jamp[0] = -Complex<double> (0, 1) * amp[176] - Complex<double> (0, 1) *
      amp[177] + amp[178] + amp[179] + amp[183];
  jamp[1] = +Complex<double> (0, 1) * amp[176] + Complex<double> (0, 1) *
      amp[177] + amp[180] + amp[181] + amp[182];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[14][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P35_sm_lq_lggq::matrix_3_epux_vexggdx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 8;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {3, 3}; 
  static const double cf[ncolor][ncolor] = {{16, -2}, {-2, 16}}; 

  // Calculate color flows
  jamp[0] = +Complex<double> (0, 1) * amp[184] + Complex<double> (0, 1) *
      amp[185] - amp[186] - amp[187] - amp[191];
  jamp[1] = -Complex<double> (0, 1) * amp[184] - Complex<double> (0, 1) *
      amp[185] - amp[188] - amp[189] - amp[190];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[15][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P35_sm_lq_lggq::matrix_3_vexu_vexggu() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 8;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {3, 3}; 
  static const double cf[ncolor][ncolor] = {{16, -2}, {-2, 16}}; 

  // Calculate color flows
  jamp[0] = -Complex<double> (0, 1) * amp[192] - Complex<double> (0, 1) *
      amp[193] + amp[194] + amp[195] + amp[199];
  jamp[1] = +Complex<double> (0, 1) * amp[192] + Complex<double> (0, 1) *
      amp[193] + amp[196] + amp[197] + amp[198];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[16][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P35_sm_lq_lggq::matrix_3_vexd_vexggd() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 8;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {3, 3}; 
  static const double cf[ncolor][ncolor] = {{16, -2}, {-2, 16}}; 

  // Calculate color flows
  jamp[0] = -Complex<double> (0, 1) * amp[200] - Complex<double> (0, 1) *
      amp[201] + amp[202] + amp[203] + amp[207];
  jamp[1] = +Complex<double> (0, 1) * amp[200] + Complex<double> (0, 1) *
      amp[201] + amp[204] + amp[205] + amp[206];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[17][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P35_sm_lq_lggq::matrix_3_vexux_vexggux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 8;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {3, 3}; 
  static const double cf[ncolor][ncolor] = {{16, -2}, {-2, 16}}; 

  // Calculate color flows
  jamp[0] = +Complex<double> (0, 1) * amp[208] + Complex<double> (0, 1) *
      amp[209] - amp[210] - amp[211] - amp[215];
  jamp[1] = -Complex<double> (0, 1) * amp[208] - Complex<double> (0, 1) *
      amp[209] - amp[212] - amp[213] - amp[214];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[18][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P35_sm_lq_lggq::matrix_3_vexdx_vexggdx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 8;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {3, 3}; 
  static const double cf[ncolor][ncolor] = {{16, -2}, {-2, 16}}; 

  // Calculate color flows
  jamp[0] = +Complex<double> (0, 1) * amp[216] + Complex<double> (0, 1) *
      amp[217] - amp[218] - amp[219] - amp[223];
  jamp[1] = -Complex<double> (0, 1) * amp[216] - Complex<double> (0, 1) *
      amp[217] - amp[220] - amp[221] - amp[222];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[19][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}


}  // end namespace PY8MEs_namespace

