//==========================================================================
// This file has been automatically generated for Pythia 8 by
// MadGraph5_aMC@NLO v. 2.6.0, 2017-08-16
// By the MadGraph5_aMC@NLO Development Team
// Visit launchpad.net/madgraph5 and amcatnlo.web.cern.ch
//==========================================================================

#include "PY8MEs_R4_P6_sm_lq_lgggq.h"
#include "HelAmps_sm.h"

using namespace Pythia8_sm; 

namespace PY8MEs_namespace 
{
//==========================================================================
// Class member functions for calculating the matrix elements for
// Process: e- u > e- g g g u WEIGHTED<=7 @4
// Process: e- c > e- g g g c WEIGHTED<=7 @4
// Process: mu- u > mu- g g g u WEIGHTED<=7 @4
// Process: mu- c > mu- g g g c WEIGHTED<=7 @4
// Process: e- d > e- g g g d WEIGHTED<=7 @4
// Process: e- s > e- g g g s WEIGHTED<=7 @4
// Process: mu- d > mu- g g g d WEIGHTED<=7 @4
// Process: mu- s > mu- g g g s WEIGHTED<=7 @4
// Process: e- u~ > e- g g g u~ WEIGHTED<=7 @4
// Process: e- c~ > e- g g g c~ WEIGHTED<=7 @4
// Process: mu- u~ > mu- g g g u~ WEIGHTED<=7 @4
// Process: mu- c~ > mu- g g g c~ WEIGHTED<=7 @4
// Process: e- d~ > e- g g g d~ WEIGHTED<=7 @4
// Process: e- s~ > e- g g g s~ WEIGHTED<=7 @4
// Process: mu- d~ > mu- g g g d~ WEIGHTED<=7 @4
// Process: mu- s~ > mu- g g g s~ WEIGHTED<=7 @4
// Process: e+ u > e+ g g g u WEIGHTED<=7 @4
// Process: e+ c > e+ g g g c WEIGHTED<=7 @4
// Process: mu+ u > mu+ g g g u WEIGHTED<=7 @4
// Process: mu+ c > mu+ g g g c WEIGHTED<=7 @4
// Process: e+ d > e+ g g g d WEIGHTED<=7 @4
// Process: e+ s > e+ g g g s WEIGHTED<=7 @4
// Process: mu+ d > mu+ g g g d WEIGHTED<=7 @4
// Process: mu+ s > mu+ g g g s WEIGHTED<=7 @4
// Process: e+ u~ > e+ g g g u~ WEIGHTED<=7 @4
// Process: e+ c~ > e+ g g g c~ WEIGHTED<=7 @4
// Process: mu+ u~ > mu+ g g g u~ WEIGHTED<=7 @4
// Process: mu+ c~ > mu+ g g g c~ WEIGHTED<=7 @4
// Process: e+ d~ > e+ g g g d~ WEIGHTED<=7 @4
// Process: e+ s~ > e+ g g g s~ WEIGHTED<=7 @4
// Process: mu+ d~ > mu+ g g g d~ WEIGHTED<=7 @4
// Process: mu+ s~ > mu+ g g g s~ WEIGHTED<=7 @4
// Process: e- u > ve g g g d WEIGHTED<=7 @4
// Process: e- c > ve g g g s WEIGHTED<=7 @4
// Process: mu- u > vm g g g d WEIGHTED<=7 @4
// Process: mu- c > vm g g g s WEIGHTED<=7 @4
// Process: ve d > e- g g g u WEIGHTED<=7 @4
// Process: ve s > e- g g g c WEIGHTED<=7 @4
// Process: vm d > mu- g g g u WEIGHTED<=7 @4
// Process: vm s > mu- g g g c WEIGHTED<=7 @4
// Process: e- d~ > ve g g g u~ WEIGHTED<=7 @4
// Process: e- s~ > ve g g g c~ WEIGHTED<=7 @4
// Process: mu- d~ > vm g g g u~ WEIGHTED<=7 @4
// Process: mu- s~ > vm g g g c~ WEIGHTED<=7 @4
// Process: ve u~ > e- g g g d~ WEIGHTED<=7 @4
// Process: ve c~ > e- g g g s~ WEIGHTED<=7 @4
// Process: vm u~ > mu- g g g d~ WEIGHTED<=7 @4
// Process: vm c~ > mu- g g g s~ WEIGHTED<=7 @4
// Process: ve u > ve g g g u WEIGHTED<=7 @4
// Process: ve c > ve g g g c WEIGHTED<=7 @4
// Process: vm u > vm g g g u WEIGHTED<=7 @4
// Process: vm c > vm g g g c WEIGHTED<=7 @4
// Process: vt u > vt g g g u WEIGHTED<=7 @4
// Process: vt c > vt g g g c WEIGHTED<=7 @4
// Process: ve d > ve g g g d WEIGHTED<=7 @4
// Process: ve s > ve g g g s WEIGHTED<=7 @4
// Process: vm d > vm g g g d WEIGHTED<=7 @4
// Process: vm s > vm g g g s WEIGHTED<=7 @4
// Process: vt d > vt g g g d WEIGHTED<=7 @4
// Process: vt s > vt g g g s WEIGHTED<=7 @4
// Process: ve u~ > ve g g g u~ WEIGHTED<=7 @4
// Process: ve c~ > ve g g g c~ WEIGHTED<=7 @4
// Process: vm u~ > vm g g g u~ WEIGHTED<=7 @4
// Process: vm c~ > vm g g g c~ WEIGHTED<=7 @4
// Process: vt u~ > vt g g g u~ WEIGHTED<=7 @4
// Process: vt c~ > vt g g g c~ WEIGHTED<=7 @4
// Process: ve d~ > ve g g g d~ WEIGHTED<=7 @4
// Process: ve s~ > ve g g g s~ WEIGHTED<=7 @4
// Process: vm d~ > vm g g g d~ WEIGHTED<=7 @4
// Process: vm s~ > vm g g g s~ WEIGHTED<=7 @4
// Process: vt d~ > vt g g g d~ WEIGHTED<=7 @4
// Process: vt s~ > vt g g g s~ WEIGHTED<=7 @4
// Process: e+ d > ve~ g g g u WEIGHTED<=7 @4
// Process: e+ s > ve~ g g g c WEIGHTED<=7 @4
// Process: mu+ d > vm~ g g g u WEIGHTED<=7 @4
// Process: mu+ s > vm~ g g g c WEIGHTED<=7 @4
// Process: ve~ u > e+ g g g d WEIGHTED<=7 @4
// Process: ve~ c > e+ g g g s WEIGHTED<=7 @4
// Process: vm~ u > mu+ g g g d WEIGHTED<=7 @4
// Process: vm~ c > mu+ g g g s WEIGHTED<=7 @4
// Process: e+ u~ > ve~ g g g d~ WEIGHTED<=7 @4
// Process: e+ c~ > ve~ g g g s~ WEIGHTED<=7 @4
// Process: mu+ u~ > vm~ g g g d~ WEIGHTED<=7 @4
// Process: mu+ c~ > vm~ g g g s~ WEIGHTED<=7 @4
// Process: ve~ d~ > e+ g g g u~ WEIGHTED<=7 @4
// Process: ve~ s~ > e+ g g g c~ WEIGHTED<=7 @4
// Process: vm~ d~ > mu+ g g g u~ WEIGHTED<=7 @4
// Process: vm~ s~ > mu+ g g g c~ WEIGHTED<=7 @4
// Process: ve~ u > ve~ g g g u WEIGHTED<=7 @4
// Process: ve~ c > ve~ g g g c WEIGHTED<=7 @4
// Process: vm~ u > vm~ g g g u WEIGHTED<=7 @4
// Process: vm~ c > vm~ g g g c WEIGHTED<=7 @4
// Process: vt~ u > vt~ g g g u WEIGHTED<=7 @4
// Process: vt~ c > vt~ g g g c WEIGHTED<=7 @4
// Process: ve~ d > ve~ g g g d WEIGHTED<=7 @4
// Process: ve~ s > ve~ g g g s WEIGHTED<=7 @4
// Process: vm~ d > vm~ g g g d WEIGHTED<=7 @4
// Process: vm~ s > vm~ g g g s WEIGHTED<=7 @4
// Process: vt~ d > vt~ g g g d WEIGHTED<=7 @4
// Process: vt~ s > vt~ g g g s WEIGHTED<=7 @4
// Process: ve~ u~ > ve~ g g g u~ WEIGHTED<=7 @4
// Process: ve~ c~ > ve~ g g g c~ WEIGHTED<=7 @4
// Process: vm~ u~ > vm~ g g g u~ WEIGHTED<=7 @4
// Process: vm~ c~ > vm~ g g g c~ WEIGHTED<=7 @4
// Process: vt~ u~ > vt~ g g g u~ WEIGHTED<=7 @4
// Process: vt~ c~ > vt~ g g g c~ WEIGHTED<=7 @4
// Process: ve~ d~ > ve~ g g g d~ WEIGHTED<=7 @4
// Process: ve~ s~ > ve~ g g g s~ WEIGHTED<=7 @4
// Process: vm~ d~ > vm~ g g g d~ WEIGHTED<=7 @4
// Process: vm~ s~ > vm~ g g g s~ WEIGHTED<=7 @4
// Process: vt~ d~ > vt~ g g g d~ WEIGHTED<=7 @4
// Process: vt~ s~ > vt~ g g g s~ WEIGHTED<=7 @4

// Exception class
class PY8MEs_R4_P6_sm_lq_lgggqException : public exception
{
  virtual const char * what() const throw()
  {
    return "Exception in class 'PY8MEs_R4_P6_sm_lq_lgggq'."; 
  }
}
PY8MEs_R4_P6_sm_lq_lgggq_exception; 

std::set<int> PY8MEs_R4_P6_sm_lq_lgggq::s_channel_proc = std::set<int>
    (createset<int> ());

int PY8MEs_R4_P6_sm_lq_lgggq::helicities[ncomb][nexternal] = {{-1, -1, -1, -1,
    -1, -1, -1}, {-1, -1, -1, -1, -1, -1, 1}, {-1, -1, -1, -1, -1, 1, -1}, {-1,
    -1, -1, -1, -1, 1, 1}, {-1, -1, -1, -1, 1, -1, -1}, {-1, -1, -1, -1, 1, -1,
    1}, {-1, -1, -1, -1, 1, 1, -1}, {-1, -1, -1, -1, 1, 1, 1}, {-1, -1, -1, 1,
    -1, -1, -1}, {-1, -1, -1, 1, -1, -1, 1}, {-1, -1, -1, 1, -1, 1, -1}, {-1,
    -1, -1, 1, -1, 1, 1}, {-1, -1, -1, 1, 1, -1, -1}, {-1, -1, -1, 1, 1, -1,
    1}, {-1, -1, -1, 1, 1, 1, -1}, {-1, -1, -1, 1, 1, 1, 1}, {-1, -1, 1, -1,
    -1, -1, -1}, {-1, -1, 1, -1, -1, -1, 1}, {-1, -1, 1, -1, -1, 1, -1}, {-1,
    -1, 1, -1, -1, 1, 1}, {-1, -1, 1, -1, 1, -1, -1}, {-1, -1, 1, -1, 1, -1,
    1}, {-1, -1, 1, -1, 1, 1, -1}, {-1, -1, 1, -1, 1, 1, 1}, {-1, -1, 1, 1, -1,
    -1, -1}, {-1, -1, 1, 1, -1, -1, 1}, {-1, -1, 1, 1, -1, 1, -1}, {-1, -1, 1,
    1, -1, 1, 1}, {-1, -1, 1, 1, 1, -1, -1}, {-1, -1, 1, 1, 1, -1, 1}, {-1, -1,
    1, 1, 1, 1, -1}, {-1, -1, 1, 1, 1, 1, 1}, {-1, 1, -1, -1, -1, -1, -1}, {-1,
    1, -1, -1, -1, -1, 1}, {-1, 1, -1, -1, -1, 1, -1}, {-1, 1, -1, -1, -1, 1,
    1}, {-1, 1, -1, -1, 1, -1, -1}, {-1, 1, -1, -1, 1, -1, 1}, {-1, 1, -1, -1,
    1, 1, -1}, {-1, 1, -1, -1, 1, 1, 1}, {-1, 1, -1, 1, -1, -1, -1}, {-1, 1,
    -1, 1, -1, -1, 1}, {-1, 1, -1, 1, -1, 1, -1}, {-1, 1, -1, 1, -1, 1, 1},
    {-1, 1, -1, 1, 1, -1, -1}, {-1, 1, -1, 1, 1, -1, 1}, {-1, 1, -1, 1, 1, 1,
    -1}, {-1, 1, -1, 1, 1, 1, 1}, {-1, 1, 1, -1, -1, -1, -1}, {-1, 1, 1, -1,
    -1, -1, 1}, {-1, 1, 1, -1, -1, 1, -1}, {-1, 1, 1, -1, -1, 1, 1}, {-1, 1, 1,
    -1, 1, -1, -1}, {-1, 1, 1, -1, 1, -1, 1}, {-1, 1, 1, -1, 1, 1, -1}, {-1, 1,
    1, -1, 1, 1, 1}, {-1, 1, 1, 1, -1, -1, -1}, {-1, 1, 1, 1, -1, -1, 1}, {-1,
    1, 1, 1, -1, 1, -1}, {-1, 1, 1, 1, -1, 1, 1}, {-1, 1, 1, 1, 1, -1, -1},
    {-1, 1, 1, 1, 1, -1, 1}, {-1, 1, 1, 1, 1, 1, -1}, {-1, 1, 1, 1, 1, 1, 1},
    {1, -1, -1, -1, -1, -1, -1}, {1, -1, -1, -1, -1, -1, 1}, {1, -1, -1, -1,
    -1, 1, -1}, {1, -1, -1, -1, -1, 1, 1}, {1, -1, -1, -1, 1, -1, -1}, {1, -1,
    -1, -1, 1, -1, 1}, {1, -1, -1, -1, 1, 1, -1}, {1, -1, -1, -1, 1, 1, 1}, {1,
    -1, -1, 1, -1, -1, -1}, {1, -1, -1, 1, -1, -1, 1}, {1, -1, -1, 1, -1, 1,
    -1}, {1, -1, -1, 1, -1, 1, 1}, {1, -1, -1, 1, 1, -1, -1}, {1, -1, -1, 1, 1,
    -1, 1}, {1, -1, -1, 1, 1, 1, -1}, {1, -1, -1, 1, 1, 1, 1}, {1, -1, 1, -1,
    -1, -1, -1}, {1, -1, 1, -1, -1, -1, 1}, {1, -1, 1, -1, -1, 1, -1}, {1, -1,
    1, -1, -1, 1, 1}, {1, -1, 1, -1, 1, -1, -1}, {1, -1, 1, -1, 1, -1, 1}, {1,
    -1, 1, -1, 1, 1, -1}, {1, -1, 1, -1, 1, 1, 1}, {1, -1, 1, 1, -1, -1, -1},
    {1, -1, 1, 1, -1, -1, 1}, {1, -1, 1, 1, -1, 1, -1}, {1, -1, 1, 1, -1, 1,
    1}, {1, -1, 1, 1, 1, -1, -1}, {1, -1, 1, 1, 1, -1, 1}, {1, -1, 1, 1, 1, 1,
    -1}, {1, -1, 1, 1, 1, 1, 1}, {1, 1, -1, -1, -1, -1, -1}, {1, 1, -1, -1, -1,
    -1, 1}, {1, 1, -1, -1, -1, 1, -1}, {1, 1, -1, -1, -1, 1, 1}, {1, 1, -1, -1,
    1, -1, -1}, {1, 1, -1, -1, 1, -1, 1}, {1, 1, -1, -1, 1, 1, -1}, {1, 1, -1,
    -1, 1, 1, 1}, {1, 1, -1, 1, -1, -1, -1}, {1, 1, -1, 1, -1, -1, 1}, {1, 1,
    -1, 1, -1, 1, -1}, {1, 1, -1, 1, -1, 1, 1}, {1, 1, -1, 1, 1, -1, -1}, {1,
    1, -1, 1, 1, -1, 1}, {1, 1, -1, 1, 1, 1, -1}, {1, 1, -1, 1, 1, 1, 1}, {1,
    1, 1, -1, -1, -1, -1}, {1, 1, 1, -1, -1, -1, 1}, {1, 1, 1, -1, -1, 1, -1},
    {1, 1, 1, -1, -1, 1, 1}, {1, 1, 1, -1, 1, -1, -1}, {1, 1, 1, -1, 1, -1, 1},
    {1, 1, 1, -1, 1, 1, -1}, {1, 1, 1, -1, 1, 1, 1}, {1, 1, 1, 1, -1, -1, -1},
    {1, 1, 1, 1, -1, -1, 1}, {1, 1, 1, 1, -1, 1, -1}, {1, 1, 1, 1, -1, 1, 1},
    {1, 1, 1, 1, 1, -1, -1}, {1, 1, 1, 1, 1, -1, 1}, {1, 1, 1, 1, 1, 1, -1},
    {1, 1, 1, 1, 1, 1, 1}};

// Normalization factors the various processes
// Denominators: spins, colors and identical particles
int PY8MEs_R4_P6_sm_lq_lgggq::denom_colors[nprocesses] = {3, 3, 3, 3, 3, 3, 3,
    3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3};
int PY8MEs_R4_P6_sm_lq_lgggq::denom_hels[nprocesses] = {4, 4, 4, 4, 4, 4, 4, 4,
    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4};
int PY8MEs_R4_P6_sm_lq_lgggq::denom_iden[nprocesses] = {6, 6, 6, 6, 6, 6, 6, 6,
    6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6};

//--------------------------------------------------------------------------
// Color config initialization
void PY8MEs_R4_P6_sm_lq_lgggq::initColorConfigs() 
{
  color_configs = vector < vec_vec_int > (); 
  jamp_nc_relative_power = vector < vec_int > (); 

  // Color flows of process Process: e- u > e- g g g u WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[0].push_back(vec_int(createvector<int>
      (0)(0)(4)(0)(0)(0)(2)(1)(3)(2)(4)(3)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #1
  color_configs[0].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(2)(1)(3)(4)(4)(2)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #2
  color_configs[0].push_back(vec_int(createvector<int>
      (0)(0)(4)(0)(0)(0)(2)(3)(3)(1)(4)(2)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #3
  color_configs[0].push_back(vec_int(createvector<int>
      (0)(0)(2)(0)(0)(0)(2)(4)(3)(1)(4)(3)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #4
  color_configs[0].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(2)(4)(3)(2)(4)(1)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #5
  color_configs[0].push_back(vec_int(createvector<int>
      (0)(0)(2)(0)(0)(0)(2)(3)(3)(4)(4)(1)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 

  // Color flows of process Process: e- d > e- g g g d WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[1].push_back(vec_int(createvector<int>
      (0)(0)(4)(0)(0)(0)(2)(1)(3)(2)(4)(3)(1)(0)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #1
  color_configs[1].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(2)(1)(3)(4)(4)(2)(1)(0)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #2
  color_configs[1].push_back(vec_int(createvector<int>
      (0)(0)(4)(0)(0)(0)(2)(3)(3)(1)(4)(2)(1)(0)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #3
  color_configs[1].push_back(vec_int(createvector<int>
      (0)(0)(2)(0)(0)(0)(2)(4)(3)(1)(4)(3)(1)(0)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #4
  color_configs[1].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(2)(4)(3)(2)(4)(1)(1)(0)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #5
  color_configs[1].push_back(vec_int(createvector<int>
      (0)(0)(2)(0)(0)(0)(2)(3)(3)(4)(4)(1)(1)(0)));
  jamp_nc_relative_power[1].push_back(0); 

  // Color flows of process Process: e- u~ > e- g g g u~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(1)(3)(2)(4)(3)(0)(4)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #1
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(1)(3)(4)(4)(2)(0)(3)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #2
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(3)(3)(1)(4)(2)(0)(4)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #3
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(4)(3)(1)(4)(3)(0)(2)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #4
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(4)(3)(2)(4)(1)(0)(3)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #5
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(3)(3)(4)(4)(1)(0)(2)));
  jamp_nc_relative_power[2].push_back(0); 

  // Color flows of process Process: e- d~ > e- g g g d~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[3].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(1)(3)(2)(4)(3)(0)(4)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #1
  color_configs[3].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(1)(3)(4)(4)(2)(0)(3)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #2
  color_configs[3].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(3)(3)(1)(4)(2)(0)(4)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #3
  color_configs[3].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(4)(3)(1)(4)(3)(0)(2)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #4
  color_configs[3].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(4)(3)(2)(4)(1)(0)(3)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #5
  color_configs[3].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(3)(3)(4)(4)(1)(0)(2)));
  jamp_nc_relative_power[3].push_back(0); 

  // Color flows of process Process: e+ u > e+ g g g u WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[4].push_back(vec_int(createvector<int>
      (0)(0)(4)(0)(0)(0)(2)(1)(3)(2)(4)(3)(1)(0)));
  jamp_nc_relative_power[4].push_back(0); 
  // JAMP #1
  color_configs[4].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(2)(1)(3)(4)(4)(2)(1)(0)));
  jamp_nc_relative_power[4].push_back(0); 
  // JAMP #2
  color_configs[4].push_back(vec_int(createvector<int>
      (0)(0)(4)(0)(0)(0)(2)(3)(3)(1)(4)(2)(1)(0)));
  jamp_nc_relative_power[4].push_back(0); 
  // JAMP #3
  color_configs[4].push_back(vec_int(createvector<int>
      (0)(0)(2)(0)(0)(0)(2)(4)(3)(1)(4)(3)(1)(0)));
  jamp_nc_relative_power[4].push_back(0); 
  // JAMP #4
  color_configs[4].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(2)(4)(3)(2)(4)(1)(1)(0)));
  jamp_nc_relative_power[4].push_back(0); 
  // JAMP #5
  color_configs[4].push_back(vec_int(createvector<int>
      (0)(0)(2)(0)(0)(0)(2)(3)(3)(4)(4)(1)(1)(0)));
  jamp_nc_relative_power[4].push_back(0); 

  // Color flows of process Process: e+ d > e+ g g g d WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[5].push_back(vec_int(createvector<int>
      (0)(0)(4)(0)(0)(0)(2)(1)(3)(2)(4)(3)(1)(0)));
  jamp_nc_relative_power[5].push_back(0); 
  // JAMP #1
  color_configs[5].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(2)(1)(3)(4)(4)(2)(1)(0)));
  jamp_nc_relative_power[5].push_back(0); 
  // JAMP #2
  color_configs[5].push_back(vec_int(createvector<int>
      (0)(0)(4)(0)(0)(0)(2)(3)(3)(1)(4)(2)(1)(0)));
  jamp_nc_relative_power[5].push_back(0); 
  // JAMP #3
  color_configs[5].push_back(vec_int(createvector<int>
      (0)(0)(2)(0)(0)(0)(2)(4)(3)(1)(4)(3)(1)(0)));
  jamp_nc_relative_power[5].push_back(0); 
  // JAMP #4
  color_configs[5].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(2)(4)(3)(2)(4)(1)(1)(0)));
  jamp_nc_relative_power[5].push_back(0); 
  // JAMP #5
  color_configs[5].push_back(vec_int(createvector<int>
      (0)(0)(2)(0)(0)(0)(2)(3)(3)(4)(4)(1)(1)(0)));
  jamp_nc_relative_power[5].push_back(0); 

  // Color flows of process Process: e+ u~ > e+ g g g u~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[6].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(1)(3)(2)(4)(3)(0)(4)));
  jamp_nc_relative_power[6].push_back(0); 
  // JAMP #1
  color_configs[6].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(1)(3)(4)(4)(2)(0)(3)));
  jamp_nc_relative_power[6].push_back(0); 
  // JAMP #2
  color_configs[6].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(3)(3)(1)(4)(2)(0)(4)));
  jamp_nc_relative_power[6].push_back(0); 
  // JAMP #3
  color_configs[6].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(4)(3)(1)(4)(3)(0)(2)));
  jamp_nc_relative_power[6].push_back(0); 
  // JAMP #4
  color_configs[6].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(4)(3)(2)(4)(1)(0)(3)));
  jamp_nc_relative_power[6].push_back(0); 
  // JAMP #5
  color_configs[6].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(3)(3)(4)(4)(1)(0)(2)));
  jamp_nc_relative_power[6].push_back(0); 

  // Color flows of process Process: e+ d~ > e+ g g g d~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[7].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(1)(3)(2)(4)(3)(0)(4)));
  jamp_nc_relative_power[7].push_back(0); 
  // JAMP #1
  color_configs[7].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(1)(3)(4)(4)(2)(0)(3)));
  jamp_nc_relative_power[7].push_back(0); 
  // JAMP #2
  color_configs[7].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(3)(3)(1)(4)(2)(0)(4)));
  jamp_nc_relative_power[7].push_back(0); 
  // JAMP #3
  color_configs[7].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(4)(3)(1)(4)(3)(0)(2)));
  jamp_nc_relative_power[7].push_back(0); 
  // JAMP #4
  color_configs[7].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(4)(3)(2)(4)(1)(0)(3)));
  jamp_nc_relative_power[7].push_back(0); 
  // JAMP #5
  color_configs[7].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(3)(3)(4)(4)(1)(0)(2)));
  jamp_nc_relative_power[7].push_back(0); 

  // Color flows of process Process: e- u > ve g g g d WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[8].push_back(vec_int(createvector<int>
      (0)(0)(4)(0)(0)(0)(2)(1)(3)(2)(4)(3)(1)(0)));
  jamp_nc_relative_power[8].push_back(0); 
  // JAMP #1
  color_configs[8].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(2)(1)(3)(4)(4)(2)(1)(0)));
  jamp_nc_relative_power[8].push_back(0); 
  // JAMP #2
  color_configs[8].push_back(vec_int(createvector<int>
      (0)(0)(4)(0)(0)(0)(2)(3)(3)(1)(4)(2)(1)(0)));
  jamp_nc_relative_power[8].push_back(0); 
  // JAMP #3
  color_configs[8].push_back(vec_int(createvector<int>
      (0)(0)(2)(0)(0)(0)(2)(4)(3)(1)(4)(3)(1)(0)));
  jamp_nc_relative_power[8].push_back(0); 
  // JAMP #4
  color_configs[8].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(2)(4)(3)(2)(4)(1)(1)(0)));
  jamp_nc_relative_power[8].push_back(0); 
  // JAMP #5
  color_configs[8].push_back(vec_int(createvector<int>
      (0)(0)(2)(0)(0)(0)(2)(3)(3)(4)(4)(1)(1)(0)));
  jamp_nc_relative_power[8].push_back(0); 

  // Color flows of process Process: e- d~ > ve g g g u~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[9].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(1)(3)(2)(4)(3)(0)(4)));
  jamp_nc_relative_power[9].push_back(0); 
  // JAMP #1
  color_configs[9].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(1)(3)(4)(4)(2)(0)(3)));
  jamp_nc_relative_power[9].push_back(0); 
  // JAMP #2
  color_configs[9].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(3)(3)(1)(4)(2)(0)(4)));
  jamp_nc_relative_power[9].push_back(0); 
  // JAMP #3
  color_configs[9].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(4)(3)(1)(4)(3)(0)(2)));
  jamp_nc_relative_power[9].push_back(0); 
  // JAMP #4
  color_configs[9].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(4)(3)(2)(4)(1)(0)(3)));
  jamp_nc_relative_power[9].push_back(0); 
  // JAMP #5
  color_configs[9].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(3)(3)(4)(4)(1)(0)(2)));
  jamp_nc_relative_power[9].push_back(0); 

  // Color flows of process Process: ve u > ve g g g u WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[10].push_back(vec_int(createvector<int>
      (0)(0)(4)(0)(0)(0)(2)(1)(3)(2)(4)(3)(1)(0)));
  jamp_nc_relative_power[10].push_back(0); 
  // JAMP #1
  color_configs[10].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(2)(1)(3)(4)(4)(2)(1)(0)));
  jamp_nc_relative_power[10].push_back(0); 
  // JAMP #2
  color_configs[10].push_back(vec_int(createvector<int>
      (0)(0)(4)(0)(0)(0)(2)(3)(3)(1)(4)(2)(1)(0)));
  jamp_nc_relative_power[10].push_back(0); 
  // JAMP #3
  color_configs[10].push_back(vec_int(createvector<int>
      (0)(0)(2)(0)(0)(0)(2)(4)(3)(1)(4)(3)(1)(0)));
  jamp_nc_relative_power[10].push_back(0); 
  // JAMP #4
  color_configs[10].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(2)(4)(3)(2)(4)(1)(1)(0)));
  jamp_nc_relative_power[10].push_back(0); 
  // JAMP #5
  color_configs[10].push_back(vec_int(createvector<int>
      (0)(0)(2)(0)(0)(0)(2)(3)(3)(4)(4)(1)(1)(0)));
  jamp_nc_relative_power[10].push_back(0); 

  // Color flows of process Process: ve d > ve g g g d WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[11].push_back(vec_int(createvector<int>
      (0)(0)(4)(0)(0)(0)(2)(1)(3)(2)(4)(3)(1)(0)));
  jamp_nc_relative_power[11].push_back(0); 
  // JAMP #1
  color_configs[11].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(2)(1)(3)(4)(4)(2)(1)(0)));
  jamp_nc_relative_power[11].push_back(0); 
  // JAMP #2
  color_configs[11].push_back(vec_int(createvector<int>
      (0)(0)(4)(0)(0)(0)(2)(3)(3)(1)(4)(2)(1)(0)));
  jamp_nc_relative_power[11].push_back(0); 
  // JAMP #3
  color_configs[11].push_back(vec_int(createvector<int>
      (0)(0)(2)(0)(0)(0)(2)(4)(3)(1)(4)(3)(1)(0)));
  jamp_nc_relative_power[11].push_back(0); 
  // JAMP #4
  color_configs[11].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(2)(4)(3)(2)(4)(1)(1)(0)));
  jamp_nc_relative_power[11].push_back(0); 
  // JAMP #5
  color_configs[11].push_back(vec_int(createvector<int>
      (0)(0)(2)(0)(0)(0)(2)(3)(3)(4)(4)(1)(1)(0)));
  jamp_nc_relative_power[11].push_back(0); 

  // Color flows of process Process: ve u~ > ve g g g u~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[12].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(1)(3)(2)(4)(3)(0)(4)));
  jamp_nc_relative_power[12].push_back(0); 
  // JAMP #1
  color_configs[12].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(1)(3)(4)(4)(2)(0)(3)));
  jamp_nc_relative_power[12].push_back(0); 
  // JAMP #2
  color_configs[12].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(3)(3)(1)(4)(2)(0)(4)));
  jamp_nc_relative_power[12].push_back(0); 
  // JAMP #3
  color_configs[12].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(4)(3)(1)(4)(3)(0)(2)));
  jamp_nc_relative_power[12].push_back(0); 
  // JAMP #4
  color_configs[12].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(4)(3)(2)(4)(1)(0)(3)));
  jamp_nc_relative_power[12].push_back(0); 
  // JAMP #5
  color_configs[12].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(3)(3)(4)(4)(1)(0)(2)));
  jamp_nc_relative_power[12].push_back(0); 

  // Color flows of process Process: ve d~ > ve g g g d~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[13].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(1)(3)(2)(4)(3)(0)(4)));
  jamp_nc_relative_power[13].push_back(0); 
  // JAMP #1
  color_configs[13].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(1)(3)(4)(4)(2)(0)(3)));
  jamp_nc_relative_power[13].push_back(0); 
  // JAMP #2
  color_configs[13].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(3)(3)(1)(4)(2)(0)(4)));
  jamp_nc_relative_power[13].push_back(0); 
  // JAMP #3
  color_configs[13].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(4)(3)(1)(4)(3)(0)(2)));
  jamp_nc_relative_power[13].push_back(0); 
  // JAMP #4
  color_configs[13].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(4)(3)(2)(4)(1)(0)(3)));
  jamp_nc_relative_power[13].push_back(0); 
  // JAMP #5
  color_configs[13].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(3)(3)(4)(4)(1)(0)(2)));
  jamp_nc_relative_power[13].push_back(0); 

  // Color flows of process Process: e+ d > ve~ g g g u WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[14].push_back(vec_int(createvector<int>
      (0)(0)(4)(0)(0)(0)(2)(1)(3)(2)(4)(3)(1)(0)));
  jamp_nc_relative_power[14].push_back(0); 
  // JAMP #1
  color_configs[14].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(2)(1)(3)(4)(4)(2)(1)(0)));
  jamp_nc_relative_power[14].push_back(0); 
  // JAMP #2
  color_configs[14].push_back(vec_int(createvector<int>
      (0)(0)(4)(0)(0)(0)(2)(3)(3)(1)(4)(2)(1)(0)));
  jamp_nc_relative_power[14].push_back(0); 
  // JAMP #3
  color_configs[14].push_back(vec_int(createvector<int>
      (0)(0)(2)(0)(0)(0)(2)(4)(3)(1)(4)(3)(1)(0)));
  jamp_nc_relative_power[14].push_back(0); 
  // JAMP #4
  color_configs[14].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(2)(4)(3)(2)(4)(1)(1)(0)));
  jamp_nc_relative_power[14].push_back(0); 
  // JAMP #5
  color_configs[14].push_back(vec_int(createvector<int>
      (0)(0)(2)(0)(0)(0)(2)(3)(3)(4)(4)(1)(1)(0)));
  jamp_nc_relative_power[14].push_back(0); 

  // Color flows of process Process: e+ u~ > ve~ g g g d~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[15].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(1)(3)(2)(4)(3)(0)(4)));
  jamp_nc_relative_power[15].push_back(0); 
  // JAMP #1
  color_configs[15].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(1)(3)(4)(4)(2)(0)(3)));
  jamp_nc_relative_power[15].push_back(0); 
  // JAMP #2
  color_configs[15].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(3)(3)(1)(4)(2)(0)(4)));
  jamp_nc_relative_power[15].push_back(0); 
  // JAMP #3
  color_configs[15].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(4)(3)(1)(4)(3)(0)(2)));
  jamp_nc_relative_power[15].push_back(0); 
  // JAMP #4
  color_configs[15].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(4)(3)(2)(4)(1)(0)(3)));
  jamp_nc_relative_power[15].push_back(0); 
  // JAMP #5
  color_configs[15].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(3)(3)(4)(4)(1)(0)(2)));
  jamp_nc_relative_power[15].push_back(0); 

  // Color flows of process Process: ve~ u > ve~ g g g u WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[16].push_back(vec_int(createvector<int>
      (0)(0)(4)(0)(0)(0)(2)(1)(3)(2)(4)(3)(1)(0)));
  jamp_nc_relative_power[16].push_back(0); 
  // JAMP #1
  color_configs[16].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(2)(1)(3)(4)(4)(2)(1)(0)));
  jamp_nc_relative_power[16].push_back(0); 
  // JAMP #2
  color_configs[16].push_back(vec_int(createvector<int>
      (0)(0)(4)(0)(0)(0)(2)(3)(3)(1)(4)(2)(1)(0)));
  jamp_nc_relative_power[16].push_back(0); 
  // JAMP #3
  color_configs[16].push_back(vec_int(createvector<int>
      (0)(0)(2)(0)(0)(0)(2)(4)(3)(1)(4)(3)(1)(0)));
  jamp_nc_relative_power[16].push_back(0); 
  // JAMP #4
  color_configs[16].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(2)(4)(3)(2)(4)(1)(1)(0)));
  jamp_nc_relative_power[16].push_back(0); 
  // JAMP #5
  color_configs[16].push_back(vec_int(createvector<int>
      (0)(0)(2)(0)(0)(0)(2)(3)(3)(4)(4)(1)(1)(0)));
  jamp_nc_relative_power[16].push_back(0); 

  // Color flows of process Process: ve~ d > ve~ g g g d WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[17].push_back(vec_int(createvector<int>
      (0)(0)(4)(0)(0)(0)(2)(1)(3)(2)(4)(3)(1)(0)));
  jamp_nc_relative_power[17].push_back(0); 
  // JAMP #1
  color_configs[17].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(2)(1)(3)(4)(4)(2)(1)(0)));
  jamp_nc_relative_power[17].push_back(0); 
  // JAMP #2
  color_configs[17].push_back(vec_int(createvector<int>
      (0)(0)(4)(0)(0)(0)(2)(3)(3)(1)(4)(2)(1)(0)));
  jamp_nc_relative_power[17].push_back(0); 
  // JAMP #3
  color_configs[17].push_back(vec_int(createvector<int>
      (0)(0)(2)(0)(0)(0)(2)(4)(3)(1)(4)(3)(1)(0)));
  jamp_nc_relative_power[17].push_back(0); 
  // JAMP #4
  color_configs[17].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(2)(4)(3)(2)(4)(1)(1)(0)));
  jamp_nc_relative_power[17].push_back(0); 
  // JAMP #5
  color_configs[17].push_back(vec_int(createvector<int>
      (0)(0)(2)(0)(0)(0)(2)(3)(3)(4)(4)(1)(1)(0)));
  jamp_nc_relative_power[17].push_back(0); 

  // Color flows of process Process: ve~ u~ > ve~ g g g u~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[18].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(1)(3)(2)(4)(3)(0)(4)));
  jamp_nc_relative_power[18].push_back(0); 
  // JAMP #1
  color_configs[18].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(1)(3)(4)(4)(2)(0)(3)));
  jamp_nc_relative_power[18].push_back(0); 
  // JAMP #2
  color_configs[18].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(3)(3)(1)(4)(2)(0)(4)));
  jamp_nc_relative_power[18].push_back(0); 
  // JAMP #3
  color_configs[18].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(4)(3)(1)(4)(3)(0)(2)));
  jamp_nc_relative_power[18].push_back(0); 
  // JAMP #4
  color_configs[18].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(4)(3)(2)(4)(1)(0)(3)));
  jamp_nc_relative_power[18].push_back(0); 
  // JAMP #5
  color_configs[18].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(3)(3)(4)(4)(1)(0)(2)));
  jamp_nc_relative_power[18].push_back(0); 

  // Color flows of process Process: ve~ d~ > ve~ g g g d~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[19].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(1)(3)(2)(4)(3)(0)(4)));
  jamp_nc_relative_power[19].push_back(0); 
  // JAMP #1
  color_configs[19].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(1)(3)(4)(4)(2)(0)(3)));
  jamp_nc_relative_power[19].push_back(0); 
  // JAMP #2
  color_configs[19].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(3)(3)(1)(4)(2)(0)(4)));
  jamp_nc_relative_power[19].push_back(0); 
  // JAMP #3
  color_configs[19].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(4)(3)(1)(4)(3)(0)(2)));
  jamp_nc_relative_power[19].push_back(0); 
  // JAMP #4
  color_configs[19].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(4)(3)(2)(4)(1)(0)(3)));
  jamp_nc_relative_power[19].push_back(0); 
  // JAMP #5
  color_configs[19].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(2)(3)(3)(4)(4)(1)(0)(2)));
  jamp_nc_relative_power[19].push_back(0); 
}

//--------------------------------------------------------------------------
// Destructor.
PY8MEs_R4_P6_sm_lq_lgggq::~PY8MEs_R4_P6_sm_lq_lgggq() 
{
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    delete[] p[i]; 
    p[i] = NULL; 
  }
}

//--------------------------------------------------------------------------
// Invert the permutation mapping
vector<int> PY8MEs_R4_P6_sm_lq_lgggq::invert_mapping(vector<int> mapping) 
{
  vector<int> inverted_mapping; 
  for (unsigned int i = 0; i < mapping.size(); i++ )
  {
    for (unsigned int j = 0; j < mapping.size(); j++ )
    {
      if (mapping[j] == ((int)i))
      {
        inverted_mapping.push_back(j); 
        break; 
      }
    }
  }
  return inverted_mapping; 
}

//--------------------------------------------------------------------------
// Return the list of possible helicity configurations
vector < vec_int > PY8MEs_R4_P6_sm_lq_lgggq::getHelicityConfigs(vector<int>
    permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(ncomb, vector<int> (nexternal, 0)); 
  for (unsigned int ihel = 0; ihel < ncomb; ihel++ )
  {
    for(unsigned int j = 0; j < nexternal; j++ )
    {
      res[ihel][chosenPerm[j]] = helicities[ihel][j]; 
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Return the list of possible color configurations
vector < vec_int > PY8MEs_R4_P6_sm_lq_lgggq::getColorConfigs(int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(color_configs[chosenProcID].size(), vector<int>
      (nexternal * 2, 0));
  for (unsigned int icol = 0; icol < color_configs[chosenProcID].size(); icol++
      )
  {
    for(unsigned int j = 0; j < (2 * nexternal); j++ )
    {
      res[icol][chosenPerm[j/2] * 2 + j%2] =
          color_configs[chosenProcID][icol][j];
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Get JAMP relative N_c power
int PY8MEs_R4_P6_sm_lq_lgggq::getColorFlowRelativeNCPower(int color_flow_ID,
    int specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return jamp_nc_relative_power[chosenProcID][color_flow_ID]; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity ID -> Helicity Config
vector<int> PY8MEs_R4_P6_sm_lq_lgggq::getHelicityConfigForID(int hel_ID,
    vector<int> permutation)
{
  if (hel_ID < 0 || hel_ID >= ncomb)
  {
    cerr <<  "Error in function 'getHelicityConfigForID' of class" << 
    " 'PY8MEs_R4_P6_sm_lq_lgggq': Specified helicity ID '" << 
    hel_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R4_P6_sm_lq_lgggq_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(nexternal, 0); 
  for (unsigned int j = 0; j < nexternal; j++ )
  {
    res[chosenPerm[j]] = helicities[hel_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity Config -> Helicity ID
int PY8MEs_R4_P6_sm_lq_lgggq::getHelicityIDForConfig(vector<int> hel_config,
    vector<int> permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  int user_ihel = -1; 
  if (hel_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < ncomb; i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < nexternal; j++ )
      {
        if (helicities[i][chosenPerm[j]] != hel_config[j])
        {
          found = false; 
          break; 
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_ihel = i; 
        break; 
      }
    }
    if (user_ihel == -1)
    {
      cerr <<  "Error in function 'getHelicityIDForConfig' of class" << 
      " 'PY8MEs_R4_P6_sm_lq_lgggq': Specified helicity" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R4_P6_sm_lq_lgggq_exception; 
    }
  }
  return user_ihel; 
}


//--------------------------------------------------------------------------
// Implements the map Color ID -> Color Config
vector<int> PY8MEs_R4_P6_sm_lq_lgggq::getColorConfigForID(int color_ID, int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < 0 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getColorConfigForID' of class" << 
    " 'PY8MEs_R4_P6_sm_lq_lgggq': Specified color ID '" << 
    color_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R4_P6_sm_lq_lgggq_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(color_configs[chosenProcID][color_ID].size(), 0); 
  for (unsigned int j = 0; j < (2 * nexternal); j++ )
  {
    res[chosenPerm[j/2] * 2 + j%2] = color_configs[chosenProcID][color_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Color Config -> Color ID
int PY8MEs_R4_P6_sm_lq_lgggq::getColorIDForConfig(vector<int> color_config, int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = -1; 
  if (color_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < color_configs[chosenProcID].size(); i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < (nexternal * 2); j++ )
      {

        // If colorless then make sure it matches
        // The little arithmetics in the color index is just
        // the permutation applies on the particle list which is
        // twice smaller since each particle can have two color indices.
        if (color_config[j] == 0)
        {
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] != 0)
          {
            found = false; 
            break; 
          }
          // Otherwise check that the color linked position matches
        }
        else
        {
          int color_linked_pos = -1; 
          // Find the other end of the line in the user color config
          for (unsigned int k = 0; k < (nexternal * 2); k++ )
          {
            if (k == j)
              continue; 
            if (color_config[j] == color_config[k])
            {
              color_linked_pos = k; 
              break; 
            }
          }
          if (color_linked_pos == -1)
          {
            cerr <<  "Error in function 'getColorIDForConfig' of class" << 
            " 'PY8MEs_R4_P6_sm_lq_lgggq': A color line could " << 
            " not be closed." << endl; 
            throw PY8MEs_R4_P6_sm_lq_lgggq_exception; 
          }
          // Now check whether the color line matches
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] !=
              color_configs[chosenProcID][i][chosenPerm[color_linked_pos/2] * 2
              + color_linked_pos%2])
          {
            found = false; 
            break; 
          }
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_icol = i; 
        break; 
      }
    }

    if (user_icol == -1)
    {
      cerr <<  "Error in function 'getColorIDForConfig' of class" << 
      " 'PY8MEs_R4_P6_sm_lq_lgggq': Specified color" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R4_P6_sm_lq_lgggq_exception; 
    }
  }
  return user_icol; 
}

//--------------------------------------------------------------------------
// Returns all result previously computed in SigmaKin
vector < vec_double > PY8MEs_R4_P6_sm_lq_lgggq::getAllResults(int
    specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return all_results[chosenProcID]; 
}

//--------------------------------------------------------------------------
// Returns a result previously computed in SigmaKin for a specific helicity
// and color ID. -1 means avg and summed over that characteristic.
double PY8MEs_R4_P6_sm_lq_lgggq::getResult(int helicity_ID, int color_ID, int
    specify_proc_ID)
{
  if (helicity_ID < - 1 || helicity_ID >= ncomb)
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R4_P6_sm_lq_lgggq': Specified helicity ID '" << 
    helicity_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R4_P6_sm_lq_lgggq_exception; 
  }
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < - 1 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R4_P6_sm_lq_lgggq': Specified color ID '" << 
    color_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R4_P6_sm_lq_lgggq_exception; 
  }
  return all_results[chosenProcID][helicity_ID + 1][color_ID + 1]; 
}

//--------------------------------------------------------------------------
// Check for the availability of the requested process and if available,
// If available, this returns the corresponding permutation and Proc_ID to use.
// If not available, this returns a negative Proc_ID.
pair < vector<int> , int > PY8MEs_R4_P6_sm_lq_lgggq::static_getPY8ME(vector<int> initial_pdgs, vector<int> final_pdgs, set<int> schannels) 
{

  // Not available return value
  pair < vector<int> , int > NA(vector<int> (), -1); 

  // Check if s-channel requirements match
  if (nreq_s_channels > 0)
  {
    if (schannels != s_channel_proc)
      return NA; 
  }
  else
  {
    if (schannels.size() != 0)
      return NA; 
  }

  // Check number of final state particles
  if (final_pdgs.size() != (nexternal - ninitial))
    return NA; 

  // Check number of initial state particles
  if (initial_pdgs.size() != ninitial)
    return NA; 

  // List of processes available in this class
  const int nprocs = 112; 
  const int proc_IDS[nprocs] = {0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3,
      4, 4, 4, 4, 5, 5, 5, 5, 6, 6, 6, 6, 7, 7, 7, 7, 8, 8, 8, 8, 8, 8, 8, 8,
      9, 9, 9, 9, 9, 9, 9, 9, 10, 10, 10, 10, 10, 10, 11, 11, 11, 11, 11, 11,
      12, 12, 12, 12, 12, 12, 13, 13, 13, 13, 13, 13, 14, 14, 14, 14, 14, 14,
      14, 14, 15, 15, 15, 15, 15, 15, 15, 15, 16, 16, 16, 16, 16, 16, 17, 17,
      17, 17, 17, 17, 18, 18, 18, 18, 18, 18, 19, 19, 19, 19, 19, 19};
  const int in_pdgs[nprocs][ninitial] = {{11, 2}, {11, 4}, {13, 2}, {13, 4},
      {11, 1}, {11, 3}, {13, 1}, {13, 3}, {11, -2}, {11, -4}, {13, -2}, {13,
      -4}, {11, -1}, {11, -3}, {13, -1}, {13, -3}, {-11, 2}, {-11, 4}, {-13,
      2}, {-13, 4}, {-11, 1}, {-11, 3}, {-13, 1}, {-13, 3}, {-11, -2}, {-11,
      -4}, {-13, -2}, {-13, -4}, {-11, -1}, {-11, -3}, {-13, -1}, {-13, -3},
      {11, 2}, {11, 4}, {13, 2}, {13, 4}, {12, 1}, {12, 3}, {14, 1}, {14, 3},
      {11, -1}, {11, -3}, {13, -1}, {13, -3}, {12, -2}, {12, -4}, {14, -2},
      {14, -4}, {12, 2}, {12, 4}, {14, 2}, {14, 4}, {16, 2}, {16, 4}, {12, 1},
      {12, 3}, {14, 1}, {14, 3}, {16, 1}, {16, 3}, {12, -2}, {12, -4}, {14,
      -2}, {14, -4}, {16, -2}, {16, -4}, {12, -1}, {12, -3}, {14, -1}, {14,
      -3}, {16, -1}, {16, -3}, {-11, 1}, {-11, 3}, {-13, 1}, {-13, 3}, {-12,
      2}, {-12, 4}, {-14, 2}, {-14, 4}, {-11, -2}, {-11, -4}, {-13, -2}, {-13,
      -4}, {-12, -1}, {-12, -3}, {-14, -1}, {-14, -3}, {-12, 2}, {-12, 4},
      {-14, 2}, {-14, 4}, {-16, 2}, {-16, 4}, {-12, 1}, {-12, 3}, {-14, 1},
      {-14, 3}, {-16, 1}, {-16, 3}, {-12, -2}, {-12, -4}, {-14, -2}, {-14, -4},
      {-16, -2}, {-16, -4}, {-12, -1}, {-12, -3}, {-14, -1}, {-14, -3}, {-16,
      -1}, {-16, -3}};
  const int out_pdgs[nprocs][nexternal - ninitial] = {{11, 21, 21, 21, 2}, {11,
      21, 21, 21, 4}, {13, 21, 21, 21, 2}, {13, 21, 21, 21, 4}, {11, 21, 21,
      21, 1}, {11, 21, 21, 21, 3}, {13, 21, 21, 21, 1}, {13, 21, 21, 21, 3},
      {11, 21, 21, 21, -2}, {11, 21, 21, 21, -4}, {13, 21, 21, 21, -2}, {13,
      21, 21, 21, -4}, {11, 21, 21, 21, -1}, {11, 21, 21, 21, -3}, {13, 21, 21,
      21, -1}, {13, 21, 21, 21, -3}, {-11, 21, 21, 21, 2}, {-11, 21, 21, 21,
      4}, {-13, 21, 21, 21, 2}, {-13, 21, 21, 21, 4}, {-11, 21, 21, 21, 1},
      {-11, 21, 21, 21, 3}, {-13, 21, 21, 21, 1}, {-13, 21, 21, 21, 3}, {-11,
      21, 21, 21, -2}, {-11, 21, 21, 21, -4}, {-13, 21, 21, 21, -2}, {-13, 21,
      21, 21, -4}, {-11, 21, 21, 21, -1}, {-11, 21, 21, 21, -3}, {-13, 21, 21,
      21, -1}, {-13, 21, 21, 21, -3}, {12, 21, 21, 21, 1}, {12, 21, 21, 21, 3},
      {14, 21, 21, 21, 1}, {14, 21, 21, 21, 3}, {11, 21, 21, 21, 2}, {11, 21,
      21, 21, 4}, {13, 21, 21, 21, 2}, {13, 21, 21, 21, 4}, {12, 21, 21, 21,
      -2}, {12, 21, 21, 21, -4}, {14, 21, 21, 21, -2}, {14, 21, 21, 21, -4},
      {11, 21, 21, 21, -1}, {11, 21, 21, 21, -3}, {13, 21, 21, 21, -1}, {13,
      21, 21, 21, -3}, {12, 21, 21, 21, 2}, {12, 21, 21, 21, 4}, {14, 21, 21,
      21, 2}, {14, 21, 21, 21, 4}, {16, 21, 21, 21, 2}, {16, 21, 21, 21, 4},
      {12, 21, 21, 21, 1}, {12, 21, 21, 21, 3}, {14, 21, 21, 21, 1}, {14, 21,
      21, 21, 3}, {16, 21, 21, 21, 1}, {16, 21, 21, 21, 3}, {12, 21, 21, 21,
      -2}, {12, 21, 21, 21, -4}, {14, 21, 21, 21, -2}, {14, 21, 21, 21, -4},
      {16, 21, 21, 21, -2}, {16, 21, 21, 21, -4}, {12, 21, 21, 21, -1}, {12,
      21, 21, 21, -3}, {14, 21, 21, 21, -1}, {14, 21, 21, 21, -3}, {16, 21, 21,
      21, -1}, {16, 21, 21, 21, -3}, {-12, 21, 21, 21, 2}, {-12, 21, 21, 21,
      4}, {-14, 21, 21, 21, 2}, {-14, 21, 21, 21, 4}, {-11, 21, 21, 21, 1},
      {-11, 21, 21, 21, 3}, {-13, 21, 21, 21, 1}, {-13, 21, 21, 21, 3}, {-12,
      21, 21, 21, -1}, {-12, 21, 21, 21, -3}, {-14, 21, 21, 21, -1}, {-14, 21,
      21, 21, -3}, {-11, 21, 21, 21, -2}, {-11, 21, 21, 21, -4}, {-13, 21, 21,
      21, -2}, {-13, 21, 21, 21, -4}, {-12, 21, 21, 21, 2}, {-12, 21, 21, 21,
      4}, {-14, 21, 21, 21, 2}, {-14, 21, 21, 21, 4}, {-16, 21, 21, 21, 2},
      {-16, 21, 21, 21, 4}, {-12, 21, 21, 21, 1}, {-12, 21, 21, 21, 3}, {-14,
      21, 21, 21, 1}, {-14, 21, 21, 21, 3}, {-16, 21, 21, 21, 1}, {-16, 21, 21,
      21, 3}, {-12, 21, 21, 21, -2}, {-12, 21, 21, 21, -4}, {-14, 21, 21, 21,
      -2}, {-14, 21, 21, 21, -4}, {-16, 21, 21, 21, -2}, {-16, 21, 21, 21, -4},
      {-12, 21, 21, 21, -1}, {-12, 21, 21, 21, -3}, {-14, 21, 21, 21, -1},
      {-14, 21, 21, 21, -3}, {-16, 21, 21, 21, -1}, {-16, 21, 21, 21, -3}};

  bool in_pdgs_used[ninitial]; 
  bool out_pdgs_used[nexternal - ninitial]; 
  for(unsigned int i = 0; i < nprocs; i++ )
  {
    int permutations[nexternal]; 

    // Reinitialize initial state look-up variables
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      in_pdgs_used[j] = false; 
      permutations[j] = -1; 
    }
    // Look for initial state matches
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      for(unsigned int k = 0; k < ninitial; k++ )
      {
        // Make sure it has not been used already
        if (in_pdgs_used[k])
          continue; 
        if (initial_pdgs[k] == in_pdgs[i][j])
        {
          permutations[j] = k; 
          in_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[ninitial - 1] == -1)
      continue; 

    // Reinitialize final state look-up variables
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      out_pdgs_used[j] = false; 
      permutations[ninitial + j] = -1; 
    }
    // Look for final state matches
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      for(unsigned int k = 0; k < (nexternal - ninitial); k++ )
      {
        // Make sure it has not been used already
        if (out_pdgs_used[k])
          continue; 
        if (final_pdgs[k] == out_pdgs[i][j])
        {
          permutations[ninitial + j] = ninitial + k; 
          out_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[ninitial + j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[nexternal - 1] == -1)
      continue; 

    // Return process found
    return pair < vector<int> , int > (vector<int> (permutations, permutations
        + nexternal), proc_IDS[i]);
  }

  // No process found
  return NA; 
}

//--------------------------------------------------------------------------
// Set momenta
void PY8MEs_R4_P6_sm_lq_lgggq::setMomenta(vector < vec_double > momenta_picked)
{
  if (momenta_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setMomenta' of class" << 
    " 'PY8MEs_R4_P6_sm_lq_lgggq': Incorrect number of" << 
    " momenta specified." << endl; 
    throw PY8MEs_R4_P6_sm_lq_lgggq_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    if (momenta_picked[i].size() != 4)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R4_P6_sm_lq_lgggq': Incorrect number of" << 
      " momenta components specified." << endl; 
      throw PY8MEs_R4_P6_sm_lq_lgggq_exception; 
    }
    if (momenta_picked[i][0] < 0.0)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R4_P6_sm_lq_lgggq': A momentum was specified" << 
      " with negative energy. Check conventions." << endl; 
      throw PY8MEs_R4_P6_sm_lq_lgggq_exception; 
    }
    for (unsigned int j = 0; j < 4; j++ )
    {
      p[i][j] = momenta_picked[i][j]; 
    }
  }
}

//--------------------------------------------------------------------------
// Set color configuration to use. An empty vector means sum over all.
void PY8MEs_R4_P6_sm_lq_lgggq::setColors(vector<int> colors_picked)
{
  if (colors_picked.size() == 0)
  {
    user_colors = vector<int> (); 
    return; 
  }
  if (colors_picked.size() != (2 * nexternal))
  {
    cerr <<  "Error in function 'setColors' of class" << 
    " 'PY8MEs_R4_P6_sm_lq_lgggq': Incorrect number" << 
    " of colors specified." << endl; 
    throw PY8MEs_R4_P6_sm_lq_lgggq_exception; 
  }
  user_colors = vector<int> ((2 * nexternal), 0); 
  for(unsigned int i = 0; i < (2 * nexternal); i++ )
  {
    user_colors[i] = colors_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the helicity configuration to use. Am empty vector means sum over all.
void PY8MEs_R4_P6_sm_lq_lgggq::setHelicities(vector<int> helicities_picked) 
{
  if (helicities_picked.size() != nexternal)
  {
    if (helicities_picked.size() == 0)
    {
      user_helicities = vector<int> (); 
      return; 
    }
    cerr <<  "Error in function 'setHelicities' of class" << 
    " 'PY8MEs_R4_P6_sm_lq_lgggq': Incorrect number" << 
    " of helicities specified." << endl; 
    throw PY8MEs_R4_P6_sm_lq_lgggq_exception; 
  }
  user_helicities = vector<int> (nexternal, 0); 
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    user_helicities[i] = helicities_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the permutation to use (will apply to momenta, colors and helicities)
void PY8MEs_R4_P6_sm_lq_lgggq::setPermutation(vector<int> perm_picked) 
{
  if (perm_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setPermutations' of class" << 
    " 'PY8MEs_R4_P6_sm_lq_lgggq': Incorrect number" << 
    " of permutations specified." << endl; 
    throw PY8MEs_R4_P6_sm_lq_lgggq_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    perm[i] = perm_picked[i]; 
  }
}

// Set the proc_ID to use
void PY8MEs_R4_P6_sm_lq_lgggq::setProcID(int procID_picked) 
{
  proc_ID = procID_picked; 
}

//--------------------------------------------------------------------------
// Initialize process.

void PY8MEs_R4_P6_sm_lq_lgggq::initProc() 
{

  // Initialize flags
  include_symmetry_factors = true; 
  include_helicity_averaging_factors = true; 
  include_color_averaging_factors = true; 

  // Initialize vectors.
  perm = vector<int> (nexternal, 0); 
  user_colors = vector<int> (2 * nexternal, 0); 
  user_helicities = vector<int> (nexternal, 0); 
  p = vector < double * > (); 
  for (unsigned int i = 0; i < nexternal; i++ )
  {
    p.push_back(new double[4]); 
  }
  initColorConfigs(); 
  // Synchronize local variables dependent on the model with the active model.
  mME = vector<double> (nexternal, 0.); 
  syncProcModelParams(); 
  jamp2 = vector < vec_double > (20); 
  jamp2[0] = vector<double> (6, 0.); 
  jamp2[1] = vector<double> (6, 0.); 
  jamp2[2] = vector<double> (6, 0.); 
  jamp2[3] = vector<double> (6, 0.); 
  jamp2[4] = vector<double> (6, 0.); 
  jamp2[5] = vector<double> (6, 0.); 
  jamp2[6] = vector<double> (6, 0.); 
  jamp2[7] = vector<double> (6, 0.); 
  jamp2[8] = vector<double> (6, 0.); 
  jamp2[9] = vector<double> (6, 0.); 
  jamp2[10] = vector<double> (6, 0.); 
  jamp2[11] = vector<double> (6, 0.); 
  jamp2[12] = vector<double> (6, 0.); 
  jamp2[13] = vector<double> (6, 0.); 
  jamp2[14] = vector<double> (6, 0.); 
  jamp2[15] = vector<double> (6, 0.); 
  jamp2[16] = vector<double> (6, 0.); 
  jamp2[17] = vector<double> (6, 0.); 
  jamp2[18] = vector<double> (6, 0.); 
  jamp2[19] = vector<double> (6, 0.); 
  all_results = vector < vec_vec_double > (20); 
  // The first entry is always the color or helicity avg/summed matrix element.
  all_results[0] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[1] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[2] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[3] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[4] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[5] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[6] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[7] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[8] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[9] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[10] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[11] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[12] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[13] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[14] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[15] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[16] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[17] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[18] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[19] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
}

// Synchronize local variables of the process that depend on the model
// parameters
void PY8MEs_R4_P6_sm_lq_lgggq::syncProcModelParams() 
{

  // Instantiate the model class and set parameters that stay fixed during run
  mME[0] = pars->ZERO; 
  mME[1] = pars->ZERO; 
  mME[2] = pars->ZERO; 
  mME[3] = pars->ZERO; 
  mME[4] = pars->ZERO; 
  mME[5] = pars->ZERO; 
  mME[6] = pars->ZERO; 
}

//--------------------------------------------------------------------------
// Setter allowing to force particular values for the external masses
void PY8MEs_R4_P6_sm_lq_lgggq::setMasses(vec_double external_masses) 
{

  if (external_masses.size() != mME.size())
  {
    cerr <<  "Error in function 'setMasses' of class" << 
    " 'PY8MEs_R4_P6_sm_lq_lgggq': Incorrect number of" << 
    " masses specified." << endl; 
    throw PY8MEs_R4_P6_sm_lq_lgggq_exception; 
  }
  for (unsigned int j = 0; j < mME.size(); j++ )
  {
    mME[j] = external_masses[perm[j]]; 
  }
}

//--------------------------------------------------------------------------
// Getter accessing external masses with the correct ordering
vector<double> PY8MEs_R4_P6_sm_lq_lgggq::getMasses() 
{

  vec_double external_masses; 
  vector<int> invertedPerm; 
  invertedPerm = invert_mapping(perm); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    external_masses.push_back(mME[invertedPerm[i]]); 
  }
  return external_masses; 

}


// Set all values of the external masses to float(-mode) where mode can be
// 0 : Mass taken from the model
// 1 : Mass taken from p_i^2 if not massless to begin with
// 2 : Mass always taken from p_i^2.
void PY8MEs_R4_P6_sm_lq_lgggq::setExternalMassesMode(int mode) 
{
  if (mode != 0 && mode != 1 && mode != 2)
  {
    cerr <<  "Error in function 'setExternalMassesMode' of class" << 
    " 'PY8MEs_R4_P6_sm_lq_lgggq': Incorrect mode selected :" << mode << 
    ". It must be either 0, 1 or 2" << endl; 
    throw PY8MEs_R4_P6_sm_lq_lgggq_exception; 
  }
  if (mode == 0)
  {
    syncProcModelParams(); 
  }
  else if (mode == 1)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      if (mME[j] != pars->ZERO)
      {
        mME[j] = -1.0; 
      }
    }
  }
  else if (mode == 2)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      mME[j] = -1.0; 
    }
  }
}

//--------------------------------------------------------------------------
// Evaluate the squared matrix element.

double PY8MEs_R4_P6_sm_lq_lgggq::sigmaKin() 
{
  // Set the parameters which change event by event
  pars->setDependentParameters(); 
  pars->setDependentCouplings(); 
  // Reset color flows
  for(int i = 0; i < 6; i++ )
    jamp2[0][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[1][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[2][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[3][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[4][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[5][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[6][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[7][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[8][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[9][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[10][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[11][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[12][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[13][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[14][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[15][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[16][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[17][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[18][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[19][i] = 0.; 

  // Save previous values of mME
  vector<double> saved_mME(mME.size(), 0.0); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (mME[i] < 0.0)
    {
      saved_mME[i] = mME[i]; 
      mME[i] = sqrt(abs(pow(p[perm[i]][0], 2) - 
      (pow(p[perm[i]][1], 2) + pow(p[perm[i]][2], 2) + pow(p[perm[i]][3],
          2))));
    }
  }

  // Local variables and constants
  const int max_tries = 10; 
  vector < vec_bool > goodhel(nprocesses, vec_bool(ncomb, false)); 
  vec_int ntry(nprocesses, 0); 
  double t = 0.; 
  double result = 0.; 

  if (ntry[proc_ID] <= max_tries)
    ntry[proc_ID] = ntry[proc_ID] + 1; 

  // Find which helicity configuration is asked for
  // -1 indicates one wants to sum over helicities
  int user_ihel = getHelicityIDForConfig(user_helicities); 

  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = getColorIDForConfig(user_colors); 

  // Reset the list of results that will be recomputed here
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      all_results[proc_ID][ihel + 1][icolor + 1] = 0.; 
    }
  }

  // Calculate the matrix element for all helicities
  // unless already detected as vanishing
  for(int ihel = 0; ihel < ncomb; ihel++ )
  {
    // Skip helicity if already detected as vanishing
    if ((ntry[proc_ID] >= max_tries) && !goodhel[proc_ID][ihel])
      continue; 

    // Also skip helicity if user asks for a specific one
    if ((ntry[proc_ID] >= max_tries) && user_ihel != -1 && user_ihel != ihel)
      continue; 

    calculate_wavefunctions(helicities[ihel]); 

    // Reset locally computed color flows
    for(int i = 0; i < 6; i++ )
      jamp2[0][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[1][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[2][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[3][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[4][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[5][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[6][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[7][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[8][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[9][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[10][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[11][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[12][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[13][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[14][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[15][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[16][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[17][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[18][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[19][i] = 0.; 

    if (proc_ID == 0)
      t = matrix_4_emu_emgggu(); 
    if (proc_ID == 1)
      t = matrix_4_emd_emgggd(); 
    if (proc_ID == 2)
      t = matrix_4_emux_emgggux(); 
    if (proc_ID == 3)
      t = matrix_4_emdx_emgggdx(); 
    if (proc_ID == 4)
      t = matrix_4_epu_epgggu(); 
    if (proc_ID == 5)
      t = matrix_4_epd_epgggd(); 
    if (proc_ID == 6)
      t = matrix_4_epux_epgggux(); 
    if (proc_ID == 7)
      t = matrix_4_epdx_epgggdx(); 
    if (proc_ID == 8)
      t = matrix_4_emu_vegggd(); 
    if (proc_ID == 9)
      t = matrix_4_emdx_vegggux(); 
    if (proc_ID == 10)
      t = matrix_4_veu_vegggu(); 
    if (proc_ID == 11)
      t = matrix_4_ved_vegggd(); 
    if (proc_ID == 12)
      t = matrix_4_veux_vegggux(); 
    if (proc_ID == 13)
      t = matrix_4_vedx_vegggdx(); 
    if (proc_ID == 14)
      t = matrix_4_epd_vexgggu(); 
    if (proc_ID == 15)
      t = matrix_4_epux_vexgggdx(); 
    if (proc_ID == 16)
      t = matrix_4_vexu_vexgggu(); 
    if (proc_ID == 17)
      t = matrix_4_vexd_vexgggd(); 
    if (proc_ID == 18)
      t = matrix_4_vexux_vexgggux(); 
    if (proc_ID == 19)
      t = matrix_4_vexdx_vexgggdx(); 

    // Store which helicities give non-zero result
    if ((ntry[proc_ID] < max_tries) && t != 0. && !goodhel[proc_ID][ihel])
      goodhel[proc_ID][ihel] = true; 

    // Aggregate results
    if (user_ihel == -1 || user_ihel == ihel)
    {
      if (user_icol == -1)
      {
        result = result + t; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][0] += t; 
          for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
          {
            all_results[proc_ID][0][i + 1] += jamp2[proc_ID][i]; 
          }
        }
        all_results[proc_ID][ihel + 1][0] += t; 
        for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
        {
          all_results[proc_ID][ihel + 1][i + 1] += jamp2[proc_ID][i]; 
        }
      }
      else
      {
        result = result + jamp2[proc_ID][user_icol]; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][user_icol + 1] += jamp2[proc_ID][user_icol]; 
        }
        all_results[proc_ID][ihel + 1][user_icol + 1] +=
            jamp2[proc_ID][user_icol];
      }
    }

  }

  // Normalize results with the identical particle factor
  if (include_symmetry_factors)
  {
    result = result/denom_iden[proc_ID]; 
  }
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      if (include_symmetry_factors)
      {
        all_results[proc_ID][ihel + 1][icolor + 1] /= denom_iden[proc_ID]; 
      }
    }
  }


  // Normalize when when summing+averaging over helicity configurations
  if (user_ihel == -1 && include_helicity_averaging_factors)
  {
    result /= denom_hels[proc_ID]; 
    if (user_icol == -1)
    {
      all_results[proc_ID][0][0] /= denom_hels[proc_ID]; 
      for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
      {
        all_results[proc_ID][0][i + 1] /= denom_hels[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][0][user_icol + 1] /= denom_hels[proc_ID]; 
    }
  }

  // Normalize when summing+averaging over color configurations
  if (user_icol == -1 && include_color_averaging_factors)
  {
    result /= denom_colors[proc_ID]; 
    if (user_ihel == -1)
    {
      all_results[proc_ID][0][0] /= denom_colors[proc_ID]; 
      for (unsigned int i = 0; i < ncomb; i++ )
      {
        all_results[proc_ID][i + 1][0] /= denom_colors[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][user_ihel + 1][0] /= denom_colors[proc_ID]; 
    }
  }

  // Reinstate previous values of mME
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (saved_mME[i] < 0.0)
    {
      mME[i] = saved_mME[i]; 
    }
  }

  // Finally return it
  return result; 
}

//==========================================================================
// Private class member functions

//--------------------------------------------------------------------------
// Evaluate |M|^2 for each subprocess

void PY8MEs_R4_P6_sm_lq_lgggq::calculate_wavefunctions(const int hel[])
{
  // Calculate wavefunctions for all processes
  // Calculate all wavefunctions
  ixxxxx(p[perm[0]], mME[0], hel[0], +1, w[0]); 
  ixxxxx(p[perm[1]], mME[1], hel[1], +1, w[1]); 
  oxxxxx(p[perm[2]], mME[2], hel[2], +1, w[2]); 
  vxxxxx(p[perm[3]], mME[3], hel[3], +1, w[3]); 
  vxxxxx(p[perm[4]], mME[4], hel[4], +1, w[4]); 
  vxxxxx(p[perm[5]], mME[5], hel[5], +1, w[5]); 
  oxxxxx(p[perm[6]], mME[6], hel[6], +1, w[6]); 
  FFV1P0_3(w[0], w[2], pars->GC_3, pars->ZERO, pars->ZERO, w[7]); 
  VVV1P0_1(w[3], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[8]); 
  FFV1_1(w[6], w[7], pars->GC_2, pars->ZERO, pars->ZERO, w[9]); 
  VVV1P0_1(w[8], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[10]); 
  FFV1_2(w[1], w[8], pars->GC_11, pars->ZERO, pars->ZERO, w[11]); 
  FFV1_2(w[1], w[7], pars->GC_2, pars->ZERO, pars->ZERO, w[12]); 
  FFV1_1(w[6], w[8], pars->GC_11, pars->ZERO, pars->ZERO, w[13]); 
  FFV2_4_3(w[0], w[2], pars->GC_50, pars->GC_59, pars->mdl_MZ, pars->mdl_WZ,
      w[14]);
  FFV2_5_1(w[6], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[15]);
  FFV2_5_2(w[1], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[16]);
  FFV1_1(w[6], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[17]); 
  FFV1_1(w[17], w[7], pars->GC_2, pars->ZERO, pars->ZERO, w[18]); 
  FFV2_5_1(w[17], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[19]);
  FFV1_2(w[1], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[20]); 
  FFV1_2(w[20], w[7], pars->GC_2, pars->ZERO, pars->ZERO, w[21]); 
  FFV2_5_2(w[20], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[22]);
  VVV1P0_1(w[3], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[23]); 
  VVV1P0_1(w[23], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[24]); 
  FFV1_2(w[1], w[23], pars->GC_11, pars->ZERO, pars->ZERO, w[25]); 
  FFV1_1(w[6], w[23], pars->GC_11, pars->ZERO, pars->ZERO, w[26]); 
  FFV1_1(w[6], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[27]); 
  FFV1_1(w[27], w[7], pars->GC_2, pars->ZERO, pars->ZERO, w[28]); 
  FFV2_5_1(w[27], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[29]);
  FFV1_2(w[1], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[30]); 
  FFV1_2(w[30], w[7], pars->GC_2, pars->ZERO, pars->ZERO, w[31]); 
  FFV2_5_2(w[30], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[32]);
  FFV1_1(w[6], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[33]); 
  FFV1_1(w[33], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[34]); 
  FFV1_1(w[33], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[35]); 
  VVV1P0_1(w[4], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[36]); 
  FFV1_1(w[33], w[7], pars->GC_2, pars->ZERO, pars->ZERO, w[37]); 
  FFV2_5_1(w[33], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[38]);
  FFV1_2(w[1], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[39]); 
  FFV1_2(w[39], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[40]); 
  FFV1_2(w[39], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[41]); 
  FFV1_2(w[39], w[7], pars->GC_2, pars->ZERO, pars->ZERO, w[42]); 
  FFV2_5_2(w[39], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[43]);
  VVV1P0_1(w[3], w[36], pars->GC_10, pars->ZERO, pars->ZERO, w[44]); 
  FFV1_2(w[1], w[36], pars->GC_11, pars->ZERO, pars->ZERO, w[45]); 
  FFV1_1(w[6], w[36], pars->GC_11, pars->ZERO, pars->ZERO, w[46]); 
  FFV1_1(w[27], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[47]); 
  FFV1_1(w[27], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[48]); 
  FFV1_2(w[30], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[49]); 
  FFV1_2(w[30], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[50]); 
  FFV1_1(w[17], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[51]); 
  FFV1_1(w[17], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[52]); 
  FFV1_2(w[20], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[53]); 
  FFV1_2(w[20], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[54]); 
  VVVV1P0_1(w[3], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[55]); 
  VVVV3P0_1(w[3], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[56]); 
  VVVV4P0_1(w[3], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[57]); 
  FFV1_1(w[6], w[7], pars->GC_1, pars->ZERO, pars->ZERO, w[58]); 
  FFV1_2(w[1], w[7], pars->GC_1, pars->ZERO, pars->ZERO, w[59]); 
  FFV2_3_1(w[6], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[60]);
  FFV2_3_2(w[1], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[61]);
  FFV1_1(w[17], w[7], pars->GC_1, pars->ZERO, pars->ZERO, w[62]); 
  FFV2_3_1(w[17], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[63]);
  FFV1_2(w[20], w[7], pars->GC_1, pars->ZERO, pars->ZERO, w[64]); 
  FFV2_3_2(w[20], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[65]);
  FFV1_1(w[27], w[7], pars->GC_1, pars->ZERO, pars->ZERO, w[66]); 
  FFV2_3_1(w[27], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[67]);
  FFV1_2(w[30], w[7], pars->GC_1, pars->ZERO, pars->ZERO, w[68]); 
  FFV2_3_2(w[30], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[69]);
  FFV1_1(w[33], w[7], pars->GC_1, pars->ZERO, pars->ZERO, w[70]); 
  FFV2_3_1(w[33], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[71]);
  FFV1_2(w[39], w[7], pars->GC_1, pars->ZERO, pars->ZERO, w[72]); 
  FFV2_3_2(w[39], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[73]);
  oxxxxx(p[perm[1]], mME[1], hel[1], -1, w[74]); 
  ixxxxx(p[perm[6]], mME[6], hel[6], -1, w[75]); 
  FFV1_1(w[74], w[7], pars->GC_2, pars->ZERO, pars->ZERO, w[76]); 
  FFV1_2(w[75], w[8], pars->GC_11, pars->ZERO, pars->ZERO, w[77]); 
  FFV1_2(w[75], w[7], pars->GC_2, pars->ZERO, pars->ZERO, w[78]); 
  FFV1_1(w[74], w[8], pars->GC_11, pars->ZERO, pars->ZERO, w[79]); 
  FFV2_5_1(w[74], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[80]);
  FFV2_5_2(w[75], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[81]);
  FFV1_1(w[74], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[82]); 
  FFV1_1(w[82], w[7], pars->GC_2, pars->ZERO, pars->ZERO, w[83]); 
  FFV2_5_1(w[82], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[84]);
  FFV1_2(w[75], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[85]); 
  FFV1_2(w[85], w[7], pars->GC_2, pars->ZERO, pars->ZERO, w[86]); 
  FFV2_5_2(w[85], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[87]);
  FFV1_2(w[75], w[23], pars->GC_11, pars->ZERO, pars->ZERO, w[88]); 
  FFV1_1(w[74], w[23], pars->GC_11, pars->ZERO, pars->ZERO, w[89]); 
  FFV1_1(w[74], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[90]); 
  FFV1_1(w[90], w[7], pars->GC_2, pars->ZERO, pars->ZERO, w[91]); 
  FFV2_5_1(w[90], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[92]);
  FFV1_2(w[75], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[93]); 
  FFV1_2(w[93], w[7], pars->GC_2, pars->ZERO, pars->ZERO, w[94]); 
  FFV2_5_2(w[93], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[95]);
  FFV1_1(w[74], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[96]); 
  FFV1_1(w[96], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[97]); 
  FFV1_1(w[96], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[98]); 
  FFV1_1(w[96], w[7], pars->GC_2, pars->ZERO, pars->ZERO, w[99]); 
  FFV2_5_1(w[96], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[100]);
  FFV1_2(w[75], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[101]); 
  FFV1_2(w[101], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[102]); 
  FFV1_2(w[101], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[103]); 
  FFV1_2(w[101], w[7], pars->GC_2, pars->ZERO, pars->ZERO, w[104]); 
  FFV2_5_2(w[101], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[105]);
  FFV1_2(w[75], w[36], pars->GC_11, pars->ZERO, pars->ZERO, w[106]); 
  FFV1_1(w[74], w[36], pars->GC_11, pars->ZERO, pars->ZERO, w[107]); 
  FFV1_1(w[90], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[108]); 
  FFV1_1(w[90], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[109]); 
  FFV1_2(w[93], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[110]); 
  FFV1_2(w[93], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[111]); 
  FFV1_1(w[82], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[112]); 
  FFV1_1(w[82], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[113]); 
  FFV1_2(w[85], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[114]); 
  FFV1_2(w[85], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[115]); 
  FFV1_1(w[74], w[7], pars->GC_1, pars->ZERO, pars->ZERO, w[116]); 
  FFV1_2(w[75], w[7], pars->GC_1, pars->ZERO, pars->ZERO, w[117]); 
  FFV2_3_1(w[74], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[118]);
  FFV2_3_2(w[75], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[119]);
  FFV1_1(w[82], w[7], pars->GC_1, pars->ZERO, pars->ZERO, w[120]); 
  FFV2_3_1(w[82], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[121]);
  FFV1_2(w[85], w[7], pars->GC_1, pars->ZERO, pars->ZERO, w[122]); 
  FFV2_3_2(w[85], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[123]);
  FFV1_1(w[90], w[7], pars->GC_1, pars->ZERO, pars->ZERO, w[124]); 
  FFV2_3_1(w[90], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[125]);
  FFV1_2(w[93], w[7], pars->GC_1, pars->ZERO, pars->ZERO, w[126]); 
  FFV2_3_2(w[93], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[127]);
  FFV1_1(w[96], w[7], pars->GC_1, pars->ZERO, pars->ZERO, w[128]); 
  FFV2_3_1(w[96], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[129]);
  FFV1_2(w[101], w[7], pars->GC_1, pars->ZERO, pars->ZERO, w[130]); 
  FFV2_3_2(w[101], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[131]);
  oxxxxx(p[perm[0]], mME[0], hel[0], -1, w[132]); 
  ixxxxx(p[perm[2]], mME[2], hel[2], -1, w[133]); 
  FFV1P0_3(w[133], w[132], pars->GC_3, pars->ZERO, pars->ZERO, w[134]); 
  FFV1_1(w[6], w[134], pars->GC_2, pars->ZERO, pars->ZERO, w[135]); 
  FFV1_2(w[1], w[134], pars->GC_2, pars->ZERO, pars->ZERO, w[136]); 
  FFV2_4_3(w[133], w[132], pars->GC_50, pars->GC_59, pars->mdl_MZ,
      pars->mdl_WZ, w[137]);
  FFV2_5_1(w[6], w[137], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[138]);
  FFV2_5_2(w[1], w[137], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[139]);
  FFV1_1(w[17], w[134], pars->GC_2, pars->ZERO, pars->ZERO, w[140]); 
  FFV2_5_1(w[17], w[137], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[141]);
  FFV1_2(w[20], w[134], pars->GC_2, pars->ZERO, pars->ZERO, w[142]); 
  FFV2_5_2(w[20], w[137], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[143]);
  FFV1_1(w[27], w[134], pars->GC_2, pars->ZERO, pars->ZERO, w[144]); 
  FFV2_5_1(w[27], w[137], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[145]);
  FFV1_2(w[30], w[134], pars->GC_2, pars->ZERO, pars->ZERO, w[146]); 
  FFV2_5_2(w[30], w[137], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[147]);
  FFV1_1(w[33], w[134], pars->GC_2, pars->ZERO, pars->ZERO, w[148]); 
  FFV2_5_1(w[33], w[137], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[149]);
  FFV1_2(w[39], w[134], pars->GC_2, pars->ZERO, pars->ZERO, w[150]); 
  FFV2_5_2(w[39], w[137], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[151]);
  FFV1_1(w[6], w[134], pars->GC_1, pars->ZERO, pars->ZERO, w[152]); 
  FFV1_2(w[1], w[134], pars->GC_1, pars->ZERO, pars->ZERO, w[153]); 
  FFV2_3_1(w[6], w[137], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[154]);
  FFV2_3_2(w[1], w[137], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[155]);
  FFV1_1(w[17], w[134], pars->GC_1, pars->ZERO, pars->ZERO, w[156]); 
  FFV2_3_1(w[17], w[137], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[157]);
  FFV1_2(w[20], w[134], pars->GC_1, pars->ZERO, pars->ZERO, w[158]); 
  FFV2_3_2(w[20], w[137], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[159]);
  FFV1_1(w[27], w[134], pars->GC_1, pars->ZERO, pars->ZERO, w[160]); 
  FFV2_3_1(w[27], w[137], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[161]);
  FFV1_2(w[30], w[134], pars->GC_1, pars->ZERO, pars->ZERO, w[162]); 
  FFV2_3_2(w[30], w[137], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[163]);
  FFV1_1(w[33], w[134], pars->GC_1, pars->ZERO, pars->ZERO, w[164]); 
  FFV2_3_1(w[33], w[137], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[165]);
  FFV1_2(w[39], w[134], pars->GC_1, pars->ZERO, pars->ZERO, w[166]); 
  FFV2_3_2(w[39], w[137], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[167]);
  FFV1_1(w[74], w[134], pars->GC_2, pars->ZERO, pars->ZERO, w[168]); 
  FFV1_2(w[75], w[134], pars->GC_2, pars->ZERO, pars->ZERO, w[169]); 
  FFV2_5_1(w[74], w[137], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[170]);
  FFV2_5_2(w[75], w[137], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[171]);
  FFV1_1(w[82], w[134], pars->GC_2, pars->ZERO, pars->ZERO, w[172]); 
  FFV2_5_1(w[82], w[137], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[173]);
  FFV1_2(w[85], w[134], pars->GC_2, pars->ZERO, pars->ZERO, w[174]); 
  FFV2_5_2(w[85], w[137], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[175]);
  FFV1_1(w[90], w[134], pars->GC_2, pars->ZERO, pars->ZERO, w[176]); 
  FFV2_5_1(w[90], w[137], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[177]);
  FFV1_2(w[93], w[134], pars->GC_2, pars->ZERO, pars->ZERO, w[178]); 
  FFV2_5_2(w[93], w[137], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[179]);
  FFV1_1(w[96], w[134], pars->GC_2, pars->ZERO, pars->ZERO, w[180]); 
  FFV2_5_1(w[96], w[137], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[181]);
  FFV1_2(w[101], w[134], pars->GC_2, pars->ZERO, pars->ZERO, w[182]); 
  FFV2_5_2(w[101], w[137], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[183]);
  FFV1_1(w[74], w[134], pars->GC_1, pars->ZERO, pars->ZERO, w[184]); 
  FFV1_2(w[75], w[134], pars->GC_1, pars->ZERO, pars->ZERO, w[185]); 
  FFV2_3_1(w[74], w[137], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[186]);
  FFV2_3_2(w[75], w[137], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[187]);
  FFV1_1(w[82], w[134], pars->GC_1, pars->ZERO, pars->ZERO, w[188]); 
  FFV2_3_1(w[82], w[137], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[189]);
  FFV1_2(w[85], w[134], pars->GC_1, pars->ZERO, pars->ZERO, w[190]); 
  FFV2_3_2(w[85], w[137], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[191]);
  FFV1_1(w[90], w[134], pars->GC_1, pars->ZERO, pars->ZERO, w[192]); 
  FFV2_3_1(w[90], w[137], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[193]);
  FFV1_2(w[93], w[134], pars->GC_1, pars->ZERO, pars->ZERO, w[194]); 
  FFV2_3_2(w[93], w[137], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[195]);
  FFV1_1(w[96], w[134], pars->GC_1, pars->ZERO, pars->ZERO, w[196]); 
  FFV2_3_1(w[96], w[137], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[197]);
  FFV1_2(w[101], w[134], pars->GC_1, pars->ZERO, pars->ZERO, w[198]); 
  FFV2_3_2(w[101], w[137], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[199]);
  FFV2_3(w[0], w[2], pars->GC_100, pars->mdl_MW, pars->mdl_WW, w[200]); 
  FFV2_1(w[6], w[200], pars->GC_100, pars->ZERO, pars->ZERO, w[201]); 
  FFV2_2(w[1], w[200], pars->GC_100, pars->ZERO, pars->ZERO, w[202]); 
  FFV2_1(w[17], w[200], pars->GC_100, pars->ZERO, pars->ZERO, w[203]); 
  FFV2_2(w[20], w[200], pars->GC_100, pars->ZERO, pars->ZERO, w[204]); 
  FFV2_1(w[27], w[200], pars->GC_100, pars->ZERO, pars->ZERO, w[205]); 
  FFV2_2(w[30], w[200], pars->GC_100, pars->ZERO, pars->ZERO, w[206]); 
  FFV2_1(w[33], w[200], pars->GC_100, pars->ZERO, pars->ZERO, w[207]); 
  FFV2_2(w[39], w[200], pars->GC_100, pars->ZERO, pars->ZERO, w[208]); 
  FFV2_1(w[74], w[200], pars->GC_100, pars->ZERO, pars->ZERO, w[209]); 
  FFV2_2(w[75], w[200], pars->GC_100, pars->ZERO, pars->ZERO, w[210]); 
  FFV2_1(w[82], w[200], pars->GC_100, pars->ZERO, pars->ZERO, w[211]); 
  FFV2_2(w[85], w[200], pars->GC_100, pars->ZERO, pars->ZERO, w[212]); 
  FFV2_1(w[90], w[200], pars->GC_100, pars->ZERO, pars->ZERO, w[213]); 
  FFV2_2(w[93], w[200], pars->GC_100, pars->ZERO, pars->ZERO, w[214]); 
  FFV2_1(w[96], w[200], pars->GC_100, pars->ZERO, pars->ZERO, w[215]); 
  FFV2_2(w[101], w[200], pars->GC_100, pars->ZERO, pars->ZERO, w[216]); 
  FFV2_3(w[0], w[2], pars->GC_62, pars->mdl_MZ, pars->mdl_WZ, w[217]); 
  FFV2_5_1(w[6], w[217], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[218]);
  FFV2_5_2(w[1], w[217], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[219]);
  FFV2_5_1(w[17], w[217], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[220]);
  FFV2_5_2(w[20], w[217], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[221]);
  FFV2_5_1(w[27], w[217], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[222]);
  FFV2_5_2(w[30], w[217], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[223]);
  FFV2_5_1(w[33], w[217], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[224]);
  FFV2_5_2(w[39], w[217], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[225]);
  FFV2_3_1(w[6], w[217], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[226]);
  FFV2_3_2(w[1], w[217], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[227]);
  FFV2_3_1(w[17], w[217], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[228]);
  FFV2_3_2(w[20], w[217], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[229]);
  FFV2_3_1(w[27], w[217], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[230]);
  FFV2_3_2(w[30], w[217], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[231]);
  FFV2_3_1(w[33], w[217], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[232]);
  FFV2_3_2(w[39], w[217], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[233]);
  FFV2_5_1(w[74], w[217], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[234]);
  FFV2_5_2(w[75], w[217], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[235]);
  FFV2_5_1(w[82], w[217], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[236]);
  FFV2_5_2(w[85], w[217], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[237]);
  FFV2_5_1(w[90], w[217], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[238]);
  FFV2_5_2(w[93], w[217], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[239]);
  FFV2_5_1(w[96], w[217], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[240]);
  FFV2_5_2(w[101], w[217], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[241]);
  FFV2_3_1(w[74], w[217], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[242]);
  FFV2_3_2(w[75], w[217], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[243]);
  FFV2_3_1(w[82], w[217], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[244]);
  FFV2_3_2(w[85], w[217], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[245]);
  FFV2_3_1(w[90], w[217], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[246]);
  FFV2_3_2(w[93], w[217], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[247]);
  FFV2_3_1(w[96], w[217], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[248]);
  FFV2_3_2(w[101], w[217], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[249]);
  FFV2_3(w[133], w[132], pars->GC_100, pars->mdl_MW, pars->mdl_WW, w[250]); 
  FFV2_1(w[6], w[250], pars->GC_100, pars->ZERO, pars->ZERO, w[251]); 
  FFV2_2(w[1], w[250], pars->GC_100, pars->ZERO, pars->ZERO, w[252]); 
  FFV2_1(w[17], w[250], pars->GC_100, pars->ZERO, pars->ZERO, w[253]); 
  FFV2_2(w[20], w[250], pars->GC_100, pars->ZERO, pars->ZERO, w[254]); 
  FFV2_1(w[27], w[250], pars->GC_100, pars->ZERO, pars->ZERO, w[255]); 
  FFV2_2(w[30], w[250], pars->GC_100, pars->ZERO, pars->ZERO, w[256]); 
  FFV2_1(w[33], w[250], pars->GC_100, pars->ZERO, pars->ZERO, w[257]); 
  FFV2_2(w[39], w[250], pars->GC_100, pars->ZERO, pars->ZERO, w[258]); 
  FFV2_1(w[74], w[250], pars->GC_100, pars->ZERO, pars->ZERO, w[259]); 
  FFV2_2(w[75], w[250], pars->GC_100, pars->ZERO, pars->ZERO, w[260]); 
  FFV2_1(w[82], w[250], pars->GC_100, pars->ZERO, pars->ZERO, w[261]); 
  FFV2_2(w[85], w[250], pars->GC_100, pars->ZERO, pars->ZERO, w[262]); 
  FFV2_1(w[90], w[250], pars->GC_100, pars->ZERO, pars->ZERO, w[263]); 
  FFV2_2(w[93], w[250], pars->GC_100, pars->ZERO, pars->ZERO, w[264]); 
  FFV2_1(w[96], w[250], pars->GC_100, pars->ZERO, pars->ZERO, w[265]); 
  FFV2_2(w[101], w[250], pars->GC_100, pars->ZERO, pars->ZERO, w[266]); 
  FFV2_3(w[133], w[132], pars->GC_62, pars->mdl_MZ, pars->mdl_WZ, w[267]); 
  FFV2_5_1(w[6], w[267], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[268]);
  FFV2_5_2(w[1], w[267], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[269]);
  FFV2_5_1(w[17], w[267], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[270]);
  FFV2_5_2(w[20], w[267], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[271]);
  FFV2_5_1(w[27], w[267], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[272]);
  FFV2_5_2(w[30], w[267], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[273]);
  FFV2_5_1(w[33], w[267], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[274]);
  FFV2_5_2(w[39], w[267], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[275]);
  FFV2_3_1(w[6], w[267], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[276]);
  FFV2_3_2(w[1], w[267], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[277]);
  FFV2_3_1(w[17], w[267], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[278]);
  FFV2_3_2(w[20], w[267], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[279]);
  FFV2_3_1(w[27], w[267], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[280]);
  FFV2_3_2(w[30], w[267], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[281]);
  FFV2_3_1(w[33], w[267], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[282]);
  FFV2_3_2(w[39], w[267], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[283]);
  FFV2_5_1(w[74], w[267], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[284]);
  FFV2_5_2(w[75], w[267], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[285]);
  FFV2_5_1(w[82], w[267], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[286]);
  FFV2_5_2(w[85], w[267], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[287]);
  FFV2_5_1(w[90], w[267], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[288]);
  FFV2_5_2(w[93], w[267], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[289]);
  FFV2_5_1(w[96], w[267], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[290]);
  FFV2_5_2(w[101], w[267], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[291]);
  FFV2_3_1(w[74], w[267], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[292]);
  FFV2_3_2(w[75], w[267], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[293]);
  FFV2_3_1(w[82], w[267], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[294]);
  FFV2_3_2(w[85], w[267], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[295]);
  FFV2_3_1(w[90], w[267], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[296]);
  FFV2_3_2(w[93], w[267], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[297]);
  FFV2_3_1(w[96], w[267], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[298]);
  FFV2_3_2(w[101], w[267], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[299]);

  // Calculate all amplitudes
  // Amplitude(s) for diagram number 0
  FFV1_0(w[1], w[9], w[10], pars->GC_11, amp[0]); 
  FFV1_0(w[11], w[9], w[5], pars->GC_11, amp[1]); 
  FFV1_0(w[12], w[6], w[10], pars->GC_11, amp[2]); 
  FFV1_0(w[12], w[13], w[5], pars->GC_11, amp[3]); 
  FFV1_0(w[1], w[15], w[10], pars->GC_11, amp[4]); 
  FFV1_0(w[11], w[15], w[5], pars->GC_11, amp[5]); 
  FFV1_0(w[16], w[6], w[10], pars->GC_11, amp[6]); 
  FFV1_0(w[16], w[13], w[5], pars->GC_11, amp[7]); 
  FFV1_0(w[1], w[18], w[8], pars->GC_11, amp[8]); 
  FFV1_0(w[12], w[17], w[8], pars->GC_11, amp[9]); 
  FFV1_0(w[1], w[19], w[8], pars->GC_11, amp[10]); 
  FFV1_0(w[16], w[17], w[8], pars->GC_11, amp[11]); 
  FFV1_0(w[21], w[6], w[8], pars->GC_11, amp[12]); 
  FFV1_0(w[20], w[9], w[8], pars->GC_11, amp[13]); 
  FFV1_0(w[22], w[6], w[8], pars->GC_11, amp[14]); 
  FFV1_0(w[20], w[15], w[8], pars->GC_11, amp[15]); 
  FFV1_0(w[1], w[9], w[24], pars->GC_11, amp[16]); 
  FFV1_0(w[25], w[9], w[4], pars->GC_11, amp[17]); 
  FFV1_0(w[12], w[6], w[24], pars->GC_11, amp[18]); 
  FFV1_0(w[12], w[26], w[4], pars->GC_11, amp[19]); 
  FFV1_0(w[1], w[15], w[24], pars->GC_11, amp[20]); 
  FFV1_0(w[25], w[15], w[4], pars->GC_11, amp[21]); 
  FFV1_0(w[16], w[6], w[24], pars->GC_11, amp[22]); 
  FFV1_0(w[16], w[26], w[4], pars->GC_11, amp[23]); 
  FFV1_0(w[1], w[28], w[23], pars->GC_11, amp[24]); 
  FFV1_0(w[12], w[27], w[23], pars->GC_11, amp[25]); 
  FFV1_0(w[1], w[29], w[23], pars->GC_11, amp[26]); 
  FFV1_0(w[16], w[27], w[23], pars->GC_11, amp[27]); 
  FFV1_0(w[31], w[6], w[23], pars->GC_11, amp[28]); 
  FFV1_0(w[30], w[9], w[23], pars->GC_11, amp[29]); 
  FFV1_0(w[32], w[6], w[23], pars->GC_11, amp[30]); 
  FFV1_0(w[30], w[15], w[23], pars->GC_11, amp[31]); 
  FFV1_0(w[12], w[34], w[5], pars->GC_11, amp[32]); 
  FFV1_0(w[12], w[35], w[4], pars->GC_11, amp[33]); 
  FFV1_0(w[16], w[34], w[5], pars->GC_11, amp[34]); 
  FFV1_0(w[16], w[35], w[4], pars->GC_11, amp[35]); 
  FFV1_0(w[1], w[37], w[36], pars->GC_11, amp[36]); 
  FFV1_0(w[12], w[33], w[36], pars->GC_11, amp[37]); 
  FFV1_0(w[1], w[38], w[36], pars->GC_11, amp[38]); 
  FFV1_0(w[16], w[33], w[36], pars->GC_11, amp[39]); 
  FFV1_0(w[30], w[37], w[5], pars->GC_11, amp[40]); 
  FFV1_0(w[31], w[33], w[5], pars->GC_11, amp[41]); 
  FFV1_0(w[30], w[38], w[5], pars->GC_11, amp[42]); 
  FFV1_0(w[32], w[33], w[5], pars->GC_11, amp[43]); 
  FFV1_0(w[20], w[37], w[4], pars->GC_11, amp[44]); 
  FFV1_0(w[21], w[33], w[4], pars->GC_11, amp[45]); 
  FFV1_0(w[20], w[38], w[4], pars->GC_11, amp[46]); 
  FFV1_0(w[22], w[33], w[4], pars->GC_11, amp[47]); 
  FFV1_0(w[40], w[9], w[5], pars->GC_11, amp[48]); 
  FFV1_0(w[41], w[9], w[4], pars->GC_11, amp[49]); 
  FFV1_0(w[40], w[15], w[5], pars->GC_11, amp[50]); 
  FFV1_0(w[41], w[15], w[4], pars->GC_11, amp[51]); 
  FFV1_0(w[42], w[6], w[36], pars->GC_11, amp[52]); 
  FFV1_0(w[39], w[9], w[36], pars->GC_11, amp[53]); 
  FFV1_0(w[43], w[6], w[36], pars->GC_11, amp[54]); 
  FFV1_0(w[39], w[15], w[36], pars->GC_11, amp[55]); 
  FFV1_0(w[42], w[27], w[5], pars->GC_11, amp[56]); 
  FFV1_0(w[39], w[28], w[5], pars->GC_11, amp[57]); 
  FFV1_0(w[43], w[27], w[5], pars->GC_11, amp[58]); 
  FFV1_0(w[39], w[29], w[5], pars->GC_11, amp[59]); 
  FFV1_0(w[42], w[17], w[4], pars->GC_11, amp[60]); 
  FFV1_0(w[39], w[18], w[4], pars->GC_11, amp[61]); 
  FFV1_0(w[43], w[17], w[4], pars->GC_11, amp[62]); 
  FFV1_0(w[39], w[19], w[4], pars->GC_11, amp[63]); 
  FFV1_0(w[1], w[9], w[44], pars->GC_11, amp[64]); 
  FFV1_0(w[45], w[9], w[3], pars->GC_11, amp[65]); 
  FFV1_0(w[12], w[6], w[44], pars->GC_11, amp[66]); 
  FFV1_0(w[12], w[46], w[3], pars->GC_11, amp[67]); 
  FFV1_0(w[1], w[15], w[44], pars->GC_11, amp[68]); 
  FFV1_0(w[45], w[15], w[3], pars->GC_11, amp[69]); 
  FFV1_0(w[16], w[6], w[44], pars->GC_11, amp[70]); 
  FFV1_0(w[16], w[46], w[3], pars->GC_11, amp[71]); 
  FFV1_0(w[12], w[47], w[5], pars->GC_11, amp[72]); 
  FFV1_0(w[12], w[48], w[3], pars->GC_11, amp[73]); 
  FFV1_0(w[16], w[47], w[5], pars->GC_11, amp[74]); 
  FFV1_0(w[16], w[48], w[3], pars->GC_11, amp[75]); 
  FFV1_0(w[20], w[28], w[3], pars->GC_11, amp[76]); 
  FFV1_0(w[21], w[27], w[3], pars->GC_11, amp[77]); 
  FFV1_0(w[20], w[29], w[3], pars->GC_11, amp[78]); 
  FFV1_0(w[22], w[27], w[3], pars->GC_11, amp[79]); 
  FFV1_0(w[49], w[9], w[5], pars->GC_11, amp[80]); 
  FFV1_0(w[50], w[9], w[3], pars->GC_11, amp[81]); 
  FFV1_0(w[49], w[15], w[5], pars->GC_11, amp[82]); 
  FFV1_0(w[50], w[15], w[3], pars->GC_11, amp[83]); 
  FFV1_0(w[31], w[17], w[3], pars->GC_11, amp[84]); 
  FFV1_0(w[30], w[18], w[3], pars->GC_11, amp[85]); 
  FFV1_0(w[32], w[17], w[3], pars->GC_11, amp[86]); 
  FFV1_0(w[30], w[19], w[3], pars->GC_11, amp[87]); 
  FFV1_0(w[12], w[51], w[4], pars->GC_11, amp[88]); 
  FFV1_0(w[12], w[52], w[3], pars->GC_11, amp[89]); 
  FFV1_0(w[16], w[51], w[4], pars->GC_11, amp[90]); 
  FFV1_0(w[16], w[52], w[3], pars->GC_11, amp[91]); 
  FFV1_0(w[53], w[9], w[4], pars->GC_11, amp[92]); 
  FFV1_0(w[54], w[9], w[3], pars->GC_11, amp[93]); 
  FFV1_0(w[53], w[15], w[4], pars->GC_11, amp[94]); 
  FFV1_0(w[54], w[15], w[3], pars->GC_11, amp[95]); 
  FFV1_0(w[1], w[9], w[55], pars->GC_11, amp[96]); 
  FFV1_0(w[1], w[9], w[56], pars->GC_11, amp[97]); 
  FFV1_0(w[1], w[9], w[57], pars->GC_11, amp[98]); 
  FFV1_0(w[12], w[6], w[55], pars->GC_11, amp[99]); 
  FFV1_0(w[12], w[6], w[56], pars->GC_11, amp[100]); 
  FFV1_0(w[12], w[6], w[57], pars->GC_11, amp[101]); 
  FFV1_0(w[1], w[15], w[55], pars->GC_11, amp[102]); 
  FFV1_0(w[1], w[15], w[56], pars->GC_11, amp[103]); 
  FFV1_0(w[1], w[15], w[57], pars->GC_11, amp[104]); 
  FFV1_0(w[16], w[6], w[55], pars->GC_11, amp[105]); 
  FFV1_0(w[16], w[6], w[56], pars->GC_11, amp[106]); 
  FFV1_0(w[16], w[6], w[57], pars->GC_11, amp[107]); 
  FFV1_0(w[1], w[58], w[10], pars->GC_11, amp[108]); 
  FFV1_0(w[11], w[58], w[5], pars->GC_11, amp[109]); 
  FFV1_0(w[59], w[6], w[10], pars->GC_11, amp[110]); 
  FFV1_0(w[59], w[13], w[5], pars->GC_11, amp[111]); 
  FFV1_0(w[1], w[60], w[10], pars->GC_11, amp[112]); 
  FFV1_0(w[11], w[60], w[5], pars->GC_11, amp[113]); 
  FFV1_0(w[61], w[6], w[10], pars->GC_11, amp[114]); 
  FFV1_0(w[61], w[13], w[5], pars->GC_11, amp[115]); 
  FFV1_0(w[1], w[62], w[8], pars->GC_11, amp[116]); 
  FFV1_0(w[59], w[17], w[8], pars->GC_11, amp[117]); 
  FFV1_0(w[1], w[63], w[8], pars->GC_11, amp[118]); 
  FFV1_0(w[61], w[17], w[8], pars->GC_11, amp[119]); 
  FFV1_0(w[64], w[6], w[8], pars->GC_11, amp[120]); 
  FFV1_0(w[20], w[58], w[8], pars->GC_11, amp[121]); 
  FFV1_0(w[65], w[6], w[8], pars->GC_11, amp[122]); 
  FFV1_0(w[20], w[60], w[8], pars->GC_11, amp[123]); 
  FFV1_0(w[1], w[58], w[24], pars->GC_11, amp[124]); 
  FFV1_0(w[25], w[58], w[4], pars->GC_11, amp[125]); 
  FFV1_0(w[59], w[6], w[24], pars->GC_11, amp[126]); 
  FFV1_0(w[59], w[26], w[4], pars->GC_11, amp[127]); 
  FFV1_0(w[1], w[60], w[24], pars->GC_11, amp[128]); 
  FFV1_0(w[25], w[60], w[4], pars->GC_11, amp[129]); 
  FFV1_0(w[61], w[6], w[24], pars->GC_11, amp[130]); 
  FFV1_0(w[61], w[26], w[4], pars->GC_11, amp[131]); 
  FFV1_0(w[1], w[66], w[23], pars->GC_11, amp[132]); 
  FFV1_0(w[59], w[27], w[23], pars->GC_11, amp[133]); 
  FFV1_0(w[1], w[67], w[23], pars->GC_11, amp[134]); 
  FFV1_0(w[61], w[27], w[23], pars->GC_11, amp[135]); 
  FFV1_0(w[68], w[6], w[23], pars->GC_11, amp[136]); 
  FFV1_0(w[30], w[58], w[23], pars->GC_11, amp[137]); 
  FFV1_0(w[69], w[6], w[23], pars->GC_11, amp[138]); 
  FFV1_0(w[30], w[60], w[23], pars->GC_11, amp[139]); 
  FFV1_0(w[59], w[34], w[5], pars->GC_11, amp[140]); 
  FFV1_0(w[59], w[35], w[4], pars->GC_11, amp[141]); 
  FFV1_0(w[61], w[34], w[5], pars->GC_11, amp[142]); 
  FFV1_0(w[61], w[35], w[4], pars->GC_11, amp[143]); 
  FFV1_0(w[1], w[70], w[36], pars->GC_11, amp[144]); 
  FFV1_0(w[59], w[33], w[36], pars->GC_11, amp[145]); 
  FFV1_0(w[1], w[71], w[36], pars->GC_11, amp[146]); 
  FFV1_0(w[61], w[33], w[36], pars->GC_11, amp[147]); 
  FFV1_0(w[30], w[70], w[5], pars->GC_11, amp[148]); 
  FFV1_0(w[68], w[33], w[5], pars->GC_11, amp[149]); 
  FFV1_0(w[30], w[71], w[5], pars->GC_11, amp[150]); 
  FFV1_0(w[69], w[33], w[5], pars->GC_11, amp[151]); 
  FFV1_0(w[20], w[70], w[4], pars->GC_11, amp[152]); 
  FFV1_0(w[64], w[33], w[4], pars->GC_11, amp[153]); 
  FFV1_0(w[20], w[71], w[4], pars->GC_11, amp[154]); 
  FFV1_0(w[65], w[33], w[4], pars->GC_11, amp[155]); 
  FFV1_0(w[40], w[58], w[5], pars->GC_11, amp[156]); 
  FFV1_0(w[41], w[58], w[4], pars->GC_11, amp[157]); 
  FFV1_0(w[40], w[60], w[5], pars->GC_11, amp[158]); 
  FFV1_0(w[41], w[60], w[4], pars->GC_11, amp[159]); 
  FFV1_0(w[72], w[6], w[36], pars->GC_11, amp[160]); 
  FFV1_0(w[39], w[58], w[36], pars->GC_11, amp[161]); 
  FFV1_0(w[73], w[6], w[36], pars->GC_11, amp[162]); 
  FFV1_0(w[39], w[60], w[36], pars->GC_11, amp[163]); 
  FFV1_0(w[72], w[27], w[5], pars->GC_11, amp[164]); 
  FFV1_0(w[39], w[66], w[5], pars->GC_11, amp[165]); 
  FFV1_0(w[73], w[27], w[5], pars->GC_11, amp[166]); 
  FFV1_0(w[39], w[67], w[5], pars->GC_11, amp[167]); 
  FFV1_0(w[72], w[17], w[4], pars->GC_11, amp[168]); 
  FFV1_0(w[39], w[62], w[4], pars->GC_11, amp[169]); 
  FFV1_0(w[73], w[17], w[4], pars->GC_11, amp[170]); 
  FFV1_0(w[39], w[63], w[4], pars->GC_11, amp[171]); 
  FFV1_0(w[1], w[58], w[44], pars->GC_11, amp[172]); 
  FFV1_0(w[45], w[58], w[3], pars->GC_11, amp[173]); 
  FFV1_0(w[59], w[6], w[44], pars->GC_11, amp[174]); 
  FFV1_0(w[59], w[46], w[3], pars->GC_11, amp[175]); 
  FFV1_0(w[1], w[60], w[44], pars->GC_11, amp[176]); 
  FFV1_0(w[45], w[60], w[3], pars->GC_11, amp[177]); 
  FFV1_0(w[61], w[6], w[44], pars->GC_11, amp[178]); 
  FFV1_0(w[61], w[46], w[3], pars->GC_11, amp[179]); 
  FFV1_0(w[59], w[47], w[5], pars->GC_11, amp[180]); 
  FFV1_0(w[59], w[48], w[3], pars->GC_11, amp[181]); 
  FFV1_0(w[61], w[47], w[5], pars->GC_11, amp[182]); 
  FFV1_0(w[61], w[48], w[3], pars->GC_11, amp[183]); 
  FFV1_0(w[20], w[66], w[3], pars->GC_11, amp[184]); 
  FFV1_0(w[64], w[27], w[3], pars->GC_11, amp[185]); 
  FFV1_0(w[20], w[67], w[3], pars->GC_11, amp[186]); 
  FFV1_0(w[65], w[27], w[3], pars->GC_11, amp[187]); 
  FFV1_0(w[49], w[58], w[5], pars->GC_11, amp[188]); 
  FFV1_0(w[50], w[58], w[3], pars->GC_11, amp[189]); 
  FFV1_0(w[49], w[60], w[5], pars->GC_11, amp[190]); 
  FFV1_0(w[50], w[60], w[3], pars->GC_11, amp[191]); 
  FFV1_0(w[68], w[17], w[3], pars->GC_11, amp[192]); 
  FFV1_0(w[30], w[62], w[3], pars->GC_11, amp[193]); 
  FFV1_0(w[69], w[17], w[3], pars->GC_11, amp[194]); 
  FFV1_0(w[30], w[63], w[3], pars->GC_11, amp[195]); 
  FFV1_0(w[59], w[51], w[4], pars->GC_11, amp[196]); 
  FFV1_0(w[59], w[52], w[3], pars->GC_11, amp[197]); 
  FFV1_0(w[61], w[51], w[4], pars->GC_11, amp[198]); 
  FFV1_0(w[61], w[52], w[3], pars->GC_11, amp[199]); 
  FFV1_0(w[53], w[58], w[4], pars->GC_11, amp[200]); 
  FFV1_0(w[54], w[58], w[3], pars->GC_11, amp[201]); 
  FFV1_0(w[53], w[60], w[4], pars->GC_11, amp[202]); 
  FFV1_0(w[54], w[60], w[3], pars->GC_11, amp[203]); 
  FFV1_0(w[1], w[58], w[55], pars->GC_11, amp[204]); 
  FFV1_0(w[1], w[58], w[56], pars->GC_11, amp[205]); 
  FFV1_0(w[1], w[58], w[57], pars->GC_11, amp[206]); 
  FFV1_0(w[59], w[6], w[55], pars->GC_11, amp[207]); 
  FFV1_0(w[59], w[6], w[56], pars->GC_11, amp[208]); 
  FFV1_0(w[59], w[6], w[57], pars->GC_11, amp[209]); 
  FFV1_0(w[1], w[60], w[55], pars->GC_11, amp[210]); 
  FFV1_0(w[1], w[60], w[56], pars->GC_11, amp[211]); 
  FFV1_0(w[1], w[60], w[57], pars->GC_11, amp[212]); 
  FFV1_0(w[61], w[6], w[55], pars->GC_11, amp[213]); 
  FFV1_0(w[61], w[6], w[56], pars->GC_11, amp[214]); 
  FFV1_0(w[61], w[6], w[57], pars->GC_11, amp[215]); 
  FFV1_0(w[75], w[76], w[10], pars->GC_11, amp[216]); 
  FFV1_0(w[77], w[76], w[5], pars->GC_11, amp[217]); 
  FFV1_0(w[78], w[74], w[10], pars->GC_11, amp[218]); 
  FFV1_0(w[78], w[79], w[5], pars->GC_11, amp[219]); 
  FFV1_0(w[75], w[80], w[10], pars->GC_11, amp[220]); 
  FFV1_0(w[77], w[80], w[5], pars->GC_11, amp[221]); 
  FFV1_0(w[81], w[74], w[10], pars->GC_11, amp[222]); 
  FFV1_0(w[81], w[79], w[5], pars->GC_11, amp[223]); 
  FFV1_0(w[75], w[83], w[8], pars->GC_11, amp[224]); 
  FFV1_0(w[78], w[82], w[8], pars->GC_11, amp[225]); 
  FFV1_0(w[75], w[84], w[8], pars->GC_11, amp[226]); 
  FFV1_0(w[81], w[82], w[8], pars->GC_11, amp[227]); 
  FFV1_0(w[86], w[74], w[8], pars->GC_11, amp[228]); 
  FFV1_0(w[85], w[76], w[8], pars->GC_11, amp[229]); 
  FFV1_0(w[87], w[74], w[8], pars->GC_11, amp[230]); 
  FFV1_0(w[85], w[80], w[8], pars->GC_11, amp[231]); 
  FFV1_0(w[75], w[76], w[24], pars->GC_11, amp[232]); 
  FFV1_0(w[88], w[76], w[4], pars->GC_11, amp[233]); 
  FFV1_0(w[78], w[74], w[24], pars->GC_11, amp[234]); 
  FFV1_0(w[78], w[89], w[4], pars->GC_11, amp[235]); 
  FFV1_0(w[75], w[80], w[24], pars->GC_11, amp[236]); 
  FFV1_0(w[88], w[80], w[4], pars->GC_11, amp[237]); 
  FFV1_0(w[81], w[74], w[24], pars->GC_11, amp[238]); 
  FFV1_0(w[81], w[89], w[4], pars->GC_11, amp[239]); 
  FFV1_0(w[75], w[91], w[23], pars->GC_11, amp[240]); 
  FFV1_0(w[78], w[90], w[23], pars->GC_11, amp[241]); 
  FFV1_0(w[75], w[92], w[23], pars->GC_11, amp[242]); 
  FFV1_0(w[81], w[90], w[23], pars->GC_11, amp[243]); 
  FFV1_0(w[94], w[74], w[23], pars->GC_11, amp[244]); 
  FFV1_0(w[93], w[76], w[23], pars->GC_11, amp[245]); 
  FFV1_0(w[95], w[74], w[23], pars->GC_11, amp[246]); 
  FFV1_0(w[93], w[80], w[23], pars->GC_11, amp[247]); 
  FFV1_0(w[78], w[97], w[5], pars->GC_11, amp[248]); 
  FFV1_0(w[78], w[98], w[4], pars->GC_11, amp[249]); 
  FFV1_0(w[81], w[97], w[5], pars->GC_11, amp[250]); 
  FFV1_0(w[81], w[98], w[4], pars->GC_11, amp[251]); 
  FFV1_0(w[75], w[99], w[36], pars->GC_11, amp[252]); 
  FFV1_0(w[78], w[96], w[36], pars->GC_11, amp[253]); 
  FFV1_0(w[75], w[100], w[36], pars->GC_11, amp[254]); 
  FFV1_0(w[81], w[96], w[36], pars->GC_11, amp[255]); 
  FFV1_0(w[93], w[99], w[5], pars->GC_11, amp[256]); 
  FFV1_0(w[94], w[96], w[5], pars->GC_11, amp[257]); 
  FFV1_0(w[93], w[100], w[5], pars->GC_11, amp[258]); 
  FFV1_0(w[95], w[96], w[5], pars->GC_11, amp[259]); 
  FFV1_0(w[85], w[99], w[4], pars->GC_11, amp[260]); 
  FFV1_0(w[86], w[96], w[4], pars->GC_11, amp[261]); 
  FFV1_0(w[85], w[100], w[4], pars->GC_11, amp[262]); 
  FFV1_0(w[87], w[96], w[4], pars->GC_11, amp[263]); 
  FFV1_0(w[102], w[76], w[5], pars->GC_11, amp[264]); 
  FFV1_0(w[103], w[76], w[4], pars->GC_11, amp[265]); 
  FFV1_0(w[102], w[80], w[5], pars->GC_11, amp[266]); 
  FFV1_0(w[103], w[80], w[4], pars->GC_11, amp[267]); 
  FFV1_0(w[104], w[74], w[36], pars->GC_11, amp[268]); 
  FFV1_0(w[101], w[76], w[36], pars->GC_11, amp[269]); 
  FFV1_0(w[105], w[74], w[36], pars->GC_11, amp[270]); 
  FFV1_0(w[101], w[80], w[36], pars->GC_11, amp[271]); 
  FFV1_0(w[104], w[90], w[5], pars->GC_11, amp[272]); 
  FFV1_0(w[101], w[91], w[5], pars->GC_11, amp[273]); 
  FFV1_0(w[105], w[90], w[5], pars->GC_11, amp[274]); 
  FFV1_0(w[101], w[92], w[5], pars->GC_11, amp[275]); 
  FFV1_0(w[104], w[82], w[4], pars->GC_11, amp[276]); 
  FFV1_0(w[101], w[83], w[4], pars->GC_11, amp[277]); 
  FFV1_0(w[105], w[82], w[4], pars->GC_11, amp[278]); 
  FFV1_0(w[101], w[84], w[4], pars->GC_11, amp[279]); 
  FFV1_0(w[75], w[76], w[44], pars->GC_11, amp[280]); 
  FFV1_0(w[106], w[76], w[3], pars->GC_11, amp[281]); 
  FFV1_0(w[78], w[74], w[44], pars->GC_11, amp[282]); 
  FFV1_0(w[78], w[107], w[3], pars->GC_11, amp[283]); 
  FFV1_0(w[75], w[80], w[44], pars->GC_11, amp[284]); 
  FFV1_0(w[106], w[80], w[3], pars->GC_11, amp[285]); 
  FFV1_0(w[81], w[74], w[44], pars->GC_11, amp[286]); 
  FFV1_0(w[81], w[107], w[3], pars->GC_11, amp[287]); 
  FFV1_0(w[78], w[108], w[5], pars->GC_11, amp[288]); 
  FFV1_0(w[78], w[109], w[3], pars->GC_11, amp[289]); 
  FFV1_0(w[81], w[108], w[5], pars->GC_11, amp[290]); 
  FFV1_0(w[81], w[109], w[3], pars->GC_11, amp[291]); 
  FFV1_0(w[85], w[91], w[3], pars->GC_11, amp[292]); 
  FFV1_0(w[86], w[90], w[3], pars->GC_11, amp[293]); 
  FFV1_0(w[85], w[92], w[3], pars->GC_11, amp[294]); 
  FFV1_0(w[87], w[90], w[3], pars->GC_11, amp[295]); 
  FFV1_0(w[110], w[76], w[5], pars->GC_11, amp[296]); 
  FFV1_0(w[111], w[76], w[3], pars->GC_11, amp[297]); 
  FFV1_0(w[110], w[80], w[5], pars->GC_11, amp[298]); 
  FFV1_0(w[111], w[80], w[3], pars->GC_11, amp[299]); 
  FFV1_0(w[94], w[82], w[3], pars->GC_11, amp[300]); 
  FFV1_0(w[93], w[83], w[3], pars->GC_11, amp[301]); 
  FFV1_0(w[95], w[82], w[3], pars->GC_11, amp[302]); 
  FFV1_0(w[93], w[84], w[3], pars->GC_11, amp[303]); 
  FFV1_0(w[78], w[112], w[4], pars->GC_11, amp[304]); 
  FFV1_0(w[78], w[113], w[3], pars->GC_11, amp[305]); 
  FFV1_0(w[81], w[112], w[4], pars->GC_11, amp[306]); 
  FFV1_0(w[81], w[113], w[3], pars->GC_11, amp[307]); 
  FFV1_0(w[114], w[76], w[4], pars->GC_11, amp[308]); 
  FFV1_0(w[115], w[76], w[3], pars->GC_11, amp[309]); 
  FFV1_0(w[114], w[80], w[4], pars->GC_11, amp[310]); 
  FFV1_0(w[115], w[80], w[3], pars->GC_11, amp[311]); 
  FFV1_0(w[75], w[76], w[55], pars->GC_11, amp[312]); 
  FFV1_0(w[75], w[76], w[56], pars->GC_11, amp[313]); 
  FFV1_0(w[75], w[76], w[57], pars->GC_11, amp[314]); 
  FFV1_0(w[78], w[74], w[55], pars->GC_11, amp[315]); 
  FFV1_0(w[78], w[74], w[56], pars->GC_11, amp[316]); 
  FFV1_0(w[78], w[74], w[57], pars->GC_11, amp[317]); 
  FFV1_0(w[75], w[80], w[55], pars->GC_11, amp[318]); 
  FFV1_0(w[75], w[80], w[56], pars->GC_11, amp[319]); 
  FFV1_0(w[75], w[80], w[57], pars->GC_11, amp[320]); 
  FFV1_0(w[81], w[74], w[55], pars->GC_11, amp[321]); 
  FFV1_0(w[81], w[74], w[56], pars->GC_11, amp[322]); 
  FFV1_0(w[81], w[74], w[57], pars->GC_11, amp[323]); 
  FFV1_0(w[75], w[116], w[10], pars->GC_11, amp[324]); 
  FFV1_0(w[77], w[116], w[5], pars->GC_11, amp[325]); 
  FFV1_0(w[117], w[74], w[10], pars->GC_11, amp[326]); 
  FFV1_0(w[117], w[79], w[5], pars->GC_11, amp[327]); 
  FFV1_0(w[75], w[118], w[10], pars->GC_11, amp[328]); 
  FFV1_0(w[77], w[118], w[5], pars->GC_11, amp[329]); 
  FFV1_0(w[119], w[74], w[10], pars->GC_11, amp[330]); 
  FFV1_0(w[119], w[79], w[5], pars->GC_11, amp[331]); 
  FFV1_0(w[75], w[120], w[8], pars->GC_11, amp[332]); 
  FFV1_0(w[117], w[82], w[8], pars->GC_11, amp[333]); 
  FFV1_0(w[75], w[121], w[8], pars->GC_11, amp[334]); 
  FFV1_0(w[119], w[82], w[8], pars->GC_11, amp[335]); 
  FFV1_0(w[122], w[74], w[8], pars->GC_11, amp[336]); 
  FFV1_0(w[85], w[116], w[8], pars->GC_11, amp[337]); 
  FFV1_0(w[123], w[74], w[8], pars->GC_11, amp[338]); 
  FFV1_0(w[85], w[118], w[8], pars->GC_11, amp[339]); 
  FFV1_0(w[75], w[116], w[24], pars->GC_11, amp[340]); 
  FFV1_0(w[88], w[116], w[4], pars->GC_11, amp[341]); 
  FFV1_0(w[117], w[74], w[24], pars->GC_11, amp[342]); 
  FFV1_0(w[117], w[89], w[4], pars->GC_11, amp[343]); 
  FFV1_0(w[75], w[118], w[24], pars->GC_11, amp[344]); 
  FFV1_0(w[88], w[118], w[4], pars->GC_11, amp[345]); 
  FFV1_0(w[119], w[74], w[24], pars->GC_11, amp[346]); 
  FFV1_0(w[119], w[89], w[4], pars->GC_11, amp[347]); 
  FFV1_0(w[75], w[124], w[23], pars->GC_11, amp[348]); 
  FFV1_0(w[117], w[90], w[23], pars->GC_11, amp[349]); 
  FFV1_0(w[75], w[125], w[23], pars->GC_11, amp[350]); 
  FFV1_0(w[119], w[90], w[23], pars->GC_11, amp[351]); 
  FFV1_0(w[126], w[74], w[23], pars->GC_11, amp[352]); 
  FFV1_0(w[93], w[116], w[23], pars->GC_11, amp[353]); 
  FFV1_0(w[127], w[74], w[23], pars->GC_11, amp[354]); 
  FFV1_0(w[93], w[118], w[23], pars->GC_11, amp[355]); 
  FFV1_0(w[117], w[97], w[5], pars->GC_11, amp[356]); 
  FFV1_0(w[117], w[98], w[4], pars->GC_11, amp[357]); 
  FFV1_0(w[119], w[97], w[5], pars->GC_11, amp[358]); 
  FFV1_0(w[119], w[98], w[4], pars->GC_11, amp[359]); 
  FFV1_0(w[75], w[128], w[36], pars->GC_11, amp[360]); 
  FFV1_0(w[117], w[96], w[36], pars->GC_11, amp[361]); 
  FFV1_0(w[75], w[129], w[36], pars->GC_11, amp[362]); 
  FFV1_0(w[119], w[96], w[36], pars->GC_11, amp[363]); 
  FFV1_0(w[93], w[128], w[5], pars->GC_11, amp[364]); 
  FFV1_0(w[126], w[96], w[5], pars->GC_11, amp[365]); 
  FFV1_0(w[93], w[129], w[5], pars->GC_11, amp[366]); 
  FFV1_0(w[127], w[96], w[5], pars->GC_11, amp[367]); 
  FFV1_0(w[85], w[128], w[4], pars->GC_11, amp[368]); 
  FFV1_0(w[122], w[96], w[4], pars->GC_11, amp[369]); 
  FFV1_0(w[85], w[129], w[4], pars->GC_11, amp[370]); 
  FFV1_0(w[123], w[96], w[4], pars->GC_11, amp[371]); 
  FFV1_0(w[102], w[116], w[5], pars->GC_11, amp[372]); 
  FFV1_0(w[103], w[116], w[4], pars->GC_11, amp[373]); 
  FFV1_0(w[102], w[118], w[5], pars->GC_11, amp[374]); 
  FFV1_0(w[103], w[118], w[4], pars->GC_11, amp[375]); 
  FFV1_0(w[130], w[74], w[36], pars->GC_11, amp[376]); 
  FFV1_0(w[101], w[116], w[36], pars->GC_11, amp[377]); 
  FFV1_0(w[131], w[74], w[36], pars->GC_11, amp[378]); 
  FFV1_0(w[101], w[118], w[36], pars->GC_11, amp[379]); 
  FFV1_0(w[130], w[90], w[5], pars->GC_11, amp[380]); 
  FFV1_0(w[101], w[124], w[5], pars->GC_11, amp[381]); 
  FFV1_0(w[131], w[90], w[5], pars->GC_11, amp[382]); 
  FFV1_0(w[101], w[125], w[5], pars->GC_11, amp[383]); 
  FFV1_0(w[130], w[82], w[4], pars->GC_11, amp[384]); 
  FFV1_0(w[101], w[120], w[4], pars->GC_11, amp[385]); 
  FFV1_0(w[131], w[82], w[4], pars->GC_11, amp[386]); 
  FFV1_0(w[101], w[121], w[4], pars->GC_11, amp[387]); 
  FFV1_0(w[75], w[116], w[44], pars->GC_11, amp[388]); 
  FFV1_0(w[106], w[116], w[3], pars->GC_11, amp[389]); 
  FFV1_0(w[117], w[74], w[44], pars->GC_11, amp[390]); 
  FFV1_0(w[117], w[107], w[3], pars->GC_11, amp[391]); 
  FFV1_0(w[75], w[118], w[44], pars->GC_11, amp[392]); 
  FFV1_0(w[106], w[118], w[3], pars->GC_11, amp[393]); 
  FFV1_0(w[119], w[74], w[44], pars->GC_11, amp[394]); 
  FFV1_0(w[119], w[107], w[3], pars->GC_11, amp[395]); 
  FFV1_0(w[117], w[108], w[5], pars->GC_11, amp[396]); 
  FFV1_0(w[117], w[109], w[3], pars->GC_11, amp[397]); 
  FFV1_0(w[119], w[108], w[5], pars->GC_11, amp[398]); 
  FFV1_0(w[119], w[109], w[3], pars->GC_11, amp[399]); 
  FFV1_0(w[85], w[124], w[3], pars->GC_11, amp[400]); 
  FFV1_0(w[122], w[90], w[3], pars->GC_11, amp[401]); 
  FFV1_0(w[85], w[125], w[3], pars->GC_11, amp[402]); 
  FFV1_0(w[123], w[90], w[3], pars->GC_11, amp[403]); 
  FFV1_0(w[110], w[116], w[5], pars->GC_11, amp[404]); 
  FFV1_0(w[111], w[116], w[3], pars->GC_11, amp[405]); 
  FFV1_0(w[110], w[118], w[5], pars->GC_11, amp[406]); 
  FFV1_0(w[111], w[118], w[3], pars->GC_11, amp[407]); 
  FFV1_0(w[126], w[82], w[3], pars->GC_11, amp[408]); 
  FFV1_0(w[93], w[120], w[3], pars->GC_11, amp[409]); 
  FFV1_0(w[127], w[82], w[3], pars->GC_11, amp[410]); 
  FFV1_0(w[93], w[121], w[3], pars->GC_11, amp[411]); 
  FFV1_0(w[117], w[112], w[4], pars->GC_11, amp[412]); 
  FFV1_0(w[117], w[113], w[3], pars->GC_11, amp[413]); 
  FFV1_0(w[119], w[112], w[4], pars->GC_11, amp[414]); 
  FFV1_0(w[119], w[113], w[3], pars->GC_11, amp[415]); 
  FFV1_0(w[114], w[116], w[4], pars->GC_11, amp[416]); 
  FFV1_0(w[115], w[116], w[3], pars->GC_11, amp[417]); 
  FFV1_0(w[114], w[118], w[4], pars->GC_11, amp[418]); 
  FFV1_0(w[115], w[118], w[3], pars->GC_11, amp[419]); 
  FFV1_0(w[75], w[116], w[55], pars->GC_11, amp[420]); 
  FFV1_0(w[75], w[116], w[56], pars->GC_11, amp[421]); 
  FFV1_0(w[75], w[116], w[57], pars->GC_11, amp[422]); 
  FFV1_0(w[117], w[74], w[55], pars->GC_11, amp[423]); 
  FFV1_0(w[117], w[74], w[56], pars->GC_11, amp[424]); 
  FFV1_0(w[117], w[74], w[57], pars->GC_11, amp[425]); 
  FFV1_0(w[75], w[118], w[55], pars->GC_11, amp[426]); 
  FFV1_0(w[75], w[118], w[56], pars->GC_11, amp[427]); 
  FFV1_0(w[75], w[118], w[57], pars->GC_11, amp[428]); 
  FFV1_0(w[119], w[74], w[55], pars->GC_11, amp[429]); 
  FFV1_0(w[119], w[74], w[56], pars->GC_11, amp[430]); 
  FFV1_0(w[119], w[74], w[57], pars->GC_11, amp[431]); 
  FFV1_0(w[1], w[135], w[10], pars->GC_11, amp[432]); 
  FFV1_0(w[11], w[135], w[5], pars->GC_11, amp[433]); 
  FFV1_0(w[136], w[6], w[10], pars->GC_11, amp[434]); 
  FFV1_0(w[136], w[13], w[5], pars->GC_11, amp[435]); 
  FFV1_0(w[1], w[138], w[10], pars->GC_11, amp[436]); 
  FFV1_0(w[11], w[138], w[5], pars->GC_11, amp[437]); 
  FFV1_0(w[139], w[6], w[10], pars->GC_11, amp[438]); 
  FFV1_0(w[139], w[13], w[5], pars->GC_11, amp[439]); 
  FFV1_0(w[1], w[140], w[8], pars->GC_11, amp[440]); 
  FFV1_0(w[136], w[17], w[8], pars->GC_11, amp[441]); 
  FFV1_0(w[1], w[141], w[8], pars->GC_11, amp[442]); 
  FFV1_0(w[139], w[17], w[8], pars->GC_11, amp[443]); 
  FFV1_0(w[142], w[6], w[8], pars->GC_11, amp[444]); 
  FFV1_0(w[20], w[135], w[8], pars->GC_11, amp[445]); 
  FFV1_0(w[143], w[6], w[8], pars->GC_11, amp[446]); 
  FFV1_0(w[20], w[138], w[8], pars->GC_11, amp[447]); 
  FFV1_0(w[1], w[135], w[24], pars->GC_11, amp[448]); 
  FFV1_0(w[25], w[135], w[4], pars->GC_11, amp[449]); 
  FFV1_0(w[136], w[6], w[24], pars->GC_11, amp[450]); 
  FFV1_0(w[136], w[26], w[4], pars->GC_11, amp[451]); 
  FFV1_0(w[1], w[138], w[24], pars->GC_11, amp[452]); 
  FFV1_0(w[25], w[138], w[4], pars->GC_11, amp[453]); 
  FFV1_0(w[139], w[6], w[24], pars->GC_11, amp[454]); 
  FFV1_0(w[139], w[26], w[4], pars->GC_11, amp[455]); 
  FFV1_0(w[1], w[144], w[23], pars->GC_11, amp[456]); 
  FFV1_0(w[136], w[27], w[23], pars->GC_11, amp[457]); 
  FFV1_0(w[1], w[145], w[23], pars->GC_11, amp[458]); 
  FFV1_0(w[139], w[27], w[23], pars->GC_11, amp[459]); 
  FFV1_0(w[146], w[6], w[23], pars->GC_11, amp[460]); 
  FFV1_0(w[30], w[135], w[23], pars->GC_11, amp[461]); 
  FFV1_0(w[147], w[6], w[23], pars->GC_11, amp[462]); 
  FFV1_0(w[30], w[138], w[23], pars->GC_11, amp[463]); 
  FFV1_0(w[136], w[34], w[5], pars->GC_11, amp[464]); 
  FFV1_0(w[136], w[35], w[4], pars->GC_11, amp[465]); 
  FFV1_0(w[139], w[34], w[5], pars->GC_11, amp[466]); 
  FFV1_0(w[139], w[35], w[4], pars->GC_11, amp[467]); 
  FFV1_0(w[1], w[148], w[36], pars->GC_11, amp[468]); 
  FFV1_0(w[136], w[33], w[36], pars->GC_11, amp[469]); 
  FFV1_0(w[1], w[149], w[36], pars->GC_11, amp[470]); 
  FFV1_0(w[139], w[33], w[36], pars->GC_11, amp[471]); 
  FFV1_0(w[30], w[148], w[5], pars->GC_11, amp[472]); 
  FFV1_0(w[146], w[33], w[5], pars->GC_11, amp[473]); 
  FFV1_0(w[30], w[149], w[5], pars->GC_11, amp[474]); 
  FFV1_0(w[147], w[33], w[5], pars->GC_11, amp[475]); 
  FFV1_0(w[20], w[148], w[4], pars->GC_11, amp[476]); 
  FFV1_0(w[142], w[33], w[4], pars->GC_11, amp[477]); 
  FFV1_0(w[20], w[149], w[4], pars->GC_11, amp[478]); 
  FFV1_0(w[143], w[33], w[4], pars->GC_11, amp[479]); 
  FFV1_0(w[40], w[135], w[5], pars->GC_11, amp[480]); 
  FFV1_0(w[41], w[135], w[4], pars->GC_11, amp[481]); 
  FFV1_0(w[40], w[138], w[5], pars->GC_11, amp[482]); 
  FFV1_0(w[41], w[138], w[4], pars->GC_11, amp[483]); 
  FFV1_0(w[150], w[6], w[36], pars->GC_11, amp[484]); 
  FFV1_0(w[39], w[135], w[36], pars->GC_11, amp[485]); 
  FFV1_0(w[151], w[6], w[36], pars->GC_11, amp[486]); 
  FFV1_0(w[39], w[138], w[36], pars->GC_11, amp[487]); 
  FFV1_0(w[150], w[27], w[5], pars->GC_11, amp[488]); 
  FFV1_0(w[39], w[144], w[5], pars->GC_11, amp[489]); 
  FFV1_0(w[151], w[27], w[5], pars->GC_11, amp[490]); 
  FFV1_0(w[39], w[145], w[5], pars->GC_11, amp[491]); 
  FFV1_0(w[150], w[17], w[4], pars->GC_11, amp[492]); 
  FFV1_0(w[39], w[140], w[4], pars->GC_11, amp[493]); 
  FFV1_0(w[151], w[17], w[4], pars->GC_11, amp[494]); 
  FFV1_0(w[39], w[141], w[4], pars->GC_11, amp[495]); 
  FFV1_0(w[1], w[135], w[44], pars->GC_11, amp[496]); 
  FFV1_0(w[45], w[135], w[3], pars->GC_11, amp[497]); 
  FFV1_0(w[136], w[6], w[44], pars->GC_11, amp[498]); 
  FFV1_0(w[136], w[46], w[3], pars->GC_11, amp[499]); 
  FFV1_0(w[1], w[138], w[44], pars->GC_11, amp[500]); 
  FFV1_0(w[45], w[138], w[3], pars->GC_11, amp[501]); 
  FFV1_0(w[139], w[6], w[44], pars->GC_11, amp[502]); 
  FFV1_0(w[139], w[46], w[3], pars->GC_11, amp[503]); 
  FFV1_0(w[136], w[47], w[5], pars->GC_11, amp[504]); 
  FFV1_0(w[136], w[48], w[3], pars->GC_11, amp[505]); 
  FFV1_0(w[139], w[47], w[5], pars->GC_11, amp[506]); 
  FFV1_0(w[139], w[48], w[3], pars->GC_11, amp[507]); 
  FFV1_0(w[20], w[144], w[3], pars->GC_11, amp[508]); 
  FFV1_0(w[142], w[27], w[3], pars->GC_11, amp[509]); 
  FFV1_0(w[20], w[145], w[3], pars->GC_11, amp[510]); 
  FFV1_0(w[143], w[27], w[3], pars->GC_11, amp[511]); 
  FFV1_0(w[49], w[135], w[5], pars->GC_11, amp[512]); 
  FFV1_0(w[50], w[135], w[3], pars->GC_11, amp[513]); 
  FFV1_0(w[49], w[138], w[5], pars->GC_11, amp[514]); 
  FFV1_0(w[50], w[138], w[3], pars->GC_11, amp[515]); 
  FFV1_0(w[146], w[17], w[3], pars->GC_11, amp[516]); 
  FFV1_0(w[30], w[140], w[3], pars->GC_11, amp[517]); 
  FFV1_0(w[147], w[17], w[3], pars->GC_11, amp[518]); 
  FFV1_0(w[30], w[141], w[3], pars->GC_11, amp[519]); 
  FFV1_0(w[136], w[51], w[4], pars->GC_11, amp[520]); 
  FFV1_0(w[136], w[52], w[3], pars->GC_11, amp[521]); 
  FFV1_0(w[139], w[51], w[4], pars->GC_11, amp[522]); 
  FFV1_0(w[139], w[52], w[3], pars->GC_11, amp[523]); 
  FFV1_0(w[53], w[135], w[4], pars->GC_11, amp[524]); 
  FFV1_0(w[54], w[135], w[3], pars->GC_11, amp[525]); 
  FFV1_0(w[53], w[138], w[4], pars->GC_11, amp[526]); 
  FFV1_0(w[54], w[138], w[3], pars->GC_11, amp[527]); 
  FFV1_0(w[1], w[135], w[55], pars->GC_11, amp[528]); 
  FFV1_0(w[1], w[135], w[56], pars->GC_11, amp[529]); 
  FFV1_0(w[1], w[135], w[57], pars->GC_11, amp[530]); 
  FFV1_0(w[136], w[6], w[55], pars->GC_11, amp[531]); 
  FFV1_0(w[136], w[6], w[56], pars->GC_11, amp[532]); 
  FFV1_0(w[136], w[6], w[57], pars->GC_11, amp[533]); 
  FFV1_0(w[1], w[138], w[55], pars->GC_11, amp[534]); 
  FFV1_0(w[1], w[138], w[56], pars->GC_11, amp[535]); 
  FFV1_0(w[1], w[138], w[57], pars->GC_11, amp[536]); 
  FFV1_0(w[139], w[6], w[55], pars->GC_11, amp[537]); 
  FFV1_0(w[139], w[6], w[56], pars->GC_11, amp[538]); 
  FFV1_0(w[139], w[6], w[57], pars->GC_11, amp[539]); 
  FFV1_0(w[1], w[152], w[10], pars->GC_11, amp[540]); 
  FFV1_0(w[11], w[152], w[5], pars->GC_11, amp[541]); 
  FFV1_0(w[153], w[6], w[10], pars->GC_11, amp[542]); 
  FFV1_0(w[153], w[13], w[5], pars->GC_11, amp[543]); 
  FFV1_0(w[1], w[154], w[10], pars->GC_11, amp[544]); 
  FFV1_0(w[11], w[154], w[5], pars->GC_11, amp[545]); 
  FFV1_0(w[155], w[6], w[10], pars->GC_11, amp[546]); 
  FFV1_0(w[155], w[13], w[5], pars->GC_11, amp[547]); 
  FFV1_0(w[1], w[156], w[8], pars->GC_11, amp[548]); 
  FFV1_0(w[153], w[17], w[8], pars->GC_11, amp[549]); 
  FFV1_0(w[1], w[157], w[8], pars->GC_11, amp[550]); 
  FFV1_0(w[155], w[17], w[8], pars->GC_11, amp[551]); 
  FFV1_0(w[158], w[6], w[8], pars->GC_11, amp[552]); 
  FFV1_0(w[20], w[152], w[8], pars->GC_11, amp[553]); 
  FFV1_0(w[159], w[6], w[8], pars->GC_11, amp[554]); 
  FFV1_0(w[20], w[154], w[8], pars->GC_11, amp[555]); 
  FFV1_0(w[1], w[152], w[24], pars->GC_11, amp[556]); 
  FFV1_0(w[25], w[152], w[4], pars->GC_11, amp[557]); 
  FFV1_0(w[153], w[6], w[24], pars->GC_11, amp[558]); 
  FFV1_0(w[153], w[26], w[4], pars->GC_11, amp[559]); 
  FFV1_0(w[1], w[154], w[24], pars->GC_11, amp[560]); 
  FFV1_0(w[25], w[154], w[4], pars->GC_11, amp[561]); 
  FFV1_0(w[155], w[6], w[24], pars->GC_11, amp[562]); 
  FFV1_0(w[155], w[26], w[4], pars->GC_11, amp[563]); 
  FFV1_0(w[1], w[160], w[23], pars->GC_11, amp[564]); 
  FFV1_0(w[153], w[27], w[23], pars->GC_11, amp[565]); 
  FFV1_0(w[1], w[161], w[23], pars->GC_11, amp[566]); 
  FFV1_0(w[155], w[27], w[23], pars->GC_11, amp[567]); 
  FFV1_0(w[162], w[6], w[23], pars->GC_11, amp[568]); 
  FFV1_0(w[30], w[152], w[23], pars->GC_11, amp[569]); 
  FFV1_0(w[163], w[6], w[23], pars->GC_11, amp[570]); 
  FFV1_0(w[30], w[154], w[23], pars->GC_11, amp[571]); 
  FFV1_0(w[153], w[34], w[5], pars->GC_11, amp[572]); 
  FFV1_0(w[153], w[35], w[4], pars->GC_11, amp[573]); 
  FFV1_0(w[155], w[34], w[5], pars->GC_11, amp[574]); 
  FFV1_0(w[155], w[35], w[4], pars->GC_11, amp[575]); 
  FFV1_0(w[1], w[164], w[36], pars->GC_11, amp[576]); 
  FFV1_0(w[153], w[33], w[36], pars->GC_11, amp[577]); 
  FFV1_0(w[1], w[165], w[36], pars->GC_11, amp[578]); 
  FFV1_0(w[155], w[33], w[36], pars->GC_11, amp[579]); 
  FFV1_0(w[30], w[164], w[5], pars->GC_11, amp[580]); 
  FFV1_0(w[162], w[33], w[5], pars->GC_11, amp[581]); 
  FFV1_0(w[30], w[165], w[5], pars->GC_11, amp[582]); 
  FFV1_0(w[163], w[33], w[5], pars->GC_11, amp[583]); 
  FFV1_0(w[20], w[164], w[4], pars->GC_11, amp[584]); 
  FFV1_0(w[158], w[33], w[4], pars->GC_11, amp[585]); 
  FFV1_0(w[20], w[165], w[4], pars->GC_11, amp[586]); 
  FFV1_0(w[159], w[33], w[4], pars->GC_11, amp[587]); 
  FFV1_0(w[40], w[152], w[5], pars->GC_11, amp[588]); 
  FFV1_0(w[41], w[152], w[4], pars->GC_11, amp[589]); 
  FFV1_0(w[40], w[154], w[5], pars->GC_11, amp[590]); 
  FFV1_0(w[41], w[154], w[4], pars->GC_11, amp[591]); 
  FFV1_0(w[166], w[6], w[36], pars->GC_11, amp[592]); 
  FFV1_0(w[39], w[152], w[36], pars->GC_11, amp[593]); 
  FFV1_0(w[167], w[6], w[36], pars->GC_11, amp[594]); 
  FFV1_0(w[39], w[154], w[36], pars->GC_11, amp[595]); 
  FFV1_0(w[166], w[27], w[5], pars->GC_11, amp[596]); 
  FFV1_0(w[39], w[160], w[5], pars->GC_11, amp[597]); 
  FFV1_0(w[167], w[27], w[5], pars->GC_11, amp[598]); 
  FFV1_0(w[39], w[161], w[5], pars->GC_11, amp[599]); 
  FFV1_0(w[166], w[17], w[4], pars->GC_11, amp[600]); 
  FFV1_0(w[39], w[156], w[4], pars->GC_11, amp[601]); 
  FFV1_0(w[167], w[17], w[4], pars->GC_11, amp[602]); 
  FFV1_0(w[39], w[157], w[4], pars->GC_11, amp[603]); 
  FFV1_0(w[1], w[152], w[44], pars->GC_11, amp[604]); 
  FFV1_0(w[45], w[152], w[3], pars->GC_11, amp[605]); 
  FFV1_0(w[153], w[6], w[44], pars->GC_11, amp[606]); 
  FFV1_0(w[153], w[46], w[3], pars->GC_11, amp[607]); 
  FFV1_0(w[1], w[154], w[44], pars->GC_11, amp[608]); 
  FFV1_0(w[45], w[154], w[3], pars->GC_11, amp[609]); 
  FFV1_0(w[155], w[6], w[44], pars->GC_11, amp[610]); 
  FFV1_0(w[155], w[46], w[3], pars->GC_11, amp[611]); 
  FFV1_0(w[153], w[47], w[5], pars->GC_11, amp[612]); 
  FFV1_0(w[153], w[48], w[3], pars->GC_11, amp[613]); 
  FFV1_0(w[155], w[47], w[5], pars->GC_11, amp[614]); 
  FFV1_0(w[155], w[48], w[3], pars->GC_11, amp[615]); 
  FFV1_0(w[20], w[160], w[3], pars->GC_11, amp[616]); 
  FFV1_0(w[158], w[27], w[3], pars->GC_11, amp[617]); 
  FFV1_0(w[20], w[161], w[3], pars->GC_11, amp[618]); 
  FFV1_0(w[159], w[27], w[3], pars->GC_11, amp[619]); 
  FFV1_0(w[49], w[152], w[5], pars->GC_11, amp[620]); 
  FFV1_0(w[50], w[152], w[3], pars->GC_11, amp[621]); 
  FFV1_0(w[49], w[154], w[5], pars->GC_11, amp[622]); 
  FFV1_0(w[50], w[154], w[3], pars->GC_11, amp[623]); 
  FFV1_0(w[162], w[17], w[3], pars->GC_11, amp[624]); 
  FFV1_0(w[30], w[156], w[3], pars->GC_11, amp[625]); 
  FFV1_0(w[163], w[17], w[3], pars->GC_11, amp[626]); 
  FFV1_0(w[30], w[157], w[3], pars->GC_11, amp[627]); 
  FFV1_0(w[153], w[51], w[4], pars->GC_11, amp[628]); 
  FFV1_0(w[153], w[52], w[3], pars->GC_11, amp[629]); 
  FFV1_0(w[155], w[51], w[4], pars->GC_11, amp[630]); 
  FFV1_0(w[155], w[52], w[3], pars->GC_11, amp[631]); 
  FFV1_0(w[53], w[152], w[4], pars->GC_11, amp[632]); 
  FFV1_0(w[54], w[152], w[3], pars->GC_11, amp[633]); 
  FFV1_0(w[53], w[154], w[4], pars->GC_11, amp[634]); 
  FFV1_0(w[54], w[154], w[3], pars->GC_11, amp[635]); 
  FFV1_0(w[1], w[152], w[55], pars->GC_11, amp[636]); 
  FFV1_0(w[1], w[152], w[56], pars->GC_11, amp[637]); 
  FFV1_0(w[1], w[152], w[57], pars->GC_11, amp[638]); 
  FFV1_0(w[153], w[6], w[55], pars->GC_11, amp[639]); 
  FFV1_0(w[153], w[6], w[56], pars->GC_11, amp[640]); 
  FFV1_0(w[153], w[6], w[57], pars->GC_11, amp[641]); 
  FFV1_0(w[1], w[154], w[55], pars->GC_11, amp[642]); 
  FFV1_0(w[1], w[154], w[56], pars->GC_11, amp[643]); 
  FFV1_0(w[1], w[154], w[57], pars->GC_11, amp[644]); 
  FFV1_0(w[155], w[6], w[55], pars->GC_11, amp[645]); 
  FFV1_0(w[155], w[6], w[56], pars->GC_11, amp[646]); 
  FFV1_0(w[155], w[6], w[57], pars->GC_11, amp[647]); 
  FFV1_0(w[75], w[168], w[10], pars->GC_11, amp[648]); 
  FFV1_0(w[77], w[168], w[5], pars->GC_11, amp[649]); 
  FFV1_0(w[169], w[74], w[10], pars->GC_11, amp[650]); 
  FFV1_0(w[169], w[79], w[5], pars->GC_11, amp[651]); 
  FFV1_0(w[75], w[170], w[10], pars->GC_11, amp[652]); 
  FFV1_0(w[77], w[170], w[5], pars->GC_11, amp[653]); 
  FFV1_0(w[171], w[74], w[10], pars->GC_11, amp[654]); 
  FFV1_0(w[171], w[79], w[5], pars->GC_11, amp[655]); 
  FFV1_0(w[75], w[172], w[8], pars->GC_11, amp[656]); 
  FFV1_0(w[169], w[82], w[8], pars->GC_11, amp[657]); 
  FFV1_0(w[75], w[173], w[8], pars->GC_11, amp[658]); 
  FFV1_0(w[171], w[82], w[8], pars->GC_11, amp[659]); 
  FFV1_0(w[174], w[74], w[8], pars->GC_11, amp[660]); 
  FFV1_0(w[85], w[168], w[8], pars->GC_11, amp[661]); 
  FFV1_0(w[175], w[74], w[8], pars->GC_11, amp[662]); 
  FFV1_0(w[85], w[170], w[8], pars->GC_11, amp[663]); 
  FFV1_0(w[75], w[168], w[24], pars->GC_11, amp[664]); 
  FFV1_0(w[88], w[168], w[4], pars->GC_11, amp[665]); 
  FFV1_0(w[169], w[74], w[24], pars->GC_11, amp[666]); 
  FFV1_0(w[169], w[89], w[4], pars->GC_11, amp[667]); 
  FFV1_0(w[75], w[170], w[24], pars->GC_11, amp[668]); 
  FFV1_0(w[88], w[170], w[4], pars->GC_11, amp[669]); 
  FFV1_0(w[171], w[74], w[24], pars->GC_11, amp[670]); 
  FFV1_0(w[171], w[89], w[4], pars->GC_11, amp[671]); 
  FFV1_0(w[75], w[176], w[23], pars->GC_11, amp[672]); 
  FFV1_0(w[169], w[90], w[23], pars->GC_11, amp[673]); 
  FFV1_0(w[75], w[177], w[23], pars->GC_11, amp[674]); 
  FFV1_0(w[171], w[90], w[23], pars->GC_11, amp[675]); 
  FFV1_0(w[178], w[74], w[23], pars->GC_11, amp[676]); 
  FFV1_0(w[93], w[168], w[23], pars->GC_11, amp[677]); 
  FFV1_0(w[179], w[74], w[23], pars->GC_11, amp[678]); 
  FFV1_0(w[93], w[170], w[23], pars->GC_11, amp[679]); 
  FFV1_0(w[169], w[97], w[5], pars->GC_11, amp[680]); 
  FFV1_0(w[169], w[98], w[4], pars->GC_11, amp[681]); 
  FFV1_0(w[171], w[97], w[5], pars->GC_11, amp[682]); 
  FFV1_0(w[171], w[98], w[4], pars->GC_11, amp[683]); 
  FFV1_0(w[75], w[180], w[36], pars->GC_11, amp[684]); 
  FFV1_0(w[169], w[96], w[36], pars->GC_11, amp[685]); 
  FFV1_0(w[75], w[181], w[36], pars->GC_11, amp[686]); 
  FFV1_0(w[171], w[96], w[36], pars->GC_11, amp[687]); 
  FFV1_0(w[93], w[180], w[5], pars->GC_11, amp[688]); 
  FFV1_0(w[178], w[96], w[5], pars->GC_11, amp[689]); 
  FFV1_0(w[93], w[181], w[5], pars->GC_11, amp[690]); 
  FFV1_0(w[179], w[96], w[5], pars->GC_11, amp[691]); 
  FFV1_0(w[85], w[180], w[4], pars->GC_11, amp[692]); 
  FFV1_0(w[174], w[96], w[4], pars->GC_11, amp[693]); 
  FFV1_0(w[85], w[181], w[4], pars->GC_11, amp[694]); 
  FFV1_0(w[175], w[96], w[4], pars->GC_11, amp[695]); 
  FFV1_0(w[102], w[168], w[5], pars->GC_11, amp[696]); 
  FFV1_0(w[103], w[168], w[4], pars->GC_11, amp[697]); 
  FFV1_0(w[102], w[170], w[5], pars->GC_11, amp[698]); 
  FFV1_0(w[103], w[170], w[4], pars->GC_11, amp[699]); 
  FFV1_0(w[182], w[74], w[36], pars->GC_11, amp[700]); 
  FFV1_0(w[101], w[168], w[36], pars->GC_11, amp[701]); 
  FFV1_0(w[183], w[74], w[36], pars->GC_11, amp[702]); 
  FFV1_0(w[101], w[170], w[36], pars->GC_11, amp[703]); 
  FFV1_0(w[182], w[90], w[5], pars->GC_11, amp[704]); 
  FFV1_0(w[101], w[176], w[5], pars->GC_11, amp[705]); 
  FFV1_0(w[183], w[90], w[5], pars->GC_11, amp[706]); 
  FFV1_0(w[101], w[177], w[5], pars->GC_11, amp[707]); 
  FFV1_0(w[182], w[82], w[4], pars->GC_11, amp[708]); 
  FFV1_0(w[101], w[172], w[4], pars->GC_11, amp[709]); 
  FFV1_0(w[183], w[82], w[4], pars->GC_11, amp[710]); 
  FFV1_0(w[101], w[173], w[4], pars->GC_11, amp[711]); 
  FFV1_0(w[75], w[168], w[44], pars->GC_11, amp[712]); 
  FFV1_0(w[106], w[168], w[3], pars->GC_11, amp[713]); 
  FFV1_0(w[169], w[74], w[44], pars->GC_11, amp[714]); 
  FFV1_0(w[169], w[107], w[3], pars->GC_11, amp[715]); 
  FFV1_0(w[75], w[170], w[44], pars->GC_11, amp[716]); 
  FFV1_0(w[106], w[170], w[3], pars->GC_11, amp[717]); 
  FFV1_0(w[171], w[74], w[44], pars->GC_11, amp[718]); 
  FFV1_0(w[171], w[107], w[3], pars->GC_11, amp[719]); 
  FFV1_0(w[169], w[108], w[5], pars->GC_11, amp[720]); 
  FFV1_0(w[169], w[109], w[3], pars->GC_11, amp[721]); 
  FFV1_0(w[171], w[108], w[5], pars->GC_11, amp[722]); 
  FFV1_0(w[171], w[109], w[3], pars->GC_11, amp[723]); 
  FFV1_0(w[85], w[176], w[3], pars->GC_11, amp[724]); 
  FFV1_0(w[174], w[90], w[3], pars->GC_11, amp[725]); 
  FFV1_0(w[85], w[177], w[3], pars->GC_11, amp[726]); 
  FFV1_0(w[175], w[90], w[3], pars->GC_11, amp[727]); 
  FFV1_0(w[110], w[168], w[5], pars->GC_11, amp[728]); 
  FFV1_0(w[111], w[168], w[3], pars->GC_11, amp[729]); 
  FFV1_0(w[110], w[170], w[5], pars->GC_11, amp[730]); 
  FFV1_0(w[111], w[170], w[3], pars->GC_11, amp[731]); 
  FFV1_0(w[178], w[82], w[3], pars->GC_11, amp[732]); 
  FFV1_0(w[93], w[172], w[3], pars->GC_11, amp[733]); 
  FFV1_0(w[179], w[82], w[3], pars->GC_11, amp[734]); 
  FFV1_0(w[93], w[173], w[3], pars->GC_11, amp[735]); 
  FFV1_0(w[169], w[112], w[4], pars->GC_11, amp[736]); 
  FFV1_0(w[169], w[113], w[3], pars->GC_11, amp[737]); 
  FFV1_0(w[171], w[112], w[4], pars->GC_11, amp[738]); 
  FFV1_0(w[171], w[113], w[3], pars->GC_11, amp[739]); 
  FFV1_0(w[114], w[168], w[4], pars->GC_11, amp[740]); 
  FFV1_0(w[115], w[168], w[3], pars->GC_11, amp[741]); 
  FFV1_0(w[114], w[170], w[4], pars->GC_11, amp[742]); 
  FFV1_0(w[115], w[170], w[3], pars->GC_11, amp[743]); 
  FFV1_0(w[75], w[168], w[55], pars->GC_11, amp[744]); 
  FFV1_0(w[75], w[168], w[56], pars->GC_11, amp[745]); 
  FFV1_0(w[75], w[168], w[57], pars->GC_11, amp[746]); 
  FFV1_0(w[169], w[74], w[55], pars->GC_11, amp[747]); 
  FFV1_0(w[169], w[74], w[56], pars->GC_11, amp[748]); 
  FFV1_0(w[169], w[74], w[57], pars->GC_11, amp[749]); 
  FFV1_0(w[75], w[170], w[55], pars->GC_11, amp[750]); 
  FFV1_0(w[75], w[170], w[56], pars->GC_11, amp[751]); 
  FFV1_0(w[75], w[170], w[57], pars->GC_11, amp[752]); 
  FFV1_0(w[171], w[74], w[55], pars->GC_11, amp[753]); 
  FFV1_0(w[171], w[74], w[56], pars->GC_11, amp[754]); 
  FFV1_0(w[171], w[74], w[57], pars->GC_11, amp[755]); 
  FFV1_0(w[75], w[184], w[10], pars->GC_11, amp[756]); 
  FFV1_0(w[77], w[184], w[5], pars->GC_11, amp[757]); 
  FFV1_0(w[185], w[74], w[10], pars->GC_11, amp[758]); 
  FFV1_0(w[185], w[79], w[5], pars->GC_11, amp[759]); 
  FFV1_0(w[75], w[186], w[10], pars->GC_11, amp[760]); 
  FFV1_0(w[77], w[186], w[5], pars->GC_11, amp[761]); 
  FFV1_0(w[187], w[74], w[10], pars->GC_11, amp[762]); 
  FFV1_0(w[187], w[79], w[5], pars->GC_11, amp[763]); 
  FFV1_0(w[75], w[188], w[8], pars->GC_11, amp[764]); 
  FFV1_0(w[185], w[82], w[8], pars->GC_11, amp[765]); 
  FFV1_0(w[75], w[189], w[8], pars->GC_11, amp[766]); 
  FFV1_0(w[187], w[82], w[8], pars->GC_11, amp[767]); 
  FFV1_0(w[190], w[74], w[8], pars->GC_11, amp[768]); 
  FFV1_0(w[85], w[184], w[8], pars->GC_11, amp[769]); 
  FFV1_0(w[191], w[74], w[8], pars->GC_11, amp[770]); 
  FFV1_0(w[85], w[186], w[8], pars->GC_11, amp[771]); 
  FFV1_0(w[75], w[184], w[24], pars->GC_11, amp[772]); 
  FFV1_0(w[88], w[184], w[4], pars->GC_11, amp[773]); 
  FFV1_0(w[185], w[74], w[24], pars->GC_11, amp[774]); 
  FFV1_0(w[185], w[89], w[4], pars->GC_11, amp[775]); 
  FFV1_0(w[75], w[186], w[24], pars->GC_11, amp[776]); 
  FFV1_0(w[88], w[186], w[4], pars->GC_11, amp[777]); 
  FFV1_0(w[187], w[74], w[24], pars->GC_11, amp[778]); 
  FFV1_0(w[187], w[89], w[4], pars->GC_11, amp[779]); 
  FFV1_0(w[75], w[192], w[23], pars->GC_11, amp[780]); 
  FFV1_0(w[185], w[90], w[23], pars->GC_11, amp[781]); 
  FFV1_0(w[75], w[193], w[23], pars->GC_11, amp[782]); 
  FFV1_0(w[187], w[90], w[23], pars->GC_11, amp[783]); 
  FFV1_0(w[194], w[74], w[23], pars->GC_11, amp[784]); 
  FFV1_0(w[93], w[184], w[23], pars->GC_11, amp[785]); 
  FFV1_0(w[195], w[74], w[23], pars->GC_11, amp[786]); 
  FFV1_0(w[93], w[186], w[23], pars->GC_11, amp[787]); 
  FFV1_0(w[185], w[97], w[5], pars->GC_11, amp[788]); 
  FFV1_0(w[185], w[98], w[4], pars->GC_11, amp[789]); 
  FFV1_0(w[187], w[97], w[5], pars->GC_11, amp[790]); 
  FFV1_0(w[187], w[98], w[4], pars->GC_11, amp[791]); 
  FFV1_0(w[75], w[196], w[36], pars->GC_11, amp[792]); 
  FFV1_0(w[185], w[96], w[36], pars->GC_11, amp[793]); 
  FFV1_0(w[75], w[197], w[36], pars->GC_11, amp[794]); 
  FFV1_0(w[187], w[96], w[36], pars->GC_11, amp[795]); 
  FFV1_0(w[93], w[196], w[5], pars->GC_11, amp[796]); 
  FFV1_0(w[194], w[96], w[5], pars->GC_11, amp[797]); 
  FFV1_0(w[93], w[197], w[5], pars->GC_11, amp[798]); 
  FFV1_0(w[195], w[96], w[5], pars->GC_11, amp[799]); 
  FFV1_0(w[85], w[196], w[4], pars->GC_11, amp[800]); 
  FFV1_0(w[190], w[96], w[4], pars->GC_11, amp[801]); 
  FFV1_0(w[85], w[197], w[4], pars->GC_11, amp[802]); 
  FFV1_0(w[191], w[96], w[4], pars->GC_11, amp[803]); 
  FFV1_0(w[102], w[184], w[5], pars->GC_11, amp[804]); 
  FFV1_0(w[103], w[184], w[4], pars->GC_11, amp[805]); 
  FFV1_0(w[102], w[186], w[5], pars->GC_11, amp[806]); 
  FFV1_0(w[103], w[186], w[4], pars->GC_11, amp[807]); 
  FFV1_0(w[198], w[74], w[36], pars->GC_11, amp[808]); 
  FFV1_0(w[101], w[184], w[36], pars->GC_11, amp[809]); 
  FFV1_0(w[199], w[74], w[36], pars->GC_11, amp[810]); 
  FFV1_0(w[101], w[186], w[36], pars->GC_11, amp[811]); 
  FFV1_0(w[198], w[90], w[5], pars->GC_11, amp[812]); 
  FFV1_0(w[101], w[192], w[5], pars->GC_11, amp[813]); 
  FFV1_0(w[199], w[90], w[5], pars->GC_11, amp[814]); 
  FFV1_0(w[101], w[193], w[5], pars->GC_11, amp[815]); 
  FFV1_0(w[198], w[82], w[4], pars->GC_11, amp[816]); 
  FFV1_0(w[101], w[188], w[4], pars->GC_11, amp[817]); 
  FFV1_0(w[199], w[82], w[4], pars->GC_11, amp[818]); 
  FFV1_0(w[101], w[189], w[4], pars->GC_11, amp[819]); 
  FFV1_0(w[75], w[184], w[44], pars->GC_11, amp[820]); 
  FFV1_0(w[106], w[184], w[3], pars->GC_11, amp[821]); 
  FFV1_0(w[185], w[74], w[44], pars->GC_11, amp[822]); 
  FFV1_0(w[185], w[107], w[3], pars->GC_11, amp[823]); 
  FFV1_0(w[75], w[186], w[44], pars->GC_11, amp[824]); 
  FFV1_0(w[106], w[186], w[3], pars->GC_11, amp[825]); 
  FFV1_0(w[187], w[74], w[44], pars->GC_11, amp[826]); 
  FFV1_0(w[187], w[107], w[3], pars->GC_11, amp[827]); 
  FFV1_0(w[185], w[108], w[5], pars->GC_11, amp[828]); 
  FFV1_0(w[185], w[109], w[3], pars->GC_11, amp[829]); 
  FFV1_0(w[187], w[108], w[5], pars->GC_11, amp[830]); 
  FFV1_0(w[187], w[109], w[3], pars->GC_11, amp[831]); 
  FFV1_0(w[85], w[192], w[3], pars->GC_11, amp[832]); 
  FFV1_0(w[190], w[90], w[3], pars->GC_11, amp[833]); 
  FFV1_0(w[85], w[193], w[3], pars->GC_11, amp[834]); 
  FFV1_0(w[191], w[90], w[3], pars->GC_11, amp[835]); 
  FFV1_0(w[110], w[184], w[5], pars->GC_11, amp[836]); 
  FFV1_0(w[111], w[184], w[3], pars->GC_11, amp[837]); 
  FFV1_0(w[110], w[186], w[5], pars->GC_11, amp[838]); 
  FFV1_0(w[111], w[186], w[3], pars->GC_11, amp[839]); 
  FFV1_0(w[194], w[82], w[3], pars->GC_11, amp[840]); 
  FFV1_0(w[93], w[188], w[3], pars->GC_11, amp[841]); 
  FFV1_0(w[195], w[82], w[3], pars->GC_11, amp[842]); 
  FFV1_0(w[93], w[189], w[3], pars->GC_11, amp[843]); 
  FFV1_0(w[185], w[112], w[4], pars->GC_11, amp[844]); 
  FFV1_0(w[185], w[113], w[3], pars->GC_11, amp[845]); 
  FFV1_0(w[187], w[112], w[4], pars->GC_11, amp[846]); 
  FFV1_0(w[187], w[113], w[3], pars->GC_11, amp[847]); 
  FFV1_0(w[114], w[184], w[4], pars->GC_11, amp[848]); 
  FFV1_0(w[115], w[184], w[3], pars->GC_11, amp[849]); 
  FFV1_0(w[114], w[186], w[4], pars->GC_11, amp[850]); 
  FFV1_0(w[115], w[186], w[3], pars->GC_11, amp[851]); 
  FFV1_0(w[75], w[184], w[55], pars->GC_11, amp[852]); 
  FFV1_0(w[75], w[184], w[56], pars->GC_11, amp[853]); 
  FFV1_0(w[75], w[184], w[57], pars->GC_11, amp[854]); 
  FFV1_0(w[185], w[74], w[55], pars->GC_11, amp[855]); 
  FFV1_0(w[185], w[74], w[56], pars->GC_11, amp[856]); 
  FFV1_0(w[185], w[74], w[57], pars->GC_11, amp[857]); 
  FFV1_0(w[75], w[186], w[55], pars->GC_11, amp[858]); 
  FFV1_0(w[75], w[186], w[56], pars->GC_11, amp[859]); 
  FFV1_0(w[75], w[186], w[57], pars->GC_11, amp[860]); 
  FFV1_0(w[187], w[74], w[55], pars->GC_11, amp[861]); 
  FFV1_0(w[187], w[74], w[56], pars->GC_11, amp[862]); 
  FFV1_0(w[187], w[74], w[57], pars->GC_11, amp[863]); 
  FFV1_0(w[1], w[201], w[10], pars->GC_11, amp[864]); 
  FFV1_0(w[11], w[201], w[5], pars->GC_11, amp[865]); 
  FFV1_0(w[202], w[6], w[10], pars->GC_11, amp[866]); 
  FFV1_0(w[202], w[13], w[5], pars->GC_11, amp[867]); 
  FFV1_0(w[1], w[203], w[8], pars->GC_11, amp[868]); 
  FFV1_0(w[202], w[17], w[8], pars->GC_11, amp[869]); 
  FFV1_0(w[204], w[6], w[8], pars->GC_11, amp[870]); 
  FFV1_0(w[20], w[201], w[8], pars->GC_11, amp[871]); 
  FFV1_0(w[1], w[201], w[24], pars->GC_11, amp[872]); 
  FFV1_0(w[25], w[201], w[4], pars->GC_11, amp[873]); 
  FFV1_0(w[202], w[6], w[24], pars->GC_11, amp[874]); 
  FFV1_0(w[202], w[26], w[4], pars->GC_11, amp[875]); 
  FFV1_0(w[1], w[205], w[23], pars->GC_11, amp[876]); 
  FFV1_0(w[202], w[27], w[23], pars->GC_11, amp[877]); 
  FFV1_0(w[206], w[6], w[23], pars->GC_11, amp[878]); 
  FFV1_0(w[30], w[201], w[23], pars->GC_11, amp[879]); 
  FFV1_0(w[202], w[34], w[5], pars->GC_11, amp[880]); 
  FFV1_0(w[202], w[35], w[4], pars->GC_11, amp[881]); 
  FFV1_0(w[1], w[207], w[36], pars->GC_11, amp[882]); 
  FFV1_0(w[202], w[33], w[36], pars->GC_11, amp[883]); 
  FFV1_0(w[30], w[207], w[5], pars->GC_11, amp[884]); 
  FFV1_0(w[206], w[33], w[5], pars->GC_11, amp[885]); 
  FFV1_0(w[20], w[207], w[4], pars->GC_11, amp[886]); 
  FFV1_0(w[204], w[33], w[4], pars->GC_11, amp[887]); 
  FFV1_0(w[40], w[201], w[5], pars->GC_11, amp[888]); 
  FFV1_0(w[41], w[201], w[4], pars->GC_11, amp[889]); 
  FFV1_0(w[208], w[6], w[36], pars->GC_11, amp[890]); 
  FFV1_0(w[39], w[201], w[36], pars->GC_11, amp[891]); 
  FFV1_0(w[208], w[27], w[5], pars->GC_11, amp[892]); 
  FFV1_0(w[39], w[205], w[5], pars->GC_11, amp[893]); 
  FFV1_0(w[208], w[17], w[4], pars->GC_11, amp[894]); 
  FFV1_0(w[39], w[203], w[4], pars->GC_11, amp[895]); 
  FFV1_0(w[1], w[201], w[44], pars->GC_11, amp[896]); 
  FFV1_0(w[45], w[201], w[3], pars->GC_11, amp[897]); 
  FFV1_0(w[202], w[6], w[44], pars->GC_11, amp[898]); 
  FFV1_0(w[202], w[46], w[3], pars->GC_11, amp[899]); 
  FFV1_0(w[202], w[47], w[5], pars->GC_11, amp[900]); 
  FFV1_0(w[202], w[48], w[3], pars->GC_11, amp[901]); 
  FFV1_0(w[20], w[205], w[3], pars->GC_11, amp[902]); 
  FFV1_0(w[204], w[27], w[3], pars->GC_11, amp[903]); 
  FFV1_0(w[49], w[201], w[5], pars->GC_11, amp[904]); 
  FFV1_0(w[50], w[201], w[3], pars->GC_11, amp[905]); 
  FFV1_0(w[206], w[17], w[3], pars->GC_11, amp[906]); 
  FFV1_0(w[30], w[203], w[3], pars->GC_11, amp[907]); 
  FFV1_0(w[202], w[51], w[4], pars->GC_11, amp[908]); 
  FFV1_0(w[202], w[52], w[3], pars->GC_11, amp[909]); 
  FFV1_0(w[53], w[201], w[4], pars->GC_11, amp[910]); 
  FFV1_0(w[54], w[201], w[3], pars->GC_11, amp[911]); 
  FFV1_0(w[1], w[201], w[55], pars->GC_11, amp[912]); 
  FFV1_0(w[1], w[201], w[56], pars->GC_11, amp[913]); 
  FFV1_0(w[1], w[201], w[57], pars->GC_11, amp[914]); 
  FFV1_0(w[202], w[6], w[55], pars->GC_11, amp[915]); 
  FFV1_0(w[202], w[6], w[56], pars->GC_11, amp[916]); 
  FFV1_0(w[202], w[6], w[57], pars->GC_11, amp[917]); 
  FFV1_0(w[75], w[209], w[10], pars->GC_11, amp[918]); 
  FFV1_0(w[77], w[209], w[5], pars->GC_11, amp[919]); 
  FFV1_0(w[210], w[74], w[10], pars->GC_11, amp[920]); 
  FFV1_0(w[210], w[79], w[5], pars->GC_11, amp[921]); 
  FFV1_0(w[75], w[211], w[8], pars->GC_11, amp[922]); 
  FFV1_0(w[210], w[82], w[8], pars->GC_11, amp[923]); 
  FFV1_0(w[212], w[74], w[8], pars->GC_11, amp[924]); 
  FFV1_0(w[85], w[209], w[8], pars->GC_11, amp[925]); 
  FFV1_0(w[75], w[209], w[24], pars->GC_11, amp[926]); 
  FFV1_0(w[88], w[209], w[4], pars->GC_11, amp[927]); 
  FFV1_0(w[210], w[74], w[24], pars->GC_11, amp[928]); 
  FFV1_0(w[210], w[89], w[4], pars->GC_11, amp[929]); 
  FFV1_0(w[75], w[213], w[23], pars->GC_11, amp[930]); 
  FFV1_0(w[210], w[90], w[23], pars->GC_11, amp[931]); 
  FFV1_0(w[214], w[74], w[23], pars->GC_11, amp[932]); 
  FFV1_0(w[93], w[209], w[23], pars->GC_11, amp[933]); 
  FFV1_0(w[210], w[97], w[5], pars->GC_11, amp[934]); 
  FFV1_0(w[210], w[98], w[4], pars->GC_11, amp[935]); 
  FFV1_0(w[75], w[215], w[36], pars->GC_11, amp[936]); 
  FFV1_0(w[210], w[96], w[36], pars->GC_11, amp[937]); 
  FFV1_0(w[93], w[215], w[5], pars->GC_11, amp[938]); 
  FFV1_0(w[214], w[96], w[5], pars->GC_11, amp[939]); 
  FFV1_0(w[85], w[215], w[4], pars->GC_11, amp[940]); 
  FFV1_0(w[212], w[96], w[4], pars->GC_11, amp[941]); 
  FFV1_0(w[102], w[209], w[5], pars->GC_11, amp[942]); 
  FFV1_0(w[103], w[209], w[4], pars->GC_11, amp[943]); 
  FFV1_0(w[216], w[74], w[36], pars->GC_11, amp[944]); 
  FFV1_0(w[101], w[209], w[36], pars->GC_11, amp[945]); 
  FFV1_0(w[216], w[90], w[5], pars->GC_11, amp[946]); 
  FFV1_0(w[101], w[213], w[5], pars->GC_11, amp[947]); 
  FFV1_0(w[216], w[82], w[4], pars->GC_11, amp[948]); 
  FFV1_0(w[101], w[211], w[4], pars->GC_11, amp[949]); 
  FFV1_0(w[75], w[209], w[44], pars->GC_11, amp[950]); 
  FFV1_0(w[106], w[209], w[3], pars->GC_11, amp[951]); 
  FFV1_0(w[210], w[74], w[44], pars->GC_11, amp[952]); 
  FFV1_0(w[210], w[107], w[3], pars->GC_11, amp[953]); 
  FFV1_0(w[210], w[108], w[5], pars->GC_11, amp[954]); 
  FFV1_0(w[210], w[109], w[3], pars->GC_11, amp[955]); 
  FFV1_0(w[85], w[213], w[3], pars->GC_11, amp[956]); 
  FFV1_0(w[212], w[90], w[3], pars->GC_11, amp[957]); 
  FFV1_0(w[110], w[209], w[5], pars->GC_11, amp[958]); 
  FFV1_0(w[111], w[209], w[3], pars->GC_11, amp[959]); 
  FFV1_0(w[214], w[82], w[3], pars->GC_11, amp[960]); 
  FFV1_0(w[93], w[211], w[3], pars->GC_11, amp[961]); 
  FFV1_0(w[210], w[112], w[4], pars->GC_11, amp[962]); 
  FFV1_0(w[210], w[113], w[3], pars->GC_11, amp[963]); 
  FFV1_0(w[114], w[209], w[4], pars->GC_11, amp[964]); 
  FFV1_0(w[115], w[209], w[3], pars->GC_11, amp[965]); 
  FFV1_0(w[75], w[209], w[55], pars->GC_11, amp[966]); 
  FFV1_0(w[75], w[209], w[56], pars->GC_11, amp[967]); 
  FFV1_0(w[75], w[209], w[57], pars->GC_11, amp[968]); 
  FFV1_0(w[210], w[74], w[55], pars->GC_11, amp[969]); 
  FFV1_0(w[210], w[74], w[56], pars->GC_11, amp[970]); 
  FFV1_0(w[210], w[74], w[57], pars->GC_11, amp[971]); 
  FFV1_0(w[1], w[218], w[10], pars->GC_11, amp[972]); 
  FFV1_0(w[11], w[218], w[5], pars->GC_11, amp[973]); 
  FFV1_0(w[219], w[6], w[10], pars->GC_11, amp[974]); 
  FFV1_0(w[219], w[13], w[5], pars->GC_11, amp[975]); 
  FFV1_0(w[1], w[220], w[8], pars->GC_11, amp[976]); 
  FFV1_0(w[219], w[17], w[8], pars->GC_11, amp[977]); 
  FFV1_0(w[221], w[6], w[8], pars->GC_11, amp[978]); 
  FFV1_0(w[20], w[218], w[8], pars->GC_11, amp[979]); 
  FFV1_0(w[1], w[218], w[24], pars->GC_11, amp[980]); 
  FFV1_0(w[25], w[218], w[4], pars->GC_11, amp[981]); 
  FFV1_0(w[219], w[6], w[24], pars->GC_11, amp[982]); 
  FFV1_0(w[219], w[26], w[4], pars->GC_11, amp[983]); 
  FFV1_0(w[1], w[222], w[23], pars->GC_11, amp[984]); 
  FFV1_0(w[219], w[27], w[23], pars->GC_11, amp[985]); 
  FFV1_0(w[223], w[6], w[23], pars->GC_11, amp[986]); 
  FFV1_0(w[30], w[218], w[23], pars->GC_11, amp[987]); 
  FFV1_0(w[219], w[34], w[5], pars->GC_11, amp[988]); 
  FFV1_0(w[219], w[35], w[4], pars->GC_11, amp[989]); 
  FFV1_0(w[1], w[224], w[36], pars->GC_11, amp[990]); 
  FFV1_0(w[219], w[33], w[36], pars->GC_11, amp[991]); 
  FFV1_0(w[30], w[224], w[5], pars->GC_11, amp[992]); 
  FFV1_0(w[223], w[33], w[5], pars->GC_11, amp[993]); 
  FFV1_0(w[20], w[224], w[4], pars->GC_11, amp[994]); 
  FFV1_0(w[221], w[33], w[4], pars->GC_11, amp[995]); 
  FFV1_0(w[40], w[218], w[5], pars->GC_11, amp[996]); 
  FFV1_0(w[41], w[218], w[4], pars->GC_11, amp[997]); 
  FFV1_0(w[225], w[6], w[36], pars->GC_11, amp[998]); 
  FFV1_0(w[39], w[218], w[36], pars->GC_11, amp[999]); 
  FFV1_0(w[225], w[27], w[5], pars->GC_11, amp[1000]); 
  FFV1_0(w[39], w[222], w[5], pars->GC_11, amp[1001]); 
  FFV1_0(w[225], w[17], w[4], pars->GC_11, amp[1002]); 
  FFV1_0(w[39], w[220], w[4], pars->GC_11, amp[1003]); 
  FFV1_0(w[1], w[218], w[44], pars->GC_11, amp[1004]); 
  FFV1_0(w[45], w[218], w[3], pars->GC_11, amp[1005]); 
  FFV1_0(w[219], w[6], w[44], pars->GC_11, amp[1006]); 
  FFV1_0(w[219], w[46], w[3], pars->GC_11, amp[1007]); 
  FFV1_0(w[219], w[47], w[5], pars->GC_11, amp[1008]); 
  FFV1_0(w[219], w[48], w[3], pars->GC_11, amp[1009]); 
  FFV1_0(w[20], w[222], w[3], pars->GC_11, amp[1010]); 
  FFV1_0(w[221], w[27], w[3], pars->GC_11, amp[1011]); 
  FFV1_0(w[49], w[218], w[5], pars->GC_11, amp[1012]); 
  FFV1_0(w[50], w[218], w[3], pars->GC_11, amp[1013]); 
  FFV1_0(w[223], w[17], w[3], pars->GC_11, amp[1014]); 
  FFV1_0(w[30], w[220], w[3], pars->GC_11, amp[1015]); 
  FFV1_0(w[219], w[51], w[4], pars->GC_11, amp[1016]); 
  FFV1_0(w[219], w[52], w[3], pars->GC_11, amp[1017]); 
  FFV1_0(w[53], w[218], w[4], pars->GC_11, amp[1018]); 
  FFV1_0(w[54], w[218], w[3], pars->GC_11, amp[1019]); 
  FFV1_0(w[1], w[218], w[55], pars->GC_11, amp[1020]); 
  FFV1_0(w[1], w[218], w[56], pars->GC_11, amp[1021]); 
  FFV1_0(w[1], w[218], w[57], pars->GC_11, amp[1022]); 
  FFV1_0(w[219], w[6], w[55], pars->GC_11, amp[1023]); 
  FFV1_0(w[219], w[6], w[56], pars->GC_11, amp[1024]); 
  FFV1_0(w[219], w[6], w[57], pars->GC_11, amp[1025]); 
  FFV1_0(w[1], w[226], w[10], pars->GC_11, amp[1026]); 
  FFV1_0(w[11], w[226], w[5], pars->GC_11, amp[1027]); 
  FFV1_0(w[227], w[6], w[10], pars->GC_11, amp[1028]); 
  FFV1_0(w[227], w[13], w[5], pars->GC_11, amp[1029]); 
  FFV1_0(w[1], w[228], w[8], pars->GC_11, amp[1030]); 
  FFV1_0(w[227], w[17], w[8], pars->GC_11, amp[1031]); 
  FFV1_0(w[229], w[6], w[8], pars->GC_11, amp[1032]); 
  FFV1_0(w[20], w[226], w[8], pars->GC_11, amp[1033]); 
  FFV1_0(w[1], w[226], w[24], pars->GC_11, amp[1034]); 
  FFV1_0(w[25], w[226], w[4], pars->GC_11, amp[1035]); 
  FFV1_0(w[227], w[6], w[24], pars->GC_11, amp[1036]); 
  FFV1_0(w[227], w[26], w[4], pars->GC_11, amp[1037]); 
  FFV1_0(w[1], w[230], w[23], pars->GC_11, amp[1038]); 
  FFV1_0(w[227], w[27], w[23], pars->GC_11, amp[1039]); 
  FFV1_0(w[231], w[6], w[23], pars->GC_11, amp[1040]); 
  FFV1_0(w[30], w[226], w[23], pars->GC_11, amp[1041]); 
  FFV1_0(w[227], w[34], w[5], pars->GC_11, amp[1042]); 
  FFV1_0(w[227], w[35], w[4], pars->GC_11, amp[1043]); 
  FFV1_0(w[1], w[232], w[36], pars->GC_11, amp[1044]); 
  FFV1_0(w[227], w[33], w[36], pars->GC_11, amp[1045]); 
  FFV1_0(w[30], w[232], w[5], pars->GC_11, amp[1046]); 
  FFV1_0(w[231], w[33], w[5], pars->GC_11, amp[1047]); 
  FFV1_0(w[20], w[232], w[4], pars->GC_11, amp[1048]); 
  FFV1_0(w[229], w[33], w[4], pars->GC_11, amp[1049]); 
  FFV1_0(w[40], w[226], w[5], pars->GC_11, amp[1050]); 
  FFV1_0(w[41], w[226], w[4], pars->GC_11, amp[1051]); 
  FFV1_0(w[233], w[6], w[36], pars->GC_11, amp[1052]); 
  FFV1_0(w[39], w[226], w[36], pars->GC_11, amp[1053]); 
  FFV1_0(w[233], w[27], w[5], pars->GC_11, amp[1054]); 
  FFV1_0(w[39], w[230], w[5], pars->GC_11, amp[1055]); 
  FFV1_0(w[233], w[17], w[4], pars->GC_11, amp[1056]); 
  FFV1_0(w[39], w[228], w[4], pars->GC_11, amp[1057]); 
  FFV1_0(w[1], w[226], w[44], pars->GC_11, amp[1058]); 
  FFV1_0(w[45], w[226], w[3], pars->GC_11, amp[1059]); 
  FFV1_0(w[227], w[6], w[44], pars->GC_11, amp[1060]); 
  FFV1_0(w[227], w[46], w[3], pars->GC_11, amp[1061]); 
  FFV1_0(w[227], w[47], w[5], pars->GC_11, amp[1062]); 
  FFV1_0(w[227], w[48], w[3], pars->GC_11, amp[1063]); 
  FFV1_0(w[20], w[230], w[3], pars->GC_11, amp[1064]); 
  FFV1_0(w[229], w[27], w[3], pars->GC_11, amp[1065]); 
  FFV1_0(w[49], w[226], w[5], pars->GC_11, amp[1066]); 
  FFV1_0(w[50], w[226], w[3], pars->GC_11, amp[1067]); 
  FFV1_0(w[231], w[17], w[3], pars->GC_11, amp[1068]); 
  FFV1_0(w[30], w[228], w[3], pars->GC_11, amp[1069]); 
  FFV1_0(w[227], w[51], w[4], pars->GC_11, amp[1070]); 
  FFV1_0(w[227], w[52], w[3], pars->GC_11, amp[1071]); 
  FFV1_0(w[53], w[226], w[4], pars->GC_11, amp[1072]); 
  FFV1_0(w[54], w[226], w[3], pars->GC_11, amp[1073]); 
  FFV1_0(w[1], w[226], w[55], pars->GC_11, amp[1074]); 
  FFV1_0(w[1], w[226], w[56], pars->GC_11, amp[1075]); 
  FFV1_0(w[1], w[226], w[57], pars->GC_11, amp[1076]); 
  FFV1_0(w[227], w[6], w[55], pars->GC_11, amp[1077]); 
  FFV1_0(w[227], w[6], w[56], pars->GC_11, amp[1078]); 
  FFV1_0(w[227], w[6], w[57], pars->GC_11, amp[1079]); 
  FFV1_0(w[75], w[234], w[10], pars->GC_11, amp[1080]); 
  FFV1_0(w[77], w[234], w[5], pars->GC_11, amp[1081]); 
  FFV1_0(w[235], w[74], w[10], pars->GC_11, amp[1082]); 
  FFV1_0(w[235], w[79], w[5], pars->GC_11, amp[1083]); 
  FFV1_0(w[75], w[236], w[8], pars->GC_11, amp[1084]); 
  FFV1_0(w[235], w[82], w[8], pars->GC_11, amp[1085]); 
  FFV1_0(w[237], w[74], w[8], pars->GC_11, amp[1086]); 
  FFV1_0(w[85], w[234], w[8], pars->GC_11, amp[1087]); 
  FFV1_0(w[75], w[234], w[24], pars->GC_11, amp[1088]); 
  FFV1_0(w[88], w[234], w[4], pars->GC_11, amp[1089]); 
  FFV1_0(w[235], w[74], w[24], pars->GC_11, amp[1090]); 
  FFV1_0(w[235], w[89], w[4], pars->GC_11, amp[1091]); 
  FFV1_0(w[75], w[238], w[23], pars->GC_11, amp[1092]); 
  FFV1_0(w[235], w[90], w[23], pars->GC_11, amp[1093]); 
  FFV1_0(w[239], w[74], w[23], pars->GC_11, amp[1094]); 
  FFV1_0(w[93], w[234], w[23], pars->GC_11, amp[1095]); 
  FFV1_0(w[235], w[97], w[5], pars->GC_11, amp[1096]); 
  FFV1_0(w[235], w[98], w[4], pars->GC_11, amp[1097]); 
  FFV1_0(w[75], w[240], w[36], pars->GC_11, amp[1098]); 
  FFV1_0(w[235], w[96], w[36], pars->GC_11, amp[1099]); 
  FFV1_0(w[93], w[240], w[5], pars->GC_11, amp[1100]); 
  FFV1_0(w[239], w[96], w[5], pars->GC_11, amp[1101]); 
  FFV1_0(w[85], w[240], w[4], pars->GC_11, amp[1102]); 
  FFV1_0(w[237], w[96], w[4], pars->GC_11, amp[1103]); 
  FFV1_0(w[102], w[234], w[5], pars->GC_11, amp[1104]); 
  FFV1_0(w[103], w[234], w[4], pars->GC_11, amp[1105]); 
  FFV1_0(w[241], w[74], w[36], pars->GC_11, amp[1106]); 
  FFV1_0(w[101], w[234], w[36], pars->GC_11, amp[1107]); 
  FFV1_0(w[241], w[90], w[5], pars->GC_11, amp[1108]); 
  FFV1_0(w[101], w[238], w[5], pars->GC_11, amp[1109]); 
  FFV1_0(w[241], w[82], w[4], pars->GC_11, amp[1110]); 
  FFV1_0(w[101], w[236], w[4], pars->GC_11, amp[1111]); 
  FFV1_0(w[75], w[234], w[44], pars->GC_11, amp[1112]); 
  FFV1_0(w[106], w[234], w[3], pars->GC_11, amp[1113]); 
  FFV1_0(w[235], w[74], w[44], pars->GC_11, amp[1114]); 
  FFV1_0(w[235], w[107], w[3], pars->GC_11, amp[1115]); 
  FFV1_0(w[235], w[108], w[5], pars->GC_11, amp[1116]); 
  FFV1_0(w[235], w[109], w[3], pars->GC_11, amp[1117]); 
  FFV1_0(w[85], w[238], w[3], pars->GC_11, amp[1118]); 
  FFV1_0(w[237], w[90], w[3], pars->GC_11, amp[1119]); 
  FFV1_0(w[110], w[234], w[5], pars->GC_11, amp[1120]); 
  FFV1_0(w[111], w[234], w[3], pars->GC_11, amp[1121]); 
  FFV1_0(w[239], w[82], w[3], pars->GC_11, amp[1122]); 
  FFV1_0(w[93], w[236], w[3], pars->GC_11, amp[1123]); 
  FFV1_0(w[235], w[112], w[4], pars->GC_11, amp[1124]); 
  FFV1_0(w[235], w[113], w[3], pars->GC_11, amp[1125]); 
  FFV1_0(w[114], w[234], w[4], pars->GC_11, amp[1126]); 
  FFV1_0(w[115], w[234], w[3], pars->GC_11, amp[1127]); 
  FFV1_0(w[75], w[234], w[55], pars->GC_11, amp[1128]); 
  FFV1_0(w[75], w[234], w[56], pars->GC_11, amp[1129]); 
  FFV1_0(w[75], w[234], w[57], pars->GC_11, amp[1130]); 
  FFV1_0(w[235], w[74], w[55], pars->GC_11, amp[1131]); 
  FFV1_0(w[235], w[74], w[56], pars->GC_11, amp[1132]); 
  FFV1_0(w[235], w[74], w[57], pars->GC_11, amp[1133]); 
  FFV1_0(w[75], w[242], w[10], pars->GC_11, amp[1134]); 
  FFV1_0(w[77], w[242], w[5], pars->GC_11, amp[1135]); 
  FFV1_0(w[243], w[74], w[10], pars->GC_11, amp[1136]); 
  FFV1_0(w[243], w[79], w[5], pars->GC_11, amp[1137]); 
  FFV1_0(w[75], w[244], w[8], pars->GC_11, amp[1138]); 
  FFV1_0(w[243], w[82], w[8], pars->GC_11, amp[1139]); 
  FFV1_0(w[245], w[74], w[8], pars->GC_11, amp[1140]); 
  FFV1_0(w[85], w[242], w[8], pars->GC_11, amp[1141]); 
  FFV1_0(w[75], w[242], w[24], pars->GC_11, amp[1142]); 
  FFV1_0(w[88], w[242], w[4], pars->GC_11, amp[1143]); 
  FFV1_0(w[243], w[74], w[24], pars->GC_11, amp[1144]); 
  FFV1_0(w[243], w[89], w[4], pars->GC_11, amp[1145]); 
  FFV1_0(w[75], w[246], w[23], pars->GC_11, amp[1146]); 
  FFV1_0(w[243], w[90], w[23], pars->GC_11, amp[1147]); 
  FFV1_0(w[247], w[74], w[23], pars->GC_11, amp[1148]); 
  FFV1_0(w[93], w[242], w[23], pars->GC_11, amp[1149]); 
  FFV1_0(w[243], w[97], w[5], pars->GC_11, amp[1150]); 
  FFV1_0(w[243], w[98], w[4], pars->GC_11, amp[1151]); 
  FFV1_0(w[75], w[248], w[36], pars->GC_11, amp[1152]); 
  FFV1_0(w[243], w[96], w[36], pars->GC_11, amp[1153]); 
  FFV1_0(w[93], w[248], w[5], pars->GC_11, amp[1154]); 
  FFV1_0(w[247], w[96], w[5], pars->GC_11, amp[1155]); 
  FFV1_0(w[85], w[248], w[4], pars->GC_11, amp[1156]); 
  FFV1_0(w[245], w[96], w[4], pars->GC_11, amp[1157]); 
  FFV1_0(w[102], w[242], w[5], pars->GC_11, amp[1158]); 
  FFV1_0(w[103], w[242], w[4], pars->GC_11, amp[1159]); 
  FFV1_0(w[249], w[74], w[36], pars->GC_11, amp[1160]); 
  FFV1_0(w[101], w[242], w[36], pars->GC_11, amp[1161]); 
  FFV1_0(w[249], w[90], w[5], pars->GC_11, amp[1162]); 
  FFV1_0(w[101], w[246], w[5], pars->GC_11, amp[1163]); 
  FFV1_0(w[249], w[82], w[4], pars->GC_11, amp[1164]); 
  FFV1_0(w[101], w[244], w[4], pars->GC_11, amp[1165]); 
  FFV1_0(w[75], w[242], w[44], pars->GC_11, amp[1166]); 
  FFV1_0(w[106], w[242], w[3], pars->GC_11, amp[1167]); 
  FFV1_0(w[243], w[74], w[44], pars->GC_11, amp[1168]); 
  FFV1_0(w[243], w[107], w[3], pars->GC_11, amp[1169]); 
  FFV1_0(w[243], w[108], w[5], pars->GC_11, amp[1170]); 
  FFV1_0(w[243], w[109], w[3], pars->GC_11, amp[1171]); 
  FFV1_0(w[85], w[246], w[3], pars->GC_11, amp[1172]); 
  FFV1_0(w[245], w[90], w[3], pars->GC_11, amp[1173]); 
  FFV1_0(w[110], w[242], w[5], pars->GC_11, amp[1174]); 
  FFV1_0(w[111], w[242], w[3], pars->GC_11, amp[1175]); 
  FFV1_0(w[247], w[82], w[3], pars->GC_11, amp[1176]); 
  FFV1_0(w[93], w[244], w[3], pars->GC_11, amp[1177]); 
  FFV1_0(w[243], w[112], w[4], pars->GC_11, amp[1178]); 
  FFV1_0(w[243], w[113], w[3], pars->GC_11, amp[1179]); 
  FFV1_0(w[114], w[242], w[4], pars->GC_11, amp[1180]); 
  FFV1_0(w[115], w[242], w[3], pars->GC_11, amp[1181]); 
  FFV1_0(w[75], w[242], w[55], pars->GC_11, amp[1182]); 
  FFV1_0(w[75], w[242], w[56], pars->GC_11, amp[1183]); 
  FFV1_0(w[75], w[242], w[57], pars->GC_11, amp[1184]); 
  FFV1_0(w[243], w[74], w[55], pars->GC_11, amp[1185]); 
  FFV1_0(w[243], w[74], w[56], pars->GC_11, amp[1186]); 
  FFV1_0(w[243], w[74], w[57], pars->GC_11, amp[1187]); 
  FFV1_0(w[1], w[251], w[10], pars->GC_11, amp[1188]); 
  FFV1_0(w[11], w[251], w[5], pars->GC_11, amp[1189]); 
  FFV1_0(w[252], w[6], w[10], pars->GC_11, amp[1190]); 
  FFV1_0(w[252], w[13], w[5], pars->GC_11, amp[1191]); 
  FFV1_0(w[1], w[253], w[8], pars->GC_11, amp[1192]); 
  FFV1_0(w[252], w[17], w[8], pars->GC_11, amp[1193]); 
  FFV1_0(w[254], w[6], w[8], pars->GC_11, amp[1194]); 
  FFV1_0(w[20], w[251], w[8], pars->GC_11, amp[1195]); 
  FFV1_0(w[1], w[251], w[24], pars->GC_11, amp[1196]); 
  FFV1_0(w[25], w[251], w[4], pars->GC_11, amp[1197]); 
  FFV1_0(w[252], w[6], w[24], pars->GC_11, amp[1198]); 
  FFV1_0(w[252], w[26], w[4], pars->GC_11, amp[1199]); 
  FFV1_0(w[1], w[255], w[23], pars->GC_11, amp[1200]); 
  FFV1_0(w[252], w[27], w[23], pars->GC_11, amp[1201]); 
  FFV1_0(w[256], w[6], w[23], pars->GC_11, amp[1202]); 
  FFV1_0(w[30], w[251], w[23], pars->GC_11, amp[1203]); 
  FFV1_0(w[252], w[34], w[5], pars->GC_11, amp[1204]); 
  FFV1_0(w[252], w[35], w[4], pars->GC_11, amp[1205]); 
  FFV1_0(w[1], w[257], w[36], pars->GC_11, amp[1206]); 
  FFV1_0(w[252], w[33], w[36], pars->GC_11, amp[1207]); 
  FFV1_0(w[30], w[257], w[5], pars->GC_11, amp[1208]); 
  FFV1_0(w[256], w[33], w[5], pars->GC_11, amp[1209]); 
  FFV1_0(w[20], w[257], w[4], pars->GC_11, amp[1210]); 
  FFV1_0(w[254], w[33], w[4], pars->GC_11, amp[1211]); 
  FFV1_0(w[40], w[251], w[5], pars->GC_11, amp[1212]); 
  FFV1_0(w[41], w[251], w[4], pars->GC_11, amp[1213]); 
  FFV1_0(w[258], w[6], w[36], pars->GC_11, amp[1214]); 
  FFV1_0(w[39], w[251], w[36], pars->GC_11, amp[1215]); 
  FFV1_0(w[258], w[27], w[5], pars->GC_11, amp[1216]); 
  FFV1_0(w[39], w[255], w[5], pars->GC_11, amp[1217]); 
  FFV1_0(w[258], w[17], w[4], pars->GC_11, amp[1218]); 
  FFV1_0(w[39], w[253], w[4], pars->GC_11, amp[1219]); 
  FFV1_0(w[1], w[251], w[44], pars->GC_11, amp[1220]); 
  FFV1_0(w[45], w[251], w[3], pars->GC_11, amp[1221]); 
  FFV1_0(w[252], w[6], w[44], pars->GC_11, amp[1222]); 
  FFV1_0(w[252], w[46], w[3], pars->GC_11, amp[1223]); 
  FFV1_0(w[252], w[47], w[5], pars->GC_11, amp[1224]); 
  FFV1_0(w[252], w[48], w[3], pars->GC_11, amp[1225]); 
  FFV1_0(w[20], w[255], w[3], pars->GC_11, amp[1226]); 
  FFV1_0(w[254], w[27], w[3], pars->GC_11, amp[1227]); 
  FFV1_0(w[49], w[251], w[5], pars->GC_11, amp[1228]); 
  FFV1_0(w[50], w[251], w[3], pars->GC_11, amp[1229]); 
  FFV1_0(w[256], w[17], w[3], pars->GC_11, amp[1230]); 
  FFV1_0(w[30], w[253], w[3], pars->GC_11, amp[1231]); 
  FFV1_0(w[252], w[51], w[4], pars->GC_11, amp[1232]); 
  FFV1_0(w[252], w[52], w[3], pars->GC_11, amp[1233]); 
  FFV1_0(w[53], w[251], w[4], pars->GC_11, amp[1234]); 
  FFV1_0(w[54], w[251], w[3], pars->GC_11, amp[1235]); 
  FFV1_0(w[1], w[251], w[55], pars->GC_11, amp[1236]); 
  FFV1_0(w[1], w[251], w[56], pars->GC_11, amp[1237]); 
  FFV1_0(w[1], w[251], w[57], pars->GC_11, amp[1238]); 
  FFV1_0(w[252], w[6], w[55], pars->GC_11, amp[1239]); 
  FFV1_0(w[252], w[6], w[56], pars->GC_11, amp[1240]); 
  FFV1_0(w[252], w[6], w[57], pars->GC_11, amp[1241]); 
  FFV1_0(w[75], w[259], w[10], pars->GC_11, amp[1242]); 
  FFV1_0(w[77], w[259], w[5], pars->GC_11, amp[1243]); 
  FFV1_0(w[260], w[74], w[10], pars->GC_11, amp[1244]); 
  FFV1_0(w[260], w[79], w[5], pars->GC_11, amp[1245]); 
  FFV1_0(w[75], w[261], w[8], pars->GC_11, amp[1246]); 
  FFV1_0(w[260], w[82], w[8], pars->GC_11, amp[1247]); 
  FFV1_0(w[262], w[74], w[8], pars->GC_11, amp[1248]); 
  FFV1_0(w[85], w[259], w[8], pars->GC_11, amp[1249]); 
  FFV1_0(w[75], w[259], w[24], pars->GC_11, amp[1250]); 
  FFV1_0(w[88], w[259], w[4], pars->GC_11, amp[1251]); 
  FFV1_0(w[260], w[74], w[24], pars->GC_11, amp[1252]); 
  FFV1_0(w[260], w[89], w[4], pars->GC_11, amp[1253]); 
  FFV1_0(w[75], w[263], w[23], pars->GC_11, amp[1254]); 
  FFV1_0(w[260], w[90], w[23], pars->GC_11, amp[1255]); 
  FFV1_0(w[264], w[74], w[23], pars->GC_11, amp[1256]); 
  FFV1_0(w[93], w[259], w[23], pars->GC_11, amp[1257]); 
  FFV1_0(w[260], w[97], w[5], pars->GC_11, amp[1258]); 
  FFV1_0(w[260], w[98], w[4], pars->GC_11, amp[1259]); 
  FFV1_0(w[75], w[265], w[36], pars->GC_11, amp[1260]); 
  FFV1_0(w[260], w[96], w[36], pars->GC_11, amp[1261]); 
  FFV1_0(w[93], w[265], w[5], pars->GC_11, amp[1262]); 
  FFV1_0(w[264], w[96], w[5], pars->GC_11, amp[1263]); 
  FFV1_0(w[85], w[265], w[4], pars->GC_11, amp[1264]); 
  FFV1_0(w[262], w[96], w[4], pars->GC_11, amp[1265]); 
  FFV1_0(w[102], w[259], w[5], pars->GC_11, amp[1266]); 
  FFV1_0(w[103], w[259], w[4], pars->GC_11, amp[1267]); 
  FFV1_0(w[266], w[74], w[36], pars->GC_11, amp[1268]); 
  FFV1_0(w[101], w[259], w[36], pars->GC_11, amp[1269]); 
  FFV1_0(w[266], w[90], w[5], pars->GC_11, amp[1270]); 
  FFV1_0(w[101], w[263], w[5], pars->GC_11, amp[1271]); 
  FFV1_0(w[266], w[82], w[4], pars->GC_11, amp[1272]); 
  FFV1_0(w[101], w[261], w[4], pars->GC_11, amp[1273]); 
  FFV1_0(w[75], w[259], w[44], pars->GC_11, amp[1274]); 
  FFV1_0(w[106], w[259], w[3], pars->GC_11, amp[1275]); 
  FFV1_0(w[260], w[74], w[44], pars->GC_11, amp[1276]); 
  FFV1_0(w[260], w[107], w[3], pars->GC_11, amp[1277]); 
  FFV1_0(w[260], w[108], w[5], pars->GC_11, amp[1278]); 
  FFV1_0(w[260], w[109], w[3], pars->GC_11, amp[1279]); 
  FFV1_0(w[85], w[263], w[3], pars->GC_11, amp[1280]); 
  FFV1_0(w[262], w[90], w[3], pars->GC_11, amp[1281]); 
  FFV1_0(w[110], w[259], w[5], pars->GC_11, amp[1282]); 
  FFV1_0(w[111], w[259], w[3], pars->GC_11, amp[1283]); 
  FFV1_0(w[264], w[82], w[3], pars->GC_11, amp[1284]); 
  FFV1_0(w[93], w[261], w[3], pars->GC_11, amp[1285]); 
  FFV1_0(w[260], w[112], w[4], pars->GC_11, amp[1286]); 
  FFV1_0(w[260], w[113], w[3], pars->GC_11, amp[1287]); 
  FFV1_0(w[114], w[259], w[4], pars->GC_11, amp[1288]); 
  FFV1_0(w[115], w[259], w[3], pars->GC_11, amp[1289]); 
  FFV1_0(w[75], w[259], w[55], pars->GC_11, amp[1290]); 
  FFV1_0(w[75], w[259], w[56], pars->GC_11, amp[1291]); 
  FFV1_0(w[75], w[259], w[57], pars->GC_11, amp[1292]); 
  FFV1_0(w[260], w[74], w[55], pars->GC_11, amp[1293]); 
  FFV1_0(w[260], w[74], w[56], pars->GC_11, amp[1294]); 
  FFV1_0(w[260], w[74], w[57], pars->GC_11, amp[1295]); 
  FFV1_0(w[1], w[268], w[10], pars->GC_11, amp[1296]); 
  FFV1_0(w[11], w[268], w[5], pars->GC_11, amp[1297]); 
  FFV1_0(w[269], w[6], w[10], pars->GC_11, amp[1298]); 
  FFV1_0(w[269], w[13], w[5], pars->GC_11, amp[1299]); 
  FFV1_0(w[1], w[270], w[8], pars->GC_11, amp[1300]); 
  FFV1_0(w[269], w[17], w[8], pars->GC_11, amp[1301]); 
  FFV1_0(w[271], w[6], w[8], pars->GC_11, amp[1302]); 
  FFV1_0(w[20], w[268], w[8], pars->GC_11, amp[1303]); 
  FFV1_0(w[1], w[268], w[24], pars->GC_11, amp[1304]); 
  FFV1_0(w[25], w[268], w[4], pars->GC_11, amp[1305]); 
  FFV1_0(w[269], w[6], w[24], pars->GC_11, amp[1306]); 
  FFV1_0(w[269], w[26], w[4], pars->GC_11, amp[1307]); 
  FFV1_0(w[1], w[272], w[23], pars->GC_11, amp[1308]); 
  FFV1_0(w[269], w[27], w[23], pars->GC_11, amp[1309]); 
  FFV1_0(w[273], w[6], w[23], pars->GC_11, amp[1310]); 
  FFV1_0(w[30], w[268], w[23], pars->GC_11, amp[1311]); 
  FFV1_0(w[269], w[34], w[5], pars->GC_11, amp[1312]); 
  FFV1_0(w[269], w[35], w[4], pars->GC_11, amp[1313]); 
  FFV1_0(w[1], w[274], w[36], pars->GC_11, amp[1314]); 
  FFV1_0(w[269], w[33], w[36], pars->GC_11, amp[1315]); 
  FFV1_0(w[30], w[274], w[5], pars->GC_11, amp[1316]); 
  FFV1_0(w[273], w[33], w[5], pars->GC_11, amp[1317]); 
  FFV1_0(w[20], w[274], w[4], pars->GC_11, amp[1318]); 
  FFV1_0(w[271], w[33], w[4], pars->GC_11, amp[1319]); 
  FFV1_0(w[40], w[268], w[5], pars->GC_11, amp[1320]); 
  FFV1_0(w[41], w[268], w[4], pars->GC_11, amp[1321]); 
  FFV1_0(w[275], w[6], w[36], pars->GC_11, amp[1322]); 
  FFV1_0(w[39], w[268], w[36], pars->GC_11, amp[1323]); 
  FFV1_0(w[275], w[27], w[5], pars->GC_11, amp[1324]); 
  FFV1_0(w[39], w[272], w[5], pars->GC_11, amp[1325]); 
  FFV1_0(w[275], w[17], w[4], pars->GC_11, amp[1326]); 
  FFV1_0(w[39], w[270], w[4], pars->GC_11, amp[1327]); 
  FFV1_0(w[1], w[268], w[44], pars->GC_11, amp[1328]); 
  FFV1_0(w[45], w[268], w[3], pars->GC_11, amp[1329]); 
  FFV1_0(w[269], w[6], w[44], pars->GC_11, amp[1330]); 
  FFV1_0(w[269], w[46], w[3], pars->GC_11, amp[1331]); 
  FFV1_0(w[269], w[47], w[5], pars->GC_11, amp[1332]); 
  FFV1_0(w[269], w[48], w[3], pars->GC_11, amp[1333]); 
  FFV1_0(w[20], w[272], w[3], pars->GC_11, amp[1334]); 
  FFV1_0(w[271], w[27], w[3], pars->GC_11, amp[1335]); 
  FFV1_0(w[49], w[268], w[5], pars->GC_11, amp[1336]); 
  FFV1_0(w[50], w[268], w[3], pars->GC_11, amp[1337]); 
  FFV1_0(w[273], w[17], w[3], pars->GC_11, amp[1338]); 
  FFV1_0(w[30], w[270], w[3], pars->GC_11, amp[1339]); 
  FFV1_0(w[269], w[51], w[4], pars->GC_11, amp[1340]); 
  FFV1_0(w[269], w[52], w[3], pars->GC_11, amp[1341]); 
  FFV1_0(w[53], w[268], w[4], pars->GC_11, amp[1342]); 
  FFV1_0(w[54], w[268], w[3], pars->GC_11, amp[1343]); 
  FFV1_0(w[1], w[268], w[55], pars->GC_11, amp[1344]); 
  FFV1_0(w[1], w[268], w[56], pars->GC_11, amp[1345]); 
  FFV1_0(w[1], w[268], w[57], pars->GC_11, amp[1346]); 
  FFV1_0(w[269], w[6], w[55], pars->GC_11, amp[1347]); 
  FFV1_0(w[269], w[6], w[56], pars->GC_11, amp[1348]); 
  FFV1_0(w[269], w[6], w[57], pars->GC_11, amp[1349]); 
  FFV1_0(w[1], w[276], w[10], pars->GC_11, amp[1350]); 
  FFV1_0(w[11], w[276], w[5], pars->GC_11, amp[1351]); 
  FFV1_0(w[277], w[6], w[10], pars->GC_11, amp[1352]); 
  FFV1_0(w[277], w[13], w[5], pars->GC_11, amp[1353]); 
  FFV1_0(w[1], w[278], w[8], pars->GC_11, amp[1354]); 
  FFV1_0(w[277], w[17], w[8], pars->GC_11, amp[1355]); 
  FFV1_0(w[279], w[6], w[8], pars->GC_11, amp[1356]); 
  FFV1_0(w[20], w[276], w[8], pars->GC_11, amp[1357]); 
  FFV1_0(w[1], w[276], w[24], pars->GC_11, amp[1358]); 
  FFV1_0(w[25], w[276], w[4], pars->GC_11, amp[1359]); 
  FFV1_0(w[277], w[6], w[24], pars->GC_11, amp[1360]); 
  FFV1_0(w[277], w[26], w[4], pars->GC_11, amp[1361]); 
  FFV1_0(w[1], w[280], w[23], pars->GC_11, amp[1362]); 
  FFV1_0(w[277], w[27], w[23], pars->GC_11, amp[1363]); 
  FFV1_0(w[281], w[6], w[23], pars->GC_11, amp[1364]); 
  FFV1_0(w[30], w[276], w[23], pars->GC_11, amp[1365]); 
  FFV1_0(w[277], w[34], w[5], pars->GC_11, amp[1366]); 
  FFV1_0(w[277], w[35], w[4], pars->GC_11, amp[1367]); 
  FFV1_0(w[1], w[282], w[36], pars->GC_11, amp[1368]); 
  FFV1_0(w[277], w[33], w[36], pars->GC_11, amp[1369]); 
  FFV1_0(w[30], w[282], w[5], pars->GC_11, amp[1370]); 
  FFV1_0(w[281], w[33], w[5], pars->GC_11, amp[1371]); 
  FFV1_0(w[20], w[282], w[4], pars->GC_11, amp[1372]); 
  FFV1_0(w[279], w[33], w[4], pars->GC_11, amp[1373]); 
  FFV1_0(w[40], w[276], w[5], pars->GC_11, amp[1374]); 
  FFV1_0(w[41], w[276], w[4], pars->GC_11, amp[1375]); 
  FFV1_0(w[283], w[6], w[36], pars->GC_11, amp[1376]); 
  FFV1_0(w[39], w[276], w[36], pars->GC_11, amp[1377]); 
  FFV1_0(w[283], w[27], w[5], pars->GC_11, amp[1378]); 
  FFV1_0(w[39], w[280], w[5], pars->GC_11, amp[1379]); 
  FFV1_0(w[283], w[17], w[4], pars->GC_11, amp[1380]); 
  FFV1_0(w[39], w[278], w[4], pars->GC_11, amp[1381]); 
  FFV1_0(w[1], w[276], w[44], pars->GC_11, amp[1382]); 
  FFV1_0(w[45], w[276], w[3], pars->GC_11, amp[1383]); 
  FFV1_0(w[277], w[6], w[44], pars->GC_11, amp[1384]); 
  FFV1_0(w[277], w[46], w[3], pars->GC_11, amp[1385]); 
  FFV1_0(w[277], w[47], w[5], pars->GC_11, amp[1386]); 
  FFV1_0(w[277], w[48], w[3], pars->GC_11, amp[1387]); 
  FFV1_0(w[20], w[280], w[3], pars->GC_11, amp[1388]); 
  FFV1_0(w[279], w[27], w[3], pars->GC_11, amp[1389]); 
  FFV1_0(w[49], w[276], w[5], pars->GC_11, amp[1390]); 
  FFV1_0(w[50], w[276], w[3], pars->GC_11, amp[1391]); 
  FFV1_0(w[281], w[17], w[3], pars->GC_11, amp[1392]); 
  FFV1_0(w[30], w[278], w[3], pars->GC_11, amp[1393]); 
  FFV1_0(w[277], w[51], w[4], pars->GC_11, amp[1394]); 
  FFV1_0(w[277], w[52], w[3], pars->GC_11, amp[1395]); 
  FFV1_0(w[53], w[276], w[4], pars->GC_11, amp[1396]); 
  FFV1_0(w[54], w[276], w[3], pars->GC_11, amp[1397]); 
  FFV1_0(w[1], w[276], w[55], pars->GC_11, amp[1398]); 
  FFV1_0(w[1], w[276], w[56], pars->GC_11, amp[1399]); 
  FFV1_0(w[1], w[276], w[57], pars->GC_11, amp[1400]); 
  FFV1_0(w[277], w[6], w[55], pars->GC_11, amp[1401]); 
  FFV1_0(w[277], w[6], w[56], pars->GC_11, amp[1402]); 
  FFV1_0(w[277], w[6], w[57], pars->GC_11, amp[1403]); 
  FFV1_0(w[75], w[284], w[10], pars->GC_11, amp[1404]); 
  FFV1_0(w[77], w[284], w[5], pars->GC_11, amp[1405]); 
  FFV1_0(w[285], w[74], w[10], pars->GC_11, amp[1406]); 
  FFV1_0(w[285], w[79], w[5], pars->GC_11, amp[1407]); 
  FFV1_0(w[75], w[286], w[8], pars->GC_11, amp[1408]); 
  FFV1_0(w[285], w[82], w[8], pars->GC_11, amp[1409]); 
  FFV1_0(w[287], w[74], w[8], pars->GC_11, amp[1410]); 
  FFV1_0(w[85], w[284], w[8], pars->GC_11, amp[1411]); 
  FFV1_0(w[75], w[284], w[24], pars->GC_11, amp[1412]); 
  FFV1_0(w[88], w[284], w[4], pars->GC_11, amp[1413]); 
  FFV1_0(w[285], w[74], w[24], pars->GC_11, amp[1414]); 
  FFV1_0(w[285], w[89], w[4], pars->GC_11, amp[1415]); 
  FFV1_0(w[75], w[288], w[23], pars->GC_11, amp[1416]); 
  FFV1_0(w[285], w[90], w[23], pars->GC_11, amp[1417]); 
  FFV1_0(w[289], w[74], w[23], pars->GC_11, amp[1418]); 
  FFV1_0(w[93], w[284], w[23], pars->GC_11, amp[1419]); 
  FFV1_0(w[285], w[97], w[5], pars->GC_11, amp[1420]); 
  FFV1_0(w[285], w[98], w[4], pars->GC_11, amp[1421]); 
  FFV1_0(w[75], w[290], w[36], pars->GC_11, amp[1422]); 
  FFV1_0(w[285], w[96], w[36], pars->GC_11, amp[1423]); 
  FFV1_0(w[93], w[290], w[5], pars->GC_11, amp[1424]); 
  FFV1_0(w[289], w[96], w[5], pars->GC_11, amp[1425]); 
  FFV1_0(w[85], w[290], w[4], pars->GC_11, amp[1426]); 
  FFV1_0(w[287], w[96], w[4], pars->GC_11, amp[1427]); 
  FFV1_0(w[102], w[284], w[5], pars->GC_11, amp[1428]); 
  FFV1_0(w[103], w[284], w[4], pars->GC_11, amp[1429]); 
  FFV1_0(w[291], w[74], w[36], pars->GC_11, amp[1430]); 
  FFV1_0(w[101], w[284], w[36], pars->GC_11, amp[1431]); 
  FFV1_0(w[291], w[90], w[5], pars->GC_11, amp[1432]); 
  FFV1_0(w[101], w[288], w[5], pars->GC_11, amp[1433]); 
  FFV1_0(w[291], w[82], w[4], pars->GC_11, amp[1434]); 
  FFV1_0(w[101], w[286], w[4], pars->GC_11, amp[1435]); 
  FFV1_0(w[75], w[284], w[44], pars->GC_11, amp[1436]); 
  FFV1_0(w[106], w[284], w[3], pars->GC_11, amp[1437]); 
  FFV1_0(w[285], w[74], w[44], pars->GC_11, amp[1438]); 
  FFV1_0(w[285], w[107], w[3], pars->GC_11, amp[1439]); 
  FFV1_0(w[285], w[108], w[5], pars->GC_11, amp[1440]); 
  FFV1_0(w[285], w[109], w[3], pars->GC_11, amp[1441]); 
  FFV1_0(w[85], w[288], w[3], pars->GC_11, amp[1442]); 
  FFV1_0(w[287], w[90], w[3], pars->GC_11, amp[1443]); 
  FFV1_0(w[110], w[284], w[5], pars->GC_11, amp[1444]); 
  FFV1_0(w[111], w[284], w[3], pars->GC_11, amp[1445]); 
  FFV1_0(w[289], w[82], w[3], pars->GC_11, amp[1446]); 
  FFV1_0(w[93], w[286], w[3], pars->GC_11, amp[1447]); 
  FFV1_0(w[285], w[112], w[4], pars->GC_11, amp[1448]); 
  FFV1_0(w[285], w[113], w[3], pars->GC_11, amp[1449]); 
  FFV1_0(w[114], w[284], w[4], pars->GC_11, amp[1450]); 
  FFV1_0(w[115], w[284], w[3], pars->GC_11, amp[1451]); 
  FFV1_0(w[75], w[284], w[55], pars->GC_11, amp[1452]); 
  FFV1_0(w[75], w[284], w[56], pars->GC_11, amp[1453]); 
  FFV1_0(w[75], w[284], w[57], pars->GC_11, amp[1454]); 
  FFV1_0(w[285], w[74], w[55], pars->GC_11, amp[1455]); 
  FFV1_0(w[285], w[74], w[56], pars->GC_11, amp[1456]); 
  FFV1_0(w[285], w[74], w[57], pars->GC_11, amp[1457]); 
  FFV1_0(w[75], w[292], w[10], pars->GC_11, amp[1458]); 
  FFV1_0(w[77], w[292], w[5], pars->GC_11, amp[1459]); 
  FFV1_0(w[293], w[74], w[10], pars->GC_11, amp[1460]); 
  FFV1_0(w[293], w[79], w[5], pars->GC_11, amp[1461]); 
  FFV1_0(w[75], w[294], w[8], pars->GC_11, amp[1462]); 
  FFV1_0(w[293], w[82], w[8], pars->GC_11, amp[1463]); 
  FFV1_0(w[295], w[74], w[8], pars->GC_11, amp[1464]); 
  FFV1_0(w[85], w[292], w[8], pars->GC_11, amp[1465]); 
  FFV1_0(w[75], w[292], w[24], pars->GC_11, amp[1466]); 
  FFV1_0(w[88], w[292], w[4], pars->GC_11, amp[1467]); 
  FFV1_0(w[293], w[74], w[24], pars->GC_11, amp[1468]); 
  FFV1_0(w[293], w[89], w[4], pars->GC_11, amp[1469]); 
  FFV1_0(w[75], w[296], w[23], pars->GC_11, amp[1470]); 
  FFV1_0(w[293], w[90], w[23], pars->GC_11, amp[1471]); 
  FFV1_0(w[297], w[74], w[23], pars->GC_11, amp[1472]); 
  FFV1_0(w[93], w[292], w[23], pars->GC_11, amp[1473]); 
  FFV1_0(w[293], w[97], w[5], pars->GC_11, amp[1474]); 
  FFV1_0(w[293], w[98], w[4], pars->GC_11, amp[1475]); 
  FFV1_0(w[75], w[298], w[36], pars->GC_11, amp[1476]); 
  FFV1_0(w[293], w[96], w[36], pars->GC_11, amp[1477]); 
  FFV1_0(w[93], w[298], w[5], pars->GC_11, amp[1478]); 
  FFV1_0(w[297], w[96], w[5], pars->GC_11, amp[1479]); 
  FFV1_0(w[85], w[298], w[4], pars->GC_11, amp[1480]); 
  FFV1_0(w[295], w[96], w[4], pars->GC_11, amp[1481]); 
  FFV1_0(w[102], w[292], w[5], pars->GC_11, amp[1482]); 
  FFV1_0(w[103], w[292], w[4], pars->GC_11, amp[1483]); 
  FFV1_0(w[299], w[74], w[36], pars->GC_11, amp[1484]); 
  FFV1_0(w[101], w[292], w[36], pars->GC_11, amp[1485]); 
  FFV1_0(w[299], w[90], w[5], pars->GC_11, amp[1486]); 
  FFV1_0(w[101], w[296], w[5], pars->GC_11, amp[1487]); 
  FFV1_0(w[299], w[82], w[4], pars->GC_11, amp[1488]); 
  FFV1_0(w[101], w[294], w[4], pars->GC_11, amp[1489]); 
  FFV1_0(w[75], w[292], w[44], pars->GC_11, amp[1490]); 
  FFV1_0(w[106], w[292], w[3], pars->GC_11, amp[1491]); 
  FFV1_0(w[293], w[74], w[44], pars->GC_11, amp[1492]); 
  FFV1_0(w[293], w[107], w[3], pars->GC_11, amp[1493]); 
  FFV1_0(w[293], w[108], w[5], pars->GC_11, amp[1494]); 
  FFV1_0(w[293], w[109], w[3], pars->GC_11, amp[1495]); 
  FFV1_0(w[85], w[296], w[3], pars->GC_11, amp[1496]); 
  FFV1_0(w[295], w[90], w[3], pars->GC_11, amp[1497]); 
  FFV1_0(w[110], w[292], w[5], pars->GC_11, amp[1498]); 
  FFV1_0(w[111], w[292], w[3], pars->GC_11, amp[1499]); 
  FFV1_0(w[297], w[82], w[3], pars->GC_11, amp[1500]); 
  FFV1_0(w[93], w[294], w[3], pars->GC_11, amp[1501]); 
  FFV1_0(w[293], w[112], w[4], pars->GC_11, amp[1502]); 
  FFV1_0(w[293], w[113], w[3], pars->GC_11, amp[1503]); 
  FFV1_0(w[114], w[292], w[4], pars->GC_11, amp[1504]); 
  FFV1_0(w[115], w[292], w[3], pars->GC_11, amp[1505]); 
  FFV1_0(w[75], w[292], w[55], pars->GC_11, amp[1506]); 
  FFV1_0(w[75], w[292], w[56], pars->GC_11, amp[1507]); 
  FFV1_0(w[75], w[292], w[57], pars->GC_11, amp[1508]); 
  FFV1_0(w[293], w[74], w[55], pars->GC_11, amp[1509]); 
  FFV1_0(w[293], w[74], w[56], pars->GC_11, amp[1510]); 
  FFV1_0(w[293], w[74], w[57], pars->GC_11, amp[1511]); 


}
double PY8MEs_R4_P6_sm_lq_lgggq::matrix_4_emu_emgggu() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 108;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = +amp[0] + amp[2] + Complex<double> (0, 1) * amp[3] + amp[4] +
      amp[6] + Complex<double> (0, 1) * amp[7] + Complex<double> (0, 1) *
      amp[12] + Complex<double> (0, 1) * amp[13] + Complex<double> (0, 1) *
      amp[14] + Complex<double> (0, 1) * amp[15] - amp[32] - amp[34] +
      Complex<double> (0, 1) * amp[36] + Complex<double> (0, 1) * amp[37] +
      Complex<double> (0, 1) * amp[38] + Complex<double> (0, 1) * amp[39] -
      amp[44] - amp[45] - amp[46] - amp[47] + amp[64] + Complex<double> (0, 1)
      * amp[65] + amp[66] + amp[68] + Complex<double> (0, 1) * amp[69] +
      amp[70] - amp[93] - amp[95] - amp[98] + amp[96] - amp[101] + amp[99] -
      amp[104] + amp[102] - amp[107] + amp[105];
  jamp[1] = +amp[16] + amp[18] + Complex<double> (0, 1) * amp[19] + amp[20] +
      amp[22] + Complex<double> (0, 1) * amp[23] + Complex<double> (0, 1) *
      amp[28] + Complex<double> (0, 1) * amp[29] + Complex<double> (0, 1) *
      amp[30] + Complex<double> (0, 1) * amp[31] - amp[33] - amp[35] -
      Complex<double> (0, 1) * amp[36] - Complex<double> (0, 1) * amp[37] -
      Complex<double> (0, 1) * amp[38] - Complex<double> (0, 1) * amp[39] -
      amp[40] - amp[41] - amp[42] - amp[43] - amp[64] - Complex<double> (0, 1)
      * amp[65] - amp[66] - amp[68] - Complex<double> (0, 1) * amp[69] -
      amp[70] - amp[81] - amp[83] - amp[96] - amp[97] - amp[99] - amp[100] -
      amp[102] - amp[103] - amp[105] - amp[106];
  jamp[2] = -amp[0] - amp[2] - Complex<double> (0, 1) * amp[3] - amp[4] -
      amp[6] - Complex<double> (0, 1) * amp[7] - Complex<double> (0, 1) *
      amp[12] - Complex<double> (0, 1) * amp[13] - Complex<double> (0, 1) *
      amp[14] - Complex<double> (0, 1) * amp[15] - amp[16] + Complex<double>
      (0, 1) * amp[17] - amp[18] - amp[20] + Complex<double> (0, 1) * amp[21] -
      amp[22] + Complex<double> (0, 1) * amp[24] + Complex<double> (0, 1) *
      amp[25] + Complex<double> (0, 1) * amp[26] + Complex<double> (0, 1) *
      amp[27] - amp[72] - amp[74] - amp[76] - amp[77] - amp[78] - amp[79] -
      amp[92] - amp[94] + amp[98] + amp[97] + amp[101] + amp[100] + amp[104] +
      amp[103] + amp[107] + amp[106];
  jamp[3] = +amp[16] - Complex<double> (0, 1) * amp[17] + amp[18] + amp[20] -
      Complex<double> (0, 1) * amp[21] + amp[22] - Complex<double> (0, 1) *
      amp[24] - Complex<double> (0, 1) * amp[25] - Complex<double> (0, 1) *
      amp[26] - Complex<double> (0, 1) * amp[27] - amp[49] - amp[51] +
      Complex<double> (0, 1) * amp[52] + Complex<double> (0, 1) * amp[53] +
      Complex<double> (0, 1) * amp[54] + Complex<double> (0, 1) * amp[55] -
      amp[56] - amp[57] - amp[58] - amp[59] - amp[64] - amp[66] +
      Complex<double> (0, 1) * amp[67] - amp[68] - amp[70] + Complex<double>
      (0, 1) * amp[71] - amp[73] - amp[75] - amp[96] - amp[97] - amp[99] -
      amp[100] - amp[102] - amp[103] - amp[105] - amp[106];
  jamp[4] = -amp[0] + Complex<double> (0, 1) * amp[1] - amp[2] - amp[4] +
      Complex<double> (0, 1) * amp[5] - amp[6] + Complex<double> (0, 1) *
      amp[8] + Complex<double> (0, 1) * amp[9] + Complex<double> (0, 1) *
      amp[10] + Complex<double> (0, 1) * amp[11] - amp[16] - amp[18] -
      Complex<double> (0, 1) * amp[19] - amp[20] - amp[22] - Complex<double>
      (0, 1) * amp[23] - Complex<double> (0, 1) * amp[28] - Complex<double> (0,
      1) * amp[29] - Complex<double> (0, 1) * amp[30] - Complex<double> (0, 1)
      * amp[31] - amp[80] - amp[82] - amp[84] - amp[85] - amp[86] - amp[87] -
      amp[88] - amp[90] + amp[98] + amp[97] + amp[101] + amp[100] + amp[104] +
      amp[103] + amp[107] + amp[106];
  jamp[5] = +amp[0] - Complex<double> (0, 1) * amp[1] + amp[2] + amp[4] -
      Complex<double> (0, 1) * amp[5] + amp[6] - Complex<double> (0, 1) *
      amp[8] - Complex<double> (0, 1) * amp[9] - Complex<double> (0, 1) *
      amp[10] - Complex<double> (0, 1) * amp[11] - amp[48] - amp[50] -
      Complex<double> (0, 1) * amp[52] - Complex<double> (0, 1) * amp[53] -
      Complex<double> (0, 1) * amp[54] - Complex<double> (0, 1) * amp[55] -
      amp[60] - amp[61] - amp[62] - amp[63] + amp[64] + amp[66] -
      Complex<double> (0, 1) * amp[67] + amp[68] + amp[70] - Complex<double>
      (0, 1) * amp[71] - amp[89] - amp[91] - amp[98] + amp[96] - amp[101] +
      amp[99] - amp[104] + amp[102] - amp[107] + amp[105];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[0][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P6_sm_lq_lgggq::matrix_4_emd_emgggd() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 108;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = +amp[108] + amp[110] + Complex<double> (0, 1) * amp[111] + amp[112]
      + amp[114] + Complex<double> (0, 1) * amp[115] + Complex<double> (0, 1) *
      amp[120] + Complex<double> (0, 1) * amp[121] + Complex<double> (0, 1) *
      amp[122] + Complex<double> (0, 1) * amp[123] - amp[140] - amp[142] +
      Complex<double> (0, 1) * amp[144] + Complex<double> (0, 1) * amp[145] +
      Complex<double> (0, 1) * amp[146] + Complex<double> (0, 1) * amp[147] -
      amp[152] - amp[153] - amp[154] - amp[155] + amp[172] + Complex<double>
      (0, 1) * amp[173] + amp[174] + amp[176] + Complex<double> (0, 1) *
      amp[177] + amp[178] - amp[201] - amp[203] - amp[206] + amp[204] -
      amp[209] + amp[207] - amp[212] + amp[210] - amp[215] + amp[213];
  jamp[1] = +amp[124] + amp[126] + Complex<double> (0, 1) * amp[127] + amp[128]
      + amp[130] + Complex<double> (0, 1) * amp[131] + Complex<double> (0, 1) *
      amp[136] + Complex<double> (0, 1) * amp[137] + Complex<double> (0, 1) *
      amp[138] + Complex<double> (0, 1) * amp[139] - amp[141] - amp[143] -
      Complex<double> (0, 1) * amp[144] - Complex<double> (0, 1) * amp[145] -
      Complex<double> (0, 1) * amp[146] - Complex<double> (0, 1) * amp[147] -
      amp[148] - amp[149] - amp[150] - amp[151] - amp[172] - Complex<double>
      (0, 1) * amp[173] - amp[174] - amp[176] - Complex<double> (0, 1) *
      amp[177] - amp[178] - amp[189] - amp[191] - amp[204] - amp[205] -
      amp[207] - amp[208] - amp[210] - amp[211] - amp[213] - amp[214];
  jamp[2] = -amp[108] - amp[110] - Complex<double> (0, 1) * amp[111] - amp[112]
      - amp[114] - Complex<double> (0, 1) * amp[115] - Complex<double> (0, 1) *
      amp[120] - Complex<double> (0, 1) * amp[121] - Complex<double> (0, 1) *
      amp[122] - Complex<double> (0, 1) * amp[123] - amp[124] + Complex<double>
      (0, 1) * amp[125] - amp[126] - amp[128] + Complex<double> (0, 1) *
      amp[129] - amp[130] + Complex<double> (0, 1) * amp[132] + Complex<double>
      (0, 1) * amp[133] + Complex<double> (0, 1) * amp[134] + Complex<double>
      (0, 1) * amp[135] - amp[180] - amp[182] - amp[184] - amp[185] - amp[186]
      - amp[187] - amp[200] - amp[202] + amp[206] + amp[205] + amp[209] +
      amp[208] + amp[212] + amp[211] + amp[215] + amp[214];
  jamp[3] = +amp[124] - Complex<double> (0, 1) * amp[125] + amp[126] + amp[128]
      - Complex<double> (0, 1) * amp[129] + amp[130] - Complex<double> (0, 1) *
      amp[132] - Complex<double> (0, 1) * amp[133] - Complex<double> (0, 1) *
      amp[134] - Complex<double> (0, 1) * amp[135] - amp[157] - amp[159] +
      Complex<double> (0, 1) * amp[160] + Complex<double> (0, 1) * amp[161] +
      Complex<double> (0, 1) * amp[162] + Complex<double> (0, 1) * amp[163] -
      amp[164] - amp[165] - amp[166] - amp[167] - amp[172] - amp[174] +
      Complex<double> (0, 1) * amp[175] - amp[176] - amp[178] + Complex<double>
      (0, 1) * amp[179] - amp[181] - amp[183] - amp[204] - amp[205] - amp[207]
      - amp[208] - amp[210] - amp[211] - amp[213] - amp[214];
  jamp[4] = -amp[108] + Complex<double> (0, 1) * amp[109] - amp[110] - amp[112]
      + Complex<double> (0, 1) * amp[113] - amp[114] + Complex<double> (0, 1) *
      amp[116] + Complex<double> (0, 1) * amp[117] + Complex<double> (0, 1) *
      amp[118] + Complex<double> (0, 1) * amp[119] - amp[124] - amp[126] -
      Complex<double> (0, 1) * amp[127] - amp[128] - amp[130] - Complex<double>
      (0, 1) * amp[131] - Complex<double> (0, 1) * amp[136] - Complex<double>
      (0, 1) * amp[137] - Complex<double> (0, 1) * amp[138] - Complex<double>
      (0, 1) * amp[139] - amp[188] - amp[190] - amp[192] - amp[193] - amp[194]
      - amp[195] - amp[196] - amp[198] + amp[206] + amp[205] + amp[209] +
      amp[208] + amp[212] + amp[211] + amp[215] + amp[214];
  jamp[5] = +amp[108] - Complex<double> (0, 1) * amp[109] + amp[110] + amp[112]
      - Complex<double> (0, 1) * amp[113] + amp[114] - Complex<double> (0, 1) *
      amp[116] - Complex<double> (0, 1) * amp[117] - Complex<double> (0, 1) *
      amp[118] - Complex<double> (0, 1) * amp[119] - amp[156] - amp[158] -
      Complex<double> (0, 1) * amp[160] - Complex<double> (0, 1) * amp[161] -
      Complex<double> (0, 1) * amp[162] - Complex<double> (0, 1) * amp[163] -
      amp[168] - amp[169] - amp[170] - amp[171] + amp[172] + amp[174] -
      Complex<double> (0, 1) * amp[175] + amp[176] + amp[178] - Complex<double>
      (0, 1) * amp[179] - amp[197] - amp[199] - amp[206] + amp[204] - amp[209]
      + amp[207] - amp[212] + amp[210] - amp[215] + amp[213];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[1][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P6_sm_lq_lgggq::matrix_4_emux_emgggux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 108;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = -amp[216] - amp[218] - Complex<double> (0, 1) * amp[219] - amp[220]
      - amp[222] - Complex<double> (0, 1) * amp[223] - Complex<double> (0, 1) *
      amp[228] - Complex<double> (0, 1) * amp[229] - Complex<double> (0, 1) *
      amp[230] - Complex<double> (0, 1) * amp[231] + amp[248] + amp[250] -
      Complex<double> (0, 1) * amp[252] - Complex<double> (0, 1) * amp[253] -
      Complex<double> (0, 1) * amp[254] - Complex<double> (0, 1) * amp[255] +
      amp[260] + amp[261] + amp[262] + amp[263] - amp[280] - Complex<double>
      (0, 1) * amp[281] - amp[282] - amp[284] - Complex<double> (0, 1) *
      amp[285] - amp[286] + amp[309] + amp[311] + amp[314] - amp[312] +
      amp[317] - amp[315] + amp[320] - amp[318] + amp[323] - amp[321];
  jamp[1] = -amp[232] - amp[234] - Complex<double> (0, 1) * amp[235] - amp[236]
      - amp[238] - Complex<double> (0, 1) * amp[239] - Complex<double> (0, 1) *
      amp[244] - Complex<double> (0, 1) * amp[245] - Complex<double> (0, 1) *
      amp[246] - Complex<double> (0, 1) * amp[247] + amp[249] + amp[251] +
      Complex<double> (0, 1) * amp[252] + Complex<double> (0, 1) * amp[253] +
      Complex<double> (0, 1) * amp[254] + Complex<double> (0, 1) * amp[255] +
      amp[256] + amp[257] + amp[258] + amp[259] + amp[280] + Complex<double>
      (0, 1) * amp[281] + amp[282] + amp[284] + Complex<double> (0, 1) *
      amp[285] + amp[286] + amp[297] + amp[299] + amp[312] + amp[313] +
      amp[315] + amp[316] + amp[318] + amp[319] + amp[321] + amp[322];
  jamp[2] = +amp[216] + amp[218] + Complex<double> (0, 1) * amp[219] + amp[220]
      + amp[222] + Complex<double> (0, 1) * amp[223] + Complex<double> (0, 1) *
      amp[228] + Complex<double> (0, 1) * amp[229] + Complex<double> (0, 1) *
      amp[230] + Complex<double> (0, 1) * amp[231] + amp[232] - Complex<double>
      (0, 1) * amp[233] + amp[234] + amp[236] - Complex<double> (0, 1) *
      amp[237] + amp[238] - Complex<double> (0, 1) * amp[240] - Complex<double>
      (0, 1) * amp[241] - Complex<double> (0, 1) * amp[242] - Complex<double>
      (0, 1) * amp[243] + amp[288] + amp[290] + amp[292] + amp[293] + amp[294]
      + amp[295] + amp[308] + amp[310] - amp[314] - amp[313] - amp[317] -
      amp[316] - amp[320] - amp[319] - amp[323] - amp[322];
  jamp[3] = -amp[232] + Complex<double> (0, 1) * amp[233] - amp[234] - amp[236]
      + Complex<double> (0, 1) * amp[237] - amp[238] + Complex<double> (0, 1) *
      amp[240] + Complex<double> (0, 1) * amp[241] + Complex<double> (0, 1) *
      amp[242] + Complex<double> (0, 1) * amp[243] + amp[265] + amp[267] -
      Complex<double> (0, 1) * amp[268] - Complex<double> (0, 1) * amp[269] -
      Complex<double> (0, 1) * amp[270] - Complex<double> (0, 1) * amp[271] +
      amp[272] + amp[273] + amp[274] + amp[275] + amp[280] + amp[282] -
      Complex<double> (0, 1) * amp[283] + amp[284] + amp[286] - Complex<double>
      (0, 1) * amp[287] + amp[289] + amp[291] + amp[312] + amp[313] + amp[315]
      + amp[316] + amp[318] + amp[319] + amp[321] + amp[322];
  jamp[4] = +amp[216] - Complex<double> (0, 1) * amp[217] + amp[218] + amp[220]
      - Complex<double> (0, 1) * amp[221] + amp[222] - Complex<double> (0, 1) *
      amp[224] - Complex<double> (0, 1) * amp[225] - Complex<double> (0, 1) *
      amp[226] - Complex<double> (0, 1) * amp[227] + amp[232] + amp[234] +
      Complex<double> (0, 1) * amp[235] + amp[236] + amp[238] + Complex<double>
      (0, 1) * amp[239] + Complex<double> (0, 1) * amp[244] + Complex<double>
      (0, 1) * amp[245] + Complex<double> (0, 1) * amp[246] + Complex<double>
      (0, 1) * amp[247] + amp[296] + amp[298] + amp[300] + amp[301] + amp[302]
      + amp[303] + amp[304] + amp[306] - amp[314] - amp[313] - amp[317] -
      amp[316] - amp[320] - amp[319] - amp[323] - amp[322];
  jamp[5] = -amp[216] + Complex<double> (0, 1) * amp[217] - amp[218] - amp[220]
      + Complex<double> (0, 1) * amp[221] - amp[222] + Complex<double> (0, 1) *
      amp[224] + Complex<double> (0, 1) * amp[225] + Complex<double> (0, 1) *
      amp[226] + Complex<double> (0, 1) * amp[227] + amp[264] + amp[266] +
      Complex<double> (0, 1) * amp[268] + Complex<double> (0, 1) * amp[269] +
      Complex<double> (0, 1) * amp[270] + Complex<double> (0, 1) * amp[271] +
      amp[276] + amp[277] + amp[278] + amp[279] - amp[280] - amp[282] +
      Complex<double> (0, 1) * amp[283] - amp[284] - amp[286] + Complex<double>
      (0, 1) * amp[287] + amp[305] + amp[307] + amp[314] - amp[312] + amp[317]
      - amp[315] + amp[320] - amp[318] + amp[323] - amp[321];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[2][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P6_sm_lq_lgggq::matrix_4_emdx_emgggdx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 108;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = -amp[324] - amp[326] - Complex<double> (0, 1) * amp[327] - amp[328]
      - amp[330] - Complex<double> (0, 1) * amp[331] - Complex<double> (0, 1) *
      amp[336] - Complex<double> (0, 1) * amp[337] - Complex<double> (0, 1) *
      amp[338] - Complex<double> (0, 1) * amp[339] + amp[356] + amp[358] -
      Complex<double> (0, 1) * amp[360] - Complex<double> (0, 1) * amp[361] -
      Complex<double> (0, 1) * amp[362] - Complex<double> (0, 1) * amp[363] +
      amp[368] + amp[369] + amp[370] + amp[371] - amp[388] - Complex<double>
      (0, 1) * amp[389] - amp[390] - amp[392] - Complex<double> (0, 1) *
      amp[393] - amp[394] + amp[417] + amp[419] + amp[422] - amp[420] +
      amp[425] - amp[423] + amp[428] - amp[426] + amp[431] - amp[429];
  jamp[1] = -amp[340] - amp[342] - Complex<double> (0, 1) * amp[343] - amp[344]
      - amp[346] - Complex<double> (0, 1) * amp[347] - Complex<double> (0, 1) *
      amp[352] - Complex<double> (0, 1) * amp[353] - Complex<double> (0, 1) *
      amp[354] - Complex<double> (0, 1) * amp[355] + amp[357] + amp[359] +
      Complex<double> (0, 1) * amp[360] + Complex<double> (0, 1) * amp[361] +
      Complex<double> (0, 1) * amp[362] + Complex<double> (0, 1) * amp[363] +
      amp[364] + amp[365] + amp[366] + amp[367] + amp[388] + Complex<double>
      (0, 1) * amp[389] + amp[390] + amp[392] + Complex<double> (0, 1) *
      amp[393] + amp[394] + amp[405] + amp[407] + amp[420] + amp[421] +
      amp[423] + amp[424] + amp[426] + amp[427] + amp[429] + amp[430];
  jamp[2] = +amp[324] + amp[326] + Complex<double> (0, 1) * amp[327] + amp[328]
      + amp[330] + Complex<double> (0, 1) * amp[331] + Complex<double> (0, 1) *
      amp[336] + Complex<double> (0, 1) * amp[337] + Complex<double> (0, 1) *
      amp[338] + Complex<double> (0, 1) * amp[339] + amp[340] - Complex<double>
      (0, 1) * amp[341] + amp[342] + amp[344] - Complex<double> (0, 1) *
      amp[345] + amp[346] - Complex<double> (0, 1) * amp[348] - Complex<double>
      (0, 1) * amp[349] - Complex<double> (0, 1) * amp[350] - Complex<double>
      (0, 1) * amp[351] + amp[396] + amp[398] + amp[400] + amp[401] + amp[402]
      + amp[403] + amp[416] + amp[418] - amp[422] - amp[421] - amp[425] -
      amp[424] - amp[428] - amp[427] - amp[431] - amp[430];
  jamp[3] = -amp[340] + Complex<double> (0, 1) * amp[341] - amp[342] - amp[344]
      + Complex<double> (0, 1) * amp[345] - amp[346] + Complex<double> (0, 1) *
      amp[348] + Complex<double> (0, 1) * amp[349] + Complex<double> (0, 1) *
      amp[350] + Complex<double> (0, 1) * amp[351] + amp[373] + amp[375] -
      Complex<double> (0, 1) * amp[376] - Complex<double> (0, 1) * amp[377] -
      Complex<double> (0, 1) * amp[378] - Complex<double> (0, 1) * amp[379] +
      amp[380] + amp[381] + amp[382] + amp[383] + amp[388] + amp[390] -
      Complex<double> (0, 1) * amp[391] + amp[392] + amp[394] - Complex<double>
      (0, 1) * amp[395] + amp[397] + amp[399] + amp[420] + amp[421] + amp[423]
      + amp[424] + amp[426] + amp[427] + amp[429] + amp[430];
  jamp[4] = +amp[324] - Complex<double> (0, 1) * amp[325] + amp[326] + amp[328]
      - Complex<double> (0, 1) * amp[329] + amp[330] - Complex<double> (0, 1) *
      amp[332] - Complex<double> (0, 1) * amp[333] - Complex<double> (0, 1) *
      amp[334] - Complex<double> (0, 1) * amp[335] + amp[340] + amp[342] +
      Complex<double> (0, 1) * amp[343] + amp[344] + amp[346] + Complex<double>
      (0, 1) * amp[347] + Complex<double> (0, 1) * amp[352] + Complex<double>
      (0, 1) * amp[353] + Complex<double> (0, 1) * amp[354] + Complex<double>
      (0, 1) * amp[355] + amp[404] + amp[406] + amp[408] + amp[409] + amp[410]
      + amp[411] + amp[412] + amp[414] - amp[422] - amp[421] - amp[425] -
      amp[424] - amp[428] - amp[427] - amp[431] - amp[430];
  jamp[5] = -amp[324] + Complex<double> (0, 1) * amp[325] - amp[326] - amp[328]
      + Complex<double> (0, 1) * amp[329] - amp[330] + Complex<double> (0, 1) *
      amp[332] + Complex<double> (0, 1) * amp[333] + Complex<double> (0, 1) *
      amp[334] + Complex<double> (0, 1) * amp[335] + amp[372] + amp[374] +
      Complex<double> (0, 1) * amp[376] + Complex<double> (0, 1) * amp[377] +
      Complex<double> (0, 1) * amp[378] + Complex<double> (0, 1) * amp[379] +
      amp[384] + amp[385] + amp[386] + amp[387] - amp[388] - amp[390] +
      Complex<double> (0, 1) * amp[391] - amp[392] - amp[394] + Complex<double>
      (0, 1) * amp[395] + amp[413] + amp[415] + amp[422] - amp[420] + amp[425]
      - amp[423] + amp[428] - amp[426] + amp[431] - amp[429];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[3][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P6_sm_lq_lgggq::matrix_4_epu_epgggu() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 108;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = -amp[432] - amp[434] - Complex<double> (0, 1) * amp[435] - amp[436]
      - amp[438] - Complex<double> (0, 1) * amp[439] - Complex<double> (0, 1) *
      amp[444] - Complex<double> (0, 1) * amp[445] - Complex<double> (0, 1) *
      amp[446] - Complex<double> (0, 1) * amp[447] + amp[464] + amp[466] -
      Complex<double> (0, 1) * amp[468] - Complex<double> (0, 1) * amp[469] -
      Complex<double> (0, 1) * amp[470] - Complex<double> (0, 1) * amp[471] +
      amp[476] + amp[477] + amp[478] + amp[479] - amp[496] - Complex<double>
      (0, 1) * amp[497] - amp[498] - amp[500] - Complex<double> (0, 1) *
      amp[501] - amp[502] + amp[525] + amp[527] + amp[530] - amp[528] +
      amp[533] - amp[531] + amp[536] - amp[534] + amp[539] - amp[537];
  jamp[1] = -amp[448] - amp[450] - Complex<double> (0, 1) * amp[451] - amp[452]
      - amp[454] - Complex<double> (0, 1) * amp[455] - Complex<double> (0, 1) *
      amp[460] - Complex<double> (0, 1) * amp[461] - Complex<double> (0, 1) *
      amp[462] - Complex<double> (0, 1) * amp[463] + amp[465] + amp[467] +
      Complex<double> (0, 1) * amp[468] + Complex<double> (0, 1) * amp[469] +
      Complex<double> (0, 1) * amp[470] + Complex<double> (0, 1) * amp[471] +
      amp[472] + amp[473] + amp[474] + amp[475] + amp[496] + Complex<double>
      (0, 1) * amp[497] + amp[498] + amp[500] + Complex<double> (0, 1) *
      amp[501] + amp[502] + amp[513] + amp[515] + amp[528] + amp[529] +
      amp[531] + amp[532] + amp[534] + amp[535] + amp[537] + amp[538];
  jamp[2] = +amp[432] + amp[434] + Complex<double> (0, 1) * amp[435] + amp[436]
      + amp[438] + Complex<double> (0, 1) * amp[439] + Complex<double> (0, 1) *
      amp[444] + Complex<double> (0, 1) * amp[445] + Complex<double> (0, 1) *
      amp[446] + Complex<double> (0, 1) * amp[447] + amp[448] - Complex<double>
      (0, 1) * amp[449] + amp[450] + amp[452] - Complex<double> (0, 1) *
      amp[453] + amp[454] - Complex<double> (0, 1) * amp[456] - Complex<double>
      (0, 1) * amp[457] - Complex<double> (0, 1) * amp[458] - Complex<double>
      (0, 1) * amp[459] + amp[504] + amp[506] + amp[508] + amp[509] + amp[510]
      + amp[511] + amp[524] + amp[526] - amp[530] - amp[529] - amp[533] -
      amp[532] - amp[536] - amp[535] - amp[539] - amp[538];
  jamp[3] = -amp[448] + Complex<double> (0, 1) * amp[449] - amp[450] - amp[452]
      + Complex<double> (0, 1) * amp[453] - amp[454] + Complex<double> (0, 1) *
      amp[456] + Complex<double> (0, 1) * amp[457] + Complex<double> (0, 1) *
      amp[458] + Complex<double> (0, 1) * amp[459] + amp[481] + amp[483] -
      Complex<double> (0, 1) * amp[484] - Complex<double> (0, 1) * amp[485] -
      Complex<double> (0, 1) * amp[486] - Complex<double> (0, 1) * amp[487] +
      amp[488] + amp[489] + amp[490] + amp[491] + amp[496] + amp[498] -
      Complex<double> (0, 1) * amp[499] + amp[500] + amp[502] - Complex<double>
      (0, 1) * amp[503] + amp[505] + amp[507] + amp[528] + amp[529] + amp[531]
      + amp[532] + amp[534] + amp[535] + amp[537] + amp[538];
  jamp[4] = +amp[432] - Complex<double> (0, 1) * amp[433] + amp[434] + amp[436]
      - Complex<double> (0, 1) * amp[437] + amp[438] - Complex<double> (0, 1) *
      amp[440] - Complex<double> (0, 1) * amp[441] - Complex<double> (0, 1) *
      amp[442] - Complex<double> (0, 1) * amp[443] + amp[448] + amp[450] +
      Complex<double> (0, 1) * amp[451] + amp[452] + amp[454] + Complex<double>
      (0, 1) * amp[455] + Complex<double> (0, 1) * amp[460] + Complex<double>
      (0, 1) * amp[461] + Complex<double> (0, 1) * amp[462] + Complex<double>
      (0, 1) * amp[463] + amp[512] + amp[514] + amp[516] + amp[517] + amp[518]
      + amp[519] + amp[520] + amp[522] - amp[530] - amp[529] - amp[533] -
      amp[532] - amp[536] - amp[535] - amp[539] - amp[538];
  jamp[5] = -amp[432] + Complex<double> (0, 1) * amp[433] - amp[434] - amp[436]
      + Complex<double> (0, 1) * amp[437] - amp[438] + Complex<double> (0, 1) *
      amp[440] + Complex<double> (0, 1) * amp[441] + Complex<double> (0, 1) *
      amp[442] + Complex<double> (0, 1) * amp[443] + amp[480] + amp[482] +
      Complex<double> (0, 1) * amp[484] + Complex<double> (0, 1) * amp[485] +
      Complex<double> (0, 1) * amp[486] + Complex<double> (0, 1) * amp[487] +
      amp[492] + amp[493] + amp[494] + amp[495] - amp[496] - amp[498] +
      Complex<double> (0, 1) * amp[499] - amp[500] - amp[502] + Complex<double>
      (0, 1) * amp[503] + amp[521] + amp[523] + amp[530] - amp[528] + amp[533]
      - amp[531] + amp[536] - amp[534] + amp[539] - amp[537];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[4][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P6_sm_lq_lgggq::matrix_4_epd_epgggd() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 108;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = -amp[540] - amp[542] - Complex<double> (0, 1) * amp[543] - amp[544]
      - amp[546] - Complex<double> (0, 1) * amp[547] - Complex<double> (0, 1) *
      amp[552] - Complex<double> (0, 1) * amp[553] - Complex<double> (0, 1) *
      amp[554] - Complex<double> (0, 1) * amp[555] + amp[572] + amp[574] -
      Complex<double> (0, 1) * amp[576] - Complex<double> (0, 1) * amp[577] -
      Complex<double> (0, 1) * amp[578] - Complex<double> (0, 1) * amp[579] +
      amp[584] + amp[585] + amp[586] + amp[587] - amp[604] - Complex<double>
      (0, 1) * amp[605] - amp[606] - amp[608] - Complex<double> (0, 1) *
      amp[609] - amp[610] + amp[633] + amp[635] + amp[638] - amp[636] +
      amp[641] - amp[639] + amp[644] - amp[642] + amp[647] - amp[645];
  jamp[1] = -amp[556] - amp[558] - Complex<double> (0, 1) * amp[559] - amp[560]
      - amp[562] - Complex<double> (0, 1) * amp[563] - Complex<double> (0, 1) *
      amp[568] - Complex<double> (0, 1) * amp[569] - Complex<double> (0, 1) *
      amp[570] - Complex<double> (0, 1) * amp[571] + amp[573] + amp[575] +
      Complex<double> (0, 1) * amp[576] + Complex<double> (0, 1) * amp[577] +
      Complex<double> (0, 1) * amp[578] + Complex<double> (0, 1) * amp[579] +
      amp[580] + amp[581] + amp[582] + amp[583] + amp[604] + Complex<double>
      (0, 1) * amp[605] + amp[606] + amp[608] + Complex<double> (0, 1) *
      amp[609] + amp[610] + amp[621] + amp[623] + amp[636] + amp[637] +
      amp[639] + amp[640] + amp[642] + amp[643] + amp[645] + amp[646];
  jamp[2] = +amp[540] + amp[542] + Complex<double> (0, 1) * amp[543] + amp[544]
      + amp[546] + Complex<double> (0, 1) * amp[547] + Complex<double> (0, 1) *
      amp[552] + Complex<double> (0, 1) * amp[553] + Complex<double> (0, 1) *
      amp[554] + Complex<double> (0, 1) * amp[555] + amp[556] - Complex<double>
      (0, 1) * amp[557] + amp[558] + amp[560] - Complex<double> (0, 1) *
      amp[561] + amp[562] - Complex<double> (0, 1) * amp[564] - Complex<double>
      (0, 1) * amp[565] - Complex<double> (0, 1) * amp[566] - Complex<double>
      (0, 1) * amp[567] + amp[612] + amp[614] + amp[616] + amp[617] + amp[618]
      + amp[619] + amp[632] + amp[634] - amp[638] - amp[637] - amp[641] -
      amp[640] - amp[644] - amp[643] - amp[647] - amp[646];
  jamp[3] = -amp[556] + Complex<double> (0, 1) * amp[557] - amp[558] - amp[560]
      + Complex<double> (0, 1) * amp[561] - amp[562] + Complex<double> (0, 1) *
      amp[564] + Complex<double> (0, 1) * amp[565] + Complex<double> (0, 1) *
      amp[566] + Complex<double> (0, 1) * amp[567] + amp[589] + amp[591] -
      Complex<double> (0, 1) * amp[592] - Complex<double> (0, 1) * amp[593] -
      Complex<double> (0, 1) * amp[594] - Complex<double> (0, 1) * amp[595] +
      amp[596] + amp[597] + amp[598] + amp[599] + amp[604] + amp[606] -
      Complex<double> (0, 1) * amp[607] + amp[608] + amp[610] - Complex<double>
      (0, 1) * amp[611] + amp[613] + amp[615] + amp[636] + amp[637] + amp[639]
      + amp[640] + amp[642] + amp[643] + amp[645] + amp[646];
  jamp[4] = +amp[540] - Complex<double> (0, 1) * amp[541] + amp[542] + amp[544]
      - Complex<double> (0, 1) * amp[545] + amp[546] - Complex<double> (0, 1) *
      amp[548] - Complex<double> (0, 1) * amp[549] - Complex<double> (0, 1) *
      amp[550] - Complex<double> (0, 1) * amp[551] + amp[556] + amp[558] +
      Complex<double> (0, 1) * amp[559] + amp[560] + amp[562] + Complex<double>
      (0, 1) * amp[563] + Complex<double> (0, 1) * amp[568] + Complex<double>
      (0, 1) * amp[569] + Complex<double> (0, 1) * amp[570] + Complex<double>
      (0, 1) * amp[571] + amp[620] + amp[622] + amp[624] + amp[625] + amp[626]
      + amp[627] + amp[628] + amp[630] - amp[638] - amp[637] - amp[641] -
      amp[640] - amp[644] - amp[643] - amp[647] - amp[646];
  jamp[5] = -amp[540] + Complex<double> (0, 1) * amp[541] - amp[542] - amp[544]
      + Complex<double> (0, 1) * amp[545] - amp[546] + Complex<double> (0, 1) *
      amp[548] + Complex<double> (0, 1) * amp[549] + Complex<double> (0, 1) *
      amp[550] + Complex<double> (0, 1) * amp[551] + amp[588] + amp[590] +
      Complex<double> (0, 1) * amp[592] + Complex<double> (0, 1) * amp[593] +
      Complex<double> (0, 1) * amp[594] + Complex<double> (0, 1) * amp[595] +
      amp[600] + amp[601] + amp[602] + amp[603] - amp[604] - amp[606] +
      Complex<double> (0, 1) * amp[607] - amp[608] - amp[610] + Complex<double>
      (0, 1) * amp[611] + amp[629] + amp[631] + amp[638] - amp[636] + amp[641]
      - amp[639] + amp[644] - amp[642] + amp[647] - amp[645];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[5][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P6_sm_lq_lgggq::matrix_4_epux_epgggux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 108;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = +amp[648] + amp[650] + Complex<double> (0, 1) * amp[651] + amp[652]
      + amp[654] + Complex<double> (0, 1) * amp[655] + Complex<double> (0, 1) *
      amp[660] + Complex<double> (0, 1) * amp[661] + Complex<double> (0, 1) *
      amp[662] + Complex<double> (0, 1) * amp[663] - amp[680] - amp[682] +
      Complex<double> (0, 1) * amp[684] + Complex<double> (0, 1) * amp[685] +
      Complex<double> (0, 1) * amp[686] + Complex<double> (0, 1) * amp[687] -
      amp[692] - amp[693] - amp[694] - amp[695] + amp[712] + Complex<double>
      (0, 1) * amp[713] + amp[714] + amp[716] + Complex<double> (0, 1) *
      amp[717] + amp[718] - amp[741] - amp[743] - amp[746] + amp[744] -
      amp[749] + amp[747] - amp[752] + amp[750] - amp[755] + amp[753];
  jamp[1] = +amp[664] + amp[666] + Complex<double> (0, 1) * amp[667] + amp[668]
      + amp[670] + Complex<double> (0, 1) * amp[671] + Complex<double> (0, 1) *
      amp[676] + Complex<double> (0, 1) * amp[677] + Complex<double> (0, 1) *
      amp[678] + Complex<double> (0, 1) * amp[679] - amp[681] - amp[683] -
      Complex<double> (0, 1) * amp[684] - Complex<double> (0, 1) * amp[685] -
      Complex<double> (0, 1) * amp[686] - Complex<double> (0, 1) * amp[687] -
      amp[688] - amp[689] - amp[690] - amp[691] - amp[712] - Complex<double>
      (0, 1) * amp[713] - amp[714] - amp[716] - Complex<double> (0, 1) *
      amp[717] - amp[718] - amp[729] - amp[731] - amp[744] - amp[745] -
      amp[747] - amp[748] - amp[750] - amp[751] - amp[753] - amp[754];
  jamp[2] = -amp[648] - amp[650] - Complex<double> (0, 1) * amp[651] - amp[652]
      - amp[654] - Complex<double> (0, 1) * amp[655] - Complex<double> (0, 1) *
      amp[660] - Complex<double> (0, 1) * amp[661] - Complex<double> (0, 1) *
      amp[662] - Complex<double> (0, 1) * amp[663] - amp[664] + Complex<double>
      (0, 1) * amp[665] - amp[666] - amp[668] + Complex<double> (0, 1) *
      amp[669] - amp[670] + Complex<double> (0, 1) * amp[672] + Complex<double>
      (0, 1) * amp[673] + Complex<double> (0, 1) * amp[674] + Complex<double>
      (0, 1) * amp[675] - amp[720] - amp[722] - amp[724] - amp[725] - amp[726]
      - amp[727] - amp[740] - amp[742] + amp[746] + amp[745] + amp[749] +
      amp[748] + amp[752] + amp[751] + amp[755] + amp[754];
  jamp[3] = +amp[664] - Complex<double> (0, 1) * amp[665] + amp[666] + amp[668]
      - Complex<double> (0, 1) * amp[669] + amp[670] - Complex<double> (0, 1) *
      amp[672] - Complex<double> (0, 1) * amp[673] - Complex<double> (0, 1) *
      amp[674] - Complex<double> (0, 1) * amp[675] - amp[697] - amp[699] +
      Complex<double> (0, 1) * amp[700] + Complex<double> (0, 1) * amp[701] +
      Complex<double> (0, 1) * amp[702] + Complex<double> (0, 1) * amp[703] -
      amp[704] - amp[705] - amp[706] - amp[707] - amp[712] - amp[714] +
      Complex<double> (0, 1) * amp[715] - amp[716] - amp[718] + Complex<double>
      (0, 1) * amp[719] - amp[721] - amp[723] - amp[744] - amp[745] - amp[747]
      - amp[748] - amp[750] - amp[751] - amp[753] - amp[754];
  jamp[4] = -amp[648] + Complex<double> (0, 1) * amp[649] - amp[650] - amp[652]
      + Complex<double> (0, 1) * amp[653] - amp[654] + Complex<double> (0, 1) *
      amp[656] + Complex<double> (0, 1) * amp[657] + Complex<double> (0, 1) *
      amp[658] + Complex<double> (0, 1) * amp[659] - amp[664] - amp[666] -
      Complex<double> (0, 1) * amp[667] - amp[668] - amp[670] - Complex<double>
      (0, 1) * amp[671] - Complex<double> (0, 1) * amp[676] - Complex<double>
      (0, 1) * amp[677] - Complex<double> (0, 1) * amp[678] - Complex<double>
      (0, 1) * amp[679] - amp[728] - amp[730] - amp[732] - amp[733] - amp[734]
      - amp[735] - amp[736] - amp[738] + amp[746] + amp[745] + amp[749] +
      amp[748] + amp[752] + amp[751] + amp[755] + amp[754];
  jamp[5] = +amp[648] - Complex<double> (0, 1) * amp[649] + amp[650] + amp[652]
      - Complex<double> (0, 1) * amp[653] + amp[654] - Complex<double> (0, 1) *
      amp[656] - Complex<double> (0, 1) * amp[657] - Complex<double> (0, 1) *
      amp[658] - Complex<double> (0, 1) * amp[659] - amp[696] - amp[698] -
      Complex<double> (0, 1) * amp[700] - Complex<double> (0, 1) * amp[701] -
      Complex<double> (0, 1) * amp[702] - Complex<double> (0, 1) * amp[703] -
      amp[708] - amp[709] - amp[710] - amp[711] + amp[712] + amp[714] -
      Complex<double> (0, 1) * amp[715] + amp[716] + amp[718] - Complex<double>
      (0, 1) * amp[719] - amp[737] - amp[739] - amp[746] + amp[744] - amp[749]
      + amp[747] - amp[752] + amp[750] - amp[755] + amp[753];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[6][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P6_sm_lq_lgggq::matrix_4_epdx_epgggdx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 108;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = +amp[756] + amp[758] + Complex<double> (0, 1) * amp[759] + amp[760]
      + amp[762] + Complex<double> (0, 1) * amp[763] + Complex<double> (0, 1) *
      amp[768] + Complex<double> (0, 1) * amp[769] + Complex<double> (0, 1) *
      amp[770] + Complex<double> (0, 1) * amp[771] - amp[788] - amp[790] +
      Complex<double> (0, 1) * amp[792] + Complex<double> (0, 1) * amp[793] +
      Complex<double> (0, 1) * amp[794] + Complex<double> (0, 1) * amp[795] -
      amp[800] - amp[801] - amp[802] - amp[803] + amp[820] + Complex<double>
      (0, 1) * amp[821] + amp[822] + amp[824] + Complex<double> (0, 1) *
      amp[825] + amp[826] - amp[849] - amp[851] - amp[854] + amp[852] -
      amp[857] + amp[855] - amp[860] + amp[858] - amp[863] + amp[861];
  jamp[1] = +amp[772] + amp[774] + Complex<double> (0, 1) * amp[775] + amp[776]
      + amp[778] + Complex<double> (0, 1) * amp[779] + Complex<double> (0, 1) *
      amp[784] + Complex<double> (0, 1) * amp[785] + Complex<double> (0, 1) *
      amp[786] + Complex<double> (0, 1) * amp[787] - amp[789] - amp[791] -
      Complex<double> (0, 1) * amp[792] - Complex<double> (0, 1) * amp[793] -
      Complex<double> (0, 1) * amp[794] - Complex<double> (0, 1) * amp[795] -
      amp[796] - amp[797] - amp[798] - amp[799] - amp[820] - Complex<double>
      (0, 1) * amp[821] - amp[822] - amp[824] - Complex<double> (0, 1) *
      amp[825] - amp[826] - amp[837] - amp[839] - amp[852] - amp[853] -
      amp[855] - amp[856] - amp[858] - amp[859] - amp[861] - amp[862];
  jamp[2] = -amp[756] - amp[758] - Complex<double> (0, 1) * amp[759] - amp[760]
      - amp[762] - Complex<double> (0, 1) * amp[763] - Complex<double> (0, 1) *
      amp[768] - Complex<double> (0, 1) * amp[769] - Complex<double> (0, 1) *
      amp[770] - Complex<double> (0, 1) * amp[771] - amp[772] + Complex<double>
      (0, 1) * amp[773] - amp[774] - amp[776] + Complex<double> (0, 1) *
      amp[777] - amp[778] + Complex<double> (0, 1) * amp[780] + Complex<double>
      (0, 1) * amp[781] + Complex<double> (0, 1) * amp[782] + Complex<double>
      (0, 1) * amp[783] - amp[828] - amp[830] - amp[832] - amp[833] - amp[834]
      - amp[835] - amp[848] - amp[850] + amp[854] + amp[853] + amp[857] +
      amp[856] + amp[860] + amp[859] + amp[863] + amp[862];
  jamp[3] = +amp[772] - Complex<double> (0, 1) * amp[773] + amp[774] + amp[776]
      - Complex<double> (0, 1) * amp[777] + amp[778] - Complex<double> (0, 1) *
      amp[780] - Complex<double> (0, 1) * amp[781] - Complex<double> (0, 1) *
      amp[782] - Complex<double> (0, 1) * amp[783] - amp[805] - amp[807] +
      Complex<double> (0, 1) * amp[808] + Complex<double> (0, 1) * amp[809] +
      Complex<double> (0, 1) * amp[810] + Complex<double> (0, 1) * amp[811] -
      amp[812] - amp[813] - amp[814] - amp[815] - amp[820] - amp[822] +
      Complex<double> (0, 1) * amp[823] - amp[824] - amp[826] + Complex<double>
      (0, 1) * amp[827] - amp[829] - amp[831] - amp[852] - amp[853] - amp[855]
      - amp[856] - amp[858] - amp[859] - amp[861] - amp[862];
  jamp[4] = -amp[756] + Complex<double> (0, 1) * amp[757] - amp[758] - amp[760]
      + Complex<double> (0, 1) * amp[761] - amp[762] + Complex<double> (0, 1) *
      amp[764] + Complex<double> (0, 1) * amp[765] + Complex<double> (0, 1) *
      amp[766] + Complex<double> (0, 1) * amp[767] - amp[772] - amp[774] -
      Complex<double> (0, 1) * amp[775] - amp[776] - amp[778] - Complex<double>
      (0, 1) * amp[779] - Complex<double> (0, 1) * amp[784] - Complex<double>
      (0, 1) * amp[785] - Complex<double> (0, 1) * amp[786] - Complex<double>
      (0, 1) * amp[787] - amp[836] - amp[838] - amp[840] - amp[841] - amp[842]
      - amp[843] - amp[844] - amp[846] + amp[854] + amp[853] + amp[857] +
      amp[856] + amp[860] + amp[859] + amp[863] + amp[862];
  jamp[5] = +amp[756] - Complex<double> (0, 1) * amp[757] + amp[758] + amp[760]
      - Complex<double> (0, 1) * amp[761] + amp[762] - Complex<double> (0, 1) *
      amp[764] - Complex<double> (0, 1) * amp[765] - Complex<double> (0, 1) *
      amp[766] - Complex<double> (0, 1) * amp[767] - amp[804] - amp[806] -
      Complex<double> (0, 1) * amp[808] - Complex<double> (0, 1) * amp[809] -
      Complex<double> (0, 1) * amp[810] - Complex<double> (0, 1) * amp[811] -
      amp[816] - amp[817] - amp[818] - amp[819] + amp[820] + amp[822] -
      Complex<double> (0, 1) * amp[823] + amp[824] + amp[826] - Complex<double>
      (0, 1) * amp[827] - amp[845] - amp[847] - amp[854] + amp[852] - amp[857]
      + amp[855] - amp[860] + amp[858] - amp[863] + amp[861];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[7][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P6_sm_lq_lgggq::matrix_4_emu_vegggd() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 54;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = +amp[864] + amp[866] + Complex<double> (0, 1) * amp[867] +
      Complex<double> (0, 1) * amp[870] + Complex<double> (0, 1) * amp[871] -
      amp[880] + Complex<double> (0, 1) * amp[882] + Complex<double> (0, 1) *
      amp[883] - amp[886] - amp[887] + amp[896] + Complex<double> (0, 1) *
      amp[897] + amp[898] - amp[911] - amp[914] + amp[912] - amp[917] +
      amp[915];
  jamp[1] = +amp[872] + amp[874] + Complex<double> (0, 1) * amp[875] +
      Complex<double> (0, 1) * amp[878] + Complex<double> (0, 1) * amp[879] -
      amp[881] - Complex<double> (0, 1) * amp[882] - Complex<double> (0, 1) *
      amp[883] - amp[884] - amp[885] - amp[896] - Complex<double> (0, 1) *
      amp[897] - amp[898] - amp[905] - amp[912] - amp[913] - amp[915] -
      amp[916];
  jamp[2] = -amp[864] - amp[866] - Complex<double> (0, 1) * amp[867] -
      Complex<double> (0, 1) * amp[870] - Complex<double> (0, 1) * amp[871] -
      amp[872] + Complex<double> (0, 1) * amp[873] - amp[874] + Complex<double>
      (0, 1) * amp[876] + Complex<double> (0, 1) * amp[877] - amp[900] -
      amp[902] - amp[903] - amp[910] + amp[914] + amp[913] + amp[917] +
      amp[916];
  jamp[3] = +amp[872] - Complex<double> (0, 1) * amp[873] + amp[874] -
      Complex<double> (0, 1) * amp[876] - Complex<double> (0, 1) * amp[877] -
      amp[889] + Complex<double> (0, 1) * amp[890] + Complex<double> (0, 1) *
      amp[891] - amp[892] - amp[893] - amp[896] - amp[898] + Complex<double>
      (0, 1) * amp[899] - amp[901] - amp[912] - amp[913] - amp[915] - amp[916];
  jamp[4] = -amp[864] + Complex<double> (0, 1) * amp[865] - amp[866] +
      Complex<double> (0, 1) * amp[868] + Complex<double> (0, 1) * amp[869] -
      amp[872] - amp[874] - Complex<double> (0, 1) * amp[875] - Complex<double>
      (0, 1) * amp[878] - Complex<double> (0, 1) * amp[879] - amp[904] -
      amp[906] - amp[907] - amp[908] + amp[914] + amp[913] + amp[917] +
      amp[916];
  jamp[5] = +amp[864] - Complex<double> (0, 1) * amp[865] + amp[866] -
      Complex<double> (0, 1) * amp[868] - Complex<double> (0, 1) * amp[869] -
      amp[888] - Complex<double> (0, 1) * amp[890] - Complex<double> (0, 1) *
      amp[891] - amp[894] - amp[895] + amp[896] + amp[898] - Complex<double>
      (0, 1) * amp[899] - amp[909] - amp[914] + amp[912] - amp[917] + amp[915];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[8][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P6_sm_lq_lgggq::matrix_4_emdx_vegggux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 54;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = -amp[918] - amp[920] - Complex<double> (0, 1) * amp[921] -
      Complex<double> (0, 1) * amp[924] - Complex<double> (0, 1) * amp[925] +
      amp[934] - Complex<double> (0, 1) * amp[936] - Complex<double> (0, 1) *
      amp[937] + amp[940] + amp[941] - amp[950] - Complex<double> (0, 1) *
      amp[951] - amp[952] + amp[965] + amp[968] - amp[966] + amp[971] -
      amp[969];
  jamp[1] = -amp[926] - amp[928] - Complex<double> (0, 1) * amp[929] -
      Complex<double> (0, 1) * amp[932] - Complex<double> (0, 1) * amp[933] +
      amp[935] + Complex<double> (0, 1) * amp[936] + Complex<double> (0, 1) *
      amp[937] + amp[938] + amp[939] + amp[950] + Complex<double> (0, 1) *
      amp[951] + amp[952] + amp[959] + amp[966] + amp[967] + amp[969] +
      amp[970];
  jamp[2] = +amp[918] + amp[920] + Complex<double> (0, 1) * amp[921] +
      Complex<double> (0, 1) * amp[924] + Complex<double> (0, 1) * amp[925] +
      amp[926] - Complex<double> (0, 1) * amp[927] + amp[928] - Complex<double>
      (0, 1) * amp[930] - Complex<double> (0, 1) * amp[931] + amp[954] +
      amp[956] + amp[957] + amp[964] - amp[968] - amp[967] - amp[971] -
      amp[970];
  jamp[3] = -amp[926] + Complex<double> (0, 1) * amp[927] - amp[928] +
      Complex<double> (0, 1) * amp[930] + Complex<double> (0, 1) * amp[931] +
      amp[943] - Complex<double> (0, 1) * amp[944] - Complex<double> (0, 1) *
      amp[945] + amp[946] + amp[947] + amp[950] + amp[952] - Complex<double>
      (0, 1) * amp[953] + amp[955] + amp[966] + amp[967] + amp[969] + amp[970];
  jamp[4] = +amp[918] - Complex<double> (0, 1) * amp[919] + amp[920] -
      Complex<double> (0, 1) * amp[922] - Complex<double> (0, 1) * amp[923] +
      amp[926] + amp[928] + Complex<double> (0, 1) * amp[929] + Complex<double>
      (0, 1) * amp[932] + Complex<double> (0, 1) * amp[933] + amp[958] +
      amp[960] + amp[961] + amp[962] - amp[968] - amp[967] - amp[971] -
      amp[970];
  jamp[5] = -amp[918] + Complex<double> (0, 1) * amp[919] - amp[920] +
      Complex<double> (0, 1) * amp[922] + Complex<double> (0, 1) * amp[923] +
      amp[942] + Complex<double> (0, 1) * amp[944] + Complex<double> (0, 1) *
      amp[945] + amp[948] + amp[949] - amp[950] - amp[952] + Complex<double>
      (0, 1) * amp[953] + amp[963] + amp[968] - amp[966] + amp[971] - amp[969];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[9][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P6_sm_lq_lgggq::matrix_4_veu_vegggu() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 54;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = +amp[972] + amp[974] + Complex<double> (0, 1) * amp[975] +
      Complex<double> (0, 1) * amp[978] + Complex<double> (0, 1) * amp[979] -
      amp[988] + Complex<double> (0, 1) * amp[990] + Complex<double> (0, 1) *
      amp[991] - amp[994] - amp[995] + amp[1004] + Complex<double> (0, 1) *
      amp[1005] + amp[1006] - amp[1019] - amp[1022] + amp[1020] - amp[1025] +
      amp[1023];
  jamp[1] = +amp[980] + amp[982] + Complex<double> (0, 1) * amp[983] +
      Complex<double> (0, 1) * amp[986] + Complex<double> (0, 1) * amp[987] -
      amp[989] - Complex<double> (0, 1) * amp[990] - Complex<double> (0, 1) *
      amp[991] - amp[992] - amp[993] - amp[1004] - Complex<double> (0, 1) *
      amp[1005] - amp[1006] - amp[1013] - amp[1020] - amp[1021] - amp[1023] -
      amp[1024];
  jamp[2] = -amp[972] - amp[974] - Complex<double> (0, 1) * amp[975] -
      Complex<double> (0, 1) * amp[978] - Complex<double> (0, 1) * amp[979] -
      amp[980] + Complex<double> (0, 1) * amp[981] - amp[982] + Complex<double>
      (0, 1) * amp[984] + Complex<double> (0, 1) * amp[985] - amp[1008] -
      amp[1010] - amp[1011] - amp[1018] + amp[1022] + amp[1021] + amp[1025] +
      amp[1024];
  jamp[3] = +amp[980] - Complex<double> (0, 1) * amp[981] + amp[982] -
      Complex<double> (0, 1) * amp[984] - Complex<double> (0, 1) * amp[985] -
      amp[997] + Complex<double> (0, 1) * amp[998] + Complex<double> (0, 1) *
      amp[999] - amp[1000] - amp[1001] - amp[1004] - amp[1006] +
      Complex<double> (0, 1) * amp[1007] - amp[1009] - amp[1020] - amp[1021] -
      amp[1023] - amp[1024];
  jamp[4] = -amp[972] + Complex<double> (0, 1) * amp[973] - amp[974] +
      Complex<double> (0, 1) * amp[976] + Complex<double> (0, 1) * amp[977] -
      amp[980] - amp[982] - Complex<double> (0, 1) * amp[983] - Complex<double>
      (0, 1) * amp[986] - Complex<double> (0, 1) * amp[987] - amp[1012] -
      amp[1014] - amp[1015] - amp[1016] + amp[1022] + amp[1021] + amp[1025] +
      amp[1024];
  jamp[5] = +amp[972] - Complex<double> (0, 1) * amp[973] + amp[974] -
      Complex<double> (0, 1) * amp[976] - Complex<double> (0, 1) * amp[977] -
      amp[996] - Complex<double> (0, 1) * amp[998] - Complex<double> (0, 1) *
      amp[999] - amp[1002] - amp[1003] + amp[1004] + amp[1006] -
      Complex<double> (0, 1) * amp[1007] - amp[1017] - amp[1022] + amp[1020] -
      amp[1025] + amp[1023];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[10][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P6_sm_lq_lgggq::matrix_4_ved_vegggd() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 54;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = +amp[1026] + amp[1028] + Complex<double> (0, 1) * amp[1029] +
      Complex<double> (0, 1) * amp[1032] + Complex<double> (0, 1) * amp[1033] -
      amp[1042] + Complex<double> (0, 1) * amp[1044] + Complex<double> (0, 1) *
      amp[1045] - amp[1048] - amp[1049] + amp[1058] + Complex<double> (0, 1) *
      amp[1059] + amp[1060] - amp[1073] - amp[1076] + amp[1074] - amp[1079] +
      amp[1077];
  jamp[1] = +amp[1034] + amp[1036] + Complex<double> (0, 1) * amp[1037] +
      Complex<double> (0, 1) * amp[1040] + Complex<double> (0, 1) * amp[1041] -
      amp[1043] - Complex<double> (0, 1) * amp[1044] - Complex<double> (0, 1) *
      amp[1045] - amp[1046] - amp[1047] - amp[1058] - Complex<double> (0, 1) *
      amp[1059] - amp[1060] - amp[1067] - amp[1074] - amp[1075] - amp[1077] -
      amp[1078];
  jamp[2] = -amp[1026] - amp[1028] - Complex<double> (0, 1) * amp[1029] -
      Complex<double> (0, 1) * amp[1032] - Complex<double> (0, 1) * amp[1033] -
      amp[1034] + Complex<double> (0, 1) * amp[1035] - amp[1036] +
      Complex<double> (0, 1) * amp[1038] + Complex<double> (0, 1) * amp[1039] -
      amp[1062] - amp[1064] - amp[1065] - amp[1072] + amp[1076] + amp[1075] +
      amp[1079] + amp[1078];
  jamp[3] = +amp[1034] - Complex<double> (0, 1) * amp[1035] + amp[1036] -
      Complex<double> (0, 1) * amp[1038] - Complex<double> (0, 1) * amp[1039] -
      amp[1051] + Complex<double> (0, 1) * amp[1052] + Complex<double> (0, 1) *
      amp[1053] - amp[1054] - amp[1055] - amp[1058] - amp[1060] +
      Complex<double> (0, 1) * amp[1061] - amp[1063] - amp[1074] - amp[1075] -
      amp[1077] - amp[1078];
  jamp[4] = -amp[1026] + Complex<double> (0, 1) * amp[1027] - amp[1028] +
      Complex<double> (0, 1) * amp[1030] + Complex<double> (0, 1) * amp[1031] -
      amp[1034] - amp[1036] - Complex<double> (0, 1) * amp[1037] -
      Complex<double> (0, 1) * amp[1040] - Complex<double> (0, 1) * amp[1041] -
      amp[1066] - amp[1068] - amp[1069] - amp[1070] + amp[1076] + amp[1075] +
      amp[1079] + amp[1078];
  jamp[5] = +amp[1026] - Complex<double> (0, 1) * amp[1027] + amp[1028] -
      Complex<double> (0, 1) * amp[1030] - Complex<double> (0, 1) * amp[1031] -
      amp[1050] - Complex<double> (0, 1) * amp[1052] - Complex<double> (0, 1) *
      amp[1053] - amp[1056] - amp[1057] + amp[1058] + amp[1060] -
      Complex<double> (0, 1) * amp[1061] - amp[1071] - amp[1076] + amp[1074] -
      amp[1079] + amp[1077];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[11][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P6_sm_lq_lgggq::matrix_4_veux_vegggux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 54;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = -amp[1080] - amp[1082] - Complex<double> (0, 1) * amp[1083] -
      Complex<double> (0, 1) * amp[1086] - Complex<double> (0, 1) * amp[1087] +
      amp[1096] - Complex<double> (0, 1) * amp[1098] - Complex<double> (0, 1) *
      amp[1099] + amp[1102] + amp[1103] - amp[1112] - Complex<double> (0, 1) *
      amp[1113] - amp[1114] + amp[1127] + amp[1130] - amp[1128] + amp[1133] -
      amp[1131];
  jamp[1] = -amp[1088] - amp[1090] - Complex<double> (0, 1) * amp[1091] -
      Complex<double> (0, 1) * amp[1094] - Complex<double> (0, 1) * amp[1095] +
      amp[1097] + Complex<double> (0, 1) * amp[1098] + Complex<double> (0, 1) *
      amp[1099] + amp[1100] + amp[1101] + amp[1112] + Complex<double> (0, 1) *
      amp[1113] + amp[1114] + amp[1121] + amp[1128] + amp[1129] + amp[1131] +
      amp[1132];
  jamp[2] = +amp[1080] + amp[1082] + Complex<double> (0, 1) * amp[1083] +
      Complex<double> (0, 1) * amp[1086] + Complex<double> (0, 1) * amp[1087] +
      amp[1088] - Complex<double> (0, 1) * amp[1089] + amp[1090] -
      Complex<double> (0, 1) * amp[1092] - Complex<double> (0, 1) * amp[1093] +
      amp[1116] + amp[1118] + amp[1119] + amp[1126] - amp[1130] - amp[1129] -
      amp[1133] - amp[1132];
  jamp[3] = -amp[1088] + Complex<double> (0, 1) * amp[1089] - amp[1090] +
      Complex<double> (0, 1) * amp[1092] + Complex<double> (0, 1) * amp[1093] +
      amp[1105] - Complex<double> (0, 1) * amp[1106] - Complex<double> (0, 1) *
      amp[1107] + amp[1108] + amp[1109] + amp[1112] + amp[1114] -
      Complex<double> (0, 1) * amp[1115] + amp[1117] + amp[1128] + amp[1129] +
      amp[1131] + amp[1132];
  jamp[4] = +amp[1080] - Complex<double> (0, 1) * amp[1081] + amp[1082] -
      Complex<double> (0, 1) * amp[1084] - Complex<double> (0, 1) * amp[1085] +
      amp[1088] + amp[1090] + Complex<double> (0, 1) * amp[1091] +
      Complex<double> (0, 1) * amp[1094] + Complex<double> (0, 1) * amp[1095] +
      amp[1120] + amp[1122] + amp[1123] + amp[1124] - amp[1130] - amp[1129] -
      amp[1133] - amp[1132];
  jamp[5] = -amp[1080] + Complex<double> (0, 1) * amp[1081] - amp[1082] +
      Complex<double> (0, 1) * amp[1084] + Complex<double> (0, 1) * amp[1085] +
      amp[1104] + Complex<double> (0, 1) * amp[1106] + Complex<double> (0, 1) *
      amp[1107] + amp[1110] + amp[1111] - amp[1112] - amp[1114] +
      Complex<double> (0, 1) * amp[1115] + amp[1125] + amp[1130] - amp[1128] +
      amp[1133] - amp[1131];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[12][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P6_sm_lq_lgggq::matrix_4_vedx_vegggdx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 54;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = -amp[1134] - amp[1136] - Complex<double> (0, 1) * amp[1137] -
      Complex<double> (0, 1) * amp[1140] - Complex<double> (0, 1) * amp[1141] +
      amp[1150] - Complex<double> (0, 1) * amp[1152] - Complex<double> (0, 1) *
      amp[1153] + amp[1156] + amp[1157] - amp[1166] - Complex<double> (0, 1) *
      amp[1167] - amp[1168] + amp[1181] + amp[1184] - amp[1182] + amp[1187] -
      amp[1185];
  jamp[1] = -amp[1142] - amp[1144] - Complex<double> (0, 1) * amp[1145] -
      Complex<double> (0, 1) * amp[1148] - Complex<double> (0, 1) * amp[1149] +
      amp[1151] + Complex<double> (0, 1) * amp[1152] + Complex<double> (0, 1) *
      amp[1153] + amp[1154] + amp[1155] + amp[1166] + Complex<double> (0, 1) *
      amp[1167] + amp[1168] + amp[1175] + amp[1182] + amp[1183] + amp[1185] +
      amp[1186];
  jamp[2] = +amp[1134] + amp[1136] + Complex<double> (0, 1) * amp[1137] +
      Complex<double> (0, 1) * amp[1140] + Complex<double> (0, 1) * amp[1141] +
      amp[1142] - Complex<double> (0, 1) * amp[1143] + amp[1144] -
      Complex<double> (0, 1) * amp[1146] - Complex<double> (0, 1) * amp[1147] +
      amp[1170] + amp[1172] + amp[1173] + amp[1180] - amp[1184] - amp[1183] -
      amp[1187] - amp[1186];
  jamp[3] = -amp[1142] + Complex<double> (0, 1) * amp[1143] - amp[1144] +
      Complex<double> (0, 1) * amp[1146] + Complex<double> (0, 1) * amp[1147] +
      amp[1159] - Complex<double> (0, 1) * amp[1160] - Complex<double> (0, 1) *
      amp[1161] + amp[1162] + amp[1163] + amp[1166] + amp[1168] -
      Complex<double> (0, 1) * amp[1169] + amp[1171] + amp[1182] + amp[1183] +
      amp[1185] + amp[1186];
  jamp[4] = +amp[1134] - Complex<double> (0, 1) * amp[1135] + amp[1136] -
      Complex<double> (0, 1) * amp[1138] - Complex<double> (0, 1) * amp[1139] +
      amp[1142] + amp[1144] + Complex<double> (0, 1) * amp[1145] +
      Complex<double> (0, 1) * amp[1148] + Complex<double> (0, 1) * amp[1149] +
      amp[1174] + amp[1176] + amp[1177] + amp[1178] - amp[1184] - amp[1183] -
      amp[1187] - amp[1186];
  jamp[5] = -amp[1134] + Complex<double> (0, 1) * amp[1135] - amp[1136] +
      Complex<double> (0, 1) * amp[1138] + Complex<double> (0, 1) * amp[1139] +
      amp[1158] + Complex<double> (0, 1) * amp[1160] + Complex<double> (0, 1) *
      amp[1161] + amp[1164] + amp[1165] - amp[1166] - amp[1168] +
      Complex<double> (0, 1) * amp[1169] + amp[1179] + amp[1184] - amp[1182] +
      amp[1187] - amp[1185];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[13][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P6_sm_lq_lgggq::matrix_4_epd_vexgggu() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 54;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = -amp[1188] - amp[1190] - Complex<double> (0, 1) * amp[1191] -
      Complex<double> (0, 1) * amp[1194] - Complex<double> (0, 1) * amp[1195] +
      amp[1204] - Complex<double> (0, 1) * amp[1206] - Complex<double> (0, 1) *
      amp[1207] + amp[1210] + amp[1211] - amp[1220] - Complex<double> (0, 1) *
      amp[1221] - amp[1222] + amp[1235] + amp[1238] - amp[1236] + amp[1241] -
      amp[1239];
  jamp[1] = -amp[1196] - amp[1198] - Complex<double> (0, 1) * amp[1199] -
      Complex<double> (0, 1) * amp[1202] - Complex<double> (0, 1) * amp[1203] +
      amp[1205] + Complex<double> (0, 1) * amp[1206] + Complex<double> (0, 1) *
      amp[1207] + amp[1208] + amp[1209] + amp[1220] + Complex<double> (0, 1) *
      amp[1221] + amp[1222] + amp[1229] + amp[1236] + amp[1237] + amp[1239] +
      amp[1240];
  jamp[2] = +amp[1188] + amp[1190] + Complex<double> (0, 1) * amp[1191] +
      Complex<double> (0, 1) * amp[1194] + Complex<double> (0, 1) * amp[1195] +
      amp[1196] - Complex<double> (0, 1) * amp[1197] + amp[1198] -
      Complex<double> (0, 1) * amp[1200] - Complex<double> (0, 1) * amp[1201] +
      amp[1224] + amp[1226] + amp[1227] + amp[1234] - amp[1238] - amp[1237] -
      amp[1241] - amp[1240];
  jamp[3] = -amp[1196] + Complex<double> (0, 1) * amp[1197] - amp[1198] +
      Complex<double> (0, 1) * amp[1200] + Complex<double> (0, 1) * amp[1201] +
      amp[1213] - Complex<double> (0, 1) * amp[1214] - Complex<double> (0, 1) *
      amp[1215] + amp[1216] + amp[1217] + amp[1220] + amp[1222] -
      Complex<double> (0, 1) * amp[1223] + amp[1225] + amp[1236] + amp[1237] +
      amp[1239] + amp[1240];
  jamp[4] = +amp[1188] - Complex<double> (0, 1) * amp[1189] + amp[1190] -
      Complex<double> (0, 1) * amp[1192] - Complex<double> (0, 1) * amp[1193] +
      amp[1196] + amp[1198] + Complex<double> (0, 1) * amp[1199] +
      Complex<double> (0, 1) * amp[1202] + Complex<double> (0, 1) * amp[1203] +
      amp[1228] + amp[1230] + amp[1231] + amp[1232] - amp[1238] - amp[1237] -
      amp[1241] - amp[1240];
  jamp[5] = -amp[1188] + Complex<double> (0, 1) * amp[1189] - amp[1190] +
      Complex<double> (0, 1) * amp[1192] + Complex<double> (0, 1) * amp[1193] +
      amp[1212] + Complex<double> (0, 1) * amp[1214] + Complex<double> (0, 1) *
      amp[1215] + amp[1218] + amp[1219] - amp[1220] - amp[1222] +
      Complex<double> (0, 1) * amp[1223] + amp[1233] + amp[1238] - amp[1236] +
      amp[1241] - amp[1239];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[14][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P6_sm_lq_lgggq::matrix_4_epux_vexgggdx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 54;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = +amp[1242] + amp[1244] + Complex<double> (0, 1) * amp[1245] +
      Complex<double> (0, 1) * amp[1248] + Complex<double> (0, 1) * amp[1249] -
      amp[1258] + Complex<double> (0, 1) * amp[1260] + Complex<double> (0, 1) *
      amp[1261] - amp[1264] - amp[1265] + amp[1274] + Complex<double> (0, 1) *
      amp[1275] + amp[1276] - amp[1289] - amp[1292] + amp[1290] - amp[1295] +
      amp[1293];
  jamp[1] = +amp[1250] + amp[1252] + Complex<double> (0, 1) * amp[1253] +
      Complex<double> (0, 1) * amp[1256] + Complex<double> (0, 1) * amp[1257] -
      amp[1259] - Complex<double> (0, 1) * amp[1260] - Complex<double> (0, 1) *
      amp[1261] - amp[1262] - amp[1263] - amp[1274] - Complex<double> (0, 1) *
      amp[1275] - amp[1276] - amp[1283] - amp[1290] - amp[1291] - amp[1293] -
      amp[1294];
  jamp[2] = -amp[1242] - amp[1244] - Complex<double> (0, 1) * amp[1245] -
      Complex<double> (0, 1) * amp[1248] - Complex<double> (0, 1) * amp[1249] -
      amp[1250] + Complex<double> (0, 1) * amp[1251] - amp[1252] +
      Complex<double> (0, 1) * amp[1254] + Complex<double> (0, 1) * amp[1255] -
      amp[1278] - amp[1280] - amp[1281] - amp[1288] + amp[1292] + amp[1291] +
      amp[1295] + amp[1294];
  jamp[3] = +amp[1250] - Complex<double> (0, 1) * amp[1251] + amp[1252] -
      Complex<double> (0, 1) * amp[1254] - Complex<double> (0, 1) * amp[1255] -
      amp[1267] + Complex<double> (0, 1) * amp[1268] + Complex<double> (0, 1) *
      amp[1269] - amp[1270] - amp[1271] - amp[1274] - amp[1276] +
      Complex<double> (0, 1) * amp[1277] - amp[1279] - amp[1290] - amp[1291] -
      amp[1293] - amp[1294];
  jamp[4] = -amp[1242] + Complex<double> (0, 1) * amp[1243] - amp[1244] +
      Complex<double> (0, 1) * amp[1246] + Complex<double> (0, 1) * amp[1247] -
      amp[1250] - amp[1252] - Complex<double> (0, 1) * amp[1253] -
      Complex<double> (0, 1) * amp[1256] - Complex<double> (0, 1) * amp[1257] -
      amp[1282] - amp[1284] - amp[1285] - amp[1286] + amp[1292] + amp[1291] +
      amp[1295] + amp[1294];
  jamp[5] = +amp[1242] - Complex<double> (0, 1) * amp[1243] + amp[1244] -
      Complex<double> (0, 1) * amp[1246] - Complex<double> (0, 1) * amp[1247] -
      amp[1266] - Complex<double> (0, 1) * amp[1268] - Complex<double> (0, 1) *
      amp[1269] - amp[1272] - amp[1273] + amp[1274] + amp[1276] -
      Complex<double> (0, 1) * amp[1277] - amp[1287] - amp[1292] + amp[1290] -
      amp[1295] + amp[1293];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[15][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P6_sm_lq_lgggq::matrix_4_vexu_vexgggu() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 54;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = -amp[1296] - amp[1298] - Complex<double> (0, 1) * amp[1299] -
      Complex<double> (0, 1) * amp[1302] - Complex<double> (0, 1) * amp[1303] +
      amp[1312] - Complex<double> (0, 1) * amp[1314] - Complex<double> (0, 1) *
      amp[1315] + amp[1318] + amp[1319] - amp[1328] - Complex<double> (0, 1) *
      amp[1329] - amp[1330] + amp[1343] + amp[1346] - amp[1344] + amp[1349] -
      amp[1347];
  jamp[1] = -amp[1304] - amp[1306] - Complex<double> (0, 1) * amp[1307] -
      Complex<double> (0, 1) * amp[1310] - Complex<double> (0, 1) * amp[1311] +
      amp[1313] + Complex<double> (0, 1) * amp[1314] + Complex<double> (0, 1) *
      amp[1315] + amp[1316] + amp[1317] + amp[1328] + Complex<double> (0, 1) *
      amp[1329] + amp[1330] + amp[1337] + amp[1344] + amp[1345] + amp[1347] +
      amp[1348];
  jamp[2] = +amp[1296] + amp[1298] + Complex<double> (0, 1) * amp[1299] +
      Complex<double> (0, 1) * amp[1302] + Complex<double> (0, 1) * amp[1303] +
      amp[1304] - Complex<double> (0, 1) * amp[1305] + amp[1306] -
      Complex<double> (0, 1) * amp[1308] - Complex<double> (0, 1) * amp[1309] +
      amp[1332] + amp[1334] + amp[1335] + amp[1342] - amp[1346] - amp[1345] -
      amp[1349] - amp[1348];
  jamp[3] = -amp[1304] + Complex<double> (0, 1) * amp[1305] - amp[1306] +
      Complex<double> (0, 1) * amp[1308] + Complex<double> (0, 1) * amp[1309] +
      amp[1321] - Complex<double> (0, 1) * amp[1322] - Complex<double> (0, 1) *
      amp[1323] + amp[1324] + amp[1325] + amp[1328] + amp[1330] -
      Complex<double> (0, 1) * amp[1331] + amp[1333] + amp[1344] + amp[1345] +
      amp[1347] + amp[1348];
  jamp[4] = +amp[1296] - Complex<double> (0, 1) * amp[1297] + amp[1298] -
      Complex<double> (0, 1) * amp[1300] - Complex<double> (0, 1) * amp[1301] +
      amp[1304] + amp[1306] + Complex<double> (0, 1) * amp[1307] +
      Complex<double> (0, 1) * amp[1310] + Complex<double> (0, 1) * amp[1311] +
      amp[1336] + amp[1338] + amp[1339] + amp[1340] - amp[1346] - amp[1345] -
      amp[1349] - amp[1348];
  jamp[5] = -amp[1296] + Complex<double> (0, 1) * amp[1297] - amp[1298] +
      Complex<double> (0, 1) * amp[1300] + Complex<double> (0, 1) * amp[1301] +
      amp[1320] + Complex<double> (0, 1) * amp[1322] + Complex<double> (0, 1) *
      amp[1323] + amp[1326] + amp[1327] - amp[1328] - amp[1330] +
      Complex<double> (0, 1) * amp[1331] + amp[1341] + amp[1346] - amp[1344] +
      amp[1349] - amp[1347];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[16][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P6_sm_lq_lgggq::matrix_4_vexd_vexgggd() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 54;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = -amp[1350] - amp[1352] - Complex<double> (0, 1) * amp[1353] -
      Complex<double> (0, 1) * amp[1356] - Complex<double> (0, 1) * amp[1357] +
      amp[1366] - Complex<double> (0, 1) * amp[1368] - Complex<double> (0, 1) *
      amp[1369] + amp[1372] + amp[1373] - amp[1382] - Complex<double> (0, 1) *
      amp[1383] - amp[1384] + amp[1397] + amp[1400] - amp[1398] + amp[1403] -
      amp[1401];
  jamp[1] = -amp[1358] - amp[1360] - Complex<double> (0, 1) * amp[1361] -
      Complex<double> (0, 1) * amp[1364] - Complex<double> (0, 1) * amp[1365] +
      amp[1367] + Complex<double> (0, 1) * amp[1368] + Complex<double> (0, 1) *
      amp[1369] + amp[1370] + amp[1371] + amp[1382] + Complex<double> (0, 1) *
      amp[1383] + amp[1384] + amp[1391] + amp[1398] + amp[1399] + amp[1401] +
      amp[1402];
  jamp[2] = +amp[1350] + amp[1352] + Complex<double> (0, 1) * amp[1353] +
      Complex<double> (0, 1) * amp[1356] + Complex<double> (0, 1) * amp[1357] +
      amp[1358] - Complex<double> (0, 1) * amp[1359] + amp[1360] -
      Complex<double> (0, 1) * amp[1362] - Complex<double> (0, 1) * amp[1363] +
      amp[1386] + amp[1388] + amp[1389] + amp[1396] - amp[1400] - amp[1399] -
      amp[1403] - amp[1402];
  jamp[3] = -amp[1358] + Complex<double> (0, 1) * amp[1359] - amp[1360] +
      Complex<double> (0, 1) * amp[1362] + Complex<double> (0, 1) * amp[1363] +
      amp[1375] - Complex<double> (0, 1) * amp[1376] - Complex<double> (0, 1) *
      amp[1377] + amp[1378] + amp[1379] + amp[1382] + amp[1384] -
      Complex<double> (0, 1) * amp[1385] + amp[1387] + amp[1398] + amp[1399] +
      amp[1401] + amp[1402];
  jamp[4] = +amp[1350] - Complex<double> (0, 1) * amp[1351] + amp[1352] -
      Complex<double> (0, 1) * amp[1354] - Complex<double> (0, 1) * amp[1355] +
      amp[1358] + amp[1360] + Complex<double> (0, 1) * amp[1361] +
      Complex<double> (0, 1) * amp[1364] + Complex<double> (0, 1) * amp[1365] +
      amp[1390] + amp[1392] + amp[1393] + amp[1394] - amp[1400] - amp[1399] -
      amp[1403] - amp[1402];
  jamp[5] = -amp[1350] + Complex<double> (0, 1) * amp[1351] - amp[1352] +
      Complex<double> (0, 1) * amp[1354] + Complex<double> (0, 1) * amp[1355] +
      amp[1374] + Complex<double> (0, 1) * amp[1376] + Complex<double> (0, 1) *
      amp[1377] + amp[1380] + amp[1381] - amp[1382] - amp[1384] +
      Complex<double> (0, 1) * amp[1385] + amp[1395] + amp[1400] - amp[1398] +
      amp[1403] - amp[1401];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[17][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P6_sm_lq_lgggq::matrix_4_vexux_vexgggux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 54;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = +amp[1404] + amp[1406] + Complex<double> (0, 1) * amp[1407] +
      Complex<double> (0, 1) * amp[1410] + Complex<double> (0, 1) * amp[1411] -
      amp[1420] + Complex<double> (0, 1) * amp[1422] + Complex<double> (0, 1) *
      amp[1423] - amp[1426] - amp[1427] + amp[1436] + Complex<double> (0, 1) *
      amp[1437] + amp[1438] - amp[1451] - amp[1454] + amp[1452] - amp[1457] +
      amp[1455];
  jamp[1] = +amp[1412] + amp[1414] + Complex<double> (0, 1) * amp[1415] +
      Complex<double> (0, 1) * amp[1418] + Complex<double> (0, 1) * amp[1419] -
      amp[1421] - Complex<double> (0, 1) * amp[1422] - Complex<double> (0, 1) *
      amp[1423] - amp[1424] - amp[1425] - amp[1436] - Complex<double> (0, 1) *
      amp[1437] - amp[1438] - amp[1445] - amp[1452] - amp[1453] - amp[1455] -
      amp[1456];
  jamp[2] = -amp[1404] - amp[1406] - Complex<double> (0, 1) * amp[1407] -
      Complex<double> (0, 1) * amp[1410] - Complex<double> (0, 1) * amp[1411] -
      amp[1412] + Complex<double> (0, 1) * amp[1413] - amp[1414] +
      Complex<double> (0, 1) * amp[1416] + Complex<double> (0, 1) * amp[1417] -
      amp[1440] - amp[1442] - amp[1443] - amp[1450] + amp[1454] + amp[1453] +
      amp[1457] + amp[1456];
  jamp[3] = +amp[1412] - Complex<double> (0, 1) * amp[1413] + amp[1414] -
      Complex<double> (0, 1) * amp[1416] - Complex<double> (0, 1) * amp[1417] -
      amp[1429] + Complex<double> (0, 1) * amp[1430] + Complex<double> (0, 1) *
      amp[1431] - amp[1432] - amp[1433] - amp[1436] - amp[1438] +
      Complex<double> (0, 1) * amp[1439] - amp[1441] - amp[1452] - amp[1453] -
      amp[1455] - amp[1456];
  jamp[4] = -amp[1404] + Complex<double> (0, 1) * amp[1405] - amp[1406] +
      Complex<double> (0, 1) * amp[1408] + Complex<double> (0, 1) * amp[1409] -
      amp[1412] - amp[1414] - Complex<double> (0, 1) * amp[1415] -
      Complex<double> (0, 1) * amp[1418] - Complex<double> (0, 1) * amp[1419] -
      amp[1444] - amp[1446] - amp[1447] - amp[1448] + amp[1454] + amp[1453] +
      amp[1457] + amp[1456];
  jamp[5] = +amp[1404] - Complex<double> (0, 1) * amp[1405] + amp[1406] -
      Complex<double> (0, 1) * amp[1408] - Complex<double> (0, 1) * amp[1409] -
      amp[1428] - Complex<double> (0, 1) * amp[1430] - Complex<double> (0, 1) *
      amp[1431] - amp[1434] - amp[1435] + amp[1436] + amp[1438] -
      Complex<double> (0, 1) * amp[1439] - amp[1449] - amp[1454] + amp[1452] -
      amp[1457] + amp[1455];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[18][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P6_sm_lq_lgggq::matrix_4_vexdx_vexgggdx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 54;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = +amp[1458] + amp[1460] + Complex<double> (0, 1) * amp[1461] +
      Complex<double> (0, 1) * amp[1464] + Complex<double> (0, 1) * amp[1465] -
      amp[1474] + Complex<double> (0, 1) * amp[1476] + Complex<double> (0, 1) *
      amp[1477] - amp[1480] - amp[1481] + amp[1490] + Complex<double> (0, 1) *
      amp[1491] + amp[1492] - amp[1505] - amp[1508] + amp[1506] - amp[1511] +
      amp[1509];
  jamp[1] = +amp[1466] + amp[1468] + Complex<double> (0, 1) * amp[1469] +
      Complex<double> (0, 1) * amp[1472] + Complex<double> (0, 1) * amp[1473] -
      amp[1475] - Complex<double> (0, 1) * amp[1476] - Complex<double> (0, 1) *
      amp[1477] - amp[1478] - amp[1479] - amp[1490] - Complex<double> (0, 1) *
      amp[1491] - amp[1492] - amp[1499] - amp[1506] - amp[1507] - amp[1509] -
      amp[1510];
  jamp[2] = -amp[1458] - amp[1460] - Complex<double> (0, 1) * amp[1461] -
      Complex<double> (0, 1) * amp[1464] - Complex<double> (0, 1) * amp[1465] -
      amp[1466] + Complex<double> (0, 1) * amp[1467] - amp[1468] +
      Complex<double> (0, 1) * amp[1470] + Complex<double> (0, 1) * amp[1471] -
      amp[1494] - amp[1496] - amp[1497] - amp[1504] + amp[1508] + amp[1507] +
      amp[1511] + amp[1510];
  jamp[3] = +amp[1466] - Complex<double> (0, 1) * amp[1467] + amp[1468] -
      Complex<double> (0, 1) * amp[1470] - Complex<double> (0, 1) * amp[1471] -
      amp[1483] + Complex<double> (0, 1) * amp[1484] + Complex<double> (0, 1) *
      amp[1485] - amp[1486] - amp[1487] - amp[1490] - amp[1492] +
      Complex<double> (0, 1) * amp[1493] - amp[1495] - amp[1506] - amp[1507] -
      amp[1509] - amp[1510];
  jamp[4] = -amp[1458] + Complex<double> (0, 1) * amp[1459] - amp[1460] +
      Complex<double> (0, 1) * amp[1462] + Complex<double> (0, 1) * amp[1463] -
      amp[1466] - amp[1468] - Complex<double> (0, 1) * amp[1469] -
      Complex<double> (0, 1) * amp[1472] - Complex<double> (0, 1) * amp[1473] -
      amp[1498] - amp[1500] - amp[1501] - amp[1502] + amp[1508] + amp[1507] +
      amp[1511] + amp[1510];
  jamp[5] = +amp[1458] - Complex<double> (0, 1) * amp[1459] + amp[1460] -
      Complex<double> (0, 1) * amp[1462] - Complex<double> (0, 1) * amp[1463] -
      amp[1482] - Complex<double> (0, 1) * amp[1484] - Complex<double> (0, 1) *
      amp[1485] - amp[1488] - amp[1489] + amp[1490] + amp[1492] -
      Complex<double> (0, 1) * amp[1493] - amp[1503] - amp[1508] + amp[1506] -
      amp[1511] + amp[1509];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[19][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}


}  // end namespace PY8MEs_namespace

