//==========================================================================
// This file has been automatically generated for Pythia 8 by
// MadGraph5_aMC@NLO v. 2.6.0, 2017-08-16
// By the MadGraph5_aMC@NLO Development Team
// Visit launchpad.net/madgraph5 and amcatnlo.web.cern.ch
//==========================================================================

#include "PY8MEs_R4_P15_sm_tamq_tamgqqq.h"
#include "HelAmps_sm.h"

using namespace Pythia8_sm; 

namespace PY8MEs_namespace 
{
//==========================================================================
// Class member functions for calculating the matrix elements for
// Process: ta- u > ta- g u u u~ WEIGHTED<=7 @4
// Process: ta- c > ta- g c c c~ WEIGHTED<=7 @4
// Process: ta- d > ta- g d d d~ WEIGHTED<=7 @4
// Process: ta- s > ta- g s s s~ WEIGHTED<=7 @4
// Process: ta- u~ > ta- g u u~ u~ WEIGHTED<=7 @4
// Process: ta- c~ > ta- g c c~ c~ WEIGHTED<=7 @4
// Process: ta- d~ > ta- g d d~ d~ WEIGHTED<=7 @4
// Process: ta- s~ > ta- g s s~ s~ WEIGHTED<=7 @4
// Process: ta+ u > ta+ g u u u~ WEIGHTED<=7 @4
// Process: ta+ c > ta+ g c c c~ WEIGHTED<=7 @4
// Process: ta+ d > ta+ g d d d~ WEIGHTED<=7 @4
// Process: ta+ s > ta+ g s s s~ WEIGHTED<=7 @4
// Process: ta+ u~ > ta+ g u u~ u~ WEIGHTED<=7 @4
// Process: ta+ c~ > ta+ g c c~ c~ WEIGHTED<=7 @4
// Process: ta+ d~ > ta+ g d d~ d~ WEIGHTED<=7 @4
// Process: ta+ s~ > ta+ g s s~ s~ WEIGHTED<=7 @4
// Process: ta- u > ta- g u c c~ WEIGHTED<=7 @4
// Process: ta- c > ta- g c u u~ WEIGHTED<=7 @4
// Process: ta- u > ta- g u d d~ WEIGHTED<=7 @4
// Process: ta- u > ta- g u s s~ WEIGHTED<=7 @4
// Process: ta- c > ta- g c d d~ WEIGHTED<=7 @4
// Process: ta- c > ta- g c s s~ WEIGHTED<=7 @4
// Process: ta- d > ta- g u d u~ WEIGHTED<=7 @4
// Process: ta- d > ta- g c d c~ WEIGHTED<=7 @4
// Process: ta- s > ta- g u s u~ WEIGHTED<=7 @4
// Process: ta- s > ta- g c s c~ WEIGHTED<=7 @4
// Process: ta- d > ta- g d s s~ WEIGHTED<=7 @4
// Process: ta- s > ta- g s d d~ WEIGHTED<=7 @4
// Process: ta- u~ > ta- g c u~ c~ WEIGHTED<=7 @4
// Process: ta- c~ > ta- g u c~ u~ WEIGHTED<=7 @4
// Process: ta- u~ > ta- g d u~ d~ WEIGHTED<=7 @4
// Process: ta- u~ > ta- g s u~ s~ WEIGHTED<=7 @4
// Process: ta- c~ > ta- g d c~ d~ WEIGHTED<=7 @4
// Process: ta- c~ > ta- g s c~ s~ WEIGHTED<=7 @4
// Process: ta- d~ > ta- g u u~ d~ WEIGHTED<=7 @4
// Process: ta- d~ > ta- g c c~ d~ WEIGHTED<=7 @4
// Process: ta- s~ > ta- g u u~ s~ WEIGHTED<=7 @4
// Process: ta- s~ > ta- g c c~ s~ WEIGHTED<=7 @4
// Process: ta- d~ > ta- g s d~ s~ WEIGHTED<=7 @4
// Process: ta- s~ > ta- g d s~ d~ WEIGHTED<=7 @4
// Process: ta+ u > ta+ g u c c~ WEIGHTED<=7 @4
// Process: ta+ c > ta+ g c u u~ WEIGHTED<=7 @4
// Process: ta+ u > ta+ g u d d~ WEIGHTED<=7 @4
// Process: ta+ u > ta+ g u s s~ WEIGHTED<=7 @4
// Process: ta+ c > ta+ g c d d~ WEIGHTED<=7 @4
// Process: ta+ c > ta+ g c s s~ WEIGHTED<=7 @4
// Process: ta+ d > ta+ g u d u~ WEIGHTED<=7 @4
// Process: ta+ d > ta+ g c d c~ WEIGHTED<=7 @4
// Process: ta+ s > ta+ g u s u~ WEIGHTED<=7 @4
// Process: ta+ s > ta+ g c s c~ WEIGHTED<=7 @4
// Process: ta+ d > ta+ g d s s~ WEIGHTED<=7 @4
// Process: ta+ s > ta+ g s d d~ WEIGHTED<=7 @4
// Process: ta+ u~ > ta+ g c u~ c~ WEIGHTED<=7 @4
// Process: ta+ c~ > ta+ g u c~ u~ WEIGHTED<=7 @4
// Process: ta+ u~ > ta+ g d u~ d~ WEIGHTED<=7 @4
// Process: ta+ u~ > ta+ g s u~ s~ WEIGHTED<=7 @4
// Process: ta+ c~ > ta+ g d c~ d~ WEIGHTED<=7 @4
// Process: ta+ c~ > ta+ g s c~ s~ WEIGHTED<=7 @4
// Process: ta+ d~ > ta+ g u u~ d~ WEIGHTED<=7 @4
// Process: ta+ d~ > ta+ g c c~ d~ WEIGHTED<=7 @4
// Process: ta+ s~ > ta+ g u u~ s~ WEIGHTED<=7 @4
// Process: ta+ s~ > ta+ g c c~ s~ WEIGHTED<=7 @4
// Process: ta+ d~ > ta+ g s d~ s~ WEIGHTED<=7 @4
// Process: ta+ s~ > ta+ g d s~ d~ WEIGHTED<=7 @4

// Exception class
class PY8MEs_R4_P15_sm_tamq_tamgqqqException : public exception
{
  virtual const char * what() const throw()
  {
    return "Exception in class 'PY8MEs_R4_P15_sm_tamq_tamgqqq'."; 
  }
}
PY8MEs_R4_P15_sm_tamq_tamgqqq_exception; 

std::set<int> PY8MEs_R4_P15_sm_tamq_tamgqqq::s_channel_proc = std::set<int>
    (createset<int> ());

int PY8MEs_R4_P15_sm_tamq_tamgqqq::helicities[ncomb][nexternal] = {{-1, -1, -1,
    -1, -1, -1, -1}, {-1, -1, -1, -1, -1, -1, 1}, {-1, -1, -1, -1, -1, 1, -1},
    {-1, -1, -1, -1, -1, 1, 1}, {-1, -1, -1, -1, 1, -1, -1}, {-1, -1, -1, -1,
    1, -1, 1}, {-1, -1, -1, -1, 1, 1, -1}, {-1, -1, -1, -1, 1, 1, 1}, {-1, -1,
    -1, 1, -1, -1, -1}, {-1, -1, -1, 1, -1, -1, 1}, {-1, -1, -1, 1, -1, 1, -1},
    {-1, -1, -1, 1, -1, 1, 1}, {-1, -1, -1, 1, 1, -1, -1}, {-1, -1, -1, 1, 1,
    -1, 1}, {-1, -1, -1, 1, 1, 1, -1}, {-1, -1, -1, 1, 1, 1, 1}, {-1, -1, 1,
    -1, -1, -1, -1}, {-1, -1, 1, -1, -1, -1, 1}, {-1, -1, 1, -1, -1, 1, -1},
    {-1, -1, 1, -1, -1, 1, 1}, {-1, -1, 1, -1, 1, -1, -1}, {-1, -1, 1, -1, 1,
    -1, 1}, {-1, -1, 1, -1, 1, 1, -1}, {-1, -1, 1, -1, 1, 1, 1}, {-1, -1, 1, 1,
    -1, -1, -1}, {-1, -1, 1, 1, -1, -1, 1}, {-1, -1, 1, 1, -1, 1, -1}, {-1, -1,
    1, 1, -1, 1, 1}, {-1, -1, 1, 1, 1, -1, -1}, {-1, -1, 1, 1, 1, -1, 1}, {-1,
    -1, 1, 1, 1, 1, -1}, {-1, -1, 1, 1, 1, 1, 1}, {-1, 1, -1, -1, -1, -1, -1},
    {-1, 1, -1, -1, -1, -1, 1}, {-1, 1, -1, -1, -1, 1, -1}, {-1, 1, -1, -1, -1,
    1, 1}, {-1, 1, -1, -1, 1, -1, -1}, {-1, 1, -1, -1, 1, -1, 1}, {-1, 1, -1,
    -1, 1, 1, -1}, {-1, 1, -1, -1, 1, 1, 1}, {-1, 1, -1, 1, -1, -1, -1}, {-1,
    1, -1, 1, -1, -1, 1}, {-1, 1, -1, 1, -1, 1, -1}, {-1, 1, -1, 1, -1, 1, 1},
    {-1, 1, -1, 1, 1, -1, -1}, {-1, 1, -1, 1, 1, -1, 1}, {-1, 1, -1, 1, 1, 1,
    -1}, {-1, 1, -1, 1, 1, 1, 1}, {-1, 1, 1, -1, -1, -1, -1}, {-1, 1, 1, -1,
    -1, -1, 1}, {-1, 1, 1, -1, -1, 1, -1}, {-1, 1, 1, -1, -1, 1, 1}, {-1, 1, 1,
    -1, 1, -1, -1}, {-1, 1, 1, -1, 1, -1, 1}, {-1, 1, 1, -1, 1, 1, -1}, {-1, 1,
    1, -1, 1, 1, 1}, {-1, 1, 1, 1, -1, -1, -1}, {-1, 1, 1, 1, -1, -1, 1}, {-1,
    1, 1, 1, -1, 1, -1}, {-1, 1, 1, 1, -1, 1, 1}, {-1, 1, 1, 1, 1, -1, -1},
    {-1, 1, 1, 1, 1, -1, 1}, {-1, 1, 1, 1, 1, 1, -1}, {-1, 1, 1, 1, 1, 1, 1},
    {1, -1, -1, -1, -1, -1, -1}, {1, -1, -1, -1, -1, -1, 1}, {1, -1, -1, -1,
    -1, 1, -1}, {1, -1, -1, -1, -1, 1, 1}, {1, -1, -1, -1, 1, -1, -1}, {1, -1,
    -1, -1, 1, -1, 1}, {1, -1, -1, -1, 1, 1, -1}, {1, -1, -1, -1, 1, 1, 1}, {1,
    -1, -1, 1, -1, -1, -1}, {1, -1, -1, 1, -1, -1, 1}, {1, -1, -1, 1, -1, 1,
    -1}, {1, -1, -1, 1, -1, 1, 1}, {1, -1, -1, 1, 1, -1, -1}, {1, -1, -1, 1, 1,
    -1, 1}, {1, -1, -1, 1, 1, 1, -1}, {1, -1, -1, 1, 1, 1, 1}, {1, -1, 1, -1,
    -1, -1, -1}, {1, -1, 1, -1, -1, -1, 1}, {1, -1, 1, -1, -1, 1, -1}, {1, -1,
    1, -1, -1, 1, 1}, {1, -1, 1, -1, 1, -1, -1}, {1, -1, 1, -1, 1, -1, 1}, {1,
    -1, 1, -1, 1, 1, -1}, {1, -1, 1, -1, 1, 1, 1}, {1, -1, 1, 1, -1, -1, -1},
    {1, -1, 1, 1, -1, -1, 1}, {1, -1, 1, 1, -1, 1, -1}, {1, -1, 1, 1, -1, 1,
    1}, {1, -1, 1, 1, 1, -1, -1}, {1, -1, 1, 1, 1, -1, 1}, {1, -1, 1, 1, 1, 1,
    -1}, {1, -1, 1, 1, 1, 1, 1}, {1, 1, -1, -1, -1, -1, -1}, {1, 1, -1, -1, -1,
    -1, 1}, {1, 1, -1, -1, -1, 1, -1}, {1, 1, -1, -1, -1, 1, 1}, {1, 1, -1, -1,
    1, -1, -1}, {1, 1, -1, -1, 1, -1, 1}, {1, 1, -1, -1, 1, 1, -1}, {1, 1, -1,
    -1, 1, 1, 1}, {1, 1, -1, 1, -1, -1, -1}, {1, 1, -1, 1, -1, -1, 1}, {1, 1,
    -1, 1, -1, 1, -1}, {1, 1, -1, 1, -1, 1, 1}, {1, 1, -1, 1, 1, -1, -1}, {1,
    1, -1, 1, 1, -1, 1}, {1, 1, -1, 1, 1, 1, -1}, {1, 1, -1, 1, 1, 1, 1}, {1,
    1, 1, -1, -1, -1, -1}, {1, 1, 1, -1, -1, -1, 1}, {1, 1, 1, -1, -1, 1, -1},
    {1, 1, 1, -1, -1, 1, 1}, {1, 1, 1, -1, 1, -1, -1}, {1, 1, 1, -1, 1, -1, 1},
    {1, 1, 1, -1, 1, 1, -1}, {1, 1, 1, -1, 1, 1, 1}, {1, 1, 1, 1, -1, -1, -1},
    {1, 1, 1, 1, -1, -1, 1}, {1, 1, 1, 1, -1, 1, -1}, {1, 1, 1, 1, -1, 1, 1},
    {1, 1, 1, 1, 1, -1, -1}, {1, 1, 1, 1, 1, -1, 1}, {1, 1, 1, 1, 1, 1, -1},
    {1, 1, 1, 1, 1, 1, 1}};

// Normalization factors the various processes
// Denominators: spins, colors and identical particles
int PY8MEs_R4_P15_sm_tamq_tamgqqq::denom_colors[nprocesses] = {3, 3, 3, 3, 3,
    3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3};
int PY8MEs_R4_P15_sm_tamq_tamgqqq::denom_hels[nprocesses] = {4, 4, 4, 4, 4, 4,
    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4};
int PY8MEs_R4_P15_sm_tamq_tamgqqq::denom_iden[nprocesses] = {2, 2, 2, 2, 2, 2,
    2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};

//--------------------------------------------------------------------------
// Color config initialization
void PY8MEs_R4_P15_sm_tamq_tamgqqq::initColorConfigs() 
{
  color_configs = vector < vec_vec_int > (); 
  jamp_nc_relative_power = vector < vec_int > (); 

  // Color flows of process Process: ta- u > ta- g u u u~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[0].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(3)(1)(1)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #1
  color_configs[0].push_back(vec_int(createvector<int>
      (0)(0)(2)(0)(0)(0)(3)(1)(1)(0)(2)(0)(0)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #2
  color_configs[0].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(3)(2)(1)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #3
  color_configs[0].push_back(vec_int(createvector<int>
      (0)(0)(1)(0)(0)(0)(3)(2)(1)(0)(2)(0)(0)(3)));
  jamp_nc_relative_power[0].push_back(0); 

  // Color flows of process Process: ta- d > ta- g d d d~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[1].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(3)(1)(1)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #1
  color_configs[1].push_back(vec_int(createvector<int>
      (0)(0)(2)(0)(0)(0)(3)(1)(1)(0)(2)(0)(0)(3)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #2
  color_configs[1].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(3)(2)(1)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #3
  color_configs[1].push_back(vec_int(createvector<int>
      (0)(0)(1)(0)(0)(0)(3)(2)(1)(0)(2)(0)(0)(3)));
  jamp_nc_relative_power[1].push_back(0); 

  // Color flows of process Process: ta- u~ > ta- g u u~ u~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(1)(0)(3)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #1
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(3)(0)(1)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #2
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(3)(0)(2)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #3
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(2)(0)(3)));
  jamp_nc_relative_power[2].push_back(0); 

  // Color flows of process Process: ta- d~ > ta- g d d~ d~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[3].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(1)(0)(3)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #1
  color_configs[3].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(3)(0)(1)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #2
  color_configs[3].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(3)(0)(2)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #3
  color_configs[3].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(2)(0)(3)));
  jamp_nc_relative_power[3].push_back(0); 

  // Color flows of process Process: ta+ u > ta+ g u u u~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[4].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(3)(1)(1)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[4].push_back(0); 
  // JAMP #1
  color_configs[4].push_back(vec_int(createvector<int>
      (0)(0)(2)(0)(0)(0)(3)(1)(1)(0)(2)(0)(0)(3)));
  jamp_nc_relative_power[4].push_back(0); 
  // JAMP #2
  color_configs[4].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(3)(2)(1)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[4].push_back(0); 
  // JAMP #3
  color_configs[4].push_back(vec_int(createvector<int>
      (0)(0)(1)(0)(0)(0)(3)(2)(1)(0)(2)(0)(0)(3)));
  jamp_nc_relative_power[4].push_back(0); 

  // Color flows of process Process: ta+ d > ta+ g d d d~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[5].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(3)(1)(1)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[5].push_back(0); 
  // JAMP #1
  color_configs[5].push_back(vec_int(createvector<int>
      (0)(0)(2)(0)(0)(0)(3)(1)(1)(0)(2)(0)(0)(3)));
  jamp_nc_relative_power[5].push_back(0); 
  // JAMP #2
  color_configs[5].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(3)(2)(1)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[5].push_back(0); 
  // JAMP #3
  color_configs[5].push_back(vec_int(createvector<int>
      (0)(0)(1)(0)(0)(0)(3)(2)(1)(0)(2)(0)(0)(3)));
  jamp_nc_relative_power[5].push_back(0); 

  // Color flows of process Process: ta+ u~ > ta+ g u u~ u~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[6].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(1)(0)(3)));
  jamp_nc_relative_power[6].push_back(0); 
  // JAMP #1
  color_configs[6].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(3)(0)(1)));
  jamp_nc_relative_power[6].push_back(0); 
  // JAMP #2
  color_configs[6].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(3)(0)(2)));
  jamp_nc_relative_power[6].push_back(0); 
  // JAMP #3
  color_configs[6].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(2)(0)(3)));
  jamp_nc_relative_power[6].push_back(0); 

  // Color flows of process Process: ta+ d~ > ta+ g d d~ d~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[7].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(1)(0)(3)));
  jamp_nc_relative_power[7].push_back(0); 
  // JAMP #1
  color_configs[7].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(3)(0)(1)));
  jamp_nc_relative_power[7].push_back(0); 
  // JAMP #2
  color_configs[7].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(3)(0)(2)));
  jamp_nc_relative_power[7].push_back(0); 
  // JAMP #3
  color_configs[7].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(2)(0)(3)));
  jamp_nc_relative_power[7].push_back(0); 

  // Color flows of process Process: ta- u > ta- g u c c~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[8].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(3)(1)(1)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[8].push_back(-1); 
  // JAMP #1
  color_configs[8].push_back(vec_int(createvector<int>
      (0)(0)(2)(0)(0)(0)(3)(1)(1)(0)(2)(0)(0)(3)));
  jamp_nc_relative_power[8].push_back(0); 
  // JAMP #2
  color_configs[8].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(3)(2)(1)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[8].push_back(0); 
  // JAMP #3
  color_configs[8].push_back(vec_int(createvector<int>
      (0)(0)(1)(0)(0)(0)(3)(2)(1)(0)(2)(0)(0)(3)));
  jamp_nc_relative_power[8].push_back(-1); 

  // Color flows of process Process: ta- u > ta- g u d d~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[9].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(3)(1)(1)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[9].push_back(-1); 
  // JAMP #1
  color_configs[9].push_back(vec_int(createvector<int>
      (0)(0)(2)(0)(0)(0)(3)(1)(1)(0)(2)(0)(0)(3)));
  jamp_nc_relative_power[9].push_back(0); 
  // JAMP #2
  color_configs[9].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(3)(2)(1)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[9].push_back(0); 
  // JAMP #3
  color_configs[9].push_back(vec_int(createvector<int>
      (0)(0)(1)(0)(0)(0)(3)(2)(1)(0)(2)(0)(0)(3)));
  jamp_nc_relative_power[9].push_back(-1); 

  // Color flows of process Process: ta- d > ta- g u d u~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[10].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(3)(1)(1)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[10].push_back(0); 
  // JAMP #1
  color_configs[10].push_back(vec_int(createvector<int>
      (0)(0)(2)(0)(0)(0)(3)(1)(1)(0)(2)(0)(0)(3)));
  jamp_nc_relative_power[10].push_back(-1); 
  // JAMP #2
  color_configs[10].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(3)(2)(1)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[10].push_back(-1); 
  // JAMP #3
  color_configs[10].push_back(vec_int(createvector<int>
      (0)(0)(1)(0)(0)(0)(3)(2)(1)(0)(2)(0)(0)(3)));
  jamp_nc_relative_power[10].push_back(0); 

  // Color flows of process Process: ta- d > ta- g d s s~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[11].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(3)(1)(1)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[11].push_back(-1); 
  // JAMP #1
  color_configs[11].push_back(vec_int(createvector<int>
      (0)(0)(2)(0)(0)(0)(3)(1)(1)(0)(2)(0)(0)(3)));
  jamp_nc_relative_power[11].push_back(0); 
  // JAMP #2
  color_configs[11].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(3)(2)(1)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[11].push_back(0); 
  // JAMP #3
  color_configs[11].push_back(vec_int(createvector<int>
      (0)(0)(1)(0)(0)(0)(3)(2)(1)(0)(2)(0)(0)(3)));
  jamp_nc_relative_power[11].push_back(-1); 

  // Color flows of process Process: ta- u~ > ta- g c u~ c~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[12].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(1)(0)(3)));
  jamp_nc_relative_power[12].push_back(-1); 
  // JAMP #1
  color_configs[12].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(3)(0)(1)));
  jamp_nc_relative_power[12].push_back(0); 
  // JAMP #2
  color_configs[12].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(3)(0)(2)));
  jamp_nc_relative_power[12].push_back(-1); 
  // JAMP #3
  color_configs[12].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(2)(0)(3)));
  jamp_nc_relative_power[12].push_back(0); 

  // Color flows of process Process: ta- u~ > ta- g d u~ d~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[13].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(1)(0)(3)));
  jamp_nc_relative_power[13].push_back(-1); 
  // JAMP #1
  color_configs[13].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(3)(0)(1)));
  jamp_nc_relative_power[13].push_back(0); 
  // JAMP #2
  color_configs[13].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(3)(0)(2)));
  jamp_nc_relative_power[13].push_back(-1); 
  // JAMP #3
  color_configs[13].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(2)(0)(3)));
  jamp_nc_relative_power[13].push_back(0); 

  // Color flows of process Process: ta- d~ > ta- g u u~ d~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[14].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(1)(0)(3)));
  jamp_nc_relative_power[14].push_back(0); 
  // JAMP #1
  color_configs[14].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(3)(0)(1)));
  jamp_nc_relative_power[14].push_back(-1); 
  // JAMP #2
  color_configs[14].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(3)(0)(2)));
  jamp_nc_relative_power[14].push_back(0); 
  // JAMP #3
  color_configs[14].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(2)(0)(3)));
  jamp_nc_relative_power[14].push_back(-1); 

  // Color flows of process Process: ta- d~ > ta- g s d~ s~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[15].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(1)(0)(3)));
  jamp_nc_relative_power[15].push_back(-1); 
  // JAMP #1
  color_configs[15].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(3)(0)(1)));
  jamp_nc_relative_power[15].push_back(0); 
  // JAMP #2
  color_configs[15].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(3)(0)(2)));
  jamp_nc_relative_power[15].push_back(-1); 
  // JAMP #3
  color_configs[15].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(2)(0)(3)));
  jamp_nc_relative_power[15].push_back(0); 

  // Color flows of process Process: ta+ u > ta+ g u c c~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[16].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(3)(1)(1)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[16].push_back(-1); 
  // JAMP #1
  color_configs[16].push_back(vec_int(createvector<int>
      (0)(0)(2)(0)(0)(0)(3)(1)(1)(0)(2)(0)(0)(3)));
  jamp_nc_relative_power[16].push_back(0); 
  // JAMP #2
  color_configs[16].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(3)(2)(1)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[16].push_back(0); 
  // JAMP #3
  color_configs[16].push_back(vec_int(createvector<int>
      (0)(0)(1)(0)(0)(0)(3)(2)(1)(0)(2)(0)(0)(3)));
  jamp_nc_relative_power[16].push_back(-1); 

  // Color flows of process Process: ta+ u > ta+ g u d d~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[17].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(3)(1)(1)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[17].push_back(-1); 
  // JAMP #1
  color_configs[17].push_back(vec_int(createvector<int>
      (0)(0)(2)(0)(0)(0)(3)(1)(1)(0)(2)(0)(0)(3)));
  jamp_nc_relative_power[17].push_back(0); 
  // JAMP #2
  color_configs[17].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(3)(2)(1)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[17].push_back(0); 
  // JAMP #3
  color_configs[17].push_back(vec_int(createvector<int>
      (0)(0)(1)(0)(0)(0)(3)(2)(1)(0)(2)(0)(0)(3)));
  jamp_nc_relative_power[17].push_back(-1); 

  // Color flows of process Process: ta+ d > ta+ g u d u~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[18].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(3)(1)(1)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[18].push_back(0); 
  // JAMP #1
  color_configs[18].push_back(vec_int(createvector<int>
      (0)(0)(2)(0)(0)(0)(3)(1)(1)(0)(2)(0)(0)(3)));
  jamp_nc_relative_power[18].push_back(-1); 
  // JAMP #2
  color_configs[18].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(3)(2)(1)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[18].push_back(-1); 
  // JAMP #3
  color_configs[18].push_back(vec_int(createvector<int>
      (0)(0)(1)(0)(0)(0)(3)(2)(1)(0)(2)(0)(0)(3)));
  jamp_nc_relative_power[18].push_back(0); 

  // Color flows of process Process: ta+ d > ta+ g d s s~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[19].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(3)(1)(1)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[19].push_back(-1); 
  // JAMP #1
  color_configs[19].push_back(vec_int(createvector<int>
      (0)(0)(2)(0)(0)(0)(3)(1)(1)(0)(2)(0)(0)(3)));
  jamp_nc_relative_power[19].push_back(0); 
  // JAMP #2
  color_configs[19].push_back(vec_int(createvector<int>
      (0)(0)(3)(0)(0)(0)(3)(2)(1)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[19].push_back(0); 
  // JAMP #3
  color_configs[19].push_back(vec_int(createvector<int>
      (0)(0)(1)(0)(0)(0)(3)(2)(1)(0)(2)(0)(0)(3)));
  jamp_nc_relative_power[19].push_back(-1); 

  // Color flows of process Process: ta+ u~ > ta+ g c u~ c~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[20].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(1)(0)(3)));
  jamp_nc_relative_power[20].push_back(-1); 
  // JAMP #1
  color_configs[20].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(3)(0)(1)));
  jamp_nc_relative_power[20].push_back(0); 
  // JAMP #2
  color_configs[20].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(3)(0)(2)));
  jamp_nc_relative_power[20].push_back(-1); 
  // JAMP #3
  color_configs[20].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(2)(0)(3)));
  jamp_nc_relative_power[20].push_back(0); 

  // Color flows of process Process: ta+ u~ > ta+ g d u~ d~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[21].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(1)(0)(3)));
  jamp_nc_relative_power[21].push_back(-1); 
  // JAMP #1
  color_configs[21].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(3)(0)(1)));
  jamp_nc_relative_power[21].push_back(0); 
  // JAMP #2
  color_configs[21].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(3)(0)(2)));
  jamp_nc_relative_power[21].push_back(-1); 
  // JAMP #3
  color_configs[21].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(2)(0)(3)));
  jamp_nc_relative_power[21].push_back(0); 

  // Color flows of process Process: ta+ d~ > ta+ g u u~ d~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[22].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(1)(0)(3)));
  jamp_nc_relative_power[22].push_back(0); 
  // JAMP #1
  color_configs[22].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(3)(0)(1)));
  jamp_nc_relative_power[22].push_back(-1); 
  // JAMP #2
  color_configs[22].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(3)(0)(2)));
  jamp_nc_relative_power[22].push_back(0); 
  // JAMP #3
  color_configs[22].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(2)(0)(3)));
  jamp_nc_relative_power[22].push_back(-1); 

  // Color flows of process Process: ta+ d~ > ta+ g s d~ s~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[23].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(1)(0)(3)));
  jamp_nc_relative_power[23].push_back(-1); 
  // JAMP #1
  color_configs[23].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(3)(0)(1)));
  jamp_nc_relative_power[23].push_back(0); 
  // JAMP #2
  color_configs[23].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(3)(0)(2)));
  jamp_nc_relative_power[23].push_back(-1); 
  // JAMP #3
  color_configs[23].push_back(vec_int(createvector<int>
      (0)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(2)(0)(3)));
  jamp_nc_relative_power[23].push_back(0); 
}

//--------------------------------------------------------------------------
// Destructor.
PY8MEs_R4_P15_sm_tamq_tamgqqq::~PY8MEs_R4_P15_sm_tamq_tamgqqq() 
{
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    delete[] p[i]; 
    p[i] = NULL; 
  }
}

//--------------------------------------------------------------------------
// Invert the permutation mapping
vector<int> PY8MEs_R4_P15_sm_tamq_tamgqqq::invert_mapping(vector<int> mapping) 
{
  vector<int> inverted_mapping; 
  for (unsigned int i = 0; i < mapping.size(); i++ )
  {
    for (unsigned int j = 0; j < mapping.size(); j++ )
    {
      if (mapping[j] == ((int)i))
      {
        inverted_mapping.push_back(j); 
        break; 
      }
    }
  }
  return inverted_mapping; 
}

//--------------------------------------------------------------------------
// Return the list of possible helicity configurations
vector < vec_int > PY8MEs_R4_P15_sm_tamq_tamgqqq::getHelicityConfigs(vector<int> permutation) 
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(ncomb, vector<int> (nexternal, 0)); 
  for (unsigned int ihel = 0; ihel < ncomb; ihel++ )
  {
    for(unsigned int j = 0; j < nexternal; j++ )
    {
      res[ihel][chosenPerm[j]] = helicities[ihel][j]; 
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Return the list of possible color configurations
vector < vec_int > PY8MEs_R4_P15_sm_tamq_tamgqqq::getColorConfigs(int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(color_configs[chosenProcID].size(), vector<int>
      (nexternal * 2, 0));
  for (unsigned int icol = 0; icol < color_configs[chosenProcID].size(); icol++
      )
  {
    for(unsigned int j = 0; j < (2 * nexternal); j++ )
    {
      res[icol][chosenPerm[j/2] * 2 + j%2] =
          color_configs[chosenProcID][icol][j];
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Get JAMP relative N_c power
int PY8MEs_R4_P15_sm_tamq_tamgqqq::getColorFlowRelativeNCPower(int
    color_flow_ID, int specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return jamp_nc_relative_power[chosenProcID][color_flow_ID]; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity ID -> Helicity Config
vector<int> PY8MEs_R4_P15_sm_tamq_tamgqqq::getHelicityConfigForID(int hel_ID,
    vector<int> permutation)
{
  if (hel_ID < 0 || hel_ID >= ncomb)
  {
    cerr <<  "Error in function 'getHelicityConfigForID' of class" << 
    " 'PY8MEs_R4_P15_sm_tamq_tamgqqq': Specified helicity ID '" << 
    hel_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R4_P15_sm_tamq_tamgqqq_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(nexternal, 0); 
  for (unsigned int j = 0; j < nexternal; j++ )
  {
    res[chosenPerm[j]] = helicities[hel_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity Config -> Helicity ID
int PY8MEs_R4_P15_sm_tamq_tamgqqq::getHelicityIDForConfig(vector<int>
    hel_config, vector<int> permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  int user_ihel = -1; 
  if (hel_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < ncomb; i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < nexternal; j++ )
      {
        if (helicities[i][chosenPerm[j]] != hel_config[j])
        {
          found = false; 
          break; 
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_ihel = i; 
        break; 
      }
    }
    if (user_ihel == -1)
    {
      cerr <<  "Error in function 'getHelicityIDForConfig' of class" << 
      " 'PY8MEs_R4_P15_sm_tamq_tamgqqq': Specified helicity" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R4_P15_sm_tamq_tamgqqq_exception; 
    }
  }
  return user_ihel; 
}


//--------------------------------------------------------------------------
// Implements the map Color ID -> Color Config
vector<int> PY8MEs_R4_P15_sm_tamq_tamgqqq::getColorConfigForID(int color_ID,
    int specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < 0 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getColorConfigForID' of class" << 
    " 'PY8MEs_R4_P15_sm_tamq_tamgqqq': Specified color ID '" << 
    color_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R4_P15_sm_tamq_tamgqqq_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(color_configs[chosenProcID][color_ID].size(), 0); 
  for (unsigned int j = 0; j < (2 * nexternal); j++ )
  {
    res[chosenPerm[j/2] * 2 + j%2] = color_configs[chosenProcID][color_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Color Config -> Color ID
int PY8MEs_R4_P15_sm_tamq_tamgqqq::getColorIDForConfig(vector<int>
    color_config, int specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = -1; 
  if (color_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < color_configs[chosenProcID].size(); i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < (nexternal * 2); j++ )
      {

        // If colorless then make sure it matches
        // The little arithmetics in the color index is just
        // the permutation applies on the particle list which is
        // twice smaller since each particle can have two color indices.
        if (color_config[j] == 0)
        {
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] != 0)
          {
            found = false; 
            break; 
          }
          // Otherwise check that the color linked position matches
        }
        else
        {
          int color_linked_pos = -1; 
          // Find the other end of the line in the user color config
          for (unsigned int k = 0; k < (nexternal * 2); k++ )
          {
            if (k == j)
              continue; 
            if (color_config[j] == color_config[k])
            {
              color_linked_pos = k; 
              break; 
            }
          }
          if (color_linked_pos == -1)
          {
            cerr <<  "Error in function 'getColorIDForConfig' of class" << 
            " 'PY8MEs_R4_P15_sm_tamq_tamgqqq': A color line could " << 
            " not be closed." << endl; 
            throw PY8MEs_R4_P15_sm_tamq_tamgqqq_exception; 
          }
          // Now check whether the color line matches
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] !=
              color_configs[chosenProcID][i][chosenPerm[color_linked_pos/2] * 2
              + color_linked_pos%2])
          {
            found = false; 
            break; 
          }
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_icol = i; 
        break; 
      }
    }

    if (user_icol == -1)
    {
      cerr <<  "Error in function 'getColorIDForConfig' of class" << 
      " 'PY8MEs_R4_P15_sm_tamq_tamgqqq': Specified color" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R4_P15_sm_tamq_tamgqqq_exception; 
    }
  }
  return user_icol; 
}

//--------------------------------------------------------------------------
// Returns all result previously computed in SigmaKin
vector < vec_double > PY8MEs_R4_P15_sm_tamq_tamgqqq::getAllResults(int
    specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return all_results[chosenProcID]; 
}

//--------------------------------------------------------------------------
// Returns a result previously computed in SigmaKin for a specific helicity
// and color ID. -1 means avg and summed over that characteristic.
double PY8MEs_R4_P15_sm_tamq_tamgqqq::getResult(int helicity_ID, int color_ID,
    int specify_proc_ID)
{
  if (helicity_ID < - 1 || helicity_ID >= ncomb)
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R4_P15_sm_tamq_tamgqqq': Specified helicity ID '" << 
    helicity_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R4_P15_sm_tamq_tamgqqq_exception; 
  }
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < - 1 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R4_P15_sm_tamq_tamgqqq': Specified color ID '" << 
    color_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R4_P15_sm_tamq_tamgqqq_exception; 
  }
  return all_results[chosenProcID][helicity_ID + 1][color_ID + 1]; 
}

//--------------------------------------------------------------------------
// Check for the availability of the requested process and if available,
// If available, this returns the corresponding permutation and Proc_ID to use.
// If not available, this returns a negative Proc_ID.
pair < vector<int> , int > PY8MEs_R4_P15_sm_tamq_tamgqqq::static_getPY8ME(vector<int> initial_pdgs, vector<int> final_pdgs, set<int> schannels) 
{

  // Not available return value
  pair < vector<int> , int > NA(vector<int> (), -1); 

  // Check if s-channel requirements match
  if (nreq_s_channels > 0)
  {
    if (schannels != s_channel_proc)
      return NA; 
  }
  else
  {
    if (schannels.size() != 0)
      return NA; 
  }

  // Check number of final state particles
  if (final_pdgs.size() != (nexternal - ninitial))
    return NA; 

  // Check number of initial state particles
  if (initial_pdgs.size() != ninitial)
    return NA; 

  // List of processes available in this class
  const int nprocs = 64; 
  const int proc_IDS[nprocs] = {0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7,
      8, 8, 9, 9, 9, 9, 10, 10, 10, 10, 11, 11, 12, 12, 13, 13, 13, 13, 14, 14,
      14, 14, 15, 15, 16, 16, 17, 17, 17, 17, 18, 18, 18, 18, 19, 19, 20, 20,
      21, 21, 21, 21, 22, 22, 22, 22, 23, 23};
  const int in_pdgs[nprocs][ninitial] = {{15, 2}, {15, 4}, {15, 1}, {15, 3},
      {15, -2}, {15, -4}, {15, -1}, {15, -3}, {-15, 2}, {-15, 4}, {-15, 1},
      {-15, 3}, {-15, -2}, {-15, -4}, {-15, -1}, {-15, -3}, {15, 2}, {15, 4},
      {15, 2}, {15, 2}, {15, 4}, {15, 4}, {15, 1}, {15, 1}, {15, 3}, {15, 3},
      {15, 1}, {15, 3}, {15, -2}, {15, -4}, {15, -2}, {15, -2}, {15, -4}, {15,
      -4}, {15, -1}, {15, -1}, {15, -3}, {15, -3}, {15, -1}, {15, -3}, {-15,
      2}, {-15, 4}, {-15, 2}, {-15, 2}, {-15, 4}, {-15, 4}, {-15, 1}, {-15, 1},
      {-15, 3}, {-15, 3}, {-15, 1}, {-15, 3}, {-15, -2}, {-15, -4}, {-15, -2},
      {-15, -2}, {-15, -4}, {-15, -4}, {-15, -1}, {-15, -1}, {-15, -3}, {-15,
      -3}, {-15, -1}, {-15, -3}};
  const int out_pdgs[nprocs][nexternal - ninitial] = {{15, 21, 2, 2, -2}, {15,
      21, 4, 4, -4}, {15, 21, 1, 1, -1}, {15, 21, 3, 3, -3}, {15, 21, 2, -2,
      -2}, {15, 21, 4, -4, -4}, {15, 21, 1, -1, -1}, {15, 21, 3, -3, -3}, {-15,
      21, 2, 2, -2}, {-15, 21, 4, 4, -4}, {-15, 21, 1, 1, -1}, {-15, 21, 3, 3,
      -3}, {-15, 21, 2, -2, -2}, {-15, 21, 4, -4, -4}, {-15, 21, 1, -1, -1},
      {-15, 21, 3, -3, -3}, {15, 21, 2, 4, -4}, {15, 21, 4, 2, -2}, {15, 21, 2,
      1, -1}, {15, 21, 2, 3, -3}, {15, 21, 4, 1, -1}, {15, 21, 4, 3, -3}, {15,
      21, 2, 1, -2}, {15, 21, 4, 1, -4}, {15, 21, 2, 3, -2}, {15, 21, 4, 3,
      -4}, {15, 21, 1, 3, -3}, {15, 21, 3, 1, -1}, {15, 21, 4, -2, -4}, {15,
      21, 2, -4, -2}, {15, 21, 1, -2, -1}, {15, 21, 3, -2, -3}, {15, 21, 1, -4,
      -1}, {15, 21, 3, -4, -3}, {15, 21, 2, -2, -1}, {15, 21, 4, -4, -1}, {15,
      21, 2, -2, -3}, {15, 21, 4, -4, -3}, {15, 21, 3, -1, -3}, {15, 21, 1, -3,
      -1}, {-15, 21, 2, 4, -4}, {-15, 21, 4, 2, -2}, {-15, 21, 2, 1, -1}, {-15,
      21, 2, 3, -3}, {-15, 21, 4, 1, -1}, {-15, 21, 4, 3, -3}, {-15, 21, 2, 1,
      -2}, {-15, 21, 4, 1, -4}, {-15, 21, 2, 3, -2}, {-15, 21, 4, 3, -4}, {-15,
      21, 1, 3, -3}, {-15, 21, 3, 1, -1}, {-15, 21, 4, -2, -4}, {-15, 21, 2,
      -4, -2}, {-15, 21, 1, -2, -1}, {-15, 21, 3, -2, -3}, {-15, 21, 1, -4,
      -1}, {-15, 21, 3, -4, -3}, {-15, 21, 2, -2, -1}, {-15, 21, 4, -4, -1},
      {-15, 21, 2, -2, -3}, {-15, 21, 4, -4, -3}, {-15, 21, 3, -1, -3}, {-15,
      21, 1, -3, -1}};

  bool in_pdgs_used[ninitial]; 
  bool out_pdgs_used[nexternal - ninitial]; 
  for(unsigned int i = 0; i < nprocs; i++ )
  {
    int permutations[nexternal]; 

    // Reinitialize initial state look-up variables
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      in_pdgs_used[j] = false; 
      permutations[j] = -1; 
    }
    // Look for initial state matches
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      for(unsigned int k = 0; k < ninitial; k++ )
      {
        // Make sure it has not been used already
        if (in_pdgs_used[k])
          continue; 
        if (initial_pdgs[k] == in_pdgs[i][j])
        {
          permutations[j] = k; 
          in_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[ninitial - 1] == -1)
      continue; 

    // Reinitialize final state look-up variables
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      out_pdgs_used[j] = false; 
      permutations[ninitial + j] = -1; 
    }
    // Look for final state matches
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      for(unsigned int k = 0; k < (nexternal - ninitial); k++ )
      {
        // Make sure it has not been used already
        if (out_pdgs_used[k])
          continue; 
        if (final_pdgs[k] == out_pdgs[i][j])
        {
          permutations[ninitial + j] = ninitial + k; 
          out_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[ninitial + j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[nexternal - 1] == -1)
      continue; 

    // Return process found
    return pair < vector<int> , int > (vector<int> (permutations, permutations
        + nexternal), proc_IDS[i]);
  }

  // No process found
  return NA; 
}

//--------------------------------------------------------------------------
// Set momenta
void PY8MEs_R4_P15_sm_tamq_tamgqqq::setMomenta(vector < vec_double >
    momenta_picked)
{
  if (momenta_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setMomenta' of class" << 
    " 'PY8MEs_R4_P15_sm_tamq_tamgqqq': Incorrect number of" << 
    " momenta specified." << endl; 
    throw PY8MEs_R4_P15_sm_tamq_tamgqqq_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    if (momenta_picked[i].size() != 4)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R4_P15_sm_tamq_tamgqqq': Incorrect number of" << 
      " momenta components specified." << endl; 
      throw PY8MEs_R4_P15_sm_tamq_tamgqqq_exception; 
    }
    if (momenta_picked[i][0] < 0.0)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R4_P15_sm_tamq_tamgqqq': A momentum was specified" << 
      " with negative energy. Check conventions." << endl; 
      throw PY8MEs_R4_P15_sm_tamq_tamgqqq_exception; 
    }
    for (unsigned int j = 0; j < 4; j++ )
    {
      p[i][j] = momenta_picked[i][j]; 
    }
  }
}

//--------------------------------------------------------------------------
// Set color configuration to use. An empty vector means sum over all.
void PY8MEs_R4_P15_sm_tamq_tamgqqq::setColors(vector<int> colors_picked)
{
  if (colors_picked.size() == 0)
  {
    user_colors = vector<int> (); 
    return; 
  }
  if (colors_picked.size() != (2 * nexternal))
  {
    cerr <<  "Error in function 'setColors' of class" << 
    " 'PY8MEs_R4_P15_sm_tamq_tamgqqq': Incorrect number" << 
    " of colors specified." << endl; 
    throw PY8MEs_R4_P15_sm_tamq_tamgqqq_exception; 
  }
  user_colors = vector<int> ((2 * nexternal), 0); 
  for(unsigned int i = 0; i < (2 * nexternal); i++ )
  {
    user_colors[i] = colors_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the helicity configuration to use. Am empty vector means sum over all.
void PY8MEs_R4_P15_sm_tamq_tamgqqq::setHelicities(vector<int>
    helicities_picked)
{
  if (helicities_picked.size() != nexternal)
  {
    if (helicities_picked.size() == 0)
    {
      user_helicities = vector<int> (); 
      return; 
    }
    cerr <<  "Error in function 'setHelicities' of class" << 
    " 'PY8MEs_R4_P15_sm_tamq_tamgqqq': Incorrect number" << 
    " of helicities specified." << endl; 
    throw PY8MEs_R4_P15_sm_tamq_tamgqqq_exception; 
  }
  user_helicities = vector<int> (nexternal, 0); 
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    user_helicities[i] = helicities_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the permutation to use (will apply to momenta, colors and helicities)
void PY8MEs_R4_P15_sm_tamq_tamgqqq::setPermutation(vector<int> perm_picked) 
{
  if (perm_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setPermutations' of class" << 
    " 'PY8MEs_R4_P15_sm_tamq_tamgqqq': Incorrect number" << 
    " of permutations specified." << endl; 
    throw PY8MEs_R4_P15_sm_tamq_tamgqqq_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    perm[i] = perm_picked[i]; 
  }
}

// Set the proc_ID to use
void PY8MEs_R4_P15_sm_tamq_tamgqqq::setProcID(int procID_picked) 
{
  proc_ID = procID_picked; 
}

//--------------------------------------------------------------------------
// Initialize process.

void PY8MEs_R4_P15_sm_tamq_tamgqqq::initProc() 
{

  // Initialize flags
  include_symmetry_factors = true; 
  include_helicity_averaging_factors = true; 
  include_color_averaging_factors = true; 

  // Initialize vectors.
  perm = vector<int> (nexternal, 0); 
  user_colors = vector<int> (2 * nexternal, 0); 
  user_helicities = vector<int> (nexternal, 0); 
  p = vector < double * > (); 
  for (unsigned int i = 0; i < nexternal; i++ )
  {
    p.push_back(new double[4]); 
  }
  initColorConfigs(); 
  // Synchronize local variables dependent on the model with the active model.
  mME = vector<double> (nexternal, 0.); 
  syncProcModelParams(); 
  jamp2 = vector < vec_double > (24); 
  jamp2[0] = vector<double> (4, 0.); 
  jamp2[1] = vector<double> (4, 0.); 
  jamp2[2] = vector<double> (4, 0.); 
  jamp2[3] = vector<double> (4, 0.); 
  jamp2[4] = vector<double> (4, 0.); 
  jamp2[5] = vector<double> (4, 0.); 
  jamp2[6] = vector<double> (4, 0.); 
  jamp2[7] = vector<double> (4, 0.); 
  jamp2[8] = vector<double> (4, 0.); 
  jamp2[9] = vector<double> (4, 0.); 
  jamp2[10] = vector<double> (4, 0.); 
  jamp2[11] = vector<double> (4, 0.); 
  jamp2[12] = vector<double> (4, 0.); 
  jamp2[13] = vector<double> (4, 0.); 
  jamp2[14] = vector<double> (4, 0.); 
  jamp2[15] = vector<double> (4, 0.); 
  jamp2[16] = vector<double> (4, 0.); 
  jamp2[17] = vector<double> (4, 0.); 
  jamp2[18] = vector<double> (4, 0.); 
  jamp2[19] = vector<double> (4, 0.); 
  jamp2[20] = vector<double> (4, 0.); 
  jamp2[21] = vector<double> (4, 0.); 
  jamp2[22] = vector<double> (4, 0.); 
  jamp2[23] = vector<double> (4, 0.); 
  all_results = vector < vec_vec_double > (24); 
  // The first entry is always the color or helicity avg/summed matrix element.
  all_results[0] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[1] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[2] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[3] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[4] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[5] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[6] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[7] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[8] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[9] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[10] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[11] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[12] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[13] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[14] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[15] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[16] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[17] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[18] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[19] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[20] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[21] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[22] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[23] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
}

// Synchronize local variables of the process that depend on the model
// parameters
void PY8MEs_R4_P15_sm_tamq_tamgqqq::syncProcModelParams() 
{

  // Instantiate the model class and set parameters that stay fixed during run
  mME[0] = pars->mdl_MTA; 
  mME[1] = pars->ZERO; 
  mME[2] = pars->mdl_MTA; 
  mME[3] = pars->ZERO; 
  mME[4] = pars->ZERO; 
  mME[5] = pars->ZERO; 
  mME[6] = pars->ZERO; 
}

//--------------------------------------------------------------------------
// Setter allowing to force particular values for the external masses
void PY8MEs_R4_P15_sm_tamq_tamgqqq::setMasses(vec_double external_masses) 
{

  if (external_masses.size() != mME.size())
  {
    cerr <<  "Error in function 'setMasses' of class" << 
    " 'PY8MEs_R4_P15_sm_tamq_tamgqqq': Incorrect number of" << 
    " masses specified." << endl; 
    throw PY8MEs_R4_P15_sm_tamq_tamgqqq_exception; 
  }
  for (unsigned int j = 0; j < mME.size(); j++ )
  {
    mME[j] = external_masses[perm[j]]; 
  }
}

//--------------------------------------------------------------------------
// Getter accessing external masses with the correct ordering
vector<double> PY8MEs_R4_P15_sm_tamq_tamgqqq::getMasses() 
{

  vec_double external_masses; 
  vector<int> invertedPerm; 
  invertedPerm = invert_mapping(perm); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    external_masses.push_back(mME[invertedPerm[i]]); 
  }
  return external_masses; 

}


// Set all values of the external masses to float(-mode) where mode can be
// 0 : Mass taken from the model
// 1 : Mass taken from p_i^2 if not massless to begin with
// 2 : Mass always taken from p_i^2.
void PY8MEs_R4_P15_sm_tamq_tamgqqq::setExternalMassesMode(int mode) 
{
  if (mode != 0 && mode != 1 && mode != 2)
  {
    cerr <<  "Error in function 'setExternalMassesMode' of class" << 
    " 'PY8MEs_R4_P15_sm_tamq_tamgqqq': Incorrect mode selected :" << mode << 
    ". It must be either 0, 1 or 2" << endl; 
    throw PY8MEs_R4_P15_sm_tamq_tamgqqq_exception; 
  }
  if (mode == 0)
  {
    syncProcModelParams(); 
  }
  else if (mode == 1)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      if (mME[j] != pars->ZERO)
      {
        mME[j] = -1.0; 
      }
    }
  }
  else if (mode == 2)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      mME[j] = -1.0; 
    }
  }
}

//--------------------------------------------------------------------------
// Evaluate the squared matrix element.

double PY8MEs_R4_P15_sm_tamq_tamgqqq::sigmaKin() 
{
  // Set the parameters which change event by event
  pars->setDependentParameters(); 
  pars->setDependentCouplings(); 
  // Reset color flows
  for(int i = 0; i < 4; i++ )
    jamp2[0][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[1][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[2][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[3][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[4][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[5][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[6][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[7][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[8][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[9][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[10][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[11][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[12][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[13][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[14][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[15][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[16][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[17][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[18][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[19][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[20][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[21][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[22][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[23][i] = 0.; 

  // Save previous values of mME
  vector<double> saved_mME(mME.size(), 0.0); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (mME[i] < 0.0)
    {
      saved_mME[i] = mME[i]; 
      mME[i] = sqrt(abs(pow(p[perm[i]][0], 2) - 
      (pow(p[perm[i]][1], 2) + pow(p[perm[i]][2], 2) + pow(p[perm[i]][3],
          2))));
    }
  }

  // Local variables and constants
  const int max_tries = 10; 
  vector < vec_bool > goodhel(nprocesses, vec_bool(ncomb, false)); 
  vec_int ntry(nprocesses, 0); 
  double t = 0.; 
  double result = 0.; 

  if (ntry[proc_ID] <= max_tries)
    ntry[proc_ID] = ntry[proc_ID] + 1; 

  // Find which helicity configuration is asked for
  // -1 indicates one wants to sum over helicities
  int user_ihel = getHelicityIDForConfig(user_helicities); 

  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = getColorIDForConfig(user_colors); 

  // Reset the list of results that will be recomputed here
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      all_results[proc_ID][ihel + 1][icolor + 1] = 0.; 
    }
  }

  // Calculate the matrix element for all helicities
  // unless already detected as vanishing
  for(int ihel = 0; ihel < ncomb; ihel++ )
  {
    // Skip helicity if already detected as vanishing
    if ((ntry[proc_ID] >= max_tries) && !goodhel[proc_ID][ihel])
      continue; 

    // Also skip helicity if user asks for a specific one
    if ((ntry[proc_ID] >= max_tries) && user_ihel != -1 && user_ihel != ihel)
      continue; 

    calculate_wavefunctions(helicities[ihel]); 

    // Reset locally computed color flows
    for(int i = 0; i < 4; i++ )
      jamp2[0][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[1][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[2][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[3][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[4][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[5][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[6][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[7][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[8][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[9][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[10][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[11][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[12][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[13][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[14][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[15][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[16][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[17][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[18][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[19][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[20][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[21][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[22][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[23][i] = 0.; 

    if (proc_ID == 0)
      t = matrix_4_tamu_tamguuux(); 
    if (proc_ID == 1)
      t = matrix_4_tamd_tamgdddx(); 
    if (proc_ID == 2)
      t = matrix_4_tamux_tamguuxux(); 
    if (proc_ID == 3)
      t = matrix_4_tamdx_tamgddxdx(); 
    if (proc_ID == 4)
      t = matrix_4_tapu_tapguuux(); 
    if (proc_ID == 5)
      t = matrix_4_tapd_tapgdddx(); 
    if (proc_ID == 6)
      t = matrix_4_tapux_tapguuxux(); 
    if (proc_ID == 7)
      t = matrix_4_tapdx_tapgddxdx(); 
    if (proc_ID == 8)
      t = matrix_4_tamu_tamguccx(); 
    if (proc_ID == 9)
      t = matrix_4_tamu_tamguddx(); 
    if (proc_ID == 10)
      t = matrix_4_tamd_tamgudux(); 
    if (proc_ID == 11)
      t = matrix_4_tamd_tamgdssx(); 
    if (proc_ID == 12)
      t = matrix_4_tamux_tamgcuxcx(); 
    if (proc_ID == 13)
      t = matrix_4_tamux_tamgduxdx(); 
    if (proc_ID == 14)
      t = matrix_4_tamdx_tamguuxdx(); 
    if (proc_ID == 15)
      t = matrix_4_tamdx_tamgsdxsx(); 
    if (proc_ID == 16)
      t = matrix_4_tapu_tapguccx(); 
    if (proc_ID == 17)
      t = matrix_4_tapu_tapguddx(); 
    if (proc_ID == 18)
      t = matrix_4_tapd_tapgudux(); 
    if (proc_ID == 19)
      t = matrix_4_tapd_tapgdssx(); 
    if (proc_ID == 20)
      t = matrix_4_tapux_tapgcuxcx(); 
    if (proc_ID == 21)
      t = matrix_4_tapux_tapgduxdx(); 
    if (proc_ID == 22)
      t = matrix_4_tapdx_tapguuxdx(); 
    if (proc_ID == 23)
      t = matrix_4_tapdx_tapgsdxsx(); 

    // Store which helicities give non-zero result
    if ((ntry[proc_ID] < max_tries) && t != 0. && !goodhel[proc_ID][ihel])
      goodhel[proc_ID][ihel] = true; 

    // Aggregate results
    if (user_ihel == -1 || user_ihel == ihel)
    {
      if (user_icol == -1)
      {
        result = result + t; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][0] += t; 
          for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
          {
            all_results[proc_ID][0][i + 1] += jamp2[proc_ID][i]; 
          }
        }
        all_results[proc_ID][ihel + 1][0] += t; 
        for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
        {
          all_results[proc_ID][ihel + 1][i + 1] += jamp2[proc_ID][i]; 
        }
      }
      else
      {
        result = result + jamp2[proc_ID][user_icol]; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][user_icol + 1] += jamp2[proc_ID][user_icol]; 
        }
        all_results[proc_ID][ihel + 1][user_icol + 1] +=
            jamp2[proc_ID][user_icol];
      }
    }

  }

  // Normalize results with the identical particle factor
  if (include_symmetry_factors)
  {
    result = result/denom_iden[proc_ID]; 
  }
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      if (include_symmetry_factors)
      {
        all_results[proc_ID][ihel + 1][icolor + 1] /= denom_iden[proc_ID]; 
      }
    }
  }


  // Normalize when when summing+averaging over helicity configurations
  if (user_ihel == -1 && include_helicity_averaging_factors)
  {
    result /= denom_hels[proc_ID]; 
    if (user_icol == -1)
    {
      all_results[proc_ID][0][0] /= denom_hels[proc_ID]; 
      for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
      {
        all_results[proc_ID][0][i + 1] /= denom_hels[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][0][user_icol + 1] /= denom_hels[proc_ID]; 
    }
  }

  // Normalize when summing+averaging over color configurations
  if (user_icol == -1 && include_color_averaging_factors)
  {
    result /= denom_colors[proc_ID]; 
    if (user_ihel == -1)
    {
      all_results[proc_ID][0][0] /= denom_colors[proc_ID]; 
      for (unsigned int i = 0; i < ncomb; i++ )
      {
        all_results[proc_ID][i + 1][0] /= denom_colors[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][user_ihel + 1][0] /= denom_colors[proc_ID]; 
    }
  }

  // Reinstate previous values of mME
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (saved_mME[i] < 0.0)
    {
      mME[i] = saved_mME[i]; 
    }
  }

  // Finally return it
  return result; 
}

//==========================================================================
// Private class member functions

//--------------------------------------------------------------------------
// Evaluate |M|^2 for each subprocess

void PY8MEs_R4_P15_sm_tamq_tamgqqq::calculate_wavefunctions(const int hel[])
{
  // Calculate wavefunctions for all processes
  // Calculate all wavefunctions
  ixxxxx(p[perm[0]], mME[0], hel[0], +1, w[0]); 
  ixxxxx(p[perm[1]], mME[1], hel[1], +1, w[1]); 
  oxxxxx(p[perm[2]], mME[2], hel[2], +1, w[2]); 
  vxxxxx(p[perm[3]], mME[3], hel[3], +1, w[3]); 
  oxxxxx(p[perm[4]], mME[4], hel[4], +1, w[4]); 
  oxxxxx(p[perm[5]], mME[5], hel[5], +1, w[5]); 
  ixxxxx(p[perm[6]], mME[6], hel[6], -1, w[6]); 
  FFV1P0_3(w[0], w[2], pars->GC_3, pars->ZERO, pars->ZERO, w[7]); 
  FFV1_1(w[4], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[8]); 
  FFV1_1(w[5], w[7], pars->GC_2, pars->ZERO, pars->ZERO, w[9]); 
  FFV1P0_3(w[1], w[8], pars->GC_11, pars->ZERO, pars->ZERO, w[10]); 
  FFV1P0_3(w[6], w[8], pars->GC_11, pars->ZERO, pars->ZERO, w[11]); 
  FFV1_2(w[1], w[7], pars->GC_2, pars->ZERO, pars->ZERO, w[12]); 
  FFV1_2(w[6], w[7], pars->GC_2, pars->ZERO, pars->ZERO, w[13]); 
  FFV2_4_3(w[0], w[2], pars->GC_50, pars->GC_59, pars->mdl_MZ, pars->mdl_WZ,
      w[14]);
  FFV2_5_1(w[5], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[15]);
  FFV2_5_2(w[1], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[16]);
  FFV2_5_2(w[6], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[17]);
  FFV1P0_3(w[1], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[18]); 
  FFV1_1(w[8], w[7], pars->GC_2, pars->ZERO, pars->ZERO, w[19]); 
  FFV2_5_1(w[8], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[20]);
  FFV1P0_3(w[6], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[21]); 
  FFV1_1(w[5], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[22]); 
  FFV1_1(w[4], w[7], pars->GC_2, pars->ZERO, pars->ZERO, w[23]); 
  FFV1P0_3(w[1], w[22], pars->GC_11, pars->ZERO, pars->ZERO, w[24]); 
  FFV1P0_3(w[6], w[22], pars->GC_11, pars->ZERO, pars->ZERO, w[25]); 
  FFV2_5_1(w[4], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[26]);
  FFV1P0_3(w[1], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[27]); 
  FFV1_1(w[22], w[7], pars->GC_2, pars->ZERO, pars->ZERO, w[28]); 
  FFV2_5_1(w[22], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[29]);
  FFV1P0_3(w[6], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[30]); 
  FFV1_2(w[1], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[31]); 
  FFV1P0_3(w[31], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[32]); 
  FFV1P0_3(w[31], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[33]); 
  FFV1_2(w[31], w[7], pars->GC_2, pars->ZERO, pars->ZERO, w[34]); 
  FFV2_5_2(w[31], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[35]);
  FFV1_2(w[6], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[36]); 
  FFV1P0_3(w[36], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[37]); 
  FFV1P0_3(w[36], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[38]); 
  FFV1_2(w[36], w[7], pars->GC_2, pars->ZERO, pars->ZERO, w[39]); 
  FFV2_5_2(w[36], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[40]);
  VVV1P0_1(w[3], w[27], pars->GC_10, pars->ZERO, pars->ZERO, w[41]); 
  FFV1_2(w[6], w[27], pars->GC_11, pars->ZERO, pars->ZERO, w[42]); 
  FFV1_1(w[5], w[27], pars->GC_11, pars->ZERO, pars->ZERO, w[43]); 
  VVV1P0_1(w[3], w[30], pars->GC_10, pars->ZERO, pars->ZERO, w[44]); 
  FFV1_2(w[1], w[30], pars->GC_11, pars->ZERO, pars->ZERO, w[45]); 
  FFV1_1(w[5], w[30], pars->GC_11, pars->ZERO, pars->ZERO, w[46]); 
  VVV1P0_1(w[3], w[18], pars->GC_10, pars->ZERO, pars->ZERO, w[47]); 
  FFV1_2(w[6], w[18], pars->GC_11, pars->ZERO, pars->ZERO, w[48]); 
  FFV1_1(w[4], w[18], pars->GC_11, pars->ZERO, pars->ZERO, w[49]); 
  VVV1P0_1(w[3], w[21], pars->GC_10, pars->ZERO, pars->ZERO, w[50]); 
  FFV1_2(w[1], w[21], pars->GC_11, pars->ZERO, pars->ZERO, w[51]); 
  FFV1_1(w[4], w[21], pars->GC_11, pars->ZERO, pars->ZERO, w[52]); 
  FFV1_1(w[5], w[7], pars->GC_1, pars->ZERO, pars->ZERO, w[53]); 
  FFV1_2(w[1], w[7], pars->GC_1, pars->ZERO, pars->ZERO, w[54]); 
  FFV1_2(w[6], w[7], pars->GC_1, pars->ZERO, pars->ZERO, w[55]); 
  FFV2_3_1(w[5], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[56]);
  FFV2_3_2(w[1], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[57]);
  FFV2_3_2(w[6], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[58]);
  FFV1_1(w[8], w[7], pars->GC_1, pars->ZERO, pars->ZERO, w[59]); 
  FFV2_3_1(w[8], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[60]);
  FFV1_1(w[4], w[7], pars->GC_1, pars->ZERO, pars->ZERO, w[61]); 
  FFV2_3_1(w[4], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[62]);
  FFV1_1(w[22], w[7], pars->GC_1, pars->ZERO, pars->ZERO, w[63]); 
  FFV2_3_1(w[22], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[64]);
  FFV1_2(w[31], w[7], pars->GC_1, pars->ZERO, pars->ZERO, w[65]); 
  FFV2_3_2(w[31], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[66]);
  FFV1_2(w[36], w[7], pars->GC_1, pars->ZERO, pars->ZERO, w[67]); 
  FFV2_3_2(w[36], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[68]);
  oxxxxx(p[perm[1]], mME[1], hel[1], -1, w[69]); 
  ixxxxx(p[perm[5]], mME[5], hel[5], -1, w[70]); 
  FFV1_1(w[69], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[71]); 
  FFV1P0_3(w[70], w[71], pars->GC_11, pars->ZERO, pars->ZERO, w[72]); 
  FFV1P0_3(w[6], w[71], pars->GC_11, pars->ZERO, pars->ZERO, w[73]); 
  FFV1_2(w[70], w[7], pars->GC_2, pars->ZERO, pars->ZERO, w[74]); 
  FFV2_5_2(w[70], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[75]);
  FFV1P0_3(w[70], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[76]); 
  FFV1_1(w[71], w[7], pars->GC_2, pars->ZERO, pars->ZERO, w[77]); 
  FFV2_5_1(w[71], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[78]);
  FFV1_1(w[69], w[7], pars->GC_2, pars->ZERO, pars->ZERO, w[79]); 
  FFV1P0_3(w[70], w[8], pars->GC_11, pars->ZERO, pars->ZERO, w[80]); 
  FFV2_5_1(w[69], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[81]);
  FFV1P0_3(w[70], w[69], pars->GC_11, pars->ZERO, pars->ZERO, w[82]); 
  FFV1P0_3(w[6], w[69], pars->GC_11, pars->ZERO, pars->ZERO, w[83]); 
  FFV1_2(w[70], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[84]); 
  FFV1P0_3(w[84], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[85]); 
  FFV1P0_3(w[84], w[69], pars->GC_11, pars->ZERO, pars->ZERO, w[86]); 
  FFV1_2(w[84], w[7], pars->GC_2, pars->ZERO, pars->ZERO, w[87]); 
  FFV2_5_2(w[84], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[88]);
  FFV1P0_3(w[36], w[69], pars->GC_11, pars->ZERO, pars->ZERO, w[89]); 
  VVV1P0_1(w[3], w[82], pars->GC_10, pars->ZERO, pars->ZERO, w[90]); 
  FFV1_2(w[6], w[82], pars->GC_11, pars->ZERO, pars->ZERO, w[91]); 
  FFV1_1(w[4], w[82], pars->GC_11, pars->ZERO, pars->ZERO, w[92]); 
  VVV1P0_1(w[3], w[83], pars->GC_10, pars->ZERO, pars->ZERO, w[93]); 
  FFV1_2(w[70], w[83], pars->GC_11, pars->ZERO, pars->ZERO, w[94]); 
  FFV1_1(w[4], w[83], pars->GC_11, pars->ZERO, pars->ZERO, w[95]); 
  VVV1P0_1(w[3], w[76], pars->GC_10, pars->ZERO, pars->ZERO, w[96]); 
  FFV1_2(w[6], w[76], pars->GC_11, pars->ZERO, pars->ZERO, w[97]); 
  FFV1_1(w[69], w[76], pars->GC_11, pars->ZERO, pars->ZERO, w[98]); 
  FFV1_2(w[70], w[30], pars->GC_11, pars->ZERO, pars->ZERO, w[99]); 
  FFV1_1(w[69], w[30], pars->GC_11, pars->ZERO, pars->ZERO, w[100]); 
  FFV1_2(w[70], w[7], pars->GC_1, pars->ZERO, pars->ZERO, w[101]); 
  FFV2_3_2(w[70], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[102]);
  FFV1_1(w[71], w[7], pars->GC_1, pars->ZERO, pars->ZERO, w[103]); 
  FFV2_3_1(w[71], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[104]);
  FFV1_1(w[69], w[7], pars->GC_1, pars->ZERO, pars->ZERO, w[105]); 
  FFV2_3_1(w[69], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[106]);
  FFV1_2(w[84], w[7], pars->GC_1, pars->ZERO, pars->ZERO, w[107]); 
  FFV2_3_2(w[84], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[108]);
  oxxxxx(p[perm[0]], mME[0], hel[0], -1, w[109]); 
  ixxxxx(p[perm[2]], mME[2], hel[2], -1, w[110]); 
  FFV1P0_3(w[110], w[109], pars->GC_3, pars->ZERO, pars->ZERO, w[111]); 
  FFV1_1(w[5], w[111], pars->GC_2, pars->ZERO, pars->ZERO, w[112]); 
  FFV1_2(w[1], w[111], pars->GC_2, pars->ZERO, pars->ZERO, w[113]); 
  FFV1_2(w[6], w[111], pars->GC_2, pars->ZERO, pars->ZERO, w[114]); 
  FFV2_4_3(w[110], w[109], pars->GC_50, pars->GC_59, pars->mdl_MZ,
      pars->mdl_WZ, w[115]);
  FFV2_5_1(w[5], w[115], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[116]);
  FFV2_5_2(w[1], w[115], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[117]);
  FFV2_5_2(w[6], w[115], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[118]);
  FFV1_1(w[8], w[111], pars->GC_2, pars->ZERO, pars->ZERO, w[119]); 
  FFV2_5_1(w[8], w[115], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[120]);
  FFV1_1(w[4], w[111], pars->GC_2, pars->ZERO, pars->ZERO, w[121]); 
  FFV2_5_1(w[4], w[115], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[122]);
  FFV1_1(w[22], w[111], pars->GC_2, pars->ZERO, pars->ZERO, w[123]); 
  FFV2_5_1(w[22], w[115], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[124]);
  FFV1_2(w[31], w[111], pars->GC_2, pars->ZERO, pars->ZERO, w[125]); 
  FFV2_5_2(w[31], w[115], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[126]);
  FFV1_2(w[36], w[111], pars->GC_2, pars->ZERO, pars->ZERO, w[127]); 
  FFV2_5_2(w[36], w[115], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[128]);
  FFV1_1(w[5], w[111], pars->GC_1, pars->ZERO, pars->ZERO, w[129]); 
  FFV1_2(w[1], w[111], pars->GC_1, pars->ZERO, pars->ZERO, w[130]); 
  FFV1_2(w[6], w[111], pars->GC_1, pars->ZERO, pars->ZERO, w[131]); 
  FFV2_3_1(w[5], w[115], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[132]);
  FFV2_3_2(w[1], w[115], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[133]);
  FFV2_3_2(w[6], w[115], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[134]);
  FFV1_1(w[8], w[111], pars->GC_1, pars->ZERO, pars->ZERO, w[135]); 
  FFV2_3_1(w[8], w[115], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[136]);
  FFV1_1(w[4], w[111], pars->GC_1, pars->ZERO, pars->ZERO, w[137]); 
  FFV2_3_1(w[4], w[115], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[138]);
  FFV1_1(w[22], w[111], pars->GC_1, pars->ZERO, pars->ZERO, w[139]); 
  FFV2_3_1(w[22], w[115], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[140]);
  FFV1_2(w[31], w[111], pars->GC_1, pars->ZERO, pars->ZERO, w[141]); 
  FFV2_3_2(w[31], w[115], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[142]);
  FFV1_2(w[36], w[111], pars->GC_1, pars->ZERO, pars->ZERO, w[143]); 
  FFV2_3_2(w[36], w[115], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[144]);
  FFV1_2(w[70], w[111], pars->GC_2, pars->ZERO, pars->ZERO, w[145]); 
  FFV2_5_2(w[70], w[115], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[146]);
  FFV1_1(w[71], w[111], pars->GC_2, pars->ZERO, pars->ZERO, w[147]); 
  FFV2_5_1(w[71], w[115], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[148]);
  FFV1_1(w[69], w[111], pars->GC_2, pars->ZERO, pars->ZERO, w[149]); 
  FFV2_5_1(w[69], w[115], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[150]);
  FFV1_2(w[84], w[111], pars->GC_2, pars->ZERO, pars->ZERO, w[151]); 
  FFV2_5_2(w[84], w[115], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[152]);
  FFV1_2(w[70], w[111], pars->GC_1, pars->ZERO, pars->ZERO, w[153]); 
  FFV2_3_2(w[70], w[115], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[154]);
  FFV1_1(w[71], w[111], pars->GC_1, pars->ZERO, pars->ZERO, w[155]); 
  FFV2_3_1(w[71], w[115], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[156]);
  FFV1_1(w[69], w[111], pars->GC_1, pars->ZERO, pars->ZERO, w[157]); 
  FFV2_3_1(w[69], w[115], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[158]);
  FFV1_2(w[84], w[111], pars->GC_1, pars->ZERO, pars->ZERO, w[159]); 
  FFV2_3_2(w[84], w[115], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[160]);

  // Calculate all amplitudes
  // Amplitude(s) for diagram number 0
  FFV1_0(w[6], w[9], w[10], pars->GC_11, amp[0]); 
  FFV1_0(w[1], w[9], w[11], pars->GC_11, amp[1]); 
  FFV1_0(w[12], w[5], w[11], pars->GC_11, amp[2]); 
  FFV1_0(w[13], w[5], w[10], pars->GC_11, amp[3]); 
  FFV1_0(w[6], w[15], w[10], pars->GC_11, amp[4]); 
  FFV1_0(w[1], w[15], w[11], pars->GC_11, amp[5]); 
  FFV1_0(w[16], w[5], w[11], pars->GC_11, amp[6]); 
  FFV1_0(w[17], w[5], w[10], pars->GC_11, amp[7]); 
  FFV1_0(w[6], w[19], w[18], pars->GC_11, amp[8]); 
  FFV1_0(w[13], w[8], w[18], pars->GC_11, amp[9]); 
  FFV1_0(w[6], w[20], w[18], pars->GC_11, amp[10]); 
  FFV1_0(w[17], w[8], w[18], pars->GC_11, amp[11]); 
  FFV1_0(w[1], w[19], w[21], pars->GC_11, amp[12]); 
  FFV1_0(w[12], w[8], w[21], pars->GC_11, amp[13]); 
  FFV1_0(w[1], w[20], w[21], pars->GC_11, amp[14]); 
  FFV1_0(w[16], w[8], w[21], pars->GC_11, amp[15]); 
  FFV1_0(w[6], w[23], w[24], pars->GC_11, amp[16]); 
  FFV1_0(w[1], w[23], w[25], pars->GC_11, amp[17]); 
  FFV1_0(w[12], w[4], w[25], pars->GC_11, amp[18]); 
  FFV1_0(w[13], w[4], w[24], pars->GC_11, amp[19]); 
  FFV1_0(w[6], w[26], w[24], pars->GC_11, amp[20]); 
  FFV1_0(w[1], w[26], w[25], pars->GC_11, amp[21]); 
  FFV1_0(w[16], w[4], w[25], pars->GC_11, amp[22]); 
  FFV1_0(w[17], w[4], w[24], pars->GC_11, amp[23]); 
  FFV1_0(w[6], w[28], w[27], pars->GC_11, amp[24]); 
  FFV1_0(w[13], w[22], w[27], pars->GC_11, amp[25]); 
  FFV1_0(w[6], w[29], w[27], pars->GC_11, amp[26]); 
  FFV1_0(w[17], w[22], w[27], pars->GC_11, amp[27]); 
  FFV1_0(w[1], w[28], w[30], pars->GC_11, amp[28]); 
  FFV1_0(w[12], w[22], w[30], pars->GC_11, amp[29]); 
  FFV1_0(w[1], w[29], w[30], pars->GC_11, amp[30]); 
  FFV1_0(w[16], w[22], w[30], pars->GC_11, amp[31]); 
  FFV1_0(w[6], w[23], w[32], pars->GC_11, amp[32]); 
  FFV1_0(w[6], w[9], w[33], pars->GC_11, amp[33]); 
  FFV1_0(w[13], w[5], w[33], pars->GC_11, amp[34]); 
  FFV1_0(w[13], w[4], w[32], pars->GC_11, amp[35]); 
  FFV1_0(w[6], w[26], w[32], pars->GC_11, amp[36]); 
  FFV1_0(w[6], w[15], w[33], pars->GC_11, amp[37]); 
  FFV1_0(w[17], w[5], w[33], pars->GC_11, amp[38]); 
  FFV1_0(w[17], w[4], w[32], pars->GC_11, amp[39]); 
  FFV1_0(w[34], w[5], w[30], pars->GC_11, amp[40]); 
  FFV1_0(w[31], w[9], w[30], pars->GC_11, amp[41]); 
  FFV1_0(w[35], w[5], w[30], pars->GC_11, amp[42]); 
  FFV1_0(w[31], w[15], w[30], pars->GC_11, amp[43]); 
  FFV1_0(w[34], w[4], w[21], pars->GC_11, amp[44]); 
  FFV1_0(w[31], w[23], w[21], pars->GC_11, amp[45]); 
  FFV1_0(w[35], w[4], w[21], pars->GC_11, amp[46]); 
  FFV1_0(w[31], w[26], w[21], pars->GC_11, amp[47]); 
  FFV1_0(w[1], w[23], w[37], pars->GC_11, amp[48]); 
  FFV1_0(w[1], w[9], w[38], pars->GC_11, amp[49]); 
  FFV1_0(w[12], w[5], w[38], pars->GC_11, amp[50]); 
  FFV1_0(w[12], w[4], w[37], pars->GC_11, amp[51]); 
  FFV1_0(w[1], w[26], w[37], pars->GC_11, amp[52]); 
  FFV1_0(w[1], w[15], w[38], pars->GC_11, amp[53]); 
  FFV1_0(w[16], w[5], w[38], pars->GC_11, amp[54]); 
  FFV1_0(w[16], w[4], w[37], pars->GC_11, amp[55]); 
  FFV1_0(w[39], w[5], w[27], pars->GC_11, amp[56]); 
  FFV1_0(w[36], w[9], w[27], pars->GC_11, amp[57]); 
  FFV1_0(w[40], w[5], w[27], pars->GC_11, amp[58]); 
  FFV1_0(w[36], w[15], w[27], pars->GC_11, amp[59]); 
  FFV1_0(w[39], w[4], w[18], pars->GC_11, amp[60]); 
  FFV1_0(w[36], w[23], w[18], pars->GC_11, amp[61]); 
  FFV1_0(w[40], w[4], w[18], pars->GC_11, amp[62]); 
  FFV1_0(w[36], w[26], w[18], pars->GC_11, amp[63]); 
  FFV1_0(w[6], w[9], w[41], pars->GC_11, amp[64]); 
  FFV1_0(w[42], w[9], w[3], pars->GC_11, amp[65]); 
  FFV1_0(w[13], w[5], w[41], pars->GC_11, amp[66]); 
  FFV1_0(w[13], w[43], w[3], pars->GC_11, amp[67]); 
  FFV1_0(w[6], w[15], w[41], pars->GC_11, amp[68]); 
  FFV1_0(w[42], w[15], w[3], pars->GC_11, amp[69]); 
  FFV1_0(w[17], w[5], w[41], pars->GC_11, amp[70]); 
  FFV1_0(w[17], w[43], w[3], pars->GC_11, amp[71]); 
  FFV1_0(w[1], w[9], w[44], pars->GC_11, amp[72]); 
  FFV1_0(w[45], w[9], w[3], pars->GC_11, amp[73]); 
  FFV1_0(w[12], w[5], w[44], pars->GC_11, amp[74]); 
  FFV1_0(w[12], w[46], w[3], pars->GC_11, amp[75]); 
  FFV1_0(w[1], w[15], w[44], pars->GC_11, amp[76]); 
  FFV1_0(w[45], w[15], w[3], pars->GC_11, amp[77]); 
  FFV1_0(w[16], w[5], w[44], pars->GC_11, amp[78]); 
  FFV1_0(w[16], w[46], w[3], pars->GC_11, amp[79]); 
  FFV1_0(w[6], w[23], w[47], pars->GC_11, amp[80]); 
  FFV1_0(w[48], w[23], w[3], pars->GC_11, amp[81]); 
  FFV1_0(w[13], w[4], w[47], pars->GC_11, amp[82]); 
  FFV1_0(w[13], w[49], w[3], pars->GC_11, amp[83]); 
  FFV1_0(w[6], w[26], w[47], pars->GC_11, amp[84]); 
  FFV1_0(w[48], w[26], w[3], pars->GC_11, amp[85]); 
  FFV1_0(w[17], w[4], w[47], pars->GC_11, amp[86]); 
  FFV1_0(w[17], w[49], w[3], pars->GC_11, amp[87]); 
  FFV1_0(w[1], w[23], w[50], pars->GC_11, amp[88]); 
  FFV1_0(w[51], w[23], w[3], pars->GC_11, amp[89]); 
  FFV1_0(w[12], w[4], w[50], pars->GC_11, amp[90]); 
  FFV1_0(w[12], w[52], w[3], pars->GC_11, amp[91]); 
  FFV1_0(w[1], w[26], w[50], pars->GC_11, amp[92]); 
  FFV1_0(w[51], w[26], w[3], pars->GC_11, amp[93]); 
  FFV1_0(w[16], w[4], w[50], pars->GC_11, amp[94]); 
  FFV1_0(w[16], w[52], w[3], pars->GC_11, amp[95]); 
  FFV1_0(w[6], w[53], w[10], pars->GC_11, amp[96]); 
  FFV1_0(w[1], w[53], w[11], pars->GC_11, amp[97]); 
  FFV1_0(w[54], w[5], w[11], pars->GC_11, amp[98]); 
  FFV1_0(w[55], w[5], w[10], pars->GC_11, amp[99]); 
  FFV1_0(w[6], w[56], w[10], pars->GC_11, amp[100]); 
  FFV1_0(w[1], w[56], w[11], pars->GC_11, amp[101]); 
  FFV1_0(w[57], w[5], w[11], pars->GC_11, amp[102]); 
  FFV1_0(w[58], w[5], w[10], pars->GC_11, amp[103]); 
  FFV1_0(w[6], w[59], w[18], pars->GC_11, amp[104]); 
  FFV1_0(w[55], w[8], w[18], pars->GC_11, amp[105]); 
  FFV1_0(w[6], w[60], w[18], pars->GC_11, amp[106]); 
  FFV1_0(w[58], w[8], w[18], pars->GC_11, amp[107]); 
  FFV1_0(w[1], w[59], w[21], pars->GC_11, amp[108]); 
  FFV1_0(w[54], w[8], w[21], pars->GC_11, amp[109]); 
  FFV1_0(w[1], w[60], w[21], pars->GC_11, amp[110]); 
  FFV1_0(w[57], w[8], w[21], pars->GC_11, amp[111]); 
  FFV1_0(w[6], w[61], w[24], pars->GC_11, amp[112]); 
  FFV1_0(w[1], w[61], w[25], pars->GC_11, amp[113]); 
  FFV1_0(w[54], w[4], w[25], pars->GC_11, amp[114]); 
  FFV1_0(w[55], w[4], w[24], pars->GC_11, amp[115]); 
  FFV1_0(w[6], w[62], w[24], pars->GC_11, amp[116]); 
  FFV1_0(w[1], w[62], w[25], pars->GC_11, amp[117]); 
  FFV1_0(w[57], w[4], w[25], pars->GC_11, amp[118]); 
  FFV1_0(w[58], w[4], w[24], pars->GC_11, amp[119]); 
  FFV1_0(w[6], w[63], w[27], pars->GC_11, amp[120]); 
  FFV1_0(w[55], w[22], w[27], pars->GC_11, amp[121]); 
  FFV1_0(w[6], w[64], w[27], pars->GC_11, amp[122]); 
  FFV1_0(w[58], w[22], w[27], pars->GC_11, amp[123]); 
  FFV1_0(w[1], w[63], w[30], pars->GC_11, amp[124]); 
  FFV1_0(w[54], w[22], w[30], pars->GC_11, amp[125]); 
  FFV1_0(w[1], w[64], w[30], pars->GC_11, amp[126]); 
  FFV1_0(w[57], w[22], w[30], pars->GC_11, amp[127]); 
  FFV1_0(w[6], w[61], w[32], pars->GC_11, amp[128]); 
  FFV1_0(w[6], w[53], w[33], pars->GC_11, amp[129]); 
  FFV1_0(w[55], w[5], w[33], pars->GC_11, amp[130]); 
  FFV1_0(w[55], w[4], w[32], pars->GC_11, amp[131]); 
  FFV1_0(w[6], w[62], w[32], pars->GC_11, amp[132]); 
  FFV1_0(w[6], w[56], w[33], pars->GC_11, amp[133]); 
  FFV1_0(w[58], w[5], w[33], pars->GC_11, amp[134]); 
  FFV1_0(w[58], w[4], w[32], pars->GC_11, amp[135]); 
  FFV1_0(w[65], w[5], w[30], pars->GC_11, amp[136]); 
  FFV1_0(w[31], w[53], w[30], pars->GC_11, amp[137]); 
  FFV1_0(w[66], w[5], w[30], pars->GC_11, amp[138]); 
  FFV1_0(w[31], w[56], w[30], pars->GC_11, amp[139]); 
  FFV1_0(w[65], w[4], w[21], pars->GC_11, amp[140]); 
  FFV1_0(w[31], w[61], w[21], pars->GC_11, amp[141]); 
  FFV1_0(w[66], w[4], w[21], pars->GC_11, amp[142]); 
  FFV1_0(w[31], w[62], w[21], pars->GC_11, amp[143]); 
  FFV1_0(w[1], w[61], w[37], pars->GC_11, amp[144]); 
  FFV1_0(w[1], w[53], w[38], pars->GC_11, amp[145]); 
  FFV1_0(w[54], w[5], w[38], pars->GC_11, amp[146]); 
  FFV1_0(w[54], w[4], w[37], pars->GC_11, amp[147]); 
  FFV1_0(w[1], w[62], w[37], pars->GC_11, amp[148]); 
  FFV1_0(w[1], w[56], w[38], pars->GC_11, amp[149]); 
  FFV1_0(w[57], w[5], w[38], pars->GC_11, amp[150]); 
  FFV1_0(w[57], w[4], w[37], pars->GC_11, amp[151]); 
  FFV1_0(w[67], w[5], w[27], pars->GC_11, amp[152]); 
  FFV1_0(w[36], w[53], w[27], pars->GC_11, amp[153]); 
  FFV1_0(w[68], w[5], w[27], pars->GC_11, amp[154]); 
  FFV1_0(w[36], w[56], w[27], pars->GC_11, amp[155]); 
  FFV1_0(w[67], w[4], w[18], pars->GC_11, amp[156]); 
  FFV1_0(w[36], w[61], w[18], pars->GC_11, amp[157]); 
  FFV1_0(w[68], w[4], w[18], pars->GC_11, amp[158]); 
  FFV1_0(w[36], w[62], w[18], pars->GC_11, amp[159]); 
  FFV1_0(w[6], w[53], w[41], pars->GC_11, amp[160]); 
  FFV1_0(w[42], w[53], w[3], pars->GC_11, amp[161]); 
  FFV1_0(w[55], w[5], w[41], pars->GC_11, amp[162]); 
  FFV1_0(w[55], w[43], w[3], pars->GC_11, amp[163]); 
  FFV1_0(w[6], w[56], w[41], pars->GC_11, amp[164]); 
  FFV1_0(w[42], w[56], w[3], pars->GC_11, amp[165]); 
  FFV1_0(w[58], w[5], w[41], pars->GC_11, amp[166]); 
  FFV1_0(w[58], w[43], w[3], pars->GC_11, amp[167]); 
  FFV1_0(w[1], w[53], w[44], pars->GC_11, amp[168]); 
  FFV1_0(w[45], w[53], w[3], pars->GC_11, amp[169]); 
  FFV1_0(w[54], w[5], w[44], pars->GC_11, amp[170]); 
  FFV1_0(w[54], w[46], w[3], pars->GC_11, amp[171]); 
  FFV1_0(w[1], w[56], w[44], pars->GC_11, amp[172]); 
  FFV1_0(w[45], w[56], w[3], pars->GC_11, amp[173]); 
  FFV1_0(w[57], w[5], w[44], pars->GC_11, amp[174]); 
  FFV1_0(w[57], w[46], w[3], pars->GC_11, amp[175]); 
  FFV1_0(w[6], w[61], w[47], pars->GC_11, amp[176]); 
  FFV1_0(w[48], w[61], w[3], pars->GC_11, amp[177]); 
  FFV1_0(w[55], w[4], w[47], pars->GC_11, amp[178]); 
  FFV1_0(w[55], w[49], w[3], pars->GC_11, amp[179]); 
  FFV1_0(w[6], w[62], w[47], pars->GC_11, amp[180]); 
  FFV1_0(w[48], w[62], w[3], pars->GC_11, amp[181]); 
  FFV1_0(w[58], w[4], w[47], pars->GC_11, amp[182]); 
  FFV1_0(w[58], w[49], w[3], pars->GC_11, amp[183]); 
  FFV1_0(w[1], w[61], w[50], pars->GC_11, amp[184]); 
  FFV1_0(w[51], w[61], w[3], pars->GC_11, amp[185]); 
  FFV1_0(w[54], w[4], w[50], pars->GC_11, amp[186]); 
  FFV1_0(w[54], w[52], w[3], pars->GC_11, amp[187]); 
  FFV1_0(w[1], w[62], w[50], pars->GC_11, amp[188]); 
  FFV1_0(w[51], w[62], w[3], pars->GC_11, amp[189]); 
  FFV1_0(w[57], w[4], w[50], pars->GC_11, amp[190]); 
  FFV1_0(w[57], w[52], w[3], pars->GC_11, amp[191]); 
  FFV1_0(w[6], w[23], w[72], pars->GC_11, amp[192]); 
  FFV1_0(w[70], w[23], w[73], pars->GC_11, amp[193]); 
  FFV1_0(w[74], w[4], w[73], pars->GC_11, amp[194]); 
  FFV1_0(w[13], w[4], w[72], pars->GC_11, amp[195]); 
  FFV1_0(w[6], w[26], w[72], pars->GC_11, amp[196]); 
  FFV1_0(w[70], w[26], w[73], pars->GC_11, amp[197]); 
  FFV1_0(w[75], w[4], w[73], pars->GC_11, amp[198]); 
  FFV1_0(w[17], w[4], w[72], pars->GC_11, amp[199]); 
  FFV1_0(w[6], w[77], w[76], pars->GC_11, amp[200]); 
  FFV1_0(w[13], w[71], w[76], pars->GC_11, amp[201]); 
  FFV1_0(w[6], w[78], w[76], pars->GC_11, amp[202]); 
  FFV1_0(w[17], w[71], w[76], pars->GC_11, amp[203]); 
  FFV1_0(w[70], w[77], w[30], pars->GC_11, amp[204]); 
  FFV1_0(w[74], w[71], w[30], pars->GC_11, amp[205]); 
  FFV1_0(w[70], w[78], w[30], pars->GC_11, amp[206]); 
  FFV1_0(w[75], w[71], w[30], pars->GC_11, amp[207]); 
  FFV1_0(w[6], w[79], w[80], pars->GC_11, amp[208]); 
  FFV1_0(w[70], w[79], w[11], pars->GC_11, amp[209]); 
  FFV1_0(w[74], w[69], w[11], pars->GC_11, amp[210]); 
  FFV1_0(w[13], w[69], w[80], pars->GC_11, amp[211]); 
  FFV1_0(w[6], w[81], w[80], pars->GC_11, amp[212]); 
  FFV1_0(w[70], w[81], w[11], pars->GC_11, amp[213]); 
  FFV1_0(w[75], w[69], w[11], pars->GC_11, amp[214]); 
  FFV1_0(w[17], w[69], w[80], pars->GC_11, amp[215]); 
  FFV1_0(w[6], w[19], w[82], pars->GC_11, amp[216]); 
  FFV1_0(w[13], w[8], w[82], pars->GC_11, amp[217]); 
  FFV1_0(w[6], w[20], w[82], pars->GC_11, amp[218]); 
  FFV1_0(w[17], w[8], w[82], pars->GC_11, amp[219]); 
  FFV1_0(w[70], w[19], w[83], pars->GC_11, amp[220]); 
  FFV1_0(w[74], w[8], w[83], pars->GC_11, amp[221]); 
  FFV1_0(w[70], w[20], w[83], pars->GC_11, amp[222]); 
  FFV1_0(w[75], w[8], w[83], pars->GC_11, amp[223]); 
  FFV1_0(w[6], w[79], w[85], pars->GC_11, amp[224]); 
  FFV1_0(w[6], w[23], w[86], pars->GC_11, amp[225]); 
  FFV1_0(w[13], w[4], w[86], pars->GC_11, amp[226]); 
  FFV1_0(w[13], w[69], w[85], pars->GC_11, amp[227]); 
  FFV1_0(w[6], w[81], w[85], pars->GC_11, amp[228]); 
  FFV1_0(w[6], w[26], w[86], pars->GC_11, amp[229]); 
  FFV1_0(w[17], w[4], w[86], pars->GC_11, amp[230]); 
  FFV1_0(w[17], w[69], w[85], pars->GC_11, amp[231]); 
  FFV1_0(w[87], w[4], w[83], pars->GC_11, amp[232]); 
  FFV1_0(w[84], w[23], w[83], pars->GC_11, amp[233]); 
  FFV1_0(w[88], w[4], w[83], pars->GC_11, amp[234]); 
  FFV1_0(w[84], w[26], w[83], pars->GC_11, amp[235]); 
  FFV1_0(w[87], w[69], w[30], pars->GC_11, amp[236]); 
  FFV1_0(w[84], w[79], w[30], pars->GC_11, amp[237]); 
  FFV1_0(w[88], w[69], w[30], pars->GC_11, amp[238]); 
  FFV1_0(w[84], w[81], w[30], pars->GC_11, amp[239]); 
  FFV1_0(w[70], w[79], w[38], pars->GC_11, amp[240]); 
  FFV1_0(w[70], w[23], w[89], pars->GC_11, amp[241]); 
  FFV1_0(w[74], w[4], w[89], pars->GC_11, amp[242]); 
  FFV1_0(w[74], w[69], w[38], pars->GC_11, amp[243]); 
  FFV1_0(w[70], w[81], w[38], pars->GC_11, amp[244]); 
  FFV1_0(w[70], w[26], w[89], pars->GC_11, amp[245]); 
  FFV1_0(w[75], w[4], w[89], pars->GC_11, amp[246]); 
  FFV1_0(w[75], w[69], w[38], pars->GC_11, amp[247]); 
  FFV1_0(w[39], w[4], w[82], pars->GC_11, amp[248]); 
  FFV1_0(w[36], w[23], w[82], pars->GC_11, amp[249]); 
  FFV1_0(w[40], w[4], w[82], pars->GC_11, amp[250]); 
  FFV1_0(w[36], w[26], w[82], pars->GC_11, amp[251]); 
  FFV1_0(w[39], w[69], w[76], pars->GC_11, amp[252]); 
  FFV1_0(w[36], w[79], w[76], pars->GC_11, amp[253]); 
  FFV1_0(w[40], w[69], w[76], pars->GC_11, amp[254]); 
  FFV1_0(w[36], w[81], w[76], pars->GC_11, amp[255]); 
  FFV1_0(w[6], w[23], w[90], pars->GC_11, amp[256]); 
  FFV1_0(w[91], w[23], w[3], pars->GC_11, amp[257]); 
  FFV1_0(w[13], w[4], w[90], pars->GC_11, amp[258]); 
  FFV1_0(w[13], w[92], w[3], pars->GC_11, amp[259]); 
  FFV1_0(w[6], w[26], w[90], pars->GC_11, amp[260]); 
  FFV1_0(w[91], w[26], w[3], pars->GC_11, amp[261]); 
  FFV1_0(w[17], w[4], w[90], pars->GC_11, amp[262]); 
  FFV1_0(w[17], w[92], w[3], pars->GC_11, amp[263]); 
  FFV1_0(w[70], w[23], w[93], pars->GC_11, amp[264]); 
  FFV1_0(w[94], w[23], w[3], pars->GC_11, amp[265]); 
  FFV1_0(w[74], w[4], w[93], pars->GC_11, amp[266]); 
  FFV1_0(w[74], w[95], w[3], pars->GC_11, amp[267]); 
  FFV1_0(w[70], w[26], w[93], pars->GC_11, amp[268]); 
  FFV1_0(w[94], w[26], w[3], pars->GC_11, amp[269]); 
  FFV1_0(w[75], w[4], w[93], pars->GC_11, amp[270]); 
  FFV1_0(w[75], w[95], w[3], pars->GC_11, amp[271]); 
  FFV1_0(w[6], w[79], w[96], pars->GC_11, amp[272]); 
  FFV1_0(w[97], w[79], w[3], pars->GC_11, amp[273]); 
  FFV1_0(w[13], w[69], w[96], pars->GC_11, amp[274]); 
  FFV1_0(w[13], w[98], w[3], pars->GC_11, amp[275]); 
  FFV1_0(w[6], w[81], w[96], pars->GC_11, amp[276]); 
  FFV1_0(w[97], w[81], w[3], pars->GC_11, amp[277]); 
  FFV1_0(w[17], w[69], w[96], pars->GC_11, amp[278]); 
  FFV1_0(w[17], w[98], w[3], pars->GC_11, amp[279]); 
  FFV1_0(w[70], w[79], w[44], pars->GC_11, amp[280]); 
  FFV1_0(w[99], w[79], w[3], pars->GC_11, amp[281]); 
  FFV1_0(w[74], w[69], w[44], pars->GC_11, amp[282]); 
  FFV1_0(w[74], w[100], w[3], pars->GC_11, amp[283]); 
  FFV1_0(w[70], w[81], w[44], pars->GC_11, amp[284]); 
  FFV1_0(w[99], w[81], w[3], pars->GC_11, amp[285]); 
  FFV1_0(w[75], w[69], w[44], pars->GC_11, amp[286]); 
  FFV1_0(w[75], w[100], w[3], pars->GC_11, amp[287]); 
  FFV1_0(w[6], w[61], w[72], pars->GC_11, amp[288]); 
  FFV1_0(w[70], w[61], w[73], pars->GC_11, amp[289]); 
  FFV1_0(w[101], w[4], w[73], pars->GC_11, amp[290]); 
  FFV1_0(w[55], w[4], w[72], pars->GC_11, amp[291]); 
  FFV1_0(w[6], w[62], w[72], pars->GC_11, amp[292]); 
  FFV1_0(w[70], w[62], w[73], pars->GC_11, amp[293]); 
  FFV1_0(w[102], w[4], w[73], pars->GC_11, amp[294]); 
  FFV1_0(w[58], w[4], w[72], pars->GC_11, amp[295]); 
  FFV1_0(w[6], w[103], w[76], pars->GC_11, amp[296]); 
  FFV1_0(w[55], w[71], w[76], pars->GC_11, amp[297]); 
  FFV1_0(w[6], w[104], w[76], pars->GC_11, amp[298]); 
  FFV1_0(w[58], w[71], w[76], pars->GC_11, amp[299]); 
  FFV1_0(w[70], w[103], w[30], pars->GC_11, amp[300]); 
  FFV1_0(w[101], w[71], w[30], pars->GC_11, amp[301]); 
  FFV1_0(w[70], w[104], w[30], pars->GC_11, amp[302]); 
  FFV1_0(w[102], w[71], w[30], pars->GC_11, amp[303]); 
  FFV1_0(w[6], w[105], w[80], pars->GC_11, amp[304]); 
  FFV1_0(w[70], w[105], w[11], pars->GC_11, amp[305]); 
  FFV1_0(w[101], w[69], w[11], pars->GC_11, amp[306]); 
  FFV1_0(w[55], w[69], w[80], pars->GC_11, amp[307]); 
  FFV1_0(w[6], w[106], w[80], pars->GC_11, amp[308]); 
  FFV1_0(w[70], w[106], w[11], pars->GC_11, amp[309]); 
  FFV1_0(w[102], w[69], w[11], pars->GC_11, amp[310]); 
  FFV1_0(w[58], w[69], w[80], pars->GC_11, amp[311]); 
  FFV1_0(w[6], w[59], w[82], pars->GC_11, amp[312]); 
  FFV1_0(w[55], w[8], w[82], pars->GC_11, amp[313]); 
  FFV1_0(w[6], w[60], w[82], pars->GC_11, amp[314]); 
  FFV1_0(w[58], w[8], w[82], pars->GC_11, amp[315]); 
  FFV1_0(w[70], w[59], w[83], pars->GC_11, amp[316]); 
  FFV1_0(w[101], w[8], w[83], pars->GC_11, amp[317]); 
  FFV1_0(w[70], w[60], w[83], pars->GC_11, amp[318]); 
  FFV1_0(w[102], w[8], w[83], pars->GC_11, amp[319]); 
  FFV1_0(w[6], w[105], w[85], pars->GC_11, amp[320]); 
  FFV1_0(w[6], w[61], w[86], pars->GC_11, amp[321]); 
  FFV1_0(w[55], w[4], w[86], pars->GC_11, amp[322]); 
  FFV1_0(w[55], w[69], w[85], pars->GC_11, amp[323]); 
  FFV1_0(w[6], w[106], w[85], pars->GC_11, amp[324]); 
  FFV1_0(w[6], w[62], w[86], pars->GC_11, amp[325]); 
  FFV1_0(w[58], w[4], w[86], pars->GC_11, amp[326]); 
  FFV1_0(w[58], w[69], w[85], pars->GC_11, amp[327]); 
  FFV1_0(w[107], w[4], w[83], pars->GC_11, amp[328]); 
  FFV1_0(w[84], w[61], w[83], pars->GC_11, amp[329]); 
  FFV1_0(w[108], w[4], w[83], pars->GC_11, amp[330]); 
  FFV1_0(w[84], w[62], w[83], pars->GC_11, amp[331]); 
  FFV1_0(w[107], w[69], w[30], pars->GC_11, amp[332]); 
  FFV1_0(w[84], w[105], w[30], pars->GC_11, amp[333]); 
  FFV1_0(w[108], w[69], w[30], pars->GC_11, amp[334]); 
  FFV1_0(w[84], w[106], w[30], pars->GC_11, amp[335]); 
  FFV1_0(w[70], w[105], w[38], pars->GC_11, amp[336]); 
  FFV1_0(w[70], w[61], w[89], pars->GC_11, amp[337]); 
  FFV1_0(w[101], w[4], w[89], pars->GC_11, amp[338]); 
  FFV1_0(w[101], w[69], w[38], pars->GC_11, amp[339]); 
  FFV1_0(w[70], w[106], w[38], pars->GC_11, amp[340]); 
  FFV1_0(w[70], w[62], w[89], pars->GC_11, amp[341]); 
  FFV1_0(w[102], w[4], w[89], pars->GC_11, amp[342]); 
  FFV1_0(w[102], w[69], w[38], pars->GC_11, amp[343]); 
  FFV1_0(w[67], w[4], w[82], pars->GC_11, amp[344]); 
  FFV1_0(w[36], w[61], w[82], pars->GC_11, amp[345]); 
  FFV1_0(w[68], w[4], w[82], pars->GC_11, amp[346]); 
  FFV1_0(w[36], w[62], w[82], pars->GC_11, amp[347]); 
  FFV1_0(w[67], w[69], w[76], pars->GC_11, amp[348]); 
  FFV1_0(w[36], w[105], w[76], pars->GC_11, amp[349]); 
  FFV1_0(w[68], w[69], w[76], pars->GC_11, amp[350]); 
  FFV1_0(w[36], w[106], w[76], pars->GC_11, amp[351]); 
  FFV1_0(w[6], w[61], w[90], pars->GC_11, amp[352]); 
  FFV1_0(w[91], w[61], w[3], pars->GC_11, amp[353]); 
  FFV1_0(w[55], w[4], w[90], pars->GC_11, amp[354]); 
  FFV1_0(w[55], w[92], w[3], pars->GC_11, amp[355]); 
  FFV1_0(w[6], w[62], w[90], pars->GC_11, amp[356]); 
  FFV1_0(w[91], w[62], w[3], pars->GC_11, amp[357]); 
  FFV1_0(w[58], w[4], w[90], pars->GC_11, amp[358]); 
  FFV1_0(w[58], w[92], w[3], pars->GC_11, amp[359]); 
  FFV1_0(w[70], w[61], w[93], pars->GC_11, amp[360]); 
  FFV1_0(w[94], w[61], w[3], pars->GC_11, amp[361]); 
  FFV1_0(w[101], w[4], w[93], pars->GC_11, amp[362]); 
  FFV1_0(w[101], w[95], w[3], pars->GC_11, amp[363]); 
  FFV1_0(w[70], w[62], w[93], pars->GC_11, amp[364]); 
  FFV1_0(w[94], w[62], w[3], pars->GC_11, amp[365]); 
  FFV1_0(w[102], w[4], w[93], pars->GC_11, amp[366]); 
  FFV1_0(w[102], w[95], w[3], pars->GC_11, amp[367]); 
  FFV1_0(w[6], w[105], w[96], pars->GC_11, amp[368]); 
  FFV1_0(w[97], w[105], w[3], pars->GC_11, amp[369]); 
  FFV1_0(w[55], w[69], w[96], pars->GC_11, amp[370]); 
  FFV1_0(w[55], w[98], w[3], pars->GC_11, amp[371]); 
  FFV1_0(w[6], w[106], w[96], pars->GC_11, amp[372]); 
  FFV1_0(w[97], w[106], w[3], pars->GC_11, amp[373]); 
  FFV1_0(w[58], w[69], w[96], pars->GC_11, amp[374]); 
  FFV1_0(w[58], w[98], w[3], pars->GC_11, amp[375]); 
  FFV1_0(w[70], w[105], w[44], pars->GC_11, amp[376]); 
  FFV1_0(w[99], w[105], w[3], pars->GC_11, amp[377]); 
  FFV1_0(w[101], w[69], w[44], pars->GC_11, amp[378]); 
  FFV1_0(w[101], w[100], w[3], pars->GC_11, amp[379]); 
  FFV1_0(w[70], w[106], w[44], pars->GC_11, amp[380]); 
  FFV1_0(w[99], w[106], w[3], pars->GC_11, amp[381]); 
  FFV1_0(w[102], w[69], w[44], pars->GC_11, amp[382]); 
  FFV1_0(w[102], w[100], w[3], pars->GC_11, amp[383]); 
  FFV1_0(w[6], w[112], w[10], pars->GC_11, amp[384]); 
  FFV1_0(w[1], w[112], w[11], pars->GC_11, amp[385]); 
  FFV1_0(w[113], w[5], w[11], pars->GC_11, amp[386]); 
  FFV1_0(w[114], w[5], w[10], pars->GC_11, amp[387]); 
  FFV1_0(w[6], w[116], w[10], pars->GC_11, amp[388]); 
  FFV1_0(w[1], w[116], w[11], pars->GC_11, amp[389]); 
  FFV1_0(w[117], w[5], w[11], pars->GC_11, amp[390]); 
  FFV1_0(w[118], w[5], w[10], pars->GC_11, amp[391]); 
  FFV1_0(w[6], w[119], w[18], pars->GC_11, amp[392]); 
  FFV1_0(w[114], w[8], w[18], pars->GC_11, amp[393]); 
  FFV1_0(w[6], w[120], w[18], pars->GC_11, amp[394]); 
  FFV1_0(w[118], w[8], w[18], pars->GC_11, amp[395]); 
  FFV1_0(w[1], w[119], w[21], pars->GC_11, amp[396]); 
  FFV1_0(w[113], w[8], w[21], pars->GC_11, amp[397]); 
  FFV1_0(w[1], w[120], w[21], pars->GC_11, amp[398]); 
  FFV1_0(w[117], w[8], w[21], pars->GC_11, amp[399]); 
  FFV1_0(w[6], w[121], w[24], pars->GC_11, amp[400]); 
  FFV1_0(w[1], w[121], w[25], pars->GC_11, amp[401]); 
  FFV1_0(w[113], w[4], w[25], pars->GC_11, amp[402]); 
  FFV1_0(w[114], w[4], w[24], pars->GC_11, amp[403]); 
  FFV1_0(w[6], w[122], w[24], pars->GC_11, amp[404]); 
  FFV1_0(w[1], w[122], w[25], pars->GC_11, amp[405]); 
  FFV1_0(w[117], w[4], w[25], pars->GC_11, amp[406]); 
  FFV1_0(w[118], w[4], w[24], pars->GC_11, amp[407]); 
  FFV1_0(w[6], w[123], w[27], pars->GC_11, amp[408]); 
  FFV1_0(w[114], w[22], w[27], pars->GC_11, amp[409]); 
  FFV1_0(w[6], w[124], w[27], pars->GC_11, amp[410]); 
  FFV1_0(w[118], w[22], w[27], pars->GC_11, amp[411]); 
  FFV1_0(w[1], w[123], w[30], pars->GC_11, amp[412]); 
  FFV1_0(w[113], w[22], w[30], pars->GC_11, amp[413]); 
  FFV1_0(w[1], w[124], w[30], pars->GC_11, amp[414]); 
  FFV1_0(w[117], w[22], w[30], pars->GC_11, amp[415]); 
  FFV1_0(w[6], w[121], w[32], pars->GC_11, amp[416]); 
  FFV1_0(w[6], w[112], w[33], pars->GC_11, amp[417]); 
  FFV1_0(w[114], w[5], w[33], pars->GC_11, amp[418]); 
  FFV1_0(w[114], w[4], w[32], pars->GC_11, amp[419]); 
  FFV1_0(w[6], w[122], w[32], pars->GC_11, amp[420]); 
  FFV1_0(w[6], w[116], w[33], pars->GC_11, amp[421]); 
  FFV1_0(w[118], w[5], w[33], pars->GC_11, amp[422]); 
  FFV1_0(w[118], w[4], w[32], pars->GC_11, amp[423]); 
  FFV1_0(w[125], w[5], w[30], pars->GC_11, amp[424]); 
  FFV1_0(w[31], w[112], w[30], pars->GC_11, amp[425]); 
  FFV1_0(w[126], w[5], w[30], pars->GC_11, amp[426]); 
  FFV1_0(w[31], w[116], w[30], pars->GC_11, amp[427]); 
  FFV1_0(w[125], w[4], w[21], pars->GC_11, amp[428]); 
  FFV1_0(w[31], w[121], w[21], pars->GC_11, amp[429]); 
  FFV1_0(w[126], w[4], w[21], pars->GC_11, amp[430]); 
  FFV1_0(w[31], w[122], w[21], pars->GC_11, amp[431]); 
  FFV1_0(w[1], w[121], w[37], pars->GC_11, amp[432]); 
  FFV1_0(w[1], w[112], w[38], pars->GC_11, amp[433]); 
  FFV1_0(w[113], w[5], w[38], pars->GC_11, amp[434]); 
  FFV1_0(w[113], w[4], w[37], pars->GC_11, amp[435]); 
  FFV1_0(w[1], w[122], w[37], pars->GC_11, amp[436]); 
  FFV1_0(w[1], w[116], w[38], pars->GC_11, amp[437]); 
  FFV1_0(w[117], w[5], w[38], pars->GC_11, amp[438]); 
  FFV1_0(w[117], w[4], w[37], pars->GC_11, amp[439]); 
  FFV1_0(w[127], w[5], w[27], pars->GC_11, amp[440]); 
  FFV1_0(w[36], w[112], w[27], pars->GC_11, amp[441]); 
  FFV1_0(w[128], w[5], w[27], pars->GC_11, amp[442]); 
  FFV1_0(w[36], w[116], w[27], pars->GC_11, amp[443]); 
  FFV1_0(w[127], w[4], w[18], pars->GC_11, amp[444]); 
  FFV1_0(w[36], w[121], w[18], pars->GC_11, amp[445]); 
  FFV1_0(w[128], w[4], w[18], pars->GC_11, amp[446]); 
  FFV1_0(w[36], w[122], w[18], pars->GC_11, amp[447]); 
  FFV1_0(w[6], w[112], w[41], pars->GC_11, amp[448]); 
  FFV1_0(w[42], w[112], w[3], pars->GC_11, amp[449]); 
  FFV1_0(w[114], w[5], w[41], pars->GC_11, amp[450]); 
  FFV1_0(w[114], w[43], w[3], pars->GC_11, amp[451]); 
  FFV1_0(w[6], w[116], w[41], pars->GC_11, amp[452]); 
  FFV1_0(w[42], w[116], w[3], pars->GC_11, amp[453]); 
  FFV1_0(w[118], w[5], w[41], pars->GC_11, amp[454]); 
  FFV1_0(w[118], w[43], w[3], pars->GC_11, amp[455]); 
  FFV1_0(w[1], w[112], w[44], pars->GC_11, amp[456]); 
  FFV1_0(w[45], w[112], w[3], pars->GC_11, amp[457]); 
  FFV1_0(w[113], w[5], w[44], pars->GC_11, amp[458]); 
  FFV1_0(w[113], w[46], w[3], pars->GC_11, amp[459]); 
  FFV1_0(w[1], w[116], w[44], pars->GC_11, amp[460]); 
  FFV1_0(w[45], w[116], w[3], pars->GC_11, amp[461]); 
  FFV1_0(w[117], w[5], w[44], pars->GC_11, amp[462]); 
  FFV1_0(w[117], w[46], w[3], pars->GC_11, amp[463]); 
  FFV1_0(w[6], w[121], w[47], pars->GC_11, amp[464]); 
  FFV1_0(w[48], w[121], w[3], pars->GC_11, amp[465]); 
  FFV1_0(w[114], w[4], w[47], pars->GC_11, amp[466]); 
  FFV1_0(w[114], w[49], w[3], pars->GC_11, amp[467]); 
  FFV1_0(w[6], w[122], w[47], pars->GC_11, amp[468]); 
  FFV1_0(w[48], w[122], w[3], pars->GC_11, amp[469]); 
  FFV1_0(w[118], w[4], w[47], pars->GC_11, amp[470]); 
  FFV1_0(w[118], w[49], w[3], pars->GC_11, amp[471]); 
  FFV1_0(w[1], w[121], w[50], pars->GC_11, amp[472]); 
  FFV1_0(w[51], w[121], w[3], pars->GC_11, amp[473]); 
  FFV1_0(w[113], w[4], w[50], pars->GC_11, amp[474]); 
  FFV1_0(w[113], w[52], w[3], pars->GC_11, amp[475]); 
  FFV1_0(w[1], w[122], w[50], pars->GC_11, amp[476]); 
  FFV1_0(w[51], w[122], w[3], pars->GC_11, amp[477]); 
  FFV1_0(w[117], w[4], w[50], pars->GC_11, amp[478]); 
  FFV1_0(w[117], w[52], w[3], pars->GC_11, amp[479]); 
  FFV1_0(w[6], w[129], w[10], pars->GC_11, amp[480]); 
  FFV1_0(w[1], w[129], w[11], pars->GC_11, amp[481]); 
  FFV1_0(w[130], w[5], w[11], pars->GC_11, amp[482]); 
  FFV1_0(w[131], w[5], w[10], pars->GC_11, amp[483]); 
  FFV1_0(w[6], w[132], w[10], pars->GC_11, amp[484]); 
  FFV1_0(w[1], w[132], w[11], pars->GC_11, amp[485]); 
  FFV1_0(w[133], w[5], w[11], pars->GC_11, amp[486]); 
  FFV1_0(w[134], w[5], w[10], pars->GC_11, amp[487]); 
  FFV1_0(w[6], w[135], w[18], pars->GC_11, amp[488]); 
  FFV1_0(w[131], w[8], w[18], pars->GC_11, amp[489]); 
  FFV1_0(w[6], w[136], w[18], pars->GC_11, amp[490]); 
  FFV1_0(w[134], w[8], w[18], pars->GC_11, amp[491]); 
  FFV1_0(w[1], w[135], w[21], pars->GC_11, amp[492]); 
  FFV1_0(w[130], w[8], w[21], pars->GC_11, amp[493]); 
  FFV1_0(w[1], w[136], w[21], pars->GC_11, amp[494]); 
  FFV1_0(w[133], w[8], w[21], pars->GC_11, amp[495]); 
  FFV1_0(w[6], w[137], w[24], pars->GC_11, amp[496]); 
  FFV1_0(w[1], w[137], w[25], pars->GC_11, amp[497]); 
  FFV1_0(w[130], w[4], w[25], pars->GC_11, amp[498]); 
  FFV1_0(w[131], w[4], w[24], pars->GC_11, amp[499]); 
  FFV1_0(w[6], w[138], w[24], pars->GC_11, amp[500]); 
  FFV1_0(w[1], w[138], w[25], pars->GC_11, amp[501]); 
  FFV1_0(w[133], w[4], w[25], pars->GC_11, amp[502]); 
  FFV1_0(w[134], w[4], w[24], pars->GC_11, amp[503]); 
  FFV1_0(w[6], w[139], w[27], pars->GC_11, amp[504]); 
  FFV1_0(w[131], w[22], w[27], pars->GC_11, amp[505]); 
  FFV1_0(w[6], w[140], w[27], pars->GC_11, amp[506]); 
  FFV1_0(w[134], w[22], w[27], pars->GC_11, amp[507]); 
  FFV1_0(w[1], w[139], w[30], pars->GC_11, amp[508]); 
  FFV1_0(w[130], w[22], w[30], pars->GC_11, amp[509]); 
  FFV1_0(w[1], w[140], w[30], pars->GC_11, amp[510]); 
  FFV1_0(w[133], w[22], w[30], pars->GC_11, amp[511]); 
  FFV1_0(w[6], w[137], w[32], pars->GC_11, amp[512]); 
  FFV1_0(w[6], w[129], w[33], pars->GC_11, amp[513]); 
  FFV1_0(w[131], w[5], w[33], pars->GC_11, amp[514]); 
  FFV1_0(w[131], w[4], w[32], pars->GC_11, amp[515]); 
  FFV1_0(w[6], w[138], w[32], pars->GC_11, amp[516]); 
  FFV1_0(w[6], w[132], w[33], pars->GC_11, amp[517]); 
  FFV1_0(w[134], w[5], w[33], pars->GC_11, amp[518]); 
  FFV1_0(w[134], w[4], w[32], pars->GC_11, amp[519]); 
  FFV1_0(w[141], w[5], w[30], pars->GC_11, amp[520]); 
  FFV1_0(w[31], w[129], w[30], pars->GC_11, amp[521]); 
  FFV1_0(w[142], w[5], w[30], pars->GC_11, amp[522]); 
  FFV1_0(w[31], w[132], w[30], pars->GC_11, amp[523]); 
  FFV1_0(w[141], w[4], w[21], pars->GC_11, amp[524]); 
  FFV1_0(w[31], w[137], w[21], pars->GC_11, amp[525]); 
  FFV1_0(w[142], w[4], w[21], pars->GC_11, amp[526]); 
  FFV1_0(w[31], w[138], w[21], pars->GC_11, amp[527]); 
  FFV1_0(w[1], w[137], w[37], pars->GC_11, amp[528]); 
  FFV1_0(w[1], w[129], w[38], pars->GC_11, amp[529]); 
  FFV1_0(w[130], w[5], w[38], pars->GC_11, amp[530]); 
  FFV1_0(w[130], w[4], w[37], pars->GC_11, amp[531]); 
  FFV1_0(w[1], w[138], w[37], pars->GC_11, amp[532]); 
  FFV1_0(w[1], w[132], w[38], pars->GC_11, amp[533]); 
  FFV1_0(w[133], w[5], w[38], pars->GC_11, amp[534]); 
  FFV1_0(w[133], w[4], w[37], pars->GC_11, amp[535]); 
  FFV1_0(w[143], w[5], w[27], pars->GC_11, amp[536]); 
  FFV1_0(w[36], w[129], w[27], pars->GC_11, amp[537]); 
  FFV1_0(w[144], w[5], w[27], pars->GC_11, amp[538]); 
  FFV1_0(w[36], w[132], w[27], pars->GC_11, amp[539]); 
  FFV1_0(w[143], w[4], w[18], pars->GC_11, amp[540]); 
  FFV1_0(w[36], w[137], w[18], pars->GC_11, amp[541]); 
  FFV1_0(w[144], w[4], w[18], pars->GC_11, amp[542]); 
  FFV1_0(w[36], w[138], w[18], pars->GC_11, amp[543]); 
  FFV1_0(w[6], w[129], w[41], pars->GC_11, amp[544]); 
  FFV1_0(w[42], w[129], w[3], pars->GC_11, amp[545]); 
  FFV1_0(w[131], w[5], w[41], pars->GC_11, amp[546]); 
  FFV1_0(w[131], w[43], w[3], pars->GC_11, amp[547]); 
  FFV1_0(w[6], w[132], w[41], pars->GC_11, amp[548]); 
  FFV1_0(w[42], w[132], w[3], pars->GC_11, amp[549]); 
  FFV1_0(w[134], w[5], w[41], pars->GC_11, amp[550]); 
  FFV1_0(w[134], w[43], w[3], pars->GC_11, amp[551]); 
  FFV1_0(w[1], w[129], w[44], pars->GC_11, amp[552]); 
  FFV1_0(w[45], w[129], w[3], pars->GC_11, amp[553]); 
  FFV1_0(w[130], w[5], w[44], pars->GC_11, amp[554]); 
  FFV1_0(w[130], w[46], w[3], pars->GC_11, amp[555]); 
  FFV1_0(w[1], w[132], w[44], pars->GC_11, amp[556]); 
  FFV1_0(w[45], w[132], w[3], pars->GC_11, amp[557]); 
  FFV1_0(w[133], w[5], w[44], pars->GC_11, amp[558]); 
  FFV1_0(w[133], w[46], w[3], pars->GC_11, amp[559]); 
  FFV1_0(w[6], w[137], w[47], pars->GC_11, amp[560]); 
  FFV1_0(w[48], w[137], w[3], pars->GC_11, amp[561]); 
  FFV1_0(w[131], w[4], w[47], pars->GC_11, amp[562]); 
  FFV1_0(w[131], w[49], w[3], pars->GC_11, amp[563]); 
  FFV1_0(w[6], w[138], w[47], pars->GC_11, amp[564]); 
  FFV1_0(w[48], w[138], w[3], pars->GC_11, amp[565]); 
  FFV1_0(w[134], w[4], w[47], pars->GC_11, amp[566]); 
  FFV1_0(w[134], w[49], w[3], pars->GC_11, amp[567]); 
  FFV1_0(w[1], w[137], w[50], pars->GC_11, amp[568]); 
  FFV1_0(w[51], w[137], w[3], pars->GC_11, amp[569]); 
  FFV1_0(w[130], w[4], w[50], pars->GC_11, amp[570]); 
  FFV1_0(w[130], w[52], w[3], pars->GC_11, amp[571]); 
  FFV1_0(w[1], w[138], w[50], pars->GC_11, amp[572]); 
  FFV1_0(w[51], w[138], w[3], pars->GC_11, amp[573]); 
  FFV1_0(w[133], w[4], w[50], pars->GC_11, amp[574]); 
  FFV1_0(w[133], w[52], w[3], pars->GC_11, amp[575]); 
  FFV1_0(w[6], w[121], w[72], pars->GC_11, amp[576]); 
  FFV1_0(w[70], w[121], w[73], pars->GC_11, amp[577]); 
  FFV1_0(w[145], w[4], w[73], pars->GC_11, amp[578]); 
  FFV1_0(w[114], w[4], w[72], pars->GC_11, amp[579]); 
  FFV1_0(w[6], w[122], w[72], pars->GC_11, amp[580]); 
  FFV1_0(w[70], w[122], w[73], pars->GC_11, amp[581]); 
  FFV1_0(w[146], w[4], w[73], pars->GC_11, amp[582]); 
  FFV1_0(w[118], w[4], w[72], pars->GC_11, amp[583]); 
  FFV1_0(w[6], w[147], w[76], pars->GC_11, amp[584]); 
  FFV1_0(w[114], w[71], w[76], pars->GC_11, amp[585]); 
  FFV1_0(w[6], w[148], w[76], pars->GC_11, amp[586]); 
  FFV1_0(w[118], w[71], w[76], pars->GC_11, amp[587]); 
  FFV1_0(w[70], w[147], w[30], pars->GC_11, amp[588]); 
  FFV1_0(w[145], w[71], w[30], pars->GC_11, amp[589]); 
  FFV1_0(w[70], w[148], w[30], pars->GC_11, amp[590]); 
  FFV1_0(w[146], w[71], w[30], pars->GC_11, amp[591]); 
  FFV1_0(w[6], w[149], w[80], pars->GC_11, amp[592]); 
  FFV1_0(w[70], w[149], w[11], pars->GC_11, amp[593]); 
  FFV1_0(w[145], w[69], w[11], pars->GC_11, amp[594]); 
  FFV1_0(w[114], w[69], w[80], pars->GC_11, amp[595]); 
  FFV1_0(w[6], w[150], w[80], pars->GC_11, amp[596]); 
  FFV1_0(w[70], w[150], w[11], pars->GC_11, amp[597]); 
  FFV1_0(w[146], w[69], w[11], pars->GC_11, amp[598]); 
  FFV1_0(w[118], w[69], w[80], pars->GC_11, amp[599]); 
  FFV1_0(w[6], w[119], w[82], pars->GC_11, amp[600]); 
  FFV1_0(w[114], w[8], w[82], pars->GC_11, amp[601]); 
  FFV1_0(w[6], w[120], w[82], pars->GC_11, amp[602]); 
  FFV1_0(w[118], w[8], w[82], pars->GC_11, amp[603]); 
  FFV1_0(w[70], w[119], w[83], pars->GC_11, amp[604]); 
  FFV1_0(w[145], w[8], w[83], pars->GC_11, amp[605]); 
  FFV1_0(w[70], w[120], w[83], pars->GC_11, amp[606]); 
  FFV1_0(w[146], w[8], w[83], pars->GC_11, amp[607]); 
  FFV1_0(w[6], w[149], w[85], pars->GC_11, amp[608]); 
  FFV1_0(w[6], w[121], w[86], pars->GC_11, amp[609]); 
  FFV1_0(w[114], w[4], w[86], pars->GC_11, amp[610]); 
  FFV1_0(w[114], w[69], w[85], pars->GC_11, amp[611]); 
  FFV1_0(w[6], w[150], w[85], pars->GC_11, amp[612]); 
  FFV1_0(w[6], w[122], w[86], pars->GC_11, amp[613]); 
  FFV1_0(w[118], w[4], w[86], pars->GC_11, amp[614]); 
  FFV1_0(w[118], w[69], w[85], pars->GC_11, amp[615]); 
  FFV1_0(w[151], w[4], w[83], pars->GC_11, amp[616]); 
  FFV1_0(w[84], w[121], w[83], pars->GC_11, amp[617]); 
  FFV1_0(w[152], w[4], w[83], pars->GC_11, amp[618]); 
  FFV1_0(w[84], w[122], w[83], pars->GC_11, amp[619]); 
  FFV1_0(w[151], w[69], w[30], pars->GC_11, amp[620]); 
  FFV1_0(w[84], w[149], w[30], pars->GC_11, amp[621]); 
  FFV1_0(w[152], w[69], w[30], pars->GC_11, amp[622]); 
  FFV1_0(w[84], w[150], w[30], pars->GC_11, amp[623]); 
  FFV1_0(w[70], w[149], w[38], pars->GC_11, amp[624]); 
  FFV1_0(w[70], w[121], w[89], pars->GC_11, amp[625]); 
  FFV1_0(w[145], w[4], w[89], pars->GC_11, amp[626]); 
  FFV1_0(w[145], w[69], w[38], pars->GC_11, amp[627]); 
  FFV1_0(w[70], w[150], w[38], pars->GC_11, amp[628]); 
  FFV1_0(w[70], w[122], w[89], pars->GC_11, amp[629]); 
  FFV1_0(w[146], w[4], w[89], pars->GC_11, amp[630]); 
  FFV1_0(w[146], w[69], w[38], pars->GC_11, amp[631]); 
  FFV1_0(w[127], w[4], w[82], pars->GC_11, amp[632]); 
  FFV1_0(w[36], w[121], w[82], pars->GC_11, amp[633]); 
  FFV1_0(w[128], w[4], w[82], pars->GC_11, amp[634]); 
  FFV1_0(w[36], w[122], w[82], pars->GC_11, amp[635]); 
  FFV1_0(w[127], w[69], w[76], pars->GC_11, amp[636]); 
  FFV1_0(w[36], w[149], w[76], pars->GC_11, amp[637]); 
  FFV1_0(w[128], w[69], w[76], pars->GC_11, amp[638]); 
  FFV1_0(w[36], w[150], w[76], pars->GC_11, amp[639]); 
  FFV1_0(w[6], w[121], w[90], pars->GC_11, amp[640]); 
  FFV1_0(w[91], w[121], w[3], pars->GC_11, amp[641]); 
  FFV1_0(w[114], w[4], w[90], pars->GC_11, amp[642]); 
  FFV1_0(w[114], w[92], w[3], pars->GC_11, amp[643]); 
  FFV1_0(w[6], w[122], w[90], pars->GC_11, amp[644]); 
  FFV1_0(w[91], w[122], w[3], pars->GC_11, amp[645]); 
  FFV1_0(w[118], w[4], w[90], pars->GC_11, amp[646]); 
  FFV1_0(w[118], w[92], w[3], pars->GC_11, amp[647]); 
  FFV1_0(w[70], w[121], w[93], pars->GC_11, amp[648]); 
  FFV1_0(w[94], w[121], w[3], pars->GC_11, amp[649]); 
  FFV1_0(w[145], w[4], w[93], pars->GC_11, amp[650]); 
  FFV1_0(w[145], w[95], w[3], pars->GC_11, amp[651]); 
  FFV1_0(w[70], w[122], w[93], pars->GC_11, amp[652]); 
  FFV1_0(w[94], w[122], w[3], pars->GC_11, amp[653]); 
  FFV1_0(w[146], w[4], w[93], pars->GC_11, amp[654]); 
  FFV1_0(w[146], w[95], w[3], pars->GC_11, amp[655]); 
  FFV1_0(w[6], w[149], w[96], pars->GC_11, amp[656]); 
  FFV1_0(w[97], w[149], w[3], pars->GC_11, amp[657]); 
  FFV1_0(w[114], w[69], w[96], pars->GC_11, amp[658]); 
  FFV1_0(w[114], w[98], w[3], pars->GC_11, amp[659]); 
  FFV1_0(w[6], w[150], w[96], pars->GC_11, amp[660]); 
  FFV1_0(w[97], w[150], w[3], pars->GC_11, amp[661]); 
  FFV1_0(w[118], w[69], w[96], pars->GC_11, amp[662]); 
  FFV1_0(w[118], w[98], w[3], pars->GC_11, amp[663]); 
  FFV1_0(w[70], w[149], w[44], pars->GC_11, amp[664]); 
  FFV1_0(w[99], w[149], w[3], pars->GC_11, amp[665]); 
  FFV1_0(w[145], w[69], w[44], pars->GC_11, amp[666]); 
  FFV1_0(w[145], w[100], w[3], pars->GC_11, amp[667]); 
  FFV1_0(w[70], w[150], w[44], pars->GC_11, amp[668]); 
  FFV1_0(w[99], w[150], w[3], pars->GC_11, amp[669]); 
  FFV1_0(w[146], w[69], w[44], pars->GC_11, amp[670]); 
  FFV1_0(w[146], w[100], w[3], pars->GC_11, amp[671]); 
  FFV1_0(w[6], w[137], w[72], pars->GC_11, amp[672]); 
  FFV1_0(w[70], w[137], w[73], pars->GC_11, amp[673]); 
  FFV1_0(w[153], w[4], w[73], pars->GC_11, amp[674]); 
  FFV1_0(w[131], w[4], w[72], pars->GC_11, amp[675]); 
  FFV1_0(w[6], w[138], w[72], pars->GC_11, amp[676]); 
  FFV1_0(w[70], w[138], w[73], pars->GC_11, amp[677]); 
  FFV1_0(w[154], w[4], w[73], pars->GC_11, amp[678]); 
  FFV1_0(w[134], w[4], w[72], pars->GC_11, amp[679]); 
  FFV1_0(w[6], w[155], w[76], pars->GC_11, amp[680]); 
  FFV1_0(w[131], w[71], w[76], pars->GC_11, amp[681]); 
  FFV1_0(w[6], w[156], w[76], pars->GC_11, amp[682]); 
  FFV1_0(w[134], w[71], w[76], pars->GC_11, amp[683]); 
  FFV1_0(w[70], w[155], w[30], pars->GC_11, amp[684]); 
  FFV1_0(w[153], w[71], w[30], pars->GC_11, amp[685]); 
  FFV1_0(w[70], w[156], w[30], pars->GC_11, amp[686]); 
  FFV1_0(w[154], w[71], w[30], pars->GC_11, amp[687]); 
  FFV1_0(w[6], w[157], w[80], pars->GC_11, amp[688]); 
  FFV1_0(w[70], w[157], w[11], pars->GC_11, amp[689]); 
  FFV1_0(w[153], w[69], w[11], pars->GC_11, amp[690]); 
  FFV1_0(w[131], w[69], w[80], pars->GC_11, amp[691]); 
  FFV1_0(w[6], w[158], w[80], pars->GC_11, amp[692]); 
  FFV1_0(w[70], w[158], w[11], pars->GC_11, amp[693]); 
  FFV1_0(w[154], w[69], w[11], pars->GC_11, amp[694]); 
  FFV1_0(w[134], w[69], w[80], pars->GC_11, amp[695]); 
  FFV1_0(w[6], w[135], w[82], pars->GC_11, amp[696]); 
  FFV1_0(w[131], w[8], w[82], pars->GC_11, amp[697]); 
  FFV1_0(w[6], w[136], w[82], pars->GC_11, amp[698]); 
  FFV1_0(w[134], w[8], w[82], pars->GC_11, amp[699]); 
  FFV1_0(w[70], w[135], w[83], pars->GC_11, amp[700]); 
  FFV1_0(w[153], w[8], w[83], pars->GC_11, amp[701]); 
  FFV1_0(w[70], w[136], w[83], pars->GC_11, amp[702]); 
  FFV1_0(w[154], w[8], w[83], pars->GC_11, amp[703]); 
  FFV1_0(w[6], w[157], w[85], pars->GC_11, amp[704]); 
  FFV1_0(w[6], w[137], w[86], pars->GC_11, amp[705]); 
  FFV1_0(w[131], w[4], w[86], pars->GC_11, amp[706]); 
  FFV1_0(w[131], w[69], w[85], pars->GC_11, amp[707]); 
  FFV1_0(w[6], w[158], w[85], pars->GC_11, amp[708]); 
  FFV1_0(w[6], w[138], w[86], pars->GC_11, amp[709]); 
  FFV1_0(w[134], w[4], w[86], pars->GC_11, amp[710]); 
  FFV1_0(w[134], w[69], w[85], pars->GC_11, amp[711]); 
  FFV1_0(w[159], w[4], w[83], pars->GC_11, amp[712]); 
  FFV1_0(w[84], w[137], w[83], pars->GC_11, amp[713]); 
  FFV1_0(w[160], w[4], w[83], pars->GC_11, amp[714]); 
  FFV1_0(w[84], w[138], w[83], pars->GC_11, amp[715]); 
  FFV1_0(w[159], w[69], w[30], pars->GC_11, amp[716]); 
  FFV1_0(w[84], w[157], w[30], pars->GC_11, amp[717]); 
  FFV1_0(w[160], w[69], w[30], pars->GC_11, amp[718]); 
  FFV1_0(w[84], w[158], w[30], pars->GC_11, amp[719]); 
  FFV1_0(w[70], w[157], w[38], pars->GC_11, amp[720]); 
  FFV1_0(w[70], w[137], w[89], pars->GC_11, amp[721]); 
  FFV1_0(w[153], w[4], w[89], pars->GC_11, amp[722]); 
  FFV1_0(w[153], w[69], w[38], pars->GC_11, amp[723]); 
  FFV1_0(w[70], w[158], w[38], pars->GC_11, amp[724]); 
  FFV1_0(w[70], w[138], w[89], pars->GC_11, amp[725]); 
  FFV1_0(w[154], w[4], w[89], pars->GC_11, amp[726]); 
  FFV1_0(w[154], w[69], w[38], pars->GC_11, amp[727]); 
  FFV1_0(w[143], w[4], w[82], pars->GC_11, amp[728]); 
  FFV1_0(w[36], w[137], w[82], pars->GC_11, amp[729]); 
  FFV1_0(w[144], w[4], w[82], pars->GC_11, amp[730]); 
  FFV1_0(w[36], w[138], w[82], pars->GC_11, amp[731]); 
  FFV1_0(w[143], w[69], w[76], pars->GC_11, amp[732]); 
  FFV1_0(w[36], w[157], w[76], pars->GC_11, amp[733]); 
  FFV1_0(w[144], w[69], w[76], pars->GC_11, amp[734]); 
  FFV1_0(w[36], w[158], w[76], pars->GC_11, amp[735]); 
  FFV1_0(w[6], w[137], w[90], pars->GC_11, amp[736]); 
  FFV1_0(w[91], w[137], w[3], pars->GC_11, amp[737]); 
  FFV1_0(w[131], w[4], w[90], pars->GC_11, amp[738]); 
  FFV1_0(w[131], w[92], w[3], pars->GC_11, amp[739]); 
  FFV1_0(w[6], w[138], w[90], pars->GC_11, amp[740]); 
  FFV1_0(w[91], w[138], w[3], pars->GC_11, amp[741]); 
  FFV1_0(w[134], w[4], w[90], pars->GC_11, amp[742]); 
  FFV1_0(w[134], w[92], w[3], pars->GC_11, amp[743]); 
  FFV1_0(w[70], w[137], w[93], pars->GC_11, amp[744]); 
  FFV1_0(w[94], w[137], w[3], pars->GC_11, amp[745]); 
  FFV1_0(w[153], w[4], w[93], pars->GC_11, amp[746]); 
  FFV1_0(w[153], w[95], w[3], pars->GC_11, amp[747]); 
  FFV1_0(w[70], w[138], w[93], pars->GC_11, amp[748]); 
  FFV1_0(w[94], w[138], w[3], pars->GC_11, amp[749]); 
  FFV1_0(w[154], w[4], w[93], pars->GC_11, amp[750]); 
  FFV1_0(w[154], w[95], w[3], pars->GC_11, amp[751]); 
  FFV1_0(w[6], w[157], w[96], pars->GC_11, amp[752]); 
  FFV1_0(w[97], w[157], w[3], pars->GC_11, amp[753]); 
  FFV1_0(w[131], w[69], w[96], pars->GC_11, amp[754]); 
  FFV1_0(w[131], w[98], w[3], pars->GC_11, amp[755]); 
  FFV1_0(w[6], w[158], w[96], pars->GC_11, amp[756]); 
  FFV1_0(w[97], w[158], w[3], pars->GC_11, amp[757]); 
  FFV1_0(w[134], w[69], w[96], pars->GC_11, amp[758]); 
  FFV1_0(w[134], w[98], w[3], pars->GC_11, amp[759]); 
  FFV1_0(w[70], w[157], w[44], pars->GC_11, amp[760]); 
  FFV1_0(w[99], w[157], w[3], pars->GC_11, amp[761]); 
  FFV1_0(w[153], w[69], w[44], pars->GC_11, amp[762]); 
  FFV1_0(w[153], w[100], w[3], pars->GC_11, amp[763]); 
  FFV1_0(w[70], w[158], w[44], pars->GC_11, amp[764]); 
  FFV1_0(w[99], w[158], w[3], pars->GC_11, amp[765]); 
  FFV1_0(w[154], w[69], w[44], pars->GC_11, amp[766]); 
  FFV1_0(w[154], w[100], w[3], pars->GC_11, amp[767]); 
  FFV1_0(w[13], w[5], w[10], pars->GC_11, amp[768]); 
  FFV1_0(w[6], w[15], w[10], pars->GC_11, amp[769]); 
  FFV1_0(w[17], w[5], w[10], pars->GC_11, amp[770]); 
  FFV1_0(w[1], w[19], w[21], pars->GC_11, amp[771]); 
  FFV1_0(w[12], w[8], w[21], pars->GC_11, amp[772]); 
  FFV1_0(w[1], w[20], w[21], pars->GC_11, amp[773]); 
  FFV1_0(w[16], w[8], w[21], pars->GC_11, amp[774]); 
  FFV1_0(w[1], w[23], w[25], pars->GC_11, amp[775]); 
  FFV1_0(w[12], w[4], w[25], pars->GC_11, amp[776]); 
  FFV1_0(w[1], w[26], w[25], pars->GC_11, amp[777]); 
  FFV1_0(w[16], w[4], w[25], pars->GC_11, amp[778]); 
  FFV1_0(w[6], w[28], w[27], pars->GC_11, amp[779]); 
  FFV1_0(w[13], w[22], w[27], pars->GC_11, amp[780]); 
  FFV1_0(w[6], w[29], w[27], pars->GC_11, amp[781]); 
  FFV1_0(w[17], w[22], w[27], pars->GC_11, amp[782]); 
  FFV1_0(w[6], w[9], w[33], pars->GC_11, amp[783]); 
  FFV1_0(w[13], w[5], w[33], pars->GC_11, amp[784]); 
  FFV1_0(w[6], w[15], w[33], pars->GC_11, amp[785]); 
  FFV1_0(w[17], w[5], w[33], pars->GC_11, amp[786]); 
  FFV1_0(w[34], w[4], w[21], pars->GC_11, amp[787]); 
  FFV1_0(w[31], w[23], w[21], pars->GC_11, amp[788]); 
  FFV1_0(w[35], w[4], w[21], pars->GC_11, amp[789]); 
  FFV1_0(w[31], w[26], w[21], pars->GC_11, amp[790]); 
  FFV1_0(w[1], w[23], w[37], pars->GC_11, amp[791]); 
  FFV1_0(w[12], w[4], w[37], pars->GC_11, amp[792]); 
  FFV1_0(w[1], w[26], w[37], pars->GC_11, amp[793]); 
  FFV1_0(w[16], w[4], w[37], pars->GC_11, amp[794]); 
  FFV1_0(w[39], w[5], w[27], pars->GC_11, amp[795]); 
  FFV1_0(w[36], w[9], w[27], pars->GC_11, amp[796]); 
  FFV1_0(w[40], w[5], w[27], pars->GC_11, amp[797]); 
  FFV1_0(w[36], w[15], w[27], pars->GC_11, amp[798]); 
  FFV1_0(w[6], w[9], w[41], pars->GC_11, amp[799]); 
  FFV1_0(w[42], w[9], w[3], pars->GC_11, amp[800]); 
  FFV1_0(w[13], w[5], w[41], pars->GC_11, amp[801]); 
  FFV1_0(w[13], w[43], w[3], pars->GC_11, amp[802]); 
  FFV1_0(w[6], w[15], w[41], pars->GC_11, amp[803]); 
  FFV1_0(w[42], w[15], w[3], pars->GC_11, amp[804]); 
  FFV1_0(w[17], w[5], w[41], pars->GC_11, amp[805]); 
  FFV1_0(w[17], w[43], w[3], pars->GC_11, amp[806]); 
  FFV1_0(w[1], w[23], w[50], pars->GC_11, amp[807]); 
  FFV1_0(w[51], w[23], w[3], pars->GC_11, amp[808]); 
  FFV1_0(w[12], w[4], w[50], pars->GC_11, amp[809]); 
  FFV1_0(w[12], w[52], w[3], pars->GC_11, amp[810]); 
  FFV1_0(w[1], w[26], w[50], pars->GC_11, amp[811]); 
  FFV1_0(w[51], w[26], w[3], pars->GC_11, amp[812]); 
  FFV1_0(w[16], w[4], w[50], pars->GC_11, amp[813]); 
  FFV1_0(w[16], w[52], w[3], pars->GC_11, amp[814]); 
  FFV1_0(w[6], w[53], w[10], pars->GC_11, amp[815]); 
  FFV1_0(w[55], w[5], w[10], pars->GC_11, amp[816]); 
  FFV1_0(w[6], w[56], w[10], pars->GC_11, amp[817]); 
  FFV1_0(w[58], w[5], w[10], pars->GC_11, amp[818]); 
  FFV1_0(w[1], w[19], w[21], pars->GC_11, amp[819]); 
  FFV1_0(w[12], w[8], w[21], pars->GC_11, amp[820]); 
  FFV1_0(w[1], w[20], w[21], pars->GC_11, amp[821]); 
  FFV1_0(w[16], w[8], w[21], pars->GC_11, amp[822]); 
  FFV1_0(w[1], w[23], w[25], pars->GC_11, amp[823]); 
  FFV1_0(w[12], w[4], w[25], pars->GC_11, amp[824]); 
  FFV1_0(w[1], w[26], w[25], pars->GC_11, amp[825]); 
  FFV1_0(w[16], w[4], w[25], pars->GC_11, amp[826]); 
  FFV1_0(w[6], w[63], w[27], pars->GC_11, amp[827]); 
  FFV1_0(w[55], w[22], w[27], pars->GC_11, amp[828]); 
  FFV1_0(w[6], w[64], w[27], pars->GC_11, amp[829]); 
  FFV1_0(w[58], w[22], w[27], pars->GC_11, amp[830]); 
  FFV1_0(w[6], w[53], w[33], pars->GC_11, amp[831]); 
  FFV1_0(w[55], w[5], w[33], pars->GC_11, amp[832]); 
  FFV1_0(w[6], w[56], w[33], pars->GC_11, amp[833]); 
  FFV1_0(w[58], w[5], w[33], pars->GC_11, amp[834]); 
  FFV1_0(w[34], w[4], w[21], pars->GC_11, amp[835]); 
  FFV1_0(w[31], w[23], w[21], pars->GC_11, amp[836]); 
  FFV1_0(w[35], w[4], w[21], pars->GC_11, amp[837]); 
  FFV1_0(w[31], w[26], w[21], pars->GC_11, amp[838]); 
  FFV1_0(w[1], w[23], w[37], pars->GC_11, amp[839]); 
  FFV1_0(w[12], w[4], w[37], pars->GC_11, amp[840]); 
  FFV1_0(w[1], w[26], w[37], pars->GC_11, amp[841]); 
  FFV1_0(w[16], w[4], w[37], pars->GC_11, amp[842]); 
  FFV1_0(w[67], w[5], w[27], pars->GC_11, amp[843]); 
  FFV1_0(w[36], w[53], w[27], pars->GC_11, amp[844]); 
  FFV1_0(w[68], w[5], w[27], pars->GC_11, amp[845]); 
  FFV1_0(w[36], w[56], w[27], pars->GC_11, amp[846]); 
  FFV1_0(w[6], w[53], w[41], pars->GC_11, amp[847]); 
  FFV1_0(w[42], w[53], w[3], pars->GC_11, amp[848]); 
  FFV1_0(w[55], w[5], w[41], pars->GC_11, amp[849]); 
  FFV1_0(w[55], w[43], w[3], pars->GC_11, amp[850]); 
  FFV1_0(w[6], w[56], w[41], pars->GC_11, amp[851]); 
  FFV1_0(w[42], w[56], w[3], pars->GC_11, amp[852]); 
  FFV1_0(w[58], w[5], w[41], pars->GC_11, amp[853]); 
  FFV1_0(w[58], w[43], w[3], pars->GC_11, amp[854]); 
  FFV1_0(w[1], w[23], w[50], pars->GC_11, amp[855]); 
  FFV1_0(w[51], w[23], w[3], pars->GC_11, amp[856]); 
  FFV1_0(w[12], w[4], w[50], pars->GC_11, amp[857]); 
  FFV1_0(w[12], w[52], w[3], pars->GC_11, amp[858]); 
  FFV1_0(w[1], w[26], w[50], pars->GC_11, amp[859]); 
  FFV1_0(w[51], w[26], w[3], pars->GC_11, amp[860]); 
  FFV1_0(w[16], w[4], w[50], pars->GC_11, amp[861]); 
  FFV1_0(w[16], w[52], w[3], pars->GC_11, amp[862]); 
  FFV1_0(w[1], w[53], w[11], pars->GC_11, amp[863]); 
  FFV1_0(w[54], w[5], w[11], pars->GC_11, amp[864]); 
  FFV1_0(w[1], w[56], w[11], pars->GC_11, amp[865]); 
  FFV1_0(w[57], w[5], w[11], pars->GC_11, amp[866]); 
  FFV1_0(w[6], w[19], w[18], pars->GC_11, amp[867]); 
  FFV1_0(w[13], w[8], w[18], pars->GC_11, amp[868]); 
  FFV1_0(w[6], w[20], w[18], pars->GC_11, amp[869]); 
  FFV1_0(w[17], w[8], w[18], pars->GC_11, amp[870]); 
  FFV1_0(w[6], w[23], w[24], pars->GC_11, amp[871]); 
  FFV1_0(w[13], w[4], w[24], pars->GC_11, amp[872]); 
  FFV1_0(w[6], w[26], w[24], pars->GC_11, amp[873]); 
  FFV1_0(w[17], w[4], w[24], pars->GC_11, amp[874]); 
  FFV1_0(w[1], w[63], w[30], pars->GC_11, amp[875]); 
  FFV1_0(w[54], w[22], w[30], pars->GC_11, amp[876]); 
  FFV1_0(w[1], w[64], w[30], pars->GC_11, amp[877]); 
  FFV1_0(w[57], w[22], w[30], pars->GC_11, amp[878]); 
  FFV1_0(w[1], w[53], w[38], pars->GC_11, amp[879]); 
  FFV1_0(w[54], w[5], w[38], pars->GC_11, amp[880]); 
  FFV1_0(w[1], w[56], w[38], pars->GC_11, amp[881]); 
  FFV1_0(w[57], w[5], w[38], pars->GC_11, amp[882]); 
  FFV1_0(w[39], w[4], w[18], pars->GC_11, amp[883]); 
  FFV1_0(w[36], w[23], w[18], pars->GC_11, amp[884]); 
  FFV1_0(w[40], w[4], w[18], pars->GC_11, amp[885]); 
  FFV1_0(w[36], w[26], w[18], pars->GC_11, amp[886]); 
  FFV1_0(w[6], w[23], w[32], pars->GC_11, amp[887]); 
  FFV1_0(w[13], w[4], w[32], pars->GC_11, amp[888]); 
  FFV1_0(w[6], w[26], w[32], pars->GC_11, amp[889]); 
  FFV1_0(w[17], w[4], w[32], pars->GC_11, amp[890]); 
  FFV1_0(w[65], w[5], w[30], pars->GC_11, amp[891]); 
  FFV1_0(w[31], w[53], w[30], pars->GC_11, amp[892]); 
  FFV1_0(w[66], w[5], w[30], pars->GC_11, amp[893]); 
  FFV1_0(w[31], w[56], w[30], pars->GC_11, amp[894]); 
  FFV1_0(w[1], w[53], w[44], pars->GC_11, amp[895]); 
  FFV1_0(w[45], w[53], w[3], pars->GC_11, amp[896]); 
  FFV1_0(w[54], w[5], w[44], pars->GC_11, amp[897]); 
  FFV1_0(w[54], w[46], w[3], pars->GC_11, amp[898]); 
  FFV1_0(w[1], w[56], w[44], pars->GC_11, amp[899]); 
  FFV1_0(w[45], w[56], w[3], pars->GC_11, amp[900]); 
  FFV1_0(w[57], w[5], w[44], pars->GC_11, amp[901]); 
  FFV1_0(w[57], w[46], w[3], pars->GC_11, amp[902]); 
  FFV1_0(w[6], w[23], w[47], pars->GC_11, amp[903]); 
  FFV1_0(w[48], w[23], w[3], pars->GC_11, amp[904]); 
  FFV1_0(w[13], w[4], w[47], pars->GC_11, amp[905]); 
  FFV1_0(w[13], w[49], w[3], pars->GC_11, amp[906]); 
  FFV1_0(w[6], w[26], w[47], pars->GC_11, amp[907]); 
  FFV1_0(w[48], w[26], w[3], pars->GC_11, amp[908]); 
  FFV1_0(w[17], w[4], w[47], pars->GC_11, amp[909]); 
  FFV1_0(w[17], w[49], w[3], pars->GC_11, amp[910]); 
  FFV1_0(w[6], w[53], w[10], pars->GC_11, amp[911]); 
  FFV1_0(w[55], w[5], w[10], pars->GC_11, amp[912]); 
  FFV1_0(w[6], w[56], w[10], pars->GC_11, amp[913]); 
  FFV1_0(w[58], w[5], w[10], pars->GC_11, amp[914]); 
  FFV1_0(w[1], w[59], w[21], pars->GC_11, amp[915]); 
  FFV1_0(w[54], w[8], w[21], pars->GC_11, amp[916]); 
  FFV1_0(w[1], w[60], w[21], pars->GC_11, amp[917]); 
  FFV1_0(w[57], w[8], w[21], pars->GC_11, amp[918]); 
  FFV1_0(w[1], w[61], w[25], pars->GC_11, amp[919]); 
  FFV1_0(w[54], w[4], w[25], pars->GC_11, amp[920]); 
  FFV1_0(w[1], w[62], w[25], pars->GC_11, amp[921]); 
  FFV1_0(w[57], w[4], w[25], pars->GC_11, amp[922]); 
  FFV1_0(w[6], w[63], w[27], pars->GC_11, amp[923]); 
  FFV1_0(w[55], w[22], w[27], pars->GC_11, amp[924]); 
  FFV1_0(w[6], w[64], w[27], pars->GC_11, amp[925]); 
  FFV1_0(w[58], w[22], w[27], pars->GC_11, amp[926]); 
  FFV1_0(w[6], w[53], w[33], pars->GC_11, amp[927]); 
  FFV1_0(w[55], w[5], w[33], pars->GC_11, amp[928]); 
  FFV1_0(w[6], w[56], w[33], pars->GC_11, amp[929]); 
  FFV1_0(w[58], w[5], w[33], pars->GC_11, amp[930]); 
  FFV1_0(w[65], w[4], w[21], pars->GC_11, amp[931]); 
  FFV1_0(w[31], w[61], w[21], pars->GC_11, amp[932]); 
  FFV1_0(w[66], w[4], w[21], pars->GC_11, amp[933]); 
  FFV1_0(w[31], w[62], w[21], pars->GC_11, amp[934]); 
  FFV1_0(w[1], w[61], w[37], pars->GC_11, amp[935]); 
  FFV1_0(w[54], w[4], w[37], pars->GC_11, amp[936]); 
  FFV1_0(w[1], w[62], w[37], pars->GC_11, amp[937]); 
  FFV1_0(w[57], w[4], w[37], pars->GC_11, amp[938]); 
  FFV1_0(w[67], w[5], w[27], pars->GC_11, amp[939]); 
  FFV1_0(w[36], w[53], w[27], pars->GC_11, amp[940]); 
  FFV1_0(w[68], w[5], w[27], pars->GC_11, amp[941]); 
  FFV1_0(w[36], w[56], w[27], pars->GC_11, amp[942]); 
  FFV1_0(w[6], w[53], w[41], pars->GC_11, amp[943]); 
  FFV1_0(w[42], w[53], w[3], pars->GC_11, amp[944]); 
  FFV1_0(w[55], w[5], w[41], pars->GC_11, amp[945]); 
  FFV1_0(w[55], w[43], w[3], pars->GC_11, amp[946]); 
  FFV1_0(w[6], w[56], w[41], pars->GC_11, amp[947]); 
  FFV1_0(w[42], w[56], w[3], pars->GC_11, amp[948]); 
  FFV1_0(w[58], w[5], w[41], pars->GC_11, amp[949]); 
  FFV1_0(w[58], w[43], w[3], pars->GC_11, amp[950]); 
  FFV1_0(w[1], w[61], w[50], pars->GC_11, amp[951]); 
  FFV1_0(w[51], w[61], w[3], pars->GC_11, amp[952]); 
  FFV1_0(w[54], w[4], w[50], pars->GC_11, amp[953]); 
  FFV1_0(w[54], w[52], w[3], pars->GC_11, amp[954]); 
  FFV1_0(w[1], w[62], w[50], pars->GC_11, amp[955]); 
  FFV1_0(w[51], w[62], w[3], pars->GC_11, amp[956]); 
  FFV1_0(w[57], w[4], w[50], pars->GC_11, amp[957]); 
  FFV1_0(w[57], w[52], w[3], pars->GC_11, amp[958]); 
  FFV1_0(w[6], w[23], w[72], pars->GC_11, amp[959]); 
  FFV1_0(w[13], w[4], w[72], pars->GC_11, amp[960]); 
  FFV1_0(w[6], w[26], w[72], pars->GC_11, amp[961]); 
  FFV1_0(w[17], w[4], w[72], pars->GC_11, amp[962]); 
  FFV1_0(w[70], w[77], w[30], pars->GC_11, amp[963]); 
  FFV1_0(w[74], w[71], w[30], pars->GC_11, amp[964]); 
  FFV1_0(w[70], w[78], w[30], pars->GC_11, amp[965]); 
  FFV1_0(w[75], w[71], w[30], pars->GC_11, amp[966]); 
  FFV1_0(w[70], w[79], w[11], pars->GC_11, amp[967]); 
  FFV1_0(w[74], w[69], w[11], pars->GC_11, amp[968]); 
  FFV1_0(w[70], w[81], w[11], pars->GC_11, amp[969]); 
  FFV1_0(w[75], w[69], w[11], pars->GC_11, amp[970]); 
  FFV1_0(w[6], w[19], w[82], pars->GC_11, amp[971]); 
  FFV1_0(w[13], w[8], w[82], pars->GC_11, amp[972]); 
  FFV1_0(w[6], w[20], w[82], pars->GC_11, amp[973]); 
  FFV1_0(w[17], w[8], w[82], pars->GC_11, amp[974]); 
  FFV1_0(w[6], w[23], w[86], pars->GC_11, amp[975]); 
  FFV1_0(w[13], w[4], w[86], pars->GC_11, amp[976]); 
  FFV1_0(w[6], w[26], w[86], pars->GC_11, amp[977]); 
  FFV1_0(w[17], w[4], w[86], pars->GC_11, amp[978]); 
  FFV1_0(w[87], w[69], w[30], pars->GC_11, amp[979]); 
  FFV1_0(w[84], w[79], w[30], pars->GC_11, amp[980]); 
  FFV1_0(w[88], w[69], w[30], pars->GC_11, amp[981]); 
  FFV1_0(w[84], w[81], w[30], pars->GC_11, amp[982]); 
  FFV1_0(w[70], w[79], w[38], pars->GC_11, amp[983]); 
  FFV1_0(w[74], w[69], w[38], pars->GC_11, amp[984]); 
  FFV1_0(w[70], w[81], w[38], pars->GC_11, amp[985]); 
  FFV1_0(w[75], w[69], w[38], pars->GC_11, amp[986]); 
  FFV1_0(w[39], w[4], w[82], pars->GC_11, amp[987]); 
  FFV1_0(w[36], w[23], w[82], pars->GC_11, amp[988]); 
  FFV1_0(w[40], w[4], w[82], pars->GC_11, amp[989]); 
  FFV1_0(w[36], w[26], w[82], pars->GC_11, amp[990]); 
  FFV1_0(w[6], w[23], w[90], pars->GC_11, amp[991]); 
  FFV1_0(w[91], w[23], w[3], pars->GC_11, amp[992]); 
  FFV1_0(w[13], w[4], w[90], pars->GC_11, amp[993]); 
  FFV1_0(w[13], w[92], w[3], pars->GC_11, amp[994]); 
  FFV1_0(w[6], w[26], w[90], pars->GC_11, amp[995]); 
  FFV1_0(w[91], w[26], w[3], pars->GC_11, amp[996]); 
  FFV1_0(w[17], w[4], w[90], pars->GC_11, amp[997]); 
  FFV1_0(w[17], w[92], w[3], pars->GC_11, amp[998]); 
  FFV1_0(w[70], w[79], w[44], pars->GC_11, amp[999]); 
  FFV1_0(w[99], w[79], w[3], pars->GC_11, amp[1000]); 
  FFV1_0(w[74], w[69], w[44], pars->GC_11, amp[1001]); 
  FFV1_0(w[74], w[100], w[3], pars->GC_11, amp[1002]); 
  FFV1_0(w[70], w[81], w[44], pars->GC_11, amp[1003]); 
  FFV1_0(w[99], w[81], w[3], pars->GC_11, amp[1004]); 
  FFV1_0(w[75], w[69], w[44], pars->GC_11, amp[1005]); 
  FFV1_0(w[75], w[100], w[3], pars->GC_11, amp[1006]); 
  FFV1_0(w[6], w[61], w[72], pars->GC_11, amp[1007]); 
  FFV1_0(w[55], w[4], w[72], pars->GC_11, amp[1008]); 
  FFV1_0(w[6], w[62], w[72], pars->GC_11, amp[1009]); 
  FFV1_0(w[58], w[4], w[72], pars->GC_11, amp[1010]); 
  FFV1_0(w[70], w[77], w[30], pars->GC_11, amp[1011]); 
  FFV1_0(w[74], w[71], w[30], pars->GC_11, amp[1012]); 
  FFV1_0(w[70], w[78], w[30], pars->GC_11, amp[1013]); 
  FFV1_0(w[75], w[71], w[30], pars->GC_11, amp[1014]); 
  FFV1_0(w[70], w[79], w[11], pars->GC_11, amp[1015]); 
  FFV1_0(w[74], w[69], w[11], pars->GC_11, amp[1016]); 
  FFV1_0(w[70], w[81], w[11], pars->GC_11, amp[1017]); 
  FFV1_0(w[75], w[69], w[11], pars->GC_11, amp[1018]); 
  FFV1_0(w[6], w[59], w[82], pars->GC_11, amp[1019]); 
  FFV1_0(w[55], w[8], w[82], pars->GC_11, amp[1020]); 
  FFV1_0(w[6], w[60], w[82], pars->GC_11, amp[1021]); 
  FFV1_0(w[58], w[8], w[82], pars->GC_11, amp[1022]); 
  FFV1_0(w[6], w[61], w[86], pars->GC_11, amp[1023]); 
  FFV1_0(w[55], w[4], w[86], pars->GC_11, amp[1024]); 
  FFV1_0(w[6], w[62], w[86], pars->GC_11, amp[1025]); 
  FFV1_0(w[58], w[4], w[86], pars->GC_11, amp[1026]); 
  FFV1_0(w[87], w[69], w[30], pars->GC_11, amp[1027]); 
  FFV1_0(w[84], w[79], w[30], pars->GC_11, amp[1028]); 
  FFV1_0(w[88], w[69], w[30], pars->GC_11, amp[1029]); 
  FFV1_0(w[84], w[81], w[30], pars->GC_11, amp[1030]); 
  FFV1_0(w[70], w[79], w[38], pars->GC_11, amp[1031]); 
  FFV1_0(w[74], w[69], w[38], pars->GC_11, amp[1032]); 
  FFV1_0(w[70], w[81], w[38], pars->GC_11, amp[1033]); 
  FFV1_0(w[75], w[69], w[38], pars->GC_11, amp[1034]); 
  FFV1_0(w[67], w[4], w[82], pars->GC_11, amp[1035]); 
  FFV1_0(w[36], w[61], w[82], pars->GC_11, amp[1036]); 
  FFV1_0(w[68], w[4], w[82], pars->GC_11, amp[1037]); 
  FFV1_0(w[36], w[62], w[82], pars->GC_11, amp[1038]); 
  FFV1_0(w[6], w[61], w[90], pars->GC_11, amp[1039]); 
  FFV1_0(w[91], w[61], w[3], pars->GC_11, amp[1040]); 
  FFV1_0(w[55], w[4], w[90], pars->GC_11, amp[1041]); 
  FFV1_0(w[55], w[92], w[3], pars->GC_11, amp[1042]); 
  FFV1_0(w[6], w[62], w[90], pars->GC_11, amp[1043]); 
  FFV1_0(w[91], w[62], w[3], pars->GC_11, amp[1044]); 
  FFV1_0(w[58], w[4], w[90], pars->GC_11, amp[1045]); 
  FFV1_0(w[58], w[92], w[3], pars->GC_11, amp[1046]); 
  FFV1_0(w[70], w[79], w[44], pars->GC_11, amp[1047]); 
  FFV1_0(w[99], w[79], w[3], pars->GC_11, amp[1048]); 
  FFV1_0(w[74], w[69], w[44], pars->GC_11, amp[1049]); 
  FFV1_0(w[74], w[100], w[3], pars->GC_11, amp[1050]); 
  FFV1_0(w[70], w[81], w[44], pars->GC_11, amp[1051]); 
  FFV1_0(w[99], w[81], w[3], pars->GC_11, amp[1052]); 
  FFV1_0(w[75], w[69], w[44], pars->GC_11, amp[1053]); 
  FFV1_0(w[75], w[100], w[3], pars->GC_11, amp[1054]); 
  FFV1_0(w[6], w[105], w[80], pars->GC_11, amp[1055]); 
  FFV1_0(w[55], w[69], w[80], pars->GC_11, amp[1056]); 
  FFV1_0(w[6], w[106], w[80], pars->GC_11, amp[1057]); 
  FFV1_0(w[58], w[69], w[80], pars->GC_11, amp[1058]); 
  FFV1_0(w[70], w[19], w[83], pars->GC_11, amp[1059]); 
  FFV1_0(w[74], w[8], w[83], pars->GC_11, amp[1060]); 
  FFV1_0(w[70], w[20], w[83], pars->GC_11, amp[1061]); 
  FFV1_0(w[75], w[8], w[83], pars->GC_11, amp[1062]); 
  FFV1_0(w[70], w[23], w[73], pars->GC_11, amp[1063]); 
  FFV1_0(w[74], w[4], w[73], pars->GC_11, amp[1064]); 
  FFV1_0(w[70], w[26], w[73], pars->GC_11, amp[1065]); 
  FFV1_0(w[75], w[4], w[73], pars->GC_11, amp[1066]); 
  FFV1_0(w[6], w[103], w[76], pars->GC_11, amp[1067]); 
  FFV1_0(w[55], w[71], w[76], pars->GC_11, amp[1068]); 
  FFV1_0(w[6], w[104], w[76], pars->GC_11, amp[1069]); 
  FFV1_0(w[58], w[71], w[76], pars->GC_11, amp[1070]); 
  FFV1_0(w[6], w[105], w[85], pars->GC_11, amp[1071]); 
  FFV1_0(w[55], w[69], w[85], pars->GC_11, amp[1072]); 
  FFV1_0(w[6], w[106], w[85], pars->GC_11, amp[1073]); 
  FFV1_0(w[58], w[69], w[85], pars->GC_11, amp[1074]); 
  FFV1_0(w[87], w[4], w[83], pars->GC_11, amp[1075]); 
  FFV1_0(w[84], w[23], w[83], pars->GC_11, amp[1076]); 
  FFV1_0(w[88], w[4], w[83], pars->GC_11, amp[1077]); 
  FFV1_0(w[84], w[26], w[83], pars->GC_11, amp[1078]); 
  FFV1_0(w[70], w[23], w[89], pars->GC_11, amp[1079]); 
  FFV1_0(w[74], w[4], w[89], pars->GC_11, amp[1080]); 
  FFV1_0(w[70], w[26], w[89], pars->GC_11, amp[1081]); 
  FFV1_0(w[75], w[4], w[89], pars->GC_11, amp[1082]); 
  FFV1_0(w[67], w[69], w[76], pars->GC_11, amp[1083]); 
  FFV1_0(w[36], w[105], w[76], pars->GC_11, amp[1084]); 
  FFV1_0(w[68], w[69], w[76], pars->GC_11, amp[1085]); 
  FFV1_0(w[36], w[106], w[76], pars->GC_11, amp[1086]); 
  FFV1_0(w[6], w[105], w[96], pars->GC_11, amp[1087]); 
  FFV1_0(w[97], w[105], w[3], pars->GC_11, amp[1088]); 
  FFV1_0(w[55], w[69], w[96], pars->GC_11, amp[1089]); 
  FFV1_0(w[55], w[98], w[3], pars->GC_11, amp[1090]); 
  FFV1_0(w[6], w[106], w[96], pars->GC_11, amp[1091]); 
  FFV1_0(w[97], w[106], w[3], pars->GC_11, amp[1092]); 
  FFV1_0(w[58], w[69], w[96], pars->GC_11, amp[1093]); 
  FFV1_0(w[58], w[98], w[3], pars->GC_11, amp[1094]); 
  FFV1_0(w[70], w[23], w[93], pars->GC_11, amp[1095]); 
  FFV1_0(w[94], w[23], w[3], pars->GC_11, amp[1096]); 
  FFV1_0(w[74], w[4], w[93], pars->GC_11, amp[1097]); 
  FFV1_0(w[74], w[95], w[3], pars->GC_11, amp[1098]); 
  FFV1_0(w[70], w[26], w[93], pars->GC_11, amp[1099]); 
  FFV1_0(w[94], w[26], w[3], pars->GC_11, amp[1100]); 
  FFV1_0(w[75], w[4], w[93], pars->GC_11, amp[1101]); 
  FFV1_0(w[75], w[95], w[3], pars->GC_11, amp[1102]); 
  FFV1_0(w[6], w[61], w[72], pars->GC_11, amp[1103]); 
  FFV1_0(w[55], w[4], w[72], pars->GC_11, amp[1104]); 
  FFV1_0(w[6], w[62], w[72], pars->GC_11, amp[1105]); 
  FFV1_0(w[58], w[4], w[72], pars->GC_11, amp[1106]); 
  FFV1_0(w[70], w[103], w[30], pars->GC_11, amp[1107]); 
  FFV1_0(w[101], w[71], w[30], pars->GC_11, amp[1108]); 
  FFV1_0(w[70], w[104], w[30], pars->GC_11, amp[1109]); 
  FFV1_0(w[102], w[71], w[30], pars->GC_11, amp[1110]); 
  FFV1_0(w[70], w[105], w[11], pars->GC_11, amp[1111]); 
  FFV1_0(w[101], w[69], w[11], pars->GC_11, amp[1112]); 
  FFV1_0(w[70], w[106], w[11], pars->GC_11, amp[1113]); 
  FFV1_0(w[102], w[69], w[11], pars->GC_11, amp[1114]); 
  FFV1_0(w[6], w[59], w[82], pars->GC_11, amp[1115]); 
  FFV1_0(w[55], w[8], w[82], pars->GC_11, amp[1116]); 
  FFV1_0(w[6], w[60], w[82], pars->GC_11, amp[1117]); 
  FFV1_0(w[58], w[8], w[82], pars->GC_11, amp[1118]); 
  FFV1_0(w[6], w[61], w[86], pars->GC_11, amp[1119]); 
  FFV1_0(w[55], w[4], w[86], pars->GC_11, amp[1120]); 
  FFV1_0(w[6], w[62], w[86], pars->GC_11, amp[1121]); 
  FFV1_0(w[58], w[4], w[86], pars->GC_11, amp[1122]); 
  FFV1_0(w[107], w[69], w[30], pars->GC_11, amp[1123]); 
  FFV1_0(w[84], w[105], w[30], pars->GC_11, amp[1124]); 
  FFV1_0(w[108], w[69], w[30], pars->GC_11, amp[1125]); 
  FFV1_0(w[84], w[106], w[30], pars->GC_11, amp[1126]); 
  FFV1_0(w[70], w[105], w[38], pars->GC_11, amp[1127]); 
  FFV1_0(w[101], w[69], w[38], pars->GC_11, amp[1128]); 
  FFV1_0(w[70], w[106], w[38], pars->GC_11, amp[1129]); 
  FFV1_0(w[102], w[69], w[38], pars->GC_11, amp[1130]); 
  FFV1_0(w[67], w[4], w[82], pars->GC_11, amp[1131]); 
  FFV1_0(w[36], w[61], w[82], pars->GC_11, amp[1132]); 
  FFV1_0(w[68], w[4], w[82], pars->GC_11, amp[1133]); 
  FFV1_0(w[36], w[62], w[82], pars->GC_11, amp[1134]); 
  FFV1_0(w[6], w[61], w[90], pars->GC_11, amp[1135]); 
  FFV1_0(w[91], w[61], w[3], pars->GC_11, amp[1136]); 
  FFV1_0(w[55], w[4], w[90], pars->GC_11, amp[1137]); 
  FFV1_0(w[55], w[92], w[3], pars->GC_11, amp[1138]); 
  FFV1_0(w[6], w[62], w[90], pars->GC_11, amp[1139]); 
  FFV1_0(w[91], w[62], w[3], pars->GC_11, amp[1140]); 
  FFV1_0(w[58], w[4], w[90], pars->GC_11, amp[1141]); 
  FFV1_0(w[58], w[92], w[3], pars->GC_11, amp[1142]); 
  FFV1_0(w[70], w[105], w[44], pars->GC_11, amp[1143]); 
  FFV1_0(w[99], w[105], w[3], pars->GC_11, amp[1144]); 
  FFV1_0(w[101], w[69], w[44], pars->GC_11, amp[1145]); 
  FFV1_0(w[101], w[100], w[3], pars->GC_11, amp[1146]); 
  FFV1_0(w[70], w[106], w[44], pars->GC_11, amp[1147]); 
  FFV1_0(w[99], w[106], w[3], pars->GC_11, amp[1148]); 
  FFV1_0(w[102], w[69], w[44], pars->GC_11, amp[1149]); 
  FFV1_0(w[102], w[100], w[3], pars->GC_11, amp[1150]); 
  FFV1_0(w[6], w[112], w[10], pars->GC_11, amp[1151]); 
  FFV1_0(w[114], w[5], w[10], pars->GC_11, amp[1152]); 
  FFV1_0(w[6], w[116], w[10], pars->GC_11, amp[1153]); 
  FFV1_0(w[118], w[5], w[10], pars->GC_11, amp[1154]); 
  FFV1_0(w[1], w[119], w[21], pars->GC_11, amp[1155]); 
  FFV1_0(w[113], w[8], w[21], pars->GC_11, amp[1156]); 
  FFV1_0(w[1], w[120], w[21], pars->GC_11, amp[1157]); 
  FFV1_0(w[117], w[8], w[21], pars->GC_11, amp[1158]); 
  FFV1_0(w[1], w[121], w[25], pars->GC_11, amp[1159]); 
  FFV1_0(w[113], w[4], w[25], pars->GC_11, amp[1160]); 
  FFV1_0(w[1], w[122], w[25], pars->GC_11, amp[1161]); 
  FFV1_0(w[117], w[4], w[25], pars->GC_11, amp[1162]); 
  FFV1_0(w[6], w[123], w[27], pars->GC_11, amp[1163]); 
  FFV1_0(w[114], w[22], w[27], pars->GC_11, amp[1164]); 
  FFV1_0(w[6], w[124], w[27], pars->GC_11, amp[1165]); 
  FFV1_0(w[118], w[22], w[27], pars->GC_11, amp[1166]); 
  FFV1_0(w[6], w[112], w[33], pars->GC_11, amp[1167]); 
  FFV1_0(w[114], w[5], w[33], pars->GC_11, amp[1168]); 
  FFV1_0(w[6], w[116], w[33], pars->GC_11, amp[1169]); 
  FFV1_0(w[118], w[5], w[33], pars->GC_11, amp[1170]); 
  FFV1_0(w[125], w[4], w[21], pars->GC_11, amp[1171]); 
  FFV1_0(w[31], w[121], w[21], pars->GC_11, amp[1172]); 
  FFV1_0(w[126], w[4], w[21], pars->GC_11, amp[1173]); 
  FFV1_0(w[31], w[122], w[21], pars->GC_11, amp[1174]); 
  FFV1_0(w[1], w[121], w[37], pars->GC_11, amp[1175]); 
  FFV1_0(w[113], w[4], w[37], pars->GC_11, amp[1176]); 
  FFV1_0(w[1], w[122], w[37], pars->GC_11, amp[1177]); 
  FFV1_0(w[117], w[4], w[37], pars->GC_11, amp[1178]); 
  FFV1_0(w[127], w[5], w[27], pars->GC_11, amp[1179]); 
  FFV1_0(w[36], w[112], w[27], pars->GC_11, amp[1180]); 
  FFV1_0(w[128], w[5], w[27], pars->GC_11, amp[1181]); 
  FFV1_0(w[36], w[116], w[27], pars->GC_11, amp[1182]); 
  FFV1_0(w[6], w[112], w[41], pars->GC_11, amp[1183]); 
  FFV1_0(w[42], w[112], w[3], pars->GC_11, amp[1184]); 
  FFV1_0(w[114], w[5], w[41], pars->GC_11, amp[1185]); 
  FFV1_0(w[114], w[43], w[3], pars->GC_11, amp[1186]); 
  FFV1_0(w[6], w[116], w[41], pars->GC_11, amp[1187]); 
  FFV1_0(w[42], w[116], w[3], pars->GC_11, amp[1188]); 
  FFV1_0(w[118], w[5], w[41], pars->GC_11, amp[1189]); 
  FFV1_0(w[118], w[43], w[3], pars->GC_11, amp[1190]); 
  FFV1_0(w[1], w[121], w[50], pars->GC_11, amp[1191]); 
  FFV1_0(w[51], w[121], w[3], pars->GC_11, amp[1192]); 
  FFV1_0(w[113], w[4], w[50], pars->GC_11, amp[1193]); 
  FFV1_0(w[113], w[52], w[3], pars->GC_11, amp[1194]); 
  FFV1_0(w[1], w[122], w[50], pars->GC_11, amp[1195]); 
  FFV1_0(w[51], w[122], w[3], pars->GC_11, amp[1196]); 
  FFV1_0(w[117], w[4], w[50], pars->GC_11, amp[1197]); 
  FFV1_0(w[117], w[52], w[3], pars->GC_11, amp[1198]); 
  FFV1_0(w[6], w[129], w[10], pars->GC_11, amp[1199]); 
  FFV1_0(w[131], w[5], w[10], pars->GC_11, amp[1200]); 
  FFV1_0(w[6], w[132], w[10], pars->GC_11, amp[1201]); 
  FFV1_0(w[134], w[5], w[10], pars->GC_11, amp[1202]); 
  FFV1_0(w[1], w[119], w[21], pars->GC_11, amp[1203]); 
  FFV1_0(w[113], w[8], w[21], pars->GC_11, amp[1204]); 
  FFV1_0(w[1], w[120], w[21], pars->GC_11, amp[1205]); 
  FFV1_0(w[117], w[8], w[21], pars->GC_11, amp[1206]); 
  FFV1_0(w[1], w[121], w[25], pars->GC_11, amp[1207]); 
  FFV1_0(w[113], w[4], w[25], pars->GC_11, amp[1208]); 
  FFV1_0(w[1], w[122], w[25], pars->GC_11, amp[1209]); 
  FFV1_0(w[117], w[4], w[25], pars->GC_11, amp[1210]); 
  FFV1_0(w[6], w[139], w[27], pars->GC_11, amp[1211]); 
  FFV1_0(w[131], w[22], w[27], pars->GC_11, amp[1212]); 
  FFV1_0(w[6], w[140], w[27], pars->GC_11, amp[1213]); 
  FFV1_0(w[134], w[22], w[27], pars->GC_11, amp[1214]); 
  FFV1_0(w[6], w[129], w[33], pars->GC_11, amp[1215]); 
  FFV1_0(w[131], w[5], w[33], pars->GC_11, amp[1216]); 
  FFV1_0(w[6], w[132], w[33], pars->GC_11, amp[1217]); 
  FFV1_0(w[134], w[5], w[33], pars->GC_11, amp[1218]); 
  FFV1_0(w[125], w[4], w[21], pars->GC_11, amp[1219]); 
  FFV1_0(w[31], w[121], w[21], pars->GC_11, amp[1220]); 
  FFV1_0(w[126], w[4], w[21], pars->GC_11, amp[1221]); 
  FFV1_0(w[31], w[122], w[21], pars->GC_11, amp[1222]); 
  FFV1_0(w[1], w[121], w[37], pars->GC_11, amp[1223]); 
  FFV1_0(w[113], w[4], w[37], pars->GC_11, amp[1224]); 
  FFV1_0(w[1], w[122], w[37], pars->GC_11, amp[1225]); 
  FFV1_0(w[117], w[4], w[37], pars->GC_11, amp[1226]); 
  FFV1_0(w[143], w[5], w[27], pars->GC_11, amp[1227]); 
  FFV1_0(w[36], w[129], w[27], pars->GC_11, amp[1228]); 
  FFV1_0(w[144], w[5], w[27], pars->GC_11, amp[1229]); 
  FFV1_0(w[36], w[132], w[27], pars->GC_11, amp[1230]); 
  FFV1_0(w[6], w[129], w[41], pars->GC_11, amp[1231]); 
  FFV1_0(w[42], w[129], w[3], pars->GC_11, amp[1232]); 
  FFV1_0(w[131], w[5], w[41], pars->GC_11, amp[1233]); 
  FFV1_0(w[131], w[43], w[3], pars->GC_11, amp[1234]); 
  FFV1_0(w[6], w[132], w[41], pars->GC_11, amp[1235]); 
  FFV1_0(w[42], w[132], w[3], pars->GC_11, amp[1236]); 
  FFV1_0(w[134], w[5], w[41], pars->GC_11, amp[1237]); 
  FFV1_0(w[134], w[43], w[3], pars->GC_11, amp[1238]); 
  FFV1_0(w[1], w[121], w[50], pars->GC_11, amp[1239]); 
  FFV1_0(w[51], w[121], w[3], pars->GC_11, amp[1240]); 
  FFV1_0(w[113], w[4], w[50], pars->GC_11, amp[1241]); 
  FFV1_0(w[113], w[52], w[3], pars->GC_11, amp[1242]); 
  FFV1_0(w[1], w[122], w[50], pars->GC_11, amp[1243]); 
  FFV1_0(w[51], w[122], w[3], pars->GC_11, amp[1244]); 
  FFV1_0(w[117], w[4], w[50], pars->GC_11, amp[1245]); 
  FFV1_0(w[117], w[52], w[3], pars->GC_11, amp[1246]); 
  FFV1_0(w[1], w[129], w[11], pars->GC_11, amp[1247]); 
  FFV1_0(w[130], w[5], w[11], pars->GC_11, amp[1248]); 
  FFV1_0(w[1], w[132], w[11], pars->GC_11, amp[1249]); 
  FFV1_0(w[133], w[5], w[11], pars->GC_11, amp[1250]); 
  FFV1_0(w[6], w[119], w[18], pars->GC_11, amp[1251]); 
  FFV1_0(w[114], w[8], w[18], pars->GC_11, amp[1252]); 
  FFV1_0(w[6], w[120], w[18], pars->GC_11, amp[1253]); 
  FFV1_0(w[118], w[8], w[18], pars->GC_11, amp[1254]); 
  FFV1_0(w[6], w[121], w[24], pars->GC_11, amp[1255]); 
  FFV1_0(w[114], w[4], w[24], pars->GC_11, amp[1256]); 
  FFV1_0(w[6], w[122], w[24], pars->GC_11, amp[1257]); 
  FFV1_0(w[118], w[4], w[24], pars->GC_11, amp[1258]); 
  FFV1_0(w[1], w[139], w[30], pars->GC_11, amp[1259]); 
  FFV1_0(w[130], w[22], w[30], pars->GC_11, amp[1260]); 
  FFV1_0(w[1], w[140], w[30], pars->GC_11, amp[1261]); 
  FFV1_0(w[133], w[22], w[30], pars->GC_11, amp[1262]); 
  FFV1_0(w[1], w[129], w[38], pars->GC_11, amp[1263]); 
  FFV1_0(w[130], w[5], w[38], pars->GC_11, amp[1264]); 
  FFV1_0(w[1], w[132], w[38], pars->GC_11, amp[1265]); 
  FFV1_0(w[133], w[5], w[38], pars->GC_11, amp[1266]); 
  FFV1_0(w[127], w[4], w[18], pars->GC_11, amp[1267]); 
  FFV1_0(w[36], w[121], w[18], pars->GC_11, amp[1268]); 
  FFV1_0(w[128], w[4], w[18], pars->GC_11, amp[1269]); 
  FFV1_0(w[36], w[122], w[18], pars->GC_11, amp[1270]); 
  FFV1_0(w[6], w[121], w[32], pars->GC_11, amp[1271]); 
  FFV1_0(w[114], w[4], w[32], pars->GC_11, amp[1272]); 
  FFV1_0(w[6], w[122], w[32], pars->GC_11, amp[1273]); 
  FFV1_0(w[118], w[4], w[32], pars->GC_11, amp[1274]); 
  FFV1_0(w[141], w[5], w[30], pars->GC_11, amp[1275]); 
  FFV1_0(w[31], w[129], w[30], pars->GC_11, amp[1276]); 
  FFV1_0(w[142], w[5], w[30], pars->GC_11, amp[1277]); 
  FFV1_0(w[31], w[132], w[30], pars->GC_11, amp[1278]); 
  FFV1_0(w[1], w[129], w[44], pars->GC_11, amp[1279]); 
  FFV1_0(w[45], w[129], w[3], pars->GC_11, amp[1280]); 
  FFV1_0(w[130], w[5], w[44], pars->GC_11, amp[1281]); 
  FFV1_0(w[130], w[46], w[3], pars->GC_11, amp[1282]); 
  FFV1_0(w[1], w[132], w[44], pars->GC_11, amp[1283]); 
  FFV1_0(w[45], w[132], w[3], pars->GC_11, amp[1284]); 
  FFV1_0(w[133], w[5], w[44], pars->GC_11, amp[1285]); 
  FFV1_0(w[133], w[46], w[3], pars->GC_11, amp[1286]); 
  FFV1_0(w[6], w[121], w[47], pars->GC_11, amp[1287]); 
  FFV1_0(w[48], w[121], w[3], pars->GC_11, amp[1288]); 
  FFV1_0(w[114], w[4], w[47], pars->GC_11, amp[1289]); 
  FFV1_0(w[114], w[49], w[3], pars->GC_11, amp[1290]); 
  FFV1_0(w[6], w[122], w[47], pars->GC_11, amp[1291]); 
  FFV1_0(w[48], w[122], w[3], pars->GC_11, amp[1292]); 
  FFV1_0(w[118], w[4], w[47], pars->GC_11, amp[1293]); 
  FFV1_0(w[118], w[49], w[3], pars->GC_11, amp[1294]); 
  FFV1_0(w[6], w[129], w[10], pars->GC_11, amp[1295]); 
  FFV1_0(w[131], w[5], w[10], pars->GC_11, amp[1296]); 
  FFV1_0(w[6], w[132], w[10], pars->GC_11, amp[1297]); 
  FFV1_0(w[134], w[5], w[10], pars->GC_11, amp[1298]); 
  FFV1_0(w[1], w[135], w[21], pars->GC_11, amp[1299]); 
  FFV1_0(w[130], w[8], w[21], pars->GC_11, amp[1300]); 
  FFV1_0(w[1], w[136], w[21], pars->GC_11, amp[1301]); 
  FFV1_0(w[133], w[8], w[21], pars->GC_11, amp[1302]); 
  FFV1_0(w[1], w[137], w[25], pars->GC_11, amp[1303]); 
  FFV1_0(w[130], w[4], w[25], pars->GC_11, amp[1304]); 
  FFV1_0(w[1], w[138], w[25], pars->GC_11, amp[1305]); 
  FFV1_0(w[133], w[4], w[25], pars->GC_11, amp[1306]); 
  FFV1_0(w[6], w[139], w[27], pars->GC_11, amp[1307]); 
  FFV1_0(w[131], w[22], w[27], pars->GC_11, amp[1308]); 
  FFV1_0(w[6], w[140], w[27], pars->GC_11, amp[1309]); 
  FFV1_0(w[134], w[22], w[27], pars->GC_11, amp[1310]); 
  FFV1_0(w[6], w[129], w[33], pars->GC_11, amp[1311]); 
  FFV1_0(w[131], w[5], w[33], pars->GC_11, amp[1312]); 
  FFV1_0(w[6], w[132], w[33], pars->GC_11, amp[1313]); 
  FFV1_0(w[134], w[5], w[33], pars->GC_11, amp[1314]); 
  FFV1_0(w[141], w[4], w[21], pars->GC_11, amp[1315]); 
  FFV1_0(w[31], w[137], w[21], pars->GC_11, amp[1316]); 
  FFV1_0(w[142], w[4], w[21], pars->GC_11, amp[1317]); 
  FFV1_0(w[31], w[138], w[21], pars->GC_11, amp[1318]); 
  FFV1_0(w[1], w[137], w[37], pars->GC_11, amp[1319]); 
  FFV1_0(w[130], w[4], w[37], pars->GC_11, amp[1320]); 
  FFV1_0(w[1], w[138], w[37], pars->GC_11, amp[1321]); 
  FFV1_0(w[133], w[4], w[37], pars->GC_11, amp[1322]); 
  FFV1_0(w[143], w[5], w[27], pars->GC_11, amp[1323]); 
  FFV1_0(w[36], w[129], w[27], pars->GC_11, amp[1324]); 
  FFV1_0(w[144], w[5], w[27], pars->GC_11, amp[1325]); 
  FFV1_0(w[36], w[132], w[27], pars->GC_11, amp[1326]); 
  FFV1_0(w[6], w[129], w[41], pars->GC_11, amp[1327]); 
  FFV1_0(w[42], w[129], w[3], pars->GC_11, amp[1328]); 
  FFV1_0(w[131], w[5], w[41], pars->GC_11, amp[1329]); 
  FFV1_0(w[131], w[43], w[3], pars->GC_11, amp[1330]); 
  FFV1_0(w[6], w[132], w[41], pars->GC_11, amp[1331]); 
  FFV1_0(w[42], w[132], w[3], pars->GC_11, amp[1332]); 
  FFV1_0(w[134], w[5], w[41], pars->GC_11, amp[1333]); 
  FFV1_0(w[134], w[43], w[3], pars->GC_11, amp[1334]); 
  FFV1_0(w[1], w[137], w[50], pars->GC_11, amp[1335]); 
  FFV1_0(w[51], w[137], w[3], pars->GC_11, amp[1336]); 
  FFV1_0(w[130], w[4], w[50], pars->GC_11, amp[1337]); 
  FFV1_0(w[130], w[52], w[3], pars->GC_11, amp[1338]); 
  FFV1_0(w[1], w[138], w[50], pars->GC_11, amp[1339]); 
  FFV1_0(w[51], w[138], w[3], pars->GC_11, amp[1340]); 
  FFV1_0(w[133], w[4], w[50], pars->GC_11, amp[1341]); 
  FFV1_0(w[133], w[52], w[3], pars->GC_11, amp[1342]); 
  FFV1_0(w[6], w[121], w[72], pars->GC_11, amp[1343]); 
  FFV1_0(w[114], w[4], w[72], pars->GC_11, amp[1344]); 
  FFV1_0(w[6], w[122], w[72], pars->GC_11, amp[1345]); 
  FFV1_0(w[118], w[4], w[72], pars->GC_11, amp[1346]); 
  FFV1_0(w[70], w[147], w[30], pars->GC_11, amp[1347]); 
  FFV1_0(w[145], w[71], w[30], pars->GC_11, amp[1348]); 
  FFV1_0(w[70], w[148], w[30], pars->GC_11, amp[1349]); 
  FFV1_0(w[146], w[71], w[30], pars->GC_11, amp[1350]); 
  FFV1_0(w[70], w[149], w[11], pars->GC_11, amp[1351]); 
  FFV1_0(w[145], w[69], w[11], pars->GC_11, amp[1352]); 
  FFV1_0(w[70], w[150], w[11], pars->GC_11, amp[1353]); 
  FFV1_0(w[146], w[69], w[11], pars->GC_11, amp[1354]); 
  FFV1_0(w[6], w[119], w[82], pars->GC_11, amp[1355]); 
  FFV1_0(w[114], w[8], w[82], pars->GC_11, amp[1356]); 
  FFV1_0(w[6], w[120], w[82], pars->GC_11, amp[1357]); 
  FFV1_0(w[118], w[8], w[82], pars->GC_11, amp[1358]); 
  FFV1_0(w[6], w[121], w[86], pars->GC_11, amp[1359]); 
  FFV1_0(w[114], w[4], w[86], pars->GC_11, amp[1360]); 
  FFV1_0(w[6], w[122], w[86], pars->GC_11, amp[1361]); 
  FFV1_0(w[118], w[4], w[86], pars->GC_11, amp[1362]); 
  FFV1_0(w[151], w[69], w[30], pars->GC_11, amp[1363]); 
  FFV1_0(w[84], w[149], w[30], pars->GC_11, amp[1364]); 
  FFV1_0(w[152], w[69], w[30], pars->GC_11, amp[1365]); 
  FFV1_0(w[84], w[150], w[30], pars->GC_11, amp[1366]); 
  FFV1_0(w[70], w[149], w[38], pars->GC_11, amp[1367]); 
  FFV1_0(w[145], w[69], w[38], pars->GC_11, amp[1368]); 
  FFV1_0(w[70], w[150], w[38], pars->GC_11, amp[1369]); 
  FFV1_0(w[146], w[69], w[38], pars->GC_11, amp[1370]); 
  FFV1_0(w[127], w[4], w[82], pars->GC_11, amp[1371]); 
  FFV1_0(w[36], w[121], w[82], pars->GC_11, amp[1372]); 
  FFV1_0(w[128], w[4], w[82], pars->GC_11, amp[1373]); 
  FFV1_0(w[36], w[122], w[82], pars->GC_11, amp[1374]); 
  FFV1_0(w[6], w[121], w[90], pars->GC_11, amp[1375]); 
  FFV1_0(w[91], w[121], w[3], pars->GC_11, amp[1376]); 
  FFV1_0(w[114], w[4], w[90], pars->GC_11, amp[1377]); 
  FFV1_0(w[114], w[92], w[3], pars->GC_11, amp[1378]); 
  FFV1_0(w[6], w[122], w[90], pars->GC_11, amp[1379]); 
  FFV1_0(w[91], w[122], w[3], pars->GC_11, amp[1380]); 
  FFV1_0(w[118], w[4], w[90], pars->GC_11, amp[1381]); 
  FFV1_0(w[118], w[92], w[3], pars->GC_11, amp[1382]); 
  FFV1_0(w[70], w[149], w[44], pars->GC_11, amp[1383]); 
  FFV1_0(w[99], w[149], w[3], pars->GC_11, amp[1384]); 
  FFV1_0(w[145], w[69], w[44], pars->GC_11, amp[1385]); 
  FFV1_0(w[145], w[100], w[3], pars->GC_11, amp[1386]); 
  FFV1_0(w[70], w[150], w[44], pars->GC_11, amp[1387]); 
  FFV1_0(w[99], w[150], w[3], pars->GC_11, amp[1388]); 
  FFV1_0(w[146], w[69], w[44], pars->GC_11, amp[1389]); 
  FFV1_0(w[146], w[100], w[3], pars->GC_11, amp[1390]); 
  FFV1_0(w[6], w[137], w[72], pars->GC_11, amp[1391]); 
  FFV1_0(w[131], w[4], w[72], pars->GC_11, amp[1392]); 
  FFV1_0(w[6], w[138], w[72], pars->GC_11, amp[1393]); 
  FFV1_0(w[134], w[4], w[72], pars->GC_11, amp[1394]); 
  FFV1_0(w[70], w[147], w[30], pars->GC_11, amp[1395]); 
  FFV1_0(w[145], w[71], w[30], pars->GC_11, amp[1396]); 
  FFV1_0(w[70], w[148], w[30], pars->GC_11, amp[1397]); 
  FFV1_0(w[146], w[71], w[30], pars->GC_11, amp[1398]); 
  FFV1_0(w[70], w[149], w[11], pars->GC_11, amp[1399]); 
  FFV1_0(w[145], w[69], w[11], pars->GC_11, amp[1400]); 
  FFV1_0(w[70], w[150], w[11], pars->GC_11, amp[1401]); 
  FFV1_0(w[146], w[69], w[11], pars->GC_11, amp[1402]); 
  FFV1_0(w[6], w[135], w[82], pars->GC_11, amp[1403]); 
  FFV1_0(w[131], w[8], w[82], pars->GC_11, amp[1404]); 
  FFV1_0(w[6], w[136], w[82], pars->GC_11, amp[1405]); 
  FFV1_0(w[134], w[8], w[82], pars->GC_11, amp[1406]); 
  FFV1_0(w[6], w[137], w[86], pars->GC_11, amp[1407]); 
  FFV1_0(w[131], w[4], w[86], pars->GC_11, amp[1408]); 
  FFV1_0(w[6], w[138], w[86], pars->GC_11, amp[1409]); 
  FFV1_0(w[134], w[4], w[86], pars->GC_11, amp[1410]); 
  FFV1_0(w[151], w[69], w[30], pars->GC_11, amp[1411]); 
  FFV1_0(w[84], w[149], w[30], pars->GC_11, amp[1412]); 
  FFV1_0(w[152], w[69], w[30], pars->GC_11, amp[1413]); 
  FFV1_0(w[84], w[150], w[30], pars->GC_11, amp[1414]); 
  FFV1_0(w[70], w[149], w[38], pars->GC_11, amp[1415]); 
  FFV1_0(w[145], w[69], w[38], pars->GC_11, amp[1416]); 
  FFV1_0(w[70], w[150], w[38], pars->GC_11, amp[1417]); 
  FFV1_0(w[146], w[69], w[38], pars->GC_11, amp[1418]); 
  FFV1_0(w[143], w[4], w[82], pars->GC_11, amp[1419]); 
  FFV1_0(w[36], w[137], w[82], pars->GC_11, amp[1420]); 
  FFV1_0(w[144], w[4], w[82], pars->GC_11, amp[1421]); 
  FFV1_0(w[36], w[138], w[82], pars->GC_11, amp[1422]); 
  FFV1_0(w[6], w[137], w[90], pars->GC_11, amp[1423]); 
  FFV1_0(w[91], w[137], w[3], pars->GC_11, amp[1424]); 
  FFV1_0(w[131], w[4], w[90], pars->GC_11, amp[1425]); 
  FFV1_0(w[131], w[92], w[3], pars->GC_11, amp[1426]); 
  FFV1_0(w[6], w[138], w[90], pars->GC_11, amp[1427]); 
  FFV1_0(w[91], w[138], w[3], pars->GC_11, amp[1428]); 
  FFV1_0(w[134], w[4], w[90], pars->GC_11, amp[1429]); 
  FFV1_0(w[134], w[92], w[3], pars->GC_11, amp[1430]); 
  FFV1_0(w[70], w[149], w[44], pars->GC_11, amp[1431]); 
  FFV1_0(w[99], w[149], w[3], pars->GC_11, amp[1432]); 
  FFV1_0(w[145], w[69], w[44], pars->GC_11, amp[1433]); 
  FFV1_0(w[145], w[100], w[3], pars->GC_11, amp[1434]); 
  FFV1_0(w[70], w[150], w[44], pars->GC_11, amp[1435]); 
  FFV1_0(w[99], w[150], w[3], pars->GC_11, amp[1436]); 
  FFV1_0(w[146], w[69], w[44], pars->GC_11, amp[1437]); 
  FFV1_0(w[146], w[100], w[3], pars->GC_11, amp[1438]); 
  FFV1_0(w[6], w[157], w[80], pars->GC_11, amp[1439]); 
  FFV1_0(w[131], w[69], w[80], pars->GC_11, amp[1440]); 
  FFV1_0(w[6], w[158], w[80], pars->GC_11, amp[1441]); 
  FFV1_0(w[134], w[69], w[80], pars->GC_11, amp[1442]); 
  FFV1_0(w[70], w[119], w[83], pars->GC_11, amp[1443]); 
  FFV1_0(w[145], w[8], w[83], pars->GC_11, amp[1444]); 
  FFV1_0(w[70], w[120], w[83], pars->GC_11, amp[1445]); 
  FFV1_0(w[146], w[8], w[83], pars->GC_11, amp[1446]); 
  FFV1_0(w[70], w[121], w[73], pars->GC_11, amp[1447]); 
  FFV1_0(w[145], w[4], w[73], pars->GC_11, amp[1448]); 
  FFV1_0(w[70], w[122], w[73], pars->GC_11, amp[1449]); 
  FFV1_0(w[146], w[4], w[73], pars->GC_11, amp[1450]); 
  FFV1_0(w[6], w[155], w[76], pars->GC_11, amp[1451]); 
  FFV1_0(w[131], w[71], w[76], pars->GC_11, amp[1452]); 
  FFV1_0(w[6], w[156], w[76], pars->GC_11, amp[1453]); 
  FFV1_0(w[134], w[71], w[76], pars->GC_11, amp[1454]); 
  FFV1_0(w[6], w[157], w[85], pars->GC_11, amp[1455]); 
  FFV1_0(w[131], w[69], w[85], pars->GC_11, amp[1456]); 
  FFV1_0(w[6], w[158], w[85], pars->GC_11, amp[1457]); 
  FFV1_0(w[134], w[69], w[85], pars->GC_11, amp[1458]); 
  FFV1_0(w[151], w[4], w[83], pars->GC_11, amp[1459]); 
  FFV1_0(w[84], w[121], w[83], pars->GC_11, amp[1460]); 
  FFV1_0(w[152], w[4], w[83], pars->GC_11, amp[1461]); 
  FFV1_0(w[84], w[122], w[83], pars->GC_11, amp[1462]); 
  FFV1_0(w[70], w[121], w[89], pars->GC_11, amp[1463]); 
  FFV1_0(w[145], w[4], w[89], pars->GC_11, amp[1464]); 
  FFV1_0(w[70], w[122], w[89], pars->GC_11, amp[1465]); 
  FFV1_0(w[146], w[4], w[89], pars->GC_11, amp[1466]); 
  FFV1_0(w[143], w[69], w[76], pars->GC_11, amp[1467]); 
  FFV1_0(w[36], w[157], w[76], pars->GC_11, amp[1468]); 
  FFV1_0(w[144], w[69], w[76], pars->GC_11, amp[1469]); 
  FFV1_0(w[36], w[158], w[76], pars->GC_11, amp[1470]); 
  FFV1_0(w[6], w[157], w[96], pars->GC_11, amp[1471]); 
  FFV1_0(w[97], w[157], w[3], pars->GC_11, amp[1472]); 
  FFV1_0(w[131], w[69], w[96], pars->GC_11, amp[1473]); 
  FFV1_0(w[131], w[98], w[3], pars->GC_11, amp[1474]); 
  FFV1_0(w[6], w[158], w[96], pars->GC_11, amp[1475]); 
  FFV1_0(w[97], w[158], w[3], pars->GC_11, amp[1476]); 
  FFV1_0(w[134], w[69], w[96], pars->GC_11, amp[1477]); 
  FFV1_0(w[134], w[98], w[3], pars->GC_11, amp[1478]); 
  FFV1_0(w[70], w[121], w[93], pars->GC_11, amp[1479]); 
  FFV1_0(w[94], w[121], w[3], pars->GC_11, amp[1480]); 
  FFV1_0(w[145], w[4], w[93], pars->GC_11, amp[1481]); 
  FFV1_0(w[145], w[95], w[3], pars->GC_11, amp[1482]); 
  FFV1_0(w[70], w[122], w[93], pars->GC_11, amp[1483]); 
  FFV1_0(w[94], w[122], w[3], pars->GC_11, amp[1484]); 
  FFV1_0(w[146], w[4], w[93], pars->GC_11, amp[1485]); 
  FFV1_0(w[146], w[95], w[3], pars->GC_11, amp[1486]); 
  FFV1_0(w[6], w[137], w[72], pars->GC_11, amp[1487]); 
  FFV1_0(w[131], w[4], w[72], pars->GC_11, amp[1488]); 
  FFV1_0(w[6], w[138], w[72], pars->GC_11, amp[1489]); 
  FFV1_0(w[134], w[4], w[72], pars->GC_11, amp[1490]); 
  FFV1_0(w[70], w[155], w[30], pars->GC_11, amp[1491]); 
  FFV1_0(w[153], w[71], w[30], pars->GC_11, amp[1492]); 
  FFV1_0(w[70], w[156], w[30], pars->GC_11, amp[1493]); 
  FFV1_0(w[154], w[71], w[30], pars->GC_11, amp[1494]); 
  FFV1_0(w[70], w[157], w[11], pars->GC_11, amp[1495]); 
  FFV1_0(w[153], w[69], w[11], pars->GC_11, amp[1496]); 
  FFV1_0(w[70], w[158], w[11], pars->GC_11, amp[1497]); 
  FFV1_0(w[154], w[69], w[11], pars->GC_11, amp[1498]); 
  FFV1_0(w[6], w[135], w[82], pars->GC_11, amp[1499]); 
  FFV1_0(w[131], w[8], w[82], pars->GC_11, amp[1500]); 
  FFV1_0(w[6], w[136], w[82], pars->GC_11, amp[1501]); 
  FFV1_0(w[134], w[8], w[82], pars->GC_11, amp[1502]); 
  FFV1_0(w[6], w[137], w[86], pars->GC_11, amp[1503]); 
  FFV1_0(w[131], w[4], w[86], pars->GC_11, amp[1504]); 
  FFV1_0(w[6], w[138], w[86], pars->GC_11, amp[1505]); 
  FFV1_0(w[134], w[4], w[86], pars->GC_11, amp[1506]); 
  FFV1_0(w[159], w[69], w[30], pars->GC_11, amp[1507]); 
  FFV1_0(w[84], w[157], w[30], pars->GC_11, amp[1508]); 
  FFV1_0(w[160], w[69], w[30], pars->GC_11, amp[1509]); 
  FFV1_0(w[84], w[158], w[30], pars->GC_11, amp[1510]); 
  FFV1_0(w[70], w[157], w[38], pars->GC_11, amp[1511]); 
  FFV1_0(w[153], w[69], w[38], pars->GC_11, amp[1512]); 
  FFV1_0(w[70], w[158], w[38], pars->GC_11, amp[1513]); 
  FFV1_0(w[154], w[69], w[38], pars->GC_11, amp[1514]); 
  FFV1_0(w[143], w[4], w[82], pars->GC_11, amp[1515]); 
  FFV1_0(w[36], w[137], w[82], pars->GC_11, amp[1516]); 
  FFV1_0(w[144], w[4], w[82], pars->GC_11, amp[1517]); 
  FFV1_0(w[36], w[138], w[82], pars->GC_11, amp[1518]); 
  FFV1_0(w[6], w[137], w[90], pars->GC_11, amp[1519]); 
  FFV1_0(w[91], w[137], w[3], pars->GC_11, amp[1520]); 
  FFV1_0(w[131], w[4], w[90], pars->GC_11, amp[1521]); 
  FFV1_0(w[131], w[92], w[3], pars->GC_11, amp[1522]); 
  FFV1_0(w[6], w[138], w[90], pars->GC_11, amp[1523]); 
  FFV1_0(w[91], w[138], w[3], pars->GC_11, amp[1524]); 
  FFV1_0(w[134], w[4], w[90], pars->GC_11, amp[1525]); 
  FFV1_0(w[134], w[92], w[3], pars->GC_11, amp[1526]); 
  FFV1_0(w[70], w[157], w[44], pars->GC_11, amp[1527]); 
  FFV1_0(w[99], w[157], w[3], pars->GC_11, amp[1528]); 
  FFV1_0(w[153], w[69], w[44], pars->GC_11, amp[1529]); 
  FFV1_0(w[153], w[100], w[3], pars->GC_11, amp[1530]); 
  FFV1_0(w[70], w[158], w[44], pars->GC_11, amp[1531]); 
  FFV1_0(w[99], w[158], w[3], pars->GC_11, amp[1532]); 
  FFV1_0(w[154], w[69], w[44], pars->GC_11, amp[1533]); 
  FFV1_0(w[154], w[100], w[3], pars->GC_11, amp[1534]); 


}
double PY8MEs_R4_P15_sm_tamq_tamgqqq::matrix_4_tamu_tamguuux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 96;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (-1./3. * amp[0] - amp[1] - amp[2] - 1./3. * amp[3] -
      1./3. * amp[4] - amp[5] - amp[6] - 1./3. * amp[7] - amp[8] - amp[9] -
      amp[10] - amp[11] - 1./3. * amp[12] - 1./3. * amp[13] - 1./3. * amp[14] -
      1./3. * amp[15] - amp[32] - 1./3. * amp[33] - 1./3. * amp[34] - amp[35] -
      amp[36] - 1./3. * amp[37] - 1./3. * amp[38] - amp[39] - amp[40] - amp[41]
      - amp[42] - amp[43] - 1./3. * amp[44] - 1./3. * amp[45] - 1./3. * amp[46]
      - 1./3. * amp[47] - Complex<double> (0, 1) * amp[72] - Complex<double>
      (0, 1) * amp[74] - amp[75] - Complex<double> (0, 1) * amp[76] -
      Complex<double> (0, 1) * amp[78] - amp[79] + Complex<double> (0, 1) *
      amp[80] - amp[81] + Complex<double> (0, 1) * amp[82] + Complex<double>
      (0, 1) * amp[84] - amp[85] + Complex<double> (0, 1) * amp[86] - 1./3. *
      amp[89] - 1./3. * amp[91] - 1./3. * amp[93] - 1./3. * amp[95]);
  jamp[1] = +1./2. * (+amp[0] + 1./3. * amp[1] + 1./3. * amp[2] + amp[3] +
      amp[4] + 1./3. * amp[5] + 1./3. * amp[6] + amp[7] + 1./3. * amp[8] +
      1./3. * amp[9] + 1./3. * amp[10] + 1./3. * amp[11] + amp[12] + amp[13] +
      amp[14] + amp[15] + amp[48] + 1./3. * amp[49] + 1./3. * amp[50] + amp[51]
      + amp[52] + 1./3. * amp[53] + 1./3. * amp[54] + amp[55] + amp[56] +
      amp[57] + amp[58] + amp[59] + 1./3. * amp[60] + 1./3. * amp[61] + 1./3. *
      amp[62] + 1./3. * amp[63] + Complex<double> (0, 1) * amp[64] +
      Complex<double> (0, 1) * amp[66] + amp[67] + Complex<double> (0, 1) *
      amp[68] + Complex<double> (0, 1) * amp[70] + amp[71] + 1./3. * amp[81] +
      1./3. * amp[83] + 1./3. * amp[85] + 1./3. * amp[87] - Complex<double> (0,
      1) * amp[88] + amp[89] - Complex<double> (0, 1) * amp[90] -
      Complex<double> (0, 1) * amp[92] + amp[93] - Complex<double> (0, 1) *
      amp[94]);
  jamp[2] = +1./2. * (+1./3. * amp[16] + amp[17] + amp[18] + 1./3. * amp[19] +
      1./3. * amp[20] + amp[21] + amp[22] + 1./3. * amp[23] + amp[24] + amp[25]
      + amp[26] + amp[27] + 1./3. * amp[28] + 1./3. * amp[29] + 1./3. * amp[30]
      + 1./3. * amp[31] + 1./3. * amp[32] + amp[33] + amp[34] + 1./3. * amp[35]
      + 1./3. * amp[36] + amp[37] + amp[38] + 1./3. * amp[39] + 1./3. * amp[40]
      + 1./3. * amp[41] + 1./3. * amp[42] + 1./3. * amp[43] + amp[44] + amp[45]
      + amp[46] + amp[47] - Complex<double> (0, 1) * amp[64] + amp[65] -
      Complex<double> (0, 1) * amp[66] - Complex<double> (0, 1) * amp[68] +
      amp[69] - Complex<double> (0, 1) * amp[70] + 1./3. * amp[73] + 1./3. *
      amp[75] + 1./3. * amp[77] + 1./3. * amp[79] + Complex<double> (0, 1) *
      amp[88] + Complex<double> (0, 1) * amp[90] + amp[91] + Complex<double>
      (0, 1) * amp[92] + Complex<double> (0, 1) * amp[94] + amp[95]);
  jamp[3] = +1./2. * (-amp[16] - 1./3. * amp[17] - 1./3. * amp[18] - amp[19] -
      amp[20] - 1./3. * amp[21] - 1./3. * amp[22] - amp[23] - 1./3. * amp[24] -
      1./3. * amp[25] - 1./3. * amp[26] - 1./3. * amp[27] - amp[28] - amp[29] -
      amp[30] - amp[31] - 1./3. * amp[48] - amp[49] - amp[50] - 1./3. * amp[51]
      - 1./3. * amp[52] - amp[53] - amp[54] - 1./3. * amp[55] - 1./3. * amp[56]
      - 1./3. * amp[57] - 1./3. * amp[58] - 1./3. * amp[59] - amp[60] - amp[61]
      - amp[62] - amp[63] - 1./3. * amp[65] - 1./3. * amp[67] - 1./3. * amp[69]
      - 1./3. * amp[71] + Complex<double> (0, 1) * amp[72] - amp[73] +
      Complex<double> (0, 1) * amp[74] + Complex<double> (0, 1) * amp[76] -
      amp[77] + Complex<double> (0, 1) * amp[78] - Complex<double> (0, 1) *
      amp[80] - Complex<double> (0, 1) * amp[82] - amp[83] - Complex<double>
      (0, 1) * amp[84] - Complex<double> (0, 1) * amp[86] - amp[87]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[0][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P15_sm_tamq_tamgqqq::matrix_4_tamd_tamgdddx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 96;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (-1./3. * amp[96] - amp[97] - amp[98] - 1./3. * amp[99] -
      1./3. * amp[100] - amp[101] - amp[102] - 1./3. * amp[103] - amp[104] -
      amp[105] - amp[106] - amp[107] - 1./3. * amp[108] - 1./3. * amp[109] -
      1./3. * amp[110] - 1./3. * amp[111] - amp[128] - 1./3. * amp[129] - 1./3.
      * amp[130] - amp[131] - amp[132] - 1./3. * amp[133] - 1./3. * amp[134] -
      amp[135] - amp[136] - amp[137] - amp[138] - amp[139] - 1./3. * amp[140] -
      1./3. * amp[141] - 1./3. * amp[142] - 1./3. * amp[143] - Complex<double>
      (0, 1) * amp[168] - Complex<double> (0, 1) * amp[170] - amp[171] -
      Complex<double> (0, 1) * amp[172] - Complex<double> (0, 1) * amp[174] -
      amp[175] + Complex<double> (0, 1) * amp[176] - amp[177] + Complex<double>
      (0, 1) * amp[178] + Complex<double> (0, 1) * amp[180] - amp[181] +
      Complex<double> (0, 1) * amp[182] - 1./3. * amp[185] - 1./3. * amp[187] -
      1./3. * amp[189] - 1./3. * amp[191]);
  jamp[1] = +1./2. * (+amp[96] + 1./3. * amp[97] + 1./3. * amp[98] + amp[99] +
      amp[100] + 1./3. * amp[101] + 1./3. * amp[102] + amp[103] + 1./3. *
      amp[104] + 1./3. * amp[105] + 1./3. * amp[106] + 1./3. * amp[107] +
      amp[108] + amp[109] + amp[110] + amp[111] + amp[144] + 1./3. * amp[145] +
      1./3. * amp[146] + amp[147] + amp[148] + 1./3. * amp[149] + 1./3. *
      amp[150] + amp[151] + amp[152] + amp[153] + amp[154] + amp[155] + 1./3. *
      amp[156] + 1./3. * amp[157] + 1./3. * amp[158] + 1./3. * amp[159] +
      Complex<double> (0, 1) * amp[160] + Complex<double> (0, 1) * amp[162] +
      amp[163] + Complex<double> (0, 1) * amp[164] + Complex<double> (0, 1) *
      amp[166] + amp[167] + 1./3. * amp[177] + 1./3. * amp[179] + 1./3. *
      amp[181] + 1./3. * amp[183] - Complex<double> (0, 1) * amp[184] +
      amp[185] - Complex<double> (0, 1) * amp[186] - Complex<double> (0, 1) *
      amp[188] + amp[189] - Complex<double> (0, 1) * amp[190]);
  jamp[2] = +1./2. * (+1./3. * amp[112] + amp[113] + amp[114] + 1./3. *
      amp[115] + 1./3. * amp[116] + amp[117] + amp[118] + 1./3. * amp[119] +
      amp[120] + amp[121] + amp[122] + amp[123] + 1./3. * amp[124] + 1./3. *
      amp[125] + 1./3. * amp[126] + 1./3. * amp[127] + 1./3. * amp[128] +
      amp[129] + amp[130] + 1./3. * amp[131] + 1./3. * amp[132] + amp[133] +
      amp[134] + 1./3. * amp[135] + 1./3. * amp[136] + 1./3. * amp[137] + 1./3.
      * amp[138] + 1./3. * amp[139] + amp[140] + amp[141] + amp[142] + amp[143]
      - Complex<double> (0, 1) * amp[160] + amp[161] - Complex<double> (0, 1) *
      amp[162] - Complex<double> (0, 1) * amp[164] + amp[165] - Complex<double>
      (0, 1) * amp[166] + 1./3. * amp[169] + 1./3. * amp[171] + 1./3. *
      amp[173] + 1./3. * amp[175] + Complex<double> (0, 1) * amp[184] +
      Complex<double> (0, 1) * amp[186] + amp[187] + Complex<double> (0, 1) *
      amp[188] + Complex<double> (0, 1) * amp[190] + amp[191]);
  jamp[3] = +1./2. * (-amp[112] - 1./3. * amp[113] - 1./3. * amp[114] -
      amp[115] - amp[116] - 1./3. * amp[117] - 1./3. * amp[118] - amp[119] -
      1./3. * amp[120] - 1./3. * amp[121] - 1./3. * amp[122] - 1./3. * amp[123]
      - amp[124] - amp[125] - amp[126] - amp[127] - 1./3. * amp[144] - amp[145]
      - amp[146] - 1./3. * amp[147] - 1./3. * amp[148] - amp[149] - amp[150] -
      1./3. * amp[151] - 1./3. * amp[152] - 1./3. * amp[153] - 1./3. * amp[154]
      - 1./3. * amp[155] - amp[156] - amp[157] - amp[158] - amp[159] - 1./3. *
      amp[161] - 1./3. * amp[163] - 1./3. * amp[165] - 1./3. * amp[167] +
      Complex<double> (0, 1) * amp[168] - amp[169] + Complex<double> (0, 1) *
      amp[170] + Complex<double> (0, 1) * amp[172] - amp[173] + Complex<double>
      (0, 1) * amp[174] - Complex<double> (0, 1) * amp[176] - Complex<double>
      (0, 1) * amp[178] - amp[179] - Complex<double> (0, 1) * amp[180] -
      Complex<double> (0, 1) * amp[182] - amp[183]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[1][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P15_sm_tamq_tamgqqq::matrix_4_tamux_tamguuxux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 96;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (-amp[208] - 1./3. * amp[209] - 1./3. * amp[210] -
      amp[211] - amp[212] - 1./3. * amp[213] - 1./3. * amp[214] - amp[215] -
      1./3. * amp[216] - 1./3. * amp[217] - 1./3. * amp[218] - 1./3. * amp[219]
      - amp[220] - amp[221] - amp[222] - amp[223] - 1./3. * amp[240] - amp[241]
      - amp[242] - 1./3. * amp[243] - 1./3. * amp[244] - amp[245] - amp[246] -
      1./3. * amp[247] - 1./3. * amp[248] - 1./3. * amp[249] - 1./3. * amp[250]
      - 1./3. * amp[251] - amp[252] - amp[253] - amp[254] - amp[255] - 1./3. *
      amp[257] - 1./3. * amp[259] - 1./3. * amp[261] - 1./3. * amp[263] +
      Complex<double> (0, 1) * amp[264] - amp[265] + Complex<double> (0, 1) *
      amp[266] + Complex<double> (0, 1) * amp[268] - amp[269] + Complex<double>
      (0, 1) * amp[270] - Complex<double> (0, 1) * amp[272] - Complex<double>
      (0, 1) * amp[274] - amp[275] - Complex<double> (0, 1) * amp[276] -
      Complex<double> (0, 1) * amp[278] - amp[279]);
  jamp[1] = +1./2. * (+1./3. * amp[208] + amp[209] + amp[210] + 1./3. *
      amp[211] + 1./3. * amp[212] + amp[213] + amp[214] + 1./3. * amp[215] +
      amp[216] + amp[217] + amp[218] + amp[219] + 1./3. * amp[220] + 1./3. *
      amp[221] + 1./3. * amp[222] + 1./3. * amp[223] + 1./3. * amp[224] +
      amp[225] + amp[226] + 1./3. * amp[227] + 1./3. * amp[228] + amp[229] +
      amp[230] + 1./3. * amp[231] + 1./3. * amp[232] + 1./3. * amp[233] + 1./3.
      * amp[234] + 1./3. * amp[235] + amp[236] + amp[237] + amp[238] + amp[239]
      - Complex<double> (0, 1) * amp[256] + amp[257] - Complex<double> (0, 1) *
      amp[258] - Complex<double> (0, 1) * amp[260] + amp[261] - Complex<double>
      (0, 1) * amp[262] + 1./3. * amp[265] + 1./3. * amp[267] + 1./3. *
      amp[269] + 1./3. * amp[271] + Complex<double> (0, 1) * amp[280] +
      Complex<double> (0, 1) * amp[282] + amp[283] + Complex<double> (0, 1) *
      amp[284] + Complex<double> (0, 1) * amp[286] + amp[287]);
  jamp[2] = +1./2. * (-1./3. * amp[192] - amp[193] - amp[194] - 1./3. *
      amp[195] - 1./3. * amp[196] - amp[197] - amp[198] - 1./3. * amp[199] -
      amp[200] - amp[201] - amp[202] - amp[203] - 1./3. * amp[204] - 1./3. *
      amp[205] - 1./3. * amp[206] - 1./3. * amp[207] - amp[224] - 1./3. *
      amp[225] - 1./3. * amp[226] - amp[227] - amp[228] - 1./3. * amp[229] -
      1./3. * amp[230] - amp[231] - amp[232] - amp[233] - amp[234] - amp[235] -
      1./3. * amp[236] - 1./3. * amp[237] - 1./3. * amp[238] - 1./3. * amp[239]
      - Complex<double> (0, 1) * amp[264] - Complex<double> (0, 1) * amp[266] -
      amp[267] - Complex<double> (0, 1) * amp[268] - Complex<double> (0, 1) *
      amp[270] - amp[271] + Complex<double> (0, 1) * amp[272] - amp[273] +
      Complex<double> (0, 1) * amp[274] + Complex<double> (0, 1) * amp[276] -
      amp[277] + Complex<double> (0, 1) * amp[278] - 1./3. * amp[281] - 1./3. *
      amp[283] - 1./3. * amp[285] - 1./3. * amp[287]);
  jamp[3] = +1./2. * (+amp[192] + 1./3. * amp[193] + 1./3. * amp[194] +
      amp[195] + amp[196] + 1./3. * amp[197] + 1./3. * amp[198] + amp[199] +
      1./3. * amp[200] + 1./3. * amp[201] + 1./3. * amp[202] + 1./3. * amp[203]
      + amp[204] + amp[205] + amp[206] + amp[207] + amp[240] + 1./3. * amp[241]
      + 1./3. * amp[242] + amp[243] + amp[244] + 1./3. * amp[245] + 1./3. *
      amp[246] + amp[247] + amp[248] + amp[249] + amp[250] + amp[251] + 1./3. *
      amp[252] + 1./3. * amp[253] + 1./3. * amp[254] + 1./3. * amp[255] +
      Complex<double> (0, 1) * amp[256] + Complex<double> (0, 1) * amp[258] +
      amp[259] + Complex<double> (0, 1) * amp[260] + Complex<double> (0, 1) *
      amp[262] + amp[263] + 1./3. * amp[273] + 1./3. * amp[275] + 1./3. *
      amp[277] + 1./3. * amp[279] - Complex<double> (0, 1) * amp[280] +
      amp[281] - Complex<double> (0, 1) * amp[282] - Complex<double> (0, 1) *
      amp[284] + amp[285] - Complex<double> (0, 1) * amp[286]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[2][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P15_sm_tamq_tamgqqq::matrix_4_tamdx_tamgddxdx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 96;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (-amp[304] - 1./3. * amp[305] - 1./3. * amp[306] -
      amp[307] - amp[308] - 1./3. * amp[309] - 1./3. * amp[310] - amp[311] -
      1./3. * amp[312] - 1./3. * amp[313] - 1./3. * amp[314] - 1./3. * amp[315]
      - amp[316] - amp[317] - amp[318] - amp[319] - 1./3. * amp[336] - amp[337]
      - amp[338] - 1./3. * amp[339] - 1./3. * amp[340] - amp[341] - amp[342] -
      1./3. * amp[343] - 1./3. * amp[344] - 1./3. * amp[345] - 1./3. * amp[346]
      - 1./3. * amp[347] - amp[348] - amp[349] - amp[350] - amp[351] - 1./3. *
      amp[353] - 1./3. * amp[355] - 1./3. * amp[357] - 1./3. * amp[359] +
      Complex<double> (0, 1) * amp[360] - amp[361] + Complex<double> (0, 1) *
      amp[362] + Complex<double> (0, 1) * amp[364] - amp[365] + Complex<double>
      (0, 1) * amp[366] - Complex<double> (0, 1) * amp[368] - Complex<double>
      (0, 1) * amp[370] - amp[371] - Complex<double> (0, 1) * amp[372] -
      Complex<double> (0, 1) * amp[374] - amp[375]);
  jamp[1] = +1./2. * (+1./3. * amp[304] + amp[305] + amp[306] + 1./3. *
      amp[307] + 1./3. * amp[308] + amp[309] + amp[310] + 1./3. * amp[311] +
      amp[312] + amp[313] + amp[314] + amp[315] + 1./3. * amp[316] + 1./3. *
      amp[317] + 1./3. * amp[318] + 1./3. * amp[319] + 1./3. * amp[320] +
      amp[321] + amp[322] + 1./3. * amp[323] + 1./3. * amp[324] + amp[325] +
      amp[326] + 1./3. * amp[327] + 1./3. * amp[328] + 1./3. * amp[329] + 1./3.
      * amp[330] + 1./3. * amp[331] + amp[332] + amp[333] + amp[334] + amp[335]
      - Complex<double> (0, 1) * amp[352] + amp[353] - Complex<double> (0, 1) *
      amp[354] - Complex<double> (0, 1) * amp[356] + amp[357] - Complex<double>
      (0, 1) * amp[358] + 1./3. * amp[361] + 1./3. * amp[363] + 1./3. *
      amp[365] + 1./3. * amp[367] + Complex<double> (0, 1) * amp[376] +
      Complex<double> (0, 1) * amp[378] + amp[379] + Complex<double> (0, 1) *
      amp[380] + Complex<double> (0, 1) * amp[382] + amp[383]);
  jamp[2] = +1./2. * (-1./3. * amp[288] - amp[289] - amp[290] - 1./3. *
      amp[291] - 1./3. * amp[292] - amp[293] - amp[294] - 1./3. * amp[295] -
      amp[296] - amp[297] - amp[298] - amp[299] - 1./3. * amp[300] - 1./3. *
      amp[301] - 1./3. * amp[302] - 1./3. * amp[303] - amp[320] - 1./3. *
      amp[321] - 1./3. * amp[322] - amp[323] - amp[324] - 1./3. * amp[325] -
      1./3. * amp[326] - amp[327] - amp[328] - amp[329] - amp[330] - amp[331] -
      1./3. * amp[332] - 1./3. * amp[333] - 1./3. * amp[334] - 1./3. * amp[335]
      - Complex<double> (0, 1) * amp[360] - Complex<double> (0, 1) * amp[362] -
      amp[363] - Complex<double> (0, 1) * amp[364] - Complex<double> (0, 1) *
      amp[366] - amp[367] + Complex<double> (0, 1) * amp[368] - amp[369] +
      Complex<double> (0, 1) * amp[370] + Complex<double> (0, 1) * amp[372] -
      amp[373] + Complex<double> (0, 1) * amp[374] - 1./3. * amp[377] - 1./3. *
      amp[379] - 1./3. * amp[381] - 1./3. * amp[383]);
  jamp[3] = +1./2. * (+amp[288] + 1./3. * amp[289] + 1./3. * amp[290] +
      amp[291] + amp[292] + 1./3. * amp[293] + 1./3. * amp[294] + amp[295] +
      1./3. * amp[296] + 1./3. * amp[297] + 1./3. * amp[298] + 1./3. * amp[299]
      + amp[300] + amp[301] + amp[302] + amp[303] + amp[336] + 1./3. * amp[337]
      + 1./3. * amp[338] + amp[339] + amp[340] + 1./3. * amp[341] + 1./3. *
      amp[342] + amp[343] + amp[344] + amp[345] + amp[346] + amp[347] + 1./3. *
      amp[348] + 1./3. * amp[349] + 1./3. * amp[350] + 1./3. * amp[351] +
      Complex<double> (0, 1) * amp[352] + Complex<double> (0, 1) * amp[354] +
      amp[355] + Complex<double> (0, 1) * amp[356] + Complex<double> (0, 1) *
      amp[358] + amp[359] + 1./3. * amp[369] + 1./3. * amp[371] + 1./3. *
      amp[373] + 1./3. * amp[375] - Complex<double> (0, 1) * amp[376] +
      amp[377] - Complex<double> (0, 1) * amp[378] - Complex<double> (0, 1) *
      amp[380] + amp[381] - Complex<double> (0, 1) * amp[382]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[3][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P15_sm_tamq_tamgqqq::matrix_4_tapu_tapguuux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 96;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[384] + amp[385] + amp[386] + 1./3. *
      amp[387] + 1./3. * amp[388] + amp[389] + amp[390] + 1./3. * amp[391] +
      amp[392] + amp[393] + amp[394] + amp[395] + 1./3. * amp[396] + 1./3. *
      amp[397] + 1./3. * amp[398] + 1./3. * amp[399] + amp[416] + 1./3. *
      amp[417] + 1./3. * amp[418] + amp[419] + amp[420] + 1./3. * amp[421] +
      1./3. * amp[422] + amp[423] + amp[424] + amp[425] + amp[426] + amp[427] +
      1./3. * amp[428] + 1./3. * amp[429] + 1./3. * amp[430] + 1./3. * amp[431]
      + Complex<double> (0, 1) * amp[456] + Complex<double> (0, 1) * amp[458] +
      amp[459] + Complex<double> (0, 1) * amp[460] + Complex<double> (0, 1) *
      amp[462] + amp[463] - Complex<double> (0, 1) * amp[464] + amp[465] -
      Complex<double> (0, 1) * amp[466] - Complex<double> (0, 1) * amp[468] +
      amp[469] - Complex<double> (0, 1) * amp[470] + 1./3. * amp[473] + 1./3. *
      amp[475] + 1./3. * amp[477] + 1./3. * amp[479]);
  jamp[1] = +1./2. * (-amp[384] - 1./3. * amp[385] - 1./3. * amp[386] -
      amp[387] - amp[388] - 1./3. * amp[389] - 1./3. * amp[390] - amp[391] -
      1./3. * amp[392] - 1./3. * amp[393] - 1./3. * amp[394] - 1./3. * amp[395]
      - amp[396] - amp[397] - amp[398] - amp[399] - amp[432] - 1./3. * amp[433]
      - 1./3. * amp[434] - amp[435] - amp[436] - 1./3. * amp[437] - 1./3. *
      amp[438] - amp[439] - amp[440] - amp[441] - amp[442] - amp[443] - 1./3. *
      amp[444] - 1./3. * amp[445] - 1./3. * amp[446] - 1./3. * amp[447] -
      Complex<double> (0, 1) * amp[448] - Complex<double> (0, 1) * amp[450] -
      amp[451] - Complex<double> (0, 1) * amp[452] - Complex<double> (0, 1) *
      amp[454] - amp[455] - 1./3. * amp[465] - 1./3. * amp[467] - 1./3. *
      amp[469] - 1./3. * amp[471] + Complex<double> (0, 1) * amp[472] -
      amp[473] + Complex<double> (0, 1) * amp[474] + Complex<double> (0, 1) *
      amp[476] - amp[477] + Complex<double> (0, 1) * amp[478]);
  jamp[2] = +1./2. * (-1./3. * amp[400] - amp[401] - amp[402] - 1./3. *
      amp[403] - 1./3. * amp[404] - amp[405] - amp[406] - 1./3. * amp[407] -
      amp[408] - amp[409] - amp[410] - amp[411] - 1./3. * amp[412] - 1./3. *
      amp[413] - 1./3. * amp[414] - 1./3. * amp[415] - 1./3. * amp[416] -
      amp[417] - amp[418] - 1./3. * amp[419] - 1./3. * amp[420] - amp[421] -
      amp[422] - 1./3. * amp[423] - 1./3. * amp[424] - 1./3. * amp[425] - 1./3.
      * amp[426] - 1./3. * amp[427] - amp[428] - amp[429] - amp[430] - amp[431]
      + Complex<double> (0, 1) * amp[448] - amp[449] + Complex<double> (0, 1) *
      amp[450] + Complex<double> (0, 1) * amp[452] - amp[453] + Complex<double>
      (0, 1) * amp[454] - 1./3. * amp[457] - 1./3. * amp[459] - 1./3. *
      amp[461] - 1./3. * amp[463] - Complex<double> (0, 1) * amp[472] -
      Complex<double> (0, 1) * amp[474] - amp[475] - Complex<double> (0, 1) *
      amp[476] - Complex<double> (0, 1) * amp[478] - amp[479]);
  jamp[3] = +1./2. * (+amp[400] + 1./3. * amp[401] + 1./3. * amp[402] +
      amp[403] + amp[404] + 1./3. * amp[405] + 1./3. * amp[406] + amp[407] +
      1./3. * amp[408] + 1./3. * amp[409] + 1./3. * amp[410] + 1./3. * amp[411]
      + amp[412] + amp[413] + amp[414] + amp[415] + 1./3. * amp[432] + amp[433]
      + amp[434] + 1./3. * amp[435] + 1./3. * amp[436] + amp[437] + amp[438] +
      1./3. * amp[439] + 1./3. * amp[440] + 1./3. * amp[441] + 1./3. * amp[442]
      + 1./3. * amp[443] + amp[444] + amp[445] + amp[446] + amp[447] + 1./3. *
      amp[449] + 1./3. * amp[451] + 1./3. * amp[453] + 1./3. * amp[455] -
      Complex<double> (0, 1) * amp[456] + amp[457] - Complex<double> (0, 1) *
      amp[458] - Complex<double> (0, 1) * amp[460] + amp[461] - Complex<double>
      (0, 1) * amp[462] + Complex<double> (0, 1) * amp[464] + Complex<double>
      (0, 1) * amp[466] + amp[467] + Complex<double> (0, 1) * amp[468] +
      Complex<double> (0, 1) * amp[470] + amp[471]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[4][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P15_sm_tamq_tamgqqq::matrix_4_tapd_tapgdddx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 96;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[480] + amp[481] + amp[482] + 1./3. *
      amp[483] + 1./3. * amp[484] + amp[485] + amp[486] + 1./3. * amp[487] +
      amp[488] + amp[489] + amp[490] + amp[491] + 1./3. * amp[492] + 1./3. *
      amp[493] + 1./3. * amp[494] + 1./3. * amp[495] + amp[512] + 1./3. *
      amp[513] + 1./3. * amp[514] + amp[515] + amp[516] + 1./3. * amp[517] +
      1./3. * amp[518] + amp[519] + amp[520] + amp[521] + amp[522] + amp[523] +
      1./3. * amp[524] + 1./3. * amp[525] + 1./3. * amp[526] + 1./3. * amp[527]
      + Complex<double> (0, 1) * amp[552] + Complex<double> (0, 1) * amp[554] +
      amp[555] + Complex<double> (0, 1) * amp[556] + Complex<double> (0, 1) *
      amp[558] + amp[559] - Complex<double> (0, 1) * amp[560] + amp[561] -
      Complex<double> (0, 1) * amp[562] - Complex<double> (0, 1) * amp[564] +
      amp[565] - Complex<double> (0, 1) * amp[566] + 1./3. * amp[569] + 1./3. *
      amp[571] + 1./3. * amp[573] + 1./3. * amp[575]);
  jamp[1] = +1./2. * (-amp[480] - 1./3. * amp[481] - 1./3. * amp[482] -
      amp[483] - amp[484] - 1./3. * amp[485] - 1./3. * amp[486] - amp[487] -
      1./3. * amp[488] - 1./3. * amp[489] - 1./3. * amp[490] - 1./3. * amp[491]
      - amp[492] - amp[493] - amp[494] - amp[495] - amp[528] - 1./3. * amp[529]
      - 1./3. * amp[530] - amp[531] - amp[532] - 1./3. * amp[533] - 1./3. *
      amp[534] - amp[535] - amp[536] - amp[537] - amp[538] - amp[539] - 1./3. *
      amp[540] - 1./3. * amp[541] - 1./3. * amp[542] - 1./3. * amp[543] -
      Complex<double> (0, 1) * amp[544] - Complex<double> (0, 1) * amp[546] -
      amp[547] - Complex<double> (0, 1) * amp[548] - Complex<double> (0, 1) *
      amp[550] - amp[551] - 1./3. * amp[561] - 1./3. * amp[563] - 1./3. *
      amp[565] - 1./3. * amp[567] + Complex<double> (0, 1) * amp[568] -
      amp[569] + Complex<double> (0, 1) * amp[570] + Complex<double> (0, 1) *
      amp[572] - amp[573] + Complex<double> (0, 1) * amp[574]);
  jamp[2] = +1./2. * (-1./3. * amp[496] - amp[497] - amp[498] - 1./3. *
      amp[499] - 1./3. * amp[500] - amp[501] - amp[502] - 1./3. * amp[503] -
      amp[504] - amp[505] - amp[506] - amp[507] - 1./3. * amp[508] - 1./3. *
      amp[509] - 1./3. * amp[510] - 1./3. * amp[511] - 1./3. * amp[512] -
      amp[513] - amp[514] - 1./3. * amp[515] - 1./3. * amp[516] - amp[517] -
      amp[518] - 1./3. * amp[519] - 1./3. * amp[520] - 1./3. * amp[521] - 1./3.
      * amp[522] - 1./3. * amp[523] - amp[524] - amp[525] - amp[526] - amp[527]
      + Complex<double> (0, 1) * amp[544] - amp[545] + Complex<double> (0, 1) *
      amp[546] + Complex<double> (0, 1) * amp[548] - amp[549] + Complex<double>
      (0, 1) * amp[550] - 1./3. * amp[553] - 1./3. * amp[555] - 1./3. *
      amp[557] - 1./3. * amp[559] - Complex<double> (0, 1) * amp[568] -
      Complex<double> (0, 1) * amp[570] - amp[571] - Complex<double> (0, 1) *
      amp[572] - Complex<double> (0, 1) * amp[574] - amp[575]);
  jamp[3] = +1./2. * (+amp[496] + 1./3. * amp[497] + 1./3. * amp[498] +
      amp[499] + amp[500] + 1./3. * amp[501] + 1./3. * amp[502] + amp[503] +
      1./3. * amp[504] + 1./3. * amp[505] + 1./3. * amp[506] + 1./3. * amp[507]
      + amp[508] + amp[509] + amp[510] + amp[511] + 1./3. * amp[528] + amp[529]
      + amp[530] + 1./3. * amp[531] + 1./3. * amp[532] + amp[533] + amp[534] +
      1./3. * amp[535] + 1./3. * amp[536] + 1./3. * amp[537] + 1./3. * amp[538]
      + 1./3. * amp[539] + amp[540] + amp[541] + amp[542] + amp[543] + 1./3. *
      amp[545] + 1./3. * amp[547] + 1./3. * amp[549] + 1./3. * amp[551] -
      Complex<double> (0, 1) * amp[552] + amp[553] - Complex<double> (0, 1) *
      amp[554] - Complex<double> (0, 1) * amp[556] + amp[557] - Complex<double>
      (0, 1) * amp[558] + Complex<double> (0, 1) * amp[560] + Complex<double>
      (0, 1) * amp[562] + amp[563] + Complex<double> (0, 1) * amp[564] +
      Complex<double> (0, 1) * amp[566] + amp[567]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[5][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P15_sm_tamq_tamgqqq::matrix_4_tapux_tapguuxux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 96;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[592] + 1./3. * amp[593] + 1./3. * amp[594] +
      amp[595] + amp[596] + 1./3. * amp[597] + 1./3. * amp[598] + amp[599] +
      1./3. * amp[600] + 1./3. * amp[601] + 1./3. * amp[602] + 1./3. * amp[603]
      + amp[604] + amp[605] + amp[606] + amp[607] + 1./3. * amp[624] + amp[625]
      + amp[626] + 1./3. * amp[627] + 1./3. * amp[628] + amp[629] + amp[630] +
      1./3. * amp[631] + 1./3. * amp[632] + 1./3. * amp[633] + 1./3. * amp[634]
      + 1./3. * amp[635] + amp[636] + amp[637] + amp[638] + amp[639] + 1./3. *
      amp[641] + 1./3. * amp[643] + 1./3. * amp[645] + 1./3. * amp[647] -
      Complex<double> (0, 1) * amp[648] + amp[649] - Complex<double> (0, 1) *
      amp[650] - Complex<double> (0, 1) * amp[652] + amp[653] - Complex<double>
      (0, 1) * amp[654] + Complex<double> (0, 1) * amp[656] + Complex<double>
      (0, 1) * amp[658] + amp[659] + Complex<double> (0, 1) * amp[660] +
      Complex<double> (0, 1) * amp[662] + amp[663]);
  jamp[1] = +1./2. * (-1./3. * amp[592] - amp[593] - amp[594] - 1./3. *
      amp[595] - 1./3. * amp[596] - amp[597] - amp[598] - 1./3. * amp[599] -
      amp[600] - amp[601] - amp[602] - amp[603] - 1./3. * amp[604] - 1./3. *
      amp[605] - 1./3. * amp[606] - 1./3. * amp[607] - 1./3. * amp[608] -
      amp[609] - amp[610] - 1./3. * amp[611] - 1./3. * amp[612] - amp[613] -
      amp[614] - 1./3. * amp[615] - 1./3. * amp[616] - 1./3. * amp[617] - 1./3.
      * amp[618] - 1./3. * amp[619] - amp[620] - amp[621] - amp[622] - amp[623]
      + Complex<double> (0, 1) * amp[640] - amp[641] + Complex<double> (0, 1) *
      amp[642] + Complex<double> (0, 1) * amp[644] - amp[645] + Complex<double>
      (0, 1) * amp[646] - 1./3. * amp[649] - 1./3. * amp[651] - 1./3. *
      amp[653] - 1./3. * amp[655] - Complex<double> (0, 1) * amp[664] -
      Complex<double> (0, 1) * amp[666] - amp[667] - Complex<double> (0, 1) *
      amp[668] - Complex<double> (0, 1) * amp[670] - amp[671]);
  jamp[2] = +1./2. * (+1./3. * amp[576] + amp[577] + amp[578] + 1./3. *
      amp[579] + 1./3. * amp[580] + amp[581] + amp[582] + 1./3. * amp[583] +
      amp[584] + amp[585] + amp[586] + amp[587] + 1./3. * amp[588] + 1./3. *
      amp[589] + 1./3. * amp[590] + 1./3. * amp[591] + amp[608] + 1./3. *
      amp[609] + 1./3. * amp[610] + amp[611] + amp[612] + 1./3. * amp[613] +
      1./3. * amp[614] + amp[615] + amp[616] + amp[617] + amp[618] + amp[619] +
      1./3. * amp[620] + 1./3. * amp[621] + 1./3. * amp[622] + 1./3. * amp[623]
      + Complex<double> (0, 1) * amp[648] + Complex<double> (0, 1) * amp[650] +
      amp[651] + Complex<double> (0, 1) * amp[652] + Complex<double> (0, 1) *
      amp[654] + amp[655] - Complex<double> (0, 1) * amp[656] + amp[657] -
      Complex<double> (0, 1) * amp[658] - Complex<double> (0, 1) * amp[660] +
      amp[661] - Complex<double> (0, 1) * amp[662] + 1./3. * amp[665] + 1./3. *
      amp[667] + 1./3. * amp[669] + 1./3. * amp[671]);
  jamp[3] = +1./2. * (-amp[576] - 1./3. * amp[577] - 1./3. * amp[578] -
      amp[579] - amp[580] - 1./3. * amp[581] - 1./3. * amp[582] - amp[583] -
      1./3. * amp[584] - 1./3. * amp[585] - 1./3. * amp[586] - 1./3. * amp[587]
      - amp[588] - amp[589] - amp[590] - amp[591] - amp[624] - 1./3. * amp[625]
      - 1./3. * amp[626] - amp[627] - amp[628] - 1./3. * amp[629] - 1./3. *
      amp[630] - amp[631] - amp[632] - amp[633] - amp[634] - amp[635] - 1./3. *
      amp[636] - 1./3. * amp[637] - 1./3. * amp[638] - 1./3. * amp[639] -
      Complex<double> (0, 1) * amp[640] - Complex<double> (0, 1) * amp[642] -
      amp[643] - Complex<double> (0, 1) * amp[644] - Complex<double> (0, 1) *
      amp[646] - amp[647] - 1./3. * amp[657] - 1./3. * amp[659] - 1./3. *
      amp[661] - 1./3. * amp[663] + Complex<double> (0, 1) * amp[664] -
      amp[665] + Complex<double> (0, 1) * amp[666] + Complex<double> (0, 1) *
      amp[668] - amp[669] + Complex<double> (0, 1) * amp[670]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[6][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P15_sm_tamq_tamgqqq::matrix_4_tapdx_tapgddxdx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 96;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[688] + 1./3. * amp[689] + 1./3. * amp[690] +
      amp[691] + amp[692] + 1./3. * amp[693] + 1./3. * amp[694] + amp[695] +
      1./3. * amp[696] + 1./3. * amp[697] + 1./3. * amp[698] + 1./3. * amp[699]
      + amp[700] + amp[701] + amp[702] + amp[703] + 1./3. * amp[720] + amp[721]
      + amp[722] + 1./3. * amp[723] + 1./3. * amp[724] + amp[725] + amp[726] +
      1./3. * amp[727] + 1./3. * amp[728] + 1./3. * amp[729] + 1./3. * amp[730]
      + 1./3. * amp[731] + amp[732] + amp[733] + amp[734] + amp[735] + 1./3. *
      amp[737] + 1./3. * amp[739] + 1./3. * amp[741] + 1./3. * amp[743] -
      Complex<double> (0, 1) * amp[744] + amp[745] - Complex<double> (0, 1) *
      amp[746] - Complex<double> (0, 1) * amp[748] + amp[749] - Complex<double>
      (0, 1) * amp[750] + Complex<double> (0, 1) * amp[752] + Complex<double>
      (0, 1) * amp[754] + amp[755] + Complex<double> (0, 1) * amp[756] +
      Complex<double> (0, 1) * amp[758] + amp[759]);
  jamp[1] = +1./2. * (-1./3. * amp[688] - amp[689] - amp[690] - 1./3. *
      amp[691] - 1./3. * amp[692] - amp[693] - amp[694] - 1./3. * amp[695] -
      amp[696] - amp[697] - amp[698] - amp[699] - 1./3. * amp[700] - 1./3. *
      amp[701] - 1./3. * amp[702] - 1./3. * amp[703] - 1./3. * amp[704] -
      amp[705] - amp[706] - 1./3. * amp[707] - 1./3. * amp[708] - amp[709] -
      amp[710] - 1./3. * amp[711] - 1./3. * amp[712] - 1./3. * amp[713] - 1./3.
      * amp[714] - 1./3. * amp[715] - amp[716] - amp[717] - amp[718] - amp[719]
      + Complex<double> (0, 1) * amp[736] - amp[737] + Complex<double> (0, 1) *
      amp[738] + Complex<double> (0, 1) * amp[740] - amp[741] + Complex<double>
      (0, 1) * amp[742] - 1./3. * amp[745] - 1./3. * amp[747] - 1./3. *
      amp[749] - 1./3. * amp[751] - Complex<double> (0, 1) * amp[760] -
      Complex<double> (0, 1) * amp[762] - amp[763] - Complex<double> (0, 1) *
      amp[764] - Complex<double> (0, 1) * amp[766] - amp[767]);
  jamp[2] = +1./2. * (+1./3. * amp[672] + amp[673] + amp[674] + 1./3. *
      amp[675] + 1./3. * amp[676] + amp[677] + amp[678] + 1./3. * amp[679] +
      amp[680] + amp[681] + amp[682] + amp[683] + 1./3. * amp[684] + 1./3. *
      amp[685] + 1./3. * amp[686] + 1./3. * amp[687] + amp[704] + 1./3. *
      amp[705] + 1./3. * amp[706] + amp[707] + amp[708] + 1./3. * amp[709] +
      1./3. * amp[710] + amp[711] + amp[712] + amp[713] + amp[714] + amp[715] +
      1./3. * amp[716] + 1./3. * amp[717] + 1./3. * amp[718] + 1./3. * amp[719]
      + Complex<double> (0, 1) * amp[744] + Complex<double> (0, 1) * amp[746] +
      amp[747] + Complex<double> (0, 1) * amp[748] + Complex<double> (0, 1) *
      amp[750] + amp[751] - Complex<double> (0, 1) * amp[752] + amp[753] -
      Complex<double> (0, 1) * amp[754] - Complex<double> (0, 1) * amp[756] +
      amp[757] - Complex<double> (0, 1) * amp[758] + 1./3. * amp[761] + 1./3. *
      amp[763] + 1./3. * amp[765] + 1./3. * amp[767]);
  jamp[3] = +1./2. * (-amp[672] - 1./3. * amp[673] - 1./3. * amp[674] -
      amp[675] - amp[676] - 1./3. * amp[677] - 1./3. * amp[678] - amp[679] -
      1./3. * amp[680] - 1./3. * amp[681] - 1./3. * amp[682] - 1./3. * amp[683]
      - amp[684] - amp[685] - amp[686] - amp[687] - amp[720] - 1./3. * amp[721]
      - 1./3. * amp[722] - amp[723] - amp[724] - 1./3. * amp[725] - 1./3. *
      amp[726] - amp[727] - amp[728] - amp[729] - amp[730] - amp[731] - 1./3. *
      amp[732] - 1./3. * amp[733] - 1./3. * amp[734] - 1./3. * amp[735] -
      Complex<double> (0, 1) * amp[736] - Complex<double> (0, 1) * amp[738] -
      amp[739] - Complex<double> (0, 1) * amp[740] - Complex<double> (0, 1) *
      amp[742] - amp[743] - 1./3. * amp[753] - 1./3. * amp[755] - 1./3. *
      amp[757] - 1./3. * amp[759] + Complex<double> (0, 1) * amp[760] -
      amp[761] + Complex<double> (0, 1) * amp[762] + Complex<double> (0, 1) *
      amp[764] - amp[765] + Complex<double> (0, 1) * amp[766]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[7][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P15_sm_tamq_tamgqqq::matrix_4_tamu_tamguccx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 48;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (-1./3. * amp[0] - 1./3. * amp[768] - 1./3. * amp[769] -
      1./3. * amp[770] - 1./3. * amp[771] - 1./3. * amp[772] - 1./3. * amp[773]
      - 1./3. * amp[774] - 1./3. * amp[783] - 1./3. * amp[784] - 1./3. *
      amp[785] - 1./3. * amp[786] - 1./3. * amp[787] - 1./3. * amp[788] - 1./3.
      * amp[789] - 1./3. * amp[790] - 1./3. * amp[808] - 1./3. * amp[810] -
      1./3. * amp[812] - 1./3. * amp[814]);
  jamp[1] = +1./2. * (+amp[0] + amp[768] + amp[769] + amp[770] + amp[771] +
      amp[772] + amp[773] + amp[774] + amp[791] + amp[792] + amp[793] +
      amp[794] + amp[795] + amp[796] + amp[797] + amp[798] + Complex<double>
      (0, 1) * amp[799] + Complex<double> (0, 1) * amp[801] + amp[802] +
      Complex<double> (0, 1) * amp[803] + Complex<double> (0, 1) * amp[805] +
      amp[806] - Complex<double> (0, 1) * amp[807] + amp[808] - Complex<double>
      (0, 1) * amp[809] - Complex<double> (0, 1) * amp[811] + amp[812] -
      Complex<double> (0, 1) * amp[813]);
  jamp[2] = +1./2. * (+amp[775] + amp[776] + amp[777] + amp[778] + amp[779] +
      amp[780] + amp[781] + amp[782] + amp[783] + amp[784] + amp[785] +
      amp[786] + amp[787] + amp[788] + amp[789] + amp[790] - Complex<double>
      (0, 1) * amp[799] + amp[800] - Complex<double> (0, 1) * amp[801] -
      Complex<double> (0, 1) * amp[803] + amp[804] - Complex<double> (0, 1) *
      amp[805] + Complex<double> (0, 1) * amp[807] + Complex<double> (0, 1) *
      amp[809] + amp[810] + Complex<double> (0, 1) * amp[811] + Complex<double>
      (0, 1) * amp[813] + amp[814]);
  jamp[3] = +1./2. * (-1./3. * amp[775] - 1./3. * amp[776] - 1./3. * amp[777] -
      1./3. * amp[778] - 1./3. * amp[779] - 1./3. * amp[780] - 1./3. * amp[781]
      - 1./3. * amp[782] - 1./3. * amp[791] - 1./3. * amp[792] - 1./3. *
      amp[793] - 1./3. * amp[794] - 1./3. * amp[795] - 1./3. * amp[796] - 1./3.
      * amp[797] - 1./3. * amp[798] - 1./3. * amp[800] - 1./3. * amp[802] -
      1./3. * amp[804] - 1./3. * amp[806]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[8][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P15_sm_tamq_tamgqqq::matrix_4_tamu_tamguddx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 48;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (-1./3. * amp[815] - 1./3. * amp[816] - 1./3. * amp[817] -
      1./3. * amp[818] - 1./3. * amp[819] - 1./3. * amp[820] - 1./3. * amp[821]
      - 1./3. * amp[822] - 1./3. * amp[831] - 1./3. * amp[832] - 1./3. *
      amp[833] - 1./3. * amp[834] - 1./3. * amp[835] - 1./3. * amp[836] - 1./3.
      * amp[837] - 1./3. * amp[838] - 1./3. * amp[856] - 1./3. * amp[858] -
      1./3. * amp[860] - 1./3. * amp[862]);
  jamp[1] = +1./2. * (+amp[815] + amp[816] + amp[817] + amp[818] + amp[819] +
      amp[820] + amp[821] + amp[822] + amp[839] + amp[840] + amp[841] +
      amp[842] + amp[843] + amp[844] + amp[845] + amp[846] + Complex<double>
      (0, 1) * amp[847] + Complex<double> (0, 1) * amp[849] + amp[850] +
      Complex<double> (0, 1) * amp[851] + Complex<double> (0, 1) * amp[853] +
      amp[854] - Complex<double> (0, 1) * amp[855] + amp[856] - Complex<double>
      (0, 1) * amp[857] - Complex<double> (0, 1) * amp[859] + amp[860] -
      Complex<double> (0, 1) * amp[861]);
  jamp[2] = +1./2. * (+amp[823] + amp[824] + amp[825] + amp[826] + amp[827] +
      amp[828] + amp[829] + amp[830] + amp[831] + amp[832] + amp[833] +
      amp[834] + amp[835] + amp[836] + amp[837] + amp[838] - Complex<double>
      (0, 1) * amp[847] + amp[848] - Complex<double> (0, 1) * amp[849] -
      Complex<double> (0, 1) * amp[851] + amp[852] - Complex<double> (0, 1) *
      amp[853] + Complex<double> (0, 1) * amp[855] + Complex<double> (0, 1) *
      amp[857] + amp[858] + Complex<double> (0, 1) * amp[859] + Complex<double>
      (0, 1) * amp[861] + amp[862]);
  jamp[3] = +1./2. * (-1./3. * amp[823] - 1./3. * amp[824] - 1./3. * amp[825] -
      1./3. * amp[826] - 1./3. * amp[827] - 1./3. * amp[828] - 1./3. * amp[829]
      - 1./3. * amp[830] - 1./3. * amp[839] - 1./3. * amp[840] - 1./3. *
      amp[841] - 1./3. * amp[842] - 1./3. * amp[843] - 1./3. * amp[844] - 1./3.
      * amp[845] - 1./3. * amp[846] - 1./3. * amp[848] - 1./3. * amp[850] -
      1./3. * amp[852] - 1./3. * amp[854]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[9][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P15_sm_tamq_tamgqqq::matrix_4_tamd_tamgudux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 48;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (-amp[863] - amp[864] - amp[865] - amp[866] - amp[867] -
      amp[868] - amp[869] - amp[870] - amp[887] - amp[888] - amp[889] -
      amp[890] - amp[891] - amp[892] - amp[893] - amp[894] - Complex<double>
      (0, 1) * amp[895] - Complex<double> (0, 1) * amp[897] - amp[898] -
      Complex<double> (0, 1) * amp[899] - Complex<double> (0, 1) * amp[901] -
      amp[902] + Complex<double> (0, 1) * amp[903] - amp[904] + Complex<double>
      (0, 1) * amp[905] + Complex<double> (0, 1) * amp[907] - amp[908] +
      Complex<double> (0, 1) * amp[909]);
  jamp[1] = +1./2. * (+1./3. * amp[863] + 1./3. * amp[864] + 1./3. * amp[865] +
      1./3. * amp[866] + 1./3. * amp[867] + 1./3. * amp[868] + 1./3. * amp[869]
      + 1./3. * amp[870] + 1./3. * amp[879] + 1./3. * amp[880] + 1./3. *
      amp[881] + 1./3. * amp[882] + 1./3. * amp[883] + 1./3. * amp[884] + 1./3.
      * amp[885] + 1./3. * amp[886] + 1./3. * amp[904] + 1./3. * amp[906] +
      1./3. * amp[908] + 1./3. * amp[910]);
  jamp[2] = +1./2. * (+1./3. * amp[871] + 1./3. * amp[872] + 1./3. * amp[873] +
      1./3. * amp[874] + 1./3. * amp[875] + 1./3. * amp[876] + 1./3. * amp[877]
      + 1./3. * amp[878] + 1./3. * amp[887] + 1./3. * amp[888] + 1./3. *
      amp[889] + 1./3. * amp[890] + 1./3. * amp[891] + 1./3. * amp[892] + 1./3.
      * amp[893] + 1./3. * amp[894] + 1./3. * amp[896] + 1./3. * amp[898] +
      1./3. * amp[900] + 1./3. * amp[902]);
  jamp[3] = +1./2. * (-amp[871] - amp[872] - amp[873] - amp[874] - amp[875] -
      amp[876] - amp[877] - amp[878] - amp[879] - amp[880] - amp[881] -
      amp[882] - amp[883] - amp[884] - amp[885] - amp[886] + Complex<double>
      (0, 1) * amp[895] - amp[896] + Complex<double> (0, 1) * amp[897] +
      Complex<double> (0, 1) * amp[899] - amp[900] + Complex<double> (0, 1) *
      amp[901] - Complex<double> (0, 1) * amp[903] - Complex<double> (0, 1) *
      amp[905] - amp[906] - Complex<double> (0, 1) * amp[907] - Complex<double>
      (0, 1) * amp[909] - amp[910]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[10][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P15_sm_tamq_tamgqqq::matrix_4_tamd_tamgdssx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 48;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (-1./3. * amp[911] - 1./3. * amp[912] - 1./3. * amp[913] -
      1./3. * amp[914] - 1./3. * amp[915] - 1./3. * amp[916] - 1./3. * amp[917]
      - 1./3. * amp[918] - 1./3. * amp[927] - 1./3. * amp[928] - 1./3. *
      amp[929] - 1./3. * amp[930] - 1./3. * amp[931] - 1./3. * amp[932] - 1./3.
      * amp[933] - 1./3. * amp[934] - 1./3. * amp[952] - 1./3. * amp[954] -
      1./3. * amp[956] - 1./3. * amp[958]);
  jamp[1] = +1./2. * (+amp[911] + amp[912] + amp[913] + amp[914] + amp[915] +
      amp[916] + amp[917] + amp[918] + amp[935] + amp[936] + amp[937] +
      amp[938] + amp[939] + amp[940] + amp[941] + amp[942] + Complex<double>
      (0, 1) * amp[943] + Complex<double> (0, 1) * amp[945] + amp[946] +
      Complex<double> (0, 1) * amp[947] + Complex<double> (0, 1) * amp[949] +
      amp[950] - Complex<double> (0, 1) * amp[951] + amp[952] - Complex<double>
      (0, 1) * amp[953] - Complex<double> (0, 1) * amp[955] + amp[956] -
      Complex<double> (0, 1) * amp[957]);
  jamp[2] = +1./2. * (+amp[919] + amp[920] + amp[921] + amp[922] + amp[923] +
      amp[924] + amp[925] + amp[926] + amp[927] + amp[928] + amp[929] +
      amp[930] + amp[931] + amp[932] + amp[933] + amp[934] - Complex<double>
      (0, 1) * amp[943] + amp[944] - Complex<double> (0, 1) * amp[945] -
      Complex<double> (0, 1) * amp[947] + amp[948] - Complex<double> (0, 1) *
      amp[949] + Complex<double> (0, 1) * amp[951] + Complex<double> (0, 1) *
      amp[953] + amp[954] + Complex<double> (0, 1) * amp[955] + Complex<double>
      (0, 1) * amp[957] + amp[958]);
  jamp[3] = +1./2. * (-1./3. * amp[919] - 1./3. * amp[920] - 1./3. * amp[921] -
      1./3. * amp[922] - 1./3. * amp[923] - 1./3. * amp[924] - 1./3. * amp[925]
      - 1./3. * amp[926] - 1./3. * amp[935] - 1./3. * amp[936] - 1./3. *
      amp[937] - 1./3. * amp[938] - 1./3. * amp[939] - 1./3. * amp[940] - 1./3.
      * amp[941] - 1./3. * amp[942] - 1./3. * amp[944] - 1./3. * amp[946] -
      1./3. * amp[948] - 1./3. * amp[950]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[11][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P15_sm_tamq_tamgqqq::matrix_4_tamux_tamgcuxcx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 48;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (-1./3. * amp[967] - 1./3. * amp[968] - 1./3. * amp[969] -
      1./3. * amp[970] - 1./3. * amp[971] - 1./3. * amp[972] - 1./3. * amp[973]
      - 1./3. * amp[974] - 1./3. * amp[983] - 1./3. * amp[984] - 1./3. *
      amp[985] - 1./3. * amp[986] - 1./3. * amp[987] - 1./3. * amp[988] - 1./3.
      * amp[989] - 1./3. * amp[990] - 1./3. * amp[992] - 1./3. * amp[994] -
      1./3. * amp[996] - 1./3. * amp[998]);
  jamp[1] = +1./2. * (+amp[967] + amp[968] + amp[969] + amp[970] + amp[971] +
      amp[972] + amp[973] + amp[974] + amp[975] + amp[976] + amp[977] +
      amp[978] + amp[979] + amp[980] + amp[981] + amp[982] - Complex<double>
      (0, 1) * amp[991] + amp[992] - Complex<double> (0, 1) * amp[993] -
      Complex<double> (0, 1) * amp[995] + amp[996] - Complex<double> (0, 1) *
      amp[997] + Complex<double> (0, 1) * amp[999] + Complex<double> (0, 1) *
      amp[1001] + amp[1002] + Complex<double> (0, 1) * amp[1003] +
      Complex<double> (0, 1) * amp[1005] + amp[1006]);
  jamp[2] = +1./2. * (-1./3. * amp[959] - 1./3. * amp[960] - 1./3. * amp[961] -
      1./3. * amp[962] - 1./3. * amp[963] - 1./3. * amp[964] - 1./3. * amp[965]
      - 1./3. * amp[966] - 1./3. * amp[975] - 1./3. * amp[976] - 1./3. *
      amp[977] - 1./3. * amp[978] - 1./3. * amp[979] - 1./3. * amp[980] - 1./3.
      * amp[981] - 1./3. * amp[982] - 1./3. * amp[1000] - 1./3. * amp[1002] -
      1./3. * amp[1004] - 1./3. * amp[1006]);
  jamp[3] = +1./2. * (+amp[959] + amp[960] + amp[961] + amp[962] + amp[963] +
      amp[964] + amp[965] + amp[966] + amp[983] + amp[984] + amp[985] +
      amp[986] + amp[987] + amp[988] + amp[989] + amp[990] + Complex<double>
      (0, 1) * amp[991] + Complex<double> (0, 1) * amp[993] + amp[994] +
      Complex<double> (0, 1) * amp[995] + Complex<double> (0, 1) * amp[997] +
      amp[998] - Complex<double> (0, 1) * amp[999] + amp[1000] -
      Complex<double> (0, 1) * amp[1001] - Complex<double> (0, 1) * amp[1003] +
      amp[1004] - Complex<double> (0, 1) * amp[1005]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[12][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P15_sm_tamq_tamgqqq::matrix_4_tamux_tamgduxdx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 48;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (-1./3. * amp[1015] - 1./3. * amp[1016] - 1./3. *
      amp[1017] - 1./3. * amp[1018] - 1./3. * amp[1019] - 1./3. * amp[1020] -
      1./3. * amp[1021] - 1./3. * amp[1022] - 1./3. * amp[1031] - 1./3. *
      amp[1032] - 1./3. * amp[1033] - 1./3. * amp[1034] - 1./3. * amp[1035] -
      1./3. * amp[1036] - 1./3. * amp[1037] - 1./3. * amp[1038] - 1./3. *
      amp[1040] - 1./3. * amp[1042] - 1./3. * amp[1044] - 1./3. * amp[1046]);
  jamp[1] = +1./2. * (+amp[1015] + amp[1016] + amp[1017] + amp[1018] +
      amp[1019] + amp[1020] + amp[1021] + amp[1022] + amp[1023] + amp[1024] +
      amp[1025] + amp[1026] + amp[1027] + amp[1028] + amp[1029] + amp[1030] -
      Complex<double> (0, 1) * amp[1039] + amp[1040] - Complex<double> (0, 1) *
      amp[1041] - Complex<double> (0, 1) * amp[1043] + amp[1044] -
      Complex<double> (0, 1) * amp[1045] + Complex<double> (0, 1) * amp[1047] +
      Complex<double> (0, 1) * amp[1049] + amp[1050] + Complex<double> (0, 1) *
      amp[1051] + Complex<double> (0, 1) * amp[1053] + amp[1054]);
  jamp[2] = +1./2. * (-1./3. * amp[1007] - 1./3. * amp[1008] - 1./3. *
      amp[1009] - 1./3. * amp[1010] - 1./3. * amp[1011] - 1./3. * amp[1012] -
      1./3. * amp[1013] - 1./3. * amp[1014] - 1./3. * amp[1023] - 1./3. *
      amp[1024] - 1./3. * amp[1025] - 1./3. * amp[1026] - 1./3. * amp[1027] -
      1./3. * amp[1028] - 1./3. * amp[1029] - 1./3. * amp[1030] - 1./3. *
      amp[1048] - 1./3. * amp[1050] - 1./3. * amp[1052] - 1./3. * amp[1054]);
  jamp[3] = +1./2. * (+amp[1007] + amp[1008] + amp[1009] + amp[1010] +
      amp[1011] + amp[1012] + amp[1013] + amp[1014] + amp[1031] + amp[1032] +
      amp[1033] + amp[1034] + amp[1035] + amp[1036] + amp[1037] + amp[1038] +
      Complex<double> (0, 1) * amp[1039] + Complex<double> (0, 1) * amp[1041] +
      amp[1042] + Complex<double> (0, 1) * amp[1043] + Complex<double> (0, 1) *
      amp[1045] + amp[1046] - Complex<double> (0, 1) * amp[1047] + amp[1048] -
      Complex<double> (0, 1) * amp[1049] - Complex<double> (0, 1) * amp[1051] +
      amp[1052] - Complex<double> (0, 1) * amp[1053]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[13][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P15_sm_tamq_tamgqqq::matrix_4_tamdx_tamguuxdx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 48;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (-amp[1055] - amp[1056] - amp[1057] - amp[1058] -
      amp[1059] - amp[1060] - amp[1061] - amp[1062] - amp[1079] - amp[1080] -
      amp[1081] - amp[1082] - amp[1083] - amp[1084] - amp[1085] - amp[1086] -
      Complex<double> (0, 1) * amp[1087] - Complex<double> (0, 1) * amp[1089] -
      amp[1090] - Complex<double> (0, 1) * amp[1091] - Complex<double> (0, 1) *
      amp[1093] - amp[1094] + Complex<double> (0, 1) * amp[1095] - amp[1096] +
      Complex<double> (0, 1) * amp[1097] + Complex<double> (0, 1) * amp[1099] -
      amp[1100] + Complex<double> (0, 1) * amp[1101]);
  jamp[1] = +1./2. * (+1./3. * amp[1055] + 1./3. * amp[1056] + 1./3. *
      amp[1057] + 1./3. * amp[1058] + 1./3. * amp[1059] + 1./3. * amp[1060] +
      1./3. * amp[1061] + 1./3. * amp[1062] + 1./3. * amp[1071] + 1./3. *
      amp[1072] + 1./3. * amp[1073] + 1./3. * amp[1074] + 1./3. * amp[1075] +
      1./3. * amp[1076] + 1./3. * amp[1077] + 1./3. * amp[1078] + 1./3. *
      amp[1096] + 1./3. * amp[1098] + 1./3. * amp[1100] + 1./3. * amp[1102]);
  jamp[2] = +1./2. * (-amp[1063] - amp[1064] - amp[1065] - amp[1066] -
      amp[1067] - amp[1068] - amp[1069] - amp[1070] - amp[1071] - amp[1072] -
      amp[1073] - amp[1074] - amp[1075] - amp[1076] - amp[1077] - amp[1078] +
      Complex<double> (0, 1) * amp[1087] - amp[1088] + Complex<double> (0, 1) *
      amp[1089] + Complex<double> (0, 1) * amp[1091] - amp[1092] +
      Complex<double> (0, 1) * amp[1093] - Complex<double> (0, 1) * amp[1095] -
      Complex<double> (0, 1) * amp[1097] - amp[1098] - Complex<double> (0, 1) *
      amp[1099] - Complex<double> (0, 1) * amp[1101] - amp[1102]);
  jamp[3] = +1./2. * (+1./3. * amp[1063] + 1./3. * amp[1064] + 1./3. *
      amp[1065] + 1./3. * amp[1066] + 1./3. * amp[1067] + 1./3. * amp[1068] +
      1./3. * amp[1069] + 1./3. * amp[1070] + 1./3. * amp[1079] + 1./3. *
      amp[1080] + 1./3. * amp[1081] + 1./3. * amp[1082] + 1./3. * amp[1083] +
      1./3. * amp[1084] + 1./3. * amp[1085] + 1./3. * amp[1086] + 1./3. *
      amp[1088] + 1./3. * amp[1090] + 1./3. * amp[1092] + 1./3. * amp[1094]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[14][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P15_sm_tamq_tamgqqq::matrix_4_tamdx_tamgsdxsx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 48;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (-1./3. * amp[1111] - 1./3. * amp[1112] - 1./3. *
      amp[1113] - 1./3. * amp[1114] - 1./3. * amp[1115] - 1./3. * amp[1116] -
      1./3. * amp[1117] - 1./3. * amp[1118] - 1./3. * amp[1127] - 1./3. *
      amp[1128] - 1./3. * amp[1129] - 1./3. * amp[1130] - 1./3. * amp[1131] -
      1./3. * amp[1132] - 1./3. * amp[1133] - 1./3. * amp[1134] - 1./3. *
      amp[1136] - 1./3. * amp[1138] - 1./3. * amp[1140] - 1./3. * amp[1142]);
  jamp[1] = +1./2. * (+amp[1111] + amp[1112] + amp[1113] + amp[1114] +
      amp[1115] + amp[1116] + amp[1117] + amp[1118] + amp[1119] + amp[1120] +
      amp[1121] + amp[1122] + amp[1123] + amp[1124] + amp[1125] + amp[1126] -
      Complex<double> (0, 1) * amp[1135] + amp[1136] - Complex<double> (0, 1) *
      amp[1137] - Complex<double> (0, 1) * amp[1139] + amp[1140] -
      Complex<double> (0, 1) * amp[1141] + Complex<double> (0, 1) * amp[1143] +
      Complex<double> (0, 1) * amp[1145] + amp[1146] + Complex<double> (0, 1) *
      amp[1147] + Complex<double> (0, 1) * amp[1149] + amp[1150]);
  jamp[2] = +1./2. * (-1./3. * amp[1103] - 1./3. * amp[1104] - 1./3. *
      amp[1105] - 1./3. * amp[1106] - 1./3. * amp[1107] - 1./3. * amp[1108] -
      1./3. * amp[1109] - 1./3. * amp[1110] - 1./3. * amp[1119] - 1./3. *
      amp[1120] - 1./3. * amp[1121] - 1./3. * amp[1122] - 1./3. * amp[1123] -
      1./3. * amp[1124] - 1./3. * amp[1125] - 1./3. * amp[1126] - 1./3. *
      amp[1144] - 1./3. * amp[1146] - 1./3. * amp[1148] - 1./3. * amp[1150]);
  jamp[3] = +1./2. * (+amp[1103] + amp[1104] + amp[1105] + amp[1106] +
      amp[1107] + amp[1108] + amp[1109] + amp[1110] + amp[1127] + amp[1128] +
      amp[1129] + amp[1130] + amp[1131] + amp[1132] + amp[1133] + amp[1134] +
      Complex<double> (0, 1) * amp[1135] + Complex<double> (0, 1) * amp[1137] +
      amp[1138] + Complex<double> (0, 1) * amp[1139] + Complex<double> (0, 1) *
      amp[1141] + amp[1142] - Complex<double> (0, 1) * amp[1143] + amp[1144] -
      Complex<double> (0, 1) * amp[1145] - Complex<double> (0, 1) * amp[1147] +
      amp[1148] - Complex<double> (0, 1) * amp[1149]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[15][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P15_sm_tamq_tamgqqq::matrix_4_tapu_tapguccx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 48;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[1151] + 1./3. * amp[1152] + 1./3. *
      amp[1153] + 1./3. * amp[1154] + 1./3. * amp[1155] + 1./3. * amp[1156] +
      1./3. * amp[1157] + 1./3. * amp[1158] + 1./3. * amp[1167] + 1./3. *
      amp[1168] + 1./3. * amp[1169] + 1./3. * amp[1170] + 1./3. * amp[1171] +
      1./3. * amp[1172] + 1./3. * amp[1173] + 1./3. * amp[1174] + 1./3. *
      amp[1192] + 1./3. * amp[1194] + 1./3. * amp[1196] + 1./3. * amp[1198]);
  jamp[1] = +1./2. * (-amp[1151] - amp[1152] - amp[1153] - amp[1154] -
      amp[1155] - amp[1156] - amp[1157] - amp[1158] - amp[1175] - amp[1176] -
      amp[1177] - amp[1178] - amp[1179] - amp[1180] - amp[1181] - amp[1182] -
      Complex<double> (0, 1) * amp[1183] - Complex<double> (0, 1) * amp[1185] -
      amp[1186] - Complex<double> (0, 1) * amp[1187] - Complex<double> (0, 1) *
      amp[1189] - amp[1190] + Complex<double> (0, 1) * amp[1191] - amp[1192] +
      Complex<double> (0, 1) * amp[1193] + Complex<double> (0, 1) * amp[1195] -
      amp[1196] + Complex<double> (0, 1) * amp[1197]);
  jamp[2] = +1./2. * (-amp[1159] - amp[1160] - amp[1161] - amp[1162] -
      amp[1163] - amp[1164] - amp[1165] - amp[1166] - amp[1167] - amp[1168] -
      amp[1169] - amp[1170] - amp[1171] - amp[1172] - amp[1173] - amp[1174] +
      Complex<double> (0, 1) * amp[1183] - amp[1184] + Complex<double> (0, 1) *
      amp[1185] + Complex<double> (0, 1) * amp[1187] - amp[1188] +
      Complex<double> (0, 1) * amp[1189] - Complex<double> (0, 1) * amp[1191] -
      Complex<double> (0, 1) * amp[1193] - amp[1194] - Complex<double> (0, 1) *
      amp[1195] - Complex<double> (0, 1) * amp[1197] - amp[1198]);
  jamp[3] = +1./2. * (+1./3. * amp[1159] + 1./3. * amp[1160] + 1./3. *
      amp[1161] + 1./3. * amp[1162] + 1./3. * amp[1163] + 1./3. * amp[1164] +
      1./3. * amp[1165] + 1./3. * amp[1166] + 1./3. * amp[1175] + 1./3. *
      amp[1176] + 1./3. * amp[1177] + 1./3. * amp[1178] + 1./3. * amp[1179] +
      1./3. * amp[1180] + 1./3. * amp[1181] + 1./3. * amp[1182] + 1./3. *
      amp[1184] + 1./3. * amp[1186] + 1./3. * amp[1188] + 1./3. * amp[1190]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[16][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P15_sm_tamq_tamgqqq::matrix_4_tapu_tapguddx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 48;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[1199] + 1./3. * amp[1200] + 1./3. *
      amp[1201] + 1./3. * amp[1202] + 1./3. * amp[1203] + 1./3. * amp[1204] +
      1./3. * amp[1205] + 1./3. * amp[1206] + 1./3. * amp[1215] + 1./3. *
      amp[1216] + 1./3. * amp[1217] + 1./3. * amp[1218] + 1./3. * amp[1219] +
      1./3. * amp[1220] + 1./3. * amp[1221] + 1./3. * amp[1222] + 1./3. *
      amp[1240] + 1./3. * amp[1242] + 1./3. * amp[1244] + 1./3. * amp[1246]);
  jamp[1] = +1./2. * (-amp[1199] - amp[1200] - amp[1201] - amp[1202] -
      amp[1203] - amp[1204] - amp[1205] - amp[1206] - amp[1223] - amp[1224] -
      amp[1225] - amp[1226] - amp[1227] - amp[1228] - amp[1229] - amp[1230] -
      Complex<double> (0, 1) * amp[1231] - Complex<double> (0, 1) * amp[1233] -
      amp[1234] - Complex<double> (0, 1) * amp[1235] - Complex<double> (0, 1) *
      amp[1237] - amp[1238] + Complex<double> (0, 1) * amp[1239] - amp[1240] +
      Complex<double> (0, 1) * amp[1241] + Complex<double> (0, 1) * amp[1243] -
      amp[1244] + Complex<double> (0, 1) * amp[1245]);
  jamp[2] = +1./2. * (-amp[1207] - amp[1208] - amp[1209] - amp[1210] -
      amp[1211] - amp[1212] - amp[1213] - amp[1214] - amp[1215] - amp[1216] -
      amp[1217] - amp[1218] - amp[1219] - amp[1220] - amp[1221] - amp[1222] +
      Complex<double> (0, 1) * amp[1231] - amp[1232] + Complex<double> (0, 1) *
      amp[1233] + Complex<double> (0, 1) * amp[1235] - amp[1236] +
      Complex<double> (0, 1) * amp[1237] - Complex<double> (0, 1) * amp[1239] -
      Complex<double> (0, 1) * amp[1241] - amp[1242] - Complex<double> (0, 1) *
      amp[1243] - Complex<double> (0, 1) * amp[1245] - amp[1246]);
  jamp[3] = +1./2. * (+1./3. * amp[1207] + 1./3. * amp[1208] + 1./3. *
      amp[1209] + 1./3. * amp[1210] + 1./3. * amp[1211] + 1./3. * amp[1212] +
      1./3. * amp[1213] + 1./3. * amp[1214] + 1./3. * amp[1223] + 1./3. *
      amp[1224] + 1./3. * amp[1225] + 1./3. * amp[1226] + 1./3. * amp[1227] +
      1./3. * amp[1228] + 1./3. * amp[1229] + 1./3. * amp[1230] + 1./3. *
      amp[1232] + 1./3. * amp[1234] + 1./3. * amp[1236] + 1./3. * amp[1238]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[17][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P15_sm_tamq_tamgqqq::matrix_4_tapd_tapgudux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 48;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[1247] + amp[1248] + amp[1249] + amp[1250] +
      amp[1251] + amp[1252] + amp[1253] + amp[1254] + amp[1271] + amp[1272] +
      amp[1273] + amp[1274] + amp[1275] + amp[1276] + amp[1277] + amp[1278] +
      Complex<double> (0, 1) * amp[1279] + Complex<double> (0, 1) * amp[1281] +
      amp[1282] + Complex<double> (0, 1) * amp[1283] + Complex<double> (0, 1) *
      amp[1285] + amp[1286] - Complex<double> (0, 1) * amp[1287] + amp[1288] -
      Complex<double> (0, 1) * amp[1289] - Complex<double> (0, 1) * amp[1291] +
      amp[1292] - Complex<double> (0, 1) * amp[1293]);
  jamp[1] = +1./2. * (-1./3. * amp[1247] - 1./3. * amp[1248] - 1./3. *
      amp[1249] - 1./3. * amp[1250] - 1./3. * amp[1251] - 1./3. * amp[1252] -
      1./3. * amp[1253] - 1./3. * amp[1254] - 1./3. * amp[1263] - 1./3. *
      amp[1264] - 1./3. * amp[1265] - 1./3. * amp[1266] - 1./3. * amp[1267] -
      1./3. * amp[1268] - 1./3. * amp[1269] - 1./3. * amp[1270] - 1./3. *
      amp[1288] - 1./3. * amp[1290] - 1./3. * amp[1292] - 1./3. * amp[1294]);
  jamp[2] = +1./2. * (-1./3. * amp[1255] - 1./3. * amp[1256] - 1./3. *
      amp[1257] - 1./3. * amp[1258] - 1./3. * amp[1259] - 1./3. * amp[1260] -
      1./3. * amp[1261] - 1./3. * amp[1262] - 1./3. * amp[1271] - 1./3. *
      amp[1272] - 1./3. * amp[1273] - 1./3. * amp[1274] - 1./3. * amp[1275] -
      1./3. * amp[1276] - 1./3. * amp[1277] - 1./3. * amp[1278] - 1./3. *
      amp[1280] - 1./3. * amp[1282] - 1./3. * amp[1284] - 1./3. * amp[1286]);
  jamp[3] = +1./2. * (+amp[1255] + amp[1256] + amp[1257] + amp[1258] +
      amp[1259] + amp[1260] + amp[1261] + amp[1262] + amp[1263] + amp[1264] +
      amp[1265] + amp[1266] + amp[1267] + amp[1268] + amp[1269] + amp[1270] -
      Complex<double> (0, 1) * amp[1279] + amp[1280] - Complex<double> (0, 1) *
      amp[1281] - Complex<double> (0, 1) * amp[1283] + amp[1284] -
      Complex<double> (0, 1) * amp[1285] + Complex<double> (0, 1) * amp[1287] +
      Complex<double> (0, 1) * amp[1289] + amp[1290] + Complex<double> (0, 1) *
      amp[1291] + Complex<double> (0, 1) * amp[1293] + amp[1294]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[18][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P15_sm_tamq_tamgqqq::matrix_4_tapd_tapgdssx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 48;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[1295] + 1./3. * amp[1296] + 1./3. *
      amp[1297] + 1./3. * amp[1298] + 1./3. * amp[1299] + 1./3. * amp[1300] +
      1./3. * amp[1301] + 1./3. * amp[1302] + 1./3. * amp[1311] + 1./3. *
      amp[1312] + 1./3. * amp[1313] + 1./3. * amp[1314] + 1./3. * amp[1315] +
      1./3. * amp[1316] + 1./3. * amp[1317] + 1./3. * amp[1318] + 1./3. *
      amp[1336] + 1./3. * amp[1338] + 1./3. * amp[1340] + 1./3. * amp[1342]);
  jamp[1] = +1./2. * (-amp[1295] - amp[1296] - amp[1297] - amp[1298] -
      amp[1299] - amp[1300] - amp[1301] - amp[1302] - amp[1319] - amp[1320] -
      amp[1321] - amp[1322] - amp[1323] - amp[1324] - amp[1325] - amp[1326] -
      Complex<double> (0, 1) * amp[1327] - Complex<double> (0, 1) * amp[1329] -
      amp[1330] - Complex<double> (0, 1) * amp[1331] - Complex<double> (0, 1) *
      amp[1333] - amp[1334] + Complex<double> (0, 1) * amp[1335] - amp[1336] +
      Complex<double> (0, 1) * amp[1337] + Complex<double> (0, 1) * amp[1339] -
      amp[1340] + Complex<double> (0, 1) * amp[1341]);
  jamp[2] = +1./2. * (-amp[1303] - amp[1304] - amp[1305] - amp[1306] -
      amp[1307] - amp[1308] - amp[1309] - amp[1310] - amp[1311] - amp[1312] -
      amp[1313] - amp[1314] - amp[1315] - amp[1316] - amp[1317] - amp[1318] +
      Complex<double> (0, 1) * amp[1327] - amp[1328] + Complex<double> (0, 1) *
      amp[1329] + Complex<double> (0, 1) * amp[1331] - amp[1332] +
      Complex<double> (0, 1) * amp[1333] - Complex<double> (0, 1) * amp[1335] -
      Complex<double> (0, 1) * amp[1337] - amp[1338] - Complex<double> (0, 1) *
      amp[1339] - Complex<double> (0, 1) * amp[1341] - amp[1342]);
  jamp[3] = +1./2. * (+1./3. * amp[1303] + 1./3. * amp[1304] + 1./3. *
      amp[1305] + 1./3. * amp[1306] + 1./3. * amp[1307] + 1./3. * amp[1308] +
      1./3. * amp[1309] + 1./3. * amp[1310] + 1./3. * amp[1319] + 1./3. *
      amp[1320] + 1./3. * amp[1321] + 1./3. * amp[1322] + 1./3. * amp[1323] +
      1./3. * amp[1324] + 1./3. * amp[1325] + 1./3. * amp[1326] + 1./3. *
      amp[1328] + 1./3. * amp[1330] + 1./3. * amp[1332] + 1./3. * amp[1334]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[19][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P15_sm_tamq_tamgqqq::matrix_4_tapux_tapgcuxcx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 48;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[1351] + 1./3. * amp[1352] + 1./3. *
      amp[1353] + 1./3. * amp[1354] + 1./3. * amp[1355] + 1./3. * amp[1356] +
      1./3. * amp[1357] + 1./3. * amp[1358] + 1./3. * amp[1367] + 1./3. *
      amp[1368] + 1./3. * amp[1369] + 1./3. * amp[1370] + 1./3. * amp[1371] +
      1./3. * amp[1372] + 1./3. * amp[1373] + 1./3. * amp[1374] + 1./3. *
      amp[1376] + 1./3. * amp[1378] + 1./3. * amp[1380] + 1./3. * amp[1382]);
  jamp[1] = +1./2. * (-amp[1351] - amp[1352] - amp[1353] - amp[1354] -
      amp[1355] - amp[1356] - amp[1357] - amp[1358] - amp[1359] - amp[1360] -
      amp[1361] - amp[1362] - amp[1363] - amp[1364] - amp[1365] - amp[1366] +
      Complex<double> (0, 1) * amp[1375] - amp[1376] + Complex<double> (0, 1) *
      amp[1377] + Complex<double> (0, 1) * amp[1379] - amp[1380] +
      Complex<double> (0, 1) * amp[1381] - Complex<double> (0, 1) * amp[1383] -
      Complex<double> (0, 1) * amp[1385] - amp[1386] - Complex<double> (0, 1) *
      amp[1387] - Complex<double> (0, 1) * amp[1389] - amp[1390]);
  jamp[2] = +1./2. * (+1./3. * amp[1343] + 1./3. * amp[1344] + 1./3. *
      amp[1345] + 1./3. * amp[1346] + 1./3. * amp[1347] + 1./3. * amp[1348] +
      1./3. * amp[1349] + 1./3. * amp[1350] + 1./3. * amp[1359] + 1./3. *
      amp[1360] + 1./3. * amp[1361] + 1./3. * amp[1362] + 1./3. * amp[1363] +
      1./3. * amp[1364] + 1./3. * amp[1365] + 1./3. * amp[1366] + 1./3. *
      amp[1384] + 1./3. * amp[1386] + 1./3. * amp[1388] + 1./3. * amp[1390]);
  jamp[3] = +1./2. * (-amp[1343] - amp[1344] - amp[1345] - amp[1346] -
      amp[1347] - amp[1348] - amp[1349] - amp[1350] - amp[1367] - amp[1368] -
      amp[1369] - amp[1370] - amp[1371] - amp[1372] - amp[1373] - amp[1374] -
      Complex<double> (0, 1) * amp[1375] - Complex<double> (0, 1) * amp[1377] -
      amp[1378] - Complex<double> (0, 1) * amp[1379] - Complex<double> (0, 1) *
      amp[1381] - amp[1382] + Complex<double> (0, 1) * amp[1383] - amp[1384] +
      Complex<double> (0, 1) * amp[1385] + Complex<double> (0, 1) * amp[1387] -
      amp[1388] + Complex<double> (0, 1) * amp[1389]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[20][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P15_sm_tamq_tamgqqq::matrix_4_tapux_tapgduxdx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 48;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[1399] + 1./3. * amp[1400] + 1./3. *
      amp[1401] + 1./3. * amp[1402] + 1./3. * amp[1403] + 1./3. * amp[1404] +
      1./3. * amp[1405] + 1./3. * amp[1406] + 1./3. * amp[1415] + 1./3. *
      amp[1416] + 1./3. * amp[1417] + 1./3. * amp[1418] + 1./3. * amp[1419] +
      1./3. * amp[1420] + 1./3. * amp[1421] + 1./3. * amp[1422] + 1./3. *
      amp[1424] + 1./3. * amp[1426] + 1./3. * amp[1428] + 1./3. * amp[1430]);
  jamp[1] = +1./2. * (-amp[1399] - amp[1400] - amp[1401] - amp[1402] -
      amp[1403] - amp[1404] - amp[1405] - amp[1406] - amp[1407] - amp[1408] -
      amp[1409] - amp[1410] - amp[1411] - amp[1412] - amp[1413] - amp[1414] +
      Complex<double> (0, 1) * amp[1423] - amp[1424] + Complex<double> (0, 1) *
      amp[1425] + Complex<double> (0, 1) * amp[1427] - amp[1428] +
      Complex<double> (0, 1) * amp[1429] - Complex<double> (0, 1) * amp[1431] -
      Complex<double> (0, 1) * amp[1433] - amp[1434] - Complex<double> (0, 1) *
      amp[1435] - Complex<double> (0, 1) * amp[1437] - amp[1438]);
  jamp[2] = +1./2. * (+1./3. * amp[1391] + 1./3. * amp[1392] + 1./3. *
      amp[1393] + 1./3. * amp[1394] + 1./3. * amp[1395] + 1./3. * amp[1396] +
      1./3. * amp[1397] + 1./3. * amp[1398] + 1./3. * amp[1407] + 1./3. *
      amp[1408] + 1./3. * amp[1409] + 1./3. * amp[1410] + 1./3. * amp[1411] +
      1./3. * amp[1412] + 1./3. * amp[1413] + 1./3. * amp[1414] + 1./3. *
      amp[1432] + 1./3. * amp[1434] + 1./3. * amp[1436] + 1./3. * amp[1438]);
  jamp[3] = +1./2. * (-amp[1391] - amp[1392] - amp[1393] - amp[1394] -
      amp[1395] - amp[1396] - amp[1397] - amp[1398] - amp[1415] - amp[1416] -
      amp[1417] - amp[1418] - amp[1419] - amp[1420] - amp[1421] - amp[1422] -
      Complex<double> (0, 1) * amp[1423] - Complex<double> (0, 1) * amp[1425] -
      amp[1426] - Complex<double> (0, 1) * amp[1427] - Complex<double> (0, 1) *
      amp[1429] - amp[1430] + Complex<double> (0, 1) * amp[1431] - amp[1432] +
      Complex<double> (0, 1) * amp[1433] + Complex<double> (0, 1) * amp[1435] -
      amp[1436] + Complex<double> (0, 1) * amp[1437]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[21][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P15_sm_tamq_tamgqqq::matrix_4_tapdx_tapguuxdx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 48;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[1439] + amp[1440] + amp[1441] + amp[1442] +
      amp[1443] + amp[1444] + amp[1445] + amp[1446] + amp[1463] + amp[1464] +
      amp[1465] + amp[1466] + amp[1467] + amp[1468] + amp[1469] + amp[1470] +
      Complex<double> (0, 1) * amp[1471] + Complex<double> (0, 1) * amp[1473] +
      amp[1474] + Complex<double> (0, 1) * amp[1475] + Complex<double> (0, 1) *
      amp[1477] + amp[1478] - Complex<double> (0, 1) * amp[1479] + amp[1480] -
      Complex<double> (0, 1) * amp[1481] - Complex<double> (0, 1) * amp[1483] +
      amp[1484] - Complex<double> (0, 1) * amp[1485]);
  jamp[1] = +1./2. * (-1./3. * amp[1439] - 1./3. * amp[1440] - 1./3. *
      amp[1441] - 1./3. * amp[1442] - 1./3. * amp[1443] - 1./3. * amp[1444] -
      1./3. * amp[1445] - 1./3. * amp[1446] - 1./3. * amp[1455] - 1./3. *
      amp[1456] - 1./3. * amp[1457] - 1./3. * amp[1458] - 1./3. * amp[1459] -
      1./3. * amp[1460] - 1./3. * amp[1461] - 1./3. * amp[1462] - 1./3. *
      amp[1480] - 1./3. * amp[1482] - 1./3. * amp[1484] - 1./3. * amp[1486]);
  jamp[2] = +1./2. * (+amp[1447] + amp[1448] + amp[1449] + amp[1450] +
      amp[1451] + amp[1452] + amp[1453] + amp[1454] + amp[1455] + amp[1456] +
      amp[1457] + amp[1458] + amp[1459] + amp[1460] + amp[1461] + amp[1462] -
      Complex<double> (0, 1) * amp[1471] + amp[1472] - Complex<double> (0, 1) *
      amp[1473] - Complex<double> (0, 1) * amp[1475] + amp[1476] -
      Complex<double> (0, 1) * amp[1477] + Complex<double> (0, 1) * amp[1479] +
      Complex<double> (0, 1) * amp[1481] + amp[1482] + Complex<double> (0, 1) *
      amp[1483] + Complex<double> (0, 1) * amp[1485] + amp[1486]);
  jamp[3] = +1./2. * (-1./3. * amp[1447] - 1./3. * amp[1448] - 1./3. *
      amp[1449] - 1./3. * amp[1450] - 1./3. * amp[1451] - 1./3. * amp[1452] -
      1./3. * amp[1453] - 1./3. * amp[1454] - 1./3. * amp[1463] - 1./3. *
      amp[1464] - 1./3. * amp[1465] - 1./3. * amp[1466] - 1./3. * amp[1467] -
      1./3. * amp[1468] - 1./3. * amp[1469] - 1./3. * amp[1470] - 1./3. *
      amp[1472] - 1./3. * amp[1474] - 1./3. * amp[1476] - 1./3. * amp[1478]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[22][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P15_sm_tamq_tamgqqq::matrix_4_tapdx_tapgsdxsx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 48;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[1495] + 1./3. * amp[1496] + 1./3. *
      amp[1497] + 1./3. * amp[1498] + 1./3. * amp[1499] + 1./3. * amp[1500] +
      1./3. * amp[1501] + 1./3. * amp[1502] + 1./3. * amp[1511] + 1./3. *
      amp[1512] + 1./3. * amp[1513] + 1./3. * amp[1514] + 1./3. * amp[1515] +
      1./3. * amp[1516] + 1./3. * amp[1517] + 1./3. * amp[1518] + 1./3. *
      amp[1520] + 1./3. * amp[1522] + 1./3. * amp[1524] + 1./3. * amp[1526]);
  jamp[1] = +1./2. * (-amp[1495] - amp[1496] - amp[1497] - amp[1498] -
      amp[1499] - amp[1500] - amp[1501] - amp[1502] - amp[1503] - amp[1504] -
      amp[1505] - amp[1506] - amp[1507] - amp[1508] - amp[1509] - amp[1510] +
      Complex<double> (0, 1) * amp[1519] - amp[1520] + Complex<double> (0, 1) *
      amp[1521] + Complex<double> (0, 1) * amp[1523] - amp[1524] +
      Complex<double> (0, 1) * amp[1525] - Complex<double> (0, 1) * amp[1527] -
      Complex<double> (0, 1) * amp[1529] - amp[1530] - Complex<double> (0, 1) *
      amp[1531] - Complex<double> (0, 1) * amp[1533] - amp[1534]);
  jamp[2] = +1./2. * (+1./3. * amp[1487] + 1./3. * amp[1488] + 1./3. *
      amp[1489] + 1./3. * amp[1490] + 1./3. * amp[1491] + 1./3. * amp[1492] +
      1./3. * amp[1493] + 1./3. * amp[1494] + 1./3. * amp[1503] + 1./3. *
      amp[1504] + 1./3. * amp[1505] + 1./3. * amp[1506] + 1./3. * amp[1507] +
      1./3. * amp[1508] + 1./3. * amp[1509] + 1./3. * amp[1510] + 1./3. *
      amp[1528] + 1./3. * amp[1530] + 1./3. * amp[1532] + 1./3. * amp[1534]);
  jamp[3] = +1./2. * (-amp[1487] - amp[1488] - amp[1489] - amp[1490] -
      amp[1491] - amp[1492] - amp[1493] - amp[1494] - amp[1511] - amp[1512] -
      amp[1513] - amp[1514] - amp[1515] - amp[1516] - amp[1517] - amp[1518] -
      Complex<double> (0, 1) * amp[1519] - Complex<double> (0, 1) * amp[1521] -
      amp[1522] - Complex<double> (0, 1) * amp[1523] - Complex<double> (0, 1) *
      amp[1525] - amp[1526] + Complex<double> (0, 1) * amp[1527] - amp[1528] +
      Complex<double> (0, 1) * amp[1529] + Complex<double> (0, 1) * amp[1531] -
      amp[1532] + Complex<double> (0, 1) * amp[1533]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[23][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}


}  // end namespace PY8MEs_namespace

