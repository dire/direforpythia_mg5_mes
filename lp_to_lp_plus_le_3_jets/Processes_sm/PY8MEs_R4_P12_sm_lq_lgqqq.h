//==========================================================================
// This file has been automatically generated for Pythia 8
// MadGraph5_aMC@NLO v. 2.6.0, 2017-08-16
// By the MadGraph5_aMC@NLO Development Team
// Visit launchpad.net/madgraph5 and amcatnlo.web.cern.ch
//==========================================================================

#ifndef PY8MEs_R4_P12_sm_lq_lgqqq_H
#define PY8MEs_R4_P12_sm_lq_lgqqq_H

#include "Complex.h" 
#include <vector> 
#include <set> 
#include <exception> 
#include <iostream> 

#include "Parameters_sm.h"
#include "PY8MEs.h"

using namespace std; 

namespace PY8MEs_namespace 
{
//==========================================================================
// A class for calculating the matrix elements for
// Process: e- u > e- g u u u~ WEIGHTED<=7 @4
// Process: e- c > e- g c c c~ WEIGHTED<=7 @4
// Process: mu- u > mu- g u u u~ WEIGHTED<=7 @4
// Process: mu- c > mu- g c c c~ WEIGHTED<=7 @4
// Process: e- d > e- g d d d~ WEIGHTED<=7 @4
// Process: e- s > e- g s s s~ WEIGHTED<=7 @4
// Process: mu- d > mu- g d d d~ WEIGHTED<=7 @4
// Process: mu- s > mu- g s s s~ WEIGHTED<=7 @4
// Process: e- u~ > e- g u u~ u~ WEIGHTED<=7 @4
// Process: e- c~ > e- g c c~ c~ WEIGHTED<=7 @4
// Process: mu- u~ > mu- g u u~ u~ WEIGHTED<=7 @4
// Process: mu- c~ > mu- g c c~ c~ WEIGHTED<=7 @4
// Process: e- d~ > e- g d d~ d~ WEIGHTED<=7 @4
// Process: e- s~ > e- g s s~ s~ WEIGHTED<=7 @4
// Process: mu- d~ > mu- g d d~ d~ WEIGHTED<=7 @4
// Process: mu- s~ > mu- g s s~ s~ WEIGHTED<=7 @4
// Process: e+ u > e+ g u u u~ WEIGHTED<=7 @4
// Process: e+ c > e+ g c c c~ WEIGHTED<=7 @4
// Process: mu+ u > mu+ g u u u~ WEIGHTED<=7 @4
// Process: mu+ c > mu+ g c c c~ WEIGHTED<=7 @4
// Process: e+ d > e+ g d d d~ WEIGHTED<=7 @4
// Process: e+ s > e+ g s s s~ WEIGHTED<=7 @4
// Process: mu+ d > mu+ g d d d~ WEIGHTED<=7 @4
// Process: mu+ s > mu+ g s s s~ WEIGHTED<=7 @4
// Process: e+ u~ > e+ g u u~ u~ WEIGHTED<=7 @4
// Process: e+ c~ > e+ g c c~ c~ WEIGHTED<=7 @4
// Process: mu+ u~ > mu+ g u u~ u~ WEIGHTED<=7 @4
// Process: mu+ c~ > mu+ g c c~ c~ WEIGHTED<=7 @4
// Process: e+ d~ > e+ g d d~ d~ WEIGHTED<=7 @4
// Process: e+ s~ > e+ g s s~ s~ WEIGHTED<=7 @4
// Process: mu+ d~ > mu+ g d d~ d~ WEIGHTED<=7 @4
// Process: mu+ s~ > mu+ g s s~ s~ WEIGHTED<=7 @4
// Process: e- u > e- g u c c~ WEIGHTED<=7 @4
// Process: e- c > e- g c u u~ WEIGHTED<=7 @4
// Process: mu- u > mu- g u c c~ WEIGHTED<=7 @4
// Process: mu- c > mu- g c u u~ WEIGHTED<=7 @4
// Process: e- u > e- g u d d~ WEIGHTED<=7 @4
// Process: e- u > e- g u s s~ WEIGHTED<=7 @4
// Process: e- c > e- g c d d~ WEIGHTED<=7 @4
// Process: e- c > e- g c s s~ WEIGHTED<=7 @4
// Process: mu- u > mu- g u d d~ WEIGHTED<=7 @4
// Process: mu- u > mu- g u s s~ WEIGHTED<=7 @4
// Process: mu- c > mu- g c d d~ WEIGHTED<=7 @4
// Process: mu- c > mu- g c s s~ WEIGHTED<=7 @4
// Process: e- d > e- g u d u~ WEIGHTED<=7 @4
// Process: e- d > e- g c d c~ WEIGHTED<=7 @4
// Process: e- s > e- g u s u~ WEIGHTED<=7 @4
// Process: e- s > e- g c s c~ WEIGHTED<=7 @4
// Process: mu- d > mu- g u d u~ WEIGHTED<=7 @4
// Process: mu- d > mu- g c d c~ WEIGHTED<=7 @4
// Process: mu- s > mu- g u s u~ WEIGHTED<=7 @4
// Process: mu- s > mu- g c s c~ WEIGHTED<=7 @4
// Process: e- d > e- g d s s~ WEIGHTED<=7 @4
// Process: e- s > e- g s d d~ WEIGHTED<=7 @4
// Process: mu- d > mu- g d s s~ WEIGHTED<=7 @4
// Process: mu- s > mu- g s d d~ WEIGHTED<=7 @4
// Process: e- u~ > e- g c u~ c~ WEIGHTED<=7 @4
// Process: e- c~ > e- g u c~ u~ WEIGHTED<=7 @4
// Process: mu- u~ > mu- g c u~ c~ WEIGHTED<=7 @4
// Process: mu- c~ > mu- g u c~ u~ WEIGHTED<=7 @4
// Process: e- u~ > e- g d u~ d~ WEIGHTED<=7 @4
// Process: e- u~ > e- g s u~ s~ WEIGHTED<=7 @4
// Process: e- c~ > e- g d c~ d~ WEIGHTED<=7 @4
// Process: e- c~ > e- g s c~ s~ WEIGHTED<=7 @4
// Process: mu- u~ > mu- g d u~ d~ WEIGHTED<=7 @4
// Process: mu- u~ > mu- g s u~ s~ WEIGHTED<=7 @4
// Process: mu- c~ > mu- g d c~ d~ WEIGHTED<=7 @4
// Process: mu- c~ > mu- g s c~ s~ WEIGHTED<=7 @4
// Process: e- d~ > e- g u u~ d~ WEIGHTED<=7 @4
// Process: e- d~ > e- g c c~ d~ WEIGHTED<=7 @4
// Process: e- s~ > e- g u u~ s~ WEIGHTED<=7 @4
// Process: e- s~ > e- g c c~ s~ WEIGHTED<=7 @4
// Process: mu- d~ > mu- g u u~ d~ WEIGHTED<=7 @4
// Process: mu- d~ > mu- g c c~ d~ WEIGHTED<=7 @4
// Process: mu- s~ > mu- g u u~ s~ WEIGHTED<=7 @4
// Process: mu- s~ > mu- g c c~ s~ WEIGHTED<=7 @4
// Process: e- d~ > e- g s d~ s~ WEIGHTED<=7 @4
// Process: e- s~ > e- g d s~ d~ WEIGHTED<=7 @4
// Process: mu- d~ > mu- g s d~ s~ WEIGHTED<=7 @4
// Process: mu- s~ > mu- g d s~ d~ WEIGHTED<=7 @4
// Process: ve u > ve g u u u~ WEIGHTED<=7 @4
// Process: ve c > ve g c c c~ WEIGHTED<=7 @4
// Process: vm u > vm g u u u~ WEIGHTED<=7 @4
// Process: vm c > vm g c c c~ WEIGHTED<=7 @4
// Process: vt u > vt g u u u~ WEIGHTED<=7 @4
// Process: vt c > vt g c c c~ WEIGHTED<=7 @4
// Process: ve d > ve g d d d~ WEIGHTED<=7 @4
// Process: ve s > ve g s s s~ WEIGHTED<=7 @4
// Process: vm d > vm g d d d~ WEIGHTED<=7 @4
// Process: vm s > vm g s s s~ WEIGHTED<=7 @4
// Process: vt d > vt g d d d~ WEIGHTED<=7 @4
// Process: vt s > vt g s s s~ WEIGHTED<=7 @4
// Process: ve u~ > ve g u u~ u~ WEIGHTED<=7 @4
// Process: ve c~ > ve g c c~ c~ WEIGHTED<=7 @4
// Process: vm u~ > vm g u u~ u~ WEIGHTED<=7 @4
// Process: vm c~ > vm g c c~ c~ WEIGHTED<=7 @4
// Process: vt u~ > vt g u u~ u~ WEIGHTED<=7 @4
// Process: vt c~ > vt g c c~ c~ WEIGHTED<=7 @4
// Process: ve d~ > ve g d d~ d~ WEIGHTED<=7 @4
// Process: ve s~ > ve g s s~ s~ WEIGHTED<=7 @4
// Process: vm d~ > vm g d d~ d~ WEIGHTED<=7 @4
// Process: vm s~ > vm g s s~ s~ WEIGHTED<=7 @4
// Process: vt d~ > vt g d d~ d~ WEIGHTED<=7 @4
// Process: vt s~ > vt g s s~ s~ WEIGHTED<=7 @4
// Process: e+ u > e+ g u c c~ WEIGHTED<=7 @4
// Process: e+ c > e+ g c u u~ WEIGHTED<=7 @4
// Process: mu+ u > mu+ g u c c~ WEIGHTED<=7 @4
// Process: mu+ c > mu+ g c u u~ WEIGHTED<=7 @4
// Process: e+ u > e+ g u d d~ WEIGHTED<=7 @4
// Process: e+ u > e+ g u s s~ WEIGHTED<=7 @4
// Process: e+ c > e+ g c d d~ WEIGHTED<=7 @4
// Process: e+ c > e+ g c s s~ WEIGHTED<=7 @4
// Process: mu+ u > mu+ g u d d~ WEIGHTED<=7 @4
// Process: mu+ u > mu+ g u s s~ WEIGHTED<=7 @4
// Process: mu+ c > mu+ g c d d~ WEIGHTED<=7 @4
// Process: mu+ c > mu+ g c s s~ WEIGHTED<=7 @4
// Process: e+ d > e+ g u d u~ WEIGHTED<=7 @4
// Process: e+ d > e+ g c d c~ WEIGHTED<=7 @4
// Process: e+ s > e+ g u s u~ WEIGHTED<=7 @4
// Process: e+ s > e+ g c s c~ WEIGHTED<=7 @4
// Process: mu+ d > mu+ g u d u~ WEIGHTED<=7 @4
// Process: mu+ d > mu+ g c d c~ WEIGHTED<=7 @4
// Process: mu+ s > mu+ g u s u~ WEIGHTED<=7 @4
// Process: mu+ s > mu+ g c s c~ WEIGHTED<=7 @4
// Process: e+ d > e+ g d s s~ WEIGHTED<=7 @4
// Process: e+ s > e+ g s d d~ WEIGHTED<=7 @4
// Process: mu+ d > mu+ g d s s~ WEIGHTED<=7 @4
// Process: mu+ s > mu+ g s d d~ WEIGHTED<=7 @4
// Process: e+ u~ > e+ g c u~ c~ WEIGHTED<=7 @4
// Process: e+ c~ > e+ g u c~ u~ WEIGHTED<=7 @4
// Process: mu+ u~ > mu+ g c u~ c~ WEIGHTED<=7 @4
// Process: mu+ c~ > mu+ g u c~ u~ WEIGHTED<=7 @4
// Process: e+ u~ > e+ g d u~ d~ WEIGHTED<=7 @4
// Process: e+ u~ > e+ g s u~ s~ WEIGHTED<=7 @4
// Process: e+ c~ > e+ g d c~ d~ WEIGHTED<=7 @4
// Process: e+ c~ > e+ g s c~ s~ WEIGHTED<=7 @4
// Process: mu+ u~ > mu+ g d u~ d~ WEIGHTED<=7 @4
// Process: mu+ u~ > mu+ g s u~ s~ WEIGHTED<=7 @4
// Process: mu+ c~ > mu+ g d c~ d~ WEIGHTED<=7 @4
// Process: mu+ c~ > mu+ g s c~ s~ WEIGHTED<=7 @4
// Process: e+ d~ > e+ g u u~ d~ WEIGHTED<=7 @4
// Process: e+ d~ > e+ g c c~ d~ WEIGHTED<=7 @4
// Process: e+ s~ > e+ g u u~ s~ WEIGHTED<=7 @4
// Process: e+ s~ > e+ g c c~ s~ WEIGHTED<=7 @4
// Process: mu+ d~ > mu+ g u u~ d~ WEIGHTED<=7 @4
// Process: mu+ d~ > mu+ g c c~ d~ WEIGHTED<=7 @4
// Process: mu+ s~ > mu+ g u u~ s~ WEIGHTED<=7 @4
// Process: mu+ s~ > mu+ g c c~ s~ WEIGHTED<=7 @4
// Process: e+ d~ > e+ g s d~ s~ WEIGHTED<=7 @4
// Process: e+ s~ > e+ g d s~ d~ WEIGHTED<=7 @4
// Process: mu+ d~ > mu+ g s d~ s~ WEIGHTED<=7 @4
// Process: mu+ s~ > mu+ g d s~ d~ WEIGHTED<=7 @4
// Process: ve~ u > ve~ g u u u~ WEIGHTED<=7 @4
// Process: ve~ c > ve~ g c c c~ WEIGHTED<=7 @4
// Process: vm~ u > vm~ g u u u~ WEIGHTED<=7 @4
// Process: vm~ c > vm~ g c c c~ WEIGHTED<=7 @4
// Process: vt~ u > vt~ g u u u~ WEIGHTED<=7 @4
// Process: vt~ c > vt~ g c c c~ WEIGHTED<=7 @4
// Process: ve~ d > ve~ g d d d~ WEIGHTED<=7 @4
// Process: ve~ s > ve~ g s s s~ WEIGHTED<=7 @4
// Process: vm~ d > vm~ g d d d~ WEIGHTED<=7 @4
// Process: vm~ s > vm~ g s s s~ WEIGHTED<=7 @4
// Process: vt~ d > vt~ g d d d~ WEIGHTED<=7 @4
// Process: vt~ s > vt~ g s s s~ WEIGHTED<=7 @4
// Process: ve~ u~ > ve~ g u u~ u~ WEIGHTED<=7 @4
// Process: ve~ c~ > ve~ g c c~ c~ WEIGHTED<=7 @4
// Process: vm~ u~ > vm~ g u u~ u~ WEIGHTED<=7 @4
// Process: vm~ c~ > vm~ g c c~ c~ WEIGHTED<=7 @4
// Process: vt~ u~ > vt~ g u u~ u~ WEIGHTED<=7 @4
// Process: vt~ c~ > vt~ g c c~ c~ WEIGHTED<=7 @4
// Process: ve~ d~ > ve~ g d d~ d~ WEIGHTED<=7 @4
// Process: ve~ s~ > ve~ g s s~ s~ WEIGHTED<=7 @4
// Process: vm~ d~ > vm~ g d d~ d~ WEIGHTED<=7 @4
// Process: vm~ s~ > vm~ g s s~ s~ WEIGHTED<=7 @4
// Process: vt~ d~ > vt~ g d d~ d~ WEIGHTED<=7 @4
// Process: vt~ s~ > vt~ g s s~ s~ WEIGHTED<=7 @4
// Process: e- u > ve g u d u~ WEIGHTED<=7 @4
// Process: e- c > ve g c s c~ WEIGHTED<=7 @4
// Process: mu- u > vm g u d u~ WEIGHTED<=7 @4
// Process: mu- c > vm g c s c~ WEIGHTED<=7 @4
// Process: ve d > e- g d u d~ WEIGHTED<=7 @4
// Process: ve s > e- g s c s~ WEIGHTED<=7 @4
// Process: vm d > mu- g d u d~ WEIGHTED<=7 @4
// Process: vm s > mu- g s c s~ WEIGHTED<=7 @4
// Process: e- u > ve g d d d~ WEIGHTED<=7 @4
// Process: e- c > ve g s s s~ WEIGHTED<=7 @4
// Process: mu- u > vm g d d d~ WEIGHTED<=7 @4
// Process: mu- c > vm g s s s~ WEIGHTED<=7 @4
// Process: ve d > e- g u u u~ WEIGHTED<=7 @4
// Process: ve s > e- g c c c~ WEIGHTED<=7 @4
// Process: vm d > mu- g u u u~ WEIGHTED<=7 @4
// Process: vm s > mu- g c c c~ WEIGHTED<=7 @4
// Process: e- d > ve g d d u~ WEIGHTED<=7 @4
// Process: e- s > ve g s s c~ WEIGHTED<=7 @4
// Process: mu- d > vm g d d u~ WEIGHTED<=7 @4
// Process: mu- s > vm g s s c~ WEIGHTED<=7 @4
// Process: ve u > e- g u u d~ WEIGHTED<=7 @4
// Process: ve c > e- g c c s~ WEIGHTED<=7 @4
// Process: vm u > mu- g u u d~ WEIGHTED<=7 @4
// Process: vm c > mu- g c c s~ WEIGHTED<=7 @4
// Process: e- u~ > ve g d u~ u~ WEIGHTED<=7 @4
// Process: e- c~ > ve g s c~ c~ WEIGHTED<=7 @4
// Process: mu- u~ > vm g d u~ u~ WEIGHTED<=7 @4
// Process: mu- c~ > vm g s c~ c~ WEIGHTED<=7 @4
// Process: ve d~ > e- g u d~ d~ WEIGHTED<=7 @4
// Process: ve s~ > e- g c s~ s~ WEIGHTED<=7 @4
// Process: vm d~ > mu- g u d~ d~ WEIGHTED<=7 @4
// Process: vm s~ > mu- g c s~ s~ WEIGHTED<=7 @4
// Process: e- d~ > ve g u u~ u~ WEIGHTED<=7 @4
// Process: e- s~ > ve g c c~ c~ WEIGHTED<=7 @4
// Process: mu- d~ > vm g u u~ u~ WEIGHTED<=7 @4
// Process: mu- s~ > vm g c c~ c~ WEIGHTED<=7 @4
// Process: ve u~ > e- g d d~ d~ WEIGHTED<=7 @4
// Process: ve c~ > e- g s s~ s~ WEIGHTED<=7 @4
// Process: vm u~ > mu- g d d~ d~ WEIGHTED<=7 @4
// Process: vm c~ > mu- g s s~ s~ WEIGHTED<=7 @4
// Process: e- d~ > ve g d u~ d~ WEIGHTED<=7 @4
// Process: e- s~ > ve g s c~ s~ WEIGHTED<=7 @4
// Process: mu- d~ > vm g d u~ d~ WEIGHTED<=7 @4
// Process: mu- s~ > vm g s c~ s~ WEIGHTED<=7 @4
// Process: ve u~ > e- g u d~ u~ WEIGHTED<=7 @4
// Process: ve c~ > e- g c s~ c~ WEIGHTED<=7 @4
// Process: vm u~ > mu- g u d~ u~ WEIGHTED<=7 @4
// Process: vm c~ > mu- g c s~ c~ WEIGHTED<=7 @4
// Process: ve u > ve g u c c~ WEIGHTED<=7 @4
// Process: ve c > ve g c u u~ WEIGHTED<=7 @4
// Process: vm u > vm g u c c~ WEIGHTED<=7 @4
// Process: vm c > vm g c u u~ WEIGHTED<=7 @4
// Process: vt u > vt g u c c~ WEIGHTED<=7 @4
// Process: vt c > vt g c u u~ WEIGHTED<=7 @4
// Process: ve u > ve g u d d~ WEIGHTED<=7 @4
// Process: ve u > ve g u s s~ WEIGHTED<=7 @4
// Process: ve c > ve g c d d~ WEIGHTED<=7 @4
// Process: ve c > ve g c s s~ WEIGHTED<=7 @4
// Process: vm u > vm g u d d~ WEIGHTED<=7 @4
// Process: vm u > vm g u s s~ WEIGHTED<=7 @4
// Process: vm c > vm g c d d~ WEIGHTED<=7 @4
// Process: vm c > vm g c s s~ WEIGHTED<=7 @4
// Process: vt u > vt g u d d~ WEIGHTED<=7 @4
// Process: vt u > vt g u s s~ WEIGHTED<=7 @4
// Process: vt c > vt g c d d~ WEIGHTED<=7 @4
// Process: vt c > vt g c s s~ WEIGHTED<=7 @4
// Process: ve d > ve g u d u~ WEIGHTED<=7 @4
// Process: ve d > ve g c d c~ WEIGHTED<=7 @4
// Process: ve s > ve g u s u~ WEIGHTED<=7 @4
// Process: ve s > ve g c s c~ WEIGHTED<=7 @4
// Process: vm d > vm g u d u~ WEIGHTED<=7 @4
// Process: vm d > vm g c d c~ WEIGHTED<=7 @4
// Process: vm s > vm g u s u~ WEIGHTED<=7 @4
// Process: vm s > vm g c s c~ WEIGHTED<=7 @4
// Process: vt d > vt g u d u~ WEIGHTED<=7 @4
// Process: vt d > vt g c d c~ WEIGHTED<=7 @4
// Process: vt s > vt g u s u~ WEIGHTED<=7 @4
// Process: vt s > vt g c s c~ WEIGHTED<=7 @4
// Process: ve d > ve g d s s~ WEIGHTED<=7 @4
// Process: ve s > ve g s d d~ WEIGHTED<=7 @4
// Process: vm d > vm g d s s~ WEIGHTED<=7 @4
// Process: vm s > vm g s d d~ WEIGHTED<=7 @4
// Process: vt d > vt g d s s~ WEIGHTED<=7 @4
// Process: vt s > vt g s d d~ WEIGHTED<=7 @4
// Process: ve u~ > ve g c u~ c~ WEIGHTED<=7 @4
// Process: ve c~ > ve g u c~ u~ WEIGHTED<=7 @4
// Process: vm u~ > vm g c u~ c~ WEIGHTED<=7 @4
// Process: vm c~ > vm g u c~ u~ WEIGHTED<=7 @4
// Process: vt u~ > vt g c u~ c~ WEIGHTED<=7 @4
// Process: vt c~ > vt g u c~ u~ WEIGHTED<=7 @4
// Process: ve u~ > ve g d u~ d~ WEIGHTED<=7 @4
// Process: ve u~ > ve g s u~ s~ WEIGHTED<=7 @4
// Process: ve c~ > ve g d c~ d~ WEIGHTED<=7 @4
// Process: ve c~ > ve g s c~ s~ WEIGHTED<=7 @4
// Process: vm u~ > vm g d u~ d~ WEIGHTED<=7 @4
// Process: vm u~ > vm g s u~ s~ WEIGHTED<=7 @4
// Process: vm c~ > vm g d c~ d~ WEIGHTED<=7 @4
// Process: vm c~ > vm g s c~ s~ WEIGHTED<=7 @4
// Process: vt u~ > vt g d u~ d~ WEIGHTED<=7 @4
// Process: vt u~ > vt g s u~ s~ WEIGHTED<=7 @4
// Process: vt c~ > vt g d c~ d~ WEIGHTED<=7 @4
// Process: vt c~ > vt g s c~ s~ WEIGHTED<=7 @4
// Process: ve d~ > ve g u u~ d~ WEIGHTED<=7 @4
// Process: ve d~ > ve g c c~ d~ WEIGHTED<=7 @4
// Process: ve s~ > ve g u u~ s~ WEIGHTED<=7 @4
// Process: ve s~ > ve g c c~ s~ WEIGHTED<=7 @4
// Process: vm d~ > vm g u u~ d~ WEIGHTED<=7 @4
// Process: vm d~ > vm g c c~ d~ WEIGHTED<=7 @4
// Process: vm s~ > vm g u u~ s~ WEIGHTED<=7 @4
// Process: vm s~ > vm g c c~ s~ WEIGHTED<=7 @4
// Process: vt d~ > vt g u u~ d~ WEIGHTED<=7 @4
// Process: vt d~ > vt g c c~ d~ WEIGHTED<=7 @4
// Process: vt s~ > vt g u u~ s~ WEIGHTED<=7 @4
// Process: vt s~ > vt g c c~ s~ WEIGHTED<=7 @4
// Process: ve d~ > ve g s d~ s~ WEIGHTED<=7 @4
// Process: ve s~ > ve g d s~ d~ WEIGHTED<=7 @4
// Process: vm d~ > vm g s d~ s~ WEIGHTED<=7 @4
// Process: vm s~ > vm g d s~ d~ WEIGHTED<=7 @4
// Process: vt d~ > vt g s d~ s~ WEIGHTED<=7 @4
// Process: vt s~ > vt g d s~ d~ WEIGHTED<=7 @4
// Process: e+ u > ve~ g u u d~ WEIGHTED<=7 @4
// Process: e+ c > ve~ g c c s~ WEIGHTED<=7 @4
// Process: mu+ u > vm~ g u u d~ WEIGHTED<=7 @4
// Process: mu+ c > vm~ g c c s~ WEIGHTED<=7 @4
// Process: ve~ d > e+ g d d u~ WEIGHTED<=7 @4
// Process: ve~ s > e+ g s s c~ WEIGHTED<=7 @4
// Process: vm~ d > mu+ g d d u~ WEIGHTED<=7 @4
// Process: vm~ s > mu+ g s s c~ WEIGHTED<=7 @4
// Process: e+ d > ve~ g u u u~ WEIGHTED<=7 @4
// Process: e+ s > ve~ g c c c~ WEIGHTED<=7 @4
// Process: mu+ d > vm~ g u u u~ WEIGHTED<=7 @4
// Process: mu+ s > vm~ g c c c~ WEIGHTED<=7 @4
// Process: ve~ u > e+ g d d d~ WEIGHTED<=7 @4
// Process: ve~ c > e+ g s s s~ WEIGHTED<=7 @4
// Process: vm~ u > mu+ g d d d~ WEIGHTED<=7 @4
// Process: vm~ c > mu+ g s s s~ WEIGHTED<=7 @4
// Process: e+ d > ve~ g u d d~ WEIGHTED<=7 @4
// Process: e+ s > ve~ g c s s~ WEIGHTED<=7 @4
// Process: mu+ d > vm~ g u d d~ WEIGHTED<=7 @4
// Process: mu+ s > vm~ g c s s~ WEIGHTED<=7 @4
// Process: ve~ u > e+ g d u u~ WEIGHTED<=7 @4
// Process: ve~ c > e+ g s c c~ WEIGHTED<=7 @4
// Process: vm~ u > mu+ g d u u~ WEIGHTED<=7 @4
// Process: vm~ c > mu+ g s c c~ WEIGHTED<=7 @4
// Process: e+ u~ > ve~ g u u~ d~ WEIGHTED<=7 @4
// Process: e+ c~ > ve~ g c c~ s~ WEIGHTED<=7 @4
// Process: mu+ u~ > vm~ g u u~ d~ WEIGHTED<=7 @4
// Process: mu+ c~ > vm~ g c c~ s~ WEIGHTED<=7 @4
// Process: ve~ d~ > e+ g d d~ u~ WEIGHTED<=7 @4
// Process: ve~ s~ > e+ g s s~ c~ WEIGHTED<=7 @4
// Process: vm~ d~ > mu+ g d d~ u~ WEIGHTED<=7 @4
// Process: vm~ s~ > mu+ g s s~ c~ WEIGHTED<=7 @4
// Process: e+ u~ > ve~ g d d~ d~ WEIGHTED<=7 @4
// Process: e+ c~ > ve~ g s s~ s~ WEIGHTED<=7 @4
// Process: mu+ u~ > vm~ g d d~ d~ WEIGHTED<=7 @4
// Process: mu+ c~ > vm~ g s s~ s~ WEIGHTED<=7 @4
// Process: ve~ d~ > e+ g u u~ u~ WEIGHTED<=7 @4
// Process: ve~ s~ > e+ g c c~ c~ WEIGHTED<=7 @4
// Process: vm~ d~ > mu+ g u u~ u~ WEIGHTED<=7 @4
// Process: vm~ s~ > mu+ g c c~ c~ WEIGHTED<=7 @4
// Process: e+ d~ > ve~ g u d~ d~ WEIGHTED<=7 @4
// Process: e+ s~ > ve~ g c s~ s~ WEIGHTED<=7 @4
// Process: mu+ d~ > vm~ g u d~ d~ WEIGHTED<=7 @4
// Process: mu+ s~ > vm~ g c s~ s~ WEIGHTED<=7 @4
// Process: ve~ u~ > e+ g d u~ u~ WEIGHTED<=7 @4
// Process: ve~ c~ > e+ g s c~ c~ WEIGHTED<=7 @4
// Process: vm~ u~ > mu+ g d u~ u~ WEIGHTED<=7 @4
// Process: vm~ c~ > mu+ g s c~ c~ WEIGHTED<=7 @4
// Process: ve~ u > ve~ g u c c~ WEIGHTED<=7 @4
// Process: ve~ c > ve~ g c u u~ WEIGHTED<=7 @4
// Process: vm~ u > vm~ g u c c~ WEIGHTED<=7 @4
// Process: vm~ c > vm~ g c u u~ WEIGHTED<=7 @4
// Process: vt~ u > vt~ g u c c~ WEIGHTED<=7 @4
// Process: vt~ c > vt~ g c u u~ WEIGHTED<=7 @4
// Process: ve~ u > ve~ g u d d~ WEIGHTED<=7 @4
// Process: ve~ u > ve~ g u s s~ WEIGHTED<=7 @4
// Process: ve~ c > ve~ g c d d~ WEIGHTED<=7 @4
// Process: ve~ c > ve~ g c s s~ WEIGHTED<=7 @4
// Process: vm~ u > vm~ g u d d~ WEIGHTED<=7 @4
// Process: vm~ u > vm~ g u s s~ WEIGHTED<=7 @4
// Process: vm~ c > vm~ g c d d~ WEIGHTED<=7 @4
// Process: vm~ c > vm~ g c s s~ WEIGHTED<=7 @4
// Process: vt~ u > vt~ g u d d~ WEIGHTED<=7 @4
// Process: vt~ u > vt~ g u s s~ WEIGHTED<=7 @4
// Process: vt~ c > vt~ g c d d~ WEIGHTED<=7 @4
// Process: vt~ c > vt~ g c s s~ WEIGHTED<=7 @4
// Process: ve~ d > ve~ g u d u~ WEIGHTED<=7 @4
// Process: ve~ d > ve~ g c d c~ WEIGHTED<=7 @4
// Process: ve~ s > ve~ g u s u~ WEIGHTED<=7 @4
// Process: ve~ s > ve~ g c s c~ WEIGHTED<=7 @4
// Process: vm~ d > vm~ g u d u~ WEIGHTED<=7 @4
// Process: vm~ d > vm~ g c d c~ WEIGHTED<=7 @4
// Process: vm~ s > vm~ g u s u~ WEIGHTED<=7 @4
// Process: vm~ s > vm~ g c s c~ WEIGHTED<=7 @4
// Process: vt~ d > vt~ g u d u~ WEIGHTED<=7 @4
// Process: vt~ d > vt~ g c d c~ WEIGHTED<=7 @4
// Process: vt~ s > vt~ g u s u~ WEIGHTED<=7 @4
// Process: vt~ s > vt~ g c s c~ WEIGHTED<=7 @4
// Process: ve~ d > ve~ g d s s~ WEIGHTED<=7 @4
// Process: ve~ s > ve~ g s d d~ WEIGHTED<=7 @4
// Process: vm~ d > vm~ g d s s~ WEIGHTED<=7 @4
// Process: vm~ s > vm~ g s d d~ WEIGHTED<=7 @4
// Process: vt~ d > vt~ g d s s~ WEIGHTED<=7 @4
// Process: vt~ s > vt~ g s d d~ WEIGHTED<=7 @4
// Process: ve~ u~ > ve~ g c u~ c~ WEIGHTED<=7 @4
// Process: ve~ c~ > ve~ g u c~ u~ WEIGHTED<=7 @4
// Process: vm~ u~ > vm~ g c u~ c~ WEIGHTED<=7 @4
// Process: vm~ c~ > vm~ g u c~ u~ WEIGHTED<=7 @4
// Process: vt~ u~ > vt~ g c u~ c~ WEIGHTED<=7 @4
// Process: vt~ c~ > vt~ g u c~ u~ WEIGHTED<=7 @4
// Process: ve~ u~ > ve~ g d u~ d~ WEIGHTED<=7 @4
// Process: ve~ u~ > ve~ g s u~ s~ WEIGHTED<=7 @4
// Process: ve~ c~ > ve~ g d c~ d~ WEIGHTED<=7 @4
// Process: ve~ c~ > ve~ g s c~ s~ WEIGHTED<=7 @4
// Process: vm~ u~ > vm~ g d u~ d~ WEIGHTED<=7 @4
// Process: vm~ u~ > vm~ g s u~ s~ WEIGHTED<=7 @4
// Process: vm~ c~ > vm~ g d c~ d~ WEIGHTED<=7 @4
// Process: vm~ c~ > vm~ g s c~ s~ WEIGHTED<=7 @4
// Process: vt~ u~ > vt~ g d u~ d~ WEIGHTED<=7 @4
// Process: vt~ u~ > vt~ g s u~ s~ WEIGHTED<=7 @4
// Process: vt~ c~ > vt~ g d c~ d~ WEIGHTED<=7 @4
// Process: vt~ c~ > vt~ g s c~ s~ WEIGHTED<=7 @4
// Process: ve~ d~ > ve~ g u u~ d~ WEIGHTED<=7 @4
// Process: ve~ d~ > ve~ g c c~ d~ WEIGHTED<=7 @4
// Process: ve~ s~ > ve~ g u u~ s~ WEIGHTED<=7 @4
// Process: ve~ s~ > ve~ g c c~ s~ WEIGHTED<=7 @4
// Process: vm~ d~ > vm~ g u u~ d~ WEIGHTED<=7 @4
// Process: vm~ d~ > vm~ g c c~ d~ WEIGHTED<=7 @4
// Process: vm~ s~ > vm~ g u u~ s~ WEIGHTED<=7 @4
// Process: vm~ s~ > vm~ g c c~ s~ WEIGHTED<=7 @4
// Process: vt~ d~ > vt~ g u u~ d~ WEIGHTED<=7 @4
// Process: vt~ d~ > vt~ g c c~ d~ WEIGHTED<=7 @4
// Process: vt~ s~ > vt~ g u u~ s~ WEIGHTED<=7 @4
// Process: vt~ s~ > vt~ g c c~ s~ WEIGHTED<=7 @4
// Process: ve~ d~ > ve~ g s d~ s~ WEIGHTED<=7 @4
// Process: ve~ s~ > ve~ g d s~ d~ WEIGHTED<=7 @4
// Process: vm~ d~ > vm~ g s d~ s~ WEIGHTED<=7 @4
// Process: vm~ s~ > vm~ g d s~ d~ WEIGHTED<=7 @4
// Process: vt~ d~ > vt~ g s d~ s~ WEIGHTED<=7 @4
// Process: vt~ s~ > vt~ g d s~ d~ WEIGHTED<=7 @4
// Process: e- u > ve g u s c~ WEIGHTED<=7 @4
// Process: e- c > ve g c d u~ WEIGHTED<=7 @4
// Process: e- d > ve g d s c~ WEIGHTED<=7 @4
// Process: e- s > ve g s d u~ WEIGHTED<=7 @4
// Process: mu- u > vm g u s c~ WEIGHTED<=7 @4
// Process: mu- c > vm g c d u~ WEIGHTED<=7 @4
// Process: mu- d > vm g d s c~ WEIGHTED<=7 @4
// Process: mu- s > vm g s d u~ WEIGHTED<=7 @4
// Process: ve u > e- g u c s~ WEIGHTED<=7 @4
// Process: ve c > e- g c u d~ WEIGHTED<=7 @4
// Process: ve d > e- g d c s~ WEIGHTED<=7 @4
// Process: ve s > e- g s u d~ WEIGHTED<=7 @4
// Process: vm u > mu- g u c s~ WEIGHTED<=7 @4
// Process: vm c > mu- g c u d~ WEIGHTED<=7 @4
// Process: vm d > mu- g d c s~ WEIGHTED<=7 @4
// Process: vm s > mu- g s u d~ WEIGHTED<=7 @4
// Process: e- u > ve g c d c~ WEIGHTED<=7 @4
// Process: e- u > ve g s d s~ WEIGHTED<=7 @4
// Process: e- c > ve g u s u~ WEIGHTED<=7 @4
// Process: e- c > ve g d s d~ WEIGHTED<=7 @4
// Process: mu- u > vm g c d c~ WEIGHTED<=7 @4
// Process: mu- u > vm g s d s~ WEIGHTED<=7 @4
// Process: mu- c > vm g u s u~ WEIGHTED<=7 @4
// Process: mu- c > vm g d s d~ WEIGHTED<=7 @4
// Process: ve d > e- g c u c~ WEIGHTED<=7 @4
// Process: ve d > e- g s u s~ WEIGHTED<=7 @4
// Process: ve s > e- g u c u~ WEIGHTED<=7 @4
// Process: ve s > e- g d c d~ WEIGHTED<=7 @4
// Process: vm d > mu- g c u c~ WEIGHTED<=7 @4
// Process: vm d > mu- g s u s~ WEIGHTED<=7 @4
// Process: vm s > mu- g u c u~ WEIGHTED<=7 @4
// Process: vm s > mu- g d c d~ WEIGHTED<=7 @4
// Process: e- u~ > ve g s u~ c~ WEIGHTED<=7 @4
// Process: e- c~ > ve g d c~ u~ WEIGHTED<=7 @4
// Process: e- d~ > ve g s d~ c~ WEIGHTED<=7 @4
// Process: e- s~ > ve g d s~ u~ WEIGHTED<=7 @4
// Process: mu- u~ > vm g s u~ c~ WEIGHTED<=7 @4
// Process: mu- c~ > vm g d c~ u~ WEIGHTED<=7 @4
// Process: mu- d~ > vm g s d~ c~ WEIGHTED<=7 @4
// Process: mu- s~ > vm g d s~ u~ WEIGHTED<=7 @4
// Process: ve u~ > e- g c u~ s~ WEIGHTED<=7 @4
// Process: ve c~ > e- g u c~ d~ WEIGHTED<=7 @4
// Process: ve d~ > e- g c d~ s~ WEIGHTED<=7 @4
// Process: ve s~ > e- g u s~ d~ WEIGHTED<=7 @4
// Process: vm u~ > mu- g c u~ s~ WEIGHTED<=7 @4
// Process: vm c~ > mu- g u c~ d~ WEIGHTED<=7 @4
// Process: vm d~ > mu- g c d~ s~ WEIGHTED<=7 @4
// Process: vm s~ > mu- g u s~ d~ WEIGHTED<=7 @4
// Process: e- d~ > ve g c u~ c~ WEIGHTED<=7 @4
// Process: e- d~ > ve g s u~ s~ WEIGHTED<=7 @4
// Process: e- s~ > ve g u c~ u~ WEIGHTED<=7 @4
// Process: e- s~ > ve g d c~ d~ WEIGHTED<=7 @4
// Process: mu- d~ > vm g c u~ c~ WEIGHTED<=7 @4
// Process: mu- d~ > vm g s u~ s~ WEIGHTED<=7 @4
// Process: mu- s~ > vm g u c~ u~ WEIGHTED<=7 @4
// Process: mu- s~ > vm g d c~ d~ WEIGHTED<=7 @4
// Process: ve u~ > e- g c d~ c~ WEIGHTED<=7 @4
// Process: ve u~ > e- g s d~ s~ WEIGHTED<=7 @4
// Process: ve c~ > e- g u s~ u~ WEIGHTED<=7 @4
// Process: ve c~ > e- g d s~ d~ WEIGHTED<=7 @4
// Process: vm u~ > mu- g c d~ c~ WEIGHTED<=7 @4
// Process: vm u~ > mu- g s d~ s~ WEIGHTED<=7 @4
// Process: vm c~ > mu- g u s~ u~ WEIGHTED<=7 @4
// Process: vm c~ > mu- g d s~ d~ WEIGHTED<=7 @4
// Process: e+ u > ve~ g u c s~ WEIGHTED<=7 @4
// Process: e+ c > ve~ g c u d~ WEIGHTED<=7 @4
// Process: e+ d > ve~ g d c s~ WEIGHTED<=7 @4
// Process: e+ s > ve~ g s u d~ WEIGHTED<=7 @4
// Process: mu+ u > vm~ g u c s~ WEIGHTED<=7 @4
// Process: mu+ c > vm~ g c u d~ WEIGHTED<=7 @4
// Process: mu+ d > vm~ g d c s~ WEIGHTED<=7 @4
// Process: mu+ s > vm~ g s u d~ WEIGHTED<=7 @4
// Process: ve~ u > e+ g u s c~ WEIGHTED<=7 @4
// Process: ve~ c > e+ g c d u~ WEIGHTED<=7 @4
// Process: ve~ d > e+ g d s c~ WEIGHTED<=7 @4
// Process: ve~ s > e+ g s d u~ WEIGHTED<=7 @4
// Process: vm~ u > mu+ g u s c~ WEIGHTED<=7 @4
// Process: vm~ c > mu+ g c d u~ WEIGHTED<=7 @4
// Process: vm~ d > mu+ g d s c~ WEIGHTED<=7 @4
// Process: vm~ s > mu+ g s d u~ WEIGHTED<=7 @4
// Process: e+ d > ve~ g u c c~ WEIGHTED<=7 @4
// Process: e+ d > ve~ g u s s~ WEIGHTED<=7 @4
// Process: e+ s > ve~ g c u u~ WEIGHTED<=7 @4
// Process: e+ s > ve~ g c d d~ WEIGHTED<=7 @4
// Process: mu+ d > vm~ g u c c~ WEIGHTED<=7 @4
// Process: mu+ d > vm~ g u s s~ WEIGHTED<=7 @4
// Process: mu+ s > vm~ g c u u~ WEIGHTED<=7 @4
// Process: mu+ s > vm~ g c d d~ WEIGHTED<=7 @4
// Process: ve~ u > e+ g d c c~ WEIGHTED<=7 @4
// Process: ve~ u > e+ g d s s~ WEIGHTED<=7 @4
// Process: ve~ c > e+ g s u u~ WEIGHTED<=7 @4
// Process: ve~ c > e+ g s d d~ WEIGHTED<=7 @4
// Process: vm~ u > mu+ g d c c~ WEIGHTED<=7 @4
// Process: vm~ u > mu+ g d s s~ WEIGHTED<=7 @4
// Process: vm~ c > mu+ g s u u~ WEIGHTED<=7 @4
// Process: vm~ c > mu+ g s d d~ WEIGHTED<=7 @4
// Process: e+ u~ > ve~ g c u~ s~ WEIGHTED<=7 @4
// Process: e+ c~ > ve~ g u c~ d~ WEIGHTED<=7 @4
// Process: e+ d~ > ve~ g c d~ s~ WEIGHTED<=7 @4
// Process: e+ s~ > ve~ g u s~ d~ WEIGHTED<=7 @4
// Process: mu+ u~ > vm~ g c u~ s~ WEIGHTED<=7 @4
// Process: mu+ c~ > vm~ g u c~ d~ WEIGHTED<=7 @4
// Process: mu+ d~ > vm~ g c d~ s~ WEIGHTED<=7 @4
// Process: mu+ s~ > vm~ g u s~ d~ WEIGHTED<=7 @4
// Process: ve~ u~ > e+ g s u~ c~ WEIGHTED<=7 @4
// Process: ve~ c~ > e+ g d c~ u~ WEIGHTED<=7 @4
// Process: ve~ d~ > e+ g s d~ c~ WEIGHTED<=7 @4
// Process: ve~ s~ > e+ g d s~ u~ WEIGHTED<=7 @4
// Process: vm~ u~ > mu+ g s u~ c~ WEIGHTED<=7 @4
// Process: vm~ c~ > mu+ g d c~ u~ WEIGHTED<=7 @4
// Process: vm~ d~ > mu+ g s d~ c~ WEIGHTED<=7 @4
// Process: vm~ s~ > mu+ g d s~ u~ WEIGHTED<=7 @4
// Process: e+ u~ > ve~ g c c~ d~ WEIGHTED<=7 @4
// Process: e+ u~ > ve~ g s s~ d~ WEIGHTED<=7 @4
// Process: e+ c~ > ve~ g u u~ s~ WEIGHTED<=7 @4
// Process: e+ c~ > ve~ g d d~ s~ WEIGHTED<=7 @4
// Process: mu+ u~ > vm~ g c c~ d~ WEIGHTED<=7 @4
// Process: mu+ u~ > vm~ g s s~ d~ WEIGHTED<=7 @4
// Process: mu+ c~ > vm~ g u u~ s~ WEIGHTED<=7 @4
// Process: mu+ c~ > vm~ g d d~ s~ WEIGHTED<=7 @4
// Process: ve~ d~ > e+ g c c~ u~ WEIGHTED<=7 @4
// Process: ve~ d~ > e+ g s s~ u~ WEIGHTED<=7 @4
// Process: ve~ s~ > e+ g u u~ c~ WEIGHTED<=7 @4
// Process: ve~ s~ > e+ g d d~ c~ WEIGHTED<=7 @4
// Process: vm~ d~ > mu+ g c c~ u~ WEIGHTED<=7 @4
// Process: vm~ d~ > mu+ g s s~ u~ WEIGHTED<=7 @4
// Process: vm~ s~ > mu+ g u u~ c~ WEIGHTED<=7 @4
// Process: vm~ s~ > mu+ g d d~ c~ WEIGHTED<=7 @4
//--------------------------------------------------------------------------

typedef vector<double> vec_double; 
typedef vector < vec_double > vec_vec_double; 
typedef vector<int> vec_int; 
typedef vector<bool> vec_bool; 
typedef vector < vec_int > vec_vec_int; 

class PY8MEs_R4_P12_sm_lq_lgqqq : public PY8ME
{
  public:

    // Check for the availability of the requested proces.
    // If available, this returns the corresponding permutation and Proc_ID  to
    // use.
    // If not available, this returns a negative Proc_ID.
    static pair < vector<int> , int > static_getPY8ME(vector<int> initial_pdgs,
        vector<int> final_pdgs, set<int> schannels = set<int> ());

    // Constructor.
    PY8MEs_R4_P12_sm_lq_lgqqq(Parameters_sm * model) : pars(model) {initProc();}

    // Destructor.
    ~PY8MEs_R4_P12_sm_lq_lgqqq(); 

    // Initialize process.
    virtual void initProc(); 

    // Calculate squared ME.
    virtual double sigmaKin(); 

    // Info on the subprocess.
    virtual string name() const {return "lq_lgqqq (sm)";}

    virtual int code() const {return 10412;}

    virtual string inFlux() const {return "ff";}

    virtual vector<double> getMasses(); 

    virtual void setMasses(vec_double external_masses); 
    // Set all values of the external masses to an integer mode:
    // 0 : Mass taken from the model
    // 1 : Mass taken from p_i^2 if not massless to begin with
    // 2 : Mass always taken from p_i^2.
    virtual void setExternalMassesMode(int mode); 

    // Synchronize local variables of the process that depend on the model
    // parameters
    virtual void syncProcModelParams(); 

    // Tell Pythia that sigmaHat returns the ME^2
    virtual bool convertM2() const {return true;}

    // Access to getPY8ME with polymorphism from a non-static context
    virtual pair < vector<int> , int > getPY8ME(vector<int> initial_pdgs,
        vector<int> final_pdgs, set<int> schannels = set<int> ())
    {
      return static_getPY8ME(initial_pdgs, final_pdgs, schannels); 
    }

    // Set momenta
    virtual void setMomenta(vector < vec_double > momenta_picked); 

    // Set color configuration to use. An empty vector means sum over all.
    virtual void setColors(vector<int> colors_picked); 

    // Set the helicity configuration to use. Am empty vector means sum over
    // all.
    virtual void setHelicities(vector<int> helicities_picked); 

    // Set the permutation to use (will apply to momenta, colors and helicities)
    virtual void setPermutation(vector<int> perm_picked); 

    // Set the proc_ID to use
    virtual void setProcID(int procID_picked); 

    // Access to all the helicity and color configurations for a given process
    virtual vector < vec_int > getColorConfigs(int specify_proc_ID = -1,
        vector<int> permutation = vector<int> ());
    virtual vector < vec_int > getHelicityConfigs(vector<int> permutation =
        vector<int> ());

    // Maps of Helicity <-> hel_ID and ColorConfig <-> colorConfig_ID.
    virtual vector<int> getHelicityConfigForID(int hel_ID, vector<int>
        permutation = vector<int> ());
    virtual int getHelicityIDForConfig(vector<int> hel_config, vector<int>
        permutation = vector<int> ());
    virtual vector<int> getColorConfigForID(int color_ID, int specify_proc_ID =
        -1, vector<int> permutation = vector<int> ());
    virtual int getColorIDForConfig(vector<int> color_config, int
        specify_proc_ID = -1, vector<int> permutation = vector<int> ());
    virtual int getColorFlowRelativeNCPower(int color_flow_ID, int
        specify_proc_ID = -1);

    // Access previously computed results
    virtual vector < vec_double > getAllResults(int specify_proc_ID = -1); 
    virtual double getResult(int helicity_ID, int color_ID, int specify_proc_ID
        = -1);

    // Accessors
    Parameters_sm * getModel() {return pars;}
    void setModel(Parameters_sm * model) {pars = model;}

    // Invert the permutation mapping
    vector<int> invert_mapping(vector<int> mapping); 

    // Control whether to include the symmetry factors or not
    virtual void setIncludeSymmetryFactors(bool OnOff) 
    {
      include_symmetry_factors = OnOff; 
    }
    virtual bool getIncludeSymmetryFactors() {return include_symmetry_factors;}
    virtual int getSymmetryFactor() {return denom_iden[proc_ID];}

    // Control whether to include helicity averaging factors or not
    virtual void setIncludeHelicityAveragingFactors(bool OnOff) 
    {
      include_helicity_averaging_factors = OnOff; 
    }
    virtual bool getIncludeHelicityAveragingFactors() 
    {
      return include_helicity_averaging_factors; 
    }
    virtual int getHelicityAveragingFactor() {return denom_hels[proc_ID];}

    // Control whether to include color averaging factors or not
    virtual void setIncludeColorAveragingFactors(bool OnOff) 
    {
      include_color_averaging_factors = OnOff; 
    }
    virtual bool getIncludeColorAveragingFactors() 
    {
      return include_color_averaging_factors; 
    }
    virtual int getColorAveragingFactor() {return denom_colors[proc_ID];}

  private:

    // Private functions to calculate the matrix element for all subprocesses
    // Calculate wavefunctions
    void calculate_wavefunctions(const int hel[]); 
    static const int nwavefuncs = 237; 
    Complex<double> w[nwavefuncs][18]; 
    static const int namplitudes = 2687; 
    Complex<double> amp[namplitudes]; 
    double matrix_4_emu_emguuux(); 
    double matrix_4_emd_emgdddx(); 
    double matrix_4_emux_emguuxux(); 
    double matrix_4_emdx_emgddxdx(); 
    double matrix_4_epu_epguuux(); 
    double matrix_4_epd_epgdddx(); 
    double matrix_4_epux_epguuxux(); 
    double matrix_4_epdx_epgddxdx(); 
    double matrix_4_emu_emguccx(); 
    double matrix_4_emu_emguddx(); 
    double matrix_4_emd_emgudux(); 
    double matrix_4_emd_emgdssx(); 
    double matrix_4_emux_emgcuxcx(); 
    double matrix_4_emux_emgduxdx(); 
    double matrix_4_emdx_emguuxdx(); 
    double matrix_4_emdx_emgsdxsx(); 
    double matrix_4_veu_veguuux(); 
    double matrix_4_ved_vegdddx(); 
    double matrix_4_veux_veguuxux(); 
    double matrix_4_vedx_vegddxdx(); 
    double matrix_4_epu_epguccx(); 
    double matrix_4_epu_epguddx(); 
    double matrix_4_epd_epgudux(); 
    double matrix_4_epd_epgdssx(); 
    double matrix_4_epux_epgcuxcx(); 
    double matrix_4_epux_epgduxdx(); 
    double matrix_4_epdx_epguuxdx(); 
    double matrix_4_epdx_epgsdxsx(); 
    double matrix_4_vexu_vexguuux(); 
    double matrix_4_vexd_vexgdddx(); 
    double matrix_4_vexux_vexguuxux(); 
    double matrix_4_vexdx_vexgddxdx(); 
    double matrix_4_emu_vegudux(); 
    double matrix_4_emu_vegdddx(); 
    double matrix_4_emd_vegddux(); 
    double matrix_4_emux_vegduxux(); 
    double matrix_4_emdx_veguuxux(); 
    double matrix_4_emdx_vegduxdx(); 
    double matrix_4_veu_veguccx(); 
    double matrix_4_veu_veguddx(); 
    double matrix_4_ved_vegudux(); 
    double matrix_4_ved_vegdssx(); 
    double matrix_4_veux_vegcuxcx(); 
    double matrix_4_veux_vegduxdx(); 
    double matrix_4_vedx_veguuxdx(); 
    double matrix_4_vedx_vegsdxsx(); 
    double matrix_4_epu_vexguudx(); 
    double matrix_4_epd_vexguuux(); 
    double matrix_4_epd_vexguddx(); 
    double matrix_4_epux_vexguuxdx(); 
    double matrix_4_epux_vexgddxdx(); 
    double matrix_4_epdx_vexgudxdx(); 
    double matrix_4_vexu_vexguccx(); 
    double matrix_4_vexu_vexguddx(); 
    double matrix_4_vexd_vexgudux(); 
    double matrix_4_vexd_vexgdssx(); 
    double matrix_4_vexux_vexgcuxcx(); 
    double matrix_4_vexux_vexgduxdx(); 
    double matrix_4_vexdx_vexguuxdx(); 
    double matrix_4_vexdx_vexgsdxsx(); 
    double matrix_4_emu_veguscx(); 
    double matrix_4_emu_vegcdcx(); 
    double matrix_4_emux_vegsuxcx(); 
    double matrix_4_emdx_vegcuxcx(); 
    double matrix_4_epu_vexgucsx(); 
    double matrix_4_epd_vexguccx(); 
    double matrix_4_epux_vexgcuxsx(); 
    double matrix_4_epux_vexgccxdx(); 

    // Constants for array limits
    static const int nexternal = 7; 
    static const int ninitial = 2; 
    static const int nprocesses = 68; 
    static const int nreq_s_channels = 0; 
    static const int ncomb = 128; 

    // Helicities for the process
    static int helicities[ncomb][nexternal]; 

    // Normalization factors the various processes
    static int denom_colors[nprocesses]; 
    static int denom_hels[nprocesses]; 
    static int denom_iden[nprocesses]; 

    // Control whether to include symmetry factors or not
    bool include_symmetry_factors; 
    // Control whether to include helicity averaging factors or not
    bool include_helicity_averaging_factors; 
    // Control whether to include color averaging factors or not
    bool include_color_averaging_factors; 

    // Color flows, used when selecting color
    vector < vec_double > jamp2; 

    // Store individual results (for each color flow, helicity configurations
    // and proc_ID)
    // computed in the last call to sigmaKin().
    vector < vec_vec_double > all_results; 

    // required s-channels specified
    static std::set<int> s_channel_proc; 

    // vector with external particle masses
    vector<double> mME; 

    // vector with momenta (to be changed for each event)
    vector < double * > p; 

    // external particles permutation (to be changed for each event)
    vector<int> perm; 

    // vector with colors (to be changed for each event)
    vector<int> user_colors; 

    // vector with helicities (to be changed for each event)
    vector<int> user_helicities; 

    // Process ID (to be changed for each event)
    int proc_ID; 

    // All color configurations
    void initColorConfigs(); 
    vector < vec_vec_int > color_configs; 

    // Color flows relative N_c power (conventions are such that all elements
    // on the color matrix diagonal are identical).
    vector < vec_int > jamp_nc_relative_power; 

    // Model pointer to be used by this matrix element
    Parameters_sm * pars; 

}; 

}  // end namespace PY8MEs_namespace

#endif  // PY8MEs_R4_P12_sm_lq_lgqqq_H

