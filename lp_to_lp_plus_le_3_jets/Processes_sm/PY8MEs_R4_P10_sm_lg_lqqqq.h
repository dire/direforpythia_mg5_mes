//==========================================================================
// This file has been automatically generated for Pythia 8
// MadGraph5_aMC@NLO v. 2.6.0, 2017-08-16
// By the MadGraph5_aMC@NLO Development Team
// Visit launchpad.net/madgraph5 and amcatnlo.web.cern.ch
//==========================================================================

#ifndef PY8MEs_R4_P10_sm_lg_lqqqq_H
#define PY8MEs_R4_P10_sm_lg_lqqqq_H

#include "Complex.h" 
#include <vector> 
#include <set> 
#include <exception> 
#include <iostream> 

#include "Parameters_sm.h"
#include "PY8MEs.h"

using namespace std; 

namespace PY8MEs_namespace 
{
//==========================================================================
// A class for calculating the matrix elements for
// Process: e- g > e- u u u~ u~ WEIGHTED<=7 @4
// Process: e- g > e- c c c~ c~ WEIGHTED<=7 @4
// Process: mu- g > mu- u u u~ u~ WEIGHTED<=7 @4
// Process: mu- g > mu- c c c~ c~ WEIGHTED<=7 @4
// Process: e- g > e- d d d~ d~ WEIGHTED<=7 @4
// Process: e- g > e- s s s~ s~ WEIGHTED<=7 @4
// Process: mu- g > mu- d d d~ d~ WEIGHTED<=7 @4
// Process: mu- g > mu- s s s~ s~ WEIGHTED<=7 @4
// Process: e+ g > e+ u u u~ u~ WEIGHTED<=7 @4
// Process: e+ g > e+ c c c~ c~ WEIGHTED<=7 @4
// Process: mu+ g > mu+ u u u~ u~ WEIGHTED<=7 @4
// Process: mu+ g > mu+ c c c~ c~ WEIGHTED<=7 @4
// Process: e+ g > e+ d d d~ d~ WEIGHTED<=7 @4
// Process: e+ g > e+ s s s~ s~ WEIGHTED<=7 @4
// Process: mu+ g > mu+ d d d~ d~ WEIGHTED<=7 @4
// Process: mu+ g > mu+ s s s~ s~ WEIGHTED<=7 @4
// Process: e- g > e- u c u~ c~ WEIGHTED<=7 @4
// Process: mu- g > mu- u c u~ c~ WEIGHTED<=7 @4
// Process: e- g > e- u d u~ d~ WEIGHTED<=7 @4
// Process: e- g > e- u s u~ s~ WEIGHTED<=7 @4
// Process: e- g > e- c d c~ d~ WEIGHTED<=7 @4
// Process: e- g > e- c s c~ s~ WEIGHTED<=7 @4
// Process: mu- g > mu- u d u~ d~ WEIGHTED<=7 @4
// Process: mu- g > mu- u s u~ s~ WEIGHTED<=7 @4
// Process: mu- g > mu- c d c~ d~ WEIGHTED<=7 @4
// Process: mu- g > mu- c s c~ s~ WEIGHTED<=7 @4
// Process: e- g > e- d s d~ s~ WEIGHTED<=7 @4
// Process: mu- g > mu- d s d~ s~ WEIGHTED<=7 @4
// Process: ve g > ve u u u~ u~ WEIGHTED<=7 @4
// Process: ve g > ve c c c~ c~ WEIGHTED<=7 @4
// Process: vm g > vm u u u~ u~ WEIGHTED<=7 @4
// Process: vm g > vm c c c~ c~ WEIGHTED<=7 @4
// Process: vt g > vt u u u~ u~ WEIGHTED<=7 @4
// Process: vt g > vt c c c~ c~ WEIGHTED<=7 @4
// Process: ve g > ve d d d~ d~ WEIGHTED<=7 @4
// Process: ve g > ve s s s~ s~ WEIGHTED<=7 @4
// Process: vm g > vm d d d~ d~ WEIGHTED<=7 @4
// Process: vm g > vm s s s~ s~ WEIGHTED<=7 @4
// Process: vt g > vt d d d~ d~ WEIGHTED<=7 @4
// Process: vt g > vt s s s~ s~ WEIGHTED<=7 @4
// Process: e+ g > e+ u c u~ c~ WEIGHTED<=7 @4
// Process: mu+ g > mu+ u c u~ c~ WEIGHTED<=7 @4
// Process: e+ g > e+ u d u~ d~ WEIGHTED<=7 @4
// Process: e+ g > e+ u s u~ s~ WEIGHTED<=7 @4
// Process: e+ g > e+ c d c~ d~ WEIGHTED<=7 @4
// Process: e+ g > e+ c s c~ s~ WEIGHTED<=7 @4
// Process: mu+ g > mu+ u d u~ d~ WEIGHTED<=7 @4
// Process: mu+ g > mu+ u s u~ s~ WEIGHTED<=7 @4
// Process: mu+ g > mu+ c d c~ d~ WEIGHTED<=7 @4
// Process: mu+ g > mu+ c s c~ s~ WEIGHTED<=7 @4
// Process: e+ g > e+ d s d~ s~ WEIGHTED<=7 @4
// Process: mu+ g > mu+ d s d~ s~ WEIGHTED<=7 @4
// Process: ve~ g > ve~ u u u~ u~ WEIGHTED<=7 @4
// Process: ve~ g > ve~ c c c~ c~ WEIGHTED<=7 @4
// Process: vm~ g > vm~ u u u~ u~ WEIGHTED<=7 @4
// Process: vm~ g > vm~ c c c~ c~ WEIGHTED<=7 @4
// Process: vt~ g > vt~ u u u~ u~ WEIGHTED<=7 @4
// Process: vt~ g > vt~ c c c~ c~ WEIGHTED<=7 @4
// Process: ve~ g > ve~ d d d~ d~ WEIGHTED<=7 @4
// Process: ve~ g > ve~ s s s~ s~ WEIGHTED<=7 @4
// Process: vm~ g > vm~ d d d~ d~ WEIGHTED<=7 @4
// Process: vm~ g > vm~ s s s~ s~ WEIGHTED<=7 @4
// Process: vt~ g > vt~ d d d~ d~ WEIGHTED<=7 @4
// Process: vt~ g > vt~ s s s~ s~ WEIGHTED<=7 @4
// Process: e- g > ve u d u~ u~ WEIGHTED<=7 @4
// Process: e- g > ve c s c~ c~ WEIGHTED<=7 @4
// Process: mu- g > vm u d u~ u~ WEIGHTED<=7 @4
// Process: mu- g > vm c s c~ c~ WEIGHTED<=7 @4
// Process: ve g > e- d u d~ d~ WEIGHTED<=7 @4
// Process: ve g > e- s c s~ s~ WEIGHTED<=7 @4
// Process: vm g > mu- d u d~ d~ WEIGHTED<=7 @4
// Process: vm g > mu- s c s~ s~ WEIGHTED<=7 @4
// Process: e- g > ve d d u~ d~ WEIGHTED<=7 @4
// Process: e- g > ve s s c~ s~ WEIGHTED<=7 @4
// Process: mu- g > vm d d u~ d~ WEIGHTED<=7 @4
// Process: mu- g > vm s s c~ s~ WEIGHTED<=7 @4
// Process: ve g > e- u u d~ u~ WEIGHTED<=7 @4
// Process: ve g > e- c c s~ c~ WEIGHTED<=7 @4
// Process: vm g > mu- u u d~ u~ WEIGHTED<=7 @4
// Process: vm g > mu- c c s~ c~ WEIGHTED<=7 @4
// Process: ve g > ve u c u~ c~ WEIGHTED<=7 @4
// Process: vm g > vm u c u~ c~ WEIGHTED<=7 @4
// Process: vt g > vt u c u~ c~ WEIGHTED<=7 @4
// Process: ve g > ve u d u~ d~ WEIGHTED<=7 @4
// Process: ve g > ve u s u~ s~ WEIGHTED<=7 @4
// Process: ve g > ve c d c~ d~ WEIGHTED<=7 @4
// Process: ve g > ve c s c~ s~ WEIGHTED<=7 @4
// Process: vm g > vm u d u~ d~ WEIGHTED<=7 @4
// Process: vm g > vm u s u~ s~ WEIGHTED<=7 @4
// Process: vm g > vm c d c~ d~ WEIGHTED<=7 @4
// Process: vm g > vm c s c~ s~ WEIGHTED<=7 @4
// Process: vt g > vt u d u~ d~ WEIGHTED<=7 @4
// Process: vt g > vt u s u~ s~ WEIGHTED<=7 @4
// Process: vt g > vt c d c~ d~ WEIGHTED<=7 @4
// Process: vt g > vt c s c~ s~ WEIGHTED<=7 @4
// Process: ve g > ve d s d~ s~ WEIGHTED<=7 @4
// Process: vm g > vm d s d~ s~ WEIGHTED<=7 @4
// Process: vt g > vt d s d~ s~ WEIGHTED<=7 @4
// Process: e+ g > ve~ u u u~ d~ WEIGHTED<=7 @4
// Process: e+ g > ve~ c c c~ s~ WEIGHTED<=7 @4
// Process: mu+ g > vm~ u u u~ d~ WEIGHTED<=7 @4
// Process: mu+ g > vm~ c c c~ s~ WEIGHTED<=7 @4
// Process: ve~ g > e+ d d d~ u~ WEIGHTED<=7 @4
// Process: ve~ g > e+ s s s~ c~ WEIGHTED<=7 @4
// Process: vm~ g > mu+ d d d~ u~ WEIGHTED<=7 @4
// Process: vm~ g > mu+ s s s~ c~ WEIGHTED<=7 @4
// Process: e+ g > ve~ u d d~ d~ WEIGHTED<=7 @4
// Process: e+ g > ve~ c s s~ s~ WEIGHTED<=7 @4
// Process: mu+ g > vm~ u d d~ d~ WEIGHTED<=7 @4
// Process: mu+ g > vm~ c s s~ s~ WEIGHTED<=7 @4
// Process: ve~ g > e+ d u u~ u~ WEIGHTED<=7 @4
// Process: ve~ g > e+ s c c~ c~ WEIGHTED<=7 @4
// Process: vm~ g > mu+ d u u~ u~ WEIGHTED<=7 @4
// Process: vm~ g > mu+ s c c~ c~ WEIGHTED<=7 @4
// Process: ve~ g > ve~ u c u~ c~ WEIGHTED<=7 @4
// Process: vm~ g > vm~ u c u~ c~ WEIGHTED<=7 @4
// Process: vt~ g > vt~ u c u~ c~ WEIGHTED<=7 @4
// Process: ve~ g > ve~ u d u~ d~ WEIGHTED<=7 @4
// Process: ve~ g > ve~ u s u~ s~ WEIGHTED<=7 @4
// Process: ve~ g > ve~ c d c~ d~ WEIGHTED<=7 @4
// Process: ve~ g > ve~ c s c~ s~ WEIGHTED<=7 @4
// Process: vm~ g > vm~ u d u~ d~ WEIGHTED<=7 @4
// Process: vm~ g > vm~ u s u~ s~ WEIGHTED<=7 @4
// Process: vm~ g > vm~ c d c~ d~ WEIGHTED<=7 @4
// Process: vm~ g > vm~ c s c~ s~ WEIGHTED<=7 @4
// Process: vt~ g > vt~ u d u~ d~ WEIGHTED<=7 @4
// Process: vt~ g > vt~ u s u~ s~ WEIGHTED<=7 @4
// Process: vt~ g > vt~ c d c~ d~ WEIGHTED<=7 @4
// Process: vt~ g > vt~ c s c~ s~ WEIGHTED<=7 @4
// Process: ve~ g > ve~ d s d~ s~ WEIGHTED<=7 @4
// Process: vm~ g > vm~ d s d~ s~ WEIGHTED<=7 @4
// Process: vt~ g > vt~ d s d~ s~ WEIGHTED<=7 @4
// Process: e- g > ve u s u~ c~ WEIGHTED<=7 @4
// Process: e- g > ve c d c~ u~ WEIGHTED<=7 @4
// Process: e- g > ve s d s~ u~ WEIGHTED<=7 @4
// Process: e- g > ve d s d~ c~ WEIGHTED<=7 @4
// Process: mu- g > vm u s u~ c~ WEIGHTED<=7 @4
// Process: mu- g > vm c d c~ u~ WEIGHTED<=7 @4
// Process: mu- g > vm s d s~ u~ WEIGHTED<=7 @4
// Process: mu- g > vm d s d~ c~ WEIGHTED<=7 @4
// Process: ve g > e- u c u~ s~ WEIGHTED<=7 @4
// Process: ve g > e- c u c~ d~ WEIGHTED<=7 @4
// Process: ve g > e- s u s~ d~ WEIGHTED<=7 @4
// Process: ve g > e- d c d~ s~ WEIGHTED<=7 @4
// Process: vm g > mu- u c u~ s~ WEIGHTED<=7 @4
// Process: vm g > mu- c u c~ d~ WEIGHTED<=7 @4
// Process: vm g > mu- s u s~ d~ WEIGHTED<=7 @4
// Process: vm g > mu- d c d~ s~ WEIGHTED<=7 @4
// Process: e+ g > ve~ u c u~ s~ WEIGHTED<=7 @4
// Process: e+ g > ve~ c u c~ d~ WEIGHTED<=7 @4
// Process: e+ g > ve~ s u s~ d~ WEIGHTED<=7 @4
// Process: e+ g > ve~ d c d~ s~ WEIGHTED<=7 @4
// Process: mu+ g > vm~ u c u~ s~ WEIGHTED<=7 @4
// Process: mu+ g > vm~ c u c~ d~ WEIGHTED<=7 @4
// Process: mu+ g > vm~ s u s~ d~ WEIGHTED<=7 @4
// Process: mu+ g > vm~ d c d~ s~ WEIGHTED<=7 @4
// Process: ve~ g > e+ u s u~ c~ WEIGHTED<=7 @4
// Process: ve~ g > e+ c d c~ u~ WEIGHTED<=7 @4
// Process: ve~ g > e+ s d s~ u~ WEIGHTED<=7 @4
// Process: ve~ g > e+ d s d~ c~ WEIGHTED<=7 @4
// Process: vm~ g > mu+ u s u~ c~ WEIGHTED<=7 @4
// Process: vm~ g > mu+ c d c~ u~ WEIGHTED<=7 @4
// Process: vm~ g > mu+ s d s~ u~ WEIGHTED<=7 @4
// Process: vm~ g > mu+ d s d~ c~ WEIGHTED<=7 @4
//--------------------------------------------------------------------------

typedef vector<double> vec_double; 
typedef vector < vec_double > vec_vec_double; 
typedef vector<int> vec_int; 
typedef vector<bool> vec_bool; 
typedef vector < vec_int > vec_vec_int; 

class PY8MEs_R4_P10_sm_lg_lqqqq : public PY8ME
{
  public:

    // Check for the availability of the requested proces.
    // If available, this returns the corresponding permutation and Proc_ID  to
    // use.
    // If not available, this returns a negative Proc_ID.
    static pair < vector<int> , int > static_getPY8ME(vector<int> initial_pdgs,
        vector<int> final_pdgs, set<int> schannels = set<int> ());

    // Constructor.
    PY8MEs_R4_P10_sm_lg_lqqqq(Parameters_sm * model) : pars(model) {initProc();}

    // Destructor.
    ~PY8MEs_R4_P10_sm_lg_lqqqq(); 

    // Initialize process.
    virtual void initProc(); 

    // Calculate squared ME.
    virtual double sigmaKin(); 

    // Info on the subprocess.
    virtual string name() const {return "lg_lqqqq (sm)";}

    virtual int code() const {return 10410;}

    virtual string inFlux() const {return "N/A";}

    virtual vector<double> getMasses(); 

    virtual void setMasses(vec_double external_masses); 
    // Set all values of the external masses to an integer mode:
    // 0 : Mass taken from the model
    // 1 : Mass taken from p_i^2 if not massless to begin with
    // 2 : Mass always taken from p_i^2.
    virtual void setExternalMassesMode(int mode); 

    // Synchronize local variables of the process that depend on the model
    // parameters
    virtual void syncProcModelParams(); 

    // Tell Pythia that sigmaHat returns the ME^2
    virtual bool convertM2() const {return true;}

    // Access to getPY8ME with polymorphism from a non-static context
    virtual pair < vector<int> , int > getPY8ME(vector<int> initial_pdgs,
        vector<int> final_pdgs, set<int> schannels = set<int> ())
    {
      return static_getPY8ME(initial_pdgs, final_pdgs, schannels); 
    }

    // Set momenta
    virtual void setMomenta(vector < vec_double > momenta_picked); 

    // Set color configuration to use. An empty vector means sum over all.
    virtual void setColors(vector<int> colors_picked); 

    // Set the helicity configuration to use. Am empty vector means sum over
    // all.
    virtual void setHelicities(vector<int> helicities_picked); 

    // Set the permutation to use (will apply to momenta, colors and helicities)
    virtual void setPermutation(vector<int> perm_picked); 

    // Set the proc_ID to use
    virtual void setProcID(int procID_picked); 

    // Access to all the helicity and color configurations for a given process
    virtual vector < vec_int > getColorConfigs(int specify_proc_ID = -1,
        vector<int> permutation = vector<int> ());
    virtual vector < vec_int > getHelicityConfigs(vector<int> permutation =
        vector<int> ());

    // Maps of Helicity <-> hel_ID and ColorConfig <-> colorConfig_ID.
    virtual vector<int> getHelicityConfigForID(int hel_ID, vector<int>
        permutation = vector<int> ());
    virtual int getHelicityIDForConfig(vector<int> hel_config, vector<int>
        permutation = vector<int> ());
    virtual vector<int> getColorConfigForID(int color_ID, int specify_proc_ID =
        -1, vector<int> permutation = vector<int> ());
    virtual int getColorIDForConfig(vector<int> color_config, int
        specify_proc_ID = -1, vector<int> permutation = vector<int> ());
    virtual int getColorFlowRelativeNCPower(int color_flow_ID, int
        specify_proc_ID = -1);

    // Access previously computed results
    virtual vector < vec_double > getAllResults(int specify_proc_ID = -1); 
    virtual double getResult(int helicity_ID, int color_ID, int specify_proc_ID
        = -1);

    // Accessors
    Parameters_sm * getModel() {return pars;}
    void setModel(Parameters_sm * model) {pars = model;}

    // Invert the permutation mapping
    vector<int> invert_mapping(vector<int> mapping); 

    // Control whether to include the symmetry factors or not
    virtual void setIncludeSymmetryFactors(bool OnOff) 
    {
      include_symmetry_factors = OnOff; 
    }
    virtual bool getIncludeSymmetryFactors() {return include_symmetry_factors;}
    virtual int getSymmetryFactor() {return denom_iden[proc_ID];}

    // Control whether to include helicity averaging factors or not
    virtual void setIncludeHelicityAveragingFactors(bool OnOff) 
    {
      include_helicity_averaging_factors = OnOff; 
    }
    virtual bool getIncludeHelicityAveragingFactors() 
    {
      return include_helicity_averaging_factors; 
    }
    virtual int getHelicityAveragingFactor() {return denom_hels[proc_ID];}

    // Control whether to include color averaging factors or not
    virtual void setIncludeColorAveragingFactors(bool OnOff) 
    {
      include_color_averaging_factors = OnOff; 
    }
    virtual bool getIncludeColorAveragingFactors() 
    {
      return include_color_averaging_factors; 
    }
    virtual int getColorAveragingFactor() {return denom_colors[proc_ID];}

  private:

    // Private functions to calculate the matrix element for all subprocesses
    // Calculate wavefunctions
    void calculate_wavefunctions(const int hel[]); 
    static const int nwavefuncs = 157; 
    Complex<double> w[nwavefuncs][18]; 
    static const int namplitudes = 1127; 
    Complex<double> amp[namplitudes]; 
    double matrix_4_emg_emuuuxux(); 
    double matrix_4_emg_emdddxdx(); 
    double matrix_4_epg_epuuuxux(); 
    double matrix_4_epg_epdddxdx(); 
    double matrix_4_emg_emucuxcx(); 
    double matrix_4_emg_emuduxdx(); 
    double matrix_4_emg_emdsdxsx(); 
    double matrix_4_veg_veuuuxux(); 
    double matrix_4_veg_vedddxdx(); 
    double matrix_4_epg_epucuxcx(); 
    double matrix_4_epg_epuduxdx(); 
    double matrix_4_epg_epdsdxsx(); 
    double matrix_4_vexg_vexuuuxux(); 
    double matrix_4_vexg_vexdddxdx(); 
    double matrix_4_emg_veuduxux(); 
    double matrix_4_emg_vedduxdx(); 
    double matrix_4_veg_veucuxcx(); 
    double matrix_4_veg_veuduxdx(); 
    double matrix_4_veg_vedsdxsx(); 
    double matrix_4_epg_vexuuuxdx(); 
    double matrix_4_epg_vexuddxdx(); 
    double matrix_4_vexg_vexucuxcx(); 
    double matrix_4_vexg_vexuduxdx(); 
    double matrix_4_vexg_vexdsdxsx(); 
    double matrix_4_emg_veusuxcx(); 
    double matrix_4_epg_vexucuxsx(); 

    // Constants for array limits
    static const int nexternal = 7; 
    static const int ninitial = 2; 
    static const int nprocesses = 26; 
    static const int nreq_s_channels = 0; 
    static const int ncomb = 128; 

    // Helicities for the process
    static int helicities[ncomb][nexternal]; 

    // Normalization factors the various processes
    static int denom_colors[nprocesses]; 
    static int denom_hels[nprocesses]; 
    static int denom_iden[nprocesses]; 

    // Control whether to include symmetry factors or not
    bool include_symmetry_factors; 
    // Control whether to include helicity averaging factors or not
    bool include_helicity_averaging_factors; 
    // Control whether to include color averaging factors or not
    bool include_color_averaging_factors; 

    // Color flows, used when selecting color
    vector < vec_double > jamp2; 

    // Store individual results (for each color flow, helicity configurations
    // and proc_ID)
    // computed in the last call to sigmaKin().
    vector < vec_vec_double > all_results; 

    // required s-channels specified
    static std::set<int> s_channel_proc; 

    // vector with external particle masses
    vector<double> mME; 

    // vector with momenta (to be changed for each event)
    vector < double * > p; 

    // external particles permutation (to be changed for each event)
    vector<int> perm; 

    // vector with colors (to be changed for each event)
    vector<int> user_colors; 

    // vector with helicities (to be changed for each event)
    vector<int> user_helicities; 

    // Process ID (to be changed for each event)
    int proc_ID; 

    // All color configurations
    void initColorConfigs(); 
    vector < vec_vec_int > color_configs; 

    // Color flows relative N_c power (conventions are such that all elements
    // on the color matrix diagonal are identical).
    vector < vec_int > jamp_nc_relative_power; 

    // Model pointer to be used by this matrix element
    Parameters_sm * pars; 

}; 

}  // end namespace PY8MEs_namespace

#endif  // PY8MEs_R4_P10_sm_lg_lqqqq_H

