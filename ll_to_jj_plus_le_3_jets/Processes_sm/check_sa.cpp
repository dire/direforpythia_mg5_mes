#include <iostream> 
#include <sstream> 
#include <iomanip> 
#include <vector> 
#include <set> 

#include "PY8ME.h"
#include "PY8MEs.h"
#include "rambo.h"

using namespace std; 

typedef vector<double> vec_double; 

// Nice string to display a process
string proc_string(vector<int> in_pdgs, vector<int> out_pdgs, set<int> 
req_s_channels = set<int> ())
{

  std::stringstream ss; 

  for (unsigned int i = 0; i < in_pdgs.size(); i++ )
    ss << in_pdgs[i] <<  " "; 
  if (req_s_channels.size() > 0)
  {
    ss <<  "> "; 
    for (set<int> ::iterator it = req_s_channels.begin(); it != 
      req_s_channels.end(); ++ it)
    ss << * it <<  " "; 
  }
  ss <<  "> "; 
  for (unsigned int i = 0; i < out_pdgs.size(); i++ )
  {
    ss << out_pdgs[i]; 
    if (i != (out_pdgs.size() - 1))
      ss <<  " "; 
  }

  return ss.str(); 
}

// Evaluate a given process with an accessor
void run_proc(PY8MEs_namespace::PY8MEs& accessor, vector<int> in_pdgs,
    vector<int> out_pdgs, set<int> req_s_channels = set<int> ())
{

  // This is not mandatory, we run it here only because we need an instance of
  // the process
  // to obtain the external masses to generate the PS point.
  PY8MEs_namespace::PY8ME * query = accessor.getProcess(in_pdgs, out_pdgs,
      req_s_channels);

  cout <<  " -> Process '"; 
  cout << proc_string(in_pdgs, out_pdgs, req_s_channels) <<  "'"; 
  if (query)
  {
    cout <<  " is available." << endl; 
  }
  else
  {
    cout <<  " is not available." << endl; 
    return; 
  }

  double energy = 1500; 
  double weight; 

  vector < vec_double > p_vec = vector < vec_double > (in_pdgs.size() +
      out_pdgs.size(), vec_double(4, 0.));

  //----
  // The line below 'get_momenta' that uses RAMBO has memory leaks. It is fine
  // for testing/debugging.
  // But replace with an hard-coded momentum configuration if you want to
  // memory-check the ME code.
  //----
  // Get phase space point
  vector < double * > p = get_momenta(in_pdgs.size(), energy,
      query->getMasses(), weight);
  // Cast to a sane type
  for (unsigned int i = 0; i < in_pdgs.size() + out_pdgs.size(); i++ )
  {
    for (unsigned int j = 0; j < 4; j++ )
    {
      p_vec[i][j] = p[i][j]; 
      // Copy-paste the printout below in this code to use an hard-coded
      // momentum configuration
      // cout<<setiosflags(ios::scientific) << setprecision(14) <<
      // setw(22)<<"p_vec["<<i<<"]["<<j<<"]"<<"="<<p_vec[i][j]<<";"<<endl;
    }
  }
  // Release arrays
  for(unsigned int i = 0; i < (in_pdgs.size() + out_pdgs.size()); i++ )
  {
    delete[] p[i]; 
    p[i] = NULL; 
  }
  //---
  // End of the segment that can cause memory leak.
  //---

  // You could require specific helicity and quantum numbers here
  // (these arguments, like req_s_channels are optional for calculateME and
  // considerd empty by default)
  // They are left empty here, meaning that these quantum numbers will be
  // summed/averaged over.
  // This vector's size would be twice the number of external legs (for color
  // and anti-color specification)
  vector<int> colors(0); 
  // This vector's size would be the number of external legs
  vector<int> helicities(0); 
  pair < double, bool > res = accessor.calculateME(in_pdgs, out_pdgs, p_vec,
      req_s_channels, colors, helicities);

  if ( !res.second)
  {
    cout <<  " | Its evaluation failed." << endl; 
    return; 
  }
  else
  {
    cout <<  " | Momenta:" << endl; 
    for(unsigned int i = 0; i < (in_pdgs.size() + out_pdgs.size()); i++ )
      cout <<  " | " << setw(4) << i + 1
     << setiosflags(ios::scientific) << setprecision(14) << setw(22) <<
        p_vec[i][0]
     << setiosflags(ios::scientific) << setprecision(14) << setw(22) <<
        p_vec[i][1]
     << setiosflags(ios::scientific) << setprecision(14) << setw(22) <<
        p_vec[i][2]
     << setiosflags(ios::scientific) << setprecision(14) << setw(22) <<
        p_vec[i][3] << endl;
    cout <<  " | Matrix element : " << setiosflags(ios::scientific) <<
        setprecision(17) << res.first;
    cout <<  " GeV^" << - (2 * int(in_pdgs.size() + out_pdgs.size()) - 8) <<
        endl;
  }
}

int main(int argc, char * * argv)
{
  // Prevent unused variable warning
  if (false)
    cout << argc; 
  if (false)
    cout << argv; 

  // Simplest way of creating a PY8MEs accessor
  PY8MEs_namespace::PY8MEs PY8MEs_accessor("param_card_sm.dat"); 

  //----------------------------------------------------------------------------
  // Here is an alternative way of creating a PY8MEs_accessor for which we
  // handle ourselves the instantiation, release and initialization of the
  // model. Notice that we need here the name of the model class because it
  // does not have a generic base class (I can add it if really necessary)
  // 
  // Parameters_sm* model = PY8MEs_namespace::PY8MEs::instantiateModel();
  // 
  // Or even directly with
  // 
  // Parameters_sm* model = new Parameters_sm();
  // 
  // With here an example of the initialization of the model using
  // Pythia8 objects.
  // 
  // model->setIndependentParameters(particleDataPtr,couplingsPtr,slhaPtr);
  // model->setIndependentCouplings();
  // model->printIndependentParameters();
  // model->printIndependentCouplings();
  // 
  // And then finally obtain the accessor with this particular model
  // 
  // PY8MEs_namespace::PY8MEs PY8MEs_accessor(model);
  //----------------------------------------------------------------------------

  //----------------------------------------------------------------------------
  // Finally one last way of creating a PY8MEs_accessor, which does the same
  // as above but this time doesn't require to define a local pointer to the
  // model (and hence to know its class name):
  // 
  // PY8MEs_namespace::PY8MEs PY8MEs_accessor();
  // 
  // We could now initialize the model from PY8 directly using the accessor
  // without having to manipulate a local pointer of the model
  // 
  // PY8MEs_namespace.initModelWithPY8(particleDataPtr, couplingsPtr, slhaPtr);
  // 
  // If needed, one can still access an instance of the model (for example
  // to be used for instantiating another PY8MEs_accessor) as follows. Be
  // wary however that as soon as you call this accessor, the PY8MEs_accessor
  // destructor will no longer take care of releasing the model instance and
  // it will be your responsability to do so.
  // 
  // Parameters_sm* model = PY8MEs_accessor.getModel();
  //----------------------------------------------------------------------------

  vector<int> vec_in_pdgs; 
  vector<int> vec_out_pdgs; 
  set<int> set_req_s_channels; 

  // Test the existence of a non-available process
  cout << endl <<  "Testing the non-existence of a non-available process:" <<
      endl;
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (33)(43)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (33)(2)(1)(5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> (2)(5)); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 

  // Testing available processes
  cout << endl <<  "Testing the evaluation of available processes:" << endl; 

  // Process Process: ta+ ta- > g g g b b~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(21)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > g b b b~ b~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(5)(5)(-5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > g g g u u~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(21)(2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > g g g c c~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(21)(4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > g g g u u~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(21)(2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > g g g c c~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(21)(4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > g g g d d~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(21)(1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > g g g s s~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(21)(3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > g g g d d~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(21)(1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > g g g s s~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(21)(3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ ve > g g g u d~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(21)(2)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ ve > g g g c s~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(21)(4)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ vm > g g g u d~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(21)(2)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ vm > g g g c s~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(21)(4)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ e- > g g g d u~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(21)(1)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ e- > g g g s c~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(21)(3)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ mu- > g g g d u~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(21)(1)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ mu- > g g g s c~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(21)(3)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > g g g u u~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(21)(2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > g g g c c~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(21)(4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > g g g u u~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(21)(2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > g g g c c~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(21)(4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > g g g u u~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(21)(2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > g g g c c~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(21)(4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > g g g d d~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(21)(1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > g g g s s~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(21)(3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > g g g d d~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(21)(1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > g g g s s~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(21)(3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > g g g d d~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(21)(1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > g g g s s~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(21)(3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > g g g b b~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(21)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > g g g b b~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(21)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > g g g b b~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(21)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > g g g b b~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(21)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > g g g b b~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(21)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > g g g u u~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(21)(2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > g g g c c~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(21)(4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > g g g d d~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(21)(1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > g g g s s~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(21)(3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > g u u u~ u~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(2)(2)(-2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > g c c c~ c~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(4)(4)(-4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > g u u u~ u~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(2)(2)(-2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > g c c c~ c~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(4)(4)(-4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > g d d d~ d~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(1)(1)(-1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > g s s s~ s~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(3)(3)(-3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > g d d d~ d~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(1)(1)(-1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > g s s s~ s~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(3)(3)(-3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > g u c u~ c~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(2)(4)(-2)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > g u c u~ c~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(2)(4)(-2)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > g u d u~ d~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(2)(1)(-2)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > g u s u~ s~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(2)(3)(-2)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > g c d c~ d~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(4)(1)(-4)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > g c s c~ s~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(4)(3)(-4)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > g u d u~ d~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(2)(1)(-2)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > g u s u~ s~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(2)(3)(-2)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > g c d c~ d~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(4)(1)(-4)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > g c s c~ s~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(4)(3)(-4)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > g d s d~ s~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(1)(3)(-1)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > g d s d~ s~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(1)(3)(-1)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > g u u u~ u~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(2)(2)(-2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > g c c c~ c~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(4)(4)(-4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > g u u u~ u~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(2)(2)(-2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > g c c c~ c~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(4)(4)(-4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > g u u u~ u~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(2)(2)(-2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > g c c c~ c~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(4)(4)(-4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > g d d d~ d~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(1)(1)(-1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > g s s s~ s~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(3)(3)(-3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > g d d d~ d~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(1)(1)(-1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > g s s s~ s~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(3)(3)(-3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > g d d d~ d~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(1)(1)(-1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > g s s s~ s~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(3)(3)(-3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ ve > g u u u~ d~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(2)(2)(-2)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ ve > g c c c~ s~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(4)(4)(-4)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ vm > g u u u~ d~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(2)(2)(-2)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ vm > g c c c~ s~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(4)(4)(-4)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ ve > g u d d~ d~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(2)(1)(-1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ ve > g c s s~ s~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(4)(3)(-3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ vm > g u d d~ d~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(2)(1)(-1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ vm > g c s s~ s~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(4)(3)(-3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ e- > g u d u~ u~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(2)(1)(-2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ e- > g c s c~ c~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(4)(3)(-4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ mu- > g u d u~ u~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(2)(1)(-2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ mu- > g c s c~ c~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(4)(3)(-4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ e- > g d d u~ d~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(1)(1)(-2)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ e- > g s s c~ s~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(3)(3)(-4)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ mu- > g d d u~ d~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(1)(1)(-2)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ mu- > g s s c~ s~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(3)(3)(-4)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > g u c u~ c~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(2)(4)(-2)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > g u c u~ c~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(2)(4)(-2)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > g u c u~ c~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(2)(4)(-2)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > g u d u~ d~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(2)(1)(-2)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > g u s u~ s~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(2)(3)(-2)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > g c d c~ d~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(4)(1)(-4)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > g c s c~ s~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(4)(3)(-4)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > g u d u~ d~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(2)(1)(-2)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > g u s u~ s~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(2)(3)(-2)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > g c d c~ d~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(4)(1)(-4)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > g c s c~ s~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(4)(3)(-4)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > g u d u~ d~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(2)(1)(-2)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > g u s u~ s~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(2)(3)(-2)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > g c d c~ d~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(4)(1)(-4)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > g c s c~ s~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(4)(3)(-4)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > g d s d~ s~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(1)(3)(-1)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > g d s d~ s~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(1)(3)(-1)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > g d s d~ s~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(1)(3)(-1)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ ve > g u c u~ s~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(2)(4)(-2)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ ve > g c u c~ d~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(4)(2)(-4)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ ve > g s u s~ d~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(3)(2)(-3)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ ve > g d c d~ s~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(1)(4)(-1)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ vm > g u c u~ s~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(2)(4)(-2)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ vm > g c u c~ d~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(4)(2)(-4)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ vm > g s u s~ d~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(3)(2)(-3)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ vm > g d c d~ s~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(1)(4)(-1)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ e- > g u s u~ c~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(2)(3)(-2)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ e- > g c d c~ u~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(4)(1)(-4)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ e- > g s d s~ u~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(3)(1)(-3)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ e- > g d s d~ c~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(1)(3)(-1)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ mu- > g u s u~ c~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(2)(3)(-2)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ mu- > g c d c~ u~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(4)(1)(-4)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ mu- > g s d s~ u~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(3)(1)(-3)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ mu- > g d s d~ c~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(1)(3)(-1)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > g b b b~ b~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(5)(5)(-5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > g b b b~ b~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(5)(5)(-5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > g b b b~ b~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(5)(5)(-5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > g b b b~ b~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(5)(5)(-5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > g b b b~ b~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(5)(5)(-5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > g u u u~ u~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(2)(2)(-2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > g c c c~ c~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(4)(4)(-4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > g d d d~ d~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(1)(1)(-1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > g s s s~ s~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(3)(3)(-3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > g u c u~ c~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(2)(4)(-2)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > g u d u~ d~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(2)(1)(-2)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > g u s u~ s~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(2)(3)(-2)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > g c d c~ d~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(4)(1)(-4)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > g c s c~ s~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(4)(3)(-4)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > g d s d~ s~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(1)(3)(-1)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > g u u~ b b~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(2)(-2)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > g c c~ b b~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(4)(-4)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > g d d~ b b~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(1)(-1)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > g s s~ b b~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(3)(-3)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ ta- > g g g d u~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(21)(1)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ ta- > g g g s c~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(21)(3)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ vt > g g g u d~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(21)(2)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ vt > g g g c s~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(21)(4)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > g u u~ b b~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(2)(-2)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > g c c~ b b~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(4)(-4)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > g u u~ b b~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(2)(-2)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > g c c~ b b~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(4)(-4)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > g d d~ b b~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(1)(-1)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > g s s~ b b~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(3)(-3)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > g d d~ b b~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(1)(-1)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > g s s~ b b~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(3)(-3)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > g u u~ b b~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(2)(-2)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > g c c~ b b~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(4)(-4)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > g u u~ b b~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(2)(-2)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > g c c~ b b~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(4)(-4)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > g u u~ b b~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(2)(-2)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > g c c~ b b~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(4)(-4)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > g d d~ b b~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(1)(-1)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > g s s~ b b~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(3)(-3)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > g d d~ b b~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(1)(-1)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > g s s~ b b~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(3)(-3)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > g d d~ b b~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(1)(-1)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > g s s~ b b~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(3)(-3)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ ve > g u d~ b b~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(2)(-1)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ ve > g c s~ b b~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(4)(-3)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ vm > g u d~ b b~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(2)(-1)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ vm > g c s~ b b~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(4)(-3)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ e- > g d u~ b b~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(1)(-2)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ e- > g s c~ b b~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(3)(-4)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ mu- > g d u~ b b~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(1)(-2)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ mu- > g s c~ b b~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(3)(-4)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > g g b b~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > b b b~ b~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (5)(5)(-5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ ta- > g u d u~ u~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(2)(1)(-2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ ta- > g c s c~ c~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(4)(3)(-4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ ta- > g d d u~ d~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(1)(1)(-2)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ ta- > g s s c~ s~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(3)(3)(-4)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ ta- > g u s u~ c~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(2)(3)(-2)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ ta- > g c d c~ u~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(4)(1)(-4)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ ta- > g s d s~ u~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(3)(1)(-3)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ ta- > g d s d~ c~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(1)(3)(-1)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ vt > g u u u~ d~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(2)(2)(-2)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ vt > g c c c~ s~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(4)(4)(-4)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ vt > g u d d~ d~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(2)(1)(-1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ vt > g c s s~ s~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(4)(3)(-3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ vt > g u c u~ s~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(2)(4)(-2)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ vt > g c u c~ d~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(4)(2)(-4)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ vt > g s u s~ d~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(3)(2)(-3)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ vt > g d c d~ s~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(1)(4)(-1)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > g g u u~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > g g c c~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > g g u u~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > g g c c~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > g g d d~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > g g s s~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > g g d d~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > g g s s~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ ve > g g u d~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(2)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ ve > g g c s~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(4)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ vm > g g u d~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(2)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ vm > g g c s~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(4)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ e- > g g d u~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(1)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ e- > g g s c~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(3)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ mu- > g g d u~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(1)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ mu- > g g s c~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(3)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > g g u u~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > g g c c~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > g g u u~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > g g c c~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > g g u u~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > g g c c~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > g g d d~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > g g s s~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > g g d d~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > g g s s~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > g g d d~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > g g s s~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > g g b b~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > g g b b~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > g g b b~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > g g b b~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > g g b b~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > u u u~ u~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (2)(2)(-2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > c c c~ c~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (4)(4)(-4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > u u u~ u~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (2)(2)(-2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > c c c~ c~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (4)(4)(-4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > d d d~ d~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (1)(1)(-1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > s s s~ s~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (3)(3)(-3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > d d d~ d~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (1)(1)(-1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > s s s~ s~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (3)(3)(-3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > u c u~ c~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (2)(4)(-2)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > u c u~ c~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (2)(4)(-2)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > u d u~ d~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (2)(1)(-2)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > u s u~ s~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (2)(3)(-2)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > c d c~ d~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (4)(1)(-4)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > c s c~ s~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (4)(3)(-4)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > u d u~ d~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (2)(1)(-2)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > u s u~ s~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (2)(3)(-2)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > c d c~ d~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (4)(1)(-4)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > c s c~ s~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (4)(3)(-4)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > d s d~ s~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (1)(3)(-1)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > d s d~ s~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (1)(3)(-1)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > u u u~ u~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (2)(2)(-2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > c c c~ c~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (4)(4)(-4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > u u u~ u~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (2)(2)(-2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > c c c~ c~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (4)(4)(-4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > u u u~ u~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (2)(2)(-2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > c c c~ c~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (4)(4)(-4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > d d d~ d~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (1)(1)(-1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > s s s~ s~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (3)(3)(-3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > d d d~ d~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (1)(1)(-1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > s s s~ s~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (3)(3)(-3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > d d d~ d~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (1)(1)(-1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > s s s~ s~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (3)(3)(-3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ ve > u u u~ d~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (2)(2)(-2)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ ve > c c c~ s~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (4)(4)(-4)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ vm > u u u~ d~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (2)(2)(-2)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ vm > c c c~ s~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (4)(4)(-4)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ ve > u d d~ d~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (2)(1)(-1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ ve > c s s~ s~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (4)(3)(-3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ vm > u d d~ d~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (2)(1)(-1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ vm > c s s~ s~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (4)(3)(-3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ e- > u d u~ u~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (2)(1)(-2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ e- > c s c~ c~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (4)(3)(-4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ mu- > u d u~ u~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (2)(1)(-2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ mu- > c s c~ c~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (4)(3)(-4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ e- > d d u~ d~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (1)(1)(-2)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ e- > s s c~ s~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (3)(3)(-4)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ mu- > d d u~ d~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (1)(1)(-2)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ mu- > s s c~ s~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (3)(3)(-4)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > u c u~ c~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (2)(4)(-2)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > u c u~ c~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (2)(4)(-2)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > u c u~ c~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (2)(4)(-2)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > u d u~ d~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (2)(1)(-2)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > u s u~ s~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (2)(3)(-2)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > c d c~ d~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (4)(1)(-4)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > c s c~ s~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (4)(3)(-4)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > u d u~ d~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (2)(1)(-2)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > u s u~ s~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (2)(3)(-2)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > c d c~ d~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (4)(1)(-4)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > c s c~ s~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (4)(3)(-4)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > u d u~ d~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (2)(1)(-2)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > u s u~ s~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (2)(3)(-2)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > c d c~ d~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (4)(1)(-4)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > c s c~ s~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (4)(3)(-4)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > d s d~ s~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (1)(3)(-1)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > d s d~ s~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (1)(3)(-1)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > d s d~ s~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (1)(3)(-1)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ ve > u c u~ s~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (2)(4)(-2)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ ve > c u c~ d~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (4)(2)(-4)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ ve > s u s~ d~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (3)(2)(-3)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ ve > d c d~ s~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (1)(4)(-1)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ vm > u c u~ s~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (2)(4)(-2)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ vm > c u c~ d~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (4)(2)(-4)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ vm > s u s~ d~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (3)(2)(-3)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ vm > d c d~ s~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (1)(4)(-1)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ e- > u s u~ c~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (2)(3)(-2)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ e- > c d c~ u~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (4)(1)(-4)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ e- > s d s~ u~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (3)(1)(-3)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ e- > d s d~ c~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (1)(3)(-1)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ mu- > u s u~ c~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (2)(3)(-2)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ mu- > c d c~ u~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (4)(1)(-4)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ mu- > s d s~ u~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (3)(1)(-3)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ mu- > d s d~ c~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (1)(3)(-1)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > b b b~ b~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (5)(5)(-5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > b b b~ b~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (5)(5)(-5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > b b b~ b~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (5)(5)(-5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > b b b~ b~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (5)(5)(-5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > b b b~ b~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (5)(5)(-5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > g g u u~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > g g c c~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > g g d d~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > g g s s~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > u u u~ u~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (2)(2)(-2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > c c c~ c~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (4)(4)(-4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > d d d~ d~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (1)(1)(-1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > s s s~ s~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (3)(3)(-3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > u c u~ c~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (2)(4)(-2)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > u d u~ d~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (2)(1)(-2)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > u s u~ s~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (2)(3)(-2)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > c d c~ d~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (4)(1)(-4)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > c s c~ s~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (4)(3)(-4)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > d s d~ s~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (1)(3)(-1)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ ta- > g d u~ b b~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(1)(-2)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ ta- > g s c~ b b~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(3)(-4)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ vt > g u d~ b b~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(2)(-1)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ vt > g c s~ b b~ WEIGHTED<=7 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(4)(-3)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > u u~ b b~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (2)(-2)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > c c~ b b~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (4)(-4)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > d d~ b b~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (1)(-1)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > s s~ b b~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (3)(-3)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > u u~ b b~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (2)(-2)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > c c~ b b~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (4)(-4)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > u u~ b b~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (2)(-2)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > c c~ b b~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (4)(-4)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > d d~ b b~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (1)(-1)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > s s~ b b~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (3)(-3)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > d d~ b b~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (1)(-1)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > s s~ b b~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (3)(-3)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > u u~ b b~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (2)(-2)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > c c~ b b~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (4)(-4)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > u u~ b b~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (2)(-2)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > c c~ b b~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (4)(-4)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > u u~ b b~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (2)(-2)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > c c~ b b~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (4)(-4)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > d d~ b b~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (1)(-1)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > s s~ b b~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (3)(-3)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > d d~ b b~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (1)(-1)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > s s~ b b~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (3)(-3)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > d d~ b b~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (1)(-1)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > s s~ b b~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (3)(-3)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ ve > u d~ b b~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (2)(-1)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ ve > c s~ b b~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (4)(-3)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ vm > u d~ b b~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (2)(-1)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ vm > c s~ b b~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (4)(-3)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ e- > d u~ b b~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (1)(-2)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ e- > s c~ b b~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (3)(-4)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ mu- > d u~ b b~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (1)(-2)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ mu- > s c~ b b~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (3)(-4)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ ta- > g g d u~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(1)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ ta- > g g s c~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(3)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ vt > g g u d~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(2)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ vt > g g c s~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (21)(21)(4)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > g b b~ WEIGHTED<=5 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(5)(-5)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > g u u~ WEIGHTED<=5 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(2)(-2)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > g c c~ WEIGHTED<=5 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(4)(-4)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > g u u~ WEIGHTED<=5 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(2)(-2)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > g c c~ WEIGHTED<=5 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(4)(-4)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > g d d~ WEIGHTED<=5 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(1)(-1)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > g s s~ WEIGHTED<=5 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(3)(-3)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > g d d~ WEIGHTED<=5 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(1)(-1)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > g s s~ WEIGHTED<=5 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(3)(-3)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ ve > g u d~ WEIGHTED<=5 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(2)(-1)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ ve > g c s~ WEIGHTED<=5 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(4)(-3)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ vm > g u d~ WEIGHTED<=5 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(2)(-1)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ vm > g c s~ WEIGHTED<=5 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(4)(-3)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ e- > g d u~ WEIGHTED<=5 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(1)(-2)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ e- > g s c~ WEIGHTED<=5 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(3)(-4)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ mu- > g d u~ WEIGHTED<=5 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(1)(-2)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ mu- > g s c~ WEIGHTED<=5 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(3)(-4)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > g u u~ WEIGHTED<=5 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(2)(-2)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > g c c~ WEIGHTED<=5 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(4)(-4)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > g u u~ WEIGHTED<=5 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(2)(-2)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > g c c~ WEIGHTED<=5 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(4)(-4)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > g u u~ WEIGHTED<=5 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(2)(-2)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > g c c~ WEIGHTED<=5 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(4)(-4)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > g d d~ WEIGHTED<=5 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(1)(-1)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > g s s~ WEIGHTED<=5 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(3)(-3)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > g d d~ WEIGHTED<=5 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(1)(-1)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > g s s~ WEIGHTED<=5 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(3)(-3)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > g d d~ WEIGHTED<=5 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(1)(-1)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > g s s~ WEIGHTED<=5 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(3)(-3)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > g b b~ WEIGHTED<=5 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(5)(-5)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > g b b~ WEIGHTED<=5 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(5)(-5)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > g b b~ WEIGHTED<=5 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(5)(-5)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > g b b~ WEIGHTED<=5 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(5)(-5)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > g b b~ WEIGHTED<=5 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(5)(-5)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > g u u~ WEIGHTED<=5 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(2)(-2)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > g c c~ WEIGHTED<=5 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(4)(-4)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > g d d~ WEIGHTED<=5 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(1)(-1)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > g s s~ WEIGHTED<=5 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(3)(-3)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ ta- > u d u~ u~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (2)(1)(-2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ ta- > c s c~ c~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (4)(3)(-4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ ta- > d d u~ d~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (1)(1)(-2)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ ta- > s s c~ s~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (3)(3)(-4)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ ta- > u s u~ c~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (2)(3)(-2)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ ta- > c d c~ u~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (4)(1)(-4)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ ta- > s d s~ u~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (3)(1)(-3)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ ta- > d s d~ c~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (1)(3)(-1)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ vt > u u u~ d~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (2)(2)(-2)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ vt > c c c~ s~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (4)(4)(-4)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ vt > u d d~ d~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (2)(1)(-1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ vt > c s s~ s~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (4)(3)(-3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ vt > u c u~ s~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (2)(4)(-2)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ vt > c u c~ d~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (4)(2)(-4)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ vt > s u s~ d~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (3)(2)(-3)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ vt > d c d~ s~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (1)(4)(-1)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > b b~ WEIGHTED<=4 @1
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (5)(-5)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > u u~ WEIGHTED<=4 @1
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-2)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > c c~ WEIGHTED<=4 @1
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-4)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > u u~ WEIGHTED<=4 @1
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-2)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > c c~ WEIGHTED<=4 @1
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-4)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > d d~ WEIGHTED<=4 @1
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-1)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > s s~ WEIGHTED<=4 @1
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-3)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > d d~ WEIGHTED<=4 @1
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-1)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > s s~ WEIGHTED<=4 @1
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-3)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ ve > u d~ WEIGHTED<=4 @1
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-1)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ ve > c s~ WEIGHTED<=4 @1
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-3)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ vm > u d~ WEIGHTED<=4 @1
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-1)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ vm > c s~ WEIGHTED<=4 @1
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-3)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ e- > d u~ WEIGHTED<=4 @1
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-2)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ e- > s c~ WEIGHTED<=4 @1
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-4)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ mu- > d u~ WEIGHTED<=4 @1
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-2)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ mu- > s c~ WEIGHTED<=4 @1
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-4)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > u u~ WEIGHTED<=4 @1
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-2)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > c c~ WEIGHTED<=4 @1
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-4)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > u u~ WEIGHTED<=4 @1
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-2)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > c c~ WEIGHTED<=4 @1
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-4)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > u u~ WEIGHTED<=4 @1
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-2)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > c c~ WEIGHTED<=4 @1
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-4)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > d d~ WEIGHTED<=4 @1
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-1)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > s s~ WEIGHTED<=4 @1
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-3)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > d d~ WEIGHTED<=4 @1
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-1)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > s s~ WEIGHTED<=4 @1
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-3)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > d d~ WEIGHTED<=4 @1
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-1)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > s s~ WEIGHTED<=4 @1
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-3)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > a a WEIGHTED<=4 @1
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (22)(22)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > a a WEIGHTED<=4 @1
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (22)(22)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: e+ e- > b b~ WEIGHTED<=4 @1
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-11)(11)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (5)(-5)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: mu+ mu- > b b~ WEIGHTED<=4 @1
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-13)(13)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (5)(-5)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ve~ ve > b b~ WEIGHTED<=4 @1
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-12)(12)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (5)(-5)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vm~ vm > b b~ WEIGHTED<=4 @1
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-14)(14)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (5)(-5)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ vt > b b~ WEIGHTED<=4 @1
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (5)(-5)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > u u~ WEIGHTED<=4 @1
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-2)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > c c~ WEIGHTED<=4 @1
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-4)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > d d~ WEIGHTED<=4 @1
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-1)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > s s~ WEIGHTED<=4 @1
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-3)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ ta- > a a WEIGHTED<=4 @1
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (22)(22)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ ta- > g d u~ WEIGHTED<=5 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(1)(-2)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ ta- > g s c~ WEIGHTED<=5 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(3)(-4)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ vt > g u d~ WEIGHTED<=5 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(2)(-1)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ vt > g c s~ WEIGHTED<=5 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(4)(-3)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ ta- > d u~ b b~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (1)(-2)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ ta- > s c~ b b~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (3)(-4)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ vt > u d~ b b~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (2)(-1)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ vt > c s~ b b~ WEIGHTED<=6 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (4)(-3)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ ta- > d u~ WEIGHTED<=4 @1
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-2)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: vt~ ta- > s c~ WEIGHTED<=4 @1
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-16)(15)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-4)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ vt > u d~ WEIGHTED<=4 @1
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-1)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: ta+ vt > c s~ WEIGHTED<=4 @1
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-15)(16)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-3)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 


  #include "check_sa_additional_runs.inc"

}
