//==========================================================================
// This file has been automatically generated for Pythia 8 by
// MadGraph5_aMC@NLO v. 2.6.0, 2017-08-16
// By the MadGraph5_aMC@NLO Development Team
// Visit launchpad.net/madgraph5 and amcatnlo.web.cern.ch
//==========================================================================

#include "PY8MEs_R12_P30_sm_gq_wpwmggq.h"
#include "HelAmps_sm.h"

using namespace Pythia8_sm; 

namespace PY8MEs_namespace 
{
//==========================================================================
// Class member functions for calculating the matrix elements for
// Process: g u > w+ w- g g u WEIGHTED<=7 @12
// Process: g c > w+ w- g g c WEIGHTED<=7 @12
// Process: g d > w+ w- g g d WEIGHTED<=7 @12
// Process: g s > w+ w- g g s WEIGHTED<=7 @12
// Process: g u~ > w+ w- g g u~ WEIGHTED<=7 @12
// Process: g c~ > w+ w- g g c~ WEIGHTED<=7 @12
// Process: g d~ > w+ w- g g d~ WEIGHTED<=7 @12
// Process: g s~ > w+ w- g g s~ WEIGHTED<=7 @12

// Exception class
class PY8MEs_R12_P30_sm_gq_wpwmggqException : public exception
{
  virtual const char * what() const throw()
  {
    return "Exception in class 'PY8MEs_R12_P30_sm_gq_wpwmggq'."; 
  }
}
PY8MEs_R12_P30_sm_gq_wpwmggq_exception; 

std::set<int> PY8MEs_R12_P30_sm_gq_wpwmggq::s_channel_proc = std::set<int>
    (createset<int> ());

int PY8MEs_R12_P30_sm_gq_wpwmggq::helicities[ncomb][nexternal] = {{-1, -1, -1,
    -1, -1, -1, -1}, {-1, -1, -1, -1, -1, -1, 1}, {-1, -1, -1, -1, -1, 1, -1},
    {-1, -1, -1, -1, -1, 1, 1}, {-1, -1, -1, -1, 1, -1, -1}, {-1, -1, -1, -1,
    1, -1, 1}, {-1, -1, -1, -1, 1, 1, -1}, {-1, -1, -1, -1, 1, 1, 1}, {-1, -1,
    -1, 0, -1, -1, -1}, {-1, -1, -1, 0, -1, -1, 1}, {-1, -1, -1, 0, -1, 1, -1},
    {-1, -1, -1, 0, -1, 1, 1}, {-1, -1, -1, 0, 1, -1, -1}, {-1, -1, -1, 0, 1,
    -1, 1}, {-1, -1, -1, 0, 1, 1, -1}, {-1, -1, -1, 0, 1, 1, 1}, {-1, -1, -1,
    1, -1, -1, -1}, {-1, -1, -1, 1, -1, -1, 1}, {-1, -1, -1, 1, -1, 1, -1},
    {-1, -1, -1, 1, -1, 1, 1}, {-1, -1, -1, 1, 1, -1, -1}, {-1, -1, -1, 1, 1,
    -1, 1}, {-1, -1, -1, 1, 1, 1, -1}, {-1, -1, -1, 1, 1, 1, 1}, {-1, -1, 0,
    -1, -1, -1, -1}, {-1, -1, 0, -1, -1, -1, 1}, {-1, -1, 0, -1, -1, 1, -1},
    {-1, -1, 0, -1, -1, 1, 1}, {-1, -1, 0, -1, 1, -1, -1}, {-1, -1, 0, -1, 1,
    -1, 1}, {-1, -1, 0, -1, 1, 1, -1}, {-1, -1, 0, -1, 1, 1, 1}, {-1, -1, 0, 0,
    -1, -1, -1}, {-1, -1, 0, 0, -1, -1, 1}, {-1, -1, 0, 0, -1, 1, -1}, {-1, -1,
    0, 0, -1, 1, 1}, {-1, -1, 0, 0, 1, -1, -1}, {-1, -1, 0, 0, 1, -1, 1}, {-1,
    -1, 0, 0, 1, 1, -1}, {-1, -1, 0, 0, 1, 1, 1}, {-1, -1, 0, 1, -1, -1, -1},
    {-1, -1, 0, 1, -1, -1, 1}, {-1, -1, 0, 1, -1, 1, -1}, {-1, -1, 0, 1, -1, 1,
    1}, {-1, -1, 0, 1, 1, -1, -1}, {-1, -1, 0, 1, 1, -1, 1}, {-1, -1, 0, 1, 1,
    1, -1}, {-1, -1, 0, 1, 1, 1, 1}, {-1, -1, 1, -1, -1, -1, -1}, {-1, -1, 1,
    -1, -1, -1, 1}, {-1, -1, 1, -1, -1, 1, -1}, {-1, -1, 1, -1, -1, 1, 1}, {-1,
    -1, 1, -1, 1, -1, -1}, {-1, -1, 1, -1, 1, -1, 1}, {-1, -1, 1, -1, 1, 1,
    -1}, {-1, -1, 1, -1, 1, 1, 1}, {-1, -1, 1, 0, -1, -1, -1}, {-1, -1, 1, 0,
    -1, -1, 1}, {-1, -1, 1, 0, -1, 1, -1}, {-1, -1, 1, 0, -1, 1, 1}, {-1, -1,
    1, 0, 1, -1, -1}, {-1, -1, 1, 0, 1, -1, 1}, {-1, -1, 1, 0, 1, 1, -1}, {-1,
    -1, 1, 0, 1, 1, 1}, {-1, -1, 1, 1, -1, -1, -1}, {-1, -1, 1, 1, -1, -1, 1},
    {-1, -1, 1, 1, -1, 1, -1}, {-1, -1, 1, 1, -1, 1, 1}, {-1, -1, 1, 1, 1, -1,
    -1}, {-1, -1, 1, 1, 1, -1, 1}, {-1, -1, 1, 1, 1, 1, -1}, {-1, -1, 1, 1, 1,
    1, 1}, {-1, 1, -1, -1, -1, -1, -1}, {-1, 1, -1, -1, -1, -1, 1}, {-1, 1, -1,
    -1, -1, 1, -1}, {-1, 1, -1, -1, -1, 1, 1}, {-1, 1, -1, -1, 1, -1, -1}, {-1,
    1, -1, -1, 1, -1, 1}, {-1, 1, -1, -1, 1, 1, -1}, {-1, 1, -1, -1, 1, 1, 1},
    {-1, 1, -1, 0, -1, -1, -1}, {-1, 1, -1, 0, -1, -1, 1}, {-1, 1, -1, 0, -1,
    1, -1}, {-1, 1, -1, 0, -1, 1, 1}, {-1, 1, -1, 0, 1, -1, -1}, {-1, 1, -1, 0,
    1, -1, 1}, {-1, 1, -1, 0, 1, 1, -1}, {-1, 1, -1, 0, 1, 1, 1}, {-1, 1, -1,
    1, -1, -1, -1}, {-1, 1, -1, 1, -1, -1, 1}, {-1, 1, -1, 1, -1, 1, -1}, {-1,
    1, -1, 1, -1, 1, 1}, {-1, 1, -1, 1, 1, -1, -1}, {-1, 1, -1, 1, 1, -1, 1},
    {-1, 1, -1, 1, 1, 1, -1}, {-1, 1, -1, 1, 1, 1, 1}, {-1, 1, 0, -1, -1, -1,
    -1}, {-1, 1, 0, -1, -1, -1, 1}, {-1, 1, 0, -1, -1, 1, -1}, {-1, 1, 0, -1,
    -1, 1, 1}, {-1, 1, 0, -1, 1, -1, -1}, {-1, 1, 0, -1, 1, -1, 1}, {-1, 1, 0,
    -1, 1, 1, -1}, {-1, 1, 0, -1, 1, 1, 1}, {-1, 1, 0, 0, -1, -1, -1}, {-1, 1,
    0, 0, -1, -1, 1}, {-1, 1, 0, 0, -1, 1, -1}, {-1, 1, 0, 0, -1, 1, 1}, {-1,
    1, 0, 0, 1, -1, -1}, {-1, 1, 0, 0, 1, -1, 1}, {-1, 1, 0, 0, 1, 1, -1}, {-1,
    1, 0, 0, 1, 1, 1}, {-1, 1, 0, 1, -1, -1, -1}, {-1, 1, 0, 1, -1, -1, 1},
    {-1, 1, 0, 1, -1, 1, -1}, {-1, 1, 0, 1, -1, 1, 1}, {-1, 1, 0, 1, 1, -1,
    -1}, {-1, 1, 0, 1, 1, -1, 1}, {-1, 1, 0, 1, 1, 1, -1}, {-1, 1, 0, 1, 1, 1,
    1}, {-1, 1, 1, -1, -1, -1, -1}, {-1, 1, 1, -1, -1, -1, 1}, {-1, 1, 1, -1,
    -1, 1, -1}, {-1, 1, 1, -1, -1, 1, 1}, {-1, 1, 1, -1, 1, -1, -1}, {-1, 1, 1,
    -1, 1, -1, 1}, {-1, 1, 1, -1, 1, 1, -1}, {-1, 1, 1, -1, 1, 1, 1}, {-1, 1,
    1, 0, -1, -1, -1}, {-1, 1, 1, 0, -1, -1, 1}, {-1, 1, 1, 0, -1, 1, -1}, {-1,
    1, 1, 0, -1, 1, 1}, {-1, 1, 1, 0, 1, -1, -1}, {-1, 1, 1, 0, 1, -1, 1}, {-1,
    1, 1, 0, 1, 1, -1}, {-1, 1, 1, 0, 1, 1, 1}, {-1, 1, 1, 1, -1, -1, -1}, {-1,
    1, 1, 1, -1, -1, 1}, {-1, 1, 1, 1, -1, 1, -1}, {-1, 1, 1, 1, -1, 1, 1},
    {-1, 1, 1, 1, 1, -1, -1}, {-1, 1, 1, 1, 1, -1, 1}, {-1, 1, 1, 1, 1, 1, -1},
    {-1, 1, 1, 1, 1, 1, 1}, {1, -1, -1, -1, -1, -1, -1}, {1, -1, -1, -1, -1,
    -1, 1}, {1, -1, -1, -1, -1, 1, -1}, {1, -1, -1, -1, -1, 1, 1}, {1, -1, -1,
    -1, 1, -1, -1}, {1, -1, -1, -1, 1, -1, 1}, {1, -1, -1, -1, 1, 1, -1}, {1,
    -1, -1, -1, 1, 1, 1}, {1, -1, -1, 0, -1, -1, -1}, {1, -1, -1, 0, -1, -1,
    1}, {1, -1, -1, 0, -1, 1, -1}, {1, -1, -1, 0, -1, 1, 1}, {1, -1, -1, 0, 1,
    -1, -1}, {1, -1, -1, 0, 1, -1, 1}, {1, -1, -1, 0, 1, 1, -1}, {1, -1, -1, 0,
    1, 1, 1}, {1, -1, -1, 1, -1, -1, -1}, {1, -1, -1, 1, -1, -1, 1}, {1, -1,
    -1, 1, -1, 1, -1}, {1, -1, -1, 1, -1, 1, 1}, {1, -1, -1, 1, 1, -1, -1}, {1,
    -1, -1, 1, 1, -1, 1}, {1, -1, -1, 1, 1, 1, -1}, {1, -1, -1, 1, 1, 1, 1},
    {1, -1, 0, -1, -1, -1, -1}, {1, -1, 0, -1, -1, -1, 1}, {1, -1, 0, -1, -1,
    1, -1}, {1, -1, 0, -1, -1, 1, 1}, {1, -1, 0, -1, 1, -1, -1}, {1, -1, 0, -1,
    1, -1, 1}, {1, -1, 0, -1, 1, 1, -1}, {1, -1, 0, -1, 1, 1, 1}, {1, -1, 0, 0,
    -1, -1, -1}, {1, -1, 0, 0, -1, -1, 1}, {1, -1, 0, 0, -1, 1, -1}, {1, -1, 0,
    0, -1, 1, 1}, {1, -1, 0, 0, 1, -1, -1}, {1, -1, 0, 0, 1, -1, 1}, {1, -1, 0,
    0, 1, 1, -1}, {1, -1, 0, 0, 1, 1, 1}, {1, -1, 0, 1, -1, -1, -1}, {1, -1, 0,
    1, -1, -1, 1}, {1, -1, 0, 1, -1, 1, -1}, {1, -1, 0, 1, -1, 1, 1}, {1, -1,
    0, 1, 1, -1, -1}, {1, -1, 0, 1, 1, -1, 1}, {1, -1, 0, 1, 1, 1, -1}, {1, -1,
    0, 1, 1, 1, 1}, {1, -1, 1, -1, -1, -1, -1}, {1, -1, 1, -1, -1, -1, 1}, {1,
    -1, 1, -1, -1, 1, -1}, {1, -1, 1, -1, -1, 1, 1}, {1, -1, 1, -1, 1, -1, -1},
    {1, -1, 1, -1, 1, -1, 1}, {1, -1, 1, -1, 1, 1, -1}, {1, -1, 1, -1, 1, 1,
    1}, {1, -1, 1, 0, -1, -1, -1}, {1, -1, 1, 0, -1, -1, 1}, {1, -1, 1, 0, -1,
    1, -1}, {1, -1, 1, 0, -1, 1, 1}, {1, -1, 1, 0, 1, -1, -1}, {1, -1, 1, 0, 1,
    -1, 1}, {1, -1, 1, 0, 1, 1, -1}, {1, -1, 1, 0, 1, 1, 1}, {1, -1, 1, 1, -1,
    -1, -1}, {1, -1, 1, 1, -1, -1, 1}, {1, -1, 1, 1, -1, 1, -1}, {1, -1, 1, 1,
    -1, 1, 1}, {1, -1, 1, 1, 1, -1, -1}, {1, -1, 1, 1, 1, -1, 1}, {1, -1, 1, 1,
    1, 1, -1}, {1, -1, 1, 1, 1, 1, 1}, {1, 1, -1, -1, -1, -1, -1}, {1, 1, -1,
    -1, -1, -1, 1}, {1, 1, -1, -1, -1, 1, -1}, {1, 1, -1, -1, -1, 1, 1}, {1, 1,
    -1, -1, 1, -1, -1}, {1, 1, -1, -1, 1, -1, 1}, {1, 1, -1, -1, 1, 1, -1}, {1,
    1, -1, -1, 1, 1, 1}, {1, 1, -1, 0, -1, -1, -1}, {1, 1, -1, 0, -1, -1, 1},
    {1, 1, -1, 0, -1, 1, -1}, {1, 1, -1, 0, -1, 1, 1}, {1, 1, -1, 0, 1, -1,
    -1}, {1, 1, -1, 0, 1, -1, 1}, {1, 1, -1, 0, 1, 1, -1}, {1, 1, -1, 0, 1, 1,
    1}, {1, 1, -1, 1, -1, -1, -1}, {1, 1, -1, 1, -1, -1, 1}, {1, 1, -1, 1, -1,
    1, -1}, {1, 1, -1, 1, -1, 1, 1}, {1, 1, -1, 1, 1, -1, -1}, {1, 1, -1, 1, 1,
    -1, 1}, {1, 1, -1, 1, 1, 1, -1}, {1, 1, -1, 1, 1, 1, 1}, {1, 1, 0, -1, -1,
    -1, -1}, {1, 1, 0, -1, -1, -1, 1}, {1, 1, 0, -1, -1, 1, -1}, {1, 1, 0, -1,
    -1, 1, 1}, {1, 1, 0, -1, 1, -1, -1}, {1, 1, 0, -1, 1, -1, 1}, {1, 1, 0, -1,
    1, 1, -1}, {1, 1, 0, -1, 1, 1, 1}, {1, 1, 0, 0, -1, -1, -1}, {1, 1, 0, 0,
    -1, -1, 1}, {1, 1, 0, 0, -1, 1, -1}, {1, 1, 0, 0, -1, 1, 1}, {1, 1, 0, 0,
    1, -1, -1}, {1, 1, 0, 0, 1, -1, 1}, {1, 1, 0, 0, 1, 1, -1}, {1, 1, 0, 0, 1,
    1, 1}, {1, 1, 0, 1, -1, -1, -1}, {1, 1, 0, 1, -1, -1, 1}, {1, 1, 0, 1, -1,
    1, -1}, {1, 1, 0, 1, -1, 1, 1}, {1, 1, 0, 1, 1, -1, -1}, {1, 1, 0, 1, 1,
    -1, 1}, {1, 1, 0, 1, 1, 1, -1}, {1, 1, 0, 1, 1, 1, 1}, {1, 1, 1, -1, -1,
    -1, -1}, {1, 1, 1, -1, -1, -1, 1}, {1, 1, 1, -1, -1, 1, -1}, {1, 1, 1, -1,
    -1, 1, 1}, {1, 1, 1, -1, 1, -1, -1}, {1, 1, 1, -1, 1, -1, 1}, {1, 1, 1, -1,
    1, 1, -1}, {1, 1, 1, -1, 1, 1, 1}, {1, 1, 1, 0, -1, -1, -1}, {1, 1, 1, 0,
    -1, -1, 1}, {1, 1, 1, 0, -1, 1, -1}, {1, 1, 1, 0, -1, 1, 1}, {1, 1, 1, 0,
    1, -1, -1}, {1, 1, 1, 0, 1, -1, 1}, {1, 1, 1, 0, 1, 1, -1}, {1, 1, 1, 0, 1,
    1, 1}, {1, 1, 1, 1, -1, -1, -1}, {1, 1, 1, 1, -1, -1, 1}, {1, 1, 1, 1, -1,
    1, -1}, {1, 1, 1, 1, -1, 1, 1}, {1, 1, 1, 1, 1, -1, -1}, {1, 1, 1, 1, 1,
    -1, 1}, {1, 1, 1, 1, 1, 1, -1}, {1, 1, 1, 1, 1, 1, 1}};

// Normalization factors the various processes
// Denominators: spins, colors and identical particles
int PY8MEs_R12_P30_sm_gq_wpwmggq::denom_colors[nprocesses] = {24, 24, 24, 24}; 
int PY8MEs_R12_P30_sm_gq_wpwmggq::denom_hels[nprocesses] = {4, 4, 4, 4}; 
int PY8MEs_R12_P30_sm_gq_wpwmggq::denom_iden[nprocesses] = {2, 2, 2, 2}; 

//--------------------------------------------------------------------------
// Color config initialization
void PY8MEs_R12_P30_sm_gq_wpwmggq::initColorConfigs() 
{
  color_configs = vector < vec_vec_int > (); 
  jamp_nc_relative_power = vector < vec_int > (); 

  // Color flows of process Process: g u > w+ w- g g u WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(2)(4)(0)(0)(0)(0)(0)(3)(2)(4)(3)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #1
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(2)(3)(0)(0)(0)(0)(0)(3)(4)(4)(2)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #2
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(2)(4)(0)(0)(0)(0)(0)(3)(1)(4)(2)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #3
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(2)(2)(0)(0)(0)(0)(0)(3)(1)(4)(3)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #4
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(2)(3)(0)(0)(0)(0)(0)(3)(2)(4)(1)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #5
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(2)(2)(0)(0)(0)(0)(0)(3)(4)(4)(1)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 

  // Color flows of process Process: g d > w+ w- g g d WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(2)(4)(0)(0)(0)(0)(0)(3)(2)(4)(3)(1)(0)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #1
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(2)(3)(0)(0)(0)(0)(0)(3)(4)(4)(2)(1)(0)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #2
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(2)(4)(0)(0)(0)(0)(0)(3)(1)(4)(2)(1)(0)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #3
  color_configs[1].push_back(vec_int(createvector<int>
      (4)(2)(2)(0)(0)(0)(0)(0)(3)(1)(4)(3)(1)(0)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #4
  color_configs[1].push_back(vec_int(createvector<int>
      (4)(2)(3)(0)(0)(0)(0)(0)(3)(2)(4)(1)(1)(0)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #5
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(2)(2)(0)(0)(0)(0)(0)(3)(4)(4)(1)(1)(0)));
  jamp_nc_relative_power[1].push_back(0); 

  // Color flows of process Process: g u~ > w+ w- g g u~ WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[2].push_back(vec_int(createvector<int>
      (1)(2)(0)(1)(0)(0)(0)(0)(3)(2)(4)(3)(0)(4)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #1
  color_configs[2].push_back(vec_int(createvector<int>
      (1)(2)(0)(1)(0)(0)(0)(0)(3)(4)(4)(2)(0)(3)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #2
  color_configs[2].push_back(vec_int(createvector<int>
      (3)(2)(0)(1)(0)(0)(0)(0)(3)(1)(4)(2)(0)(4)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #3
  color_configs[2].push_back(vec_int(createvector<int>
      (4)(2)(0)(1)(0)(0)(0)(0)(3)(1)(4)(3)(0)(2)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #4
  color_configs[2].push_back(vec_int(createvector<int>
      (4)(2)(0)(1)(0)(0)(0)(0)(3)(2)(4)(1)(0)(3)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #5
  color_configs[2].push_back(vec_int(createvector<int>
      (3)(2)(0)(1)(0)(0)(0)(0)(3)(4)(4)(1)(0)(2)));
  jamp_nc_relative_power[2].push_back(0); 

  // Color flows of process Process: g d~ > w+ w- g g d~ WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[3].push_back(vec_int(createvector<int>
      (1)(2)(0)(1)(0)(0)(0)(0)(3)(2)(4)(3)(0)(4)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #1
  color_configs[3].push_back(vec_int(createvector<int>
      (1)(2)(0)(1)(0)(0)(0)(0)(3)(4)(4)(2)(0)(3)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #2
  color_configs[3].push_back(vec_int(createvector<int>
      (3)(2)(0)(1)(0)(0)(0)(0)(3)(1)(4)(2)(0)(4)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #3
  color_configs[3].push_back(vec_int(createvector<int>
      (4)(2)(0)(1)(0)(0)(0)(0)(3)(1)(4)(3)(0)(2)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #4
  color_configs[3].push_back(vec_int(createvector<int>
      (4)(2)(0)(1)(0)(0)(0)(0)(3)(2)(4)(1)(0)(3)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #5
  color_configs[3].push_back(vec_int(createvector<int>
      (3)(2)(0)(1)(0)(0)(0)(0)(3)(4)(4)(1)(0)(2)));
  jamp_nc_relative_power[3].push_back(0); 
}

//--------------------------------------------------------------------------
// Destructor.
PY8MEs_R12_P30_sm_gq_wpwmggq::~PY8MEs_R12_P30_sm_gq_wpwmggq() 
{
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    delete[] p[i]; 
    p[i] = NULL; 
  }
}

//--------------------------------------------------------------------------
// Invert the permutation mapping
vector<int> PY8MEs_R12_P30_sm_gq_wpwmggq::invert_mapping(vector<int> mapping) 
{
  vector<int> inverted_mapping; 
  for (unsigned int i = 0; i < mapping.size(); i++ )
  {
    for (unsigned int j = 0; j < mapping.size(); j++ )
    {
      if (mapping[j] == ((int)i))
      {
        inverted_mapping.push_back(j); 
        break; 
      }
    }
  }
  return inverted_mapping; 
}

//--------------------------------------------------------------------------
// Return the list of possible helicity configurations
vector < vec_int > PY8MEs_R12_P30_sm_gq_wpwmggq::getHelicityConfigs(vector<int>
    permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(ncomb, vector<int> (nexternal, 0)); 
  for (unsigned int ihel = 0; ihel < ncomb; ihel++ )
  {
    for(unsigned int j = 0; j < nexternal; j++ )
    {
      res[ihel][chosenPerm[j]] = helicities[ihel][j]; 
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Return the list of possible color configurations
vector < vec_int > PY8MEs_R12_P30_sm_gq_wpwmggq::getColorConfigs(int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(color_configs[chosenProcID].size(), vector<int>
      (nexternal * 2, 0));
  for (unsigned int icol = 0; icol < color_configs[chosenProcID].size(); icol++
      )
  {
    for(unsigned int j = 0; j < (2 * nexternal); j++ )
    {
      res[icol][chosenPerm[j/2] * 2 + j%2] =
          color_configs[chosenProcID][icol][j];
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Get JAMP relative N_c power
int PY8MEs_R12_P30_sm_gq_wpwmggq::getColorFlowRelativeNCPower(int
    color_flow_ID, int specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return jamp_nc_relative_power[chosenProcID][color_flow_ID]; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity ID -> Helicity Config
vector<int> PY8MEs_R12_P30_sm_gq_wpwmggq::getHelicityConfigForID(int hel_ID,
    vector<int> permutation)
{
  if (hel_ID < 0 || hel_ID >= ncomb)
  {
    cerr <<  "Error in function 'getHelicityConfigForID' of class" << 
    " 'PY8MEs_R12_P30_sm_gq_wpwmggq': Specified helicity ID '" << 
    hel_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R12_P30_sm_gq_wpwmggq_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(nexternal, 0); 
  for (unsigned int j = 0; j < nexternal; j++ )
  {
    res[chosenPerm[j]] = helicities[hel_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity Config -> Helicity ID
int PY8MEs_R12_P30_sm_gq_wpwmggq::getHelicityIDForConfig(vector<int>
    hel_config, vector<int> permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  int user_ihel = -1; 
  if (hel_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < ncomb; i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < nexternal; j++ )
      {
        if (helicities[i][chosenPerm[j]] != hel_config[j])
        {
          found = false; 
          break; 
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_ihel = i; 
        break; 
      }
    }
    if (user_ihel == -1)
    {
      cerr <<  "Error in function 'getHelicityIDForConfig' of class" << 
      " 'PY8MEs_R12_P30_sm_gq_wpwmggq': Specified helicity" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R12_P30_sm_gq_wpwmggq_exception; 
    }
  }
  return user_ihel; 
}


//--------------------------------------------------------------------------
// Implements the map Color ID -> Color Config
vector<int> PY8MEs_R12_P30_sm_gq_wpwmggq::getColorConfigForID(int color_ID, int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < 0 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getColorConfigForID' of class" << 
    " 'PY8MEs_R12_P30_sm_gq_wpwmggq': Specified color ID '" << 
    color_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R12_P30_sm_gq_wpwmggq_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(color_configs[chosenProcID][color_ID].size(), 0); 
  for (unsigned int j = 0; j < (2 * nexternal); j++ )
  {
    res[chosenPerm[j/2] * 2 + j%2] = color_configs[chosenProcID][color_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Color Config -> Color ID
int PY8MEs_R12_P30_sm_gq_wpwmggq::getColorIDForConfig(vector<int> color_config,
    int specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = -1; 
  if (color_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < color_configs[chosenProcID].size(); i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < (nexternal * 2); j++ )
      {

        // If colorless then make sure it matches
        // The little arithmetics in the color index is just
        // the permutation applies on the particle list which is
        // twice smaller since each particle can have two color indices.
        if (color_config[j] == 0)
        {
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] != 0)
          {
            found = false; 
            break; 
          }
          // Otherwise check that the color linked position matches
        }
        else
        {
          int color_linked_pos = -1; 
          // Find the other end of the line in the user color config
          for (unsigned int k = 0; k < (nexternal * 2); k++ )
          {
            if (k == j)
              continue; 
            if (color_config[j] == color_config[k])
            {
              color_linked_pos = k; 
              break; 
            }
          }
          if (color_linked_pos == -1)
          {
            cerr <<  "Error in function 'getColorIDForConfig' of class" << 
            " 'PY8MEs_R12_P30_sm_gq_wpwmggq': A color line could " << 
            " not be closed." << endl; 
            throw PY8MEs_R12_P30_sm_gq_wpwmggq_exception; 
          }
          // Now check whether the color line matches
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] !=
              color_configs[chosenProcID][i][chosenPerm[color_linked_pos/2] * 2
              + color_linked_pos%2])
          {
            found = false; 
            break; 
          }
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_icol = i; 
        break; 
      }
    }

    if (user_icol == -1)
    {
      cerr <<  "Error in function 'getColorIDForConfig' of class" << 
      " 'PY8MEs_R12_P30_sm_gq_wpwmggq': Specified color" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R12_P30_sm_gq_wpwmggq_exception; 
    }
  }
  return user_icol; 
}

//--------------------------------------------------------------------------
// Returns all result previously computed in SigmaKin
vector < vec_double > PY8MEs_R12_P30_sm_gq_wpwmggq::getAllResults(int
    specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return all_results[chosenProcID]; 
}

//--------------------------------------------------------------------------
// Returns a result previously computed in SigmaKin for a specific helicity
// and color ID. -1 means avg and summed over that characteristic.
double PY8MEs_R12_P30_sm_gq_wpwmggq::getResult(int helicity_ID, int color_ID,
    int specify_proc_ID)
{
  if (helicity_ID < - 1 || helicity_ID >= ncomb)
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R12_P30_sm_gq_wpwmggq': Specified helicity ID '" << 
    helicity_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R12_P30_sm_gq_wpwmggq_exception; 
  }
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < - 1 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R12_P30_sm_gq_wpwmggq': Specified color ID '" << 
    color_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R12_P30_sm_gq_wpwmggq_exception; 
  }
  return all_results[chosenProcID][helicity_ID + 1][color_ID + 1]; 
}

//--------------------------------------------------------------------------
// Check for the availability of the requested process and if available,
// If available, this returns the corresponding permutation and Proc_ID to use.
// If not available, this returns a negative Proc_ID.
pair < vector<int> , int > PY8MEs_R12_P30_sm_gq_wpwmggq::static_getPY8ME(vector<int> initial_pdgs, vector<int> final_pdgs, set<int> schannels) 
{

  // Not available return value
  pair < vector<int> , int > NA(vector<int> (), -1); 

  // Check if s-channel requirements match
  if (nreq_s_channels > 0)
  {
    if (schannels != s_channel_proc)
      return NA; 
  }
  else
  {
    if (schannels.size() != 0)
      return NA; 
  }

  // Check number of final state particles
  if (final_pdgs.size() != (nexternal - ninitial))
    return NA; 

  // Check number of initial state particles
  if (initial_pdgs.size() != ninitial)
    return NA; 

  // List of processes available in this class
  const int nprocs = 16; 
  const int proc_IDS[nprocs] = {0, 0, 1, 1, 2, 2, 3, 3, 0, 0, 1, 1, 2, 2, 3,
      3};
  const int in_pdgs[nprocs][ninitial] = {{21, 2}, {21, 4}, {21, 1}, {21, 3},
      {21, -2}, {21, -4}, {21, -1}, {21, -3}, {2, 21}, {4, 21}, {1, 21}, {3,
      21}, {-2, 21}, {-4, 21}, {-1, 21}, {-3, 21}};
  const int out_pdgs[nprocs][nexternal - ninitial] = {{24, -24, 21, 21, 2},
      {24, -24, 21, 21, 4}, {24, -24, 21, 21, 1}, {24, -24, 21, 21, 3}, {24,
      -24, 21, 21, -2}, {24, -24, 21, 21, -4}, {24, -24, 21, 21, -1}, {24, -24,
      21, 21, -3}, {24, -24, 21, 21, 2}, {24, -24, 21, 21, 4}, {24, -24, 21,
      21, 1}, {24, -24, 21, 21, 3}, {24, -24, 21, 21, -2}, {24, -24, 21, 21,
      -4}, {24, -24, 21, 21, -1}, {24, -24, 21, 21, -3}};

  bool in_pdgs_used[ninitial]; 
  bool out_pdgs_used[nexternal - ninitial]; 
  for(unsigned int i = 0; i < nprocs; i++ )
  {
    int permutations[nexternal]; 

    // Reinitialize initial state look-up variables
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      in_pdgs_used[j] = false; 
      permutations[j] = -1; 
    }
    // Look for initial state matches
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      for(unsigned int k = 0; k < ninitial; k++ )
      {
        // Make sure it has not been used already
        if (in_pdgs_used[k])
          continue; 
        if (initial_pdgs[k] == in_pdgs[i][j])
        {
          permutations[j] = k; 
          in_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[ninitial - 1] == -1)
      continue; 

    // Reinitialize final state look-up variables
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      out_pdgs_used[j] = false; 
      permutations[ninitial + j] = -1; 
    }
    // Look for final state matches
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      for(unsigned int k = 0; k < (nexternal - ninitial); k++ )
      {
        // Make sure it has not been used already
        if (out_pdgs_used[k])
          continue; 
        if (final_pdgs[k] == out_pdgs[i][j])
        {
          permutations[ninitial + j] = ninitial + k; 
          out_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[ninitial + j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[nexternal - 1] == -1)
      continue; 

    // Return process found
    return pair < vector<int> , int > (vector<int> (permutations, permutations
        + nexternal), proc_IDS[i]);
  }

  // No process found
  return NA; 
}

//--------------------------------------------------------------------------
// Set momenta
void PY8MEs_R12_P30_sm_gq_wpwmggq::setMomenta(vector < vec_double >
    momenta_picked)
{
  if (momenta_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setMomenta' of class" << 
    " 'PY8MEs_R12_P30_sm_gq_wpwmggq': Incorrect number of" << 
    " momenta specified." << endl; 
    throw PY8MEs_R12_P30_sm_gq_wpwmggq_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    if (momenta_picked[i].size() != 4)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R12_P30_sm_gq_wpwmggq': Incorrect number of" << 
      " momenta components specified." << endl; 
      throw PY8MEs_R12_P30_sm_gq_wpwmggq_exception; 
    }
    if (momenta_picked[i][0] < 0.0)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R12_P30_sm_gq_wpwmggq': A momentum was specified" << 
      " with negative energy. Check conventions." << endl; 
      throw PY8MEs_R12_P30_sm_gq_wpwmggq_exception; 
    }
    for (unsigned int j = 0; j < 4; j++ )
    {
      p[i][j] = momenta_picked[i][j]; 
    }
  }
}

//--------------------------------------------------------------------------
// Set color configuration to use. An empty vector means sum over all.
void PY8MEs_R12_P30_sm_gq_wpwmggq::setColors(vector<int> colors_picked)
{
  if (colors_picked.size() == 0)
  {
    user_colors = vector<int> (); 
    return; 
  }
  if (colors_picked.size() != (2 * nexternal))
  {
    cerr <<  "Error in function 'setColors' of class" << 
    " 'PY8MEs_R12_P30_sm_gq_wpwmggq': Incorrect number" << 
    " of colors specified." << endl; 
    throw PY8MEs_R12_P30_sm_gq_wpwmggq_exception; 
  }
  user_colors = vector<int> ((2 * nexternal), 0); 
  for(unsigned int i = 0; i < (2 * nexternal); i++ )
  {
    user_colors[i] = colors_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the helicity configuration to use. Am empty vector means sum over all.
void PY8MEs_R12_P30_sm_gq_wpwmggq::setHelicities(vector<int> helicities_picked) 
{
  if (helicities_picked.size() != nexternal)
  {
    if (helicities_picked.size() == 0)
    {
      user_helicities = vector<int> (); 
      return; 
    }
    cerr <<  "Error in function 'setHelicities' of class" << 
    " 'PY8MEs_R12_P30_sm_gq_wpwmggq': Incorrect number" << 
    " of helicities specified." << endl; 
    throw PY8MEs_R12_P30_sm_gq_wpwmggq_exception; 
  }
  user_helicities = vector<int> (nexternal, 0); 
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    user_helicities[i] = helicities_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the permutation to use (will apply to momenta, colors and helicities)
void PY8MEs_R12_P30_sm_gq_wpwmggq::setPermutation(vector<int> perm_picked) 
{
  if (perm_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setPermutations' of class" << 
    " 'PY8MEs_R12_P30_sm_gq_wpwmggq': Incorrect number" << 
    " of permutations specified." << endl; 
    throw PY8MEs_R12_P30_sm_gq_wpwmggq_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    perm[i] = perm_picked[i]; 
  }
}

// Set the proc_ID to use
void PY8MEs_R12_P30_sm_gq_wpwmggq::setProcID(int procID_picked) 
{
  proc_ID = procID_picked; 
}

//--------------------------------------------------------------------------
// Initialize process.

void PY8MEs_R12_P30_sm_gq_wpwmggq::initProc() 
{

  // Initialize flags
  include_symmetry_factors = true; 
  include_helicity_averaging_factors = true; 
  include_color_averaging_factors = true; 

  // Initialize vectors.
  perm = vector<int> (nexternal, 0); 
  user_colors = vector<int> (2 * nexternal, 0); 
  user_helicities = vector<int> (nexternal, 0); 
  p = vector < double * > (); 
  for (unsigned int i = 0; i < nexternal; i++ )
  {
    p.push_back(new double[4]); 
  }
  initColorConfigs(); 
  // Synchronize local variables dependent on the model with the active model.
  mME = vector<double> (nexternal, 0.); 
  syncProcModelParams(); 
  jamp2 = vector < vec_double > (4); 
  jamp2[0] = vector<double> (6, 0.); 
  jamp2[1] = vector<double> (6, 0.); 
  jamp2[2] = vector<double> (6, 0.); 
  jamp2[3] = vector<double> (6, 0.); 
  all_results = vector < vec_vec_double > (4); 
  // The first entry is always the color or helicity avg/summed matrix element.
  all_results[0] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[1] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[2] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[3] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
}

// Synchronize local variables of the process that depend on the model
// parameters
void PY8MEs_R12_P30_sm_gq_wpwmggq::syncProcModelParams() 
{

  // Instantiate the model class and set parameters that stay fixed during run
  mME[0] = pars->ZERO; 
  mME[1] = pars->ZERO; 
  mME[2] = pars->mdl_MW; 
  mME[3] = pars->mdl_MW; 
  mME[4] = pars->ZERO; 
  mME[5] = pars->ZERO; 
  mME[6] = pars->ZERO; 
}

//--------------------------------------------------------------------------
// Setter allowing to force particular values for the external masses
void PY8MEs_R12_P30_sm_gq_wpwmggq::setMasses(vec_double external_masses) 
{

  if (external_masses.size() != mME.size())
  {
    cerr <<  "Error in function 'setMasses' of class" << 
    " 'PY8MEs_R12_P30_sm_gq_wpwmggq': Incorrect number of" << 
    " masses specified." << endl; 
    throw PY8MEs_R12_P30_sm_gq_wpwmggq_exception; 
  }
  for (unsigned int j = 0; j < mME.size(); j++ )
  {
    mME[j] = external_masses[perm[j]]; 
  }
}

//--------------------------------------------------------------------------
// Getter accessing external masses with the correct ordering
vector<double> PY8MEs_R12_P30_sm_gq_wpwmggq::getMasses() 
{

  vec_double external_masses; 
  vector<int> invertedPerm; 
  invertedPerm = invert_mapping(perm); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    external_masses.push_back(mME[invertedPerm[i]]); 
  }
  return external_masses; 

}


// Set all values of the external masses to float(-mode) where mode can be
// 0 : Mass taken from the model
// 1 : Mass taken from p_i^2 if not massless to begin with
// 2 : Mass always taken from p_i^2.
void PY8MEs_R12_P30_sm_gq_wpwmggq::setExternalMassesMode(int mode) 
{
  if (mode != 0 && mode != 1 && mode != 2)
  {
    cerr <<  "Error in function 'setExternalMassesMode' of class" << 
    " 'PY8MEs_R12_P30_sm_gq_wpwmggq': Incorrect mode selected :" << mode << 
    ". It must be either 0, 1 or 2" << endl; 
    throw PY8MEs_R12_P30_sm_gq_wpwmggq_exception; 
  }
  if (mode == 0)
  {
    syncProcModelParams(); 
  }
  else if (mode == 1)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      if (mME[j] != pars->ZERO)
      {
        mME[j] = -1.0; 
      }
    }
  }
  else if (mode == 2)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      mME[j] = -1.0; 
    }
  }
}

//--------------------------------------------------------------------------
// Evaluate the squared matrix element.

double PY8MEs_R12_P30_sm_gq_wpwmggq::sigmaKin() 
{
  // Set the parameters which change event by event
  pars->setDependentParameters(); 
  pars->setDependentCouplings(); 
  // Reset color flows
  for(int i = 0; i < 6; i++ )
    jamp2[0][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[1][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[2][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[3][i] = 0.; 

  // Save previous values of mME
  vector<double> saved_mME(mME.size(), 0.0); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (mME[i] < 0.0)
    {
      saved_mME[i] = mME[i]; 
      mME[i] = sqrt(abs(pow(p[perm[i]][0], 2) - 
      (pow(p[perm[i]][1], 2) + pow(p[perm[i]][2], 2) + pow(p[perm[i]][3],
          2))));
    }
  }

  // Local variables and constants
  const int max_tries = 10; 
  vector < vec_bool > goodhel(nprocesses, vec_bool(ncomb, false)); 
  vec_int ntry(nprocesses, 0); 
  double t = 0.; 
  double result = 0.; 

  if (ntry[proc_ID] <= max_tries)
    ntry[proc_ID] = ntry[proc_ID] + 1; 

  // Find which helicity configuration is asked for
  // -1 indicates one wants to sum over helicities
  int user_ihel = getHelicityIDForConfig(user_helicities); 

  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = getColorIDForConfig(user_colors); 

  // Reset the list of results that will be recomputed here
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      all_results[proc_ID][ihel + 1][icolor + 1] = 0.; 
    }
  }

  // Calculate the matrix element for all helicities
  // unless already detected as vanishing
  for(int ihel = 0; ihel < ncomb; ihel++ )
  {
    // Skip helicity if already detected as vanishing
    if ((ntry[proc_ID] >= max_tries) && !goodhel[proc_ID][ihel])
      continue; 

    // Also skip helicity if user asks for a specific one
    if ((ntry[proc_ID] >= max_tries) && user_ihel != -1 && user_ihel != ihel)
      continue; 

    calculate_wavefunctions(helicities[ihel]); 

    // Reset locally computed color flows
    for(int i = 0; i < 6; i++ )
      jamp2[0][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[1][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[2][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[3][i] = 0.; 

    if (proc_ID == 0)
      t = matrix_12_gu_wpwmggu(); 
    if (proc_ID == 1)
      t = matrix_12_gd_wpwmggd(); 
    if (proc_ID == 2)
      t = matrix_12_gux_wpwmggux(); 
    if (proc_ID == 3)
      t = matrix_12_gdx_wpwmggdx(); 

    // Store which helicities give non-zero result
    if ((ntry[proc_ID] < max_tries) && t != 0. && !goodhel[proc_ID][ihel])
      goodhel[proc_ID][ihel] = true; 

    // Aggregate results
    if (user_ihel == -1 || user_ihel == ihel)
    {
      if (user_icol == -1)
      {
        result = result + t; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][0] += t; 
          for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
          {
            all_results[proc_ID][0][i + 1] += jamp2[proc_ID][i]; 
          }
        }
        all_results[proc_ID][ihel + 1][0] += t; 
        for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
        {
          all_results[proc_ID][ihel + 1][i + 1] += jamp2[proc_ID][i]; 
        }
      }
      else
      {
        result = result + jamp2[proc_ID][user_icol]; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][user_icol + 1] += jamp2[proc_ID][user_icol]; 
        }
        all_results[proc_ID][ihel + 1][user_icol + 1] +=
            jamp2[proc_ID][user_icol];
      }
    }

  }

  // Normalize results with the identical particle factor
  if (include_symmetry_factors)
  {
    result = result/denom_iden[proc_ID]; 
  }
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      if (include_symmetry_factors)
      {
        all_results[proc_ID][ihel + 1][icolor + 1] /= denom_iden[proc_ID]; 
      }
    }
  }


  // Normalize when when summing+averaging over helicity configurations
  if (user_ihel == -1 && include_helicity_averaging_factors)
  {
    result /= denom_hels[proc_ID]; 
    if (user_icol == -1)
    {
      all_results[proc_ID][0][0] /= denom_hels[proc_ID]; 
      for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
      {
        all_results[proc_ID][0][i + 1] /= denom_hels[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][0][user_icol + 1] /= denom_hels[proc_ID]; 
    }
  }

  // Normalize when summing+averaging over color configurations
  if (user_icol == -1 && include_color_averaging_factors)
  {
    result /= denom_colors[proc_ID]; 
    if (user_ihel == -1)
    {
      all_results[proc_ID][0][0] /= denom_colors[proc_ID]; 
      for (unsigned int i = 0; i < ncomb; i++ )
      {
        all_results[proc_ID][i + 1][0] /= denom_colors[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][user_ihel + 1][0] /= denom_colors[proc_ID]; 
    }
  }

  // Reinstate previous values of mME
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (saved_mME[i] < 0.0)
    {
      mME[i] = saved_mME[i]; 
    }
  }

  // Finally return it
  return result; 
}

//==========================================================================
// Private class member functions

//--------------------------------------------------------------------------
// Evaluate |M|^2 for each subprocess

void PY8MEs_R12_P30_sm_gq_wpwmggq::calculate_wavefunctions(const int hel[])
{
  // Calculate wavefunctions for all processes
  // Calculate all wavefunctions
  vxxxxx(p[perm[0]], mME[0], hel[0], -1, w[0]); 
  ixxxxx(p[perm[1]], mME[1], hel[1], +1, w[1]); 
  vxxxxx(p[perm[2]], mME[2], hel[2], +1, w[2]); 
  vxxxxx(p[perm[3]], mME[3], hel[3], +1, w[3]); 
  vxxxxx(p[perm[4]], mME[4], hel[4], +1, w[4]); 
  vxxxxx(p[perm[5]], mME[5], hel[5], +1, w[5]); 
  oxxxxx(p[perm[6]], mME[6], hel[6], +1, w[6]); 
  VVV1P0_1(w[0], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[7]); 
  VVV1P0_1(w[3], w[2], pars->GC_4, pars->ZERO, pars->ZERO, w[8]); 
  VVV1P0_1(w[7], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[9]); 
  FFV1_1(w[6], w[8], pars->GC_2, pars->ZERO, pars->ZERO, w[10]); 
  FFV1_2(w[1], w[8], pars->GC_2, pars->ZERO, pars->ZERO, w[11]); 
  FFV1_1(w[6], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[12]); 
  FFV1_2(w[1], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[13]); 
  VVV1_3(w[3], w[2], pars->GC_53, pars->mdl_MZ, pars->mdl_WZ, w[14]); 
  FFV2_5_1(w[6], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[15]);
  FFV2_5_2(w[1], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[16]);
  FFV1_1(w[6], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[17]); 
  FFV1_1(w[17], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[18]); 
  FFV1_2(w[1], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[19]); 
  FFV1_2(w[19], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[20]); 
  FFV2_2(w[1], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[21]); 
  FFV2_2(w[21], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[22]); 
  FFV1_2(w[21], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[23]); 
  FFV2_1(w[6], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[24]); 
  FFV1_2(w[21], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[25]); 
  FFV1_1(w[24], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[26]); 
  FFV2_1(w[24], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[27]); 
  FFV1_1(w[24], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[28]); 
  FFV2_1(w[17], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[29]); 
  FFV2_2(w[19], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[30]); 
  VVV1P0_1(w[0], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[31]); 
  FFV1_1(w[6], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[32]); 
  FFV1_2(w[1], w[31], pars->GC_11, pars->ZERO, pars->ZERO, w[33]); 
  FFV2_1(w[32], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[34]); 
  FFV1_1(w[32], w[31], pars->GC_11, pars->ZERO, pars->ZERO, w[35]); 
  FFV1_2(w[21], w[31], pars->GC_11, pars->ZERO, pars->ZERO, w[36]); 
  FFV1_2(w[1], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[37]); 
  FFV1_1(w[6], w[31], pars->GC_11, pars->ZERO, pars->ZERO, w[38]); 
  FFV2_2(w[37], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[39]); 
  FFV1_2(w[37], w[31], pars->GC_11, pars->ZERO, pars->ZERO, w[40]); 
  FFV1_1(w[24], w[31], pars->GC_11, pars->ZERO, pars->ZERO, w[41]); 
  VVV1P0_1(w[31], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[42]); 
  FFV1_2(w[21], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[43]); 
  FFV1_1(w[24], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[44]); 
  FFV1_1(w[6], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[45]); 
  VVV1P0_1(w[4], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[46]); 
  FFV2_1(w[45], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[47]); 
  FFV1_2(w[1], w[46], pars->GC_11, pars->ZERO, pars->ZERO, w[48]); 
  FFV1_1(w[45], w[46], pars->GC_11, pars->ZERO, pars->ZERO, w[49]); 
  FFV1_1(w[45], w[8], pars->GC_2, pars->ZERO, pars->ZERO, w[50]); 
  FFV2_5_1(w[45], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[51]);
  FFV1_2(w[37], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[52]); 
  FFV1_1(w[45], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[53]); 
  FFV1_1(w[45], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[54]); 
  FFV1_2(w[19], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[55]); 
  FFV1_2(w[1], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[56]); 
  FFV2_2(w[56], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[57]); 
  FFV1_1(w[6], w[46], pars->GC_11, pars->ZERO, pars->ZERO, w[58]); 
  FFV1_2(w[56], w[46], pars->GC_11, pars->ZERO, pars->ZERO, w[59]); 
  FFV1_2(w[56], w[8], pars->GC_2, pars->ZERO, pars->ZERO, w[60]); 
  FFV2_5_2(w[56], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[61]);
  FFV1_1(w[32], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[62]); 
  FFV1_2(w[56], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[63]); 
  FFV1_2(w[56], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[64]); 
  FFV1_1(w[17], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[65]); 
  VVV1P0_1(w[0], w[46], pars->GC_10, pars->ZERO, pars->ZERO, w[66]); 
  FFV1_2(w[21], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[67]); 
  FFV1_1(w[24], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[68]); 
  FFV1_1(w[32], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[69]); 
  FFV1_2(w[19], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[70]); 
  FFV1_2(w[37], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[71]); 
  FFV1_1(w[17], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[72]); 
  VVVV1P0_1(w[0], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[73]); 
  VVVV3P0_1(w[0], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[74]); 
  VVVV4P0_1(w[0], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[75]); 
  FFV1_1(w[6], w[73], pars->GC_11, pars->ZERO, pars->ZERO, w[76]); 
  FFV1_1(w[6], w[74], pars->GC_11, pars->ZERO, pars->ZERO, w[77]); 
  FFV1_1(w[6], w[75], pars->GC_11, pars->ZERO, pars->ZERO, w[78]); 
  FFV1_2(w[1], w[73], pars->GC_11, pars->ZERO, pars->ZERO, w[79]); 
  FFV1_2(w[1], w[74], pars->GC_11, pars->ZERO, pars->ZERO, w[80]); 
  FFV1_2(w[1], w[75], pars->GC_11, pars->ZERO, pars->ZERO, w[81]); 
  FFV1_1(w[6], w[8], pars->GC_1, pars->ZERO, pars->ZERO, w[82]); 
  FFV1_2(w[1], w[8], pars->GC_1, pars->ZERO, pars->ZERO, w[83]); 
  FFV2_3_1(w[6], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[84]);
  FFV2_3_2(w[1], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[85]);
  FFV2_1(w[6], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[86]); 
  FFV2_1(w[86], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[87]); 
  FFV1_1(w[86], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[88]); 
  FFV2_2(w[1], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[89]); 
  FFV1_1(w[86], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[90]); 
  FFV1_2(w[89], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[91]); 
  FFV2_2(w[89], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[92]); 
  FFV1_2(w[89], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[93]); 
  FFV2_1(w[17], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[94]); 
  FFV2_2(w[19], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[95]); 
  FFV2_1(w[32], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[96]); 
  FFV1_2(w[89], w[31], pars->GC_11, pars->ZERO, pars->ZERO, w[97]); 
  FFV2_2(w[37], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[98]); 
  FFV1_1(w[86], w[31], pars->GC_11, pars->ZERO, pars->ZERO, w[99]); 
  FFV1_1(w[86], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[100]); 
  FFV1_2(w[89], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[101]); 
  FFV2_1(w[45], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[102]); 
  FFV1_1(w[45], w[8], pars->GC_1, pars->ZERO, pars->ZERO, w[103]); 
  FFV2_3_1(w[45], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[104]);
  FFV2_2(w[56], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[105]); 
  FFV1_2(w[56], w[8], pars->GC_1, pars->ZERO, pars->ZERO, w[106]); 
  FFV2_3_2(w[56], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[107]);
  FFV1_1(w[86], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[108]); 
  FFV1_2(w[89], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[109]); 
  oxxxxx(p[perm[1]], mME[1], hel[1], -1, w[110]); 
  ixxxxx(p[perm[6]], mME[6], hel[6], -1, w[111]); 
  FFV1_1(w[110], w[8], pars->GC_2, pars->ZERO, pars->ZERO, w[112]); 
  FFV1_2(w[111], w[8], pars->GC_2, pars->ZERO, pars->ZERO, w[113]); 
  FFV1_1(w[110], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[114]); 
  FFV1_2(w[111], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[115]); 
  FFV2_5_1(w[110], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[116]);
  FFV2_5_2(w[111], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[117]);
  FFV1_1(w[110], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[118]); 
  FFV1_1(w[118], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[119]); 
  FFV1_2(w[111], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[120]); 
  FFV1_2(w[120], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[121]); 
  FFV2_2(w[111], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[122]); 
  FFV2_2(w[122], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[123]); 
  FFV1_2(w[122], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[124]); 
  FFV2_1(w[110], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[125]); 
  FFV1_2(w[122], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[126]); 
  FFV1_1(w[125], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[127]); 
  FFV2_1(w[125], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[128]); 
  FFV1_1(w[125], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[129]); 
  FFV2_1(w[118], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[130]); 
  FFV2_2(w[120], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[131]); 
  FFV1_1(w[110], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[132]); 
  FFV1_2(w[111], w[31], pars->GC_11, pars->ZERO, pars->ZERO, w[133]); 
  FFV2_1(w[132], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[134]); 
  FFV1_1(w[132], w[31], pars->GC_11, pars->ZERO, pars->ZERO, w[135]); 
  FFV1_2(w[122], w[31], pars->GC_11, pars->ZERO, pars->ZERO, w[136]); 
  FFV1_2(w[111], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[137]); 
  FFV1_1(w[110], w[31], pars->GC_11, pars->ZERO, pars->ZERO, w[138]); 
  FFV2_2(w[137], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[139]); 
  FFV1_2(w[137], w[31], pars->GC_11, pars->ZERO, pars->ZERO, w[140]); 
  FFV1_1(w[125], w[31], pars->GC_11, pars->ZERO, pars->ZERO, w[141]); 
  FFV1_2(w[122], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[142]); 
  FFV1_1(w[125], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[143]); 
  FFV1_1(w[110], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[144]); 
  FFV2_1(w[144], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[145]); 
  FFV1_2(w[111], w[46], pars->GC_11, pars->ZERO, pars->ZERO, w[146]); 
  FFV1_1(w[144], w[46], pars->GC_11, pars->ZERO, pars->ZERO, w[147]); 
  FFV1_1(w[144], w[8], pars->GC_2, pars->ZERO, pars->ZERO, w[148]); 
  FFV2_5_1(w[144], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[149]);
  FFV1_2(w[137], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[150]); 
  FFV1_1(w[144], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[151]); 
  FFV1_1(w[144], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[152]); 
  FFV1_2(w[120], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[153]); 
  FFV1_2(w[111], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[154]); 
  FFV2_2(w[154], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[155]); 
  FFV1_1(w[110], w[46], pars->GC_11, pars->ZERO, pars->ZERO, w[156]); 
  FFV1_2(w[154], w[46], pars->GC_11, pars->ZERO, pars->ZERO, w[157]); 
  FFV1_2(w[154], w[8], pars->GC_2, pars->ZERO, pars->ZERO, w[158]); 
  FFV2_5_2(w[154], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[159]);
  FFV1_1(w[132], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[160]); 
  FFV1_2(w[154], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[161]); 
  FFV1_2(w[154], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[162]); 
  FFV1_1(w[118], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[163]); 
  FFV1_2(w[122], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[164]); 
  FFV1_1(w[125], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[165]); 
  FFV1_1(w[132], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[166]); 
  FFV1_2(w[120], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[167]); 
  FFV1_2(w[137], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[168]); 
  FFV1_1(w[118], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[169]); 
  FFV1_1(w[110], w[73], pars->GC_11, pars->ZERO, pars->ZERO, w[170]); 
  FFV1_1(w[110], w[74], pars->GC_11, pars->ZERO, pars->ZERO, w[171]); 
  FFV1_1(w[110], w[75], pars->GC_11, pars->ZERO, pars->ZERO, w[172]); 
  FFV1_2(w[111], w[73], pars->GC_11, pars->ZERO, pars->ZERO, w[173]); 
  FFV1_2(w[111], w[74], pars->GC_11, pars->ZERO, pars->ZERO, w[174]); 
  FFV1_2(w[111], w[75], pars->GC_11, pars->ZERO, pars->ZERO, w[175]); 
  FFV1_1(w[110], w[8], pars->GC_1, pars->ZERO, pars->ZERO, w[176]); 
  FFV1_2(w[111], w[8], pars->GC_1, pars->ZERO, pars->ZERO, w[177]); 
  FFV2_3_1(w[110], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[178]);
  FFV2_3_2(w[111], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[179]);
  FFV2_1(w[110], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[180]); 
  FFV2_1(w[180], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[181]); 
  FFV1_1(w[180], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[182]); 
  FFV2_2(w[111], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[183]); 
  FFV1_1(w[180], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[184]); 
  FFV1_2(w[183], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[185]); 
  FFV2_2(w[183], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[186]); 
  FFV1_2(w[183], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[187]); 
  FFV2_1(w[118], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[188]); 
  FFV2_2(w[120], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[189]); 
  FFV2_1(w[132], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[190]); 
  FFV1_2(w[183], w[31], pars->GC_11, pars->ZERO, pars->ZERO, w[191]); 
  FFV2_2(w[137], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[192]); 
  FFV1_1(w[180], w[31], pars->GC_11, pars->ZERO, pars->ZERO, w[193]); 
  FFV1_1(w[180], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[194]); 
  FFV1_2(w[183], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[195]); 
  FFV2_1(w[144], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[196]); 
  FFV1_1(w[144], w[8], pars->GC_1, pars->ZERO, pars->ZERO, w[197]); 
  FFV2_3_1(w[144], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[198]);
  FFV2_2(w[154], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[199]); 
  FFV1_2(w[154], w[8], pars->GC_1, pars->ZERO, pars->ZERO, w[200]); 
  FFV2_3_2(w[154], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[201]);
  FFV1_1(w[180], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[202]); 
  FFV1_2(w[183], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[203]); 

  // Calculate all amplitudes
  // Amplitude(s) for diagram number 0
  FFV1_0(w[1], w[10], w[9], pars->GC_11, amp[0]); 
  FFV1_0(w[11], w[6], w[9], pars->GC_11, amp[1]); 
  FFV1_0(w[11], w[12], w[5], pars->GC_11, amp[2]); 
  FFV1_0(w[13], w[10], w[5], pars->GC_11, amp[3]); 
  FFV1_0(w[1], w[15], w[9], pars->GC_11, amp[4]); 
  FFV1_0(w[16], w[6], w[9], pars->GC_11, amp[5]); 
  FFV1_0(w[16], w[12], w[5], pars->GC_11, amp[6]); 
  FFV1_0(w[13], w[15], w[5], pars->GC_11, amp[7]); 
  FFV1_0(w[1], w[18], w[8], pars->GC_2, amp[8]); 
  FFV1_0(w[13], w[17], w[8], pars->GC_2, amp[9]); 
  FFV2_5_0(w[1], w[18], w[14], pars->GC_51, pars->GC_58, amp[10]); 
  FFV2_5_0(w[13], w[17], w[14], pars->GC_51, pars->GC_58, amp[11]); 
  FFV1_0(w[20], w[6], w[8], pars->GC_2, amp[12]); 
  FFV1_0(w[19], w[12], w[8], pars->GC_2, amp[13]); 
  FFV2_5_0(w[20], w[6], w[14], pars->GC_51, pars->GC_58, amp[14]); 
  FFV2_5_0(w[19], w[12], w[14], pars->GC_51, pars->GC_58, amp[15]); 
  FFV1_0(w[22], w[6], w[9], pars->GC_11, amp[16]); 
  FFV1_0(w[22], w[12], w[5], pars->GC_11, amp[17]); 
  FFV2_0(w[23], w[12], w[3], pars->GC_100, amp[18]); 
  FFV1_0(w[25], w[24], w[5], pars->GC_11, amp[19]); 
  FFV1_0(w[21], w[26], w[5], pars->GC_11, amp[20]); 
  FFV1_0(w[21], w[24], w[9], pars->GC_11, amp[21]); 
  FFV2_0(w[25], w[17], w[3], pars->GC_100, amp[22]); 
  FFV2_0(w[21], w[18], w[3], pars->GC_100, amp[23]); 
  FFV1_0(w[1], w[27], w[9], pars->GC_11, amp[24]); 
  FFV1_0(w[13], w[27], w[5], pars->GC_11, amp[25]); 
  FFV2_0(w[13], w[28], w[2], pars->GC_100, amp[26]); 
  FFV2_0(w[19], w[26], w[2], pars->GC_100, amp[27]); 
  FFV2_0(w[20], w[24], w[2], pars->GC_100, amp[28]); 
  FFV2_0(w[13], w[29], w[2], pars->GC_100, amp[29]); 
  FFV2_0(w[30], w[12], w[3], pars->GC_100, amp[30]); 
  FFV2_0(w[33], w[34], w[2], pars->GC_100, amp[31]); 
  FFV1_0(w[1], w[35], w[8], pars->GC_2, amp[32]); 
  FFV1_0(w[33], w[32], w[8], pars->GC_2, amp[33]); 
  FFV2_5_0(w[1], w[35], w[14], pars->GC_51, pars->GC_58, amp[34]); 
  FFV2_5_0(w[33], w[32], w[14], pars->GC_51, pars->GC_58, amp[35]); 
  FFV2_0(w[21], w[35], w[3], pars->GC_100, amp[36]); 
  FFV2_0(w[36], w[32], w[3], pars->GC_100, amp[37]); 
  FFV2_0(w[39], w[38], w[3], pars->GC_100, amp[38]); 
  FFV1_0(w[40], w[6], w[8], pars->GC_2, amp[39]); 
  FFV1_0(w[37], w[38], w[8], pars->GC_2, amp[40]); 
  FFV2_5_0(w[40], w[6], w[14], pars->GC_51, pars->GC_58, amp[41]); 
  FFV2_5_0(w[37], w[38], w[14], pars->GC_51, pars->GC_58, amp[42]); 
  FFV2_0(w[40], w[24], w[2], pars->GC_100, amp[43]); 
  FFV2_0(w[37], w[41], w[2], pars->GC_100, amp[44]); 
  FFV1_0(w[1], w[10], w[42], pars->GC_11, amp[45]); 
  FFV1_0(w[11], w[6], w[42], pars->GC_11, amp[46]); 
  FFV1_0(w[11], w[38], w[4], pars->GC_11, amp[47]); 
  FFV1_0(w[33], w[10], w[4], pars->GC_11, amp[48]); 
  FFV1_0(w[1], w[15], w[42], pars->GC_11, amp[49]); 
  FFV1_0(w[16], w[6], w[42], pars->GC_11, amp[50]); 
  FFV1_0(w[16], w[38], w[4], pars->GC_11, amp[51]); 
  FFV1_0(w[33], w[15], w[4], pars->GC_11, amp[52]); 
  FFV1_0(w[22], w[6], w[42], pars->GC_11, amp[53]); 
  FFV2_0(w[43], w[38], w[3], pars->GC_100, amp[54]); 
  FFV1_0(w[22], w[38], w[4], pars->GC_11, amp[55]); 
  FFV1_0(w[21], w[24], w[42], pars->GC_11, amp[56]); 
  FFV1_0(w[36], w[24], w[4], pars->GC_11, amp[57]); 
  FFV1_0(w[21], w[41], w[4], pars->GC_11, amp[58]); 
  FFV1_0(w[1], w[27], w[42], pars->GC_11, amp[59]); 
  FFV2_0(w[33], w[44], w[2], pars->GC_100, amp[60]); 
  FFV1_0(w[33], w[27], w[4], pars->GC_11, amp[61]); 
  FFV2_0(w[48], w[47], w[2], pars->GC_100, amp[62]); 
  FFV1_0(w[1], w[49], w[8], pars->GC_2, amp[63]); 
  FFV1_0(w[1], w[50], w[46], pars->GC_11, amp[64]); 
  FFV2_5_0(w[1], w[49], w[14], pars->GC_51, pars->GC_58, amp[65]); 
  FFV1_0(w[1], w[51], w[46], pars->GC_11, amp[66]); 
  FFV2_0(w[21], w[49], w[3], pars->GC_100, amp[67]); 
  FFV1_0(w[21], w[47], w[46], pars->GC_11, amp[68]); 
  FFV1_0(w[39], w[47], w[5], pars->GC_11, amp[69]); 
  FFV2_0(w[52], w[47], w[2], pars->GC_100, amp[70]); 
  FFV2_0(w[39], w[53], w[3], pars->GC_100, amp[71]); 
  FFV1_0(w[37], w[50], w[5], pars->GC_11, amp[72]); 
  FFV1_0(w[37], w[53], w[8], pars->GC_2, amp[73]); 
  FFV1_0(w[37], w[51], w[5], pars->GC_11, amp[74]); 
  FFV2_5_0(w[37], w[53], w[14], pars->GC_51, pars->GC_58, amp[75]); 
  FFV1_0(w[11], w[54], w[5], pars->GC_11, amp[76]); 
  FFV1_0(w[11], w[53], w[4], pars->GC_11, amp[77]); 
  FFV1_0(w[16], w[54], w[5], pars->GC_11, amp[78]); 
  FFV1_0(w[16], w[53], w[4], pars->GC_11, amp[79]); 
  FFV1_0(w[19], w[54], w[8], pars->GC_2, amp[80]); 
  FFV1_0(w[19], w[50], w[4], pars->GC_11, amp[81]); 
  FFV2_5_0(w[19], w[54], w[14], pars->GC_51, pars->GC_58, amp[82]); 
  FFV1_0(w[19], w[51], w[4], pars->GC_11, amp[83]); 
  FFV1_0(w[22], w[54], w[5], pars->GC_11, amp[84]); 
  FFV2_0(w[23], w[54], w[3], pars->GC_100, amp[85]); 
  FFV1_0(w[43], w[47], w[5], pars->GC_11, amp[86]); 
  FFV1_0(w[23], w[47], w[4], pars->GC_11, amp[87]); 
  FFV2_0(w[43], w[53], w[3], pars->GC_100, amp[88]); 
  FFV1_0(w[22], w[53], w[4], pars->GC_11, amp[89]); 
  FFV2_0(w[30], w[54], w[3], pars->GC_100, amp[90]); 
  FFV2_0(w[55], w[47], w[2], pars->GC_100, amp[91]); 
  FFV1_0(w[30], w[47], w[4], pars->GC_11, amp[92]); 
  FFV2_0(w[57], w[58], w[3], pars->GC_100, amp[93]); 
  FFV1_0(w[59], w[6], w[8], pars->GC_2, amp[94]); 
  FFV1_0(w[60], w[6], w[46], pars->GC_11, amp[95]); 
  FFV2_5_0(w[59], w[6], w[14], pars->GC_51, pars->GC_58, amp[96]); 
  FFV1_0(w[61], w[6], w[46], pars->GC_11, amp[97]); 
  FFV2_0(w[59], w[24], w[2], pars->GC_100, amp[98]); 
  FFV1_0(w[57], w[24], w[46], pars->GC_11, amp[99]); 
  FFV1_0(w[57], w[34], w[5], pars->GC_11, amp[100]); 
  FFV2_0(w[57], w[62], w[3], pars->GC_100, amp[101]); 
  FFV2_0(w[63], w[34], w[2], pars->GC_100, amp[102]); 
  FFV1_0(w[60], w[32], w[5], pars->GC_11, amp[103]); 
  FFV1_0(w[63], w[32], w[8], pars->GC_2, amp[104]); 
  FFV1_0(w[61], w[32], w[5], pars->GC_11, amp[105]); 
  FFV2_5_0(w[63], w[32], w[14], pars->GC_51, pars->GC_58, amp[106]); 
  FFV1_0(w[64], w[10], w[5], pars->GC_11, amp[107]); 
  FFV1_0(w[63], w[10], w[4], pars->GC_11, amp[108]); 
  FFV1_0(w[64], w[15], w[5], pars->GC_11, amp[109]); 
  FFV1_0(w[63], w[15], w[4], pars->GC_11, amp[110]); 
  FFV1_0(w[64], w[17], w[8], pars->GC_2, amp[111]); 
  FFV1_0(w[60], w[17], w[4], pars->GC_11, amp[112]); 
  FFV2_5_0(w[64], w[17], w[14], pars->GC_51, pars->GC_58, amp[113]); 
  FFV1_0(w[61], w[17], w[4], pars->GC_11, amp[114]); 
  FFV1_0(w[64], w[27], w[5], pars->GC_11, amp[115]); 
  FFV2_0(w[64], w[28], w[2], pars->GC_100, amp[116]); 
  FFV1_0(w[57], w[44], w[5], pars->GC_11, amp[117]); 
  FFV1_0(w[57], w[28], w[4], pars->GC_11, amp[118]); 
  FFV2_0(w[63], w[44], w[2], pars->GC_100, amp[119]); 
  FFV1_0(w[63], w[27], w[4], pars->GC_11, amp[120]); 
  FFV2_0(w[64], w[29], w[2], pars->GC_100, amp[121]); 
  FFV2_0(w[57], w[65], w[3], pars->GC_100, amp[122]); 
  FFV1_0(w[57], w[29], w[4], pars->GC_11, amp[123]); 
  FFV1_0(w[1], w[10], w[66], pars->GC_11, amp[124]); 
  FFV1_0(w[11], w[6], w[66], pars->GC_11, amp[125]); 
  FFV1_0(w[11], w[58], w[0], pars->GC_11, amp[126]); 
  FFV1_0(w[48], w[10], w[0], pars->GC_11, amp[127]); 
  FFV1_0(w[1], w[15], w[66], pars->GC_11, amp[128]); 
  FFV1_0(w[16], w[6], w[66], pars->GC_11, amp[129]); 
  FFV1_0(w[16], w[58], w[0], pars->GC_11, amp[130]); 
  FFV1_0(w[48], w[15], w[0], pars->GC_11, amp[131]); 
  FFV1_0(w[22], w[6], w[66], pars->GC_11, amp[132]); 
  FFV2_0(w[67], w[58], w[3], pars->GC_100, amp[133]); 
  FFV1_0(w[22], w[58], w[0], pars->GC_11, amp[134]); 
  FFV1_0(w[21], w[24], w[66], pars->GC_11, amp[135]); 
  FFV1_0(w[67], w[24], w[46], pars->GC_11, amp[136]); 
  FFV1_0(w[21], w[68], w[46], pars->GC_11, amp[137]); 
  FFV1_0(w[1], w[27], w[66], pars->GC_11, amp[138]); 
  FFV2_0(w[48], w[68], w[2], pars->GC_100, amp[139]); 
  FFV1_0(w[48], w[27], w[0], pars->GC_11, amp[140]); 
  FFV1_0(w[11], w[69], w[5], pars->GC_11, amp[141]); 
  FFV1_0(w[11], w[62], w[0], pars->GC_11, amp[142]); 
  FFV1_0(w[16], w[69], w[5], pars->GC_11, amp[143]); 
  FFV1_0(w[16], w[62], w[0], pars->GC_11, amp[144]); 
  FFV1_0(w[19], w[69], w[8], pars->GC_2, amp[145]); 
  FFV1_0(w[70], w[32], w[8], pars->GC_2, amp[146]); 
  FFV2_5_0(w[19], w[69], w[14], pars->GC_51, pars->GC_58, amp[147]); 
  FFV2_5_0(w[70], w[32], w[14], pars->GC_51, pars->GC_58, amp[148]); 
  FFV1_0(w[22], w[69], w[5], pars->GC_11, amp[149]); 
  FFV2_0(w[23], w[69], w[3], pars->GC_100, amp[150]); 
  FFV1_0(w[67], w[34], w[5], pars->GC_11, amp[151]); 
  FFV2_0(w[67], w[62], w[3], pars->GC_100, amp[152]); 
  FFV1_0(w[23], w[34], w[0], pars->GC_11, amp[153]); 
  FFV1_0(w[22], w[62], w[0], pars->GC_11, amp[154]); 
  FFV2_0(w[30], w[69], w[3], pars->GC_100, amp[155]); 
  FFV2_0(w[70], w[34], w[2], pars->GC_100, amp[156]); 
  FFV1_0(w[30], w[34], w[0], pars->GC_11, amp[157]); 
  FFV1_0(w[71], w[10], w[5], pars->GC_11, amp[158]); 
  FFV1_0(w[52], w[10], w[0], pars->GC_11, amp[159]); 
  FFV1_0(w[71], w[15], w[5], pars->GC_11, amp[160]); 
  FFV1_0(w[52], w[15], w[0], pars->GC_11, amp[161]); 
  FFV1_0(w[71], w[17], w[8], pars->GC_2, amp[162]); 
  FFV1_0(w[37], w[72], w[8], pars->GC_2, amp[163]); 
  FFV2_5_0(w[71], w[17], w[14], pars->GC_51, pars->GC_58, amp[164]); 
  FFV2_5_0(w[37], w[72], w[14], pars->GC_51, pars->GC_58, amp[165]); 
  FFV1_0(w[71], w[27], w[5], pars->GC_11, amp[166]); 
  FFV2_0(w[71], w[28], w[2], pars->GC_100, amp[167]); 
  FFV1_0(w[39], w[68], w[5], pars->GC_11, amp[168]); 
  FFV2_0(w[52], w[68], w[2], pars->GC_100, amp[169]); 
  FFV1_0(w[39], w[28], w[0], pars->GC_11, amp[170]); 
  FFV1_0(w[52], w[27], w[0], pars->GC_11, amp[171]); 
  FFV2_0(w[71], w[29], w[2], pars->GC_100, amp[172]); 
  FFV2_0(w[39], w[72], w[3], pars->GC_100, amp[173]); 
  FFV1_0(w[39], w[29], w[0], pars->GC_11, amp[174]); 
  FFV1_0(w[11], w[72], w[4], pars->GC_11, amp[175]); 
  FFV1_0(w[11], w[65], w[0], pars->GC_11, amp[176]); 
  FFV1_0(w[16], w[72], w[4], pars->GC_11, amp[177]); 
  FFV1_0(w[16], w[65], w[0], pars->GC_11, amp[178]); 
  FFV1_0(w[70], w[10], w[4], pars->GC_11, amp[179]); 
  FFV1_0(w[55], w[10], w[0], pars->GC_11, amp[180]); 
  FFV1_0(w[70], w[15], w[4], pars->GC_11, amp[181]); 
  FFV1_0(w[55], w[15], w[0], pars->GC_11, amp[182]); 
  FFV1_0(w[67], w[44], w[5], pars->GC_11, amp[183]); 
  FFV1_0(w[67], w[28], w[4], pars->GC_11, amp[184]); 
  FFV1_0(w[43], w[68], w[5], pars->GC_11, amp[185]); 
  FFV1_0(w[23], w[68], w[4], pars->GC_11, amp[186]); 
  FFV1_0(w[43], w[28], w[0], pars->GC_11, amp[187]); 
  FFV1_0(w[23], w[44], w[0], pars->GC_11, amp[188]); 
  FFV2_0(w[67], w[65], w[3], pars->GC_100, amp[189]); 
  FFV1_0(w[67], w[29], w[4], pars->GC_11, amp[190]); 
  FFV2_0(w[43], w[72], w[3], pars->GC_100, amp[191]); 
  FFV1_0(w[22], w[72], w[4], pars->GC_11, amp[192]); 
  FFV1_0(w[43], w[29], w[0], pars->GC_11, amp[193]); 
  FFV1_0(w[22], w[65], w[0], pars->GC_11, amp[194]); 
  FFV2_0(w[55], w[68], w[2], pars->GC_100, amp[195]); 
  FFV1_0(w[30], w[68], w[4], pars->GC_11, amp[196]); 
  FFV2_0(w[70], w[44], w[2], pars->GC_100, amp[197]); 
  FFV1_0(w[70], w[27], w[4], pars->GC_11, amp[198]); 
  FFV1_0(w[30], w[44], w[0], pars->GC_11, amp[199]); 
  FFV1_0(w[55], w[27], w[0], pars->GC_11, amp[200]); 
  FFV1_0(w[1], w[76], w[8], pars->GC_2, amp[201]); 
  FFV1_0(w[1], w[77], w[8], pars->GC_2, amp[202]); 
  FFV1_0(w[1], w[78], w[8], pars->GC_2, amp[203]); 
  FFV1_0(w[79], w[6], w[8], pars->GC_2, amp[204]); 
  FFV1_0(w[80], w[6], w[8], pars->GC_2, amp[205]); 
  FFV1_0(w[81], w[6], w[8], pars->GC_2, amp[206]); 
  FFV2_5_0(w[1], w[76], w[14], pars->GC_51, pars->GC_58, amp[207]); 
  FFV2_5_0(w[1], w[77], w[14], pars->GC_51, pars->GC_58, amp[208]); 
  FFV2_5_0(w[1], w[78], w[14], pars->GC_51, pars->GC_58, amp[209]); 
  FFV2_5_0(w[79], w[6], w[14], pars->GC_51, pars->GC_58, amp[210]); 
  FFV2_5_0(w[80], w[6], w[14], pars->GC_51, pars->GC_58, amp[211]); 
  FFV2_5_0(w[81], w[6], w[14], pars->GC_51, pars->GC_58, amp[212]); 
  FFV2_0(w[21], w[76], w[3], pars->GC_100, amp[213]); 
  FFV2_0(w[21], w[77], w[3], pars->GC_100, amp[214]); 
  FFV2_0(w[21], w[78], w[3], pars->GC_100, amp[215]); 
  FFV1_0(w[21], w[24], w[73], pars->GC_11, amp[216]); 
  FFV1_0(w[21], w[24], w[74], pars->GC_11, amp[217]); 
  FFV1_0(w[21], w[24], w[75], pars->GC_11, amp[218]); 
  FFV2_0(w[79], w[24], w[2], pars->GC_100, amp[219]); 
  FFV2_0(w[80], w[24], w[2], pars->GC_100, amp[220]); 
  FFV2_0(w[81], w[24], w[2], pars->GC_100, amp[221]); 
  FFV1_0(w[1], w[82], w[9], pars->GC_11, amp[222]); 
  FFV1_0(w[83], w[6], w[9], pars->GC_11, amp[223]); 
  FFV1_0(w[83], w[12], w[5], pars->GC_11, amp[224]); 
  FFV1_0(w[13], w[82], w[5], pars->GC_11, amp[225]); 
  FFV1_0(w[1], w[84], w[9], pars->GC_11, amp[226]); 
  FFV1_0(w[85], w[6], w[9], pars->GC_11, amp[227]); 
  FFV1_0(w[85], w[12], w[5], pars->GC_11, amp[228]); 
  FFV1_0(w[13], w[84], w[5], pars->GC_11, amp[229]); 
  FFV1_0(w[1], w[18], w[8], pars->GC_1, amp[230]); 
  FFV1_0(w[13], w[17], w[8], pars->GC_1, amp[231]); 
  FFV2_3_0(w[1], w[18], w[14], pars->GC_50, pars->GC_58, amp[232]); 
  FFV2_3_0(w[13], w[17], w[14], pars->GC_50, pars->GC_58, amp[233]); 
  FFV1_0(w[20], w[6], w[8], pars->GC_1, amp[234]); 
  FFV1_0(w[19], w[12], w[8], pars->GC_1, amp[235]); 
  FFV2_3_0(w[20], w[6], w[14], pars->GC_50, pars->GC_58, amp[236]); 
  FFV2_3_0(w[19], w[12], w[14], pars->GC_50, pars->GC_58, amp[237]); 
  FFV1_0(w[1], w[87], w[9], pars->GC_11, amp[238]); 
  FFV1_0(w[13], w[87], w[5], pars->GC_11, amp[239]); 
  FFV2_0(w[13], w[88], w[3], pars->GC_100, amp[240]); 
  FFV1_0(w[89], w[90], w[5], pars->GC_11, amp[241]); 
  FFV1_0(w[91], w[86], w[5], pars->GC_11, amp[242]); 
  FFV1_0(w[89], w[86], w[9], pars->GC_11, amp[243]); 
  FFV2_0(w[19], w[90], w[3], pars->GC_100, amp[244]); 
  FFV2_0(w[20], w[86], w[3], pars->GC_100, amp[245]); 
  FFV1_0(w[92], w[6], w[9], pars->GC_11, amp[246]); 
  FFV1_0(w[92], w[12], w[5], pars->GC_11, amp[247]); 
  FFV2_0(w[93], w[12], w[2], pars->GC_100, amp[248]); 
  FFV2_0(w[91], w[17], w[2], pars->GC_100, amp[249]); 
  FFV2_0(w[89], w[18], w[2], pars->GC_100, amp[250]); 
  FFV2_0(w[13], w[94], w[3], pars->GC_100, amp[251]); 
  FFV2_0(w[95], w[12], w[2], pars->GC_100, amp[252]); 
  FFV2_0(w[33], w[96], w[3], pars->GC_100, amp[253]); 
  FFV1_0(w[1], w[35], w[8], pars->GC_1, amp[254]); 
  FFV1_0(w[33], w[32], w[8], pars->GC_1, amp[255]); 
  FFV2_3_0(w[1], w[35], w[14], pars->GC_50, pars->GC_58, amp[256]); 
  FFV2_3_0(w[33], w[32], w[14], pars->GC_50, pars->GC_58, amp[257]); 
  FFV2_0(w[89], w[35], w[2], pars->GC_100, amp[258]); 
  FFV2_0(w[97], w[32], w[2], pars->GC_100, amp[259]); 
  FFV2_0(w[98], w[38], w[2], pars->GC_100, amp[260]); 
  FFV1_0(w[40], w[6], w[8], pars->GC_1, amp[261]); 
  FFV1_0(w[37], w[38], w[8], pars->GC_1, amp[262]); 
  FFV2_3_0(w[40], w[6], w[14], pars->GC_50, pars->GC_58, amp[263]); 
  FFV2_3_0(w[37], w[38], w[14], pars->GC_50, pars->GC_58, amp[264]); 
  FFV2_0(w[40], w[86], w[3], pars->GC_100, amp[265]); 
  FFV2_0(w[37], w[99], w[3], pars->GC_100, amp[266]); 
  FFV1_0(w[1], w[82], w[42], pars->GC_11, amp[267]); 
  FFV1_0(w[83], w[6], w[42], pars->GC_11, amp[268]); 
  FFV1_0(w[83], w[38], w[4], pars->GC_11, amp[269]); 
  FFV1_0(w[33], w[82], w[4], pars->GC_11, amp[270]); 
  FFV1_0(w[1], w[84], w[42], pars->GC_11, amp[271]); 
  FFV1_0(w[85], w[6], w[42], pars->GC_11, amp[272]); 
  FFV1_0(w[85], w[38], w[4], pars->GC_11, amp[273]); 
  FFV1_0(w[33], w[84], w[4], pars->GC_11, amp[274]); 
  FFV1_0(w[1], w[87], w[42], pars->GC_11, amp[275]); 
  FFV2_0(w[33], w[100], w[3], pars->GC_100, amp[276]); 
  FFV1_0(w[33], w[87], w[4], pars->GC_11, amp[277]); 
  FFV1_0(w[89], w[86], w[42], pars->GC_11, amp[278]); 
  FFV1_0(w[89], w[99], w[4], pars->GC_11, amp[279]); 
  FFV1_0(w[97], w[86], w[4], pars->GC_11, amp[280]); 
  FFV1_0(w[92], w[6], w[42], pars->GC_11, amp[281]); 
  FFV2_0(w[101], w[38], w[2], pars->GC_100, amp[282]); 
  FFV1_0(w[92], w[38], w[4], pars->GC_11, amp[283]); 
  FFV2_0(w[48], w[102], w[3], pars->GC_100, amp[284]); 
  FFV1_0(w[1], w[49], w[8], pars->GC_1, amp[285]); 
  FFV1_0(w[1], w[103], w[46], pars->GC_11, amp[286]); 
  FFV2_3_0(w[1], w[49], w[14], pars->GC_50, pars->GC_58, amp[287]); 
  FFV1_0(w[1], w[104], w[46], pars->GC_11, amp[288]); 
  FFV2_0(w[89], w[49], w[2], pars->GC_100, amp[289]); 
  FFV1_0(w[89], w[102], w[46], pars->GC_11, amp[290]); 
  FFV1_0(w[98], w[102], w[5], pars->GC_11, amp[291]); 
  FFV2_0(w[52], w[102], w[3], pars->GC_100, amp[292]); 
  FFV2_0(w[98], w[53], w[2], pars->GC_100, amp[293]); 
  FFV1_0(w[37], w[103], w[5], pars->GC_11, amp[294]); 
  FFV1_0(w[37], w[53], w[8], pars->GC_1, amp[295]); 
  FFV1_0(w[37], w[104], w[5], pars->GC_11, amp[296]); 
  FFV2_3_0(w[37], w[53], w[14], pars->GC_50, pars->GC_58, amp[297]); 
  FFV1_0(w[83], w[54], w[5], pars->GC_11, amp[298]); 
  FFV1_0(w[83], w[53], w[4], pars->GC_11, amp[299]); 
  FFV1_0(w[85], w[54], w[5], pars->GC_11, amp[300]); 
  FFV1_0(w[85], w[53], w[4], pars->GC_11, amp[301]); 
  FFV1_0(w[19], w[54], w[8], pars->GC_1, amp[302]); 
  FFV1_0(w[19], w[103], w[4], pars->GC_11, amp[303]); 
  FFV2_3_0(w[19], w[54], w[14], pars->GC_50, pars->GC_58, amp[304]); 
  FFV1_0(w[19], w[104], w[4], pars->GC_11, amp[305]); 
  FFV1_0(w[92], w[54], w[5], pars->GC_11, amp[306]); 
  FFV2_0(w[93], w[54], w[2], pars->GC_100, amp[307]); 
  FFV1_0(w[101], w[102], w[5], pars->GC_11, amp[308]); 
  FFV1_0(w[93], w[102], w[4], pars->GC_11, amp[309]); 
  FFV2_0(w[101], w[53], w[2], pars->GC_100, amp[310]); 
  FFV1_0(w[92], w[53], w[4], pars->GC_11, amp[311]); 
  FFV2_0(w[95], w[54], w[2], pars->GC_100, amp[312]); 
  FFV2_0(w[55], w[102], w[3], pars->GC_100, amp[313]); 
  FFV1_0(w[95], w[102], w[4], pars->GC_11, amp[314]); 
  FFV2_0(w[105], w[58], w[2], pars->GC_100, amp[315]); 
  FFV1_0(w[59], w[6], w[8], pars->GC_1, amp[316]); 
  FFV1_0(w[106], w[6], w[46], pars->GC_11, amp[317]); 
  FFV2_3_0(w[59], w[6], w[14], pars->GC_50, pars->GC_58, amp[318]); 
  FFV1_0(w[107], w[6], w[46], pars->GC_11, amp[319]); 
  FFV2_0(w[59], w[86], w[3], pars->GC_100, amp[320]); 
  FFV1_0(w[105], w[86], w[46], pars->GC_11, amp[321]); 
  FFV1_0(w[105], w[96], w[5], pars->GC_11, amp[322]); 
  FFV2_0(w[105], w[62], w[2], pars->GC_100, amp[323]); 
  FFV2_0(w[63], w[96], w[3], pars->GC_100, amp[324]); 
  FFV1_0(w[106], w[32], w[5], pars->GC_11, amp[325]); 
  FFV1_0(w[63], w[32], w[8], pars->GC_1, amp[326]); 
  FFV1_0(w[107], w[32], w[5], pars->GC_11, amp[327]); 
  FFV2_3_0(w[63], w[32], w[14], pars->GC_50, pars->GC_58, amp[328]); 
  FFV1_0(w[64], w[82], w[5], pars->GC_11, amp[329]); 
  FFV1_0(w[63], w[82], w[4], pars->GC_11, amp[330]); 
  FFV1_0(w[64], w[84], w[5], pars->GC_11, amp[331]); 
  FFV1_0(w[63], w[84], w[4], pars->GC_11, amp[332]); 
  FFV1_0(w[64], w[17], w[8], pars->GC_1, amp[333]); 
  FFV1_0(w[106], w[17], w[4], pars->GC_11, amp[334]); 
  FFV2_3_0(w[64], w[17], w[14], pars->GC_50, pars->GC_58, amp[335]); 
  FFV1_0(w[107], w[17], w[4], pars->GC_11, amp[336]); 
  FFV1_0(w[64], w[87], w[5], pars->GC_11, amp[337]); 
  FFV2_0(w[64], w[88], w[3], pars->GC_100, amp[338]); 
  FFV1_0(w[105], w[100], w[5], pars->GC_11, amp[339]); 
  FFV1_0(w[105], w[88], w[4], pars->GC_11, amp[340]); 
  FFV2_0(w[63], w[100], w[3], pars->GC_100, amp[341]); 
  FFV1_0(w[63], w[87], w[4], pars->GC_11, amp[342]); 
  FFV2_0(w[64], w[94], w[3], pars->GC_100, amp[343]); 
  FFV2_0(w[105], w[65], w[2], pars->GC_100, amp[344]); 
  FFV1_0(w[105], w[94], w[4], pars->GC_11, amp[345]); 
  FFV1_0(w[1], w[82], w[66], pars->GC_11, amp[346]); 
  FFV1_0(w[83], w[6], w[66], pars->GC_11, amp[347]); 
  FFV1_0(w[83], w[58], w[0], pars->GC_11, amp[348]); 
  FFV1_0(w[48], w[82], w[0], pars->GC_11, amp[349]); 
  FFV1_0(w[1], w[84], w[66], pars->GC_11, amp[350]); 
  FFV1_0(w[85], w[6], w[66], pars->GC_11, amp[351]); 
  FFV1_0(w[85], w[58], w[0], pars->GC_11, amp[352]); 
  FFV1_0(w[48], w[84], w[0], pars->GC_11, amp[353]); 
  FFV1_0(w[1], w[87], w[66], pars->GC_11, amp[354]); 
  FFV2_0(w[48], w[108], w[3], pars->GC_100, amp[355]); 
  FFV1_0(w[48], w[87], w[0], pars->GC_11, amp[356]); 
  FFV1_0(w[89], w[86], w[66], pars->GC_11, amp[357]); 
  FFV1_0(w[89], w[108], w[46], pars->GC_11, amp[358]); 
  FFV1_0(w[109], w[86], w[46], pars->GC_11, amp[359]); 
  FFV1_0(w[92], w[6], w[66], pars->GC_11, amp[360]); 
  FFV2_0(w[109], w[58], w[2], pars->GC_100, amp[361]); 
  FFV1_0(w[92], w[58], w[0], pars->GC_11, amp[362]); 
  FFV1_0(w[83], w[69], w[5], pars->GC_11, amp[363]); 
  FFV1_0(w[83], w[62], w[0], pars->GC_11, amp[364]); 
  FFV1_0(w[85], w[69], w[5], pars->GC_11, amp[365]); 
  FFV1_0(w[85], w[62], w[0], pars->GC_11, amp[366]); 
  FFV1_0(w[19], w[69], w[8], pars->GC_1, amp[367]); 
  FFV1_0(w[70], w[32], w[8], pars->GC_1, amp[368]); 
  FFV2_3_0(w[19], w[69], w[14], pars->GC_50, pars->GC_58, amp[369]); 
  FFV2_3_0(w[70], w[32], w[14], pars->GC_50, pars->GC_58, amp[370]); 
  FFV1_0(w[92], w[69], w[5], pars->GC_11, amp[371]); 
  FFV2_0(w[93], w[69], w[2], pars->GC_100, amp[372]); 
  FFV1_0(w[109], w[96], w[5], pars->GC_11, amp[373]); 
  FFV2_0(w[109], w[62], w[2], pars->GC_100, amp[374]); 
  FFV1_0(w[93], w[96], w[0], pars->GC_11, amp[375]); 
  FFV1_0(w[92], w[62], w[0], pars->GC_11, amp[376]); 
  FFV2_0(w[95], w[69], w[2], pars->GC_100, amp[377]); 
  FFV2_0(w[70], w[96], w[3], pars->GC_100, amp[378]); 
  FFV1_0(w[95], w[96], w[0], pars->GC_11, amp[379]); 
  FFV1_0(w[71], w[82], w[5], pars->GC_11, amp[380]); 
  FFV1_0(w[52], w[82], w[0], pars->GC_11, amp[381]); 
  FFV1_0(w[71], w[84], w[5], pars->GC_11, amp[382]); 
  FFV1_0(w[52], w[84], w[0], pars->GC_11, amp[383]); 
  FFV1_0(w[71], w[17], w[8], pars->GC_1, amp[384]); 
  FFV1_0(w[37], w[72], w[8], pars->GC_1, amp[385]); 
  FFV2_3_0(w[71], w[17], w[14], pars->GC_50, pars->GC_58, amp[386]); 
  FFV2_3_0(w[37], w[72], w[14], pars->GC_50, pars->GC_58, amp[387]); 
  FFV1_0(w[71], w[87], w[5], pars->GC_11, amp[388]); 
  FFV2_0(w[71], w[88], w[3], pars->GC_100, amp[389]); 
  FFV1_0(w[98], w[108], w[5], pars->GC_11, amp[390]); 
  FFV2_0(w[52], w[108], w[3], pars->GC_100, amp[391]); 
  FFV1_0(w[98], w[88], w[0], pars->GC_11, amp[392]); 
  FFV1_0(w[52], w[87], w[0], pars->GC_11, amp[393]); 
  FFV2_0(w[71], w[94], w[3], pars->GC_100, amp[394]); 
  FFV2_0(w[98], w[72], w[2], pars->GC_100, amp[395]); 
  FFV1_0(w[98], w[94], w[0], pars->GC_11, amp[396]); 
  FFV1_0(w[83], w[72], w[4], pars->GC_11, amp[397]); 
  FFV1_0(w[83], w[65], w[0], pars->GC_11, amp[398]); 
  FFV1_0(w[85], w[72], w[4], pars->GC_11, amp[399]); 
  FFV1_0(w[85], w[65], w[0], pars->GC_11, amp[400]); 
  FFV1_0(w[70], w[82], w[4], pars->GC_11, amp[401]); 
  FFV1_0(w[55], w[82], w[0], pars->GC_11, amp[402]); 
  FFV1_0(w[70], w[84], w[4], pars->GC_11, amp[403]); 
  FFV1_0(w[55], w[84], w[0], pars->GC_11, amp[404]); 
  FFV1_0(w[101], w[108], w[5], pars->GC_11, amp[405]); 
  FFV1_0(w[93], w[108], w[4], pars->GC_11, amp[406]); 
  FFV1_0(w[109], w[100], w[5], pars->GC_11, amp[407]); 
  FFV1_0(w[109], w[88], w[4], pars->GC_11, amp[408]); 
  FFV1_0(w[93], w[100], w[0], pars->GC_11, amp[409]); 
  FFV1_0(w[101], w[88], w[0], pars->GC_11, amp[410]); 
  FFV2_0(w[55], w[108], w[3], pars->GC_100, amp[411]); 
  FFV1_0(w[95], w[108], w[4], pars->GC_11, amp[412]); 
  FFV2_0(w[70], w[100], w[3], pars->GC_100, amp[413]); 
  FFV1_0(w[70], w[87], w[4], pars->GC_11, amp[414]); 
  FFV1_0(w[95], w[100], w[0], pars->GC_11, amp[415]); 
  FFV1_0(w[55], w[87], w[0], pars->GC_11, amp[416]); 
  FFV2_0(w[109], w[65], w[2], pars->GC_100, amp[417]); 
  FFV1_0(w[109], w[94], w[4], pars->GC_11, amp[418]); 
  FFV2_0(w[101], w[72], w[2], pars->GC_100, amp[419]); 
  FFV1_0(w[92], w[72], w[4], pars->GC_11, amp[420]); 
  FFV1_0(w[101], w[94], w[0], pars->GC_11, amp[421]); 
  FFV1_0(w[92], w[65], w[0], pars->GC_11, amp[422]); 
  FFV1_0(w[1], w[76], w[8], pars->GC_1, amp[423]); 
  FFV1_0(w[1], w[77], w[8], pars->GC_1, amp[424]); 
  FFV1_0(w[1], w[78], w[8], pars->GC_1, amp[425]); 
  FFV1_0(w[79], w[6], w[8], pars->GC_1, amp[426]); 
  FFV1_0(w[80], w[6], w[8], pars->GC_1, amp[427]); 
  FFV1_0(w[81], w[6], w[8], pars->GC_1, amp[428]); 
  FFV2_3_0(w[1], w[76], w[14], pars->GC_50, pars->GC_58, amp[429]); 
  FFV2_3_0(w[1], w[77], w[14], pars->GC_50, pars->GC_58, amp[430]); 
  FFV2_3_0(w[1], w[78], w[14], pars->GC_50, pars->GC_58, amp[431]); 
  FFV2_3_0(w[79], w[6], w[14], pars->GC_50, pars->GC_58, amp[432]); 
  FFV2_3_0(w[80], w[6], w[14], pars->GC_50, pars->GC_58, amp[433]); 
  FFV2_3_0(w[81], w[6], w[14], pars->GC_50, pars->GC_58, amp[434]); 
  FFV2_0(w[79], w[86], w[3], pars->GC_100, amp[435]); 
  FFV2_0(w[80], w[86], w[3], pars->GC_100, amp[436]); 
  FFV2_0(w[81], w[86], w[3], pars->GC_100, amp[437]); 
  FFV1_0(w[89], w[86], w[73], pars->GC_11, amp[438]); 
  FFV1_0(w[89], w[86], w[74], pars->GC_11, amp[439]); 
  FFV1_0(w[89], w[86], w[75], pars->GC_11, amp[440]); 
  FFV2_0(w[89], w[76], w[2], pars->GC_100, amp[441]); 
  FFV2_0(w[89], w[77], w[2], pars->GC_100, amp[442]); 
  FFV2_0(w[89], w[78], w[2], pars->GC_100, amp[443]); 
  FFV1_0(w[111], w[112], w[9], pars->GC_11, amp[444]); 
  FFV1_0(w[113], w[110], w[9], pars->GC_11, amp[445]); 
  FFV1_0(w[113], w[114], w[5], pars->GC_11, amp[446]); 
  FFV1_0(w[115], w[112], w[5], pars->GC_11, amp[447]); 
  FFV1_0(w[111], w[116], w[9], pars->GC_11, amp[448]); 
  FFV1_0(w[117], w[110], w[9], pars->GC_11, amp[449]); 
  FFV1_0(w[117], w[114], w[5], pars->GC_11, amp[450]); 
  FFV1_0(w[115], w[116], w[5], pars->GC_11, amp[451]); 
  FFV1_0(w[111], w[119], w[8], pars->GC_2, amp[452]); 
  FFV1_0(w[115], w[118], w[8], pars->GC_2, amp[453]); 
  FFV2_5_0(w[111], w[119], w[14], pars->GC_51, pars->GC_58, amp[454]); 
  FFV2_5_0(w[115], w[118], w[14], pars->GC_51, pars->GC_58, amp[455]); 
  FFV1_0(w[121], w[110], w[8], pars->GC_2, amp[456]); 
  FFV1_0(w[120], w[114], w[8], pars->GC_2, amp[457]); 
  FFV2_5_0(w[121], w[110], w[14], pars->GC_51, pars->GC_58, amp[458]); 
  FFV2_5_0(w[120], w[114], w[14], pars->GC_51, pars->GC_58, amp[459]); 
  FFV1_0(w[123], w[110], w[9], pars->GC_11, amp[460]); 
  FFV1_0(w[123], w[114], w[5], pars->GC_11, amp[461]); 
  FFV2_0(w[124], w[114], w[3], pars->GC_100, amp[462]); 
  FFV1_0(w[126], w[125], w[5], pars->GC_11, amp[463]); 
  FFV1_0(w[122], w[127], w[5], pars->GC_11, amp[464]); 
  FFV1_0(w[122], w[125], w[9], pars->GC_11, amp[465]); 
  FFV2_0(w[126], w[118], w[3], pars->GC_100, amp[466]); 
  FFV2_0(w[122], w[119], w[3], pars->GC_100, amp[467]); 
  FFV1_0(w[111], w[128], w[9], pars->GC_11, amp[468]); 
  FFV1_0(w[115], w[128], w[5], pars->GC_11, amp[469]); 
  FFV2_0(w[115], w[129], w[2], pars->GC_100, amp[470]); 
  FFV2_0(w[120], w[127], w[2], pars->GC_100, amp[471]); 
  FFV2_0(w[121], w[125], w[2], pars->GC_100, amp[472]); 
  FFV2_0(w[115], w[130], w[2], pars->GC_100, amp[473]); 
  FFV2_0(w[131], w[114], w[3], pars->GC_100, amp[474]); 
  FFV2_0(w[133], w[134], w[2], pars->GC_100, amp[475]); 
  FFV1_0(w[111], w[135], w[8], pars->GC_2, amp[476]); 
  FFV1_0(w[133], w[132], w[8], pars->GC_2, amp[477]); 
  FFV2_5_0(w[111], w[135], w[14], pars->GC_51, pars->GC_58, amp[478]); 
  FFV2_5_0(w[133], w[132], w[14], pars->GC_51, pars->GC_58, amp[479]); 
  FFV2_0(w[122], w[135], w[3], pars->GC_100, amp[480]); 
  FFV2_0(w[136], w[132], w[3], pars->GC_100, amp[481]); 
  FFV2_0(w[139], w[138], w[3], pars->GC_100, amp[482]); 
  FFV1_0(w[140], w[110], w[8], pars->GC_2, amp[483]); 
  FFV1_0(w[137], w[138], w[8], pars->GC_2, amp[484]); 
  FFV2_5_0(w[140], w[110], w[14], pars->GC_51, pars->GC_58, amp[485]); 
  FFV2_5_0(w[137], w[138], w[14], pars->GC_51, pars->GC_58, amp[486]); 
  FFV2_0(w[140], w[125], w[2], pars->GC_100, amp[487]); 
  FFV2_0(w[137], w[141], w[2], pars->GC_100, amp[488]); 
  FFV1_0(w[111], w[112], w[42], pars->GC_11, amp[489]); 
  FFV1_0(w[113], w[110], w[42], pars->GC_11, amp[490]); 
  FFV1_0(w[113], w[138], w[4], pars->GC_11, amp[491]); 
  FFV1_0(w[133], w[112], w[4], pars->GC_11, amp[492]); 
  FFV1_0(w[111], w[116], w[42], pars->GC_11, amp[493]); 
  FFV1_0(w[117], w[110], w[42], pars->GC_11, amp[494]); 
  FFV1_0(w[117], w[138], w[4], pars->GC_11, amp[495]); 
  FFV1_0(w[133], w[116], w[4], pars->GC_11, amp[496]); 
  FFV1_0(w[123], w[110], w[42], pars->GC_11, amp[497]); 
  FFV2_0(w[142], w[138], w[3], pars->GC_100, amp[498]); 
  FFV1_0(w[123], w[138], w[4], pars->GC_11, amp[499]); 
  FFV1_0(w[122], w[125], w[42], pars->GC_11, amp[500]); 
  FFV1_0(w[136], w[125], w[4], pars->GC_11, amp[501]); 
  FFV1_0(w[122], w[141], w[4], pars->GC_11, amp[502]); 
  FFV1_0(w[111], w[128], w[42], pars->GC_11, amp[503]); 
  FFV2_0(w[133], w[143], w[2], pars->GC_100, amp[504]); 
  FFV1_0(w[133], w[128], w[4], pars->GC_11, amp[505]); 
  FFV2_0(w[146], w[145], w[2], pars->GC_100, amp[506]); 
  FFV1_0(w[111], w[147], w[8], pars->GC_2, amp[507]); 
  FFV1_0(w[111], w[148], w[46], pars->GC_11, amp[508]); 
  FFV2_5_0(w[111], w[147], w[14], pars->GC_51, pars->GC_58, amp[509]); 
  FFV1_0(w[111], w[149], w[46], pars->GC_11, amp[510]); 
  FFV2_0(w[122], w[147], w[3], pars->GC_100, amp[511]); 
  FFV1_0(w[122], w[145], w[46], pars->GC_11, amp[512]); 
  FFV1_0(w[139], w[145], w[5], pars->GC_11, amp[513]); 
  FFV2_0(w[150], w[145], w[2], pars->GC_100, amp[514]); 
  FFV2_0(w[139], w[151], w[3], pars->GC_100, amp[515]); 
  FFV1_0(w[137], w[148], w[5], pars->GC_11, amp[516]); 
  FFV1_0(w[137], w[151], w[8], pars->GC_2, amp[517]); 
  FFV1_0(w[137], w[149], w[5], pars->GC_11, amp[518]); 
  FFV2_5_0(w[137], w[151], w[14], pars->GC_51, pars->GC_58, amp[519]); 
  FFV1_0(w[113], w[152], w[5], pars->GC_11, amp[520]); 
  FFV1_0(w[113], w[151], w[4], pars->GC_11, amp[521]); 
  FFV1_0(w[117], w[152], w[5], pars->GC_11, amp[522]); 
  FFV1_0(w[117], w[151], w[4], pars->GC_11, amp[523]); 
  FFV1_0(w[120], w[152], w[8], pars->GC_2, amp[524]); 
  FFV1_0(w[120], w[148], w[4], pars->GC_11, amp[525]); 
  FFV2_5_0(w[120], w[152], w[14], pars->GC_51, pars->GC_58, amp[526]); 
  FFV1_0(w[120], w[149], w[4], pars->GC_11, amp[527]); 
  FFV1_0(w[123], w[152], w[5], pars->GC_11, amp[528]); 
  FFV2_0(w[124], w[152], w[3], pars->GC_100, amp[529]); 
  FFV1_0(w[142], w[145], w[5], pars->GC_11, amp[530]); 
  FFV1_0(w[124], w[145], w[4], pars->GC_11, amp[531]); 
  FFV2_0(w[142], w[151], w[3], pars->GC_100, amp[532]); 
  FFV1_0(w[123], w[151], w[4], pars->GC_11, amp[533]); 
  FFV2_0(w[131], w[152], w[3], pars->GC_100, amp[534]); 
  FFV2_0(w[153], w[145], w[2], pars->GC_100, amp[535]); 
  FFV1_0(w[131], w[145], w[4], pars->GC_11, amp[536]); 
  FFV2_0(w[155], w[156], w[3], pars->GC_100, amp[537]); 
  FFV1_0(w[157], w[110], w[8], pars->GC_2, amp[538]); 
  FFV1_0(w[158], w[110], w[46], pars->GC_11, amp[539]); 
  FFV2_5_0(w[157], w[110], w[14], pars->GC_51, pars->GC_58, amp[540]); 
  FFV1_0(w[159], w[110], w[46], pars->GC_11, amp[541]); 
  FFV2_0(w[157], w[125], w[2], pars->GC_100, amp[542]); 
  FFV1_0(w[155], w[125], w[46], pars->GC_11, amp[543]); 
  FFV1_0(w[155], w[134], w[5], pars->GC_11, amp[544]); 
  FFV2_0(w[155], w[160], w[3], pars->GC_100, amp[545]); 
  FFV2_0(w[161], w[134], w[2], pars->GC_100, amp[546]); 
  FFV1_0(w[158], w[132], w[5], pars->GC_11, amp[547]); 
  FFV1_0(w[161], w[132], w[8], pars->GC_2, amp[548]); 
  FFV1_0(w[159], w[132], w[5], pars->GC_11, amp[549]); 
  FFV2_5_0(w[161], w[132], w[14], pars->GC_51, pars->GC_58, amp[550]); 
  FFV1_0(w[162], w[112], w[5], pars->GC_11, amp[551]); 
  FFV1_0(w[161], w[112], w[4], pars->GC_11, amp[552]); 
  FFV1_0(w[162], w[116], w[5], pars->GC_11, amp[553]); 
  FFV1_0(w[161], w[116], w[4], pars->GC_11, amp[554]); 
  FFV1_0(w[162], w[118], w[8], pars->GC_2, amp[555]); 
  FFV1_0(w[158], w[118], w[4], pars->GC_11, amp[556]); 
  FFV2_5_0(w[162], w[118], w[14], pars->GC_51, pars->GC_58, amp[557]); 
  FFV1_0(w[159], w[118], w[4], pars->GC_11, amp[558]); 
  FFV1_0(w[162], w[128], w[5], pars->GC_11, amp[559]); 
  FFV2_0(w[162], w[129], w[2], pars->GC_100, amp[560]); 
  FFV1_0(w[155], w[143], w[5], pars->GC_11, amp[561]); 
  FFV1_0(w[155], w[129], w[4], pars->GC_11, amp[562]); 
  FFV2_0(w[161], w[143], w[2], pars->GC_100, amp[563]); 
  FFV1_0(w[161], w[128], w[4], pars->GC_11, amp[564]); 
  FFV2_0(w[162], w[130], w[2], pars->GC_100, amp[565]); 
  FFV2_0(w[155], w[163], w[3], pars->GC_100, amp[566]); 
  FFV1_0(w[155], w[130], w[4], pars->GC_11, amp[567]); 
  FFV1_0(w[111], w[112], w[66], pars->GC_11, amp[568]); 
  FFV1_0(w[113], w[110], w[66], pars->GC_11, amp[569]); 
  FFV1_0(w[113], w[156], w[0], pars->GC_11, amp[570]); 
  FFV1_0(w[146], w[112], w[0], pars->GC_11, amp[571]); 
  FFV1_0(w[111], w[116], w[66], pars->GC_11, amp[572]); 
  FFV1_0(w[117], w[110], w[66], pars->GC_11, amp[573]); 
  FFV1_0(w[117], w[156], w[0], pars->GC_11, amp[574]); 
  FFV1_0(w[146], w[116], w[0], pars->GC_11, amp[575]); 
  FFV1_0(w[123], w[110], w[66], pars->GC_11, amp[576]); 
  FFV2_0(w[164], w[156], w[3], pars->GC_100, amp[577]); 
  FFV1_0(w[123], w[156], w[0], pars->GC_11, amp[578]); 
  FFV1_0(w[122], w[125], w[66], pars->GC_11, amp[579]); 
  FFV1_0(w[164], w[125], w[46], pars->GC_11, amp[580]); 
  FFV1_0(w[122], w[165], w[46], pars->GC_11, amp[581]); 
  FFV1_0(w[111], w[128], w[66], pars->GC_11, amp[582]); 
  FFV2_0(w[146], w[165], w[2], pars->GC_100, amp[583]); 
  FFV1_0(w[146], w[128], w[0], pars->GC_11, amp[584]); 
  FFV1_0(w[113], w[166], w[5], pars->GC_11, amp[585]); 
  FFV1_0(w[113], w[160], w[0], pars->GC_11, amp[586]); 
  FFV1_0(w[117], w[166], w[5], pars->GC_11, amp[587]); 
  FFV1_0(w[117], w[160], w[0], pars->GC_11, amp[588]); 
  FFV1_0(w[120], w[166], w[8], pars->GC_2, amp[589]); 
  FFV1_0(w[167], w[132], w[8], pars->GC_2, amp[590]); 
  FFV2_5_0(w[120], w[166], w[14], pars->GC_51, pars->GC_58, amp[591]); 
  FFV2_5_0(w[167], w[132], w[14], pars->GC_51, pars->GC_58, amp[592]); 
  FFV1_0(w[123], w[166], w[5], pars->GC_11, amp[593]); 
  FFV2_0(w[124], w[166], w[3], pars->GC_100, amp[594]); 
  FFV1_0(w[164], w[134], w[5], pars->GC_11, amp[595]); 
  FFV2_0(w[164], w[160], w[3], pars->GC_100, amp[596]); 
  FFV1_0(w[124], w[134], w[0], pars->GC_11, amp[597]); 
  FFV1_0(w[123], w[160], w[0], pars->GC_11, amp[598]); 
  FFV2_0(w[131], w[166], w[3], pars->GC_100, amp[599]); 
  FFV2_0(w[167], w[134], w[2], pars->GC_100, amp[600]); 
  FFV1_0(w[131], w[134], w[0], pars->GC_11, amp[601]); 
  FFV1_0(w[168], w[112], w[5], pars->GC_11, amp[602]); 
  FFV1_0(w[150], w[112], w[0], pars->GC_11, amp[603]); 
  FFV1_0(w[168], w[116], w[5], pars->GC_11, amp[604]); 
  FFV1_0(w[150], w[116], w[0], pars->GC_11, amp[605]); 
  FFV1_0(w[168], w[118], w[8], pars->GC_2, amp[606]); 
  FFV1_0(w[137], w[169], w[8], pars->GC_2, amp[607]); 
  FFV2_5_0(w[168], w[118], w[14], pars->GC_51, pars->GC_58, amp[608]); 
  FFV2_5_0(w[137], w[169], w[14], pars->GC_51, pars->GC_58, amp[609]); 
  FFV1_0(w[168], w[128], w[5], pars->GC_11, amp[610]); 
  FFV2_0(w[168], w[129], w[2], pars->GC_100, amp[611]); 
  FFV1_0(w[139], w[165], w[5], pars->GC_11, amp[612]); 
  FFV2_0(w[150], w[165], w[2], pars->GC_100, amp[613]); 
  FFV1_0(w[139], w[129], w[0], pars->GC_11, amp[614]); 
  FFV1_0(w[150], w[128], w[0], pars->GC_11, amp[615]); 
  FFV2_0(w[168], w[130], w[2], pars->GC_100, amp[616]); 
  FFV2_0(w[139], w[169], w[3], pars->GC_100, amp[617]); 
  FFV1_0(w[139], w[130], w[0], pars->GC_11, amp[618]); 
  FFV1_0(w[113], w[169], w[4], pars->GC_11, amp[619]); 
  FFV1_0(w[113], w[163], w[0], pars->GC_11, amp[620]); 
  FFV1_0(w[117], w[169], w[4], pars->GC_11, amp[621]); 
  FFV1_0(w[117], w[163], w[0], pars->GC_11, amp[622]); 
  FFV1_0(w[167], w[112], w[4], pars->GC_11, amp[623]); 
  FFV1_0(w[153], w[112], w[0], pars->GC_11, amp[624]); 
  FFV1_0(w[167], w[116], w[4], pars->GC_11, amp[625]); 
  FFV1_0(w[153], w[116], w[0], pars->GC_11, amp[626]); 
  FFV1_0(w[164], w[143], w[5], pars->GC_11, amp[627]); 
  FFV1_0(w[164], w[129], w[4], pars->GC_11, amp[628]); 
  FFV1_0(w[142], w[165], w[5], pars->GC_11, amp[629]); 
  FFV1_0(w[124], w[165], w[4], pars->GC_11, amp[630]); 
  FFV1_0(w[142], w[129], w[0], pars->GC_11, amp[631]); 
  FFV1_0(w[124], w[143], w[0], pars->GC_11, amp[632]); 
  FFV2_0(w[164], w[163], w[3], pars->GC_100, amp[633]); 
  FFV1_0(w[164], w[130], w[4], pars->GC_11, amp[634]); 
  FFV2_0(w[142], w[169], w[3], pars->GC_100, amp[635]); 
  FFV1_0(w[123], w[169], w[4], pars->GC_11, amp[636]); 
  FFV1_0(w[142], w[130], w[0], pars->GC_11, amp[637]); 
  FFV1_0(w[123], w[163], w[0], pars->GC_11, amp[638]); 
  FFV2_0(w[153], w[165], w[2], pars->GC_100, amp[639]); 
  FFV1_0(w[131], w[165], w[4], pars->GC_11, amp[640]); 
  FFV2_0(w[167], w[143], w[2], pars->GC_100, amp[641]); 
  FFV1_0(w[167], w[128], w[4], pars->GC_11, amp[642]); 
  FFV1_0(w[131], w[143], w[0], pars->GC_11, amp[643]); 
  FFV1_0(w[153], w[128], w[0], pars->GC_11, amp[644]); 
  FFV1_0(w[111], w[170], w[8], pars->GC_2, amp[645]); 
  FFV1_0(w[111], w[171], w[8], pars->GC_2, amp[646]); 
  FFV1_0(w[111], w[172], w[8], pars->GC_2, amp[647]); 
  FFV1_0(w[173], w[110], w[8], pars->GC_2, amp[648]); 
  FFV1_0(w[174], w[110], w[8], pars->GC_2, amp[649]); 
  FFV1_0(w[175], w[110], w[8], pars->GC_2, amp[650]); 
  FFV2_5_0(w[111], w[170], w[14], pars->GC_51, pars->GC_58, amp[651]); 
  FFV2_5_0(w[111], w[171], w[14], pars->GC_51, pars->GC_58, amp[652]); 
  FFV2_5_0(w[111], w[172], w[14], pars->GC_51, pars->GC_58, amp[653]); 
  FFV2_5_0(w[173], w[110], w[14], pars->GC_51, pars->GC_58, amp[654]); 
  FFV2_5_0(w[174], w[110], w[14], pars->GC_51, pars->GC_58, amp[655]); 
  FFV2_5_0(w[175], w[110], w[14], pars->GC_51, pars->GC_58, amp[656]); 
  FFV2_0(w[122], w[170], w[3], pars->GC_100, amp[657]); 
  FFV2_0(w[122], w[171], w[3], pars->GC_100, amp[658]); 
  FFV2_0(w[122], w[172], w[3], pars->GC_100, amp[659]); 
  FFV1_0(w[122], w[125], w[73], pars->GC_11, amp[660]); 
  FFV1_0(w[122], w[125], w[74], pars->GC_11, amp[661]); 
  FFV1_0(w[122], w[125], w[75], pars->GC_11, amp[662]); 
  FFV2_0(w[173], w[125], w[2], pars->GC_100, amp[663]); 
  FFV2_0(w[174], w[125], w[2], pars->GC_100, amp[664]); 
  FFV2_0(w[175], w[125], w[2], pars->GC_100, amp[665]); 
  FFV1_0(w[111], w[176], w[9], pars->GC_11, amp[666]); 
  FFV1_0(w[177], w[110], w[9], pars->GC_11, amp[667]); 
  FFV1_0(w[177], w[114], w[5], pars->GC_11, amp[668]); 
  FFV1_0(w[115], w[176], w[5], pars->GC_11, amp[669]); 
  FFV1_0(w[111], w[178], w[9], pars->GC_11, amp[670]); 
  FFV1_0(w[179], w[110], w[9], pars->GC_11, amp[671]); 
  FFV1_0(w[179], w[114], w[5], pars->GC_11, amp[672]); 
  FFV1_0(w[115], w[178], w[5], pars->GC_11, amp[673]); 
  FFV1_0(w[111], w[119], w[8], pars->GC_1, amp[674]); 
  FFV1_0(w[115], w[118], w[8], pars->GC_1, amp[675]); 
  FFV2_3_0(w[111], w[119], w[14], pars->GC_50, pars->GC_58, amp[676]); 
  FFV2_3_0(w[115], w[118], w[14], pars->GC_50, pars->GC_58, amp[677]); 
  FFV1_0(w[121], w[110], w[8], pars->GC_1, amp[678]); 
  FFV1_0(w[120], w[114], w[8], pars->GC_1, amp[679]); 
  FFV2_3_0(w[121], w[110], w[14], pars->GC_50, pars->GC_58, amp[680]); 
  FFV2_3_0(w[120], w[114], w[14], pars->GC_50, pars->GC_58, amp[681]); 
  FFV1_0(w[111], w[181], w[9], pars->GC_11, amp[682]); 
  FFV1_0(w[115], w[181], w[5], pars->GC_11, amp[683]); 
  FFV2_0(w[115], w[182], w[3], pars->GC_100, amp[684]); 
  FFV1_0(w[183], w[184], w[5], pars->GC_11, amp[685]); 
  FFV1_0(w[185], w[180], w[5], pars->GC_11, amp[686]); 
  FFV1_0(w[183], w[180], w[9], pars->GC_11, amp[687]); 
  FFV2_0(w[120], w[184], w[3], pars->GC_100, amp[688]); 
  FFV2_0(w[121], w[180], w[3], pars->GC_100, amp[689]); 
  FFV1_0(w[186], w[110], w[9], pars->GC_11, amp[690]); 
  FFV1_0(w[186], w[114], w[5], pars->GC_11, amp[691]); 
  FFV2_0(w[187], w[114], w[2], pars->GC_100, amp[692]); 
  FFV2_0(w[185], w[118], w[2], pars->GC_100, amp[693]); 
  FFV2_0(w[183], w[119], w[2], pars->GC_100, amp[694]); 
  FFV2_0(w[115], w[188], w[3], pars->GC_100, amp[695]); 
  FFV2_0(w[189], w[114], w[2], pars->GC_100, amp[696]); 
  FFV2_0(w[133], w[190], w[3], pars->GC_100, amp[697]); 
  FFV1_0(w[111], w[135], w[8], pars->GC_1, amp[698]); 
  FFV1_0(w[133], w[132], w[8], pars->GC_1, amp[699]); 
  FFV2_3_0(w[111], w[135], w[14], pars->GC_50, pars->GC_58, amp[700]); 
  FFV2_3_0(w[133], w[132], w[14], pars->GC_50, pars->GC_58, amp[701]); 
  FFV2_0(w[183], w[135], w[2], pars->GC_100, amp[702]); 
  FFV2_0(w[191], w[132], w[2], pars->GC_100, amp[703]); 
  FFV2_0(w[192], w[138], w[2], pars->GC_100, amp[704]); 
  FFV1_0(w[140], w[110], w[8], pars->GC_1, amp[705]); 
  FFV1_0(w[137], w[138], w[8], pars->GC_1, amp[706]); 
  FFV2_3_0(w[140], w[110], w[14], pars->GC_50, pars->GC_58, amp[707]); 
  FFV2_3_0(w[137], w[138], w[14], pars->GC_50, pars->GC_58, amp[708]); 
  FFV2_0(w[140], w[180], w[3], pars->GC_100, amp[709]); 
  FFV2_0(w[137], w[193], w[3], pars->GC_100, amp[710]); 
  FFV1_0(w[111], w[176], w[42], pars->GC_11, amp[711]); 
  FFV1_0(w[177], w[110], w[42], pars->GC_11, amp[712]); 
  FFV1_0(w[177], w[138], w[4], pars->GC_11, amp[713]); 
  FFV1_0(w[133], w[176], w[4], pars->GC_11, amp[714]); 
  FFV1_0(w[111], w[178], w[42], pars->GC_11, amp[715]); 
  FFV1_0(w[179], w[110], w[42], pars->GC_11, amp[716]); 
  FFV1_0(w[179], w[138], w[4], pars->GC_11, amp[717]); 
  FFV1_0(w[133], w[178], w[4], pars->GC_11, amp[718]); 
  FFV1_0(w[111], w[181], w[42], pars->GC_11, amp[719]); 
  FFV2_0(w[133], w[194], w[3], pars->GC_100, amp[720]); 
  FFV1_0(w[133], w[181], w[4], pars->GC_11, amp[721]); 
  FFV1_0(w[183], w[180], w[42], pars->GC_11, amp[722]); 
  FFV1_0(w[183], w[193], w[4], pars->GC_11, amp[723]); 
  FFV1_0(w[191], w[180], w[4], pars->GC_11, amp[724]); 
  FFV1_0(w[186], w[110], w[42], pars->GC_11, amp[725]); 
  FFV2_0(w[195], w[138], w[2], pars->GC_100, amp[726]); 
  FFV1_0(w[186], w[138], w[4], pars->GC_11, amp[727]); 
  FFV2_0(w[146], w[196], w[3], pars->GC_100, amp[728]); 
  FFV1_0(w[111], w[147], w[8], pars->GC_1, amp[729]); 
  FFV1_0(w[111], w[197], w[46], pars->GC_11, amp[730]); 
  FFV2_3_0(w[111], w[147], w[14], pars->GC_50, pars->GC_58, amp[731]); 
  FFV1_0(w[111], w[198], w[46], pars->GC_11, amp[732]); 
  FFV2_0(w[183], w[147], w[2], pars->GC_100, amp[733]); 
  FFV1_0(w[183], w[196], w[46], pars->GC_11, amp[734]); 
  FFV1_0(w[192], w[196], w[5], pars->GC_11, amp[735]); 
  FFV2_0(w[150], w[196], w[3], pars->GC_100, amp[736]); 
  FFV2_0(w[192], w[151], w[2], pars->GC_100, amp[737]); 
  FFV1_0(w[137], w[197], w[5], pars->GC_11, amp[738]); 
  FFV1_0(w[137], w[151], w[8], pars->GC_1, amp[739]); 
  FFV1_0(w[137], w[198], w[5], pars->GC_11, amp[740]); 
  FFV2_3_0(w[137], w[151], w[14], pars->GC_50, pars->GC_58, amp[741]); 
  FFV1_0(w[177], w[152], w[5], pars->GC_11, amp[742]); 
  FFV1_0(w[177], w[151], w[4], pars->GC_11, amp[743]); 
  FFV1_0(w[179], w[152], w[5], pars->GC_11, amp[744]); 
  FFV1_0(w[179], w[151], w[4], pars->GC_11, amp[745]); 
  FFV1_0(w[120], w[152], w[8], pars->GC_1, amp[746]); 
  FFV1_0(w[120], w[197], w[4], pars->GC_11, amp[747]); 
  FFV2_3_0(w[120], w[152], w[14], pars->GC_50, pars->GC_58, amp[748]); 
  FFV1_0(w[120], w[198], w[4], pars->GC_11, amp[749]); 
  FFV1_0(w[186], w[152], w[5], pars->GC_11, amp[750]); 
  FFV2_0(w[187], w[152], w[2], pars->GC_100, amp[751]); 
  FFV1_0(w[195], w[196], w[5], pars->GC_11, amp[752]); 
  FFV1_0(w[187], w[196], w[4], pars->GC_11, amp[753]); 
  FFV2_0(w[195], w[151], w[2], pars->GC_100, amp[754]); 
  FFV1_0(w[186], w[151], w[4], pars->GC_11, amp[755]); 
  FFV2_0(w[189], w[152], w[2], pars->GC_100, amp[756]); 
  FFV2_0(w[153], w[196], w[3], pars->GC_100, amp[757]); 
  FFV1_0(w[189], w[196], w[4], pars->GC_11, amp[758]); 
  FFV2_0(w[199], w[156], w[2], pars->GC_100, amp[759]); 
  FFV1_0(w[157], w[110], w[8], pars->GC_1, amp[760]); 
  FFV1_0(w[200], w[110], w[46], pars->GC_11, amp[761]); 
  FFV2_3_0(w[157], w[110], w[14], pars->GC_50, pars->GC_58, amp[762]); 
  FFV1_0(w[201], w[110], w[46], pars->GC_11, amp[763]); 
  FFV2_0(w[157], w[180], w[3], pars->GC_100, amp[764]); 
  FFV1_0(w[199], w[180], w[46], pars->GC_11, amp[765]); 
  FFV1_0(w[199], w[190], w[5], pars->GC_11, amp[766]); 
  FFV2_0(w[199], w[160], w[2], pars->GC_100, amp[767]); 
  FFV2_0(w[161], w[190], w[3], pars->GC_100, amp[768]); 
  FFV1_0(w[200], w[132], w[5], pars->GC_11, amp[769]); 
  FFV1_0(w[161], w[132], w[8], pars->GC_1, amp[770]); 
  FFV1_0(w[201], w[132], w[5], pars->GC_11, amp[771]); 
  FFV2_3_0(w[161], w[132], w[14], pars->GC_50, pars->GC_58, amp[772]); 
  FFV1_0(w[162], w[176], w[5], pars->GC_11, amp[773]); 
  FFV1_0(w[161], w[176], w[4], pars->GC_11, amp[774]); 
  FFV1_0(w[162], w[178], w[5], pars->GC_11, amp[775]); 
  FFV1_0(w[161], w[178], w[4], pars->GC_11, amp[776]); 
  FFV1_0(w[162], w[118], w[8], pars->GC_1, amp[777]); 
  FFV1_0(w[200], w[118], w[4], pars->GC_11, amp[778]); 
  FFV2_3_0(w[162], w[118], w[14], pars->GC_50, pars->GC_58, amp[779]); 
  FFV1_0(w[201], w[118], w[4], pars->GC_11, amp[780]); 
  FFV1_0(w[162], w[181], w[5], pars->GC_11, amp[781]); 
  FFV2_0(w[162], w[182], w[3], pars->GC_100, amp[782]); 
  FFV1_0(w[199], w[194], w[5], pars->GC_11, amp[783]); 
  FFV1_0(w[199], w[182], w[4], pars->GC_11, amp[784]); 
  FFV2_0(w[161], w[194], w[3], pars->GC_100, amp[785]); 
  FFV1_0(w[161], w[181], w[4], pars->GC_11, amp[786]); 
  FFV2_0(w[162], w[188], w[3], pars->GC_100, amp[787]); 
  FFV2_0(w[199], w[163], w[2], pars->GC_100, amp[788]); 
  FFV1_0(w[199], w[188], w[4], pars->GC_11, amp[789]); 
  FFV1_0(w[111], w[176], w[66], pars->GC_11, amp[790]); 
  FFV1_0(w[177], w[110], w[66], pars->GC_11, amp[791]); 
  FFV1_0(w[177], w[156], w[0], pars->GC_11, amp[792]); 
  FFV1_0(w[146], w[176], w[0], pars->GC_11, amp[793]); 
  FFV1_0(w[111], w[178], w[66], pars->GC_11, amp[794]); 
  FFV1_0(w[179], w[110], w[66], pars->GC_11, amp[795]); 
  FFV1_0(w[179], w[156], w[0], pars->GC_11, amp[796]); 
  FFV1_0(w[146], w[178], w[0], pars->GC_11, amp[797]); 
  FFV1_0(w[111], w[181], w[66], pars->GC_11, amp[798]); 
  FFV2_0(w[146], w[202], w[3], pars->GC_100, amp[799]); 
  FFV1_0(w[146], w[181], w[0], pars->GC_11, amp[800]); 
  FFV1_0(w[183], w[180], w[66], pars->GC_11, amp[801]); 
  FFV1_0(w[183], w[202], w[46], pars->GC_11, amp[802]); 
  FFV1_0(w[203], w[180], w[46], pars->GC_11, amp[803]); 
  FFV1_0(w[186], w[110], w[66], pars->GC_11, amp[804]); 
  FFV2_0(w[203], w[156], w[2], pars->GC_100, amp[805]); 
  FFV1_0(w[186], w[156], w[0], pars->GC_11, amp[806]); 
  FFV1_0(w[177], w[166], w[5], pars->GC_11, amp[807]); 
  FFV1_0(w[177], w[160], w[0], pars->GC_11, amp[808]); 
  FFV1_0(w[179], w[166], w[5], pars->GC_11, amp[809]); 
  FFV1_0(w[179], w[160], w[0], pars->GC_11, amp[810]); 
  FFV1_0(w[120], w[166], w[8], pars->GC_1, amp[811]); 
  FFV1_0(w[167], w[132], w[8], pars->GC_1, amp[812]); 
  FFV2_3_0(w[120], w[166], w[14], pars->GC_50, pars->GC_58, amp[813]); 
  FFV2_3_0(w[167], w[132], w[14], pars->GC_50, pars->GC_58, amp[814]); 
  FFV1_0(w[186], w[166], w[5], pars->GC_11, amp[815]); 
  FFV2_0(w[187], w[166], w[2], pars->GC_100, amp[816]); 
  FFV1_0(w[203], w[190], w[5], pars->GC_11, amp[817]); 
  FFV2_0(w[203], w[160], w[2], pars->GC_100, amp[818]); 
  FFV1_0(w[187], w[190], w[0], pars->GC_11, amp[819]); 
  FFV1_0(w[186], w[160], w[0], pars->GC_11, amp[820]); 
  FFV2_0(w[189], w[166], w[2], pars->GC_100, amp[821]); 
  FFV2_0(w[167], w[190], w[3], pars->GC_100, amp[822]); 
  FFV1_0(w[189], w[190], w[0], pars->GC_11, amp[823]); 
  FFV1_0(w[168], w[176], w[5], pars->GC_11, amp[824]); 
  FFV1_0(w[150], w[176], w[0], pars->GC_11, amp[825]); 
  FFV1_0(w[168], w[178], w[5], pars->GC_11, amp[826]); 
  FFV1_0(w[150], w[178], w[0], pars->GC_11, amp[827]); 
  FFV1_0(w[168], w[118], w[8], pars->GC_1, amp[828]); 
  FFV1_0(w[137], w[169], w[8], pars->GC_1, amp[829]); 
  FFV2_3_0(w[168], w[118], w[14], pars->GC_50, pars->GC_58, amp[830]); 
  FFV2_3_0(w[137], w[169], w[14], pars->GC_50, pars->GC_58, amp[831]); 
  FFV1_0(w[168], w[181], w[5], pars->GC_11, amp[832]); 
  FFV2_0(w[168], w[182], w[3], pars->GC_100, amp[833]); 
  FFV1_0(w[192], w[202], w[5], pars->GC_11, amp[834]); 
  FFV2_0(w[150], w[202], w[3], pars->GC_100, amp[835]); 
  FFV1_0(w[192], w[182], w[0], pars->GC_11, amp[836]); 
  FFV1_0(w[150], w[181], w[0], pars->GC_11, amp[837]); 
  FFV2_0(w[168], w[188], w[3], pars->GC_100, amp[838]); 
  FFV2_0(w[192], w[169], w[2], pars->GC_100, amp[839]); 
  FFV1_0(w[192], w[188], w[0], pars->GC_11, amp[840]); 
  FFV1_0(w[177], w[169], w[4], pars->GC_11, amp[841]); 
  FFV1_0(w[177], w[163], w[0], pars->GC_11, amp[842]); 
  FFV1_0(w[179], w[169], w[4], pars->GC_11, amp[843]); 
  FFV1_0(w[179], w[163], w[0], pars->GC_11, amp[844]); 
  FFV1_0(w[167], w[176], w[4], pars->GC_11, amp[845]); 
  FFV1_0(w[153], w[176], w[0], pars->GC_11, amp[846]); 
  FFV1_0(w[167], w[178], w[4], pars->GC_11, amp[847]); 
  FFV1_0(w[153], w[178], w[0], pars->GC_11, amp[848]); 
  FFV1_0(w[195], w[202], w[5], pars->GC_11, amp[849]); 
  FFV1_0(w[187], w[202], w[4], pars->GC_11, amp[850]); 
  FFV1_0(w[203], w[194], w[5], pars->GC_11, amp[851]); 
  FFV1_0(w[203], w[182], w[4], pars->GC_11, amp[852]); 
  FFV1_0(w[187], w[194], w[0], pars->GC_11, amp[853]); 
  FFV1_0(w[195], w[182], w[0], pars->GC_11, amp[854]); 
  FFV2_0(w[153], w[202], w[3], pars->GC_100, amp[855]); 
  FFV1_0(w[189], w[202], w[4], pars->GC_11, amp[856]); 
  FFV2_0(w[167], w[194], w[3], pars->GC_100, amp[857]); 
  FFV1_0(w[167], w[181], w[4], pars->GC_11, amp[858]); 
  FFV1_0(w[189], w[194], w[0], pars->GC_11, amp[859]); 
  FFV1_0(w[153], w[181], w[0], pars->GC_11, amp[860]); 
  FFV2_0(w[203], w[163], w[2], pars->GC_100, amp[861]); 
  FFV1_0(w[203], w[188], w[4], pars->GC_11, amp[862]); 
  FFV2_0(w[195], w[169], w[2], pars->GC_100, amp[863]); 
  FFV1_0(w[186], w[169], w[4], pars->GC_11, amp[864]); 
  FFV1_0(w[195], w[188], w[0], pars->GC_11, amp[865]); 
  FFV1_0(w[186], w[163], w[0], pars->GC_11, amp[866]); 
  FFV1_0(w[111], w[170], w[8], pars->GC_1, amp[867]); 
  FFV1_0(w[111], w[171], w[8], pars->GC_1, amp[868]); 
  FFV1_0(w[111], w[172], w[8], pars->GC_1, amp[869]); 
  FFV1_0(w[173], w[110], w[8], pars->GC_1, amp[870]); 
  FFV1_0(w[174], w[110], w[8], pars->GC_1, amp[871]); 
  FFV1_0(w[175], w[110], w[8], pars->GC_1, amp[872]); 
  FFV2_3_0(w[111], w[170], w[14], pars->GC_50, pars->GC_58, amp[873]); 
  FFV2_3_0(w[111], w[171], w[14], pars->GC_50, pars->GC_58, amp[874]); 
  FFV2_3_0(w[111], w[172], w[14], pars->GC_50, pars->GC_58, amp[875]); 
  FFV2_3_0(w[173], w[110], w[14], pars->GC_50, pars->GC_58, amp[876]); 
  FFV2_3_0(w[174], w[110], w[14], pars->GC_50, pars->GC_58, amp[877]); 
  FFV2_3_0(w[175], w[110], w[14], pars->GC_50, pars->GC_58, amp[878]); 
  FFV2_0(w[173], w[180], w[3], pars->GC_100, amp[879]); 
  FFV2_0(w[174], w[180], w[3], pars->GC_100, amp[880]); 
  FFV2_0(w[175], w[180], w[3], pars->GC_100, amp[881]); 
  FFV1_0(w[183], w[180], w[73], pars->GC_11, amp[882]); 
  FFV1_0(w[183], w[180], w[74], pars->GC_11, amp[883]); 
  FFV1_0(w[183], w[180], w[75], pars->GC_11, amp[884]); 
  FFV2_0(w[183], w[170], w[2], pars->GC_100, amp[885]); 
  FFV2_0(w[183], w[171], w[2], pars->GC_100, amp[886]); 
  FFV2_0(w[183], w[172], w[2], pars->GC_100, amp[887]); 


}
double PY8MEs_R12_P30_sm_gq_wpwmggq::matrix_12_gu_wpwmggu() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 222;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = -amp[0] - amp[1] - Complex<double> (0, 1) * amp[2] - amp[4] -
      amp[5] - Complex<double> (0, 1) * amp[6] - Complex<double> (0, 1) *
      amp[12] - Complex<double> (0, 1) * amp[13] - Complex<double> (0, 1) *
      amp[14] - Complex<double> (0, 1) * amp[15] - amp[16] - Complex<double>
      (0, 1) * amp[17] - Complex<double> (0, 1) * amp[18] - Complex<double> (0,
      1) * amp[20] - amp[21] - amp[24] - Complex<double> (0, 1) * amp[27] -
      Complex<double> (0, 1) * amp[28] - Complex<double> (0, 1) * amp[30] -
      Complex<double> (0, 1) * amp[62] - Complex<double> (0, 1) * amp[63] -
      Complex<double> (0, 1) * amp[64] - Complex<double> (0, 1) * amp[65] -
      Complex<double> (0, 1) * amp[66] - Complex<double> (0, 1) * amp[67] -
      Complex<double> (0, 1) * amp[68] + amp[76] + amp[78] + amp[80] + amp[81]
      + amp[82] + amp[83] + amp[84] + amp[85] + amp[87] + amp[90] + amp[91] +
      amp[92] - amp[124] - amp[125] - Complex<double> (0, 1) * amp[127] -
      amp[128] - amp[129] - Complex<double> (0, 1) * amp[131] - amp[132] -
      amp[135] - Complex<double> (0, 1) * amp[137] - amp[138] - Complex<double>
      (0, 1) * amp[139] - Complex<double> (0, 1) * amp[140] + amp[180] +
      amp[182] + amp[186] + amp[195] + amp[196] + amp[200] - amp[201] +
      amp[203] - amp[204] + amp[206] - amp[207] + amp[209] - amp[210] +
      amp[212] + amp[215] - amp[213] + amp[218] - amp[216] - amp[219] +
      amp[221];
  jamp[1] = -Complex<double> (0, 1) * amp[38] - Complex<double> (0, 1) *
      amp[39] - Complex<double> (0, 1) * amp[40] - Complex<double> (0, 1) *
      amp[41] - Complex<double> (0, 1) * amp[42] - Complex<double> (0, 1) *
      amp[43] - Complex<double> (0, 1) * amp[44] - amp[45] - amp[46] -
      Complex<double> (0, 1) * amp[47] - amp[49] - amp[50] - Complex<double>
      (0, 1) * amp[51] - amp[53] - Complex<double> (0, 1) * amp[54] -
      Complex<double> (0, 1) * amp[55] - amp[56] - Complex<double> (0, 1) *
      amp[58] - amp[59] + Complex<double> (0, 1) * amp[62] + Complex<double>
      (0, 1) * amp[63] + Complex<double> (0, 1) * amp[64] + Complex<double> (0,
      1) * amp[65] + Complex<double> (0, 1) * amp[66] + Complex<double> (0, 1)
      * amp[67] + Complex<double> (0, 1) * amp[68] + amp[69] + amp[70] +
      amp[71] + amp[72] + amp[73] + amp[74] + amp[75] + amp[77] + amp[79] +
      amp[86] + amp[88] + amp[89] + amp[124] + amp[125] + Complex<double> (0,
      1) * amp[127] + amp[128] + amp[129] + Complex<double> (0, 1) * amp[131] +
      amp[132] + amp[135] + Complex<double> (0, 1) * amp[137] + amp[138] +
      Complex<double> (0, 1) * amp[139] + Complex<double> (0, 1) * amp[140] +
      amp[159] + amp[161] + amp[168] + amp[169] + amp[171] + amp[185] +
      amp[201] + amp[202] + amp[204] + amp[205] + amp[207] + amp[208] +
      amp[210] + amp[211] + amp[213] + amp[214] + amp[216] + amp[217] +
      amp[219] + amp[220];
  jamp[2] = +amp[0] + amp[1] + Complex<double> (0, 1) * amp[2] + amp[4] +
      amp[5] + Complex<double> (0, 1) * amp[6] + Complex<double> (0, 1) *
      amp[12] + Complex<double> (0, 1) * amp[13] + Complex<double> (0, 1) *
      amp[14] + Complex<double> (0, 1) * amp[15] + amp[16] + Complex<double>
      (0, 1) * amp[17] + Complex<double> (0, 1) * amp[18] + Complex<double> (0,
      1) * amp[20] + amp[21] + amp[24] + Complex<double> (0, 1) * amp[27] +
      Complex<double> (0, 1) * amp[28] + Complex<double> (0, 1) * amp[30] -
      Complex<double> (0, 1) * amp[31] - Complex<double> (0, 1) * amp[32] -
      Complex<double> (0, 1) * amp[33] - Complex<double> (0, 1) * amp[34] -
      Complex<double> (0, 1) * amp[35] - Complex<double> (0, 1) * amp[36] -
      Complex<double> (0, 1) * amp[37] + amp[45] + amp[46] - Complex<double>
      (0, 1) * amp[48] + amp[49] + amp[50] - Complex<double> (0, 1) * amp[52] +
      amp[53] + amp[56] - Complex<double> (0, 1) * amp[57] + amp[59] -
      Complex<double> (0, 1) * amp[60] - Complex<double> (0, 1) * amp[61] +
      amp[141] + amp[143] + amp[145] + amp[146] + amp[147] + amp[148] +
      amp[149] + amp[150] + amp[153] + amp[155] + amp[156] + amp[157] +
      amp[179] + amp[181] + amp[188] + amp[197] + amp[198] + amp[199] -
      amp[202] - amp[203] - amp[205] - amp[206] - amp[208] - amp[209] -
      amp[211] - amp[212] - amp[215] - amp[214] - amp[218] - amp[217] -
      amp[220] - amp[221];
  jamp[3] = +Complex<double> (0, 1) * amp[31] + Complex<double> (0, 1) *
      amp[32] + Complex<double> (0, 1) * amp[33] + Complex<double> (0, 1) *
      amp[34] + Complex<double> (0, 1) * amp[35] + Complex<double> (0, 1) *
      amp[36] + Complex<double> (0, 1) * amp[37] - amp[45] - amp[46] +
      Complex<double> (0, 1) * amp[48] - amp[49] - amp[50] + Complex<double>
      (0, 1) * amp[52] - amp[53] - amp[56] + Complex<double> (0, 1) * amp[57] -
      amp[59] + Complex<double> (0, 1) * amp[60] + Complex<double> (0, 1) *
      amp[61] - Complex<double> (0, 1) * amp[93] - Complex<double> (0, 1) *
      amp[94] - Complex<double> (0, 1) * amp[95] - Complex<double> (0, 1) *
      amp[96] - Complex<double> (0, 1) * amp[97] - Complex<double> (0, 1) *
      amp[98] - Complex<double> (0, 1) * amp[99] + amp[100] + amp[101] +
      amp[102] + amp[103] + amp[104] + amp[105] + amp[106] + amp[108] +
      amp[110] + amp[117] + amp[119] + amp[120] + amp[124] + amp[125] -
      Complex<double> (0, 1) * amp[126] + amp[128] + amp[129] - Complex<double>
      (0, 1) * amp[130] + amp[132] - Complex<double> (0, 1) * amp[133] -
      Complex<double> (0, 1) * amp[134] + amp[135] - Complex<double> (0, 1) *
      amp[136] + amp[138] + amp[142] + amp[144] + amp[151] + amp[152] +
      amp[154] + amp[183] + amp[201] + amp[202] + amp[204] + amp[205] +
      amp[207] + amp[208] + amp[210] + amp[211] + amp[213] + amp[214] +
      amp[216] + amp[217] + amp[219] + amp[220];
  jamp[4] = +amp[0] + amp[1] - Complex<double> (0, 1) * amp[3] + amp[4] +
      amp[5] - Complex<double> (0, 1) * amp[7] - Complex<double> (0, 1) *
      amp[8] - Complex<double> (0, 1) * amp[9] - Complex<double> (0, 1) *
      amp[10] - Complex<double> (0, 1) * amp[11] + amp[16] - Complex<double>
      (0, 1) * amp[19] + amp[21] - Complex<double> (0, 1) * amp[22] -
      Complex<double> (0, 1) * amp[23] + amp[24] - Complex<double> (0, 1) *
      amp[25] - Complex<double> (0, 1) * amp[26] - Complex<double> (0, 1) *
      amp[29] + Complex<double> (0, 1) * amp[38] + Complex<double> (0, 1) *
      amp[39] + Complex<double> (0, 1) * amp[40] + Complex<double> (0, 1) *
      amp[41] + Complex<double> (0, 1) * amp[42] + Complex<double> (0, 1) *
      amp[43] + Complex<double> (0, 1) * amp[44] + amp[45] + amp[46] +
      Complex<double> (0, 1) * amp[47] + amp[49] + amp[50] + Complex<double>
      (0, 1) * amp[51] + amp[53] + Complex<double> (0, 1) * amp[54] +
      Complex<double> (0, 1) * amp[55] + amp[56] + Complex<double> (0, 1) *
      amp[58] + amp[59] + amp[158] + amp[160] + amp[162] + amp[163] + amp[164]
      + amp[165] + amp[166] + amp[167] + amp[170] + amp[172] + amp[173] +
      amp[174] + amp[175] + amp[177] + amp[187] + amp[191] + amp[192] +
      amp[193] - amp[202] - amp[203] - amp[205] - amp[206] - amp[208] -
      amp[209] - amp[211] - amp[212] - amp[215] - amp[214] - amp[218] -
      amp[217] - amp[220] - amp[221];
  jamp[5] = -amp[0] - amp[1] + Complex<double> (0, 1) * amp[3] - amp[4] -
      amp[5] + Complex<double> (0, 1) * amp[7] + Complex<double> (0, 1) *
      amp[8] + Complex<double> (0, 1) * amp[9] + Complex<double> (0, 1) *
      amp[10] + Complex<double> (0, 1) * amp[11] - amp[16] + Complex<double>
      (0, 1) * amp[19] - amp[21] + Complex<double> (0, 1) * amp[22] +
      Complex<double> (0, 1) * amp[23] - amp[24] + Complex<double> (0, 1) *
      amp[25] + Complex<double> (0, 1) * amp[26] + Complex<double> (0, 1) *
      amp[29] + Complex<double> (0, 1) * amp[93] + Complex<double> (0, 1) *
      amp[94] + Complex<double> (0, 1) * amp[95] + Complex<double> (0, 1) *
      amp[96] + Complex<double> (0, 1) * amp[97] + Complex<double> (0, 1) *
      amp[98] + Complex<double> (0, 1) * amp[99] + amp[107] + amp[109] +
      amp[111] + amp[112] + amp[113] + amp[114] + amp[115] + amp[116] +
      amp[118] + amp[121] + amp[122] + amp[123] - amp[124] - amp[125] +
      Complex<double> (0, 1) * amp[126] - amp[128] - amp[129] + Complex<double>
      (0, 1) * amp[130] - amp[132] + Complex<double> (0, 1) * amp[133] +
      Complex<double> (0, 1) * amp[134] - amp[135] + Complex<double> (0, 1) *
      amp[136] - amp[138] + amp[176] + amp[178] + amp[184] + amp[189] +
      amp[190] + amp[194] - amp[201] + amp[203] - amp[204] + amp[206] -
      amp[207] + amp[209] - amp[210] + amp[212] + amp[215] - amp[213] +
      amp[218] - amp[216] - amp[219] + amp[221];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[0][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P30_sm_gq_wpwmggq::matrix_12_gd_wpwmggd() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 222;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = -amp[222] - amp[223] - Complex<double> (0, 1) * amp[224] - amp[226]
      - amp[227] - Complex<double> (0, 1) * amp[228] - Complex<double> (0, 1) *
      amp[234] - Complex<double> (0, 1) * amp[235] - Complex<double> (0, 1) *
      amp[236] - Complex<double> (0, 1) * amp[237] - amp[238] - Complex<double>
      (0, 1) * amp[241] - amp[243] - Complex<double> (0, 1) * amp[244] -
      Complex<double> (0, 1) * amp[245] - amp[246] - Complex<double> (0, 1) *
      amp[247] - Complex<double> (0, 1) * amp[248] - Complex<double> (0, 1) *
      amp[252] - Complex<double> (0, 1) * amp[284] - Complex<double> (0, 1) *
      amp[285] - Complex<double> (0, 1) * amp[286] - Complex<double> (0, 1) *
      amp[287] - Complex<double> (0, 1) * amp[288] - Complex<double> (0, 1) *
      amp[289] - Complex<double> (0, 1) * amp[290] + amp[298] + amp[300] +
      amp[302] + amp[303] + amp[304] + amp[305] + amp[306] + amp[307] +
      amp[309] + amp[312] + amp[313] + amp[314] - amp[346] - amp[347] -
      Complex<double> (0, 1) * amp[349] - amp[350] - amp[351] - Complex<double>
      (0, 1) * amp[353] - amp[354] - Complex<double> (0, 1) * amp[355] -
      Complex<double> (0, 1) * amp[356] - amp[357] - Complex<double> (0, 1) *
      amp[358] - amp[360] + amp[402] + amp[404] + amp[406] + amp[411] +
      amp[412] + amp[416] - amp[423] + amp[425] - amp[426] + amp[428] -
      amp[429] + amp[431] - amp[432] + amp[434] - amp[435] + amp[437] +
      amp[440] - amp[438] + amp[443] - amp[441];
  jamp[1] = -Complex<double> (0, 1) * amp[260] - Complex<double> (0, 1) *
      amp[261] - Complex<double> (0, 1) * amp[262] - Complex<double> (0, 1) *
      amp[263] - Complex<double> (0, 1) * amp[264] - Complex<double> (0, 1) *
      amp[265] - Complex<double> (0, 1) * amp[266] - amp[267] - amp[268] -
      Complex<double> (0, 1) * amp[269] - amp[271] - amp[272] - Complex<double>
      (0, 1) * amp[273] - amp[275] - amp[278] - Complex<double> (0, 1) *
      amp[279] - amp[281] - Complex<double> (0, 1) * amp[282] - Complex<double>
      (0, 1) * amp[283] + Complex<double> (0, 1) * amp[284] + Complex<double>
      (0, 1) * amp[285] + Complex<double> (0, 1) * amp[286] + Complex<double>
      (0, 1) * amp[287] + Complex<double> (0, 1) * amp[288] + Complex<double>
      (0, 1) * amp[289] + Complex<double> (0, 1) * amp[290] + amp[291] +
      amp[292] + amp[293] + amp[294] + amp[295] + amp[296] + amp[297] +
      amp[299] + amp[301] + amp[308] + amp[310] + amp[311] + amp[346] +
      amp[347] + Complex<double> (0, 1) * amp[349] + amp[350] + amp[351] +
      Complex<double> (0, 1) * amp[353] + amp[354] + Complex<double> (0, 1) *
      amp[355] + Complex<double> (0, 1) * amp[356] + amp[357] + Complex<double>
      (0, 1) * amp[358] + amp[360] + amp[381] + amp[383] + amp[390] + amp[391]
      + amp[393] + amp[405] + amp[423] + amp[424] + amp[426] + amp[427] +
      amp[429] + amp[430] + amp[432] + amp[433] + amp[435] + amp[436] +
      amp[438] + amp[439] + amp[441] + amp[442];
  jamp[2] = +amp[222] + amp[223] + Complex<double> (0, 1) * amp[224] + amp[226]
      + amp[227] + Complex<double> (0, 1) * amp[228] + Complex<double> (0, 1) *
      amp[234] + Complex<double> (0, 1) * amp[235] + Complex<double> (0, 1) *
      amp[236] + Complex<double> (0, 1) * amp[237] + amp[238] + Complex<double>
      (0, 1) * amp[241] + amp[243] + Complex<double> (0, 1) * amp[244] +
      Complex<double> (0, 1) * amp[245] + amp[246] + Complex<double> (0, 1) *
      amp[247] + Complex<double> (0, 1) * amp[248] + Complex<double> (0, 1) *
      amp[252] - Complex<double> (0, 1) * amp[253] - Complex<double> (0, 1) *
      amp[254] - Complex<double> (0, 1) * amp[255] - Complex<double> (0, 1) *
      amp[256] - Complex<double> (0, 1) * amp[257] - Complex<double> (0, 1) *
      amp[258] - Complex<double> (0, 1) * amp[259] + amp[267] + amp[268] -
      Complex<double> (0, 1) * amp[270] + amp[271] + amp[272] - Complex<double>
      (0, 1) * amp[274] + amp[275] - Complex<double> (0, 1) * amp[276] -
      Complex<double> (0, 1) * amp[277] + amp[278] - Complex<double> (0, 1) *
      amp[280] + amp[281] + amp[363] + amp[365] + amp[367] + amp[368] +
      amp[369] + amp[370] + amp[371] + amp[372] + amp[375] + amp[377] +
      amp[378] + amp[379] + amp[401] + amp[403] + amp[409] + amp[413] +
      amp[414] + amp[415] - amp[424] - amp[425] - amp[427] - amp[428] -
      amp[430] - amp[431] - amp[433] - amp[434] - amp[436] - amp[437] -
      amp[440] - amp[439] - amp[443] - amp[442];
  jamp[3] = +Complex<double> (0, 1) * amp[253] + Complex<double> (0, 1) *
      amp[254] + Complex<double> (0, 1) * amp[255] + Complex<double> (0, 1) *
      amp[256] + Complex<double> (0, 1) * amp[257] + Complex<double> (0, 1) *
      amp[258] + Complex<double> (0, 1) * amp[259] - amp[267] - amp[268] +
      Complex<double> (0, 1) * amp[270] - amp[271] - amp[272] + Complex<double>
      (0, 1) * amp[274] - amp[275] + Complex<double> (0, 1) * amp[276] +
      Complex<double> (0, 1) * amp[277] - amp[278] + Complex<double> (0, 1) *
      amp[280] - amp[281] - Complex<double> (0, 1) * amp[315] - Complex<double>
      (0, 1) * amp[316] - Complex<double> (0, 1) * amp[317] - Complex<double>
      (0, 1) * amp[318] - Complex<double> (0, 1) * amp[319] - Complex<double>
      (0, 1) * amp[320] - Complex<double> (0, 1) * amp[321] + amp[322] +
      amp[323] + amp[324] + amp[325] + amp[326] + amp[327] + amp[328] +
      amp[330] + amp[332] + amp[339] + amp[341] + amp[342] + amp[346] +
      amp[347] - Complex<double> (0, 1) * amp[348] + amp[350] + amp[351] -
      Complex<double> (0, 1) * amp[352] + amp[354] + amp[357] - Complex<double>
      (0, 1) * amp[359] + amp[360] - Complex<double> (0, 1) * amp[361] -
      Complex<double> (0, 1) * amp[362] + amp[364] + amp[366] + amp[373] +
      amp[374] + amp[376] + amp[407] + amp[423] + amp[424] + amp[426] +
      amp[427] + amp[429] + amp[430] + amp[432] + amp[433] + amp[435] +
      amp[436] + amp[438] + amp[439] + amp[441] + amp[442];
  jamp[4] = +amp[222] + amp[223] - Complex<double> (0, 1) * amp[225] + amp[226]
      + amp[227] - Complex<double> (0, 1) * amp[229] - Complex<double> (0, 1) *
      amp[230] - Complex<double> (0, 1) * amp[231] - Complex<double> (0, 1) *
      amp[232] - Complex<double> (0, 1) * amp[233] + amp[238] - Complex<double>
      (0, 1) * amp[239] - Complex<double> (0, 1) * amp[240] - Complex<double>
      (0, 1) * amp[242] + amp[243] + amp[246] - Complex<double> (0, 1) *
      amp[249] - Complex<double> (0, 1) * amp[250] - Complex<double> (0, 1) *
      amp[251] + Complex<double> (0, 1) * amp[260] + Complex<double> (0, 1) *
      amp[261] + Complex<double> (0, 1) * amp[262] + Complex<double> (0, 1) *
      amp[263] + Complex<double> (0, 1) * amp[264] + Complex<double> (0, 1) *
      amp[265] + Complex<double> (0, 1) * amp[266] + amp[267] + amp[268] +
      Complex<double> (0, 1) * amp[269] + amp[271] + amp[272] + Complex<double>
      (0, 1) * amp[273] + amp[275] + amp[278] + Complex<double> (0, 1) *
      amp[279] + amp[281] + Complex<double> (0, 1) * amp[282] + Complex<double>
      (0, 1) * amp[283] + amp[380] + amp[382] + amp[384] + amp[385] + amp[386]
      + amp[387] + amp[388] + amp[389] + amp[392] + amp[394] + amp[395] +
      amp[396] + amp[397] + amp[399] + amp[410] + amp[419] + amp[420] +
      amp[421] - amp[424] - amp[425] - amp[427] - amp[428] - amp[430] -
      amp[431] - amp[433] - amp[434] - amp[436] - amp[437] - amp[440] -
      amp[439] - amp[443] - amp[442];
  jamp[5] = -amp[222] - amp[223] + Complex<double> (0, 1) * amp[225] - amp[226]
      - amp[227] + Complex<double> (0, 1) * amp[229] + Complex<double> (0, 1) *
      amp[230] + Complex<double> (0, 1) * amp[231] + Complex<double> (0, 1) *
      amp[232] + Complex<double> (0, 1) * amp[233] - amp[238] + Complex<double>
      (0, 1) * amp[239] + Complex<double> (0, 1) * amp[240] + Complex<double>
      (0, 1) * amp[242] - amp[243] - amp[246] + Complex<double> (0, 1) *
      amp[249] + Complex<double> (0, 1) * amp[250] + Complex<double> (0, 1) *
      amp[251] + Complex<double> (0, 1) * amp[315] + Complex<double> (0, 1) *
      amp[316] + Complex<double> (0, 1) * amp[317] + Complex<double> (0, 1) *
      amp[318] + Complex<double> (0, 1) * amp[319] + Complex<double> (0, 1) *
      amp[320] + Complex<double> (0, 1) * amp[321] + amp[329] + amp[331] +
      amp[333] + amp[334] + amp[335] + amp[336] + amp[337] + amp[338] +
      amp[340] + amp[343] + amp[344] + amp[345] - amp[346] - amp[347] +
      Complex<double> (0, 1) * amp[348] - amp[350] - amp[351] + Complex<double>
      (0, 1) * amp[352] - amp[354] - amp[357] + Complex<double> (0, 1) *
      amp[359] - amp[360] + Complex<double> (0, 1) * amp[361] + Complex<double>
      (0, 1) * amp[362] + amp[398] + amp[400] + amp[408] + amp[417] + amp[418]
      + amp[422] - amp[423] + amp[425] - amp[426] + amp[428] - amp[429] +
      amp[431] - amp[432] + amp[434] - amp[435] + amp[437] + amp[440] -
      amp[438] + amp[443] - amp[441];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[1][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P30_sm_gq_wpwmggq::matrix_12_gux_wpwmggux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 222;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = +amp[444] + amp[445] + Complex<double> (0, 1) * amp[446] + amp[448]
      + amp[449] + Complex<double> (0, 1) * amp[450] + Complex<double> (0, 1) *
      amp[456] + Complex<double> (0, 1) * amp[457] + Complex<double> (0, 1) *
      amp[458] + Complex<double> (0, 1) * amp[459] + amp[460] + Complex<double>
      (0, 1) * amp[461] + Complex<double> (0, 1) * amp[462] + Complex<double>
      (0, 1) * amp[464] + amp[465] + amp[468] + Complex<double> (0, 1) *
      amp[471] + Complex<double> (0, 1) * amp[472] + Complex<double> (0, 1) *
      amp[474] + Complex<double> (0, 1) * amp[506] + Complex<double> (0, 1) *
      amp[507] + Complex<double> (0, 1) * amp[508] + Complex<double> (0, 1) *
      amp[509] + Complex<double> (0, 1) * amp[510] + Complex<double> (0, 1) *
      amp[511] + Complex<double> (0, 1) * amp[512] - amp[520] - amp[522] -
      amp[524] - amp[525] - amp[526] - amp[527] - amp[528] - amp[529] -
      amp[531] - amp[534] - amp[535] - amp[536] + amp[568] + amp[569] +
      Complex<double> (0, 1) * amp[571] + amp[572] + amp[573] + Complex<double>
      (0, 1) * amp[575] + amp[576] + amp[579] + Complex<double> (0, 1) *
      amp[581] + amp[582] + Complex<double> (0, 1) * amp[583] + Complex<double>
      (0, 1) * amp[584] - amp[624] - amp[626] - amp[630] - amp[639] - amp[640]
      - amp[644] + amp[645] - amp[647] + amp[648] - amp[650] + amp[651] -
      amp[653] + amp[654] - amp[656] - amp[659] + amp[657] - amp[662] +
      amp[660] + amp[663] - amp[665];
  jamp[1] = +Complex<double> (0, 1) * amp[482] + Complex<double> (0, 1) *
      amp[483] + Complex<double> (0, 1) * amp[484] + Complex<double> (0, 1) *
      amp[485] + Complex<double> (0, 1) * amp[486] + Complex<double> (0, 1) *
      amp[487] + Complex<double> (0, 1) * amp[488] + amp[489] + amp[490] +
      Complex<double> (0, 1) * amp[491] + amp[493] + amp[494] + Complex<double>
      (0, 1) * amp[495] + amp[497] + Complex<double> (0, 1) * amp[498] +
      Complex<double> (0, 1) * amp[499] + amp[500] + Complex<double> (0, 1) *
      amp[502] + amp[503] - Complex<double> (0, 1) * amp[506] - Complex<double>
      (0, 1) * amp[507] - Complex<double> (0, 1) * amp[508] - Complex<double>
      (0, 1) * amp[509] - Complex<double> (0, 1) * amp[510] - Complex<double>
      (0, 1) * amp[511] - Complex<double> (0, 1) * amp[512] - amp[513] -
      amp[514] - amp[515] - amp[516] - amp[517] - amp[518] - amp[519] -
      amp[521] - amp[523] - amp[530] - amp[532] - amp[533] - amp[568] -
      amp[569] - Complex<double> (0, 1) * amp[571] - amp[572] - amp[573] -
      Complex<double> (0, 1) * amp[575] - amp[576] - amp[579] - Complex<double>
      (0, 1) * amp[581] - amp[582] - Complex<double> (0, 1) * amp[583] -
      Complex<double> (0, 1) * amp[584] - amp[603] - amp[605] - amp[612] -
      amp[613] - amp[615] - amp[629] - amp[645] - amp[646] - amp[648] -
      amp[649] - amp[651] - amp[652] - amp[654] - amp[655] - amp[657] -
      amp[658] - amp[660] - amp[661] - amp[663] - amp[664];
  jamp[2] = -amp[444] - amp[445] - Complex<double> (0, 1) * amp[446] - amp[448]
      - amp[449] - Complex<double> (0, 1) * amp[450] - Complex<double> (0, 1) *
      amp[456] - Complex<double> (0, 1) * amp[457] - Complex<double> (0, 1) *
      amp[458] - Complex<double> (0, 1) * amp[459] - amp[460] - Complex<double>
      (0, 1) * amp[461] - Complex<double> (0, 1) * amp[462] - Complex<double>
      (0, 1) * amp[464] - amp[465] - amp[468] - Complex<double> (0, 1) *
      amp[471] - Complex<double> (0, 1) * amp[472] - Complex<double> (0, 1) *
      amp[474] + Complex<double> (0, 1) * amp[475] + Complex<double> (0, 1) *
      amp[476] + Complex<double> (0, 1) * amp[477] + Complex<double> (0, 1) *
      amp[478] + Complex<double> (0, 1) * amp[479] + Complex<double> (0, 1) *
      amp[480] + Complex<double> (0, 1) * amp[481] - amp[489] - amp[490] +
      Complex<double> (0, 1) * amp[492] - amp[493] - amp[494] + Complex<double>
      (0, 1) * amp[496] - amp[497] - amp[500] + Complex<double> (0, 1) *
      amp[501] - amp[503] + Complex<double> (0, 1) * amp[504] + Complex<double>
      (0, 1) * amp[505] - amp[585] - amp[587] - amp[589] - amp[590] - amp[591]
      - amp[592] - amp[593] - amp[594] - amp[597] - amp[599] - amp[600] -
      amp[601] - amp[623] - amp[625] - amp[632] - amp[641] - amp[642] -
      amp[643] + amp[646] + amp[647] + amp[649] + amp[650] + amp[652] +
      amp[653] + amp[655] + amp[656] + amp[659] + amp[658] + amp[662] +
      amp[661] + amp[664] + amp[665];
  jamp[3] = -Complex<double> (0, 1) * amp[475] - Complex<double> (0, 1) *
      amp[476] - Complex<double> (0, 1) * amp[477] - Complex<double> (0, 1) *
      amp[478] - Complex<double> (0, 1) * amp[479] - Complex<double> (0, 1) *
      amp[480] - Complex<double> (0, 1) * amp[481] + amp[489] + amp[490] -
      Complex<double> (0, 1) * amp[492] + amp[493] + amp[494] - Complex<double>
      (0, 1) * amp[496] + amp[497] + amp[500] - Complex<double> (0, 1) *
      amp[501] + amp[503] - Complex<double> (0, 1) * amp[504] - Complex<double>
      (0, 1) * amp[505] + Complex<double> (0, 1) * amp[537] + Complex<double>
      (0, 1) * amp[538] + Complex<double> (0, 1) * amp[539] + Complex<double>
      (0, 1) * amp[540] + Complex<double> (0, 1) * amp[541] + Complex<double>
      (0, 1) * amp[542] + Complex<double> (0, 1) * amp[543] - amp[544] -
      amp[545] - amp[546] - amp[547] - amp[548] - amp[549] - amp[550] -
      amp[552] - amp[554] - amp[561] - amp[563] - amp[564] - amp[568] -
      amp[569] + Complex<double> (0, 1) * amp[570] - amp[572] - amp[573] +
      Complex<double> (0, 1) * amp[574] - amp[576] + Complex<double> (0, 1) *
      amp[577] + Complex<double> (0, 1) * amp[578] - amp[579] + Complex<double>
      (0, 1) * amp[580] - amp[582] - amp[586] - amp[588] - amp[595] - amp[596]
      - amp[598] - amp[627] - amp[645] - amp[646] - amp[648] - amp[649] -
      amp[651] - amp[652] - amp[654] - amp[655] - amp[657] - amp[658] -
      amp[660] - amp[661] - amp[663] - amp[664];
  jamp[4] = -amp[444] - amp[445] + Complex<double> (0, 1) * amp[447] - amp[448]
      - amp[449] + Complex<double> (0, 1) * amp[451] + Complex<double> (0, 1) *
      amp[452] + Complex<double> (0, 1) * amp[453] + Complex<double> (0, 1) *
      amp[454] + Complex<double> (0, 1) * amp[455] - amp[460] + Complex<double>
      (0, 1) * amp[463] - amp[465] + Complex<double> (0, 1) * amp[466] +
      Complex<double> (0, 1) * amp[467] - amp[468] + Complex<double> (0, 1) *
      amp[469] + Complex<double> (0, 1) * amp[470] + Complex<double> (0, 1) *
      amp[473] - Complex<double> (0, 1) * amp[482] - Complex<double> (0, 1) *
      amp[483] - Complex<double> (0, 1) * amp[484] - Complex<double> (0, 1) *
      amp[485] - Complex<double> (0, 1) * amp[486] - Complex<double> (0, 1) *
      amp[487] - Complex<double> (0, 1) * amp[488] - amp[489] - amp[490] -
      Complex<double> (0, 1) * amp[491] - amp[493] - amp[494] - Complex<double>
      (0, 1) * amp[495] - amp[497] - Complex<double> (0, 1) * amp[498] -
      Complex<double> (0, 1) * amp[499] - amp[500] - Complex<double> (0, 1) *
      amp[502] - amp[503] - amp[602] - amp[604] - amp[606] - amp[607] -
      amp[608] - amp[609] - amp[610] - amp[611] - amp[614] - amp[616] -
      amp[617] - amp[618] - amp[619] - amp[621] - amp[631] - amp[635] -
      amp[636] - amp[637] + amp[646] + amp[647] + amp[649] + amp[650] +
      amp[652] + amp[653] + amp[655] + amp[656] + amp[659] + amp[658] +
      amp[662] + amp[661] + amp[664] + amp[665];
  jamp[5] = +amp[444] + amp[445] - Complex<double> (0, 1) * amp[447] + amp[448]
      + amp[449] - Complex<double> (0, 1) * amp[451] - Complex<double> (0, 1) *
      amp[452] - Complex<double> (0, 1) * amp[453] - Complex<double> (0, 1) *
      amp[454] - Complex<double> (0, 1) * amp[455] + amp[460] - Complex<double>
      (0, 1) * amp[463] + amp[465] - Complex<double> (0, 1) * amp[466] -
      Complex<double> (0, 1) * amp[467] + amp[468] - Complex<double> (0, 1) *
      amp[469] - Complex<double> (0, 1) * amp[470] - Complex<double> (0, 1) *
      amp[473] - Complex<double> (0, 1) * amp[537] - Complex<double> (0, 1) *
      amp[538] - Complex<double> (0, 1) * amp[539] - Complex<double> (0, 1) *
      amp[540] - Complex<double> (0, 1) * amp[541] - Complex<double> (0, 1) *
      amp[542] - Complex<double> (0, 1) * amp[543] - amp[551] - amp[553] -
      amp[555] - amp[556] - amp[557] - amp[558] - amp[559] - amp[560] -
      amp[562] - amp[565] - amp[566] - amp[567] + amp[568] + amp[569] -
      Complex<double> (0, 1) * amp[570] + amp[572] + amp[573] - Complex<double>
      (0, 1) * amp[574] + amp[576] - Complex<double> (0, 1) * amp[577] -
      Complex<double> (0, 1) * amp[578] + amp[579] - Complex<double> (0, 1) *
      amp[580] + amp[582] - amp[620] - amp[622] - amp[628] - amp[633] -
      amp[634] - amp[638] + amp[645] - amp[647] + amp[648] - amp[650] +
      amp[651] - amp[653] + amp[654] - amp[656] - amp[659] + amp[657] -
      amp[662] + amp[660] + amp[663] - amp[665];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[2][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P30_sm_gq_wpwmggq::matrix_12_gdx_wpwmggdx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 222;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = +amp[666] + amp[667] + Complex<double> (0, 1) * amp[668] + amp[670]
      + amp[671] + Complex<double> (0, 1) * amp[672] + Complex<double> (0, 1) *
      amp[678] + Complex<double> (0, 1) * amp[679] + Complex<double> (0, 1) *
      amp[680] + Complex<double> (0, 1) * amp[681] + amp[682] + Complex<double>
      (0, 1) * amp[685] + amp[687] + Complex<double> (0, 1) * amp[688] +
      Complex<double> (0, 1) * amp[689] + amp[690] + Complex<double> (0, 1) *
      amp[691] + Complex<double> (0, 1) * amp[692] + Complex<double> (0, 1) *
      amp[696] + Complex<double> (0, 1) * amp[728] + Complex<double> (0, 1) *
      amp[729] + Complex<double> (0, 1) * amp[730] + Complex<double> (0, 1) *
      amp[731] + Complex<double> (0, 1) * amp[732] + Complex<double> (0, 1) *
      amp[733] + Complex<double> (0, 1) * amp[734] - amp[742] - amp[744] -
      amp[746] - amp[747] - amp[748] - amp[749] - amp[750] - amp[751] -
      amp[753] - amp[756] - amp[757] - amp[758] + amp[790] + amp[791] +
      Complex<double> (0, 1) * amp[793] + amp[794] + amp[795] + Complex<double>
      (0, 1) * amp[797] + amp[798] + Complex<double> (0, 1) * amp[799] +
      Complex<double> (0, 1) * amp[800] + amp[801] + Complex<double> (0, 1) *
      amp[802] + amp[804] - amp[846] - amp[848] - amp[850] - amp[855] -
      amp[856] - amp[860] + amp[867] - amp[869] + amp[870] - amp[872] +
      amp[873] - amp[875] + amp[876] - amp[878] + amp[879] - amp[881] -
      amp[884] + amp[882] - amp[887] + amp[885];
  jamp[1] = +Complex<double> (0, 1) * amp[704] + Complex<double> (0, 1) *
      amp[705] + Complex<double> (0, 1) * amp[706] + Complex<double> (0, 1) *
      amp[707] + Complex<double> (0, 1) * amp[708] + Complex<double> (0, 1) *
      amp[709] + Complex<double> (0, 1) * amp[710] + amp[711] + amp[712] +
      Complex<double> (0, 1) * amp[713] + amp[715] + amp[716] + Complex<double>
      (0, 1) * amp[717] + amp[719] + amp[722] + Complex<double> (0, 1) *
      amp[723] + amp[725] + Complex<double> (0, 1) * amp[726] + Complex<double>
      (0, 1) * amp[727] - Complex<double> (0, 1) * amp[728] - Complex<double>
      (0, 1) * amp[729] - Complex<double> (0, 1) * amp[730] - Complex<double>
      (0, 1) * amp[731] - Complex<double> (0, 1) * amp[732] - Complex<double>
      (0, 1) * amp[733] - Complex<double> (0, 1) * amp[734] - amp[735] -
      amp[736] - amp[737] - amp[738] - amp[739] - amp[740] - amp[741] -
      amp[743] - amp[745] - amp[752] - amp[754] - amp[755] - amp[790] -
      amp[791] - Complex<double> (0, 1) * amp[793] - amp[794] - amp[795] -
      Complex<double> (0, 1) * amp[797] - amp[798] - Complex<double> (0, 1) *
      amp[799] - Complex<double> (0, 1) * amp[800] - amp[801] - Complex<double>
      (0, 1) * amp[802] - amp[804] - amp[825] - amp[827] - amp[834] - amp[835]
      - amp[837] - amp[849] - amp[867] - amp[868] - amp[870] - amp[871] -
      amp[873] - amp[874] - amp[876] - amp[877] - amp[879] - amp[880] -
      amp[882] - amp[883] - amp[885] - amp[886];
  jamp[2] = -amp[666] - amp[667] - Complex<double> (0, 1) * amp[668] - amp[670]
      - amp[671] - Complex<double> (0, 1) * amp[672] - Complex<double> (0, 1) *
      amp[678] - Complex<double> (0, 1) * amp[679] - Complex<double> (0, 1) *
      amp[680] - Complex<double> (0, 1) * amp[681] - amp[682] - Complex<double>
      (0, 1) * amp[685] - amp[687] - Complex<double> (0, 1) * amp[688] -
      Complex<double> (0, 1) * amp[689] - amp[690] - Complex<double> (0, 1) *
      amp[691] - Complex<double> (0, 1) * amp[692] - Complex<double> (0, 1) *
      amp[696] + Complex<double> (0, 1) * amp[697] + Complex<double> (0, 1) *
      amp[698] + Complex<double> (0, 1) * amp[699] + Complex<double> (0, 1) *
      amp[700] + Complex<double> (0, 1) * amp[701] + Complex<double> (0, 1) *
      amp[702] + Complex<double> (0, 1) * amp[703] - amp[711] - amp[712] +
      Complex<double> (0, 1) * amp[714] - amp[715] - amp[716] + Complex<double>
      (0, 1) * amp[718] - amp[719] + Complex<double> (0, 1) * amp[720] +
      Complex<double> (0, 1) * amp[721] - amp[722] + Complex<double> (0, 1) *
      amp[724] - amp[725] - amp[807] - amp[809] - amp[811] - amp[812] -
      amp[813] - amp[814] - amp[815] - amp[816] - amp[819] - amp[821] -
      amp[822] - amp[823] - amp[845] - amp[847] - amp[853] - amp[857] -
      amp[858] - amp[859] + amp[868] + amp[869] + amp[871] + amp[872] +
      amp[874] + amp[875] + amp[877] + amp[878] + amp[880] + amp[881] +
      amp[884] + amp[883] + amp[887] + amp[886];
  jamp[3] = -Complex<double> (0, 1) * amp[697] - Complex<double> (0, 1) *
      amp[698] - Complex<double> (0, 1) * amp[699] - Complex<double> (0, 1) *
      amp[700] - Complex<double> (0, 1) * amp[701] - Complex<double> (0, 1) *
      amp[702] - Complex<double> (0, 1) * amp[703] + amp[711] + amp[712] -
      Complex<double> (0, 1) * amp[714] + amp[715] + amp[716] - Complex<double>
      (0, 1) * amp[718] + amp[719] - Complex<double> (0, 1) * amp[720] -
      Complex<double> (0, 1) * amp[721] + amp[722] - Complex<double> (0, 1) *
      amp[724] + amp[725] + Complex<double> (0, 1) * amp[759] + Complex<double>
      (0, 1) * amp[760] + Complex<double> (0, 1) * amp[761] + Complex<double>
      (0, 1) * amp[762] + Complex<double> (0, 1) * amp[763] + Complex<double>
      (0, 1) * amp[764] + Complex<double> (0, 1) * amp[765] - amp[766] -
      amp[767] - amp[768] - amp[769] - amp[770] - amp[771] - amp[772] -
      amp[774] - amp[776] - amp[783] - amp[785] - amp[786] - amp[790] -
      amp[791] + Complex<double> (0, 1) * amp[792] - amp[794] - amp[795] +
      Complex<double> (0, 1) * amp[796] - amp[798] - amp[801] + Complex<double>
      (0, 1) * amp[803] - amp[804] + Complex<double> (0, 1) * amp[805] +
      Complex<double> (0, 1) * amp[806] - amp[808] - amp[810] - amp[817] -
      amp[818] - amp[820] - amp[851] - amp[867] - amp[868] - amp[870] -
      amp[871] - amp[873] - amp[874] - amp[876] - amp[877] - amp[879] -
      amp[880] - amp[882] - amp[883] - amp[885] - amp[886];
  jamp[4] = -amp[666] - amp[667] + Complex<double> (0, 1) * amp[669] - amp[670]
      - amp[671] + Complex<double> (0, 1) * amp[673] + Complex<double> (0, 1) *
      amp[674] + Complex<double> (0, 1) * amp[675] + Complex<double> (0, 1) *
      amp[676] + Complex<double> (0, 1) * amp[677] - amp[682] + Complex<double>
      (0, 1) * amp[683] + Complex<double> (0, 1) * amp[684] + Complex<double>
      (0, 1) * amp[686] - amp[687] - amp[690] + Complex<double> (0, 1) *
      amp[693] + Complex<double> (0, 1) * amp[694] + Complex<double> (0, 1) *
      amp[695] - Complex<double> (0, 1) * amp[704] - Complex<double> (0, 1) *
      amp[705] - Complex<double> (0, 1) * amp[706] - Complex<double> (0, 1) *
      amp[707] - Complex<double> (0, 1) * amp[708] - Complex<double> (0, 1) *
      amp[709] - Complex<double> (0, 1) * amp[710] - amp[711] - amp[712] -
      Complex<double> (0, 1) * amp[713] - amp[715] - amp[716] - Complex<double>
      (0, 1) * amp[717] - amp[719] - amp[722] - Complex<double> (0, 1) *
      amp[723] - amp[725] - Complex<double> (0, 1) * amp[726] - Complex<double>
      (0, 1) * amp[727] - amp[824] - amp[826] - amp[828] - amp[829] - amp[830]
      - amp[831] - amp[832] - amp[833] - amp[836] - amp[838] - amp[839] -
      amp[840] - amp[841] - amp[843] - amp[854] - amp[863] - amp[864] -
      amp[865] + amp[868] + amp[869] + amp[871] + amp[872] + amp[874] +
      amp[875] + amp[877] + amp[878] + amp[880] + amp[881] + amp[884] +
      amp[883] + amp[887] + amp[886];
  jamp[5] = +amp[666] + amp[667] - Complex<double> (0, 1) * amp[669] + amp[670]
      + amp[671] - Complex<double> (0, 1) * amp[673] - Complex<double> (0, 1) *
      amp[674] - Complex<double> (0, 1) * amp[675] - Complex<double> (0, 1) *
      amp[676] - Complex<double> (0, 1) * amp[677] + amp[682] - Complex<double>
      (0, 1) * amp[683] - Complex<double> (0, 1) * amp[684] - Complex<double>
      (0, 1) * amp[686] + amp[687] + amp[690] - Complex<double> (0, 1) *
      amp[693] - Complex<double> (0, 1) * amp[694] - Complex<double> (0, 1) *
      amp[695] - Complex<double> (0, 1) * amp[759] - Complex<double> (0, 1) *
      amp[760] - Complex<double> (0, 1) * amp[761] - Complex<double> (0, 1) *
      amp[762] - Complex<double> (0, 1) * amp[763] - Complex<double> (0, 1) *
      amp[764] - Complex<double> (0, 1) * amp[765] - amp[773] - amp[775] -
      amp[777] - amp[778] - amp[779] - amp[780] - amp[781] - amp[782] -
      amp[784] - amp[787] - amp[788] - amp[789] + amp[790] + amp[791] -
      Complex<double> (0, 1) * amp[792] + amp[794] + amp[795] - Complex<double>
      (0, 1) * amp[796] + amp[798] + amp[801] - Complex<double> (0, 1) *
      amp[803] + amp[804] - Complex<double> (0, 1) * amp[805] - Complex<double>
      (0, 1) * amp[806] - amp[842] - amp[844] - amp[852] - amp[861] - amp[862]
      - amp[866] + amp[867] - amp[869] + amp[870] - amp[872] + amp[873] -
      amp[875] + amp[876] - amp[878] + amp[879] - amp[881] - amp[884] +
      amp[882] - amp[887] + amp[885];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[3][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}


}  // end namespace PY8MEs_namespace

