//==========================================================================
// This file has been automatically generated for Pythia 8 by
// MadGraph5_aMC@NLO v. 2.6.0, 2017-08-16
// By the MadGraph5_aMC@NLO Development Team
// Visit launchpad.net/madgraph5 and amcatnlo.web.cern.ch
//==========================================================================

#include "PY8MEs_R12_P10_sm_qq_zzgqq.h"
#include "HelAmps_sm.h"

using namespace Pythia8_sm; 

namespace PY8MEs_namespace 
{
//==========================================================================
// Class member functions for calculating the matrix elements for
// Process: u u > z z g u u WEIGHTED<=7 @12
// Process: c c > z z g c c WEIGHTED<=7 @12
// Process: u u~ > z z g u u~ WEIGHTED<=7 @12
// Process: c c~ > z z g c c~ WEIGHTED<=7 @12
// Process: d d > z z g d d WEIGHTED<=7 @12
// Process: s s > z z g s s WEIGHTED<=7 @12
// Process: d d~ > z z g d d~ WEIGHTED<=7 @12
// Process: s s~ > z z g s s~ WEIGHTED<=7 @12
// Process: u~ u~ > z z g u~ u~ WEIGHTED<=7 @12
// Process: c~ c~ > z z g c~ c~ WEIGHTED<=7 @12
// Process: d~ d~ > z z g d~ d~ WEIGHTED<=7 @12
// Process: s~ s~ > z z g s~ s~ WEIGHTED<=7 @12
// Process: u c > z z g u c WEIGHTED<=7 @12
// Process: u d > z z g u d WEIGHTED<=7 @12
// Process: u s > z z g u s WEIGHTED<=7 @12
// Process: c d > z z g c d WEIGHTED<=7 @12
// Process: c s > z z g c s WEIGHTED<=7 @12
// Process: u u~ > z z g c c~ WEIGHTED<=7 @12
// Process: c c~ > z z g u u~ WEIGHTED<=7 @12
// Process: u u~ > z z g d d~ WEIGHTED<=7 @12
// Process: u u~ > z z g s s~ WEIGHTED<=7 @12
// Process: c c~ > z z g d d~ WEIGHTED<=7 @12
// Process: c c~ > z z g s s~ WEIGHTED<=7 @12
// Process: u c~ > z z g u c~ WEIGHTED<=7 @12
// Process: c u~ > z z g c u~ WEIGHTED<=7 @12
// Process: u d~ > z z g u d~ WEIGHTED<=7 @12
// Process: u s~ > z z g u s~ WEIGHTED<=7 @12
// Process: c d~ > z z g c d~ WEIGHTED<=7 @12
// Process: c s~ > z z g c s~ WEIGHTED<=7 @12
// Process: d s > z z g d s WEIGHTED<=7 @12
// Process: d u~ > z z g d u~ WEIGHTED<=7 @12
// Process: d c~ > z z g d c~ WEIGHTED<=7 @12
// Process: s u~ > z z g s u~ WEIGHTED<=7 @12
// Process: s c~ > z z g s c~ WEIGHTED<=7 @12
// Process: d d~ > z z g u u~ WEIGHTED<=7 @12
// Process: d d~ > z z g c c~ WEIGHTED<=7 @12
// Process: s s~ > z z g u u~ WEIGHTED<=7 @12
// Process: s s~ > z z g c c~ WEIGHTED<=7 @12
// Process: d d~ > z z g s s~ WEIGHTED<=7 @12
// Process: s s~ > z z g d d~ WEIGHTED<=7 @12
// Process: d s~ > z z g d s~ WEIGHTED<=7 @12
// Process: s d~ > z z g s d~ WEIGHTED<=7 @12
// Process: u~ c~ > z z g u~ c~ WEIGHTED<=7 @12
// Process: u~ d~ > z z g u~ d~ WEIGHTED<=7 @12
// Process: u~ s~ > z z g u~ s~ WEIGHTED<=7 @12
// Process: c~ d~ > z z g c~ d~ WEIGHTED<=7 @12
// Process: c~ s~ > z z g c~ s~ WEIGHTED<=7 @12
// Process: d~ s~ > z z g d~ s~ WEIGHTED<=7 @12

// Exception class
class PY8MEs_R12_P10_sm_qq_zzgqqException : public exception
{
  virtual const char * what() const throw()
  {
    return "Exception in class 'PY8MEs_R12_P10_sm_qq_zzgqq'."; 
  }
}
PY8MEs_R12_P10_sm_qq_zzgqq_exception; 

std::set<int> PY8MEs_R12_P10_sm_qq_zzgqq::s_channel_proc = std::set<int>
    (createset<int> ());

int PY8MEs_R12_P10_sm_qq_zzgqq::helicities[ncomb][nexternal] = {{-1, -1, -1,
    -1, -1, -1, -1}, {-1, -1, -1, -1, -1, -1, 1}, {-1, -1, -1, -1, -1, 1, -1},
    {-1, -1, -1, -1, -1, 1, 1}, {-1, -1, -1, -1, 1, -1, -1}, {-1, -1, -1, -1,
    1, -1, 1}, {-1, -1, -1, -1, 1, 1, -1}, {-1, -1, -1, -1, 1, 1, 1}, {-1, -1,
    -1, 0, -1, -1, -1}, {-1, -1, -1, 0, -1, -1, 1}, {-1, -1, -1, 0, -1, 1, -1},
    {-1, -1, -1, 0, -1, 1, 1}, {-1, -1, -1, 0, 1, -1, -1}, {-1, -1, -1, 0, 1,
    -1, 1}, {-1, -1, -1, 0, 1, 1, -1}, {-1, -1, -1, 0, 1, 1, 1}, {-1, -1, -1,
    1, -1, -1, -1}, {-1, -1, -1, 1, -1, -1, 1}, {-1, -1, -1, 1, -1, 1, -1},
    {-1, -1, -1, 1, -1, 1, 1}, {-1, -1, -1, 1, 1, -1, -1}, {-1, -1, -1, 1, 1,
    -1, 1}, {-1, -1, -1, 1, 1, 1, -1}, {-1, -1, -1, 1, 1, 1, 1}, {-1, -1, 0,
    -1, -1, -1, -1}, {-1, -1, 0, -1, -1, -1, 1}, {-1, -1, 0, -1, -1, 1, -1},
    {-1, -1, 0, -1, -1, 1, 1}, {-1, -1, 0, -1, 1, -1, -1}, {-1, -1, 0, -1, 1,
    -1, 1}, {-1, -1, 0, -1, 1, 1, -1}, {-1, -1, 0, -1, 1, 1, 1}, {-1, -1, 0, 0,
    -1, -1, -1}, {-1, -1, 0, 0, -1, -1, 1}, {-1, -1, 0, 0, -1, 1, -1}, {-1, -1,
    0, 0, -1, 1, 1}, {-1, -1, 0, 0, 1, -1, -1}, {-1, -1, 0, 0, 1, -1, 1}, {-1,
    -1, 0, 0, 1, 1, -1}, {-1, -1, 0, 0, 1, 1, 1}, {-1, -1, 0, 1, -1, -1, -1},
    {-1, -1, 0, 1, -1, -1, 1}, {-1, -1, 0, 1, -1, 1, -1}, {-1, -1, 0, 1, -1, 1,
    1}, {-1, -1, 0, 1, 1, -1, -1}, {-1, -1, 0, 1, 1, -1, 1}, {-1, -1, 0, 1, 1,
    1, -1}, {-1, -1, 0, 1, 1, 1, 1}, {-1, -1, 1, -1, -1, -1, -1}, {-1, -1, 1,
    -1, -1, -1, 1}, {-1, -1, 1, -1, -1, 1, -1}, {-1, -1, 1, -1, -1, 1, 1}, {-1,
    -1, 1, -1, 1, -1, -1}, {-1, -1, 1, -1, 1, -1, 1}, {-1, -1, 1, -1, 1, 1,
    -1}, {-1, -1, 1, -1, 1, 1, 1}, {-1, -1, 1, 0, -1, -1, -1}, {-1, -1, 1, 0,
    -1, -1, 1}, {-1, -1, 1, 0, -1, 1, -1}, {-1, -1, 1, 0, -1, 1, 1}, {-1, -1,
    1, 0, 1, -1, -1}, {-1, -1, 1, 0, 1, -1, 1}, {-1, -1, 1, 0, 1, 1, -1}, {-1,
    -1, 1, 0, 1, 1, 1}, {-1, -1, 1, 1, -1, -1, -1}, {-1, -1, 1, 1, -1, -1, 1},
    {-1, -1, 1, 1, -1, 1, -1}, {-1, -1, 1, 1, -1, 1, 1}, {-1, -1, 1, 1, 1, -1,
    -1}, {-1, -1, 1, 1, 1, -1, 1}, {-1, -1, 1, 1, 1, 1, -1}, {-1, -1, 1, 1, 1,
    1, 1}, {-1, 1, -1, -1, -1, -1, -1}, {-1, 1, -1, -1, -1, -1, 1}, {-1, 1, -1,
    -1, -1, 1, -1}, {-1, 1, -1, -1, -1, 1, 1}, {-1, 1, -1, -1, 1, -1, -1}, {-1,
    1, -1, -1, 1, -1, 1}, {-1, 1, -1, -1, 1, 1, -1}, {-1, 1, -1, -1, 1, 1, 1},
    {-1, 1, -1, 0, -1, -1, -1}, {-1, 1, -1, 0, -1, -1, 1}, {-1, 1, -1, 0, -1,
    1, -1}, {-1, 1, -1, 0, -1, 1, 1}, {-1, 1, -1, 0, 1, -1, -1}, {-1, 1, -1, 0,
    1, -1, 1}, {-1, 1, -1, 0, 1, 1, -1}, {-1, 1, -1, 0, 1, 1, 1}, {-1, 1, -1,
    1, -1, -1, -1}, {-1, 1, -1, 1, -1, -1, 1}, {-1, 1, -1, 1, -1, 1, -1}, {-1,
    1, -1, 1, -1, 1, 1}, {-1, 1, -1, 1, 1, -1, -1}, {-1, 1, -1, 1, 1, -1, 1},
    {-1, 1, -1, 1, 1, 1, -1}, {-1, 1, -1, 1, 1, 1, 1}, {-1, 1, 0, -1, -1, -1,
    -1}, {-1, 1, 0, -1, -1, -1, 1}, {-1, 1, 0, -1, -1, 1, -1}, {-1, 1, 0, -1,
    -1, 1, 1}, {-1, 1, 0, -1, 1, -1, -1}, {-1, 1, 0, -1, 1, -1, 1}, {-1, 1, 0,
    -1, 1, 1, -1}, {-1, 1, 0, -1, 1, 1, 1}, {-1, 1, 0, 0, -1, -1, -1}, {-1, 1,
    0, 0, -1, -1, 1}, {-1, 1, 0, 0, -1, 1, -1}, {-1, 1, 0, 0, -1, 1, 1}, {-1,
    1, 0, 0, 1, -1, -1}, {-1, 1, 0, 0, 1, -1, 1}, {-1, 1, 0, 0, 1, 1, -1}, {-1,
    1, 0, 0, 1, 1, 1}, {-1, 1, 0, 1, -1, -1, -1}, {-1, 1, 0, 1, -1, -1, 1},
    {-1, 1, 0, 1, -1, 1, -1}, {-1, 1, 0, 1, -1, 1, 1}, {-1, 1, 0, 1, 1, -1,
    -1}, {-1, 1, 0, 1, 1, -1, 1}, {-1, 1, 0, 1, 1, 1, -1}, {-1, 1, 0, 1, 1, 1,
    1}, {-1, 1, 1, -1, -1, -1, -1}, {-1, 1, 1, -1, -1, -1, 1}, {-1, 1, 1, -1,
    -1, 1, -1}, {-1, 1, 1, -1, -1, 1, 1}, {-1, 1, 1, -1, 1, -1, -1}, {-1, 1, 1,
    -1, 1, -1, 1}, {-1, 1, 1, -1, 1, 1, -1}, {-1, 1, 1, -1, 1, 1, 1}, {-1, 1,
    1, 0, -1, -1, -1}, {-1, 1, 1, 0, -1, -1, 1}, {-1, 1, 1, 0, -1, 1, -1}, {-1,
    1, 1, 0, -1, 1, 1}, {-1, 1, 1, 0, 1, -1, -1}, {-1, 1, 1, 0, 1, -1, 1}, {-1,
    1, 1, 0, 1, 1, -1}, {-1, 1, 1, 0, 1, 1, 1}, {-1, 1, 1, 1, -1, -1, -1}, {-1,
    1, 1, 1, -1, -1, 1}, {-1, 1, 1, 1, -1, 1, -1}, {-1, 1, 1, 1, -1, 1, 1},
    {-1, 1, 1, 1, 1, -1, -1}, {-1, 1, 1, 1, 1, -1, 1}, {-1, 1, 1, 1, 1, 1, -1},
    {-1, 1, 1, 1, 1, 1, 1}, {1, -1, -1, -1, -1, -1, -1}, {1, -1, -1, -1, -1,
    -1, 1}, {1, -1, -1, -1, -1, 1, -1}, {1, -1, -1, -1, -1, 1, 1}, {1, -1, -1,
    -1, 1, -1, -1}, {1, -1, -1, -1, 1, -1, 1}, {1, -1, -1, -1, 1, 1, -1}, {1,
    -1, -1, -1, 1, 1, 1}, {1, -1, -1, 0, -1, -1, -1}, {1, -1, -1, 0, -1, -1,
    1}, {1, -1, -1, 0, -1, 1, -1}, {1, -1, -1, 0, -1, 1, 1}, {1, -1, -1, 0, 1,
    -1, -1}, {1, -1, -1, 0, 1, -1, 1}, {1, -1, -1, 0, 1, 1, -1}, {1, -1, -1, 0,
    1, 1, 1}, {1, -1, -1, 1, -1, -1, -1}, {1, -1, -1, 1, -1, -1, 1}, {1, -1,
    -1, 1, -1, 1, -1}, {1, -1, -1, 1, -1, 1, 1}, {1, -1, -1, 1, 1, -1, -1}, {1,
    -1, -1, 1, 1, -1, 1}, {1, -1, -1, 1, 1, 1, -1}, {1, -1, -1, 1, 1, 1, 1},
    {1, -1, 0, -1, -1, -1, -1}, {1, -1, 0, -1, -1, -1, 1}, {1, -1, 0, -1, -1,
    1, -1}, {1, -1, 0, -1, -1, 1, 1}, {1, -1, 0, -1, 1, -1, -1}, {1, -1, 0, -1,
    1, -1, 1}, {1, -1, 0, -1, 1, 1, -1}, {1, -1, 0, -1, 1, 1, 1}, {1, -1, 0, 0,
    -1, -1, -1}, {1, -1, 0, 0, -1, -1, 1}, {1, -1, 0, 0, -1, 1, -1}, {1, -1, 0,
    0, -1, 1, 1}, {1, -1, 0, 0, 1, -1, -1}, {1, -1, 0, 0, 1, -1, 1}, {1, -1, 0,
    0, 1, 1, -1}, {1, -1, 0, 0, 1, 1, 1}, {1, -1, 0, 1, -1, -1, -1}, {1, -1, 0,
    1, -1, -1, 1}, {1, -1, 0, 1, -1, 1, -1}, {1, -1, 0, 1, -1, 1, 1}, {1, -1,
    0, 1, 1, -1, -1}, {1, -1, 0, 1, 1, -1, 1}, {1, -1, 0, 1, 1, 1, -1}, {1, -1,
    0, 1, 1, 1, 1}, {1, -1, 1, -1, -1, -1, -1}, {1, -1, 1, -1, -1, -1, 1}, {1,
    -1, 1, -1, -1, 1, -1}, {1, -1, 1, -1, -1, 1, 1}, {1, -1, 1, -1, 1, -1, -1},
    {1, -1, 1, -1, 1, -1, 1}, {1, -1, 1, -1, 1, 1, -1}, {1, -1, 1, -1, 1, 1,
    1}, {1, -1, 1, 0, -1, -1, -1}, {1, -1, 1, 0, -1, -1, 1}, {1, -1, 1, 0, -1,
    1, -1}, {1, -1, 1, 0, -1, 1, 1}, {1, -1, 1, 0, 1, -1, -1}, {1, -1, 1, 0, 1,
    -1, 1}, {1, -1, 1, 0, 1, 1, -1}, {1, -1, 1, 0, 1, 1, 1}, {1, -1, 1, 1, -1,
    -1, -1}, {1, -1, 1, 1, -1, -1, 1}, {1, -1, 1, 1, -1, 1, -1}, {1, -1, 1, 1,
    -1, 1, 1}, {1, -1, 1, 1, 1, -1, -1}, {1, -1, 1, 1, 1, -1, 1}, {1, -1, 1, 1,
    1, 1, -1}, {1, -1, 1, 1, 1, 1, 1}, {1, 1, -1, -1, -1, -1, -1}, {1, 1, -1,
    -1, -1, -1, 1}, {1, 1, -1, -1, -1, 1, -1}, {1, 1, -1, -1, -1, 1, 1}, {1, 1,
    -1, -1, 1, -1, -1}, {1, 1, -1, -1, 1, -1, 1}, {1, 1, -1, -1, 1, 1, -1}, {1,
    1, -1, -1, 1, 1, 1}, {1, 1, -1, 0, -1, -1, -1}, {1, 1, -1, 0, -1, -1, 1},
    {1, 1, -1, 0, -1, 1, -1}, {1, 1, -1, 0, -1, 1, 1}, {1, 1, -1, 0, 1, -1,
    -1}, {1, 1, -1, 0, 1, -1, 1}, {1, 1, -1, 0, 1, 1, -1}, {1, 1, -1, 0, 1, 1,
    1}, {1, 1, -1, 1, -1, -1, -1}, {1, 1, -1, 1, -1, -1, 1}, {1, 1, -1, 1, -1,
    1, -1}, {1, 1, -1, 1, -1, 1, 1}, {1, 1, -1, 1, 1, -1, -1}, {1, 1, -1, 1, 1,
    -1, 1}, {1, 1, -1, 1, 1, 1, -1}, {1, 1, -1, 1, 1, 1, 1}, {1, 1, 0, -1, -1,
    -1, -1}, {1, 1, 0, -1, -1, -1, 1}, {1, 1, 0, -1, -1, 1, -1}, {1, 1, 0, -1,
    -1, 1, 1}, {1, 1, 0, -1, 1, -1, -1}, {1, 1, 0, -1, 1, -1, 1}, {1, 1, 0, -1,
    1, 1, -1}, {1, 1, 0, -1, 1, 1, 1}, {1, 1, 0, 0, -1, -1, -1}, {1, 1, 0, 0,
    -1, -1, 1}, {1, 1, 0, 0, -1, 1, -1}, {1, 1, 0, 0, -1, 1, 1}, {1, 1, 0, 0,
    1, -1, -1}, {1, 1, 0, 0, 1, -1, 1}, {1, 1, 0, 0, 1, 1, -1}, {1, 1, 0, 0, 1,
    1, 1}, {1, 1, 0, 1, -1, -1, -1}, {1, 1, 0, 1, -1, -1, 1}, {1, 1, 0, 1, -1,
    1, -1}, {1, 1, 0, 1, -1, 1, 1}, {1, 1, 0, 1, 1, -1, -1}, {1, 1, 0, 1, 1,
    -1, 1}, {1, 1, 0, 1, 1, 1, -1}, {1, 1, 0, 1, 1, 1, 1}, {1, 1, 1, -1, -1,
    -1, -1}, {1, 1, 1, -1, -1, -1, 1}, {1, 1, 1, -1, -1, 1, -1}, {1, 1, 1, -1,
    -1, 1, 1}, {1, 1, 1, -1, 1, -1, -1}, {1, 1, 1, -1, 1, -1, 1}, {1, 1, 1, -1,
    1, 1, -1}, {1, 1, 1, -1, 1, 1, 1}, {1, 1, 1, 0, -1, -1, -1}, {1, 1, 1, 0,
    -1, -1, 1}, {1, 1, 1, 0, -1, 1, -1}, {1, 1, 1, 0, -1, 1, 1}, {1, 1, 1, 0,
    1, -1, -1}, {1, 1, 1, 0, 1, -1, 1}, {1, 1, 1, 0, 1, 1, -1}, {1, 1, 1, 0, 1,
    1, 1}, {1, 1, 1, 1, -1, -1, -1}, {1, 1, 1, 1, -1, -1, 1}, {1, 1, 1, 1, -1,
    1, -1}, {1, 1, 1, 1, -1, 1, 1}, {1, 1, 1, 1, 1, -1, -1}, {1, 1, 1, 1, 1,
    -1, 1}, {1, 1, 1, 1, 1, 1, -1}, {1, 1, 1, 1, 1, 1, 1}};

// Normalization factors the various processes
// Denominators: spins, colors and identical particles
int PY8MEs_R12_P10_sm_qq_zzgqq::denom_colors[nprocesses] = {9, 9, 9, 9, 9, 9,
    9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9};
int PY8MEs_R12_P10_sm_qq_zzgqq::denom_hels[nprocesses] = {4, 4, 4, 4, 4, 4, 4,
    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4};
int PY8MEs_R12_P10_sm_qq_zzgqq::denom_iden[nprocesses] = {4, 2, 4, 2, 4, 4, 2,
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2};

//--------------------------------------------------------------------------
// Color config initialization
void PY8MEs_R12_P10_sm_qq_zzgqq::initColorConfigs() 
{
  color_configs = vector < vec_vec_int > (); 
  jamp_nc_relative_power = vector < vec_int > (); 

  // Color flows of process Process: u u > z z g u u WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #1
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #2
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #3
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 

  // Color flows of process Process: u u~ > z z g u u~ WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #1
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #2
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #3
  color_configs[1].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[1].push_back(0); 

  // Color flows of process Process: d d > z z g d d WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[2].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #1
  color_configs[2].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #2
  color_configs[2].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #3
  color_configs[2].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[2].push_back(0); 

  // Color flows of process Process: d d~ > z z g d d~ WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[3].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #1
  color_configs[3].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #2
  color_configs[3].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #3
  color_configs[3].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[3].push_back(0); 

  // Color flows of process Process: u~ u~ > z z g u~ u~ WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[4].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[4].push_back(0); 
  // JAMP #1
  color_configs[4].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[4].push_back(0); 
  // JAMP #2
  color_configs[4].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[4].push_back(0); 
  // JAMP #3
  color_configs[4].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[4].push_back(0); 

  // Color flows of process Process: d~ d~ > z z g d~ d~ WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[5].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[5].push_back(0); 
  // JAMP #1
  color_configs[5].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[5].push_back(0); 
  // JAMP #2
  color_configs[5].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[5].push_back(0); 
  // JAMP #3
  color_configs[5].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[5].push_back(0); 

  // Color flows of process Process: u c > z z g u c WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[6].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[6].push_back(-1); 
  // JAMP #1
  color_configs[6].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[6].push_back(0); 
  // JAMP #2
  color_configs[6].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[6].push_back(0); 
  // JAMP #3
  color_configs[6].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[6].push_back(-1); 

  // Color flows of process Process: u d > z z g u d WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[7].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[7].push_back(-1); 
  // JAMP #1
  color_configs[7].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[7].push_back(0); 
  // JAMP #2
  color_configs[7].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[7].push_back(0); 
  // JAMP #3
  color_configs[7].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[7].push_back(-1); 

  // Color flows of process Process: u u~ > z z g c c~ WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[8].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[8].push_back(-1); 
  // JAMP #1
  color_configs[8].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[8].push_back(0); 
  // JAMP #2
  color_configs[8].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[8].push_back(-1); 
  // JAMP #3
  color_configs[8].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[8].push_back(0); 

  // Color flows of process Process: u u~ > z z g d d~ WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[9].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[9].push_back(-1); 
  // JAMP #1
  color_configs[9].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[9].push_back(0); 
  // JAMP #2
  color_configs[9].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[9].push_back(-1); 
  // JAMP #3
  color_configs[9].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[9].push_back(0); 

  // Color flows of process Process: u c~ > z z g u c~ WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[10].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[10].push_back(0); 
  // JAMP #1
  color_configs[10].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[10].push_back(-1); 
  // JAMP #2
  color_configs[10].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[10].push_back(0); 
  // JAMP #3
  color_configs[10].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[10].push_back(-1); 

  // Color flows of process Process: u d~ > z z g u d~ WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[11].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[11].push_back(0); 
  // JAMP #1
  color_configs[11].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[11].push_back(-1); 
  // JAMP #2
  color_configs[11].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[11].push_back(0); 
  // JAMP #3
  color_configs[11].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[11].push_back(-1); 

  // Color flows of process Process: d s > z z g d s WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[12].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[12].push_back(-1); 
  // JAMP #1
  color_configs[12].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[12].push_back(0); 
  // JAMP #2
  color_configs[12].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[12].push_back(0); 
  // JAMP #3
  color_configs[12].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[12].push_back(-1); 

  // Color flows of process Process: d u~ > z z g d u~ WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[13].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[13].push_back(0); 
  // JAMP #1
  color_configs[13].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[13].push_back(-1); 
  // JAMP #2
  color_configs[13].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[13].push_back(0); 
  // JAMP #3
  color_configs[13].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[13].push_back(-1); 

  // Color flows of process Process: d d~ > z z g u u~ WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[14].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[14].push_back(-1); 
  // JAMP #1
  color_configs[14].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[14].push_back(0); 
  // JAMP #2
  color_configs[14].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[14].push_back(-1); 
  // JAMP #3
  color_configs[14].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[14].push_back(0); 

  // Color flows of process Process: d d~ > z z g s s~ WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[15].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[15].push_back(-1); 
  // JAMP #1
  color_configs[15].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[15].push_back(0); 
  // JAMP #2
  color_configs[15].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[15].push_back(-1); 
  // JAMP #3
  color_configs[15].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[15].push_back(0); 

  // Color flows of process Process: d s~ > z z g d s~ WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[16].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[16].push_back(0); 
  // JAMP #1
  color_configs[16].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[16].push_back(-1); 
  // JAMP #2
  color_configs[16].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[16].push_back(0); 
  // JAMP #3
  color_configs[16].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[16].push_back(-1); 

  // Color flows of process Process: u~ c~ > z z g u~ c~ WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[17].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[17].push_back(-1); 
  // JAMP #1
  color_configs[17].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[17].push_back(0); 
  // JAMP #2
  color_configs[17].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[17].push_back(0); 
  // JAMP #3
  color_configs[17].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[17].push_back(-1); 

  // Color flows of process Process: u~ d~ > z z g u~ d~ WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[18].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[18].push_back(-1); 
  // JAMP #1
  color_configs[18].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[18].push_back(0); 
  // JAMP #2
  color_configs[18].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[18].push_back(0); 
  // JAMP #3
  color_configs[18].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[18].push_back(-1); 

  // Color flows of process Process: d~ s~ > z z g d~ s~ WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[19].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[19].push_back(-1); 
  // JAMP #1
  color_configs[19].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[19].push_back(0); 
  // JAMP #2
  color_configs[19].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[19].push_back(0); 
  // JAMP #3
  color_configs[19].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[19].push_back(-1); 
}

//--------------------------------------------------------------------------
// Destructor.
PY8MEs_R12_P10_sm_qq_zzgqq::~PY8MEs_R12_P10_sm_qq_zzgqq() 
{
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    delete[] p[i]; 
    p[i] = NULL; 
  }
}

//--------------------------------------------------------------------------
// Invert the permutation mapping
vector<int> PY8MEs_R12_P10_sm_qq_zzgqq::invert_mapping(vector<int> mapping) 
{
  vector<int> inverted_mapping; 
  for (unsigned int i = 0; i < mapping.size(); i++ )
  {
    for (unsigned int j = 0; j < mapping.size(); j++ )
    {
      if (mapping[j] == ((int)i))
      {
        inverted_mapping.push_back(j); 
        break; 
      }
    }
  }
  return inverted_mapping; 
}

//--------------------------------------------------------------------------
// Return the list of possible helicity configurations
vector < vec_int > PY8MEs_R12_P10_sm_qq_zzgqq::getHelicityConfigs(vector<int>
    permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(ncomb, vector<int> (nexternal, 0)); 
  for (unsigned int ihel = 0; ihel < ncomb; ihel++ )
  {
    for(unsigned int j = 0; j < nexternal; j++ )
    {
      res[ihel][chosenPerm[j]] = helicities[ihel][j]; 
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Return the list of possible color configurations
vector < vec_int > PY8MEs_R12_P10_sm_qq_zzgqq::getColorConfigs(int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(color_configs[chosenProcID].size(), vector<int>
      (nexternal * 2, 0));
  for (unsigned int icol = 0; icol < color_configs[chosenProcID].size(); icol++
      )
  {
    for(unsigned int j = 0; j < (2 * nexternal); j++ )
    {
      res[icol][chosenPerm[j/2] * 2 + j%2] =
          color_configs[chosenProcID][icol][j];
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Get JAMP relative N_c power
int PY8MEs_R12_P10_sm_qq_zzgqq::getColorFlowRelativeNCPower(int color_flow_ID,
    int specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return jamp_nc_relative_power[chosenProcID][color_flow_ID]; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity ID -> Helicity Config
vector<int> PY8MEs_R12_P10_sm_qq_zzgqq::getHelicityConfigForID(int hel_ID,
    vector<int> permutation)
{
  if (hel_ID < 0 || hel_ID >= ncomb)
  {
    cerr <<  "Error in function 'getHelicityConfigForID' of class" << 
    " 'PY8MEs_R12_P10_sm_qq_zzgqq': Specified helicity ID '" << 
    hel_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R12_P10_sm_qq_zzgqq_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(nexternal, 0); 
  for (unsigned int j = 0; j < nexternal; j++ )
  {
    res[chosenPerm[j]] = helicities[hel_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity Config -> Helicity ID
int PY8MEs_R12_P10_sm_qq_zzgqq::getHelicityIDForConfig(vector<int> hel_config,
    vector<int> permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  int user_ihel = -1; 
  if (hel_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < ncomb; i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < nexternal; j++ )
      {
        if (helicities[i][chosenPerm[j]] != hel_config[j])
        {
          found = false; 
          break; 
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_ihel = i; 
        break; 
      }
    }
    if (user_ihel == -1)
    {
      cerr <<  "Error in function 'getHelicityIDForConfig' of class" << 
      " 'PY8MEs_R12_P10_sm_qq_zzgqq': Specified helicity" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R12_P10_sm_qq_zzgqq_exception; 
    }
  }
  return user_ihel; 
}


//--------------------------------------------------------------------------
// Implements the map Color ID -> Color Config
vector<int> PY8MEs_R12_P10_sm_qq_zzgqq::getColorConfigForID(int color_ID, int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < 0 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getColorConfigForID' of class" << 
    " 'PY8MEs_R12_P10_sm_qq_zzgqq': Specified color ID '" << 
    color_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R12_P10_sm_qq_zzgqq_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(color_configs[chosenProcID][color_ID].size(), 0); 
  for (unsigned int j = 0; j < (2 * nexternal); j++ )
  {
    res[chosenPerm[j/2] * 2 + j%2] = color_configs[chosenProcID][color_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Color Config -> Color ID
int PY8MEs_R12_P10_sm_qq_zzgqq::getColorIDForConfig(vector<int> color_config,
    int specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = -1; 
  if (color_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < color_configs[chosenProcID].size(); i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < (nexternal * 2); j++ )
      {

        // If colorless then make sure it matches
        // The little arithmetics in the color index is just
        // the permutation applies on the particle list which is
        // twice smaller since each particle can have two color indices.
        if (color_config[j] == 0)
        {
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] != 0)
          {
            found = false; 
            break; 
          }
          // Otherwise check that the color linked position matches
        }
        else
        {
          int color_linked_pos = -1; 
          // Find the other end of the line in the user color config
          for (unsigned int k = 0; k < (nexternal * 2); k++ )
          {
            if (k == j)
              continue; 
            if (color_config[j] == color_config[k])
            {
              color_linked_pos = k; 
              break; 
            }
          }
          if (color_linked_pos == -1)
          {
            cerr <<  "Error in function 'getColorIDForConfig' of class" << 
            " 'PY8MEs_R12_P10_sm_qq_zzgqq': A color line could " << 
            " not be closed." << endl; 
            throw PY8MEs_R12_P10_sm_qq_zzgqq_exception; 
          }
          // Now check whether the color line matches
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] !=
              color_configs[chosenProcID][i][chosenPerm[color_linked_pos/2] * 2
              + color_linked_pos%2])
          {
            found = false; 
            break; 
          }
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_icol = i; 
        break; 
      }
    }

    if (user_icol == -1)
    {
      cerr <<  "Error in function 'getColorIDForConfig' of class" << 
      " 'PY8MEs_R12_P10_sm_qq_zzgqq': Specified color" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R12_P10_sm_qq_zzgqq_exception; 
    }
  }
  return user_icol; 
}

//--------------------------------------------------------------------------
// Returns all result previously computed in SigmaKin
vector < vec_double > PY8MEs_R12_P10_sm_qq_zzgqq::getAllResults(int
    specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return all_results[chosenProcID]; 
}

//--------------------------------------------------------------------------
// Returns a result previously computed in SigmaKin for a specific helicity
// and color ID. -1 means avg and summed over that characteristic.
double PY8MEs_R12_P10_sm_qq_zzgqq::getResult(int helicity_ID, int color_ID, int
    specify_proc_ID)
{
  if (helicity_ID < - 1 || helicity_ID >= ncomb)
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R12_P10_sm_qq_zzgqq': Specified helicity ID '" << 
    helicity_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R12_P10_sm_qq_zzgqq_exception; 
  }
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < - 1 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R12_P10_sm_qq_zzgqq': Specified color ID '" << 
    color_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R12_P10_sm_qq_zzgqq_exception; 
  }
  return all_results[chosenProcID][helicity_ID + 1][color_ID + 1]; 
}

//--------------------------------------------------------------------------
// Check for the availability of the requested process and if available,
// If available, this returns the corresponding permutation and Proc_ID to use.
// If not available, this returns a negative Proc_ID.
pair < vector<int> , int > PY8MEs_R12_P10_sm_qq_zzgqq::static_getPY8ME(vector<int> initial_pdgs, vector<int> final_pdgs, set<int> schannels) 
{

  // Not available return value
  pair < vector<int> , int > NA(vector<int> (), -1); 

  // Check if s-channel requirements match
  if (nreq_s_channels > 0)
  {
    if (schannels != s_channel_proc)
      return NA; 
  }
  else
  {
    if (schannels.size() != 0)
      return NA; 
  }

  // Check number of final state particles
  if (final_pdgs.size() != (nexternal - ninitial))
    return NA; 

  // Check number of initial state particles
  if (initial_pdgs.size() != ninitial)
    return NA; 

  // List of processes available in this class
  const int nprocs = 88; 
  const int proc_IDS[nprocs] = {0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 7, 7, 7,
      7, 8, 8, 9, 9, 9, 9, 10, 10, 11, 11, 11, 11, 12, 13, 13, 13, 13, 14, 14,
      14, 14, 15, 15, 16, 16, 17, 18, 18, 18, 18, 19, 1, 1, 3, 3, 6, 7, 7, 7,
      7, 8, 8, 9, 9, 9, 9, 10, 10, 11, 11, 11, 11, 12, 13, 13, 13, 13, 14, 14,
      14, 14, 15, 15, 16, 16, 17, 18, 18, 18, 18, 19};
  const int in_pdgs[nprocs][ninitial] = {{2, 2}, {4, 4}, {2, -2}, {4, -4}, {1,
      1}, {3, 3}, {1, -1}, {3, -3}, {-2, -2}, {-4, -4}, {-1, -1}, {-3, -3}, {2,
      4}, {2, 1}, {2, 3}, {4, 1}, {4, 3}, {2, -2}, {4, -4}, {2, -2}, {2, -2},
      {4, -4}, {4, -4}, {2, -4}, {4, -2}, {2, -1}, {2, -3}, {4, -1}, {4, -3},
      {1, 3}, {1, -2}, {1, -4}, {3, -2}, {3, -4}, {1, -1}, {1, -1}, {3, -3},
      {3, -3}, {1, -1}, {3, -3}, {1, -3}, {3, -1}, {-2, -4}, {-2, -1}, {-2,
      -3}, {-4, -1}, {-4, -3}, {-1, -3}, {-2, 2}, {-4, 4}, {-1, 1}, {-3, 3},
      {4, 2}, {1, 2}, {3, 2}, {1, 4}, {3, 4}, {-2, 2}, {-4, 4}, {-2, 2}, {-2,
      2}, {-4, 4}, {-4, 4}, {-4, 2}, {-2, 4}, {-1, 2}, {-3, 2}, {-1, 4}, {-3,
      4}, {3, 1}, {-2, 1}, {-4, 1}, {-2, 3}, {-4, 3}, {-1, 1}, {-1, 1}, {-3,
      3}, {-3, 3}, {-1, 1}, {-3, 3}, {-3, 1}, {-1, 3}, {-4, -2}, {-1, -2}, {-3,
      -2}, {-1, -4}, {-3, -4}, {-3, -1}};
  const int out_pdgs[nprocs][nexternal - ninitial] = {{23, 23, 21, 2, 2}, {23,
      23, 21, 4, 4}, {23, 23, 21, 2, -2}, {23, 23, 21, 4, -4}, {23, 23, 21, 1,
      1}, {23, 23, 21, 3, 3}, {23, 23, 21, 1, -1}, {23, 23, 21, 3, -3}, {23,
      23, 21, -2, -2}, {23, 23, 21, -4, -4}, {23, 23, 21, -1, -1}, {23, 23, 21,
      -3, -3}, {23, 23, 21, 2, 4}, {23, 23, 21, 2, 1}, {23, 23, 21, 2, 3}, {23,
      23, 21, 4, 1}, {23, 23, 21, 4, 3}, {23, 23, 21, 4, -4}, {23, 23, 21, 2,
      -2}, {23, 23, 21, 1, -1}, {23, 23, 21, 3, -3}, {23, 23, 21, 1, -1}, {23,
      23, 21, 3, -3}, {23, 23, 21, 2, -4}, {23, 23, 21, 4, -2}, {23, 23, 21, 2,
      -1}, {23, 23, 21, 2, -3}, {23, 23, 21, 4, -1}, {23, 23, 21, 4, -3}, {23,
      23, 21, 1, 3}, {23, 23, 21, 1, -2}, {23, 23, 21, 1, -4}, {23, 23, 21, 3,
      -2}, {23, 23, 21, 3, -4}, {23, 23, 21, 2, -2}, {23, 23, 21, 4, -4}, {23,
      23, 21, 2, -2}, {23, 23, 21, 4, -4}, {23, 23, 21, 3, -3}, {23, 23, 21, 1,
      -1}, {23, 23, 21, 1, -3}, {23, 23, 21, 3, -1}, {23, 23, 21, -2, -4}, {23,
      23, 21, -2, -1}, {23, 23, 21, -2, -3}, {23, 23, 21, -4, -1}, {23, 23, 21,
      -4, -3}, {23, 23, 21, -1, -3}, {23, 23, 21, 2, -2}, {23, 23, 21, 4, -4},
      {23, 23, 21, 1, -1}, {23, 23, 21, 3, -3}, {23, 23, 21, 2, 4}, {23, 23,
      21, 2, 1}, {23, 23, 21, 2, 3}, {23, 23, 21, 4, 1}, {23, 23, 21, 4, 3},
      {23, 23, 21, 4, -4}, {23, 23, 21, 2, -2}, {23, 23, 21, 1, -1}, {23, 23,
      21, 3, -3}, {23, 23, 21, 1, -1}, {23, 23, 21, 3, -3}, {23, 23, 21, 2,
      -4}, {23, 23, 21, 4, -2}, {23, 23, 21, 2, -1}, {23, 23, 21, 2, -3}, {23,
      23, 21, 4, -1}, {23, 23, 21, 4, -3}, {23, 23, 21, 1, 3}, {23, 23, 21, 1,
      -2}, {23, 23, 21, 1, -4}, {23, 23, 21, 3, -2}, {23, 23, 21, 3, -4}, {23,
      23, 21, 2, -2}, {23, 23, 21, 4, -4}, {23, 23, 21, 2, -2}, {23, 23, 21, 4,
      -4}, {23, 23, 21, 3, -3}, {23, 23, 21, 1, -1}, {23, 23, 21, 1, -3}, {23,
      23, 21, 3, -1}, {23, 23, 21, -2, -4}, {23, 23, 21, -2, -1}, {23, 23, 21,
      -2, -3}, {23, 23, 21, -4, -1}, {23, 23, 21, -4, -3}, {23, 23, 21, -1,
      -3}};

  bool in_pdgs_used[ninitial]; 
  bool out_pdgs_used[nexternal - ninitial]; 
  for(unsigned int i = 0; i < nprocs; i++ )
  {
    int permutations[nexternal]; 

    // Reinitialize initial state look-up variables
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      in_pdgs_used[j] = false; 
      permutations[j] = -1; 
    }
    // Look for initial state matches
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      for(unsigned int k = 0; k < ninitial; k++ )
      {
        // Make sure it has not been used already
        if (in_pdgs_used[k])
          continue; 
        if (initial_pdgs[k] == in_pdgs[i][j])
        {
          permutations[j] = k; 
          in_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[ninitial - 1] == -1)
      continue; 

    // Reinitialize final state look-up variables
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      out_pdgs_used[j] = false; 
      permutations[ninitial + j] = -1; 
    }
    // Look for final state matches
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      for(unsigned int k = 0; k < (nexternal - ninitial); k++ )
      {
        // Make sure it has not been used already
        if (out_pdgs_used[k])
          continue; 
        if (final_pdgs[k] == out_pdgs[i][j])
        {
          permutations[ninitial + j] = ninitial + k; 
          out_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[ninitial + j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[nexternal - 1] == -1)
      continue; 

    // Return process found
    return pair < vector<int> , int > (vector<int> (permutations, permutations
        + nexternal), proc_IDS[i]);
  }

  // No process found
  return NA; 
}

//--------------------------------------------------------------------------
// Set momenta
void PY8MEs_R12_P10_sm_qq_zzgqq::setMomenta(vector < vec_double >
    momenta_picked)
{
  if (momenta_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setMomenta' of class" << 
    " 'PY8MEs_R12_P10_sm_qq_zzgqq': Incorrect number of" << 
    " momenta specified." << endl; 
    throw PY8MEs_R12_P10_sm_qq_zzgqq_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    if (momenta_picked[i].size() != 4)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R12_P10_sm_qq_zzgqq': Incorrect number of" << 
      " momenta components specified." << endl; 
      throw PY8MEs_R12_P10_sm_qq_zzgqq_exception; 
    }
    if (momenta_picked[i][0] < 0.0)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R12_P10_sm_qq_zzgqq': A momentum was specified" << 
      " with negative energy. Check conventions." << endl; 
      throw PY8MEs_R12_P10_sm_qq_zzgqq_exception; 
    }
    for (unsigned int j = 0; j < 4; j++ )
    {
      p[i][j] = momenta_picked[i][j]; 
    }
  }
}

//--------------------------------------------------------------------------
// Set color configuration to use. An empty vector means sum over all.
void PY8MEs_R12_P10_sm_qq_zzgqq::setColors(vector<int> colors_picked)
{
  if (colors_picked.size() == 0)
  {
    user_colors = vector<int> (); 
    return; 
  }
  if (colors_picked.size() != (2 * nexternal))
  {
    cerr <<  "Error in function 'setColors' of class" << 
    " 'PY8MEs_R12_P10_sm_qq_zzgqq': Incorrect number" << 
    " of colors specified." << endl; 
    throw PY8MEs_R12_P10_sm_qq_zzgqq_exception; 
  }
  user_colors = vector<int> ((2 * nexternal), 0); 
  for(unsigned int i = 0; i < (2 * nexternal); i++ )
  {
    user_colors[i] = colors_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the helicity configuration to use. Am empty vector means sum over all.
void PY8MEs_R12_P10_sm_qq_zzgqq::setHelicities(vector<int> helicities_picked) 
{
  if (helicities_picked.size() != nexternal)
  {
    if (helicities_picked.size() == 0)
    {
      user_helicities = vector<int> (); 
      return; 
    }
    cerr <<  "Error in function 'setHelicities' of class" << 
    " 'PY8MEs_R12_P10_sm_qq_zzgqq': Incorrect number" << 
    " of helicities specified." << endl; 
    throw PY8MEs_R12_P10_sm_qq_zzgqq_exception; 
  }
  user_helicities = vector<int> (nexternal, 0); 
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    user_helicities[i] = helicities_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the permutation to use (will apply to momenta, colors and helicities)
void PY8MEs_R12_P10_sm_qq_zzgqq::setPermutation(vector<int> perm_picked) 
{
  if (perm_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setPermutations' of class" << 
    " 'PY8MEs_R12_P10_sm_qq_zzgqq': Incorrect number" << 
    " of permutations specified." << endl; 
    throw PY8MEs_R12_P10_sm_qq_zzgqq_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    perm[i] = perm_picked[i]; 
  }
}

// Set the proc_ID to use
void PY8MEs_R12_P10_sm_qq_zzgqq::setProcID(int procID_picked) 
{
  proc_ID = procID_picked; 
}

//--------------------------------------------------------------------------
// Initialize process.

void PY8MEs_R12_P10_sm_qq_zzgqq::initProc() 
{

  // Initialize flags
  include_symmetry_factors = true; 
  include_helicity_averaging_factors = true; 
  include_color_averaging_factors = true; 

  // Initialize vectors.
  perm = vector<int> (nexternal, 0); 
  user_colors = vector<int> (2 * nexternal, 0); 
  user_helicities = vector<int> (nexternal, 0); 
  p = vector < double * > (); 
  for (unsigned int i = 0; i < nexternal; i++ )
  {
    p.push_back(new double[4]); 
  }
  initColorConfigs(); 
  // Synchronize local variables dependent on the model with the active model.
  mME = vector<double> (nexternal, 0.); 
  syncProcModelParams(); 
  jamp2 = vector < vec_double > (20); 
  jamp2[0] = vector<double> (4, 0.); 
  jamp2[1] = vector<double> (4, 0.); 
  jamp2[2] = vector<double> (4, 0.); 
  jamp2[3] = vector<double> (4, 0.); 
  jamp2[4] = vector<double> (4, 0.); 
  jamp2[5] = vector<double> (4, 0.); 
  jamp2[6] = vector<double> (4, 0.); 
  jamp2[7] = vector<double> (4, 0.); 
  jamp2[8] = vector<double> (4, 0.); 
  jamp2[9] = vector<double> (4, 0.); 
  jamp2[10] = vector<double> (4, 0.); 
  jamp2[11] = vector<double> (4, 0.); 
  jamp2[12] = vector<double> (4, 0.); 
  jamp2[13] = vector<double> (4, 0.); 
  jamp2[14] = vector<double> (4, 0.); 
  jamp2[15] = vector<double> (4, 0.); 
  jamp2[16] = vector<double> (4, 0.); 
  jamp2[17] = vector<double> (4, 0.); 
  jamp2[18] = vector<double> (4, 0.); 
  jamp2[19] = vector<double> (4, 0.); 
  all_results = vector < vec_vec_double > (20); 
  // The first entry is always the color or helicity avg/summed matrix element.
  all_results[0] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[1] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[2] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[3] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[4] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[5] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[6] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[7] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[8] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[9] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[10] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[11] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[12] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[13] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[14] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[15] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[16] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[17] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[18] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[19] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
}

// Synchronize local variables of the process that depend on the model
// parameters
void PY8MEs_R12_P10_sm_qq_zzgqq::syncProcModelParams() 
{

  // Instantiate the model class and set parameters that stay fixed during run
  mME[0] = pars->ZERO; 
  mME[1] = pars->ZERO; 
  mME[2] = pars->mdl_MZ; 
  mME[3] = pars->mdl_MZ; 
  mME[4] = pars->ZERO; 
  mME[5] = pars->ZERO; 
  mME[6] = pars->ZERO; 
}

//--------------------------------------------------------------------------
// Setter allowing to force particular values for the external masses
void PY8MEs_R12_P10_sm_qq_zzgqq::setMasses(vec_double external_masses) 
{

  if (external_masses.size() != mME.size())
  {
    cerr <<  "Error in function 'setMasses' of class" << 
    " 'PY8MEs_R12_P10_sm_qq_zzgqq': Incorrect number of" << 
    " masses specified." << endl; 
    throw PY8MEs_R12_P10_sm_qq_zzgqq_exception; 
  }
  for (unsigned int j = 0; j < mME.size(); j++ )
  {
    mME[j] = external_masses[perm[j]]; 
  }
}

//--------------------------------------------------------------------------
// Getter accessing external masses with the correct ordering
vector<double> PY8MEs_R12_P10_sm_qq_zzgqq::getMasses() 
{

  vec_double external_masses; 
  vector<int> invertedPerm; 
  invertedPerm = invert_mapping(perm); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    external_masses.push_back(mME[invertedPerm[i]]); 
  }
  return external_masses; 

}


// Set all values of the external masses to float(-mode) where mode can be
// 0 : Mass taken from the model
// 1 : Mass taken from p_i^2 if not massless to begin with
// 2 : Mass always taken from p_i^2.
void PY8MEs_R12_P10_sm_qq_zzgqq::setExternalMassesMode(int mode) 
{
  if (mode != 0 && mode != 1 && mode != 2)
  {
    cerr <<  "Error in function 'setExternalMassesMode' of class" << 
    " 'PY8MEs_R12_P10_sm_qq_zzgqq': Incorrect mode selected :" << mode << 
    ". It must be either 0, 1 or 2" << endl; 
    throw PY8MEs_R12_P10_sm_qq_zzgqq_exception; 
  }
  if (mode == 0)
  {
    syncProcModelParams(); 
  }
  else if (mode == 1)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      if (mME[j] != pars->ZERO)
      {
        mME[j] = -1.0; 
      }
    }
  }
  else if (mode == 2)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      mME[j] = -1.0; 
    }
  }
}

//--------------------------------------------------------------------------
// Evaluate the squared matrix element.

double PY8MEs_R12_P10_sm_qq_zzgqq::sigmaKin() 
{
  // Set the parameters which change event by event
  pars->setDependentParameters(); 
  pars->setDependentCouplings(); 
  // Reset color flows
  for(int i = 0; i < 4; i++ )
    jamp2[0][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[1][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[2][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[3][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[4][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[5][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[6][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[7][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[8][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[9][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[10][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[11][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[12][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[13][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[14][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[15][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[16][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[17][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[18][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[19][i] = 0.; 

  // Save previous values of mME
  vector<double> saved_mME(mME.size(), 0.0); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (mME[i] < 0.0)
    {
      saved_mME[i] = mME[i]; 
      mME[i] = sqrt(abs(pow(p[perm[i]][0], 2) - 
      (pow(p[perm[i]][1], 2) + pow(p[perm[i]][2], 2) + pow(p[perm[i]][3],
          2))));
    }
  }

  // Local variables and constants
  const int max_tries = 10; 
  vector < vec_bool > goodhel(nprocesses, vec_bool(ncomb, false)); 
  vec_int ntry(nprocesses, 0); 
  double t = 0.; 
  double result = 0.; 

  if (ntry[proc_ID] <= max_tries)
    ntry[proc_ID] = ntry[proc_ID] + 1; 

  // Find which helicity configuration is asked for
  // -1 indicates one wants to sum over helicities
  int user_ihel = getHelicityIDForConfig(user_helicities); 

  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = getColorIDForConfig(user_colors); 

  // Reset the list of results that will be recomputed here
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      all_results[proc_ID][ihel + 1][icolor + 1] = 0.; 
    }
  }

  // Calculate the matrix element for all helicities
  // unless already detected as vanishing
  for(int ihel = 0; ihel < ncomb; ihel++ )
  {
    // Skip helicity if already detected as vanishing
    if ((ntry[proc_ID] >= max_tries) && !goodhel[proc_ID][ihel])
      continue; 

    // Also skip helicity if user asks for a specific one
    if ((ntry[proc_ID] >= max_tries) && user_ihel != -1 && user_ihel != ihel)
      continue; 

    calculate_wavefunctions(helicities[ihel]); 

    // Reset locally computed color flows
    for(int i = 0; i < 4; i++ )
      jamp2[0][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[1][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[2][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[3][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[4][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[5][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[6][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[7][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[8][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[9][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[10][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[11][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[12][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[13][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[14][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[15][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[16][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[17][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[18][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[19][i] = 0.; 

    if (proc_ID == 0)
      t = matrix_12_uu_zzguu(); 
    if (proc_ID == 1)
      t = matrix_12_uux_zzguux(); 
    if (proc_ID == 2)
      t = matrix_12_dd_zzgdd(); 
    if (proc_ID == 3)
      t = matrix_12_ddx_zzgddx(); 
    if (proc_ID == 4)
      t = matrix_12_uxux_zzguxux(); 
    if (proc_ID == 5)
      t = matrix_12_dxdx_zzgdxdx(); 
    if (proc_ID == 6)
      t = matrix_12_uc_zzguc(); 
    if (proc_ID == 7)
      t = matrix_12_ud_zzgud(); 
    if (proc_ID == 8)
      t = matrix_12_uux_zzgccx(); 
    if (proc_ID == 9)
      t = matrix_12_uux_zzgddx(); 
    if (proc_ID == 10)
      t = matrix_12_ucx_zzgucx(); 
    if (proc_ID == 11)
      t = matrix_12_udx_zzgudx(); 
    if (proc_ID == 12)
      t = matrix_12_ds_zzgds(); 
    if (proc_ID == 13)
      t = matrix_12_dux_zzgdux(); 
    if (proc_ID == 14)
      t = matrix_12_ddx_zzguux(); 
    if (proc_ID == 15)
      t = matrix_12_ddx_zzgssx(); 
    if (proc_ID == 16)
      t = matrix_12_dsx_zzgdsx(); 
    if (proc_ID == 17)
      t = matrix_12_uxcx_zzguxcx(); 
    if (proc_ID == 18)
      t = matrix_12_uxdx_zzguxdx(); 
    if (proc_ID == 19)
      t = matrix_12_dxsx_zzgdxsx(); 

    // Store which helicities give non-zero result
    if ((ntry[proc_ID] < max_tries) && t != 0. && !goodhel[proc_ID][ihel])
      goodhel[proc_ID][ihel] = true; 

    // Aggregate results
    if (user_ihel == -1 || user_ihel == ihel)
    {
      if (user_icol == -1)
      {
        result = result + t; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][0] += t; 
          for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
          {
            all_results[proc_ID][0][i + 1] += jamp2[proc_ID][i]; 
          }
        }
        all_results[proc_ID][ihel + 1][0] += t; 
        for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
        {
          all_results[proc_ID][ihel + 1][i + 1] += jamp2[proc_ID][i]; 
        }
      }
      else
      {
        result = result + jamp2[proc_ID][user_icol]; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][user_icol + 1] += jamp2[proc_ID][user_icol]; 
        }
        all_results[proc_ID][ihel + 1][user_icol + 1] +=
            jamp2[proc_ID][user_icol];
      }
    }

  }

  // Normalize results with the identical particle factor
  if (include_symmetry_factors)
  {
    result = result/denom_iden[proc_ID]; 
  }
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      if (include_symmetry_factors)
      {
        all_results[proc_ID][ihel + 1][icolor + 1] /= denom_iden[proc_ID]; 
      }
    }
  }


  // Normalize when when summing+averaging over helicity configurations
  if (user_ihel == -1 && include_helicity_averaging_factors)
  {
    result /= denom_hels[proc_ID]; 
    if (user_icol == -1)
    {
      all_results[proc_ID][0][0] /= denom_hels[proc_ID]; 
      for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
      {
        all_results[proc_ID][0][i + 1] /= denom_hels[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][0][user_icol + 1] /= denom_hels[proc_ID]; 
    }
  }

  // Normalize when summing+averaging over color configurations
  if (user_icol == -1 && include_color_averaging_factors)
  {
    result /= denom_colors[proc_ID]; 
    if (user_ihel == -1)
    {
      all_results[proc_ID][0][0] /= denom_colors[proc_ID]; 
      for (unsigned int i = 0; i < ncomb; i++ )
      {
        all_results[proc_ID][i + 1][0] /= denom_colors[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][user_ihel + 1][0] /= denom_colors[proc_ID]; 
    }
  }

  // Reinstate previous values of mME
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (saved_mME[i] < 0.0)
    {
      mME[i] = saved_mME[i]; 
    }
  }

  // Finally return it
  return result; 
}

//==========================================================================
// Private class member functions

//--------------------------------------------------------------------------
// Evaluate |M|^2 for each subprocess

void PY8MEs_R12_P10_sm_qq_zzgqq::calculate_wavefunctions(const int hel[])
{
  // Calculate wavefunctions for all processes
  // Calculate all wavefunctions
  ixxxxx(p[perm[0]], mME[0], hel[0], +1, w[0]); 
  ixxxxx(p[perm[1]], mME[1], hel[1], +1, w[1]); 
  vxxxxx(p[perm[2]], mME[2], hel[2], +1, w[2]); 
  vxxxxx(p[perm[3]], mME[3], hel[3], +1, w[3]); 
  vxxxxx(p[perm[4]], mME[4], hel[4], +1, w[4]); 
  oxxxxx(p[perm[5]], mME[5], hel[5], +1, w[5]); 
  oxxxxx(p[perm[6]], mME[6], hel[6], +1, w[6]); 
  FFV1_2(w[0], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[7]); 
  FFV2_5_1(w[5], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO, w[8]); 
  FFV2_5_2(w[7], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO, w[9]); 
  FFV1P0_3(w[1], w[8], pars->GC_11, pars->ZERO, pars->ZERO, w[10]); 
  FFV1P0_3(w[7], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[11]); 
  FFV2_5_1(w[8], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[12]);
  FFV2_5_1(w[6], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[13]);
  FFV1P0_3(w[7], w[8], pars->GC_11, pars->ZERO, pars->ZERO, w[14]); 
  FFV1P0_3(w[7], w[13], pars->GC_11, pars->ZERO, pars->ZERO, w[15]); 
  FFV2_5_2(w[1], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[16]);
  FFV1P0_3(w[1], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[17]); 
  FFV1_2(w[7], w[17], pars->GC_11, pars->ZERO, pars->ZERO, w[18]); 
  FFV2_5_1(w[6], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[19]);
  FFV1P0_3(w[1], w[19], pars->GC_11, pars->ZERO, pars->ZERO, w[20]); 
  FFV1P0_3(w[7], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[21]); 
  FFV2_5_1(w[19], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[22]);
  FFV2_5_1(w[5], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[23]);
  FFV1P0_3(w[7], w[19], pars->GC_11, pars->ZERO, pars->ZERO, w[24]); 
  FFV1P0_3(w[7], w[23], pars->GC_11, pars->ZERO, pars->ZERO, w[25]); 
  FFV1P0_3(w[1], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[26]); 
  FFV1_2(w[7], w[26], pars->GC_11, pars->ZERO, pars->ZERO, w[27]); 
  FFV2_5_2(w[1], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[28]);
  FFV1P0_3(w[28], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[29]); 
  FFV1P0_3(w[28], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[30]); 
  FFV2_5_2(w[28], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[31]);
  FFV2_5_2(w[7], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[32]);
  FFV1P0_3(w[1], w[23], pars->GC_11, pars->ZERO, pars->ZERO, w[33]); 
  FFV2_5_1(w[23], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[34]);
  FFV1P0_3(w[1], w[13], pars->GC_11, pars->ZERO, pars->ZERO, w[35]); 
  FFV2_5_1(w[13], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[36]);
  FFV1P0_3(w[16], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[37]); 
  FFV1P0_3(w[16], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[38]); 
  FFV2_5_2(w[16], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[39]);
  FFV1_1(w[6], w[26], pars->GC_11, pars->ZERO, pars->ZERO, w[40]); 
  FFV1_1(w[5], w[17], pars->GC_11, pars->ZERO, pars->ZERO, w[41]); 
  FFV1_1(w[5], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[42]); 
  FFV2_5_2(w[0], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[43]);
  FFV2_5_1(w[42], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[44]);
  FFV1P0_3(w[43], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[45]); 
  FFV1P0_3(w[1], w[42], pars->GC_11, pars->ZERO, pars->ZERO, w[46]); 
  FFV2_5_2(w[43], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[47]);
  FFV1P0_3(w[43], w[42], pars->GC_11, pars->ZERO, pars->ZERO, w[48]); 
  FFV1P0_3(w[16], w[42], pars->GC_11, pars->ZERO, pars->ZERO, w[49]); 
  FFV1_1(w[42], w[17], pars->GC_11, pars->ZERO, pars->ZERO, w[50]); 
  FFV2_5_2(w[0], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[51]);
  FFV2_5_1(w[42], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[52]);
  FFV1P0_3(w[51], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[53]); 
  FFV2_5_2(w[51], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[54]);
  FFV1P0_3(w[51], w[42], pars->GC_11, pars->ZERO, pars->ZERO, w[55]); 
  FFV1P0_3(w[28], w[42], pars->GC_11, pars->ZERO, pars->ZERO, w[56]); 
  FFV1P0_3(w[0], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[57]); 
  FFV1_2(w[1], w[57], pars->GC_11, pars->ZERO, pars->ZERO, w[58]); 
  FFV1_1(w[42], w[57], pars->GC_11, pars->ZERO, pars->ZERO, w[59]); 
  FFV1P0_3(w[0], w[42], pars->GC_11, pars->ZERO, pars->ZERO, w[60]); 
  FFV1P0_3(w[0], w[19], pars->GC_11, pars->ZERO, pars->ZERO, w[61]); 
  FFV1P0_3(w[0], w[13], pars->GC_11, pars->ZERO, pars->ZERO, w[62]); 
  FFV1_2(w[0], w[17], pars->GC_11, pars->ZERO, pars->ZERO, w[63]); 
  FFV1_1(w[6], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[64]); 
  FFV2_5_1(w[64], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[65]);
  FFV1P0_3(w[43], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[66]); 
  FFV1P0_3(w[1], w[64], pars->GC_11, pars->ZERO, pars->ZERO, w[67]); 
  FFV1P0_3(w[43], w[64], pars->GC_11, pars->ZERO, pars->ZERO, w[68]); 
  FFV1P0_3(w[16], w[64], pars->GC_11, pars->ZERO, pars->ZERO, w[69]); 
  FFV1_1(w[64], w[26], pars->GC_11, pars->ZERO, pars->ZERO, w[70]); 
  FFV2_5_1(w[64], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[71]);
  FFV1P0_3(w[51], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[72]); 
  FFV1P0_3(w[51], w[64], pars->GC_11, pars->ZERO, pars->ZERO, w[73]); 
  FFV1P0_3(w[28], w[64], pars->GC_11, pars->ZERO, pars->ZERO, w[74]); 
  FFV1P0_3(w[0], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[75]); 
  FFV1_2(w[1], w[75], pars->GC_11, pars->ZERO, pars->ZERO, w[76]); 
  FFV1_1(w[64], w[75], pars->GC_11, pars->ZERO, pars->ZERO, w[77]); 
  FFV1P0_3(w[0], w[64], pars->GC_11, pars->ZERO, pars->ZERO, w[78]); 
  FFV1P0_3(w[0], w[8], pars->GC_11, pars->ZERO, pars->ZERO, w[79]); 
  FFV1P0_3(w[0], w[23], pars->GC_11, pars->ZERO, pars->ZERO, w[80]); 
  FFV1_2(w[0], w[26], pars->GC_11, pars->ZERO, pars->ZERO, w[81]); 
  FFV1_2(w[1], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[82]); 
  FFV2_5_2(w[82], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[83]);
  FFV1P0_3(w[82], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[84]); 
  FFV1P0_3(w[82], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[85]); 
  FFV1P0_3(w[82], w[23], pars->GC_11, pars->ZERO, pars->ZERO, w[86]); 
  FFV1P0_3(w[82], w[13], pars->GC_11, pars->ZERO, pars->ZERO, w[87]); 
  FFV2_5_2(w[82], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[88]);
  FFV1P0_3(w[82], w[8], pars->GC_11, pars->ZERO, pars->ZERO, w[89]); 
  FFV1P0_3(w[82], w[19], pars->GC_11, pars->ZERO, pars->ZERO, w[90]); 
  FFV1_1(w[6], w[75], pars->GC_11, pars->ZERO, pars->ZERO, w[91]); 
  FFV1_2(w[82], w[75], pars->GC_11, pars->ZERO, pars->ZERO, w[92]); 
  FFV1_1(w[5], w[57], pars->GC_11, pars->ZERO, pars->ZERO, w[93]); 
  FFV1_2(w[82], w[57], pars->GC_11, pars->ZERO, pars->ZERO, w[94]); 
  FFV1_2(w[43], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[95]); 
  FFV1_1(w[23], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[96]); 
  VVV1P0_1(w[4], w[17], pars->GC_10, pars->ZERO, pars->ZERO, w[97]); 
  FFV1_1(w[13], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[98]); 
  VVV1P0_1(w[4], w[26], pars->GC_10, pars->ZERO, pars->ZERO, w[99]); 
  FFV1_2(w[16], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[100]); 
  FFV1_2(w[51], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[101]); 
  FFV1_1(w[8], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[102]); 
  FFV1_1(w[19], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[103]); 
  FFV1_2(w[28], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[104]); 
  VVV1P0_1(w[4], w[75], pars->GC_10, pars->ZERO, pars->ZERO, w[105]); 
  VVV1P0_1(w[4], w[57], pars->GC_10, pars->ZERO, pars->ZERO, w[106]); 
  oxxxxx(p[perm[1]], mME[1], hel[1], -1, w[107]); 
  ixxxxx(p[perm[6]], mME[6], hel[6], -1, w[108]); 
  FFV2_5_1(w[107], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[109]);
  FFV1P0_3(w[108], w[109], pars->GC_11, pars->ZERO, pars->ZERO, w[110]); 
  FFV2_5_1(w[109], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[111]);
  FFV1P0_3(w[7], w[109], pars->GC_11, pars->ZERO, pars->ZERO, w[112]); 
  FFV2_5_2(w[108], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[113]);
  FFV1P0_3(w[108], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[114]); 
  FFV1_2(w[7], w[114], pars->GC_11, pars->ZERO, pars->ZERO, w[115]); 
  FFV1P0_3(w[108], w[8], pars->GC_11, pars->ZERO, pars->ZERO, w[116]); 
  FFV1P0_3(w[7], w[107], pars->GC_11, pars->ZERO, pars->ZERO, w[117]); 
  FFV2_5_1(w[107], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[118]);
  FFV1P0_3(w[7], w[118], pars->GC_11, pars->ZERO, pars->ZERO, w[119]); 
  FFV1P0_3(w[108], w[107], pars->GC_11, pars->ZERO, pars->ZERO, w[120]); 
  FFV1_2(w[7], w[120], pars->GC_11, pars->ZERO, pars->ZERO, w[121]); 
  FFV2_5_2(w[108], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[122]);
  FFV1P0_3(w[122], w[107], pars->GC_11, pars->ZERO, pars->ZERO, w[123]); 
  FFV1P0_3(w[122], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[124]); 
  FFV2_5_2(w[122], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[125]);
  FFV1P0_3(w[108], w[118], pars->GC_11, pars->ZERO, pars->ZERO, w[126]); 
  FFV2_5_1(w[118], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[127]);
  FFV1P0_3(w[108], w[23], pars->GC_11, pars->ZERO, pars->ZERO, w[128]); 
  FFV1P0_3(w[113], w[107], pars->GC_11, pars->ZERO, pars->ZERO, w[129]); 
  FFV1P0_3(w[113], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[130]); 
  FFV2_5_2(w[113], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[131]);
  FFV1_1(w[5], w[120], pars->GC_11, pars->ZERO, pars->ZERO, w[132]); 
  FFV1_1(w[107], w[114], pars->GC_11, pars->ZERO, pars->ZERO, w[133]); 
  FFV1_1(w[107], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[134]); 
  FFV2_5_1(w[134], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[135]);
  FFV1P0_3(w[108], w[134], pars->GC_11, pars->ZERO, pars->ZERO, w[136]); 
  FFV1P0_3(w[43], w[134], pars->GC_11, pars->ZERO, pars->ZERO, w[137]); 
  FFV1P0_3(w[113], w[134], pars->GC_11, pars->ZERO, pars->ZERO, w[138]); 
  FFV1_1(w[134], w[114], pars->GC_11, pars->ZERO, pars->ZERO, w[139]); 
  FFV2_5_1(w[134], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[140]);
  FFV1P0_3(w[51], w[134], pars->GC_11, pars->ZERO, pars->ZERO, w[141]); 
  FFV1P0_3(w[122], w[134], pars->GC_11, pars->ZERO, pars->ZERO, w[142]); 
  FFV1_2(w[108], w[75], pars->GC_11, pars->ZERO, pars->ZERO, w[143]); 
  FFV1_1(w[134], w[75], pars->GC_11, pars->ZERO, pars->ZERO, w[144]); 
  FFV1P0_3(w[0], w[134], pars->GC_11, pars->ZERO, pars->ZERO, w[145]); 
  FFV1_2(w[0], w[114], pars->GC_11, pars->ZERO, pars->ZERO, w[146]); 
  FFV1P0_3(w[43], w[107], pars->GC_11, pars->ZERO, pars->ZERO, w[147]); 
  FFV1P0_3(w[108], w[42], pars->GC_11, pars->ZERO, pars->ZERO, w[148]); 
  FFV1P0_3(w[113], w[42], pars->GC_11, pars->ZERO, pars->ZERO, w[149]); 
  FFV1_1(w[42], w[120], pars->GC_11, pars->ZERO, pars->ZERO, w[150]); 
  FFV1P0_3(w[51], w[107], pars->GC_11, pars->ZERO, pars->ZERO, w[151]); 
  FFV1P0_3(w[122], w[42], pars->GC_11, pars->ZERO, pars->ZERO, w[152]); 
  FFV1P0_3(w[0], w[107], pars->GC_11, pars->ZERO, pars->ZERO, w[153]); 
  FFV1_2(w[108], w[153], pars->GC_11, pars->ZERO, pars->ZERO, w[154]); 
  FFV1_1(w[42], w[153], pars->GC_11, pars->ZERO, pars->ZERO, w[155]); 
  FFV1P0_3(w[0], w[109], pars->GC_11, pars->ZERO, pars->ZERO, w[156]); 
  FFV1P0_3(w[0], w[118], pars->GC_11, pars->ZERO, pars->ZERO, w[157]); 
  FFV1_2(w[0], w[120], pars->GC_11, pars->ZERO, pars->ZERO, w[158]); 
  FFV1_2(w[108], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[159]); 
  FFV2_5_2(w[159], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[160]);
  FFV1P0_3(w[159], w[107], pars->GC_11, pars->ZERO, pars->ZERO, w[161]); 
  FFV1P0_3(w[159], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[162]); 
  FFV1P0_3(w[159], w[118], pars->GC_11, pars->ZERO, pars->ZERO, w[163]); 
  FFV1P0_3(w[159], w[23], pars->GC_11, pars->ZERO, pars->ZERO, w[164]); 
  FFV2_5_2(w[159], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[165]);
  FFV1P0_3(w[159], w[109], pars->GC_11, pars->ZERO, pars->ZERO, w[166]); 
  FFV1P0_3(w[159], w[8], pars->GC_11, pars->ZERO, pars->ZERO, w[167]); 
  FFV1_1(w[5], w[153], pars->GC_11, pars->ZERO, pars->ZERO, w[168]); 
  FFV1_2(w[159], w[153], pars->GC_11, pars->ZERO, pars->ZERO, w[169]); 
  FFV1_1(w[107], w[75], pars->GC_11, pars->ZERO, pars->ZERO, w[170]); 
  FFV1_2(w[159], w[75], pars->GC_11, pars->ZERO, pars->ZERO, w[171]); 
  FFV1_1(w[118], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[172]); 
  VVV1P0_1(w[4], w[114], pars->GC_10, pars->ZERO, pars->ZERO, w[173]); 
  VVV1P0_1(w[4], w[120], pars->GC_10, pars->ZERO, pars->ZERO, w[174]); 
  FFV1_2(w[113], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[175]); 
  FFV1_1(w[109], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[176]); 
  FFV1_2(w[122], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[177]); 
  VVV1P0_1(w[4], w[153], pars->GC_10, pars->ZERO, pars->ZERO, w[178]); 
  FFV2_3_1(w[5], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[179]);
  FFV2_3_2(w[7], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[180]);
  FFV1P0_3(w[1], w[179], pars->GC_11, pars->ZERO, pars->ZERO, w[181]); 
  FFV2_3_1(w[179], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[182]);
  FFV2_3_1(w[6], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[183]);
  FFV1P0_3(w[7], w[179], pars->GC_11, pars->ZERO, pars->ZERO, w[184]); 
  FFV1P0_3(w[7], w[183], pars->GC_11, pars->ZERO, pars->ZERO, w[185]); 
  FFV2_3_2(w[1], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[186]);
  FFV2_3_1(w[6], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[187]);
  FFV1P0_3(w[1], w[187], pars->GC_11, pars->ZERO, pars->ZERO, w[188]); 
  FFV2_3_1(w[187], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[189]);
  FFV2_3_1(w[5], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[190]);
  FFV1P0_3(w[7], w[187], pars->GC_11, pars->ZERO, pars->ZERO, w[191]); 
  FFV1P0_3(w[7], w[190], pars->GC_11, pars->ZERO, pars->ZERO, w[192]); 
  FFV2_3_2(w[1], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[193]);
  FFV1P0_3(w[193], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[194]); 
  FFV1P0_3(w[193], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[195]); 
  FFV2_3_2(w[193], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[196]);
  FFV2_3_2(w[7], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[197]);
  FFV1P0_3(w[1], w[190], pars->GC_11, pars->ZERO, pars->ZERO, w[198]); 
  FFV2_3_1(w[190], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[199]);
  FFV1P0_3(w[1], w[183], pars->GC_11, pars->ZERO, pars->ZERO, w[200]); 
  FFV2_3_1(w[183], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[201]);
  FFV1P0_3(w[186], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[202]); 
  FFV1P0_3(w[186], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[203]); 
  FFV2_3_2(w[186], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[204]);
  FFV2_3_2(w[0], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[205]);
  FFV2_3_1(w[42], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[206]);
  FFV1P0_3(w[205], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[207]); 
  FFV2_3_2(w[205], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[208]);
  FFV1P0_3(w[205], w[42], pars->GC_11, pars->ZERO, pars->ZERO, w[209]); 
  FFV1P0_3(w[186], w[42], pars->GC_11, pars->ZERO, pars->ZERO, w[210]); 
  FFV2_3_2(w[0], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[211]);
  FFV2_3_1(w[42], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[212]);
  FFV1P0_3(w[211], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[213]); 
  FFV2_3_2(w[211], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[214]);
  FFV1P0_3(w[211], w[42], pars->GC_11, pars->ZERO, pars->ZERO, w[215]); 
  FFV1P0_3(w[193], w[42], pars->GC_11, pars->ZERO, pars->ZERO, w[216]); 
  FFV1P0_3(w[0], w[187], pars->GC_11, pars->ZERO, pars->ZERO, w[217]); 
  FFV1P0_3(w[0], w[183], pars->GC_11, pars->ZERO, pars->ZERO, w[218]); 
  FFV2_3_1(w[64], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[219]);
  FFV1P0_3(w[205], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[220]); 
  FFV1P0_3(w[205], w[64], pars->GC_11, pars->ZERO, pars->ZERO, w[221]); 
  FFV1P0_3(w[186], w[64], pars->GC_11, pars->ZERO, pars->ZERO, w[222]); 
  FFV2_3_1(w[64], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[223]);
  FFV1P0_3(w[211], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[224]); 
  FFV1P0_3(w[211], w[64], pars->GC_11, pars->ZERO, pars->ZERO, w[225]); 
  FFV1P0_3(w[193], w[64], pars->GC_11, pars->ZERO, pars->ZERO, w[226]); 
  FFV1P0_3(w[0], w[179], pars->GC_11, pars->ZERO, pars->ZERO, w[227]); 
  FFV1P0_3(w[0], w[190], pars->GC_11, pars->ZERO, pars->ZERO, w[228]); 
  FFV2_3_2(w[82], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[229]);
  FFV1P0_3(w[82], w[190], pars->GC_11, pars->ZERO, pars->ZERO, w[230]); 
  FFV1P0_3(w[82], w[183], pars->GC_11, pars->ZERO, pars->ZERO, w[231]); 
  FFV2_3_2(w[82], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[232]);
  FFV1P0_3(w[82], w[179], pars->GC_11, pars->ZERO, pars->ZERO, w[233]); 
  FFV1P0_3(w[82], w[187], pars->GC_11, pars->ZERO, pars->ZERO, w[234]); 
  FFV1_2(w[205], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[235]); 
  FFV1_1(w[190], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[236]); 
  FFV1_1(w[183], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[237]); 
  FFV1_2(w[186], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[238]); 
  FFV1_2(w[211], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[239]); 
  FFV1_1(w[179], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[240]); 
  FFV1_1(w[187], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[241]); 
  FFV1_2(w[193], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[242]); 
  FFV2_3_1(w[107], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[243]);
  FFV1P0_3(w[108], w[243], pars->GC_11, pars->ZERO, pars->ZERO, w[244]); 
  FFV2_3_1(w[243], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[245]);
  FFV1P0_3(w[7], w[243], pars->GC_11, pars->ZERO, pars->ZERO, w[246]); 
  FFV2_3_2(w[108], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[247]);
  FFV1P0_3(w[108], w[179], pars->GC_11, pars->ZERO, pars->ZERO, w[248]); 
  FFV2_3_1(w[107], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[249]);
  FFV1P0_3(w[7], w[249], pars->GC_11, pars->ZERO, pars->ZERO, w[250]); 
  FFV2_3_2(w[108], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[251]);
  FFV1P0_3(w[251], w[107], pars->GC_11, pars->ZERO, pars->ZERO, w[252]); 
  FFV1P0_3(w[251], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[253]); 
  FFV2_3_2(w[251], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[254]);
  FFV1P0_3(w[108], w[249], pars->GC_11, pars->ZERO, pars->ZERO, w[255]); 
  FFV2_3_1(w[249], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[256]);
  FFV1P0_3(w[108], w[190], pars->GC_11, pars->ZERO, pars->ZERO, w[257]); 
  FFV1P0_3(w[247], w[107], pars->GC_11, pars->ZERO, pars->ZERO, w[258]); 
  FFV1P0_3(w[247], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[259]); 
  FFV2_3_2(w[247], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[260]);
  FFV2_3_1(w[134], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[261]);
  FFV1P0_3(w[205], w[134], pars->GC_11, pars->ZERO, pars->ZERO, w[262]); 
  FFV1P0_3(w[247], w[134], pars->GC_11, pars->ZERO, pars->ZERO, w[263]); 
  FFV2_3_1(w[134], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[264]);
  FFV1P0_3(w[211], w[134], pars->GC_11, pars->ZERO, pars->ZERO, w[265]); 
  FFV1P0_3(w[251], w[134], pars->GC_11, pars->ZERO, pars->ZERO, w[266]); 
  FFV1P0_3(w[205], w[107], pars->GC_11, pars->ZERO, pars->ZERO, w[267]); 
  FFV1P0_3(w[247], w[42], pars->GC_11, pars->ZERO, pars->ZERO, w[268]); 
  FFV1P0_3(w[211], w[107], pars->GC_11, pars->ZERO, pars->ZERO, w[269]); 
  FFV1P0_3(w[251], w[42], pars->GC_11, pars->ZERO, pars->ZERO, w[270]); 
  FFV1P0_3(w[0], w[243], pars->GC_11, pars->ZERO, pars->ZERO, w[271]); 
  FFV1P0_3(w[0], w[249], pars->GC_11, pars->ZERO, pars->ZERO, w[272]); 
  FFV2_3_2(w[159], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[273]);
  FFV1P0_3(w[159], w[249], pars->GC_11, pars->ZERO, pars->ZERO, w[274]); 
  FFV1P0_3(w[159], w[190], pars->GC_11, pars->ZERO, pars->ZERO, w[275]); 
  FFV2_3_2(w[159], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[276]);
  FFV1P0_3(w[159], w[243], pars->GC_11, pars->ZERO, pars->ZERO, w[277]); 
  FFV1P0_3(w[159], w[179], pars->GC_11, pars->ZERO, pars->ZERO, w[278]); 
  FFV1_1(w[249], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[279]); 
  FFV1_2(w[247], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[280]); 
  FFV1_1(w[243], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[281]); 
  FFV1_2(w[251], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[282]); 
  oxxxxx(p[perm[0]], mME[0], hel[0], -1, w[283]); 
  ixxxxx(p[perm[5]], mME[5], hel[5], -1, w[284]); 
  FFV1_2(w[284], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[285]); 
  FFV2_5_1(w[283], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[286]);
  FFV2_5_2(w[285], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[287]);
  FFV1P0_3(w[108], w[286], pars->GC_11, pars->ZERO, pars->ZERO, w[288]); 
  FFV1P0_3(w[285], w[107], pars->GC_11, pars->ZERO, pars->ZERO, w[289]); 
  FFV2_5_1(w[286], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[290]);
  FFV1P0_3(w[285], w[286], pars->GC_11, pars->ZERO, pars->ZERO, w[291]); 
  FFV1P0_3(w[285], w[118], pars->GC_11, pars->ZERO, pars->ZERO, w[292]); 
  FFV1_2(w[285], w[120], pars->GC_11, pars->ZERO, pars->ZERO, w[293]); 
  FFV1P0_3(w[285], w[283], pars->GC_11, pars->ZERO, pars->ZERO, w[294]); 
  FFV2_5_1(w[283], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[295]);
  FFV1P0_3(w[285], w[109], pars->GC_11, pars->ZERO, pars->ZERO, w[296]); 
  FFV1P0_3(w[285], w[295], pars->GC_11, pars->ZERO, pars->ZERO, w[297]); 
  FFV1P0_3(w[108], w[283], pars->GC_11, pars->ZERO, pars->ZERO, w[298]); 
  FFV1_2(w[285], w[298], pars->GC_11, pars->ZERO, pars->ZERO, w[299]); 
  FFV1P0_3(w[122], w[283], pars->GC_11, pars->ZERO, pars->ZERO, w[300]); 
  FFV2_5_2(w[285], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[301]);
  FFV1P0_3(w[108], w[295], pars->GC_11, pars->ZERO, pars->ZERO, w[302]); 
  FFV2_5_1(w[295], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[303]);
  FFV1P0_3(w[113], w[283], pars->GC_11, pars->ZERO, pars->ZERO, w[304]); 
  FFV1_1(w[107], w[298], pars->GC_11, pars->ZERO, pars->ZERO, w[305]); 
  FFV1_1(w[283], w[120], pars->GC_11, pars->ZERO, pars->ZERO, w[306]); 
  FFV1_1(w[283], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[307]); 
  FFV2_5_2(w[284], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[308]);
  FFV2_5_1(w[307], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[309]);
  FFV1P0_3(w[308], w[107], pars->GC_11, pars->ZERO, pars->ZERO, w[310]); 
  FFV1P0_3(w[108], w[307], pars->GC_11, pars->ZERO, pars->ZERO, w[311]); 
  FFV2_5_2(w[308], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[312]);
  FFV1P0_3(w[308], w[307], pars->GC_11, pars->ZERO, pars->ZERO, w[313]); 
  FFV1P0_3(w[113], w[307], pars->GC_11, pars->ZERO, pars->ZERO, w[314]); 
  FFV1_1(w[307], w[120], pars->GC_11, pars->ZERO, pars->ZERO, w[315]); 
  FFV2_5_2(w[284], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[316]);
  FFV2_5_1(w[307], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[317]);
  FFV1P0_3(w[316], w[107], pars->GC_11, pars->ZERO, pars->ZERO, w[318]); 
  FFV2_5_2(w[316], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[319]);
  FFV1P0_3(w[316], w[307], pars->GC_11, pars->ZERO, pars->ZERO, w[320]); 
  FFV1P0_3(w[122], w[307], pars->GC_11, pars->ZERO, pars->ZERO, w[321]); 
  FFV1P0_3(w[284], w[107], pars->GC_11, pars->ZERO, pars->ZERO, w[322]); 
  FFV1_2(w[108], w[322], pars->GC_11, pars->ZERO, pars->ZERO, w[323]); 
  FFV1_1(w[307], w[322], pars->GC_11, pars->ZERO, pars->ZERO, w[324]); 
  FFV1P0_3(w[284], w[307], pars->GC_11, pars->ZERO, pars->ZERO, w[325]); 
  FFV1P0_3(w[284], w[109], pars->GC_11, pars->ZERO, pars->ZERO, w[326]); 
  FFV1P0_3(w[284], w[118], pars->GC_11, pars->ZERO, pars->ZERO, w[327]); 
  FFV1_2(w[284], w[120], pars->GC_11, pars->ZERO, pars->ZERO, w[328]); 
  FFV1P0_3(w[308], w[283], pars->GC_11, pars->ZERO, pars->ZERO, w[329]); 
  FFV1P0_3(w[308], w[134], pars->GC_11, pars->ZERO, pars->ZERO, w[330]); 
  FFV1_1(w[134], w[298], pars->GC_11, pars->ZERO, pars->ZERO, w[331]); 
  FFV1P0_3(w[316], w[283], pars->GC_11, pars->ZERO, pars->ZERO, w[332]); 
  FFV1P0_3(w[316], w[134], pars->GC_11, pars->ZERO, pars->ZERO, w[333]); 
  FFV1P0_3(w[284], w[283], pars->GC_11, pars->ZERO, pars->ZERO, w[334]); 
  FFV1_2(w[108], w[334], pars->GC_11, pars->ZERO, pars->ZERO, w[335]); 
  FFV1_1(w[134], w[334], pars->GC_11, pars->ZERO, pars->ZERO, w[336]); 
  FFV1P0_3(w[284], w[134], pars->GC_11, pars->ZERO, pars->ZERO, w[337]); 
  FFV1P0_3(w[284], w[286], pars->GC_11, pars->ZERO, pars->ZERO, w[338]); 
  FFV1P0_3(w[284], w[295], pars->GC_11, pars->ZERO, pars->ZERO, w[339]); 
  FFV1_2(w[284], w[298], pars->GC_11, pars->ZERO, pars->ZERO, w[340]); 
  FFV1P0_3(w[159], w[283], pars->GC_11, pars->ZERO, pars->ZERO, w[341]); 
  FFV1P0_3(w[159], w[295], pars->GC_11, pars->ZERO, pars->ZERO, w[342]); 
  FFV1P0_3(w[159], w[286], pars->GC_11, pars->ZERO, pars->ZERO, w[343]); 
  FFV1_1(w[107], w[334], pars->GC_11, pars->ZERO, pars->ZERO, w[344]); 
  FFV1_2(w[159], w[334], pars->GC_11, pars->ZERO, pars->ZERO, w[345]); 
  FFV1_1(w[283], w[322], pars->GC_11, pars->ZERO, pars->ZERO, w[346]); 
  FFV1_2(w[159], w[322], pars->GC_11, pars->ZERO, pars->ZERO, w[347]); 
  FFV1_2(w[308], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[348]); 
  FFV1_1(w[295], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[349]); 
  VVV1P0_1(w[4], w[298], pars->GC_10, pars->ZERO, pars->ZERO, w[350]); 
  FFV1_2(w[316], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[351]); 
  FFV1_1(w[286], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[352]); 
  VVV1P0_1(w[4], w[334], pars->GC_10, pars->ZERO, pars->ZERO, w[353]); 
  VVV1P0_1(w[4], w[322], pars->GC_10, pars->ZERO, pars->ZERO, w[354]); 
  FFV2_3_1(w[283], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[355]);
  FFV2_3_2(w[285], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[356]);
  FFV1P0_3(w[108], w[355], pars->GC_11, pars->ZERO, pars->ZERO, w[357]); 
  FFV2_3_1(w[355], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[358]);
  FFV1P0_3(w[285], w[355], pars->GC_11, pars->ZERO, pars->ZERO, w[359]); 
  FFV1P0_3(w[285], w[249], pars->GC_11, pars->ZERO, pars->ZERO, w[360]); 
  FFV2_3_1(w[283], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[361]);
  FFV1P0_3(w[285], w[243], pars->GC_11, pars->ZERO, pars->ZERO, w[362]); 
  FFV1P0_3(w[285], w[361], pars->GC_11, pars->ZERO, pars->ZERO, w[363]); 
  FFV1P0_3(w[251], w[283], pars->GC_11, pars->ZERO, pars->ZERO, w[364]); 
  FFV2_3_2(w[285], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[365]);
  FFV1P0_3(w[108], w[361], pars->GC_11, pars->ZERO, pars->ZERO, w[366]); 
  FFV2_3_1(w[361], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[367]);
  FFV1P0_3(w[247], w[283], pars->GC_11, pars->ZERO, pars->ZERO, w[368]); 
  FFV2_3_2(w[284], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[369]);
  FFV2_3_1(w[307], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[370]);
  FFV1P0_3(w[369], w[107], pars->GC_11, pars->ZERO, pars->ZERO, w[371]); 
  FFV2_3_2(w[369], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[372]);
  FFV1P0_3(w[369], w[307], pars->GC_11, pars->ZERO, pars->ZERO, w[373]); 
  FFV1P0_3(w[247], w[307], pars->GC_11, pars->ZERO, pars->ZERO, w[374]); 
  FFV2_3_2(w[284], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[375]);
  FFV2_3_1(w[307], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[376]);
  FFV1P0_3(w[375], w[107], pars->GC_11, pars->ZERO, pars->ZERO, w[377]); 
  FFV2_3_2(w[375], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[378]);
  FFV1P0_3(w[375], w[307], pars->GC_11, pars->ZERO, pars->ZERO, w[379]); 
  FFV1P0_3(w[251], w[307], pars->GC_11, pars->ZERO, pars->ZERO, w[380]); 
  FFV1P0_3(w[284], w[243], pars->GC_11, pars->ZERO, pars->ZERO, w[381]); 
  FFV1P0_3(w[284], w[249], pars->GC_11, pars->ZERO, pars->ZERO, w[382]); 
  FFV1P0_3(w[369], w[283], pars->GC_11, pars->ZERO, pars->ZERO, w[383]); 
  FFV1P0_3(w[369], w[134], pars->GC_11, pars->ZERO, pars->ZERO, w[384]); 
  FFV1P0_3(w[375], w[283], pars->GC_11, pars->ZERO, pars->ZERO, w[385]); 
  FFV1P0_3(w[375], w[134], pars->GC_11, pars->ZERO, pars->ZERO, w[386]); 
  FFV1P0_3(w[284], w[355], pars->GC_11, pars->ZERO, pars->ZERO, w[387]); 
  FFV1P0_3(w[284], w[361], pars->GC_11, pars->ZERO, pars->ZERO, w[388]); 
  FFV1P0_3(w[159], w[361], pars->GC_11, pars->ZERO, pars->ZERO, w[389]); 
  FFV1P0_3(w[159], w[355], pars->GC_11, pars->ZERO, pars->ZERO, w[390]); 
  FFV1_2(w[369], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[391]); 
  FFV1_1(w[361], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[392]); 
  FFV1_2(w[375], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[393]); 
  FFV1_1(w[355], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[394]); 

  // Calculate all amplitudes
  // Amplitude(s) for diagram number 0
  FFV1_0(w[9], w[6], w[10], pars->GC_11, amp[0]); 
  FFV1_0(w[1], w[12], w[11], pars->GC_11, amp[1]); 
  FFV1_0(w[1], w[13], w[14], pars->GC_11, amp[2]); 
  FFV1_0(w[1], w[8], w[15], pars->GC_11, amp[3]); 
  FFV1_0(w[16], w[6], w[14], pars->GC_11, amp[4]); 
  FFV1_0(w[16], w[8], w[11], pars->GC_11, amp[5]); 
  FFV1_0(w[9], w[8], w[17], pars->GC_11, amp[6]); 
  FFV2_5_0(w[18], w[8], w[3], pars->GC_51, pars->GC_58, amp[7]); 
  FFV1_0(w[9], w[5], w[20], pars->GC_11, amp[8]); 
  FFV1_0(w[1], w[22], w[21], pars->GC_11, amp[9]); 
  FFV1_0(w[1], w[23], w[24], pars->GC_11, amp[10]); 
  FFV1_0(w[1], w[19], w[25], pars->GC_11, amp[11]); 
  FFV1_0(w[16], w[5], w[24], pars->GC_11, amp[12]); 
  FFV1_0(w[16], w[19], w[21], pars->GC_11, amp[13]); 
  FFV1_0(w[9], w[19], w[26], pars->GC_11, amp[14]); 
  FFV2_5_0(w[27], w[19], w[3], pars->GC_51, pars->GC_58, amp[15]); 
  FFV1_0(w[9], w[6], w[29], pars->GC_11, amp[16]); 
  FFV1_0(w[9], w[5], w[30], pars->GC_11, amp[17]); 
  FFV1_0(w[31], w[6], w[21], pars->GC_11, amp[18]); 
  FFV1_0(w[31], w[5], w[11], pars->GC_11, amp[19]); 
  FFV1_0(w[28], w[6], w[25], pars->GC_11, amp[20]); 
  FFV1_0(w[28], w[23], w[11], pars->GC_11, amp[21]); 
  FFV1_0(w[28], w[5], w[15], pars->GC_11, amp[22]); 
  FFV1_0(w[28], w[13], w[21], pars->GC_11, amp[23]); 
  FFV1_0(w[32], w[6], w[33], pars->GC_11, amp[24]); 
  FFV1_0(w[1], w[34], w[11], pars->GC_11, amp[25]); 
  FFV1_0(w[32], w[23], w[17], pars->GC_11, amp[26]); 
  FFV2_5_0(w[18], w[23], w[2], pars->GC_51, pars->GC_58, amp[27]); 
  FFV1_0(w[32], w[5], w[35], pars->GC_11, amp[28]); 
  FFV1_0(w[1], w[36], w[21], pars->GC_11, amp[29]); 
  FFV1_0(w[32], w[13], w[26], pars->GC_11, amp[30]); 
  FFV2_5_0(w[27], w[13], w[2], pars->GC_51, pars->GC_58, amp[31]); 
  FFV1_0(w[32], w[6], w[37], pars->GC_11, amp[32]); 
  FFV1_0(w[32], w[5], w[38], pars->GC_11, amp[33]); 
  FFV1_0(w[39], w[6], w[21], pars->GC_11, amp[34]); 
  FFV1_0(w[39], w[5], w[11], pars->GC_11, amp[35]); 
  FFV2_5_0(w[32], w[40], w[3], pars->GC_51, pars->GC_58, amp[36]); 
  FFV2_5_0(w[9], w[40], w[2], pars->GC_51, pars->GC_58, amp[37]); 
  FFV2_5_0(w[32], w[41], w[3], pars->GC_51, pars->GC_58, amp[38]); 
  FFV2_5_0(w[9], w[41], w[2], pars->GC_51, pars->GC_58, amp[39]); 
  FFV1_0(w[1], w[44], w[45], pars->GC_11, amp[40]); 
  FFV1_0(w[47], w[6], w[46], pars->GC_11, amp[41]); 
  FFV1_0(w[1], w[13], w[48], pars->GC_11, amp[42]); 
  FFV1_0(w[43], w[13], w[46], pars->GC_11, amp[43]); 
  FFV1_0(w[16], w[6], w[48], pars->GC_11, amp[44]); 
  FFV1_0(w[43], w[6], w[49], pars->GC_11, amp[45]); 
  FFV1_0(w[43], w[44], w[17], pars->GC_11, amp[46]); 
  FFV2_5_0(w[43], w[50], w[3], pars->GC_51, pars->GC_58, amp[47]); 
  FFV1_0(w[1], w[52], w[53], pars->GC_11, amp[48]); 
  FFV1_0(w[54], w[6], w[46], pars->GC_11, amp[49]); 
  FFV1_0(w[1], w[19], w[55], pars->GC_11, amp[50]); 
  FFV1_0(w[51], w[19], w[46], pars->GC_11, amp[51]); 
  FFV1_0(w[28], w[6], w[55], pars->GC_11, amp[52]); 
  FFV1_0(w[51], w[6], w[56], pars->GC_11, amp[53]); 
  FFV1_0(w[51], w[52], w[17], pars->GC_11, amp[54]); 
  FFV2_5_0(w[51], w[50], w[2], pars->GC_51, pars->GC_58, amp[55]); 
  FFV2_5_0(w[58], w[52], w[3], pars->GC_51, pars->GC_58, amp[56]); 
  FFV2_5_0(w[58], w[44], w[2], pars->GC_51, pars->GC_58, amp[57]); 
  FFV2_5_0(w[28], w[59], w[3], pars->GC_51, pars->GC_58, amp[58]); 
  FFV1_0(w[28], w[44], w[57], pars->GC_11, amp[59]); 
  FFV2_5_0(w[16], w[59], w[2], pars->GC_51, pars->GC_58, amp[60]); 
  FFV1_0(w[16], w[52], w[57], pars->GC_11, amp[61]); 
  FFV1_0(w[1], w[22], w[60], pars->GC_11, amp[62]); 
  FFV1_0(w[1], w[44], w[61], pars->GC_11, amp[63]); 
  FFV1_0(w[0], w[44], w[20], pars->GC_11, amp[64]); 
  FFV1_0(w[0], w[22], w[46], pars->GC_11, amp[65]); 
  FFV1_0(w[16], w[19], w[60], pars->GC_11, amp[66]); 
  FFV1_0(w[0], w[19], w[49], pars->GC_11, amp[67]); 
  FFV1_0(w[31], w[6], w[60], pars->GC_11, amp[68]); 
  FFV1_0(w[0], w[44], w[30], pars->GC_11, amp[69]); 
  FFV1_0(w[28], w[13], w[60], pars->GC_11, amp[70]); 
  FFV1_0(w[0], w[13], w[56], pars->GC_11, amp[71]); 
  FFV1_0(w[1], w[36], w[60], pars->GC_11, amp[72]); 
  FFV1_0(w[1], w[52], w[62], pars->GC_11, amp[73]); 
  FFV1_0(w[0], w[52], w[35], pars->GC_11, amp[74]); 
  FFV1_0(w[0], w[36], w[46], pars->GC_11, amp[75]); 
  FFV1_0(w[39], w[6], w[60], pars->GC_11, amp[76]); 
  FFV1_0(w[0], w[52], w[38], pars->GC_11, amp[77]); 
  FFV2_5_0(w[63], w[52], w[3], pars->GC_51, pars->GC_58, amp[78]); 
  FFV2_5_0(w[63], w[44], w[2], pars->GC_51, pars->GC_58, amp[79]); 
  FFV1_0(w[1], w[65], w[66], pars->GC_11, amp[80]); 
  FFV1_0(w[47], w[5], w[67], pars->GC_11, amp[81]); 
  FFV1_0(w[1], w[23], w[68], pars->GC_11, amp[82]); 
  FFV1_0(w[43], w[23], w[67], pars->GC_11, amp[83]); 
  FFV1_0(w[16], w[5], w[68], pars->GC_11, amp[84]); 
  FFV1_0(w[43], w[5], w[69], pars->GC_11, amp[85]); 
  FFV1_0(w[43], w[65], w[26], pars->GC_11, amp[86]); 
  FFV2_5_0(w[43], w[70], w[3], pars->GC_51, pars->GC_58, amp[87]); 
  FFV1_0(w[1], w[71], w[72], pars->GC_11, amp[88]); 
  FFV1_0(w[54], w[5], w[67], pars->GC_11, amp[89]); 
  FFV1_0(w[1], w[8], w[73], pars->GC_11, amp[90]); 
  FFV1_0(w[51], w[8], w[67], pars->GC_11, amp[91]); 
  FFV1_0(w[28], w[5], w[73], pars->GC_11, amp[92]); 
  FFV1_0(w[51], w[5], w[74], pars->GC_11, amp[93]); 
  FFV1_0(w[51], w[71], w[26], pars->GC_11, amp[94]); 
  FFV2_5_0(w[51], w[70], w[2], pars->GC_51, pars->GC_58, amp[95]); 
  FFV2_5_0(w[76], w[71], w[3], pars->GC_51, pars->GC_58, amp[96]); 
  FFV2_5_0(w[76], w[65], w[2], pars->GC_51, pars->GC_58, amp[97]); 
  FFV2_5_0(w[28], w[77], w[3], pars->GC_51, pars->GC_58, amp[98]); 
  FFV1_0(w[28], w[65], w[75], pars->GC_11, amp[99]); 
  FFV2_5_0(w[16], w[77], w[2], pars->GC_51, pars->GC_58, amp[100]); 
  FFV1_0(w[16], w[71], w[75], pars->GC_11, amp[101]); 
  FFV1_0(w[1], w[12], w[78], pars->GC_11, amp[102]); 
  FFV1_0(w[1], w[65], w[79], pars->GC_11, amp[103]); 
  FFV1_0(w[0], w[65], w[10], pars->GC_11, amp[104]); 
  FFV1_0(w[0], w[12], w[67], pars->GC_11, amp[105]); 
  FFV1_0(w[16], w[8], w[78], pars->GC_11, amp[106]); 
  FFV1_0(w[0], w[8], w[69], pars->GC_11, amp[107]); 
  FFV1_0(w[31], w[5], w[78], pars->GC_11, amp[108]); 
  FFV1_0(w[0], w[65], w[29], pars->GC_11, amp[109]); 
  FFV1_0(w[28], w[23], w[78], pars->GC_11, amp[110]); 
  FFV1_0(w[0], w[23], w[74], pars->GC_11, amp[111]); 
  FFV1_0(w[1], w[34], w[78], pars->GC_11, amp[112]); 
  FFV1_0(w[1], w[71], w[80], pars->GC_11, amp[113]); 
  FFV1_0(w[0], w[71], w[33], pars->GC_11, amp[114]); 
  FFV1_0(w[0], w[34], w[67], pars->GC_11, amp[115]); 
  FFV1_0(w[39], w[5], w[78], pars->GC_11, amp[116]); 
  FFV1_0(w[0], w[71], w[37], pars->GC_11, amp[117]); 
  FFV2_5_0(w[81], w[71], w[3], pars->GC_51, pars->GC_58, amp[118]); 
  FFV2_5_0(w[81], w[65], w[2], pars->GC_51, pars->GC_58, amp[119]); 
  FFV1_0(w[83], w[6], w[66], pars->GC_11, amp[120]); 
  FFV1_0(w[83], w[5], w[45], pars->GC_11, amp[121]); 
  FFV1_0(w[47], w[6], w[84], pars->GC_11, amp[122]); 
  FFV1_0(w[47], w[5], w[85], pars->GC_11, amp[123]); 
  FFV1_0(w[43], w[6], w[86], pars->GC_11, amp[124]); 
  FFV1_0(w[43], w[23], w[85], pars->GC_11, amp[125]); 
  FFV1_0(w[43], w[5], w[87], pars->GC_11, amp[126]); 
  FFV1_0(w[43], w[13], w[84], pars->GC_11, amp[127]); 
  FFV1_0(w[88], w[6], w[72], pars->GC_11, amp[128]); 
  FFV1_0(w[88], w[5], w[53], pars->GC_11, amp[129]); 
  FFV1_0(w[54], w[6], w[84], pars->GC_11, amp[130]); 
  FFV1_0(w[54], w[5], w[85], pars->GC_11, amp[131]); 
  FFV1_0(w[51], w[6], w[89], pars->GC_11, amp[132]); 
  FFV1_0(w[51], w[8], w[85], pars->GC_11, amp[133]); 
  FFV1_0(w[51], w[5], w[90], pars->GC_11, amp[134]); 
  FFV1_0(w[51], w[19], w[84], pars->GC_11, amp[135]); 
  FFV2_5_0(w[88], w[91], w[3], pars->GC_51, pars->GC_58, amp[136]); 
  FFV2_5_0(w[83], w[91], w[2], pars->GC_51, pars->GC_58, amp[137]); 
  FFV2_5_0(w[92], w[19], w[3], pars->GC_51, pars->GC_58, amp[138]); 
  FFV1_0(w[83], w[19], w[75], pars->GC_11, amp[139]); 
  FFV2_5_0(w[92], w[13], w[2], pars->GC_51, pars->GC_58, amp[140]); 
  FFV1_0(w[88], w[13], w[75], pars->GC_11, amp[141]); 
  FFV2_5_0(w[88], w[93], w[3], pars->GC_51, pars->GC_58, amp[142]); 
  FFV2_5_0(w[83], w[93], w[2], pars->GC_51, pars->GC_58, amp[143]); 
  FFV2_5_0(w[94], w[8], w[3], pars->GC_51, pars->GC_58, amp[144]); 
  FFV1_0(w[83], w[8], w[57], pars->GC_11, amp[145]); 
  FFV2_5_0(w[94], w[23], w[2], pars->GC_51, pars->GC_58, amp[146]); 
  FFV1_0(w[88], w[23], w[57], pars->GC_11, amp[147]); 
  FFV1_0(w[83], w[6], w[79], pars->GC_11, amp[148]); 
  FFV1_0(w[0], w[12], w[85], pars->GC_11, amp[149]); 
  FFV1_0(w[0], w[13], w[89], pars->GC_11, amp[150]); 
  FFV1_0(w[0], w[8], w[87], pars->GC_11, amp[151]); 
  FFV1_0(w[83], w[5], w[61], pars->GC_11, amp[152]); 
  FFV1_0(w[0], w[22], w[84], pars->GC_11, amp[153]); 
  FFV1_0(w[0], w[23], w[90], pars->GC_11, amp[154]); 
  FFV1_0(w[0], w[19], w[86], pars->GC_11, amp[155]); 
  FFV1_0(w[88], w[6], w[80], pars->GC_11, amp[156]); 
  FFV1_0(w[0], w[34], w[85], pars->GC_11, amp[157]); 
  FFV1_0(w[88], w[5], w[62], pars->GC_11, amp[158]); 
  FFV1_0(w[0], w[36], w[84], pars->GC_11, amp[159]); 
  FFV1_0(w[95], w[6], w[33], pars->GC_11, amp[160]); 
  FFV1_0(w[1], w[96], w[45], pars->GC_11, amp[161]); 
  VVV1_0(w[4], w[45], w[33], pars->GC_10, amp[162]); 
  FFV1_0(w[95], w[23], w[17], pars->GC_11, amp[163]); 
  FFV1_0(w[43], w[96], w[17], pars->GC_11, amp[164]); 
  FFV1_0(w[43], w[23], w[97], pars->GC_11, amp[165]); 
  FFV1_0(w[95], w[5], w[35], pars->GC_11, amp[166]); 
  FFV1_0(w[1], w[98], w[66], pars->GC_11, amp[167]); 
  VVV1_0(w[4], w[66], w[35], pars->GC_10, amp[168]); 
  FFV1_0(w[95], w[13], w[26], pars->GC_11, amp[169]); 
  FFV1_0(w[43], w[98], w[26], pars->GC_11, amp[170]); 
  FFV1_0(w[43], w[13], w[99], pars->GC_11, amp[171]); 
  FFV1_0(w[95], w[6], w[37], pars->GC_11, amp[172]); 
  FFV1_0(w[95], w[5], w[38], pars->GC_11, amp[173]); 
  FFV1_0(w[100], w[6], w[66], pars->GC_11, amp[174]); 
  FFV1_0(w[100], w[5], w[45], pars->GC_11, amp[175]); 
  VVV1_0(w[4], w[66], w[38], pars->GC_10, amp[176]); 
  VVV1_0(w[4], w[45], w[37], pars->GC_10, amp[177]); 
  FFV2_5_0(w[95], w[40], w[3], pars->GC_51, pars->GC_58, amp[178]); 
  FFV1_0(w[47], w[6], w[99], pars->GC_11, amp[179]); 
  FFV1_0(w[47], w[40], w[4], pars->GC_11, amp[180]); 
  FFV2_5_0(w[95], w[41], w[3], pars->GC_51, pars->GC_58, amp[181]); 
  FFV1_0(w[47], w[5], w[97], pars->GC_11, amp[182]); 
  FFV1_0(w[47], w[41], w[4], pars->GC_11, amp[183]); 
  FFV1_0(w[101], w[6], w[10], pars->GC_11, amp[184]); 
  FFV1_0(w[1], w[102], w[53], pars->GC_11, amp[185]); 
  VVV1_0(w[4], w[53], w[10], pars->GC_10, amp[186]); 
  FFV1_0(w[101], w[8], w[17], pars->GC_11, amp[187]); 
  FFV1_0(w[51], w[102], w[17], pars->GC_11, amp[188]); 
  FFV1_0(w[51], w[8], w[97], pars->GC_11, amp[189]); 
  FFV1_0(w[101], w[5], w[20], pars->GC_11, amp[190]); 
  FFV1_0(w[1], w[103], w[72], pars->GC_11, amp[191]); 
  VVV1_0(w[4], w[72], w[20], pars->GC_10, amp[192]); 
  FFV1_0(w[101], w[19], w[26], pars->GC_11, amp[193]); 
  FFV1_0(w[51], w[103], w[26], pars->GC_11, amp[194]); 
  FFV1_0(w[51], w[19], w[99], pars->GC_11, amp[195]); 
  FFV1_0(w[101], w[6], w[29], pars->GC_11, amp[196]); 
  FFV1_0(w[101], w[5], w[30], pars->GC_11, amp[197]); 
  FFV1_0(w[104], w[6], w[72], pars->GC_11, amp[198]); 
  FFV1_0(w[104], w[5], w[53], pars->GC_11, amp[199]); 
  VVV1_0(w[4], w[72], w[30], pars->GC_10, amp[200]); 
  VVV1_0(w[4], w[53], w[29], pars->GC_10, amp[201]); 
  FFV2_5_0(w[101], w[40], w[2], pars->GC_51, pars->GC_58, amp[202]); 
  FFV1_0(w[54], w[6], w[99], pars->GC_11, amp[203]); 
  FFV1_0(w[54], w[40], w[4], pars->GC_11, amp[204]); 
  FFV2_5_0(w[101], w[41], w[2], pars->GC_51, pars->GC_58, amp[205]); 
  FFV1_0(w[54], w[5], w[97], pars->GC_11, amp[206]); 
  FFV1_0(w[54], w[41], w[4], pars->GC_11, amp[207]); 
  FFV1_0(w[1], w[22], w[105], pars->GC_11, amp[208]); 
  FFV2_5_0(w[76], w[103], w[3], pars->GC_51, pars->GC_58, amp[209]); 
  FFV1_0(w[76], w[22], w[4], pars->GC_11, amp[210]); 
  FFV1_0(w[16], w[19], w[105], pars->GC_11, amp[211]); 
  FFV1_0(w[16], w[103], w[75], pars->GC_11, amp[212]); 
  FFV1_0(w[100], w[19], w[75], pars->GC_11, amp[213]); 
  FFV1_0(w[31], w[6], w[105], pars->GC_11, amp[214]); 
  FFV2_5_0(w[104], w[91], w[3], pars->GC_51, pars->GC_58, amp[215]); 
  FFV1_0(w[31], w[91], w[4], pars->GC_11, amp[216]); 
  FFV1_0(w[28], w[13], w[105], pars->GC_11, amp[217]); 
  FFV1_0(w[104], w[13], w[75], pars->GC_11, amp[218]); 
  FFV1_0(w[28], w[98], w[75], pars->GC_11, amp[219]); 
  FFV1_0(w[1], w[36], w[105], pars->GC_11, amp[220]); 
  FFV2_5_0(w[76], w[98], w[2], pars->GC_51, pars->GC_58, amp[221]); 
  FFV1_0(w[76], w[36], w[4], pars->GC_11, amp[222]); 
  FFV1_0(w[39], w[6], w[105], pars->GC_11, amp[223]); 
  FFV2_5_0(w[100], w[91], w[2], pars->GC_51, pars->GC_58, amp[224]); 
  FFV1_0(w[39], w[91], w[4], pars->GC_11, amp[225]); 
  FFV1_0(w[1], w[12], w[106], pars->GC_11, amp[226]); 
  FFV2_5_0(w[58], w[102], w[3], pars->GC_51, pars->GC_58, amp[227]); 
  FFV1_0(w[58], w[12], w[4], pars->GC_11, amp[228]); 
  FFV1_0(w[16], w[8], w[106], pars->GC_11, amp[229]); 
  FFV1_0(w[16], w[102], w[57], pars->GC_11, amp[230]); 
  FFV1_0(w[100], w[8], w[57], pars->GC_11, amp[231]); 
  FFV1_0(w[31], w[5], w[106], pars->GC_11, amp[232]); 
  FFV2_5_0(w[104], w[93], w[3], pars->GC_51, pars->GC_58, amp[233]); 
  FFV1_0(w[31], w[93], w[4], pars->GC_11, amp[234]); 
  FFV1_0(w[28], w[23], w[106], pars->GC_11, amp[235]); 
  FFV1_0(w[104], w[23], w[57], pars->GC_11, amp[236]); 
  FFV1_0(w[28], w[96], w[57], pars->GC_11, amp[237]); 
  FFV1_0(w[1], w[34], w[106], pars->GC_11, amp[238]); 
  FFV2_5_0(w[58], w[96], w[2], pars->GC_51, pars->GC_58, amp[239]); 
  FFV1_0(w[58], w[34], w[4], pars->GC_11, amp[240]); 
  FFV1_0(w[39], w[5], w[106], pars->GC_11, amp[241]); 
  FFV2_5_0(w[100], w[93], w[2], pars->GC_51, pars->GC_58, amp[242]); 
  FFV1_0(w[39], w[93], w[4], pars->GC_11, amp[243]); 
  FFV1_0(w[1], w[102], w[62], pars->GC_11, amp[244]); 
  FFV1_0(w[0], w[102], w[35], pars->GC_11, amp[245]); 
  FFV1_0(w[1], w[98], w[79], pars->GC_11, amp[246]); 
  FFV1_0(w[0], w[98], w[10], pars->GC_11, amp[247]); 
  VVV1_0(w[4], w[79], w[35], pars->GC_10, amp[248]); 
  VVV1_0(w[4], w[62], w[10], pars->GC_10, amp[249]); 
  FFV1_0(w[0], w[102], w[38], pars->GC_11, amp[250]); 
  FFV1_0(w[100], w[6], w[79], pars->GC_11, amp[251]); 
  VVV1_0(w[4], w[79], w[38], pars->GC_10, amp[252]); 
  FFV2_5_0(w[63], w[102], w[3], pars->GC_51, pars->GC_58, amp[253]); 
  FFV1_0(w[0], w[12], w[97], pars->GC_11, amp[254]); 
  FFV1_0(w[63], w[12], w[4], pars->GC_11, amp[255]); 
  FFV1_0(w[1], w[103], w[80], pars->GC_11, amp[256]); 
  FFV1_0(w[0], w[103], w[33], pars->GC_11, amp[257]); 
  FFV1_0(w[1], w[96], w[61], pars->GC_11, amp[258]); 
  FFV1_0(w[0], w[96], w[20], pars->GC_11, amp[259]); 
  VVV1_0(w[4], w[61], w[33], pars->GC_10, amp[260]); 
  VVV1_0(w[4], w[80], w[20], pars->GC_10, amp[261]); 
  FFV1_0(w[0], w[103], w[37], pars->GC_11, amp[262]); 
  FFV1_0(w[100], w[5], w[61], pars->GC_11, amp[263]); 
  VVV1_0(w[4], w[61], w[37], pars->GC_10, amp[264]); 
  FFV2_5_0(w[81], w[103], w[3], pars->GC_51, pars->GC_58, amp[265]); 
  FFV1_0(w[0], w[22], w[99], pars->GC_11, amp[266]); 
  FFV1_0(w[81], w[22], w[4], pars->GC_11, amp[267]); 
  FFV1_0(w[104], w[6], w[80], pars->GC_11, amp[268]); 
  FFV1_0(w[0], w[96], w[30], pars->GC_11, amp[269]); 
  VVV1_0(w[4], w[80], w[30], pars->GC_10, amp[270]); 
  FFV1_0(w[104], w[5], w[62], pars->GC_11, amp[271]); 
  FFV1_0(w[0], w[98], w[29], pars->GC_11, amp[272]); 
  VVV1_0(w[4], w[62], w[29], pars->GC_10, amp[273]); 
  FFV2_5_0(w[63], w[96], w[2], pars->GC_51, pars->GC_58, amp[274]); 
  FFV1_0(w[0], w[34], w[97], pars->GC_11, amp[275]); 
  FFV1_0(w[63], w[34], w[4], pars->GC_11, amp[276]); 
  FFV2_5_0(w[81], w[98], w[2], pars->GC_51, pars->GC_58, amp[277]); 
  FFV1_0(w[0], w[36], w[99], pars->GC_11, amp[278]); 
  FFV1_0(w[81], w[36], w[4], pars->GC_11, amp[279]); 
  FFV1_0(w[9], w[5], w[110], pars->GC_11, amp[280]); 
  FFV1_0(w[108], w[111], w[21], pars->GC_11, amp[281]); 
  FFV1_0(w[108], w[23], w[112], pars->GC_11, amp[282]); 
  FFV1_0(w[108], w[109], w[25], pars->GC_11, amp[283]); 
  FFV1_0(w[113], w[5], w[112], pars->GC_11, amp[284]); 
  FFV1_0(w[113], w[109], w[21], pars->GC_11, amp[285]); 
  FFV1_0(w[9], w[109], w[114], pars->GC_11, amp[286]); 
  FFV2_5_0(w[115], w[109], w[3], pars->GC_51, pars->GC_58, amp[287]); 
  FFV1_0(w[9], w[107], w[116], pars->GC_11, amp[288]); 
  FFV1_0(w[108], w[12], w[117], pars->GC_11, amp[289]); 
  FFV1_0(w[108], w[118], w[14], pars->GC_11, amp[290]); 
  FFV1_0(w[108], w[8], w[119], pars->GC_11, amp[291]); 
  FFV1_0(w[113], w[107], w[14], pars->GC_11, amp[292]); 
  FFV1_0(w[113], w[8], w[117], pars->GC_11, amp[293]); 
  FFV1_0(w[9], w[8], w[120], pars->GC_11, amp[294]); 
  FFV2_5_0(w[121], w[8], w[3], pars->GC_51, pars->GC_58, amp[295]); 
  FFV1_0(w[9], w[5], w[123], pars->GC_11, amp[296]); 
  FFV1_0(w[9], w[107], w[124], pars->GC_11, amp[297]); 
  FFV1_0(w[125], w[5], w[117], pars->GC_11, amp[298]); 
  FFV1_0(w[125], w[107], w[21], pars->GC_11, amp[299]); 
  FFV1_0(w[122], w[5], w[119], pars->GC_11, amp[300]); 
  FFV1_0(w[122], w[118], w[21], pars->GC_11, amp[301]); 
  FFV1_0(w[122], w[107], w[25], pars->GC_11, amp[302]); 
  FFV1_0(w[122], w[23], w[117], pars->GC_11, amp[303]); 
  FFV1_0(w[32], w[5], w[126], pars->GC_11, amp[304]); 
  FFV1_0(w[108], w[127], w[21], pars->GC_11, amp[305]); 
  FFV1_0(w[32], w[118], w[114], pars->GC_11, amp[306]); 
  FFV2_5_0(w[115], w[118], w[2], pars->GC_51, pars->GC_58, amp[307]); 
  FFV1_0(w[32], w[107], w[128], pars->GC_11, amp[308]); 
  FFV1_0(w[108], w[34], w[117], pars->GC_11, amp[309]); 
  FFV1_0(w[32], w[23], w[120], pars->GC_11, amp[310]); 
  FFV2_5_0(w[121], w[23], w[2], pars->GC_51, pars->GC_58, amp[311]); 
  FFV1_0(w[32], w[5], w[129], pars->GC_11, amp[312]); 
  FFV1_0(w[32], w[107], w[130], pars->GC_11, amp[313]); 
  FFV1_0(w[131], w[5], w[117], pars->GC_11, amp[314]); 
  FFV1_0(w[131], w[107], w[21], pars->GC_11, amp[315]); 
  FFV2_5_0(w[32], w[132], w[3], pars->GC_51, pars->GC_58, amp[316]); 
  FFV2_5_0(w[9], w[132], w[2], pars->GC_51, pars->GC_58, amp[317]); 
  FFV2_5_0(w[32], w[133], w[3], pars->GC_51, pars->GC_58, amp[318]); 
  FFV2_5_0(w[9], w[133], w[2], pars->GC_51, pars->GC_58, amp[319]); 
  FFV1_0(w[108], w[135], w[66], pars->GC_11, amp[320]); 
  FFV1_0(w[47], w[5], w[136], pars->GC_11, amp[321]); 
  FFV1_0(w[108], w[23], w[137], pars->GC_11, amp[322]); 
  FFV1_0(w[43], w[23], w[136], pars->GC_11, amp[323]); 
  FFV1_0(w[113], w[5], w[137], pars->GC_11, amp[324]); 
  FFV1_0(w[43], w[5], w[138], pars->GC_11, amp[325]); 
  FFV1_0(w[43], w[135], w[114], pars->GC_11, amp[326]); 
  FFV2_5_0(w[43], w[139], w[3], pars->GC_51, pars->GC_58, amp[327]); 
  FFV1_0(w[108], w[140], w[72], pars->GC_11, amp[328]); 
  FFV1_0(w[54], w[5], w[136], pars->GC_11, amp[329]); 
  FFV1_0(w[108], w[8], w[141], pars->GC_11, amp[330]); 
  FFV1_0(w[51], w[8], w[136], pars->GC_11, amp[331]); 
  FFV1_0(w[122], w[5], w[141], pars->GC_11, amp[332]); 
  FFV1_0(w[51], w[5], w[142], pars->GC_11, amp[333]); 
  FFV1_0(w[51], w[140], w[114], pars->GC_11, amp[334]); 
  FFV2_5_0(w[51], w[139], w[2], pars->GC_51, pars->GC_58, amp[335]); 
  FFV2_5_0(w[143], w[140], w[3], pars->GC_51, pars->GC_58, amp[336]); 
  FFV2_5_0(w[143], w[135], w[2], pars->GC_51, pars->GC_58, amp[337]); 
  FFV2_5_0(w[122], w[144], w[3], pars->GC_51, pars->GC_58, amp[338]); 
  FFV1_0(w[122], w[135], w[75], pars->GC_11, amp[339]); 
  FFV2_5_0(w[113], w[144], w[2], pars->GC_51, pars->GC_58, amp[340]); 
  FFV1_0(w[113], w[140], w[75], pars->GC_11, amp[341]); 
  FFV1_0(w[108], w[12], w[145], pars->GC_11, amp[342]); 
  FFV1_0(w[108], w[135], w[79], pars->GC_11, amp[343]); 
  FFV1_0(w[0], w[135], w[116], pars->GC_11, amp[344]); 
  FFV1_0(w[0], w[12], w[136], pars->GC_11, amp[345]); 
  FFV1_0(w[113], w[8], w[145], pars->GC_11, amp[346]); 
  FFV1_0(w[0], w[8], w[138], pars->GC_11, amp[347]); 
  FFV1_0(w[125], w[5], w[145], pars->GC_11, amp[348]); 
  FFV1_0(w[0], w[135], w[124], pars->GC_11, amp[349]); 
  FFV1_0(w[122], w[23], w[145], pars->GC_11, amp[350]); 
  FFV1_0(w[0], w[23], w[142], pars->GC_11, amp[351]); 
  FFV1_0(w[108], w[34], w[145], pars->GC_11, amp[352]); 
  FFV1_0(w[108], w[140], w[80], pars->GC_11, amp[353]); 
  FFV1_0(w[0], w[140], w[128], pars->GC_11, amp[354]); 
  FFV1_0(w[0], w[34], w[136], pars->GC_11, amp[355]); 
  FFV1_0(w[131], w[5], w[145], pars->GC_11, amp[356]); 
  FFV1_0(w[0], w[140], w[130], pars->GC_11, amp[357]); 
  FFV2_5_0(w[146], w[140], w[3], pars->GC_51, pars->GC_58, amp[358]); 
  FFV2_5_0(w[146], w[135], w[2], pars->GC_51, pars->GC_58, amp[359]); 
  FFV1_0(w[108], w[44], w[147], pars->GC_11, amp[360]); 
  FFV1_0(w[47], w[107], w[148], pars->GC_11, amp[361]); 
  FFV1_0(w[108], w[118], w[48], pars->GC_11, amp[362]); 
  FFV1_0(w[43], w[118], w[148], pars->GC_11, amp[363]); 
  FFV1_0(w[113], w[107], w[48], pars->GC_11, amp[364]); 
  FFV1_0(w[43], w[107], w[149], pars->GC_11, amp[365]); 
  FFV1_0(w[43], w[44], w[120], pars->GC_11, amp[366]); 
  FFV2_5_0(w[43], w[150], w[3], pars->GC_51, pars->GC_58, amp[367]); 
  FFV1_0(w[108], w[52], w[151], pars->GC_11, amp[368]); 
  FFV1_0(w[54], w[107], w[148], pars->GC_11, amp[369]); 
  FFV1_0(w[108], w[109], w[55], pars->GC_11, amp[370]); 
  FFV1_0(w[51], w[109], w[148], pars->GC_11, amp[371]); 
  FFV1_0(w[122], w[107], w[55], pars->GC_11, amp[372]); 
  FFV1_0(w[51], w[107], w[152], pars->GC_11, amp[373]); 
  FFV1_0(w[51], w[52], w[120], pars->GC_11, amp[374]); 
  FFV2_5_0(w[51], w[150], w[2], pars->GC_51, pars->GC_58, amp[375]); 
  FFV2_5_0(w[154], w[52], w[3], pars->GC_51, pars->GC_58, amp[376]); 
  FFV2_5_0(w[154], w[44], w[2], pars->GC_51, pars->GC_58, amp[377]); 
  FFV2_5_0(w[122], w[155], w[3], pars->GC_51, pars->GC_58, amp[378]); 
  FFV1_0(w[122], w[44], w[153], pars->GC_11, amp[379]); 
  FFV2_5_0(w[113], w[155], w[2], pars->GC_51, pars->GC_58, amp[380]); 
  FFV1_0(w[113], w[52], w[153], pars->GC_11, amp[381]); 
  FFV1_0(w[108], w[111], w[60], pars->GC_11, amp[382]); 
  FFV1_0(w[108], w[44], w[156], pars->GC_11, amp[383]); 
  FFV1_0(w[0], w[44], w[110], pars->GC_11, amp[384]); 
  FFV1_0(w[0], w[111], w[148], pars->GC_11, amp[385]); 
  FFV1_0(w[113], w[109], w[60], pars->GC_11, amp[386]); 
  FFV1_0(w[0], w[109], w[149], pars->GC_11, amp[387]); 
  FFV1_0(w[125], w[107], w[60], pars->GC_11, amp[388]); 
  FFV1_0(w[0], w[44], w[123], pars->GC_11, amp[389]); 
  FFV1_0(w[122], w[118], w[60], pars->GC_11, amp[390]); 
  FFV1_0(w[0], w[118], w[152], pars->GC_11, amp[391]); 
  FFV1_0(w[108], w[127], w[60], pars->GC_11, amp[392]); 
  FFV1_0(w[108], w[52], w[157], pars->GC_11, amp[393]); 
  FFV1_0(w[0], w[52], w[126], pars->GC_11, amp[394]); 
  FFV1_0(w[0], w[127], w[148], pars->GC_11, amp[395]); 
  FFV1_0(w[131], w[107], w[60], pars->GC_11, amp[396]); 
  FFV1_0(w[0], w[52], w[129], pars->GC_11, amp[397]); 
  FFV2_5_0(w[158], w[52], w[3], pars->GC_51, pars->GC_58, amp[398]); 
  FFV2_5_0(w[158], w[44], w[2], pars->GC_51, pars->GC_58, amp[399]); 
  FFV1_0(w[160], w[5], w[147], pars->GC_11, amp[400]); 
  FFV1_0(w[160], w[107], w[66], pars->GC_11, amp[401]); 
  FFV1_0(w[47], w[5], w[161], pars->GC_11, amp[402]); 
  FFV1_0(w[47], w[107], w[162], pars->GC_11, amp[403]); 
  FFV1_0(w[43], w[5], w[163], pars->GC_11, amp[404]); 
  FFV1_0(w[43], w[118], w[162], pars->GC_11, amp[405]); 
  FFV1_0(w[43], w[107], w[164], pars->GC_11, amp[406]); 
  FFV1_0(w[43], w[23], w[161], pars->GC_11, amp[407]); 
  FFV1_0(w[165], w[5], w[151], pars->GC_11, amp[408]); 
  FFV1_0(w[165], w[107], w[72], pars->GC_11, amp[409]); 
  FFV1_0(w[54], w[5], w[161], pars->GC_11, amp[410]); 
  FFV1_0(w[54], w[107], w[162], pars->GC_11, amp[411]); 
  FFV1_0(w[51], w[5], w[166], pars->GC_11, amp[412]); 
  FFV1_0(w[51], w[109], w[162], pars->GC_11, amp[413]); 
  FFV1_0(w[51], w[107], w[167], pars->GC_11, amp[414]); 
  FFV1_0(w[51], w[8], w[161], pars->GC_11, amp[415]); 
  FFV2_5_0(w[165], w[168], w[3], pars->GC_51, pars->GC_58, amp[416]); 
  FFV2_5_0(w[160], w[168], w[2], pars->GC_51, pars->GC_58, amp[417]); 
  FFV2_5_0(w[169], w[8], w[3], pars->GC_51, pars->GC_58, amp[418]); 
  FFV1_0(w[160], w[8], w[153], pars->GC_11, amp[419]); 
  FFV2_5_0(w[169], w[23], w[2], pars->GC_51, pars->GC_58, amp[420]); 
  FFV1_0(w[165], w[23], w[153], pars->GC_11, amp[421]); 
  FFV2_5_0(w[165], w[170], w[3], pars->GC_51, pars->GC_58, amp[422]); 
  FFV2_5_0(w[160], w[170], w[2], pars->GC_51, pars->GC_58, amp[423]); 
  FFV2_5_0(w[171], w[109], w[3], pars->GC_51, pars->GC_58, amp[424]); 
  FFV1_0(w[160], w[109], w[75], pars->GC_11, amp[425]); 
  FFV2_5_0(w[171], w[118], w[2], pars->GC_51, pars->GC_58, amp[426]); 
  FFV1_0(w[165], w[118], w[75], pars->GC_11, amp[427]); 
  FFV1_0(w[160], w[5], w[156], pars->GC_11, amp[428]); 
  FFV1_0(w[0], w[111], w[162], pars->GC_11, amp[429]); 
  FFV1_0(w[0], w[23], w[166], pars->GC_11, amp[430]); 
  FFV1_0(w[0], w[109], w[164], pars->GC_11, amp[431]); 
  FFV1_0(w[160], w[107], w[79], pars->GC_11, amp[432]); 
  FFV1_0(w[0], w[12], w[161], pars->GC_11, amp[433]); 
  FFV1_0(w[0], w[118], w[167], pars->GC_11, amp[434]); 
  FFV1_0(w[0], w[8], w[163], pars->GC_11, amp[435]); 
  FFV1_0(w[165], w[5], w[157], pars->GC_11, amp[436]); 
  FFV1_0(w[0], w[127], w[162], pars->GC_11, amp[437]); 
  FFV1_0(w[165], w[107], w[80], pars->GC_11, amp[438]); 
  FFV1_0(w[0], w[34], w[161], pars->GC_11, amp[439]); 
  FFV1_0(w[95], w[5], w[126], pars->GC_11, amp[440]); 
  FFV1_0(w[108], w[172], w[66], pars->GC_11, amp[441]); 
  VVV1_0(w[4], w[66], w[126], pars->GC_10, amp[442]); 
  FFV1_0(w[95], w[118], w[114], pars->GC_11, amp[443]); 
  FFV1_0(w[43], w[172], w[114], pars->GC_11, amp[444]); 
  FFV1_0(w[43], w[118], w[173], pars->GC_11, amp[445]); 
  FFV1_0(w[95], w[107], w[128], pars->GC_11, amp[446]); 
  FFV1_0(w[108], w[96], w[147], pars->GC_11, amp[447]); 
  VVV1_0(w[4], w[147], w[128], pars->GC_10, amp[448]); 
  FFV1_0(w[95], w[23], w[120], pars->GC_11, amp[449]); 
  FFV1_0(w[43], w[96], w[120], pars->GC_11, amp[450]); 
  FFV1_0(w[43], w[23], w[174], pars->GC_11, amp[451]); 
  FFV1_0(w[95], w[5], w[129], pars->GC_11, amp[452]); 
  FFV1_0(w[95], w[107], w[130], pars->GC_11, amp[453]); 
  FFV1_0(w[175], w[5], w[147], pars->GC_11, amp[454]); 
  FFV1_0(w[175], w[107], w[66], pars->GC_11, amp[455]); 
  VVV1_0(w[4], w[147], w[130], pars->GC_10, amp[456]); 
  VVV1_0(w[4], w[66], w[129], pars->GC_10, amp[457]); 
  FFV2_5_0(w[95], w[132], w[3], pars->GC_51, pars->GC_58, amp[458]); 
  FFV1_0(w[47], w[5], w[174], pars->GC_11, amp[459]); 
  FFV1_0(w[47], w[132], w[4], pars->GC_11, amp[460]); 
  FFV2_5_0(w[95], w[133], w[3], pars->GC_51, pars->GC_58, amp[461]); 
  FFV1_0(w[47], w[107], w[173], pars->GC_11, amp[462]); 
  FFV1_0(w[47], w[133], w[4], pars->GC_11, amp[463]); 
  FFV1_0(w[101], w[5], w[110], pars->GC_11, amp[464]); 
  FFV1_0(w[108], w[176], w[72], pars->GC_11, amp[465]); 
  VVV1_0(w[4], w[72], w[110], pars->GC_10, amp[466]); 
  FFV1_0(w[101], w[109], w[114], pars->GC_11, amp[467]); 
  FFV1_0(w[51], w[176], w[114], pars->GC_11, amp[468]); 
  FFV1_0(w[51], w[109], w[173], pars->GC_11, amp[469]); 
  FFV1_0(w[101], w[107], w[116], pars->GC_11, amp[470]); 
  FFV1_0(w[108], w[102], w[151], pars->GC_11, amp[471]); 
  VVV1_0(w[4], w[151], w[116], pars->GC_10, amp[472]); 
  FFV1_0(w[101], w[8], w[120], pars->GC_11, amp[473]); 
  FFV1_0(w[51], w[102], w[120], pars->GC_11, amp[474]); 
  FFV1_0(w[51], w[8], w[174], pars->GC_11, amp[475]); 
  FFV1_0(w[101], w[5], w[123], pars->GC_11, amp[476]); 
  FFV1_0(w[101], w[107], w[124], pars->GC_11, amp[477]); 
  FFV1_0(w[177], w[5], w[151], pars->GC_11, amp[478]); 
  FFV1_0(w[177], w[107], w[72], pars->GC_11, amp[479]); 
  VVV1_0(w[4], w[151], w[124], pars->GC_10, amp[480]); 
  VVV1_0(w[4], w[72], w[123], pars->GC_10, amp[481]); 
  FFV2_5_0(w[101], w[132], w[2], pars->GC_51, pars->GC_58, amp[482]); 
  FFV1_0(w[54], w[5], w[174], pars->GC_11, amp[483]); 
  FFV1_0(w[54], w[132], w[4], pars->GC_11, amp[484]); 
  FFV2_5_0(w[101], w[133], w[2], pars->GC_51, pars->GC_58, amp[485]); 
  FFV1_0(w[54], w[107], w[173], pars->GC_11, amp[486]); 
  FFV1_0(w[54], w[133], w[4], pars->GC_11, amp[487]); 
  FFV1_0(w[108], w[12], w[178], pars->GC_11, amp[488]); 
  FFV2_5_0(w[154], w[102], w[3], pars->GC_51, pars->GC_58, amp[489]); 
  FFV1_0(w[154], w[12], w[4], pars->GC_11, amp[490]); 
  FFV1_0(w[113], w[8], w[178], pars->GC_11, amp[491]); 
  FFV1_0(w[113], w[102], w[153], pars->GC_11, amp[492]); 
  FFV1_0(w[175], w[8], w[153], pars->GC_11, amp[493]); 
  FFV1_0(w[125], w[5], w[178], pars->GC_11, amp[494]); 
  FFV2_5_0(w[177], w[168], w[3], pars->GC_51, pars->GC_58, amp[495]); 
  FFV1_0(w[125], w[168], w[4], pars->GC_11, amp[496]); 
  FFV1_0(w[122], w[23], w[178], pars->GC_11, amp[497]); 
  FFV1_0(w[177], w[23], w[153], pars->GC_11, amp[498]); 
  FFV1_0(w[122], w[96], w[153], pars->GC_11, amp[499]); 
  FFV1_0(w[108], w[34], w[178], pars->GC_11, amp[500]); 
  FFV2_5_0(w[154], w[96], w[2], pars->GC_51, pars->GC_58, amp[501]); 
  FFV1_0(w[154], w[34], w[4], pars->GC_11, amp[502]); 
  FFV1_0(w[131], w[5], w[178], pars->GC_11, amp[503]); 
  FFV2_5_0(w[175], w[168], w[2], pars->GC_51, pars->GC_58, amp[504]); 
  FFV1_0(w[131], w[168], w[4], pars->GC_11, amp[505]); 
  FFV1_0(w[108], w[111], w[105], pars->GC_11, amp[506]); 
  FFV2_5_0(w[143], w[176], w[3], pars->GC_51, pars->GC_58, amp[507]); 
  FFV1_0(w[143], w[111], w[4], pars->GC_11, amp[508]); 
  FFV1_0(w[113], w[109], w[105], pars->GC_11, amp[509]); 
  FFV1_0(w[113], w[176], w[75], pars->GC_11, amp[510]); 
  FFV1_0(w[175], w[109], w[75], pars->GC_11, amp[511]); 
  FFV1_0(w[125], w[107], w[105], pars->GC_11, amp[512]); 
  FFV2_5_0(w[177], w[170], w[3], pars->GC_51, pars->GC_58, amp[513]); 
  FFV1_0(w[125], w[170], w[4], pars->GC_11, amp[514]); 
  FFV1_0(w[122], w[118], w[105], pars->GC_11, amp[515]); 
  FFV1_0(w[177], w[118], w[75], pars->GC_11, amp[516]); 
  FFV1_0(w[122], w[172], w[75], pars->GC_11, amp[517]); 
  FFV1_0(w[108], w[127], w[105], pars->GC_11, amp[518]); 
  FFV2_5_0(w[143], w[172], w[2], pars->GC_51, pars->GC_58, amp[519]); 
  FFV1_0(w[143], w[127], w[4], pars->GC_11, amp[520]); 
  FFV1_0(w[131], w[107], w[105], pars->GC_11, amp[521]); 
  FFV2_5_0(w[175], w[170], w[2], pars->GC_51, pars->GC_58, amp[522]); 
  FFV1_0(w[131], w[170], w[4], pars->GC_11, amp[523]); 
  FFV1_0(w[108], w[176], w[80], pars->GC_11, amp[524]); 
  FFV1_0(w[0], w[176], w[128], pars->GC_11, amp[525]); 
  FFV1_0(w[108], w[96], w[156], pars->GC_11, amp[526]); 
  FFV1_0(w[0], w[96], w[110], pars->GC_11, amp[527]); 
  VVV1_0(w[4], w[156], w[128], pars->GC_10, amp[528]); 
  VVV1_0(w[4], w[80], w[110], pars->GC_10, amp[529]); 
  FFV1_0(w[0], w[176], w[130], pars->GC_11, amp[530]); 
  FFV1_0(w[175], w[5], w[156], pars->GC_11, amp[531]); 
  VVV1_0(w[4], w[156], w[130], pars->GC_10, amp[532]); 
  FFV2_5_0(w[146], w[176], w[3], pars->GC_51, pars->GC_58, amp[533]); 
  FFV1_0(w[0], w[111], w[173], pars->GC_11, amp[534]); 
  FFV1_0(w[146], w[111], w[4], pars->GC_11, amp[535]); 
  FFV1_0(w[108], w[102], w[157], pars->GC_11, amp[536]); 
  FFV1_0(w[0], w[102], w[126], pars->GC_11, amp[537]); 
  FFV1_0(w[108], w[172], w[79], pars->GC_11, amp[538]); 
  FFV1_0(w[0], w[172], w[116], pars->GC_11, amp[539]); 
  VVV1_0(w[4], w[79], w[126], pars->GC_10, amp[540]); 
  VVV1_0(w[4], w[157], w[116], pars->GC_10, amp[541]); 
  FFV1_0(w[0], w[102], w[129], pars->GC_11, amp[542]); 
  FFV1_0(w[175], w[107], w[79], pars->GC_11, amp[543]); 
  VVV1_0(w[4], w[79], w[129], pars->GC_10, amp[544]); 
  FFV2_5_0(w[158], w[102], w[3], pars->GC_51, pars->GC_58, amp[545]); 
  FFV1_0(w[0], w[12], w[174], pars->GC_11, amp[546]); 
  FFV1_0(w[158], w[12], w[4], pars->GC_11, amp[547]); 
  FFV1_0(w[177], w[5], w[157], pars->GC_11, amp[548]); 
  FFV1_0(w[0], w[172], w[124], pars->GC_11, amp[549]); 
  VVV1_0(w[4], w[157], w[124], pars->GC_10, amp[550]); 
  FFV1_0(w[177], w[107], w[80], pars->GC_11, amp[551]); 
  FFV1_0(w[0], w[96], w[123], pars->GC_11, amp[552]); 
  VVV1_0(w[4], w[80], w[123], pars->GC_10, amp[553]); 
  FFV2_5_0(w[146], w[172], w[2], pars->GC_51, pars->GC_58, amp[554]); 
  FFV1_0(w[0], w[127], w[173], pars->GC_11, amp[555]); 
  FFV1_0(w[146], w[127], w[4], pars->GC_11, amp[556]); 
  FFV2_5_0(w[158], w[96], w[2], pars->GC_51, pars->GC_58, amp[557]); 
  FFV1_0(w[0], w[34], w[174], pars->GC_11, amp[558]); 
  FFV1_0(w[158], w[34], w[4], pars->GC_11, amp[559]); 
  FFV1_0(w[180], w[6], w[181], pars->GC_11, amp[560]); 
  FFV1_0(w[1], w[182], w[11], pars->GC_11, amp[561]); 
  FFV1_0(w[1], w[183], w[184], pars->GC_11, amp[562]); 
  FFV1_0(w[1], w[179], w[185], pars->GC_11, amp[563]); 
  FFV1_0(w[186], w[6], w[184], pars->GC_11, amp[564]); 
  FFV1_0(w[186], w[179], w[11], pars->GC_11, amp[565]); 
  FFV1_0(w[180], w[179], w[17], pars->GC_11, amp[566]); 
  FFV2_3_0(w[18], w[179], w[3], pars->GC_50, pars->GC_58, amp[567]); 
  FFV1_0(w[180], w[5], w[188], pars->GC_11, amp[568]); 
  FFV1_0(w[1], w[189], w[21], pars->GC_11, amp[569]); 
  FFV1_0(w[1], w[190], w[191], pars->GC_11, amp[570]); 
  FFV1_0(w[1], w[187], w[192], pars->GC_11, amp[571]); 
  FFV1_0(w[186], w[5], w[191], pars->GC_11, amp[572]); 
  FFV1_0(w[186], w[187], w[21], pars->GC_11, amp[573]); 
  FFV1_0(w[180], w[187], w[26], pars->GC_11, amp[574]); 
  FFV2_3_0(w[27], w[187], w[3], pars->GC_50, pars->GC_58, amp[575]); 
  FFV1_0(w[180], w[6], w[194], pars->GC_11, amp[576]); 
  FFV1_0(w[180], w[5], w[195], pars->GC_11, amp[577]); 
  FFV1_0(w[196], w[6], w[21], pars->GC_11, amp[578]); 
  FFV1_0(w[196], w[5], w[11], pars->GC_11, amp[579]); 
  FFV1_0(w[193], w[6], w[192], pars->GC_11, amp[580]); 
  FFV1_0(w[193], w[190], w[11], pars->GC_11, amp[581]); 
  FFV1_0(w[193], w[5], w[185], pars->GC_11, amp[582]); 
  FFV1_0(w[193], w[183], w[21], pars->GC_11, amp[583]); 
  FFV1_0(w[197], w[6], w[198], pars->GC_11, amp[584]); 
  FFV1_0(w[1], w[199], w[11], pars->GC_11, amp[585]); 
  FFV1_0(w[197], w[190], w[17], pars->GC_11, amp[586]); 
  FFV2_3_0(w[18], w[190], w[2], pars->GC_50, pars->GC_58, amp[587]); 
  FFV1_0(w[197], w[5], w[200], pars->GC_11, amp[588]); 
  FFV1_0(w[1], w[201], w[21], pars->GC_11, amp[589]); 
  FFV1_0(w[197], w[183], w[26], pars->GC_11, amp[590]); 
  FFV2_3_0(w[27], w[183], w[2], pars->GC_50, pars->GC_58, amp[591]); 
  FFV1_0(w[197], w[6], w[202], pars->GC_11, amp[592]); 
  FFV1_0(w[197], w[5], w[203], pars->GC_11, amp[593]); 
  FFV1_0(w[204], w[6], w[21], pars->GC_11, amp[594]); 
  FFV1_0(w[204], w[5], w[11], pars->GC_11, amp[595]); 
  FFV2_3_0(w[197], w[40], w[3], pars->GC_50, pars->GC_58, amp[596]); 
  FFV2_3_0(w[180], w[40], w[2], pars->GC_50, pars->GC_58, amp[597]); 
  FFV2_3_0(w[197], w[41], w[3], pars->GC_50, pars->GC_58, amp[598]); 
  FFV2_3_0(w[180], w[41], w[2], pars->GC_50, pars->GC_58, amp[599]); 
  FFV1_0(w[1], w[206], w[207], pars->GC_11, amp[600]); 
  FFV1_0(w[208], w[6], w[46], pars->GC_11, amp[601]); 
  FFV1_0(w[1], w[183], w[209], pars->GC_11, amp[602]); 
  FFV1_0(w[205], w[183], w[46], pars->GC_11, amp[603]); 
  FFV1_0(w[186], w[6], w[209], pars->GC_11, amp[604]); 
  FFV1_0(w[205], w[6], w[210], pars->GC_11, amp[605]); 
  FFV1_0(w[205], w[206], w[17], pars->GC_11, amp[606]); 
  FFV2_3_0(w[205], w[50], w[3], pars->GC_50, pars->GC_58, amp[607]); 
  FFV1_0(w[1], w[212], w[213], pars->GC_11, amp[608]); 
  FFV1_0(w[214], w[6], w[46], pars->GC_11, amp[609]); 
  FFV1_0(w[1], w[187], w[215], pars->GC_11, amp[610]); 
  FFV1_0(w[211], w[187], w[46], pars->GC_11, amp[611]); 
  FFV1_0(w[193], w[6], w[215], pars->GC_11, amp[612]); 
  FFV1_0(w[211], w[6], w[216], pars->GC_11, amp[613]); 
  FFV1_0(w[211], w[212], w[17], pars->GC_11, amp[614]); 
  FFV2_3_0(w[211], w[50], w[2], pars->GC_50, pars->GC_58, amp[615]); 
  FFV2_3_0(w[58], w[212], w[3], pars->GC_50, pars->GC_58, amp[616]); 
  FFV2_3_0(w[58], w[206], w[2], pars->GC_50, pars->GC_58, amp[617]); 
  FFV2_3_0(w[193], w[59], w[3], pars->GC_50, pars->GC_58, amp[618]); 
  FFV1_0(w[193], w[206], w[57], pars->GC_11, amp[619]); 
  FFV2_3_0(w[186], w[59], w[2], pars->GC_50, pars->GC_58, amp[620]); 
  FFV1_0(w[186], w[212], w[57], pars->GC_11, amp[621]); 
  FFV1_0(w[1], w[189], w[60], pars->GC_11, amp[622]); 
  FFV1_0(w[1], w[206], w[217], pars->GC_11, amp[623]); 
  FFV1_0(w[0], w[206], w[188], pars->GC_11, amp[624]); 
  FFV1_0(w[0], w[189], w[46], pars->GC_11, amp[625]); 
  FFV1_0(w[186], w[187], w[60], pars->GC_11, amp[626]); 
  FFV1_0(w[0], w[187], w[210], pars->GC_11, amp[627]); 
  FFV1_0(w[196], w[6], w[60], pars->GC_11, amp[628]); 
  FFV1_0(w[0], w[206], w[195], pars->GC_11, amp[629]); 
  FFV1_0(w[193], w[183], w[60], pars->GC_11, amp[630]); 
  FFV1_0(w[0], w[183], w[216], pars->GC_11, amp[631]); 
  FFV1_0(w[1], w[201], w[60], pars->GC_11, amp[632]); 
  FFV1_0(w[1], w[212], w[218], pars->GC_11, amp[633]); 
  FFV1_0(w[0], w[212], w[200], pars->GC_11, amp[634]); 
  FFV1_0(w[0], w[201], w[46], pars->GC_11, amp[635]); 
  FFV1_0(w[204], w[6], w[60], pars->GC_11, amp[636]); 
  FFV1_0(w[0], w[212], w[203], pars->GC_11, amp[637]); 
  FFV2_3_0(w[63], w[212], w[3], pars->GC_50, pars->GC_58, amp[638]); 
  FFV2_3_0(w[63], w[206], w[2], pars->GC_50, pars->GC_58, amp[639]); 
  FFV1_0(w[1], w[219], w[220], pars->GC_11, amp[640]); 
  FFV1_0(w[208], w[5], w[67], pars->GC_11, amp[641]); 
  FFV1_0(w[1], w[190], w[221], pars->GC_11, amp[642]); 
  FFV1_0(w[205], w[190], w[67], pars->GC_11, amp[643]); 
  FFV1_0(w[186], w[5], w[221], pars->GC_11, amp[644]); 
  FFV1_0(w[205], w[5], w[222], pars->GC_11, amp[645]); 
  FFV1_0(w[205], w[219], w[26], pars->GC_11, amp[646]); 
  FFV2_3_0(w[205], w[70], w[3], pars->GC_50, pars->GC_58, amp[647]); 
  FFV1_0(w[1], w[223], w[224], pars->GC_11, amp[648]); 
  FFV1_0(w[214], w[5], w[67], pars->GC_11, amp[649]); 
  FFV1_0(w[1], w[179], w[225], pars->GC_11, amp[650]); 
  FFV1_0(w[211], w[179], w[67], pars->GC_11, amp[651]); 
  FFV1_0(w[193], w[5], w[225], pars->GC_11, amp[652]); 
  FFV1_0(w[211], w[5], w[226], pars->GC_11, amp[653]); 
  FFV1_0(w[211], w[223], w[26], pars->GC_11, amp[654]); 
  FFV2_3_0(w[211], w[70], w[2], pars->GC_50, pars->GC_58, amp[655]); 
  FFV2_3_0(w[76], w[223], w[3], pars->GC_50, pars->GC_58, amp[656]); 
  FFV2_3_0(w[76], w[219], w[2], pars->GC_50, pars->GC_58, amp[657]); 
  FFV2_3_0(w[193], w[77], w[3], pars->GC_50, pars->GC_58, amp[658]); 
  FFV1_0(w[193], w[219], w[75], pars->GC_11, amp[659]); 
  FFV2_3_0(w[186], w[77], w[2], pars->GC_50, pars->GC_58, amp[660]); 
  FFV1_0(w[186], w[223], w[75], pars->GC_11, amp[661]); 
  FFV1_0(w[1], w[182], w[78], pars->GC_11, amp[662]); 
  FFV1_0(w[1], w[219], w[227], pars->GC_11, amp[663]); 
  FFV1_0(w[0], w[219], w[181], pars->GC_11, amp[664]); 
  FFV1_0(w[0], w[182], w[67], pars->GC_11, amp[665]); 
  FFV1_0(w[186], w[179], w[78], pars->GC_11, amp[666]); 
  FFV1_0(w[0], w[179], w[222], pars->GC_11, amp[667]); 
  FFV1_0(w[196], w[5], w[78], pars->GC_11, amp[668]); 
  FFV1_0(w[0], w[219], w[194], pars->GC_11, amp[669]); 
  FFV1_0(w[193], w[190], w[78], pars->GC_11, amp[670]); 
  FFV1_0(w[0], w[190], w[226], pars->GC_11, amp[671]); 
  FFV1_0(w[1], w[199], w[78], pars->GC_11, amp[672]); 
  FFV1_0(w[1], w[223], w[228], pars->GC_11, amp[673]); 
  FFV1_0(w[0], w[223], w[198], pars->GC_11, amp[674]); 
  FFV1_0(w[0], w[199], w[67], pars->GC_11, amp[675]); 
  FFV1_0(w[204], w[5], w[78], pars->GC_11, amp[676]); 
  FFV1_0(w[0], w[223], w[202], pars->GC_11, amp[677]); 
  FFV2_3_0(w[81], w[223], w[3], pars->GC_50, pars->GC_58, amp[678]); 
  FFV2_3_0(w[81], w[219], w[2], pars->GC_50, pars->GC_58, amp[679]); 
  FFV1_0(w[229], w[6], w[220], pars->GC_11, amp[680]); 
  FFV1_0(w[229], w[5], w[207], pars->GC_11, amp[681]); 
  FFV1_0(w[208], w[6], w[84], pars->GC_11, amp[682]); 
  FFV1_0(w[208], w[5], w[85], pars->GC_11, amp[683]); 
  FFV1_0(w[205], w[6], w[230], pars->GC_11, amp[684]); 
  FFV1_0(w[205], w[190], w[85], pars->GC_11, amp[685]); 
  FFV1_0(w[205], w[5], w[231], pars->GC_11, amp[686]); 
  FFV1_0(w[205], w[183], w[84], pars->GC_11, amp[687]); 
  FFV1_0(w[232], w[6], w[224], pars->GC_11, amp[688]); 
  FFV1_0(w[232], w[5], w[213], pars->GC_11, amp[689]); 
  FFV1_0(w[214], w[6], w[84], pars->GC_11, amp[690]); 
  FFV1_0(w[214], w[5], w[85], pars->GC_11, amp[691]); 
  FFV1_0(w[211], w[6], w[233], pars->GC_11, amp[692]); 
  FFV1_0(w[211], w[179], w[85], pars->GC_11, amp[693]); 
  FFV1_0(w[211], w[5], w[234], pars->GC_11, amp[694]); 
  FFV1_0(w[211], w[187], w[84], pars->GC_11, amp[695]); 
  FFV2_3_0(w[232], w[91], w[3], pars->GC_50, pars->GC_58, amp[696]); 
  FFV2_3_0(w[229], w[91], w[2], pars->GC_50, pars->GC_58, amp[697]); 
  FFV2_3_0(w[92], w[187], w[3], pars->GC_50, pars->GC_58, amp[698]); 
  FFV1_0(w[229], w[187], w[75], pars->GC_11, amp[699]); 
  FFV2_3_0(w[92], w[183], w[2], pars->GC_50, pars->GC_58, amp[700]); 
  FFV1_0(w[232], w[183], w[75], pars->GC_11, amp[701]); 
  FFV2_3_0(w[232], w[93], w[3], pars->GC_50, pars->GC_58, amp[702]); 
  FFV2_3_0(w[229], w[93], w[2], pars->GC_50, pars->GC_58, amp[703]); 
  FFV2_3_0(w[94], w[179], w[3], pars->GC_50, pars->GC_58, amp[704]); 
  FFV1_0(w[229], w[179], w[57], pars->GC_11, amp[705]); 
  FFV2_3_0(w[94], w[190], w[2], pars->GC_50, pars->GC_58, amp[706]); 
  FFV1_0(w[232], w[190], w[57], pars->GC_11, amp[707]); 
  FFV1_0(w[229], w[6], w[227], pars->GC_11, amp[708]); 
  FFV1_0(w[0], w[182], w[85], pars->GC_11, amp[709]); 
  FFV1_0(w[0], w[183], w[233], pars->GC_11, amp[710]); 
  FFV1_0(w[0], w[179], w[231], pars->GC_11, amp[711]); 
  FFV1_0(w[229], w[5], w[217], pars->GC_11, amp[712]); 
  FFV1_0(w[0], w[189], w[84], pars->GC_11, amp[713]); 
  FFV1_0(w[0], w[190], w[234], pars->GC_11, amp[714]); 
  FFV1_0(w[0], w[187], w[230], pars->GC_11, amp[715]); 
  FFV1_0(w[232], w[6], w[228], pars->GC_11, amp[716]); 
  FFV1_0(w[0], w[199], w[85], pars->GC_11, amp[717]); 
  FFV1_0(w[232], w[5], w[218], pars->GC_11, amp[718]); 
  FFV1_0(w[0], w[201], w[84], pars->GC_11, amp[719]); 
  FFV1_0(w[235], w[6], w[198], pars->GC_11, amp[720]); 
  FFV1_0(w[1], w[236], w[207], pars->GC_11, amp[721]); 
  VVV1_0(w[4], w[207], w[198], pars->GC_10, amp[722]); 
  FFV1_0(w[235], w[190], w[17], pars->GC_11, amp[723]); 
  FFV1_0(w[205], w[236], w[17], pars->GC_11, amp[724]); 
  FFV1_0(w[205], w[190], w[97], pars->GC_11, amp[725]); 
  FFV1_0(w[235], w[5], w[200], pars->GC_11, amp[726]); 
  FFV1_0(w[1], w[237], w[220], pars->GC_11, amp[727]); 
  VVV1_0(w[4], w[220], w[200], pars->GC_10, amp[728]); 
  FFV1_0(w[235], w[183], w[26], pars->GC_11, amp[729]); 
  FFV1_0(w[205], w[237], w[26], pars->GC_11, amp[730]); 
  FFV1_0(w[205], w[183], w[99], pars->GC_11, amp[731]); 
  FFV1_0(w[235], w[6], w[202], pars->GC_11, amp[732]); 
  FFV1_0(w[235], w[5], w[203], pars->GC_11, amp[733]); 
  FFV1_0(w[238], w[6], w[220], pars->GC_11, amp[734]); 
  FFV1_0(w[238], w[5], w[207], pars->GC_11, amp[735]); 
  VVV1_0(w[4], w[220], w[203], pars->GC_10, amp[736]); 
  VVV1_0(w[4], w[207], w[202], pars->GC_10, amp[737]); 
  FFV2_3_0(w[235], w[40], w[3], pars->GC_50, pars->GC_58, amp[738]); 
  FFV1_0(w[208], w[6], w[99], pars->GC_11, amp[739]); 
  FFV1_0(w[208], w[40], w[4], pars->GC_11, amp[740]); 
  FFV2_3_0(w[235], w[41], w[3], pars->GC_50, pars->GC_58, amp[741]); 
  FFV1_0(w[208], w[5], w[97], pars->GC_11, amp[742]); 
  FFV1_0(w[208], w[41], w[4], pars->GC_11, amp[743]); 
  FFV1_0(w[239], w[6], w[181], pars->GC_11, amp[744]); 
  FFV1_0(w[1], w[240], w[213], pars->GC_11, amp[745]); 
  VVV1_0(w[4], w[213], w[181], pars->GC_10, amp[746]); 
  FFV1_0(w[239], w[179], w[17], pars->GC_11, amp[747]); 
  FFV1_0(w[211], w[240], w[17], pars->GC_11, amp[748]); 
  FFV1_0(w[211], w[179], w[97], pars->GC_11, amp[749]); 
  FFV1_0(w[239], w[5], w[188], pars->GC_11, amp[750]); 
  FFV1_0(w[1], w[241], w[224], pars->GC_11, amp[751]); 
  VVV1_0(w[4], w[224], w[188], pars->GC_10, amp[752]); 
  FFV1_0(w[239], w[187], w[26], pars->GC_11, amp[753]); 
  FFV1_0(w[211], w[241], w[26], pars->GC_11, amp[754]); 
  FFV1_0(w[211], w[187], w[99], pars->GC_11, amp[755]); 
  FFV1_0(w[239], w[6], w[194], pars->GC_11, amp[756]); 
  FFV1_0(w[239], w[5], w[195], pars->GC_11, amp[757]); 
  FFV1_0(w[242], w[6], w[224], pars->GC_11, amp[758]); 
  FFV1_0(w[242], w[5], w[213], pars->GC_11, amp[759]); 
  VVV1_0(w[4], w[224], w[195], pars->GC_10, amp[760]); 
  VVV1_0(w[4], w[213], w[194], pars->GC_10, amp[761]); 
  FFV2_3_0(w[239], w[40], w[2], pars->GC_50, pars->GC_58, amp[762]); 
  FFV1_0(w[214], w[6], w[99], pars->GC_11, amp[763]); 
  FFV1_0(w[214], w[40], w[4], pars->GC_11, amp[764]); 
  FFV2_3_0(w[239], w[41], w[2], pars->GC_50, pars->GC_58, amp[765]); 
  FFV1_0(w[214], w[5], w[97], pars->GC_11, amp[766]); 
  FFV1_0(w[214], w[41], w[4], pars->GC_11, amp[767]); 
  FFV1_0(w[1], w[189], w[105], pars->GC_11, amp[768]); 
  FFV2_3_0(w[76], w[241], w[3], pars->GC_50, pars->GC_58, amp[769]); 
  FFV1_0(w[76], w[189], w[4], pars->GC_11, amp[770]); 
  FFV1_0(w[186], w[187], w[105], pars->GC_11, amp[771]); 
  FFV1_0(w[186], w[241], w[75], pars->GC_11, amp[772]); 
  FFV1_0(w[238], w[187], w[75], pars->GC_11, amp[773]); 
  FFV1_0(w[196], w[6], w[105], pars->GC_11, amp[774]); 
  FFV2_3_0(w[242], w[91], w[3], pars->GC_50, pars->GC_58, amp[775]); 
  FFV1_0(w[196], w[91], w[4], pars->GC_11, amp[776]); 
  FFV1_0(w[193], w[183], w[105], pars->GC_11, amp[777]); 
  FFV1_0(w[242], w[183], w[75], pars->GC_11, amp[778]); 
  FFV1_0(w[193], w[237], w[75], pars->GC_11, amp[779]); 
  FFV1_0(w[1], w[201], w[105], pars->GC_11, amp[780]); 
  FFV2_3_0(w[76], w[237], w[2], pars->GC_50, pars->GC_58, amp[781]); 
  FFV1_0(w[76], w[201], w[4], pars->GC_11, amp[782]); 
  FFV1_0(w[204], w[6], w[105], pars->GC_11, amp[783]); 
  FFV2_3_0(w[238], w[91], w[2], pars->GC_50, pars->GC_58, amp[784]); 
  FFV1_0(w[204], w[91], w[4], pars->GC_11, amp[785]); 
  FFV1_0(w[1], w[182], w[106], pars->GC_11, amp[786]); 
  FFV2_3_0(w[58], w[240], w[3], pars->GC_50, pars->GC_58, amp[787]); 
  FFV1_0(w[58], w[182], w[4], pars->GC_11, amp[788]); 
  FFV1_0(w[186], w[179], w[106], pars->GC_11, amp[789]); 
  FFV1_0(w[186], w[240], w[57], pars->GC_11, amp[790]); 
  FFV1_0(w[238], w[179], w[57], pars->GC_11, amp[791]); 
  FFV1_0(w[196], w[5], w[106], pars->GC_11, amp[792]); 
  FFV2_3_0(w[242], w[93], w[3], pars->GC_50, pars->GC_58, amp[793]); 
  FFV1_0(w[196], w[93], w[4], pars->GC_11, amp[794]); 
  FFV1_0(w[193], w[190], w[106], pars->GC_11, amp[795]); 
  FFV1_0(w[242], w[190], w[57], pars->GC_11, amp[796]); 
  FFV1_0(w[193], w[236], w[57], pars->GC_11, amp[797]); 
  FFV1_0(w[1], w[199], w[106], pars->GC_11, amp[798]); 
  FFV2_3_0(w[58], w[236], w[2], pars->GC_50, pars->GC_58, amp[799]); 
  FFV1_0(w[58], w[199], w[4], pars->GC_11, amp[800]); 
  FFV1_0(w[204], w[5], w[106], pars->GC_11, amp[801]); 
  FFV2_3_0(w[238], w[93], w[2], pars->GC_50, pars->GC_58, amp[802]); 
  FFV1_0(w[204], w[93], w[4], pars->GC_11, amp[803]); 
  FFV1_0(w[1], w[240], w[218], pars->GC_11, amp[804]); 
  FFV1_0(w[0], w[240], w[200], pars->GC_11, amp[805]); 
  FFV1_0(w[1], w[237], w[227], pars->GC_11, amp[806]); 
  FFV1_0(w[0], w[237], w[181], pars->GC_11, amp[807]); 
  VVV1_0(w[4], w[227], w[200], pars->GC_10, amp[808]); 
  VVV1_0(w[4], w[218], w[181], pars->GC_10, amp[809]); 
  FFV1_0(w[0], w[240], w[203], pars->GC_11, amp[810]); 
  FFV1_0(w[238], w[6], w[227], pars->GC_11, amp[811]); 
  VVV1_0(w[4], w[227], w[203], pars->GC_10, amp[812]); 
  FFV2_3_0(w[63], w[240], w[3], pars->GC_50, pars->GC_58, amp[813]); 
  FFV1_0(w[0], w[182], w[97], pars->GC_11, amp[814]); 
  FFV1_0(w[63], w[182], w[4], pars->GC_11, amp[815]); 
  FFV1_0(w[1], w[241], w[228], pars->GC_11, amp[816]); 
  FFV1_0(w[0], w[241], w[198], pars->GC_11, amp[817]); 
  FFV1_0(w[1], w[236], w[217], pars->GC_11, amp[818]); 
  FFV1_0(w[0], w[236], w[188], pars->GC_11, amp[819]); 
  VVV1_0(w[4], w[217], w[198], pars->GC_10, amp[820]); 
  VVV1_0(w[4], w[228], w[188], pars->GC_10, amp[821]); 
  FFV1_0(w[0], w[241], w[202], pars->GC_11, amp[822]); 
  FFV1_0(w[238], w[5], w[217], pars->GC_11, amp[823]); 
  VVV1_0(w[4], w[217], w[202], pars->GC_10, amp[824]); 
  FFV2_3_0(w[81], w[241], w[3], pars->GC_50, pars->GC_58, amp[825]); 
  FFV1_0(w[0], w[189], w[99], pars->GC_11, amp[826]); 
  FFV1_0(w[81], w[189], w[4], pars->GC_11, amp[827]); 
  FFV1_0(w[242], w[6], w[228], pars->GC_11, amp[828]); 
  FFV1_0(w[0], w[236], w[195], pars->GC_11, amp[829]); 
  VVV1_0(w[4], w[228], w[195], pars->GC_10, amp[830]); 
  FFV1_0(w[242], w[5], w[218], pars->GC_11, amp[831]); 
  FFV1_0(w[0], w[237], w[194], pars->GC_11, amp[832]); 
  VVV1_0(w[4], w[218], w[194], pars->GC_10, amp[833]); 
  FFV2_3_0(w[63], w[236], w[2], pars->GC_50, pars->GC_58, amp[834]); 
  FFV1_0(w[0], w[199], w[97], pars->GC_11, amp[835]); 
  FFV1_0(w[63], w[199], w[4], pars->GC_11, amp[836]); 
  FFV2_3_0(w[81], w[237], w[2], pars->GC_50, pars->GC_58, amp[837]); 
  FFV1_0(w[0], w[201], w[99], pars->GC_11, amp[838]); 
  FFV1_0(w[81], w[201], w[4], pars->GC_11, amp[839]); 
  FFV1_0(w[180], w[5], w[244], pars->GC_11, amp[840]); 
  FFV1_0(w[108], w[245], w[21], pars->GC_11, amp[841]); 
  FFV1_0(w[108], w[190], w[246], pars->GC_11, amp[842]); 
  FFV1_0(w[108], w[243], w[192], pars->GC_11, amp[843]); 
  FFV1_0(w[247], w[5], w[246], pars->GC_11, amp[844]); 
  FFV1_0(w[247], w[243], w[21], pars->GC_11, amp[845]); 
  FFV1_0(w[180], w[243], w[114], pars->GC_11, amp[846]); 
  FFV2_3_0(w[115], w[243], w[3], pars->GC_50, pars->GC_58, amp[847]); 
  FFV1_0(w[180], w[107], w[248], pars->GC_11, amp[848]); 
  FFV1_0(w[108], w[182], w[117], pars->GC_11, amp[849]); 
  FFV1_0(w[108], w[249], w[184], pars->GC_11, amp[850]); 
  FFV1_0(w[108], w[179], w[250], pars->GC_11, amp[851]); 
  FFV1_0(w[247], w[107], w[184], pars->GC_11, amp[852]); 
  FFV1_0(w[247], w[179], w[117], pars->GC_11, amp[853]); 
  FFV1_0(w[180], w[179], w[120], pars->GC_11, amp[854]); 
  FFV2_3_0(w[121], w[179], w[3], pars->GC_50, pars->GC_58, amp[855]); 
  FFV1_0(w[180], w[5], w[252], pars->GC_11, amp[856]); 
  FFV1_0(w[180], w[107], w[253], pars->GC_11, amp[857]); 
  FFV1_0(w[254], w[5], w[117], pars->GC_11, amp[858]); 
  FFV1_0(w[254], w[107], w[21], pars->GC_11, amp[859]); 
  FFV1_0(w[251], w[5], w[250], pars->GC_11, amp[860]); 
  FFV1_0(w[251], w[249], w[21], pars->GC_11, amp[861]); 
  FFV1_0(w[251], w[107], w[192], pars->GC_11, amp[862]); 
  FFV1_0(w[251], w[190], w[117], pars->GC_11, amp[863]); 
  FFV1_0(w[197], w[5], w[255], pars->GC_11, amp[864]); 
  FFV1_0(w[108], w[256], w[21], pars->GC_11, amp[865]); 
  FFV1_0(w[197], w[249], w[114], pars->GC_11, amp[866]); 
  FFV2_3_0(w[115], w[249], w[2], pars->GC_50, pars->GC_58, amp[867]); 
  FFV1_0(w[197], w[107], w[257], pars->GC_11, amp[868]); 
  FFV1_0(w[108], w[199], w[117], pars->GC_11, amp[869]); 
  FFV1_0(w[197], w[190], w[120], pars->GC_11, amp[870]); 
  FFV2_3_0(w[121], w[190], w[2], pars->GC_50, pars->GC_58, amp[871]); 
  FFV1_0(w[197], w[5], w[258], pars->GC_11, amp[872]); 
  FFV1_0(w[197], w[107], w[259], pars->GC_11, amp[873]); 
  FFV1_0(w[260], w[5], w[117], pars->GC_11, amp[874]); 
  FFV1_0(w[260], w[107], w[21], pars->GC_11, amp[875]); 
  FFV2_3_0(w[197], w[132], w[3], pars->GC_50, pars->GC_58, amp[876]); 
  FFV2_3_0(w[180], w[132], w[2], pars->GC_50, pars->GC_58, amp[877]); 
  FFV2_3_0(w[197], w[133], w[3], pars->GC_50, pars->GC_58, amp[878]); 
  FFV2_3_0(w[180], w[133], w[2], pars->GC_50, pars->GC_58, amp[879]); 
  FFV1_0(w[108], w[261], w[220], pars->GC_11, amp[880]); 
  FFV1_0(w[208], w[5], w[136], pars->GC_11, amp[881]); 
  FFV1_0(w[108], w[190], w[262], pars->GC_11, amp[882]); 
  FFV1_0(w[205], w[190], w[136], pars->GC_11, amp[883]); 
  FFV1_0(w[247], w[5], w[262], pars->GC_11, amp[884]); 
  FFV1_0(w[205], w[5], w[263], pars->GC_11, amp[885]); 
  FFV1_0(w[205], w[261], w[114], pars->GC_11, amp[886]); 
  FFV2_3_0(w[205], w[139], w[3], pars->GC_50, pars->GC_58, amp[887]); 
  FFV1_0(w[108], w[264], w[224], pars->GC_11, amp[888]); 
  FFV1_0(w[214], w[5], w[136], pars->GC_11, amp[889]); 
  FFV1_0(w[108], w[179], w[265], pars->GC_11, amp[890]); 
  FFV1_0(w[211], w[179], w[136], pars->GC_11, amp[891]); 
  FFV1_0(w[251], w[5], w[265], pars->GC_11, amp[892]); 
  FFV1_0(w[211], w[5], w[266], pars->GC_11, amp[893]); 
  FFV1_0(w[211], w[264], w[114], pars->GC_11, amp[894]); 
  FFV2_3_0(w[211], w[139], w[2], pars->GC_50, pars->GC_58, amp[895]); 
  FFV2_3_0(w[143], w[264], w[3], pars->GC_50, pars->GC_58, amp[896]); 
  FFV2_3_0(w[143], w[261], w[2], pars->GC_50, pars->GC_58, amp[897]); 
  FFV2_3_0(w[251], w[144], w[3], pars->GC_50, pars->GC_58, amp[898]); 
  FFV1_0(w[251], w[261], w[75], pars->GC_11, amp[899]); 
  FFV2_3_0(w[247], w[144], w[2], pars->GC_50, pars->GC_58, amp[900]); 
  FFV1_0(w[247], w[264], w[75], pars->GC_11, amp[901]); 
  FFV1_0(w[108], w[182], w[145], pars->GC_11, amp[902]); 
  FFV1_0(w[108], w[261], w[227], pars->GC_11, amp[903]); 
  FFV1_0(w[0], w[261], w[248], pars->GC_11, amp[904]); 
  FFV1_0(w[0], w[182], w[136], pars->GC_11, amp[905]); 
  FFV1_0(w[247], w[179], w[145], pars->GC_11, amp[906]); 
  FFV1_0(w[0], w[179], w[263], pars->GC_11, amp[907]); 
  FFV1_0(w[254], w[5], w[145], pars->GC_11, amp[908]); 
  FFV1_0(w[0], w[261], w[253], pars->GC_11, amp[909]); 
  FFV1_0(w[251], w[190], w[145], pars->GC_11, amp[910]); 
  FFV1_0(w[0], w[190], w[266], pars->GC_11, amp[911]); 
  FFV1_0(w[108], w[199], w[145], pars->GC_11, amp[912]); 
  FFV1_0(w[108], w[264], w[228], pars->GC_11, amp[913]); 
  FFV1_0(w[0], w[264], w[257], pars->GC_11, amp[914]); 
  FFV1_0(w[0], w[199], w[136], pars->GC_11, amp[915]); 
  FFV1_0(w[260], w[5], w[145], pars->GC_11, amp[916]); 
  FFV1_0(w[0], w[264], w[259], pars->GC_11, amp[917]); 
  FFV2_3_0(w[146], w[264], w[3], pars->GC_50, pars->GC_58, amp[918]); 
  FFV2_3_0(w[146], w[261], w[2], pars->GC_50, pars->GC_58, amp[919]); 
  FFV1_0(w[108], w[206], w[267], pars->GC_11, amp[920]); 
  FFV1_0(w[208], w[107], w[148], pars->GC_11, amp[921]); 
  FFV1_0(w[108], w[249], w[209], pars->GC_11, amp[922]); 
  FFV1_0(w[205], w[249], w[148], pars->GC_11, amp[923]); 
  FFV1_0(w[247], w[107], w[209], pars->GC_11, amp[924]); 
  FFV1_0(w[205], w[107], w[268], pars->GC_11, amp[925]); 
  FFV1_0(w[205], w[206], w[120], pars->GC_11, amp[926]); 
  FFV2_3_0(w[205], w[150], w[3], pars->GC_50, pars->GC_58, amp[927]); 
  FFV1_0(w[108], w[212], w[269], pars->GC_11, amp[928]); 
  FFV1_0(w[214], w[107], w[148], pars->GC_11, amp[929]); 
  FFV1_0(w[108], w[243], w[215], pars->GC_11, amp[930]); 
  FFV1_0(w[211], w[243], w[148], pars->GC_11, amp[931]); 
  FFV1_0(w[251], w[107], w[215], pars->GC_11, amp[932]); 
  FFV1_0(w[211], w[107], w[270], pars->GC_11, amp[933]); 
  FFV1_0(w[211], w[212], w[120], pars->GC_11, amp[934]); 
  FFV2_3_0(w[211], w[150], w[2], pars->GC_50, pars->GC_58, amp[935]); 
  FFV2_3_0(w[154], w[212], w[3], pars->GC_50, pars->GC_58, amp[936]); 
  FFV2_3_0(w[154], w[206], w[2], pars->GC_50, pars->GC_58, amp[937]); 
  FFV2_3_0(w[251], w[155], w[3], pars->GC_50, pars->GC_58, amp[938]); 
  FFV1_0(w[251], w[206], w[153], pars->GC_11, amp[939]); 
  FFV2_3_0(w[247], w[155], w[2], pars->GC_50, pars->GC_58, amp[940]); 
  FFV1_0(w[247], w[212], w[153], pars->GC_11, amp[941]); 
  FFV1_0(w[108], w[245], w[60], pars->GC_11, amp[942]); 
  FFV1_0(w[108], w[206], w[271], pars->GC_11, amp[943]); 
  FFV1_0(w[0], w[206], w[244], pars->GC_11, amp[944]); 
  FFV1_0(w[0], w[245], w[148], pars->GC_11, amp[945]); 
  FFV1_0(w[247], w[243], w[60], pars->GC_11, amp[946]); 
  FFV1_0(w[0], w[243], w[268], pars->GC_11, amp[947]); 
  FFV1_0(w[254], w[107], w[60], pars->GC_11, amp[948]); 
  FFV1_0(w[0], w[206], w[252], pars->GC_11, amp[949]); 
  FFV1_0(w[251], w[249], w[60], pars->GC_11, amp[950]); 
  FFV1_0(w[0], w[249], w[270], pars->GC_11, amp[951]); 
  FFV1_0(w[108], w[256], w[60], pars->GC_11, amp[952]); 
  FFV1_0(w[108], w[212], w[272], pars->GC_11, amp[953]); 
  FFV1_0(w[0], w[212], w[255], pars->GC_11, amp[954]); 
  FFV1_0(w[0], w[256], w[148], pars->GC_11, amp[955]); 
  FFV1_0(w[260], w[107], w[60], pars->GC_11, amp[956]); 
  FFV1_0(w[0], w[212], w[258], pars->GC_11, amp[957]); 
  FFV2_3_0(w[158], w[212], w[3], pars->GC_50, pars->GC_58, amp[958]); 
  FFV2_3_0(w[158], w[206], w[2], pars->GC_50, pars->GC_58, amp[959]); 
  FFV1_0(w[273], w[5], w[267], pars->GC_11, amp[960]); 
  FFV1_0(w[273], w[107], w[220], pars->GC_11, amp[961]); 
  FFV1_0(w[208], w[5], w[161], pars->GC_11, amp[962]); 
  FFV1_0(w[208], w[107], w[162], pars->GC_11, amp[963]); 
  FFV1_0(w[205], w[5], w[274], pars->GC_11, amp[964]); 
  FFV1_0(w[205], w[249], w[162], pars->GC_11, amp[965]); 
  FFV1_0(w[205], w[107], w[275], pars->GC_11, amp[966]); 
  FFV1_0(w[205], w[190], w[161], pars->GC_11, amp[967]); 
  FFV1_0(w[276], w[5], w[269], pars->GC_11, amp[968]); 
  FFV1_0(w[276], w[107], w[224], pars->GC_11, amp[969]); 
  FFV1_0(w[214], w[5], w[161], pars->GC_11, amp[970]); 
  FFV1_0(w[214], w[107], w[162], pars->GC_11, amp[971]); 
  FFV1_0(w[211], w[5], w[277], pars->GC_11, amp[972]); 
  FFV1_0(w[211], w[243], w[162], pars->GC_11, amp[973]); 
  FFV1_0(w[211], w[107], w[278], pars->GC_11, amp[974]); 
  FFV1_0(w[211], w[179], w[161], pars->GC_11, amp[975]); 
  FFV2_3_0(w[276], w[168], w[3], pars->GC_50, pars->GC_58, amp[976]); 
  FFV2_3_0(w[273], w[168], w[2], pars->GC_50, pars->GC_58, amp[977]); 
  FFV2_3_0(w[169], w[179], w[3], pars->GC_50, pars->GC_58, amp[978]); 
  FFV1_0(w[273], w[179], w[153], pars->GC_11, amp[979]); 
  FFV2_3_0(w[169], w[190], w[2], pars->GC_50, pars->GC_58, amp[980]); 
  FFV1_0(w[276], w[190], w[153], pars->GC_11, amp[981]); 
  FFV2_3_0(w[276], w[170], w[3], pars->GC_50, pars->GC_58, amp[982]); 
  FFV2_3_0(w[273], w[170], w[2], pars->GC_50, pars->GC_58, amp[983]); 
  FFV2_3_0(w[171], w[243], w[3], pars->GC_50, pars->GC_58, amp[984]); 
  FFV1_0(w[273], w[243], w[75], pars->GC_11, amp[985]); 
  FFV2_3_0(w[171], w[249], w[2], pars->GC_50, pars->GC_58, amp[986]); 
  FFV1_0(w[276], w[249], w[75], pars->GC_11, amp[987]); 
  FFV1_0(w[273], w[5], w[271], pars->GC_11, amp[988]); 
  FFV1_0(w[0], w[245], w[162], pars->GC_11, amp[989]); 
  FFV1_0(w[0], w[190], w[277], pars->GC_11, amp[990]); 
  FFV1_0(w[0], w[243], w[275], pars->GC_11, amp[991]); 
  FFV1_0(w[273], w[107], w[227], pars->GC_11, amp[992]); 
  FFV1_0(w[0], w[182], w[161], pars->GC_11, amp[993]); 
  FFV1_0(w[0], w[249], w[278], pars->GC_11, amp[994]); 
  FFV1_0(w[0], w[179], w[274], pars->GC_11, amp[995]); 
  FFV1_0(w[276], w[5], w[272], pars->GC_11, amp[996]); 
  FFV1_0(w[0], w[256], w[162], pars->GC_11, amp[997]); 
  FFV1_0(w[276], w[107], w[228], pars->GC_11, amp[998]); 
  FFV1_0(w[0], w[199], w[161], pars->GC_11, amp[999]); 
  FFV1_0(w[235], w[5], w[255], pars->GC_11, amp[1000]); 
  FFV1_0(w[108], w[279], w[220], pars->GC_11, amp[1001]); 
  VVV1_0(w[4], w[220], w[255], pars->GC_10, amp[1002]); 
  FFV1_0(w[235], w[249], w[114], pars->GC_11, amp[1003]); 
  FFV1_0(w[205], w[279], w[114], pars->GC_11, amp[1004]); 
  FFV1_0(w[205], w[249], w[173], pars->GC_11, amp[1005]); 
  FFV1_0(w[235], w[107], w[257], pars->GC_11, amp[1006]); 
  FFV1_0(w[108], w[236], w[267], pars->GC_11, amp[1007]); 
  VVV1_0(w[4], w[267], w[257], pars->GC_10, amp[1008]); 
  FFV1_0(w[235], w[190], w[120], pars->GC_11, amp[1009]); 
  FFV1_0(w[205], w[236], w[120], pars->GC_11, amp[1010]); 
  FFV1_0(w[205], w[190], w[174], pars->GC_11, amp[1011]); 
  FFV1_0(w[235], w[5], w[258], pars->GC_11, amp[1012]); 
  FFV1_0(w[235], w[107], w[259], pars->GC_11, amp[1013]); 
  FFV1_0(w[280], w[5], w[267], pars->GC_11, amp[1014]); 
  FFV1_0(w[280], w[107], w[220], pars->GC_11, amp[1015]); 
  VVV1_0(w[4], w[267], w[259], pars->GC_10, amp[1016]); 
  VVV1_0(w[4], w[220], w[258], pars->GC_10, amp[1017]); 
  FFV2_3_0(w[235], w[132], w[3], pars->GC_50, pars->GC_58, amp[1018]); 
  FFV1_0(w[208], w[5], w[174], pars->GC_11, amp[1019]); 
  FFV1_0(w[208], w[132], w[4], pars->GC_11, amp[1020]); 
  FFV2_3_0(w[235], w[133], w[3], pars->GC_50, pars->GC_58, amp[1021]); 
  FFV1_0(w[208], w[107], w[173], pars->GC_11, amp[1022]); 
  FFV1_0(w[208], w[133], w[4], pars->GC_11, amp[1023]); 
  FFV1_0(w[239], w[5], w[244], pars->GC_11, amp[1024]); 
  FFV1_0(w[108], w[281], w[224], pars->GC_11, amp[1025]); 
  VVV1_0(w[4], w[224], w[244], pars->GC_10, amp[1026]); 
  FFV1_0(w[239], w[243], w[114], pars->GC_11, amp[1027]); 
  FFV1_0(w[211], w[281], w[114], pars->GC_11, amp[1028]); 
  FFV1_0(w[211], w[243], w[173], pars->GC_11, amp[1029]); 
  FFV1_0(w[239], w[107], w[248], pars->GC_11, amp[1030]); 
  FFV1_0(w[108], w[240], w[269], pars->GC_11, amp[1031]); 
  VVV1_0(w[4], w[269], w[248], pars->GC_10, amp[1032]); 
  FFV1_0(w[239], w[179], w[120], pars->GC_11, amp[1033]); 
  FFV1_0(w[211], w[240], w[120], pars->GC_11, amp[1034]); 
  FFV1_0(w[211], w[179], w[174], pars->GC_11, amp[1035]); 
  FFV1_0(w[239], w[5], w[252], pars->GC_11, amp[1036]); 
  FFV1_0(w[239], w[107], w[253], pars->GC_11, amp[1037]); 
  FFV1_0(w[282], w[5], w[269], pars->GC_11, amp[1038]); 
  FFV1_0(w[282], w[107], w[224], pars->GC_11, amp[1039]); 
  VVV1_0(w[4], w[269], w[253], pars->GC_10, amp[1040]); 
  VVV1_0(w[4], w[224], w[252], pars->GC_10, amp[1041]); 
  FFV2_3_0(w[239], w[132], w[2], pars->GC_50, pars->GC_58, amp[1042]); 
  FFV1_0(w[214], w[5], w[174], pars->GC_11, amp[1043]); 
  FFV1_0(w[214], w[132], w[4], pars->GC_11, amp[1044]); 
  FFV2_3_0(w[239], w[133], w[2], pars->GC_50, pars->GC_58, amp[1045]); 
  FFV1_0(w[214], w[107], w[173], pars->GC_11, amp[1046]); 
  FFV1_0(w[214], w[133], w[4], pars->GC_11, amp[1047]); 
  FFV1_0(w[108], w[182], w[178], pars->GC_11, amp[1048]); 
  FFV2_3_0(w[154], w[240], w[3], pars->GC_50, pars->GC_58, amp[1049]); 
  FFV1_0(w[154], w[182], w[4], pars->GC_11, amp[1050]); 
  FFV1_0(w[247], w[179], w[178], pars->GC_11, amp[1051]); 
  FFV1_0(w[247], w[240], w[153], pars->GC_11, amp[1052]); 
  FFV1_0(w[280], w[179], w[153], pars->GC_11, amp[1053]); 
  FFV1_0(w[254], w[5], w[178], pars->GC_11, amp[1054]); 
  FFV2_3_0(w[282], w[168], w[3], pars->GC_50, pars->GC_58, amp[1055]); 
  FFV1_0(w[254], w[168], w[4], pars->GC_11, amp[1056]); 
  FFV1_0(w[251], w[190], w[178], pars->GC_11, amp[1057]); 
  FFV1_0(w[282], w[190], w[153], pars->GC_11, amp[1058]); 
  FFV1_0(w[251], w[236], w[153], pars->GC_11, amp[1059]); 
  FFV1_0(w[108], w[199], w[178], pars->GC_11, amp[1060]); 
  FFV2_3_0(w[154], w[236], w[2], pars->GC_50, pars->GC_58, amp[1061]); 
  FFV1_0(w[154], w[199], w[4], pars->GC_11, amp[1062]); 
  FFV1_0(w[260], w[5], w[178], pars->GC_11, amp[1063]); 
  FFV2_3_0(w[280], w[168], w[2], pars->GC_50, pars->GC_58, amp[1064]); 
  FFV1_0(w[260], w[168], w[4], pars->GC_11, amp[1065]); 
  FFV1_0(w[108], w[245], w[105], pars->GC_11, amp[1066]); 
  FFV2_3_0(w[143], w[281], w[3], pars->GC_50, pars->GC_58, amp[1067]); 
  FFV1_0(w[143], w[245], w[4], pars->GC_11, amp[1068]); 
  FFV1_0(w[247], w[243], w[105], pars->GC_11, amp[1069]); 
  FFV1_0(w[247], w[281], w[75], pars->GC_11, amp[1070]); 
  FFV1_0(w[280], w[243], w[75], pars->GC_11, amp[1071]); 
  FFV1_0(w[254], w[107], w[105], pars->GC_11, amp[1072]); 
  FFV2_3_0(w[282], w[170], w[3], pars->GC_50, pars->GC_58, amp[1073]); 
  FFV1_0(w[254], w[170], w[4], pars->GC_11, amp[1074]); 
  FFV1_0(w[251], w[249], w[105], pars->GC_11, amp[1075]); 
  FFV1_0(w[282], w[249], w[75], pars->GC_11, amp[1076]); 
  FFV1_0(w[251], w[279], w[75], pars->GC_11, amp[1077]); 
  FFV1_0(w[108], w[256], w[105], pars->GC_11, amp[1078]); 
  FFV2_3_0(w[143], w[279], w[2], pars->GC_50, pars->GC_58, amp[1079]); 
  FFV1_0(w[143], w[256], w[4], pars->GC_11, amp[1080]); 
  FFV1_0(w[260], w[107], w[105], pars->GC_11, amp[1081]); 
  FFV2_3_0(w[280], w[170], w[2], pars->GC_50, pars->GC_58, amp[1082]); 
  FFV1_0(w[260], w[170], w[4], pars->GC_11, amp[1083]); 
  FFV1_0(w[108], w[281], w[228], pars->GC_11, amp[1084]); 
  FFV1_0(w[0], w[281], w[257], pars->GC_11, amp[1085]); 
  FFV1_0(w[108], w[236], w[271], pars->GC_11, amp[1086]); 
  FFV1_0(w[0], w[236], w[244], pars->GC_11, amp[1087]); 
  VVV1_0(w[4], w[271], w[257], pars->GC_10, amp[1088]); 
  VVV1_0(w[4], w[228], w[244], pars->GC_10, amp[1089]); 
  FFV1_0(w[0], w[281], w[259], pars->GC_11, amp[1090]); 
  FFV1_0(w[280], w[5], w[271], pars->GC_11, amp[1091]); 
  VVV1_0(w[4], w[271], w[259], pars->GC_10, amp[1092]); 
  FFV2_3_0(w[146], w[281], w[3], pars->GC_50, pars->GC_58, amp[1093]); 
  FFV1_0(w[0], w[245], w[173], pars->GC_11, amp[1094]); 
  FFV1_0(w[146], w[245], w[4], pars->GC_11, amp[1095]); 
  FFV1_0(w[108], w[240], w[272], pars->GC_11, amp[1096]); 
  FFV1_0(w[0], w[240], w[255], pars->GC_11, amp[1097]); 
  FFV1_0(w[108], w[279], w[227], pars->GC_11, amp[1098]); 
  FFV1_0(w[0], w[279], w[248], pars->GC_11, amp[1099]); 
  VVV1_0(w[4], w[227], w[255], pars->GC_10, amp[1100]); 
  VVV1_0(w[4], w[272], w[248], pars->GC_10, amp[1101]); 
  FFV1_0(w[0], w[240], w[258], pars->GC_11, amp[1102]); 
  FFV1_0(w[280], w[107], w[227], pars->GC_11, amp[1103]); 
  VVV1_0(w[4], w[227], w[258], pars->GC_10, amp[1104]); 
  FFV2_3_0(w[158], w[240], w[3], pars->GC_50, pars->GC_58, amp[1105]); 
  FFV1_0(w[0], w[182], w[174], pars->GC_11, amp[1106]); 
  FFV1_0(w[158], w[182], w[4], pars->GC_11, amp[1107]); 
  FFV1_0(w[282], w[5], w[272], pars->GC_11, amp[1108]); 
  FFV1_0(w[0], w[279], w[253], pars->GC_11, amp[1109]); 
  VVV1_0(w[4], w[272], w[253], pars->GC_10, amp[1110]); 
  FFV1_0(w[282], w[107], w[228], pars->GC_11, amp[1111]); 
  FFV1_0(w[0], w[236], w[252], pars->GC_11, amp[1112]); 
  VVV1_0(w[4], w[228], w[252], pars->GC_10, amp[1113]); 
  FFV2_3_0(w[146], w[279], w[2], pars->GC_50, pars->GC_58, amp[1114]); 
  FFV1_0(w[0], w[256], w[173], pars->GC_11, amp[1115]); 
  FFV1_0(w[146], w[256], w[4], pars->GC_11, amp[1116]); 
  FFV2_3_0(w[158], w[236], w[2], pars->GC_50, pars->GC_58, amp[1117]); 
  FFV1_0(w[0], w[199], w[174], pars->GC_11, amp[1118]); 
  FFV1_0(w[158], w[199], w[4], pars->GC_11, amp[1119]); 
  FFV1_0(w[287], w[107], w[288], pars->GC_11, amp[1120]); 
  FFV1_0(w[108], w[290], w[289], pars->GC_11, amp[1121]); 
  FFV1_0(w[108], w[118], w[291], pars->GC_11, amp[1122]); 
  FFV1_0(w[108], w[286], w[292], pars->GC_11, amp[1123]); 
  FFV1_0(w[113], w[107], w[291], pars->GC_11, amp[1124]); 
  FFV1_0(w[113], w[286], w[289], pars->GC_11, amp[1125]); 
  FFV1_0(w[287], w[286], w[120], pars->GC_11, amp[1126]); 
  FFV2_5_0(w[293], w[286], w[3], pars->GC_51, pars->GC_58, amp[1127]); 
  FFV1_0(w[287], w[283], w[110], pars->GC_11, amp[1128]); 
  FFV1_0(w[108], w[111], w[294], pars->GC_11, amp[1129]); 
  FFV1_0(w[108], w[295], w[296], pars->GC_11, amp[1130]); 
  FFV1_0(w[108], w[109], w[297], pars->GC_11, amp[1131]); 
  FFV1_0(w[113], w[283], w[296], pars->GC_11, amp[1132]); 
  FFV1_0(w[113], w[109], w[294], pars->GC_11, amp[1133]); 
  FFV1_0(w[287], w[109], w[298], pars->GC_11, amp[1134]); 
  FFV2_5_0(w[299], w[109], w[3], pars->GC_51, pars->GC_58, amp[1135]); 
  FFV1_0(w[287], w[107], w[300], pars->GC_11, amp[1136]); 
  FFV1_0(w[287], w[283], w[123], pars->GC_11, amp[1137]); 
  FFV1_0(w[125], w[107], w[294], pars->GC_11, amp[1138]); 
  FFV1_0(w[125], w[283], w[289], pars->GC_11, amp[1139]); 
  FFV1_0(w[122], w[107], w[297], pars->GC_11, amp[1140]); 
  FFV1_0(w[122], w[295], w[289], pars->GC_11, amp[1141]); 
  FFV1_0(w[122], w[283], w[292], pars->GC_11, amp[1142]); 
  FFV1_0(w[122], w[118], w[294], pars->GC_11, amp[1143]); 
  FFV1_0(w[301], w[107], w[302], pars->GC_11, amp[1144]); 
  FFV1_0(w[108], w[303], w[289], pars->GC_11, amp[1145]); 
  FFV1_0(w[301], w[295], w[120], pars->GC_11, amp[1146]); 
  FFV2_5_0(w[293], w[295], w[2], pars->GC_51, pars->GC_58, amp[1147]); 
  FFV1_0(w[301], w[283], w[126], pars->GC_11, amp[1148]); 
  FFV1_0(w[108], w[127], w[294], pars->GC_11, amp[1149]); 
  FFV1_0(w[301], w[118], w[298], pars->GC_11, amp[1150]); 
  FFV2_5_0(w[299], w[118], w[2], pars->GC_51, pars->GC_58, amp[1151]); 
  FFV1_0(w[301], w[107], w[304], pars->GC_11, amp[1152]); 
  FFV1_0(w[301], w[283], w[129], pars->GC_11, amp[1153]); 
  FFV1_0(w[131], w[107], w[294], pars->GC_11, amp[1154]); 
  FFV1_0(w[131], w[283], w[289], pars->GC_11, amp[1155]); 
  FFV2_5_0(w[301], w[305], w[3], pars->GC_51, pars->GC_58, amp[1156]); 
  FFV2_5_0(w[287], w[305], w[2], pars->GC_51, pars->GC_58, amp[1157]); 
  FFV2_5_0(w[301], w[306], w[3], pars->GC_51, pars->GC_58, amp[1158]); 
  FFV2_5_0(w[287], w[306], w[2], pars->GC_51, pars->GC_58, amp[1159]); 
  FFV1_0(w[108], w[309], w[310], pars->GC_11, amp[1160]); 
  FFV1_0(w[312], w[107], w[311], pars->GC_11, amp[1161]); 
  FFV1_0(w[108], w[118], w[313], pars->GC_11, amp[1162]); 
  FFV1_0(w[308], w[118], w[311], pars->GC_11, amp[1163]); 
  FFV1_0(w[113], w[107], w[313], pars->GC_11, amp[1164]); 
  FFV1_0(w[308], w[107], w[314], pars->GC_11, amp[1165]); 
  FFV1_0(w[308], w[309], w[120], pars->GC_11, amp[1166]); 
  FFV2_5_0(w[308], w[315], w[3], pars->GC_51, pars->GC_58, amp[1167]); 
  FFV1_0(w[108], w[317], w[318], pars->GC_11, amp[1168]); 
  FFV1_0(w[319], w[107], w[311], pars->GC_11, amp[1169]); 
  FFV1_0(w[108], w[109], w[320], pars->GC_11, amp[1170]); 
  FFV1_0(w[316], w[109], w[311], pars->GC_11, amp[1171]); 
  FFV1_0(w[122], w[107], w[320], pars->GC_11, amp[1172]); 
  FFV1_0(w[316], w[107], w[321], pars->GC_11, amp[1173]); 
  FFV1_0(w[316], w[317], w[120], pars->GC_11, amp[1174]); 
  FFV2_5_0(w[316], w[315], w[2], pars->GC_51, pars->GC_58, amp[1175]); 
  FFV2_5_0(w[323], w[317], w[3], pars->GC_51, pars->GC_58, amp[1176]); 
  FFV2_5_0(w[323], w[309], w[2], pars->GC_51, pars->GC_58, amp[1177]); 
  FFV2_5_0(w[122], w[324], w[3], pars->GC_51, pars->GC_58, amp[1178]); 
  FFV1_0(w[122], w[309], w[322], pars->GC_11, amp[1179]); 
  FFV2_5_0(w[113], w[324], w[2], pars->GC_51, pars->GC_58, amp[1180]); 
  FFV1_0(w[113], w[317], w[322], pars->GC_11, amp[1181]); 
  FFV1_0(w[108], w[111], w[325], pars->GC_11, amp[1182]); 
  FFV1_0(w[108], w[309], w[326], pars->GC_11, amp[1183]); 
  FFV1_0(w[284], w[309], w[110], pars->GC_11, amp[1184]); 
  FFV1_0(w[284], w[111], w[311], pars->GC_11, amp[1185]); 
  FFV1_0(w[113], w[109], w[325], pars->GC_11, amp[1186]); 
  FFV1_0(w[284], w[109], w[314], pars->GC_11, amp[1187]); 
  FFV1_0(w[125], w[107], w[325], pars->GC_11, amp[1188]); 
  FFV1_0(w[284], w[309], w[123], pars->GC_11, amp[1189]); 
  FFV1_0(w[122], w[118], w[325], pars->GC_11, amp[1190]); 
  FFV1_0(w[284], w[118], w[321], pars->GC_11, amp[1191]); 
  FFV1_0(w[108], w[127], w[325], pars->GC_11, amp[1192]); 
  FFV1_0(w[108], w[317], w[327], pars->GC_11, amp[1193]); 
  FFV1_0(w[284], w[317], w[126], pars->GC_11, amp[1194]); 
  FFV1_0(w[284], w[127], w[311], pars->GC_11, amp[1195]); 
  FFV1_0(w[131], w[107], w[325], pars->GC_11, amp[1196]); 
  FFV1_0(w[284], w[317], w[129], pars->GC_11, amp[1197]); 
  FFV2_5_0(w[328], w[317], w[3], pars->GC_51, pars->GC_58, amp[1198]); 
  FFV2_5_0(w[328], w[309], w[2], pars->GC_51, pars->GC_58, amp[1199]); 
  FFV1_0(w[108], w[135], w[329], pars->GC_11, amp[1200]); 
  FFV1_0(w[312], w[283], w[136], pars->GC_11, amp[1201]); 
  FFV1_0(w[108], w[295], w[330], pars->GC_11, amp[1202]); 
  FFV1_0(w[308], w[295], w[136], pars->GC_11, amp[1203]); 
  FFV1_0(w[113], w[283], w[330], pars->GC_11, amp[1204]); 
  FFV1_0(w[308], w[283], w[138], pars->GC_11, amp[1205]); 
  FFV1_0(w[308], w[135], w[298], pars->GC_11, amp[1206]); 
  FFV2_5_0(w[308], w[331], w[3], pars->GC_51, pars->GC_58, amp[1207]); 
  FFV1_0(w[108], w[140], w[332], pars->GC_11, amp[1208]); 
  FFV1_0(w[319], w[283], w[136], pars->GC_11, amp[1209]); 
  FFV1_0(w[108], w[286], w[333], pars->GC_11, amp[1210]); 
  FFV1_0(w[316], w[286], w[136], pars->GC_11, amp[1211]); 
  FFV1_0(w[122], w[283], w[333], pars->GC_11, amp[1212]); 
  FFV1_0(w[316], w[283], w[142], pars->GC_11, amp[1213]); 
  FFV1_0(w[316], w[140], w[298], pars->GC_11, amp[1214]); 
  FFV2_5_0(w[316], w[331], w[2], pars->GC_51, pars->GC_58, amp[1215]); 
  FFV2_5_0(w[335], w[140], w[3], pars->GC_51, pars->GC_58, amp[1216]); 
  FFV2_5_0(w[335], w[135], w[2], pars->GC_51, pars->GC_58, amp[1217]); 
  FFV2_5_0(w[122], w[336], w[3], pars->GC_51, pars->GC_58, amp[1218]); 
  FFV1_0(w[122], w[135], w[334], pars->GC_11, amp[1219]); 
  FFV2_5_0(w[113], w[336], w[2], pars->GC_51, pars->GC_58, amp[1220]); 
  FFV1_0(w[113], w[140], w[334], pars->GC_11, amp[1221]); 
  FFV1_0(w[108], w[290], w[337], pars->GC_11, amp[1222]); 
  FFV1_0(w[108], w[135], w[338], pars->GC_11, amp[1223]); 
  FFV1_0(w[284], w[135], w[288], pars->GC_11, amp[1224]); 
  FFV1_0(w[284], w[290], w[136], pars->GC_11, amp[1225]); 
  FFV1_0(w[113], w[286], w[337], pars->GC_11, amp[1226]); 
  FFV1_0(w[284], w[286], w[138], pars->GC_11, amp[1227]); 
  FFV1_0(w[125], w[283], w[337], pars->GC_11, amp[1228]); 
  FFV1_0(w[284], w[135], w[300], pars->GC_11, amp[1229]); 
  FFV1_0(w[122], w[295], w[337], pars->GC_11, amp[1230]); 
  FFV1_0(w[284], w[295], w[142], pars->GC_11, amp[1231]); 
  FFV1_0(w[108], w[303], w[337], pars->GC_11, amp[1232]); 
  FFV1_0(w[108], w[140], w[339], pars->GC_11, amp[1233]); 
  FFV1_0(w[284], w[140], w[302], pars->GC_11, amp[1234]); 
  FFV1_0(w[284], w[303], w[136], pars->GC_11, amp[1235]); 
  FFV1_0(w[131], w[283], w[337], pars->GC_11, amp[1236]); 
  FFV1_0(w[284], w[140], w[304], pars->GC_11, amp[1237]); 
  FFV2_5_0(w[340], w[140], w[3], pars->GC_51, pars->GC_58, amp[1238]); 
  FFV2_5_0(w[340], w[135], w[2], pars->GC_51, pars->GC_58, amp[1239]); 
  FFV1_0(w[160], w[107], w[329], pars->GC_11, amp[1240]); 
  FFV1_0(w[160], w[283], w[310], pars->GC_11, amp[1241]); 
  FFV1_0(w[312], w[107], w[341], pars->GC_11, amp[1242]); 
  FFV1_0(w[312], w[283], w[161], pars->GC_11, amp[1243]); 
  FFV1_0(w[308], w[107], w[342], pars->GC_11, amp[1244]); 
  FFV1_0(w[308], w[295], w[161], pars->GC_11, amp[1245]); 
  FFV1_0(w[308], w[283], w[163], pars->GC_11, amp[1246]); 
  FFV1_0(w[308], w[118], w[341], pars->GC_11, amp[1247]); 
  FFV1_0(w[165], w[107], w[332], pars->GC_11, amp[1248]); 
  FFV1_0(w[165], w[283], w[318], pars->GC_11, amp[1249]); 
  FFV1_0(w[319], w[107], w[341], pars->GC_11, amp[1250]); 
  FFV1_0(w[319], w[283], w[161], pars->GC_11, amp[1251]); 
  FFV1_0(w[316], w[107], w[343], pars->GC_11, amp[1252]); 
  FFV1_0(w[316], w[286], w[161], pars->GC_11, amp[1253]); 
  FFV1_0(w[316], w[283], w[166], pars->GC_11, amp[1254]); 
  FFV1_0(w[316], w[109], w[341], pars->GC_11, amp[1255]); 
  FFV2_5_0(w[165], w[344], w[3], pars->GC_51, pars->GC_58, amp[1256]); 
  FFV2_5_0(w[160], w[344], w[2], pars->GC_51, pars->GC_58, amp[1257]); 
  FFV2_5_0(w[345], w[109], w[3], pars->GC_51, pars->GC_58, amp[1258]); 
  FFV1_0(w[160], w[109], w[334], pars->GC_11, amp[1259]); 
  FFV2_5_0(w[345], w[118], w[2], pars->GC_51, pars->GC_58, amp[1260]); 
  FFV1_0(w[165], w[118], w[334], pars->GC_11, amp[1261]); 
  FFV2_5_0(w[165], w[346], w[3], pars->GC_51, pars->GC_58, amp[1262]); 
  FFV2_5_0(w[160], w[346], w[2], pars->GC_51, pars->GC_58, amp[1263]); 
  FFV2_5_0(w[347], w[286], w[3], pars->GC_51, pars->GC_58, amp[1264]); 
  FFV1_0(w[160], w[286], w[322], pars->GC_11, amp[1265]); 
  FFV2_5_0(w[347], w[295], w[2], pars->GC_51, pars->GC_58, amp[1266]); 
  FFV1_0(w[165], w[295], w[322], pars->GC_11, amp[1267]); 
  FFV1_0(w[160], w[107], w[338], pars->GC_11, amp[1268]); 
  FFV1_0(w[284], w[290], w[161], pars->GC_11, amp[1269]); 
  FFV1_0(w[284], w[118], w[343], pars->GC_11, amp[1270]); 
  FFV1_0(w[284], w[286], w[163], pars->GC_11, amp[1271]); 
  FFV1_0(w[160], w[283], w[326], pars->GC_11, amp[1272]); 
  FFV1_0(w[284], w[111], w[341], pars->GC_11, amp[1273]); 
  FFV1_0(w[284], w[295], w[166], pars->GC_11, amp[1274]); 
  FFV1_0(w[284], w[109], w[342], pars->GC_11, amp[1275]); 
  FFV1_0(w[165], w[107], w[339], pars->GC_11, amp[1276]); 
  FFV1_0(w[284], w[303], w[161], pars->GC_11, amp[1277]); 
  FFV1_0(w[165], w[283], w[327], pars->GC_11, amp[1278]); 
  FFV1_0(w[284], w[127], w[341], pars->GC_11, amp[1279]); 
  FFV1_0(w[348], w[107], w[302], pars->GC_11, amp[1280]); 
  FFV1_0(w[108], w[349], w[310], pars->GC_11, amp[1281]); 
  VVV1_0(w[4], w[310], w[302], pars->GC_10, amp[1282]); 
  FFV1_0(w[348], w[295], w[120], pars->GC_11, amp[1283]); 
  FFV1_0(w[308], w[349], w[120], pars->GC_11, amp[1284]); 
  FFV1_0(w[308], w[295], w[174], pars->GC_11, amp[1285]); 
  FFV1_0(w[348], w[283], w[126], pars->GC_11, amp[1286]); 
  FFV1_0(w[108], w[172], w[329], pars->GC_11, amp[1287]); 
  VVV1_0(w[4], w[329], w[126], pars->GC_10, amp[1288]); 
  FFV1_0(w[348], w[118], w[298], pars->GC_11, amp[1289]); 
  FFV1_0(w[308], w[172], w[298], pars->GC_11, amp[1290]); 
  FFV1_0(w[308], w[118], w[350], pars->GC_11, amp[1291]); 
  FFV1_0(w[348], w[107], w[304], pars->GC_11, amp[1292]); 
  FFV1_0(w[348], w[283], w[129], pars->GC_11, amp[1293]); 
  FFV1_0(w[175], w[107], w[329], pars->GC_11, amp[1294]); 
  FFV1_0(w[175], w[283], w[310], pars->GC_11, amp[1295]); 
  VVV1_0(w[4], w[329], w[129], pars->GC_10, amp[1296]); 
  VVV1_0(w[4], w[310], w[304], pars->GC_10, amp[1297]); 
  FFV2_5_0(w[348], w[305], w[3], pars->GC_51, pars->GC_58, amp[1298]); 
  FFV1_0(w[312], w[107], w[350], pars->GC_11, amp[1299]); 
  FFV1_0(w[312], w[305], w[4], pars->GC_11, amp[1300]); 
  FFV2_5_0(w[348], w[306], w[3], pars->GC_51, pars->GC_58, amp[1301]); 
  FFV1_0(w[312], w[283], w[174], pars->GC_11, amp[1302]); 
  FFV1_0(w[312], w[306], w[4], pars->GC_11, amp[1303]); 
  FFV1_0(w[351], w[107], w[288], pars->GC_11, amp[1304]); 
  FFV1_0(w[108], w[352], w[318], pars->GC_11, amp[1305]); 
  VVV1_0(w[4], w[318], w[288], pars->GC_10, amp[1306]); 
  FFV1_0(w[351], w[286], w[120], pars->GC_11, amp[1307]); 
  FFV1_0(w[316], w[352], w[120], pars->GC_11, amp[1308]); 
  FFV1_0(w[316], w[286], w[174], pars->GC_11, amp[1309]); 
  FFV1_0(w[351], w[283], w[110], pars->GC_11, amp[1310]); 
  FFV1_0(w[108], w[176], w[332], pars->GC_11, amp[1311]); 
  VVV1_0(w[4], w[332], w[110], pars->GC_10, amp[1312]); 
  FFV1_0(w[351], w[109], w[298], pars->GC_11, amp[1313]); 
  FFV1_0(w[316], w[176], w[298], pars->GC_11, amp[1314]); 
  FFV1_0(w[316], w[109], w[350], pars->GC_11, amp[1315]); 
  FFV1_0(w[351], w[107], w[300], pars->GC_11, amp[1316]); 
  FFV1_0(w[351], w[283], w[123], pars->GC_11, amp[1317]); 
  FFV1_0(w[177], w[107], w[332], pars->GC_11, amp[1318]); 
  FFV1_0(w[177], w[283], w[318], pars->GC_11, amp[1319]); 
  VVV1_0(w[4], w[332], w[123], pars->GC_10, amp[1320]); 
  VVV1_0(w[4], w[318], w[300], pars->GC_10, amp[1321]); 
  FFV2_5_0(w[351], w[305], w[2], pars->GC_51, pars->GC_58, amp[1322]); 
  FFV1_0(w[319], w[107], w[350], pars->GC_11, amp[1323]); 
  FFV1_0(w[319], w[305], w[4], pars->GC_11, amp[1324]); 
  FFV2_5_0(w[351], w[306], w[2], pars->GC_51, pars->GC_58, amp[1325]); 
  FFV1_0(w[319], w[283], w[174], pars->GC_11, amp[1326]); 
  FFV1_0(w[319], w[306], w[4], pars->GC_11, amp[1327]); 
  FFV1_0(w[108], w[111], w[353], pars->GC_11, amp[1328]); 
  FFV2_5_0(w[335], w[176], w[3], pars->GC_51, pars->GC_58, amp[1329]); 
  FFV1_0(w[335], w[111], w[4], pars->GC_11, amp[1330]); 
  FFV1_0(w[113], w[109], w[353], pars->GC_11, amp[1331]); 
  FFV1_0(w[113], w[176], w[334], pars->GC_11, amp[1332]); 
  FFV1_0(w[175], w[109], w[334], pars->GC_11, amp[1333]); 
  FFV1_0(w[125], w[107], w[353], pars->GC_11, amp[1334]); 
  FFV2_5_0(w[177], w[344], w[3], pars->GC_51, pars->GC_58, amp[1335]); 
  FFV1_0(w[125], w[344], w[4], pars->GC_11, amp[1336]); 
  FFV1_0(w[122], w[118], w[353], pars->GC_11, amp[1337]); 
  FFV1_0(w[177], w[118], w[334], pars->GC_11, amp[1338]); 
  FFV1_0(w[122], w[172], w[334], pars->GC_11, amp[1339]); 
  FFV1_0(w[108], w[127], w[353], pars->GC_11, amp[1340]); 
  FFV2_5_0(w[335], w[172], w[2], pars->GC_51, pars->GC_58, amp[1341]); 
  FFV1_0(w[335], w[127], w[4], pars->GC_11, amp[1342]); 
  FFV1_0(w[131], w[107], w[353], pars->GC_11, amp[1343]); 
  FFV2_5_0(w[175], w[344], w[2], pars->GC_51, pars->GC_58, amp[1344]); 
  FFV1_0(w[131], w[344], w[4], pars->GC_11, amp[1345]); 
  FFV1_0(w[108], w[290], w[354], pars->GC_11, amp[1346]); 
  FFV2_5_0(w[323], w[352], w[3], pars->GC_51, pars->GC_58, amp[1347]); 
  FFV1_0(w[323], w[290], w[4], pars->GC_11, amp[1348]); 
  FFV1_0(w[113], w[286], w[354], pars->GC_11, amp[1349]); 
  FFV1_0(w[113], w[352], w[322], pars->GC_11, amp[1350]); 
  FFV1_0(w[175], w[286], w[322], pars->GC_11, amp[1351]); 
  FFV1_0(w[125], w[283], w[354], pars->GC_11, amp[1352]); 
  FFV2_5_0(w[177], w[346], w[3], pars->GC_51, pars->GC_58, amp[1353]); 
  FFV1_0(w[125], w[346], w[4], pars->GC_11, amp[1354]); 
  FFV1_0(w[122], w[295], w[354], pars->GC_11, amp[1355]); 
  FFV1_0(w[177], w[295], w[322], pars->GC_11, amp[1356]); 
  FFV1_0(w[122], w[349], w[322], pars->GC_11, amp[1357]); 
  FFV1_0(w[108], w[303], w[354], pars->GC_11, amp[1358]); 
  FFV2_5_0(w[323], w[349], w[2], pars->GC_51, pars->GC_58, amp[1359]); 
  FFV1_0(w[323], w[303], w[4], pars->GC_11, amp[1360]); 
  FFV1_0(w[131], w[283], w[354], pars->GC_11, amp[1361]); 
  FFV2_5_0(w[175], w[346], w[2], pars->GC_51, pars->GC_58, amp[1362]); 
  FFV1_0(w[131], w[346], w[4], pars->GC_11, amp[1363]); 
  FFV1_0(w[108], w[352], w[327], pars->GC_11, amp[1364]); 
  FFV1_0(w[284], w[352], w[126], pars->GC_11, amp[1365]); 
  FFV1_0(w[108], w[172], w[338], pars->GC_11, amp[1366]); 
  FFV1_0(w[284], w[172], w[288], pars->GC_11, amp[1367]); 
  VVV1_0(w[4], w[338], w[126], pars->GC_10, amp[1368]); 
  VVV1_0(w[4], w[327], w[288], pars->GC_10, amp[1369]); 
  FFV1_0(w[284], w[352], w[129], pars->GC_11, amp[1370]); 
  FFV1_0(w[175], w[107], w[338], pars->GC_11, amp[1371]); 
  VVV1_0(w[4], w[338], w[129], pars->GC_10, amp[1372]); 
  FFV2_5_0(w[328], w[352], w[3], pars->GC_51, pars->GC_58, amp[1373]); 
  FFV1_0(w[284], w[290], w[174], pars->GC_11, amp[1374]); 
  FFV1_0(w[328], w[290], w[4], pars->GC_11, amp[1375]); 
  FFV1_0(w[108], w[176], w[339], pars->GC_11, amp[1376]); 
  FFV1_0(w[284], w[176], w[302], pars->GC_11, amp[1377]); 
  FFV1_0(w[108], w[349], w[326], pars->GC_11, amp[1378]); 
  FFV1_0(w[284], w[349], w[110], pars->GC_11, amp[1379]); 
  VVV1_0(w[4], w[326], w[302], pars->GC_10, amp[1380]); 
  VVV1_0(w[4], w[339], w[110], pars->GC_10, amp[1381]); 
  FFV1_0(w[284], w[176], w[304], pars->GC_11, amp[1382]); 
  FFV1_0(w[175], w[283], w[326], pars->GC_11, amp[1383]); 
  VVV1_0(w[4], w[326], w[304], pars->GC_10, amp[1384]); 
  FFV2_5_0(w[340], w[176], w[3], pars->GC_51, pars->GC_58, amp[1385]); 
  FFV1_0(w[284], w[111], w[350], pars->GC_11, amp[1386]); 
  FFV1_0(w[340], w[111], w[4], pars->GC_11, amp[1387]); 
  FFV1_0(w[177], w[107], w[339], pars->GC_11, amp[1388]); 
  FFV1_0(w[284], w[349], w[123], pars->GC_11, amp[1389]); 
  VVV1_0(w[4], w[339], w[123], pars->GC_10, amp[1390]); 
  FFV1_0(w[177], w[283], w[327], pars->GC_11, amp[1391]); 
  FFV1_0(w[284], w[172], w[300], pars->GC_11, amp[1392]); 
  VVV1_0(w[4], w[327], w[300], pars->GC_10, amp[1393]); 
  FFV2_5_0(w[328], w[349], w[2], pars->GC_51, pars->GC_58, amp[1394]); 
  FFV1_0(w[284], w[303], w[174], pars->GC_11, amp[1395]); 
  FFV1_0(w[328], w[303], w[4], pars->GC_11, amp[1396]); 
  FFV2_5_0(w[340], w[172], w[2], pars->GC_51, pars->GC_58, amp[1397]); 
  FFV1_0(w[284], w[127], w[350], pars->GC_11, amp[1398]); 
  FFV1_0(w[340], w[127], w[4], pars->GC_11, amp[1399]); 
  FFV1_0(w[356], w[107], w[357], pars->GC_11, amp[1400]); 
  FFV1_0(w[108], w[358], w[289], pars->GC_11, amp[1401]); 
  FFV1_0(w[108], w[249], w[359], pars->GC_11, amp[1402]); 
  FFV1_0(w[108], w[355], w[360], pars->GC_11, amp[1403]); 
  FFV1_0(w[247], w[107], w[359], pars->GC_11, amp[1404]); 
  FFV1_0(w[247], w[355], w[289], pars->GC_11, amp[1405]); 
  FFV1_0(w[356], w[355], w[120], pars->GC_11, amp[1406]); 
  FFV2_3_0(w[293], w[355], w[3], pars->GC_50, pars->GC_58, amp[1407]); 
  FFV1_0(w[356], w[283], w[244], pars->GC_11, amp[1408]); 
  FFV1_0(w[108], w[245], w[294], pars->GC_11, amp[1409]); 
  FFV1_0(w[108], w[361], w[362], pars->GC_11, amp[1410]); 
  FFV1_0(w[108], w[243], w[363], pars->GC_11, amp[1411]); 
  FFV1_0(w[247], w[283], w[362], pars->GC_11, amp[1412]); 
  FFV1_0(w[247], w[243], w[294], pars->GC_11, amp[1413]); 
  FFV1_0(w[356], w[243], w[298], pars->GC_11, amp[1414]); 
  FFV2_3_0(w[299], w[243], w[3], pars->GC_50, pars->GC_58, amp[1415]); 
  FFV1_0(w[356], w[107], w[364], pars->GC_11, amp[1416]); 
  FFV1_0(w[356], w[283], w[252], pars->GC_11, amp[1417]); 
  FFV1_0(w[254], w[107], w[294], pars->GC_11, amp[1418]); 
  FFV1_0(w[254], w[283], w[289], pars->GC_11, amp[1419]); 
  FFV1_0(w[251], w[107], w[363], pars->GC_11, amp[1420]); 
  FFV1_0(w[251], w[361], w[289], pars->GC_11, amp[1421]); 
  FFV1_0(w[251], w[283], w[360], pars->GC_11, amp[1422]); 
  FFV1_0(w[251], w[249], w[294], pars->GC_11, amp[1423]); 
  FFV1_0(w[365], w[107], w[366], pars->GC_11, amp[1424]); 
  FFV1_0(w[108], w[367], w[289], pars->GC_11, amp[1425]); 
  FFV1_0(w[365], w[361], w[120], pars->GC_11, amp[1426]); 
  FFV2_3_0(w[293], w[361], w[2], pars->GC_50, pars->GC_58, amp[1427]); 
  FFV1_0(w[365], w[283], w[255], pars->GC_11, amp[1428]); 
  FFV1_0(w[108], w[256], w[294], pars->GC_11, amp[1429]); 
  FFV1_0(w[365], w[249], w[298], pars->GC_11, amp[1430]); 
  FFV2_3_0(w[299], w[249], w[2], pars->GC_50, pars->GC_58, amp[1431]); 
  FFV1_0(w[365], w[107], w[368], pars->GC_11, amp[1432]); 
  FFV1_0(w[365], w[283], w[258], pars->GC_11, amp[1433]); 
  FFV1_0(w[260], w[107], w[294], pars->GC_11, amp[1434]); 
  FFV1_0(w[260], w[283], w[289], pars->GC_11, amp[1435]); 
  FFV2_3_0(w[365], w[305], w[3], pars->GC_50, pars->GC_58, amp[1436]); 
  FFV2_3_0(w[356], w[305], w[2], pars->GC_50, pars->GC_58, amp[1437]); 
  FFV2_3_0(w[365], w[306], w[3], pars->GC_50, pars->GC_58, amp[1438]); 
  FFV2_3_0(w[356], w[306], w[2], pars->GC_50, pars->GC_58, amp[1439]); 
  FFV1_0(w[108], w[370], w[371], pars->GC_11, amp[1440]); 
  FFV1_0(w[372], w[107], w[311], pars->GC_11, amp[1441]); 
  FFV1_0(w[108], w[249], w[373], pars->GC_11, amp[1442]); 
  FFV1_0(w[369], w[249], w[311], pars->GC_11, amp[1443]); 
  FFV1_0(w[247], w[107], w[373], pars->GC_11, amp[1444]); 
  FFV1_0(w[369], w[107], w[374], pars->GC_11, amp[1445]); 
  FFV1_0(w[369], w[370], w[120], pars->GC_11, amp[1446]); 
  FFV2_3_0(w[369], w[315], w[3], pars->GC_50, pars->GC_58, amp[1447]); 
  FFV1_0(w[108], w[376], w[377], pars->GC_11, amp[1448]); 
  FFV1_0(w[378], w[107], w[311], pars->GC_11, amp[1449]); 
  FFV1_0(w[108], w[243], w[379], pars->GC_11, amp[1450]); 
  FFV1_0(w[375], w[243], w[311], pars->GC_11, amp[1451]); 
  FFV1_0(w[251], w[107], w[379], pars->GC_11, amp[1452]); 
  FFV1_0(w[375], w[107], w[380], pars->GC_11, amp[1453]); 
  FFV1_0(w[375], w[376], w[120], pars->GC_11, amp[1454]); 
  FFV2_3_0(w[375], w[315], w[2], pars->GC_50, pars->GC_58, amp[1455]); 
  FFV2_3_0(w[323], w[376], w[3], pars->GC_50, pars->GC_58, amp[1456]); 
  FFV2_3_0(w[323], w[370], w[2], pars->GC_50, pars->GC_58, amp[1457]); 
  FFV2_3_0(w[251], w[324], w[3], pars->GC_50, pars->GC_58, amp[1458]); 
  FFV1_0(w[251], w[370], w[322], pars->GC_11, amp[1459]); 
  FFV2_3_0(w[247], w[324], w[2], pars->GC_50, pars->GC_58, amp[1460]); 
  FFV1_0(w[247], w[376], w[322], pars->GC_11, amp[1461]); 
  FFV1_0(w[108], w[245], w[325], pars->GC_11, amp[1462]); 
  FFV1_0(w[108], w[370], w[381], pars->GC_11, amp[1463]); 
  FFV1_0(w[284], w[370], w[244], pars->GC_11, amp[1464]); 
  FFV1_0(w[284], w[245], w[311], pars->GC_11, amp[1465]); 
  FFV1_0(w[247], w[243], w[325], pars->GC_11, amp[1466]); 
  FFV1_0(w[284], w[243], w[374], pars->GC_11, amp[1467]); 
  FFV1_0(w[254], w[107], w[325], pars->GC_11, amp[1468]); 
  FFV1_0(w[284], w[370], w[252], pars->GC_11, amp[1469]); 
  FFV1_0(w[251], w[249], w[325], pars->GC_11, amp[1470]); 
  FFV1_0(w[284], w[249], w[380], pars->GC_11, amp[1471]); 
  FFV1_0(w[108], w[256], w[325], pars->GC_11, amp[1472]); 
  FFV1_0(w[108], w[376], w[382], pars->GC_11, amp[1473]); 
  FFV1_0(w[284], w[376], w[255], pars->GC_11, amp[1474]); 
  FFV1_0(w[284], w[256], w[311], pars->GC_11, amp[1475]); 
  FFV1_0(w[260], w[107], w[325], pars->GC_11, amp[1476]); 
  FFV1_0(w[284], w[376], w[258], pars->GC_11, amp[1477]); 
  FFV2_3_0(w[328], w[376], w[3], pars->GC_50, pars->GC_58, amp[1478]); 
  FFV2_3_0(w[328], w[370], w[2], pars->GC_50, pars->GC_58, amp[1479]); 
  FFV1_0(w[108], w[261], w[383], pars->GC_11, amp[1480]); 
  FFV1_0(w[372], w[283], w[136], pars->GC_11, amp[1481]); 
  FFV1_0(w[108], w[361], w[384], pars->GC_11, amp[1482]); 
  FFV1_0(w[369], w[361], w[136], pars->GC_11, amp[1483]); 
  FFV1_0(w[247], w[283], w[384], pars->GC_11, amp[1484]); 
  FFV1_0(w[369], w[283], w[263], pars->GC_11, amp[1485]); 
  FFV1_0(w[369], w[261], w[298], pars->GC_11, amp[1486]); 
  FFV2_3_0(w[369], w[331], w[3], pars->GC_50, pars->GC_58, amp[1487]); 
  FFV1_0(w[108], w[264], w[385], pars->GC_11, amp[1488]); 
  FFV1_0(w[378], w[283], w[136], pars->GC_11, amp[1489]); 
  FFV1_0(w[108], w[355], w[386], pars->GC_11, amp[1490]); 
  FFV1_0(w[375], w[355], w[136], pars->GC_11, amp[1491]); 
  FFV1_0(w[251], w[283], w[386], pars->GC_11, amp[1492]); 
  FFV1_0(w[375], w[283], w[266], pars->GC_11, amp[1493]); 
  FFV1_0(w[375], w[264], w[298], pars->GC_11, amp[1494]); 
  FFV2_3_0(w[375], w[331], w[2], pars->GC_50, pars->GC_58, amp[1495]); 
  FFV2_3_0(w[335], w[264], w[3], pars->GC_50, pars->GC_58, amp[1496]); 
  FFV2_3_0(w[335], w[261], w[2], pars->GC_50, pars->GC_58, amp[1497]); 
  FFV2_3_0(w[251], w[336], w[3], pars->GC_50, pars->GC_58, amp[1498]); 
  FFV1_0(w[251], w[261], w[334], pars->GC_11, amp[1499]); 
  FFV2_3_0(w[247], w[336], w[2], pars->GC_50, pars->GC_58, amp[1500]); 
  FFV1_0(w[247], w[264], w[334], pars->GC_11, amp[1501]); 
  FFV1_0(w[108], w[358], w[337], pars->GC_11, amp[1502]); 
  FFV1_0(w[108], w[261], w[387], pars->GC_11, amp[1503]); 
  FFV1_0(w[284], w[261], w[357], pars->GC_11, amp[1504]); 
  FFV1_0(w[284], w[358], w[136], pars->GC_11, amp[1505]); 
  FFV1_0(w[247], w[355], w[337], pars->GC_11, amp[1506]); 
  FFV1_0(w[284], w[355], w[263], pars->GC_11, amp[1507]); 
  FFV1_0(w[254], w[283], w[337], pars->GC_11, amp[1508]); 
  FFV1_0(w[284], w[261], w[364], pars->GC_11, amp[1509]); 
  FFV1_0(w[251], w[361], w[337], pars->GC_11, amp[1510]); 
  FFV1_0(w[284], w[361], w[266], pars->GC_11, amp[1511]); 
  FFV1_0(w[108], w[367], w[337], pars->GC_11, amp[1512]); 
  FFV1_0(w[108], w[264], w[388], pars->GC_11, amp[1513]); 
  FFV1_0(w[284], w[264], w[366], pars->GC_11, amp[1514]); 
  FFV1_0(w[284], w[367], w[136], pars->GC_11, amp[1515]); 
  FFV1_0(w[260], w[283], w[337], pars->GC_11, amp[1516]); 
  FFV1_0(w[284], w[264], w[368], pars->GC_11, amp[1517]); 
  FFV2_3_0(w[340], w[264], w[3], pars->GC_50, pars->GC_58, amp[1518]); 
  FFV2_3_0(w[340], w[261], w[2], pars->GC_50, pars->GC_58, amp[1519]); 
  FFV1_0(w[273], w[107], w[383], pars->GC_11, amp[1520]); 
  FFV1_0(w[273], w[283], w[371], pars->GC_11, amp[1521]); 
  FFV1_0(w[372], w[107], w[341], pars->GC_11, amp[1522]); 
  FFV1_0(w[372], w[283], w[161], pars->GC_11, amp[1523]); 
  FFV1_0(w[369], w[107], w[389], pars->GC_11, amp[1524]); 
  FFV1_0(w[369], w[361], w[161], pars->GC_11, amp[1525]); 
  FFV1_0(w[369], w[283], w[274], pars->GC_11, amp[1526]); 
  FFV1_0(w[369], w[249], w[341], pars->GC_11, amp[1527]); 
  FFV1_0(w[276], w[107], w[385], pars->GC_11, amp[1528]); 
  FFV1_0(w[276], w[283], w[377], pars->GC_11, amp[1529]); 
  FFV1_0(w[378], w[107], w[341], pars->GC_11, amp[1530]); 
  FFV1_0(w[378], w[283], w[161], pars->GC_11, amp[1531]); 
  FFV1_0(w[375], w[107], w[390], pars->GC_11, amp[1532]); 
  FFV1_0(w[375], w[355], w[161], pars->GC_11, amp[1533]); 
  FFV1_0(w[375], w[283], w[277], pars->GC_11, amp[1534]); 
  FFV1_0(w[375], w[243], w[341], pars->GC_11, amp[1535]); 
  FFV2_3_0(w[276], w[344], w[3], pars->GC_50, pars->GC_58, amp[1536]); 
  FFV2_3_0(w[273], w[344], w[2], pars->GC_50, pars->GC_58, amp[1537]); 
  FFV2_3_0(w[345], w[243], w[3], pars->GC_50, pars->GC_58, amp[1538]); 
  FFV1_0(w[273], w[243], w[334], pars->GC_11, amp[1539]); 
  FFV2_3_0(w[345], w[249], w[2], pars->GC_50, pars->GC_58, amp[1540]); 
  FFV1_0(w[276], w[249], w[334], pars->GC_11, amp[1541]); 
  FFV2_3_0(w[276], w[346], w[3], pars->GC_50, pars->GC_58, amp[1542]); 
  FFV2_3_0(w[273], w[346], w[2], pars->GC_50, pars->GC_58, amp[1543]); 
  FFV2_3_0(w[347], w[355], w[3], pars->GC_50, pars->GC_58, amp[1544]); 
  FFV1_0(w[273], w[355], w[322], pars->GC_11, amp[1545]); 
  FFV2_3_0(w[347], w[361], w[2], pars->GC_50, pars->GC_58, amp[1546]); 
  FFV1_0(w[276], w[361], w[322], pars->GC_11, amp[1547]); 
  FFV1_0(w[273], w[107], w[387], pars->GC_11, amp[1548]); 
  FFV1_0(w[284], w[358], w[161], pars->GC_11, amp[1549]); 
  FFV1_0(w[284], w[249], w[390], pars->GC_11, amp[1550]); 
  FFV1_0(w[284], w[355], w[274], pars->GC_11, amp[1551]); 
  FFV1_0(w[273], w[283], w[381], pars->GC_11, amp[1552]); 
  FFV1_0(w[284], w[245], w[341], pars->GC_11, amp[1553]); 
  FFV1_0(w[284], w[361], w[277], pars->GC_11, amp[1554]); 
  FFV1_0(w[284], w[243], w[389], pars->GC_11, amp[1555]); 
  FFV1_0(w[276], w[107], w[388], pars->GC_11, amp[1556]); 
  FFV1_0(w[284], w[367], w[161], pars->GC_11, amp[1557]); 
  FFV1_0(w[276], w[283], w[382], pars->GC_11, amp[1558]); 
  FFV1_0(w[284], w[256], w[341], pars->GC_11, amp[1559]); 
  FFV1_0(w[391], w[107], w[366], pars->GC_11, amp[1560]); 
  FFV1_0(w[108], w[392], w[371], pars->GC_11, amp[1561]); 
  VVV1_0(w[4], w[371], w[366], pars->GC_10, amp[1562]); 
  FFV1_0(w[391], w[361], w[120], pars->GC_11, amp[1563]); 
  FFV1_0(w[369], w[392], w[120], pars->GC_11, amp[1564]); 
  FFV1_0(w[369], w[361], w[174], pars->GC_11, amp[1565]); 
  FFV1_0(w[391], w[283], w[255], pars->GC_11, amp[1566]); 
  FFV1_0(w[108], w[279], w[383], pars->GC_11, amp[1567]); 
  VVV1_0(w[4], w[383], w[255], pars->GC_10, amp[1568]); 
  FFV1_0(w[391], w[249], w[298], pars->GC_11, amp[1569]); 
  FFV1_0(w[369], w[279], w[298], pars->GC_11, amp[1570]); 
  FFV1_0(w[369], w[249], w[350], pars->GC_11, amp[1571]); 
  FFV1_0(w[391], w[107], w[368], pars->GC_11, amp[1572]); 
  FFV1_0(w[391], w[283], w[258], pars->GC_11, amp[1573]); 
  FFV1_0(w[280], w[107], w[383], pars->GC_11, amp[1574]); 
  FFV1_0(w[280], w[283], w[371], pars->GC_11, amp[1575]); 
  VVV1_0(w[4], w[383], w[258], pars->GC_10, amp[1576]); 
  VVV1_0(w[4], w[371], w[368], pars->GC_10, amp[1577]); 
  FFV2_3_0(w[391], w[305], w[3], pars->GC_50, pars->GC_58, amp[1578]); 
  FFV1_0(w[372], w[107], w[350], pars->GC_11, amp[1579]); 
  FFV1_0(w[372], w[305], w[4], pars->GC_11, amp[1580]); 
  FFV2_3_0(w[391], w[306], w[3], pars->GC_50, pars->GC_58, amp[1581]); 
  FFV1_0(w[372], w[283], w[174], pars->GC_11, amp[1582]); 
  FFV1_0(w[372], w[306], w[4], pars->GC_11, amp[1583]); 
  FFV1_0(w[393], w[107], w[357], pars->GC_11, amp[1584]); 
  FFV1_0(w[108], w[394], w[377], pars->GC_11, amp[1585]); 
  VVV1_0(w[4], w[377], w[357], pars->GC_10, amp[1586]); 
  FFV1_0(w[393], w[355], w[120], pars->GC_11, amp[1587]); 
  FFV1_0(w[375], w[394], w[120], pars->GC_11, amp[1588]); 
  FFV1_0(w[375], w[355], w[174], pars->GC_11, amp[1589]); 
  FFV1_0(w[393], w[283], w[244], pars->GC_11, amp[1590]); 
  FFV1_0(w[108], w[281], w[385], pars->GC_11, amp[1591]); 
  VVV1_0(w[4], w[385], w[244], pars->GC_10, amp[1592]); 
  FFV1_0(w[393], w[243], w[298], pars->GC_11, amp[1593]); 
  FFV1_0(w[375], w[281], w[298], pars->GC_11, amp[1594]); 
  FFV1_0(w[375], w[243], w[350], pars->GC_11, amp[1595]); 
  FFV1_0(w[393], w[107], w[364], pars->GC_11, amp[1596]); 
  FFV1_0(w[393], w[283], w[252], pars->GC_11, amp[1597]); 
  FFV1_0(w[282], w[107], w[385], pars->GC_11, amp[1598]); 
  FFV1_0(w[282], w[283], w[377], pars->GC_11, amp[1599]); 
  VVV1_0(w[4], w[385], w[252], pars->GC_10, amp[1600]); 
  VVV1_0(w[4], w[377], w[364], pars->GC_10, amp[1601]); 
  FFV2_3_0(w[393], w[305], w[2], pars->GC_50, pars->GC_58, amp[1602]); 
  FFV1_0(w[378], w[107], w[350], pars->GC_11, amp[1603]); 
  FFV1_0(w[378], w[305], w[4], pars->GC_11, amp[1604]); 
  FFV2_3_0(w[393], w[306], w[2], pars->GC_50, pars->GC_58, amp[1605]); 
  FFV1_0(w[378], w[283], w[174], pars->GC_11, amp[1606]); 
  FFV1_0(w[378], w[306], w[4], pars->GC_11, amp[1607]); 
  FFV1_0(w[108], w[245], w[353], pars->GC_11, amp[1608]); 
  FFV2_3_0(w[335], w[281], w[3], pars->GC_50, pars->GC_58, amp[1609]); 
  FFV1_0(w[335], w[245], w[4], pars->GC_11, amp[1610]); 
  FFV1_0(w[247], w[243], w[353], pars->GC_11, amp[1611]); 
  FFV1_0(w[247], w[281], w[334], pars->GC_11, amp[1612]); 
  FFV1_0(w[280], w[243], w[334], pars->GC_11, amp[1613]); 
  FFV1_0(w[254], w[107], w[353], pars->GC_11, amp[1614]); 
  FFV2_3_0(w[282], w[344], w[3], pars->GC_50, pars->GC_58, amp[1615]); 
  FFV1_0(w[254], w[344], w[4], pars->GC_11, amp[1616]); 
  FFV1_0(w[251], w[249], w[353], pars->GC_11, amp[1617]); 
  FFV1_0(w[282], w[249], w[334], pars->GC_11, amp[1618]); 
  FFV1_0(w[251], w[279], w[334], pars->GC_11, amp[1619]); 
  FFV1_0(w[108], w[256], w[353], pars->GC_11, amp[1620]); 
  FFV2_3_0(w[335], w[279], w[2], pars->GC_50, pars->GC_58, amp[1621]); 
  FFV1_0(w[335], w[256], w[4], pars->GC_11, amp[1622]); 
  FFV1_0(w[260], w[107], w[353], pars->GC_11, amp[1623]); 
  FFV2_3_0(w[280], w[344], w[2], pars->GC_50, pars->GC_58, amp[1624]); 
  FFV1_0(w[260], w[344], w[4], pars->GC_11, amp[1625]); 
  FFV1_0(w[108], w[358], w[354], pars->GC_11, amp[1626]); 
  FFV2_3_0(w[323], w[394], w[3], pars->GC_50, pars->GC_58, amp[1627]); 
  FFV1_0(w[323], w[358], w[4], pars->GC_11, amp[1628]); 
  FFV1_0(w[247], w[355], w[354], pars->GC_11, amp[1629]); 
  FFV1_0(w[247], w[394], w[322], pars->GC_11, amp[1630]); 
  FFV1_0(w[280], w[355], w[322], pars->GC_11, amp[1631]); 
  FFV1_0(w[254], w[283], w[354], pars->GC_11, amp[1632]); 
  FFV2_3_0(w[282], w[346], w[3], pars->GC_50, pars->GC_58, amp[1633]); 
  FFV1_0(w[254], w[346], w[4], pars->GC_11, amp[1634]); 
  FFV1_0(w[251], w[361], w[354], pars->GC_11, amp[1635]); 
  FFV1_0(w[282], w[361], w[322], pars->GC_11, amp[1636]); 
  FFV1_0(w[251], w[392], w[322], pars->GC_11, amp[1637]); 
  FFV1_0(w[108], w[367], w[354], pars->GC_11, amp[1638]); 
  FFV2_3_0(w[323], w[392], w[2], pars->GC_50, pars->GC_58, amp[1639]); 
  FFV1_0(w[323], w[367], w[4], pars->GC_11, amp[1640]); 
  FFV1_0(w[260], w[283], w[354], pars->GC_11, amp[1641]); 
  FFV2_3_0(w[280], w[346], w[2], pars->GC_50, pars->GC_58, amp[1642]); 
  FFV1_0(w[260], w[346], w[4], pars->GC_11, amp[1643]); 
  FFV1_0(w[108], w[394], w[382], pars->GC_11, amp[1644]); 
  FFV1_0(w[284], w[394], w[255], pars->GC_11, amp[1645]); 
  FFV1_0(w[108], w[279], w[387], pars->GC_11, amp[1646]); 
  FFV1_0(w[284], w[279], w[357], pars->GC_11, amp[1647]); 
  VVV1_0(w[4], w[387], w[255], pars->GC_10, amp[1648]); 
  VVV1_0(w[4], w[382], w[357], pars->GC_10, amp[1649]); 
  FFV1_0(w[284], w[394], w[258], pars->GC_11, amp[1650]); 
  FFV1_0(w[280], w[107], w[387], pars->GC_11, amp[1651]); 
  VVV1_0(w[4], w[387], w[258], pars->GC_10, amp[1652]); 
  FFV2_3_0(w[328], w[394], w[3], pars->GC_50, pars->GC_58, amp[1653]); 
  FFV1_0(w[284], w[358], w[174], pars->GC_11, amp[1654]); 
  FFV1_0(w[328], w[358], w[4], pars->GC_11, amp[1655]); 
  FFV1_0(w[108], w[281], w[388], pars->GC_11, amp[1656]); 
  FFV1_0(w[284], w[281], w[366], pars->GC_11, amp[1657]); 
  FFV1_0(w[108], w[392], w[381], pars->GC_11, amp[1658]); 
  FFV1_0(w[284], w[392], w[244], pars->GC_11, amp[1659]); 
  VVV1_0(w[4], w[381], w[366], pars->GC_10, amp[1660]); 
  VVV1_0(w[4], w[388], w[244], pars->GC_10, amp[1661]); 
  FFV1_0(w[284], w[281], w[368], pars->GC_11, amp[1662]); 
  FFV1_0(w[280], w[283], w[381], pars->GC_11, amp[1663]); 
  VVV1_0(w[4], w[381], w[368], pars->GC_10, amp[1664]); 
  FFV2_3_0(w[340], w[281], w[3], pars->GC_50, pars->GC_58, amp[1665]); 
  FFV1_0(w[284], w[245], w[350], pars->GC_11, amp[1666]); 
  FFV1_0(w[340], w[245], w[4], pars->GC_11, amp[1667]); 
  FFV1_0(w[282], w[107], w[388], pars->GC_11, amp[1668]); 
  FFV1_0(w[284], w[392], w[252], pars->GC_11, amp[1669]); 
  VVV1_0(w[4], w[388], w[252], pars->GC_10, amp[1670]); 
  FFV1_0(w[282], w[283], w[382], pars->GC_11, amp[1671]); 
  FFV1_0(w[284], w[279], w[364], pars->GC_11, amp[1672]); 
  VVV1_0(w[4], w[382], w[364], pars->GC_10, amp[1673]); 
  FFV2_3_0(w[328], w[392], w[2], pars->GC_50, pars->GC_58, amp[1674]); 
  FFV1_0(w[284], w[367], w[174], pars->GC_11, amp[1675]); 
  FFV1_0(w[328], w[367], w[4], pars->GC_11, amp[1676]); 
  FFV2_3_0(w[340], w[279], w[2], pars->GC_50, pars->GC_58, amp[1677]); 
  FFV1_0(w[284], w[256], w[350], pars->GC_11, amp[1678]); 
  FFV1_0(w[340], w[256], w[4], pars->GC_11, amp[1679]); 
  FFV1_0(w[1], w[13], w[14], pars->GC_11, amp[1680]); 
  FFV1_0(w[16], w[6], w[14], pars->GC_11, amp[1681]); 
  FFV1_0(w[9], w[8], w[17], pars->GC_11, amp[1682]); 
  FFV2_5_0(w[18], w[8], w[3], pars->GC_51, pars->GC_58, amp[1683]); 
  FFV1_0(w[9], w[5], w[20], pars->GC_11, amp[1684]); 
  FFV1_0(w[1], w[22], w[21], pars->GC_11, amp[1685]); 
  FFV1_0(w[1], w[19], w[25], pars->GC_11, amp[1686]); 
  FFV1_0(w[16], w[19], w[21], pars->GC_11, amp[1687]); 
  FFV1_0(w[9], w[5], w[30], pars->GC_11, amp[1688]); 
  FFV1_0(w[31], w[6], w[21], pars->GC_11, amp[1689]); 
  FFV1_0(w[28], w[6], w[25], pars->GC_11, amp[1690]); 
  FFV1_0(w[28], w[13], w[21], pars->GC_11, amp[1691]); 
  FFV1_0(w[32], w[23], w[17], pars->GC_11, amp[1692]); 
  FFV2_5_0(w[18], w[23], w[2], pars->GC_51, pars->GC_58, amp[1693]); 
  FFV1_0(w[32], w[5], w[35], pars->GC_11, amp[1694]); 
  FFV1_0(w[1], w[36], w[21], pars->GC_11, amp[1695]); 
  FFV1_0(w[32], w[5], w[38], pars->GC_11, amp[1696]); 
  FFV1_0(w[39], w[6], w[21], pars->GC_11, amp[1697]); 
  FFV2_5_0(w[32], w[41], w[3], pars->GC_51, pars->GC_58, amp[1698]); 
  FFV2_5_0(w[9], w[41], w[2], pars->GC_51, pars->GC_58, amp[1699]); 
  FFV1_0(w[1], w[13], w[48], pars->GC_11, amp[1700]); 
  FFV1_0(w[16], w[6], w[48], pars->GC_11, amp[1701]); 
  FFV1_0(w[43], w[44], w[17], pars->GC_11, amp[1702]); 
  FFV2_5_0(w[43], w[50], w[3], pars->GC_51, pars->GC_58, amp[1703]); 
  FFV1_0(w[1], w[19], w[55], pars->GC_11, amp[1704]); 
  FFV1_0(w[28], w[6], w[55], pars->GC_11, amp[1705]); 
  FFV1_0(w[51], w[52], w[17], pars->GC_11, amp[1706]); 
  FFV2_5_0(w[51], w[50], w[2], pars->GC_51, pars->GC_58, amp[1707]); 
  FFV1_0(w[1], w[22], w[60], pars->GC_11, amp[1708]); 
  FFV1_0(w[0], w[44], w[20], pars->GC_11, amp[1709]); 
  FFV1_0(w[16], w[19], w[60], pars->GC_11, amp[1710]); 
  FFV1_0(w[31], w[6], w[60], pars->GC_11, amp[1711]); 
  FFV1_0(w[0], w[44], w[30], pars->GC_11, amp[1712]); 
  FFV1_0(w[28], w[13], w[60], pars->GC_11, amp[1713]); 
  FFV1_0(w[1], w[36], w[60], pars->GC_11, amp[1714]); 
  FFV1_0(w[0], w[52], w[35], pars->GC_11, amp[1715]); 
  FFV1_0(w[39], w[6], w[60], pars->GC_11, amp[1716]); 
  FFV1_0(w[0], w[52], w[38], pars->GC_11, amp[1717]); 
  FFV2_5_0(w[63], w[52], w[3], pars->GC_51, pars->GC_58, amp[1718]); 
  FFV2_5_0(w[63], w[44], w[2], pars->GC_51, pars->GC_58, amp[1719]); 
  FFV1_0(w[1], w[65], w[66], pars->GC_11, amp[1720]); 
  FFV1_0(w[47], w[5], w[67], pars->GC_11, amp[1721]); 
  FFV1_0(w[43], w[23], w[67], pars->GC_11, amp[1722]); 
  FFV1_0(w[43], w[5], w[69], pars->GC_11, amp[1723]); 
  FFV1_0(w[1], w[71], w[72], pars->GC_11, amp[1724]); 
  FFV1_0(w[54], w[5], w[67], pars->GC_11, amp[1725]); 
  FFV1_0(w[51], w[8], w[67], pars->GC_11, amp[1726]); 
  FFV1_0(w[51], w[5], w[74], pars->GC_11, amp[1727]); 
  FFV2_5_0(w[76], w[71], w[3], pars->GC_51, pars->GC_58, amp[1728]); 
  FFV2_5_0(w[76], w[65], w[2], pars->GC_51, pars->GC_58, amp[1729]); 
  FFV2_5_0(w[28], w[77], w[3], pars->GC_51, pars->GC_58, amp[1730]); 
  FFV1_0(w[28], w[65], w[75], pars->GC_11, amp[1731]); 
  FFV2_5_0(w[16], w[77], w[2], pars->GC_51, pars->GC_58, amp[1732]); 
  FFV1_0(w[16], w[71], w[75], pars->GC_11, amp[1733]); 
  FFV1_0(w[1], w[65], w[79], pars->GC_11, amp[1734]); 
  FFV1_0(w[0], w[12], w[67], pars->GC_11, amp[1735]); 
  FFV1_0(w[0], w[8], w[69], pars->GC_11, amp[1736]); 
  FFV1_0(w[0], w[23], w[74], pars->GC_11, amp[1737]); 
  FFV1_0(w[1], w[71], w[80], pars->GC_11, amp[1738]); 
  FFV1_0(w[0], w[34], w[67], pars->GC_11, amp[1739]); 
  FFV1_0(w[83], w[6], w[66], pars->GC_11, amp[1740]); 
  FFV1_0(w[47], w[5], w[85], pars->GC_11, amp[1741]); 
  FFV1_0(w[43], w[23], w[85], pars->GC_11, amp[1742]); 
  FFV1_0(w[43], w[5], w[87], pars->GC_11, amp[1743]); 
  FFV1_0(w[88], w[6], w[72], pars->GC_11, amp[1744]); 
  FFV1_0(w[54], w[5], w[85], pars->GC_11, amp[1745]); 
  FFV1_0(w[51], w[8], w[85], pars->GC_11, amp[1746]); 
  FFV1_0(w[51], w[5], w[90], pars->GC_11, amp[1747]); 
  FFV2_5_0(w[88], w[91], w[3], pars->GC_51, pars->GC_58, amp[1748]); 
  FFV2_5_0(w[83], w[91], w[2], pars->GC_51, pars->GC_58, amp[1749]); 
  FFV2_5_0(w[92], w[19], w[3], pars->GC_51, pars->GC_58, amp[1750]); 
  FFV1_0(w[83], w[19], w[75], pars->GC_11, amp[1751]); 
  FFV2_5_0(w[92], w[13], w[2], pars->GC_51, pars->GC_58, amp[1752]); 
  FFV1_0(w[88], w[13], w[75], pars->GC_11, amp[1753]); 
  FFV1_0(w[83], w[6], w[79], pars->GC_11, amp[1754]); 
  FFV1_0(w[0], w[12], w[85], pars->GC_11, amp[1755]); 
  FFV1_0(w[0], w[8], w[87], pars->GC_11, amp[1756]); 
  FFV1_0(w[0], w[23], w[90], pars->GC_11, amp[1757]); 
  FFV1_0(w[88], w[6], w[80], pars->GC_11, amp[1758]); 
  FFV1_0(w[0], w[34], w[85], pars->GC_11, amp[1759]); 
  FFV1_0(w[95], w[23], w[17], pars->GC_11, amp[1760]); 
  FFV1_0(w[43], w[96], w[17], pars->GC_11, amp[1761]); 
  FFV1_0(w[43], w[23], w[97], pars->GC_11, amp[1762]); 
  FFV1_0(w[95], w[5], w[35], pars->GC_11, amp[1763]); 
  FFV1_0(w[1], w[98], w[66], pars->GC_11, amp[1764]); 
  VVV1_0(w[4], w[66], w[35], pars->GC_10, amp[1765]); 
  FFV1_0(w[95], w[5], w[38], pars->GC_11, amp[1766]); 
  FFV1_0(w[100], w[6], w[66], pars->GC_11, amp[1767]); 
  VVV1_0(w[4], w[66], w[38], pars->GC_10, amp[1768]); 
  FFV2_5_0(w[95], w[41], w[3], pars->GC_51, pars->GC_58, amp[1769]); 
  FFV1_0(w[47], w[5], w[97], pars->GC_11, amp[1770]); 
  FFV1_0(w[47], w[41], w[4], pars->GC_11, amp[1771]); 
  FFV1_0(w[101], w[8], w[17], pars->GC_11, amp[1772]); 
  FFV1_0(w[51], w[102], w[17], pars->GC_11, amp[1773]); 
  FFV1_0(w[51], w[8], w[97], pars->GC_11, amp[1774]); 
  FFV1_0(w[101], w[5], w[20], pars->GC_11, amp[1775]); 
  FFV1_0(w[1], w[103], w[72], pars->GC_11, amp[1776]); 
  VVV1_0(w[4], w[72], w[20], pars->GC_10, amp[1777]); 
  FFV1_0(w[101], w[5], w[30], pars->GC_11, amp[1778]); 
  FFV1_0(w[104], w[6], w[72], pars->GC_11, amp[1779]); 
  VVV1_0(w[4], w[72], w[30], pars->GC_10, amp[1780]); 
  FFV2_5_0(w[101], w[41], w[2], pars->GC_51, pars->GC_58, amp[1781]); 
  FFV1_0(w[54], w[5], w[97], pars->GC_11, amp[1782]); 
  FFV1_0(w[54], w[41], w[4], pars->GC_11, amp[1783]); 
  FFV1_0(w[1], w[22], w[105], pars->GC_11, amp[1784]); 
  FFV2_5_0(w[76], w[103], w[3], pars->GC_51, pars->GC_58, amp[1785]); 
  FFV1_0(w[76], w[22], w[4], pars->GC_11, amp[1786]); 
  FFV1_0(w[16], w[19], w[105], pars->GC_11, amp[1787]); 
  FFV1_0(w[16], w[103], w[75], pars->GC_11, amp[1788]); 
  FFV1_0(w[100], w[19], w[75], pars->GC_11, amp[1789]); 
  FFV1_0(w[31], w[6], w[105], pars->GC_11, amp[1790]); 
  FFV2_5_0(w[104], w[91], w[3], pars->GC_51, pars->GC_58, amp[1791]); 
  FFV1_0(w[31], w[91], w[4], pars->GC_11, amp[1792]); 
  FFV1_0(w[28], w[13], w[105], pars->GC_11, amp[1793]); 
  FFV1_0(w[104], w[13], w[75], pars->GC_11, amp[1794]); 
  FFV1_0(w[28], w[98], w[75], pars->GC_11, amp[1795]); 
  FFV1_0(w[1], w[36], w[105], pars->GC_11, amp[1796]); 
  FFV2_5_0(w[76], w[98], w[2], pars->GC_51, pars->GC_58, amp[1797]); 
  FFV1_0(w[76], w[36], w[4], pars->GC_11, amp[1798]); 
  FFV1_0(w[39], w[6], w[105], pars->GC_11, amp[1799]); 
  FFV2_5_0(w[100], w[91], w[2], pars->GC_51, pars->GC_58, amp[1800]); 
  FFV1_0(w[39], w[91], w[4], pars->GC_11, amp[1801]); 
  FFV1_0(w[0], w[102], w[35], pars->GC_11, amp[1802]); 
  FFV1_0(w[1], w[98], w[79], pars->GC_11, amp[1803]); 
  VVV1_0(w[4], w[79], w[35], pars->GC_10, amp[1804]); 
  FFV1_0(w[0], w[102], w[38], pars->GC_11, amp[1805]); 
  FFV1_0(w[100], w[6], w[79], pars->GC_11, amp[1806]); 
  VVV1_0(w[4], w[79], w[38], pars->GC_10, amp[1807]); 
  FFV2_5_0(w[63], w[102], w[3], pars->GC_51, pars->GC_58, amp[1808]); 
  FFV1_0(w[0], w[12], w[97], pars->GC_11, amp[1809]); 
  FFV1_0(w[63], w[12], w[4], pars->GC_11, amp[1810]); 
  FFV1_0(w[1], w[103], w[80], pars->GC_11, amp[1811]); 
  FFV1_0(w[0], w[96], w[20], pars->GC_11, amp[1812]); 
  VVV1_0(w[4], w[80], w[20], pars->GC_10, amp[1813]); 
  FFV1_0(w[104], w[6], w[80], pars->GC_11, amp[1814]); 
  FFV1_0(w[0], w[96], w[30], pars->GC_11, amp[1815]); 
  VVV1_0(w[4], w[80], w[30], pars->GC_10, amp[1816]); 
  FFV2_5_0(w[63], w[96], w[2], pars->GC_51, pars->GC_58, amp[1817]); 
  FFV1_0(w[0], w[34], w[97], pars->GC_11, amp[1818]); 
  FFV1_0(w[63], w[34], w[4], pars->GC_11, amp[1819]); 
  FFV1_0(w[1], w[183], w[14], pars->GC_11, amp[1820]); 
  FFV1_0(w[186], w[6], w[14], pars->GC_11, amp[1821]); 
  FFV1_0(w[9], w[8], w[17], pars->GC_11, amp[1822]); 
  FFV2_5_0(w[18], w[8], w[3], pars->GC_51, pars->GC_58, amp[1823]); 
  FFV1_0(w[9], w[5], w[188], pars->GC_11, amp[1824]); 
  FFV1_0(w[1], w[189], w[21], pars->GC_11, amp[1825]); 
  FFV1_0(w[1], w[187], w[25], pars->GC_11, amp[1826]); 
  FFV1_0(w[186], w[187], w[21], pars->GC_11, amp[1827]); 
  FFV1_0(w[9], w[5], w[195], pars->GC_11, amp[1828]); 
  FFV1_0(w[196], w[6], w[21], pars->GC_11, amp[1829]); 
  FFV1_0(w[193], w[6], w[25], pars->GC_11, amp[1830]); 
  FFV1_0(w[193], w[183], w[21], pars->GC_11, amp[1831]); 
  FFV1_0(w[32], w[23], w[17], pars->GC_11, amp[1832]); 
  FFV2_5_0(w[18], w[23], w[2], pars->GC_51, pars->GC_58, amp[1833]); 
  FFV1_0(w[32], w[5], w[200], pars->GC_11, amp[1834]); 
  FFV1_0(w[1], w[201], w[21], pars->GC_11, amp[1835]); 
  FFV1_0(w[32], w[5], w[203], pars->GC_11, amp[1836]); 
  FFV1_0(w[204], w[6], w[21], pars->GC_11, amp[1837]); 
  FFV2_5_0(w[32], w[41], w[3], pars->GC_51, pars->GC_58, amp[1838]); 
  FFV2_5_0(w[9], w[41], w[2], pars->GC_51, pars->GC_58, amp[1839]); 
  FFV1_0(w[1], w[183], w[48], pars->GC_11, amp[1840]); 
  FFV1_0(w[186], w[6], w[48], pars->GC_11, amp[1841]); 
  FFV1_0(w[43], w[44], w[17], pars->GC_11, amp[1842]); 
  FFV2_5_0(w[43], w[50], w[3], pars->GC_51, pars->GC_58, amp[1843]); 
  FFV1_0(w[1], w[187], w[55], pars->GC_11, amp[1844]); 
  FFV1_0(w[193], w[6], w[55], pars->GC_11, amp[1845]); 
  FFV1_0(w[51], w[52], w[17], pars->GC_11, amp[1846]); 
  FFV2_5_0(w[51], w[50], w[2], pars->GC_51, pars->GC_58, amp[1847]); 
  FFV1_0(w[1], w[189], w[60], pars->GC_11, amp[1848]); 
  FFV1_0(w[0], w[44], w[188], pars->GC_11, amp[1849]); 
  FFV1_0(w[186], w[187], w[60], pars->GC_11, amp[1850]); 
  FFV1_0(w[196], w[6], w[60], pars->GC_11, amp[1851]); 
  FFV1_0(w[0], w[44], w[195], pars->GC_11, amp[1852]); 
  FFV1_0(w[193], w[183], w[60], pars->GC_11, amp[1853]); 
  FFV1_0(w[1], w[201], w[60], pars->GC_11, amp[1854]); 
  FFV1_0(w[0], w[52], w[200], pars->GC_11, amp[1855]); 
  FFV1_0(w[204], w[6], w[60], pars->GC_11, amp[1856]); 
  FFV1_0(w[0], w[52], w[203], pars->GC_11, amp[1857]); 
  FFV2_5_0(w[63], w[52], w[3], pars->GC_51, pars->GC_58, amp[1858]); 
  FFV2_5_0(w[63], w[44], w[2], pars->GC_51, pars->GC_58, amp[1859]); 
  FFV1_0(w[1], w[219], w[66], pars->GC_11, amp[1860]); 
  FFV1_0(w[47], w[5], w[67], pars->GC_11, amp[1861]); 
  FFV1_0(w[43], w[23], w[67], pars->GC_11, amp[1862]); 
  FFV1_0(w[43], w[5], w[222], pars->GC_11, amp[1863]); 
  FFV1_0(w[1], w[223], w[72], pars->GC_11, amp[1864]); 
  FFV1_0(w[54], w[5], w[67], pars->GC_11, amp[1865]); 
  FFV1_0(w[51], w[8], w[67], pars->GC_11, amp[1866]); 
  FFV1_0(w[51], w[5], w[226], pars->GC_11, amp[1867]); 
  FFV2_3_0(w[76], w[223], w[3], pars->GC_50, pars->GC_58, amp[1868]); 
  FFV2_3_0(w[76], w[219], w[2], pars->GC_50, pars->GC_58, amp[1869]); 
  FFV2_3_0(w[193], w[77], w[3], pars->GC_50, pars->GC_58, amp[1870]); 
  FFV1_0(w[193], w[219], w[75], pars->GC_11, amp[1871]); 
  FFV2_3_0(w[186], w[77], w[2], pars->GC_50, pars->GC_58, amp[1872]); 
  FFV1_0(w[186], w[223], w[75], pars->GC_11, amp[1873]); 
  FFV1_0(w[1], w[219], w[79], pars->GC_11, amp[1874]); 
  FFV1_0(w[0], w[12], w[67], pars->GC_11, amp[1875]); 
  FFV1_0(w[0], w[8], w[222], pars->GC_11, amp[1876]); 
  FFV1_0(w[0], w[23], w[226], pars->GC_11, amp[1877]); 
  FFV1_0(w[1], w[223], w[80], pars->GC_11, amp[1878]); 
  FFV1_0(w[0], w[34], w[67], pars->GC_11, amp[1879]); 
  FFV1_0(w[229], w[6], w[66], pars->GC_11, amp[1880]); 
  FFV1_0(w[47], w[5], w[85], pars->GC_11, amp[1881]); 
  FFV1_0(w[43], w[23], w[85], pars->GC_11, amp[1882]); 
  FFV1_0(w[43], w[5], w[231], pars->GC_11, amp[1883]); 
  FFV1_0(w[232], w[6], w[72], pars->GC_11, amp[1884]); 
  FFV1_0(w[54], w[5], w[85], pars->GC_11, amp[1885]); 
  FFV1_0(w[51], w[8], w[85], pars->GC_11, amp[1886]); 
  FFV1_0(w[51], w[5], w[234], pars->GC_11, amp[1887]); 
  FFV2_3_0(w[232], w[91], w[3], pars->GC_50, pars->GC_58, amp[1888]); 
  FFV2_3_0(w[229], w[91], w[2], pars->GC_50, pars->GC_58, amp[1889]); 
  FFV2_3_0(w[92], w[187], w[3], pars->GC_50, pars->GC_58, amp[1890]); 
  FFV1_0(w[229], w[187], w[75], pars->GC_11, amp[1891]); 
  FFV2_3_0(w[92], w[183], w[2], pars->GC_50, pars->GC_58, amp[1892]); 
  FFV1_0(w[232], w[183], w[75], pars->GC_11, amp[1893]); 
  FFV1_0(w[229], w[6], w[79], pars->GC_11, amp[1894]); 
  FFV1_0(w[0], w[12], w[85], pars->GC_11, amp[1895]); 
  FFV1_0(w[0], w[8], w[231], pars->GC_11, amp[1896]); 
  FFV1_0(w[0], w[23], w[234], pars->GC_11, amp[1897]); 
  FFV1_0(w[232], w[6], w[80], pars->GC_11, amp[1898]); 
  FFV1_0(w[0], w[34], w[85], pars->GC_11, amp[1899]); 
  FFV1_0(w[95], w[23], w[17], pars->GC_11, amp[1900]); 
  FFV1_0(w[43], w[96], w[17], pars->GC_11, amp[1901]); 
  FFV1_0(w[43], w[23], w[97], pars->GC_11, amp[1902]); 
  FFV1_0(w[95], w[5], w[200], pars->GC_11, amp[1903]); 
  FFV1_0(w[1], w[237], w[66], pars->GC_11, amp[1904]); 
  VVV1_0(w[4], w[66], w[200], pars->GC_10, amp[1905]); 
  FFV1_0(w[95], w[5], w[203], pars->GC_11, amp[1906]); 
  FFV1_0(w[238], w[6], w[66], pars->GC_11, amp[1907]); 
  VVV1_0(w[4], w[66], w[203], pars->GC_10, amp[1908]); 
  FFV2_5_0(w[95], w[41], w[3], pars->GC_51, pars->GC_58, amp[1909]); 
  FFV1_0(w[47], w[5], w[97], pars->GC_11, amp[1910]); 
  FFV1_0(w[47], w[41], w[4], pars->GC_11, amp[1911]); 
  FFV1_0(w[101], w[8], w[17], pars->GC_11, amp[1912]); 
  FFV1_0(w[51], w[102], w[17], pars->GC_11, amp[1913]); 
  FFV1_0(w[51], w[8], w[97], pars->GC_11, amp[1914]); 
  FFV1_0(w[101], w[5], w[188], pars->GC_11, amp[1915]); 
  FFV1_0(w[1], w[241], w[72], pars->GC_11, amp[1916]); 
  VVV1_0(w[4], w[72], w[188], pars->GC_10, amp[1917]); 
  FFV1_0(w[101], w[5], w[195], pars->GC_11, amp[1918]); 
  FFV1_0(w[242], w[6], w[72], pars->GC_11, amp[1919]); 
  VVV1_0(w[4], w[72], w[195], pars->GC_10, amp[1920]); 
  FFV2_5_0(w[101], w[41], w[2], pars->GC_51, pars->GC_58, amp[1921]); 
  FFV1_0(w[54], w[5], w[97], pars->GC_11, amp[1922]); 
  FFV1_0(w[54], w[41], w[4], pars->GC_11, amp[1923]); 
  FFV1_0(w[1], w[189], w[105], pars->GC_11, amp[1924]); 
  FFV2_3_0(w[76], w[241], w[3], pars->GC_50, pars->GC_58, amp[1925]); 
  FFV1_0(w[76], w[189], w[4], pars->GC_11, amp[1926]); 
  FFV1_0(w[186], w[187], w[105], pars->GC_11, amp[1927]); 
  FFV1_0(w[186], w[241], w[75], pars->GC_11, amp[1928]); 
  FFV1_0(w[238], w[187], w[75], pars->GC_11, amp[1929]); 
  FFV1_0(w[196], w[6], w[105], pars->GC_11, amp[1930]); 
  FFV2_3_0(w[242], w[91], w[3], pars->GC_50, pars->GC_58, amp[1931]); 
  FFV1_0(w[196], w[91], w[4], pars->GC_11, amp[1932]); 
  FFV1_0(w[193], w[183], w[105], pars->GC_11, amp[1933]); 
  FFV1_0(w[242], w[183], w[75], pars->GC_11, amp[1934]); 
  FFV1_0(w[193], w[237], w[75], pars->GC_11, amp[1935]); 
  FFV1_0(w[1], w[201], w[105], pars->GC_11, amp[1936]); 
  FFV2_3_0(w[76], w[237], w[2], pars->GC_50, pars->GC_58, amp[1937]); 
  FFV1_0(w[76], w[201], w[4], pars->GC_11, amp[1938]); 
  FFV1_0(w[204], w[6], w[105], pars->GC_11, amp[1939]); 
  FFV2_3_0(w[238], w[91], w[2], pars->GC_50, pars->GC_58, amp[1940]); 
  FFV1_0(w[204], w[91], w[4], pars->GC_11, amp[1941]); 
  FFV1_0(w[0], w[102], w[200], pars->GC_11, amp[1942]); 
  FFV1_0(w[1], w[237], w[79], pars->GC_11, amp[1943]); 
  VVV1_0(w[4], w[79], w[200], pars->GC_10, amp[1944]); 
  FFV1_0(w[0], w[102], w[203], pars->GC_11, amp[1945]); 
  FFV1_0(w[238], w[6], w[79], pars->GC_11, amp[1946]); 
  VVV1_0(w[4], w[79], w[203], pars->GC_10, amp[1947]); 
  FFV2_5_0(w[63], w[102], w[3], pars->GC_51, pars->GC_58, amp[1948]); 
  FFV1_0(w[0], w[12], w[97], pars->GC_11, amp[1949]); 
  FFV1_0(w[63], w[12], w[4], pars->GC_11, amp[1950]); 
  FFV1_0(w[1], w[241], w[80], pars->GC_11, amp[1951]); 
  FFV1_0(w[0], w[96], w[188], pars->GC_11, amp[1952]); 
  VVV1_0(w[4], w[80], w[188], pars->GC_10, amp[1953]); 
  FFV1_0(w[242], w[6], w[80], pars->GC_11, amp[1954]); 
  FFV1_0(w[0], w[96], w[195], pars->GC_11, amp[1955]); 
  VVV1_0(w[4], w[80], w[195], pars->GC_10, amp[1956]); 
  FFV2_5_0(w[63], w[96], w[2], pars->GC_51, pars->GC_58, amp[1957]); 
  FFV1_0(w[0], w[34], w[97], pars->GC_11, amp[1958]); 
  FFV1_0(w[63], w[34], w[4], pars->GC_11, amp[1959]); 
  FFV1_0(w[108], w[23], w[112], pars->GC_11, amp[1960]); 
  FFV1_0(w[113], w[5], w[112], pars->GC_11, amp[1961]); 
  FFV1_0(w[9], w[109], w[114], pars->GC_11, amp[1962]); 
  FFV2_5_0(w[115], w[109], w[3], pars->GC_51, pars->GC_58, amp[1963]); 
  FFV1_0(w[9], w[107], w[116], pars->GC_11, amp[1964]); 
  FFV1_0(w[108], w[12], w[117], pars->GC_11, amp[1965]); 
  FFV1_0(w[108], w[8], w[119], pars->GC_11, amp[1966]); 
  FFV1_0(w[113], w[8], w[117], pars->GC_11, amp[1967]); 
  FFV1_0(w[9], w[107], w[124], pars->GC_11, amp[1968]); 
  FFV1_0(w[125], w[5], w[117], pars->GC_11, amp[1969]); 
  FFV1_0(w[122], w[5], w[119], pars->GC_11, amp[1970]); 
  FFV1_0(w[122], w[23], w[117], pars->GC_11, amp[1971]); 
  FFV1_0(w[32], w[118], w[114], pars->GC_11, amp[1972]); 
  FFV2_5_0(w[115], w[118], w[2], pars->GC_51, pars->GC_58, amp[1973]); 
  FFV1_0(w[32], w[107], w[128], pars->GC_11, amp[1974]); 
  FFV1_0(w[108], w[34], w[117], pars->GC_11, amp[1975]); 
  FFV1_0(w[32], w[107], w[130], pars->GC_11, amp[1976]); 
  FFV1_0(w[131], w[5], w[117], pars->GC_11, amp[1977]); 
  FFV2_5_0(w[32], w[133], w[3], pars->GC_51, pars->GC_58, amp[1978]); 
  FFV2_5_0(w[9], w[133], w[2], pars->GC_51, pars->GC_58, amp[1979]); 
  FFV1_0(w[108], w[23], w[137], pars->GC_11, amp[1980]); 
  FFV1_0(w[113], w[5], w[137], pars->GC_11, amp[1981]); 
  FFV1_0(w[43], w[135], w[114], pars->GC_11, amp[1982]); 
  FFV2_5_0(w[43], w[139], w[3], pars->GC_51, pars->GC_58, amp[1983]); 
  FFV1_0(w[108], w[8], w[141], pars->GC_11, amp[1984]); 
  FFV1_0(w[122], w[5], w[141], pars->GC_11, amp[1985]); 
  FFV1_0(w[51], w[140], w[114], pars->GC_11, amp[1986]); 
  FFV2_5_0(w[51], w[139], w[2], pars->GC_51, pars->GC_58, amp[1987]); 
  FFV1_0(w[108], w[12], w[145], pars->GC_11, amp[1988]); 
  FFV1_0(w[0], w[135], w[116], pars->GC_11, amp[1989]); 
  FFV1_0(w[113], w[8], w[145], pars->GC_11, amp[1990]); 
  FFV1_0(w[125], w[5], w[145], pars->GC_11, amp[1991]); 
  FFV1_0(w[0], w[135], w[124], pars->GC_11, amp[1992]); 
  FFV1_0(w[122], w[23], w[145], pars->GC_11, amp[1993]); 
  FFV1_0(w[108], w[34], w[145], pars->GC_11, amp[1994]); 
  FFV1_0(w[0], w[140], w[128], pars->GC_11, amp[1995]); 
  FFV1_0(w[131], w[5], w[145], pars->GC_11, amp[1996]); 
  FFV1_0(w[0], w[140], w[130], pars->GC_11, amp[1997]); 
  FFV2_5_0(w[146], w[140], w[3], pars->GC_51, pars->GC_58, amp[1998]); 
  FFV2_5_0(w[146], w[135], w[2], pars->GC_51, pars->GC_58, amp[1999]); 
  FFV1_0(w[108], w[44], w[147], pars->GC_11, amp[2000]); 
  FFV1_0(w[47], w[107], w[148], pars->GC_11, amp[2001]); 
  FFV1_0(w[43], w[118], w[148], pars->GC_11, amp[2002]); 
  FFV1_0(w[43], w[107], w[149], pars->GC_11, amp[2003]); 
  FFV1_0(w[108], w[52], w[151], pars->GC_11, amp[2004]); 
  FFV1_0(w[54], w[107], w[148], pars->GC_11, amp[2005]); 
  FFV1_0(w[51], w[109], w[148], pars->GC_11, amp[2006]); 
  FFV1_0(w[51], w[107], w[152], pars->GC_11, amp[2007]); 
  FFV2_5_0(w[154], w[52], w[3], pars->GC_51, pars->GC_58, amp[2008]); 
  FFV2_5_0(w[154], w[44], w[2], pars->GC_51, pars->GC_58, amp[2009]); 
  FFV2_5_0(w[122], w[155], w[3], pars->GC_51, pars->GC_58, amp[2010]); 
  FFV1_0(w[122], w[44], w[153], pars->GC_11, amp[2011]); 
  FFV2_5_0(w[113], w[155], w[2], pars->GC_51, pars->GC_58, amp[2012]); 
  FFV1_0(w[113], w[52], w[153], pars->GC_11, amp[2013]); 
  FFV1_0(w[108], w[44], w[156], pars->GC_11, amp[2014]); 
  FFV1_0(w[0], w[111], w[148], pars->GC_11, amp[2015]); 
  FFV1_0(w[0], w[109], w[149], pars->GC_11, amp[2016]); 
  FFV1_0(w[0], w[118], w[152], pars->GC_11, amp[2017]); 
  FFV1_0(w[108], w[52], w[157], pars->GC_11, amp[2018]); 
  FFV1_0(w[0], w[127], w[148], pars->GC_11, amp[2019]); 
  FFV1_0(w[160], w[5], w[147], pars->GC_11, amp[2020]); 
  FFV1_0(w[47], w[107], w[162], pars->GC_11, amp[2021]); 
  FFV1_0(w[43], w[118], w[162], pars->GC_11, amp[2022]); 
  FFV1_0(w[43], w[107], w[164], pars->GC_11, amp[2023]); 
  FFV1_0(w[165], w[5], w[151], pars->GC_11, amp[2024]); 
  FFV1_0(w[54], w[107], w[162], pars->GC_11, amp[2025]); 
  FFV1_0(w[51], w[109], w[162], pars->GC_11, amp[2026]); 
  FFV1_0(w[51], w[107], w[167], pars->GC_11, amp[2027]); 
  FFV2_5_0(w[165], w[168], w[3], pars->GC_51, pars->GC_58, amp[2028]); 
  FFV2_5_0(w[160], w[168], w[2], pars->GC_51, pars->GC_58, amp[2029]); 
  FFV2_5_0(w[169], w[8], w[3], pars->GC_51, pars->GC_58, amp[2030]); 
  FFV1_0(w[160], w[8], w[153], pars->GC_11, amp[2031]); 
  FFV2_5_0(w[169], w[23], w[2], pars->GC_51, pars->GC_58, amp[2032]); 
  FFV1_0(w[165], w[23], w[153], pars->GC_11, amp[2033]); 
  FFV1_0(w[160], w[5], w[156], pars->GC_11, amp[2034]); 
  FFV1_0(w[0], w[111], w[162], pars->GC_11, amp[2035]); 
  FFV1_0(w[0], w[109], w[164], pars->GC_11, amp[2036]); 
  FFV1_0(w[0], w[118], w[167], pars->GC_11, amp[2037]); 
  FFV1_0(w[165], w[5], w[157], pars->GC_11, amp[2038]); 
  FFV1_0(w[0], w[127], w[162], pars->GC_11, amp[2039]); 
  FFV1_0(w[95], w[118], w[114], pars->GC_11, amp[2040]); 
  FFV1_0(w[43], w[172], w[114], pars->GC_11, amp[2041]); 
  FFV1_0(w[43], w[118], w[173], pars->GC_11, amp[2042]); 
  FFV1_0(w[95], w[107], w[128], pars->GC_11, amp[2043]); 
  FFV1_0(w[108], w[96], w[147], pars->GC_11, amp[2044]); 
  VVV1_0(w[4], w[147], w[128], pars->GC_10, amp[2045]); 
  FFV1_0(w[95], w[107], w[130], pars->GC_11, amp[2046]); 
  FFV1_0(w[175], w[5], w[147], pars->GC_11, amp[2047]); 
  VVV1_0(w[4], w[147], w[130], pars->GC_10, amp[2048]); 
  FFV2_5_0(w[95], w[133], w[3], pars->GC_51, pars->GC_58, amp[2049]); 
  FFV1_0(w[47], w[107], w[173], pars->GC_11, amp[2050]); 
  FFV1_0(w[47], w[133], w[4], pars->GC_11, amp[2051]); 
  FFV1_0(w[101], w[109], w[114], pars->GC_11, amp[2052]); 
  FFV1_0(w[51], w[176], w[114], pars->GC_11, amp[2053]); 
  FFV1_0(w[51], w[109], w[173], pars->GC_11, amp[2054]); 
  FFV1_0(w[101], w[107], w[116], pars->GC_11, amp[2055]); 
  FFV1_0(w[108], w[102], w[151], pars->GC_11, amp[2056]); 
  VVV1_0(w[4], w[151], w[116], pars->GC_10, amp[2057]); 
  FFV1_0(w[101], w[107], w[124], pars->GC_11, amp[2058]); 
  FFV1_0(w[177], w[5], w[151], pars->GC_11, amp[2059]); 
  VVV1_0(w[4], w[151], w[124], pars->GC_10, amp[2060]); 
  FFV2_5_0(w[101], w[133], w[2], pars->GC_51, pars->GC_58, amp[2061]); 
  FFV1_0(w[54], w[107], w[173], pars->GC_11, amp[2062]); 
  FFV1_0(w[54], w[133], w[4], pars->GC_11, amp[2063]); 
  FFV1_0(w[108], w[12], w[178], pars->GC_11, amp[2064]); 
  FFV2_5_0(w[154], w[102], w[3], pars->GC_51, pars->GC_58, amp[2065]); 
  FFV1_0(w[154], w[12], w[4], pars->GC_11, amp[2066]); 
  FFV1_0(w[113], w[8], w[178], pars->GC_11, amp[2067]); 
  FFV1_0(w[113], w[102], w[153], pars->GC_11, amp[2068]); 
  FFV1_0(w[175], w[8], w[153], pars->GC_11, amp[2069]); 
  FFV1_0(w[125], w[5], w[178], pars->GC_11, amp[2070]); 
  FFV2_5_0(w[177], w[168], w[3], pars->GC_51, pars->GC_58, amp[2071]); 
  FFV1_0(w[125], w[168], w[4], pars->GC_11, amp[2072]); 
  FFV1_0(w[122], w[23], w[178], pars->GC_11, amp[2073]); 
  FFV1_0(w[177], w[23], w[153], pars->GC_11, amp[2074]); 
  FFV1_0(w[122], w[96], w[153], pars->GC_11, amp[2075]); 
  FFV1_0(w[108], w[34], w[178], pars->GC_11, amp[2076]); 
  FFV2_5_0(w[154], w[96], w[2], pars->GC_51, pars->GC_58, amp[2077]); 
  FFV1_0(w[154], w[34], w[4], pars->GC_11, amp[2078]); 
  FFV1_0(w[131], w[5], w[178], pars->GC_11, amp[2079]); 
  FFV2_5_0(w[175], w[168], w[2], pars->GC_51, pars->GC_58, amp[2080]); 
  FFV1_0(w[131], w[168], w[4], pars->GC_11, amp[2081]); 
  FFV1_0(w[0], w[176], w[128], pars->GC_11, amp[2082]); 
  FFV1_0(w[108], w[96], w[156], pars->GC_11, amp[2083]); 
  VVV1_0(w[4], w[156], w[128], pars->GC_10, amp[2084]); 
  FFV1_0(w[0], w[176], w[130], pars->GC_11, amp[2085]); 
  FFV1_0(w[175], w[5], w[156], pars->GC_11, amp[2086]); 
  VVV1_0(w[4], w[156], w[130], pars->GC_10, amp[2087]); 
  FFV2_5_0(w[146], w[176], w[3], pars->GC_51, pars->GC_58, amp[2088]); 
  FFV1_0(w[0], w[111], w[173], pars->GC_11, amp[2089]); 
  FFV1_0(w[146], w[111], w[4], pars->GC_11, amp[2090]); 
  FFV1_0(w[108], w[102], w[157], pars->GC_11, amp[2091]); 
  FFV1_0(w[0], w[172], w[116], pars->GC_11, amp[2092]); 
  VVV1_0(w[4], w[157], w[116], pars->GC_10, amp[2093]); 
  FFV1_0(w[177], w[5], w[157], pars->GC_11, amp[2094]); 
  FFV1_0(w[0], w[172], w[124], pars->GC_11, amp[2095]); 
  VVV1_0(w[4], w[157], w[124], pars->GC_10, amp[2096]); 
  FFV2_5_0(w[146], w[172], w[2], pars->GC_51, pars->GC_58, amp[2097]); 
  FFV1_0(w[0], w[127], w[173], pars->GC_11, amp[2098]); 
  FFV1_0(w[146], w[127], w[4], pars->GC_11, amp[2099]); 
  FFV1_0(w[108], w[190], w[112], pars->GC_11, amp[2100]); 
  FFV1_0(w[247], w[5], w[112], pars->GC_11, amp[2101]); 
  FFV1_0(w[9], w[109], w[114], pars->GC_11, amp[2102]); 
  FFV2_5_0(w[115], w[109], w[3], pars->GC_51, pars->GC_58, amp[2103]); 
  FFV1_0(w[9], w[107], w[248], pars->GC_11, amp[2104]); 
  FFV1_0(w[108], w[182], w[117], pars->GC_11, amp[2105]); 
  FFV1_0(w[108], w[179], w[119], pars->GC_11, amp[2106]); 
  FFV1_0(w[247], w[179], w[117], pars->GC_11, amp[2107]); 
  FFV1_0(w[9], w[107], w[253], pars->GC_11, amp[2108]); 
  FFV1_0(w[254], w[5], w[117], pars->GC_11, amp[2109]); 
  FFV1_0(w[251], w[5], w[119], pars->GC_11, amp[2110]); 
  FFV1_0(w[251], w[190], w[117], pars->GC_11, amp[2111]); 
  FFV1_0(w[32], w[118], w[114], pars->GC_11, amp[2112]); 
  FFV2_5_0(w[115], w[118], w[2], pars->GC_51, pars->GC_58, amp[2113]); 
  FFV1_0(w[32], w[107], w[257], pars->GC_11, amp[2114]); 
  FFV1_0(w[108], w[199], w[117], pars->GC_11, amp[2115]); 
  FFV1_0(w[32], w[107], w[259], pars->GC_11, amp[2116]); 
  FFV1_0(w[260], w[5], w[117], pars->GC_11, amp[2117]); 
  FFV2_5_0(w[32], w[133], w[3], pars->GC_51, pars->GC_58, amp[2118]); 
  FFV2_5_0(w[9], w[133], w[2], pars->GC_51, pars->GC_58, amp[2119]); 
  FFV1_0(w[108], w[190], w[137], pars->GC_11, amp[2120]); 
  FFV1_0(w[247], w[5], w[137], pars->GC_11, amp[2121]); 
  FFV1_0(w[43], w[135], w[114], pars->GC_11, amp[2122]); 
  FFV2_5_0(w[43], w[139], w[3], pars->GC_51, pars->GC_58, amp[2123]); 
  FFV1_0(w[108], w[179], w[141], pars->GC_11, amp[2124]); 
  FFV1_0(w[251], w[5], w[141], pars->GC_11, amp[2125]); 
  FFV1_0(w[51], w[140], w[114], pars->GC_11, amp[2126]); 
  FFV2_5_0(w[51], w[139], w[2], pars->GC_51, pars->GC_58, amp[2127]); 
  FFV1_0(w[108], w[182], w[145], pars->GC_11, amp[2128]); 
  FFV1_0(w[0], w[135], w[248], pars->GC_11, amp[2129]); 
  FFV1_0(w[247], w[179], w[145], pars->GC_11, amp[2130]); 
  FFV1_0(w[254], w[5], w[145], pars->GC_11, amp[2131]); 
  FFV1_0(w[0], w[135], w[253], pars->GC_11, amp[2132]); 
  FFV1_0(w[251], w[190], w[145], pars->GC_11, amp[2133]); 
  FFV1_0(w[108], w[199], w[145], pars->GC_11, amp[2134]); 
  FFV1_0(w[0], w[140], w[257], pars->GC_11, amp[2135]); 
  FFV1_0(w[260], w[5], w[145], pars->GC_11, amp[2136]); 
  FFV1_0(w[0], w[140], w[259], pars->GC_11, amp[2137]); 
  FFV2_5_0(w[146], w[140], w[3], pars->GC_51, pars->GC_58, amp[2138]); 
  FFV2_5_0(w[146], w[135], w[2], pars->GC_51, pars->GC_58, amp[2139]); 
  FFV1_0(w[108], w[206], w[147], pars->GC_11, amp[2140]); 
  FFV1_0(w[47], w[107], w[148], pars->GC_11, amp[2141]); 
  FFV1_0(w[43], w[118], w[148], pars->GC_11, amp[2142]); 
  FFV1_0(w[43], w[107], w[268], pars->GC_11, amp[2143]); 
  FFV1_0(w[108], w[212], w[151], pars->GC_11, amp[2144]); 
  FFV1_0(w[54], w[107], w[148], pars->GC_11, amp[2145]); 
  FFV1_0(w[51], w[109], w[148], pars->GC_11, amp[2146]); 
  FFV1_0(w[51], w[107], w[270], pars->GC_11, amp[2147]); 
  FFV2_3_0(w[154], w[212], w[3], pars->GC_50, pars->GC_58, amp[2148]); 
  FFV2_3_0(w[154], w[206], w[2], pars->GC_50, pars->GC_58, amp[2149]); 
  FFV2_3_0(w[251], w[155], w[3], pars->GC_50, pars->GC_58, amp[2150]); 
  FFV1_0(w[251], w[206], w[153], pars->GC_11, amp[2151]); 
  FFV2_3_0(w[247], w[155], w[2], pars->GC_50, pars->GC_58, amp[2152]); 
  FFV1_0(w[247], w[212], w[153], pars->GC_11, amp[2153]); 
  FFV1_0(w[108], w[206], w[156], pars->GC_11, amp[2154]); 
  FFV1_0(w[0], w[111], w[148], pars->GC_11, amp[2155]); 
  FFV1_0(w[0], w[109], w[268], pars->GC_11, amp[2156]); 
  FFV1_0(w[0], w[118], w[270], pars->GC_11, amp[2157]); 
  FFV1_0(w[108], w[212], w[157], pars->GC_11, amp[2158]); 
  FFV1_0(w[0], w[127], w[148], pars->GC_11, amp[2159]); 
  FFV1_0(w[273], w[5], w[147], pars->GC_11, amp[2160]); 
  FFV1_0(w[47], w[107], w[162], pars->GC_11, amp[2161]); 
  FFV1_0(w[43], w[118], w[162], pars->GC_11, amp[2162]); 
  FFV1_0(w[43], w[107], w[275], pars->GC_11, amp[2163]); 
  FFV1_0(w[276], w[5], w[151], pars->GC_11, amp[2164]); 
  FFV1_0(w[54], w[107], w[162], pars->GC_11, amp[2165]); 
  FFV1_0(w[51], w[109], w[162], pars->GC_11, amp[2166]); 
  FFV1_0(w[51], w[107], w[278], pars->GC_11, amp[2167]); 
  FFV2_3_0(w[276], w[168], w[3], pars->GC_50, pars->GC_58, amp[2168]); 
  FFV2_3_0(w[273], w[168], w[2], pars->GC_50, pars->GC_58, amp[2169]); 
  FFV2_3_0(w[169], w[179], w[3], pars->GC_50, pars->GC_58, amp[2170]); 
  FFV1_0(w[273], w[179], w[153], pars->GC_11, amp[2171]); 
  FFV2_3_0(w[169], w[190], w[2], pars->GC_50, pars->GC_58, amp[2172]); 
  FFV1_0(w[276], w[190], w[153], pars->GC_11, amp[2173]); 
  FFV1_0(w[273], w[5], w[156], pars->GC_11, amp[2174]); 
  FFV1_0(w[0], w[111], w[162], pars->GC_11, amp[2175]); 
  FFV1_0(w[0], w[109], w[275], pars->GC_11, amp[2176]); 
  FFV1_0(w[0], w[118], w[278], pars->GC_11, amp[2177]); 
  FFV1_0(w[276], w[5], w[157], pars->GC_11, amp[2178]); 
  FFV1_0(w[0], w[127], w[162], pars->GC_11, amp[2179]); 
  FFV1_0(w[95], w[118], w[114], pars->GC_11, amp[2180]); 
  FFV1_0(w[43], w[172], w[114], pars->GC_11, amp[2181]); 
  FFV1_0(w[43], w[118], w[173], pars->GC_11, amp[2182]); 
  FFV1_0(w[95], w[107], w[257], pars->GC_11, amp[2183]); 
  FFV1_0(w[108], w[236], w[147], pars->GC_11, amp[2184]); 
  VVV1_0(w[4], w[147], w[257], pars->GC_10, amp[2185]); 
  FFV1_0(w[95], w[107], w[259], pars->GC_11, amp[2186]); 
  FFV1_0(w[280], w[5], w[147], pars->GC_11, amp[2187]); 
  VVV1_0(w[4], w[147], w[259], pars->GC_10, amp[2188]); 
  FFV2_5_0(w[95], w[133], w[3], pars->GC_51, pars->GC_58, amp[2189]); 
  FFV1_0(w[47], w[107], w[173], pars->GC_11, amp[2190]); 
  FFV1_0(w[47], w[133], w[4], pars->GC_11, amp[2191]); 
  FFV1_0(w[101], w[109], w[114], pars->GC_11, amp[2192]); 
  FFV1_0(w[51], w[176], w[114], pars->GC_11, amp[2193]); 
  FFV1_0(w[51], w[109], w[173], pars->GC_11, amp[2194]); 
  FFV1_0(w[101], w[107], w[248], pars->GC_11, amp[2195]); 
  FFV1_0(w[108], w[240], w[151], pars->GC_11, amp[2196]); 
  VVV1_0(w[4], w[151], w[248], pars->GC_10, amp[2197]); 
  FFV1_0(w[101], w[107], w[253], pars->GC_11, amp[2198]); 
  FFV1_0(w[282], w[5], w[151], pars->GC_11, amp[2199]); 
  VVV1_0(w[4], w[151], w[253], pars->GC_10, amp[2200]); 
  FFV2_5_0(w[101], w[133], w[2], pars->GC_51, pars->GC_58, amp[2201]); 
  FFV1_0(w[54], w[107], w[173], pars->GC_11, amp[2202]); 
  FFV1_0(w[54], w[133], w[4], pars->GC_11, amp[2203]); 
  FFV1_0(w[108], w[182], w[178], pars->GC_11, amp[2204]); 
  FFV2_3_0(w[154], w[240], w[3], pars->GC_50, pars->GC_58, amp[2205]); 
  FFV1_0(w[154], w[182], w[4], pars->GC_11, amp[2206]); 
  FFV1_0(w[247], w[179], w[178], pars->GC_11, amp[2207]); 
  FFV1_0(w[247], w[240], w[153], pars->GC_11, amp[2208]); 
  FFV1_0(w[280], w[179], w[153], pars->GC_11, amp[2209]); 
  FFV1_0(w[254], w[5], w[178], pars->GC_11, amp[2210]); 
  FFV2_3_0(w[282], w[168], w[3], pars->GC_50, pars->GC_58, amp[2211]); 
  FFV1_0(w[254], w[168], w[4], pars->GC_11, amp[2212]); 
  FFV1_0(w[251], w[190], w[178], pars->GC_11, amp[2213]); 
  FFV1_0(w[282], w[190], w[153], pars->GC_11, amp[2214]); 
  FFV1_0(w[251], w[236], w[153], pars->GC_11, amp[2215]); 
  FFV1_0(w[108], w[199], w[178], pars->GC_11, amp[2216]); 
  FFV2_3_0(w[154], w[236], w[2], pars->GC_50, pars->GC_58, amp[2217]); 
  FFV1_0(w[154], w[199], w[4], pars->GC_11, amp[2218]); 
  FFV1_0(w[260], w[5], w[178], pars->GC_11, amp[2219]); 
  FFV2_3_0(w[280], w[168], w[2], pars->GC_50, pars->GC_58, amp[2220]); 
  FFV1_0(w[260], w[168], w[4], pars->GC_11, amp[2221]); 
  FFV1_0(w[0], w[176], w[257], pars->GC_11, amp[2222]); 
  FFV1_0(w[108], w[236], w[156], pars->GC_11, amp[2223]); 
  VVV1_0(w[4], w[156], w[257], pars->GC_10, amp[2224]); 
  FFV1_0(w[0], w[176], w[259], pars->GC_11, amp[2225]); 
  FFV1_0(w[280], w[5], w[156], pars->GC_11, amp[2226]); 
  VVV1_0(w[4], w[156], w[259], pars->GC_10, amp[2227]); 
  FFV2_5_0(w[146], w[176], w[3], pars->GC_51, pars->GC_58, amp[2228]); 
  FFV1_0(w[0], w[111], w[173], pars->GC_11, amp[2229]); 
  FFV1_0(w[146], w[111], w[4], pars->GC_11, amp[2230]); 
  FFV1_0(w[108], w[240], w[157], pars->GC_11, amp[2231]); 
  FFV1_0(w[0], w[172], w[248], pars->GC_11, amp[2232]); 
  VVV1_0(w[4], w[157], w[248], pars->GC_10, amp[2233]); 
  FFV1_0(w[282], w[5], w[157], pars->GC_11, amp[2234]); 
  FFV1_0(w[0], w[172], w[253], pars->GC_11, amp[2235]); 
  VVV1_0(w[4], w[157], w[253], pars->GC_10, amp[2236]); 
  FFV2_5_0(w[146], w[172], w[2], pars->GC_51, pars->GC_58, amp[2237]); 
  FFV1_0(w[0], w[127], w[173], pars->GC_11, amp[2238]); 
  FFV1_0(w[146], w[127], w[4], pars->GC_11, amp[2239]); 
  FFV1_0(w[108], w[118], w[14], pars->GC_11, amp[2240]); 
  FFV1_0(w[113], w[107], w[14], pars->GC_11, amp[2241]); 
  FFV1_0(w[9], w[8], w[120], pars->GC_11, amp[2242]); 
  FFV2_5_0(w[121], w[8], w[3], pars->GC_51, pars->GC_58, amp[2243]); 
  FFV1_0(w[9], w[5], w[110], pars->GC_11, amp[2244]); 
  FFV1_0(w[108], w[111], w[21], pars->GC_11, amp[2245]); 
  FFV1_0(w[108], w[109], w[25], pars->GC_11, amp[2246]); 
  FFV1_0(w[113], w[109], w[21], pars->GC_11, amp[2247]); 
  FFV1_0(w[9], w[5], w[123], pars->GC_11, amp[2248]); 
  FFV1_0(w[125], w[107], w[21], pars->GC_11, amp[2249]); 
  FFV1_0(w[122], w[107], w[25], pars->GC_11, amp[2250]); 
  FFV1_0(w[122], w[118], w[21], pars->GC_11, amp[2251]); 
  FFV1_0(w[32], w[23], w[120], pars->GC_11, amp[2252]); 
  FFV2_5_0(w[121], w[23], w[2], pars->GC_51, pars->GC_58, amp[2253]); 
  FFV1_0(w[32], w[5], w[126], pars->GC_11, amp[2254]); 
  FFV1_0(w[108], w[127], w[21], pars->GC_11, amp[2255]); 
  FFV1_0(w[32], w[5], w[129], pars->GC_11, amp[2256]); 
  FFV1_0(w[131], w[107], w[21], pars->GC_11, amp[2257]); 
  FFV2_5_0(w[32], w[132], w[3], pars->GC_51, pars->GC_58, amp[2258]); 
  FFV2_5_0(w[9], w[132], w[2], pars->GC_51, pars->GC_58, amp[2259]); 
  FFV1_0(w[108], w[118], w[48], pars->GC_11, amp[2260]); 
  FFV1_0(w[113], w[107], w[48], pars->GC_11, amp[2261]); 
  FFV1_0(w[43], w[44], w[120], pars->GC_11, amp[2262]); 
  FFV2_5_0(w[43], w[150], w[3], pars->GC_51, pars->GC_58, amp[2263]); 
  FFV1_0(w[108], w[109], w[55], pars->GC_11, amp[2264]); 
  FFV1_0(w[122], w[107], w[55], pars->GC_11, amp[2265]); 
  FFV1_0(w[51], w[52], w[120], pars->GC_11, amp[2266]); 
  FFV2_5_0(w[51], w[150], w[2], pars->GC_51, pars->GC_58, amp[2267]); 
  FFV1_0(w[108], w[111], w[60], pars->GC_11, amp[2268]); 
  FFV1_0(w[0], w[44], w[110], pars->GC_11, amp[2269]); 
  FFV1_0(w[113], w[109], w[60], pars->GC_11, amp[2270]); 
  FFV1_0(w[125], w[107], w[60], pars->GC_11, amp[2271]); 
  FFV1_0(w[0], w[44], w[123], pars->GC_11, amp[2272]); 
  FFV1_0(w[122], w[118], w[60], pars->GC_11, amp[2273]); 
  FFV1_0(w[108], w[127], w[60], pars->GC_11, amp[2274]); 
  FFV1_0(w[0], w[52], w[126], pars->GC_11, amp[2275]); 
  FFV1_0(w[131], w[107], w[60], pars->GC_11, amp[2276]); 
  FFV1_0(w[0], w[52], w[129], pars->GC_11, amp[2277]); 
  FFV2_5_0(w[158], w[52], w[3], pars->GC_51, pars->GC_58, amp[2278]); 
  FFV2_5_0(w[158], w[44], w[2], pars->GC_51, pars->GC_58, amp[2279]); 
  FFV1_0(w[108], w[135], w[66], pars->GC_11, amp[2280]); 
  FFV1_0(w[47], w[5], w[136], pars->GC_11, amp[2281]); 
  FFV1_0(w[43], w[23], w[136], pars->GC_11, amp[2282]); 
  FFV1_0(w[43], w[5], w[138], pars->GC_11, amp[2283]); 
  FFV1_0(w[108], w[140], w[72], pars->GC_11, amp[2284]); 
  FFV1_0(w[54], w[5], w[136], pars->GC_11, amp[2285]); 
  FFV1_0(w[51], w[8], w[136], pars->GC_11, amp[2286]); 
  FFV1_0(w[51], w[5], w[142], pars->GC_11, amp[2287]); 
  FFV2_5_0(w[143], w[140], w[3], pars->GC_51, pars->GC_58, amp[2288]); 
  FFV2_5_0(w[143], w[135], w[2], pars->GC_51, pars->GC_58, amp[2289]); 
  FFV2_5_0(w[122], w[144], w[3], pars->GC_51, pars->GC_58, amp[2290]); 
  FFV1_0(w[122], w[135], w[75], pars->GC_11, amp[2291]); 
  FFV2_5_0(w[113], w[144], w[2], pars->GC_51, pars->GC_58, amp[2292]); 
  FFV1_0(w[113], w[140], w[75], pars->GC_11, amp[2293]); 
  FFV1_0(w[108], w[135], w[79], pars->GC_11, amp[2294]); 
  FFV1_0(w[0], w[12], w[136], pars->GC_11, amp[2295]); 
  FFV1_0(w[0], w[8], w[138], pars->GC_11, amp[2296]); 
  FFV1_0(w[0], w[23], w[142], pars->GC_11, amp[2297]); 
  FFV1_0(w[108], w[140], w[80], pars->GC_11, amp[2298]); 
  FFV1_0(w[0], w[34], w[136], pars->GC_11, amp[2299]); 
  FFV1_0(w[160], w[107], w[66], pars->GC_11, amp[2300]); 
  FFV1_0(w[47], w[5], w[161], pars->GC_11, amp[2301]); 
  FFV1_0(w[43], w[23], w[161], pars->GC_11, amp[2302]); 
  FFV1_0(w[43], w[5], w[163], pars->GC_11, amp[2303]); 
  FFV1_0(w[165], w[107], w[72], pars->GC_11, amp[2304]); 
  FFV1_0(w[54], w[5], w[161], pars->GC_11, amp[2305]); 
  FFV1_0(w[51], w[8], w[161], pars->GC_11, amp[2306]); 
  FFV1_0(w[51], w[5], w[166], pars->GC_11, amp[2307]); 
  FFV2_5_0(w[165], w[170], w[3], pars->GC_51, pars->GC_58, amp[2308]); 
  FFV2_5_0(w[160], w[170], w[2], pars->GC_51, pars->GC_58, amp[2309]); 
  FFV2_5_0(w[171], w[109], w[3], pars->GC_51, pars->GC_58, amp[2310]); 
  FFV1_0(w[160], w[109], w[75], pars->GC_11, amp[2311]); 
  FFV2_5_0(w[171], w[118], w[2], pars->GC_51, pars->GC_58, amp[2312]); 
  FFV1_0(w[165], w[118], w[75], pars->GC_11, amp[2313]); 
  FFV1_0(w[160], w[107], w[79], pars->GC_11, amp[2314]); 
  FFV1_0(w[0], w[12], w[161], pars->GC_11, amp[2315]); 
  FFV1_0(w[0], w[8], w[163], pars->GC_11, amp[2316]); 
  FFV1_0(w[0], w[23], w[166], pars->GC_11, amp[2317]); 
  FFV1_0(w[165], w[107], w[80], pars->GC_11, amp[2318]); 
  FFV1_0(w[0], w[34], w[161], pars->GC_11, amp[2319]); 
  FFV1_0(w[95], w[23], w[120], pars->GC_11, amp[2320]); 
  FFV1_0(w[43], w[96], w[120], pars->GC_11, amp[2321]); 
  FFV1_0(w[43], w[23], w[174], pars->GC_11, amp[2322]); 
  FFV1_0(w[95], w[5], w[126], pars->GC_11, amp[2323]); 
  FFV1_0(w[108], w[172], w[66], pars->GC_11, amp[2324]); 
  VVV1_0(w[4], w[66], w[126], pars->GC_10, amp[2325]); 
  FFV1_0(w[95], w[5], w[129], pars->GC_11, amp[2326]); 
  FFV1_0(w[175], w[107], w[66], pars->GC_11, amp[2327]); 
  VVV1_0(w[4], w[66], w[129], pars->GC_10, amp[2328]); 
  FFV2_5_0(w[95], w[132], w[3], pars->GC_51, pars->GC_58, amp[2329]); 
  FFV1_0(w[47], w[5], w[174], pars->GC_11, amp[2330]); 
  FFV1_0(w[47], w[132], w[4], pars->GC_11, amp[2331]); 
  FFV1_0(w[101], w[8], w[120], pars->GC_11, amp[2332]); 
  FFV1_0(w[51], w[102], w[120], pars->GC_11, amp[2333]); 
  FFV1_0(w[51], w[8], w[174], pars->GC_11, amp[2334]); 
  FFV1_0(w[101], w[5], w[110], pars->GC_11, amp[2335]); 
  FFV1_0(w[108], w[176], w[72], pars->GC_11, amp[2336]); 
  VVV1_0(w[4], w[72], w[110], pars->GC_10, amp[2337]); 
  FFV1_0(w[101], w[5], w[123], pars->GC_11, amp[2338]); 
  FFV1_0(w[177], w[107], w[72], pars->GC_11, amp[2339]); 
  VVV1_0(w[4], w[72], w[123], pars->GC_10, amp[2340]); 
  FFV2_5_0(w[101], w[132], w[2], pars->GC_51, pars->GC_58, amp[2341]); 
  FFV1_0(w[54], w[5], w[174], pars->GC_11, amp[2342]); 
  FFV1_0(w[54], w[132], w[4], pars->GC_11, amp[2343]); 
  FFV1_0(w[108], w[111], w[105], pars->GC_11, amp[2344]); 
  FFV2_5_0(w[143], w[176], w[3], pars->GC_51, pars->GC_58, amp[2345]); 
  FFV1_0(w[143], w[111], w[4], pars->GC_11, amp[2346]); 
  FFV1_0(w[113], w[109], w[105], pars->GC_11, amp[2347]); 
  FFV1_0(w[113], w[176], w[75], pars->GC_11, amp[2348]); 
  FFV1_0(w[175], w[109], w[75], pars->GC_11, amp[2349]); 
  FFV1_0(w[125], w[107], w[105], pars->GC_11, amp[2350]); 
  FFV2_5_0(w[177], w[170], w[3], pars->GC_51, pars->GC_58, amp[2351]); 
  FFV1_0(w[125], w[170], w[4], pars->GC_11, amp[2352]); 
  FFV1_0(w[122], w[118], w[105], pars->GC_11, amp[2353]); 
  FFV1_0(w[177], w[118], w[75], pars->GC_11, amp[2354]); 
  FFV1_0(w[122], w[172], w[75], pars->GC_11, amp[2355]); 
  FFV1_0(w[108], w[127], w[105], pars->GC_11, amp[2356]); 
  FFV2_5_0(w[143], w[172], w[2], pars->GC_51, pars->GC_58, amp[2357]); 
  FFV1_0(w[143], w[127], w[4], pars->GC_11, amp[2358]); 
  FFV1_0(w[131], w[107], w[105], pars->GC_11, amp[2359]); 
  FFV2_5_0(w[175], w[170], w[2], pars->GC_51, pars->GC_58, amp[2360]); 
  FFV1_0(w[131], w[170], w[4], pars->GC_11, amp[2361]); 
  FFV1_0(w[0], w[102], w[126], pars->GC_11, amp[2362]); 
  FFV1_0(w[108], w[172], w[79], pars->GC_11, amp[2363]); 
  VVV1_0(w[4], w[79], w[126], pars->GC_10, amp[2364]); 
  FFV1_0(w[0], w[102], w[129], pars->GC_11, amp[2365]); 
  FFV1_0(w[175], w[107], w[79], pars->GC_11, amp[2366]); 
  VVV1_0(w[4], w[79], w[129], pars->GC_10, amp[2367]); 
  FFV2_5_0(w[158], w[102], w[3], pars->GC_51, pars->GC_58, amp[2368]); 
  FFV1_0(w[0], w[12], w[174], pars->GC_11, amp[2369]); 
  FFV1_0(w[158], w[12], w[4], pars->GC_11, amp[2370]); 
  FFV1_0(w[108], w[176], w[80], pars->GC_11, amp[2371]); 
  FFV1_0(w[0], w[96], w[110], pars->GC_11, amp[2372]); 
  VVV1_0(w[4], w[80], w[110], pars->GC_10, amp[2373]); 
  FFV1_0(w[177], w[107], w[80], pars->GC_11, amp[2374]); 
  FFV1_0(w[0], w[96], w[123], pars->GC_11, amp[2375]); 
  VVV1_0(w[4], w[80], w[123], pars->GC_10, amp[2376]); 
  FFV2_5_0(w[158], w[96], w[2], pars->GC_51, pars->GC_58, amp[2377]); 
  FFV1_0(w[0], w[34], w[174], pars->GC_11, amp[2378]); 
  FFV1_0(w[158], w[34], w[4], pars->GC_11, amp[2379]); 
  FFV1_0(w[108], w[249], w[14], pars->GC_11, amp[2380]); 
  FFV1_0(w[247], w[107], w[14], pars->GC_11, amp[2381]); 
  FFV1_0(w[9], w[8], w[120], pars->GC_11, amp[2382]); 
  FFV2_5_0(w[121], w[8], w[3], pars->GC_51, pars->GC_58, amp[2383]); 
  FFV1_0(w[9], w[5], w[244], pars->GC_11, amp[2384]); 
  FFV1_0(w[108], w[245], w[21], pars->GC_11, amp[2385]); 
  FFV1_0(w[108], w[243], w[25], pars->GC_11, amp[2386]); 
  FFV1_0(w[247], w[243], w[21], pars->GC_11, amp[2387]); 
  FFV1_0(w[9], w[5], w[252], pars->GC_11, amp[2388]); 
  FFV1_0(w[254], w[107], w[21], pars->GC_11, amp[2389]); 
  FFV1_0(w[251], w[107], w[25], pars->GC_11, amp[2390]); 
  FFV1_0(w[251], w[249], w[21], pars->GC_11, amp[2391]); 
  FFV1_0(w[32], w[23], w[120], pars->GC_11, amp[2392]); 
  FFV2_5_0(w[121], w[23], w[2], pars->GC_51, pars->GC_58, amp[2393]); 
  FFV1_0(w[32], w[5], w[255], pars->GC_11, amp[2394]); 
  FFV1_0(w[108], w[256], w[21], pars->GC_11, amp[2395]); 
  FFV1_0(w[32], w[5], w[258], pars->GC_11, amp[2396]); 
  FFV1_0(w[260], w[107], w[21], pars->GC_11, amp[2397]); 
  FFV2_5_0(w[32], w[132], w[3], pars->GC_51, pars->GC_58, amp[2398]); 
  FFV2_5_0(w[9], w[132], w[2], pars->GC_51, pars->GC_58, amp[2399]); 
  FFV1_0(w[108], w[249], w[48], pars->GC_11, amp[2400]); 
  FFV1_0(w[247], w[107], w[48], pars->GC_11, amp[2401]); 
  FFV1_0(w[43], w[44], w[120], pars->GC_11, amp[2402]); 
  FFV2_5_0(w[43], w[150], w[3], pars->GC_51, pars->GC_58, amp[2403]); 
  FFV1_0(w[108], w[243], w[55], pars->GC_11, amp[2404]); 
  FFV1_0(w[251], w[107], w[55], pars->GC_11, amp[2405]); 
  FFV1_0(w[51], w[52], w[120], pars->GC_11, amp[2406]); 
  FFV2_5_0(w[51], w[150], w[2], pars->GC_51, pars->GC_58, amp[2407]); 
  FFV1_0(w[108], w[245], w[60], pars->GC_11, amp[2408]); 
  FFV1_0(w[0], w[44], w[244], pars->GC_11, amp[2409]); 
  FFV1_0(w[247], w[243], w[60], pars->GC_11, amp[2410]); 
  FFV1_0(w[254], w[107], w[60], pars->GC_11, amp[2411]); 
  FFV1_0(w[0], w[44], w[252], pars->GC_11, amp[2412]); 
  FFV1_0(w[251], w[249], w[60], pars->GC_11, amp[2413]); 
  FFV1_0(w[108], w[256], w[60], pars->GC_11, amp[2414]); 
  FFV1_0(w[0], w[52], w[255], pars->GC_11, amp[2415]); 
  FFV1_0(w[260], w[107], w[60], pars->GC_11, amp[2416]); 
  FFV1_0(w[0], w[52], w[258], pars->GC_11, amp[2417]); 
  FFV2_5_0(w[158], w[52], w[3], pars->GC_51, pars->GC_58, amp[2418]); 
  FFV2_5_0(w[158], w[44], w[2], pars->GC_51, pars->GC_58, amp[2419]); 
  FFV1_0(w[108], w[261], w[66], pars->GC_11, amp[2420]); 
  FFV1_0(w[47], w[5], w[136], pars->GC_11, amp[2421]); 
  FFV1_0(w[43], w[23], w[136], pars->GC_11, amp[2422]); 
  FFV1_0(w[43], w[5], w[263], pars->GC_11, amp[2423]); 
  FFV1_0(w[108], w[264], w[72], pars->GC_11, amp[2424]); 
  FFV1_0(w[54], w[5], w[136], pars->GC_11, amp[2425]); 
  FFV1_0(w[51], w[8], w[136], pars->GC_11, amp[2426]); 
  FFV1_0(w[51], w[5], w[266], pars->GC_11, amp[2427]); 
  FFV2_3_0(w[143], w[264], w[3], pars->GC_50, pars->GC_58, amp[2428]); 
  FFV2_3_0(w[143], w[261], w[2], pars->GC_50, pars->GC_58, amp[2429]); 
  FFV2_3_0(w[251], w[144], w[3], pars->GC_50, pars->GC_58, amp[2430]); 
  FFV1_0(w[251], w[261], w[75], pars->GC_11, amp[2431]); 
  FFV2_3_0(w[247], w[144], w[2], pars->GC_50, pars->GC_58, amp[2432]); 
  FFV1_0(w[247], w[264], w[75], pars->GC_11, amp[2433]); 
  FFV1_0(w[108], w[261], w[79], pars->GC_11, amp[2434]); 
  FFV1_0(w[0], w[12], w[136], pars->GC_11, amp[2435]); 
  FFV1_0(w[0], w[8], w[263], pars->GC_11, amp[2436]); 
  FFV1_0(w[0], w[23], w[266], pars->GC_11, amp[2437]); 
  FFV1_0(w[108], w[264], w[80], pars->GC_11, amp[2438]); 
  FFV1_0(w[0], w[34], w[136], pars->GC_11, amp[2439]); 
  FFV1_0(w[273], w[107], w[66], pars->GC_11, amp[2440]); 
  FFV1_0(w[47], w[5], w[161], pars->GC_11, amp[2441]); 
  FFV1_0(w[43], w[23], w[161], pars->GC_11, amp[2442]); 
  FFV1_0(w[43], w[5], w[274], pars->GC_11, amp[2443]); 
  FFV1_0(w[276], w[107], w[72], pars->GC_11, amp[2444]); 
  FFV1_0(w[54], w[5], w[161], pars->GC_11, amp[2445]); 
  FFV1_0(w[51], w[8], w[161], pars->GC_11, amp[2446]); 
  FFV1_0(w[51], w[5], w[277], pars->GC_11, amp[2447]); 
  FFV2_3_0(w[276], w[170], w[3], pars->GC_50, pars->GC_58, amp[2448]); 
  FFV2_3_0(w[273], w[170], w[2], pars->GC_50, pars->GC_58, amp[2449]); 
  FFV2_3_0(w[171], w[243], w[3], pars->GC_50, pars->GC_58, amp[2450]); 
  FFV1_0(w[273], w[243], w[75], pars->GC_11, amp[2451]); 
  FFV2_3_0(w[171], w[249], w[2], pars->GC_50, pars->GC_58, amp[2452]); 
  FFV1_0(w[276], w[249], w[75], pars->GC_11, amp[2453]); 
  FFV1_0(w[273], w[107], w[79], pars->GC_11, amp[2454]); 
  FFV1_0(w[0], w[12], w[161], pars->GC_11, amp[2455]); 
  FFV1_0(w[0], w[8], w[274], pars->GC_11, amp[2456]); 
  FFV1_0(w[0], w[23], w[277], pars->GC_11, amp[2457]); 
  FFV1_0(w[276], w[107], w[80], pars->GC_11, amp[2458]); 
  FFV1_0(w[0], w[34], w[161], pars->GC_11, amp[2459]); 
  FFV1_0(w[95], w[23], w[120], pars->GC_11, amp[2460]); 
  FFV1_0(w[43], w[96], w[120], pars->GC_11, amp[2461]); 
  FFV1_0(w[43], w[23], w[174], pars->GC_11, amp[2462]); 
  FFV1_0(w[95], w[5], w[255], pars->GC_11, amp[2463]); 
  FFV1_0(w[108], w[279], w[66], pars->GC_11, amp[2464]); 
  VVV1_0(w[4], w[66], w[255], pars->GC_10, amp[2465]); 
  FFV1_0(w[95], w[5], w[258], pars->GC_11, amp[2466]); 
  FFV1_0(w[280], w[107], w[66], pars->GC_11, amp[2467]); 
  VVV1_0(w[4], w[66], w[258], pars->GC_10, amp[2468]); 
  FFV2_5_0(w[95], w[132], w[3], pars->GC_51, pars->GC_58, amp[2469]); 
  FFV1_0(w[47], w[5], w[174], pars->GC_11, amp[2470]); 
  FFV1_0(w[47], w[132], w[4], pars->GC_11, amp[2471]); 
  FFV1_0(w[101], w[8], w[120], pars->GC_11, amp[2472]); 
  FFV1_0(w[51], w[102], w[120], pars->GC_11, amp[2473]); 
  FFV1_0(w[51], w[8], w[174], pars->GC_11, amp[2474]); 
  FFV1_0(w[101], w[5], w[244], pars->GC_11, amp[2475]); 
  FFV1_0(w[108], w[281], w[72], pars->GC_11, amp[2476]); 
  VVV1_0(w[4], w[72], w[244], pars->GC_10, amp[2477]); 
  FFV1_0(w[101], w[5], w[252], pars->GC_11, amp[2478]); 
  FFV1_0(w[282], w[107], w[72], pars->GC_11, amp[2479]); 
  VVV1_0(w[4], w[72], w[252], pars->GC_10, amp[2480]); 
  FFV2_5_0(w[101], w[132], w[2], pars->GC_51, pars->GC_58, amp[2481]); 
  FFV1_0(w[54], w[5], w[174], pars->GC_11, amp[2482]); 
  FFV1_0(w[54], w[132], w[4], pars->GC_11, amp[2483]); 
  FFV1_0(w[108], w[245], w[105], pars->GC_11, amp[2484]); 
  FFV2_3_0(w[143], w[281], w[3], pars->GC_50, pars->GC_58, amp[2485]); 
  FFV1_0(w[143], w[245], w[4], pars->GC_11, amp[2486]); 
  FFV1_0(w[247], w[243], w[105], pars->GC_11, amp[2487]); 
  FFV1_0(w[247], w[281], w[75], pars->GC_11, amp[2488]); 
  FFV1_0(w[280], w[243], w[75], pars->GC_11, amp[2489]); 
  FFV1_0(w[254], w[107], w[105], pars->GC_11, amp[2490]); 
  FFV2_3_0(w[282], w[170], w[3], pars->GC_50, pars->GC_58, amp[2491]); 
  FFV1_0(w[254], w[170], w[4], pars->GC_11, amp[2492]); 
  FFV1_0(w[251], w[249], w[105], pars->GC_11, amp[2493]); 
  FFV1_0(w[282], w[249], w[75], pars->GC_11, amp[2494]); 
  FFV1_0(w[251], w[279], w[75], pars->GC_11, amp[2495]); 
  FFV1_0(w[108], w[256], w[105], pars->GC_11, amp[2496]); 
  FFV2_3_0(w[143], w[279], w[2], pars->GC_50, pars->GC_58, amp[2497]); 
  FFV1_0(w[143], w[256], w[4], pars->GC_11, amp[2498]); 
  FFV1_0(w[260], w[107], w[105], pars->GC_11, amp[2499]); 
  FFV2_3_0(w[280], w[170], w[2], pars->GC_50, pars->GC_58, amp[2500]); 
  FFV1_0(w[260], w[170], w[4], pars->GC_11, amp[2501]); 
  FFV1_0(w[0], w[102], w[255], pars->GC_11, amp[2502]); 
  FFV1_0(w[108], w[279], w[79], pars->GC_11, amp[2503]); 
  VVV1_0(w[4], w[79], w[255], pars->GC_10, amp[2504]); 
  FFV1_0(w[0], w[102], w[258], pars->GC_11, amp[2505]); 
  FFV1_0(w[280], w[107], w[79], pars->GC_11, amp[2506]); 
  VVV1_0(w[4], w[79], w[258], pars->GC_10, amp[2507]); 
  FFV2_5_0(w[158], w[102], w[3], pars->GC_51, pars->GC_58, amp[2508]); 
  FFV1_0(w[0], w[12], w[174], pars->GC_11, amp[2509]); 
  FFV1_0(w[158], w[12], w[4], pars->GC_11, amp[2510]); 
  FFV1_0(w[108], w[281], w[80], pars->GC_11, amp[2511]); 
  FFV1_0(w[0], w[96], w[244], pars->GC_11, amp[2512]); 
  VVV1_0(w[4], w[80], w[244], pars->GC_10, amp[2513]); 
  FFV1_0(w[282], w[107], w[80], pars->GC_11, amp[2514]); 
  FFV1_0(w[0], w[96], w[252], pars->GC_11, amp[2515]); 
  VVV1_0(w[4], w[80], w[252], pars->GC_10, amp[2516]); 
  FFV2_5_0(w[158], w[96], w[2], pars->GC_51, pars->GC_58, amp[2517]); 
  FFV1_0(w[0], w[34], w[174], pars->GC_11, amp[2518]); 
  FFV1_0(w[158], w[34], w[4], pars->GC_11, amp[2519]); 
  FFV1_0(w[1], w[183], w[184], pars->GC_11, amp[2520]); 
  FFV1_0(w[186], w[6], w[184], pars->GC_11, amp[2521]); 
  FFV1_0(w[180], w[179], w[17], pars->GC_11, amp[2522]); 
  FFV2_3_0(w[18], w[179], w[3], pars->GC_50, pars->GC_58, amp[2523]); 
  FFV1_0(w[180], w[5], w[188], pars->GC_11, amp[2524]); 
  FFV1_0(w[1], w[189], w[21], pars->GC_11, amp[2525]); 
  FFV1_0(w[1], w[187], w[192], pars->GC_11, amp[2526]); 
  FFV1_0(w[186], w[187], w[21], pars->GC_11, amp[2527]); 
  FFV1_0(w[180], w[5], w[195], pars->GC_11, amp[2528]); 
  FFV1_0(w[196], w[6], w[21], pars->GC_11, amp[2529]); 
  FFV1_0(w[193], w[6], w[192], pars->GC_11, amp[2530]); 
  FFV1_0(w[193], w[183], w[21], pars->GC_11, amp[2531]); 
  FFV1_0(w[197], w[190], w[17], pars->GC_11, amp[2532]); 
  FFV2_3_0(w[18], w[190], w[2], pars->GC_50, pars->GC_58, amp[2533]); 
  FFV1_0(w[197], w[5], w[200], pars->GC_11, amp[2534]); 
  FFV1_0(w[1], w[201], w[21], pars->GC_11, amp[2535]); 
  FFV1_0(w[197], w[5], w[203], pars->GC_11, amp[2536]); 
  FFV1_0(w[204], w[6], w[21], pars->GC_11, amp[2537]); 
  FFV2_3_0(w[197], w[41], w[3], pars->GC_50, pars->GC_58, amp[2538]); 
  FFV2_3_0(w[180], w[41], w[2], pars->GC_50, pars->GC_58, amp[2539]); 
  FFV1_0(w[1], w[183], w[209], pars->GC_11, amp[2540]); 
  FFV1_0(w[186], w[6], w[209], pars->GC_11, amp[2541]); 
  FFV1_0(w[205], w[206], w[17], pars->GC_11, amp[2542]); 
  FFV2_3_0(w[205], w[50], w[3], pars->GC_50, pars->GC_58, amp[2543]); 
  FFV1_0(w[1], w[187], w[215], pars->GC_11, amp[2544]); 
  FFV1_0(w[193], w[6], w[215], pars->GC_11, amp[2545]); 
  FFV1_0(w[211], w[212], w[17], pars->GC_11, amp[2546]); 
  FFV2_3_0(w[211], w[50], w[2], pars->GC_50, pars->GC_58, amp[2547]); 
  FFV1_0(w[1], w[189], w[60], pars->GC_11, amp[2548]); 
  FFV1_0(w[0], w[206], w[188], pars->GC_11, amp[2549]); 
  FFV1_0(w[186], w[187], w[60], pars->GC_11, amp[2550]); 
  FFV1_0(w[196], w[6], w[60], pars->GC_11, amp[2551]); 
  FFV1_0(w[0], w[206], w[195], pars->GC_11, amp[2552]); 
  FFV1_0(w[193], w[183], w[60], pars->GC_11, amp[2553]); 
  FFV1_0(w[1], w[201], w[60], pars->GC_11, amp[2554]); 
  FFV1_0(w[0], w[212], w[200], pars->GC_11, amp[2555]); 
  FFV1_0(w[204], w[6], w[60], pars->GC_11, amp[2556]); 
  FFV1_0(w[0], w[212], w[203], pars->GC_11, amp[2557]); 
  FFV2_3_0(w[63], w[212], w[3], pars->GC_50, pars->GC_58, amp[2558]); 
  FFV2_3_0(w[63], w[206], w[2], pars->GC_50, pars->GC_58, amp[2559]); 
  FFV1_0(w[1], w[219], w[220], pars->GC_11, amp[2560]); 
  FFV1_0(w[208], w[5], w[67], pars->GC_11, amp[2561]); 
  FFV1_0(w[205], w[190], w[67], pars->GC_11, amp[2562]); 
  FFV1_0(w[205], w[5], w[222], pars->GC_11, amp[2563]); 
  FFV1_0(w[1], w[223], w[224], pars->GC_11, amp[2564]); 
  FFV1_0(w[214], w[5], w[67], pars->GC_11, amp[2565]); 
  FFV1_0(w[211], w[179], w[67], pars->GC_11, amp[2566]); 
  FFV1_0(w[211], w[5], w[226], pars->GC_11, amp[2567]); 
  FFV2_3_0(w[76], w[223], w[3], pars->GC_50, pars->GC_58, amp[2568]); 
  FFV2_3_0(w[76], w[219], w[2], pars->GC_50, pars->GC_58, amp[2569]); 
  FFV2_3_0(w[193], w[77], w[3], pars->GC_50, pars->GC_58, amp[2570]); 
  FFV1_0(w[193], w[219], w[75], pars->GC_11, amp[2571]); 
  FFV2_3_0(w[186], w[77], w[2], pars->GC_50, pars->GC_58, amp[2572]); 
  FFV1_0(w[186], w[223], w[75], pars->GC_11, amp[2573]); 
  FFV1_0(w[1], w[219], w[227], pars->GC_11, amp[2574]); 
  FFV1_0(w[0], w[182], w[67], pars->GC_11, amp[2575]); 
  FFV1_0(w[0], w[179], w[222], pars->GC_11, amp[2576]); 
  FFV1_0(w[0], w[190], w[226], pars->GC_11, amp[2577]); 
  FFV1_0(w[1], w[223], w[228], pars->GC_11, amp[2578]); 
  FFV1_0(w[0], w[199], w[67], pars->GC_11, amp[2579]); 
  FFV1_0(w[229], w[6], w[220], pars->GC_11, amp[2580]); 
  FFV1_0(w[208], w[5], w[85], pars->GC_11, amp[2581]); 
  FFV1_0(w[205], w[190], w[85], pars->GC_11, amp[2582]); 
  FFV1_0(w[205], w[5], w[231], pars->GC_11, amp[2583]); 
  FFV1_0(w[232], w[6], w[224], pars->GC_11, amp[2584]); 
  FFV1_0(w[214], w[5], w[85], pars->GC_11, amp[2585]); 
  FFV1_0(w[211], w[179], w[85], pars->GC_11, amp[2586]); 
  FFV1_0(w[211], w[5], w[234], pars->GC_11, amp[2587]); 
  FFV2_3_0(w[232], w[91], w[3], pars->GC_50, pars->GC_58, amp[2588]); 
  FFV2_3_0(w[229], w[91], w[2], pars->GC_50, pars->GC_58, amp[2589]); 
  FFV2_3_0(w[92], w[187], w[3], pars->GC_50, pars->GC_58, amp[2590]); 
  FFV1_0(w[229], w[187], w[75], pars->GC_11, amp[2591]); 
  FFV2_3_0(w[92], w[183], w[2], pars->GC_50, pars->GC_58, amp[2592]); 
  FFV1_0(w[232], w[183], w[75], pars->GC_11, amp[2593]); 
  FFV1_0(w[229], w[6], w[227], pars->GC_11, amp[2594]); 
  FFV1_0(w[0], w[182], w[85], pars->GC_11, amp[2595]); 
  FFV1_0(w[0], w[179], w[231], pars->GC_11, amp[2596]); 
  FFV1_0(w[0], w[190], w[234], pars->GC_11, amp[2597]); 
  FFV1_0(w[232], w[6], w[228], pars->GC_11, amp[2598]); 
  FFV1_0(w[0], w[199], w[85], pars->GC_11, amp[2599]); 
  FFV1_0(w[235], w[190], w[17], pars->GC_11, amp[2600]); 
  FFV1_0(w[205], w[236], w[17], pars->GC_11, amp[2601]); 
  FFV1_0(w[205], w[190], w[97], pars->GC_11, amp[2602]); 
  FFV1_0(w[235], w[5], w[200], pars->GC_11, amp[2603]); 
  FFV1_0(w[1], w[237], w[220], pars->GC_11, amp[2604]); 
  VVV1_0(w[4], w[220], w[200], pars->GC_10, amp[2605]); 
  FFV1_0(w[235], w[5], w[203], pars->GC_11, amp[2606]); 
  FFV1_0(w[238], w[6], w[220], pars->GC_11, amp[2607]); 
  VVV1_0(w[4], w[220], w[203], pars->GC_10, amp[2608]); 
  FFV2_3_0(w[235], w[41], w[3], pars->GC_50, pars->GC_58, amp[2609]); 
  FFV1_0(w[208], w[5], w[97], pars->GC_11, amp[2610]); 
  FFV1_0(w[208], w[41], w[4], pars->GC_11, amp[2611]); 
  FFV1_0(w[239], w[179], w[17], pars->GC_11, amp[2612]); 
  FFV1_0(w[211], w[240], w[17], pars->GC_11, amp[2613]); 
  FFV1_0(w[211], w[179], w[97], pars->GC_11, amp[2614]); 
  FFV1_0(w[239], w[5], w[188], pars->GC_11, amp[2615]); 
  FFV1_0(w[1], w[241], w[224], pars->GC_11, amp[2616]); 
  VVV1_0(w[4], w[224], w[188], pars->GC_10, amp[2617]); 
  FFV1_0(w[239], w[5], w[195], pars->GC_11, amp[2618]); 
  FFV1_0(w[242], w[6], w[224], pars->GC_11, amp[2619]); 
  VVV1_0(w[4], w[224], w[195], pars->GC_10, amp[2620]); 
  FFV2_3_0(w[239], w[41], w[2], pars->GC_50, pars->GC_58, amp[2621]); 
  FFV1_0(w[214], w[5], w[97], pars->GC_11, amp[2622]); 
  FFV1_0(w[214], w[41], w[4], pars->GC_11, amp[2623]); 
  FFV1_0(w[1], w[189], w[105], pars->GC_11, amp[2624]); 
  FFV2_3_0(w[76], w[241], w[3], pars->GC_50, pars->GC_58, amp[2625]); 
  FFV1_0(w[76], w[189], w[4], pars->GC_11, amp[2626]); 
  FFV1_0(w[186], w[187], w[105], pars->GC_11, amp[2627]); 
  FFV1_0(w[186], w[241], w[75], pars->GC_11, amp[2628]); 
  FFV1_0(w[238], w[187], w[75], pars->GC_11, amp[2629]); 
  FFV1_0(w[196], w[6], w[105], pars->GC_11, amp[2630]); 
  FFV2_3_0(w[242], w[91], w[3], pars->GC_50, pars->GC_58, amp[2631]); 
  FFV1_0(w[196], w[91], w[4], pars->GC_11, amp[2632]); 
  FFV1_0(w[193], w[183], w[105], pars->GC_11, amp[2633]); 
  FFV1_0(w[242], w[183], w[75], pars->GC_11, amp[2634]); 
  FFV1_0(w[193], w[237], w[75], pars->GC_11, amp[2635]); 
  FFV1_0(w[1], w[201], w[105], pars->GC_11, amp[2636]); 
  FFV2_3_0(w[76], w[237], w[2], pars->GC_50, pars->GC_58, amp[2637]); 
  FFV1_0(w[76], w[201], w[4], pars->GC_11, amp[2638]); 
  FFV1_0(w[204], w[6], w[105], pars->GC_11, amp[2639]); 
  FFV2_3_0(w[238], w[91], w[2], pars->GC_50, pars->GC_58, amp[2640]); 
  FFV1_0(w[204], w[91], w[4], pars->GC_11, amp[2641]); 
  FFV1_0(w[0], w[240], w[200], pars->GC_11, amp[2642]); 
  FFV1_0(w[1], w[237], w[227], pars->GC_11, amp[2643]); 
  VVV1_0(w[4], w[227], w[200], pars->GC_10, amp[2644]); 
  FFV1_0(w[0], w[240], w[203], pars->GC_11, amp[2645]); 
  FFV1_0(w[238], w[6], w[227], pars->GC_11, amp[2646]); 
  VVV1_0(w[4], w[227], w[203], pars->GC_10, amp[2647]); 
  FFV2_3_0(w[63], w[240], w[3], pars->GC_50, pars->GC_58, amp[2648]); 
  FFV1_0(w[0], w[182], w[97], pars->GC_11, amp[2649]); 
  FFV1_0(w[63], w[182], w[4], pars->GC_11, amp[2650]); 
  FFV1_0(w[1], w[241], w[228], pars->GC_11, amp[2651]); 
  FFV1_0(w[0], w[236], w[188], pars->GC_11, amp[2652]); 
  VVV1_0(w[4], w[228], w[188], pars->GC_10, amp[2653]); 
  FFV1_0(w[242], w[6], w[228], pars->GC_11, amp[2654]); 
  FFV1_0(w[0], w[236], w[195], pars->GC_11, amp[2655]); 
  VVV1_0(w[4], w[228], w[195], pars->GC_10, amp[2656]); 
  FFV2_3_0(w[63], w[236], w[2], pars->GC_50, pars->GC_58, amp[2657]); 
  FFV1_0(w[0], w[199], w[97], pars->GC_11, amp[2658]); 
  FFV1_0(w[63], w[199], w[4], pars->GC_11, amp[2659]); 
  FFV1_0(w[0], w[190], w[166], pars->GC_11, amp[2660]); 
  FFV1_0(w[211], w[5], w[166], pars->GC_11, amp[2661]); 
  FFV1_0(w[160], w[109], w[75], pars->GC_11, amp[2662]); 
  FFV2_5_0(w[171], w[109], w[3], pars->GC_51, pars->GC_58, amp[2663]); 
  FFV1_0(w[160], w[107], w[227], pars->GC_11, amp[2664]); 
  FFV1_0(w[0], w[182], w[161], pars->GC_11, amp[2665]); 
  FFV1_0(w[0], w[179], w[163], pars->GC_11, amp[2666]); 
  FFV1_0(w[211], w[179], w[161], pars->GC_11, amp[2667]); 
  FFV1_0(w[160], w[107], w[220], pars->GC_11, amp[2668]); 
  FFV1_0(w[208], w[5], w[161], pars->GC_11, amp[2669]); 
  FFV1_0(w[205], w[5], w[163], pars->GC_11, amp[2670]); 
  FFV1_0(w[205], w[190], w[161], pars->GC_11, amp[2671]); 
  FFV1_0(w[165], w[118], w[75], pars->GC_11, amp[2672]); 
  FFV2_5_0(w[171], w[118], w[2], pars->GC_51, pars->GC_58, amp[2673]); 
  FFV1_0(w[165], w[107], w[228], pars->GC_11, amp[2674]); 
  FFV1_0(w[0], w[199], w[161], pars->GC_11, amp[2675]); 
  FFV1_0(w[165], w[107], w[224], pars->GC_11, amp[2676]); 
  FFV1_0(w[214], w[5], w[161], pars->GC_11, amp[2677]); 
  FFV2_5_0(w[165], w[170], w[3], pars->GC_51, pars->GC_58, amp[2678]); 
  FFV2_5_0(w[160], w[170], w[2], pars->GC_51, pars->GC_58, amp[2679]); 
  FFV1_0(w[0], w[190], w[142], pars->GC_11, amp[2680]); 
  FFV1_0(w[211], w[5], w[142], pars->GC_11, amp[2681]); 
  FFV1_0(w[122], w[135], w[75], pars->GC_11, amp[2682]); 
  FFV2_5_0(w[122], w[144], w[3], pars->GC_51, pars->GC_58, amp[2683]); 
  FFV1_0(w[0], w[179], w[138], pars->GC_11, amp[2684]); 
  FFV1_0(w[205], w[5], w[138], pars->GC_11, amp[2685]); 
  FFV1_0(w[113], w[140], w[75], pars->GC_11, amp[2686]); 
  FFV2_5_0(w[113], w[144], w[2], pars->GC_51, pars->GC_58, amp[2687]); 
  FFV1_0(w[0], w[182], w[136], pars->GC_11, amp[2688]); 
  FFV1_0(w[108], w[135], w[227], pars->GC_11, amp[2689]); 
  FFV1_0(w[211], w[179], w[136], pars->GC_11, amp[2690]); 
  FFV1_0(w[208], w[5], w[136], pars->GC_11, amp[2691]); 
  FFV1_0(w[108], w[135], w[220], pars->GC_11, amp[2692]); 
  FFV1_0(w[205], w[190], w[136], pars->GC_11, amp[2693]); 
  FFV1_0(w[0], w[199], w[136], pars->GC_11, amp[2694]); 
  FFV1_0(w[108], w[140], w[228], pars->GC_11, amp[2695]); 
  FFV1_0(w[214], w[5], w[136], pars->GC_11, amp[2696]); 
  FFV1_0(w[108], w[140], w[224], pars->GC_11, amp[2697]); 
  FFV2_5_0(w[143], w[140], w[3], pars->GC_51, pars->GC_58, amp[2698]); 
  FFV2_5_0(w[143], w[135], w[2], pars->GC_51, pars->GC_58, amp[2699]); 
  FFV1_0(w[0], w[206], w[123], pars->GC_11, amp[2700]); 
  FFV1_0(w[125], w[107], w[60], pars->GC_11, amp[2701]); 
  FFV1_0(w[122], w[118], w[60], pars->GC_11, amp[2702]); 
  FFV1_0(w[122], w[107], w[215], pars->GC_11, amp[2703]); 
  FFV1_0(w[0], w[212], w[129], pars->GC_11, amp[2704]); 
  FFV1_0(w[131], w[107], w[60], pars->GC_11, amp[2705]); 
  FFV1_0(w[113], w[109], w[60], pars->GC_11, amp[2706]); 
  FFV1_0(w[113], w[107], w[209], pars->GC_11, amp[2707]); 
  FFV2_3_0(w[158], w[212], w[3], pars->GC_50, pars->GC_58, amp[2708]); 
  FFV2_3_0(w[158], w[206], w[2], pars->GC_50, pars->GC_58, amp[2709]); 
  FFV2_3_0(w[205], w[150], w[3], pars->GC_50, pars->GC_58, amp[2710]); 
  FFV1_0(w[205], w[206], w[120], pars->GC_11, amp[2711]); 
  FFV2_3_0(w[211], w[150], w[2], pars->GC_50, pars->GC_58, amp[2712]); 
  FFV1_0(w[211], w[212], w[120], pars->GC_11, amp[2713]); 
  FFV1_0(w[0], w[206], w[110], pars->GC_11, amp[2714]); 
  FFV1_0(w[108], w[111], w[60], pars->GC_11, amp[2715]); 
  FFV1_0(w[108], w[109], w[215], pars->GC_11, amp[2716]); 
  FFV1_0(w[108], w[118], w[209], pars->GC_11, amp[2717]); 
  FFV1_0(w[0], w[212], w[126], pars->GC_11, amp[2718]); 
  FFV1_0(w[108], w[127], w[60], pars->GC_11, amp[2719]); 
  FFV1_0(w[180], w[5], w[123], pars->GC_11, amp[2720]); 
  FFV1_0(w[125], w[107], w[21], pars->GC_11, amp[2721]); 
  FFV1_0(w[122], w[118], w[21], pars->GC_11, amp[2722]); 
  FFV1_0(w[122], w[107], w[192], pars->GC_11, amp[2723]); 
  FFV1_0(w[197], w[5], w[129], pars->GC_11, amp[2724]); 
  FFV1_0(w[131], w[107], w[21], pars->GC_11, amp[2725]); 
  FFV1_0(w[113], w[109], w[21], pars->GC_11, amp[2726]); 
  FFV1_0(w[113], w[107], w[184], pars->GC_11, amp[2727]); 
  FFV2_3_0(w[197], w[132], w[3], pars->GC_50, pars->GC_58, amp[2728]); 
  FFV2_3_0(w[180], w[132], w[2], pars->GC_50, pars->GC_58, amp[2729]); 
  FFV2_3_0(w[121], w[179], w[3], pars->GC_50, pars->GC_58, amp[2730]); 
  FFV1_0(w[180], w[179], w[120], pars->GC_11, amp[2731]); 
  FFV2_3_0(w[121], w[190], w[2], pars->GC_50, pars->GC_58, amp[2732]); 
  FFV1_0(w[197], w[190], w[120], pars->GC_11, amp[2733]); 
  FFV1_0(w[180], w[5], w[110], pars->GC_11, amp[2734]); 
  FFV1_0(w[108], w[111], w[21], pars->GC_11, amp[2735]); 
  FFV1_0(w[108], w[109], w[192], pars->GC_11, amp[2736]); 
  FFV1_0(w[108], w[118], w[184], pars->GC_11, amp[2737]); 
  FFV1_0(w[197], w[5], w[126], pars->GC_11, amp[2738]); 
  FFV1_0(w[108], w[127], w[21], pars->GC_11, amp[2739]); 
  FFV1_0(w[177], w[118], w[75], pars->GC_11, amp[2740]); 
  FFV1_0(w[122], w[172], w[75], pars->GC_11, amp[2741]); 
  FFV1_0(w[122], w[118], w[105], pars->GC_11, amp[2742]); 
  FFV1_0(w[177], w[107], w[228], pars->GC_11, amp[2743]); 
  FFV1_0(w[0], w[236], w[123], pars->GC_11, amp[2744]); 
  VVV1_0(w[4], w[123], w[228], pars->GC_10, amp[2745]); 
  FFV1_0(w[177], w[107], w[224], pars->GC_11, amp[2746]); 
  FFV1_0(w[239], w[5], w[123], pars->GC_11, amp[2747]); 
  VVV1_0(w[4], w[123], w[224], pars->GC_10, amp[2748]); 
  FFV2_5_0(w[177], w[170], w[3], pars->GC_51, pars->GC_58, amp[2749]); 
  FFV1_0(w[125], w[107], w[105], pars->GC_11, amp[2750]); 
  FFV1_0(w[125], w[170], w[4], pars->GC_11, amp[2751]); 
  FFV1_0(w[175], w[109], w[75], pars->GC_11, amp[2752]); 
  FFV1_0(w[113], w[176], w[75], pars->GC_11, amp[2753]); 
  FFV1_0(w[113], w[109], w[105], pars->GC_11, amp[2754]); 
  FFV1_0(w[175], w[107], w[227], pars->GC_11, amp[2755]); 
  FFV1_0(w[0], w[240], w[129], pars->GC_11, amp[2756]); 
  VVV1_0(w[4], w[129], w[227], pars->GC_10, amp[2757]); 
  FFV1_0(w[175], w[107], w[220], pars->GC_11, amp[2758]); 
  FFV1_0(w[235], w[5], w[129], pars->GC_11, amp[2759]); 
  VVV1_0(w[4], w[129], w[220], pars->GC_10, amp[2760]); 
  FFV2_5_0(w[175], w[170], w[2], pars->GC_51, pars->GC_58, amp[2761]); 
  FFV1_0(w[131], w[107], w[105], pars->GC_11, amp[2762]); 
  FFV1_0(w[131], w[170], w[4], pars->GC_11, amp[2763]); 
  FFV1_0(w[0], w[182], w[174], pars->GC_11, amp[2764]); 
  FFV2_3_0(w[158], w[240], w[3], pars->GC_50, pars->GC_58, amp[2765]); 
  FFV1_0(w[158], w[182], w[4], pars->GC_11, amp[2766]); 
  FFV1_0(w[211], w[179], w[174], pars->GC_11, amp[2767]); 
  FFV1_0(w[211], w[240], w[120], pars->GC_11, amp[2768]); 
  FFV1_0(w[239], w[179], w[120], pars->GC_11, amp[2769]); 
  FFV1_0(w[208], w[5], w[174], pars->GC_11, amp[2770]); 
  FFV2_3_0(w[235], w[132], w[3], pars->GC_50, pars->GC_58, amp[2771]); 
  FFV1_0(w[208], w[132], w[4], pars->GC_11, amp[2772]); 
  FFV1_0(w[205], w[190], w[174], pars->GC_11, amp[2773]); 
  FFV1_0(w[235], w[190], w[120], pars->GC_11, amp[2774]); 
  FFV1_0(w[205], w[236], w[120], pars->GC_11, amp[2775]); 
  FFV1_0(w[0], w[199], w[174], pars->GC_11, amp[2776]); 
  FFV2_3_0(w[158], w[236], w[2], pars->GC_50, pars->GC_58, amp[2777]); 
  FFV1_0(w[158], w[199], w[4], pars->GC_11, amp[2778]); 
  FFV1_0(w[214], w[5], w[174], pars->GC_11, amp[2779]); 
  FFV2_3_0(w[239], w[132], w[2], pars->GC_50, pars->GC_58, amp[2780]); 
  FFV1_0(w[214], w[132], w[4], pars->GC_11, amp[2781]); 
  FFV1_0(w[108], w[176], w[228], pars->GC_11, amp[2782]); 
  FFV1_0(w[0], w[236], w[110], pars->GC_11, amp[2783]); 
  VVV1_0(w[4], w[110], w[228], pars->GC_10, amp[2784]); 
  FFV1_0(w[108], w[176], w[224], pars->GC_11, amp[2785]); 
  FFV1_0(w[239], w[5], w[110], pars->GC_11, amp[2786]); 
  VVV1_0(w[4], w[110], w[224], pars->GC_10, amp[2787]); 
  FFV2_5_0(w[143], w[176], w[3], pars->GC_51, pars->GC_58, amp[2788]); 
  FFV1_0(w[108], w[111], w[105], pars->GC_11, amp[2789]); 
  FFV1_0(w[143], w[111], w[4], pars->GC_11, amp[2790]); 
  FFV1_0(w[0], w[240], w[126], pars->GC_11, amp[2791]); 
  FFV1_0(w[108], w[172], w[227], pars->GC_11, amp[2792]); 
  VVV1_0(w[4], w[126], w[227], pars->GC_10, amp[2793]); 
  FFV1_0(w[235], w[5], w[126], pars->GC_11, amp[2794]); 
  FFV1_0(w[108], w[172], w[220], pars->GC_11, amp[2795]); 
  VVV1_0(w[4], w[126], w[220], pars->GC_10, amp[2796]); 
  FFV2_5_0(w[143], w[172], w[2], pars->GC_51, pars->GC_58, amp[2797]); 
  FFV1_0(w[108], w[127], w[105], pars->GC_11, amp[2798]); 
  FFV1_0(w[143], w[127], w[4], pars->GC_11, amp[2799]); 
  FFV1_0(w[0], w[249], w[167], pars->GC_11, amp[2800]); 
  FFV1_0(w[211], w[107], w[167], pars->GC_11, amp[2801]); 
  FFV1_0(w[160], w[8], w[153], pars->GC_11, amp[2802]); 
  FFV2_5_0(w[169], w[8], w[3], pars->GC_51, pars->GC_58, amp[2803]); 
  FFV1_0(w[160], w[5], w[271], pars->GC_11, amp[2804]); 
  FFV1_0(w[0], w[245], w[162], pars->GC_11, amp[2805]); 
  FFV1_0(w[0], w[243], w[164], pars->GC_11, amp[2806]); 
  FFV1_0(w[211], w[243], w[162], pars->GC_11, amp[2807]); 
  FFV1_0(w[160], w[5], w[267], pars->GC_11, amp[2808]); 
  FFV1_0(w[208], w[107], w[162], pars->GC_11, amp[2809]); 
  FFV1_0(w[205], w[107], w[164], pars->GC_11, amp[2810]); 
  FFV1_0(w[205], w[249], w[162], pars->GC_11, amp[2811]); 
  FFV1_0(w[165], w[23], w[153], pars->GC_11, amp[2812]); 
  FFV2_5_0(w[169], w[23], w[2], pars->GC_51, pars->GC_58, amp[2813]); 
  FFV1_0(w[165], w[5], w[272], pars->GC_11, amp[2814]); 
  FFV1_0(w[0], w[256], w[162], pars->GC_11, amp[2815]); 
  FFV1_0(w[165], w[5], w[269], pars->GC_11, amp[2816]); 
  FFV1_0(w[214], w[107], w[162], pars->GC_11, amp[2817]); 
  FFV2_5_0(w[165], w[168], w[3], pars->GC_51, pars->GC_58, amp[2818]); 
  FFV2_5_0(w[160], w[168], w[2], pars->GC_51, pars->GC_58, amp[2819]); 
  FFV1_0(w[0], w[249], w[152], pars->GC_11, amp[2820]); 
  FFV1_0(w[211], w[107], w[152], pars->GC_11, amp[2821]); 
  FFV1_0(w[122], w[44], w[153], pars->GC_11, amp[2822]); 
  FFV2_5_0(w[122], w[155], w[3], pars->GC_51, pars->GC_58, amp[2823]); 
  FFV1_0(w[0], w[243], w[149], pars->GC_11, amp[2824]); 
  FFV1_0(w[205], w[107], w[149], pars->GC_11, amp[2825]); 
  FFV1_0(w[113], w[52], w[153], pars->GC_11, amp[2826]); 
  FFV2_5_0(w[113], w[155], w[2], pars->GC_51, pars->GC_58, amp[2827]); 
  FFV1_0(w[0], w[245], w[148], pars->GC_11, amp[2828]); 
  FFV1_0(w[108], w[44], w[271], pars->GC_11, amp[2829]); 
  FFV1_0(w[211], w[243], w[148], pars->GC_11, amp[2830]); 
  FFV1_0(w[208], w[107], w[148], pars->GC_11, amp[2831]); 
  FFV1_0(w[108], w[44], w[267], pars->GC_11, amp[2832]); 
  FFV1_0(w[205], w[249], w[148], pars->GC_11, amp[2833]); 
  FFV1_0(w[0], w[256], w[148], pars->GC_11, amp[2834]); 
  FFV1_0(w[108], w[52], w[272], pars->GC_11, amp[2835]); 
  FFV1_0(w[214], w[107], w[148], pars->GC_11, amp[2836]); 
  FFV1_0(w[108], w[52], w[269], pars->GC_11, amp[2837]); 
  FFV2_5_0(w[154], w[52], w[3], pars->GC_51, pars->GC_58, amp[2838]); 
  FFV2_5_0(w[154], w[44], w[2], pars->GC_51, pars->GC_58, amp[2839]); 
  FFV1_0(w[0], w[261], w[124], pars->GC_11, amp[2840]); 
  FFV1_0(w[125], w[5], w[145], pars->GC_11, amp[2841]); 
  FFV1_0(w[122], w[23], w[145], pars->GC_11, amp[2842]); 
  FFV1_0(w[122], w[5], w[265], pars->GC_11, amp[2843]); 
  FFV1_0(w[0], w[264], w[130], pars->GC_11, amp[2844]); 
  FFV1_0(w[131], w[5], w[145], pars->GC_11, amp[2845]); 
  FFV1_0(w[113], w[8], w[145], pars->GC_11, amp[2846]); 
  FFV1_0(w[113], w[5], w[262], pars->GC_11, amp[2847]); 
  FFV2_3_0(w[146], w[264], w[3], pars->GC_50, pars->GC_58, amp[2848]); 
  FFV2_3_0(w[146], w[261], w[2], pars->GC_50, pars->GC_58, amp[2849]); 
  FFV2_3_0(w[205], w[139], w[3], pars->GC_50, pars->GC_58, amp[2850]); 
  FFV1_0(w[205], w[261], w[114], pars->GC_11, amp[2851]); 
  FFV2_3_0(w[211], w[139], w[2], pars->GC_50, pars->GC_58, amp[2852]); 
  FFV1_0(w[211], w[264], w[114], pars->GC_11, amp[2853]); 
  FFV1_0(w[0], w[261], w[116], pars->GC_11, amp[2854]); 
  FFV1_0(w[108], w[12], w[145], pars->GC_11, amp[2855]); 
  FFV1_0(w[108], w[8], w[265], pars->GC_11, amp[2856]); 
  FFV1_0(w[108], w[23], w[262], pars->GC_11, amp[2857]); 
  FFV1_0(w[0], w[264], w[128], pars->GC_11, amp[2858]); 
  FFV1_0(w[108], w[34], w[145], pars->GC_11, amp[2859]); 
  FFV1_0(w[180], w[107], w[124], pars->GC_11, amp[2860]); 
  FFV1_0(w[125], w[5], w[117], pars->GC_11, amp[2861]); 
  FFV1_0(w[122], w[23], w[117], pars->GC_11, amp[2862]); 
  FFV1_0(w[122], w[5], w[250], pars->GC_11, amp[2863]); 
  FFV1_0(w[197], w[107], w[130], pars->GC_11, amp[2864]); 
  FFV1_0(w[131], w[5], w[117], pars->GC_11, amp[2865]); 
  FFV1_0(w[113], w[8], w[117], pars->GC_11, amp[2866]); 
  FFV1_0(w[113], w[5], w[246], pars->GC_11, amp[2867]); 
  FFV2_3_0(w[197], w[133], w[3], pars->GC_50, pars->GC_58, amp[2868]); 
  FFV2_3_0(w[180], w[133], w[2], pars->GC_50, pars->GC_58, amp[2869]); 
  FFV2_3_0(w[115], w[243], w[3], pars->GC_50, pars->GC_58, amp[2870]); 
  FFV1_0(w[180], w[243], w[114], pars->GC_11, amp[2871]); 
  FFV2_3_0(w[115], w[249], w[2], pars->GC_50, pars->GC_58, amp[2872]); 
  FFV1_0(w[197], w[249], w[114], pars->GC_11, amp[2873]); 
  FFV1_0(w[180], w[107], w[116], pars->GC_11, amp[2874]); 
  FFV1_0(w[108], w[12], w[117], pars->GC_11, amp[2875]); 
  FFV1_0(w[108], w[8], w[250], pars->GC_11, amp[2876]); 
  FFV1_0(w[108], w[23], w[246], pars->GC_11, amp[2877]); 
  FFV1_0(w[197], w[107], w[128], pars->GC_11, amp[2878]); 
  FFV1_0(w[108], w[34], w[117], pars->GC_11, amp[2879]); 
  FFV1_0(w[177], w[23], w[153], pars->GC_11, amp[2880]); 
  FFV1_0(w[122], w[96], w[153], pars->GC_11, amp[2881]); 
  FFV1_0(w[122], w[23], w[178], pars->GC_11, amp[2882]); 
  FFV1_0(w[177], w[5], w[272], pars->GC_11, amp[2883]); 
  FFV1_0(w[0], w[279], w[124], pars->GC_11, amp[2884]); 
  VVV1_0(w[4], w[124], w[272], pars->GC_10, amp[2885]); 
  FFV1_0(w[177], w[5], w[269], pars->GC_11, amp[2886]); 
  FFV1_0(w[239], w[107], w[124], pars->GC_11, amp[2887]); 
  VVV1_0(w[4], w[124], w[269], pars->GC_10, amp[2888]); 
  FFV2_5_0(w[177], w[168], w[3], pars->GC_51, pars->GC_58, amp[2889]); 
  FFV1_0(w[125], w[5], w[178], pars->GC_11, amp[2890]); 
  FFV1_0(w[125], w[168], w[4], pars->GC_11, amp[2891]); 
  FFV1_0(w[175], w[8], w[153], pars->GC_11, amp[2892]); 
  FFV1_0(w[113], w[102], w[153], pars->GC_11, amp[2893]); 
  FFV1_0(w[113], w[8], w[178], pars->GC_11, amp[2894]); 
  FFV1_0(w[175], w[5], w[271], pars->GC_11, amp[2895]); 
  FFV1_0(w[0], w[281], w[130], pars->GC_11, amp[2896]); 
  VVV1_0(w[4], w[130], w[271], pars->GC_10, amp[2897]); 
  FFV1_0(w[175], w[5], w[267], pars->GC_11, amp[2898]); 
  FFV1_0(w[235], w[107], w[130], pars->GC_11, amp[2899]); 
  VVV1_0(w[4], w[130], w[267], pars->GC_10, amp[2900]); 
  FFV2_5_0(w[175], w[168], w[2], pars->GC_51, pars->GC_58, amp[2901]); 
  FFV1_0(w[131], w[5], w[178], pars->GC_11, amp[2902]); 
  FFV1_0(w[131], w[168], w[4], pars->GC_11, amp[2903]); 
  FFV1_0(w[0], w[245], w[173], pars->GC_11, amp[2904]); 
  FFV2_3_0(w[146], w[281], w[3], pars->GC_50, pars->GC_58, amp[2905]); 
  FFV1_0(w[146], w[245], w[4], pars->GC_11, amp[2906]); 
  FFV1_0(w[211], w[243], w[173], pars->GC_11, amp[2907]); 
  FFV1_0(w[211], w[281], w[114], pars->GC_11, amp[2908]); 
  FFV1_0(w[239], w[243], w[114], pars->GC_11, amp[2909]); 
  FFV1_0(w[208], w[107], w[173], pars->GC_11, amp[2910]); 
  FFV2_3_0(w[235], w[133], w[3], pars->GC_50, pars->GC_58, amp[2911]); 
  FFV1_0(w[208], w[133], w[4], pars->GC_11, amp[2912]); 
  FFV1_0(w[205], w[249], w[173], pars->GC_11, amp[2913]); 
  FFV1_0(w[235], w[249], w[114], pars->GC_11, amp[2914]); 
  FFV1_0(w[205], w[279], w[114], pars->GC_11, amp[2915]); 
  FFV1_0(w[0], w[256], w[173], pars->GC_11, amp[2916]); 
  FFV2_3_0(w[146], w[279], w[2], pars->GC_50, pars->GC_58, amp[2917]); 
  FFV1_0(w[146], w[256], w[4], pars->GC_11, amp[2918]); 
  FFV1_0(w[214], w[107], w[173], pars->GC_11, amp[2919]); 
  FFV2_3_0(w[239], w[133], w[2], pars->GC_50, pars->GC_58, amp[2920]); 
  FFV1_0(w[214], w[133], w[4], pars->GC_11, amp[2921]); 
  FFV1_0(w[108], w[102], w[272], pars->GC_11, amp[2922]); 
  FFV1_0(w[0], w[279], w[116], pars->GC_11, amp[2923]); 
  VVV1_0(w[4], w[116], w[272], pars->GC_10, amp[2924]); 
  FFV1_0(w[108], w[102], w[269], pars->GC_11, amp[2925]); 
  FFV1_0(w[239], w[107], w[116], pars->GC_11, amp[2926]); 
  VVV1_0(w[4], w[116], w[269], pars->GC_10, amp[2927]); 
  FFV2_5_0(w[154], w[102], w[3], pars->GC_51, pars->GC_58, amp[2928]); 
  FFV1_0(w[108], w[12], w[178], pars->GC_11, amp[2929]); 
  FFV1_0(w[154], w[12], w[4], pars->GC_11, amp[2930]); 
  FFV1_0(w[0], w[281], w[128], pars->GC_11, amp[2931]); 
  FFV1_0(w[108], w[96], w[271], pars->GC_11, amp[2932]); 
  VVV1_0(w[4], w[128], w[271], pars->GC_10, amp[2933]); 
  FFV1_0(w[235], w[107], w[128], pars->GC_11, amp[2934]); 
  FFV1_0(w[108], w[96], w[267], pars->GC_11, amp[2935]); 
  VVV1_0(w[4], w[128], w[267], pars->GC_10, amp[2936]); 
  FFV2_5_0(w[154], w[96], w[2], pars->GC_51, pars->GC_58, amp[2937]); 
  FFV1_0(w[108], w[34], w[178], pars->GC_11, amp[2938]); 
  FFV1_0(w[154], w[34], w[4], pars->GC_11, amp[2939]); 
  FFV1_0(w[108], w[190], w[246], pars->GC_11, amp[2940]); 
  FFV1_0(w[247], w[5], w[246], pars->GC_11, amp[2941]); 
  FFV1_0(w[180], w[243], w[114], pars->GC_11, amp[2942]); 
  FFV2_3_0(w[115], w[243], w[3], pars->GC_50, pars->GC_58, amp[2943]); 
  FFV1_0(w[180], w[107], w[248], pars->GC_11, amp[2944]); 
  FFV1_0(w[108], w[182], w[117], pars->GC_11, amp[2945]); 
  FFV1_0(w[108], w[179], w[250], pars->GC_11, amp[2946]); 
  FFV1_0(w[247], w[179], w[117], pars->GC_11, amp[2947]); 
  FFV1_0(w[180], w[107], w[253], pars->GC_11, amp[2948]); 
  FFV1_0(w[254], w[5], w[117], pars->GC_11, amp[2949]); 
  FFV1_0(w[251], w[5], w[250], pars->GC_11, amp[2950]); 
  FFV1_0(w[251], w[190], w[117], pars->GC_11, amp[2951]); 
  FFV1_0(w[197], w[249], w[114], pars->GC_11, amp[2952]); 
  FFV2_3_0(w[115], w[249], w[2], pars->GC_50, pars->GC_58, amp[2953]); 
  FFV1_0(w[197], w[107], w[257], pars->GC_11, amp[2954]); 
  FFV1_0(w[108], w[199], w[117], pars->GC_11, amp[2955]); 
  FFV1_0(w[197], w[107], w[259], pars->GC_11, amp[2956]); 
  FFV1_0(w[260], w[5], w[117], pars->GC_11, amp[2957]); 
  FFV2_3_0(w[197], w[133], w[3], pars->GC_50, pars->GC_58, amp[2958]); 
  FFV2_3_0(w[180], w[133], w[2], pars->GC_50, pars->GC_58, amp[2959]); 
  FFV1_0(w[108], w[190], w[262], pars->GC_11, amp[2960]); 
  FFV1_0(w[247], w[5], w[262], pars->GC_11, amp[2961]); 
  FFV1_0(w[205], w[261], w[114], pars->GC_11, amp[2962]); 
  FFV2_3_0(w[205], w[139], w[3], pars->GC_50, pars->GC_58, amp[2963]); 
  FFV1_0(w[108], w[179], w[265], pars->GC_11, amp[2964]); 
  FFV1_0(w[251], w[5], w[265], pars->GC_11, amp[2965]); 
  FFV1_0(w[211], w[264], w[114], pars->GC_11, amp[2966]); 
  FFV2_3_0(w[211], w[139], w[2], pars->GC_50, pars->GC_58, amp[2967]); 
  FFV1_0(w[108], w[182], w[145], pars->GC_11, amp[2968]); 
  FFV1_0(w[0], w[261], w[248], pars->GC_11, amp[2969]); 
  FFV1_0(w[247], w[179], w[145], pars->GC_11, amp[2970]); 
  FFV1_0(w[254], w[5], w[145], pars->GC_11, amp[2971]); 
  FFV1_0(w[0], w[261], w[253], pars->GC_11, amp[2972]); 
  FFV1_0(w[251], w[190], w[145], pars->GC_11, amp[2973]); 
  FFV1_0(w[108], w[199], w[145], pars->GC_11, amp[2974]); 
  FFV1_0(w[0], w[264], w[257], pars->GC_11, amp[2975]); 
  FFV1_0(w[260], w[5], w[145], pars->GC_11, amp[2976]); 
  FFV1_0(w[0], w[264], w[259], pars->GC_11, amp[2977]); 
  FFV2_3_0(w[146], w[264], w[3], pars->GC_50, pars->GC_58, amp[2978]); 
  FFV2_3_0(w[146], w[261], w[2], pars->GC_50, pars->GC_58, amp[2979]); 
  FFV1_0(w[108], w[206], w[267], pars->GC_11, amp[2980]); 
  FFV1_0(w[208], w[107], w[148], pars->GC_11, amp[2981]); 
  FFV1_0(w[205], w[249], w[148], pars->GC_11, amp[2982]); 
  FFV1_0(w[205], w[107], w[268], pars->GC_11, amp[2983]); 
  FFV1_0(w[108], w[212], w[269], pars->GC_11, amp[2984]); 
  FFV1_0(w[214], w[107], w[148], pars->GC_11, amp[2985]); 
  FFV1_0(w[211], w[243], w[148], pars->GC_11, amp[2986]); 
  FFV1_0(w[211], w[107], w[270], pars->GC_11, amp[2987]); 
  FFV2_3_0(w[154], w[212], w[3], pars->GC_50, pars->GC_58, amp[2988]); 
  FFV2_3_0(w[154], w[206], w[2], pars->GC_50, pars->GC_58, amp[2989]); 
  FFV2_3_0(w[251], w[155], w[3], pars->GC_50, pars->GC_58, amp[2990]); 
  FFV1_0(w[251], w[206], w[153], pars->GC_11, amp[2991]); 
  FFV2_3_0(w[247], w[155], w[2], pars->GC_50, pars->GC_58, amp[2992]); 
  FFV1_0(w[247], w[212], w[153], pars->GC_11, amp[2993]); 
  FFV1_0(w[108], w[206], w[271], pars->GC_11, amp[2994]); 
  FFV1_0(w[0], w[245], w[148], pars->GC_11, amp[2995]); 
  FFV1_0(w[0], w[243], w[268], pars->GC_11, amp[2996]); 
  FFV1_0(w[0], w[249], w[270], pars->GC_11, amp[2997]); 
  FFV1_0(w[108], w[212], w[272], pars->GC_11, amp[2998]); 
  FFV1_0(w[0], w[256], w[148], pars->GC_11, amp[2999]); 
  FFV1_0(w[273], w[5], w[267], pars->GC_11, amp[3000]); 
  FFV1_0(w[208], w[107], w[162], pars->GC_11, amp[3001]); 
  FFV1_0(w[205], w[249], w[162], pars->GC_11, amp[3002]); 
  FFV1_0(w[205], w[107], w[275], pars->GC_11, amp[3003]); 
  FFV1_0(w[276], w[5], w[269], pars->GC_11, amp[3004]); 
  FFV1_0(w[214], w[107], w[162], pars->GC_11, amp[3005]); 
  FFV1_0(w[211], w[243], w[162], pars->GC_11, amp[3006]); 
  FFV1_0(w[211], w[107], w[278], pars->GC_11, amp[3007]); 
  FFV2_3_0(w[276], w[168], w[3], pars->GC_50, pars->GC_58, amp[3008]); 
  FFV2_3_0(w[273], w[168], w[2], pars->GC_50, pars->GC_58, amp[3009]); 
  FFV2_3_0(w[169], w[179], w[3], pars->GC_50, pars->GC_58, amp[3010]); 
  FFV1_0(w[273], w[179], w[153], pars->GC_11, amp[3011]); 
  FFV2_3_0(w[169], w[190], w[2], pars->GC_50, pars->GC_58, amp[3012]); 
  FFV1_0(w[276], w[190], w[153], pars->GC_11, amp[3013]); 
  FFV1_0(w[273], w[5], w[271], pars->GC_11, amp[3014]); 
  FFV1_0(w[0], w[245], w[162], pars->GC_11, amp[3015]); 
  FFV1_0(w[0], w[243], w[275], pars->GC_11, amp[3016]); 
  FFV1_0(w[0], w[249], w[278], pars->GC_11, amp[3017]); 
  FFV1_0(w[276], w[5], w[272], pars->GC_11, amp[3018]); 
  FFV1_0(w[0], w[256], w[162], pars->GC_11, amp[3019]); 
  FFV1_0(w[235], w[249], w[114], pars->GC_11, amp[3020]); 
  FFV1_0(w[205], w[279], w[114], pars->GC_11, amp[3021]); 
  FFV1_0(w[205], w[249], w[173], pars->GC_11, amp[3022]); 
  FFV1_0(w[235], w[107], w[257], pars->GC_11, amp[3023]); 
  FFV1_0(w[108], w[236], w[267], pars->GC_11, amp[3024]); 
  VVV1_0(w[4], w[267], w[257], pars->GC_10, amp[3025]); 
  FFV1_0(w[235], w[107], w[259], pars->GC_11, amp[3026]); 
  FFV1_0(w[280], w[5], w[267], pars->GC_11, amp[3027]); 
  VVV1_0(w[4], w[267], w[259], pars->GC_10, amp[3028]); 
  FFV2_3_0(w[235], w[133], w[3], pars->GC_50, pars->GC_58, amp[3029]); 
  FFV1_0(w[208], w[107], w[173], pars->GC_11, amp[3030]); 
  FFV1_0(w[208], w[133], w[4], pars->GC_11, amp[3031]); 
  FFV1_0(w[239], w[243], w[114], pars->GC_11, amp[3032]); 
  FFV1_0(w[211], w[281], w[114], pars->GC_11, amp[3033]); 
  FFV1_0(w[211], w[243], w[173], pars->GC_11, amp[3034]); 
  FFV1_0(w[239], w[107], w[248], pars->GC_11, amp[3035]); 
  FFV1_0(w[108], w[240], w[269], pars->GC_11, amp[3036]); 
  VVV1_0(w[4], w[269], w[248], pars->GC_10, amp[3037]); 
  FFV1_0(w[239], w[107], w[253], pars->GC_11, amp[3038]); 
  FFV1_0(w[282], w[5], w[269], pars->GC_11, amp[3039]); 
  VVV1_0(w[4], w[269], w[253], pars->GC_10, amp[3040]); 
  FFV2_3_0(w[239], w[133], w[2], pars->GC_50, pars->GC_58, amp[3041]); 
  FFV1_0(w[214], w[107], w[173], pars->GC_11, amp[3042]); 
  FFV1_0(w[214], w[133], w[4], pars->GC_11, amp[3043]); 
  FFV1_0(w[108], w[182], w[178], pars->GC_11, amp[3044]); 
  FFV2_3_0(w[154], w[240], w[3], pars->GC_50, pars->GC_58, amp[3045]); 
  FFV1_0(w[154], w[182], w[4], pars->GC_11, amp[3046]); 
  FFV1_0(w[247], w[179], w[178], pars->GC_11, amp[3047]); 
  FFV1_0(w[247], w[240], w[153], pars->GC_11, amp[3048]); 
  FFV1_0(w[280], w[179], w[153], pars->GC_11, amp[3049]); 
  FFV1_0(w[254], w[5], w[178], pars->GC_11, amp[3050]); 
  FFV2_3_0(w[282], w[168], w[3], pars->GC_50, pars->GC_58, amp[3051]); 
  FFV1_0(w[254], w[168], w[4], pars->GC_11, amp[3052]); 
  FFV1_0(w[251], w[190], w[178], pars->GC_11, amp[3053]); 
  FFV1_0(w[282], w[190], w[153], pars->GC_11, amp[3054]); 
  FFV1_0(w[251], w[236], w[153], pars->GC_11, amp[3055]); 
  FFV1_0(w[108], w[199], w[178], pars->GC_11, amp[3056]); 
  FFV2_3_0(w[154], w[236], w[2], pars->GC_50, pars->GC_58, amp[3057]); 
  FFV1_0(w[154], w[199], w[4], pars->GC_11, amp[3058]); 
  FFV1_0(w[260], w[5], w[178], pars->GC_11, amp[3059]); 
  FFV2_3_0(w[280], w[168], w[2], pars->GC_50, pars->GC_58, amp[3060]); 
  FFV1_0(w[260], w[168], w[4], pars->GC_11, amp[3061]); 
  FFV1_0(w[0], w[281], w[257], pars->GC_11, amp[3062]); 
  FFV1_0(w[108], w[236], w[271], pars->GC_11, amp[3063]); 
  VVV1_0(w[4], w[271], w[257], pars->GC_10, amp[3064]); 
  FFV1_0(w[0], w[281], w[259], pars->GC_11, amp[3065]); 
  FFV1_0(w[280], w[5], w[271], pars->GC_11, amp[3066]); 
  VVV1_0(w[4], w[271], w[259], pars->GC_10, amp[3067]); 
  FFV2_3_0(w[146], w[281], w[3], pars->GC_50, pars->GC_58, amp[3068]); 
  FFV1_0(w[0], w[245], w[173], pars->GC_11, amp[3069]); 
  FFV1_0(w[146], w[245], w[4], pars->GC_11, amp[3070]); 
  FFV1_0(w[108], w[240], w[272], pars->GC_11, amp[3071]); 
  FFV1_0(w[0], w[279], w[248], pars->GC_11, amp[3072]); 
  VVV1_0(w[4], w[272], w[248], pars->GC_10, amp[3073]); 
  FFV1_0(w[282], w[5], w[272], pars->GC_11, amp[3074]); 
  FFV1_0(w[0], w[279], w[253], pars->GC_11, amp[3075]); 
  VVV1_0(w[4], w[272], w[253], pars->GC_10, amp[3076]); 
  FFV2_3_0(w[146], w[279], w[2], pars->GC_50, pars->GC_58, amp[3077]); 
  FFV1_0(w[0], w[256], w[173], pars->GC_11, amp[3078]); 
  FFV1_0(w[146], w[256], w[4], pars->GC_11, amp[3079]); 
  FFV1_0(w[108], w[249], w[184], pars->GC_11, amp[3080]); 
  FFV1_0(w[247], w[107], w[184], pars->GC_11, amp[3081]); 
  FFV1_0(w[180], w[179], w[120], pars->GC_11, amp[3082]); 
  FFV2_3_0(w[121], w[179], w[3], pars->GC_50, pars->GC_58, amp[3083]); 
  FFV1_0(w[180], w[5], w[244], pars->GC_11, amp[3084]); 
  FFV1_0(w[108], w[245], w[21], pars->GC_11, amp[3085]); 
  FFV1_0(w[108], w[243], w[192], pars->GC_11, amp[3086]); 
  FFV1_0(w[247], w[243], w[21], pars->GC_11, amp[3087]); 
  FFV1_0(w[180], w[5], w[252], pars->GC_11, amp[3088]); 
  FFV1_0(w[254], w[107], w[21], pars->GC_11, amp[3089]); 
  FFV1_0(w[251], w[107], w[192], pars->GC_11, amp[3090]); 
  FFV1_0(w[251], w[249], w[21], pars->GC_11, amp[3091]); 
  FFV1_0(w[197], w[190], w[120], pars->GC_11, amp[3092]); 
  FFV2_3_0(w[121], w[190], w[2], pars->GC_50, pars->GC_58, amp[3093]); 
  FFV1_0(w[197], w[5], w[255], pars->GC_11, amp[3094]); 
  FFV1_0(w[108], w[256], w[21], pars->GC_11, amp[3095]); 
  FFV1_0(w[197], w[5], w[258], pars->GC_11, amp[3096]); 
  FFV1_0(w[260], w[107], w[21], pars->GC_11, amp[3097]); 
  FFV2_3_0(w[197], w[132], w[3], pars->GC_50, pars->GC_58, amp[3098]); 
  FFV2_3_0(w[180], w[132], w[2], pars->GC_50, pars->GC_58, amp[3099]); 
  FFV1_0(w[108], w[249], w[209], pars->GC_11, amp[3100]); 
  FFV1_0(w[247], w[107], w[209], pars->GC_11, amp[3101]); 
  FFV1_0(w[205], w[206], w[120], pars->GC_11, amp[3102]); 
  FFV2_3_0(w[205], w[150], w[3], pars->GC_50, pars->GC_58, amp[3103]); 
  FFV1_0(w[108], w[243], w[215], pars->GC_11, amp[3104]); 
  FFV1_0(w[251], w[107], w[215], pars->GC_11, amp[3105]); 
  FFV1_0(w[211], w[212], w[120], pars->GC_11, amp[3106]); 
  FFV2_3_0(w[211], w[150], w[2], pars->GC_50, pars->GC_58, amp[3107]); 
  FFV1_0(w[108], w[245], w[60], pars->GC_11, amp[3108]); 
  FFV1_0(w[0], w[206], w[244], pars->GC_11, amp[3109]); 
  FFV1_0(w[247], w[243], w[60], pars->GC_11, amp[3110]); 
  FFV1_0(w[254], w[107], w[60], pars->GC_11, amp[3111]); 
  FFV1_0(w[0], w[206], w[252], pars->GC_11, amp[3112]); 
  FFV1_0(w[251], w[249], w[60], pars->GC_11, amp[3113]); 
  FFV1_0(w[108], w[256], w[60], pars->GC_11, amp[3114]); 
  FFV1_0(w[0], w[212], w[255], pars->GC_11, amp[3115]); 
  FFV1_0(w[260], w[107], w[60], pars->GC_11, amp[3116]); 
  FFV1_0(w[0], w[212], w[258], pars->GC_11, amp[3117]); 
  FFV2_3_0(w[158], w[212], w[3], pars->GC_50, pars->GC_58, amp[3118]); 
  FFV2_3_0(w[158], w[206], w[2], pars->GC_50, pars->GC_58, amp[3119]); 
  FFV1_0(w[108], w[261], w[220], pars->GC_11, amp[3120]); 
  FFV1_0(w[208], w[5], w[136], pars->GC_11, amp[3121]); 
  FFV1_0(w[205], w[190], w[136], pars->GC_11, amp[3122]); 
  FFV1_0(w[205], w[5], w[263], pars->GC_11, amp[3123]); 
  FFV1_0(w[108], w[264], w[224], pars->GC_11, amp[3124]); 
  FFV1_0(w[214], w[5], w[136], pars->GC_11, amp[3125]); 
  FFV1_0(w[211], w[179], w[136], pars->GC_11, amp[3126]); 
  FFV1_0(w[211], w[5], w[266], pars->GC_11, amp[3127]); 
  FFV2_3_0(w[143], w[264], w[3], pars->GC_50, pars->GC_58, amp[3128]); 
  FFV2_3_0(w[143], w[261], w[2], pars->GC_50, pars->GC_58, amp[3129]); 
  FFV2_3_0(w[251], w[144], w[3], pars->GC_50, pars->GC_58, amp[3130]); 
  FFV1_0(w[251], w[261], w[75], pars->GC_11, amp[3131]); 
  FFV2_3_0(w[247], w[144], w[2], pars->GC_50, pars->GC_58, amp[3132]); 
  FFV1_0(w[247], w[264], w[75], pars->GC_11, amp[3133]); 
  FFV1_0(w[108], w[261], w[227], pars->GC_11, amp[3134]); 
  FFV1_0(w[0], w[182], w[136], pars->GC_11, amp[3135]); 
  FFV1_0(w[0], w[179], w[263], pars->GC_11, amp[3136]); 
  FFV1_0(w[0], w[190], w[266], pars->GC_11, amp[3137]); 
  FFV1_0(w[108], w[264], w[228], pars->GC_11, amp[3138]); 
  FFV1_0(w[0], w[199], w[136], pars->GC_11, amp[3139]); 
  FFV1_0(w[273], w[107], w[220], pars->GC_11, amp[3140]); 
  FFV1_0(w[208], w[5], w[161], pars->GC_11, amp[3141]); 
  FFV1_0(w[205], w[190], w[161], pars->GC_11, amp[3142]); 
  FFV1_0(w[205], w[5], w[274], pars->GC_11, amp[3143]); 
  FFV1_0(w[276], w[107], w[224], pars->GC_11, amp[3144]); 
  FFV1_0(w[214], w[5], w[161], pars->GC_11, amp[3145]); 
  FFV1_0(w[211], w[179], w[161], pars->GC_11, amp[3146]); 
  FFV1_0(w[211], w[5], w[277], pars->GC_11, amp[3147]); 
  FFV2_3_0(w[276], w[170], w[3], pars->GC_50, pars->GC_58, amp[3148]); 
  FFV2_3_0(w[273], w[170], w[2], pars->GC_50, pars->GC_58, amp[3149]); 
  FFV2_3_0(w[171], w[243], w[3], pars->GC_50, pars->GC_58, amp[3150]); 
  FFV1_0(w[273], w[243], w[75], pars->GC_11, amp[3151]); 
  FFV2_3_0(w[171], w[249], w[2], pars->GC_50, pars->GC_58, amp[3152]); 
  FFV1_0(w[276], w[249], w[75], pars->GC_11, amp[3153]); 
  FFV1_0(w[273], w[107], w[227], pars->GC_11, amp[3154]); 
  FFV1_0(w[0], w[182], w[161], pars->GC_11, amp[3155]); 
  FFV1_0(w[0], w[179], w[274], pars->GC_11, amp[3156]); 
  FFV1_0(w[0], w[190], w[277], pars->GC_11, amp[3157]); 
  FFV1_0(w[276], w[107], w[228], pars->GC_11, amp[3158]); 
  FFV1_0(w[0], w[199], w[161], pars->GC_11, amp[3159]); 
  FFV1_0(w[235], w[190], w[120], pars->GC_11, amp[3160]); 
  FFV1_0(w[205], w[236], w[120], pars->GC_11, amp[3161]); 
  FFV1_0(w[205], w[190], w[174], pars->GC_11, amp[3162]); 
  FFV1_0(w[235], w[5], w[255], pars->GC_11, amp[3163]); 
  FFV1_0(w[108], w[279], w[220], pars->GC_11, amp[3164]); 
  VVV1_0(w[4], w[220], w[255], pars->GC_10, amp[3165]); 
  FFV1_0(w[235], w[5], w[258], pars->GC_11, amp[3166]); 
  FFV1_0(w[280], w[107], w[220], pars->GC_11, amp[3167]); 
  VVV1_0(w[4], w[220], w[258], pars->GC_10, amp[3168]); 
  FFV2_3_0(w[235], w[132], w[3], pars->GC_50, pars->GC_58, amp[3169]); 
  FFV1_0(w[208], w[5], w[174], pars->GC_11, amp[3170]); 
  FFV1_0(w[208], w[132], w[4], pars->GC_11, amp[3171]); 
  FFV1_0(w[239], w[179], w[120], pars->GC_11, amp[3172]); 
  FFV1_0(w[211], w[240], w[120], pars->GC_11, amp[3173]); 
  FFV1_0(w[211], w[179], w[174], pars->GC_11, amp[3174]); 
  FFV1_0(w[239], w[5], w[244], pars->GC_11, amp[3175]); 
  FFV1_0(w[108], w[281], w[224], pars->GC_11, amp[3176]); 
  VVV1_0(w[4], w[224], w[244], pars->GC_10, amp[3177]); 
  FFV1_0(w[239], w[5], w[252], pars->GC_11, amp[3178]); 
  FFV1_0(w[282], w[107], w[224], pars->GC_11, amp[3179]); 
  VVV1_0(w[4], w[224], w[252], pars->GC_10, amp[3180]); 
  FFV2_3_0(w[239], w[132], w[2], pars->GC_50, pars->GC_58, amp[3181]); 
  FFV1_0(w[214], w[5], w[174], pars->GC_11, amp[3182]); 
  FFV1_0(w[214], w[132], w[4], pars->GC_11, amp[3183]); 
  FFV1_0(w[108], w[245], w[105], pars->GC_11, amp[3184]); 
  FFV2_3_0(w[143], w[281], w[3], pars->GC_50, pars->GC_58, amp[3185]); 
  FFV1_0(w[143], w[245], w[4], pars->GC_11, amp[3186]); 
  FFV1_0(w[247], w[243], w[105], pars->GC_11, amp[3187]); 
  FFV1_0(w[247], w[281], w[75], pars->GC_11, amp[3188]); 
  FFV1_0(w[280], w[243], w[75], pars->GC_11, amp[3189]); 
  FFV1_0(w[254], w[107], w[105], pars->GC_11, amp[3190]); 
  FFV2_3_0(w[282], w[170], w[3], pars->GC_50, pars->GC_58, amp[3191]); 
  FFV1_0(w[254], w[170], w[4], pars->GC_11, amp[3192]); 
  FFV1_0(w[251], w[249], w[105], pars->GC_11, amp[3193]); 
  FFV1_0(w[282], w[249], w[75], pars->GC_11, amp[3194]); 
  FFV1_0(w[251], w[279], w[75], pars->GC_11, amp[3195]); 
  FFV1_0(w[108], w[256], w[105], pars->GC_11, amp[3196]); 
  FFV2_3_0(w[143], w[279], w[2], pars->GC_50, pars->GC_58, amp[3197]); 
  FFV1_0(w[143], w[256], w[4], pars->GC_11, amp[3198]); 
  FFV1_0(w[260], w[107], w[105], pars->GC_11, amp[3199]); 
  FFV2_3_0(w[280], w[170], w[2], pars->GC_50, pars->GC_58, amp[3200]); 
  FFV1_0(w[260], w[170], w[4], pars->GC_11, amp[3201]); 
  FFV1_0(w[0], w[240], w[255], pars->GC_11, amp[3202]); 
  FFV1_0(w[108], w[279], w[227], pars->GC_11, amp[3203]); 
  VVV1_0(w[4], w[227], w[255], pars->GC_10, amp[3204]); 
  FFV1_0(w[0], w[240], w[258], pars->GC_11, amp[3205]); 
  FFV1_0(w[280], w[107], w[227], pars->GC_11, amp[3206]); 
  VVV1_0(w[4], w[227], w[258], pars->GC_10, amp[3207]); 
  FFV2_3_0(w[158], w[240], w[3], pars->GC_50, pars->GC_58, amp[3208]); 
  FFV1_0(w[0], w[182], w[174], pars->GC_11, amp[3209]); 
  FFV1_0(w[158], w[182], w[4], pars->GC_11, amp[3210]); 
  FFV1_0(w[108], w[281], w[228], pars->GC_11, amp[3211]); 
  FFV1_0(w[0], w[236], w[244], pars->GC_11, amp[3212]); 
  VVV1_0(w[4], w[228], w[244], pars->GC_10, amp[3213]); 
  FFV1_0(w[282], w[107], w[228], pars->GC_11, amp[3214]); 
  FFV1_0(w[0], w[236], w[252], pars->GC_11, amp[3215]); 
  VVV1_0(w[4], w[228], w[252], pars->GC_10, amp[3216]); 
  FFV2_3_0(w[158], w[236], w[2], pars->GC_50, pars->GC_58, amp[3217]); 
  FFV1_0(w[0], w[199], w[174], pars->GC_11, amp[3218]); 
  FFV1_0(w[158], w[199], w[4], pars->GC_11, amp[3219]); 
  FFV1_0(w[108], w[118], w[291], pars->GC_11, amp[3220]); 
  FFV1_0(w[113], w[107], w[291], pars->GC_11, amp[3221]); 
  FFV1_0(w[287], w[286], w[120], pars->GC_11, amp[3222]); 
  FFV2_5_0(w[293], w[286], w[3], pars->GC_51, pars->GC_58, amp[3223]); 
  FFV1_0(w[287], w[283], w[110], pars->GC_11, amp[3224]); 
  FFV1_0(w[108], w[111], w[294], pars->GC_11, amp[3225]); 
  FFV1_0(w[108], w[109], w[297], pars->GC_11, amp[3226]); 
  FFV1_0(w[113], w[109], w[294], pars->GC_11, amp[3227]); 
  FFV1_0(w[287], w[283], w[123], pars->GC_11, amp[3228]); 
  FFV1_0(w[125], w[107], w[294], pars->GC_11, amp[3229]); 
  FFV1_0(w[122], w[107], w[297], pars->GC_11, amp[3230]); 
  FFV1_0(w[122], w[118], w[294], pars->GC_11, amp[3231]); 
  FFV1_0(w[301], w[295], w[120], pars->GC_11, amp[3232]); 
  FFV2_5_0(w[293], w[295], w[2], pars->GC_51, pars->GC_58, amp[3233]); 
  FFV1_0(w[301], w[283], w[126], pars->GC_11, amp[3234]); 
  FFV1_0(w[108], w[127], w[294], pars->GC_11, amp[3235]); 
  FFV1_0(w[301], w[283], w[129], pars->GC_11, amp[3236]); 
  FFV1_0(w[131], w[107], w[294], pars->GC_11, amp[3237]); 
  FFV2_5_0(w[301], w[306], w[3], pars->GC_51, pars->GC_58, amp[3238]); 
  FFV2_5_0(w[287], w[306], w[2], pars->GC_51, pars->GC_58, amp[3239]); 
  FFV1_0(w[108], w[118], w[313], pars->GC_11, amp[3240]); 
  FFV1_0(w[113], w[107], w[313], pars->GC_11, amp[3241]); 
  FFV1_0(w[308], w[309], w[120], pars->GC_11, amp[3242]); 
  FFV2_5_0(w[308], w[315], w[3], pars->GC_51, pars->GC_58, amp[3243]); 
  FFV1_0(w[108], w[109], w[320], pars->GC_11, amp[3244]); 
  FFV1_0(w[122], w[107], w[320], pars->GC_11, amp[3245]); 
  FFV1_0(w[316], w[317], w[120], pars->GC_11, amp[3246]); 
  FFV2_5_0(w[316], w[315], w[2], pars->GC_51, pars->GC_58, amp[3247]); 
  FFV1_0(w[108], w[111], w[325], pars->GC_11, amp[3248]); 
  FFV1_0(w[284], w[309], w[110], pars->GC_11, amp[3249]); 
  FFV1_0(w[113], w[109], w[325], pars->GC_11, amp[3250]); 
  FFV1_0(w[125], w[107], w[325], pars->GC_11, amp[3251]); 
  FFV1_0(w[284], w[309], w[123], pars->GC_11, amp[3252]); 
  FFV1_0(w[122], w[118], w[325], pars->GC_11, amp[3253]); 
  FFV1_0(w[108], w[127], w[325], pars->GC_11, amp[3254]); 
  FFV1_0(w[284], w[317], w[126], pars->GC_11, amp[3255]); 
  FFV1_0(w[131], w[107], w[325], pars->GC_11, amp[3256]); 
  FFV1_0(w[284], w[317], w[129], pars->GC_11, amp[3257]); 
  FFV2_5_0(w[328], w[317], w[3], pars->GC_51, pars->GC_58, amp[3258]); 
  FFV2_5_0(w[328], w[309], w[2], pars->GC_51, pars->GC_58, amp[3259]); 
  FFV1_0(w[108], w[135], w[329], pars->GC_11, amp[3260]); 
  FFV1_0(w[312], w[283], w[136], pars->GC_11, amp[3261]); 
  FFV1_0(w[308], w[295], w[136], pars->GC_11, amp[3262]); 
  FFV1_0(w[308], w[283], w[138], pars->GC_11, amp[3263]); 
  FFV1_0(w[108], w[140], w[332], pars->GC_11, amp[3264]); 
  FFV1_0(w[319], w[283], w[136], pars->GC_11, amp[3265]); 
  FFV1_0(w[316], w[286], w[136], pars->GC_11, amp[3266]); 
  FFV1_0(w[316], w[283], w[142], pars->GC_11, amp[3267]); 
  FFV2_5_0(w[335], w[140], w[3], pars->GC_51, pars->GC_58, amp[3268]); 
  FFV2_5_0(w[335], w[135], w[2], pars->GC_51, pars->GC_58, amp[3269]); 
  FFV2_5_0(w[122], w[336], w[3], pars->GC_51, pars->GC_58, amp[3270]); 
  FFV1_0(w[122], w[135], w[334], pars->GC_11, amp[3271]); 
  FFV2_5_0(w[113], w[336], w[2], pars->GC_51, pars->GC_58, amp[3272]); 
  FFV1_0(w[113], w[140], w[334], pars->GC_11, amp[3273]); 
  FFV1_0(w[108], w[135], w[338], pars->GC_11, amp[3274]); 
  FFV1_0(w[284], w[290], w[136], pars->GC_11, amp[3275]); 
  FFV1_0(w[284], w[286], w[138], pars->GC_11, amp[3276]); 
  FFV1_0(w[284], w[295], w[142], pars->GC_11, amp[3277]); 
  FFV1_0(w[108], w[140], w[339], pars->GC_11, amp[3278]); 
  FFV1_0(w[284], w[303], w[136], pars->GC_11, amp[3279]); 
  FFV1_0(w[160], w[107], w[329], pars->GC_11, amp[3280]); 
  FFV1_0(w[312], w[283], w[161], pars->GC_11, amp[3281]); 
  FFV1_0(w[308], w[295], w[161], pars->GC_11, amp[3282]); 
  FFV1_0(w[308], w[283], w[163], pars->GC_11, amp[3283]); 
  FFV1_0(w[165], w[107], w[332], pars->GC_11, amp[3284]); 
  FFV1_0(w[319], w[283], w[161], pars->GC_11, amp[3285]); 
  FFV1_0(w[316], w[286], w[161], pars->GC_11, amp[3286]); 
  FFV1_0(w[316], w[283], w[166], pars->GC_11, amp[3287]); 
  FFV2_5_0(w[165], w[344], w[3], pars->GC_51, pars->GC_58, amp[3288]); 
  FFV2_5_0(w[160], w[344], w[2], pars->GC_51, pars->GC_58, amp[3289]); 
  FFV2_5_0(w[345], w[109], w[3], pars->GC_51, pars->GC_58, amp[3290]); 
  FFV1_0(w[160], w[109], w[334], pars->GC_11, amp[3291]); 
  FFV2_5_0(w[345], w[118], w[2], pars->GC_51, pars->GC_58, amp[3292]); 
  FFV1_0(w[165], w[118], w[334], pars->GC_11, amp[3293]); 
  FFV1_0(w[160], w[107], w[338], pars->GC_11, amp[3294]); 
  FFV1_0(w[284], w[290], w[161], pars->GC_11, amp[3295]); 
  FFV1_0(w[284], w[286], w[163], pars->GC_11, amp[3296]); 
  FFV1_0(w[284], w[295], w[166], pars->GC_11, amp[3297]); 
  FFV1_0(w[165], w[107], w[339], pars->GC_11, amp[3298]); 
  FFV1_0(w[284], w[303], w[161], pars->GC_11, amp[3299]); 
  FFV1_0(w[348], w[295], w[120], pars->GC_11, amp[3300]); 
  FFV1_0(w[308], w[349], w[120], pars->GC_11, amp[3301]); 
  FFV1_0(w[308], w[295], w[174], pars->GC_11, amp[3302]); 
  FFV1_0(w[348], w[283], w[126], pars->GC_11, amp[3303]); 
  FFV1_0(w[108], w[172], w[329], pars->GC_11, amp[3304]); 
  VVV1_0(w[4], w[329], w[126], pars->GC_10, amp[3305]); 
  FFV1_0(w[348], w[283], w[129], pars->GC_11, amp[3306]); 
  FFV1_0(w[175], w[107], w[329], pars->GC_11, amp[3307]); 
  VVV1_0(w[4], w[329], w[129], pars->GC_10, amp[3308]); 
  FFV2_5_0(w[348], w[306], w[3], pars->GC_51, pars->GC_58, amp[3309]); 
  FFV1_0(w[312], w[283], w[174], pars->GC_11, amp[3310]); 
  FFV1_0(w[312], w[306], w[4], pars->GC_11, amp[3311]); 
  FFV1_0(w[351], w[286], w[120], pars->GC_11, amp[3312]); 
  FFV1_0(w[316], w[352], w[120], pars->GC_11, amp[3313]); 
  FFV1_0(w[316], w[286], w[174], pars->GC_11, amp[3314]); 
  FFV1_0(w[351], w[283], w[110], pars->GC_11, amp[3315]); 
  FFV1_0(w[108], w[176], w[332], pars->GC_11, amp[3316]); 
  VVV1_0(w[4], w[332], w[110], pars->GC_10, amp[3317]); 
  FFV1_0(w[351], w[283], w[123], pars->GC_11, amp[3318]); 
  FFV1_0(w[177], w[107], w[332], pars->GC_11, amp[3319]); 
  VVV1_0(w[4], w[332], w[123], pars->GC_10, amp[3320]); 
  FFV2_5_0(w[351], w[306], w[2], pars->GC_51, pars->GC_58, amp[3321]); 
  FFV1_0(w[319], w[283], w[174], pars->GC_11, amp[3322]); 
  FFV1_0(w[319], w[306], w[4], pars->GC_11, amp[3323]); 
  FFV1_0(w[108], w[111], w[353], pars->GC_11, amp[3324]); 
  FFV2_5_0(w[335], w[176], w[3], pars->GC_51, pars->GC_58, amp[3325]); 
  FFV1_0(w[335], w[111], w[4], pars->GC_11, amp[3326]); 
  FFV1_0(w[113], w[109], w[353], pars->GC_11, amp[3327]); 
  FFV1_0(w[113], w[176], w[334], pars->GC_11, amp[3328]); 
  FFV1_0(w[175], w[109], w[334], pars->GC_11, amp[3329]); 
  FFV1_0(w[125], w[107], w[353], pars->GC_11, amp[3330]); 
  FFV2_5_0(w[177], w[344], w[3], pars->GC_51, pars->GC_58, amp[3331]); 
  FFV1_0(w[125], w[344], w[4], pars->GC_11, amp[3332]); 
  FFV1_0(w[122], w[118], w[353], pars->GC_11, amp[3333]); 
  FFV1_0(w[177], w[118], w[334], pars->GC_11, amp[3334]); 
  FFV1_0(w[122], w[172], w[334], pars->GC_11, amp[3335]); 
  FFV1_0(w[108], w[127], w[353], pars->GC_11, amp[3336]); 
  FFV2_5_0(w[335], w[172], w[2], pars->GC_51, pars->GC_58, amp[3337]); 
  FFV1_0(w[335], w[127], w[4], pars->GC_11, amp[3338]); 
  FFV1_0(w[131], w[107], w[353], pars->GC_11, amp[3339]); 
  FFV2_5_0(w[175], w[344], w[2], pars->GC_51, pars->GC_58, amp[3340]); 
  FFV1_0(w[131], w[344], w[4], pars->GC_11, amp[3341]); 
  FFV1_0(w[284], w[352], w[126], pars->GC_11, amp[3342]); 
  FFV1_0(w[108], w[172], w[338], pars->GC_11, amp[3343]); 
  VVV1_0(w[4], w[338], w[126], pars->GC_10, amp[3344]); 
  FFV1_0(w[284], w[352], w[129], pars->GC_11, amp[3345]); 
  FFV1_0(w[175], w[107], w[338], pars->GC_11, amp[3346]); 
  VVV1_0(w[4], w[338], w[129], pars->GC_10, amp[3347]); 
  FFV2_5_0(w[328], w[352], w[3], pars->GC_51, pars->GC_58, amp[3348]); 
  FFV1_0(w[284], w[290], w[174], pars->GC_11, amp[3349]); 
  FFV1_0(w[328], w[290], w[4], pars->GC_11, amp[3350]); 
  FFV1_0(w[108], w[176], w[339], pars->GC_11, amp[3351]); 
  FFV1_0(w[284], w[349], w[110], pars->GC_11, amp[3352]); 
  VVV1_0(w[4], w[339], w[110], pars->GC_10, amp[3353]); 
  FFV1_0(w[177], w[107], w[339], pars->GC_11, amp[3354]); 
  FFV1_0(w[284], w[349], w[123], pars->GC_11, amp[3355]); 
  VVV1_0(w[4], w[339], w[123], pars->GC_10, amp[3356]); 
  FFV2_5_0(w[328], w[349], w[2], pars->GC_51, pars->GC_58, amp[3357]); 
  FFV1_0(w[284], w[303], w[174], pars->GC_11, amp[3358]); 
  FFV1_0(w[328], w[303], w[4], pars->GC_11, amp[3359]); 
  FFV1_0(w[108], w[249], w[291], pars->GC_11, amp[3360]); 
  FFV1_0(w[247], w[107], w[291], pars->GC_11, amp[3361]); 
  FFV1_0(w[287], w[286], w[120], pars->GC_11, amp[3362]); 
  FFV2_5_0(w[293], w[286], w[3], pars->GC_51, pars->GC_58, amp[3363]); 
  FFV1_0(w[287], w[283], w[244], pars->GC_11, amp[3364]); 
  FFV1_0(w[108], w[245], w[294], pars->GC_11, amp[3365]); 
  FFV1_0(w[108], w[243], w[297], pars->GC_11, amp[3366]); 
  FFV1_0(w[247], w[243], w[294], pars->GC_11, amp[3367]); 
  FFV1_0(w[287], w[283], w[252], pars->GC_11, amp[3368]); 
  FFV1_0(w[254], w[107], w[294], pars->GC_11, amp[3369]); 
  FFV1_0(w[251], w[107], w[297], pars->GC_11, amp[3370]); 
  FFV1_0(w[251], w[249], w[294], pars->GC_11, amp[3371]); 
  FFV1_0(w[301], w[295], w[120], pars->GC_11, amp[3372]); 
  FFV2_5_0(w[293], w[295], w[2], pars->GC_51, pars->GC_58, amp[3373]); 
  FFV1_0(w[301], w[283], w[255], pars->GC_11, amp[3374]); 
  FFV1_0(w[108], w[256], w[294], pars->GC_11, amp[3375]); 
  FFV1_0(w[301], w[283], w[258], pars->GC_11, amp[3376]); 
  FFV1_0(w[260], w[107], w[294], pars->GC_11, amp[3377]); 
  FFV2_5_0(w[301], w[306], w[3], pars->GC_51, pars->GC_58, amp[3378]); 
  FFV2_5_0(w[287], w[306], w[2], pars->GC_51, pars->GC_58, amp[3379]); 
  FFV1_0(w[108], w[249], w[313], pars->GC_11, amp[3380]); 
  FFV1_0(w[247], w[107], w[313], pars->GC_11, amp[3381]); 
  FFV1_0(w[308], w[309], w[120], pars->GC_11, amp[3382]); 
  FFV2_5_0(w[308], w[315], w[3], pars->GC_51, pars->GC_58, amp[3383]); 
  FFV1_0(w[108], w[243], w[320], pars->GC_11, amp[3384]); 
  FFV1_0(w[251], w[107], w[320], pars->GC_11, amp[3385]); 
  FFV1_0(w[316], w[317], w[120], pars->GC_11, amp[3386]); 
  FFV2_5_0(w[316], w[315], w[2], pars->GC_51, pars->GC_58, amp[3387]); 
  FFV1_0(w[108], w[245], w[325], pars->GC_11, amp[3388]); 
  FFV1_0(w[284], w[309], w[244], pars->GC_11, amp[3389]); 
  FFV1_0(w[247], w[243], w[325], pars->GC_11, amp[3390]); 
  FFV1_0(w[254], w[107], w[325], pars->GC_11, amp[3391]); 
  FFV1_0(w[284], w[309], w[252], pars->GC_11, amp[3392]); 
  FFV1_0(w[251], w[249], w[325], pars->GC_11, amp[3393]); 
  FFV1_0(w[108], w[256], w[325], pars->GC_11, amp[3394]); 
  FFV1_0(w[284], w[317], w[255], pars->GC_11, amp[3395]); 
  FFV1_0(w[260], w[107], w[325], pars->GC_11, amp[3396]); 
  FFV1_0(w[284], w[317], w[258], pars->GC_11, amp[3397]); 
  FFV2_5_0(w[328], w[317], w[3], pars->GC_51, pars->GC_58, amp[3398]); 
  FFV2_5_0(w[328], w[309], w[2], pars->GC_51, pars->GC_58, amp[3399]); 
  FFV1_0(w[108], w[261], w[329], pars->GC_11, amp[3400]); 
  FFV1_0(w[312], w[283], w[136], pars->GC_11, amp[3401]); 
  FFV1_0(w[308], w[295], w[136], pars->GC_11, amp[3402]); 
  FFV1_0(w[308], w[283], w[263], pars->GC_11, amp[3403]); 
  FFV1_0(w[108], w[264], w[332], pars->GC_11, amp[3404]); 
  FFV1_0(w[319], w[283], w[136], pars->GC_11, amp[3405]); 
  FFV1_0(w[316], w[286], w[136], pars->GC_11, amp[3406]); 
  FFV1_0(w[316], w[283], w[266], pars->GC_11, amp[3407]); 
  FFV2_3_0(w[335], w[264], w[3], pars->GC_50, pars->GC_58, amp[3408]); 
  FFV2_3_0(w[335], w[261], w[2], pars->GC_50, pars->GC_58, amp[3409]); 
  FFV2_3_0(w[251], w[336], w[3], pars->GC_50, pars->GC_58, amp[3410]); 
  FFV1_0(w[251], w[261], w[334], pars->GC_11, amp[3411]); 
  FFV2_3_0(w[247], w[336], w[2], pars->GC_50, pars->GC_58, amp[3412]); 
  FFV1_0(w[247], w[264], w[334], pars->GC_11, amp[3413]); 
  FFV1_0(w[108], w[261], w[338], pars->GC_11, amp[3414]); 
  FFV1_0(w[284], w[290], w[136], pars->GC_11, amp[3415]); 
  FFV1_0(w[284], w[286], w[263], pars->GC_11, amp[3416]); 
  FFV1_0(w[284], w[295], w[266], pars->GC_11, amp[3417]); 
  FFV1_0(w[108], w[264], w[339], pars->GC_11, amp[3418]); 
  FFV1_0(w[284], w[303], w[136], pars->GC_11, amp[3419]); 
  FFV1_0(w[273], w[107], w[329], pars->GC_11, amp[3420]); 
  FFV1_0(w[312], w[283], w[161], pars->GC_11, amp[3421]); 
  FFV1_0(w[308], w[295], w[161], pars->GC_11, amp[3422]); 
  FFV1_0(w[308], w[283], w[274], pars->GC_11, amp[3423]); 
  FFV1_0(w[276], w[107], w[332], pars->GC_11, amp[3424]); 
  FFV1_0(w[319], w[283], w[161], pars->GC_11, amp[3425]); 
  FFV1_0(w[316], w[286], w[161], pars->GC_11, amp[3426]); 
  FFV1_0(w[316], w[283], w[277], pars->GC_11, amp[3427]); 
  FFV2_3_0(w[276], w[344], w[3], pars->GC_50, pars->GC_58, amp[3428]); 
  FFV2_3_0(w[273], w[344], w[2], pars->GC_50, pars->GC_58, amp[3429]); 
  FFV2_3_0(w[345], w[243], w[3], pars->GC_50, pars->GC_58, amp[3430]); 
  FFV1_0(w[273], w[243], w[334], pars->GC_11, amp[3431]); 
  FFV2_3_0(w[345], w[249], w[2], pars->GC_50, pars->GC_58, amp[3432]); 
  FFV1_0(w[276], w[249], w[334], pars->GC_11, amp[3433]); 
  FFV1_0(w[273], w[107], w[338], pars->GC_11, amp[3434]); 
  FFV1_0(w[284], w[290], w[161], pars->GC_11, amp[3435]); 
  FFV1_0(w[284], w[286], w[274], pars->GC_11, amp[3436]); 
  FFV1_0(w[284], w[295], w[277], pars->GC_11, amp[3437]); 
  FFV1_0(w[276], w[107], w[339], pars->GC_11, amp[3438]); 
  FFV1_0(w[284], w[303], w[161], pars->GC_11, amp[3439]); 
  FFV1_0(w[348], w[295], w[120], pars->GC_11, amp[3440]); 
  FFV1_0(w[308], w[349], w[120], pars->GC_11, amp[3441]); 
  FFV1_0(w[308], w[295], w[174], pars->GC_11, amp[3442]); 
  FFV1_0(w[348], w[283], w[255], pars->GC_11, amp[3443]); 
  FFV1_0(w[108], w[279], w[329], pars->GC_11, amp[3444]); 
  VVV1_0(w[4], w[329], w[255], pars->GC_10, amp[3445]); 
  FFV1_0(w[348], w[283], w[258], pars->GC_11, amp[3446]); 
  FFV1_0(w[280], w[107], w[329], pars->GC_11, amp[3447]); 
  VVV1_0(w[4], w[329], w[258], pars->GC_10, amp[3448]); 
  FFV2_5_0(w[348], w[306], w[3], pars->GC_51, pars->GC_58, amp[3449]); 
  FFV1_0(w[312], w[283], w[174], pars->GC_11, amp[3450]); 
  FFV1_0(w[312], w[306], w[4], pars->GC_11, amp[3451]); 
  FFV1_0(w[351], w[286], w[120], pars->GC_11, amp[3452]); 
  FFV1_0(w[316], w[352], w[120], pars->GC_11, amp[3453]); 
  FFV1_0(w[316], w[286], w[174], pars->GC_11, amp[3454]); 
  FFV1_0(w[351], w[283], w[244], pars->GC_11, amp[3455]); 
  FFV1_0(w[108], w[281], w[332], pars->GC_11, amp[3456]); 
  VVV1_0(w[4], w[332], w[244], pars->GC_10, amp[3457]); 
  FFV1_0(w[351], w[283], w[252], pars->GC_11, amp[3458]); 
  FFV1_0(w[282], w[107], w[332], pars->GC_11, amp[3459]); 
  VVV1_0(w[4], w[332], w[252], pars->GC_10, amp[3460]); 
  FFV2_5_0(w[351], w[306], w[2], pars->GC_51, pars->GC_58, amp[3461]); 
  FFV1_0(w[319], w[283], w[174], pars->GC_11, amp[3462]); 
  FFV1_0(w[319], w[306], w[4], pars->GC_11, amp[3463]); 
  FFV1_0(w[108], w[245], w[353], pars->GC_11, amp[3464]); 
  FFV2_3_0(w[335], w[281], w[3], pars->GC_50, pars->GC_58, amp[3465]); 
  FFV1_0(w[335], w[245], w[4], pars->GC_11, amp[3466]); 
  FFV1_0(w[247], w[243], w[353], pars->GC_11, amp[3467]); 
  FFV1_0(w[247], w[281], w[334], pars->GC_11, amp[3468]); 
  FFV1_0(w[280], w[243], w[334], pars->GC_11, amp[3469]); 
  FFV1_0(w[254], w[107], w[353], pars->GC_11, amp[3470]); 
  FFV2_3_0(w[282], w[344], w[3], pars->GC_50, pars->GC_58, amp[3471]); 
  FFV1_0(w[254], w[344], w[4], pars->GC_11, amp[3472]); 
  FFV1_0(w[251], w[249], w[353], pars->GC_11, amp[3473]); 
  FFV1_0(w[282], w[249], w[334], pars->GC_11, amp[3474]); 
  FFV1_0(w[251], w[279], w[334], pars->GC_11, amp[3475]); 
  FFV1_0(w[108], w[256], w[353], pars->GC_11, amp[3476]); 
  FFV2_3_0(w[335], w[279], w[2], pars->GC_50, pars->GC_58, amp[3477]); 
  FFV1_0(w[335], w[256], w[4], pars->GC_11, amp[3478]); 
  FFV1_0(w[260], w[107], w[353], pars->GC_11, amp[3479]); 
  FFV2_3_0(w[280], w[344], w[2], pars->GC_50, pars->GC_58, amp[3480]); 
  FFV1_0(w[260], w[344], w[4], pars->GC_11, amp[3481]); 
  FFV1_0(w[284], w[352], w[255], pars->GC_11, amp[3482]); 
  FFV1_0(w[108], w[279], w[338], pars->GC_11, amp[3483]); 
  VVV1_0(w[4], w[338], w[255], pars->GC_10, amp[3484]); 
  FFV1_0(w[284], w[352], w[258], pars->GC_11, amp[3485]); 
  FFV1_0(w[280], w[107], w[338], pars->GC_11, amp[3486]); 
  VVV1_0(w[4], w[338], w[258], pars->GC_10, amp[3487]); 
  FFV2_5_0(w[328], w[352], w[3], pars->GC_51, pars->GC_58, amp[3488]); 
  FFV1_0(w[284], w[290], w[174], pars->GC_11, amp[3489]); 
  FFV1_0(w[328], w[290], w[4], pars->GC_11, amp[3490]); 
  FFV1_0(w[108], w[281], w[339], pars->GC_11, amp[3491]); 
  FFV1_0(w[284], w[349], w[244], pars->GC_11, amp[3492]); 
  VVV1_0(w[4], w[339], w[244], pars->GC_10, amp[3493]); 
  FFV1_0(w[282], w[107], w[339], pars->GC_11, amp[3494]); 
  FFV1_0(w[284], w[349], w[252], pars->GC_11, amp[3495]); 
  VVV1_0(w[4], w[339], w[252], pars->GC_10, amp[3496]); 
  FFV2_5_0(w[328], w[349], w[2], pars->GC_51, pars->GC_58, amp[3497]); 
  FFV1_0(w[284], w[303], w[174], pars->GC_11, amp[3498]); 
  FFV1_0(w[328], w[303], w[4], pars->GC_11, amp[3499]); 
  FFV1_0(w[108], w[249], w[359], pars->GC_11, amp[3500]); 
  FFV1_0(w[247], w[107], w[359], pars->GC_11, amp[3501]); 
  FFV1_0(w[356], w[355], w[120], pars->GC_11, amp[3502]); 
  FFV2_3_0(w[293], w[355], w[3], pars->GC_50, pars->GC_58, amp[3503]); 
  FFV1_0(w[356], w[283], w[244], pars->GC_11, amp[3504]); 
  FFV1_0(w[108], w[245], w[294], pars->GC_11, amp[3505]); 
  FFV1_0(w[108], w[243], w[363], pars->GC_11, amp[3506]); 
  FFV1_0(w[247], w[243], w[294], pars->GC_11, amp[3507]); 
  FFV1_0(w[356], w[283], w[252], pars->GC_11, amp[3508]); 
  FFV1_0(w[254], w[107], w[294], pars->GC_11, amp[3509]); 
  FFV1_0(w[251], w[107], w[363], pars->GC_11, amp[3510]); 
  FFV1_0(w[251], w[249], w[294], pars->GC_11, amp[3511]); 
  FFV1_0(w[365], w[361], w[120], pars->GC_11, amp[3512]); 
  FFV2_3_0(w[293], w[361], w[2], pars->GC_50, pars->GC_58, amp[3513]); 
  FFV1_0(w[365], w[283], w[255], pars->GC_11, amp[3514]); 
  FFV1_0(w[108], w[256], w[294], pars->GC_11, amp[3515]); 
  FFV1_0(w[365], w[283], w[258], pars->GC_11, amp[3516]); 
  FFV1_0(w[260], w[107], w[294], pars->GC_11, amp[3517]); 
  FFV2_3_0(w[365], w[306], w[3], pars->GC_50, pars->GC_58, amp[3518]); 
  FFV2_3_0(w[356], w[306], w[2], pars->GC_50, pars->GC_58, amp[3519]); 
  FFV1_0(w[108], w[249], w[373], pars->GC_11, amp[3520]); 
  FFV1_0(w[247], w[107], w[373], pars->GC_11, amp[3521]); 
  FFV1_0(w[369], w[370], w[120], pars->GC_11, amp[3522]); 
  FFV2_3_0(w[369], w[315], w[3], pars->GC_50, pars->GC_58, amp[3523]); 
  FFV1_0(w[108], w[243], w[379], pars->GC_11, amp[3524]); 
  FFV1_0(w[251], w[107], w[379], pars->GC_11, amp[3525]); 
  FFV1_0(w[375], w[376], w[120], pars->GC_11, amp[3526]); 
  FFV2_3_0(w[375], w[315], w[2], pars->GC_50, pars->GC_58, amp[3527]); 
  FFV1_0(w[108], w[245], w[325], pars->GC_11, amp[3528]); 
  FFV1_0(w[284], w[370], w[244], pars->GC_11, amp[3529]); 
  FFV1_0(w[247], w[243], w[325], pars->GC_11, amp[3530]); 
  FFV1_0(w[254], w[107], w[325], pars->GC_11, amp[3531]); 
  FFV1_0(w[284], w[370], w[252], pars->GC_11, amp[3532]); 
  FFV1_0(w[251], w[249], w[325], pars->GC_11, amp[3533]); 
  FFV1_0(w[108], w[256], w[325], pars->GC_11, amp[3534]); 
  FFV1_0(w[284], w[376], w[255], pars->GC_11, amp[3535]); 
  FFV1_0(w[260], w[107], w[325], pars->GC_11, amp[3536]); 
  FFV1_0(w[284], w[376], w[258], pars->GC_11, amp[3537]); 
  FFV2_3_0(w[328], w[376], w[3], pars->GC_50, pars->GC_58, amp[3538]); 
  FFV2_3_0(w[328], w[370], w[2], pars->GC_50, pars->GC_58, amp[3539]); 
  FFV1_0(w[108], w[261], w[383], pars->GC_11, amp[3540]); 
  FFV1_0(w[372], w[283], w[136], pars->GC_11, amp[3541]); 
  FFV1_0(w[369], w[361], w[136], pars->GC_11, amp[3542]); 
  FFV1_0(w[369], w[283], w[263], pars->GC_11, amp[3543]); 
  FFV1_0(w[108], w[264], w[385], pars->GC_11, amp[3544]); 
  FFV1_0(w[378], w[283], w[136], pars->GC_11, amp[3545]); 
  FFV1_0(w[375], w[355], w[136], pars->GC_11, amp[3546]); 
  FFV1_0(w[375], w[283], w[266], pars->GC_11, amp[3547]); 
  FFV2_3_0(w[335], w[264], w[3], pars->GC_50, pars->GC_58, amp[3548]); 
  FFV2_3_0(w[335], w[261], w[2], pars->GC_50, pars->GC_58, amp[3549]); 
  FFV2_3_0(w[251], w[336], w[3], pars->GC_50, pars->GC_58, amp[3550]); 
  FFV1_0(w[251], w[261], w[334], pars->GC_11, amp[3551]); 
  FFV2_3_0(w[247], w[336], w[2], pars->GC_50, pars->GC_58, amp[3552]); 
  FFV1_0(w[247], w[264], w[334], pars->GC_11, amp[3553]); 
  FFV1_0(w[108], w[261], w[387], pars->GC_11, amp[3554]); 
  FFV1_0(w[284], w[358], w[136], pars->GC_11, amp[3555]); 
  FFV1_0(w[284], w[355], w[263], pars->GC_11, amp[3556]); 
  FFV1_0(w[284], w[361], w[266], pars->GC_11, amp[3557]); 
  FFV1_0(w[108], w[264], w[388], pars->GC_11, amp[3558]); 
  FFV1_0(w[284], w[367], w[136], pars->GC_11, amp[3559]); 
  FFV1_0(w[273], w[107], w[383], pars->GC_11, amp[3560]); 
  FFV1_0(w[372], w[283], w[161], pars->GC_11, amp[3561]); 
  FFV1_0(w[369], w[361], w[161], pars->GC_11, amp[3562]); 
  FFV1_0(w[369], w[283], w[274], pars->GC_11, amp[3563]); 
  FFV1_0(w[276], w[107], w[385], pars->GC_11, amp[3564]); 
  FFV1_0(w[378], w[283], w[161], pars->GC_11, amp[3565]); 
  FFV1_0(w[375], w[355], w[161], pars->GC_11, amp[3566]); 
  FFV1_0(w[375], w[283], w[277], pars->GC_11, amp[3567]); 
  FFV2_3_0(w[276], w[344], w[3], pars->GC_50, pars->GC_58, amp[3568]); 
  FFV2_3_0(w[273], w[344], w[2], pars->GC_50, pars->GC_58, amp[3569]); 
  FFV2_3_0(w[345], w[243], w[3], pars->GC_50, pars->GC_58, amp[3570]); 
  FFV1_0(w[273], w[243], w[334], pars->GC_11, amp[3571]); 
  FFV2_3_0(w[345], w[249], w[2], pars->GC_50, pars->GC_58, amp[3572]); 
  FFV1_0(w[276], w[249], w[334], pars->GC_11, amp[3573]); 
  FFV1_0(w[273], w[107], w[387], pars->GC_11, amp[3574]); 
  FFV1_0(w[284], w[358], w[161], pars->GC_11, amp[3575]); 
  FFV1_0(w[284], w[355], w[274], pars->GC_11, amp[3576]); 
  FFV1_0(w[284], w[361], w[277], pars->GC_11, amp[3577]); 
  FFV1_0(w[276], w[107], w[388], pars->GC_11, amp[3578]); 
  FFV1_0(w[284], w[367], w[161], pars->GC_11, amp[3579]); 
  FFV1_0(w[391], w[361], w[120], pars->GC_11, amp[3580]); 
  FFV1_0(w[369], w[392], w[120], pars->GC_11, amp[3581]); 
  FFV1_0(w[369], w[361], w[174], pars->GC_11, amp[3582]); 
  FFV1_0(w[391], w[283], w[255], pars->GC_11, amp[3583]); 
  FFV1_0(w[108], w[279], w[383], pars->GC_11, amp[3584]); 
  VVV1_0(w[4], w[383], w[255], pars->GC_10, amp[3585]); 
  FFV1_0(w[391], w[283], w[258], pars->GC_11, amp[3586]); 
  FFV1_0(w[280], w[107], w[383], pars->GC_11, amp[3587]); 
  VVV1_0(w[4], w[383], w[258], pars->GC_10, amp[3588]); 
  FFV2_3_0(w[391], w[306], w[3], pars->GC_50, pars->GC_58, amp[3589]); 
  FFV1_0(w[372], w[283], w[174], pars->GC_11, amp[3590]); 
  FFV1_0(w[372], w[306], w[4], pars->GC_11, amp[3591]); 
  FFV1_0(w[393], w[355], w[120], pars->GC_11, amp[3592]); 
  FFV1_0(w[375], w[394], w[120], pars->GC_11, amp[3593]); 
  FFV1_0(w[375], w[355], w[174], pars->GC_11, amp[3594]); 
  FFV1_0(w[393], w[283], w[244], pars->GC_11, amp[3595]); 
  FFV1_0(w[108], w[281], w[385], pars->GC_11, amp[3596]); 
  VVV1_0(w[4], w[385], w[244], pars->GC_10, amp[3597]); 
  FFV1_0(w[393], w[283], w[252], pars->GC_11, amp[3598]); 
  FFV1_0(w[282], w[107], w[385], pars->GC_11, amp[3599]); 
  VVV1_0(w[4], w[385], w[252], pars->GC_10, amp[3600]); 
  FFV2_3_0(w[393], w[306], w[2], pars->GC_50, pars->GC_58, amp[3601]); 
  FFV1_0(w[378], w[283], w[174], pars->GC_11, amp[3602]); 
  FFV1_0(w[378], w[306], w[4], pars->GC_11, amp[3603]); 
  FFV1_0(w[108], w[245], w[353], pars->GC_11, amp[3604]); 
  FFV2_3_0(w[335], w[281], w[3], pars->GC_50, pars->GC_58, amp[3605]); 
  FFV1_0(w[335], w[245], w[4], pars->GC_11, amp[3606]); 
  FFV1_0(w[247], w[243], w[353], pars->GC_11, amp[3607]); 
  FFV1_0(w[247], w[281], w[334], pars->GC_11, amp[3608]); 
  FFV1_0(w[280], w[243], w[334], pars->GC_11, amp[3609]); 
  FFV1_0(w[254], w[107], w[353], pars->GC_11, amp[3610]); 
  FFV2_3_0(w[282], w[344], w[3], pars->GC_50, pars->GC_58, amp[3611]); 
  FFV1_0(w[254], w[344], w[4], pars->GC_11, amp[3612]); 
  FFV1_0(w[251], w[249], w[353], pars->GC_11, amp[3613]); 
  FFV1_0(w[282], w[249], w[334], pars->GC_11, amp[3614]); 
  FFV1_0(w[251], w[279], w[334], pars->GC_11, amp[3615]); 
  FFV1_0(w[108], w[256], w[353], pars->GC_11, amp[3616]); 
  FFV2_3_0(w[335], w[279], w[2], pars->GC_50, pars->GC_58, amp[3617]); 
  FFV1_0(w[335], w[256], w[4], pars->GC_11, amp[3618]); 
  FFV1_0(w[260], w[107], w[353], pars->GC_11, amp[3619]); 
  FFV2_3_0(w[280], w[344], w[2], pars->GC_50, pars->GC_58, amp[3620]); 
  FFV1_0(w[260], w[344], w[4], pars->GC_11, amp[3621]); 
  FFV1_0(w[284], w[394], w[255], pars->GC_11, amp[3622]); 
  FFV1_0(w[108], w[279], w[387], pars->GC_11, amp[3623]); 
  VVV1_0(w[4], w[387], w[255], pars->GC_10, amp[3624]); 
  FFV1_0(w[284], w[394], w[258], pars->GC_11, amp[3625]); 
  FFV1_0(w[280], w[107], w[387], pars->GC_11, amp[3626]); 
  VVV1_0(w[4], w[387], w[258], pars->GC_10, amp[3627]); 
  FFV2_3_0(w[328], w[394], w[3], pars->GC_50, pars->GC_58, amp[3628]); 
  FFV1_0(w[284], w[358], w[174], pars->GC_11, amp[3629]); 
  FFV1_0(w[328], w[358], w[4], pars->GC_11, amp[3630]); 
  FFV1_0(w[108], w[281], w[388], pars->GC_11, amp[3631]); 
  FFV1_0(w[284], w[392], w[244], pars->GC_11, amp[3632]); 
  VVV1_0(w[4], w[388], w[244], pars->GC_10, amp[3633]); 
  FFV1_0(w[282], w[107], w[388], pars->GC_11, amp[3634]); 
  FFV1_0(w[284], w[392], w[252], pars->GC_11, amp[3635]); 
  VVV1_0(w[4], w[388], w[252], pars->GC_10, amp[3636]); 
  FFV2_3_0(w[328], w[392], w[2], pars->GC_50, pars->GC_58, amp[3637]); 
  FFV1_0(w[284], w[367], w[174], pars->GC_11, amp[3638]); 
  FFV1_0(w[328], w[367], w[4], pars->GC_11, amp[3639]); 


}
double PY8MEs_R12_P10_sm_qq_zzgqq::matrix_12_uu_zzguu() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 280;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[0] + amp[1] + 1./3. * amp[2] + amp[3] + 1./3. *
      amp[4] + amp[5] + 1./3. * amp[6] + 1./3. * amp[7] + 1./3. * amp[8] +
      1./3. * amp[9] + amp[10] + 1./3. * amp[11] + amp[12] + 1./3. * amp[13] +
      amp[14] + amp[15] + amp[16] + 1./3. * amp[17] + 1./3. * amp[18] + amp[19]
      + 1./3. * amp[20] + amp[21] + amp[22] + 1./3. * amp[23] + amp[24] +
      amp[25] + 1./3. * amp[26] + 1./3. * amp[27] + 1./3. * amp[28] + 1./3. *
      amp[29] + amp[30] + amp[31] + amp[32] + 1./3. * amp[33] + 1./3. * amp[34]
      + amp[35] + amp[36] + amp[37] + 1./3. * amp[38] + 1./3. * amp[39] +
      amp[40] + amp[41] + 1./3. * amp[42] + amp[43] + 1./3. * amp[44] + amp[45]
      + 1./3. * amp[46] + 1./3. * amp[47] + amp[48] + amp[49] + 1./3. * amp[50]
      + amp[51] + 1./3. * amp[52] + amp[53] + 1./3. * amp[54] + 1./3. * amp[55]
      + amp[56] + amp[57] + amp[58] + amp[59] + amp[60] + amp[61] + 1./3. *
      amp[62] + amp[63] + 1./3. * amp[64] + amp[65] + 1./3. * amp[66] + amp[67]
      + 1./3. * amp[68] + 1./3. * amp[69] + 1./3. * amp[70] + amp[71] + 1./3. *
      amp[72] + amp[73] + 1./3. * amp[74] + amp[75] + 1./3. * amp[76] + 1./3. *
      amp[77] + 1./3. * amp[78] + 1./3. * amp[79] + amp[160] + amp[161] -
      Complex<double> (0, 1) * amp[162] + 1./3. * amp[163] + 1./3. * amp[164] +
      1./3. * amp[166] + amp[169] + Complex<double> (0, 1) * amp[171] +
      amp[172] + 1./3. * amp[173] - Complex<double> (0, 1) * amp[177] +
      amp[178] + Complex<double> (0, 1) * amp[179] + amp[180] + 1./3. *
      amp[181] + 1./3. * amp[183] + amp[184] + amp[185] - Complex<double> (0,
      1) * amp[186] + 1./3. * amp[187] + 1./3. * amp[188] + 1./3. * amp[190] +
      amp[193] + Complex<double> (0, 1) * amp[195] + amp[196] + 1./3. *
      amp[197] - Complex<double> (0, 1) * amp[201] + amp[202] + Complex<double>
      (0, 1) * amp[203] + amp[204] + 1./3. * amp[205] + 1./3. * amp[207] -
      Complex<double> (0, 1) * amp[226] + amp[227] + amp[228] - Complex<double>
      (0, 1) * amp[229] + amp[230] - Complex<double> (0, 1) * amp[232] -
      Complex<double> (0, 1) * amp[235] + amp[237] - Complex<double> (0, 1) *
      amp[238] + amp[239] + amp[240] - Complex<double> (0, 1) * amp[241] +
      amp[244] + 1./3. * amp[245] - Complex<double> (0, 1) * amp[249] + 1./3. *
      amp[250] + 1./3. * amp[253] + 1./3. * amp[255] + amp[258] + 1./3. *
      amp[259] - Complex<double> (0, 1) * amp[260] - Complex<double> (0, 1) *
      amp[264] + Complex<double> (0, 1) * amp[266] + 1./3. * amp[269] -
      Complex<double> (0, 1) * amp[273] + 1./3. * amp[274] + 1./3. * amp[276] +
      Complex<double> (0, 1) * amp[278]);
  jamp[1] = +1./2. * (-1./3. * amp[40] - 1./3. * amp[41] - amp[42] - 1./3. *
      amp[43] - amp[44] - 1./3. * amp[45] - amp[46] - amp[47] - 1./3. * amp[48]
      - 1./3. * amp[49] - amp[50] - 1./3. * amp[51] - amp[52] - 1./3. * amp[53]
      - amp[54] - amp[55] - 1./3. * amp[56] - 1./3. * amp[57] - 1./3. * amp[58]
      - 1./3. * amp[59] - 1./3. * amp[60] - 1./3. * amp[61] - amp[62] - 1./3. *
      amp[63] - amp[64] - 1./3. * amp[65] - amp[66] - 1./3. * amp[67] - amp[68]
      - amp[69] - amp[70] - 1./3. * amp[71] - amp[72] - 1./3. * amp[73] -
      amp[74] - 1./3. * amp[75] - amp[76] - amp[77] - amp[78] - amp[79] -
      amp[120] - 1./3. * amp[121] - 1./3. * amp[122] - amp[123] - 1./3. *
      amp[124] - amp[125] - amp[126] - 1./3. * amp[127] - amp[128] - 1./3. *
      amp[129] - 1./3. * amp[130] - amp[131] - 1./3. * amp[132] - amp[133] -
      amp[134] - 1./3. * amp[135] - amp[136] - amp[137] - amp[138] - amp[139] -
      amp[140] - amp[141] - 1./3. * amp[142] - 1./3. * amp[143] - 1./3. *
      amp[144] - 1./3. * amp[145] - 1./3. * amp[146] - 1./3. * amp[147] -
      amp[148] - amp[149] - 1./3. * amp[150] - amp[151] - 1./3. * amp[152] -
      1./3. * amp[153] - amp[154] - 1./3. * amp[155] - amp[156] - amp[157] -
      1./3. * amp[158] - 1./3. * amp[159] - 1./3. * amp[161] - amp[164] +
      Complex<double> (0, 1) * amp[165] - Complex<double> (0, 1) * amp[168] -
      amp[174] - 1./3. * amp[175] - Complex<double> (0, 1) * amp[176] +
      Complex<double> (0, 1) * amp[182] - 1./3. * amp[185] - amp[188] +
      Complex<double> (0, 1) * amp[189] - Complex<double> (0, 1) * amp[192] -
      amp[198] - 1./3. * amp[199] - Complex<double> (0, 1) * amp[200] +
      Complex<double> (0, 1) * amp[206] - Complex<double> (0, 1) * amp[208] -
      Complex<double> (0, 1) * amp[211] - amp[213] - Complex<double> (0, 1) *
      amp[214] - amp[215] - amp[216] - Complex<double> (0, 1) * amp[217] -
      amp[218] - Complex<double> (0, 1) * amp[220] - Complex<double> (0, 1) *
      amp[223] - amp[224] - amp[225] - 1./3. * amp[227] - 1./3. * amp[228] -
      1./3. * amp[230] - 1./3. * amp[231] - 1./3. * amp[233] - 1./3. * amp[234]
      - 1./3. * amp[236] - 1./3. * amp[237] - 1./3. * amp[239] - 1./3. *
      amp[240] - 1./3. * amp[242] - 1./3. * amp[243] - 1./3. * amp[244] -
      amp[245] - Complex<double> (0, 1) * amp[248] - amp[250] - amp[251] -
      Complex<double> (0, 1) * amp[252] - amp[253] + Complex<double> (0, 1) *
      amp[254] - amp[255] - 1./3. * amp[258] - amp[259] - Complex<double> (0,
      1) * amp[261] - 1./3. * amp[263] - amp[268] - amp[269] - Complex<double>
      (0, 1) * amp[270] - 1./3. * amp[271] - amp[274] + Complex<double> (0, 1)
      * amp[275] - amp[276]);
  jamp[2] = +1./2. * (-1./3. * amp[0] - 1./3. * amp[1] - amp[2] - 1./3. *
      amp[3] - amp[4] - 1./3. * amp[5] - amp[6] - amp[7] - amp[8] - amp[9] -
      1./3. * amp[10] - amp[11] - 1./3. * amp[12] - amp[13] - 1./3. * amp[14] -
      1./3. * amp[15] - 1./3. * amp[16] - amp[17] - amp[18] - 1./3. * amp[19] -
      amp[20] - 1./3. * amp[21] - 1./3. * amp[22] - amp[23] - 1./3. * amp[24] -
      1./3. * amp[25] - amp[26] - amp[27] - amp[28] - amp[29] - 1./3. * amp[30]
      - 1./3. * amp[31] - 1./3. * amp[32] - amp[33] - amp[34] - 1./3. * amp[35]
      - 1./3. * amp[36] - 1./3. * amp[37] - amp[38] - amp[39] - amp[80] -
      amp[81] - 1./3. * amp[82] - amp[83] - 1./3. * amp[84] - amp[85] - 1./3. *
      amp[86] - 1./3. * amp[87] - amp[88] - amp[89] - 1./3. * amp[90] - amp[91]
      - 1./3. * amp[92] - amp[93] - 1./3. * amp[94] - 1./3. * amp[95] - amp[96]
      - amp[97] - amp[98] - amp[99] - amp[100] - amp[101] - 1./3. * amp[102] -
      amp[103] - 1./3. * amp[104] - amp[105] - 1./3. * amp[106] - amp[107] -
      1./3. * amp[108] - 1./3. * amp[109] - 1./3. * amp[110] - amp[111] - 1./3.
      * amp[112] - amp[113] - 1./3. * amp[114] - amp[115] - 1./3. * amp[116] -
      1./3. * amp[117] - 1./3. * amp[118] - 1./3. * amp[119] - 1./3. * amp[160]
      - amp[163] - Complex<double> (0, 1) * amp[165] - amp[166] - amp[167] +
      Complex<double> (0, 1) * amp[168] - 1./3. * amp[169] - 1./3. * amp[170] -
      1./3. * amp[172] - amp[173] + Complex<double> (0, 1) * amp[176] - 1./3. *
      amp[178] - 1./3. * amp[180] - amp[181] - Complex<double> (0, 1) *
      amp[182] - amp[183] - 1./3. * amp[184] - amp[187] - Complex<double> (0,
      1) * amp[189] - amp[190] - amp[191] + Complex<double> (0, 1) * amp[192] -
      1./3. * amp[193] - 1./3. * amp[194] - 1./3. * amp[196] - amp[197] +
      Complex<double> (0, 1) * amp[200] - 1./3. * amp[202] - 1./3. * amp[204] -
      amp[205] - Complex<double> (0, 1) * amp[206] - amp[207] + Complex<double>
      (0, 1) * amp[208] - amp[209] - amp[210] + Complex<double> (0, 1) *
      amp[211] - amp[212] + Complex<double> (0, 1) * amp[214] + Complex<double>
      (0, 1) * amp[217] - amp[219] + Complex<double> (0, 1) * amp[220] -
      amp[221] - amp[222] + Complex<double> (0, 1) * amp[223] - amp[246] -
      1./3. * amp[247] + Complex<double> (0, 1) * amp[248] + Complex<double>
      (0, 1) * amp[252] - Complex<double> (0, 1) * amp[254] - amp[256] - 1./3.
      * amp[257] + Complex<double> (0, 1) * amp[261] - 1./3. * amp[262] - 1./3.
      * amp[265] - 1./3. * amp[267] + Complex<double> (0, 1) * amp[270] - 1./3.
      * amp[272] - Complex<double> (0, 1) * amp[275] - 1./3. * amp[277] - 1./3.
      * amp[279]);
  jamp[3] = +1./2. * (+1./3. * amp[80] + 1./3. * amp[81] + amp[82] + 1./3. *
      amp[83] + amp[84] + 1./3. * amp[85] + amp[86] + amp[87] + 1./3. * amp[88]
      + 1./3. * amp[89] + amp[90] + 1./3. * amp[91] + amp[92] + 1./3. * amp[93]
      + amp[94] + amp[95] + 1./3. * amp[96] + 1./3. * amp[97] + 1./3. * amp[98]
      + 1./3. * amp[99] + 1./3. * amp[100] + 1./3. * amp[101] + amp[102] +
      1./3. * amp[103] + amp[104] + 1./3. * amp[105] + amp[106] + 1./3. *
      amp[107] + amp[108] + amp[109] + amp[110] + 1./3. * amp[111] + amp[112] +
      1./3. * amp[113] + amp[114] + 1./3. * amp[115] + amp[116] + amp[117] +
      amp[118] + amp[119] + 1./3. * amp[120] + amp[121] + amp[122] + 1./3. *
      amp[123] + amp[124] + 1./3. * amp[125] + 1./3. * amp[126] + amp[127] +
      1./3. * amp[128] + amp[129] + amp[130] + 1./3. * amp[131] + amp[132] +
      1./3. * amp[133] + 1./3. * amp[134] + amp[135] + 1./3. * amp[136] + 1./3.
      * amp[137] + 1./3. * amp[138] + 1./3. * amp[139] + 1./3. * amp[140] +
      1./3. * amp[141] + amp[142] + amp[143] + amp[144] + amp[145] + amp[146] +
      amp[147] + 1./3. * amp[148] + 1./3. * amp[149] + amp[150] + 1./3. *
      amp[151] + amp[152] + amp[153] + 1./3. * amp[154] + amp[155] + 1./3. *
      amp[156] + 1./3. * amp[157] + amp[158] + amp[159] + Complex<double> (0,
      1) * amp[162] + 1./3. * amp[167] + amp[170] - Complex<double> (0, 1) *
      amp[171] + 1./3. * amp[174] + amp[175] + Complex<double> (0, 1) *
      amp[177] - Complex<double> (0, 1) * amp[179] + Complex<double> (0, 1) *
      amp[186] + 1./3. * amp[191] + amp[194] - Complex<double> (0, 1) *
      amp[195] + 1./3. * amp[198] + amp[199] + Complex<double> (0, 1) *
      amp[201] - Complex<double> (0, 1) * amp[203] + 1./3. * amp[209] + 1./3. *
      amp[210] + 1./3. * amp[212] + 1./3. * amp[213] + 1./3. * amp[215] + 1./3.
      * amp[216] + 1./3. * amp[218] + 1./3. * amp[219] + 1./3. * amp[221] +
      1./3. * amp[222] + 1./3. * amp[224] + 1./3. * amp[225] + Complex<double>
      (0, 1) * amp[226] + Complex<double> (0, 1) * amp[229] + amp[231] +
      Complex<double> (0, 1) * amp[232] + amp[233] + amp[234] + Complex<double>
      (0, 1) * amp[235] + amp[236] + Complex<double> (0, 1) * amp[238] +
      Complex<double> (0, 1) * amp[241] + amp[242] + amp[243] + 1./3. *
      amp[246] + amp[247] + Complex<double> (0, 1) * amp[249] + 1./3. *
      amp[251] + 1./3. * amp[256] + amp[257] + Complex<double> (0, 1) *
      amp[260] + amp[262] + amp[263] + Complex<double> (0, 1) * amp[264] +
      amp[265] - Complex<double> (0, 1) * amp[266] + amp[267] + 1./3. *
      amp[268] + amp[271] + amp[272] + Complex<double> (0, 1) * amp[273] +
      amp[277] - Complex<double> (0, 1) * amp[278] + amp[279]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[0][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P10_sm_qq_zzgqq::matrix_12_uux_zzguux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 280;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[360] + 1./3. * amp[361] + amp[362] + 1./3. *
      amp[363] + amp[364] + 1./3. * amp[365] + amp[366] + amp[367] + 1./3. *
      amp[368] + 1./3. * amp[369] + amp[370] + 1./3. * amp[371] + amp[372] +
      1./3. * amp[373] + amp[374] + amp[375] + 1./3. * amp[376] + 1./3. *
      amp[377] + 1./3. * amp[378] + 1./3. * amp[379] + 1./3. * amp[380] + 1./3.
      * amp[381] + amp[382] + 1./3. * amp[383] + amp[384] + 1./3. * amp[385] +
      amp[386] + 1./3. * amp[387] + amp[388] + amp[389] + amp[390] + 1./3. *
      amp[391] + amp[392] + 1./3. * amp[393] + amp[394] + 1./3. * amp[395] +
      amp[396] + amp[397] + amp[398] + amp[399] + 1./3. * amp[400] + amp[401] +
      amp[402] + 1./3. * amp[403] + amp[404] + 1./3. * amp[405] + 1./3. *
      amp[406] + amp[407] + 1./3. * amp[408] + amp[409] + amp[410] + 1./3. *
      amp[411] + amp[412] + 1./3. * amp[413] + 1./3. * amp[414] + amp[415] +
      1./3. * amp[416] + 1./3. * amp[417] + 1./3. * amp[418] + 1./3. * amp[419]
      + 1./3. * amp[420] + 1./3. * amp[421] + amp[422] + amp[423] + amp[424] +
      amp[425] + amp[426] + amp[427] + 1./3. * amp[428] + 1./3. * amp[429] +
      amp[430] + 1./3. * amp[431] + amp[432] + amp[433] + 1./3. * amp[434] +
      amp[435] + 1./3. * amp[436] + 1./3. * amp[437] + amp[438] + amp[439] +
      Complex<double> (0, 1) * amp[442] + 1./3. * amp[447] + amp[450] -
      Complex<double> (0, 1) * amp[451] + 1./3. * amp[454] + amp[455] +
      Complex<double> (0, 1) * amp[457] - Complex<double> (0, 1) * amp[459] +
      Complex<double> (0, 1) * amp[466] + 1./3. * amp[471] + amp[474] -
      Complex<double> (0, 1) * amp[475] + 1./3. * amp[478] + amp[479] +
      Complex<double> (0, 1) * amp[481] - Complex<double> (0, 1) * amp[483] +
      1./3. * amp[489] + 1./3. * amp[490] + 1./3. * amp[492] + 1./3. * amp[493]
      + 1./3. * amp[495] + 1./3. * amp[496] + 1./3. * amp[498] + 1./3. *
      amp[499] + 1./3. * amp[501] + 1./3. * amp[502] + 1./3. * amp[504] + 1./3.
      * amp[505] + Complex<double> (0, 1) * amp[506] + Complex<double> (0, 1) *
      amp[509] + amp[511] + Complex<double> (0, 1) * amp[512] + amp[513] +
      amp[514] + Complex<double> (0, 1) * amp[515] + amp[516] + Complex<double>
      (0, 1) * amp[518] + Complex<double> (0, 1) * amp[521] + amp[522] +
      amp[523] + 1./3. * amp[526] + amp[527] + Complex<double> (0, 1) *
      amp[529] + 1./3. * amp[531] + 1./3. * amp[536] + amp[537] +
      Complex<double> (0, 1) * amp[540] + amp[542] + amp[543] + Complex<double>
      (0, 1) * amp[544] + amp[545] - Complex<double> (0, 1) * amp[546] +
      amp[547] + 1./3. * amp[548] + amp[551] + amp[552] + Complex<double> (0,
      1) * amp[553] + amp[557] - Complex<double> (0, 1) * amp[558] + amp[559]);
  jamp[1] = +1./2. * (-1./3. * amp[280] - 1./3. * amp[281] - amp[282] - 1./3. *
      amp[283] - amp[284] - 1./3. * amp[285] - amp[286] - amp[287] - amp[288] -
      amp[289] - 1./3. * amp[290] - amp[291] - 1./3. * amp[292] - amp[293] -
      1./3. * amp[294] - 1./3. * amp[295] - 1./3. * amp[296] - amp[297] -
      amp[298] - 1./3. * amp[299] - amp[300] - 1./3. * amp[301] - 1./3. *
      amp[302] - amp[303] - 1./3. * amp[304] - 1./3. * amp[305] - amp[306] -
      amp[307] - amp[308] - amp[309] - 1./3. * amp[310] - 1./3. * amp[311] -
      1./3. * amp[312] - amp[313] - amp[314] - 1./3. * amp[315] - 1./3. *
      amp[316] - 1./3. * amp[317] - amp[318] - amp[319] - amp[360] - amp[361] -
      1./3. * amp[362] - amp[363] - 1./3. * amp[364] - amp[365] - 1./3. *
      amp[366] - 1./3. * amp[367] - amp[368] - amp[369] - 1./3. * amp[370] -
      amp[371] - 1./3. * amp[372] - amp[373] - 1./3. * amp[374] - 1./3. *
      amp[375] - amp[376] - amp[377] - amp[378] - amp[379] - amp[380] -
      amp[381] - 1./3. * amp[382] - amp[383] - 1./3. * amp[384] - amp[385] -
      1./3. * amp[386] - amp[387] - 1./3. * amp[388] - 1./3. * amp[389] - 1./3.
      * amp[390] - amp[391] - 1./3. * amp[392] - amp[393] - 1./3. * amp[394] -
      amp[395] - 1./3. * amp[396] - 1./3. * amp[397] - 1./3. * amp[398] - 1./3.
      * amp[399] - 1./3. * amp[440] - amp[443] - Complex<double> (0, 1) *
      amp[445] - amp[446] - amp[447] + Complex<double> (0, 1) * amp[448] -
      1./3. * amp[449] - 1./3. * amp[450] - 1./3. * amp[452] - amp[453] +
      Complex<double> (0, 1) * amp[456] - 1./3. * amp[458] - 1./3. * amp[460] -
      amp[461] - Complex<double> (0, 1) * amp[462] - amp[463] - 1./3. *
      amp[464] - amp[467] - Complex<double> (0, 1) * amp[469] - amp[470] -
      amp[471] + Complex<double> (0, 1) * amp[472] - 1./3. * amp[473] - 1./3. *
      amp[474] - 1./3. * amp[476] - amp[477] + Complex<double> (0, 1) *
      amp[480] - 1./3. * amp[482] - 1./3. * amp[484] - amp[485] -
      Complex<double> (0, 1) * amp[486] - amp[487] + Complex<double> (0, 1) *
      amp[488] - amp[489] - amp[490] + Complex<double> (0, 1) * amp[491] -
      amp[492] + Complex<double> (0, 1) * amp[494] + Complex<double> (0, 1) *
      amp[497] - amp[499] + Complex<double> (0, 1) * amp[500] - amp[501] -
      amp[502] + Complex<double> (0, 1) * amp[503] - amp[526] - 1./3. *
      amp[527] + Complex<double> (0, 1) * amp[528] + Complex<double> (0, 1) *
      amp[532] - Complex<double> (0, 1) * amp[534] - amp[536] - 1./3. *
      amp[537] + Complex<double> (0, 1) * amp[541] - 1./3. * amp[542] - 1./3. *
      amp[545] - 1./3. * amp[547] + Complex<double> (0, 1) * amp[550] - 1./3. *
      amp[552] - Complex<double> (0, 1) * amp[555] - 1./3. * amp[557] - 1./3. *
      amp[559]);
  jamp[2] = +1./2. * (+amp[280] + amp[281] + 1./3. * amp[282] + amp[283] +
      1./3. * amp[284] + amp[285] + 1./3. * amp[286] + 1./3. * amp[287] + 1./3.
      * amp[288] + 1./3. * amp[289] + amp[290] + 1./3. * amp[291] + amp[292] +
      1./3. * amp[293] + amp[294] + amp[295] + amp[296] + 1./3. * amp[297] +
      1./3. * amp[298] + amp[299] + 1./3. * amp[300] + amp[301] + amp[302] +
      1./3. * amp[303] + amp[304] + amp[305] + 1./3. * amp[306] + 1./3. *
      amp[307] + 1./3. * amp[308] + 1./3. * amp[309] + amp[310] + amp[311] +
      amp[312] + 1./3. * amp[313] + 1./3. * amp[314] + amp[315] + amp[316] +
      amp[317] + 1./3. * amp[318] + 1./3. * amp[319] + amp[320] + amp[321] +
      1./3. * amp[322] + amp[323] + 1./3. * amp[324] + amp[325] + 1./3. *
      amp[326] + 1./3. * amp[327] + amp[328] + amp[329] + 1./3. * amp[330] +
      amp[331] + 1./3. * amp[332] + amp[333] + 1./3. * amp[334] + 1./3. *
      amp[335] + amp[336] + amp[337] + amp[338] + amp[339] + amp[340] +
      amp[341] + 1./3. * amp[342] + amp[343] + 1./3. * amp[344] + amp[345] +
      1./3. * amp[346] + amp[347] + 1./3. * amp[348] + 1./3. * amp[349] + 1./3.
      * amp[350] + amp[351] + 1./3. * amp[352] + amp[353] + 1./3. * amp[354] +
      amp[355] + 1./3. * amp[356] + 1./3. * amp[357] + 1./3. * amp[358] + 1./3.
      * amp[359] + amp[440] + amp[441] - Complex<double> (0, 1) * amp[442] +
      1./3. * amp[443] + 1./3. * amp[444] + 1./3. * amp[446] + amp[449] +
      Complex<double> (0, 1) * amp[451] + amp[452] + 1./3. * amp[453] -
      Complex<double> (0, 1) * amp[457] + amp[458] + Complex<double> (0, 1) *
      amp[459] + amp[460] + 1./3. * amp[461] + 1./3. * amp[463] + amp[464] +
      amp[465] - Complex<double> (0, 1) * amp[466] + 1./3. * amp[467] + 1./3. *
      amp[468] + 1./3. * amp[470] + amp[473] + Complex<double> (0, 1) *
      amp[475] + amp[476] + 1./3. * amp[477] - Complex<double> (0, 1) *
      amp[481] + amp[482] + Complex<double> (0, 1) * amp[483] + amp[484] +
      1./3. * amp[485] + 1./3. * amp[487] - Complex<double> (0, 1) * amp[506] +
      amp[507] + amp[508] - Complex<double> (0, 1) * amp[509] + amp[510] -
      Complex<double> (0, 1) * amp[512] - Complex<double> (0, 1) * amp[515] +
      amp[517] - Complex<double> (0, 1) * amp[518] + amp[519] + amp[520] -
      Complex<double> (0, 1) * amp[521] + amp[524] + 1./3. * amp[525] -
      Complex<double> (0, 1) * amp[529] + 1./3. * amp[530] + 1./3. * amp[533] +
      1./3. * amp[535] + amp[538] + 1./3. * amp[539] - Complex<double> (0, 1) *
      amp[540] - Complex<double> (0, 1) * amp[544] + Complex<double> (0, 1) *
      amp[546] + 1./3. * amp[549] - Complex<double> (0, 1) * amp[553] + 1./3. *
      amp[554] + 1./3. * amp[556] + Complex<double> (0, 1) * amp[558]);
  jamp[3] = +1./2. * (-1./3. * amp[320] - 1./3. * amp[321] - amp[322] - 1./3. *
      amp[323] - amp[324] - 1./3. * amp[325] - amp[326] - amp[327] - 1./3. *
      amp[328] - 1./3. * amp[329] - amp[330] - 1./3. * amp[331] - amp[332] -
      1./3. * amp[333] - amp[334] - amp[335] - 1./3. * amp[336] - 1./3. *
      amp[337] - 1./3. * amp[338] - 1./3. * amp[339] - 1./3. * amp[340] - 1./3.
      * amp[341] - amp[342] - 1./3. * amp[343] - amp[344] - 1./3. * amp[345] -
      amp[346] - 1./3. * amp[347] - amp[348] - amp[349] - amp[350] - 1./3. *
      amp[351] - amp[352] - 1./3. * amp[353] - amp[354] - 1./3. * amp[355] -
      amp[356] - amp[357] - amp[358] - amp[359] - amp[400] - 1./3. * amp[401] -
      1./3. * amp[402] - amp[403] - 1./3. * amp[404] - amp[405] - amp[406] -
      1./3. * amp[407] - amp[408] - 1./3. * amp[409] - 1./3. * amp[410] -
      amp[411] - 1./3. * amp[412] - amp[413] - amp[414] - 1./3. * amp[415] -
      amp[416] - amp[417] - amp[418] - amp[419] - amp[420] - amp[421] - 1./3. *
      amp[422] - 1./3. * amp[423] - 1./3. * amp[424] - 1./3. * amp[425] - 1./3.
      * amp[426] - 1./3. * amp[427] - amp[428] - amp[429] - 1./3. * amp[430] -
      amp[431] - 1./3. * amp[432] - 1./3. * amp[433] - amp[434] - 1./3. *
      amp[435] - amp[436] - amp[437] - 1./3. * amp[438] - 1./3. * amp[439] -
      1./3. * amp[441] - amp[444] + Complex<double> (0, 1) * amp[445] -
      Complex<double> (0, 1) * amp[448] - amp[454] - 1./3. * amp[455] -
      Complex<double> (0, 1) * amp[456] + Complex<double> (0, 1) * amp[462] -
      1./3. * amp[465] - amp[468] + Complex<double> (0, 1) * amp[469] -
      Complex<double> (0, 1) * amp[472] - amp[478] - 1./3. * amp[479] -
      Complex<double> (0, 1) * amp[480] + Complex<double> (0, 1) * amp[486] -
      Complex<double> (0, 1) * amp[488] - Complex<double> (0, 1) * amp[491] -
      amp[493] - Complex<double> (0, 1) * amp[494] - amp[495] - amp[496] -
      Complex<double> (0, 1) * amp[497] - amp[498] - Complex<double> (0, 1) *
      amp[500] - Complex<double> (0, 1) * amp[503] - amp[504] - amp[505] -
      1./3. * amp[507] - 1./3. * amp[508] - 1./3. * amp[510] - 1./3. * amp[511]
      - 1./3. * amp[513] - 1./3. * amp[514] - 1./3. * amp[516] - 1./3. *
      amp[517] - 1./3. * amp[519] - 1./3. * amp[520] - 1./3. * amp[522] - 1./3.
      * amp[523] - 1./3. * amp[524] - amp[525] - Complex<double> (0, 1) *
      amp[528] - amp[530] - amp[531] - Complex<double> (0, 1) * amp[532] -
      amp[533] + Complex<double> (0, 1) * amp[534] - amp[535] - 1./3. *
      amp[538] - amp[539] - Complex<double> (0, 1) * amp[541] - 1./3. *
      amp[543] - amp[548] - amp[549] - Complex<double> (0, 1) * amp[550] -
      1./3. * amp[551] - amp[554] + Complex<double> (0, 1) * amp[555] -
      amp[556]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[1][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P10_sm_qq_zzgqq::matrix_12_dd_zzgdd() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 280;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[560] + amp[561] + 1./3. * amp[562] + amp[563] +
      1./3. * amp[564] + amp[565] + 1./3. * amp[566] + 1./3. * amp[567] + 1./3.
      * amp[568] + 1./3. * amp[569] + amp[570] + 1./3. * amp[571] + amp[572] +
      1./3. * amp[573] + amp[574] + amp[575] + amp[576] + 1./3. * amp[577] +
      1./3. * amp[578] + amp[579] + 1./3. * amp[580] + amp[581] + amp[582] +
      1./3. * amp[583] + amp[584] + amp[585] + 1./3. * amp[586] + 1./3. *
      amp[587] + 1./3. * amp[588] + 1./3. * amp[589] + amp[590] + amp[591] +
      amp[592] + 1./3. * amp[593] + 1./3. * amp[594] + amp[595] + amp[596] +
      amp[597] + 1./3. * amp[598] + 1./3. * amp[599] + amp[600] + amp[601] +
      1./3. * amp[602] + amp[603] + 1./3. * amp[604] + amp[605] + 1./3. *
      amp[606] + 1./3. * amp[607] + amp[608] + amp[609] + 1./3. * amp[610] +
      amp[611] + 1./3. * amp[612] + amp[613] + 1./3. * amp[614] + 1./3. *
      amp[615] + amp[616] + amp[617] + amp[618] + amp[619] + amp[620] +
      amp[621] + 1./3. * amp[622] + amp[623] + 1./3. * amp[624] + amp[625] +
      1./3. * amp[626] + amp[627] + 1./3. * amp[628] + 1./3. * amp[629] + 1./3.
      * amp[630] + amp[631] + 1./3. * amp[632] + amp[633] + 1./3. * amp[634] +
      amp[635] + 1./3. * amp[636] + 1./3. * amp[637] + 1./3. * amp[638] + 1./3.
      * amp[639] + amp[720] + amp[721] - Complex<double> (0, 1) * amp[722] +
      1./3. * amp[723] + 1./3. * amp[724] + 1./3. * amp[726] + amp[729] +
      Complex<double> (0, 1) * amp[731] + amp[732] + 1./3. * amp[733] -
      Complex<double> (0, 1) * amp[737] + amp[738] + Complex<double> (0, 1) *
      amp[739] + amp[740] + 1./3. * amp[741] + 1./3. * amp[743] + amp[744] +
      amp[745] - Complex<double> (0, 1) * amp[746] + 1./3. * amp[747] + 1./3. *
      amp[748] + 1./3. * amp[750] + amp[753] + Complex<double> (0, 1) *
      amp[755] + amp[756] + 1./3. * amp[757] - Complex<double> (0, 1) *
      amp[761] + amp[762] + Complex<double> (0, 1) * amp[763] + amp[764] +
      1./3. * amp[765] + 1./3. * amp[767] - Complex<double> (0, 1) * amp[786] +
      amp[787] + amp[788] - Complex<double> (0, 1) * amp[789] + amp[790] -
      Complex<double> (0, 1) * amp[792] - Complex<double> (0, 1) * amp[795] +
      amp[797] - Complex<double> (0, 1) * amp[798] + amp[799] + amp[800] -
      Complex<double> (0, 1) * amp[801] + amp[804] + 1./3. * amp[805] -
      Complex<double> (0, 1) * amp[809] + 1./3. * amp[810] + 1./3. * amp[813] +
      1./3. * amp[815] + amp[818] + 1./3. * amp[819] - Complex<double> (0, 1) *
      amp[820] - Complex<double> (0, 1) * amp[824] + Complex<double> (0, 1) *
      amp[826] + 1./3. * amp[829] - Complex<double> (0, 1) * amp[833] + 1./3. *
      amp[834] + 1./3. * amp[836] + Complex<double> (0, 1) * amp[838]);
  jamp[1] = +1./2. * (-1./3. * amp[600] - 1./3. * amp[601] - amp[602] - 1./3. *
      amp[603] - amp[604] - 1./3. * amp[605] - amp[606] - amp[607] - 1./3. *
      amp[608] - 1./3. * amp[609] - amp[610] - 1./3. * amp[611] - amp[612] -
      1./3. * amp[613] - amp[614] - amp[615] - 1./3. * amp[616] - 1./3. *
      amp[617] - 1./3. * amp[618] - 1./3. * amp[619] - 1./3. * amp[620] - 1./3.
      * amp[621] - amp[622] - 1./3. * amp[623] - amp[624] - 1./3. * amp[625] -
      amp[626] - 1./3. * amp[627] - amp[628] - amp[629] - amp[630] - 1./3. *
      amp[631] - amp[632] - 1./3. * amp[633] - amp[634] - 1./3. * amp[635] -
      amp[636] - amp[637] - amp[638] - amp[639] - amp[680] - 1./3. * amp[681] -
      1./3. * amp[682] - amp[683] - 1./3. * amp[684] - amp[685] - amp[686] -
      1./3. * amp[687] - amp[688] - 1./3. * amp[689] - 1./3. * amp[690] -
      amp[691] - 1./3. * amp[692] - amp[693] - amp[694] - 1./3. * amp[695] -
      amp[696] - amp[697] - amp[698] - amp[699] - amp[700] - amp[701] - 1./3. *
      amp[702] - 1./3. * amp[703] - 1./3. * amp[704] - 1./3. * amp[705] - 1./3.
      * amp[706] - 1./3. * amp[707] - amp[708] - amp[709] - 1./3. * amp[710] -
      amp[711] - 1./3. * amp[712] - 1./3. * amp[713] - amp[714] - 1./3. *
      amp[715] - amp[716] - amp[717] - 1./3. * amp[718] - 1./3. * amp[719] -
      1./3. * amp[721] - amp[724] + Complex<double> (0, 1) * amp[725] -
      Complex<double> (0, 1) * amp[728] - amp[734] - 1./3. * amp[735] -
      Complex<double> (0, 1) * amp[736] + Complex<double> (0, 1) * amp[742] -
      1./3. * amp[745] - amp[748] + Complex<double> (0, 1) * amp[749] -
      Complex<double> (0, 1) * amp[752] - amp[758] - 1./3. * amp[759] -
      Complex<double> (0, 1) * amp[760] + Complex<double> (0, 1) * amp[766] -
      Complex<double> (0, 1) * amp[768] - Complex<double> (0, 1) * amp[771] -
      amp[773] - Complex<double> (0, 1) * amp[774] - amp[775] - amp[776] -
      Complex<double> (0, 1) * amp[777] - amp[778] - Complex<double> (0, 1) *
      amp[780] - Complex<double> (0, 1) * amp[783] - amp[784] - amp[785] -
      1./3. * amp[787] - 1./3. * amp[788] - 1./3. * amp[790] - 1./3. * amp[791]
      - 1./3. * amp[793] - 1./3. * amp[794] - 1./3. * amp[796] - 1./3. *
      amp[797] - 1./3. * amp[799] - 1./3. * amp[800] - 1./3. * amp[802] - 1./3.
      * amp[803] - 1./3. * amp[804] - amp[805] - Complex<double> (0, 1) *
      amp[808] - amp[810] - amp[811] - Complex<double> (0, 1) * amp[812] -
      amp[813] + Complex<double> (0, 1) * amp[814] - amp[815] - 1./3. *
      amp[818] - amp[819] - Complex<double> (0, 1) * amp[821] - 1./3. *
      amp[823] - amp[828] - amp[829] - Complex<double> (0, 1) * amp[830] -
      1./3. * amp[831] - amp[834] + Complex<double> (0, 1) * amp[835] -
      amp[836]);
  jamp[2] = +1./2. * (-1./3. * amp[560] - 1./3. * amp[561] - amp[562] - 1./3. *
      amp[563] - amp[564] - 1./3. * amp[565] - amp[566] - amp[567] - amp[568] -
      amp[569] - 1./3. * amp[570] - amp[571] - 1./3. * amp[572] - amp[573] -
      1./3. * amp[574] - 1./3. * amp[575] - 1./3. * amp[576] - amp[577] -
      amp[578] - 1./3. * amp[579] - amp[580] - 1./3. * amp[581] - 1./3. *
      amp[582] - amp[583] - 1./3. * amp[584] - 1./3. * amp[585] - amp[586] -
      amp[587] - amp[588] - amp[589] - 1./3. * amp[590] - 1./3. * amp[591] -
      1./3. * amp[592] - amp[593] - amp[594] - 1./3. * amp[595] - 1./3. *
      amp[596] - 1./3. * amp[597] - amp[598] - amp[599] - amp[640] - amp[641] -
      1./3. * amp[642] - amp[643] - 1./3. * amp[644] - amp[645] - 1./3. *
      amp[646] - 1./3. * amp[647] - amp[648] - amp[649] - 1./3. * amp[650] -
      amp[651] - 1./3. * amp[652] - amp[653] - 1./3. * amp[654] - 1./3. *
      amp[655] - amp[656] - amp[657] - amp[658] - amp[659] - amp[660] -
      amp[661] - 1./3. * amp[662] - amp[663] - 1./3. * amp[664] - amp[665] -
      1./3. * amp[666] - amp[667] - 1./3. * amp[668] - 1./3. * amp[669] - 1./3.
      * amp[670] - amp[671] - 1./3. * amp[672] - amp[673] - 1./3. * amp[674] -
      amp[675] - 1./3. * amp[676] - 1./3. * amp[677] - 1./3. * amp[678] - 1./3.
      * amp[679] - 1./3. * amp[720] - amp[723] - Complex<double> (0, 1) *
      amp[725] - amp[726] - amp[727] + Complex<double> (0, 1) * amp[728] -
      1./3. * amp[729] - 1./3. * amp[730] - 1./3. * amp[732] - amp[733] +
      Complex<double> (0, 1) * amp[736] - 1./3. * amp[738] - 1./3. * amp[740] -
      amp[741] - Complex<double> (0, 1) * amp[742] - amp[743] - 1./3. *
      amp[744] - amp[747] - Complex<double> (0, 1) * amp[749] - amp[750] -
      amp[751] + Complex<double> (0, 1) * amp[752] - 1./3. * amp[753] - 1./3. *
      amp[754] - 1./3. * amp[756] - amp[757] + Complex<double> (0, 1) *
      amp[760] - 1./3. * amp[762] - 1./3. * amp[764] - amp[765] -
      Complex<double> (0, 1) * amp[766] - amp[767] + Complex<double> (0, 1) *
      amp[768] - amp[769] - amp[770] + Complex<double> (0, 1) * amp[771] -
      amp[772] + Complex<double> (0, 1) * amp[774] + Complex<double> (0, 1) *
      amp[777] - amp[779] + Complex<double> (0, 1) * amp[780] - amp[781] -
      amp[782] + Complex<double> (0, 1) * amp[783] - amp[806] - 1./3. *
      amp[807] + Complex<double> (0, 1) * amp[808] + Complex<double> (0, 1) *
      amp[812] - Complex<double> (0, 1) * amp[814] - amp[816] - 1./3. *
      amp[817] + Complex<double> (0, 1) * amp[821] - 1./3. * amp[822] - 1./3. *
      amp[825] - 1./3. * amp[827] + Complex<double> (0, 1) * amp[830] - 1./3. *
      amp[832] - Complex<double> (0, 1) * amp[835] - 1./3. * amp[837] - 1./3. *
      amp[839]);
  jamp[3] = +1./2. * (+1./3. * amp[640] + 1./3. * amp[641] + amp[642] + 1./3. *
      amp[643] + amp[644] + 1./3. * amp[645] + amp[646] + amp[647] + 1./3. *
      amp[648] + 1./3. * amp[649] + amp[650] + 1./3. * amp[651] + amp[652] +
      1./3. * amp[653] + amp[654] + amp[655] + 1./3. * amp[656] + 1./3. *
      amp[657] + 1./3. * amp[658] + 1./3. * amp[659] + 1./3. * amp[660] + 1./3.
      * amp[661] + amp[662] + 1./3. * amp[663] + amp[664] + 1./3. * amp[665] +
      amp[666] + 1./3. * amp[667] + amp[668] + amp[669] + amp[670] + 1./3. *
      amp[671] + amp[672] + 1./3. * amp[673] + amp[674] + 1./3. * amp[675] +
      amp[676] + amp[677] + amp[678] + amp[679] + 1./3. * amp[680] + amp[681] +
      amp[682] + 1./3. * amp[683] + amp[684] + 1./3. * amp[685] + 1./3. *
      amp[686] + amp[687] + 1./3. * amp[688] + amp[689] + amp[690] + 1./3. *
      amp[691] + amp[692] + 1./3. * amp[693] + 1./3. * amp[694] + amp[695] +
      1./3. * amp[696] + 1./3. * amp[697] + 1./3. * amp[698] + 1./3. * amp[699]
      + 1./3. * amp[700] + 1./3. * amp[701] + amp[702] + amp[703] + amp[704] +
      amp[705] + amp[706] + amp[707] + 1./3. * amp[708] + 1./3. * amp[709] +
      amp[710] + 1./3. * amp[711] + amp[712] + amp[713] + 1./3. * amp[714] +
      amp[715] + 1./3. * amp[716] + 1./3. * amp[717] + amp[718] + amp[719] +
      Complex<double> (0, 1) * amp[722] + 1./3. * amp[727] + amp[730] -
      Complex<double> (0, 1) * amp[731] + 1./3. * amp[734] + amp[735] +
      Complex<double> (0, 1) * amp[737] - Complex<double> (0, 1) * amp[739] +
      Complex<double> (0, 1) * amp[746] + 1./3. * amp[751] + amp[754] -
      Complex<double> (0, 1) * amp[755] + 1./3. * amp[758] + amp[759] +
      Complex<double> (0, 1) * amp[761] - Complex<double> (0, 1) * amp[763] +
      1./3. * amp[769] + 1./3. * amp[770] + 1./3. * amp[772] + 1./3. * amp[773]
      + 1./3. * amp[775] + 1./3. * amp[776] + 1./3. * amp[778] + 1./3. *
      amp[779] + 1./3. * amp[781] + 1./3. * amp[782] + 1./3. * amp[784] + 1./3.
      * amp[785] + Complex<double> (0, 1) * amp[786] + Complex<double> (0, 1) *
      amp[789] + amp[791] + Complex<double> (0, 1) * amp[792] + amp[793] +
      amp[794] + Complex<double> (0, 1) * amp[795] + amp[796] + Complex<double>
      (0, 1) * amp[798] + Complex<double> (0, 1) * amp[801] + amp[802] +
      amp[803] + 1./3. * amp[806] + amp[807] + Complex<double> (0, 1) *
      amp[809] + 1./3. * amp[811] + 1./3. * amp[816] + amp[817] +
      Complex<double> (0, 1) * amp[820] + amp[822] + amp[823] + Complex<double>
      (0, 1) * amp[824] + amp[825] - Complex<double> (0, 1) * amp[826] +
      amp[827] + 1./3. * amp[828] + amp[831] + amp[832] + Complex<double> (0,
      1) * amp[833] + amp[837] - Complex<double> (0, 1) * amp[838] + amp[839]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[2][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P10_sm_qq_zzgqq::matrix_12_ddx_zzgddx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 280;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[920] + 1./3. * amp[921] + amp[922] + 1./3. *
      amp[923] + amp[924] + 1./3. * amp[925] + amp[926] + amp[927] + 1./3. *
      amp[928] + 1./3. * amp[929] + amp[930] + 1./3. * amp[931] + amp[932] +
      1./3. * amp[933] + amp[934] + amp[935] + 1./3. * amp[936] + 1./3. *
      amp[937] + 1./3. * amp[938] + 1./3. * amp[939] + 1./3. * amp[940] + 1./3.
      * amp[941] + amp[942] + 1./3. * amp[943] + amp[944] + 1./3. * amp[945] +
      amp[946] + 1./3. * amp[947] + amp[948] + amp[949] + amp[950] + 1./3. *
      amp[951] + amp[952] + 1./3. * amp[953] + amp[954] + 1./3. * amp[955] +
      amp[956] + amp[957] + amp[958] + amp[959] + 1./3. * amp[960] + amp[961] +
      amp[962] + 1./3. * amp[963] + amp[964] + 1./3. * amp[965] + 1./3. *
      amp[966] + amp[967] + 1./3. * amp[968] + amp[969] + amp[970] + 1./3. *
      amp[971] + amp[972] + 1./3. * amp[973] + 1./3. * amp[974] + amp[975] +
      1./3. * amp[976] + 1./3. * amp[977] + 1./3. * amp[978] + 1./3. * amp[979]
      + 1./3. * amp[980] + 1./3. * amp[981] + amp[982] + amp[983] + amp[984] +
      amp[985] + amp[986] + amp[987] + 1./3. * amp[988] + 1./3. * amp[989] +
      amp[990] + 1./3. * amp[991] + amp[992] + amp[993] + 1./3. * amp[994] +
      amp[995] + 1./3. * amp[996] + 1./3. * amp[997] + amp[998] + amp[999] +
      Complex<double> (0, 1) * amp[1002] + 1./3. * amp[1007] + amp[1010] -
      Complex<double> (0, 1) * amp[1011] + 1./3. * amp[1014] + amp[1015] +
      Complex<double> (0, 1) * amp[1017] - Complex<double> (0, 1) * amp[1019] +
      Complex<double> (0, 1) * amp[1026] + 1./3. * amp[1031] + amp[1034] -
      Complex<double> (0, 1) * amp[1035] + 1./3. * amp[1038] + amp[1039] +
      Complex<double> (0, 1) * amp[1041] - Complex<double> (0, 1) * amp[1043] +
      1./3. * amp[1049] + 1./3. * amp[1050] + 1./3. * amp[1052] + 1./3. *
      amp[1053] + 1./3. * amp[1055] + 1./3. * amp[1056] + 1./3. * amp[1058] +
      1./3. * amp[1059] + 1./3. * amp[1061] + 1./3. * amp[1062] + 1./3. *
      amp[1064] + 1./3. * amp[1065] + Complex<double> (0, 1) * amp[1066] +
      Complex<double> (0, 1) * amp[1069] + amp[1071] + Complex<double> (0, 1) *
      amp[1072] + amp[1073] + amp[1074] + Complex<double> (0, 1) * amp[1075] +
      amp[1076] + Complex<double> (0, 1) * amp[1078] + Complex<double> (0, 1) *
      amp[1081] + amp[1082] + amp[1083] + 1./3. * amp[1086] + amp[1087] +
      Complex<double> (0, 1) * amp[1089] + 1./3. * amp[1091] + 1./3. *
      amp[1096] + amp[1097] + Complex<double> (0, 1) * amp[1100] + amp[1102] +
      amp[1103] + Complex<double> (0, 1) * amp[1104] + amp[1105] -
      Complex<double> (0, 1) * amp[1106] + amp[1107] + 1./3. * amp[1108] +
      amp[1111] + amp[1112] + Complex<double> (0, 1) * amp[1113] + amp[1117] -
      Complex<double> (0, 1) * amp[1118] + amp[1119]);
  jamp[1] = +1./2. * (-1./3. * amp[840] - 1./3. * amp[841] - amp[842] - 1./3. *
      amp[843] - amp[844] - 1./3. * amp[845] - amp[846] - amp[847] - amp[848] -
      amp[849] - 1./3. * amp[850] - amp[851] - 1./3. * amp[852] - amp[853] -
      1./3. * amp[854] - 1./3. * amp[855] - 1./3. * amp[856] - amp[857] -
      amp[858] - 1./3. * amp[859] - amp[860] - 1./3. * amp[861] - 1./3. *
      amp[862] - amp[863] - 1./3. * amp[864] - 1./3. * amp[865] - amp[866] -
      amp[867] - amp[868] - amp[869] - 1./3. * amp[870] - 1./3. * amp[871] -
      1./3. * amp[872] - amp[873] - amp[874] - 1./3. * amp[875] - 1./3. *
      amp[876] - 1./3. * amp[877] - amp[878] - amp[879] - amp[920] - amp[921] -
      1./3. * amp[922] - amp[923] - 1./3. * amp[924] - amp[925] - 1./3. *
      amp[926] - 1./3. * amp[927] - amp[928] - amp[929] - 1./3. * amp[930] -
      amp[931] - 1./3. * amp[932] - amp[933] - 1./3. * amp[934] - 1./3. *
      amp[935] - amp[936] - amp[937] - amp[938] - amp[939] - amp[940] -
      amp[941] - 1./3. * amp[942] - amp[943] - 1./3. * amp[944] - amp[945] -
      1./3. * amp[946] - amp[947] - 1./3. * amp[948] - 1./3. * amp[949] - 1./3.
      * amp[950] - amp[951] - 1./3. * amp[952] - amp[953] - 1./3. * amp[954] -
      amp[955] - 1./3. * amp[956] - 1./3. * amp[957] - 1./3. * amp[958] - 1./3.
      * amp[959] - 1./3. * amp[1000] - amp[1003] - Complex<double> (0, 1) *
      amp[1005] - amp[1006] - amp[1007] + Complex<double> (0, 1) * amp[1008] -
      1./3. * amp[1009] - 1./3. * amp[1010] - 1./3. * amp[1012] - amp[1013] +
      Complex<double> (0, 1) * amp[1016] - 1./3. * amp[1018] - 1./3. *
      amp[1020] - amp[1021] - Complex<double> (0, 1) * amp[1022] - amp[1023] -
      1./3. * amp[1024] - amp[1027] - Complex<double> (0, 1) * amp[1029] -
      amp[1030] - amp[1031] + Complex<double> (0, 1) * amp[1032] - 1./3. *
      amp[1033] - 1./3. * amp[1034] - 1./3. * amp[1036] - amp[1037] +
      Complex<double> (0, 1) * amp[1040] - 1./3. * amp[1042] - 1./3. *
      amp[1044] - amp[1045] - Complex<double> (0, 1) * amp[1046] - amp[1047] +
      Complex<double> (0, 1) * amp[1048] - amp[1049] - amp[1050] +
      Complex<double> (0, 1) * amp[1051] - amp[1052] + Complex<double> (0, 1) *
      amp[1054] + Complex<double> (0, 1) * amp[1057] - amp[1059] +
      Complex<double> (0, 1) * amp[1060] - amp[1061] - amp[1062] +
      Complex<double> (0, 1) * amp[1063] - amp[1086] - 1./3. * amp[1087] +
      Complex<double> (0, 1) * amp[1088] + Complex<double> (0, 1) * amp[1092] -
      Complex<double> (0, 1) * amp[1094] - amp[1096] - 1./3. * amp[1097] +
      Complex<double> (0, 1) * amp[1101] - 1./3. * amp[1102] - 1./3. *
      amp[1105] - 1./3. * amp[1107] + Complex<double> (0, 1) * amp[1110] -
      1./3. * amp[1112] - Complex<double> (0, 1) * amp[1115] - 1./3. *
      amp[1117] - 1./3. * amp[1119]);
  jamp[2] = +1./2. * (+amp[840] + amp[841] + 1./3. * amp[842] + amp[843] +
      1./3. * amp[844] + amp[845] + 1./3. * amp[846] + 1./3. * amp[847] + 1./3.
      * amp[848] + 1./3. * amp[849] + amp[850] + 1./3. * amp[851] + amp[852] +
      1./3. * amp[853] + amp[854] + amp[855] + amp[856] + 1./3. * amp[857] +
      1./3. * amp[858] + amp[859] + 1./3. * amp[860] + amp[861] + amp[862] +
      1./3. * amp[863] + amp[864] + amp[865] + 1./3. * amp[866] + 1./3. *
      amp[867] + 1./3. * amp[868] + 1./3. * amp[869] + amp[870] + amp[871] +
      amp[872] + 1./3. * amp[873] + 1./3. * amp[874] + amp[875] + amp[876] +
      amp[877] + 1./3. * amp[878] + 1./3. * amp[879] + amp[880] + amp[881] +
      1./3. * amp[882] + amp[883] + 1./3. * amp[884] + amp[885] + 1./3. *
      amp[886] + 1./3. * amp[887] + amp[888] + amp[889] + 1./3. * amp[890] +
      amp[891] + 1./3. * amp[892] + amp[893] + 1./3. * amp[894] + 1./3. *
      amp[895] + amp[896] + amp[897] + amp[898] + amp[899] + amp[900] +
      amp[901] + 1./3. * amp[902] + amp[903] + 1./3. * amp[904] + amp[905] +
      1./3. * amp[906] + amp[907] + 1./3. * amp[908] + 1./3. * amp[909] + 1./3.
      * amp[910] + amp[911] + 1./3. * amp[912] + amp[913] + 1./3. * amp[914] +
      amp[915] + 1./3. * amp[916] + 1./3. * amp[917] + 1./3. * amp[918] + 1./3.
      * amp[919] + amp[1000] + amp[1001] - Complex<double> (0, 1) * amp[1002] +
      1./3. * amp[1003] + 1./3. * amp[1004] + 1./3. * amp[1006] + amp[1009] +
      Complex<double> (0, 1) * amp[1011] + amp[1012] + 1./3. * amp[1013] -
      Complex<double> (0, 1) * amp[1017] + amp[1018] + Complex<double> (0, 1) *
      amp[1019] + amp[1020] + 1./3. * amp[1021] + 1./3. * amp[1023] + amp[1024]
      + amp[1025] - Complex<double> (0, 1) * amp[1026] + 1./3. * amp[1027] +
      1./3. * amp[1028] + 1./3. * amp[1030] + amp[1033] + Complex<double> (0,
      1) * amp[1035] + amp[1036] + 1./3. * amp[1037] - Complex<double> (0, 1) *
      amp[1041] + amp[1042] + Complex<double> (0, 1) * amp[1043] + amp[1044] +
      1./3. * amp[1045] + 1./3. * amp[1047] - Complex<double> (0, 1) *
      amp[1066] + amp[1067] + amp[1068] - Complex<double> (0, 1) * amp[1069] +
      amp[1070] - Complex<double> (0, 1) * amp[1072] - Complex<double> (0, 1) *
      amp[1075] + amp[1077] - Complex<double> (0, 1) * amp[1078] + amp[1079] +
      amp[1080] - Complex<double> (0, 1) * amp[1081] + amp[1084] + 1./3. *
      amp[1085] - Complex<double> (0, 1) * amp[1089] + 1./3. * amp[1090] +
      1./3. * amp[1093] + 1./3. * amp[1095] + amp[1098] + 1./3. * amp[1099] -
      Complex<double> (0, 1) * amp[1100] - Complex<double> (0, 1) * amp[1104] +
      Complex<double> (0, 1) * amp[1106] + 1./3. * amp[1109] - Complex<double>
      (0, 1) * amp[1113] + 1./3. * amp[1114] + 1./3. * amp[1116] +
      Complex<double> (0, 1) * amp[1118]);
  jamp[3] = +1./2. * (-1./3. * amp[880] - 1./3. * amp[881] - amp[882] - 1./3. *
      amp[883] - amp[884] - 1./3. * amp[885] - amp[886] - amp[887] - 1./3. *
      amp[888] - 1./3. * amp[889] - amp[890] - 1./3. * amp[891] - amp[892] -
      1./3. * amp[893] - amp[894] - amp[895] - 1./3. * amp[896] - 1./3. *
      amp[897] - 1./3. * amp[898] - 1./3. * amp[899] - 1./3. * amp[900] - 1./3.
      * amp[901] - amp[902] - 1./3. * amp[903] - amp[904] - 1./3. * amp[905] -
      amp[906] - 1./3. * amp[907] - amp[908] - amp[909] - amp[910] - 1./3. *
      amp[911] - amp[912] - 1./3. * amp[913] - amp[914] - 1./3. * amp[915] -
      amp[916] - amp[917] - amp[918] - amp[919] - amp[960] - 1./3. * amp[961] -
      1./3. * amp[962] - amp[963] - 1./3. * amp[964] - amp[965] - amp[966] -
      1./3. * amp[967] - amp[968] - 1./3. * amp[969] - 1./3. * amp[970] -
      amp[971] - 1./3. * amp[972] - amp[973] - amp[974] - 1./3. * amp[975] -
      amp[976] - amp[977] - amp[978] - amp[979] - amp[980] - amp[981] - 1./3. *
      amp[982] - 1./3. * amp[983] - 1./3. * amp[984] - 1./3. * amp[985] - 1./3.
      * amp[986] - 1./3. * amp[987] - amp[988] - amp[989] - 1./3. * amp[990] -
      amp[991] - 1./3. * amp[992] - 1./3. * amp[993] - amp[994] - 1./3. *
      amp[995] - amp[996] - amp[997] - 1./3. * amp[998] - 1./3. * amp[999] -
      1./3. * amp[1001] - amp[1004] + Complex<double> (0, 1) * amp[1005] -
      Complex<double> (0, 1) * amp[1008] - amp[1014] - 1./3. * amp[1015] -
      Complex<double> (0, 1) * amp[1016] + Complex<double> (0, 1) * amp[1022] -
      1./3. * amp[1025] - amp[1028] + Complex<double> (0, 1) * amp[1029] -
      Complex<double> (0, 1) * amp[1032] - amp[1038] - 1./3. * amp[1039] -
      Complex<double> (0, 1) * amp[1040] + Complex<double> (0, 1) * amp[1046] -
      Complex<double> (0, 1) * amp[1048] - Complex<double> (0, 1) * amp[1051] -
      amp[1053] - Complex<double> (0, 1) * amp[1054] - amp[1055] - amp[1056] -
      Complex<double> (0, 1) * amp[1057] - amp[1058] - Complex<double> (0, 1) *
      amp[1060] - Complex<double> (0, 1) * amp[1063] - amp[1064] - amp[1065] -
      1./3. * amp[1067] - 1./3. * amp[1068] - 1./3. * amp[1070] - 1./3. *
      amp[1071] - 1./3. * amp[1073] - 1./3. * amp[1074] - 1./3. * amp[1076] -
      1./3. * amp[1077] - 1./3. * amp[1079] - 1./3. * amp[1080] - 1./3. *
      amp[1082] - 1./3. * amp[1083] - 1./3. * amp[1084] - amp[1085] -
      Complex<double> (0, 1) * amp[1088] - amp[1090] - amp[1091] -
      Complex<double> (0, 1) * amp[1092] - amp[1093] + Complex<double> (0, 1) *
      amp[1094] - amp[1095] - 1./3. * amp[1098] - amp[1099] - Complex<double>
      (0, 1) * amp[1101] - 1./3. * amp[1103] - amp[1108] - amp[1109] -
      Complex<double> (0, 1) * amp[1110] - 1./3. * amp[1111] - amp[1114] +
      Complex<double> (0, 1) * amp[1115] - amp[1116]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[3][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P10_sm_qq_zzgqq::matrix_12_uxux_zzguxux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 280;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[1200] + 1./3. * amp[1201] + amp[1202] +
      1./3. * amp[1203] + amp[1204] + 1./3. * amp[1205] + amp[1206] + amp[1207]
      + 1./3. * amp[1208] + 1./3. * amp[1209] + amp[1210] + 1./3. * amp[1211] +
      amp[1212] + 1./3. * amp[1213] + amp[1214] + amp[1215] + 1./3. * amp[1216]
      + 1./3. * amp[1217] + 1./3. * amp[1218] + 1./3. * amp[1219] + 1./3. *
      amp[1220] + 1./3. * amp[1221] + amp[1222] + 1./3. * amp[1223] + amp[1224]
      + 1./3. * amp[1225] + amp[1226] + 1./3. * amp[1227] + amp[1228] +
      amp[1229] + amp[1230] + 1./3. * amp[1231] + amp[1232] + 1./3. * amp[1233]
      + amp[1234] + 1./3. * amp[1235] + amp[1236] + amp[1237] + amp[1238] +
      amp[1239] + 1./3. * amp[1240] + amp[1241] + amp[1242] + 1./3. * amp[1243]
      + amp[1244] + 1./3. * amp[1245] + 1./3. * amp[1246] + amp[1247] + 1./3. *
      amp[1248] + amp[1249] + amp[1250] + 1./3. * amp[1251] + amp[1252] + 1./3.
      * amp[1253] + 1./3. * amp[1254] + amp[1255] + 1./3. * amp[1256] + 1./3. *
      amp[1257] + 1./3. * amp[1258] + 1./3. * amp[1259] + 1./3. * amp[1260] +
      1./3. * amp[1261] + amp[1262] + amp[1263] + amp[1264] + amp[1265] +
      amp[1266] + amp[1267] + 1./3. * amp[1268] + 1./3. * amp[1269] + amp[1270]
      + 1./3. * amp[1271] + amp[1272] + amp[1273] + 1./3. * amp[1274] +
      amp[1275] + 1./3. * amp[1276] + 1./3. * amp[1277] + amp[1278] + amp[1279]
      + Complex<double> (0, 1) * amp[1282] + 1./3. * amp[1287] + amp[1290] -
      Complex<double> (0, 1) * amp[1291] + 1./3. * amp[1294] + amp[1295] +
      Complex<double> (0, 1) * amp[1297] - Complex<double> (0, 1) * amp[1299] +
      Complex<double> (0, 1) * amp[1306] + 1./3. * amp[1311] + amp[1314] -
      Complex<double> (0, 1) * amp[1315] + 1./3. * amp[1318] + amp[1319] +
      Complex<double> (0, 1) * amp[1321] - Complex<double> (0, 1) * amp[1323] +
      1./3. * amp[1329] + 1./3. * amp[1330] + 1./3. * amp[1332] + 1./3. *
      amp[1333] + 1./3. * amp[1335] + 1./3. * amp[1336] + 1./3. * amp[1338] +
      1./3. * amp[1339] + 1./3. * amp[1341] + 1./3. * amp[1342] + 1./3. *
      amp[1344] + 1./3. * amp[1345] + Complex<double> (0, 1) * amp[1346] +
      Complex<double> (0, 1) * amp[1349] + amp[1351] + Complex<double> (0, 1) *
      amp[1352] + amp[1353] + amp[1354] + Complex<double> (0, 1) * amp[1355] +
      amp[1356] + Complex<double> (0, 1) * amp[1358] + Complex<double> (0, 1) *
      amp[1361] + amp[1362] + amp[1363] + 1./3. * amp[1366] + amp[1367] +
      Complex<double> (0, 1) * amp[1369] + 1./3. * amp[1371] + 1./3. *
      amp[1376] + amp[1377] + Complex<double> (0, 1) * amp[1380] + amp[1382] +
      amp[1383] + Complex<double> (0, 1) * amp[1384] + amp[1385] -
      Complex<double> (0, 1) * amp[1386] + amp[1387] + 1./3. * amp[1388] +
      amp[1391] + amp[1392] + Complex<double> (0, 1) * amp[1393] + amp[1397] -
      Complex<double> (0, 1) * amp[1398] + amp[1399]);
  jamp[1] = +1./2. * (-1./3. * amp[1120] - 1./3. * amp[1121] - amp[1122] -
      1./3. * amp[1123] - amp[1124] - 1./3. * amp[1125] - amp[1126] - amp[1127]
      - amp[1128] - amp[1129] - 1./3. * amp[1130] - amp[1131] - 1./3. *
      amp[1132] - amp[1133] - 1./3. * amp[1134] - 1./3. * amp[1135] - 1./3. *
      amp[1136] - amp[1137] - amp[1138] - 1./3. * amp[1139] - amp[1140] - 1./3.
      * amp[1141] - 1./3. * amp[1142] - amp[1143] - 1./3. * amp[1144] - 1./3. *
      amp[1145] - amp[1146] - amp[1147] - amp[1148] - amp[1149] - 1./3. *
      amp[1150] - 1./3. * amp[1151] - 1./3. * amp[1152] - amp[1153] - amp[1154]
      - 1./3. * amp[1155] - 1./3. * amp[1156] - 1./3. * amp[1157] - amp[1158] -
      amp[1159] - amp[1200] - amp[1201] - 1./3. * amp[1202] - amp[1203] - 1./3.
      * amp[1204] - amp[1205] - 1./3. * amp[1206] - 1./3. * amp[1207] -
      amp[1208] - amp[1209] - 1./3. * amp[1210] - amp[1211] - 1./3. * amp[1212]
      - amp[1213] - 1./3. * amp[1214] - 1./3. * amp[1215] - amp[1216] -
      amp[1217] - amp[1218] - amp[1219] - amp[1220] - amp[1221] - 1./3. *
      amp[1222] - amp[1223] - 1./3. * amp[1224] - amp[1225] - 1./3. * amp[1226]
      - amp[1227] - 1./3. * amp[1228] - 1./3. * amp[1229] - 1./3. * amp[1230] -
      amp[1231] - 1./3. * amp[1232] - amp[1233] - 1./3. * amp[1234] - amp[1235]
      - 1./3. * amp[1236] - 1./3. * amp[1237] - 1./3. * amp[1238] - 1./3. *
      amp[1239] - 1./3. * amp[1280] - amp[1283] - Complex<double> (0, 1) *
      amp[1285] - amp[1286] - amp[1287] + Complex<double> (0, 1) * amp[1288] -
      1./3. * amp[1289] - 1./3. * amp[1290] - 1./3. * amp[1292] - amp[1293] +
      Complex<double> (0, 1) * amp[1296] - 1./3. * amp[1298] - 1./3. *
      amp[1300] - amp[1301] - Complex<double> (0, 1) * amp[1302] - amp[1303] -
      1./3. * amp[1304] - amp[1307] - Complex<double> (0, 1) * amp[1309] -
      amp[1310] - amp[1311] + Complex<double> (0, 1) * amp[1312] - 1./3. *
      amp[1313] - 1./3. * amp[1314] - 1./3. * amp[1316] - amp[1317] +
      Complex<double> (0, 1) * amp[1320] - 1./3. * amp[1322] - 1./3. *
      amp[1324] - amp[1325] - Complex<double> (0, 1) * amp[1326] - amp[1327] +
      Complex<double> (0, 1) * amp[1328] - amp[1329] - amp[1330] +
      Complex<double> (0, 1) * amp[1331] - amp[1332] + Complex<double> (0, 1) *
      amp[1334] + Complex<double> (0, 1) * amp[1337] - amp[1339] +
      Complex<double> (0, 1) * amp[1340] - amp[1341] - amp[1342] +
      Complex<double> (0, 1) * amp[1343] - amp[1366] - 1./3. * amp[1367] +
      Complex<double> (0, 1) * amp[1368] + Complex<double> (0, 1) * amp[1372] -
      Complex<double> (0, 1) * amp[1374] - amp[1376] - 1./3. * amp[1377] +
      Complex<double> (0, 1) * amp[1381] - 1./3. * amp[1382] - 1./3. *
      amp[1385] - 1./3. * amp[1387] + Complex<double> (0, 1) * amp[1390] -
      1./3. * amp[1392] - Complex<double> (0, 1) * amp[1395] - 1./3. *
      amp[1397] - 1./3. * amp[1399]);
  jamp[2] = +1./2. * (-1./3. * amp[1160] - 1./3. * amp[1161] - amp[1162] -
      1./3. * amp[1163] - amp[1164] - 1./3. * amp[1165] - amp[1166] - amp[1167]
      - 1./3. * amp[1168] - 1./3. * amp[1169] - amp[1170] - 1./3. * amp[1171] -
      amp[1172] - 1./3. * amp[1173] - amp[1174] - amp[1175] - 1./3. * amp[1176]
      - 1./3. * amp[1177] - 1./3. * amp[1178] - 1./3. * amp[1179] - 1./3. *
      amp[1180] - 1./3. * amp[1181] - amp[1182] - 1./3. * amp[1183] - amp[1184]
      - 1./3. * amp[1185] - amp[1186] - 1./3. * amp[1187] - amp[1188] -
      amp[1189] - amp[1190] - 1./3. * amp[1191] - amp[1192] - 1./3. * amp[1193]
      - amp[1194] - 1./3. * amp[1195] - amp[1196] - amp[1197] - amp[1198] -
      amp[1199] - amp[1240] - 1./3. * amp[1241] - 1./3. * amp[1242] - amp[1243]
      - 1./3. * amp[1244] - amp[1245] - amp[1246] - 1./3. * amp[1247] -
      amp[1248] - 1./3. * amp[1249] - 1./3. * amp[1250] - amp[1251] - 1./3. *
      amp[1252] - amp[1253] - amp[1254] - 1./3. * amp[1255] - amp[1256] -
      amp[1257] - amp[1258] - amp[1259] - amp[1260] - amp[1261] - 1./3. *
      amp[1262] - 1./3. * amp[1263] - 1./3. * amp[1264] - 1./3. * amp[1265] -
      1./3. * amp[1266] - 1./3. * amp[1267] - amp[1268] - amp[1269] - 1./3. *
      amp[1270] - amp[1271] - 1./3. * amp[1272] - 1./3. * amp[1273] - amp[1274]
      - 1./3. * amp[1275] - amp[1276] - amp[1277] - 1./3. * amp[1278] - 1./3. *
      amp[1279] - 1./3. * amp[1281] - amp[1284] + Complex<double> (0, 1) *
      amp[1285] - Complex<double> (0, 1) * amp[1288] - amp[1294] - 1./3. *
      amp[1295] - Complex<double> (0, 1) * amp[1296] + Complex<double> (0, 1) *
      amp[1302] - 1./3. * amp[1305] - amp[1308] + Complex<double> (0, 1) *
      amp[1309] - Complex<double> (0, 1) * amp[1312] - amp[1318] - 1./3. *
      amp[1319] - Complex<double> (0, 1) * amp[1320] + Complex<double> (0, 1) *
      amp[1326] - Complex<double> (0, 1) * amp[1328] - Complex<double> (0, 1) *
      amp[1331] - amp[1333] - Complex<double> (0, 1) * amp[1334] - amp[1335] -
      amp[1336] - Complex<double> (0, 1) * amp[1337] - amp[1338] -
      Complex<double> (0, 1) * amp[1340] - Complex<double> (0, 1) * amp[1343] -
      amp[1344] - amp[1345] - 1./3. * amp[1347] - 1./3. * amp[1348] - 1./3. *
      amp[1350] - 1./3. * amp[1351] - 1./3. * amp[1353] - 1./3. * amp[1354] -
      1./3. * amp[1356] - 1./3. * amp[1357] - 1./3. * amp[1359] - 1./3. *
      amp[1360] - 1./3. * amp[1362] - 1./3. * amp[1363] - 1./3. * amp[1364] -
      amp[1365] - Complex<double> (0, 1) * amp[1368] - amp[1370] - amp[1371] -
      Complex<double> (0, 1) * amp[1372] - amp[1373] + Complex<double> (0, 1) *
      amp[1374] - amp[1375] - 1./3. * amp[1378] - amp[1379] - Complex<double>
      (0, 1) * amp[1381] - 1./3. * amp[1383] - amp[1388] - amp[1389] -
      Complex<double> (0, 1) * amp[1390] - 1./3. * amp[1391] - amp[1394] +
      Complex<double> (0, 1) * amp[1395] - amp[1396]);
  jamp[3] = +1./2. * (+amp[1120] + amp[1121] + 1./3. * amp[1122] + amp[1123] +
      1./3. * amp[1124] + amp[1125] + 1./3. * amp[1126] + 1./3. * amp[1127] +
      1./3. * amp[1128] + 1./3. * amp[1129] + amp[1130] + 1./3. * amp[1131] +
      amp[1132] + 1./3. * amp[1133] + amp[1134] + amp[1135] + amp[1136] + 1./3.
      * amp[1137] + 1./3. * amp[1138] + amp[1139] + 1./3. * amp[1140] +
      amp[1141] + amp[1142] + 1./3. * amp[1143] + amp[1144] + amp[1145] + 1./3.
      * amp[1146] + 1./3. * amp[1147] + 1./3. * amp[1148] + 1./3. * amp[1149] +
      amp[1150] + amp[1151] + amp[1152] + 1./3. * amp[1153] + 1./3. * amp[1154]
      + amp[1155] + amp[1156] + amp[1157] + 1./3. * amp[1158] + 1./3. *
      amp[1159] + amp[1160] + amp[1161] + 1./3. * amp[1162] + amp[1163] + 1./3.
      * amp[1164] + amp[1165] + 1./3. * amp[1166] + 1./3. * amp[1167] +
      amp[1168] + amp[1169] + 1./3. * amp[1170] + amp[1171] + 1./3. * amp[1172]
      + amp[1173] + 1./3. * amp[1174] + 1./3. * amp[1175] + amp[1176] +
      amp[1177] + amp[1178] + amp[1179] + amp[1180] + amp[1181] + 1./3. *
      amp[1182] + amp[1183] + 1./3. * amp[1184] + amp[1185] + 1./3. * amp[1186]
      + amp[1187] + 1./3. * amp[1188] + 1./3. * amp[1189] + 1./3. * amp[1190] +
      amp[1191] + 1./3. * amp[1192] + amp[1193] + 1./3. * amp[1194] + amp[1195]
      + 1./3. * amp[1196] + 1./3. * amp[1197] + 1./3. * amp[1198] + 1./3. *
      amp[1199] + amp[1280] + amp[1281] - Complex<double> (0, 1) * amp[1282] +
      1./3. * amp[1283] + 1./3. * amp[1284] + 1./3. * amp[1286] + amp[1289] +
      Complex<double> (0, 1) * amp[1291] + amp[1292] + 1./3. * amp[1293] -
      Complex<double> (0, 1) * amp[1297] + amp[1298] + Complex<double> (0, 1) *
      amp[1299] + amp[1300] + 1./3. * amp[1301] + 1./3. * amp[1303] + amp[1304]
      + amp[1305] - Complex<double> (0, 1) * amp[1306] + 1./3. * amp[1307] +
      1./3. * amp[1308] + 1./3. * amp[1310] + amp[1313] + Complex<double> (0,
      1) * amp[1315] + amp[1316] + 1./3. * amp[1317] - Complex<double> (0, 1) *
      amp[1321] + amp[1322] + Complex<double> (0, 1) * amp[1323] + amp[1324] +
      1./3. * amp[1325] + 1./3. * amp[1327] - Complex<double> (0, 1) *
      amp[1346] + amp[1347] + amp[1348] - Complex<double> (0, 1) * amp[1349] +
      amp[1350] - Complex<double> (0, 1) * amp[1352] - Complex<double> (0, 1) *
      amp[1355] + amp[1357] - Complex<double> (0, 1) * amp[1358] + amp[1359] +
      amp[1360] - Complex<double> (0, 1) * amp[1361] + amp[1364] + 1./3. *
      amp[1365] - Complex<double> (0, 1) * amp[1369] + 1./3. * amp[1370] +
      1./3. * amp[1373] + 1./3. * amp[1375] + amp[1378] + 1./3. * amp[1379] -
      Complex<double> (0, 1) * amp[1380] - Complex<double> (0, 1) * amp[1384] +
      Complex<double> (0, 1) * amp[1386] + 1./3. * amp[1389] - Complex<double>
      (0, 1) * amp[1393] + 1./3. * amp[1394] + 1./3. * amp[1396] +
      Complex<double> (0, 1) * amp[1398]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[4][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P10_sm_qq_zzgqq::matrix_12_dxdx_zzgdxdx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 280;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[1480] + 1./3. * amp[1481] + amp[1482] +
      1./3. * amp[1483] + amp[1484] + 1./3. * amp[1485] + amp[1486] + amp[1487]
      + 1./3. * amp[1488] + 1./3. * amp[1489] + amp[1490] + 1./3. * amp[1491] +
      amp[1492] + 1./3. * amp[1493] + amp[1494] + amp[1495] + 1./3. * amp[1496]
      + 1./3. * amp[1497] + 1./3. * amp[1498] + 1./3. * amp[1499] + 1./3. *
      amp[1500] + 1./3. * amp[1501] + amp[1502] + 1./3. * amp[1503] + amp[1504]
      + 1./3. * amp[1505] + amp[1506] + 1./3. * amp[1507] + amp[1508] +
      amp[1509] + amp[1510] + 1./3. * amp[1511] + amp[1512] + 1./3. * amp[1513]
      + amp[1514] + 1./3. * amp[1515] + amp[1516] + amp[1517] + amp[1518] +
      amp[1519] + 1./3. * amp[1520] + amp[1521] + amp[1522] + 1./3. * amp[1523]
      + amp[1524] + 1./3. * amp[1525] + 1./3. * amp[1526] + amp[1527] + 1./3. *
      amp[1528] + amp[1529] + amp[1530] + 1./3. * amp[1531] + amp[1532] + 1./3.
      * amp[1533] + 1./3. * amp[1534] + amp[1535] + 1./3. * amp[1536] + 1./3. *
      amp[1537] + 1./3. * amp[1538] + 1./3. * amp[1539] + 1./3. * amp[1540] +
      1./3. * amp[1541] + amp[1542] + amp[1543] + amp[1544] + amp[1545] +
      amp[1546] + amp[1547] + 1./3. * amp[1548] + 1./3. * amp[1549] + amp[1550]
      + 1./3. * amp[1551] + amp[1552] + amp[1553] + 1./3. * amp[1554] +
      amp[1555] + 1./3. * amp[1556] + 1./3. * amp[1557] + amp[1558] + amp[1559]
      + Complex<double> (0, 1) * amp[1562] + 1./3. * amp[1567] + amp[1570] -
      Complex<double> (0, 1) * amp[1571] + 1./3. * amp[1574] + amp[1575] +
      Complex<double> (0, 1) * amp[1577] - Complex<double> (0, 1) * amp[1579] +
      Complex<double> (0, 1) * amp[1586] + 1./3. * amp[1591] + amp[1594] -
      Complex<double> (0, 1) * amp[1595] + 1./3. * amp[1598] + amp[1599] +
      Complex<double> (0, 1) * amp[1601] - Complex<double> (0, 1) * amp[1603] +
      1./3. * amp[1609] + 1./3. * amp[1610] + 1./3. * amp[1612] + 1./3. *
      amp[1613] + 1./3. * amp[1615] + 1./3. * amp[1616] + 1./3. * amp[1618] +
      1./3. * amp[1619] + 1./3. * amp[1621] + 1./3. * amp[1622] + 1./3. *
      amp[1624] + 1./3. * amp[1625] + Complex<double> (0, 1) * amp[1626] +
      Complex<double> (0, 1) * amp[1629] + amp[1631] + Complex<double> (0, 1) *
      amp[1632] + amp[1633] + amp[1634] + Complex<double> (0, 1) * amp[1635] +
      amp[1636] + Complex<double> (0, 1) * amp[1638] + Complex<double> (0, 1) *
      amp[1641] + amp[1642] + amp[1643] + 1./3. * amp[1646] + amp[1647] +
      Complex<double> (0, 1) * amp[1649] + 1./3. * amp[1651] + 1./3. *
      amp[1656] + amp[1657] + Complex<double> (0, 1) * amp[1660] + amp[1662] +
      amp[1663] + Complex<double> (0, 1) * amp[1664] + amp[1665] -
      Complex<double> (0, 1) * amp[1666] + amp[1667] + 1./3. * amp[1668] +
      amp[1671] + amp[1672] + Complex<double> (0, 1) * amp[1673] + amp[1677] -
      Complex<double> (0, 1) * amp[1678] + amp[1679]);
  jamp[1] = +1./2. * (-1./3. * amp[1400] - 1./3. * amp[1401] - amp[1402] -
      1./3. * amp[1403] - amp[1404] - 1./3. * amp[1405] - amp[1406] - amp[1407]
      - amp[1408] - amp[1409] - 1./3. * amp[1410] - amp[1411] - 1./3. *
      amp[1412] - amp[1413] - 1./3. * amp[1414] - 1./3. * amp[1415] - 1./3. *
      amp[1416] - amp[1417] - amp[1418] - 1./3. * amp[1419] - amp[1420] - 1./3.
      * amp[1421] - 1./3. * amp[1422] - amp[1423] - 1./3. * amp[1424] - 1./3. *
      amp[1425] - amp[1426] - amp[1427] - amp[1428] - amp[1429] - 1./3. *
      amp[1430] - 1./3. * amp[1431] - 1./3. * amp[1432] - amp[1433] - amp[1434]
      - 1./3. * amp[1435] - 1./3. * amp[1436] - 1./3. * amp[1437] - amp[1438] -
      amp[1439] - amp[1480] - amp[1481] - 1./3. * amp[1482] - amp[1483] - 1./3.
      * amp[1484] - amp[1485] - 1./3. * amp[1486] - 1./3. * amp[1487] -
      amp[1488] - amp[1489] - 1./3. * amp[1490] - amp[1491] - 1./3. * amp[1492]
      - amp[1493] - 1./3. * amp[1494] - 1./3. * amp[1495] - amp[1496] -
      amp[1497] - amp[1498] - amp[1499] - amp[1500] - amp[1501] - 1./3. *
      amp[1502] - amp[1503] - 1./3. * amp[1504] - amp[1505] - 1./3. * amp[1506]
      - amp[1507] - 1./3. * amp[1508] - 1./3. * amp[1509] - 1./3. * amp[1510] -
      amp[1511] - 1./3. * amp[1512] - amp[1513] - 1./3. * amp[1514] - amp[1515]
      - 1./3. * amp[1516] - 1./3. * amp[1517] - 1./3. * amp[1518] - 1./3. *
      amp[1519] - 1./3. * amp[1560] - amp[1563] - Complex<double> (0, 1) *
      amp[1565] - amp[1566] - amp[1567] + Complex<double> (0, 1) * amp[1568] -
      1./3. * amp[1569] - 1./3. * amp[1570] - 1./3. * amp[1572] - amp[1573] +
      Complex<double> (0, 1) * amp[1576] - 1./3. * amp[1578] - 1./3. *
      amp[1580] - amp[1581] - Complex<double> (0, 1) * amp[1582] - amp[1583] -
      1./3. * amp[1584] - amp[1587] - Complex<double> (0, 1) * amp[1589] -
      amp[1590] - amp[1591] + Complex<double> (0, 1) * amp[1592] - 1./3. *
      amp[1593] - 1./3. * amp[1594] - 1./3. * amp[1596] - amp[1597] +
      Complex<double> (0, 1) * amp[1600] - 1./3. * amp[1602] - 1./3. *
      amp[1604] - amp[1605] - Complex<double> (0, 1) * amp[1606] - amp[1607] +
      Complex<double> (0, 1) * amp[1608] - amp[1609] - amp[1610] +
      Complex<double> (0, 1) * amp[1611] - amp[1612] + Complex<double> (0, 1) *
      amp[1614] + Complex<double> (0, 1) * amp[1617] - amp[1619] +
      Complex<double> (0, 1) * amp[1620] - amp[1621] - amp[1622] +
      Complex<double> (0, 1) * amp[1623] - amp[1646] - 1./3. * amp[1647] +
      Complex<double> (0, 1) * amp[1648] + Complex<double> (0, 1) * amp[1652] -
      Complex<double> (0, 1) * amp[1654] - amp[1656] - 1./3. * amp[1657] +
      Complex<double> (0, 1) * amp[1661] - 1./3. * amp[1662] - 1./3. *
      amp[1665] - 1./3. * amp[1667] + Complex<double> (0, 1) * amp[1670] -
      1./3. * amp[1672] - Complex<double> (0, 1) * amp[1675] - 1./3. *
      amp[1677] - 1./3. * amp[1679]);
  jamp[2] = +1./2. * (-1./3. * amp[1440] - 1./3. * amp[1441] - amp[1442] -
      1./3. * amp[1443] - amp[1444] - 1./3. * amp[1445] - amp[1446] - amp[1447]
      - 1./3. * amp[1448] - 1./3. * amp[1449] - amp[1450] - 1./3. * amp[1451] -
      amp[1452] - 1./3. * amp[1453] - amp[1454] - amp[1455] - 1./3. * amp[1456]
      - 1./3. * amp[1457] - 1./3. * amp[1458] - 1./3. * amp[1459] - 1./3. *
      amp[1460] - 1./3. * amp[1461] - amp[1462] - 1./3. * amp[1463] - amp[1464]
      - 1./3. * amp[1465] - amp[1466] - 1./3. * amp[1467] - amp[1468] -
      amp[1469] - amp[1470] - 1./3. * amp[1471] - amp[1472] - 1./3. * amp[1473]
      - amp[1474] - 1./3. * amp[1475] - amp[1476] - amp[1477] - amp[1478] -
      amp[1479] - amp[1520] - 1./3. * amp[1521] - 1./3. * amp[1522] - amp[1523]
      - 1./3. * amp[1524] - amp[1525] - amp[1526] - 1./3. * amp[1527] -
      amp[1528] - 1./3. * amp[1529] - 1./3. * amp[1530] - amp[1531] - 1./3. *
      amp[1532] - amp[1533] - amp[1534] - 1./3. * amp[1535] - amp[1536] -
      amp[1537] - amp[1538] - amp[1539] - amp[1540] - amp[1541] - 1./3. *
      amp[1542] - 1./3. * amp[1543] - 1./3. * amp[1544] - 1./3. * amp[1545] -
      1./3. * amp[1546] - 1./3. * amp[1547] - amp[1548] - amp[1549] - 1./3. *
      amp[1550] - amp[1551] - 1./3. * amp[1552] - 1./3. * amp[1553] - amp[1554]
      - 1./3. * amp[1555] - amp[1556] - amp[1557] - 1./3. * amp[1558] - 1./3. *
      amp[1559] - 1./3. * amp[1561] - amp[1564] + Complex<double> (0, 1) *
      amp[1565] - Complex<double> (0, 1) * amp[1568] - amp[1574] - 1./3. *
      amp[1575] - Complex<double> (0, 1) * amp[1576] + Complex<double> (0, 1) *
      amp[1582] - 1./3. * amp[1585] - amp[1588] + Complex<double> (0, 1) *
      amp[1589] - Complex<double> (0, 1) * amp[1592] - amp[1598] - 1./3. *
      amp[1599] - Complex<double> (0, 1) * amp[1600] + Complex<double> (0, 1) *
      amp[1606] - Complex<double> (0, 1) * amp[1608] - Complex<double> (0, 1) *
      amp[1611] - amp[1613] - Complex<double> (0, 1) * amp[1614] - amp[1615] -
      amp[1616] - Complex<double> (0, 1) * amp[1617] - amp[1618] -
      Complex<double> (0, 1) * amp[1620] - Complex<double> (0, 1) * amp[1623] -
      amp[1624] - amp[1625] - 1./3. * amp[1627] - 1./3. * amp[1628] - 1./3. *
      amp[1630] - 1./3. * amp[1631] - 1./3. * amp[1633] - 1./3. * amp[1634] -
      1./3. * amp[1636] - 1./3. * amp[1637] - 1./3. * amp[1639] - 1./3. *
      amp[1640] - 1./3. * amp[1642] - 1./3. * amp[1643] - 1./3. * amp[1644] -
      amp[1645] - Complex<double> (0, 1) * amp[1648] - amp[1650] - amp[1651] -
      Complex<double> (0, 1) * amp[1652] - amp[1653] + Complex<double> (0, 1) *
      amp[1654] - amp[1655] - 1./3. * amp[1658] - amp[1659] - Complex<double>
      (0, 1) * amp[1661] - 1./3. * amp[1663] - amp[1668] - amp[1669] -
      Complex<double> (0, 1) * amp[1670] - 1./3. * amp[1671] - amp[1674] +
      Complex<double> (0, 1) * amp[1675] - amp[1676]);
  jamp[3] = +1./2. * (+amp[1400] + amp[1401] + 1./3. * amp[1402] + amp[1403] +
      1./3. * amp[1404] + amp[1405] + 1./3. * amp[1406] + 1./3. * amp[1407] +
      1./3. * amp[1408] + 1./3. * amp[1409] + amp[1410] + 1./3. * amp[1411] +
      amp[1412] + 1./3. * amp[1413] + amp[1414] + amp[1415] + amp[1416] + 1./3.
      * amp[1417] + 1./3. * amp[1418] + amp[1419] + 1./3. * amp[1420] +
      amp[1421] + amp[1422] + 1./3. * amp[1423] + amp[1424] + amp[1425] + 1./3.
      * amp[1426] + 1./3. * amp[1427] + 1./3. * amp[1428] + 1./3. * amp[1429] +
      amp[1430] + amp[1431] + amp[1432] + 1./3. * amp[1433] + 1./3. * amp[1434]
      + amp[1435] + amp[1436] + amp[1437] + 1./3. * amp[1438] + 1./3. *
      amp[1439] + amp[1440] + amp[1441] + 1./3. * amp[1442] + amp[1443] + 1./3.
      * amp[1444] + amp[1445] + 1./3. * amp[1446] + 1./3. * amp[1447] +
      amp[1448] + amp[1449] + 1./3. * amp[1450] + amp[1451] + 1./3. * amp[1452]
      + amp[1453] + 1./3. * amp[1454] + 1./3. * amp[1455] + amp[1456] +
      amp[1457] + amp[1458] + amp[1459] + amp[1460] + amp[1461] + 1./3. *
      amp[1462] + amp[1463] + 1./3. * amp[1464] + amp[1465] + 1./3. * amp[1466]
      + amp[1467] + 1./3. * amp[1468] + 1./3. * amp[1469] + 1./3. * amp[1470] +
      amp[1471] + 1./3. * amp[1472] + amp[1473] + 1./3. * amp[1474] + amp[1475]
      + 1./3. * amp[1476] + 1./3. * amp[1477] + 1./3. * amp[1478] + 1./3. *
      amp[1479] + amp[1560] + amp[1561] - Complex<double> (0, 1) * amp[1562] +
      1./3. * amp[1563] + 1./3. * amp[1564] + 1./3. * amp[1566] + amp[1569] +
      Complex<double> (0, 1) * amp[1571] + amp[1572] + 1./3. * amp[1573] -
      Complex<double> (0, 1) * amp[1577] + amp[1578] + Complex<double> (0, 1) *
      amp[1579] + amp[1580] + 1./3. * amp[1581] + 1./3. * amp[1583] + amp[1584]
      + amp[1585] - Complex<double> (0, 1) * amp[1586] + 1./3. * amp[1587] +
      1./3. * amp[1588] + 1./3. * amp[1590] + amp[1593] + Complex<double> (0,
      1) * amp[1595] + amp[1596] + 1./3. * amp[1597] - Complex<double> (0, 1) *
      amp[1601] + amp[1602] + Complex<double> (0, 1) * amp[1603] + amp[1604] +
      1./3. * amp[1605] + 1./3. * amp[1607] - Complex<double> (0, 1) *
      amp[1626] + amp[1627] + amp[1628] - Complex<double> (0, 1) * amp[1629] +
      amp[1630] - Complex<double> (0, 1) * amp[1632] - Complex<double> (0, 1) *
      amp[1635] + amp[1637] - Complex<double> (0, 1) * amp[1638] + amp[1639] +
      amp[1640] - Complex<double> (0, 1) * amp[1641] + amp[1644] + 1./3. *
      amp[1645] - Complex<double> (0, 1) * amp[1649] + 1./3. * amp[1650] +
      1./3. * amp[1653] + 1./3. * amp[1655] + amp[1658] + 1./3. * amp[1659] -
      Complex<double> (0, 1) * amp[1660] - Complex<double> (0, 1) * amp[1664] +
      Complex<double> (0, 1) * amp[1666] + 1./3. * amp[1669] - Complex<double>
      (0, 1) * amp[1673] + 1./3. * amp[1674] + 1./3. * amp[1676] +
      Complex<double> (0, 1) * amp[1678]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[5][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P10_sm_qq_zzgqq::matrix_12_uc_zzguc() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 140;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[1680] + 1./3. * amp[1681] + 1./3. *
      amp[1682] + 1./3. * amp[1683] + 1./3. * amp[1684] + 1./3. * amp[1685] +
      1./3. * amp[1686] + 1./3. * amp[1687] + 1./3. * amp[1688] + 1./3. *
      amp[1689] + 1./3. * amp[1690] + 1./3. * amp[1691] + 1./3. * amp[1692] +
      1./3. * amp[1693] + 1./3. * amp[1694] + 1./3. * amp[1695] + 1./3. *
      amp[1696] + 1./3. * amp[1697] + 1./3. * amp[1698] + 1./3. * amp[1699] +
      1./3. * amp[1700] + 1./3. * amp[1701] + 1./3. * amp[1702] + 1./3. *
      amp[1703] + 1./3. * amp[1704] + 1./3. * amp[1705] + 1./3. * amp[1706] +
      1./3. * amp[1707] + 1./3. * amp[1708] + 1./3. * amp[1709] + 1./3. *
      amp[1710] + 1./3. * amp[1711] + 1./3. * amp[1712] + 1./3. * amp[1713] +
      1./3. * amp[1714] + 1./3. * amp[1715] + 1./3. * amp[1716] + 1./3. *
      amp[1717] + 1./3. * amp[1718] + 1./3. * amp[1719] + 1./3. * amp[1760] +
      1./3. * amp[1761] + 1./3. * amp[1763] + 1./3. * amp[1766] + 1./3. *
      amp[1769] + 1./3. * amp[1771] + 1./3. * amp[1772] + 1./3. * amp[1773] +
      1./3. * amp[1775] + 1./3. * amp[1778] + 1./3. * amp[1781] + 1./3. *
      amp[1783] + 1./3. * amp[1802] + 1./3. * amp[1805] + 1./3. * amp[1808] +
      1./3. * amp[1810] + 1./3. * amp[1812] + 1./3. * amp[1815] + 1./3. *
      amp[1817] + 1./3. * amp[1819]);
  jamp[1] = +1./2. * (-amp[1700] - amp[1701] - amp[1702] - amp[1703] -
      amp[1704] - amp[1705] - amp[1706] - amp[1707] - amp[1708] - amp[1709] -
      amp[1710] - amp[1711] - amp[1712] - amp[1713] - amp[1714] - amp[1715] -
      amp[1716] - amp[1717] - amp[1718] - amp[1719] - amp[1740] - amp[1741] -
      amp[1742] - amp[1743] - amp[1744] - amp[1745] - amp[1746] - amp[1747] -
      amp[1748] - amp[1749] - amp[1750] - amp[1751] - amp[1752] - amp[1753] -
      amp[1754] - amp[1755] - amp[1756] - amp[1757] - amp[1758] - amp[1759] -
      amp[1761] + Complex<double> (0, 1) * amp[1762] - Complex<double> (0, 1) *
      amp[1765] - amp[1767] - Complex<double> (0, 1) * amp[1768] +
      Complex<double> (0, 1) * amp[1770] - amp[1773] + Complex<double> (0, 1) *
      amp[1774] - Complex<double> (0, 1) * amp[1777] - amp[1779] -
      Complex<double> (0, 1) * amp[1780] + Complex<double> (0, 1) * amp[1782] -
      Complex<double> (0, 1) * amp[1784] - Complex<double> (0, 1) * amp[1787] -
      amp[1789] - Complex<double> (0, 1) * amp[1790] - amp[1791] - amp[1792] -
      Complex<double> (0, 1) * amp[1793] - amp[1794] - Complex<double> (0, 1) *
      amp[1796] - Complex<double> (0, 1) * amp[1799] - amp[1800] - amp[1801] -
      amp[1802] - Complex<double> (0, 1) * amp[1804] - amp[1805] - amp[1806] -
      Complex<double> (0, 1) * amp[1807] - amp[1808] + Complex<double> (0, 1) *
      amp[1809] - amp[1810] - amp[1812] - Complex<double> (0, 1) * amp[1813] -
      amp[1814] - amp[1815] - Complex<double> (0, 1) * amp[1816] - amp[1817] +
      Complex<double> (0, 1) * amp[1818] - amp[1819]);
  jamp[2] = +1./2. * (-amp[1680] - amp[1681] - amp[1682] - amp[1683] -
      amp[1684] - amp[1685] - amp[1686] - amp[1687] - amp[1688] - amp[1689] -
      amp[1690] - amp[1691] - amp[1692] - amp[1693] - amp[1694] - amp[1695] -
      amp[1696] - amp[1697] - amp[1698] - amp[1699] - amp[1720] - amp[1721] -
      amp[1722] - amp[1723] - amp[1724] - amp[1725] - amp[1726] - amp[1727] -
      amp[1728] - amp[1729] - amp[1730] - amp[1731] - amp[1732] - amp[1733] -
      amp[1734] - amp[1735] - amp[1736] - amp[1737] - amp[1738] - amp[1739] -
      amp[1760] - Complex<double> (0, 1) * amp[1762] - amp[1763] - amp[1764] +
      Complex<double> (0, 1) * amp[1765] - amp[1766] + Complex<double> (0, 1) *
      amp[1768] - amp[1769] - Complex<double> (0, 1) * amp[1770] - amp[1771] -
      amp[1772] - Complex<double> (0, 1) * amp[1774] - amp[1775] - amp[1776] +
      Complex<double> (0, 1) * amp[1777] - amp[1778] + Complex<double> (0, 1) *
      amp[1780] - amp[1781] - Complex<double> (0, 1) * amp[1782] - amp[1783] +
      Complex<double> (0, 1) * amp[1784] - amp[1785] - amp[1786] +
      Complex<double> (0, 1) * amp[1787] - amp[1788] + Complex<double> (0, 1) *
      amp[1790] + Complex<double> (0, 1) * amp[1793] - amp[1795] +
      Complex<double> (0, 1) * amp[1796] - amp[1797] - amp[1798] +
      Complex<double> (0, 1) * amp[1799] - amp[1803] + Complex<double> (0, 1) *
      amp[1804] + Complex<double> (0, 1) * amp[1807] - Complex<double> (0, 1) *
      amp[1809] - amp[1811] + Complex<double> (0, 1) * amp[1813] +
      Complex<double> (0, 1) * amp[1816] - Complex<double> (0, 1) * amp[1818]);
  jamp[3] = +1./2. * (+1./3. * amp[1720] + 1./3. * amp[1721] + 1./3. *
      amp[1722] + 1./3. * amp[1723] + 1./3. * amp[1724] + 1./3. * amp[1725] +
      1./3. * amp[1726] + 1./3. * amp[1727] + 1./3. * amp[1728] + 1./3. *
      amp[1729] + 1./3. * amp[1730] + 1./3. * amp[1731] + 1./3. * amp[1732] +
      1./3. * amp[1733] + 1./3. * amp[1734] + 1./3. * amp[1735] + 1./3. *
      amp[1736] + 1./3. * amp[1737] + 1./3. * amp[1738] + 1./3. * amp[1739] +
      1./3. * amp[1740] + 1./3. * amp[1741] + 1./3. * amp[1742] + 1./3. *
      amp[1743] + 1./3. * amp[1744] + 1./3. * amp[1745] + 1./3. * amp[1746] +
      1./3. * amp[1747] + 1./3. * amp[1748] + 1./3. * amp[1749] + 1./3. *
      amp[1750] + 1./3. * amp[1751] + 1./3. * amp[1752] + 1./3. * amp[1753] +
      1./3. * amp[1754] + 1./3. * amp[1755] + 1./3. * amp[1756] + 1./3. *
      amp[1757] + 1./3. * amp[1758] + 1./3. * amp[1759] + 1./3. * amp[1764] +
      1./3. * amp[1767] + 1./3. * amp[1776] + 1./3. * amp[1779] + 1./3. *
      amp[1785] + 1./3. * amp[1786] + 1./3. * amp[1788] + 1./3. * amp[1789] +
      1./3. * amp[1791] + 1./3. * amp[1792] + 1./3. * amp[1794] + 1./3. *
      amp[1795] + 1./3. * amp[1797] + 1./3. * amp[1798] + 1./3. * amp[1800] +
      1./3. * amp[1801] + 1./3. * amp[1803] + 1./3. * amp[1806] + 1./3. *
      amp[1811] + 1./3. * amp[1814]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[6][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P10_sm_qq_zzgqq::matrix_12_ud_zzgud() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 140;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[1820] + 1./3. * amp[1821] + 1./3. *
      amp[1822] + 1./3. * amp[1823] + 1./3. * amp[1824] + 1./3. * amp[1825] +
      1./3. * amp[1826] + 1./3. * amp[1827] + 1./3. * amp[1828] + 1./3. *
      amp[1829] + 1./3. * amp[1830] + 1./3. * amp[1831] + 1./3. * amp[1832] +
      1./3. * amp[1833] + 1./3. * amp[1834] + 1./3. * amp[1835] + 1./3. *
      amp[1836] + 1./3. * amp[1837] + 1./3. * amp[1838] + 1./3. * amp[1839] +
      1./3. * amp[1840] + 1./3. * amp[1841] + 1./3. * amp[1842] + 1./3. *
      amp[1843] + 1./3. * amp[1844] + 1./3. * amp[1845] + 1./3. * amp[1846] +
      1./3. * amp[1847] + 1./3. * amp[1848] + 1./3. * amp[1849] + 1./3. *
      amp[1850] + 1./3. * amp[1851] + 1./3. * amp[1852] + 1./3. * amp[1853] +
      1./3. * amp[1854] + 1./3. * amp[1855] + 1./3. * amp[1856] + 1./3. *
      amp[1857] + 1./3. * amp[1858] + 1./3. * amp[1859] + 1./3. * amp[1900] +
      1./3. * amp[1901] + 1./3. * amp[1903] + 1./3. * amp[1906] + 1./3. *
      amp[1909] + 1./3. * amp[1911] + 1./3. * amp[1912] + 1./3. * amp[1913] +
      1./3. * amp[1915] + 1./3. * amp[1918] + 1./3. * amp[1921] + 1./3. *
      amp[1923] + 1./3. * amp[1942] + 1./3. * amp[1945] + 1./3. * amp[1948] +
      1./3. * amp[1950] + 1./3. * amp[1952] + 1./3. * amp[1955] + 1./3. *
      amp[1957] + 1./3. * amp[1959]);
  jamp[1] = +1./2. * (-amp[1840] - amp[1841] - amp[1842] - amp[1843] -
      amp[1844] - amp[1845] - amp[1846] - amp[1847] - amp[1848] - amp[1849] -
      amp[1850] - amp[1851] - amp[1852] - amp[1853] - amp[1854] - amp[1855] -
      amp[1856] - amp[1857] - amp[1858] - amp[1859] - amp[1880] - amp[1881] -
      amp[1882] - amp[1883] - amp[1884] - amp[1885] - amp[1886] - amp[1887] -
      amp[1888] - amp[1889] - amp[1890] - amp[1891] - amp[1892] - amp[1893] -
      amp[1894] - amp[1895] - amp[1896] - amp[1897] - amp[1898] - amp[1899] -
      amp[1901] + Complex<double> (0, 1) * amp[1902] - Complex<double> (0, 1) *
      amp[1905] - amp[1907] - Complex<double> (0, 1) * amp[1908] +
      Complex<double> (0, 1) * amp[1910] - amp[1913] + Complex<double> (0, 1) *
      amp[1914] - Complex<double> (0, 1) * amp[1917] - amp[1919] -
      Complex<double> (0, 1) * amp[1920] + Complex<double> (0, 1) * amp[1922] -
      Complex<double> (0, 1) * amp[1924] - Complex<double> (0, 1) * amp[1927] -
      amp[1929] - Complex<double> (0, 1) * amp[1930] - amp[1931] - amp[1932] -
      Complex<double> (0, 1) * amp[1933] - amp[1934] - Complex<double> (0, 1) *
      amp[1936] - Complex<double> (0, 1) * amp[1939] - amp[1940] - amp[1941] -
      amp[1942] - Complex<double> (0, 1) * amp[1944] - amp[1945] - amp[1946] -
      Complex<double> (0, 1) * amp[1947] - amp[1948] + Complex<double> (0, 1) *
      amp[1949] - amp[1950] - amp[1952] - Complex<double> (0, 1) * amp[1953] -
      amp[1954] - amp[1955] - Complex<double> (0, 1) * amp[1956] - amp[1957] +
      Complex<double> (0, 1) * amp[1958] - amp[1959]);
  jamp[2] = +1./2. * (-amp[1820] - amp[1821] - amp[1822] - amp[1823] -
      amp[1824] - amp[1825] - amp[1826] - amp[1827] - amp[1828] - amp[1829] -
      amp[1830] - amp[1831] - amp[1832] - amp[1833] - amp[1834] - amp[1835] -
      amp[1836] - amp[1837] - amp[1838] - amp[1839] - amp[1860] - amp[1861] -
      amp[1862] - amp[1863] - amp[1864] - amp[1865] - amp[1866] - amp[1867] -
      amp[1868] - amp[1869] - amp[1870] - amp[1871] - amp[1872] - amp[1873] -
      amp[1874] - amp[1875] - amp[1876] - amp[1877] - amp[1878] - amp[1879] -
      amp[1900] - Complex<double> (0, 1) * amp[1902] - amp[1903] - amp[1904] +
      Complex<double> (0, 1) * amp[1905] - amp[1906] + Complex<double> (0, 1) *
      amp[1908] - amp[1909] - Complex<double> (0, 1) * amp[1910] - amp[1911] -
      amp[1912] - Complex<double> (0, 1) * amp[1914] - amp[1915] - amp[1916] +
      Complex<double> (0, 1) * amp[1917] - amp[1918] + Complex<double> (0, 1) *
      amp[1920] - amp[1921] - Complex<double> (0, 1) * amp[1922] - amp[1923] +
      Complex<double> (0, 1) * amp[1924] - amp[1925] - amp[1926] +
      Complex<double> (0, 1) * amp[1927] - amp[1928] + Complex<double> (0, 1) *
      amp[1930] + Complex<double> (0, 1) * amp[1933] - amp[1935] +
      Complex<double> (0, 1) * amp[1936] - amp[1937] - amp[1938] +
      Complex<double> (0, 1) * amp[1939] - amp[1943] + Complex<double> (0, 1) *
      amp[1944] + Complex<double> (0, 1) * amp[1947] - Complex<double> (0, 1) *
      amp[1949] - amp[1951] + Complex<double> (0, 1) * amp[1953] +
      Complex<double> (0, 1) * amp[1956] - Complex<double> (0, 1) * amp[1958]);
  jamp[3] = +1./2. * (+1./3. * amp[1860] + 1./3. * amp[1861] + 1./3. *
      amp[1862] + 1./3. * amp[1863] + 1./3. * amp[1864] + 1./3. * amp[1865] +
      1./3. * amp[1866] + 1./3. * amp[1867] + 1./3. * amp[1868] + 1./3. *
      amp[1869] + 1./3. * amp[1870] + 1./3. * amp[1871] + 1./3. * amp[1872] +
      1./3. * amp[1873] + 1./3. * amp[1874] + 1./3. * amp[1875] + 1./3. *
      amp[1876] + 1./3. * amp[1877] + 1./3. * amp[1878] + 1./3. * amp[1879] +
      1./3. * amp[1880] + 1./3. * amp[1881] + 1./3. * amp[1882] + 1./3. *
      amp[1883] + 1./3. * amp[1884] + 1./3. * amp[1885] + 1./3. * amp[1886] +
      1./3. * amp[1887] + 1./3. * amp[1888] + 1./3. * amp[1889] + 1./3. *
      amp[1890] + 1./3. * amp[1891] + 1./3. * amp[1892] + 1./3. * amp[1893] +
      1./3. * amp[1894] + 1./3. * amp[1895] + 1./3. * amp[1896] + 1./3. *
      amp[1897] + 1./3. * amp[1898] + 1./3. * amp[1899] + 1./3. * amp[1904] +
      1./3. * amp[1907] + 1./3. * amp[1916] + 1./3. * amp[1919] + 1./3. *
      amp[1925] + 1./3. * amp[1926] + 1./3. * amp[1928] + 1./3. * amp[1929] +
      1./3. * amp[1931] + 1./3. * amp[1932] + 1./3. * amp[1934] + 1./3. *
      amp[1935] + 1./3. * amp[1937] + 1./3. * amp[1938] + 1./3. * amp[1940] +
      1./3. * amp[1941] + 1./3. * amp[1943] + 1./3. * amp[1946] + 1./3. *
      amp[1951] + 1./3. * amp[1954]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[7][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P10_sm_qq_zzgqq::matrix_12_uux_zzgccx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 140;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[2000] + 1./3. * amp[2001] + 1./3. *
      amp[2002] + 1./3. * amp[2003] + 1./3. * amp[2004] + 1./3. * amp[2005] +
      1./3. * amp[2006] + 1./3. * amp[2007] + 1./3. * amp[2008] + 1./3. *
      amp[2009] + 1./3. * amp[2010] + 1./3. * amp[2011] + 1./3. * amp[2012] +
      1./3. * amp[2013] + 1./3. * amp[2014] + 1./3. * amp[2015] + 1./3. *
      amp[2016] + 1./3. * amp[2017] + 1./3. * amp[2018] + 1./3. * amp[2019] +
      1./3. * amp[2020] + 1./3. * amp[2021] + 1./3. * amp[2022] + 1./3. *
      amp[2023] + 1./3. * amp[2024] + 1./3. * amp[2025] + 1./3. * amp[2026] +
      1./3. * amp[2027] + 1./3. * amp[2028] + 1./3. * amp[2029] + 1./3. *
      amp[2030] + 1./3. * amp[2031] + 1./3. * amp[2032] + 1./3. * amp[2033] +
      1./3. * amp[2034] + 1./3. * amp[2035] + 1./3. * amp[2036] + 1./3. *
      amp[2037] + 1./3. * amp[2038] + 1./3. * amp[2039] + 1./3. * amp[2044] +
      1./3. * amp[2047] + 1./3. * amp[2056] + 1./3. * amp[2059] + 1./3. *
      amp[2065] + 1./3. * amp[2066] + 1./3. * amp[2068] + 1./3. * amp[2069] +
      1./3. * amp[2071] + 1./3. * amp[2072] + 1./3. * amp[2074] + 1./3. *
      amp[2075] + 1./3. * amp[2077] + 1./3. * amp[2078] + 1./3. * amp[2080] +
      1./3. * amp[2081] + 1./3. * amp[2083] + 1./3. * amp[2086] + 1./3. *
      amp[2091] + 1./3. * amp[2094]);
  jamp[1] = +1./2. * (-amp[1960] - amp[1961] - amp[1962] - amp[1963] -
      amp[1964] - amp[1965] - amp[1966] - amp[1967] - amp[1968] - amp[1969] -
      amp[1970] - amp[1971] - amp[1972] - amp[1973] - amp[1974] - amp[1975] -
      amp[1976] - amp[1977] - amp[1978] - amp[1979] - amp[2000] - amp[2001] -
      amp[2002] - amp[2003] - amp[2004] - amp[2005] - amp[2006] - amp[2007] -
      amp[2008] - amp[2009] - amp[2010] - amp[2011] - amp[2012] - amp[2013] -
      amp[2014] - amp[2015] - amp[2016] - amp[2017] - amp[2018] - amp[2019] -
      amp[2040] - Complex<double> (0, 1) * amp[2042] - amp[2043] - amp[2044] +
      Complex<double> (0, 1) * amp[2045] - amp[2046] + Complex<double> (0, 1) *
      amp[2048] - amp[2049] - Complex<double> (0, 1) * amp[2050] - amp[2051] -
      amp[2052] - Complex<double> (0, 1) * amp[2054] - amp[2055] - amp[2056] +
      Complex<double> (0, 1) * amp[2057] - amp[2058] + Complex<double> (0, 1) *
      amp[2060] - amp[2061] - Complex<double> (0, 1) * amp[2062] - amp[2063] +
      Complex<double> (0, 1) * amp[2064] - amp[2065] - amp[2066] +
      Complex<double> (0, 1) * amp[2067] - amp[2068] + Complex<double> (0, 1) *
      amp[2070] + Complex<double> (0, 1) * amp[2073] - amp[2075] +
      Complex<double> (0, 1) * amp[2076] - amp[2077] - amp[2078] +
      Complex<double> (0, 1) * amp[2079] - amp[2083] + Complex<double> (0, 1) *
      amp[2084] + Complex<double> (0, 1) * amp[2087] - Complex<double> (0, 1) *
      amp[2089] - amp[2091] + Complex<double> (0, 1) * amp[2093] +
      Complex<double> (0, 1) * amp[2096] - Complex<double> (0, 1) * amp[2098]);
  jamp[2] = +1./2. * (+1./3. * amp[1960] + 1./3. * amp[1961] + 1./3. *
      amp[1962] + 1./3. * amp[1963] + 1./3. * amp[1964] + 1./3. * amp[1965] +
      1./3. * amp[1966] + 1./3. * amp[1967] + 1./3. * amp[1968] + 1./3. *
      amp[1969] + 1./3. * amp[1970] + 1./3. * amp[1971] + 1./3. * amp[1972] +
      1./3. * amp[1973] + 1./3. * amp[1974] + 1./3. * amp[1975] + 1./3. *
      amp[1976] + 1./3. * amp[1977] + 1./3. * amp[1978] + 1./3. * amp[1979] +
      1./3. * amp[1980] + 1./3. * amp[1981] + 1./3. * amp[1982] + 1./3. *
      amp[1983] + 1./3. * amp[1984] + 1./3. * amp[1985] + 1./3. * amp[1986] +
      1./3. * amp[1987] + 1./3. * amp[1988] + 1./3. * amp[1989] + 1./3. *
      amp[1990] + 1./3. * amp[1991] + 1./3. * amp[1992] + 1./3. * amp[1993] +
      1./3. * amp[1994] + 1./3. * amp[1995] + 1./3. * amp[1996] + 1./3. *
      amp[1997] + 1./3. * amp[1998] + 1./3. * amp[1999] + 1./3. * amp[2040] +
      1./3. * amp[2041] + 1./3. * amp[2043] + 1./3. * amp[2046] + 1./3. *
      amp[2049] + 1./3. * amp[2051] + 1./3. * amp[2052] + 1./3. * amp[2053] +
      1./3. * amp[2055] + 1./3. * amp[2058] + 1./3. * amp[2061] + 1./3. *
      amp[2063] + 1./3. * amp[2082] + 1./3. * amp[2085] + 1./3. * amp[2088] +
      1./3. * amp[2090] + 1./3. * amp[2092] + 1./3. * amp[2095] + 1./3. *
      amp[2097] + 1./3. * amp[2099]);
  jamp[3] = +1./2. * (-amp[1980] - amp[1981] - amp[1982] - amp[1983] -
      amp[1984] - amp[1985] - amp[1986] - amp[1987] - amp[1988] - amp[1989] -
      amp[1990] - amp[1991] - amp[1992] - amp[1993] - amp[1994] - amp[1995] -
      amp[1996] - amp[1997] - amp[1998] - amp[1999] - amp[2020] - amp[2021] -
      amp[2022] - amp[2023] - amp[2024] - amp[2025] - amp[2026] - amp[2027] -
      amp[2028] - amp[2029] - amp[2030] - amp[2031] - amp[2032] - amp[2033] -
      amp[2034] - amp[2035] - amp[2036] - amp[2037] - amp[2038] - amp[2039] -
      amp[2041] + Complex<double> (0, 1) * amp[2042] - Complex<double> (0, 1) *
      amp[2045] - amp[2047] - Complex<double> (0, 1) * amp[2048] +
      Complex<double> (0, 1) * amp[2050] - amp[2053] + Complex<double> (0, 1) *
      amp[2054] - Complex<double> (0, 1) * amp[2057] - amp[2059] -
      Complex<double> (0, 1) * amp[2060] + Complex<double> (0, 1) * amp[2062] -
      Complex<double> (0, 1) * amp[2064] - Complex<double> (0, 1) * amp[2067] -
      amp[2069] - Complex<double> (0, 1) * amp[2070] - amp[2071] - amp[2072] -
      Complex<double> (0, 1) * amp[2073] - amp[2074] - Complex<double> (0, 1) *
      amp[2076] - Complex<double> (0, 1) * amp[2079] - amp[2080] - amp[2081] -
      amp[2082] - Complex<double> (0, 1) * amp[2084] - amp[2085] - amp[2086] -
      Complex<double> (0, 1) * amp[2087] - amp[2088] + Complex<double> (0, 1) *
      amp[2089] - amp[2090] - amp[2092] - Complex<double> (0, 1) * amp[2093] -
      amp[2094] - amp[2095] - Complex<double> (0, 1) * amp[2096] - amp[2097] +
      Complex<double> (0, 1) * amp[2098] - amp[2099]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[8][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P10_sm_qq_zzgqq::matrix_12_uux_zzgddx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 140;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[2140] + 1./3. * amp[2141] + 1./3. *
      amp[2142] + 1./3. * amp[2143] + 1./3. * amp[2144] + 1./3. * amp[2145] +
      1./3. * amp[2146] + 1./3. * amp[2147] + 1./3. * amp[2148] + 1./3. *
      amp[2149] + 1./3. * amp[2150] + 1./3. * amp[2151] + 1./3. * amp[2152] +
      1./3. * amp[2153] + 1./3. * amp[2154] + 1./3. * amp[2155] + 1./3. *
      amp[2156] + 1./3. * amp[2157] + 1./3. * amp[2158] + 1./3. * amp[2159] +
      1./3. * amp[2160] + 1./3. * amp[2161] + 1./3. * amp[2162] + 1./3. *
      amp[2163] + 1./3. * amp[2164] + 1./3. * amp[2165] + 1./3. * amp[2166] +
      1./3. * amp[2167] + 1./3. * amp[2168] + 1./3. * amp[2169] + 1./3. *
      amp[2170] + 1./3. * amp[2171] + 1./3. * amp[2172] + 1./3. * amp[2173] +
      1./3. * amp[2174] + 1./3. * amp[2175] + 1./3. * amp[2176] + 1./3. *
      amp[2177] + 1./3. * amp[2178] + 1./3. * amp[2179] + 1./3. * amp[2184] +
      1./3. * amp[2187] + 1./3. * amp[2196] + 1./3. * amp[2199] + 1./3. *
      amp[2205] + 1./3. * amp[2206] + 1./3. * amp[2208] + 1./3. * amp[2209] +
      1./3. * amp[2211] + 1./3. * amp[2212] + 1./3. * amp[2214] + 1./3. *
      amp[2215] + 1./3. * amp[2217] + 1./3. * amp[2218] + 1./3. * amp[2220] +
      1./3. * amp[2221] + 1./3. * amp[2223] + 1./3. * amp[2226] + 1./3. *
      amp[2231] + 1./3. * amp[2234]);
  jamp[1] = +1./2. * (-amp[2100] - amp[2101] - amp[2102] - amp[2103] -
      amp[2104] - amp[2105] - amp[2106] - amp[2107] - amp[2108] - amp[2109] -
      amp[2110] - amp[2111] - amp[2112] - amp[2113] - amp[2114] - amp[2115] -
      amp[2116] - amp[2117] - amp[2118] - amp[2119] - amp[2140] - amp[2141] -
      amp[2142] - amp[2143] - amp[2144] - amp[2145] - amp[2146] - amp[2147] -
      amp[2148] - amp[2149] - amp[2150] - amp[2151] - amp[2152] - amp[2153] -
      amp[2154] - amp[2155] - amp[2156] - amp[2157] - amp[2158] - amp[2159] -
      amp[2180] - Complex<double> (0, 1) * amp[2182] - amp[2183] - amp[2184] +
      Complex<double> (0, 1) * amp[2185] - amp[2186] + Complex<double> (0, 1) *
      amp[2188] - amp[2189] - Complex<double> (0, 1) * amp[2190] - amp[2191] -
      amp[2192] - Complex<double> (0, 1) * amp[2194] - amp[2195] - amp[2196] +
      Complex<double> (0, 1) * amp[2197] - amp[2198] + Complex<double> (0, 1) *
      amp[2200] - amp[2201] - Complex<double> (0, 1) * amp[2202] - amp[2203] +
      Complex<double> (0, 1) * amp[2204] - amp[2205] - amp[2206] +
      Complex<double> (0, 1) * amp[2207] - amp[2208] + Complex<double> (0, 1) *
      amp[2210] + Complex<double> (0, 1) * amp[2213] - amp[2215] +
      Complex<double> (0, 1) * amp[2216] - amp[2217] - amp[2218] +
      Complex<double> (0, 1) * amp[2219] - amp[2223] + Complex<double> (0, 1) *
      amp[2224] + Complex<double> (0, 1) * amp[2227] - Complex<double> (0, 1) *
      amp[2229] - amp[2231] + Complex<double> (0, 1) * amp[2233] +
      Complex<double> (0, 1) * amp[2236] - Complex<double> (0, 1) * amp[2238]);
  jamp[2] = +1./2. * (+1./3. * amp[2100] + 1./3. * amp[2101] + 1./3. *
      amp[2102] + 1./3. * amp[2103] + 1./3. * amp[2104] + 1./3. * amp[2105] +
      1./3. * amp[2106] + 1./3. * amp[2107] + 1./3. * amp[2108] + 1./3. *
      amp[2109] + 1./3. * amp[2110] + 1./3. * amp[2111] + 1./3. * amp[2112] +
      1./3. * amp[2113] + 1./3. * amp[2114] + 1./3. * amp[2115] + 1./3. *
      amp[2116] + 1./3. * amp[2117] + 1./3. * amp[2118] + 1./3. * amp[2119] +
      1./3. * amp[2120] + 1./3. * amp[2121] + 1./3. * amp[2122] + 1./3. *
      amp[2123] + 1./3. * amp[2124] + 1./3. * amp[2125] + 1./3. * amp[2126] +
      1./3. * amp[2127] + 1./3. * amp[2128] + 1./3. * amp[2129] + 1./3. *
      amp[2130] + 1./3. * amp[2131] + 1./3. * amp[2132] + 1./3. * amp[2133] +
      1./3. * amp[2134] + 1./3. * amp[2135] + 1./3. * amp[2136] + 1./3. *
      amp[2137] + 1./3. * amp[2138] + 1./3. * amp[2139] + 1./3. * amp[2180] +
      1./3. * amp[2181] + 1./3. * amp[2183] + 1./3. * amp[2186] + 1./3. *
      amp[2189] + 1./3. * amp[2191] + 1./3. * amp[2192] + 1./3. * amp[2193] +
      1./3. * amp[2195] + 1./3. * amp[2198] + 1./3. * amp[2201] + 1./3. *
      amp[2203] + 1./3. * amp[2222] + 1./3. * amp[2225] + 1./3. * amp[2228] +
      1./3. * amp[2230] + 1./3. * amp[2232] + 1./3. * amp[2235] + 1./3. *
      amp[2237] + 1./3. * amp[2239]);
  jamp[3] = +1./2. * (-amp[2120] - amp[2121] - amp[2122] - amp[2123] -
      amp[2124] - amp[2125] - amp[2126] - amp[2127] - amp[2128] - amp[2129] -
      amp[2130] - amp[2131] - amp[2132] - amp[2133] - amp[2134] - amp[2135] -
      amp[2136] - amp[2137] - amp[2138] - amp[2139] - amp[2160] - amp[2161] -
      amp[2162] - amp[2163] - amp[2164] - amp[2165] - amp[2166] - amp[2167] -
      amp[2168] - amp[2169] - amp[2170] - amp[2171] - amp[2172] - amp[2173] -
      amp[2174] - amp[2175] - amp[2176] - amp[2177] - amp[2178] - amp[2179] -
      amp[2181] + Complex<double> (0, 1) * amp[2182] - Complex<double> (0, 1) *
      amp[2185] - amp[2187] - Complex<double> (0, 1) * amp[2188] +
      Complex<double> (0, 1) * amp[2190] - amp[2193] + Complex<double> (0, 1) *
      amp[2194] - Complex<double> (0, 1) * amp[2197] - amp[2199] -
      Complex<double> (0, 1) * amp[2200] + Complex<double> (0, 1) * amp[2202] -
      Complex<double> (0, 1) * amp[2204] - Complex<double> (0, 1) * amp[2207] -
      amp[2209] - Complex<double> (0, 1) * amp[2210] - amp[2211] - amp[2212] -
      Complex<double> (0, 1) * amp[2213] - amp[2214] - Complex<double> (0, 1) *
      amp[2216] - Complex<double> (0, 1) * amp[2219] - amp[2220] - amp[2221] -
      amp[2222] - Complex<double> (0, 1) * amp[2224] - amp[2225] - amp[2226] -
      Complex<double> (0, 1) * amp[2227] - amp[2228] + Complex<double> (0, 1) *
      amp[2229] - amp[2230] - amp[2232] - Complex<double> (0, 1) * amp[2233] -
      amp[2234] - amp[2235] - Complex<double> (0, 1) * amp[2236] - amp[2237] +
      Complex<double> (0, 1) * amp[2238] - amp[2239]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[9][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P10_sm_qq_zzgqq::matrix_12_ucx_zzgucx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 140;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[2260] + amp[2261] + amp[2262] + amp[2263] +
      amp[2264] + amp[2265] + amp[2266] + amp[2267] + amp[2268] + amp[2269] +
      amp[2270] + amp[2271] + amp[2272] + amp[2273] + amp[2274] + amp[2275] +
      amp[2276] + amp[2277] + amp[2278] + amp[2279] + amp[2300] + amp[2301] +
      amp[2302] + amp[2303] + amp[2304] + amp[2305] + amp[2306] + amp[2307] +
      amp[2308] + amp[2309] + amp[2310] + amp[2311] + amp[2312] + amp[2313] +
      amp[2314] + amp[2315] + amp[2316] + amp[2317] + amp[2318] + amp[2319] +
      amp[2321] - Complex<double> (0, 1) * amp[2322] + Complex<double> (0, 1) *
      amp[2325] + amp[2327] + Complex<double> (0, 1) * amp[2328] -
      Complex<double> (0, 1) * amp[2330] + amp[2333] - Complex<double> (0, 1) *
      amp[2334] + Complex<double> (0, 1) * amp[2337] + amp[2339] +
      Complex<double> (0, 1) * amp[2340] - Complex<double> (0, 1) * amp[2342] +
      Complex<double> (0, 1) * amp[2344] + Complex<double> (0, 1) * amp[2347] +
      amp[2349] + Complex<double> (0, 1) * amp[2350] + amp[2351] + amp[2352] +
      Complex<double> (0, 1) * amp[2353] + amp[2354] + Complex<double> (0, 1) *
      amp[2356] + Complex<double> (0, 1) * amp[2359] + amp[2360] + amp[2361] +
      amp[2362] + Complex<double> (0, 1) * amp[2364] + amp[2365] + amp[2366] +
      Complex<double> (0, 1) * amp[2367] + amp[2368] - Complex<double> (0, 1) *
      amp[2369] + amp[2370] + amp[2372] + Complex<double> (0, 1) * amp[2373] +
      amp[2374] + amp[2375] + Complex<double> (0, 1) * amp[2376] + amp[2377] -
      Complex<double> (0, 1) * amp[2378] + amp[2379]);
  jamp[1] = +1./2. * (-1./3. * amp[2240] - 1./3. * amp[2241] - 1./3. *
      amp[2242] - 1./3. * amp[2243] - 1./3. * amp[2244] - 1./3. * amp[2245] -
      1./3. * amp[2246] - 1./3. * amp[2247] - 1./3. * amp[2248] - 1./3. *
      amp[2249] - 1./3. * amp[2250] - 1./3. * amp[2251] - 1./3. * amp[2252] -
      1./3. * amp[2253] - 1./3. * amp[2254] - 1./3. * amp[2255] - 1./3. *
      amp[2256] - 1./3. * amp[2257] - 1./3. * amp[2258] - 1./3. * amp[2259] -
      1./3. * amp[2260] - 1./3. * amp[2261] - 1./3. * amp[2262] - 1./3. *
      amp[2263] - 1./3. * amp[2264] - 1./3. * amp[2265] - 1./3. * amp[2266] -
      1./3. * amp[2267] - 1./3. * amp[2268] - 1./3. * amp[2269] - 1./3. *
      amp[2270] - 1./3. * amp[2271] - 1./3. * amp[2272] - 1./3. * amp[2273] -
      1./3. * amp[2274] - 1./3. * amp[2275] - 1./3. * amp[2276] - 1./3. *
      amp[2277] - 1./3. * amp[2278] - 1./3. * amp[2279] - 1./3. * amp[2320] -
      1./3. * amp[2321] - 1./3. * amp[2323] - 1./3. * amp[2326] - 1./3. *
      amp[2329] - 1./3. * amp[2331] - 1./3. * amp[2332] - 1./3. * amp[2333] -
      1./3. * amp[2335] - 1./3. * amp[2338] - 1./3. * amp[2341] - 1./3. *
      amp[2343] - 1./3. * amp[2362] - 1./3. * amp[2365] - 1./3. * amp[2368] -
      1./3. * amp[2370] - 1./3. * amp[2372] - 1./3. * amp[2375] - 1./3. *
      amp[2377] - 1./3. * amp[2379]);
  jamp[2] = +1./2. * (+amp[2240] + amp[2241] + amp[2242] + amp[2243] +
      amp[2244] + amp[2245] + amp[2246] + amp[2247] + amp[2248] + amp[2249] +
      amp[2250] + amp[2251] + amp[2252] + amp[2253] + amp[2254] + amp[2255] +
      amp[2256] + amp[2257] + amp[2258] + amp[2259] + amp[2280] + amp[2281] +
      amp[2282] + amp[2283] + amp[2284] + amp[2285] + amp[2286] + amp[2287] +
      amp[2288] + amp[2289] + amp[2290] + amp[2291] + amp[2292] + amp[2293] +
      amp[2294] + amp[2295] + amp[2296] + amp[2297] + amp[2298] + amp[2299] +
      amp[2320] + Complex<double> (0, 1) * amp[2322] + amp[2323] + amp[2324] -
      Complex<double> (0, 1) * amp[2325] + amp[2326] - Complex<double> (0, 1) *
      amp[2328] + amp[2329] + Complex<double> (0, 1) * amp[2330] + amp[2331] +
      amp[2332] + Complex<double> (0, 1) * amp[2334] + amp[2335] + amp[2336] -
      Complex<double> (0, 1) * amp[2337] + amp[2338] - Complex<double> (0, 1) *
      amp[2340] + amp[2341] + Complex<double> (0, 1) * amp[2342] + amp[2343] -
      Complex<double> (0, 1) * amp[2344] + amp[2345] + amp[2346] -
      Complex<double> (0, 1) * amp[2347] + amp[2348] - Complex<double> (0, 1) *
      amp[2350] - Complex<double> (0, 1) * amp[2353] + amp[2355] -
      Complex<double> (0, 1) * amp[2356] + amp[2357] + amp[2358] -
      Complex<double> (0, 1) * amp[2359] + amp[2363] - Complex<double> (0, 1) *
      amp[2364] - Complex<double> (0, 1) * amp[2367] + Complex<double> (0, 1) *
      amp[2369] + amp[2371] - Complex<double> (0, 1) * amp[2373] -
      Complex<double> (0, 1) * amp[2376] + Complex<double> (0, 1) * amp[2378]);
  jamp[3] = +1./2. * (-1./3. * amp[2280] - 1./3. * amp[2281] - 1./3. *
      amp[2282] - 1./3. * amp[2283] - 1./3. * amp[2284] - 1./3. * amp[2285] -
      1./3. * amp[2286] - 1./3. * amp[2287] - 1./3. * amp[2288] - 1./3. *
      amp[2289] - 1./3. * amp[2290] - 1./3. * amp[2291] - 1./3. * amp[2292] -
      1./3. * amp[2293] - 1./3. * amp[2294] - 1./3. * amp[2295] - 1./3. *
      amp[2296] - 1./3. * amp[2297] - 1./3. * amp[2298] - 1./3. * amp[2299] -
      1./3. * amp[2300] - 1./3. * amp[2301] - 1./3. * amp[2302] - 1./3. *
      amp[2303] - 1./3. * amp[2304] - 1./3. * amp[2305] - 1./3. * amp[2306] -
      1./3. * amp[2307] - 1./3. * amp[2308] - 1./3. * amp[2309] - 1./3. *
      amp[2310] - 1./3. * amp[2311] - 1./3. * amp[2312] - 1./3. * amp[2313] -
      1./3. * amp[2314] - 1./3. * amp[2315] - 1./3. * amp[2316] - 1./3. *
      amp[2317] - 1./3. * amp[2318] - 1./3. * amp[2319] - 1./3. * amp[2324] -
      1./3. * amp[2327] - 1./3. * amp[2336] - 1./3. * amp[2339] - 1./3. *
      amp[2345] - 1./3. * amp[2346] - 1./3. * amp[2348] - 1./3. * amp[2349] -
      1./3. * amp[2351] - 1./3. * amp[2352] - 1./3. * amp[2354] - 1./3. *
      amp[2355] - 1./3. * amp[2357] - 1./3. * amp[2358] - 1./3. * amp[2360] -
      1./3. * amp[2361] - 1./3. * amp[2363] - 1./3. * amp[2366] - 1./3. *
      amp[2371] - 1./3. * amp[2374]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[10][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P10_sm_qq_zzgqq::matrix_12_udx_zzgudx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 140;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[2400] + amp[2401] + amp[2402] + amp[2403] +
      amp[2404] + amp[2405] + amp[2406] + amp[2407] + amp[2408] + amp[2409] +
      amp[2410] + amp[2411] + amp[2412] + amp[2413] + amp[2414] + amp[2415] +
      amp[2416] + amp[2417] + amp[2418] + amp[2419] + amp[2440] + amp[2441] +
      amp[2442] + amp[2443] + amp[2444] + amp[2445] + amp[2446] + amp[2447] +
      amp[2448] + amp[2449] + amp[2450] + amp[2451] + amp[2452] + amp[2453] +
      amp[2454] + amp[2455] + amp[2456] + amp[2457] + amp[2458] + amp[2459] +
      amp[2461] - Complex<double> (0, 1) * amp[2462] + Complex<double> (0, 1) *
      amp[2465] + amp[2467] + Complex<double> (0, 1) * amp[2468] -
      Complex<double> (0, 1) * amp[2470] + amp[2473] - Complex<double> (0, 1) *
      amp[2474] + Complex<double> (0, 1) * amp[2477] + amp[2479] +
      Complex<double> (0, 1) * amp[2480] - Complex<double> (0, 1) * amp[2482] +
      Complex<double> (0, 1) * amp[2484] + Complex<double> (0, 1) * amp[2487] +
      amp[2489] + Complex<double> (0, 1) * amp[2490] + amp[2491] + amp[2492] +
      Complex<double> (0, 1) * amp[2493] + amp[2494] + Complex<double> (0, 1) *
      amp[2496] + Complex<double> (0, 1) * amp[2499] + amp[2500] + amp[2501] +
      amp[2502] + Complex<double> (0, 1) * amp[2504] + amp[2505] + amp[2506] +
      Complex<double> (0, 1) * amp[2507] + amp[2508] - Complex<double> (0, 1) *
      amp[2509] + amp[2510] + amp[2512] + Complex<double> (0, 1) * amp[2513] +
      amp[2514] + amp[2515] + Complex<double> (0, 1) * amp[2516] + amp[2517] -
      Complex<double> (0, 1) * amp[2518] + amp[2519]);
  jamp[1] = +1./2. * (-1./3. * amp[2380] - 1./3. * amp[2381] - 1./3. *
      amp[2382] - 1./3. * amp[2383] - 1./3. * amp[2384] - 1./3. * amp[2385] -
      1./3. * amp[2386] - 1./3. * amp[2387] - 1./3. * amp[2388] - 1./3. *
      amp[2389] - 1./3. * amp[2390] - 1./3. * amp[2391] - 1./3. * amp[2392] -
      1./3. * amp[2393] - 1./3. * amp[2394] - 1./3. * amp[2395] - 1./3. *
      amp[2396] - 1./3. * amp[2397] - 1./3. * amp[2398] - 1./3. * amp[2399] -
      1./3. * amp[2400] - 1./3. * amp[2401] - 1./3. * amp[2402] - 1./3. *
      amp[2403] - 1./3. * amp[2404] - 1./3. * amp[2405] - 1./3. * amp[2406] -
      1./3. * amp[2407] - 1./3. * amp[2408] - 1./3. * amp[2409] - 1./3. *
      amp[2410] - 1./3. * amp[2411] - 1./3. * amp[2412] - 1./3. * amp[2413] -
      1./3. * amp[2414] - 1./3. * amp[2415] - 1./3. * amp[2416] - 1./3. *
      amp[2417] - 1./3. * amp[2418] - 1./3. * amp[2419] - 1./3. * amp[2460] -
      1./3. * amp[2461] - 1./3. * amp[2463] - 1./3. * amp[2466] - 1./3. *
      amp[2469] - 1./3. * amp[2471] - 1./3. * amp[2472] - 1./3. * amp[2473] -
      1./3. * amp[2475] - 1./3. * amp[2478] - 1./3. * amp[2481] - 1./3. *
      amp[2483] - 1./3. * amp[2502] - 1./3. * amp[2505] - 1./3. * amp[2508] -
      1./3. * amp[2510] - 1./3. * amp[2512] - 1./3. * amp[2515] - 1./3. *
      amp[2517] - 1./3. * amp[2519]);
  jamp[2] = +1./2. * (+amp[2380] + amp[2381] + amp[2382] + amp[2383] +
      amp[2384] + amp[2385] + amp[2386] + amp[2387] + amp[2388] + amp[2389] +
      amp[2390] + amp[2391] + amp[2392] + amp[2393] + amp[2394] + amp[2395] +
      amp[2396] + amp[2397] + amp[2398] + amp[2399] + amp[2420] + amp[2421] +
      amp[2422] + amp[2423] + amp[2424] + amp[2425] + amp[2426] + amp[2427] +
      amp[2428] + amp[2429] + amp[2430] + amp[2431] + amp[2432] + amp[2433] +
      amp[2434] + amp[2435] + amp[2436] + amp[2437] + amp[2438] + amp[2439] +
      amp[2460] + Complex<double> (0, 1) * amp[2462] + amp[2463] + amp[2464] -
      Complex<double> (0, 1) * amp[2465] + amp[2466] - Complex<double> (0, 1) *
      amp[2468] + amp[2469] + Complex<double> (0, 1) * amp[2470] + amp[2471] +
      amp[2472] + Complex<double> (0, 1) * amp[2474] + amp[2475] + amp[2476] -
      Complex<double> (0, 1) * amp[2477] + amp[2478] - Complex<double> (0, 1) *
      amp[2480] + amp[2481] + Complex<double> (0, 1) * amp[2482] + amp[2483] -
      Complex<double> (0, 1) * amp[2484] + amp[2485] + amp[2486] -
      Complex<double> (0, 1) * amp[2487] + amp[2488] - Complex<double> (0, 1) *
      amp[2490] - Complex<double> (0, 1) * amp[2493] + amp[2495] -
      Complex<double> (0, 1) * amp[2496] + amp[2497] + amp[2498] -
      Complex<double> (0, 1) * amp[2499] + amp[2503] - Complex<double> (0, 1) *
      amp[2504] - Complex<double> (0, 1) * amp[2507] + Complex<double> (0, 1) *
      amp[2509] + amp[2511] - Complex<double> (0, 1) * amp[2513] -
      Complex<double> (0, 1) * amp[2516] + Complex<double> (0, 1) * amp[2518]);
  jamp[3] = +1./2. * (-1./3. * amp[2420] - 1./3. * amp[2421] - 1./3. *
      amp[2422] - 1./3. * amp[2423] - 1./3. * amp[2424] - 1./3. * amp[2425] -
      1./3. * amp[2426] - 1./3. * amp[2427] - 1./3. * amp[2428] - 1./3. *
      amp[2429] - 1./3. * amp[2430] - 1./3. * amp[2431] - 1./3. * amp[2432] -
      1./3. * amp[2433] - 1./3. * amp[2434] - 1./3. * amp[2435] - 1./3. *
      amp[2436] - 1./3. * amp[2437] - 1./3. * amp[2438] - 1./3. * amp[2439] -
      1./3. * amp[2440] - 1./3. * amp[2441] - 1./3. * amp[2442] - 1./3. *
      amp[2443] - 1./3. * amp[2444] - 1./3. * amp[2445] - 1./3. * amp[2446] -
      1./3. * amp[2447] - 1./3. * amp[2448] - 1./3. * amp[2449] - 1./3. *
      amp[2450] - 1./3. * amp[2451] - 1./3. * amp[2452] - 1./3. * amp[2453] -
      1./3. * amp[2454] - 1./3. * amp[2455] - 1./3. * amp[2456] - 1./3. *
      amp[2457] - 1./3. * amp[2458] - 1./3. * amp[2459] - 1./3. * amp[2464] -
      1./3. * amp[2467] - 1./3. * amp[2476] - 1./3. * amp[2479] - 1./3. *
      amp[2485] - 1./3. * amp[2486] - 1./3. * amp[2488] - 1./3. * amp[2489] -
      1./3. * amp[2491] - 1./3. * amp[2492] - 1./3. * amp[2494] - 1./3. *
      amp[2495] - 1./3. * amp[2497] - 1./3. * amp[2498] - 1./3. * amp[2500] -
      1./3. * amp[2501] - 1./3. * amp[2503] - 1./3. * amp[2506] - 1./3. *
      amp[2511] - 1./3. * amp[2514]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[11][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P10_sm_qq_zzgqq::matrix_12_ds_zzgds() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 140;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[2520] + 1./3. * amp[2521] + 1./3. *
      amp[2522] + 1./3. * amp[2523] + 1./3. * amp[2524] + 1./3. * amp[2525] +
      1./3. * amp[2526] + 1./3. * amp[2527] + 1./3. * amp[2528] + 1./3. *
      amp[2529] + 1./3. * amp[2530] + 1./3. * amp[2531] + 1./3. * amp[2532] +
      1./3. * amp[2533] + 1./3. * amp[2534] + 1./3. * amp[2535] + 1./3. *
      amp[2536] + 1./3. * amp[2537] + 1./3. * amp[2538] + 1./3. * amp[2539] +
      1./3. * amp[2540] + 1./3. * amp[2541] + 1./3. * amp[2542] + 1./3. *
      amp[2543] + 1./3. * amp[2544] + 1./3. * amp[2545] + 1./3. * amp[2546] +
      1./3. * amp[2547] + 1./3. * amp[2548] + 1./3. * amp[2549] + 1./3. *
      amp[2550] + 1./3. * amp[2551] + 1./3. * amp[2552] + 1./3. * amp[2553] +
      1./3. * amp[2554] + 1./3. * amp[2555] + 1./3. * amp[2556] + 1./3. *
      amp[2557] + 1./3. * amp[2558] + 1./3. * amp[2559] + 1./3. * amp[2600] +
      1./3. * amp[2601] + 1./3. * amp[2603] + 1./3. * amp[2606] + 1./3. *
      amp[2609] + 1./3. * amp[2611] + 1./3. * amp[2612] + 1./3. * amp[2613] +
      1./3. * amp[2615] + 1./3. * amp[2618] + 1./3. * amp[2621] + 1./3. *
      amp[2623] + 1./3. * amp[2642] + 1./3. * amp[2645] + 1./3. * amp[2648] +
      1./3. * amp[2650] + 1./3. * amp[2652] + 1./3. * amp[2655] + 1./3. *
      amp[2657] + 1./3. * amp[2659]);
  jamp[1] = +1./2. * (-amp[2540] - amp[2541] - amp[2542] - amp[2543] -
      amp[2544] - amp[2545] - amp[2546] - amp[2547] - amp[2548] - amp[2549] -
      amp[2550] - amp[2551] - amp[2552] - amp[2553] - amp[2554] - amp[2555] -
      amp[2556] - amp[2557] - amp[2558] - amp[2559] - amp[2580] - amp[2581] -
      amp[2582] - amp[2583] - amp[2584] - amp[2585] - amp[2586] - amp[2587] -
      amp[2588] - amp[2589] - amp[2590] - amp[2591] - amp[2592] - amp[2593] -
      amp[2594] - amp[2595] - amp[2596] - amp[2597] - amp[2598] - amp[2599] -
      amp[2601] + Complex<double> (0, 1) * amp[2602] - Complex<double> (0, 1) *
      amp[2605] - amp[2607] - Complex<double> (0, 1) * amp[2608] +
      Complex<double> (0, 1) * amp[2610] - amp[2613] + Complex<double> (0, 1) *
      amp[2614] - Complex<double> (0, 1) * amp[2617] - amp[2619] -
      Complex<double> (0, 1) * amp[2620] + Complex<double> (0, 1) * amp[2622] -
      Complex<double> (0, 1) * amp[2624] - Complex<double> (0, 1) * amp[2627] -
      amp[2629] - Complex<double> (0, 1) * amp[2630] - amp[2631] - amp[2632] -
      Complex<double> (0, 1) * amp[2633] - amp[2634] - Complex<double> (0, 1) *
      amp[2636] - Complex<double> (0, 1) * amp[2639] - amp[2640] - amp[2641] -
      amp[2642] - Complex<double> (0, 1) * amp[2644] - amp[2645] - amp[2646] -
      Complex<double> (0, 1) * amp[2647] - amp[2648] + Complex<double> (0, 1) *
      amp[2649] - amp[2650] - amp[2652] - Complex<double> (0, 1) * amp[2653] -
      amp[2654] - amp[2655] - Complex<double> (0, 1) * amp[2656] - amp[2657] +
      Complex<double> (0, 1) * amp[2658] - amp[2659]);
  jamp[2] = +1./2. * (-amp[2520] - amp[2521] - amp[2522] - amp[2523] -
      amp[2524] - amp[2525] - amp[2526] - amp[2527] - amp[2528] - amp[2529] -
      amp[2530] - amp[2531] - amp[2532] - amp[2533] - amp[2534] - amp[2535] -
      amp[2536] - amp[2537] - amp[2538] - amp[2539] - amp[2560] - amp[2561] -
      amp[2562] - amp[2563] - amp[2564] - amp[2565] - amp[2566] - amp[2567] -
      amp[2568] - amp[2569] - amp[2570] - amp[2571] - amp[2572] - amp[2573] -
      amp[2574] - amp[2575] - amp[2576] - amp[2577] - amp[2578] - amp[2579] -
      amp[2600] - Complex<double> (0, 1) * amp[2602] - amp[2603] - amp[2604] +
      Complex<double> (0, 1) * amp[2605] - amp[2606] + Complex<double> (0, 1) *
      amp[2608] - amp[2609] - Complex<double> (0, 1) * amp[2610] - amp[2611] -
      amp[2612] - Complex<double> (0, 1) * amp[2614] - amp[2615] - amp[2616] +
      Complex<double> (0, 1) * amp[2617] - amp[2618] + Complex<double> (0, 1) *
      amp[2620] - amp[2621] - Complex<double> (0, 1) * amp[2622] - amp[2623] +
      Complex<double> (0, 1) * amp[2624] - amp[2625] - amp[2626] +
      Complex<double> (0, 1) * amp[2627] - amp[2628] + Complex<double> (0, 1) *
      amp[2630] + Complex<double> (0, 1) * amp[2633] - amp[2635] +
      Complex<double> (0, 1) * amp[2636] - amp[2637] - amp[2638] +
      Complex<double> (0, 1) * amp[2639] - amp[2643] + Complex<double> (0, 1) *
      amp[2644] + Complex<double> (0, 1) * amp[2647] - Complex<double> (0, 1) *
      amp[2649] - amp[2651] + Complex<double> (0, 1) * amp[2653] +
      Complex<double> (0, 1) * amp[2656] - Complex<double> (0, 1) * amp[2658]);
  jamp[3] = +1./2. * (+1./3. * amp[2560] + 1./3. * amp[2561] + 1./3. *
      amp[2562] + 1./3. * amp[2563] + 1./3. * amp[2564] + 1./3. * amp[2565] +
      1./3. * amp[2566] + 1./3. * amp[2567] + 1./3. * amp[2568] + 1./3. *
      amp[2569] + 1./3. * amp[2570] + 1./3. * amp[2571] + 1./3. * amp[2572] +
      1./3. * amp[2573] + 1./3. * amp[2574] + 1./3. * amp[2575] + 1./3. *
      amp[2576] + 1./3. * amp[2577] + 1./3. * amp[2578] + 1./3. * amp[2579] +
      1./3. * amp[2580] + 1./3. * amp[2581] + 1./3. * amp[2582] + 1./3. *
      amp[2583] + 1./3. * amp[2584] + 1./3. * amp[2585] + 1./3. * amp[2586] +
      1./3. * amp[2587] + 1./3. * amp[2588] + 1./3. * amp[2589] + 1./3. *
      amp[2590] + 1./3. * amp[2591] + 1./3. * amp[2592] + 1./3. * amp[2593] +
      1./3. * amp[2594] + 1./3. * amp[2595] + 1./3. * amp[2596] + 1./3. *
      amp[2597] + 1./3. * amp[2598] + 1./3. * amp[2599] + 1./3. * amp[2604] +
      1./3. * amp[2607] + 1./3. * amp[2616] + 1./3. * amp[2619] + 1./3. *
      amp[2625] + 1./3. * amp[2626] + 1./3. * amp[2628] + 1./3. * amp[2629] +
      1./3. * amp[2631] + 1./3. * amp[2632] + 1./3. * amp[2634] + 1./3. *
      amp[2635] + 1./3. * amp[2637] + 1./3. * amp[2638] + 1./3. * amp[2640] +
      1./3. * amp[2641] + 1./3. * amp[2643] + 1./3. * amp[2646] + 1./3. *
      amp[2651] + 1./3. * amp[2654]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[12][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P10_sm_qq_zzgqq::matrix_12_dux_zzgdux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 140;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[2660] + amp[2661] + amp[2662] + amp[2663] +
      amp[2664] + amp[2665] + amp[2666] + amp[2667] + amp[2668] + amp[2669] +
      amp[2670] + amp[2671] + amp[2672] + amp[2673] + amp[2674] + amp[2675] +
      amp[2676] + amp[2677] + amp[2678] + amp[2679] + amp[2700] + amp[2701] +
      amp[2702] + amp[2703] + amp[2704] + amp[2705] + amp[2706] + amp[2707] +
      amp[2708] + amp[2709] + amp[2710] + amp[2711] + amp[2712] + amp[2713] +
      amp[2714] + amp[2715] + amp[2716] + amp[2717] + amp[2718] + amp[2719] +
      amp[2740] + Complex<double> (0, 1) * amp[2742] + amp[2743] + amp[2744] -
      Complex<double> (0, 1) * amp[2745] + amp[2746] - Complex<double> (0, 1) *
      amp[2748] + amp[2749] + Complex<double> (0, 1) * amp[2750] + amp[2751] +
      amp[2752] + Complex<double> (0, 1) * amp[2754] + amp[2755] + amp[2756] -
      Complex<double> (0, 1) * amp[2757] + amp[2758] - Complex<double> (0, 1) *
      amp[2760] + amp[2761] + Complex<double> (0, 1) * amp[2762] + amp[2763] -
      Complex<double> (0, 1) * amp[2764] + amp[2765] + amp[2766] -
      Complex<double> (0, 1) * amp[2767] + amp[2768] - Complex<double> (0, 1) *
      amp[2770] - Complex<double> (0, 1) * amp[2773] + amp[2775] -
      Complex<double> (0, 1) * amp[2776] + amp[2777] + amp[2778] -
      Complex<double> (0, 1) * amp[2779] + amp[2783] - Complex<double> (0, 1) *
      amp[2784] - Complex<double> (0, 1) * amp[2787] + Complex<double> (0, 1) *
      amp[2789] + amp[2791] - Complex<double> (0, 1) * amp[2793] -
      Complex<double> (0, 1) * amp[2796] + Complex<double> (0, 1) * amp[2798]);
  jamp[1] = +1./2. * (-1./3. * amp[2700] - 1./3. * amp[2701] - 1./3. *
      amp[2702] - 1./3. * amp[2703] - 1./3. * amp[2704] - 1./3. * amp[2705] -
      1./3. * amp[2706] - 1./3. * amp[2707] - 1./3. * amp[2708] - 1./3. *
      amp[2709] - 1./3. * amp[2710] - 1./3. * amp[2711] - 1./3. * amp[2712] -
      1./3. * amp[2713] - 1./3. * amp[2714] - 1./3. * amp[2715] - 1./3. *
      amp[2716] - 1./3. * amp[2717] - 1./3. * amp[2718] - 1./3. * amp[2719] -
      1./3. * amp[2720] - 1./3. * amp[2721] - 1./3. * amp[2722] - 1./3. *
      amp[2723] - 1./3. * amp[2724] - 1./3. * amp[2725] - 1./3. * amp[2726] -
      1./3. * amp[2727] - 1./3. * amp[2728] - 1./3. * amp[2729] - 1./3. *
      amp[2730] - 1./3. * amp[2731] - 1./3. * amp[2732] - 1./3. * amp[2733] -
      1./3. * amp[2734] - 1./3. * amp[2735] - 1./3. * amp[2736] - 1./3. *
      amp[2737] - 1./3. * amp[2738] - 1./3. * amp[2739] - 1./3. * amp[2744] -
      1./3. * amp[2747] - 1./3. * amp[2756] - 1./3. * amp[2759] - 1./3. *
      amp[2765] - 1./3. * amp[2766] - 1./3. * amp[2768] - 1./3. * amp[2769] -
      1./3. * amp[2771] - 1./3. * amp[2772] - 1./3. * amp[2774] - 1./3. *
      amp[2775] - 1./3. * amp[2777] - 1./3. * amp[2778] - 1./3. * amp[2780] -
      1./3. * amp[2781] - 1./3. * amp[2783] - 1./3. * amp[2786] - 1./3. *
      amp[2791] - 1./3. * amp[2794]);
  jamp[2] = +1./2. * (+amp[2680] + amp[2681] + amp[2682] + amp[2683] +
      amp[2684] + amp[2685] + amp[2686] + amp[2687] + amp[2688] + amp[2689] +
      amp[2690] + amp[2691] + amp[2692] + amp[2693] + amp[2694] + amp[2695] +
      amp[2696] + amp[2697] + amp[2698] + amp[2699] + amp[2720] + amp[2721] +
      amp[2722] + amp[2723] + amp[2724] + amp[2725] + amp[2726] + amp[2727] +
      amp[2728] + amp[2729] + amp[2730] + amp[2731] + amp[2732] + amp[2733] +
      amp[2734] + amp[2735] + amp[2736] + amp[2737] + amp[2738] + amp[2739] +
      amp[2741] - Complex<double> (0, 1) * amp[2742] + Complex<double> (0, 1) *
      amp[2745] + amp[2747] + Complex<double> (0, 1) * amp[2748] -
      Complex<double> (0, 1) * amp[2750] + amp[2753] - Complex<double> (0, 1) *
      amp[2754] + Complex<double> (0, 1) * amp[2757] + amp[2759] +
      Complex<double> (0, 1) * amp[2760] - Complex<double> (0, 1) * amp[2762] +
      Complex<double> (0, 1) * amp[2764] + Complex<double> (0, 1) * amp[2767] +
      amp[2769] + Complex<double> (0, 1) * amp[2770] + amp[2771] + amp[2772] +
      Complex<double> (0, 1) * amp[2773] + amp[2774] + Complex<double> (0, 1) *
      amp[2776] + Complex<double> (0, 1) * amp[2779] + amp[2780] + amp[2781] +
      amp[2782] + Complex<double> (0, 1) * amp[2784] + amp[2785] + amp[2786] +
      Complex<double> (0, 1) * amp[2787] + amp[2788] - Complex<double> (0, 1) *
      amp[2789] + amp[2790] + amp[2792] + Complex<double> (0, 1) * amp[2793] +
      amp[2794] + amp[2795] + Complex<double> (0, 1) * amp[2796] + amp[2797] -
      Complex<double> (0, 1) * amp[2798] + amp[2799]);
  jamp[3] = +1./2. * (-1./3. * amp[2660] - 1./3. * amp[2661] - 1./3. *
      amp[2662] - 1./3. * amp[2663] - 1./3. * amp[2664] - 1./3. * amp[2665] -
      1./3. * amp[2666] - 1./3. * amp[2667] - 1./3. * amp[2668] - 1./3. *
      amp[2669] - 1./3. * amp[2670] - 1./3. * amp[2671] - 1./3. * amp[2672] -
      1./3. * amp[2673] - 1./3. * amp[2674] - 1./3. * amp[2675] - 1./3. *
      amp[2676] - 1./3. * amp[2677] - 1./3. * amp[2678] - 1./3. * amp[2679] -
      1./3. * amp[2680] - 1./3. * amp[2681] - 1./3. * amp[2682] - 1./3. *
      amp[2683] - 1./3. * amp[2684] - 1./3. * amp[2685] - 1./3. * amp[2686] -
      1./3. * amp[2687] - 1./3. * amp[2688] - 1./3. * amp[2689] - 1./3. *
      amp[2690] - 1./3. * amp[2691] - 1./3. * amp[2692] - 1./3. * amp[2693] -
      1./3. * amp[2694] - 1./3. * amp[2695] - 1./3. * amp[2696] - 1./3. *
      amp[2697] - 1./3. * amp[2698] - 1./3. * amp[2699] - 1./3. * amp[2740] -
      1./3. * amp[2741] - 1./3. * amp[2743] - 1./3. * amp[2746] - 1./3. *
      amp[2749] - 1./3. * amp[2751] - 1./3. * amp[2752] - 1./3. * amp[2753] -
      1./3. * amp[2755] - 1./3. * amp[2758] - 1./3. * amp[2761] - 1./3. *
      amp[2763] - 1./3. * amp[2782] - 1./3. * amp[2785] - 1./3. * amp[2788] -
      1./3. * amp[2790] - 1./3. * amp[2792] - 1./3. * amp[2795] - 1./3. *
      amp[2797] - 1./3. * amp[2799]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[13][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P10_sm_qq_zzgqq::matrix_12_ddx_zzguux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 140;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[2800] + 1./3. * amp[2801] + 1./3. *
      amp[2802] + 1./3. * amp[2803] + 1./3. * amp[2804] + 1./3. * amp[2805] +
      1./3. * amp[2806] + 1./3. * amp[2807] + 1./3. * amp[2808] + 1./3. *
      amp[2809] + 1./3. * amp[2810] + 1./3. * amp[2811] + 1./3. * amp[2812] +
      1./3. * amp[2813] + 1./3. * amp[2814] + 1./3. * amp[2815] + 1./3. *
      amp[2816] + 1./3. * amp[2817] + 1./3. * amp[2818] + 1./3. * amp[2819] +
      1./3. * amp[2820] + 1./3. * amp[2821] + 1./3. * amp[2822] + 1./3. *
      amp[2823] + 1./3. * amp[2824] + 1./3. * amp[2825] + 1./3. * amp[2826] +
      1./3. * amp[2827] + 1./3. * amp[2828] + 1./3. * amp[2829] + 1./3. *
      amp[2830] + 1./3. * amp[2831] + 1./3. * amp[2832] + 1./3. * amp[2833] +
      1./3. * amp[2834] + 1./3. * amp[2835] + 1./3. * amp[2836] + 1./3. *
      amp[2837] + 1./3. * amp[2838] + 1./3. * amp[2839] + 1./3. * amp[2880] +
      1./3. * amp[2881] + 1./3. * amp[2883] + 1./3. * amp[2886] + 1./3. *
      amp[2889] + 1./3. * amp[2891] + 1./3. * amp[2892] + 1./3. * amp[2893] +
      1./3. * amp[2895] + 1./3. * amp[2898] + 1./3. * amp[2901] + 1./3. *
      amp[2903] + 1./3. * amp[2922] + 1./3. * amp[2925] + 1./3. * amp[2928] +
      1./3. * amp[2930] + 1./3. * amp[2932] + 1./3. * amp[2935] + 1./3. *
      amp[2937] + 1./3. * amp[2939]);
  jamp[1] = +1./2. * (-amp[2820] - amp[2821] - amp[2822] - amp[2823] -
      amp[2824] - amp[2825] - amp[2826] - amp[2827] - amp[2828] - amp[2829] -
      amp[2830] - amp[2831] - amp[2832] - amp[2833] - amp[2834] - amp[2835] -
      amp[2836] - amp[2837] - amp[2838] - amp[2839] - amp[2860] - amp[2861] -
      amp[2862] - amp[2863] - amp[2864] - amp[2865] - amp[2866] - amp[2867] -
      amp[2868] - amp[2869] - amp[2870] - amp[2871] - amp[2872] - amp[2873] -
      amp[2874] - amp[2875] - amp[2876] - amp[2877] - amp[2878] - amp[2879] -
      amp[2881] + Complex<double> (0, 1) * amp[2882] - Complex<double> (0, 1) *
      amp[2885] - amp[2887] - Complex<double> (0, 1) * amp[2888] +
      Complex<double> (0, 1) * amp[2890] - amp[2893] + Complex<double> (0, 1) *
      amp[2894] - Complex<double> (0, 1) * amp[2897] - amp[2899] -
      Complex<double> (0, 1) * amp[2900] + Complex<double> (0, 1) * amp[2902] -
      Complex<double> (0, 1) * amp[2904] - Complex<double> (0, 1) * amp[2907] -
      amp[2909] - Complex<double> (0, 1) * amp[2910] - amp[2911] - amp[2912] -
      Complex<double> (0, 1) * amp[2913] - amp[2914] - Complex<double> (0, 1) *
      amp[2916] - Complex<double> (0, 1) * amp[2919] - amp[2920] - amp[2921] -
      amp[2922] - Complex<double> (0, 1) * amp[2924] - amp[2925] - amp[2926] -
      Complex<double> (0, 1) * amp[2927] - amp[2928] + Complex<double> (0, 1) *
      amp[2929] - amp[2930] - amp[2932] - Complex<double> (0, 1) * amp[2933] -
      amp[2934] - amp[2935] - Complex<double> (0, 1) * amp[2936] - amp[2937] +
      Complex<double> (0, 1) * amp[2938] - amp[2939]);
  jamp[2] = +1./2. * (+1./3. * amp[2840] + 1./3. * amp[2841] + 1./3. *
      amp[2842] + 1./3. * amp[2843] + 1./3. * amp[2844] + 1./3. * amp[2845] +
      1./3. * amp[2846] + 1./3. * amp[2847] + 1./3. * amp[2848] + 1./3. *
      amp[2849] + 1./3. * amp[2850] + 1./3. * amp[2851] + 1./3. * amp[2852] +
      1./3. * amp[2853] + 1./3. * amp[2854] + 1./3. * amp[2855] + 1./3. *
      amp[2856] + 1./3. * amp[2857] + 1./3. * amp[2858] + 1./3. * amp[2859] +
      1./3. * amp[2860] + 1./3. * amp[2861] + 1./3. * amp[2862] + 1./3. *
      amp[2863] + 1./3. * amp[2864] + 1./3. * amp[2865] + 1./3. * amp[2866] +
      1./3. * amp[2867] + 1./3. * amp[2868] + 1./3. * amp[2869] + 1./3. *
      amp[2870] + 1./3. * amp[2871] + 1./3. * amp[2872] + 1./3. * amp[2873] +
      1./3. * amp[2874] + 1./3. * amp[2875] + 1./3. * amp[2876] + 1./3. *
      amp[2877] + 1./3. * amp[2878] + 1./3. * amp[2879] + 1./3. * amp[2884] +
      1./3. * amp[2887] + 1./3. * amp[2896] + 1./3. * amp[2899] + 1./3. *
      amp[2905] + 1./3. * amp[2906] + 1./3. * amp[2908] + 1./3. * amp[2909] +
      1./3. * amp[2911] + 1./3. * amp[2912] + 1./3. * amp[2914] + 1./3. *
      amp[2915] + 1./3. * amp[2917] + 1./3. * amp[2918] + 1./3. * amp[2920] +
      1./3. * amp[2921] + 1./3. * amp[2923] + 1./3. * amp[2926] + 1./3. *
      amp[2931] + 1./3. * amp[2934]);
  jamp[3] = +1./2. * (-amp[2800] - amp[2801] - amp[2802] - amp[2803] -
      amp[2804] - amp[2805] - amp[2806] - amp[2807] - amp[2808] - amp[2809] -
      amp[2810] - amp[2811] - amp[2812] - amp[2813] - amp[2814] - amp[2815] -
      amp[2816] - amp[2817] - amp[2818] - amp[2819] - amp[2840] - amp[2841] -
      amp[2842] - amp[2843] - amp[2844] - amp[2845] - amp[2846] - amp[2847] -
      amp[2848] - amp[2849] - amp[2850] - amp[2851] - amp[2852] - amp[2853] -
      amp[2854] - amp[2855] - amp[2856] - amp[2857] - amp[2858] - amp[2859] -
      amp[2880] - Complex<double> (0, 1) * amp[2882] - amp[2883] - amp[2884] +
      Complex<double> (0, 1) * amp[2885] - amp[2886] + Complex<double> (0, 1) *
      amp[2888] - amp[2889] - Complex<double> (0, 1) * amp[2890] - amp[2891] -
      amp[2892] - Complex<double> (0, 1) * amp[2894] - amp[2895] - amp[2896] +
      Complex<double> (0, 1) * amp[2897] - amp[2898] + Complex<double> (0, 1) *
      amp[2900] - amp[2901] - Complex<double> (0, 1) * amp[2902] - amp[2903] +
      Complex<double> (0, 1) * amp[2904] - amp[2905] - amp[2906] +
      Complex<double> (0, 1) * amp[2907] - amp[2908] + Complex<double> (0, 1) *
      amp[2910] + Complex<double> (0, 1) * amp[2913] - amp[2915] +
      Complex<double> (0, 1) * amp[2916] - amp[2917] - amp[2918] +
      Complex<double> (0, 1) * amp[2919] - amp[2923] + Complex<double> (0, 1) *
      amp[2924] + Complex<double> (0, 1) * amp[2927] - Complex<double> (0, 1) *
      amp[2929] - amp[2931] + Complex<double> (0, 1) * amp[2933] +
      Complex<double> (0, 1) * amp[2936] - Complex<double> (0, 1) * amp[2938]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[14][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P10_sm_qq_zzgqq::matrix_12_ddx_zzgssx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 140;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[2980] + 1./3. * amp[2981] + 1./3. *
      amp[2982] + 1./3. * amp[2983] + 1./3. * amp[2984] + 1./3. * amp[2985] +
      1./3. * amp[2986] + 1./3. * amp[2987] + 1./3. * amp[2988] + 1./3. *
      amp[2989] + 1./3. * amp[2990] + 1./3. * amp[2991] + 1./3. * amp[2992] +
      1./3. * amp[2993] + 1./3. * amp[2994] + 1./3. * amp[2995] + 1./3. *
      amp[2996] + 1./3. * amp[2997] + 1./3. * amp[2998] + 1./3. * amp[2999] +
      1./3. * amp[3000] + 1./3. * amp[3001] + 1./3. * amp[3002] + 1./3. *
      amp[3003] + 1./3. * amp[3004] + 1./3. * amp[3005] + 1./3. * amp[3006] +
      1./3. * amp[3007] + 1./3. * amp[3008] + 1./3. * amp[3009] + 1./3. *
      amp[3010] + 1./3. * amp[3011] + 1./3. * amp[3012] + 1./3. * amp[3013] +
      1./3. * amp[3014] + 1./3. * amp[3015] + 1./3. * amp[3016] + 1./3. *
      amp[3017] + 1./3. * amp[3018] + 1./3. * amp[3019] + 1./3. * amp[3024] +
      1./3. * amp[3027] + 1./3. * amp[3036] + 1./3. * amp[3039] + 1./3. *
      amp[3045] + 1./3. * amp[3046] + 1./3. * amp[3048] + 1./3. * amp[3049] +
      1./3. * amp[3051] + 1./3. * amp[3052] + 1./3. * amp[3054] + 1./3. *
      amp[3055] + 1./3. * amp[3057] + 1./3. * amp[3058] + 1./3. * amp[3060] +
      1./3. * amp[3061] + 1./3. * amp[3063] + 1./3. * amp[3066] + 1./3. *
      amp[3071] + 1./3. * amp[3074]);
  jamp[1] = +1./2. * (-amp[2940] - amp[2941] - amp[2942] - amp[2943] -
      amp[2944] - amp[2945] - amp[2946] - amp[2947] - amp[2948] - amp[2949] -
      amp[2950] - amp[2951] - amp[2952] - amp[2953] - amp[2954] - amp[2955] -
      amp[2956] - amp[2957] - amp[2958] - amp[2959] - amp[2980] - amp[2981] -
      amp[2982] - amp[2983] - amp[2984] - amp[2985] - amp[2986] - amp[2987] -
      amp[2988] - amp[2989] - amp[2990] - amp[2991] - amp[2992] - amp[2993] -
      amp[2994] - amp[2995] - amp[2996] - amp[2997] - amp[2998] - amp[2999] -
      amp[3020] - Complex<double> (0, 1) * amp[3022] - amp[3023] - amp[3024] +
      Complex<double> (0, 1) * amp[3025] - amp[3026] + Complex<double> (0, 1) *
      amp[3028] - amp[3029] - Complex<double> (0, 1) * amp[3030] - amp[3031] -
      amp[3032] - Complex<double> (0, 1) * amp[3034] - amp[3035] - amp[3036] +
      Complex<double> (0, 1) * amp[3037] - amp[3038] + Complex<double> (0, 1) *
      amp[3040] - amp[3041] - Complex<double> (0, 1) * amp[3042] - amp[3043] +
      Complex<double> (0, 1) * amp[3044] - amp[3045] - amp[3046] +
      Complex<double> (0, 1) * amp[3047] - amp[3048] + Complex<double> (0, 1) *
      amp[3050] + Complex<double> (0, 1) * amp[3053] - amp[3055] +
      Complex<double> (0, 1) * amp[3056] - amp[3057] - amp[3058] +
      Complex<double> (0, 1) * amp[3059] - amp[3063] + Complex<double> (0, 1) *
      amp[3064] + Complex<double> (0, 1) * amp[3067] - Complex<double> (0, 1) *
      amp[3069] - amp[3071] + Complex<double> (0, 1) * amp[3073] +
      Complex<double> (0, 1) * amp[3076] - Complex<double> (0, 1) * amp[3078]);
  jamp[2] = +1./2. * (+1./3. * amp[2940] + 1./3. * amp[2941] + 1./3. *
      amp[2942] + 1./3. * amp[2943] + 1./3. * amp[2944] + 1./3. * amp[2945] +
      1./3. * amp[2946] + 1./3. * amp[2947] + 1./3. * amp[2948] + 1./3. *
      amp[2949] + 1./3. * amp[2950] + 1./3. * amp[2951] + 1./3. * amp[2952] +
      1./3. * amp[2953] + 1./3. * amp[2954] + 1./3. * amp[2955] + 1./3. *
      amp[2956] + 1./3. * amp[2957] + 1./3. * amp[2958] + 1./3. * amp[2959] +
      1./3. * amp[2960] + 1./3. * amp[2961] + 1./3. * amp[2962] + 1./3. *
      amp[2963] + 1./3. * amp[2964] + 1./3. * amp[2965] + 1./3. * amp[2966] +
      1./3. * amp[2967] + 1./3. * amp[2968] + 1./3. * amp[2969] + 1./3. *
      amp[2970] + 1./3. * amp[2971] + 1./3. * amp[2972] + 1./3. * amp[2973] +
      1./3. * amp[2974] + 1./3. * amp[2975] + 1./3. * amp[2976] + 1./3. *
      amp[2977] + 1./3. * amp[2978] + 1./3. * amp[2979] + 1./3. * amp[3020] +
      1./3. * amp[3021] + 1./3. * amp[3023] + 1./3. * amp[3026] + 1./3. *
      amp[3029] + 1./3. * amp[3031] + 1./3. * amp[3032] + 1./3. * amp[3033] +
      1./3. * amp[3035] + 1./3. * amp[3038] + 1./3. * amp[3041] + 1./3. *
      amp[3043] + 1./3. * amp[3062] + 1./3. * amp[3065] + 1./3. * amp[3068] +
      1./3. * amp[3070] + 1./3. * amp[3072] + 1./3. * amp[3075] + 1./3. *
      amp[3077] + 1./3. * amp[3079]);
  jamp[3] = +1./2. * (-amp[2960] - amp[2961] - amp[2962] - amp[2963] -
      amp[2964] - amp[2965] - amp[2966] - amp[2967] - amp[2968] - amp[2969] -
      amp[2970] - amp[2971] - amp[2972] - amp[2973] - amp[2974] - amp[2975] -
      amp[2976] - amp[2977] - amp[2978] - amp[2979] - amp[3000] - amp[3001] -
      amp[3002] - amp[3003] - amp[3004] - amp[3005] - amp[3006] - amp[3007] -
      amp[3008] - amp[3009] - amp[3010] - amp[3011] - amp[3012] - amp[3013] -
      amp[3014] - amp[3015] - amp[3016] - amp[3017] - amp[3018] - amp[3019] -
      amp[3021] + Complex<double> (0, 1) * amp[3022] - Complex<double> (0, 1) *
      amp[3025] - amp[3027] - Complex<double> (0, 1) * amp[3028] +
      Complex<double> (0, 1) * amp[3030] - amp[3033] + Complex<double> (0, 1) *
      amp[3034] - Complex<double> (0, 1) * amp[3037] - amp[3039] -
      Complex<double> (0, 1) * amp[3040] + Complex<double> (0, 1) * amp[3042] -
      Complex<double> (0, 1) * amp[3044] - Complex<double> (0, 1) * amp[3047] -
      amp[3049] - Complex<double> (0, 1) * amp[3050] - amp[3051] - amp[3052] -
      Complex<double> (0, 1) * amp[3053] - amp[3054] - Complex<double> (0, 1) *
      amp[3056] - Complex<double> (0, 1) * amp[3059] - amp[3060] - amp[3061] -
      amp[3062] - Complex<double> (0, 1) * amp[3064] - amp[3065] - amp[3066] -
      Complex<double> (0, 1) * amp[3067] - amp[3068] + Complex<double> (0, 1) *
      amp[3069] - amp[3070] - amp[3072] - Complex<double> (0, 1) * amp[3073] -
      amp[3074] - amp[3075] - Complex<double> (0, 1) * amp[3076] - amp[3077] +
      Complex<double> (0, 1) * amp[3078] - amp[3079]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[15][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P10_sm_qq_zzgqq::matrix_12_dsx_zzgdsx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 140;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[3100] + amp[3101] + amp[3102] + amp[3103] +
      amp[3104] + amp[3105] + amp[3106] + amp[3107] + amp[3108] + amp[3109] +
      amp[3110] + amp[3111] + amp[3112] + amp[3113] + amp[3114] + amp[3115] +
      amp[3116] + amp[3117] + amp[3118] + amp[3119] + amp[3140] + amp[3141] +
      amp[3142] + amp[3143] + amp[3144] + amp[3145] + amp[3146] + amp[3147] +
      amp[3148] + amp[3149] + amp[3150] + amp[3151] + amp[3152] + amp[3153] +
      amp[3154] + amp[3155] + amp[3156] + amp[3157] + amp[3158] + amp[3159] +
      amp[3161] - Complex<double> (0, 1) * amp[3162] + Complex<double> (0, 1) *
      amp[3165] + amp[3167] + Complex<double> (0, 1) * amp[3168] -
      Complex<double> (0, 1) * amp[3170] + amp[3173] - Complex<double> (0, 1) *
      amp[3174] + Complex<double> (0, 1) * amp[3177] + amp[3179] +
      Complex<double> (0, 1) * amp[3180] - Complex<double> (0, 1) * amp[3182] +
      Complex<double> (0, 1) * amp[3184] + Complex<double> (0, 1) * amp[3187] +
      amp[3189] + Complex<double> (0, 1) * amp[3190] + amp[3191] + amp[3192] +
      Complex<double> (0, 1) * amp[3193] + amp[3194] + Complex<double> (0, 1) *
      amp[3196] + Complex<double> (0, 1) * amp[3199] + amp[3200] + amp[3201] +
      amp[3202] + Complex<double> (0, 1) * amp[3204] + amp[3205] + amp[3206] +
      Complex<double> (0, 1) * amp[3207] + amp[3208] - Complex<double> (0, 1) *
      amp[3209] + amp[3210] + amp[3212] + Complex<double> (0, 1) * amp[3213] +
      amp[3214] + amp[3215] + Complex<double> (0, 1) * amp[3216] + amp[3217] -
      Complex<double> (0, 1) * amp[3218] + amp[3219]);
  jamp[1] = +1./2. * (-1./3. * amp[3080] - 1./3. * amp[3081] - 1./3. *
      amp[3082] - 1./3. * amp[3083] - 1./3. * amp[3084] - 1./3. * amp[3085] -
      1./3. * amp[3086] - 1./3. * amp[3087] - 1./3. * amp[3088] - 1./3. *
      amp[3089] - 1./3. * amp[3090] - 1./3. * amp[3091] - 1./3. * amp[3092] -
      1./3. * amp[3093] - 1./3. * amp[3094] - 1./3. * amp[3095] - 1./3. *
      amp[3096] - 1./3. * amp[3097] - 1./3. * amp[3098] - 1./3. * amp[3099] -
      1./3. * amp[3100] - 1./3. * amp[3101] - 1./3. * amp[3102] - 1./3. *
      amp[3103] - 1./3. * amp[3104] - 1./3. * amp[3105] - 1./3. * amp[3106] -
      1./3. * amp[3107] - 1./3. * amp[3108] - 1./3. * amp[3109] - 1./3. *
      amp[3110] - 1./3. * amp[3111] - 1./3. * amp[3112] - 1./3. * amp[3113] -
      1./3. * amp[3114] - 1./3. * amp[3115] - 1./3. * amp[3116] - 1./3. *
      amp[3117] - 1./3. * amp[3118] - 1./3. * amp[3119] - 1./3. * amp[3160] -
      1./3. * amp[3161] - 1./3. * amp[3163] - 1./3. * amp[3166] - 1./3. *
      amp[3169] - 1./3. * amp[3171] - 1./3. * amp[3172] - 1./3. * amp[3173] -
      1./3. * amp[3175] - 1./3. * amp[3178] - 1./3. * amp[3181] - 1./3. *
      amp[3183] - 1./3. * amp[3202] - 1./3. * amp[3205] - 1./3. * amp[3208] -
      1./3. * amp[3210] - 1./3. * amp[3212] - 1./3. * amp[3215] - 1./3. *
      amp[3217] - 1./3. * amp[3219]);
  jamp[2] = +1./2. * (+amp[3080] + amp[3081] + amp[3082] + amp[3083] +
      amp[3084] + amp[3085] + amp[3086] + amp[3087] + amp[3088] + amp[3089] +
      amp[3090] + amp[3091] + amp[3092] + amp[3093] + amp[3094] + amp[3095] +
      amp[3096] + amp[3097] + amp[3098] + amp[3099] + amp[3120] + amp[3121] +
      amp[3122] + amp[3123] + amp[3124] + amp[3125] + amp[3126] + amp[3127] +
      amp[3128] + amp[3129] + amp[3130] + amp[3131] + amp[3132] + amp[3133] +
      amp[3134] + amp[3135] + amp[3136] + amp[3137] + amp[3138] + amp[3139] +
      amp[3160] + Complex<double> (0, 1) * amp[3162] + amp[3163] + amp[3164] -
      Complex<double> (0, 1) * amp[3165] + amp[3166] - Complex<double> (0, 1) *
      amp[3168] + amp[3169] + Complex<double> (0, 1) * amp[3170] + amp[3171] +
      amp[3172] + Complex<double> (0, 1) * amp[3174] + amp[3175] + amp[3176] -
      Complex<double> (0, 1) * amp[3177] + amp[3178] - Complex<double> (0, 1) *
      amp[3180] + amp[3181] + Complex<double> (0, 1) * amp[3182] + amp[3183] -
      Complex<double> (0, 1) * amp[3184] + amp[3185] + amp[3186] -
      Complex<double> (0, 1) * amp[3187] + amp[3188] - Complex<double> (0, 1) *
      amp[3190] - Complex<double> (0, 1) * amp[3193] + amp[3195] -
      Complex<double> (0, 1) * amp[3196] + amp[3197] + amp[3198] -
      Complex<double> (0, 1) * amp[3199] + amp[3203] - Complex<double> (0, 1) *
      amp[3204] - Complex<double> (0, 1) * amp[3207] + Complex<double> (0, 1) *
      amp[3209] + amp[3211] - Complex<double> (0, 1) * amp[3213] -
      Complex<double> (0, 1) * amp[3216] + Complex<double> (0, 1) * amp[3218]);
  jamp[3] = +1./2. * (-1./3. * amp[3120] - 1./3. * amp[3121] - 1./3. *
      amp[3122] - 1./3. * amp[3123] - 1./3. * amp[3124] - 1./3. * amp[3125] -
      1./3. * amp[3126] - 1./3. * amp[3127] - 1./3. * amp[3128] - 1./3. *
      amp[3129] - 1./3. * amp[3130] - 1./3. * amp[3131] - 1./3. * amp[3132] -
      1./3. * amp[3133] - 1./3. * amp[3134] - 1./3. * amp[3135] - 1./3. *
      amp[3136] - 1./3. * amp[3137] - 1./3. * amp[3138] - 1./3. * amp[3139] -
      1./3. * amp[3140] - 1./3. * amp[3141] - 1./3. * amp[3142] - 1./3. *
      amp[3143] - 1./3. * amp[3144] - 1./3. * amp[3145] - 1./3. * amp[3146] -
      1./3. * amp[3147] - 1./3. * amp[3148] - 1./3. * amp[3149] - 1./3. *
      amp[3150] - 1./3. * amp[3151] - 1./3. * amp[3152] - 1./3. * amp[3153] -
      1./3. * amp[3154] - 1./3. * amp[3155] - 1./3. * amp[3156] - 1./3. *
      amp[3157] - 1./3. * amp[3158] - 1./3. * amp[3159] - 1./3. * amp[3164] -
      1./3. * amp[3167] - 1./3. * amp[3176] - 1./3. * amp[3179] - 1./3. *
      amp[3185] - 1./3. * amp[3186] - 1./3. * amp[3188] - 1./3. * amp[3189] -
      1./3. * amp[3191] - 1./3. * amp[3192] - 1./3. * amp[3194] - 1./3. *
      amp[3195] - 1./3. * amp[3197] - 1./3. * amp[3198] - 1./3. * amp[3200] -
      1./3. * amp[3201] - 1./3. * amp[3203] - 1./3. * amp[3206] - 1./3. *
      amp[3211] - 1./3. * amp[3214]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[16][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P10_sm_qq_zzgqq::matrix_12_uxcx_zzguxcx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 140;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[3260] + 1./3. * amp[3261] + 1./3. *
      amp[3262] + 1./3. * amp[3263] + 1./3. * amp[3264] + 1./3. * amp[3265] +
      1./3. * amp[3266] + 1./3. * amp[3267] + 1./3. * amp[3268] + 1./3. *
      amp[3269] + 1./3. * amp[3270] + 1./3. * amp[3271] + 1./3. * amp[3272] +
      1./3. * amp[3273] + 1./3. * amp[3274] + 1./3. * amp[3275] + 1./3. *
      amp[3276] + 1./3. * amp[3277] + 1./3. * amp[3278] + 1./3. * amp[3279] +
      1./3. * amp[3280] + 1./3. * amp[3281] + 1./3. * amp[3282] + 1./3. *
      amp[3283] + 1./3. * amp[3284] + 1./3. * amp[3285] + 1./3. * amp[3286] +
      1./3. * amp[3287] + 1./3. * amp[3288] + 1./3. * amp[3289] + 1./3. *
      amp[3290] + 1./3. * amp[3291] + 1./3. * amp[3292] + 1./3. * amp[3293] +
      1./3. * amp[3294] + 1./3. * amp[3295] + 1./3. * amp[3296] + 1./3. *
      amp[3297] + 1./3. * amp[3298] + 1./3. * amp[3299] + 1./3. * amp[3304] +
      1./3. * amp[3307] + 1./3. * amp[3316] + 1./3. * amp[3319] + 1./3. *
      amp[3325] + 1./3. * amp[3326] + 1./3. * amp[3328] + 1./3. * amp[3329] +
      1./3. * amp[3331] + 1./3. * amp[3332] + 1./3. * amp[3334] + 1./3. *
      amp[3335] + 1./3. * amp[3337] + 1./3. * amp[3338] + 1./3. * amp[3340] +
      1./3. * amp[3341] + 1./3. * amp[3343] + 1./3. * amp[3346] + 1./3. *
      amp[3351] + 1./3. * amp[3354]);
  jamp[1] = +1./2. * (-amp[3220] - amp[3221] - amp[3222] - amp[3223] -
      amp[3224] - amp[3225] - amp[3226] - amp[3227] - amp[3228] - amp[3229] -
      amp[3230] - amp[3231] - amp[3232] - amp[3233] - amp[3234] - amp[3235] -
      amp[3236] - amp[3237] - amp[3238] - amp[3239] - amp[3260] - amp[3261] -
      amp[3262] - amp[3263] - amp[3264] - amp[3265] - amp[3266] - amp[3267] -
      amp[3268] - amp[3269] - amp[3270] - amp[3271] - amp[3272] - amp[3273] -
      amp[3274] - amp[3275] - amp[3276] - amp[3277] - amp[3278] - amp[3279] -
      amp[3300] - Complex<double> (0, 1) * amp[3302] - amp[3303] - amp[3304] +
      Complex<double> (0, 1) * amp[3305] - amp[3306] + Complex<double> (0, 1) *
      amp[3308] - amp[3309] - Complex<double> (0, 1) * amp[3310] - amp[3311] -
      amp[3312] - Complex<double> (0, 1) * amp[3314] - amp[3315] - amp[3316] +
      Complex<double> (0, 1) * amp[3317] - amp[3318] + Complex<double> (0, 1) *
      amp[3320] - amp[3321] - Complex<double> (0, 1) * amp[3322] - amp[3323] +
      Complex<double> (0, 1) * amp[3324] - amp[3325] - amp[3326] +
      Complex<double> (0, 1) * amp[3327] - amp[3328] + Complex<double> (0, 1) *
      amp[3330] + Complex<double> (0, 1) * amp[3333] - amp[3335] +
      Complex<double> (0, 1) * amp[3336] - amp[3337] - amp[3338] +
      Complex<double> (0, 1) * amp[3339] - amp[3343] + Complex<double> (0, 1) *
      amp[3344] + Complex<double> (0, 1) * amp[3347] - Complex<double> (0, 1) *
      amp[3349] - amp[3351] + Complex<double> (0, 1) * amp[3353] +
      Complex<double> (0, 1) * amp[3356] - Complex<double> (0, 1) * amp[3358]);
  jamp[2] = +1./2. * (-amp[3240] - amp[3241] - amp[3242] - amp[3243] -
      amp[3244] - amp[3245] - amp[3246] - amp[3247] - amp[3248] - amp[3249] -
      amp[3250] - amp[3251] - amp[3252] - amp[3253] - amp[3254] - amp[3255] -
      amp[3256] - amp[3257] - amp[3258] - amp[3259] - amp[3280] - amp[3281] -
      amp[3282] - amp[3283] - amp[3284] - amp[3285] - amp[3286] - amp[3287] -
      amp[3288] - amp[3289] - amp[3290] - amp[3291] - amp[3292] - amp[3293] -
      amp[3294] - amp[3295] - amp[3296] - amp[3297] - amp[3298] - amp[3299] -
      amp[3301] + Complex<double> (0, 1) * amp[3302] - Complex<double> (0, 1) *
      amp[3305] - amp[3307] - Complex<double> (0, 1) * amp[3308] +
      Complex<double> (0, 1) * amp[3310] - amp[3313] + Complex<double> (0, 1) *
      amp[3314] - Complex<double> (0, 1) * amp[3317] - amp[3319] -
      Complex<double> (0, 1) * amp[3320] + Complex<double> (0, 1) * amp[3322] -
      Complex<double> (0, 1) * amp[3324] - Complex<double> (0, 1) * amp[3327] -
      amp[3329] - Complex<double> (0, 1) * amp[3330] - amp[3331] - amp[3332] -
      Complex<double> (0, 1) * amp[3333] - amp[3334] - Complex<double> (0, 1) *
      amp[3336] - Complex<double> (0, 1) * amp[3339] - amp[3340] - amp[3341] -
      amp[3342] - Complex<double> (0, 1) * amp[3344] - amp[3345] - amp[3346] -
      Complex<double> (0, 1) * amp[3347] - amp[3348] + Complex<double> (0, 1) *
      amp[3349] - amp[3350] - amp[3352] - Complex<double> (0, 1) * amp[3353] -
      amp[3354] - amp[3355] - Complex<double> (0, 1) * amp[3356] - amp[3357] +
      Complex<double> (0, 1) * amp[3358] - amp[3359]);
  jamp[3] = +1./2. * (+1./3. * amp[3220] + 1./3. * amp[3221] + 1./3. *
      amp[3222] + 1./3. * amp[3223] + 1./3. * amp[3224] + 1./3. * amp[3225] +
      1./3. * amp[3226] + 1./3. * amp[3227] + 1./3. * amp[3228] + 1./3. *
      amp[3229] + 1./3. * amp[3230] + 1./3. * amp[3231] + 1./3. * amp[3232] +
      1./3. * amp[3233] + 1./3. * amp[3234] + 1./3. * amp[3235] + 1./3. *
      amp[3236] + 1./3. * amp[3237] + 1./3. * amp[3238] + 1./3. * amp[3239] +
      1./3. * amp[3240] + 1./3. * amp[3241] + 1./3. * amp[3242] + 1./3. *
      amp[3243] + 1./3. * amp[3244] + 1./3. * amp[3245] + 1./3. * amp[3246] +
      1./3. * amp[3247] + 1./3. * amp[3248] + 1./3. * amp[3249] + 1./3. *
      amp[3250] + 1./3. * amp[3251] + 1./3. * amp[3252] + 1./3. * amp[3253] +
      1./3. * amp[3254] + 1./3. * amp[3255] + 1./3. * amp[3256] + 1./3. *
      amp[3257] + 1./3. * amp[3258] + 1./3. * amp[3259] + 1./3. * amp[3300] +
      1./3. * amp[3301] + 1./3. * amp[3303] + 1./3. * amp[3306] + 1./3. *
      amp[3309] + 1./3. * amp[3311] + 1./3. * amp[3312] + 1./3. * amp[3313] +
      1./3. * amp[3315] + 1./3. * amp[3318] + 1./3. * amp[3321] + 1./3. *
      amp[3323] + 1./3. * amp[3342] + 1./3. * amp[3345] + 1./3. * amp[3348] +
      1./3. * amp[3350] + 1./3. * amp[3352] + 1./3. * amp[3355] + 1./3. *
      amp[3357] + 1./3. * amp[3359]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[17][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P10_sm_qq_zzgqq::matrix_12_uxdx_zzguxdx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 140;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[3400] + 1./3. * amp[3401] + 1./3. *
      amp[3402] + 1./3. * amp[3403] + 1./3. * amp[3404] + 1./3. * amp[3405] +
      1./3. * amp[3406] + 1./3. * amp[3407] + 1./3. * amp[3408] + 1./3. *
      amp[3409] + 1./3. * amp[3410] + 1./3. * amp[3411] + 1./3. * amp[3412] +
      1./3. * amp[3413] + 1./3. * amp[3414] + 1./3. * amp[3415] + 1./3. *
      amp[3416] + 1./3. * amp[3417] + 1./3. * amp[3418] + 1./3. * amp[3419] +
      1./3. * amp[3420] + 1./3. * amp[3421] + 1./3. * amp[3422] + 1./3. *
      amp[3423] + 1./3. * amp[3424] + 1./3. * amp[3425] + 1./3. * amp[3426] +
      1./3. * amp[3427] + 1./3. * amp[3428] + 1./3. * amp[3429] + 1./3. *
      amp[3430] + 1./3. * amp[3431] + 1./3. * amp[3432] + 1./3. * amp[3433] +
      1./3. * amp[3434] + 1./3. * amp[3435] + 1./3. * amp[3436] + 1./3. *
      amp[3437] + 1./3. * amp[3438] + 1./3. * amp[3439] + 1./3. * amp[3444] +
      1./3. * amp[3447] + 1./3. * amp[3456] + 1./3. * amp[3459] + 1./3. *
      amp[3465] + 1./3. * amp[3466] + 1./3. * amp[3468] + 1./3. * amp[3469] +
      1./3. * amp[3471] + 1./3. * amp[3472] + 1./3. * amp[3474] + 1./3. *
      amp[3475] + 1./3. * amp[3477] + 1./3. * amp[3478] + 1./3. * amp[3480] +
      1./3. * amp[3481] + 1./3. * amp[3483] + 1./3. * amp[3486] + 1./3. *
      amp[3491] + 1./3. * amp[3494]);
  jamp[1] = +1./2. * (-amp[3360] - amp[3361] - amp[3362] - amp[3363] -
      amp[3364] - amp[3365] - amp[3366] - amp[3367] - amp[3368] - amp[3369] -
      amp[3370] - amp[3371] - amp[3372] - amp[3373] - amp[3374] - amp[3375] -
      amp[3376] - amp[3377] - amp[3378] - amp[3379] - amp[3400] - amp[3401] -
      amp[3402] - amp[3403] - amp[3404] - amp[3405] - amp[3406] - amp[3407] -
      amp[3408] - amp[3409] - amp[3410] - amp[3411] - amp[3412] - amp[3413] -
      amp[3414] - amp[3415] - amp[3416] - amp[3417] - amp[3418] - amp[3419] -
      amp[3440] - Complex<double> (0, 1) * amp[3442] - amp[3443] - amp[3444] +
      Complex<double> (0, 1) * amp[3445] - amp[3446] + Complex<double> (0, 1) *
      amp[3448] - amp[3449] - Complex<double> (0, 1) * amp[3450] - amp[3451] -
      amp[3452] - Complex<double> (0, 1) * amp[3454] - amp[3455] - amp[3456] +
      Complex<double> (0, 1) * amp[3457] - amp[3458] + Complex<double> (0, 1) *
      amp[3460] - amp[3461] - Complex<double> (0, 1) * amp[3462] - amp[3463] +
      Complex<double> (0, 1) * amp[3464] - amp[3465] - amp[3466] +
      Complex<double> (0, 1) * amp[3467] - amp[3468] + Complex<double> (0, 1) *
      amp[3470] + Complex<double> (0, 1) * amp[3473] - amp[3475] +
      Complex<double> (0, 1) * amp[3476] - amp[3477] - amp[3478] +
      Complex<double> (0, 1) * amp[3479] - amp[3483] + Complex<double> (0, 1) *
      amp[3484] + Complex<double> (0, 1) * amp[3487] - Complex<double> (0, 1) *
      amp[3489] - amp[3491] + Complex<double> (0, 1) * amp[3493] +
      Complex<double> (0, 1) * amp[3496] - Complex<double> (0, 1) * amp[3498]);
  jamp[2] = +1./2. * (-amp[3380] - amp[3381] - amp[3382] - amp[3383] -
      amp[3384] - amp[3385] - amp[3386] - amp[3387] - amp[3388] - amp[3389] -
      amp[3390] - amp[3391] - amp[3392] - amp[3393] - amp[3394] - amp[3395] -
      amp[3396] - amp[3397] - amp[3398] - amp[3399] - amp[3420] - amp[3421] -
      amp[3422] - amp[3423] - amp[3424] - amp[3425] - amp[3426] - amp[3427] -
      amp[3428] - amp[3429] - amp[3430] - amp[3431] - amp[3432] - amp[3433] -
      amp[3434] - amp[3435] - amp[3436] - amp[3437] - amp[3438] - amp[3439] -
      amp[3441] + Complex<double> (0, 1) * amp[3442] - Complex<double> (0, 1) *
      amp[3445] - amp[3447] - Complex<double> (0, 1) * amp[3448] +
      Complex<double> (0, 1) * amp[3450] - amp[3453] + Complex<double> (0, 1) *
      amp[3454] - Complex<double> (0, 1) * amp[3457] - amp[3459] -
      Complex<double> (0, 1) * amp[3460] + Complex<double> (0, 1) * amp[3462] -
      Complex<double> (0, 1) * amp[3464] - Complex<double> (0, 1) * amp[3467] -
      amp[3469] - Complex<double> (0, 1) * amp[3470] - amp[3471] - amp[3472] -
      Complex<double> (0, 1) * amp[3473] - amp[3474] - Complex<double> (0, 1) *
      amp[3476] - Complex<double> (0, 1) * amp[3479] - amp[3480] - amp[3481] -
      amp[3482] - Complex<double> (0, 1) * amp[3484] - amp[3485] - amp[3486] -
      Complex<double> (0, 1) * amp[3487] - amp[3488] + Complex<double> (0, 1) *
      amp[3489] - amp[3490] - amp[3492] - Complex<double> (0, 1) * amp[3493] -
      amp[3494] - amp[3495] - Complex<double> (0, 1) * amp[3496] - amp[3497] +
      Complex<double> (0, 1) * amp[3498] - amp[3499]);
  jamp[3] = +1./2. * (+1./3. * amp[3360] + 1./3. * amp[3361] + 1./3. *
      amp[3362] + 1./3. * amp[3363] + 1./3. * amp[3364] + 1./3. * amp[3365] +
      1./3. * amp[3366] + 1./3. * amp[3367] + 1./3. * amp[3368] + 1./3. *
      amp[3369] + 1./3. * amp[3370] + 1./3. * amp[3371] + 1./3. * amp[3372] +
      1./3. * amp[3373] + 1./3. * amp[3374] + 1./3. * amp[3375] + 1./3. *
      amp[3376] + 1./3. * amp[3377] + 1./3. * amp[3378] + 1./3. * amp[3379] +
      1./3. * amp[3380] + 1./3. * amp[3381] + 1./3. * amp[3382] + 1./3. *
      amp[3383] + 1./3. * amp[3384] + 1./3. * amp[3385] + 1./3. * amp[3386] +
      1./3. * amp[3387] + 1./3. * amp[3388] + 1./3. * amp[3389] + 1./3. *
      amp[3390] + 1./3. * amp[3391] + 1./3. * amp[3392] + 1./3. * amp[3393] +
      1./3. * amp[3394] + 1./3. * amp[3395] + 1./3. * amp[3396] + 1./3. *
      amp[3397] + 1./3. * amp[3398] + 1./3. * amp[3399] + 1./3. * amp[3440] +
      1./3. * amp[3441] + 1./3. * amp[3443] + 1./3. * amp[3446] + 1./3. *
      amp[3449] + 1./3. * amp[3451] + 1./3. * amp[3452] + 1./3. * amp[3453] +
      1./3. * amp[3455] + 1./3. * amp[3458] + 1./3. * amp[3461] + 1./3. *
      amp[3463] + 1./3. * amp[3482] + 1./3. * amp[3485] + 1./3. * amp[3488] +
      1./3. * amp[3490] + 1./3. * amp[3492] + 1./3. * amp[3495] + 1./3. *
      amp[3497] + 1./3. * amp[3499]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[18][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P10_sm_qq_zzgqq::matrix_12_dxsx_zzgdxsx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 140;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[3540] + 1./3. * amp[3541] + 1./3. *
      amp[3542] + 1./3. * amp[3543] + 1./3. * amp[3544] + 1./3. * amp[3545] +
      1./3. * amp[3546] + 1./3. * amp[3547] + 1./3. * amp[3548] + 1./3. *
      amp[3549] + 1./3. * amp[3550] + 1./3. * amp[3551] + 1./3. * amp[3552] +
      1./3. * amp[3553] + 1./3. * amp[3554] + 1./3. * amp[3555] + 1./3. *
      amp[3556] + 1./3. * amp[3557] + 1./3. * amp[3558] + 1./3. * amp[3559] +
      1./3. * amp[3560] + 1./3. * amp[3561] + 1./3. * amp[3562] + 1./3. *
      amp[3563] + 1./3. * amp[3564] + 1./3. * amp[3565] + 1./3. * amp[3566] +
      1./3. * amp[3567] + 1./3. * amp[3568] + 1./3. * amp[3569] + 1./3. *
      amp[3570] + 1./3. * amp[3571] + 1./3. * amp[3572] + 1./3. * amp[3573] +
      1./3. * amp[3574] + 1./3. * amp[3575] + 1./3. * amp[3576] + 1./3. *
      amp[3577] + 1./3. * amp[3578] + 1./3. * amp[3579] + 1./3. * amp[3584] +
      1./3. * amp[3587] + 1./3. * amp[3596] + 1./3. * amp[3599] + 1./3. *
      amp[3605] + 1./3. * amp[3606] + 1./3. * amp[3608] + 1./3. * amp[3609] +
      1./3. * amp[3611] + 1./3. * amp[3612] + 1./3. * amp[3614] + 1./3. *
      amp[3615] + 1./3. * amp[3617] + 1./3. * amp[3618] + 1./3. * amp[3620] +
      1./3. * amp[3621] + 1./3. * amp[3623] + 1./3. * amp[3626] + 1./3. *
      amp[3631] + 1./3. * amp[3634]);
  jamp[1] = +1./2. * (-amp[3500] - amp[3501] - amp[3502] - amp[3503] -
      amp[3504] - amp[3505] - amp[3506] - amp[3507] - amp[3508] - amp[3509] -
      amp[3510] - amp[3511] - amp[3512] - amp[3513] - amp[3514] - amp[3515] -
      amp[3516] - amp[3517] - amp[3518] - amp[3519] - amp[3540] - amp[3541] -
      amp[3542] - amp[3543] - amp[3544] - amp[3545] - amp[3546] - amp[3547] -
      amp[3548] - amp[3549] - amp[3550] - amp[3551] - amp[3552] - amp[3553] -
      amp[3554] - amp[3555] - amp[3556] - amp[3557] - amp[3558] - amp[3559] -
      amp[3580] - Complex<double> (0, 1) * amp[3582] - amp[3583] - amp[3584] +
      Complex<double> (0, 1) * amp[3585] - amp[3586] + Complex<double> (0, 1) *
      amp[3588] - amp[3589] - Complex<double> (0, 1) * amp[3590] - amp[3591] -
      amp[3592] - Complex<double> (0, 1) * amp[3594] - amp[3595] - amp[3596] +
      Complex<double> (0, 1) * amp[3597] - amp[3598] + Complex<double> (0, 1) *
      amp[3600] - amp[3601] - Complex<double> (0, 1) * amp[3602] - amp[3603] +
      Complex<double> (0, 1) * amp[3604] - amp[3605] - amp[3606] +
      Complex<double> (0, 1) * amp[3607] - amp[3608] + Complex<double> (0, 1) *
      amp[3610] + Complex<double> (0, 1) * amp[3613] - amp[3615] +
      Complex<double> (0, 1) * amp[3616] - amp[3617] - amp[3618] +
      Complex<double> (0, 1) * amp[3619] - amp[3623] + Complex<double> (0, 1) *
      amp[3624] + Complex<double> (0, 1) * amp[3627] - Complex<double> (0, 1) *
      amp[3629] - amp[3631] + Complex<double> (0, 1) * amp[3633] +
      Complex<double> (0, 1) * amp[3636] - Complex<double> (0, 1) * amp[3638]);
  jamp[2] = +1./2. * (-amp[3520] - amp[3521] - amp[3522] - amp[3523] -
      amp[3524] - amp[3525] - amp[3526] - amp[3527] - amp[3528] - amp[3529] -
      amp[3530] - amp[3531] - amp[3532] - amp[3533] - amp[3534] - amp[3535] -
      amp[3536] - amp[3537] - amp[3538] - amp[3539] - amp[3560] - amp[3561] -
      amp[3562] - amp[3563] - amp[3564] - amp[3565] - amp[3566] - amp[3567] -
      amp[3568] - amp[3569] - amp[3570] - amp[3571] - amp[3572] - amp[3573] -
      amp[3574] - amp[3575] - amp[3576] - amp[3577] - amp[3578] - amp[3579] -
      amp[3581] + Complex<double> (0, 1) * amp[3582] - Complex<double> (0, 1) *
      amp[3585] - amp[3587] - Complex<double> (0, 1) * amp[3588] +
      Complex<double> (0, 1) * amp[3590] - amp[3593] + Complex<double> (0, 1) *
      amp[3594] - Complex<double> (0, 1) * amp[3597] - amp[3599] -
      Complex<double> (0, 1) * amp[3600] + Complex<double> (0, 1) * amp[3602] -
      Complex<double> (0, 1) * amp[3604] - Complex<double> (0, 1) * amp[3607] -
      amp[3609] - Complex<double> (0, 1) * amp[3610] - amp[3611] - amp[3612] -
      Complex<double> (0, 1) * amp[3613] - amp[3614] - Complex<double> (0, 1) *
      amp[3616] - Complex<double> (0, 1) * amp[3619] - amp[3620] - amp[3621] -
      amp[3622] - Complex<double> (0, 1) * amp[3624] - amp[3625] - amp[3626] -
      Complex<double> (0, 1) * amp[3627] - amp[3628] + Complex<double> (0, 1) *
      amp[3629] - amp[3630] - amp[3632] - Complex<double> (0, 1) * amp[3633] -
      amp[3634] - amp[3635] - Complex<double> (0, 1) * amp[3636] - amp[3637] +
      Complex<double> (0, 1) * amp[3638] - amp[3639]);
  jamp[3] = +1./2. * (+1./3. * amp[3500] + 1./3. * amp[3501] + 1./3. *
      amp[3502] + 1./3. * amp[3503] + 1./3. * amp[3504] + 1./3. * amp[3505] +
      1./3. * amp[3506] + 1./3. * amp[3507] + 1./3. * amp[3508] + 1./3. *
      amp[3509] + 1./3. * amp[3510] + 1./3. * amp[3511] + 1./3. * amp[3512] +
      1./3. * amp[3513] + 1./3. * amp[3514] + 1./3. * amp[3515] + 1./3. *
      amp[3516] + 1./3. * amp[3517] + 1./3. * amp[3518] + 1./3. * amp[3519] +
      1./3. * amp[3520] + 1./3. * amp[3521] + 1./3. * amp[3522] + 1./3. *
      amp[3523] + 1./3. * amp[3524] + 1./3. * amp[3525] + 1./3. * amp[3526] +
      1./3. * amp[3527] + 1./3. * amp[3528] + 1./3. * amp[3529] + 1./3. *
      amp[3530] + 1./3. * amp[3531] + 1./3. * amp[3532] + 1./3. * amp[3533] +
      1./3. * amp[3534] + 1./3. * amp[3535] + 1./3. * amp[3536] + 1./3. *
      amp[3537] + 1./3. * amp[3538] + 1./3. * amp[3539] + 1./3. * amp[3580] +
      1./3. * amp[3581] + 1./3. * amp[3583] + 1./3. * amp[3586] + 1./3. *
      amp[3589] + 1./3. * amp[3591] + 1./3. * amp[3592] + 1./3. * amp[3593] +
      1./3. * amp[3595] + 1./3. * amp[3598] + 1./3. * amp[3601] + 1./3. *
      amp[3603] + 1./3. * amp[3622] + 1./3. * amp[3625] + 1./3. * amp[3628] +
      1./3. * amp[3630] + 1./3. * amp[3632] + 1./3. * amp[3635] + 1./3. *
      amp[3637] + 1./3. * amp[3639]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[19][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}


}  // end namespace PY8MEs_namespace

