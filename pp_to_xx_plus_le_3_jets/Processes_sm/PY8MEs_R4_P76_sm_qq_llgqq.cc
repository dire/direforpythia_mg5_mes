//==========================================================================
// This file has been automatically generated for Pythia 8 by
// MadGraph5_aMC@NLO v. 2.6.0, 2017-08-16
// By the MadGraph5_aMC@NLO Development Team
// Visit launchpad.net/madgraph5 and amcatnlo.web.cern.ch
//==========================================================================

#include "PY8MEs_R4_P76_sm_qq_llgqq.h"
#include "HelAmps_sm.h"

using namespace Pythia8_sm; 

namespace PY8MEs_namespace 
{
//==========================================================================
// Class member functions for calculating the matrix elements for
// Process: u u > e+ e- g u u WEIGHTED<=7 @4
// Process: u u > mu+ mu- g u u WEIGHTED<=7 @4
// Process: c c > e+ e- g c c WEIGHTED<=7 @4
// Process: c c > mu+ mu- g c c WEIGHTED<=7 @4
// Process: u u~ > e+ e- g u u~ WEIGHTED<=7 @4
// Process: u u~ > mu+ mu- g u u~ WEIGHTED<=7 @4
// Process: c c~ > e+ e- g c c~ WEIGHTED<=7 @4
// Process: c c~ > mu+ mu- g c c~ WEIGHTED<=7 @4
// Process: d d > e+ e- g d d WEIGHTED<=7 @4
// Process: d d > mu+ mu- g d d WEIGHTED<=7 @4
// Process: s s > e+ e- g s s WEIGHTED<=7 @4
// Process: s s > mu+ mu- g s s WEIGHTED<=7 @4
// Process: d d~ > e+ e- g d d~ WEIGHTED<=7 @4
// Process: d d~ > mu+ mu- g d d~ WEIGHTED<=7 @4
// Process: s s~ > e+ e- g s s~ WEIGHTED<=7 @4
// Process: s s~ > mu+ mu- g s s~ WEIGHTED<=7 @4
// Process: u~ u~ > e+ e- g u~ u~ WEIGHTED<=7 @4
// Process: u~ u~ > mu+ mu- g u~ u~ WEIGHTED<=7 @4
// Process: c~ c~ > e+ e- g c~ c~ WEIGHTED<=7 @4
// Process: c~ c~ > mu+ mu- g c~ c~ WEIGHTED<=7 @4
// Process: d~ d~ > e+ e- g d~ d~ WEIGHTED<=7 @4
// Process: d~ d~ > mu+ mu- g d~ d~ WEIGHTED<=7 @4
// Process: s~ s~ > e+ e- g s~ s~ WEIGHTED<=7 @4
// Process: s~ s~ > mu+ mu- g s~ s~ WEIGHTED<=7 @4
// Process: u u > ve~ ve g u u WEIGHTED<=7 @4
// Process: u u > vm~ vm g u u WEIGHTED<=7 @4
// Process: u u > vt~ vt g u u WEIGHTED<=7 @4
// Process: c c > ve~ ve g c c WEIGHTED<=7 @4
// Process: c c > vm~ vm g c c WEIGHTED<=7 @4
// Process: c c > vt~ vt g c c WEIGHTED<=7 @4
// Process: u c > e+ e- g u c WEIGHTED<=7 @4
// Process: u c > mu+ mu- g u c WEIGHTED<=7 @4
// Process: u d > e+ e- g u d WEIGHTED<=7 @4
// Process: u d > mu+ mu- g u d WEIGHTED<=7 @4
// Process: u s > e+ e- g u s WEIGHTED<=7 @4
// Process: u s > mu+ mu- g u s WEIGHTED<=7 @4
// Process: c d > e+ e- g c d WEIGHTED<=7 @4
// Process: c d > mu+ mu- g c d WEIGHTED<=7 @4
// Process: c s > e+ e- g c s WEIGHTED<=7 @4
// Process: c s > mu+ mu- g c s WEIGHTED<=7 @4
// Process: u u~ > e+ e- g c c~ WEIGHTED<=7 @4
// Process: u u~ > mu+ mu- g c c~ WEIGHTED<=7 @4
// Process: c c~ > e+ e- g u u~ WEIGHTED<=7 @4
// Process: c c~ > mu+ mu- g u u~ WEIGHTED<=7 @4
// Process: u u~ > e+ e- g d d~ WEIGHTED<=7 @4
// Process: u u~ > e+ e- g s s~ WEIGHTED<=7 @4
// Process: u u~ > mu+ mu- g d d~ WEIGHTED<=7 @4
// Process: u u~ > mu+ mu- g s s~ WEIGHTED<=7 @4
// Process: c c~ > e+ e- g d d~ WEIGHTED<=7 @4
// Process: c c~ > e+ e- g s s~ WEIGHTED<=7 @4
// Process: c c~ > mu+ mu- g d d~ WEIGHTED<=7 @4
// Process: c c~ > mu+ mu- g s s~ WEIGHTED<=7 @4
// Process: u u~ > ve~ ve g u u~ WEIGHTED<=7 @4
// Process: u u~ > vm~ vm g u u~ WEIGHTED<=7 @4
// Process: u u~ > vt~ vt g u u~ WEIGHTED<=7 @4
// Process: c c~ > ve~ ve g c c~ WEIGHTED<=7 @4
// Process: c c~ > vm~ vm g c c~ WEIGHTED<=7 @4
// Process: c c~ > vt~ vt g c c~ WEIGHTED<=7 @4
// Process: u c~ > e+ e- g u c~ WEIGHTED<=7 @4
// Process: u c~ > mu+ mu- g u c~ WEIGHTED<=7 @4
// Process: c u~ > e+ e- g c u~ WEIGHTED<=7 @4
// Process: c u~ > mu+ mu- g c u~ WEIGHTED<=7 @4
// Process: u d~ > e+ e- g u d~ WEIGHTED<=7 @4
// Process: u d~ > mu+ mu- g u d~ WEIGHTED<=7 @4
// Process: u s~ > e+ e- g u s~ WEIGHTED<=7 @4
// Process: u s~ > mu+ mu- g u s~ WEIGHTED<=7 @4
// Process: c d~ > e+ e- g c d~ WEIGHTED<=7 @4
// Process: c d~ > mu+ mu- g c d~ WEIGHTED<=7 @4
// Process: c s~ > e+ e- g c s~ WEIGHTED<=7 @4
// Process: c s~ > mu+ mu- g c s~ WEIGHTED<=7 @4
// Process: d d > ve~ ve g d d WEIGHTED<=7 @4
// Process: d d > vm~ vm g d d WEIGHTED<=7 @4
// Process: d d > vt~ vt g d d WEIGHTED<=7 @4
// Process: s s > ve~ ve g s s WEIGHTED<=7 @4
// Process: s s > vm~ vm g s s WEIGHTED<=7 @4
// Process: s s > vt~ vt g s s WEIGHTED<=7 @4
// Process: d s > e+ e- g d s WEIGHTED<=7 @4
// Process: d s > mu+ mu- g d s WEIGHTED<=7 @4
// Process: d u~ > e+ e- g d u~ WEIGHTED<=7 @4
// Process: d u~ > mu+ mu- g d u~ WEIGHTED<=7 @4
// Process: d c~ > e+ e- g d c~ WEIGHTED<=7 @4
// Process: d c~ > mu+ mu- g d c~ WEIGHTED<=7 @4
// Process: s u~ > e+ e- g s u~ WEIGHTED<=7 @4
// Process: s u~ > mu+ mu- g s u~ WEIGHTED<=7 @4
// Process: s c~ > e+ e- g s c~ WEIGHTED<=7 @4
// Process: s c~ > mu+ mu- g s c~ WEIGHTED<=7 @4
// Process: d d~ > e+ e- g u u~ WEIGHTED<=7 @4
// Process: d d~ > e+ e- g c c~ WEIGHTED<=7 @4
// Process: d d~ > mu+ mu- g u u~ WEIGHTED<=7 @4
// Process: d d~ > mu+ mu- g c c~ WEIGHTED<=7 @4
// Process: s s~ > e+ e- g u u~ WEIGHTED<=7 @4
// Process: s s~ > e+ e- g c c~ WEIGHTED<=7 @4
// Process: s s~ > mu+ mu- g u u~ WEIGHTED<=7 @4
// Process: s s~ > mu+ mu- g c c~ WEIGHTED<=7 @4
// Process: d d~ > e+ e- g s s~ WEIGHTED<=7 @4
// Process: d d~ > mu+ mu- g s s~ WEIGHTED<=7 @4
// Process: s s~ > e+ e- g d d~ WEIGHTED<=7 @4
// Process: s s~ > mu+ mu- g d d~ WEIGHTED<=7 @4
// Process: d d~ > ve~ ve g d d~ WEIGHTED<=7 @4
// Process: d d~ > vm~ vm g d d~ WEIGHTED<=7 @4
// Process: d d~ > vt~ vt g d d~ WEIGHTED<=7 @4
// Process: s s~ > ve~ ve g s s~ WEIGHTED<=7 @4
// Process: s s~ > vm~ vm g s s~ WEIGHTED<=7 @4
// Process: s s~ > vt~ vt g s s~ WEIGHTED<=7 @4
// Process: d s~ > e+ e- g d s~ WEIGHTED<=7 @4
// Process: d s~ > mu+ mu- g d s~ WEIGHTED<=7 @4
// Process: s d~ > e+ e- g s d~ WEIGHTED<=7 @4
// Process: s d~ > mu+ mu- g s d~ WEIGHTED<=7 @4
// Process: u~ u~ > ve~ ve g u~ u~ WEIGHTED<=7 @4
// Process: u~ u~ > vm~ vm g u~ u~ WEIGHTED<=7 @4
// Process: u~ u~ > vt~ vt g u~ u~ WEIGHTED<=7 @4
// Process: c~ c~ > ve~ ve g c~ c~ WEIGHTED<=7 @4
// Process: c~ c~ > vm~ vm g c~ c~ WEIGHTED<=7 @4
// Process: c~ c~ > vt~ vt g c~ c~ WEIGHTED<=7 @4
// Process: u~ c~ > e+ e- g u~ c~ WEIGHTED<=7 @4
// Process: u~ c~ > mu+ mu- g u~ c~ WEIGHTED<=7 @4
// Process: u~ d~ > e+ e- g u~ d~ WEIGHTED<=7 @4
// Process: u~ d~ > mu+ mu- g u~ d~ WEIGHTED<=7 @4
// Process: u~ s~ > e+ e- g u~ s~ WEIGHTED<=7 @4
// Process: u~ s~ > mu+ mu- g u~ s~ WEIGHTED<=7 @4
// Process: c~ d~ > e+ e- g c~ d~ WEIGHTED<=7 @4
// Process: c~ d~ > mu+ mu- g c~ d~ WEIGHTED<=7 @4
// Process: c~ s~ > e+ e- g c~ s~ WEIGHTED<=7 @4
// Process: c~ s~ > mu+ mu- g c~ s~ WEIGHTED<=7 @4
// Process: d~ d~ > ve~ ve g d~ d~ WEIGHTED<=7 @4
// Process: d~ d~ > vm~ vm g d~ d~ WEIGHTED<=7 @4
// Process: d~ d~ > vt~ vt g d~ d~ WEIGHTED<=7 @4
// Process: s~ s~ > ve~ ve g s~ s~ WEIGHTED<=7 @4
// Process: s~ s~ > vm~ vm g s~ s~ WEIGHTED<=7 @4
// Process: s~ s~ > vt~ vt g s~ s~ WEIGHTED<=7 @4
// Process: d~ s~ > e+ e- g d~ s~ WEIGHTED<=7 @4
// Process: d~ s~ > mu+ mu- g d~ s~ WEIGHTED<=7 @4
// Process: u u > e+ ve g u d WEIGHTED<=7 @4
// Process: u u > mu+ vm g u d WEIGHTED<=7 @4
// Process: c c > e+ ve g c s WEIGHTED<=7 @4
// Process: c c > mu+ vm g c s WEIGHTED<=7 @4
// Process: u c > ve~ ve g u c WEIGHTED<=7 @4
// Process: u c > vm~ vm g u c WEIGHTED<=7 @4
// Process: u c > vt~ vt g u c WEIGHTED<=7 @4
// Process: u d > e+ ve g d d WEIGHTED<=7 @4
// Process: u d > mu+ vm g d d WEIGHTED<=7 @4
// Process: c s > e+ ve g s s WEIGHTED<=7 @4
// Process: c s > mu+ vm g s s WEIGHTED<=7 @4
// Process: u d > ve~ e- g u u WEIGHTED<=7 @4
// Process: u d > vm~ mu- g u u WEIGHTED<=7 @4
// Process: c s > ve~ e- g c c WEIGHTED<=7 @4
// Process: c s > vm~ mu- g c c WEIGHTED<=7 @4
// Process: u d > ve~ ve g u d WEIGHTED<=7 @4
// Process: u d > vm~ vm g u d WEIGHTED<=7 @4
// Process: u d > vt~ vt g u d WEIGHTED<=7 @4
// Process: u s > ve~ ve g u s WEIGHTED<=7 @4
// Process: u s > vm~ vm g u s WEIGHTED<=7 @4
// Process: u s > vt~ vt g u s WEIGHTED<=7 @4
// Process: c d > ve~ ve g c d WEIGHTED<=7 @4
// Process: c d > vm~ vm g c d WEIGHTED<=7 @4
// Process: c d > vt~ vt g c d WEIGHTED<=7 @4
// Process: c s > ve~ ve g c s WEIGHTED<=7 @4
// Process: c s > vm~ vm g c s WEIGHTED<=7 @4
// Process: c s > vt~ vt g c s WEIGHTED<=7 @4
// Process: u u~ > e+ ve g d u~ WEIGHTED<=7 @4
// Process: u u~ > mu+ vm g d u~ WEIGHTED<=7 @4
// Process: c c~ > e+ ve g s c~ WEIGHTED<=7 @4
// Process: c c~ > mu+ vm g s c~ WEIGHTED<=7 @4
// Process: u u~ > ve~ e- g u d~ WEIGHTED<=7 @4
// Process: u u~ > vm~ mu- g u d~ WEIGHTED<=7 @4
// Process: c c~ > ve~ e- g c s~ WEIGHTED<=7 @4
// Process: c c~ > vm~ mu- g c s~ WEIGHTED<=7 @4
// Process: u u~ > ve~ ve g c c~ WEIGHTED<=7 @4
// Process: u u~ > vm~ vm g c c~ WEIGHTED<=7 @4
// Process: u u~ > vt~ vt g c c~ WEIGHTED<=7 @4
// Process: c c~ > ve~ ve g u u~ WEIGHTED<=7 @4
// Process: c c~ > vm~ vm g u u~ WEIGHTED<=7 @4
// Process: c c~ > vt~ vt g u u~ WEIGHTED<=7 @4
// Process: u u~ > ve~ ve g d d~ WEIGHTED<=7 @4
// Process: u u~ > ve~ ve g s s~ WEIGHTED<=7 @4
// Process: u u~ > vm~ vm g d d~ WEIGHTED<=7 @4
// Process: u u~ > vm~ vm g s s~ WEIGHTED<=7 @4
// Process: u u~ > vt~ vt g d d~ WEIGHTED<=7 @4
// Process: u u~ > vt~ vt g s s~ WEIGHTED<=7 @4
// Process: c c~ > ve~ ve g d d~ WEIGHTED<=7 @4
// Process: c c~ > ve~ ve g s s~ WEIGHTED<=7 @4
// Process: c c~ > vm~ vm g d d~ WEIGHTED<=7 @4
// Process: c c~ > vm~ vm g s s~ WEIGHTED<=7 @4
// Process: c c~ > vt~ vt g d d~ WEIGHTED<=7 @4
// Process: c c~ > vt~ vt g s s~ WEIGHTED<=7 @4
// Process: u c~ > ve~ ve g u c~ WEIGHTED<=7 @4
// Process: u c~ > vm~ vm g u c~ WEIGHTED<=7 @4
// Process: u c~ > vt~ vt g u c~ WEIGHTED<=7 @4
// Process: c u~ > ve~ ve g c u~ WEIGHTED<=7 @4
// Process: c u~ > vm~ vm g c u~ WEIGHTED<=7 @4
// Process: c u~ > vt~ vt g c u~ WEIGHTED<=7 @4
// Process: u d~ > e+ ve g u u~ WEIGHTED<=7 @4
// Process: u d~ > mu+ vm g u u~ WEIGHTED<=7 @4
// Process: c s~ > e+ ve g c c~ WEIGHTED<=7 @4
// Process: c s~ > mu+ vm g c c~ WEIGHTED<=7 @4
// Process: u d~ > e+ ve g d d~ WEIGHTED<=7 @4
// Process: u d~ > mu+ vm g d d~ WEIGHTED<=7 @4
// Process: c s~ > e+ ve g s s~ WEIGHTED<=7 @4
// Process: c s~ > mu+ vm g s s~ WEIGHTED<=7 @4
// Process: u d~ > ve~ ve g u d~ WEIGHTED<=7 @4
// Process: u d~ > vm~ vm g u d~ WEIGHTED<=7 @4
// Process: u d~ > vt~ vt g u d~ WEIGHTED<=7 @4
// Process: u s~ > ve~ ve g u s~ WEIGHTED<=7 @4
// Process: u s~ > vm~ vm g u s~ WEIGHTED<=7 @4
// Process: u s~ > vt~ vt g u s~ WEIGHTED<=7 @4
// Process: c d~ > ve~ ve g c d~ WEIGHTED<=7 @4
// Process: c d~ > vm~ vm g c d~ WEIGHTED<=7 @4
// Process: c d~ > vt~ vt g c d~ WEIGHTED<=7 @4
// Process: c s~ > ve~ ve g c s~ WEIGHTED<=7 @4
// Process: c s~ > vm~ vm g c s~ WEIGHTED<=7 @4
// Process: c s~ > vt~ vt g c s~ WEIGHTED<=7 @4
// Process: d d > ve~ e- g u d WEIGHTED<=7 @4
// Process: d d > vm~ mu- g u d WEIGHTED<=7 @4
// Process: s s > ve~ e- g c s WEIGHTED<=7 @4
// Process: s s > vm~ mu- g c s WEIGHTED<=7 @4
// Process: d s > ve~ ve g d s WEIGHTED<=7 @4
// Process: d s > vm~ vm g d s WEIGHTED<=7 @4
// Process: d s > vt~ vt g d s WEIGHTED<=7 @4
// Process: d u~ > ve~ e- g u u~ WEIGHTED<=7 @4
// Process: d u~ > vm~ mu- g u u~ WEIGHTED<=7 @4
// Process: s c~ > ve~ e- g c c~ WEIGHTED<=7 @4
// Process: s c~ > vm~ mu- g c c~ WEIGHTED<=7 @4
// Process: d u~ > ve~ e- g d d~ WEIGHTED<=7 @4
// Process: d u~ > vm~ mu- g d d~ WEIGHTED<=7 @4
// Process: s c~ > ve~ e- g s s~ WEIGHTED<=7 @4
// Process: s c~ > vm~ mu- g s s~ WEIGHTED<=7 @4
// Process: d u~ > ve~ ve g d u~ WEIGHTED<=7 @4
// Process: d u~ > vm~ vm g d u~ WEIGHTED<=7 @4
// Process: d u~ > vt~ vt g d u~ WEIGHTED<=7 @4
// Process: d c~ > ve~ ve g d c~ WEIGHTED<=7 @4
// Process: d c~ > vm~ vm g d c~ WEIGHTED<=7 @4
// Process: d c~ > vt~ vt g d c~ WEIGHTED<=7 @4
// Process: s u~ > ve~ ve g s u~ WEIGHTED<=7 @4
// Process: s u~ > vm~ vm g s u~ WEIGHTED<=7 @4
// Process: s u~ > vt~ vt g s u~ WEIGHTED<=7 @4
// Process: s c~ > ve~ ve g s c~ WEIGHTED<=7 @4
// Process: s c~ > vm~ vm g s c~ WEIGHTED<=7 @4
// Process: s c~ > vt~ vt g s c~ WEIGHTED<=7 @4
// Process: d d~ > e+ ve g d u~ WEIGHTED<=7 @4
// Process: d d~ > mu+ vm g d u~ WEIGHTED<=7 @4
// Process: s s~ > e+ ve g s c~ WEIGHTED<=7 @4
// Process: s s~ > mu+ vm g s c~ WEIGHTED<=7 @4
// Process: d d~ > ve~ e- g u d~ WEIGHTED<=7 @4
// Process: d d~ > vm~ mu- g u d~ WEIGHTED<=7 @4
// Process: s s~ > ve~ e- g c s~ WEIGHTED<=7 @4
// Process: s s~ > vm~ mu- g c s~ WEIGHTED<=7 @4
// Process: d d~ > ve~ ve g u u~ WEIGHTED<=7 @4
// Process: d d~ > ve~ ve g c c~ WEIGHTED<=7 @4
// Process: d d~ > vm~ vm g u u~ WEIGHTED<=7 @4
// Process: d d~ > vm~ vm g c c~ WEIGHTED<=7 @4
// Process: d d~ > vt~ vt g u u~ WEIGHTED<=7 @4
// Process: d d~ > vt~ vt g c c~ WEIGHTED<=7 @4
// Process: s s~ > ve~ ve g u u~ WEIGHTED<=7 @4
// Process: s s~ > ve~ ve g c c~ WEIGHTED<=7 @4
// Process: s s~ > vm~ vm g u u~ WEIGHTED<=7 @4
// Process: s s~ > vm~ vm g c c~ WEIGHTED<=7 @4
// Process: s s~ > vt~ vt g u u~ WEIGHTED<=7 @4
// Process: s s~ > vt~ vt g c c~ WEIGHTED<=7 @4
// Process: d d~ > ve~ ve g s s~ WEIGHTED<=7 @4
// Process: d d~ > vm~ vm g s s~ WEIGHTED<=7 @4
// Process: d d~ > vt~ vt g s s~ WEIGHTED<=7 @4
// Process: s s~ > ve~ ve g d d~ WEIGHTED<=7 @4
// Process: s s~ > vm~ vm g d d~ WEIGHTED<=7 @4
// Process: s s~ > vt~ vt g d d~ WEIGHTED<=7 @4
// Process: d s~ > ve~ ve g d s~ WEIGHTED<=7 @4
// Process: d s~ > vm~ vm g d s~ WEIGHTED<=7 @4
// Process: d s~ > vt~ vt g d s~ WEIGHTED<=7 @4
// Process: s d~ > ve~ ve g s d~ WEIGHTED<=7 @4
// Process: s d~ > vm~ vm g s d~ WEIGHTED<=7 @4
// Process: s d~ > vt~ vt g s d~ WEIGHTED<=7 @4
// Process: u~ u~ > ve~ e- g u~ d~ WEIGHTED<=7 @4
// Process: u~ u~ > vm~ mu- g u~ d~ WEIGHTED<=7 @4
// Process: c~ c~ > ve~ e- g c~ s~ WEIGHTED<=7 @4
// Process: c~ c~ > vm~ mu- g c~ s~ WEIGHTED<=7 @4
// Process: u~ c~ > ve~ ve g u~ c~ WEIGHTED<=7 @4
// Process: u~ c~ > vm~ vm g u~ c~ WEIGHTED<=7 @4
// Process: u~ c~ > vt~ vt g u~ c~ WEIGHTED<=7 @4
// Process: u~ d~ > e+ ve g u~ u~ WEIGHTED<=7 @4
// Process: u~ d~ > mu+ vm g u~ u~ WEIGHTED<=7 @4
// Process: c~ s~ > e+ ve g c~ c~ WEIGHTED<=7 @4
// Process: c~ s~ > mu+ vm g c~ c~ WEIGHTED<=7 @4
// Process: u~ d~ > ve~ e- g d~ d~ WEIGHTED<=7 @4
// Process: u~ d~ > vm~ mu- g d~ d~ WEIGHTED<=7 @4
// Process: c~ s~ > ve~ e- g s~ s~ WEIGHTED<=7 @4
// Process: c~ s~ > vm~ mu- g s~ s~ WEIGHTED<=7 @4
// Process: u~ d~ > ve~ ve g u~ d~ WEIGHTED<=7 @4
// Process: u~ d~ > vm~ vm g u~ d~ WEIGHTED<=7 @4
// Process: u~ d~ > vt~ vt g u~ d~ WEIGHTED<=7 @4
// Process: u~ s~ > ve~ ve g u~ s~ WEIGHTED<=7 @4
// Process: u~ s~ > vm~ vm g u~ s~ WEIGHTED<=7 @4
// Process: u~ s~ > vt~ vt g u~ s~ WEIGHTED<=7 @4
// Process: c~ d~ > ve~ ve g c~ d~ WEIGHTED<=7 @4
// Process: c~ d~ > vm~ vm g c~ d~ WEIGHTED<=7 @4
// Process: c~ d~ > vt~ vt g c~ d~ WEIGHTED<=7 @4
// Process: c~ s~ > ve~ ve g c~ s~ WEIGHTED<=7 @4
// Process: c~ s~ > vm~ vm g c~ s~ WEIGHTED<=7 @4
// Process: c~ s~ > vt~ vt g c~ s~ WEIGHTED<=7 @4
// Process: d~ d~ > e+ ve g u~ d~ WEIGHTED<=7 @4
// Process: d~ d~ > mu+ vm g u~ d~ WEIGHTED<=7 @4
// Process: s~ s~ > e+ ve g c~ s~ WEIGHTED<=7 @4
// Process: s~ s~ > mu+ vm g c~ s~ WEIGHTED<=7 @4
// Process: d~ s~ > ve~ ve g d~ s~ WEIGHTED<=7 @4
// Process: d~ s~ > vm~ vm g d~ s~ WEIGHTED<=7 @4
// Process: d~ s~ > vt~ vt g d~ s~ WEIGHTED<=7 @4
// Process: u c > e+ ve g u s WEIGHTED<=7 @4
// Process: u c > mu+ vm g u s WEIGHTED<=7 @4
// Process: u c > e+ ve g c d WEIGHTED<=7 @4
// Process: u c > mu+ vm g c d WEIGHTED<=7 @4
// Process: u s > e+ ve g s d WEIGHTED<=7 @4
// Process: u s > mu+ vm g s d WEIGHTED<=7 @4
// Process: c d > e+ ve g d s WEIGHTED<=7 @4
// Process: c d > mu+ vm g d s WEIGHTED<=7 @4
// Process: u s > ve~ e- g u c WEIGHTED<=7 @4
// Process: u s > vm~ mu- g u c WEIGHTED<=7 @4
// Process: c d > ve~ e- g c u WEIGHTED<=7 @4
// Process: c d > vm~ mu- g c u WEIGHTED<=7 @4
// Process: d s > ve~ e- g d c WEIGHTED<=7 @4
// Process: d s > vm~ mu- g d c WEIGHTED<=7 @4
// Process: u u~ > e+ ve g s c~ WEIGHTED<=7 @4
// Process: u u~ > mu+ vm g s c~ WEIGHTED<=7 @4
// Process: c c~ > e+ ve g d u~ WEIGHTED<=7 @4
// Process: c c~ > mu+ vm g d u~ WEIGHTED<=7 @4
// Process: d d~ > e+ ve g s c~ WEIGHTED<=7 @4
// Process: d d~ > mu+ vm g s c~ WEIGHTED<=7 @4
// Process: s s~ > e+ ve g d u~ WEIGHTED<=7 @4
// Process: s s~ > mu+ vm g d u~ WEIGHTED<=7 @4
// Process: u u~ > ve~ e- g c s~ WEIGHTED<=7 @4
// Process: u u~ > vm~ mu- g c s~ WEIGHTED<=7 @4
// Process: c c~ > ve~ e- g u d~ WEIGHTED<=7 @4
// Process: c c~ > vm~ mu- g u d~ WEIGHTED<=7 @4
// Process: d d~ > ve~ e- g c s~ WEIGHTED<=7 @4
// Process: d d~ > vm~ mu- g c s~ WEIGHTED<=7 @4
// Process: s s~ > ve~ e- g u d~ WEIGHTED<=7 @4
// Process: s s~ > vm~ mu- g u d~ WEIGHTED<=7 @4
// Process: u c~ > e+ ve g d c~ WEIGHTED<=7 @4
// Process: u c~ > mu+ vm g d c~ WEIGHTED<=7 @4
// Process: u s~ > e+ ve g d s~ WEIGHTED<=7 @4
// Process: u s~ > mu+ vm g d s~ WEIGHTED<=7 @4
// Process: c u~ > e+ ve g s u~ WEIGHTED<=7 @4
// Process: c u~ > mu+ vm g s u~ WEIGHTED<=7 @4
// Process: c d~ > e+ ve g s d~ WEIGHTED<=7 @4
// Process: c d~ > mu+ vm g s d~ WEIGHTED<=7 @4
// Process: u c~ > ve~ e- g u s~ WEIGHTED<=7 @4
// Process: u c~ > vm~ mu- g u s~ WEIGHTED<=7 @4
// Process: c u~ > ve~ e- g c d~ WEIGHTED<=7 @4
// Process: c u~ > vm~ mu- g c d~ WEIGHTED<=7 @4
// Process: d c~ > ve~ e- g d s~ WEIGHTED<=7 @4
// Process: d c~ > vm~ mu- g d s~ WEIGHTED<=7 @4
// Process: s u~ > ve~ e- g s d~ WEIGHTED<=7 @4
// Process: s u~ > vm~ mu- g s d~ WEIGHTED<=7 @4
// Process: u d~ > e+ ve g c c~ WEIGHTED<=7 @4
// Process: u d~ > e+ ve g s s~ WEIGHTED<=7 @4
// Process: u d~ > mu+ vm g c c~ WEIGHTED<=7 @4
// Process: u d~ > mu+ vm g s s~ WEIGHTED<=7 @4
// Process: c s~ > e+ ve g u u~ WEIGHTED<=7 @4
// Process: c s~ > e+ ve g d d~ WEIGHTED<=7 @4
// Process: c s~ > mu+ vm g u u~ WEIGHTED<=7 @4
// Process: c s~ > mu+ vm g d d~ WEIGHTED<=7 @4
// Process: u s~ > e+ ve g u c~ WEIGHTED<=7 @4
// Process: u s~ > mu+ vm g u c~ WEIGHTED<=7 @4
// Process: c d~ > e+ ve g c u~ WEIGHTED<=7 @4
// Process: c d~ > mu+ vm g c u~ WEIGHTED<=7 @4
// Process: d s~ > e+ ve g d c~ WEIGHTED<=7 @4
// Process: d s~ > mu+ vm g d c~ WEIGHTED<=7 @4
// Process: s d~ > e+ ve g s u~ WEIGHTED<=7 @4
// Process: s d~ > mu+ vm g s u~ WEIGHTED<=7 @4
// Process: d s > ve~ e- g u s WEIGHTED<=7 @4
// Process: d s > vm~ mu- g u s WEIGHTED<=7 @4
// Process: d u~ > ve~ e- g c c~ WEIGHTED<=7 @4
// Process: d u~ > ve~ e- g s s~ WEIGHTED<=7 @4
// Process: d u~ > vm~ mu- g c c~ WEIGHTED<=7 @4
// Process: d u~ > vm~ mu- g s s~ WEIGHTED<=7 @4
// Process: s c~ > ve~ e- g u u~ WEIGHTED<=7 @4
// Process: s c~ > ve~ e- g d d~ WEIGHTED<=7 @4
// Process: s c~ > vm~ mu- g u u~ WEIGHTED<=7 @4
// Process: s c~ > vm~ mu- g d d~ WEIGHTED<=7 @4
// Process: d c~ > ve~ e- g u c~ WEIGHTED<=7 @4
// Process: d c~ > vm~ mu- g u c~ WEIGHTED<=7 @4
// Process: d s~ > ve~ e- g u s~ WEIGHTED<=7 @4
// Process: d s~ > vm~ mu- g u s~ WEIGHTED<=7 @4
// Process: s u~ > ve~ e- g c u~ WEIGHTED<=7 @4
// Process: s u~ > vm~ mu- g c u~ WEIGHTED<=7 @4
// Process: s d~ > ve~ e- g c d~ WEIGHTED<=7 @4
// Process: s d~ > vm~ mu- g c d~ WEIGHTED<=7 @4
// Process: u~ c~ > ve~ e- g u~ s~ WEIGHTED<=7 @4
// Process: u~ c~ > vm~ mu- g u~ s~ WEIGHTED<=7 @4
// Process: u~ c~ > ve~ e- g c~ d~ WEIGHTED<=7 @4
// Process: u~ c~ > vm~ mu- g c~ d~ WEIGHTED<=7 @4
// Process: u~ s~ > ve~ e- g s~ d~ WEIGHTED<=7 @4
// Process: u~ s~ > vm~ mu- g s~ d~ WEIGHTED<=7 @4
// Process: c~ d~ > ve~ e- g d~ s~ WEIGHTED<=7 @4
// Process: c~ d~ > vm~ mu- g d~ s~ WEIGHTED<=7 @4
// Process: u~ s~ > e+ ve g u~ c~ WEIGHTED<=7 @4
// Process: u~ s~ > mu+ vm g u~ c~ WEIGHTED<=7 @4
// Process: c~ d~ > e+ ve g c~ u~ WEIGHTED<=7 @4
// Process: c~ d~ > mu+ vm g c~ u~ WEIGHTED<=7 @4
// Process: d~ s~ > e+ ve g d~ c~ WEIGHTED<=7 @4
// Process: d~ s~ > mu+ vm g d~ c~ WEIGHTED<=7 @4
// Process: d~ s~ > e+ ve g u~ s~ WEIGHTED<=7 @4
// Process: d~ s~ > mu+ vm g u~ s~ WEIGHTED<=7 @4

// Exception class
class PY8MEs_R4_P76_sm_qq_llgqqException : public exception
{
  virtual const char * what() const throw()
  {
    return "Exception in class 'PY8MEs_R4_P76_sm_qq_llgqq'."; 
  }
}
PY8MEs_R4_P76_sm_qq_llgqq_exception; 

std::set<int> PY8MEs_R4_P76_sm_qq_llgqq::s_channel_proc = std::set<int>
    (createset<int> ());

int PY8MEs_R4_P76_sm_qq_llgqq::helicities[ncomb][nexternal] = {{-1, -1, -1, -1,
    -1, -1, -1}, {-1, -1, -1, -1, -1, -1, 1}, {-1, -1, -1, -1, -1, 1, -1}, {-1,
    -1, -1, -1, -1, 1, 1}, {-1, -1, -1, -1, 1, -1, -1}, {-1, -1, -1, -1, 1, -1,
    1}, {-1, -1, -1, -1, 1, 1, -1}, {-1, -1, -1, -1, 1, 1, 1}, {-1, -1, -1, 1,
    -1, -1, -1}, {-1, -1, -1, 1, -1, -1, 1}, {-1, -1, -1, 1, -1, 1, -1}, {-1,
    -1, -1, 1, -1, 1, 1}, {-1, -1, -1, 1, 1, -1, -1}, {-1, -1, -1, 1, 1, -1,
    1}, {-1, -1, -1, 1, 1, 1, -1}, {-1, -1, -1, 1, 1, 1, 1}, {-1, -1, 1, -1,
    -1, -1, -1}, {-1, -1, 1, -1, -1, -1, 1}, {-1, -1, 1, -1, -1, 1, -1}, {-1,
    -1, 1, -1, -1, 1, 1}, {-1, -1, 1, -1, 1, -1, -1}, {-1, -1, 1, -1, 1, -1,
    1}, {-1, -1, 1, -1, 1, 1, -1}, {-1, -1, 1, -1, 1, 1, 1}, {-1, -1, 1, 1, -1,
    -1, -1}, {-1, -1, 1, 1, -1, -1, 1}, {-1, -1, 1, 1, -1, 1, -1}, {-1, -1, 1,
    1, -1, 1, 1}, {-1, -1, 1, 1, 1, -1, -1}, {-1, -1, 1, 1, 1, -1, 1}, {-1, -1,
    1, 1, 1, 1, -1}, {-1, -1, 1, 1, 1, 1, 1}, {-1, 1, -1, -1, -1, -1, -1}, {-1,
    1, -1, -1, -1, -1, 1}, {-1, 1, -1, -1, -1, 1, -1}, {-1, 1, -1, -1, -1, 1,
    1}, {-1, 1, -1, -1, 1, -1, -1}, {-1, 1, -1, -1, 1, -1, 1}, {-1, 1, -1, -1,
    1, 1, -1}, {-1, 1, -1, -1, 1, 1, 1}, {-1, 1, -1, 1, -1, -1, -1}, {-1, 1,
    -1, 1, -1, -1, 1}, {-1, 1, -1, 1, -1, 1, -1}, {-1, 1, -1, 1, -1, 1, 1},
    {-1, 1, -1, 1, 1, -1, -1}, {-1, 1, -1, 1, 1, -1, 1}, {-1, 1, -1, 1, 1, 1,
    -1}, {-1, 1, -1, 1, 1, 1, 1}, {-1, 1, 1, -1, -1, -1, -1}, {-1, 1, 1, -1,
    -1, -1, 1}, {-1, 1, 1, -1, -1, 1, -1}, {-1, 1, 1, -1, -1, 1, 1}, {-1, 1, 1,
    -1, 1, -1, -1}, {-1, 1, 1, -1, 1, -1, 1}, {-1, 1, 1, -1, 1, 1, -1}, {-1, 1,
    1, -1, 1, 1, 1}, {-1, 1, 1, 1, -1, -1, -1}, {-1, 1, 1, 1, -1, -1, 1}, {-1,
    1, 1, 1, -1, 1, -1}, {-1, 1, 1, 1, -1, 1, 1}, {-1, 1, 1, 1, 1, -1, -1},
    {-1, 1, 1, 1, 1, -1, 1}, {-1, 1, 1, 1, 1, 1, -1}, {-1, 1, 1, 1, 1, 1, 1},
    {1, -1, -1, -1, -1, -1, -1}, {1, -1, -1, -1, -1, -1, 1}, {1, -1, -1, -1,
    -1, 1, -1}, {1, -1, -1, -1, -1, 1, 1}, {1, -1, -1, -1, 1, -1, -1}, {1, -1,
    -1, -1, 1, -1, 1}, {1, -1, -1, -1, 1, 1, -1}, {1, -1, -1, -1, 1, 1, 1}, {1,
    -1, -1, 1, -1, -1, -1}, {1, -1, -1, 1, -1, -1, 1}, {1, -1, -1, 1, -1, 1,
    -1}, {1, -1, -1, 1, -1, 1, 1}, {1, -1, -1, 1, 1, -1, -1}, {1, -1, -1, 1, 1,
    -1, 1}, {1, -1, -1, 1, 1, 1, -1}, {1, -1, -1, 1, 1, 1, 1}, {1, -1, 1, -1,
    -1, -1, -1}, {1, -1, 1, -1, -1, -1, 1}, {1, -1, 1, -1, -1, 1, -1}, {1, -1,
    1, -1, -1, 1, 1}, {1, -1, 1, -1, 1, -1, -1}, {1, -1, 1, -1, 1, -1, 1}, {1,
    -1, 1, -1, 1, 1, -1}, {1, -1, 1, -1, 1, 1, 1}, {1, -1, 1, 1, -1, -1, -1},
    {1, -1, 1, 1, -1, -1, 1}, {1, -1, 1, 1, -1, 1, -1}, {1, -1, 1, 1, -1, 1,
    1}, {1, -1, 1, 1, 1, -1, -1}, {1, -1, 1, 1, 1, -1, 1}, {1, -1, 1, 1, 1, 1,
    -1}, {1, -1, 1, 1, 1, 1, 1}, {1, 1, -1, -1, -1, -1, -1}, {1, 1, -1, -1, -1,
    -1, 1}, {1, 1, -1, -1, -1, 1, -1}, {1, 1, -1, -1, -1, 1, 1}, {1, 1, -1, -1,
    1, -1, -1}, {1, 1, -1, -1, 1, -1, 1}, {1, 1, -1, -1, 1, 1, -1}, {1, 1, -1,
    -1, 1, 1, 1}, {1, 1, -1, 1, -1, -1, -1}, {1, 1, -1, 1, -1, -1, 1}, {1, 1,
    -1, 1, -1, 1, -1}, {1, 1, -1, 1, -1, 1, 1}, {1, 1, -1, 1, 1, -1, -1}, {1,
    1, -1, 1, 1, -1, 1}, {1, 1, -1, 1, 1, 1, -1}, {1, 1, -1, 1, 1, 1, 1}, {1,
    1, 1, -1, -1, -1, -1}, {1, 1, 1, -1, -1, -1, 1}, {1, 1, 1, -1, -1, 1, -1},
    {1, 1, 1, -1, -1, 1, 1}, {1, 1, 1, -1, 1, -1, -1}, {1, 1, 1, -1, 1, -1, 1},
    {1, 1, 1, -1, 1, 1, -1}, {1, 1, 1, -1, 1, 1, 1}, {1, 1, 1, 1, -1, -1, -1},
    {1, 1, 1, 1, -1, -1, 1}, {1, 1, 1, 1, -1, 1, -1}, {1, 1, 1, 1, -1, 1, 1},
    {1, 1, 1, 1, 1, -1, -1}, {1, 1, 1, 1, 1, -1, 1}, {1, 1, 1, 1, 1, 1, -1},
    {1, 1, 1, 1, 1, 1, 1}};

// Normalization factors the various processes
// Denominators: spins, colors and identical particles
int PY8MEs_R4_P76_sm_qq_llgqq::denom_colors[nprocesses] = {9, 9, 9, 9, 9, 9, 9,
    9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
    9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
    9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9};
int PY8MEs_R4_P76_sm_qq_llgqq::denom_hels[nprocesses] = {4, 4, 4, 4, 4, 4, 4,
    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4};
int PY8MEs_R4_P76_sm_qq_llgqq::denom_iden[nprocesses] = {2, 1, 2, 1, 2, 2, 2,
    1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 2, 1, 1, 2, 1, 1, 1, 2, 2, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};

//--------------------------------------------------------------------------
// Color config initialization
void PY8MEs_R4_P76_sm_qq_llgqq::initColorConfigs() 
{
  color_configs = vector < vec_vec_int > (); 
  jamp_nc_relative_power = vector < vec_int > (); 

  // Color flows of process Process: u u > e+ e- g u u WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #1
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #2
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #3
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 

  // Color flows of process Process: u u~ > e+ e- g u u~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #1
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #2
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #3
  color_configs[1].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[1].push_back(0); 

  // Color flows of process Process: d d > e+ e- g d d WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[2].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #1
  color_configs[2].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #2
  color_configs[2].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #3
  color_configs[2].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[2].push_back(0); 

  // Color flows of process Process: d d~ > e+ e- g d d~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[3].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #1
  color_configs[3].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #2
  color_configs[3].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #3
  color_configs[3].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[3].push_back(0); 

  // Color flows of process Process: u~ u~ > e+ e- g u~ u~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[4].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[4].push_back(0); 
  // JAMP #1
  color_configs[4].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[4].push_back(0); 
  // JAMP #2
  color_configs[4].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[4].push_back(0); 
  // JAMP #3
  color_configs[4].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[4].push_back(0); 

  // Color flows of process Process: d~ d~ > e+ e- g d~ d~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[5].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[5].push_back(0); 
  // JAMP #1
  color_configs[5].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[5].push_back(0); 
  // JAMP #2
  color_configs[5].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[5].push_back(0); 
  // JAMP #3
  color_configs[5].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[5].push_back(0); 

  // Color flows of process Process: u u > ve~ ve g u u WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[6].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[6].push_back(0); 
  // JAMP #1
  color_configs[6].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[6].push_back(0); 
  // JAMP #2
  color_configs[6].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[6].push_back(0); 
  // JAMP #3
  color_configs[6].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[6].push_back(0); 

  // Color flows of process Process: u c > e+ e- g u c WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[7].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[7].push_back(-1); 
  // JAMP #1
  color_configs[7].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[7].push_back(0); 
  // JAMP #2
  color_configs[7].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[7].push_back(0); 
  // JAMP #3
  color_configs[7].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[7].push_back(-1); 

  // Color flows of process Process: u d > e+ e- g u d WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[8].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[8].push_back(-1); 
  // JAMP #1
  color_configs[8].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[8].push_back(0); 
  // JAMP #2
  color_configs[8].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[8].push_back(0); 
  // JAMP #3
  color_configs[8].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[8].push_back(-1); 

  // Color flows of process Process: u u~ > e+ e- g c c~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[9].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[9].push_back(-1); 
  // JAMP #1
  color_configs[9].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[9].push_back(0); 
  // JAMP #2
  color_configs[9].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[9].push_back(-1); 
  // JAMP #3
  color_configs[9].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[9].push_back(0); 

  // Color flows of process Process: u u~ > e+ e- g d d~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[10].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[10].push_back(-1); 
  // JAMP #1
  color_configs[10].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[10].push_back(0); 
  // JAMP #2
  color_configs[10].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[10].push_back(-1); 
  // JAMP #3
  color_configs[10].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[10].push_back(0); 

  // Color flows of process Process: u u~ > ve~ ve g u u~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[11].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[11].push_back(0); 
  // JAMP #1
  color_configs[11].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[11].push_back(0); 
  // JAMP #2
  color_configs[11].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[11].push_back(0); 
  // JAMP #3
  color_configs[11].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[11].push_back(0); 

  // Color flows of process Process: u c~ > e+ e- g u c~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[12].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[12].push_back(0); 
  // JAMP #1
  color_configs[12].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[12].push_back(-1); 
  // JAMP #2
  color_configs[12].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[12].push_back(0); 
  // JAMP #3
  color_configs[12].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[12].push_back(-1); 

  // Color flows of process Process: u d~ > e+ e- g u d~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[13].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[13].push_back(0); 
  // JAMP #1
  color_configs[13].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[13].push_back(-1); 
  // JAMP #2
  color_configs[13].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[13].push_back(0); 
  // JAMP #3
  color_configs[13].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[13].push_back(-1); 

  // Color flows of process Process: d d > ve~ ve g d d WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[14].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[14].push_back(0); 
  // JAMP #1
  color_configs[14].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[14].push_back(0); 
  // JAMP #2
  color_configs[14].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[14].push_back(0); 
  // JAMP #3
  color_configs[14].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[14].push_back(0); 

  // Color flows of process Process: d s > e+ e- g d s WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[15].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[15].push_back(-1); 
  // JAMP #1
  color_configs[15].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[15].push_back(0); 
  // JAMP #2
  color_configs[15].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[15].push_back(0); 
  // JAMP #3
  color_configs[15].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[15].push_back(-1); 

  // Color flows of process Process: d u~ > e+ e- g d u~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[16].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[16].push_back(0); 
  // JAMP #1
  color_configs[16].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[16].push_back(-1); 
  // JAMP #2
  color_configs[16].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[16].push_back(0); 
  // JAMP #3
  color_configs[16].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[16].push_back(-1); 

  // Color flows of process Process: d d~ > e+ e- g u u~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[17].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[17].push_back(-1); 
  // JAMP #1
  color_configs[17].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[17].push_back(0); 
  // JAMP #2
  color_configs[17].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[17].push_back(-1); 
  // JAMP #3
  color_configs[17].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[17].push_back(0); 

  // Color flows of process Process: d d~ > e+ e- g s s~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[18].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[18].push_back(-1); 
  // JAMP #1
  color_configs[18].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[18].push_back(0); 
  // JAMP #2
  color_configs[18].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[18].push_back(-1); 
  // JAMP #3
  color_configs[18].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[18].push_back(0); 

  // Color flows of process Process: d d~ > ve~ ve g d d~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[19].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[19].push_back(0); 
  // JAMP #1
  color_configs[19].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[19].push_back(0); 
  // JAMP #2
  color_configs[19].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[19].push_back(0); 
  // JAMP #3
  color_configs[19].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[19].push_back(0); 

  // Color flows of process Process: d s~ > e+ e- g d s~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[20].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[20].push_back(0); 
  // JAMP #1
  color_configs[20].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[20].push_back(-1); 
  // JAMP #2
  color_configs[20].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[20].push_back(0); 
  // JAMP #3
  color_configs[20].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[20].push_back(-1); 

  // Color flows of process Process: u~ u~ > ve~ ve g u~ u~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[21].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[21].push_back(0); 
  // JAMP #1
  color_configs[21].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[21].push_back(0); 
  // JAMP #2
  color_configs[21].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[21].push_back(0); 
  // JAMP #3
  color_configs[21].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[21].push_back(0); 

  // Color flows of process Process: u~ c~ > e+ e- g u~ c~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[22].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[22].push_back(-1); 
  // JAMP #1
  color_configs[22].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[22].push_back(0); 
  // JAMP #2
  color_configs[22].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[22].push_back(0); 
  // JAMP #3
  color_configs[22].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[22].push_back(-1); 

  // Color flows of process Process: u~ d~ > e+ e- g u~ d~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[23].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[23].push_back(-1); 
  // JAMP #1
  color_configs[23].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[23].push_back(0); 
  // JAMP #2
  color_configs[23].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[23].push_back(0); 
  // JAMP #3
  color_configs[23].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[23].push_back(-1); 

  // Color flows of process Process: d~ d~ > ve~ ve g d~ d~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[24].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[24].push_back(0); 
  // JAMP #1
  color_configs[24].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[24].push_back(0); 
  // JAMP #2
  color_configs[24].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[24].push_back(0); 
  // JAMP #3
  color_configs[24].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[24].push_back(0); 

  // Color flows of process Process: d~ s~ > e+ e- g d~ s~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[25].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[25].push_back(-1); 
  // JAMP #1
  color_configs[25].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[25].push_back(0); 
  // JAMP #2
  color_configs[25].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[25].push_back(0); 
  // JAMP #3
  color_configs[25].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[25].push_back(-1); 

  // Color flows of process Process: u u > e+ ve g u d WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[26].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[26].push_back(0); 
  // JAMP #1
  color_configs[26].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[26].push_back(0); 
  // JAMP #2
  color_configs[26].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[26].push_back(0); 
  // JAMP #3
  color_configs[26].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[26].push_back(0); 

  // Color flows of process Process: u c > ve~ ve g u c WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[27].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[27].push_back(-1); 
  // JAMP #1
  color_configs[27].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[27].push_back(0); 
  // JAMP #2
  color_configs[27].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[27].push_back(0); 
  // JAMP #3
  color_configs[27].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[27].push_back(-1); 

  // Color flows of process Process: u d > e+ ve g d d WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[28].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[28].push_back(0); 
  // JAMP #1
  color_configs[28].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[28].push_back(0); 
  // JAMP #2
  color_configs[28].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[28].push_back(0); 
  // JAMP #3
  color_configs[28].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[28].push_back(0); 

  // Color flows of process Process: u d > ve~ e- g u u WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[29].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[29].push_back(0); 
  // JAMP #1
  color_configs[29].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[29].push_back(0); 
  // JAMP #2
  color_configs[29].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[29].push_back(0); 
  // JAMP #3
  color_configs[29].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[29].push_back(0); 

  // Color flows of process Process: u d > ve~ ve g u d WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[30].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[30].push_back(-1); 
  // JAMP #1
  color_configs[30].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[30].push_back(0); 
  // JAMP #2
  color_configs[30].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[30].push_back(0); 
  // JAMP #3
  color_configs[30].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[30].push_back(-1); 

  // Color flows of process Process: u u~ > e+ ve g d u~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[31].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[31].push_back(0); 
  // JAMP #1
  color_configs[31].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[31].push_back(0); 
  // JAMP #2
  color_configs[31].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[31].push_back(0); 
  // JAMP #3
  color_configs[31].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[31].push_back(0); 

  // Color flows of process Process: u u~ > ve~ e- g u d~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[32].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[32].push_back(0); 
  // JAMP #1
  color_configs[32].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[32].push_back(0); 
  // JAMP #2
  color_configs[32].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[32].push_back(0); 
  // JAMP #3
  color_configs[32].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[32].push_back(0); 

  // Color flows of process Process: u u~ > ve~ ve g c c~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[33].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[33].push_back(-1); 
  // JAMP #1
  color_configs[33].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[33].push_back(0); 
  // JAMP #2
  color_configs[33].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[33].push_back(-1); 
  // JAMP #3
  color_configs[33].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[33].push_back(0); 

  // Color flows of process Process: u u~ > ve~ ve g d d~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[34].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[34].push_back(-1); 
  // JAMP #1
  color_configs[34].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[34].push_back(0); 
  // JAMP #2
  color_configs[34].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[34].push_back(-1); 
  // JAMP #3
  color_configs[34].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[34].push_back(0); 

  // Color flows of process Process: u c~ > ve~ ve g u c~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[35].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[35].push_back(0); 
  // JAMP #1
  color_configs[35].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[35].push_back(-1); 
  // JAMP #2
  color_configs[35].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[35].push_back(0); 
  // JAMP #3
  color_configs[35].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[35].push_back(-1); 

  // Color flows of process Process: u d~ > e+ ve g u u~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[36].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[36].push_back(0); 
  // JAMP #1
  color_configs[36].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[36].push_back(0); 
  // JAMP #2
  color_configs[36].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[36].push_back(0); 
  // JAMP #3
  color_configs[36].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[36].push_back(0); 

  // Color flows of process Process: u d~ > e+ ve g d d~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[37].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[37].push_back(0); 
  // JAMP #1
  color_configs[37].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[37].push_back(0); 
  // JAMP #2
  color_configs[37].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[37].push_back(0); 
  // JAMP #3
  color_configs[37].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[37].push_back(0); 

  // Color flows of process Process: u d~ > ve~ ve g u d~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[38].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[38].push_back(0); 
  // JAMP #1
  color_configs[38].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[38].push_back(-1); 
  // JAMP #2
  color_configs[38].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[38].push_back(0); 
  // JAMP #3
  color_configs[38].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[38].push_back(-1); 

  // Color flows of process Process: d d > ve~ e- g u d WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[39].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[39].push_back(0); 
  // JAMP #1
  color_configs[39].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[39].push_back(0); 
  // JAMP #2
  color_configs[39].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[39].push_back(0); 
  // JAMP #3
  color_configs[39].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[39].push_back(0); 

  // Color flows of process Process: d s > ve~ ve g d s WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[40].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[40].push_back(-1); 
  // JAMP #1
  color_configs[40].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[40].push_back(0); 
  // JAMP #2
  color_configs[40].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[40].push_back(0); 
  // JAMP #3
  color_configs[40].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[40].push_back(-1); 

  // Color flows of process Process: d u~ > ve~ e- g u u~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[41].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[41].push_back(0); 
  // JAMP #1
  color_configs[41].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[41].push_back(0); 
  // JAMP #2
  color_configs[41].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[41].push_back(0); 
  // JAMP #3
  color_configs[41].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[41].push_back(0); 

  // Color flows of process Process: d u~ > ve~ e- g d d~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[42].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[42].push_back(0); 
  // JAMP #1
  color_configs[42].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[42].push_back(0); 
  // JAMP #2
  color_configs[42].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[42].push_back(0); 
  // JAMP #3
  color_configs[42].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[42].push_back(0); 

  // Color flows of process Process: d u~ > ve~ ve g d u~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[43].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[43].push_back(0); 
  // JAMP #1
  color_configs[43].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[43].push_back(-1); 
  // JAMP #2
  color_configs[43].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[43].push_back(0); 
  // JAMP #3
  color_configs[43].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[43].push_back(-1); 

  // Color flows of process Process: d d~ > e+ ve g d u~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[44].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[44].push_back(0); 
  // JAMP #1
  color_configs[44].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[44].push_back(0); 
  // JAMP #2
  color_configs[44].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[44].push_back(0); 
  // JAMP #3
  color_configs[44].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[44].push_back(0); 

  // Color flows of process Process: d d~ > ve~ e- g u d~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[45].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[45].push_back(0); 
  // JAMP #1
  color_configs[45].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[45].push_back(0); 
  // JAMP #2
  color_configs[45].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[45].push_back(0); 
  // JAMP #3
  color_configs[45].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[45].push_back(0); 

  // Color flows of process Process: d d~ > ve~ ve g u u~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[46].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[46].push_back(-1); 
  // JAMP #1
  color_configs[46].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[46].push_back(0); 
  // JAMP #2
  color_configs[46].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[46].push_back(-1); 
  // JAMP #3
  color_configs[46].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[46].push_back(0); 

  // Color flows of process Process: d d~ > ve~ ve g s s~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[47].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[47].push_back(-1); 
  // JAMP #1
  color_configs[47].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[47].push_back(0); 
  // JAMP #2
  color_configs[47].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[47].push_back(-1); 
  // JAMP #3
  color_configs[47].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[47].push_back(0); 

  // Color flows of process Process: d s~ > ve~ ve g d s~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[48].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[48].push_back(0); 
  // JAMP #1
  color_configs[48].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[48].push_back(-1); 
  // JAMP #2
  color_configs[48].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[48].push_back(0); 
  // JAMP #3
  color_configs[48].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[48].push_back(-1); 

  // Color flows of process Process: u~ u~ > ve~ e- g u~ d~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[49].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[49].push_back(0); 
  // JAMP #1
  color_configs[49].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[49].push_back(0); 
  // JAMP #2
  color_configs[49].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[49].push_back(0); 
  // JAMP #3
  color_configs[49].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[49].push_back(0); 

  // Color flows of process Process: u~ c~ > ve~ ve g u~ c~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[50].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[50].push_back(-1); 
  // JAMP #1
  color_configs[50].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[50].push_back(0); 
  // JAMP #2
  color_configs[50].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[50].push_back(0); 
  // JAMP #3
  color_configs[50].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[50].push_back(-1); 

  // Color flows of process Process: u~ d~ > e+ ve g u~ u~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[51].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[51].push_back(0); 
  // JAMP #1
  color_configs[51].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[51].push_back(0); 
  // JAMP #2
  color_configs[51].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[51].push_back(0); 
  // JAMP #3
  color_configs[51].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[51].push_back(0); 

  // Color flows of process Process: u~ d~ > ve~ e- g d~ d~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[52].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[52].push_back(0); 
  // JAMP #1
  color_configs[52].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[52].push_back(0); 
  // JAMP #2
  color_configs[52].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[52].push_back(0); 
  // JAMP #3
  color_configs[52].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[52].push_back(0); 

  // Color flows of process Process: u~ d~ > ve~ ve g u~ d~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[53].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[53].push_back(-1); 
  // JAMP #1
  color_configs[53].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[53].push_back(0); 
  // JAMP #2
  color_configs[53].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[53].push_back(0); 
  // JAMP #3
  color_configs[53].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[53].push_back(-1); 

  // Color flows of process Process: d~ d~ > e+ ve g u~ d~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[54].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[54].push_back(0); 
  // JAMP #1
  color_configs[54].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[54].push_back(0); 
  // JAMP #2
  color_configs[54].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[54].push_back(0); 
  // JAMP #3
  color_configs[54].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[54].push_back(0); 

  // Color flows of process Process: d~ s~ > ve~ ve g d~ s~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[55].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[55].push_back(-1); 
  // JAMP #1
  color_configs[55].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[55].push_back(0); 
  // JAMP #2
  color_configs[55].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[55].push_back(0); 
  // JAMP #3
  color_configs[55].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[55].push_back(-1); 

  // Color flows of process Process: u c > e+ ve g u s WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[56].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[56].push_back(-1); 
  // JAMP #1
  color_configs[56].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[56].push_back(0); 
  // JAMP #2
  color_configs[56].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[56].push_back(0); 
  // JAMP #3
  color_configs[56].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[56].push_back(-1); 

  // Color flows of process Process: u c > e+ ve g c d WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[57].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[57].push_back(0); 
  // JAMP #1
  color_configs[57].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[57].push_back(-1); 
  // JAMP #2
  color_configs[57].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[57].push_back(-1); 
  // JAMP #3
  color_configs[57].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[57].push_back(0); 

  // Color flows of process Process: u s > ve~ e- g u c WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[58].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[58].push_back(-1); 
  // JAMP #1
  color_configs[58].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[58].push_back(0); 
  // JAMP #2
  color_configs[58].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[58].push_back(0); 
  // JAMP #3
  color_configs[58].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[58].push_back(-1); 

  // Color flows of process Process: u u~ > e+ ve g s c~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[59].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[59].push_back(-1); 
  // JAMP #1
  color_configs[59].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[59].push_back(0); 
  // JAMP #2
  color_configs[59].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[59].push_back(-1); 
  // JAMP #3
  color_configs[59].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[59].push_back(0); 

  // Color flows of process Process: u u~ > ve~ e- g c s~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[60].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[60].push_back(-1); 
  // JAMP #1
  color_configs[60].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[60].push_back(0); 
  // JAMP #2
  color_configs[60].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[60].push_back(-1); 
  // JAMP #3
  color_configs[60].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[60].push_back(0); 

  // Color flows of process Process: u c~ > e+ ve g d c~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[61].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[61].push_back(0); 
  // JAMP #1
  color_configs[61].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[61].push_back(-1); 
  // JAMP #2
  color_configs[61].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[61].push_back(0); 
  // JAMP #3
  color_configs[61].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[61].push_back(-1); 

  // Color flows of process Process: u c~ > ve~ e- g u s~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[62].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[62].push_back(0); 
  // JAMP #1
  color_configs[62].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[62].push_back(-1); 
  // JAMP #2
  color_configs[62].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[62].push_back(0); 
  // JAMP #3
  color_configs[62].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[62].push_back(-1); 

  // Color flows of process Process: u d~ > e+ ve g c c~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[63].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[63].push_back(-1); 
  // JAMP #1
  color_configs[63].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[63].push_back(0); 
  // JAMP #2
  color_configs[63].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[63].push_back(-1); 
  // JAMP #3
  color_configs[63].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[63].push_back(0); 

  // Color flows of process Process: u s~ > e+ ve g u c~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[64].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[64].push_back(0); 
  // JAMP #1
  color_configs[64].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[64].push_back(-1); 
  // JAMP #2
  color_configs[64].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[64].push_back(0); 
  // JAMP #3
  color_configs[64].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[64].push_back(-1); 

  // Color flows of process Process: d s > ve~ e- g u s WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[65].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[65].push_back(-1); 
  // JAMP #1
  color_configs[65].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[65].push_back(0); 
  // JAMP #2
  color_configs[65].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[65].push_back(0); 
  // JAMP #3
  color_configs[65].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[65].push_back(-1); 

  // Color flows of process Process: d u~ > ve~ e- g c c~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[66].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[66].push_back(-1); 
  // JAMP #1
  color_configs[66].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[66].push_back(0); 
  // JAMP #2
  color_configs[66].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[66].push_back(-1); 
  // JAMP #3
  color_configs[66].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[66].push_back(0); 

  // Color flows of process Process: d c~ > ve~ e- g u c~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[67].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[67].push_back(0); 
  // JAMP #1
  color_configs[67].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[67].push_back(-1); 
  // JAMP #2
  color_configs[67].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[67].push_back(0); 
  // JAMP #3
  color_configs[67].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[67].push_back(-1); 

  // Color flows of process Process: u~ c~ > ve~ e- g u~ s~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[68].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[68].push_back(-1); 
  // JAMP #1
  color_configs[68].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[68].push_back(0); 
  // JAMP #2
  color_configs[68].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[68].push_back(0); 
  // JAMP #3
  color_configs[68].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[68].push_back(-1); 

  // Color flows of process Process: u~ c~ > ve~ e- g c~ d~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[69].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[69].push_back(0); 
  // JAMP #1
  color_configs[69].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[69].push_back(-1); 
  // JAMP #2
  color_configs[69].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[69].push_back(-1); 
  // JAMP #3
  color_configs[69].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[69].push_back(0); 

  // Color flows of process Process: u~ s~ > e+ ve g u~ c~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[70].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[70].push_back(-1); 
  // JAMP #1
  color_configs[70].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[70].push_back(0); 
  // JAMP #2
  color_configs[70].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[70].push_back(0); 
  // JAMP #3
  color_configs[70].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[70].push_back(-1); 

  // Color flows of process Process: d~ s~ > e+ ve g u~ s~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[71].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[71].push_back(-1); 
  // JAMP #1
  color_configs[71].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[71].push_back(0); 
  // JAMP #2
  color_configs[71].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[71].push_back(0); 
  // JAMP #3
  color_configs[71].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[71].push_back(-1); 
}

//--------------------------------------------------------------------------
// Destructor.
PY8MEs_R4_P76_sm_qq_llgqq::~PY8MEs_R4_P76_sm_qq_llgqq() 
{
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    delete[] p[i]; 
    p[i] = NULL; 
  }
}

//--------------------------------------------------------------------------
// Invert the permutation mapping
vector<int> PY8MEs_R4_P76_sm_qq_llgqq::invert_mapping(vector<int> mapping) 
{
  vector<int> inverted_mapping; 
  for (unsigned int i = 0; i < mapping.size(); i++ )
  {
    for (unsigned int j = 0; j < mapping.size(); j++ )
    {
      if (mapping[j] == ((int)i))
      {
        inverted_mapping.push_back(j); 
        break; 
      }
    }
  }
  return inverted_mapping; 
}

//--------------------------------------------------------------------------
// Return the list of possible helicity configurations
vector < vec_int > PY8MEs_R4_P76_sm_qq_llgqq::getHelicityConfigs(vector<int>
    permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(ncomb, vector<int> (nexternal, 0)); 
  for (unsigned int ihel = 0; ihel < ncomb; ihel++ )
  {
    for(unsigned int j = 0; j < nexternal; j++ )
    {
      res[ihel][chosenPerm[j]] = helicities[ihel][j]; 
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Return the list of possible color configurations
vector < vec_int > PY8MEs_R4_P76_sm_qq_llgqq::getColorConfigs(int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(color_configs[chosenProcID].size(), vector<int>
      (nexternal * 2, 0));
  for (unsigned int icol = 0; icol < color_configs[chosenProcID].size(); icol++
      )
  {
    for(unsigned int j = 0; j < (2 * nexternal); j++ )
    {
      res[icol][chosenPerm[j/2] * 2 + j%2] =
          color_configs[chosenProcID][icol][j];
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Get JAMP relative N_c power
int PY8MEs_R4_P76_sm_qq_llgqq::getColorFlowRelativeNCPower(int color_flow_ID,
    int specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return jamp_nc_relative_power[chosenProcID][color_flow_ID]; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity ID -> Helicity Config
vector<int> PY8MEs_R4_P76_sm_qq_llgqq::getHelicityConfigForID(int hel_ID,
    vector<int> permutation)
{
  if (hel_ID < 0 || hel_ID >= ncomb)
  {
    cerr <<  "Error in function 'getHelicityConfigForID' of class" << 
    " 'PY8MEs_R4_P76_sm_qq_llgqq': Specified helicity ID '" << 
    hel_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R4_P76_sm_qq_llgqq_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(nexternal, 0); 
  for (unsigned int j = 0; j < nexternal; j++ )
  {
    res[chosenPerm[j]] = helicities[hel_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity Config -> Helicity ID
int PY8MEs_R4_P76_sm_qq_llgqq::getHelicityIDForConfig(vector<int> hel_config,
    vector<int> permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  int user_ihel = -1; 
  if (hel_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < ncomb; i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < nexternal; j++ )
      {
        if (helicities[i][chosenPerm[j]] != hel_config[j])
        {
          found = false; 
          break; 
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_ihel = i; 
        break; 
      }
    }
    if (user_ihel == -1)
    {
      cerr <<  "Error in function 'getHelicityIDForConfig' of class" << 
      " 'PY8MEs_R4_P76_sm_qq_llgqq': Specified helicity" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R4_P76_sm_qq_llgqq_exception; 
    }
  }
  return user_ihel; 
}


//--------------------------------------------------------------------------
// Implements the map Color ID -> Color Config
vector<int> PY8MEs_R4_P76_sm_qq_llgqq::getColorConfigForID(int color_ID, int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < 0 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getColorConfigForID' of class" << 
    " 'PY8MEs_R4_P76_sm_qq_llgqq': Specified color ID '" << 
    color_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R4_P76_sm_qq_llgqq_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(color_configs[chosenProcID][color_ID].size(), 0); 
  for (unsigned int j = 0; j < (2 * nexternal); j++ )
  {
    res[chosenPerm[j/2] * 2 + j%2] = color_configs[chosenProcID][color_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Color Config -> Color ID
int PY8MEs_R4_P76_sm_qq_llgqq::getColorIDForConfig(vector<int> color_config,
    int specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = -1; 
  if (color_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < color_configs[chosenProcID].size(); i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < (nexternal * 2); j++ )
      {

        // If colorless then make sure it matches
        // The little arithmetics in the color index is just
        // the permutation applies on the particle list which is
        // twice smaller since each particle can have two color indices.
        if (color_config[j] == 0)
        {
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] != 0)
          {
            found = false; 
            break; 
          }
          // Otherwise check that the color linked position matches
        }
        else
        {
          int color_linked_pos = -1; 
          // Find the other end of the line in the user color config
          for (unsigned int k = 0; k < (nexternal * 2); k++ )
          {
            if (k == j)
              continue; 
            if (color_config[j] == color_config[k])
            {
              color_linked_pos = k; 
              break; 
            }
          }
          if (color_linked_pos == -1)
          {
            cerr <<  "Error in function 'getColorIDForConfig' of class" << 
            " 'PY8MEs_R4_P76_sm_qq_llgqq': A color line could " << 
            " not be closed." << endl; 
            throw PY8MEs_R4_P76_sm_qq_llgqq_exception; 
          }
          // Now check whether the color line matches
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] !=
              color_configs[chosenProcID][i][chosenPerm[color_linked_pos/2] * 2
              + color_linked_pos%2])
          {
            found = false; 
            break; 
          }
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_icol = i; 
        break; 
      }
    }

    if (user_icol == -1)
    {
      cerr <<  "Error in function 'getColorIDForConfig' of class" << 
      " 'PY8MEs_R4_P76_sm_qq_llgqq': Specified color" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R4_P76_sm_qq_llgqq_exception; 
    }
  }
  return user_icol; 
}

//--------------------------------------------------------------------------
// Returns all result previously computed in SigmaKin
vector < vec_double > PY8MEs_R4_P76_sm_qq_llgqq::getAllResults(int
    specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return all_results[chosenProcID]; 
}

//--------------------------------------------------------------------------
// Returns a result previously computed in SigmaKin for a specific helicity
// and color ID. -1 means avg and summed over that characteristic.
double PY8MEs_R4_P76_sm_qq_llgqq::getResult(int helicity_ID, int color_ID, int
    specify_proc_ID)
{
  if (helicity_ID < - 1 || helicity_ID >= ncomb)
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R4_P76_sm_qq_llgqq': Specified helicity ID '" << 
    helicity_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R4_P76_sm_qq_llgqq_exception; 
  }
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < - 1 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R4_P76_sm_qq_llgqq': Specified color ID '" << 
    color_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R4_P76_sm_qq_llgqq_exception; 
  }
  return all_results[chosenProcID][helicity_ID + 1][color_ID + 1]; 
}

//--------------------------------------------------------------------------
// Check for the availability of the requested process and if available,
// If available, this returns the corresponding permutation and Proc_ID to use.
// If not available, this returns a negative Proc_ID.
pair < vector<int> , int > PY8MEs_R4_P76_sm_qq_llgqq::static_getPY8ME(vector<int> initial_pdgs, vector<int> final_pdgs, set<int> schannels) 
{

  // Not available return value
  pair < vector<int> , int > NA(vector<int> (), -1); 

  // Check if s-channel requirements match
  if (nreq_s_channels > 0)
  {
    if (schannels != s_channel_proc)
      return NA; 
  }
  else
  {
    if (schannels.size() != 0)
      return NA; 
  }

  // Check number of final state particles
  if (final_pdgs.size() != (nexternal - ninitial))
    return NA; 

  // Check number of initial state particles
  if (initial_pdgs.size() != ninitial)
    return NA; 

  // List of processes available in this class
  const int nprocs = 744; 
  const int proc_IDS[nprocs] = {0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3,
      4, 4, 4, 4, 5, 5, 5, 5, 6, 6, 6, 6, 6, 6, 7, 7, 8, 8, 8, 8, 8, 8, 8, 8,
      9, 9, 9, 9, 10, 10, 10, 10, 10, 10, 10, 10, 11, 11, 11, 11, 11, 11, 12,
      12, 12, 12, 13, 13, 13, 13, 13, 13, 13, 13, 14, 14, 14, 14, 14, 14, 15,
      15, 16, 16, 16, 16, 16, 16, 16, 16, 17, 17, 17, 17, 17, 17, 17, 17, 18,
      18, 18, 18, 19, 19, 19, 19, 19, 19, 20, 20, 20, 20, 21, 21, 21, 21, 21,
      21, 22, 22, 23, 23, 23, 23, 23, 23, 23, 23, 24, 24, 24, 24, 24, 24, 25,
      25, 26, 26, 26, 26, 27, 27, 27, 28, 28, 28, 28, 29, 29, 29, 29, 30, 30,
      30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 31, 31, 31, 31, 32, 32, 32, 32,
      33, 33, 33, 33, 33, 33, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34,
      35, 35, 35, 35, 35, 35, 36, 36, 36, 36, 37, 37, 37, 37, 38, 38, 38, 38,
      38, 38, 38, 38, 38, 38, 38, 38, 39, 39, 39, 39, 40, 40, 40, 41, 41, 41,
      41, 42, 42, 42, 42, 43, 43, 43, 43, 43, 43, 43, 43, 43, 43, 43, 43, 44,
      44, 44, 44, 45, 45, 45, 45, 46, 46, 46, 46, 46, 46, 46, 46, 46, 46, 46,
      46, 47, 47, 47, 47, 47, 47, 48, 48, 48, 48, 48, 48, 49, 49, 49, 49, 50,
      50, 50, 51, 51, 51, 51, 52, 52, 52, 52, 53, 53, 53, 53, 53, 53, 53, 53,
      53, 53, 53, 53, 54, 54, 54, 54, 55, 55, 55, 56, 56, 57, 57, 57, 57, 57,
      57, 58, 58, 58, 58, 58, 58, 59, 59, 59, 59, 59, 59, 59, 59, 60, 60, 60,
      60, 60, 60, 60, 60, 61, 61, 61, 61, 61, 61, 61, 61, 62, 62, 62, 62, 62,
      62, 62, 62, 63, 63, 63, 63, 63, 63, 63, 63, 64, 64, 64, 64, 64, 64, 64,
      64, 65, 65, 66, 66, 66, 66, 66, 66, 66, 66, 67, 67, 67, 67, 67, 67, 67,
      67, 68, 68, 69, 69, 69, 69, 69, 69, 70, 70, 70, 70, 70, 70, 71, 71, 1, 1,
      1, 1, 3, 3, 3, 3, 7, 7, 8, 8, 8, 8, 8, 8, 8, 8, 9, 9, 9, 9, 10, 10, 10,
      10, 10, 10, 10, 10, 11, 11, 11, 11, 11, 11, 12, 12, 12, 12, 13, 13, 13,
      13, 13, 13, 13, 13, 15, 15, 16, 16, 16, 16, 16, 16, 16, 16, 17, 17, 17,
      17, 17, 17, 17, 17, 18, 18, 18, 18, 19, 19, 19, 19, 19, 19, 20, 20, 20,
      20, 22, 22, 23, 23, 23, 23, 23, 23, 23, 23, 25, 25, 27, 27, 27, 28, 28,
      28, 28, 29, 29, 29, 29, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30,
      31, 31, 31, 31, 32, 32, 32, 32, 33, 33, 33, 33, 33, 33, 34, 34, 34, 34,
      34, 34, 34, 34, 34, 34, 34, 34, 35, 35, 35, 35, 35, 35, 36, 36, 36, 36,
      37, 37, 37, 37, 38, 38, 38, 38, 38, 38, 38, 38, 38, 38, 38, 38, 40, 40,
      40, 41, 41, 41, 41, 42, 42, 42, 42, 43, 43, 43, 43, 43, 43, 43, 43, 43,
      43, 43, 43, 44, 44, 44, 44, 45, 45, 45, 45, 46, 46, 46, 46, 46, 46, 46,
      46, 46, 46, 46, 46, 47, 47, 47, 47, 47, 47, 48, 48, 48, 48, 48, 48, 50,
      50, 50, 51, 51, 51, 51, 52, 52, 52, 52, 53, 53, 53, 53, 53, 53, 53, 53,
      53, 53, 53, 53, 55, 55, 55, 56, 56, 57, 57, 57, 57, 57, 57, 58, 58, 58,
      58, 58, 58, 59, 59, 59, 59, 59, 59, 59, 59, 60, 60, 60, 60, 60, 60, 60,
      60, 61, 61, 61, 61, 61, 61, 61, 61, 62, 62, 62, 62, 62, 62, 62, 62, 63,
      63, 63, 63, 63, 63, 63, 63, 64, 64, 64, 64, 64, 64, 64, 64, 65, 65, 66,
      66, 66, 66, 66, 66, 66, 66, 67, 67, 67, 67, 67, 67, 67, 67, 68, 68, 69,
      69, 69, 69, 69, 69, 70, 70, 70, 70, 70, 70, 71, 71};
  const int in_pdgs[nprocs][ninitial] = {{2, 2}, {2, 2}, {4, 4}, {4, 4}, {2,
      -2}, {2, -2}, {4, -4}, {4, -4}, {1, 1}, {1, 1}, {3, 3}, {3, 3}, {1, -1},
      {1, -1}, {3, -3}, {3, -3}, {-2, -2}, {-2, -2}, {-4, -4}, {-4, -4}, {-1,
      -1}, {-1, -1}, {-3, -3}, {-3, -3}, {2, 2}, {2, 2}, {2, 2}, {4, 4}, {4,
      4}, {4, 4}, {2, 4}, {2, 4}, {2, 1}, {2, 1}, {2, 3}, {2, 3}, {4, 1}, {4,
      1}, {4, 3}, {4, 3}, {2, -2}, {2, -2}, {4, -4}, {4, -4}, {2, -2}, {2, -2},
      {2, -2}, {2, -2}, {4, -4}, {4, -4}, {4, -4}, {4, -4}, {2, -2}, {2, -2},
      {2, -2}, {4, -4}, {4, -4}, {4, -4}, {2, -4}, {2, -4}, {4, -2}, {4, -2},
      {2, -1}, {2, -1}, {2, -3}, {2, -3}, {4, -1}, {4, -1}, {4, -3}, {4, -3},
      {1, 1}, {1, 1}, {1, 1}, {3, 3}, {3, 3}, {3, 3}, {1, 3}, {1, 3}, {1, -2},
      {1, -2}, {1, -4}, {1, -4}, {3, -2}, {3, -2}, {3, -4}, {3, -4}, {1, -1},
      {1, -1}, {1, -1}, {1, -1}, {3, -3}, {3, -3}, {3, -3}, {3, -3}, {1, -1},
      {1, -1}, {3, -3}, {3, -3}, {1, -1}, {1, -1}, {1, -1}, {3, -3}, {3, -3},
      {3, -3}, {1, -3}, {1, -3}, {3, -1}, {3, -1}, {-2, -2}, {-2, -2}, {-2,
      -2}, {-4, -4}, {-4, -4}, {-4, -4}, {-2, -4}, {-2, -4}, {-2, -1}, {-2,
      -1}, {-2, -3}, {-2, -3}, {-4, -1}, {-4, -1}, {-4, -3}, {-4, -3}, {-1,
      -1}, {-1, -1}, {-1, -1}, {-3, -3}, {-3, -3}, {-3, -3}, {-1, -3}, {-1,
      -3}, {2, 2}, {2, 2}, {4, 4}, {4, 4}, {2, 4}, {2, 4}, {2, 4}, {2, 1}, {2,
      1}, {4, 3}, {4, 3}, {2, 1}, {2, 1}, {4, 3}, {4, 3}, {2, 1}, {2, 1}, {2,
      1}, {2, 3}, {2, 3}, {2, 3}, {4, 1}, {4, 1}, {4, 1}, {4, 3}, {4, 3}, {4,
      3}, {2, -2}, {2, -2}, {4, -4}, {4, -4}, {2, -2}, {2, -2}, {4, -4}, {4,
      -4}, {2, -2}, {2, -2}, {2, -2}, {4, -4}, {4, -4}, {4, -4}, {2, -2}, {2,
      -2}, {2, -2}, {2, -2}, {2, -2}, {2, -2}, {4, -4}, {4, -4}, {4, -4}, {4,
      -4}, {4, -4}, {4, -4}, {2, -4}, {2, -4}, {2, -4}, {4, -2}, {4, -2}, {4,
      -2}, {2, -1}, {2, -1}, {4, -3}, {4, -3}, {2, -1}, {2, -1}, {4, -3}, {4,
      -3}, {2, -1}, {2, -1}, {2, -1}, {2, -3}, {2, -3}, {2, -3}, {4, -1}, {4,
      -1}, {4, -1}, {4, -3}, {4, -3}, {4, -3}, {1, 1}, {1, 1}, {3, 3}, {3, 3},
      {1, 3}, {1, 3}, {1, 3}, {1, -2}, {1, -2}, {3, -4}, {3, -4}, {1, -2}, {1,
      -2}, {3, -4}, {3, -4}, {1, -2}, {1, -2}, {1, -2}, {1, -4}, {1, -4}, {1,
      -4}, {3, -2}, {3, -2}, {3, -2}, {3, -4}, {3, -4}, {3, -4}, {1, -1}, {1,
      -1}, {3, -3}, {3, -3}, {1, -1}, {1, -1}, {3, -3}, {3, -3}, {1, -1}, {1,
      -1}, {1, -1}, {1, -1}, {1, -1}, {1, -1}, {3, -3}, {3, -3}, {3, -3}, {3,
      -3}, {3, -3}, {3, -3}, {1, -1}, {1, -1}, {1, -1}, {3, -3}, {3, -3}, {3,
      -3}, {1, -3}, {1, -3}, {1, -3}, {3, -1}, {3, -1}, {3, -1}, {-2, -2}, {-2,
      -2}, {-4, -4}, {-4, -4}, {-2, -4}, {-2, -4}, {-2, -4}, {-2, -1}, {-2,
      -1}, {-4, -3}, {-4, -3}, {-2, -1}, {-2, -1}, {-4, -3}, {-4, -3}, {-2,
      -1}, {-2, -1}, {-2, -1}, {-2, -3}, {-2, -3}, {-2, -3}, {-4, -1}, {-4,
      -1}, {-4, -1}, {-4, -3}, {-4, -3}, {-4, -3}, {-1, -1}, {-1, -1}, {-3,
      -3}, {-3, -3}, {-1, -3}, {-1, -3}, {-1, -3}, {2, 4}, {2, 4}, {2, 4}, {2,
      4}, {2, 3}, {2, 3}, {4, 1}, {4, 1}, {2, 3}, {2, 3}, {4, 1}, {4, 1}, {1,
      3}, {1, 3}, {2, -2}, {2, -2}, {4, -4}, {4, -4}, {1, -1}, {1, -1}, {3,
      -3}, {3, -3}, {2, -2}, {2, -2}, {4, -4}, {4, -4}, {1, -1}, {1, -1}, {3,
      -3}, {3, -3}, {2, -4}, {2, -4}, {2, -3}, {2, -3}, {4, -2}, {4, -2}, {4,
      -1}, {4, -1}, {2, -4}, {2, -4}, {4, -2}, {4, -2}, {1, -4}, {1, -4}, {3,
      -2}, {3, -2}, {2, -1}, {2, -1}, {2, -1}, {2, -1}, {4, -3}, {4, -3}, {4,
      -3}, {4, -3}, {2, -3}, {2, -3}, {4, -1}, {4, -1}, {1, -3}, {1, -3}, {3,
      -1}, {3, -1}, {1, 3}, {1, 3}, {1, -2}, {1, -2}, {1, -2}, {1, -2}, {3,
      -4}, {3, -4}, {3, -4}, {3, -4}, {1, -4}, {1, -4}, {1, -3}, {1, -3}, {3,
      -2}, {3, -2}, {3, -1}, {3, -1}, {-2, -4}, {-2, -4}, {-2, -4}, {-2, -4},
      {-2, -3}, {-2, -3}, {-4, -1}, {-4, -1}, {-2, -3}, {-2, -3}, {-4, -1},
      {-4, -1}, {-1, -3}, {-1, -3}, {-1, -3}, {-1, -3}, {-2, 2}, {-2, 2}, {-4,
      4}, {-4, 4}, {-1, 1}, {-1, 1}, {-3, 3}, {-3, 3}, {4, 2}, {4, 2}, {1, 2},
      {1, 2}, {3, 2}, {3, 2}, {1, 4}, {1, 4}, {3, 4}, {3, 4}, {-2, 2}, {-2, 2},
      {-4, 4}, {-4, 4}, {-2, 2}, {-2, 2}, {-2, 2}, {-2, 2}, {-4, 4}, {-4, 4},
      {-4, 4}, {-4, 4}, {-2, 2}, {-2, 2}, {-2, 2}, {-4, 4}, {-4, 4}, {-4, 4},
      {-4, 2}, {-4, 2}, {-2, 4}, {-2, 4}, {-1, 2}, {-1, 2}, {-3, 2}, {-3, 2},
      {-1, 4}, {-1, 4}, {-3, 4}, {-3, 4}, {3, 1}, {3, 1}, {-2, 1}, {-2, 1},
      {-4, 1}, {-4, 1}, {-2, 3}, {-2, 3}, {-4, 3}, {-4, 3}, {-1, 1}, {-1, 1},
      {-1, 1}, {-1, 1}, {-3, 3}, {-3, 3}, {-3, 3}, {-3, 3}, {-1, 1}, {-1, 1},
      {-3, 3}, {-3, 3}, {-1, 1}, {-1, 1}, {-1, 1}, {-3, 3}, {-3, 3}, {-3, 3},
      {-3, 1}, {-3, 1}, {-1, 3}, {-1, 3}, {-4, -2}, {-4, -2}, {-1, -2}, {-1,
      -2}, {-3, -2}, {-3, -2}, {-1, -4}, {-1, -4}, {-3, -4}, {-3, -4}, {-3,
      -1}, {-3, -1}, {4, 2}, {4, 2}, {4, 2}, {1, 2}, {1, 2}, {3, 4}, {3, 4},
      {1, 2}, {1, 2}, {3, 4}, {3, 4}, {1, 2}, {1, 2}, {1, 2}, {3, 2}, {3, 2},
      {3, 2}, {1, 4}, {1, 4}, {1, 4}, {3, 4}, {3, 4}, {3, 4}, {-2, 2}, {-2, 2},
      {-4, 4}, {-4, 4}, {-2, 2}, {-2, 2}, {-4, 4}, {-4, 4}, {-2, 2}, {-2, 2},
      {-2, 2}, {-4, 4}, {-4, 4}, {-4, 4}, {-2, 2}, {-2, 2}, {-2, 2}, {-2, 2},
      {-2, 2}, {-2, 2}, {-4, 4}, {-4, 4}, {-4, 4}, {-4, 4}, {-4, 4}, {-4, 4},
      {-4, 2}, {-4, 2}, {-4, 2}, {-2, 4}, {-2, 4}, {-2, 4}, {-1, 2}, {-1, 2},
      {-3, 4}, {-3, 4}, {-1, 2}, {-1, 2}, {-3, 4}, {-3, 4}, {-1, 2}, {-1, 2},
      {-1, 2}, {-3, 2}, {-3, 2}, {-3, 2}, {-1, 4}, {-1, 4}, {-1, 4}, {-3, 4},
      {-3, 4}, {-3, 4}, {3, 1}, {3, 1}, {3, 1}, {-2, 1}, {-2, 1}, {-4, 3}, {-4,
      3}, {-2, 1}, {-2, 1}, {-4, 3}, {-4, 3}, {-2, 1}, {-2, 1}, {-2, 1}, {-4,
      1}, {-4, 1}, {-4, 1}, {-2, 3}, {-2, 3}, {-2, 3}, {-4, 3}, {-4, 3}, {-4,
      3}, {-1, 1}, {-1, 1}, {-3, 3}, {-3, 3}, {-1, 1}, {-1, 1}, {-3, 3}, {-3,
      3}, {-1, 1}, {-1, 1}, {-1, 1}, {-1, 1}, {-1, 1}, {-1, 1}, {-3, 3}, {-3,
      3}, {-3, 3}, {-3, 3}, {-3, 3}, {-3, 3}, {-1, 1}, {-1, 1}, {-1, 1}, {-3,
      3}, {-3, 3}, {-3, 3}, {-3, 1}, {-3, 1}, {-3, 1}, {-1, 3}, {-1, 3}, {-1,
      3}, {-4, -2}, {-4, -2}, {-4, -2}, {-1, -2}, {-1, -2}, {-3, -4}, {-3, -4},
      {-1, -2}, {-1, -2}, {-3, -4}, {-3, -4}, {-1, -2}, {-1, -2}, {-1, -2},
      {-3, -2}, {-3, -2}, {-3, -2}, {-1, -4}, {-1, -4}, {-1, -4}, {-3, -4},
      {-3, -4}, {-3, -4}, {-3, -1}, {-3, -1}, {-3, -1}, {4, 2}, {4, 2}, {4, 2},
      {4, 2}, {3, 2}, {3, 2}, {1, 4}, {1, 4}, {3, 2}, {3, 2}, {1, 4}, {1, 4},
      {3, 1}, {3, 1}, {-2, 2}, {-2, 2}, {-4, 4}, {-4, 4}, {-1, 1}, {-1, 1},
      {-3, 3}, {-3, 3}, {-2, 2}, {-2, 2}, {-4, 4}, {-4, 4}, {-1, 1}, {-1, 1},
      {-3, 3}, {-3, 3}, {-4, 2}, {-4, 2}, {-3, 2}, {-3, 2}, {-2, 4}, {-2, 4},
      {-1, 4}, {-1, 4}, {-4, 2}, {-4, 2}, {-2, 4}, {-2, 4}, {-4, 1}, {-4, 1},
      {-2, 3}, {-2, 3}, {-1, 2}, {-1, 2}, {-1, 2}, {-1, 2}, {-3, 4}, {-3, 4},
      {-3, 4}, {-3, 4}, {-3, 2}, {-3, 2}, {-1, 4}, {-1, 4}, {-3, 1}, {-3, 1},
      {-1, 3}, {-1, 3}, {3, 1}, {3, 1}, {-2, 1}, {-2, 1}, {-2, 1}, {-2, 1},
      {-4, 3}, {-4, 3}, {-4, 3}, {-4, 3}, {-4, 1}, {-4, 1}, {-3, 1}, {-3, 1},
      {-2, 3}, {-2, 3}, {-1, 3}, {-1, 3}, {-4, -2}, {-4, -2}, {-4, -2}, {-4,
      -2}, {-3, -2}, {-3, -2}, {-1, -4}, {-1, -4}, {-3, -2}, {-3, -2}, {-1,
      -4}, {-1, -4}, {-3, -1}, {-3, -1}, {-3, -1}, {-3, -1}};
  const int out_pdgs[nprocs][nexternal - ninitial] = {{-11, 11, 21, 2, 2},
      {-13, 13, 21, 2, 2}, {-11, 11, 21, 4, 4}, {-13, 13, 21, 4, 4}, {-11, 11,
      21, 2, -2}, {-13, 13, 21, 2, -2}, {-11, 11, 21, 4, -4}, {-13, 13, 21, 4,
      -4}, {-11, 11, 21, 1, 1}, {-13, 13, 21, 1, 1}, {-11, 11, 21, 3, 3}, {-13,
      13, 21, 3, 3}, {-11, 11, 21, 1, -1}, {-13, 13, 21, 1, -1}, {-11, 11, 21,
      3, -3}, {-13, 13, 21, 3, -3}, {-11, 11, 21, -2, -2}, {-13, 13, 21, -2,
      -2}, {-11, 11, 21, -4, -4}, {-13, 13, 21, -4, -4}, {-11, 11, 21, -1, -1},
      {-13, 13, 21, -1, -1}, {-11, 11, 21, -3, -3}, {-13, 13, 21, -3, -3},
      {-12, 12, 21, 2, 2}, {-14, 14, 21, 2, 2}, {-16, 16, 21, 2, 2}, {-12, 12,
      21, 4, 4}, {-14, 14, 21, 4, 4}, {-16, 16, 21, 4, 4}, {-11, 11, 21, 2, 4},
      {-13, 13, 21, 2, 4}, {-11, 11, 21, 2, 1}, {-13, 13, 21, 2, 1}, {-11, 11,
      21, 2, 3}, {-13, 13, 21, 2, 3}, {-11, 11, 21, 4, 1}, {-13, 13, 21, 4, 1},
      {-11, 11, 21, 4, 3}, {-13, 13, 21, 4, 3}, {-11, 11, 21, 4, -4}, {-13, 13,
      21, 4, -4}, {-11, 11, 21, 2, -2}, {-13, 13, 21, 2, -2}, {-11, 11, 21, 1,
      -1}, {-11, 11, 21, 3, -3}, {-13, 13, 21, 1, -1}, {-13, 13, 21, 3, -3},
      {-11, 11, 21, 1, -1}, {-11, 11, 21, 3, -3}, {-13, 13, 21, 1, -1}, {-13,
      13, 21, 3, -3}, {-12, 12, 21, 2, -2}, {-14, 14, 21, 2, -2}, {-16, 16, 21,
      2, -2}, {-12, 12, 21, 4, -4}, {-14, 14, 21, 4, -4}, {-16, 16, 21, 4, -4},
      {-11, 11, 21, 2, -4}, {-13, 13, 21, 2, -4}, {-11, 11, 21, 4, -2}, {-13,
      13, 21, 4, -2}, {-11, 11, 21, 2, -1}, {-13, 13, 21, 2, -1}, {-11, 11, 21,
      2, -3}, {-13, 13, 21, 2, -3}, {-11, 11, 21, 4, -1}, {-13, 13, 21, 4, -1},
      {-11, 11, 21, 4, -3}, {-13, 13, 21, 4, -3}, {-12, 12, 21, 1, 1}, {-14,
      14, 21, 1, 1}, {-16, 16, 21, 1, 1}, {-12, 12, 21, 3, 3}, {-14, 14, 21, 3,
      3}, {-16, 16, 21, 3, 3}, {-11, 11, 21, 1, 3}, {-13, 13, 21, 1, 3}, {-11,
      11, 21, 1, -2}, {-13, 13, 21, 1, -2}, {-11, 11, 21, 1, -4}, {-13, 13, 21,
      1, -4}, {-11, 11, 21, 3, -2}, {-13, 13, 21, 3, -2}, {-11, 11, 21, 3, -4},
      {-13, 13, 21, 3, -4}, {-11, 11, 21, 2, -2}, {-11, 11, 21, 4, -4}, {-13,
      13, 21, 2, -2}, {-13, 13, 21, 4, -4}, {-11, 11, 21, 2, -2}, {-11, 11, 21,
      4, -4}, {-13, 13, 21, 2, -2}, {-13, 13, 21, 4, -4}, {-11, 11, 21, 3, -3},
      {-13, 13, 21, 3, -3}, {-11, 11, 21, 1, -1}, {-13, 13, 21, 1, -1}, {-12,
      12, 21, 1, -1}, {-14, 14, 21, 1, -1}, {-16, 16, 21, 1, -1}, {-12, 12, 21,
      3, -3}, {-14, 14, 21, 3, -3}, {-16, 16, 21, 3, -3}, {-11, 11, 21, 1, -3},
      {-13, 13, 21, 1, -3}, {-11, 11, 21, 3, -1}, {-13, 13, 21, 3, -1}, {-12,
      12, 21, -2, -2}, {-14, 14, 21, -2, -2}, {-16, 16, 21, -2, -2}, {-12, 12,
      21, -4, -4}, {-14, 14, 21, -4, -4}, {-16, 16, 21, -4, -4}, {-11, 11, 21,
      -2, -4}, {-13, 13, 21, -2, -4}, {-11, 11, 21, -2, -1}, {-13, 13, 21, -2,
      -1}, {-11, 11, 21, -2, -3}, {-13, 13, 21, -2, -3}, {-11, 11, 21, -4, -1},
      {-13, 13, 21, -4, -1}, {-11, 11, 21, -4, -3}, {-13, 13, 21, -4, -3},
      {-12, 12, 21, -1, -1}, {-14, 14, 21, -1, -1}, {-16, 16, 21, -1, -1},
      {-12, 12, 21, -3, -3}, {-14, 14, 21, -3, -3}, {-16, 16, 21, -3, -3},
      {-11, 11, 21, -1, -3}, {-13, 13, 21, -1, -3}, {-11, 12, 21, 2, 1}, {-13,
      14, 21, 2, 1}, {-11, 12, 21, 4, 3}, {-13, 14, 21, 4, 3}, {-12, 12, 21, 2,
      4}, {-14, 14, 21, 2, 4}, {-16, 16, 21, 2, 4}, {-11, 12, 21, 1, 1}, {-13,
      14, 21, 1, 1}, {-11, 12, 21, 3, 3}, {-13, 14, 21, 3, 3}, {-12, 11, 21, 2,
      2}, {-14, 13, 21, 2, 2}, {-12, 11, 21, 4, 4}, {-14, 13, 21, 4, 4}, {-12,
      12, 21, 2, 1}, {-14, 14, 21, 2, 1}, {-16, 16, 21, 2, 1}, {-12, 12, 21, 2,
      3}, {-14, 14, 21, 2, 3}, {-16, 16, 21, 2, 3}, {-12, 12, 21, 4, 1}, {-14,
      14, 21, 4, 1}, {-16, 16, 21, 4, 1}, {-12, 12, 21, 4, 3}, {-14, 14, 21, 4,
      3}, {-16, 16, 21, 4, 3}, {-11, 12, 21, 1, -2}, {-13, 14, 21, 1, -2},
      {-11, 12, 21, 3, -4}, {-13, 14, 21, 3, -4}, {-12, 11, 21, 2, -1}, {-14,
      13, 21, 2, -1}, {-12, 11, 21, 4, -3}, {-14, 13, 21, 4, -3}, {-12, 12, 21,
      4, -4}, {-14, 14, 21, 4, -4}, {-16, 16, 21, 4, -4}, {-12, 12, 21, 2, -2},
      {-14, 14, 21, 2, -2}, {-16, 16, 21, 2, -2}, {-12, 12, 21, 1, -1}, {-12,
      12, 21, 3, -3}, {-14, 14, 21, 1, -1}, {-14, 14, 21, 3, -3}, {-16, 16, 21,
      1, -1}, {-16, 16, 21, 3, -3}, {-12, 12, 21, 1, -1}, {-12, 12, 21, 3, -3},
      {-14, 14, 21, 1, -1}, {-14, 14, 21, 3, -3}, {-16, 16, 21, 1, -1}, {-16,
      16, 21, 3, -3}, {-12, 12, 21, 2, -4}, {-14, 14, 21, 2, -4}, {-16, 16, 21,
      2, -4}, {-12, 12, 21, 4, -2}, {-14, 14, 21, 4, -2}, {-16, 16, 21, 4, -2},
      {-11, 12, 21, 2, -2}, {-13, 14, 21, 2, -2}, {-11, 12, 21, 4, -4}, {-13,
      14, 21, 4, -4}, {-11, 12, 21, 1, -1}, {-13, 14, 21, 1, -1}, {-11, 12, 21,
      3, -3}, {-13, 14, 21, 3, -3}, {-12, 12, 21, 2, -1}, {-14, 14, 21, 2, -1},
      {-16, 16, 21, 2, -1}, {-12, 12, 21, 2, -3}, {-14, 14, 21, 2, -3}, {-16,
      16, 21, 2, -3}, {-12, 12, 21, 4, -1}, {-14, 14, 21, 4, -1}, {-16, 16, 21,
      4, -1}, {-12, 12, 21, 4, -3}, {-14, 14, 21, 4, -3}, {-16, 16, 21, 4, -3},
      {-12, 11, 21, 2, 1}, {-14, 13, 21, 2, 1}, {-12, 11, 21, 4, 3}, {-14, 13,
      21, 4, 3}, {-12, 12, 21, 1, 3}, {-14, 14, 21, 1, 3}, {-16, 16, 21, 1, 3},
      {-12, 11, 21, 2, -2}, {-14, 13, 21, 2, -2}, {-12, 11, 21, 4, -4}, {-14,
      13, 21, 4, -4}, {-12, 11, 21, 1, -1}, {-14, 13, 21, 1, -1}, {-12, 11, 21,
      3, -3}, {-14, 13, 21, 3, -3}, {-12, 12, 21, 1, -2}, {-14, 14, 21, 1, -2},
      {-16, 16, 21, 1, -2}, {-12, 12, 21, 1, -4}, {-14, 14, 21, 1, -4}, {-16,
      16, 21, 1, -4}, {-12, 12, 21, 3, -2}, {-14, 14, 21, 3, -2}, {-16, 16, 21,
      3, -2}, {-12, 12, 21, 3, -4}, {-14, 14, 21, 3, -4}, {-16, 16, 21, 3, -4},
      {-11, 12, 21, 1, -2}, {-13, 14, 21, 1, -2}, {-11, 12, 21, 3, -4}, {-13,
      14, 21, 3, -4}, {-12, 11, 21, 2, -1}, {-14, 13, 21, 2, -1}, {-12, 11, 21,
      4, -3}, {-14, 13, 21, 4, -3}, {-12, 12, 21, 2, -2}, {-12, 12, 21, 4, -4},
      {-14, 14, 21, 2, -2}, {-14, 14, 21, 4, -4}, {-16, 16, 21, 2, -2}, {-16,
      16, 21, 4, -4}, {-12, 12, 21, 2, -2}, {-12, 12, 21, 4, -4}, {-14, 14, 21,
      2, -2}, {-14, 14, 21, 4, -4}, {-16, 16, 21, 2, -2}, {-16, 16, 21, 4, -4},
      {-12, 12, 21, 3, -3}, {-14, 14, 21, 3, -3}, {-16, 16, 21, 3, -3}, {-12,
      12, 21, 1, -1}, {-14, 14, 21, 1, -1}, {-16, 16, 21, 1, -1}, {-12, 12, 21,
      1, -3}, {-14, 14, 21, 1, -3}, {-16, 16, 21, 1, -3}, {-12, 12, 21, 3, -1},
      {-14, 14, 21, 3, -1}, {-16, 16, 21, 3, -1}, {-12, 11, 21, -2, -1}, {-14,
      13, 21, -2, -1}, {-12, 11, 21, -4, -3}, {-14, 13, 21, -4, -3}, {-12, 12,
      21, -2, -4}, {-14, 14, 21, -2, -4}, {-16, 16, 21, -2, -4}, {-11, 12, 21,
      -2, -2}, {-13, 14, 21, -2, -2}, {-11, 12, 21, -4, -4}, {-13, 14, 21, -4,
      -4}, {-12, 11, 21, -1, -1}, {-14, 13, 21, -1, -1}, {-12, 11, 21, -3, -3},
      {-14, 13, 21, -3, -3}, {-12, 12, 21, -2, -1}, {-14, 14, 21, -2, -1},
      {-16, 16, 21, -2, -1}, {-12, 12, 21, -2, -3}, {-14, 14, 21, -2, -3},
      {-16, 16, 21, -2, -3}, {-12, 12, 21, -4, -1}, {-14, 14, 21, -4, -1},
      {-16, 16, 21, -4, -1}, {-12, 12, 21, -4, -3}, {-14, 14, 21, -4, -3},
      {-16, 16, 21, -4, -3}, {-11, 12, 21, -2, -1}, {-13, 14, 21, -2, -1},
      {-11, 12, 21, -4, -3}, {-13, 14, 21, -4, -3}, {-12, 12, 21, -1, -3},
      {-14, 14, 21, -1, -3}, {-16, 16, 21, -1, -3}, {-11, 12, 21, 2, 3}, {-13,
      14, 21, 2, 3}, {-11, 12, 21, 4, 1}, {-13, 14, 21, 4, 1}, {-11, 12, 21, 3,
      1}, {-13, 14, 21, 3, 1}, {-11, 12, 21, 1, 3}, {-13, 14, 21, 1, 3}, {-12,
      11, 21, 2, 4}, {-14, 13, 21, 2, 4}, {-12, 11, 21, 4, 2}, {-14, 13, 21, 4,
      2}, {-12, 11, 21, 1, 4}, {-14, 13, 21, 1, 4}, {-11, 12, 21, 3, -4}, {-13,
      14, 21, 3, -4}, {-11, 12, 21, 1, -2}, {-13, 14, 21, 1, -2}, {-11, 12, 21,
      3, -4}, {-13, 14, 21, 3, -4}, {-11, 12, 21, 1, -2}, {-13, 14, 21, 1, -2},
      {-12, 11, 21, 4, -3}, {-14, 13, 21, 4, -3}, {-12, 11, 21, 2, -1}, {-14,
      13, 21, 2, -1}, {-12, 11, 21, 4, -3}, {-14, 13, 21, 4, -3}, {-12, 11, 21,
      2, -1}, {-14, 13, 21, 2, -1}, {-11, 12, 21, 1, -4}, {-13, 14, 21, 1, -4},
      {-11, 12, 21, 1, -3}, {-13, 14, 21, 1, -3}, {-11, 12, 21, 3, -2}, {-13,
      14, 21, 3, -2}, {-11, 12, 21, 3, -1}, {-13, 14, 21, 3, -1}, {-12, 11, 21,
      2, -3}, {-14, 13, 21, 2, -3}, {-12, 11, 21, 4, -1}, {-14, 13, 21, 4, -1},
      {-12, 11, 21, 1, -3}, {-14, 13, 21, 1, -3}, {-12, 11, 21, 3, -1}, {-14,
      13, 21, 3, -1}, {-11, 12, 21, 4, -4}, {-11, 12, 21, 3, -3}, {-13, 14, 21,
      4, -4}, {-13, 14, 21, 3, -3}, {-11, 12, 21, 2, -2}, {-11, 12, 21, 1, -1},
      {-13, 14, 21, 2, -2}, {-13, 14, 21, 1, -1}, {-11, 12, 21, 2, -4}, {-13,
      14, 21, 2, -4}, {-11, 12, 21, 4, -2}, {-13, 14, 21, 4, -2}, {-11, 12, 21,
      1, -4}, {-13, 14, 21, 1, -4}, {-11, 12, 21, 3, -2}, {-13, 14, 21, 3, -2},
      {-12, 11, 21, 2, 3}, {-14, 13, 21, 2, 3}, {-12, 11, 21, 4, -4}, {-12, 11,
      21, 3, -3}, {-14, 13, 21, 4, -4}, {-14, 13, 21, 3, -3}, {-12, 11, 21, 2,
      -2}, {-12, 11, 21, 1, -1}, {-14, 13, 21, 2, -2}, {-14, 13, 21, 1, -1},
      {-12, 11, 21, 2, -4}, {-14, 13, 21, 2, -4}, {-12, 11, 21, 2, -3}, {-14,
      13, 21, 2, -3}, {-12, 11, 21, 4, -2}, {-14, 13, 21, 4, -2}, {-12, 11, 21,
      4, -1}, {-14, 13, 21, 4, -1}, {-12, 11, 21, -2, -3}, {-14, 13, 21, -2,
      -3}, {-12, 11, 21, -4, -1}, {-14, 13, 21, -4, -1}, {-12, 11, 21, -3, -1},
      {-14, 13, 21, -3, -1}, {-12, 11, 21, -1, -3}, {-14, 13, 21, -1, -3},
      {-11, 12, 21, -2, -4}, {-13, 14, 21, -2, -4}, {-11, 12, 21, -4, -2},
      {-13, 14, 21, -4, -2}, {-11, 12, 21, -1, -4}, {-13, 14, 21, -1, -4},
      {-11, 12, 21, -2, -3}, {-13, 14, 21, -2, -3}, {-11, 11, 21, 2, -2}, {-13,
      13, 21, 2, -2}, {-11, 11, 21, 4, -4}, {-13, 13, 21, 4, -4}, {-11, 11, 21,
      1, -1}, {-13, 13, 21, 1, -1}, {-11, 11, 21, 3, -3}, {-13, 13, 21, 3, -3},
      {-11, 11, 21, 2, 4}, {-13, 13, 21, 2, 4}, {-11, 11, 21, 2, 1}, {-13, 13,
      21, 2, 1}, {-11, 11, 21, 2, 3}, {-13, 13, 21, 2, 3}, {-11, 11, 21, 4, 1},
      {-13, 13, 21, 4, 1}, {-11, 11, 21, 4, 3}, {-13, 13, 21, 4, 3}, {-11, 11,
      21, 4, -4}, {-13, 13, 21, 4, -4}, {-11, 11, 21, 2, -2}, {-13, 13, 21, 2,
      -2}, {-11, 11, 21, 1, -1}, {-11, 11, 21, 3, -3}, {-13, 13, 21, 1, -1},
      {-13, 13, 21, 3, -3}, {-11, 11, 21, 1, -1}, {-11, 11, 21, 3, -3}, {-13,
      13, 21, 1, -1}, {-13, 13, 21, 3, -3}, {-12, 12, 21, 2, -2}, {-14, 14, 21,
      2, -2}, {-16, 16, 21, 2, -2}, {-12, 12, 21, 4, -4}, {-14, 14, 21, 4, -4},
      {-16, 16, 21, 4, -4}, {-11, 11, 21, 2, -4}, {-13, 13, 21, 2, -4}, {-11,
      11, 21, 4, -2}, {-13, 13, 21, 4, -2}, {-11, 11, 21, 2, -1}, {-13, 13, 21,
      2, -1}, {-11, 11, 21, 2, -3}, {-13, 13, 21, 2, -3}, {-11, 11, 21, 4, -1},
      {-13, 13, 21, 4, -1}, {-11, 11, 21, 4, -3}, {-13, 13, 21, 4, -3}, {-11,
      11, 21, 1, 3}, {-13, 13, 21, 1, 3}, {-11, 11, 21, 1, -2}, {-13, 13, 21,
      1, -2}, {-11, 11, 21, 1, -4}, {-13, 13, 21, 1, -4}, {-11, 11, 21, 3, -2},
      {-13, 13, 21, 3, -2}, {-11, 11, 21, 3, -4}, {-13, 13, 21, 3, -4}, {-11,
      11, 21, 2, -2}, {-11, 11, 21, 4, -4}, {-13, 13, 21, 2, -2}, {-13, 13, 21,
      4, -4}, {-11, 11, 21, 2, -2}, {-11, 11, 21, 4, -4}, {-13, 13, 21, 2, -2},
      {-13, 13, 21, 4, -4}, {-11, 11, 21, 3, -3}, {-13, 13, 21, 3, -3}, {-11,
      11, 21, 1, -1}, {-13, 13, 21, 1, -1}, {-12, 12, 21, 1, -1}, {-14, 14, 21,
      1, -1}, {-16, 16, 21, 1, -1}, {-12, 12, 21, 3, -3}, {-14, 14, 21, 3, -3},
      {-16, 16, 21, 3, -3}, {-11, 11, 21, 1, -3}, {-13, 13, 21, 1, -3}, {-11,
      11, 21, 3, -1}, {-13, 13, 21, 3, -1}, {-11, 11, 21, -2, -4}, {-13, 13,
      21, -2, -4}, {-11, 11, 21, -2, -1}, {-13, 13, 21, -2, -1}, {-11, 11, 21,
      -2, -3}, {-13, 13, 21, -2, -3}, {-11, 11, 21, -4, -1}, {-13, 13, 21, -4,
      -1}, {-11, 11, 21, -4, -3}, {-13, 13, 21, -4, -3}, {-11, 11, 21, -1, -3},
      {-13, 13, 21, -1, -3}, {-12, 12, 21, 2, 4}, {-14, 14, 21, 2, 4}, {-16,
      16, 21, 2, 4}, {-11, 12, 21, 1, 1}, {-13, 14, 21, 1, 1}, {-11, 12, 21, 3,
      3}, {-13, 14, 21, 3, 3}, {-12, 11, 21, 2, 2}, {-14, 13, 21, 2, 2}, {-12,
      11, 21, 4, 4}, {-14, 13, 21, 4, 4}, {-12, 12, 21, 2, 1}, {-14, 14, 21, 2,
      1}, {-16, 16, 21, 2, 1}, {-12, 12, 21, 2, 3}, {-14, 14, 21, 2, 3}, {-16,
      16, 21, 2, 3}, {-12, 12, 21, 4, 1}, {-14, 14, 21, 4, 1}, {-16, 16, 21, 4,
      1}, {-12, 12, 21, 4, 3}, {-14, 14, 21, 4, 3}, {-16, 16, 21, 4, 3}, {-11,
      12, 21, 1, -2}, {-13, 14, 21, 1, -2}, {-11, 12, 21, 3, -4}, {-13, 14, 21,
      3, -4}, {-12, 11, 21, 2, -1}, {-14, 13, 21, 2, -1}, {-12, 11, 21, 4, -3},
      {-14, 13, 21, 4, -3}, {-12, 12, 21, 4, -4}, {-14, 14, 21, 4, -4}, {-16,
      16, 21, 4, -4}, {-12, 12, 21, 2, -2}, {-14, 14, 21, 2, -2}, {-16, 16, 21,
      2, -2}, {-12, 12, 21, 1, -1}, {-12, 12, 21, 3, -3}, {-14, 14, 21, 1, -1},
      {-14, 14, 21, 3, -3}, {-16, 16, 21, 1, -1}, {-16, 16, 21, 3, -3}, {-12,
      12, 21, 1, -1}, {-12, 12, 21, 3, -3}, {-14, 14, 21, 1, -1}, {-14, 14, 21,
      3, -3}, {-16, 16, 21, 1, -1}, {-16, 16, 21, 3, -3}, {-12, 12, 21, 2, -4},
      {-14, 14, 21, 2, -4}, {-16, 16, 21, 2, -4}, {-12, 12, 21, 4, -2}, {-14,
      14, 21, 4, -2}, {-16, 16, 21, 4, -2}, {-11, 12, 21, 2, -2}, {-13, 14, 21,
      2, -2}, {-11, 12, 21, 4, -4}, {-13, 14, 21, 4, -4}, {-11, 12, 21, 1, -1},
      {-13, 14, 21, 1, -1}, {-11, 12, 21, 3, -3}, {-13, 14, 21, 3, -3}, {-12,
      12, 21, 2, -1}, {-14, 14, 21, 2, -1}, {-16, 16, 21, 2, -1}, {-12, 12, 21,
      2, -3}, {-14, 14, 21, 2, -3}, {-16, 16, 21, 2, -3}, {-12, 12, 21, 4, -1},
      {-14, 14, 21, 4, -1}, {-16, 16, 21, 4, -1}, {-12, 12, 21, 4, -3}, {-14,
      14, 21, 4, -3}, {-16, 16, 21, 4, -3}, {-12, 12, 21, 1, 3}, {-14, 14, 21,
      1, 3}, {-16, 16, 21, 1, 3}, {-12, 11, 21, 2, -2}, {-14, 13, 21, 2, -2},
      {-12, 11, 21, 4, -4}, {-14, 13, 21, 4, -4}, {-12, 11, 21, 1, -1}, {-14,
      13, 21, 1, -1}, {-12, 11, 21, 3, -3}, {-14, 13, 21, 3, -3}, {-12, 12, 21,
      1, -2}, {-14, 14, 21, 1, -2}, {-16, 16, 21, 1, -2}, {-12, 12, 21, 1, -4},
      {-14, 14, 21, 1, -4}, {-16, 16, 21, 1, -4}, {-12, 12, 21, 3, -2}, {-14,
      14, 21, 3, -2}, {-16, 16, 21, 3, -2}, {-12, 12, 21, 3, -4}, {-14, 14, 21,
      3, -4}, {-16, 16, 21, 3, -4}, {-11, 12, 21, 1, -2}, {-13, 14, 21, 1, -2},
      {-11, 12, 21, 3, -4}, {-13, 14, 21, 3, -4}, {-12, 11, 21, 2, -1}, {-14,
      13, 21, 2, -1}, {-12, 11, 21, 4, -3}, {-14, 13, 21, 4, -3}, {-12, 12, 21,
      2, -2}, {-12, 12, 21, 4, -4}, {-14, 14, 21, 2, -2}, {-14, 14, 21, 4, -4},
      {-16, 16, 21, 2, -2}, {-16, 16, 21, 4, -4}, {-12, 12, 21, 2, -2}, {-12,
      12, 21, 4, -4}, {-14, 14, 21, 2, -2}, {-14, 14, 21, 4, -4}, {-16, 16, 21,
      2, -2}, {-16, 16, 21, 4, -4}, {-12, 12, 21, 3, -3}, {-14, 14, 21, 3, -3},
      {-16, 16, 21, 3, -3}, {-12, 12, 21, 1, -1}, {-14, 14, 21, 1, -1}, {-16,
      16, 21, 1, -1}, {-12, 12, 21, 1, -3}, {-14, 14, 21, 1, -3}, {-16, 16, 21,
      1, -3}, {-12, 12, 21, 3, -1}, {-14, 14, 21, 3, -1}, {-16, 16, 21, 3, -1},
      {-12, 12, 21, -2, -4}, {-14, 14, 21, -2, -4}, {-16, 16, 21, -2, -4},
      {-11, 12, 21, -2, -2}, {-13, 14, 21, -2, -2}, {-11, 12, 21, -4, -4},
      {-13, 14, 21, -4, -4}, {-12, 11, 21, -1, -1}, {-14, 13, 21, -1, -1},
      {-12, 11, 21, -3, -3}, {-14, 13, 21, -3, -3}, {-12, 12, 21, -2, -1},
      {-14, 14, 21, -2, -1}, {-16, 16, 21, -2, -1}, {-12, 12, 21, -2, -3},
      {-14, 14, 21, -2, -3}, {-16, 16, 21, -2, -3}, {-12, 12, 21, -4, -1},
      {-14, 14, 21, -4, -1}, {-16, 16, 21, -4, -1}, {-12, 12, 21, -4, -3},
      {-14, 14, 21, -4, -3}, {-16, 16, 21, -4, -3}, {-12, 12, 21, -1, -3},
      {-14, 14, 21, -1, -3}, {-16, 16, 21, -1, -3}, {-11, 12, 21, 2, 3}, {-13,
      14, 21, 2, 3}, {-11, 12, 21, 4, 1}, {-13, 14, 21, 4, 1}, {-11, 12, 21, 3,
      1}, {-13, 14, 21, 3, 1}, {-11, 12, 21, 1, 3}, {-13, 14, 21, 1, 3}, {-12,
      11, 21, 2, 4}, {-14, 13, 21, 2, 4}, {-12, 11, 21, 4, 2}, {-14, 13, 21, 4,
      2}, {-12, 11, 21, 1, 4}, {-14, 13, 21, 1, 4}, {-11, 12, 21, 3, -4}, {-13,
      14, 21, 3, -4}, {-11, 12, 21, 1, -2}, {-13, 14, 21, 1, -2}, {-11, 12, 21,
      3, -4}, {-13, 14, 21, 3, -4}, {-11, 12, 21, 1, -2}, {-13, 14, 21, 1, -2},
      {-12, 11, 21, 4, -3}, {-14, 13, 21, 4, -3}, {-12, 11, 21, 2, -1}, {-14,
      13, 21, 2, -1}, {-12, 11, 21, 4, -3}, {-14, 13, 21, 4, -3}, {-12, 11, 21,
      2, -1}, {-14, 13, 21, 2, -1}, {-11, 12, 21, 1, -4}, {-13, 14, 21, 1, -4},
      {-11, 12, 21, 1, -3}, {-13, 14, 21, 1, -3}, {-11, 12, 21, 3, -2}, {-13,
      14, 21, 3, -2}, {-11, 12, 21, 3, -1}, {-13, 14, 21, 3, -1}, {-12, 11, 21,
      2, -3}, {-14, 13, 21, 2, -3}, {-12, 11, 21, 4, -1}, {-14, 13, 21, 4, -1},
      {-12, 11, 21, 1, -3}, {-14, 13, 21, 1, -3}, {-12, 11, 21, 3, -1}, {-14,
      13, 21, 3, -1}, {-11, 12, 21, 4, -4}, {-11, 12, 21, 3, -3}, {-13, 14, 21,
      4, -4}, {-13, 14, 21, 3, -3}, {-11, 12, 21, 2, -2}, {-11, 12, 21, 1, -1},
      {-13, 14, 21, 2, -2}, {-13, 14, 21, 1, -1}, {-11, 12, 21, 2, -4}, {-13,
      14, 21, 2, -4}, {-11, 12, 21, 4, -2}, {-13, 14, 21, 4, -2}, {-11, 12, 21,
      1, -4}, {-13, 14, 21, 1, -4}, {-11, 12, 21, 3, -2}, {-13, 14, 21, 3, -2},
      {-12, 11, 21, 2, 3}, {-14, 13, 21, 2, 3}, {-12, 11, 21, 4, -4}, {-12, 11,
      21, 3, -3}, {-14, 13, 21, 4, -4}, {-14, 13, 21, 3, -3}, {-12, 11, 21, 2,
      -2}, {-12, 11, 21, 1, -1}, {-14, 13, 21, 2, -2}, {-14, 13, 21, 1, -1},
      {-12, 11, 21, 2, -4}, {-14, 13, 21, 2, -4}, {-12, 11, 21, 2, -3}, {-14,
      13, 21, 2, -3}, {-12, 11, 21, 4, -2}, {-14, 13, 21, 4, -2}, {-12, 11, 21,
      4, -1}, {-14, 13, 21, 4, -1}, {-12, 11, 21, -2, -3}, {-14, 13, 21, -2,
      -3}, {-12, 11, 21, -4, -1}, {-14, 13, 21, -4, -1}, {-12, 11, 21, -3, -1},
      {-14, 13, 21, -3, -1}, {-12, 11, 21, -1, -3}, {-14, 13, 21, -1, -3},
      {-11, 12, 21, -2, -4}, {-13, 14, 21, -2, -4}, {-11, 12, 21, -4, -2},
      {-13, 14, 21, -4, -2}, {-11, 12, 21, -1, -4}, {-13, 14, 21, -1, -4},
      {-11, 12, 21, -2, -3}, {-13, 14, 21, -2, -3}};

  bool in_pdgs_used[ninitial]; 
  bool out_pdgs_used[nexternal - ninitial]; 
  for(unsigned int i = 0; i < nprocs; i++ )
  {
    int permutations[nexternal]; 

    // Reinitialize initial state look-up variables
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      in_pdgs_used[j] = false; 
      permutations[j] = -1; 
    }
    // Look for initial state matches
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      for(unsigned int k = 0; k < ninitial; k++ )
      {
        // Make sure it has not been used already
        if (in_pdgs_used[k])
          continue; 
        if (initial_pdgs[k] == in_pdgs[i][j])
        {
          permutations[j] = k; 
          in_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[ninitial - 1] == -1)
      continue; 

    // Reinitialize final state look-up variables
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      out_pdgs_used[j] = false; 
      permutations[ninitial + j] = -1; 
    }
    // Look for final state matches
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      for(unsigned int k = 0; k < (nexternal - ninitial); k++ )
      {
        // Make sure it has not been used already
        if (out_pdgs_used[k])
          continue; 
        if (final_pdgs[k] == out_pdgs[i][j])
        {
          permutations[ninitial + j] = ninitial + k; 
          out_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[ninitial + j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[nexternal - 1] == -1)
      continue; 

    // Return process found
    return pair < vector<int> , int > (vector<int> (permutations, permutations
        + nexternal), proc_IDS[i]);
  }

  // No process found
  return NA; 
}

//--------------------------------------------------------------------------
// Set momenta
void PY8MEs_R4_P76_sm_qq_llgqq::setMomenta(vector < vec_double > momenta_picked)
{
  if (momenta_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setMomenta' of class" << 
    " 'PY8MEs_R4_P76_sm_qq_llgqq': Incorrect number of" << 
    " momenta specified." << endl; 
    throw PY8MEs_R4_P76_sm_qq_llgqq_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    if (momenta_picked[i].size() != 4)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R4_P76_sm_qq_llgqq': Incorrect number of" << 
      " momenta components specified." << endl; 
      throw PY8MEs_R4_P76_sm_qq_llgqq_exception; 
    }
    if (momenta_picked[i][0] < 0.0)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R4_P76_sm_qq_llgqq': A momentum was specified" << 
      " with negative energy. Check conventions." << endl; 
      throw PY8MEs_R4_P76_sm_qq_llgqq_exception; 
    }
    for (unsigned int j = 0; j < 4; j++ )
    {
      p[i][j] = momenta_picked[i][j]; 
    }
  }
}

//--------------------------------------------------------------------------
// Set color configuration to use. An empty vector means sum over all.
void PY8MEs_R4_P76_sm_qq_llgqq::setColors(vector<int> colors_picked)
{
  if (colors_picked.size() == 0)
  {
    user_colors = vector<int> (); 
    return; 
  }
  if (colors_picked.size() != (2 * nexternal))
  {
    cerr <<  "Error in function 'setColors' of class" << 
    " 'PY8MEs_R4_P76_sm_qq_llgqq': Incorrect number" << 
    " of colors specified." << endl; 
    throw PY8MEs_R4_P76_sm_qq_llgqq_exception; 
  }
  user_colors = vector<int> ((2 * nexternal), 0); 
  for(unsigned int i = 0; i < (2 * nexternal); i++ )
  {
    user_colors[i] = colors_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the helicity configuration to use. Am empty vector means sum over all.
void PY8MEs_R4_P76_sm_qq_llgqq::setHelicities(vector<int> helicities_picked) 
{
  if (helicities_picked.size() != nexternal)
  {
    if (helicities_picked.size() == 0)
    {
      user_helicities = vector<int> (); 
      return; 
    }
    cerr <<  "Error in function 'setHelicities' of class" << 
    " 'PY8MEs_R4_P76_sm_qq_llgqq': Incorrect number" << 
    " of helicities specified." << endl; 
    throw PY8MEs_R4_P76_sm_qq_llgqq_exception; 
  }
  user_helicities = vector<int> (nexternal, 0); 
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    user_helicities[i] = helicities_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the permutation to use (will apply to momenta, colors and helicities)
void PY8MEs_R4_P76_sm_qq_llgqq::setPermutation(vector<int> perm_picked) 
{
  if (perm_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setPermutations' of class" << 
    " 'PY8MEs_R4_P76_sm_qq_llgqq': Incorrect number" << 
    " of permutations specified." << endl; 
    throw PY8MEs_R4_P76_sm_qq_llgqq_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    perm[i] = perm_picked[i]; 
  }
}

// Set the proc_ID to use
void PY8MEs_R4_P76_sm_qq_llgqq::setProcID(int procID_picked) 
{
  proc_ID = procID_picked; 
}

//--------------------------------------------------------------------------
// Initialize process.

void PY8MEs_R4_P76_sm_qq_llgqq::initProc() 
{

  // Initialize flags
  include_symmetry_factors = true; 
  include_helicity_averaging_factors = true; 
  include_color_averaging_factors = true; 

  // Initialize vectors.
  perm = vector<int> (nexternal, 0); 
  user_colors = vector<int> (2 * nexternal, 0); 
  user_helicities = vector<int> (nexternal, 0); 
  p = vector < double * > (); 
  for (unsigned int i = 0; i < nexternal; i++ )
  {
    p.push_back(new double[4]); 
  }
  initColorConfigs(); 
  // Synchronize local variables dependent on the model with the active model.
  mME = vector<double> (nexternal, 0.); 
  syncProcModelParams(); 
  jamp2 = vector < vec_double > (72); 
  jamp2[0] = vector<double> (4, 0.); 
  jamp2[1] = vector<double> (4, 0.); 
  jamp2[2] = vector<double> (4, 0.); 
  jamp2[3] = vector<double> (4, 0.); 
  jamp2[4] = vector<double> (4, 0.); 
  jamp2[5] = vector<double> (4, 0.); 
  jamp2[6] = vector<double> (4, 0.); 
  jamp2[7] = vector<double> (4, 0.); 
  jamp2[8] = vector<double> (4, 0.); 
  jamp2[9] = vector<double> (4, 0.); 
  jamp2[10] = vector<double> (4, 0.); 
  jamp2[11] = vector<double> (4, 0.); 
  jamp2[12] = vector<double> (4, 0.); 
  jamp2[13] = vector<double> (4, 0.); 
  jamp2[14] = vector<double> (4, 0.); 
  jamp2[15] = vector<double> (4, 0.); 
  jamp2[16] = vector<double> (4, 0.); 
  jamp2[17] = vector<double> (4, 0.); 
  jamp2[18] = vector<double> (4, 0.); 
  jamp2[19] = vector<double> (4, 0.); 
  jamp2[20] = vector<double> (4, 0.); 
  jamp2[21] = vector<double> (4, 0.); 
  jamp2[22] = vector<double> (4, 0.); 
  jamp2[23] = vector<double> (4, 0.); 
  jamp2[24] = vector<double> (4, 0.); 
  jamp2[25] = vector<double> (4, 0.); 
  jamp2[26] = vector<double> (4, 0.); 
  jamp2[27] = vector<double> (4, 0.); 
  jamp2[28] = vector<double> (4, 0.); 
  jamp2[29] = vector<double> (4, 0.); 
  jamp2[30] = vector<double> (4, 0.); 
  jamp2[31] = vector<double> (4, 0.); 
  jamp2[32] = vector<double> (4, 0.); 
  jamp2[33] = vector<double> (4, 0.); 
  jamp2[34] = vector<double> (4, 0.); 
  jamp2[35] = vector<double> (4, 0.); 
  jamp2[36] = vector<double> (4, 0.); 
  jamp2[37] = vector<double> (4, 0.); 
  jamp2[38] = vector<double> (4, 0.); 
  jamp2[39] = vector<double> (4, 0.); 
  jamp2[40] = vector<double> (4, 0.); 
  jamp2[41] = vector<double> (4, 0.); 
  jamp2[42] = vector<double> (4, 0.); 
  jamp2[43] = vector<double> (4, 0.); 
  jamp2[44] = vector<double> (4, 0.); 
  jamp2[45] = vector<double> (4, 0.); 
  jamp2[46] = vector<double> (4, 0.); 
  jamp2[47] = vector<double> (4, 0.); 
  jamp2[48] = vector<double> (4, 0.); 
  jamp2[49] = vector<double> (4, 0.); 
  jamp2[50] = vector<double> (4, 0.); 
  jamp2[51] = vector<double> (4, 0.); 
  jamp2[52] = vector<double> (4, 0.); 
  jamp2[53] = vector<double> (4, 0.); 
  jamp2[54] = vector<double> (4, 0.); 
  jamp2[55] = vector<double> (4, 0.); 
  jamp2[56] = vector<double> (4, 0.); 
  jamp2[57] = vector<double> (4, 0.); 
  jamp2[58] = vector<double> (4, 0.); 
  jamp2[59] = vector<double> (4, 0.); 
  jamp2[60] = vector<double> (4, 0.); 
  jamp2[61] = vector<double> (4, 0.); 
  jamp2[62] = vector<double> (4, 0.); 
  jamp2[63] = vector<double> (4, 0.); 
  jamp2[64] = vector<double> (4, 0.); 
  jamp2[65] = vector<double> (4, 0.); 
  jamp2[66] = vector<double> (4, 0.); 
  jamp2[67] = vector<double> (4, 0.); 
  jamp2[68] = vector<double> (4, 0.); 
  jamp2[69] = vector<double> (4, 0.); 
  jamp2[70] = vector<double> (4, 0.); 
  jamp2[71] = vector<double> (4, 0.); 
  all_results = vector < vec_vec_double > (72); 
  // The first entry is always the color or helicity avg/summed matrix element.
  all_results[0] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[1] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[2] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[3] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[4] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[5] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[6] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[7] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[8] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[9] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[10] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[11] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[12] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[13] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[14] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[15] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[16] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[17] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[18] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[19] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[20] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[21] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[22] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[23] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[24] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[25] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[26] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[27] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[28] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[29] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[30] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[31] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[32] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[33] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[34] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[35] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[36] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[37] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[38] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[39] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[40] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[41] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[42] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[43] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[44] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[45] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[46] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[47] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[48] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[49] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[50] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[51] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[52] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[53] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[54] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[55] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[56] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[57] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[58] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[59] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[60] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[61] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[62] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[63] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[64] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[65] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[66] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[67] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[68] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[69] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[70] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[71] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
}

// Synchronize local variables of the process that depend on the model
// parameters
void PY8MEs_R4_P76_sm_qq_llgqq::syncProcModelParams() 
{

  // Instantiate the model class and set parameters that stay fixed during run
  mME[0] = pars->ZERO; 
  mME[1] = pars->ZERO; 
  mME[2] = pars->ZERO; 
  mME[3] = pars->ZERO; 
  mME[4] = pars->ZERO; 
  mME[5] = pars->ZERO; 
  mME[6] = pars->ZERO; 
}

//--------------------------------------------------------------------------
// Setter allowing to force particular values for the external masses
void PY8MEs_R4_P76_sm_qq_llgqq::setMasses(vec_double external_masses) 
{

  if (external_masses.size() != mME.size())
  {
    cerr <<  "Error in function 'setMasses' of class" << 
    " 'PY8MEs_R4_P76_sm_qq_llgqq': Incorrect number of" << 
    " masses specified." << endl; 
    throw PY8MEs_R4_P76_sm_qq_llgqq_exception; 
  }
  for (unsigned int j = 0; j < mME.size(); j++ )
  {
    mME[j] = external_masses[perm[j]]; 
  }
}

//--------------------------------------------------------------------------
// Getter accessing external masses with the correct ordering
vector<double> PY8MEs_R4_P76_sm_qq_llgqq::getMasses() 
{

  vec_double external_masses; 
  vector<int> invertedPerm; 
  invertedPerm = invert_mapping(perm); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    external_masses.push_back(mME[invertedPerm[i]]); 
  }
  return external_masses; 

}


// Set all values of the external masses to float(-mode) where mode can be
// 0 : Mass taken from the model
// 1 : Mass taken from p_i^2 if not massless to begin with
// 2 : Mass always taken from p_i^2.
void PY8MEs_R4_P76_sm_qq_llgqq::setExternalMassesMode(int mode) 
{
  if (mode != 0 && mode != 1 && mode != 2)
  {
    cerr <<  "Error in function 'setExternalMassesMode' of class" << 
    " 'PY8MEs_R4_P76_sm_qq_llgqq': Incorrect mode selected :" << mode << 
    ". It must be either 0, 1 or 2" << endl; 
    throw PY8MEs_R4_P76_sm_qq_llgqq_exception; 
  }
  if (mode == 0)
  {
    syncProcModelParams(); 
  }
  else if (mode == 1)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      if (mME[j] != pars->ZERO)
      {
        mME[j] = -1.0; 
      }
    }
  }
  else if (mode == 2)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      mME[j] = -1.0; 
    }
  }
}

//--------------------------------------------------------------------------
// Evaluate the squared matrix element.

double PY8MEs_R4_P76_sm_qq_llgqq::sigmaKin() 
{
  // Set the parameters which change event by event
  pars->setDependentParameters(); 
  pars->setDependentCouplings(); 
  // Reset color flows
  for(int i = 0; i < 4; i++ )
    jamp2[0][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[1][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[2][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[3][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[4][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[5][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[6][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[7][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[8][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[9][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[10][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[11][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[12][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[13][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[14][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[15][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[16][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[17][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[18][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[19][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[20][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[21][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[22][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[23][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[24][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[25][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[26][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[27][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[28][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[29][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[30][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[31][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[32][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[33][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[34][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[35][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[36][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[37][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[38][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[39][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[40][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[41][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[42][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[43][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[44][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[45][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[46][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[47][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[48][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[49][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[50][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[51][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[52][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[53][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[54][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[55][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[56][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[57][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[58][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[59][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[60][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[61][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[62][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[63][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[64][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[65][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[66][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[67][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[68][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[69][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[70][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[71][i] = 0.; 

  // Save previous values of mME
  vector<double> saved_mME(mME.size(), 0.0); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (mME[i] < 0.0)
    {
      saved_mME[i] = mME[i]; 
      mME[i] = sqrt(abs(pow(p[perm[i]][0], 2) - 
      (pow(p[perm[i]][1], 2) + pow(p[perm[i]][2], 2) + pow(p[perm[i]][3],
          2))));
    }
  }

  // Local variables and constants
  const int max_tries = 10; 
  vector < vec_bool > goodhel(nprocesses, vec_bool(ncomb, false)); 
  vec_int ntry(nprocesses, 0); 
  double t = 0.; 
  double result = 0.; 

  if (ntry[proc_ID] <= max_tries)
    ntry[proc_ID] = ntry[proc_ID] + 1; 

  // Find which helicity configuration is asked for
  // -1 indicates one wants to sum over helicities
  int user_ihel = getHelicityIDForConfig(user_helicities); 

  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = getColorIDForConfig(user_colors); 

  // Reset the list of results that will be recomputed here
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      all_results[proc_ID][ihel + 1][icolor + 1] = 0.; 
    }
  }

  // Calculate the matrix element for all helicities
  // unless already detected as vanishing
  for(int ihel = 0; ihel < ncomb; ihel++ )
  {
    // Skip helicity if already detected as vanishing
    if ((ntry[proc_ID] >= max_tries) && !goodhel[proc_ID][ihel])
      continue; 

    // Also skip helicity if user asks for a specific one
    if ((ntry[proc_ID] >= max_tries) && user_ihel != -1 && user_ihel != ihel)
      continue; 

    calculate_wavefunctions(helicities[ihel]); 

    // Reset locally computed color flows
    for(int i = 0; i < 4; i++ )
      jamp2[0][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[1][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[2][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[3][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[4][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[5][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[6][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[7][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[8][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[9][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[10][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[11][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[12][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[13][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[14][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[15][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[16][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[17][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[18][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[19][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[20][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[21][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[22][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[23][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[24][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[25][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[26][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[27][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[28][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[29][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[30][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[31][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[32][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[33][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[34][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[35][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[36][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[37][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[38][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[39][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[40][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[41][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[42][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[43][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[44][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[45][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[46][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[47][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[48][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[49][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[50][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[51][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[52][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[53][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[54][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[55][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[56][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[57][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[58][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[59][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[60][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[61][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[62][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[63][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[64][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[65][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[66][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[67][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[68][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[69][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[70][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[71][i] = 0.; 

    if (proc_ID == 0)
      t = matrix_4_uu_epemguu(); 
    if (proc_ID == 1)
      t = matrix_4_uux_epemguux(); 
    if (proc_ID == 2)
      t = matrix_4_dd_epemgdd(); 
    if (proc_ID == 3)
      t = matrix_4_ddx_epemgddx(); 
    if (proc_ID == 4)
      t = matrix_4_uxux_epemguxux(); 
    if (proc_ID == 5)
      t = matrix_4_dxdx_epemgdxdx(); 
    if (proc_ID == 6)
      t = matrix_4_uu_vexveguu(); 
    if (proc_ID == 7)
      t = matrix_4_uc_epemguc(); 
    if (proc_ID == 8)
      t = matrix_4_ud_epemgud(); 
    if (proc_ID == 9)
      t = matrix_4_uux_epemgccx(); 
    if (proc_ID == 10)
      t = matrix_4_uux_epemgddx(); 
    if (proc_ID == 11)
      t = matrix_4_uux_vexveguux(); 
    if (proc_ID == 12)
      t = matrix_4_ucx_epemgucx(); 
    if (proc_ID == 13)
      t = matrix_4_udx_epemgudx(); 
    if (proc_ID == 14)
      t = matrix_4_dd_vexvegdd(); 
    if (proc_ID == 15)
      t = matrix_4_ds_epemgds(); 
    if (proc_ID == 16)
      t = matrix_4_dux_epemgdux(); 
    if (proc_ID == 17)
      t = matrix_4_ddx_epemguux(); 
    if (proc_ID == 18)
      t = matrix_4_ddx_epemgssx(); 
    if (proc_ID == 19)
      t = matrix_4_ddx_vexvegddx(); 
    if (proc_ID == 20)
      t = matrix_4_dsx_epemgdsx(); 
    if (proc_ID == 21)
      t = matrix_4_uxux_vexveguxux(); 
    if (proc_ID == 22)
      t = matrix_4_uxcx_epemguxcx(); 
    if (proc_ID == 23)
      t = matrix_4_uxdx_epemguxdx(); 
    if (proc_ID == 24)
      t = matrix_4_dxdx_vexvegdxdx(); 
    if (proc_ID == 25)
      t = matrix_4_dxsx_epemgdxsx(); 
    if (proc_ID == 26)
      t = matrix_4_uu_epvegud(); 
    if (proc_ID == 27)
      t = matrix_4_uc_vexveguc(); 
    if (proc_ID == 28)
      t = matrix_4_ud_epvegdd(); 
    if (proc_ID == 29)
      t = matrix_4_ud_vexemguu(); 
    if (proc_ID == 30)
      t = matrix_4_ud_vexvegud(); 
    if (proc_ID == 31)
      t = matrix_4_uux_epvegdux(); 
    if (proc_ID == 32)
      t = matrix_4_uux_vexemgudx(); 
    if (proc_ID == 33)
      t = matrix_4_uux_vexvegccx(); 
    if (proc_ID == 34)
      t = matrix_4_uux_vexvegddx(); 
    if (proc_ID == 35)
      t = matrix_4_ucx_vexvegucx(); 
    if (proc_ID == 36)
      t = matrix_4_udx_epveguux(); 
    if (proc_ID == 37)
      t = matrix_4_udx_epvegddx(); 
    if (proc_ID == 38)
      t = matrix_4_udx_vexvegudx(); 
    if (proc_ID == 39)
      t = matrix_4_dd_vexemgud(); 
    if (proc_ID == 40)
      t = matrix_4_ds_vexvegds(); 
    if (proc_ID == 41)
      t = matrix_4_dux_vexemguux(); 
    if (proc_ID == 42)
      t = matrix_4_dux_vexemgddx(); 
    if (proc_ID == 43)
      t = matrix_4_dux_vexvegdux(); 
    if (proc_ID == 44)
      t = matrix_4_ddx_epvegdux(); 
    if (proc_ID == 45)
      t = matrix_4_ddx_vexemgudx(); 
    if (proc_ID == 46)
      t = matrix_4_ddx_vexveguux(); 
    if (proc_ID == 47)
      t = matrix_4_ddx_vexvegssx(); 
    if (proc_ID == 48)
      t = matrix_4_dsx_vexvegdsx(); 
    if (proc_ID == 49)
      t = matrix_4_uxux_vexemguxdx(); 
    if (proc_ID == 50)
      t = matrix_4_uxcx_vexveguxcx(); 
    if (proc_ID == 51)
      t = matrix_4_uxdx_epveguxux(); 
    if (proc_ID == 52)
      t = matrix_4_uxdx_vexemgdxdx(); 
    if (proc_ID == 53)
      t = matrix_4_uxdx_vexveguxdx(); 
    if (proc_ID == 54)
      t = matrix_4_dxdx_epveguxdx(); 
    if (proc_ID == 55)
      t = matrix_4_dxsx_vexvegdxsx(); 
    if (proc_ID == 56)
      t = matrix_4_uc_epvegus(); 
    if (proc_ID == 57)
      t = matrix_4_uc_epvegcd(); 
    if (proc_ID == 58)
      t = matrix_4_us_vexemguc(); 
    if (proc_ID == 59)
      t = matrix_4_uux_epvegscx(); 
    if (proc_ID == 60)
      t = matrix_4_uux_vexemgcsx(); 
    if (proc_ID == 61)
      t = matrix_4_ucx_epvegdcx(); 
    if (proc_ID == 62)
      t = matrix_4_ucx_vexemgusx(); 
    if (proc_ID == 63)
      t = matrix_4_udx_epvegccx(); 
    if (proc_ID == 64)
      t = matrix_4_usx_epvegucx(); 
    if (proc_ID == 65)
      t = matrix_4_ds_vexemgus(); 
    if (proc_ID == 66)
      t = matrix_4_dux_vexemgccx(); 
    if (proc_ID == 67)
      t = matrix_4_dcx_vexemgucx(); 
    if (proc_ID == 68)
      t = matrix_4_uxcx_vexemguxsx(); 
    if (proc_ID == 69)
      t = matrix_4_uxcx_vexemgcxdx(); 
    if (proc_ID == 70)
      t = matrix_4_uxsx_epveguxcx(); 
    if (proc_ID == 71)
      t = matrix_4_dxsx_epveguxsx(); 

    // Store which helicities give non-zero result
    if ((ntry[proc_ID] < max_tries) && t != 0. && !goodhel[proc_ID][ihel])
      goodhel[proc_ID][ihel] = true; 

    // Aggregate results
    if (user_ihel == -1 || user_ihel == ihel)
    {
      if (user_icol == -1)
      {
        result = result + t; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][0] += t; 
          for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
          {
            all_results[proc_ID][0][i + 1] += jamp2[proc_ID][i]; 
          }
        }
        all_results[proc_ID][ihel + 1][0] += t; 
        for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
        {
          all_results[proc_ID][ihel + 1][i + 1] += jamp2[proc_ID][i]; 
        }
      }
      else
      {
        result = result + jamp2[proc_ID][user_icol]; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][user_icol + 1] += jamp2[proc_ID][user_icol]; 
        }
        all_results[proc_ID][ihel + 1][user_icol + 1] +=
            jamp2[proc_ID][user_icol];
      }
    }

  }

  // Normalize results with the identical particle factor
  if (include_symmetry_factors)
  {
    result = result/denom_iden[proc_ID]; 
  }
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      if (include_symmetry_factors)
      {
        all_results[proc_ID][ihel + 1][icolor + 1] /= denom_iden[proc_ID]; 
      }
    }
  }


  // Normalize when when summing+averaging over helicity configurations
  if (user_ihel == -1 && include_helicity_averaging_factors)
  {
    result /= denom_hels[proc_ID]; 
    if (user_icol == -1)
    {
      all_results[proc_ID][0][0] /= denom_hels[proc_ID]; 
      for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
      {
        all_results[proc_ID][0][i + 1] /= denom_hels[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][0][user_icol + 1] /= denom_hels[proc_ID]; 
    }
  }

  // Normalize when summing+averaging over color configurations
  if (user_icol == -1 && include_color_averaging_factors)
  {
    result /= denom_colors[proc_ID]; 
    if (user_ihel == -1)
    {
      all_results[proc_ID][0][0] /= denom_colors[proc_ID]; 
      for (unsigned int i = 0; i < ncomb; i++ )
      {
        all_results[proc_ID][i + 1][0] /= denom_colors[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][user_ihel + 1][0] /= denom_colors[proc_ID]; 
    }
  }

  // Reinstate previous values of mME
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (saved_mME[i] < 0.0)
    {
      mME[i] = saved_mME[i]; 
    }
  }

  // Finally return it
  return result; 
}

//==========================================================================
// Private class member functions

//--------------------------------------------------------------------------
// Evaluate |M|^2 for each subprocess

void PY8MEs_R4_P76_sm_qq_llgqq::calculate_wavefunctions(const int hel[])
{
  // Calculate wavefunctions for all processes
  // Calculate all wavefunctions
  ixxxxx(p[perm[0]], mME[0], hel[0], +1, w[0]); 
  ixxxxx(p[perm[1]], mME[1], hel[1], +1, w[1]); 
  ixxxxx(p[perm[2]], mME[2], hel[2], -1, w[2]); 
  oxxxxx(p[perm[3]], mME[3], hel[3], +1, w[3]); 
  vxxxxx(p[perm[4]], mME[4], hel[4], +1, w[4]); 
  oxxxxx(p[perm[5]], mME[5], hel[5], +1, w[5]); 
  oxxxxx(p[perm[6]], mME[6], hel[6], +1, w[6]); 
  FFV1_2(w[0], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[7]); 
  FFV1P0_3(w[2], w[3], pars->GC_3, pars->ZERO, pars->ZERO, w[8]); 
  FFV1P0_3(w[7], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[9]); 
  FFV1_1(w[6], w[8], pars->GC_2, pars->ZERO, pars->ZERO, w[10]); 
  FFV1_2(w[1], w[8], pars->GC_2, pars->ZERO, pars->ZERO, w[11]); 
  FFV1P0_3(w[7], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[12]); 
  FFV1_1(w[5], w[8], pars->GC_2, pars->ZERO, pars->ZERO, w[13]); 
  FFV2_4_3(w[2], w[3], pars->GC_50, pars->GC_59, pars->mdl_MZ, pars->mdl_WZ,
      w[14]);
  FFV2_5_1(w[6], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[15]);
  FFV2_5_2(w[1], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[16]);
  FFV2_5_1(w[5], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[17]);
  FFV1P0_3(w[1], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[18]); 
  FFV1_2(w[7], w[8], pars->GC_2, pars->ZERO, pars->ZERO, w[19]); 
  FFV1_2(w[7], w[18], pars->GC_11, pars->ZERO, pars->ZERO, w[20]); 
  FFV2_5_2(w[7], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[21]);
  FFV1P0_3(w[1], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[22]); 
  FFV1_2(w[7], w[22], pars->GC_11, pars->ZERO, pars->ZERO, w[23]); 
  FFV1_1(w[5], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[24]); 
  FFV1P0_3(w[0], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[25]); 
  FFV1_1(w[24], w[25], pars->GC_11, pars->ZERO, pars->ZERO, w[26]); 
  FFV1_1(w[24], w[8], pars->GC_2, pars->ZERO, pars->ZERO, w[27]); 
  FFV2_5_1(w[24], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[28]);
  FFV1P0_3(w[0], w[24], pars->GC_11, pars->ZERO, pars->ZERO, w[29]); 
  FFV1P0_3(w[1], w[24], pars->GC_11, pars->ZERO, pars->ZERO, w[30]); 
  FFV1_2(w[0], w[8], pars->GC_2, pars->ZERO, pars->ZERO, w[31]); 
  FFV2_5_2(w[0], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[32]);
  FFV1_1(w[24], w[22], pars->GC_11, pars->ZERO, pars->ZERO, w[33]); 
  FFV1_1(w[6], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[34]); 
  FFV1P0_3(w[0], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[35]); 
  FFV1_1(w[34], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[36]); 
  FFV1_1(w[34], w[8], pars->GC_2, pars->ZERO, pars->ZERO, w[37]); 
  FFV2_5_1(w[34], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[38]);
  FFV1P0_3(w[0], w[34], pars->GC_11, pars->ZERO, pars->ZERO, w[39]); 
  FFV1P0_3(w[1], w[34], pars->GC_11, pars->ZERO, pars->ZERO, w[40]); 
  FFV1_1(w[34], w[18], pars->GC_11, pars->ZERO, pars->ZERO, w[41]); 
  FFV1_2(w[1], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[42]); 
  FFV1_2(w[42], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[43]); 
  FFV1_2(w[42], w[8], pars->GC_2, pars->ZERO, pars->ZERO, w[44]); 
  FFV2_5_2(w[42], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[45]);
  FFV1_2(w[42], w[25], pars->GC_11, pars->ZERO, pars->ZERO, w[46]); 
  FFV1P0_3(w[42], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[47]); 
  FFV1P0_3(w[42], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[48]); 
  VVV1P0_1(w[4], w[35], pars->GC_10, pars->ZERO, pars->ZERO, w[49]); 
  FFV1_1(w[6], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[50]); 
  FFV1_2(w[1], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[51]); 
  VVV1P0_1(w[4], w[25], pars->GC_10, pars->ZERO, pars->ZERO, w[52]); 
  FFV1_1(w[5], w[25], pars->GC_11, pars->ZERO, pars->ZERO, w[53]); 
  FFV1_2(w[1], w[25], pars->GC_11, pars->ZERO, pars->ZERO, w[54]); 
  VVV1P0_1(w[4], w[18], pars->GC_10, pars->ZERO, pars->ZERO, w[55]); 
  FFV1_1(w[6], w[18], pars->GC_11, pars->ZERO, pars->ZERO, w[56]); 
  FFV1_2(w[0], w[18], pars->GC_11, pars->ZERO, pars->ZERO, w[57]); 
  VVV1P0_1(w[4], w[22], pars->GC_10, pars->ZERO, pars->ZERO, w[58]); 
  FFV1_1(w[5], w[22], pars->GC_11, pars->ZERO, pars->ZERO, w[59]); 
  FFV1_2(w[0], w[22], pars->GC_11, pars->ZERO, pars->ZERO, w[60]); 
  oxxxxx(p[perm[1]], mME[1], hel[1], -1, w[61]); 
  ixxxxx(p[perm[6]], mME[6], hel[6], -1, w[62]); 
  FFV1P0_3(w[7], w[61], pars->GC_11, pars->ZERO, pars->ZERO, w[63]); 
  FFV1_2(w[62], w[8], pars->GC_2, pars->ZERO, pars->ZERO, w[64]); 
  FFV1_1(w[61], w[8], pars->GC_2, pars->ZERO, pars->ZERO, w[65]); 
  FFV2_5_2(w[62], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[66]);
  FFV2_5_1(w[61], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[67]);
  FFV1P0_3(w[62], w[61], pars->GC_11, pars->ZERO, pars->ZERO, w[68]); 
  FFV1_2(w[7], w[68], pars->GC_11, pars->ZERO, pars->ZERO, w[69]); 
  FFV1P0_3(w[62], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[70]); 
  FFV1_2(w[7], w[70], pars->GC_11, pars->ZERO, pars->ZERO, w[71]); 
  FFV1_1(w[61], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[72]); 
  FFV1_1(w[72], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[73]); 
  FFV1_1(w[72], w[8], pars->GC_2, pars->ZERO, pars->ZERO, w[74]); 
  FFV2_5_1(w[72], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[75]);
  FFV1P0_3(w[0], w[72], pars->GC_11, pars->ZERO, pars->ZERO, w[76]); 
  FFV1P0_3(w[62], w[72], pars->GC_11, pars->ZERO, pars->ZERO, w[77]); 
  FFV1_1(w[72], w[70], pars->GC_11, pars->ZERO, pars->ZERO, w[78]); 
  FFV1P0_3(w[0], w[61], pars->GC_11, pars->ZERO, pars->ZERO, w[79]); 
  FFV1_1(w[24], w[79], pars->GC_11, pars->ZERO, pars->ZERO, w[80]); 
  FFV1P0_3(w[62], w[24], pars->GC_11, pars->ZERO, pars->ZERO, w[81]); 
  FFV1_1(w[24], w[68], pars->GC_11, pars->ZERO, pars->ZERO, w[82]); 
  FFV1_2(w[62], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[83]); 
  FFV1_2(w[83], w[79], pars->GC_11, pars->ZERO, pars->ZERO, w[84]); 
  FFV1_2(w[83], w[8], pars->GC_2, pars->ZERO, pars->ZERO, w[85]); 
  FFV2_5_2(w[83], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[86]);
  FFV1_2(w[83], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[87]); 
  FFV1P0_3(w[83], w[61], pars->GC_11, pars->ZERO, pars->ZERO, w[88]); 
  FFV1P0_3(w[83], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[89]); 
  VVV1P0_1(w[4], w[79], pars->GC_10, pars->ZERO, pars->ZERO, w[90]); 
  FFV1_1(w[5], w[79], pars->GC_11, pars->ZERO, pars->ZERO, w[91]); 
  FFV1_2(w[62], w[79], pars->GC_11, pars->ZERO, pars->ZERO, w[92]); 
  FFV1_1(w[61], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[93]); 
  FFV1_2(w[62], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[94]); 
  VVV1P0_1(w[4], w[68], pars->GC_10, pars->ZERO, pars->ZERO, w[95]); 
  FFV1_1(w[5], w[68], pars->GC_11, pars->ZERO, pars->ZERO, w[96]); 
  FFV1_2(w[0], w[68], pars->GC_11, pars->ZERO, pars->ZERO, w[97]); 
  VVV1P0_1(w[4], w[70], pars->GC_10, pars->ZERO, pars->ZERO, w[98]); 
  FFV1_1(w[61], w[70], pars->GC_11, pars->ZERO, pars->ZERO, w[99]); 
  FFV1_2(w[0], w[70], pars->GC_11, pars->ZERO, pars->ZERO, w[100]); 
  FFV1_1(w[6], w[8], pars->GC_1, pars->ZERO, pars->ZERO, w[101]); 
  FFV1_2(w[1], w[8], pars->GC_1, pars->ZERO, pars->ZERO, w[102]); 
  FFV1_1(w[5], w[8], pars->GC_1, pars->ZERO, pars->ZERO, w[103]); 
  FFV2_3_1(w[6], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[104]);
  FFV2_3_2(w[1], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[105]);
  FFV2_3_1(w[5], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[106]);
  FFV1_2(w[7], w[8], pars->GC_1, pars->ZERO, pars->ZERO, w[107]); 
  FFV2_3_2(w[7], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[108]);
  FFV1_1(w[24], w[8], pars->GC_1, pars->ZERO, pars->ZERO, w[109]); 
  FFV2_3_1(w[24], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[110]);
  FFV1_2(w[0], w[8], pars->GC_1, pars->ZERO, pars->ZERO, w[111]); 
  FFV2_3_2(w[0], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[112]);
  FFV1_1(w[34], w[8], pars->GC_1, pars->ZERO, pars->ZERO, w[113]); 
  FFV2_3_1(w[34], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[114]);
  FFV1_2(w[42], w[8], pars->GC_1, pars->ZERO, pars->ZERO, w[115]); 
  FFV2_3_2(w[42], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[116]);
  FFV1_2(w[62], w[8], pars->GC_1, pars->ZERO, pars->ZERO, w[117]); 
  FFV1_1(w[61], w[8], pars->GC_1, pars->ZERO, pars->ZERO, w[118]); 
  FFV2_3_2(w[62], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[119]);
  FFV2_3_1(w[61], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[120]);
  FFV1_1(w[72], w[8], pars->GC_1, pars->ZERO, pars->ZERO, w[121]); 
  FFV2_3_1(w[72], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[122]);
  FFV1_2(w[83], w[8], pars->GC_1, pars->ZERO, pars->ZERO, w[123]); 
  FFV2_3_2(w[83], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[124]);
  oxxxxx(p[perm[0]], mME[0], hel[0], -1, w[125]); 
  ixxxxx(p[perm[5]], mME[5], hel[5], -1, w[126]); 
  FFV1_2(w[126], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[127]); 
  FFV1P0_3(w[127], w[125], pars->GC_11, pars->ZERO, pars->ZERO, w[128]); 
  FFV1P0_3(w[127], w[61], pars->GC_11, pars->ZERO, pars->ZERO, w[129]); 
  FFV1_1(w[125], w[8], pars->GC_2, pars->ZERO, pars->ZERO, w[130]); 
  FFV2_5_1(w[125], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[131]);
  FFV1P0_3(w[62], w[125], pars->GC_11, pars->ZERO, pars->ZERO, w[132]); 
  FFV1_2(w[127], w[8], pars->GC_2, pars->ZERO, pars->ZERO, w[133]); 
  FFV1_2(w[127], w[132], pars->GC_11, pars->ZERO, pars->ZERO, w[134]); 
  FFV2_5_2(w[127], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[135]);
  FFV1_2(w[127], w[68], pars->GC_11, pars->ZERO, pars->ZERO, w[136]); 
  FFV1_1(w[125], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[137]); 
  FFV1P0_3(w[126], w[61], pars->GC_11, pars->ZERO, pars->ZERO, w[138]); 
  FFV1_1(w[137], w[138], pars->GC_11, pars->ZERO, pars->ZERO, w[139]); 
  FFV1_1(w[137], w[8], pars->GC_2, pars->ZERO, pars->ZERO, w[140]); 
  FFV2_5_1(w[137], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[141]);
  FFV1P0_3(w[126], w[137], pars->GC_11, pars->ZERO, pars->ZERO, w[142]); 
  FFV1P0_3(w[62], w[137], pars->GC_11, pars->ZERO, pars->ZERO, w[143]); 
  FFV1_2(w[126], w[8], pars->GC_2, pars->ZERO, pars->ZERO, w[144]); 
  FFV2_5_2(w[126], w[14], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[145]);
  FFV1_1(w[137], w[68], pars->GC_11, pars->ZERO, pars->ZERO, w[146]); 
  FFV1P0_3(w[126], w[125], pars->GC_11, pars->ZERO, pars->ZERO, w[147]); 
  FFV1_1(w[72], w[147], pars->GC_11, pars->ZERO, pars->ZERO, w[148]); 
  FFV1P0_3(w[126], w[72], pars->GC_11, pars->ZERO, pars->ZERO, w[149]); 
  FFV1_1(w[72], w[132], pars->GC_11, pars->ZERO, pars->ZERO, w[150]); 
  FFV1_2(w[83], w[147], pars->GC_11, pars->ZERO, pars->ZERO, w[151]); 
  FFV1_2(w[83], w[138], pars->GC_11, pars->ZERO, pars->ZERO, w[152]); 
  FFV1P0_3(w[83], w[125], pars->GC_11, pars->ZERO, pars->ZERO, w[153]); 
  VVV1P0_1(w[4], w[147], pars->GC_10, pars->ZERO, pars->ZERO, w[154]); 
  FFV1_1(w[61], w[147], pars->GC_11, pars->ZERO, pars->ZERO, w[155]); 
  FFV1_2(w[62], w[147], pars->GC_11, pars->ZERO, pars->ZERO, w[156]); 
  VVV1P0_1(w[4], w[138], pars->GC_10, pars->ZERO, pars->ZERO, w[157]); 
  FFV1_1(w[125], w[138], pars->GC_11, pars->ZERO, pars->ZERO, w[158]); 
  FFV1_2(w[62], w[138], pars->GC_11, pars->ZERO, pars->ZERO, w[159]); 
  VVV1P0_1(w[4], w[132], pars->GC_10, pars->ZERO, pars->ZERO, w[160]); 
  FFV1_1(w[61], w[132], pars->GC_11, pars->ZERO, pars->ZERO, w[161]); 
  FFV1_2(w[126], w[132], pars->GC_11, pars->ZERO, pars->ZERO, w[162]); 
  FFV1_1(w[125], w[68], pars->GC_11, pars->ZERO, pars->ZERO, w[163]); 
  FFV1_2(w[126], w[68], pars->GC_11, pars->ZERO, pars->ZERO, w[164]); 
  FFV1_1(w[125], w[8], pars->GC_1, pars->ZERO, pars->ZERO, w[165]); 
  FFV2_3_1(w[125], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[166]);
  FFV1_2(w[127], w[8], pars->GC_1, pars->ZERO, pars->ZERO, w[167]); 
  FFV2_3_2(w[127], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[168]);
  FFV1_1(w[137], w[8], pars->GC_1, pars->ZERO, pars->ZERO, w[169]); 
  FFV2_3_1(w[137], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[170]);
  FFV1_2(w[126], w[8], pars->GC_1, pars->ZERO, pars->ZERO, w[171]); 
  FFV2_3_2(w[126], w[14], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[172]);
  FFV2_3(w[2], w[3], pars->GC_62, pars->mdl_MZ, pars->mdl_WZ, w[173]); 
  FFV2_5_1(w[6], w[173], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[174]);
  FFV2_5_2(w[1], w[173], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[175]);
  FFV2_5_1(w[5], w[173], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[176]);
  FFV2_5_2(w[7], w[173], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[177]);
  FFV2_5_1(w[24], w[173], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[178]);
  FFV2_5_2(w[0], w[173], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[179]);
  FFV2_5_1(w[34], w[173], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[180]);
  FFV2_5_2(w[42], w[173], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[181]);
  FFV2_5_2(w[62], w[173], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[182]);
  FFV2_5_1(w[61], w[173], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[183]);
  FFV2_5_1(w[72], w[173], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[184]);
  FFV2_5_2(w[83], w[173], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[185]);
  FFV2_3_1(w[6], w[173], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[186]);
  FFV2_3_2(w[1], w[173], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[187]);
  FFV2_3_1(w[5], w[173], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[188]);
  FFV2_3_2(w[7], w[173], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[189]);
  FFV2_3_1(w[24], w[173], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[190]);
  FFV2_3_2(w[0], w[173], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[191]);
  FFV2_3_1(w[34], w[173], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[192]);
  FFV2_3_2(w[42], w[173], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[193]);
  FFV2_3_2(w[62], w[173], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[194]);
  FFV2_3_1(w[61], w[173], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[195]);
  FFV2_3_1(w[72], w[173], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[196]);
  FFV2_3_2(w[83], w[173], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[197]);
  FFV2_5_1(w[125], w[173], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[198]);
  FFV2_5_2(w[127], w[173], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[199]);
  FFV2_5_1(w[137], w[173], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[200]);
  FFV2_5_2(w[126], w[173], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[201]);
  FFV2_3_1(w[125], w[173], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[202]);
  FFV2_3_2(w[127], w[173], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[203]);
  FFV2_3_1(w[137], w[173], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[204]);
  FFV2_3_2(w[126], w[173], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[205]);
  FFV2_3(w[2], w[3], pars->GC_100, pars->mdl_MW, pars->mdl_WW, w[206]); 
  FFV2_1(w[6], w[206], pars->GC_100, pars->ZERO, pars->ZERO, w[207]); 
  FFV2_2(w[1], w[206], pars->GC_100, pars->ZERO, pars->ZERO, w[208]); 
  FFV2_2(w[7], w[206], pars->GC_100, pars->ZERO, pars->ZERO, w[209]); 
  FFV2_2(w[0], w[206], pars->GC_100, pars->ZERO, pars->ZERO, w[210]); 
  FFV2_1(w[34], w[206], pars->GC_100, pars->ZERO, pars->ZERO, w[211]); 
  FFV2_2(w[42], w[206], pars->GC_100, pars->ZERO, pars->ZERO, w[212]); 
  FFV2_1(w[24], w[206], pars->GC_100, pars->ZERO, pars->ZERO, w[213]); 
  FFV2_1(w[5], w[206], pars->GC_100, pars->ZERO, pars->ZERO, w[214]); 
  FFV2_2(w[62], w[206], pars->GC_100, pars->ZERO, pars->ZERO, w[215]); 
  FFV2_2(w[83], w[206], pars->GC_100, pars->ZERO, pars->ZERO, w[216]); 
  FFV2_1(w[61], w[206], pars->GC_100, pars->ZERO, pars->ZERO, w[217]); 
  FFV2_1(w[72], w[206], pars->GC_100, pars->ZERO, pars->ZERO, w[218]); 
  FFV2_1(w[125], w[206], pars->GC_100, pars->ZERO, pars->ZERO, w[219]); 
  FFV2_1(w[137], w[206], pars->GC_100, pars->ZERO, pars->ZERO, w[220]); 
  FFV2_2(w[127], w[206], pars->GC_100, pars->ZERO, pars->ZERO, w[221]); 
  FFV2_2(w[126], w[206], pars->GC_100, pars->ZERO, pars->ZERO, w[222]); 

  // Calculate all amplitudes
  // Amplitude(s) for diagram number 0
  FFV1_0(w[1], w[10], w[9], pars->GC_11, amp[0]); 
  FFV1_0(w[11], w[6], w[9], pars->GC_11, amp[1]); 
  FFV1_0(w[1], w[13], w[12], pars->GC_11, amp[2]); 
  FFV1_0(w[11], w[5], w[12], pars->GC_11, amp[3]); 
  FFV1_0(w[1], w[15], w[9], pars->GC_11, amp[4]); 
  FFV1_0(w[16], w[6], w[9], pars->GC_11, amp[5]); 
  FFV1_0(w[1], w[17], w[12], pars->GC_11, amp[6]); 
  FFV1_0(w[16], w[5], w[12], pars->GC_11, amp[7]); 
  FFV1_0(w[19], w[6], w[18], pars->GC_11, amp[8]); 
  FFV1_0(w[20], w[6], w[8], pars->GC_2, amp[9]); 
  FFV1_0(w[21], w[6], w[18], pars->GC_11, amp[10]); 
  FFV2_5_0(w[20], w[6], w[14], pars->GC_51, pars->GC_58, amp[11]); 
  FFV1_0(w[19], w[5], w[22], pars->GC_11, amp[12]); 
  FFV1_0(w[23], w[5], w[8], pars->GC_2, amp[13]); 
  FFV1_0(w[21], w[5], w[22], pars->GC_11, amp[14]); 
  FFV2_5_0(w[23], w[5], w[14], pars->GC_51, pars->GC_58, amp[15]); 
  FFV1_0(w[1], w[26], w[8], pars->GC_2, amp[16]); 
  FFV1_0(w[1], w[27], w[25], pars->GC_11, amp[17]); 
  FFV2_5_0(w[1], w[26], w[14], pars->GC_51, pars->GC_58, amp[18]); 
  FFV1_0(w[1], w[28], w[25], pars->GC_11, amp[19]); 
  FFV1_0(w[1], w[10], w[29], pars->GC_11, amp[20]); 
  FFV1_0(w[11], w[6], w[29], pars->GC_11, amp[21]); 
  FFV1_0(w[31], w[6], w[30], pars->GC_11, amp[22]); 
  FFV1_0(w[0], w[10], w[30], pars->GC_11, amp[23]); 
  FFV1_0(w[1], w[15], w[29], pars->GC_11, amp[24]); 
  FFV1_0(w[16], w[6], w[29], pars->GC_11, amp[25]); 
  FFV1_0(w[32], w[6], w[30], pars->GC_11, amp[26]); 
  FFV1_0(w[0], w[15], w[30], pars->GC_11, amp[27]); 
  FFV1_0(w[0], w[27], w[22], pars->GC_11, amp[28]); 
  FFV1_0(w[0], w[33], w[8], pars->GC_2, amp[29]); 
  FFV1_0(w[0], w[28], w[22], pars->GC_11, amp[30]); 
  FFV2_5_0(w[0], w[33], w[14], pars->GC_51, pars->GC_58, amp[31]); 
  FFV1_0(w[1], w[36], w[8], pars->GC_2, amp[32]); 
  FFV1_0(w[1], w[37], w[35], pars->GC_11, amp[33]); 
  FFV2_5_0(w[1], w[36], w[14], pars->GC_51, pars->GC_58, amp[34]); 
  FFV1_0(w[1], w[38], w[35], pars->GC_11, amp[35]); 
  FFV1_0(w[1], w[13], w[39], pars->GC_11, amp[36]); 
  FFV1_0(w[11], w[5], w[39], pars->GC_11, amp[37]); 
  FFV1_0(w[31], w[5], w[40], pars->GC_11, amp[38]); 
  FFV1_0(w[0], w[13], w[40], pars->GC_11, amp[39]); 
  FFV1_0(w[1], w[17], w[39], pars->GC_11, amp[40]); 
  FFV1_0(w[16], w[5], w[39], pars->GC_11, amp[41]); 
  FFV1_0(w[32], w[5], w[40], pars->GC_11, amp[42]); 
  FFV1_0(w[0], w[17], w[40], pars->GC_11, amp[43]); 
  FFV1_0(w[0], w[37], w[18], pars->GC_11, amp[44]); 
  FFV1_0(w[0], w[41], w[8], pars->GC_2, amp[45]); 
  FFV1_0(w[0], w[38], w[18], pars->GC_11, amp[46]); 
  FFV2_5_0(w[0], w[41], w[14], pars->GC_51, pars->GC_58, amp[47]); 
  FFV1_0(w[43], w[6], w[8], pars->GC_2, amp[48]); 
  FFV1_0(w[44], w[6], w[35], pars->GC_11, amp[49]); 
  FFV2_5_0(w[43], w[6], w[14], pars->GC_51, pars->GC_58, amp[50]); 
  FFV1_0(w[45], w[6], w[35], pars->GC_11, amp[51]); 
  FFV1_0(w[46], w[5], w[8], pars->GC_2, amp[52]); 
  FFV1_0(w[44], w[5], w[25], pars->GC_11, amp[53]); 
  FFV2_5_0(w[46], w[5], w[14], pars->GC_51, pars->GC_58, amp[54]); 
  FFV1_0(w[45], w[5], w[25], pars->GC_11, amp[55]); 
  FFV1_0(w[31], w[6], w[47], pars->GC_11, amp[56]); 
  FFV1_0(w[0], w[10], w[47], pars->GC_11, amp[57]); 
  FFV1_0(w[31], w[5], w[48], pars->GC_11, amp[58]); 
  FFV1_0(w[0], w[13], w[48], pars->GC_11, amp[59]); 
  FFV1_0(w[32], w[6], w[47], pars->GC_11, amp[60]); 
  FFV1_0(w[0], w[15], w[47], pars->GC_11, amp[61]); 
  FFV1_0(w[32], w[5], w[48], pars->GC_11, amp[62]); 
  FFV1_0(w[0], w[17], w[48], pars->GC_11, amp[63]); 
  FFV1_0(w[1], w[10], w[49], pars->GC_11, amp[64]); 
  FFV1_0(w[11], w[6], w[49], pars->GC_11, amp[65]); 
  FFV1_0(w[11], w[50], w[4], pars->GC_11, amp[66]); 
  FFV1_0(w[51], w[10], w[4], pars->GC_11, amp[67]); 
  FFV1_0(w[1], w[15], w[49], pars->GC_11, amp[68]); 
  FFV1_0(w[16], w[6], w[49], pars->GC_11, amp[69]); 
  FFV1_0(w[16], w[50], w[4], pars->GC_11, amp[70]); 
  FFV1_0(w[51], w[15], w[4], pars->GC_11, amp[71]); 
  FFV1_0(w[1], w[13], w[52], pars->GC_11, amp[72]); 
  FFV1_0(w[11], w[5], w[52], pars->GC_11, amp[73]); 
  FFV1_0(w[11], w[53], w[4], pars->GC_11, amp[74]); 
  FFV1_0(w[54], w[13], w[4], pars->GC_11, amp[75]); 
  FFV1_0(w[1], w[17], w[52], pars->GC_11, amp[76]); 
  FFV1_0(w[16], w[5], w[52], pars->GC_11, amp[77]); 
  FFV1_0(w[16], w[53], w[4], pars->GC_11, amp[78]); 
  FFV1_0(w[54], w[17], w[4], pars->GC_11, amp[79]); 
  FFV1_0(w[31], w[6], w[55], pars->GC_11, amp[80]); 
  FFV1_0(w[0], w[10], w[55], pars->GC_11, amp[81]); 
  FFV1_0(w[31], w[56], w[4], pars->GC_11, amp[82]); 
  FFV1_0(w[57], w[10], w[4], pars->GC_11, amp[83]); 
  FFV1_0(w[32], w[6], w[55], pars->GC_11, amp[84]); 
  FFV1_0(w[0], w[15], w[55], pars->GC_11, amp[85]); 
  FFV1_0(w[32], w[56], w[4], pars->GC_11, amp[86]); 
  FFV1_0(w[57], w[15], w[4], pars->GC_11, amp[87]); 
  FFV1_0(w[31], w[5], w[58], pars->GC_11, amp[88]); 
  FFV1_0(w[0], w[13], w[58], pars->GC_11, amp[89]); 
  FFV1_0(w[31], w[59], w[4], pars->GC_11, amp[90]); 
  FFV1_0(w[60], w[13], w[4], pars->GC_11, amp[91]); 
  FFV1_0(w[32], w[5], w[58], pars->GC_11, amp[92]); 
  FFV1_0(w[0], w[17], w[58], pars->GC_11, amp[93]); 
  FFV1_0(w[32], w[59], w[4], pars->GC_11, amp[94]); 
  FFV1_0(w[60], w[17], w[4], pars->GC_11, amp[95]); 
  FFV1_0(w[62], w[13], w[63], pars->GC_11, amp[96]); 
  FFV1_0(w[64], w[5], w[63], pars->GC_11, amp[97]); 
  FFV1_0(w[62], w[65], w[9], pars->GC_11, amp[98]); 
  FFV1_0(w[64], w[61], w[9], pars->GC_11, amp[99]); 
  FFV1_0(w[62], w[17], w[63], pars->GC_11, amp[100]); 
  FFV1_0(w[66], w[5], w[63], pars->GC_11, amp[101]); 
  FFV1_0(w[62], w[67], w[9], pars->GC_11, amp[102]); 
  FFV1_0(w[66], w[61], w[9], pars->GC_11, amp[103]); 
  FFV1_0(w[19], w[5], w[68], pars->GC_11, amp[104]); 
  FFV1_0(w[69], w[5], w[8], pars->GC_2, amp[105]); 
  FFV1_0(w[21], w[5], w[68], pars->GC_11, amp[106]); 
  FFV2_5_0(w[69], w[5], w[14], pars->GC_51, pars->GC_58, amp[107]); 
  FFV1_0(w[19], w[61], w[70], pars->GC_11, amp[108]); 
  FFV1_0(w[71], w[61], w[8], pars->GC_2, amp[109]); 
  FFV1_0(w[21], w[61], w[70], pars->GC_11, amp[110]); 
  FFV2_5_0(w[71], w[61], w[14], pars->GC_51, pars->GC_58, amp[111]); 
  FFV1_0(w[62], w[73], w[8], pars->GC_2, amp[112]); 
  FFV1_0(w[62], w[74], w[35], pars->GC_11, amp[113]); 
  FFV2_5_0(w[62], w[73], w[14], pars->GC_51, pars->GC_58, amp[114]); 
  FFV1_0(w[62], w[75], w[35], pars->GC_11, amp[115]); 
  FFV1_0(w[62], w[13], w[76], pars->GC_11, amp[116]); 
  FFV1_0(w[64], w[5], w[76], pars->GC_11, amp[117]); 
  FFV1_0(w[31], w[5], w[77], pars->GC_11, amp[118]); 
  FFV1_0(w[0], w[13], w[77], pars->GC_11, amp[119]); 
  FFV1_0(w[62], w[17], w[76], pars->GC_11, amp[120]); 
  FFV1_0(w[66], w[5], w[76], pars->GC_11, amp[121]); 
  FFV1_0(w[32], w[5], w[77], pars->GC_11, amp[122]); 
  FFV1_0(w[0], w[17], w[77], pars->GC_11, amp[123]); 
  FFV1_0(w[0], w[74], w[70], pars->GC_11, amp[124]); 
  FFV1_0(w[0], w[78], w[8], pars->GC_2, amp[125]); 
  FFV1_0(w[0], w[75], w[70], pars->GC_11, amp[126]); 
  FFV2_5_0(w[0], w[78], w[14], pars->GC_51, pars->GC_58, amp[127]); 
  FFV1_0(w[62], w[80], w[8], pars->GC_2, amp[128]); 
  FFV1_0(w[62], w[27], w[79], pars->GC_11, amp[129]); 
  FFV2_5_0(w[62], w[80], w[14], pars->GC_51, pars->GC_58, amp[130]); 
  FFV1_0(w[62], w[28], w[79], pars->GC_11, amp[131]); 
  FFV1_0(w[62], w[65], w[29], pars->GC_11, amp[132]); 
  FFV1_0(w[64], w[61], w[29], pars->GC_11, amp[133]); 
  FFV1_0(w[31], w[61], w[81], pars->GC_11, amp[134]); 
  FFV1_0(w[0], w[65], w[81], pars->GC_11, amp[135]); 
  FFV1_0(w[62], w[67], w[29], pars->GC_11, amp[136]); 
  FFV1_0(w[66], w[61], w[29], pars->GC_11, amp[137]); 
  FFV1_0(w[32], w[61], w[81], pars->GC_11, amp[138]); 
  FFV1_0(w[0], w[67], w[81], pars->GC_11, amp[139]); 
  FFV1_0(w[0], w[27], w[68], pars->GC_11, amp[140]); 
  FFV1_0(w[0], w[82], w[8], pars->GC_2, amp[141]); 
  FFV1_0(w[0], w[28], w[68], pars->GC_11, amp[142]); 
  FFV2_5_0(w[0], w[82], w[14], pars->GC_51, pars->GC_58, amp[143]); 
  FFV1_0(w[84], w[5], w[8], pars->GC_2, amp[144]); 
  FFV1_0(w[85], w[5], w[79], pars->GC_11, amp[145]); 
  FFV2_5_0(w[84], w[5], w[14], pars->GC_51, pars->GC_58, amp[146]); 
  FFV1_0(w[86], w[5], w[79], pars->GC_11, amp[147]); 
  FFV1_0(w[87], w[61], w[8], pars->GC_2, amp[148]); 
  FFV1_0(w[85], w[61], w[35], pars->GC_11, amp[149]); 
  FFV2_5_0(w[87], w[61], w[14], pars->GC_51, pars->GC_58, amp[150]); 
  FFV1_0(w[86], w[61], w[35], pars->GC_11, amp[151]); 
  FFV1_0(w[31], w[5], w[88], pars->GC_11, amp[152]); 
  FFV1_0(w[0], w[13], w[88], pars->GC_11, amp[153]); 
  FFV1_0(w[31], w[61], w[89], pars->GC_11, amp[154]); 
  FFV1_0(w[0], w[65], w[89], pars->GC_11, amp[155]); 
  FFV1_0(w[32], w[5], w[88], pars->GC_11, amp[156]); 
  FFV1_0(w[0], w[17], w[88], pars->GC_11, amp[157]); 
  FFV1_0(w[32], w[61], w[89], pars->GC_11, amp[158]); 
  FFV1_0(w[0], w[67], w[89], pars->GC_11, amp[159]); 
  FFV1_0(w[62], w[13], w[90], pars->GC_11, amp[160]); 
  FFV1_0(w[64], w[5], w[90], pars->GC_11, amp[161]); 
  FFV1_0(w[64], w[91], w[4], pars->GC_11, amp[162]); 
  FFV1_0(w[92], w[13], w[4], pars->GC_11, amp[163]); 
  FFV1_0(w[62], w[17], w[90], pars->GC_11, amp[164]); 
  FFV1_0(w[66], w[5], w[90], pars->GC_11, amp[165]); 
  FFV1_0(w[66], w[91], w[4], pars->GC_11, amp[166]); 
  FFV1_0(w[92], w[17], w[4], pars->GC_11, amp[167]); 
  FFV1_0(w[62], w[65], w[49], pars->GC_11, amp[168]); 
  FFV1_0(w[64], w[61], w[49], pars->GC_11, amp[169]); 
  FFV1_0(w[64], w[93], w[4], pars->GC_11, amp[170]); 
  FFV1_0(w[94], w[65], w[4], pars->GC_11, amp[171]); 
  FFV1_0(w[62], w[67], w[49], pars->GC_11, amp[172]); 
  FFV1_0(w[66], w[61], w[49], pars->GC_11, amp[173]); 
  FFV1_0(w[66], w[93], w[4], pars->GC_11, amp[174]); 
  FFV1_0(w[94], w[67], w[4], pars->GC_11, amp[175]); 
  FFV1_0(w[31], w[5], w[95], pars->GC_11, amp[176]); 
  FFV1_0(w[0], w[13], w[95], pars->GC_11, amp[177]); 
  FFV1_0(w[31], w[96], w[4], pars->GC_11, amp[178]); 
  FFV1_0(w[97], w[13], w[4], pars->GC_11, amp[179]); 
  FFV1_0(w[32], w[5], w[95], pars->GC_11, amp[180]); 
  FFV1_0(w[0], w[17], w[95], pars->GC_11, amp[181]); 
  FFV1_0(w[32], w[96], w[4], pars->GC_11, amp[182]); 
  FFV1_0(w[97], w[17], w[4], pars->GC_11, amp[183]); 
  FFV1_0(w[31], w[61], w[98], pars->GC_11, amp[184]); 
  FFV1_0(w[0], w[65], w[98], pars->GC_11, amp[185]); 
  FFV1_0(w[31], w[99], w[4], pars->GC_11, amp[186]); 
  FFV1_0(w[100], w[65], w[4], pars->GC_11, amp[187]); 
  FFV1_0(w[32], w[61], w[98], pars->GC_11, amp[188]); 
  FFV1_0(w[0], w[67], w[98], pars->GC_11, amp[189]); 
  FFV1_0(w[32], w[99], w[4], pars->GC_11, amp[190]); 
  FFV1_0(w[100], w[67], w[4], pars->GC_11, amp[191]); 
  FFV1_0(w[1], w[101], w[9], pars->GC_11, amp[192]); 
  FFV1_0(w[102], w[6], w[9], pars->GC_11, amp[193]); 
  FFV1_0(w[1], w[103], w[12], pars->GC_11, amp[194]); 
  FFV1_0(w[102], w[5], w[12], pars->GC_11, amp[195]); 
  FFV1_0(w[1], w[104], w[9], pars->GC_11, amp[196]); 
  FFV1_0(w[105], w[6], w[9], pars->GC_11, amp[197]); 
  FFV1_0(w[1], w[106], w[12], pars->GC_11, amp[198]); 
  FFV1_0(w[105], w[5], w[12], pars->GC_11, amp[199]); 
  FFV1_0(w[107], w[6], w[18], pars->GC_11, amp[200]); 
  FFV1_0(w[20], w[6], w[8], pars->GC_1, amp[201]); 
  FFV1_0(w[108], w[6], w[18], pars->GC_11, amp[202]); 
  FFV2_3_0(w[20], w[6], w[14], pars->GC_50, pars->GC_58, amp[203]); 
  FFV1_0(w[107], w[5], w[22], pars->GC_11, amp[204]); 
  FFV1_0(w[23], w[5], w[8], pars->GC_1, amp[205]); 
  FFV1_0(w[108], w[5], w[22], pars->GC_11, amp[206]); 
  FFV2_3_0(w[23], w[5], w[14], pars->GC_50, pars->GC_58, amp[207]); 
  FFV1_0(w[1], w[26], w[8], pars->GC_1, amp[208]); 
  FFV1_0(w[1], w[109], w[25], pars->GC_11, amp[209]); 
  FFV2_3_0(w[1], w[26], w[14], pars->GC_50, pars->GC_58, amp[210]); 
  FFV1_0(w[1], w[110], w[25], pars->GC_11, amp[211]); 
  FFV1_0(w[1], w[101], w[29], pars->GC_11, amp[212]); 
  FFV1_0(w[102], w[6], w[29], pars->GC_11, amp[213]); 
  FFV1_0(w[111], w[6], w[30], pars->GC_11, amp[214]); 
  FFV1_0(w[0], w[101], w[30], pars->GC_11, amp[215]); 
  FFV1_0(w[1], w[104], w[29], pars->GC_11, amp[216]); 
  FFV1_0(w[105], w[6], w[29], pars->GC_11, amp[217]); 
  FFV1_0(w[112], w[6], w[30], pars->GC_11, amp[218]); 
  FFV1_0(w[0], w[104], w[30], pars->GC_11, amp[219]); 
  FFV1_0(w[0], w[109], w[22], pars->GC_11, amp[220]); 
  FFV1_0(w[0], w[33], w[8], pars->GC_1, amp[221]); 
  FFV1_0(w[0], w[110], w[22], pars->GC_11, amp[222]); 
  FFV2_3_0(w[0], w[33], w[14], pars->GC_50, pars->GC_58, amp[223]); 
  FFV1_0(w[1], w[36], w[8], pars->GC_1, amp[224]); 
  FFV1_0(w[1], w[113], w[35], pars->GC_11, amp[225]); 
  FFV2_3_0(w[1], w[36], w[14], pars->GC_50, pars->GC_58, amp[226]); 
  FFV1_0(w[1], w[114], w[35], pars->GC_11, amp[227]); 
  FFV1_0(w[1], w[103], w[39], pars->GC_11, amp[228]); 
  FFV1_0(w[102], w[5], w[39], pars->GC_11, amp[229]); 
  FFV1_0(w[111], w[5], w[40], pars->GC_11, amp[230]); 
  FFV1_0(w[0], w[103], w[40], pars->GC_11, amp[231]); 
  FFV1_0(w[1], w[106], w[39], pars->GC_11, amp[232]); 
  FFV1_0(w[105], w[5], w[39], pars->GC_11, amp[233]); 
  FFV1_0(w[112], w[5], w[40], pars->GC_11, amp[234]); 
  FFV1_0(w[0], w[106], w[40], pars->GC_11, amp[235]); 
  FFV1_0(w[0], w[113], w[18], pars->GC_11, amp[236]); 
  FFV1_0(w[0], w[41], w[8], pars->GC_1, amp[237]); 
  FFV1_0(w[0], w[114], w[18], pars->GC_11, amp[238]); 
  FFV2_3_0(w[0], w[41], w[14], pars->GC_50, pars->GC_58, amp[239]); 
  FFV1_0(w[43], w[6], w[8], pars->GC_1, amp[240]); 
  FFV1_0(w[115], w[6], w[35], pars->GC_11, amp[241]); 
  FFV2_3_0(w[43], w[6], w[14], pars->GC_50, pars->GC_58, amp[242]); 
  FFV1_0(w[116], w[6], w[35], pars->GC_11, amp[243]); 
  FFV1_0(w[46], w[5], w[8], pars->GC_1, amp[244]); 
  FFV1_0(w[115], w[5], w[25], pars->GC_11, amp[245]); 
  FFV2_3_0(w[46], w[5], w[14], pars->GC_50, pars->GC_58, amp[246]); 
  FFV1_0(w[116], w[5], w[25], pars->GC_11, amp[247]); 
  FFV1_0(w[111], w[6], w[47], pars->GC_11, amp[248]); 
  FFV1_0(w[0], w[101], w[47], pars->GC_11, amp[249]); 
  FFV1_0(w[111], w[5], w[48], pars->GC_11, amp[250]); 
  FFV1_0(w[0], w[103], w[48], pars->GC_11, amp[251]); 
  FFV1_0(w[112], w[6], w[47], pars->GC_11, amp[252]); 
  FFV1_0(w[0], w[104], w[47], pars->GC_11, amp[253]); 
  FFV1_0(w[112], w[5], w[48], pars->GC_11, amp[254]); 
  FFV1_0(w[0], w[106], w[48], pars->GC_11, amp[255]); 
  FFV1_0(w[1], w[101], w[49], pars->GC_11, amp[256]); 
  FFV1_0(w[102], w[6], w[49], pars->GC_11, amp[257]); 
  FFV1_0(w[102], w[50], w[4], pars->GC_11, amp[258]); 
  FFV1_0(w[51], w[101], w[4], pars->GC_11, amp[259]); 
  FFV1_0(w[1], w[104], w[49], pars->GC_11, amp[260]); 
  FFV1_0(w[105], w[6], w[49], pars->GC_11, amp[261]); 
  FFV1_0(w[105], w[50], w[4], pars->GC_11, amp[262]); 
  FFV1_0(w[51], w[104], w[4], pars->GC_11, amp[263]); 
  FFV1_0(w[1], w[103], w[52], pars->GC_11, amp[264]); 
  FFV1_0(w[102], w[5], w[52], pars->GC_11, amp[265]); 
  FFV1_0(w[102], w[53], w[4], pars->GC_11, amp[266]); 
  FFV1_0(w[54], w[103], w[4], pars->GC_11, amp[267]); 
  FFV1_0(w[1], w[106], w[52], pars->GC_11, amp[268]); 
  FFV1_0(w[105], w[5], w[52], pars->GC_11, amp[269]); 
  FFV1_0(w[105], w[53], w[4], pars->GC_11, amp[270]); 
  FFV1_0(w[54], w[106], w[4], pars->GC_11, amp[271]); 
  FFV1_0(w[111], w[6], w[55], pars->GC_11, amp[272]); 
  FFV1_0(w[0], w[101], w[55], pars->GC_11, amp[273]); 
  FFV1_0(w[111], w[56], w[4], pars->GC_11, amp[274]); 
  FFV1_0(w[57], w[101], w[4], pars->GC_11, amp[275]); 
  FFV1_0(w[112], w[6], w[55], pars->GC_11, amp[276]); 
  FFV1_0(w[0], w[104], w[55], pars->GC_11, amp[277]); 
  FFV1_0(w[112], w[56], w[4], pars->GC_11, amp[278]); 
  FFV1_0(w[57], w[104], w[4], pars->GC_11, amp[279]); 
  FFV1_0(w[111], w[5], w[58], pars->GC_11, amp[280]); 
  FFV1_0(w[0], w[103], w[58], pars->GC_11, amp[281]); 
  FFV1_0(w[111], w[59], w[4], pars->GC_11, amp[282]); 
  FFV1_0(w[60], w[103], w[4], pars->GC_11, amp[283]); 
  FFV1_0(w[112], w[5], w[58], pars->GC_11, amp[284]); 
  FFV1_0(w[0], w[106], w[58], pars->GC_11, amp[285]); 
  FFV1_0(w[112], w[59], w[4], pars->GC_11, amp[286]); 
  FFV1_0(w[60], w[106], w[4], pars->GC_11, amp[287]); 
  FFV1_0(w[62], w[103], w[63], pars->GC_11, amp[288]); 
  FFV1_0(w[117], w[5], w[63], pars->GC_11, amp[289]); 
  FFV1_0(w[62], w[118], w[9], pars->GC_11, amp[290]); 
  FFV1_0(w[117], w[61], w[9], pars->GC_11, amp[291]); 
  FFV1_0(w[62], w[106], w[63], pars->GC_11, amp[292]); 
  FFV1_0(w[119], w[5], w[63], pars->GC_11, amp[293]); 
  FFV1_0(w[62], w[120], w[9], pars->GC_11, amp[294]); 
  FFV1_0(w[119], w[61], w[9], pars->GC_11, amp[295]); 
  FFV1_0(w[107], w[5], w[68], pars->GC_11, amp[296]); 
  FFV1_0(w[69], w[5], w[8], pars->GC_1, amp[297]); 
  FFV1_0(w[108], w[5], w[68], pars->GC_11, amp[298]); 
  FFV2_3_0(w[69], w[5], w[14], pars->GC_50, pars->GC_58, amp[299]); 
  FFV1_0(w[107], w[61], w[70], pars->GC_11, amp[300]); 
  FFV1_0(w[71], w[61], w[8], pars->GC_1, amp[301]); 
  FFV1_0(w[108], w[61], w[70], pars->GC_11, amp[302]); 
  FFV2_3_0(w[71], w[61], w[14], pars->GC_50, pars->GC_58, amp[303]); 
  FFV1_0(w[62], w[73], w[8], pars->GC_1, amp[304]); 
  FFV1_0(w[62], w[121], w[35], pars->GC_11, amp[305]); 
  FFV2_3_0(w[62], w[73], w[14], pars->GC_50, pars->GC_58, amp[306]); 
  FFV1_0(w[62], w[122], w[35], pars->GC_11, amp[307]); 
  FFV1_0(w[62], w[103], w[76], pars->GC_11, amp[308]); 
  FFV1_0(w[117], w[5], w[76], pars->GC_11, amp[309]); 
  FFV1_0(w[111], w[5], w[77], pars->GC_11, amp[310]); 
  FFV1_0(w[0], w[103], w[77], pars->GC_11, amp[311]); 
  FFV1_0(w[62], w[106], w[76], pars->GC_11, amp[312]); 
  FFV1_0(w[119], w[5], w[76], pars->GC_11, amp[313]); 
  FFV1_0(w[112], w[5], w[77], pars->GC_11, amp[314]); 
  FFV1_0(w[0], w[106], w[77], pars->GC_11, amp[315]); 
  FFV1_0(w[0], w[121], w[70], pars->GC_11, amp[316]); 
  FFV1_0(w[0], w[78], w[8], pars->GC_1, amp[317]); 
  FFV1_0(w[0], w[122], w[70], pars->GC_11, amp[318]); 
  FFV2_3_0(w[0], w[78], w[14], pars->GC_50, pars->GC_58, amp[319]); 
  FFV1_0(w[62], w[80], w[8], pars->GC_1, amp[320]); 
  FFV1_0(w[62], w[109], w[79], pars->GC_11, amp[321]); 
  FFV2_3_0(w[62], w[80], w[14], pars->GC_50, pars->GC_58, amp[322]); 
  FFV1_0(w[62], w[110], w[79], pars->GC_11, amp[323]); 
  FFV1_0(w[62], w[118], w[29], pars->GC_11, amp[324]); 
  FFV1_0(w[117], w[61], w[29], pars->GC_11, amp[325]); 
  FFV1_0(w[111], w[61], w[81], pars->GC_11, amp[326]); 
  FFV1_0(w[0], w[118], w[81], pars->GC_11, amp[327]); 
  FFV1_0(w[62], w[120], w[29], pars->GC_11, amp[328]); 
  FFV1_0(w[119], w[61], w[29], pars->GC_11, amp[329]); 
  FFV1_0(w[112], w[61], w[81], pars->GC_11, amp[330]); 
  FFV1_0(w[0], w[120], w[81], pars->GC_11, amp[331]); 
  FFV1_0(w[0], w[109], w[68], pars->GC_11, amp[332]); 
  FFV1_0(w[0], w[82], w[8], pars->GC_1, amp[333]); 
  FFV1_0(w[0], w[110], w[68], pars->GC_11, amp[334]); 
  FFV2_3_0(w[0], w[82], w[14], pars->GC_50, pars->GC_58, amp[335]); 
  FFV1_0(w[84], w[5], w[8], pars->GC_1, amp[336]); 
  FFV1_0(w[123], w[5], w[79], pars->GC_11, amp[337]); 
  FFV2_3_0(w[84], w[5], w[14], pars->GC_50, pars->GC_58, amp[338]); 
  FFV1_0(w[124], w[5], w[79], pars->GC_11, amp[339]); 
  FFV1_0(w[87], w[61], w[8], pars->GC_1, amp[340]); 
  FFV1_0(w[123], w[61], w[35], pars->GC_11, amp[341]); 
  FFV2_3_0(w[87], w[61], w[14], pars->GC_50, pars->GC_58, amp[342]); 
  FFV1_0(w[124], w[61], w[35], pars->GC_11, amp[343]); 
  FFV1_0(w[111], w[5], w[88], pars->GC_11, amp[344]); 
  FFV1_0(w[0], w[103], w[88], pars->GC_11, amp[345]); 
  FFV1_0(w[111], w[61], w[89], pars->GC_11, amp[346]); 
  FFV1_0(w[0], w[118], w[89], pars->GC_11, amp[347]); 
  FFV1_0(w[112], w[5], w[88], pars->GC_11, amp[348]); 
  FFV1_0(w[0], w[106], w[88], pars->GC_11, amp[349]); 
  FFV1_0(w[112], w[61], w[89], pars->GC_11, amp[350]); 
  FFV1_0(w[0], w[120], w[89], pars->GC_11, amp[351]); 
  FFV1_0(w[62], w[103], w[90], pars->GC_11, amp[352]); 
  FFV1_0(w[117], w[5], w[90], pars->GC_11, amp[353]); 
  FFV1_0(w[117], w[91], w[4], pars->GC_11, amp[354]); 
  FFV1_0(w[92], w[103], w[4], pars->GC_11, amp[355]); 
  FFV1_0(w[62], w[106], w[90], pars->GC_11, amp[356]); 
  FFV1_0(w[119], w[5], w[90], pars->GC_11, amp[357]); 
  FFV1_0(w[119], w[91], w[4], pars->GC_11, amp[358]); 
  FFV1_0(w[92], w[106], w[4], pars->GC_11, amp[359]); 
  FFV1_0(w[62], w[118], w[49], pars->GC_11, amp[360]); 
  FFV1_0(w[117], w[61], w[49], pars->GC_11, amp[361]); 
  FFV1_0(w[117], w[93], w[4], pars->GC_11, amp[362]); 
  FFV1_0(w[94], w[118], w[4], pars->GC_11, amp[363]); 
  FFV1_0(w[62], w[120], w[49], pars->GC_11, amp[364]); 
  FFV1_0(w[119], w[61], w[49], pars->GC_11, amp[365]); 
  FFV1_0(w[119], w[93], w[4], pars->GC_11, amp[366]); 
  FFV1_0(w[94], w[120], w[4], pars->GC_11, amp[367]); 
  FFV1_0(w[111], w[5], w[95], pars->GC_11, amp[368]); 
  FFV1_0(w[0], w[103], w[95], pars->GC_11, amp[369]); 
  FFV1_0(w[111], w[96], w[4], pars->GC_11, amp[370]); 
  FFV1_0(w[97], w[103], w[4], pars->GC_11, amp[371]); 
  FFV1_0(w[112], w[5], w[95], pars->GC_11, amp[372]); 
  FFV1_0(w[0], w[106], w[95], pars->GC_11, amp[373]); 
  FFV1_0(w[112], w[96], w[4], pars->GC_11, amp[374]); 
  FFV1_0(w[97], w[106], w[4], pars->GC_11, amp[375]); 
  FFV1_0(w[111], w[61], w[98], pars->GC_11, amp[376]); 
  FFV1_0(w[0], w[118], w[98], pars->GC_11, amp[377]); 
  FFV1_0(w[111], w[99], w[4], pars->GC_11, amp[378]); 
  FFV1_0(w[100], w[118], w[4], pars->GC_11, amp[379]); 
  FFV1_0(w[112], w[61], w[98], pars->GC_11, amp[380]); 
  FFV1_0(w[0], w[120], w[98], pars->GC_11, amp[381]); 
  FFV1_0(w[112], w[99], w[4], pars->GC_11, amp[382]); 
  FFV1_0(w[100], w[120], w[4], pars->GC_11, amp[383]); 
  FFV1_0(w[62], w[65], w[128], pars->GC_11, amp[384]); 
  FFV1_0(w[64], w[61], w[128], pars->GC_11, amp[385]); 
  FFV1_0(w[62], w[130], w[129], pars->GC_11, amp[386]); 
  FFV1_0(w[64], w[125], w[129], pars->GC_11, amp[387]); 
  FFV1_0(w[62], w[67], w[128], pars->GC_11, amp[388]); 
  FFV1_0(w[66], w[61], w[128], pars->GC_11, amp[389]); 
  FFV1_0(w[62], w[131], w[129], pars->GC_11, amp[390]); 
  FFV1_0(w[66], w[125], w[129], pars->GC_11, amp[391]); 
  FFV1_0(w[133], w[61], w[132], pars->GC_11, amp[392]); 
  FFV1_0(w[134], w[61], w[8], pars->GC_2, amp[393]); 
  FFV1_0(w[135], w[61], w[132], pars->GC_11, amp[394]); 
  FFV2_5_0(w[134], w[61], w[14], pars->GC_51, pars->GC_58, amp[395]); 
  FFV1_0(w[133], w[125], w[68], pars->GC_11, amp[396]); 
  FFV1_0(w[136], w[125], w[8], pars->GC_2, amp[397]); 
  FFV1_0(w[135], w[125], w[68], pars->GC_11, amp[398]); 
  FFV2_5_0(w[136], w[125], w[14], pars->GC_51, pars->GC_58, amp[399]); 
  FFV1_0(w[62], w[139], w[8], pars->GC_2, amp[400]); 
  FFV1_0(w[62], w[140], w[138], pars->GC_11, amp[401]); 
  FFV2_5_0(w[62], w[139], w[14], pars->GC_51, pars->GC_58, amp[402]); 
  FFV1_0(w[62], w[141], w[138], pars->GC_11, amp[403]); 
  FFV1_0(w[62], w[65], w[142], pars->GC_11, amp[404]); 
  FFV1_0(w[64], w[61], w[142], pars->GC_11, amp[405]); 
  FFV1_0(w[144], w[61], w[143], pars->GC_11, amp[406]); 
  FFV1_0(w[126], w[65], w[143], pars->GC_11, amp[407]); 
  FFV1_0(w[62], w[67], w[142], pars->GC_11, amp[408]); 
  FFV1_0(w[66], w[61], w[142], pars->GC_11, amp[409]); 
  FFV1_0(w[145], w[61], w[143], pars->GC_11, amp[410]); 
  FFV1_0(w[126], w[67], w[143], pars->GC_11, amp[411]); 
  FFV1_0(w[126], w[140], w[68], pars->GC_11, amp[412]); 
  FFV1_0(w[126], w[146], w[8], pars->GC_2, amp[413]); 
  FFV1_0(w[126], w[141], w[68], pars->GC_11, amp[414]); 
  FFV2_5_0(w[126], w[146], w[14], pars->GC_51, pars->GC_58, amp[415]); 
  FFV1_0(w[62], w[148], w[8], pars->GC_2, amp[416]); 
  FFV1_0(w[62], w[74], w[147], pars->GC_11, amp[417]); 
  FFV2_5_0(w[62], w[148], w[14], pars->GC_51, pars->GC_58, amp[418]); 
  FFV1_0(w[62], w[75], w[147], pars->GC_11, amp[419]); 
  FFV1_0(w[62], w[130], w[149], pars->GC_11, amp[420]); 
  FFV1_0(w[64], w[125], w[149], pars->GC_11, amp[421]); 
  FFV1_0(w[144], w[125], w[77], pars->GC_11, amp[422]); 
  FFV1_0(w[126], w[130], w[77], pars->GC_11, amp[423]); 
  FFV1_0(w[62], w[131], w[149], pars->GC_11, amp[424]); 
  FFV1_0(w[66], w[125], w[149], pars->GC_11, amp[425]); 
  FFV1_0(w[145], w[125], w[77], pars->GC_11, amp[426]); 
  FFV1_0(w[126], w[131], w[77], pars->GC_11, amp[427]); 
  FFV1_0(w[126], w[74], w[132], pars->GC_11, amp[428]); 
  FFV1_0(w[126], w[150], w[8], pars->GC_2, amp[429]); 
  FFV1_0(w[126], w[75], w[132], pars->GC_11, amp[430]); 
  FFV2_5_0(w[126], w[150], w[14], pars->GC_51, pars->GC_58, amp[431]); 
  FFV1_0(w[151], w[61], w[8], pars->GC_2, amp[432]); 
  FFV1_0(w[85], w[61], w[147], pars->GC_11, amp[433]); 
  FFV2_5_0(w[151], w[61], w[14], pars->GC_51, pars->GC_58, amp[434]); 
  FFV1_0(w[86], w[61], w[147], pars->GC_11, amp[435]); 
  FFV1_0(w[152], w[125], w[8], pars->GC_2, amp[436]); 
  FFV1_0(w[85], w[125], w[138], pars->GC_11, amp[437]); 
  FFV2_5_0(w[152], w[125], w[14], pars->GC_51, pars->GC_58, amp[438]); 
  FFV1_0(w[86], w[125], w[138], pars->GC_11, amp[439]); 
  FFV1_0(w[144], w[61], w[153], pars->GC_11, amp[440]); 
  FFV1_0(w[126], w[65], w[153], pars->GC_11, amp[441]); 
  FFV1_0(w[144], w[125], w[88], pars->GC_11, amp[442]); 
  FFV1_0(w[126], w[130], w[88], pars->GC_11, amp[443]); 
  FFV1_0(w[145], w[61], w[153], pars->GC_11, amp[444]); 
  FFV1_0(w[126], w[67], w[153], pars->GC_11, amp[445]); 
  FFV1_0(w[145], w[125], w[88], pars->GC_11, amp[446]); 
  FFV1_0(w[126], w[131], w[88], pars->GC_11, amp[447]); 
  FFV1_0(w[62], w[65], w[154], pars->GC_11, amp[448]); 
  FFV1_0(w[64], w[61], w[154], pars->GC_11, amp[449]); 
  FFV1_0(w[64], w[155], w[4], pars->GC_11, amp[450]); 
  FFV1_0(w[156], w[65], w[4], pars->GC_11, amp[451]); 
  FFV1_0(w[62], w[67], w[154], pars->GC_11, amp[452]); 
  FFV1_0(w[66], w[61], w[154], pars->GC_11, amp[453]); 
  FFV1_0(w[66], w[155], w[4], pars->GC_11, amp[454]); 
  FFV1_0(w[156], w[67], w[4], pars->GC_11, amp[455]); 
  FFV1_0(w[62], w[130], w[157], pars->GC_11, amp[456]); 
  FFV1_0(w[64], w[125], w[157], pars->GC_11, amp[457]); 
  FFV1_0(w[64], w[158], w[4], pars->GC_11, amp[458]); 
  FFV1_0(w[159], w[130], w[4], pars->GC_11, amp[459]); 
  FFV1_0(w[62], w[131], w[157], pars->GC_11, amp[460]); 
  FFV1_0(w[66], w[125], w[157], pars->GC_11, amp[461]); 
  FFV1_0(w[66], w[158], w[4], pars->GC_11, amp[462]); 
  FFV1_0(w[159], w[131], w[4], pars->GC_11, amp[463]); 
  FFV1_0(w[144], w[61], w[160], pars->GC_11, amp[464]); 
  FFV1_0(w[126], w[65], w[160], pars->GC_11, amp[465]); 
  FFV1_0(w[144], w[161], w[4], pars->GC_11, amp[466]); 
  FFV1_0(w[162], w[65], w[4], pars->GC_11, amp[467]); 
  FFV1_0(w[145], w[61], w[160], pars->GC_11, amp[468]); 
  FFV1_0(w[126], w[67], w[160], pars->GC_11, amp[469]); 
  FFV1_0(w[145], w[161], w[4], pars->GC_11, amp[470]); 
  FFV1_0(w[162], w[67], w[4], pars->GC_11, amp[471]); 
  FFV1_0(w[144], w[125], w[95], pars->GC_11, amp[472]); 
  FFV1_0(w[126], w[130], w[95], pars->GC_11, amp[473]); 
  FFV1_0(w[144], w[163], w[4], pars->GC_11, amp[474]); 
  FFV1_0(w[164], w[130], w[4], pars->GC_11, amp[475]); 
  FFV1_0(w[145], w[125], w[95], pars->GC_11, amp[476]); 
  FFV1_0(w[126], w[131], w[95], pars->GC_11, amp[477]); 
  FFV1_0(w[145], w[163], w[4], pars->GC_11, amp[478]); 
  FFV1_0(w[164], w[131], w[4], pars->GC_11, amp[479]); 
  FFV1_0(w[62], w[118], w[128], pars->GC_11, amp[480]); 
  FFV1_0(w[117], w[61], w[128], pars->GC_11, amp[481]); 
  FFV1_0(w[62], w[165], w[129], pars->GC_11, amp[482]); 
  FFV1_0(w[117], w[125], w[129], pars->GC_11, amp[483]); 
  FFV1_0(w[62], w[120], w[128], pars->GC_11, amp[484]); 
  FFV1_0(w[119], w[61], w[128], pars->GC_11, amp[485]); 
  FFV1_0(w[62], w[166], w[129], pars->GC_11, amp[486]); 
  FFV1_0(w[119], w[125], w[129], pars->GC_11, amp[487]); 
  FFV1_0(w[167], w[61], w[132], pars->GC_11, amp[488]); 
  FFV1_0(w[134], w[61], w[8], pars->GC_1, amp[489]); 
  FFV1_0(w[168], w[61], w[132], pars->GC_11, amp[490]); 
  FFV2_3_0(w[134], w[61], w[14], pars->GC_50, pars->GC_58, amp[491]); 
  FFV1_0(w[167], w[125], w[68], pars->GC_11, amp[492]); 
  FFV1_0(w[136], w[125], w[8], pars->GC_1, amp[493]); 
  FFV1_0(w[168], w[125], w[68], pars->GC_11, amp[494]); 
  FFV2_3_0(w[136], w[125], w[14], pars->GC_50, pars->GC_58, amp[495]); 
  FFV1_0(w[62], w[139], w[8], pars->GC_1, amp[496]); 
  FFV1_0(w[62], w[169], w[138], pars->GC_11, amp[497]); 
  FFV2_3_0(w[62], w[139], w[14], pars->GC_50, pars->GC_58, amp[498]); 
  FFV1_0(w[62], w[170], w[138], pars->GC_11, amp[499]); 
  FFV1_0(w[62], w[118], w[142], pars->GC_11, amp[500]); 
  FFV1_0(w[117], w[61], w[142], pars->GC_11, amp[501]); 
  FFV1_0(w[171], w[61], w[143], pars->GC_11, amp[502]); 
  FFV1_0(w[126], w[118], w[143], pars->GC_11, amp[503]); 
  FFV1_0(w[62], w[120], w[142], pars->GC_11, amp[504]); 
  FFV1_0(w[119], w[61], w[142], pars->GC_11, amp[505]); 
  FFV1_0(w[172], w[61], w[143], pars->GC_11, amp[506]); 
  FFV1_0(w[126], w[120], w[143], pars->GC_11, amp[507]); 
  FFV1_0(w[126], w[169], w[68], pars->GC_11, amp[508]); 
  FFV1_0(w[126], w[146], w[8], pars->GC_1, amp[509]); 
  FFV1_0(w[126], w[170], w[68], pars->GC_11, amp[510]); 
  FFV2_3_0(w[126], w[146], w[14], pars->GC_50, pars->GC_58, amp[511]); 
  FFV1_0(w[62], w[148], w[8], pars->GC_1, amp[512]); 
  FFV1_0(w[62], w[121], w[147], pars->GC_11, amp[513]); 
  FFV2_3_0(w[62], w[148], w[14], pars->GC_50, pars->GC_58, amp[514]); 
  FFV1_0(w[62], w[122], w[147], pars->GC_11, amp[515]); 
  FFV1_0(w[62], w[165], w[149], pars->GC_11, amp[516]); 
  FFV1_0(w[117], w[125], w[149], pars->GC_11, amp[517]); 
  FFV1_0(w[171], w[125], w[77], pars->GC_11, amp[518]); 
  FFV1_0(w[126], w[165], w[77], pars->GC_11, amp[519]); 
  FFV1_0(w[62], w[166], w[149], pars->GC_11, amp[520]); 
  FFV1_0(w[119], w[125], w[149], pars->GC_11, amp[521]); 
  FFV1_0(w[172], w[125], w[77], pars->GC_11, amp[522]); 
  FFV1_0(w[126], w[166], w[77], pars->GC_11, amp[523]); 
  FFV1_0(w[126], w[121], w[132], pars->GC_11, amp[524]); 
  FFV1_0(w[126], w[150], w[8], pars->GC_1, amp[525]); 
  FFV1_0(w[126], w[122], w[132], pars->GC_11, amp[526]); 
  FFV2_3_0(w[126], w[150], w[14], pars->GC_50, pars->GC_58, amp[527]); 
  FFV1_0(w[151], w[61], w[8], pars->GC_1, amp[528]); 
  FFV1_0(w[123], w[61], w[147], pars->GC_11, amp[529]); 
  FFV2_3_0(w[151], w[61], w[14], pars->GC_50, pars->GC_58, amp[530]); 
  FFV1_0(w[124], w[61], w[147], pars->GC_11, amp[531]); 
  FFV1_0(w[152], w[125], w[8], pars->GC_1, amp[532]); 
  FFV1_0(w[123], w[125], w[138], pars->GC_11, amp[533]); 
  FFV2_3_0(w[152], w[125], w[14], pars->GC_50, pars->GC_58, amp[534]); 
  FFV1_0(w[124], w[125], w[138], pars->GC_11, amp[535]); 
  FFV1_0(w[171], w[61], w[153], pars->GC_11, amp[536]); 
  FFV1_0(w[126], w[118], w[153], pars->GC_11, amp[537]); 
  FFV1_0(w[171], w[125], w[88], pars->GC_11, amp[538]); 
  FFV1_0(w[126], w[165], w[88], pars->GC_11, amp[539]); 
  FFV1_0(w[172], w[61], w[153], pars->GC_11, amp[540]); 
  FFV1_0(w[126], w[120], w[153], pars->GC_11, amp[541]); 
  FFV1_0(w[172], w[125], w[88], pars->GC_11, amp[542]); 
  FFV1_0(w[126], w[166], w[88], pars->GC_11, amp[543]); 
  FFV1_0(w[62], w[118], w[154], pars->GC_11, amp[544]); 
  FFV1_0(w[117], w[61], w[154], pars->GC_11, amp[545]); 
  FFV1_0(w[117], w[155], w[4], pars->GC_11, amp[546]); 
  FFV1_0(w[156], w[118], w[4], pars->GC_11, amp[547]); 
  FFV1_0(w[62], w[120], w[154], pars->GC_11, amp[548]); 
  FFV1_0(w[119], w[61], w[154], pars->GC_11, amp[549]); 
  FFV1_0(w[119], w[155], w[4], pars->GC_11, amp[550]); 
  FFV1_0(w[156], w[120], w[4], pars->GC_11, amp[551]); 
  FFV1_0(w[62], w[165], w[157], pars->GC_11, amp[552]); 
  FFV1_0(w[117], w[125], w[157], pars->GC_11, amp[553]); 
  FFV1_0(w[117], w[158], w[4], pars->GC_11, amp[554]); 
  FFV1_0(w[159], w[165], w[4], pars->GC_11, amp[555]); 
  FFV1_0(w[62], w[166], w[157], pars->GC_11, amp[556]); 
  FFV1_0(w[119], w[125], w[157], pars->GC_11, amp[557]); 
  FFV1_0(w[119], w[158], w[4], pars->GC_11, amp[558]); 
  FFV1_0(w[159], w[166], w[4], pars->GC_11, amp[559]); 
  FFV1_0(w[171], w[61], w[160], pars->GC_11, amp[560]); 
  FFV1_0(w[126], w[118], w[160], pars->GC_11, amp[561]); 
  FFV1_0(w[171], w[161], w[4], pars->GC_11, amp[562]); 
  FFV1_0(w[162], w[118], w[4], pars->GC_11, amp[563]); 
  FFV1_0(w[172], w[61], w[160], pars->GC_11, amp[564]); 
  FFV1_0(w[126], w[120], w[160], pars->GC_11, amp[565]); 
  FFV1_0(w[172], w[161], w[4], pars->GC_11, amp[566]); 
  FFV1_0(w[162], w[120], w[4], pars->GC_11, amp[567]); 
  FFV1_0(w[171], w[125], w[95], pars->GC_11, amp[568]); 
  FFV1_0(w[126], w[165], w[95], pars->GC_11, amp[569]); 
  FFV1_0(w[171], w[163], w[4], pars->GC_11, amp[570]); 
  FFV1_0(w[164], w[165], w[4], pars->GC_11, amp[571]); 
  FFV1_0(w[172], w[125], w[95], pars->GC_11, amp[572]); 
  FFV1_0(w[126], w[166], w[95], pars->GC_11, amp[573]); 
  FFV1_0(w[172], w[163], w[4], pars->GC_11, amp[574]); 
  FFV1_0(w[164], w[166], w[4], pars->GC_11, amp[575]); 
  FFV1_0(w[1], w[174], w[9], pars->GC_11, amp[576]); 
  FFV1_0(w[175], w[6], w[9], pars->GC_11, amp[577]); 
  FFV1_0(w[1], w[176], w[12], pars->GC_11, amp[578]); 
  FFV1_0(w[175], w[5], w[12], pars->GC_11, amp[579]); 
  FFV1_0(w[177], w[6], w[18], pars->GC_11, amp[580]); 
  FFV2_5_0(w[20], w[6], w[173], pars->GC_51, pars->GC_58, amp[581]); 
  FFV1_0(w[177], w[5], w[22], pars->GC_11, amp[582]); 
  FFV2_5_0(w[23], w[5], w[173], pars->GC_51, pars->GC_58, amp[583]); 
  FFV2_5_0(w[1], w[26], w[173], pars->GC_51, pars->GC_58, amp[584]); 
  FFV1_0(w[1], w[178], w[25], pars->GC_11, amp[585]); 
  FFV1_0(w[1], w[174], w[29], pars->GC_11, amp[586]); 
  FFV1_0(w[175], w[6], w[29], pars->GC_11, amp[587]); 
  FFV1_0(w[179], w[6], w[30], pars->GC_11, amp[588]); 
  FFV1_0(w[0], w[174], w[30], pars->GC_11, amp[589]); 
  FFV1_0(w[0], w[178], w[22], pars->GC_11, amp[590]); 
  FFV2_5_0(w[0], w[33], w[173], pars->GC_51, pars->GC_58, amp[591]); 
  FFV2_5_0(w[1], w[36], w[173], pars->GC_51, pars->GC_58, amp[592]); 
  FFV1_0(w[1], w[180], w[35], pars->GC_11, amp[593]); 
  FFV1_0(w[1], w[176], w[39], pars->GC_11, amp[594]); 
  FFV1_0(w[175], w[5], w[39], pars->GC_11, amp[595]); 
  FFV1_0(w[179], w[5], w[40], pars->GC_11, amp[596]); 
  FFV1_0(w[0], w[176], w[40], pars->GC_11, amp[597]); 
  FFV1_0(w[0], w[180], w[18], pars->GC_11, amp[598]); 
  FFV2_5_0(w[0], w[41], w[173], pars->GC_51, pars->GC_58, amp[599]); 
  FFV2_5_0(w[43], w[6], w[173], pars->GC_51, pars->GC_58, amp[600]); 
  FFV1_0(w[181], w[6], w[35], pars->GC_11, amp[601]); 
  FFV2_5_0(w[46], w[5], w[173], pars->GC_51, pars->GC_58, amp[602]); 
  FFV1_0(w[181], w[5], w[25], pars->GC_11, amp[603]); 
  FFV1_0(w[179], w[6], w[47], pars->GC_11, amp[604]); 
  FFV1_0(w[0], w[174], w[47], pars->GC_11, amp[605]); 
  FFV1_0(w[179], w[5], w[48], pars->GC_11, amp[606]); 
  FFV1_0(w[0], w[176], w[48], pars->GC_11, amp[607]); 
  FFV1_0(w[1], w[174], w[49], pars->GC_11, amp[608]); 
  FFV1_0(w[175], w[6], w[49], pars->GC_11, amp[609]); 
  FFV1_0(w[175], w[50], w[4], pars->GC_11, amp[610]); 
  FFV1_0(w[51], w[174], w[4], pars->GC_11, amp[611]); 
  FFV1_0(w[1], w[176], w[52], pars->GC_11, amp[612]); 
  FFV1_0(w[175], w[5], w[52], pars->GC_11, amp[613]); 
  FFV1_0(w[175], w[53], w[4], pars->GC_11, amp[614]); 
  FFV1_0(w[54], w[176], w[4], pars->GC_11, amp[615]); 
  FFV1_0(w[179], w[6], w[55], pars->GC_11, amp[616]); 
  FFV1_0(w[0], w[174], w[55], pars->GC_11, amp[617]); 
  FFV1_0(w[179], w[56], w[4], pars->GC_11, amp[618]); 
  FFV1_0(w[57], w[174], w[4], pars->GC_11, amp[619]); 
  FFV1_0(w[179], w[5], w[58], pars->GC_11, amp[620]); 
  FFV1_0(w[0], w[176], w[58], pars->GC_11, amp[621]); 
  FFV1_0(w[179], w[59], w[4], pars->GC_11, amp[622]); 
  FFV1_0(w[60], w[176], w[4], pars->GC_11, amp[623]); 
  FFV1_0(w[1], w[15], w[9], pars->GC_11, amp[624]); 
  FFV1_0(w[16], w[6], w[9], pars->GC_11, amp[625]); 
  FFV1_0(w[19], w[5], w[22], pars->GC_11, amp[626]); 
  FFV1_0(w[23], w[5], w[8], pars->GC_2, amp[627]); 
  FFV1_0(w[21], w[5], w[22], pars->GC_11, amp[628]); 
  FFV2_5_0(w[23], w[5], w[14], pars->GC_51, pars->GC_58, amp[629]); 
  FFV1_0(w[1], w[10], w[29], pars->GC_11, amp[630]); 
  FFV1_0(w[11], w[6], w[29], pars->GC_11, amp[631]); 
  FFV1_0(w[1], w[15], w[29], pars->GC_11, amp[632]); 
  FFV1_0(w[16], w[6], w[29], pars->GC_11, amp[633]); 
  FFV1_0(w[0], w[27], w[22], pars->GC_11, amp[634]); 
  FFV1_0(w[0], w[33], w[8], pars->GC_2, amp[635]); 
  FFV1_0(w[0], w[28], w[22], pars->GC_11, amp[636]); 
  FFV2_5_0(w[0], w[33], w[14], pars->GC_51, pars->GC_58, amp[637]); 
  FFV1_0(w[1], w[36], w[8], pars->GC_2, amp[638]); 
  FFV1_0(w[1], w[37], w[35], pars->GC_11, amp[639]); 
  FFV2_5_0(w[1], w[36], w[14], pars->GC_51, pars->GC_58, amp[640]); 
  FFV1_0(w[1], w[38], w[35], pars->GC_11, amp[641]); 
  FFV1_0(w[31], w[5], w[40], pars->GC_11, amp[642]); 
  FFV1_0(w[0], w[13], w[40], pars->GC_11, amp[643]); 
  FFV1_0(w[32], w[5], w[40], pars->GC_11, amp[644]); 
  FFV1_0(w[0], w[17], w[40], pars->GC_11, amp[645]); 
  FFV1_0(w[43], w[6], w[8], pars->GC_2, amp[646]); 
  FFV1_0(w[44], w[6], w[35], pars->GC_11, amp[647]); 
  FFV2_5_0(w[43], w[6], w[14], pars->GC_51, pars->GC_58, amp[648]); 
  FFV1_0(w[45], w[6], w[35], pars->GC_11, amp[649]); 
  FFV1_0(w[31], w[5], w[48], pars->GC_11, amp[650]); 
  FFV1_0(w[0], w[13], w[48], pars->GC_11, amp[651]); 
  FFV1_0(w[32], w[5], w[48], pars->GC_11, amp[652]); 
  FFV1_0(w[0], w[17], w[48], pars->GC_11, amp[653]); 
  FFV1_0(w[1], w[10], w[49], pars->GC_11, amp[654]); 
  FFV1_0(w[11], w[6], w[49], pars->GC_11, amp[655]); 
  FFV1_0(w[11], w[50], w[4], pars->GC_11, amp[656]); 
  FFV1_0(w[51], w[10], w[4], pars->GC_11, amp[657]); 
  FFV1_0(w[1], w[15], w[49], pars->GC_11, amp[658]); 
  FFV1_0(w[16], w[6], w[49], pars->GC_11, amp[659]); 
  FFV1_0(w[16], w[50], w[4], pars->GC_11, amp[660]); 
  FFV1_0(w[51], w[15], w[4], pars->GC_11, amp[661]); 
  FFV1_0(w[31], w[5], w[58], pars->GC_11, amp[662]); 
  FFV1_0(w[0], w[13], w[58], pars->GC_11, amp[663]); 
  FFV1_0(w[31], w[59], w[4], pars->GC_11, amp[664]); 
  FFV1_0(w[60], w[13], w[4], pars->GC_11, amp[665]); 
  FFV1_0(w[32], w[5], w[58], pars->GC_11, amp[666]); 
  FFV1_0(w[0], w[17], w[58], pars->GC_11, amp[667]); 
  FFV1_0(w[32], w[59], w[4], pars->GC_11, amp[668]); 
  FFV1_0(w[60], w[17], w[4], pars->GC_11, amp[669]); 
  FFV1_0(w[1], w[101], w[9], pars->GC_11, amp[670]); 
  FFV1_0(w[102], w[6], w[9], pars->GC_11, amp[671]); 
  FFV1_0(w[1], w[104], w[9], pars->GC_11, amp[672]); 
  FFV1_0(w[105], w[6], w[9], pars->GC_11, amp[673]); 
  FFV1_0(w[19], w[5], w[22], pars->GC_11, amp[674]); 
  FFV1_0(w[23], w[5], w[8], pars->GC_2, amp[675]); 
  FFV1_0(w[21], w[5], w[22], pars->GC_11, amp[676]); 
  FFV2_5_0(w[23], w[5], w[14], pars->GC_51, pars->GC_58, amp[677]); 
  FFV1_0(w[1], w[101], w[29], pars->GC_11, amp[678]); 
  FFV1_0(w[102], w[6], w[29], pars->GC_11, amp[679]); 
  FFV1_0(w[1], w[104], w[29], pars->GC_11, amp[680]); 
  FFV1_0(w[105], w[6], w[29], pars->GC_11, amp[681]); 
  FFV1_0(w[0], w[27], w[22], pars->GC_11, amp[682]); 
  FFV1_0(w[0], w[33], w[8], pars->GC_2, amp[683]); 
  FFV1_0(w[0], w[28], w[22], pars->GC_11, amp[684]); 
  FFV2_5_0(w[0], w[33], w[14], pars->GC_51, pars->GC_58, amp[685]); 
  FFV1_0(w[1], w[36], w[8], pars->GC_1, amp[686]); 
  FFV1_0(w[1], w[113], w[35], pars->GC_11, amp[687]); 
  FFV2_3_0(w[1], w[36], w[14], pars->GC_50, pars->GC_58, amp[688]); 
  FFV1_0(w[1], w[114], w[35], pars->GC_11, amp[689]); 
  FFV1_0(w[31], w[5], w[40], pars->GC_11, amp[690]); 
  FFV1_0(w[0], w[13], w[40], pars->GC_11, amp[691]); 
  FFV1_0(w[32], w[5], w[40], pars->GC_11, amp[692]); 
  FFV1_0(w[0], w[17], w[40], pars->GC_11, amp[693]); 
  FFV1_0(w[43], w[6], w[8], pars->GC_1, amp[694]); 
  FFV1_0(w[115], w[6], w[35], pars->GC_11, amp[695]); 
  FFV2_3_0(w[43], w[6], w[14], pars->GC_50, pars->GC_58, amp[696]); 
  FFV1_0(w[116], w[6], w[35], pars->GC_11, amp[697]); 
  FFV1_0(w[31], w[5], w[48], pars->GC_11, amp[698]); 
  FFV1_0(w[0], w[13], w[48], pars->GC_11, amp[699]); 
  FFV1_0(w[32], w[5], w[48], pars->GC_11, amp[700]); 
  FFV1_0(w[0], w[17], w[48], pars->GC_11, amp[701]); 
  FFV1_0(w[1], w[101], w[49], pars->GC_11, amp[702]); 
  FFV1_0(w[102], w[6], w[49], pars->GC_11, amp[703]); 
  FFV1_0(w[102], w[50], w[4], pars->GC_11, amp[704]); 
  FFV1_0(w[51], w[101], w[4], pars->GC_11, amp[705]); 
  FFV1_0(w[1], w[104], w[49], pars->GC_11, amp[706]); 
  FFV1_0(w[105], w[6], w[49], pars->GC_11, amp[707]); 
  FFV1_0(w[105], w[50], w[4], pars->GC_11, amp[708]); 
  FFV1_0(w[51], w[104], w[4], pars->GC_11, amp[709]); 
  FFV1_0(w[31], w[5], w[58], pars->GC_11, amp[710]); 
  FFV1_0(w[0], w[13], w[58], pars->GC_11, amp[711]); 
  FFV1_0(w[31], w[59], w[4], pars->GC_11, amp[712]); 
  FFV1_0(w[60], w[13], w[4], pars->GC_11, amp[713]); 
  FFV1_0(w[32], w[5], w[58], pars->GC_11, amp[714]); 
  FFV1_0(w[0], w[17], w[58], pars->GC_11, amp[715]); 
  FFV1_0(w[32], w[59], w[4], pars->GC_11, amp[716]); 
  FFV1_0(w[60], w[17], w[4], pars->GC_11, amp[717]); 
  FFV1_0(w[62], w[13], w[63], pars->GC_11, amp[718]); 
  FFV1_0(w[64], w[5], w[63], pars->GC_11, amp[719]); 
  FFV1_0(w[62], w[17], w[63], pars->GC_11, amp[720]); 
  FFV1_0(w[66], w[5], w[63], pars->GC_11, amp[721]); 
  FFV1_0(w[19], w[61], w[70], pars->GC_11, amp[722]); 
  FFV1_0(w[71], w[61], w[8], pars->GC_2, amp[723]); 
  FFV1_0(w[21], w[61], w[70], pars->GC_11, amp[724]); 
  FFV2_5_0(w[71], w[61], w[14], pars->GC_51, pars->GC_58, amp[725]); 
  FFV1_0(w[62], w[13], w[76], pars->GC_11, amp[726]); 
  FFV1_0(w[64], w[5], w[76], pars->GC_11, amp[727]); 
  FFV1_0(w[62], w[17], w[76], pars->GC_11, amp[728]); 
  FFV1_0(w[66], w[5], w[76], pars->GC_11, amp[729]); 
  FFV1_0(w[0], w[74], w[70], pars->GC_11, amp[730]); 
  FFV1_0(w[0], w[78], w[8], pars->GC_2, amp[731]); 
  FFV1_0(w[0], w[75], w[70], pars->GC_11, amp[732]); 
  FFV2_5_0(w[0], w[78], w[14], pars->GC_51, pars->GC_58, amp[733]); 
  FFV1_0(w[62], w[80], w[8], pars->GC_2, amp[734]); 
  FFV1_0(w[62], w[27], w[79], pars->GC_11, amp[735]); 
  FFV2_5_0(w[62], w[80], w[14], pars->GC_51, pars->GC_58, amp[736]); 
  FFV1_0(w[62], w[28], w[79], pars->GC_11, amp[737]); 
  FFV1_0(w[31], w[61], w[81], pars->GC_11, amp[738]); 
  FFV1_0(w[0], w[65], w[81], pars->GC_11, amp[739]); 
  FFV1_0(w[32], w[61], w[81], pars->GC_11, amp[740]); 
  FFV1_0(w[0], w[67], w[81], pars->GC_11, amp[741]); 
  FFV1_0(w[84], w[5], w[8], pars->GC_2, amp[742]); 
  FFV1_0(w[85], w[5], w[79], pars->GC_11, amp[743]); 
  FFV2_5_0(w[84], w[5], w[14], pars->GC_51, pars->GC_58, amp[744]); 
  FFV1_0(w[86], w[5], w[79], pars->GC_11, amp[745]); 
  FFV1_0(w[31], w[61], w[89], pars->GC_11, amp[746]); 
  FFV1_0(w[0], w[65], w[89], pars->GC_11, amp[747]); 
  FFV1_0(w[32], w[61], w[89], pars->GC_11, amp[748]); 
  FFV1_0(w[0], w[67], w[89], pars->GC_11, amp[749]); 
  FFV1_0(w[62], w[13], w[90], pars->GC_11, amp[750]); 
  FFV1_0(w[64], w[5], w[90], pars->GC_11, amp[751]); 
  FFV1_0(w[64], w[91], w[4], pars->GC_11, amp[752]); 
  FFV1_0(w[92], w[13], w[4], pars->GC_11, amp[753]); 
  FFV1_0(w[62], w[17], w[90], pars->GC_11, amp[754]); 
  FFV1_0(w[66], w[5], w[90], pars->GC_11, amp[755]); 
  FFV1_0(w[66], w[91], w[4], pars->GC_11, amp[756]); 
  FFV1_0(w[92], w[17], w[4], pars->GC_11, amp[757]); 
  FFV1_0(w[31], w[61], w[98], pars->GC_11, amp[758]); 
  FFV1_0(w[0], w[65], w[98], pars->GC_11, amp[759]); 
  FFV1_0(w[31], w[99], w[4], pars->GC_11, amp[760]); 
  FFV1_0(w[100], w[65], w[4], pars->GC_11, amp[761]); 
  FFV1_0(w[32], w[61], w[98], pars->GC_11, amp[762]); 
  FFV1_0(w[0], w[67], w[98], pars->GC_11, amp[763]); 
  FFV1_0(w[32], w[99], w[4], pars->GC_11, amp[764]); 
  FFV1_0(w[100], w[67], w[4], pars->GC_11, amp[765]); 
  FFV1_0(w[62], w[103], w[63], pars->GC_11, amp[766]); 
  FFV1_0(w[117], w[5], w[63], pars->GC_11, amp[767]); 
  FFV1_0(w[62], w[106], w[63], pars->GC_11, amp[768]); 
  FFV1_0(w[119], w[5], w[63], pars->GC_11, amp[769]); 
  FFV1_0(w[19], w[61], w[70], pars->GC_11, amp[770]); 
  FFV1_0(w[71], w[61], w[8], pars->GC_2, amp[771]); 
  FFV1_0(w[21], w[61], w[70], pars->GC_11, amp[772]); 
  FFV2_5_0(w[71], w[61], w[14], pars->GC_51, pars->GC_58, amp[773]); 
  FFV1_0(w[62], w[103], w[76], pars->GC_11, amp[774]); 
  FFV1_0(w[117], w[5], w[76], pars->GC_11, amp[775]); 
  FFV1_0(w[62], w[106], w[76], pars->GC_11, amp[776]); 
  FFV1_0(w[119], w[5], w[76], pars->GC_11, amp[777]); 
  FFV1_0(w[0], w[74], w[70], pars->GC_11, amp[778]); 
  FFV1_0(w[0], w[78], w[8], pars->GC_2, amp[779]); 
  FFV1_0(w[0], w[75], w[70], pars->GC_11, amp[780]); 
  FFV2_5_0(w[0], w[78], w[14], pars->GC_51, pars->GC_58, amp[781]); 
  FFV1_0(w[62], w[80], w[8], pars->GC_1, amp[782]); 
  FFV1_0(w[62], w[109], w[79], pars->GC_11, amp[783]); 
  FFV2_3_0(w[62], w[80], w[14], pars->GC_50, pars->GC_58, amp[784]); 
  FFV1_0(w[62], w[110], w[79], pars->GC_11, amp[785]); 
  FFV1_0(w[31], w[61], w[81], pars->GC_11, amp[786]); 
  FFV1_0(w[0], w[65], w[81], pars->GC_11, amp[787]); 
  FFV1_0(w[32], w[61], w[81], pars->GC_11, amp[788]); 
  FFV1_0(w[0], w[67], w[81], pars->GC_11, amp[789]); 
  FFV1_0(w[84], w[5], w[8], pars->GC_1, amp[790]); 
  FFV1_0(w[123], w[5], w[79], pars->GC_11, amp[791]); 
  FFV2_3_0(w[84], w[5], w[14], pars->GC_50, pars->GC_58, amp[792]); 
  FFV1_0(w[124], w[5], w[79], pars->GC_11, amp[793]); 
  FFV1_0(w[31], w[61], w[89], pars->GC_11, amp[794]); 
  FFV1_0(w[0], w[65], w[89], pars->GC_11, amp[795]); 
  FFV1_0(w[32], w[61], w[89], pars->GC_11, amp[796]); 
  FFV1_0(w[0], w[67], w[89], pars->GC_11, amp[797]); 
  FFV1_0(w[62], w[103], w[90], pars->GC_11, amp[798]); 
  FFV1_0(w[117], w[5], w[90], pars->GC_11, amp[799]); 
  FFV1_0(w[117], w[91], w[4], pars->GC_11, amp[800]); 
  FFV1_0(w[92], w[103], w[4], pars->GC_11, amp[801]); 
  FFV1_0(w[62], w[106], w[90], pars->GC_11, amp[802]); 
  FFV1_0(w[119], w[5], w[90], pars->GC_11, amp[803]); 
  FFV1_0(w[119], w[91], w[4], pars->GC_11, amp[804]); 
  FFV1_0(w[92], w[106], w[4], pars->GC_11, amp[805]); 
  FFV1_0(w[31], w[61], w[98], pars->GC_11, amp[806]); 
  FFV1_0(w[0], w[65], w[98], pars->GC_11, amp[807]); 
  FFV1_0(w[31], w[99], w[4], pars->GC_11, amp[808]); 
  FFV1_0(w[100], w[65], w[4], pars->GC_11, amp[809]); 
  FFV1_0(w[32], w[61], w[98], pars->GC_11, amp[810]); 
  FFV1_0(w[0], w[67], w[98], pars->GC_11, amp[811]); 
  FFV1_0(w[32], w[99], w[4], pars->GC_11, amp[812]); 
  FFV1_0(w[100], w[67], w[4], pars->GC_11, amp[813]); 
  FFV1_0(w[62], w[176], w[63], pars->GC_11, amp[814]); 
  FFV1_0(w[182], w[5], w[63], pars->GC_11, amp[815]); 
  FFV1_0(w[62], w[183], w[9], pars->GC_11, amp[816]); 
  FFV1_0(w[182], w[61], w[9], pars->GC_11, amp[817]); 
  FFV1_0(w[177], w[5], w[68], pars->GC_11, amp[818]); 
  FFV2_5_0(w[69], w[5], w[173], pars->GC_51, pars->GC_58, amp[819]); 
  FFV1_0(w[177], w[61], w[70], pars->GC_11, amp[820]); 
  FFV2_5_0(w[71], w[61], w[173], pars->GC_51, pars->GC_58, amp[821]); 
  FFV2_5_0(w[62], w[73], w[173], pars->GC_51, pars->GC_58, amp[822]); 
  FFV1_0(w[62], w[184], w[35], pars->GC_11, amp[823]); 
  FFV1_0(w[62], w[176], w[76], pars->GC_11, amp[824]); 
  FFV1_0(w[182], w[5], w[76], pars->GC_11, amp[825]); 
  FFV1_0(w[179], w[5], w[77], pars->GC_11, amp[826]); 
  FFV1_0(w[0], w[176], w[77], pars->GC_11, amp[827]); 
  FFV1_0(w[0], w[184], w[70], pars->GC_11, amp[828]); 
  FFV2_5_0(w[0], w[78], w[173], pars->GC_51, pars->GC_58, amp[829]); 
  FFV2_5_0(w[62], w[80], w[173], pars->GC_51, pars->GC_58, amp[830]); 
  FFV1_0(w[62], w[178], w[79], pars->GC_11, amp[831]); 
  FFV1_0(w[62], w[183], w[29], pars->GC_11, amp[832]); 
  FFV1_0(w[182], w[61], w[29], pars->GC_11, amp[833]); 
  FFV1_0(w[179], w[61], w[81], pars->GC_11, amp[834]); 
  FFV1_0(w[0], w[183], w[81], pars->GC_11, amp[835]); 
  FFV1_0(w[0], w[178], w[68], pars->GC_11, amp[836]); 
  FFV2_5_0(w[0], w[82], w[173], pars->GC_51, pars->GC_58, amp[837]); 
  FFV2_5_0(w[84], w[5], w[173], pars->GC_51, pars->GC_58, amp[838]); 
  FFV1_0(w[185], w[5], w[79], pars->GC_11, amp[839]); 
  FFV2_5_0(w[87], w[61], w[173], pars->GC_51, pars->GC_58, amp[840]); 
  FFV1_0(w[185], w[61], w[35], pars->GC_11, amp[841]); 
  FFV1_0(w[179], w[5], w[88], pars->GC_11, amp[842]); 
  FFV1_0(w[0], w[176], w[88], pars->GC_11, amp[843]); 
  FFV1_0(w[179], w[61], w[89], pars->GC_11, amp[844]); 
  FFV1_0(w[0], w[183], w[89], pars->GC_11, amp[845]); 
  FFV1_0(w[62], w[176], w[90], pars->GC_11, amp[846]); 
  FFV1_0(w[182], w[5], w[90], pars->GC_11, amp[847]); 
  FFV1_0(w[182], w[91], w[4], pars->GC_11, amp[848]); 
  FFV1_0(w[92], w[176], w[4], pars->GC_11, amp[849]); 
  FFV1_0(w[62], w[183], w[49], pars->GC_11, amp[850]); 
  FFV1_0(w[182], w[61], w[49], pars->GC_11, amp[851]); 
  FFV1_0(w[182], w[93], w[4], pars->GC_11, amp[852]); 
  FFV1_0(w[94], w[183], w[4], pars->GC_11, amp[853]); 
  FFV1_0(w[179], w[5], w[95], pars->GC_11, amp[854]); 
  FFV1_0(w[0], w[176], w[95], pars->GC_11, amp[855]); 
  FFV1_0(w[179], w[96], w[4], pars->GC_11, amp[856]); 
  FFV1_0(w[97], w[176], w[4], pars->GC_11, amp[857]); 
  FFV1_0(w[179], w[61], w[98], pars->GC_11, amp[858]); 
  FFV1_0(w[0], w[183], w[98], pars->GC_11, amp[859]); 
  FFV1_0(w[179], w[99], w[4], pars->GC_11, amp[860]); 
  FFV1_0(w[100], w[183], w[4], pars->GC_11, amp[861]); 
  FFV1_0(w[62], w[65], w[9], pars->GC_11, amp[862]); 
  FFV1_0(w[64], w[61], w[9], pars->GC_11, amp[863]); 
  FFV1_0(w[62], w[67], w[9], pars->GC_11, amp[864]); 
  FFV1_0(w[66], w[61], w[9], pars->GC_11, amp[865]); 
  FFV1_0(w[19], w[5], w[68], pars->GC_11, amp[866]); 
  FFV1_0(w[69], w[5], w[8], pars->GC_2, amp[867]); 
  FFV1_0(w[21], w[5], w[68], pars->GC_11, amp[868]); 
  FFV2_5_0(w[69], w[5], w[14], pars->GC_51, pars->GC_58, amp[869]); 
  FFV1_0(w[62], w[65], w[29], pars->GC_11, amp[870]); 
  FFV1_0(w[64], w[61], w[29], pars->GC_11, amp[871]); 
  FFV1_0(w[62], w[67], w[29], pars->GC_11, amp[872]); 
  FFV1_0(w[66], w[61], w[29], pars->GC_11, amp[873]); 
  FFV1_0(w[0], w[27], w[68], pars->GC_11, amp[874]); 
  FFV1_0(w[0], w[82], w[8], pars->GC_2, amp[875]); 
  FFV1_0(w[0], w[28], w[68], pars->GC_11, amp[876]); 
  FFV2_5_0(w[0], w[82], w[14], pars->GC_51, pars->GC_58, amp[877]); 
  FFV1_0(w[62], w[73], w[8], pars->GC_2, amp[878]); 
  FFV1_0(w[62], w[74], w[35], pars->GC_11, amp[879]); 
  FFV2_5_0(w[62], w[73], w[14], pars->GC_51, pars->GC_58, amp[880]); 
  FFV1_0(w[62], w[75], w[35], pars->GC_11, amp[881]); 
  FFV1_0(w[31], w[5], w[77], pars->GC_11, amp[882]); 
  FFV1_0(w[0], w[13], w[77], pars->GC_11, amp[883]); 
  FFV1_0(w[32], w[5], w[77], pars->GC_11, amp[884]); 
  FFV1_0(w[0], w[17], w[77], pars->GC_11, amp[885]); 
  FFV1_0(w[87], w[61], w[8], pars->GC_2, amp[886]); 
  FFV1_0(w[85], w[61], w[35], pars->GC_11, amp[887]); 
  FFV2_5_0(w[87], w[61], w[14], pars->GC_51, pars->GC_58, amp[888]); 
  FFV1_0(w[86], w[61], w[35], pars->GC_11, amp[889]); 
  FFV1_0(w[31], w[5], w[88], pars->GC_11, amp[890]); 
  FFV1_0(w[0], w[13], w[88], pars->GC_11, amp[891]); 
  FFV1_0(w[32], w[5], w[88], pars->GC_11, amp[892]); 
  FFV1_0(w[0], w[17], w[88], pars->GC_11, amp[893]); 
  FFV1_0(w[62], w[65], w[49], pars->GC_11, amp[894]); 
  FFV1_0(w[64], w[61], w[49], pars->GC_11, amp[895]); 
  FFV1_0(w[64], w[93], w[4], pars->GC_11, amp[896]); 
  FFV1_0(w[94], w[65], w[4], pars->GC_11, amp[897]); 
  FFV1_0(w[62], w[67], w[49], pars->GC_11, amp[898]); 
  FFV1_0(w[66], w[61], w[49], pars->GC_11, amp[899]); 
  FFV1_0(w[66], w[93], w[4], pars->GC_11, amp[900]); 
  FFV1_0(w[94], w[67], w[4], pars->GC_11, amp[901]); 
  FFV1_0(w[31], w[5], w[95], pars->GC_11, amp[902]); 
  FFV1_0(w[0], w[13], w[95], pars->GC_11, amp[903]); 
  FFV1_0(w[31], w[96], w[4], pars->GC_11, amp[904]); 
  FFV1_0(w[97], w[13], w[4], pars->GC_11, amp[905]); 
  FFV1_0(w[32], w[5], w[95], pars->GC_11, amp[906]); 
  FFV1_0(w[0], w[17], w[95], pars->GC_11, amp[907]); 
  FFV1_0(w[32], w[96], w[4], pars->GC_11, amp[908]); 
  FFV1_0(w[97], w[17], w[4], pars->GC_11, amp[909]); 
  FFV1_0(w[62], w[118], w[9], pars->GC_11, amp[910]); 
  FFV1_0(w[117], w[61], w[9], pars->GC_11, amp[911]); 
  FFV1_0(w[62], w[120], w[9], pars->GC_11, amp[912]); 
  FFV1_0(w[119], w[61], w[9], pars->GC_11, amp[913]); 
  FFV1_0(w[19], w[5], w[68], pars->GC_11, amp[914]); 
  FFV1_0(w[69], w[5], w[8], pars->GC_2, amp[915]); 
  FFV1_0(w[21], w[5], w[68], pars->GC_11, amp[916]); 
  FFV2_5_0(w[69], w[5], w[14], pars->GC_51, pars->GC_58, amp[917]); 
  FFV1_0(w[62], w[118], w[29], pars->GC_11, amp[918]); 
  FFV1_0(w[117], w[61], w[29], pars->GC_11, amp[919]); 
  FFV1_0(w[62], w[120], w[29], pars->GC_11, amp[920]); 
  FFV1_0(w[119], w[61], w[29], pars->GC_11, amp[921]); 
  FFV1_0(w[0], w[27], w[68], pars->GC_11, amp[922]); 
  FFV1_0(w[0], w[82], w[8], pars->GC_2, amp[923]); 
  FFV1_0(w[0], w[28], w[68], pars->GC_11, amp[924]); 
  FFV2_5_0(w[0], w[82], w[14], pars->GC_51, pars->GC_58, amp[925]); 
  FFV1_0(w[62], w[73], w[8], pars->GC_1, amp[926]); 
  FFV1_0(w[62], w[121], w[35], pars->GC_11, amp[927]); 
  FFV2_3_0(w[62], w[73], w[14], pars->GC_50, pars->GC_58, amp[928]); 
  FFV1_0(w[62], w[122], w[35], pars->GC_11, amp[929]); 
  FFV1_0(w[31], w[5], w[77], pars->GC_11, amp[930]); 
  FFV1_0(w[0], w[13], w[77], pars->GC_11, amp[931]); 
  FFV1_0(w[32], w[5], w[77], pars->GC_11, amp[932]); 
  FFV1_0(w[0], w[17], w[77], pars->GC_11, amp[933]); 
  FFV1_0(w[87], w[61], w[8], pars->GC_1, amp[934]); 
  FFV1_0(w[123], w[61], w[35], pars->GC_11, amp[935]); 
  FFV2_3_0(w[87], w[61], w[14], pars->GC_50, pars->GC_58, amp[936]); 
  FFV1_0(w[124], w[61], w[35], pars->GC_11, amp[937]); 
  FFV1_0(w[31], w[5], w[88], pars->GC_11, amp[938]); 
  FFV1_0(w[0], w[13], w[88], pars->GC_11, amp[939]); 
  FFV1_0(w[32], w[5], w[88], pars->GC_11, amp[940]); 
  FFV1_0(w[0], w[17], w[88], pars->GC_11, amp[941]); 
  FFV1_0(w[62], w[118], w[49], pars->GC_11, amp[942]); 
  FFV1_0(w[117], w[61], w[49], pars->GC_11, amp[943]); 
  FFV1_0(w[117], w[93], w[4], pars->GC_11, amp[944]); 
  FFV1_0(w[94], w[118], w[4], pars->GC_11, amp[945]); 
  FFV1_0(w[62], w[120], w[49], pars->GC_11, amp[946]); 
  FFV1_0(w[119], w[61], w[49], pars->GC_11, amp[947]); 
  FFV1_0(w[119], w[93], w[4], pars->GC_11, amp[948]); 
  FFV1_0(w[94], w[120], w[4], pars->GC_11, amp[949]); 
  FFV1_0(w[31], w[5], w[95], pars->GC_11, amp[950]); 
  FFV1_0(w[0], w[13], w[95], pars->GC_11, amp[951]); 
  FFV1_0(w[31], w[96], w[4], pars->GC_11, amp[952]); 
  FFV1_0(w[97], w[13], w[4], pars->GC_11, amp[953]); 
  FFV1_0(w[32], w[5], w[95], pars->GC_11, amp[954]); 
  FFV1_0(w[0], w[17], w[95], pars->GC_11, amp[955]); 
  FFV1_0(w[32], w[96], w[4], pars->GC_11, amp[956]); 
  FFV1_0(w[97], w[17], w[4], pars->GC_11, amp[957]); 
  FFV1_0(w[1], w[186], w[9], pars->GC_11, amp[958]); 
  FFV1_0(w[187], w[6], w[9], pars->GC_11, amp[959]); 
  FFV1_0(w[1], w[188], w[12], pars->GC_11, amp[960]); 
  FFV1_0(w[187], w[5], w[12], pars->GC_11, amp[961]); 
  FFV1_0(w[189], w[6], w[18], pars->GC_11, amp[962]); 
  FFV2_3_0(w[20], w[6], w[173], pars->GC_50, pars->GC_58, amp[963]); 
  FFV1_0(w[189], w[5], w[22], pars->GC_11, amp[964]); 
  FFV2_3_0(w[23], w[5], w[173], pars->GC_50, pars->GC_58, amp[965]); 
  FFV2_3_0(w[1], w[26], w[173], pars->GC_50, pars->GC_58, amp[966]); 
  FFV1_0(w[1], w[190], w[25], pars->GC_11, amp[967]); 
  FFV1_0(w[1], w[186], w[29], pars->GC_11, amp[968]); 
  FFV1_0(w[187], w[6], w[29], pars->GC_11, amp[969]); 
  FFV1_0(w[191], w[6], w[30], pars->GC_11, amp[970]); 
  FFV1_0(w[0], w[186], w[30], pars->GC_11, amp[971]); 
  FFV1_0(w[0], w[190], w[22], pars->GC_11, amp[972]); 
  FFV2_3_0(w[0], w[33], w[173], pars->GC_50, pars->GC_58, amp[973]); 
  FFV2_3_0(w[1], w[36], w[173], pars->GC_50, pars->GC_58, amp[974]); 
  FFV1_0(w[1], w[192], w[35], pars->GC_11, amp[975]); 
  FFV1_0(w[1], w[188], w[39], pars->GC_11, amp[976]); 
  FFV1_0(w[187], w[5], w[39], pars->GC_11, amp[977]); 
  FFV1_0(w[191], w[5], w[40], pars->GC_11, amp[978]); 
  FFV1_0(w[0], w[188], w[40], pars->GC_11, amp[979]); 
  FFV1_0(w[0], w[192], w[18], pars->GC_11, amp[980]); 
  FFV2_3_0(w[0], w[41], w[173], pars->GC_50, pars->GC_58, amp[981]); 
  FFV2_3_0(w[43], w[6], w[173], pars->GC_50, pars->GC_58, amp[982]); 
  FFV1_0(w[193], w[6], w[35], pars->GC_11, amp[983]); 
  FFV2_3_0(w[46], w[5], w[173], pars->GC_50, pars->GC_58, amp[984]); 
  FFV1_0(w[193], w[5], w[25], pars->GC_11, amp[985]); 
  FFV1_0(w[191], w[6], w[47], pars->GC_11, amp[986]); 
  FFV1_0(w[0], w[186], w[47], pars->GC_11, amp[987]); 
  FFV1_0(w[191], w[5], w[48], pars->GC_11, amp[988]); 
  FFV1_0(w[0], w[188], w[48], pars->GC_11, amp[989]); 
  FFV1_0(w[1], w[186], w[49], pars->GC_11, amp[990]); 
  FFV1_0(w[187], w[6], w[49], pars->GC_11, amp[991]); 
  FFV1_0(w[187], w[50], w[4], pars->GC_11, amp[992]); 
  FFV1_0(w[51], w[186], w[4], pars->GC_11, amp[993]); 
  FFV1_0(w[1], w[188], w[52], pars->GC_11, amp[994]); 
  FFV1_0(w[187], w[5], w[52], pars->GC_11, amp[995]); 
  FFV1_0(w[187], w[53], w[4], pars->GC_11, amp[996]); 
  FFV1_0(w[54], w[188], w[4], pars->GC_11, amp[997]); 
  FFV1_0(w[191], w[6], w[55], pars->GC_11, amp[998]); 
  FFV1_0(w[0], w[186], w[55], pars->GC_11, amp[999]); 
  FFV1_0(w[191], w[56], w[4], pars->GC_11, amp[1000]); 
  FFV1_0(w[57], w[186], w[4], pars->GC_11, amp[1001]); 
  FFV1_0(w[191], w[5], w[58], pars->GC_11, amp[1002]); 
  FFV1_0(w[0], w[188], w[58], pars->GC_11, amp[1003]); 
  FFV1_0(w[191], w[59], w[4], pars->GC_11, amp[1004]); 
  FFV1_0(w[60], w[188], w[4], pars->GC_11, amp[1005]); 
  FFV1_0(w[1], w[101], w[9], pars->GC_11, amp[1006]); 
  FFV1_0(w[102], w[6], w[9], pars->GC_11, amp[1007]); 
  FFV1_0(w[1], w[104], w[9], pars->GC_11, amp[1008]); 
  FFV1_0(w[105], w[6], w[9], pars->GC_11, amp[1009]); 
  FFV1_0(w[107], w[5], w[22], pars->GC_11, amp[1010]); 
  FFV1_0(w[23], w[5], w[8], pars->GC_1, amp[1011]); 
  FFV1_0(w[108], w[5], w[22], pars->GC_11, amp[1012]); 
  FFV2_3_0(w[23], w[5], w[14], pars->GC_50, pars->GC_58, amp[1013]); 
  FFV1_0(w[1], w[101], w[29], pars->GC_11, amp[1014]); 
  FFV1_0(w[102], w[6], w[29], pars->GC_11, amp[1015]); 
  FFV1_0(w[1], w[104], w[29], pars->GC_11, amp[1016]); 
  FFV1_0(w[105], w[6], w[29], pars->GC_11, amp[1017]); 
  FFV1_0(w[0], w[109], w[22], pars->GC_11, amp[1018]); 
  FFV1_0(w[0], w[33], w[8], pars->GC_1, amp[1019]); 
  FFV1_0(w[0], w[110], w[22], pars->GC_11, amp[1020]); 
  FFV2_3_0(w[0], w[33], w[14], pars->GC_50, pars->GC_58, amp[1021]); 
  FFV1_0(w[1], w[36], w[8], pars->GC_1, amp[1022]); 
  FFV1_0(w[1], w[113], w[35], pars->GC_11, amp[1023]); 
  FFV2_3_0(w[1], w[36], w[14], pars->GC_50, pars->GC_58, amp[1024]); 
  FFV1_0(w[1], w[114], w[35], pars->GC_11, amp[1025]); 
  FFV1_0(w[111], w[5], w[40], pars->GC_11, amp[1026]); 
  FFV1_0(w[0], w[103], w[40], pars->GC_11, amp[1027]); 
  FFV1_0(w[112], w[5], w[40], pars->GC_11, amp[1028]); 
  FFV1_0(w[0], w[106], w[40], pars->GC_11, amp[1029]); 
  FFV1_0(w[43], w[6], w[8], pars->GC_1, amp[1030]); 
  FFV1_0(w[115], w[6], w[35], pars->GC_11, amp[1031]); 
  FFV2_3_0(w[43], w[6], w[14], pars->GC_50, pars->GC_58, amp[1032]); 
  FFV1_0(w[116], w[6], w[35], pars->GC_11, amp[1033]); 
  FFV1_0(w[111], w[5], w[48], pars->GC_11, amp[1034]); 
  FFV1_0(w[0], w[103], w[48], pars->GC_11, amp[1035]); 
  FFV1_0(w[112], w[5], w[48], pars->GC_11, amp[1036]); 
  FFV1_0(w[0], w[106], w[48], pars->GC_11, amp[1037]); 
  FFV1_0(w[1], w[101], w[49], pars->GC_11, amp[1038]); 
  FFV1_0(w[102], w[6], w[49], pars->GC_11, amp[1039]); 
  FFV1_0(w[102], w[50], w[4], pars->GC_11, amp[1040]); 
  FFV1_0(w[51], w[101], w[4], pars->GC_11, amp[1041]); 
  FFV1_0(w[1], w[104], w[49], pars->GC_11, amp[1042]); 
  FFV1_0(w[105], w[6], w[49], pars->GC_11, amp[1043]); 
  FFV1_0(w[105], w[50], w[4], pars->GC_11, amp[1044]); 
  FFV1_0(w[51], w[104], w[4], pars->GC_11, amp[1045]); 
  FFV1_0(w[111], w[5], w[58], pars->GC_11, amp[1046]); 
  FFV1_0(w[0], w[103], w[58], pars->GC_11, amp[1047]); 
  FFV1_0(w[111], w[59], w[4], pars->GC_11, amp[1048]); 
  FFV1_0(w[60], w[103], w[4], pars->GC_11, amp[1049]); 
  FFV1_0(w[112], w[5], w[58], pars->GC_11, amp[1050]); 
  FFV1_0(w[0], w[106], w[58], pars->GC_11, amp[1051]); 
  FFV1_0(w[112], w[59], w[4], pars->GC_11, amp[1052]); 
  FFV1_0(w[60], w[106], w[4], pars->GC_11, amp[1053]); 
  FFV1_0(w[0], w[103], w[88], pars->GC_11, amp[1054]); 
  FFV1_0(w[111], w[5], w[88], pars->GC_11, amp[1055]); 
  FFV1_0(w[0], w[106], w[88], pars->GC_11, amp[1056]); 
  FFV1_0(w[112], w[5], w[88], pars->GC_11, amp[1057]); 
  FFV1_0(w[85], w[61], w[35], pars->GC_11, amp[1058]); 
  FFV1_0(w[87], w[61], w[8], pars->GC_2, amp[1059]); 
  FFV1_0(w[86], w[61], w[35], pars->GC_11, amp[1060]); 
  FFV2_5_0(w[87], w[61], w[14], pars->GC_51, pars->GC_58, amp[1061]); 
  FFV1_0(w[0], w[103], w[77], pars->GC_11, amp[1062]); 
  FFV1_0(w[111], w[5], w[77], pars->GC_11, amp[1063]); 
  FFV1_0(w[0], w[106], w[77], pars->GC_11, amp[1064]); 
  FFV1_0(w[112], w[5], w[77], pars->GC_11, amp[1065]); 
  FFV1_0(w[62], w[74], w[35], pars->GC_11, amp[1066]); 
  FFV1_0(w[62], w[73], w[8], pars->GC_2, amp[1067]); 
  FFV1_0(w[62], w[75], w[35], pars->GC_11, amp[1068]); 
  FFV2_5_0(w[62], w[73], w[14], pars->GC_51, pars->GC_58, amp[1069]); 
  FFV1_0(w[0], w[82], w[8], pars->GC_1, amp[1070]); 
  FFV1_0(w[0], w[109], w[68], pars->GC_11, amp[1071]); 
  FFV2_3_0(w[0], w[82], w[14], pars->GC_50, pars->GC_58, amp[1072]); 
  FFV1_0(w[0], w[110], w[68], pars->GC_11, amp[1073]); 
  FFV1_0(w[64], w[61], w[29], pars->GC_11, amp[1074]); 
  FFV1_0(w[62], w[65], w[29], pars->GC_11, amp[1075]); 
  FFV1_0(w[66], w[61], w[29], pars->GC_11, amp[1076]); 
  FFV1_0(w[62], w[67], w[29], pars->GC_11, amp[1077]); 
  FFV1_0(w[69], w[5], w[8], pars->GC_1, amp[1078]); 
  FFV1_0(w[107], w[5], w[68], pars->GC_11, amp[1079]); 
  FFV2_3_0(w[69], w[5], w[14], pars->GC_50, pars->GC_58, amp[1080]); 
  FFV1_0(w[108], w[5], w[68], pars->GC_11, amp[1081]); 
  FFV1_0(w[64], w[61], w[9], pars->GC_11, amp[1082]); 
  FFV1_0(w[62], w[65], w[9], pars->GC_11, amp[1083]); 
  FFV1_0(w[66], w[61], w[9], pars->GC_11, amp[1084]); 
  FFV1_0(w[62], w[67], w[9], pars->GC_11, amp[1085]); 
  FFV1_0(w[0], w[103], w[95], pars->GC_11, amp[1086]); 
  FFV1_0(w[111], w[5], w[95], pars->GC_11, amp[1087]); 
  FFV1_0(w[111], w[96], w[4], pars->GC_11, amp[1088]); 
  FFV1_0(w[97], w[103], w[4], pars->GC_11, amp[1089]); 
  FFV1_0(w[0], w[106], w[95], pars->GC_11, amp[1090]); 
  FFV1_0(w[112], w[5], w[95], pars->GC_11, amp[1091]); 
  FFV1_0(w[112], w[96], w[4], pars->GC_11, amp[1092]); 
  FFV1_0(w[97], w[106], w[4], pars->GC_11, amp[1093]); 
  FFV1_0(w[64], w[61], w[49], pars->GC_11, amp[1094]); 
  FFV1_0(w[62], w[65], w[49], pars->GC_11, amp[1095]); 
  FFV1_0(w[64], w[93], w[4], pars->GC_11, amp[1096]); 
  FFV1_0(w[94], w[65], w[4], pars->GC_11, amp[1097]); 
  FFV1_0(w[66], w[61], w[49], pars->GC_11, amp[1098]); 
  FFV1_0(w[62], w[67], w[49], pars->GC_11, amp[1099]); 
  FFV1_0(w[66], w[93], w[4], pars->GC_11, amp[1100]); 
  FFV1_0(w[94], w[67], w[4], pars->GC_11, amp[1101]); 
  FFV1_0(w[0], w[118], w[89], pars->GC_11, amp[1102]); 
  FFV1_0(w[111], w[61], w[89], pars->GC_11, amp[1103]); 
  FFV1_0(w[0], w[120], w[89], pars->GC_11, amp[1104]); 
  FFV1_0(w[112], w[61], w[89], pars->GC_11, amp[1105]); 
  FFV1_0(w[85], w[5], w[79], pars->GC_11, amp[1106]); 
  FFV1_0(w[84], w[5], w[8], pars->GC_2, amp[1107]); 
  FFV1_0(w[86], w[5], w[79], pars->GC_11, amp[1108]); 
  FFV2_5_0(w[84], w[5], w[14], pars->GC_51, pars->GC_58, amp[1109]); 
  FFV1_0(w[0], w[118], w[81], pars->GC_11, amp[1110]); 
  FFV1_0(w[111], w[61], w[81], pars->GC_11, amp[1111]); 
  FFV1_0(w[0], w[120], w[81], pars->GC_11, amp[1112]); 
  FFV1_0(w[112], w[61], w[81], pars->GC_11, amp[1113]); 
  FFV1_0(w[62], w[27], w[79], pars->GC_11, amp[1114]); 
  FFV1_0(w[62], w[80], w[8], pars->GC_2, amp[1115]); 
  FFV1_0(w[62], w[28], w[79], pars->GC_11, amp[1116]); 
  FFV2_5_0(w[62], w[80], w[14], pars->GC_51, pars->GC_58, amp[1117]); 
  FFV1_0(w[0], w[78], w[8], pars->GC_1, amp[1118]); 
  FFV1_0(w[0], w[121], w[70], pars->GC_11, amp[1119]); 
  FFV2_3_0(w[0], w[78], w[14], pars->GC_50, pars->GC_58, amp[1120]); 
  FFV1_0(w[0], w[122], w[70], pars->GC_11, amp[1121]); 
  FFV1_0(w[64], w[5], w[76], pars->GC_11, amp[1122]); 
  FFV1_0(w[62], w[13], w[76], pars->GC_11, amp[1123]); 
  FFV1_0(w[66], w[5], w[76], pars->GC_11, amp[1124]); 
  FFV1_0(w[62], w[17], w[76], pars->GC_11, amp[1125]); 
  FFV1_0(w[71], w[61], w[8], pars->GC_1, amp[1126]); 
  FFV1_0(w[107], w[61], w[70], pars->GC_11, amp[1127]); 
  FFV2_3_0(w[71], w[61], w[14], pars->GC_50, pars->GC_58, amp[1128]); 
  FFV1_0(w[108], w[61], w[70], pars->GC_11, amp[1129]); 
  FFV1_0(w[64], w[5], w[63], pars->GC_11, amp[1130]); 
  FFV1_0(w[62], w[13], w[63], pars->GC_11, amp[1131]); 
  FFV1_0(w[66], w[5], w[63], pars->GC_11, amp[1132]); 
  FFV1_0(w[62], w[17], w[63], pars->GC_11, amp[1133]); 
  FFV1_0(w[0], w[118], w[98], pars->GC_11, amp[1134]); 
  FFV1_0(w[111], w[61], w[98], pars->GC_11, amp[1135]); 
  FFV1_0(w[111], w[99], w[4], pars->GC_11, amp[1136]); 
  FFV1_0(w[100], w[118], w[4], pars->GC_11, amp[1137]); 
  FFV1_0(w[0], w[120], w[98], pars->GC_11, amp[1138]); 
  FFV1_0(w[112], w[61], w[98], pars->GC_11, amp[1139]); 
  FFV1_0(w[112], w[99], w[4], pars->GC_11, amp[1140]); 
  FFV1_0(w[100], w[120], w[4], pars->GC_11, amp[1141]); 
  FFV1_0(w[64], w[5], w[90], pars->GC_11, amp[1142]); 
  FFV1_0(w[62], w[13], w[90], pars->GC_11, amp[1143]); 
  FFV1_0(w[64], w[91], w[4], pars->GC_11, amp[1144]); 
  FFV1_0(w[92], w[13], w[4], pars->GC_11, amp[1145]); 
  FFV1_0(w[66], w[5], w[90], pars->GC_11, amp[1146]); 
  FFV1_0(w[62], w[17], w[90], pars->GC_11, amp[1147]); 
  FFV1_0(w[66], w[91], w[4], pars->GC_11, amp[1148]); 
  FFV1_0(w[92], w[17], w[4], pars->GC_11, amp[1149]); 
  FFV1_0(w[62], w[103], w[63], pars->GC_11, amp[1150]); 
  FFV1_0(w[117], w[5], w[63], pars->GC_11, amp[1151]); 
  FFV1_0(w[62], w[106], w[63], pars->GC_11, amp[1152]); 
  FFV1_0(w[119], w[5], w[63], pars->GC_11, amp[1153]); 
  FFV1_0(w[107], w[61], w[70], pars->GC_11, amp[1154]); 
  FFV1_0(w[71], w[61], w[8], pars->GC_1, amp[1155]); 
  FFV1_0(w[108], w[61], w[70], pars->GC_11, amp[1156]); 
  FFV2_3_0(w[71], w[61], w[14], pars->GC_50, pars->GC_58, amp[1157]); 
  FFV1_0(w[62], w[103], w[76], pars->GC_11, amp[1158]); 
  FFV1_0(w[117], w[5], w[76], pars->GC_11, amp[1159]); 
  FFV1_0(w[62], w[106], w[76], pars->GC_11, amp[1160]); 
  FFV1_0(w[119], w[5], w[76], pars->GC_11, amp[1161]); 
  FFV1_0(w[0], w[121], w[70], pars->GC_11, amp[1162]); 
  FFV1_0(w[0], w[78], w[8], pars->GC_1, amp[1163]); 
  FFV1_0(w[0], w[122], w[70], pars->GC_11, amp[1164]); 
  FFV2_3_0(w[0], w[78], w[14], pars->GC_50, pars->GC_58, amp[1165]); 
  FFV1_0(w[62], w[80], w[8], pars->GC_1, amp[1166]); 
  FFV1_0(w[62], w[109], w[79], pars->GC_11, amp[1167]); 
  FFV2_3_0(w[62], w[80], w[14], pars->GC_50, pars->GC_58, amp[1168]); 
  FFV1_0(w[62], w[110], w[79], pars->GC_11, amp[1169]); 
  FFV1_0(w[111], w[61], w[81], pars->GC_11, amp[1170]); 
  FFV1_0(w[0], w[118], w[81], pars->GC_11, amp[1171]); 
  FFV1_0(w[112], w[61], w[81], pars->GC_11, amp[1172]); 
  FFV1_0(w[0], w[120], w[81], pars->GC_11, amp[1173]); 
  FFV1_0(w[84], w[5], w[8], pars->GC_1, amp[1174]); 
  FFV1_0(w[123], w[5], w[79], pars->GC_11, amp[1175]); 
  FFV2_3_0(w[84], w[5], w[14], pars->GC_50, pars->GC_58, amp[1176]); 
  FFV1_0(w[124], w[5], w[79], pars->GC_11, amp[1177]); 
  FFV1_0(w[111], w[61], w[89], pars->GC_11, amp[1178]); 
  FFV1_0(w[0], w[118], w[89], pars->GC_11, amp[1179]); 
  FFV1_0(w[112], w[61], w[89], pars->GC_11, amp[1180]); 
  FFV1_0(w[0], w[120], w[89], pars->GC_11, amp[1181]); 
  FFV1_0(w[62], w[103], w[90], pars->GC_11, amp[1182]); 
  FFV1_0(w[117], w[5], w[90], pars->GC_11, amp[1183]); 
  FFV1_0(w[117], w[91], w[4], pars->GC_11, amp[1184]); 
  FFV1_0(w[92], w[103], w[4], pars->GC_11, amp[1185]); 
  FFV1_0(w[62], w[106], w[90], pars->GC_11, amp[1186]); 
  FFV1_0(w[119], w[5], w[90], pars->GC_11, amp[1187]); 
  FFV1_0(w[119], w[91], w[4], pars->GC_11, amp[1188]); 
  FFV1_0(w[92], w[106], w[4], pars->GC_11, amp[1189]); 
  FFV1_0(w[111], w[61], w[98], pars->GC_11, amp[1190]); 
  FFV1_0(w[0], w[118], w[98], pars->GC_11, amp[1191]); 
  FFV1_0(w[111], w[99], w[4], pars->GC_11, amp[1192]); 
  FFV1_0(w[100], w[118], w[4], pars->GC_11, amp[1193]); 
  FFV1_0(w[112], w[61], w[98], pars->GC_11, amp[1194]); 
  FFV1_0(w[0], w[120], w[98], pars->GC_11, amp[1195]); 
  FFV1_0(w[112], w[99], w[4], pars->GC_11, amp[1196]); 
  FFV1_0(w[100], w[120], w[4], pars->GC_11, amp[1197]); 
  FFV1_0(w[62], w[188], w[63], pars->GC_11, amp[1198]); 
  FFV1_0(w[194], w[5], w[63], pars->GC_11, amp[1199]); 
  FFV1_0(w[62], w[195], w[9], pars->GC_11, amp[1200]); 
  FFV1_0(w[194], w[61], w[9], pars->GC_11, amp[1201]); 
  FFV1_0(w[189], w[5], w[68], pars->GC_11, amp[1202]); 
  FFV2_3_0(w[69], w[5], w[173], pars->GC_50, pars->GC_58, amp[1203]); 
  FFV1_0(w[189], w[61], w[70], pars->GC_11, amp[1204]); 
  FFV2_3_0(w[71], w[61], w[173], pars->GC_50, pars->GC_58, amp[1205]); 
  FFV2_3_0(w[62], w[73], w[173], pars->GC_50, pars->GC_58, amp[1206]); 
  FFV1_0(w[62], w[196], w[35], pars->GC_11, amp[1207]); 
  FFV1_0(w[62], w[188], w[76], pars->GC_11, amp[1208]); 
  FFV1_0(w[194], w[5], w[76], pars->GC_11, amp[1209]); 
  FFV1_0(w[191], w[5], w[77], pars->GC_11, amp[1210]); 
  FFV1_0(w[0], w[188], w[77], pars->GC_11, amp[1211]); 
  FFV1_0(w[0], w[196], w[70], pars->GC_11, amp[1212]); 
  FFV2_3_0(w[0], w[78], w[173], pars->GC_50, pars->GC_58, amp[1213]); 
  FFV2_3_0(w[62], w[80], w[173], pars->GC_50, pars->GC_58, amp[1214]); 
  FFV1_0(w[62], w[190], w[79], pars->GC_11, amp[1215]); 
  FFV1_0(w[62], w[195], w[29], pars->GC_11, amp[1216]); 
  FFV1_0(w[194], w[61], w[29], pars->GC_11, amp[1217]); 
  FFV1_0(w[191], w[61], w[81], pars->GC_11, amp[1218]); 
  FFV1_0(w[0], w[195], w[81], pars->GC_11, amp[1219]); 
  FFV1_0(w[0], w[190], w[68], pars->GC_11, amp[1220]); 
  FFV2_3_0(w[0], w[82], w[173], pars->GC_50, pars->GC_58, amp[1221]); 
  FFV2_3_0(w[84], w[5], w[173], pars->GC_50, pars->GC_58, amp[1222]); 
  FFV1_0(w[197], w[5], w[79], pars->GC_11, amp[1223]); 
  FFV2_3_0(w[87], w[61], w[173], pars->GC_50, pars->GC_58, amp[1224]); 
  FFV1_0(w[197], w[61], w[35], pars->GC_11, amp[1225]); 
  FFV1_0(w[191], w[5], w[88], pars->GC_11, amp[1226]); 
  FFV1_0(w[0], w[188], w[88], pars->GC_11, amp[1227]); 
  FFV1_0(w[191], w[61], w[89], pars->GC_11, amp[1228]); 
  FFV1_0(w[0], w[195], w[89], pars->GC_11, amp[1229]); 
  FFV1_0(w[62], w[188], w[90], pars->GC_11, amp[1230]); 
  FFV1_0(w[194], w[5], w[90], pars->GC_11, amp[1231]); 
  FFV1_0(w[194], w[91], w[4], pars->GC_11, amp[1232]); 
  FFV1_0(w[92], w[188], w[4], pars->GC_11, amp[1233]); 
  FFV1_0(w[62], w[195], w[49], pars->GC_11, amp[1234]); 
  FFV1_0(w[194], w[61], w[49], pars->GC_11, amp[1235]); 
  FFV1_0(w[194], w[93], w[4], pars->GC_11, amp[1236]); 
  FFV1_0(w[94], w[195], w[4], pars->GC_11, amp[1237]); 
  FFV1_0(w[191], w[5], w[95], pars->GC_11, amp[1238]); 
  FFV1_0(w[0], w[188], w[95], pars->GC_11, amp[1239]); 
  FFV1_0(w[191], w[96], w[4], pars->GC_11, amp[1240]); 
  FFV1_0(w[97], w[188], w[4], pars->GC_11, amp[1241]); 
  FFV1_0(w[191], w[61], w[98], pars->GC_11, amp[1242]); 
  FFV1_0(w[0], w[195], w[98], pars->GC_11, amp[1243]); 
  FFV1_0(w[191], w[99], w[4], pars->GC_11, amp[1244]); 
  FFV1_0(w[100], w[195], w[4], pars->GC_11, amp[1245]); 
  FFV1_0(w[62], w[118], w[9], pars->GC_11, amp[1246]); 
  FFV1_0(w[117], w[61], w[9], pars->GC_11, amp[1247]); 
  FFV1_0(w[62], w[120], w[9], pars->GC_11, amp[1248]); 
  FFV1_0(w[119], w[61], w[9], pars->GC_11, amp[1249]); 
  FFV1_0(w[107], w[5], w[68], pars->GC_11, amp[1250]); 
  FFV1_0(w[69], w[5], w[8], pars->GC_1, amp[1251]); 
  FFV1_0(w[108], w[5], w[68], pars->GC_11, amp[1252]); 
  FFV2_3_0(w[69], w[5], w[14], pars->GC_50, pars->GC_58, amp[1253]); 
  FFV1_0(w[62], w[118], w[29], pars->GC_11, amp[1254]); 
  FFV1_0(w[117], w[61], w[29], pars->GC_11, amp[1255]); 
  FFV1_0(w[62], w[120], w[29], pars->GC_11, amp[1256]); 
  FFV1_0(w[119], w[61], w[29], pars->GC_11, amp[1257]); 
  FFV1_0(w[0], w[109], w[68], pars->GC_11, amp[1258]); 
  FFV1_0(w[0], w[82], w[8], pars->GC_1, amp[1259]); 
  FFV1_0(w[0], w[110], w[68], pars->GC_11, amp[1260]); 
  FFV2_3_0(w[0], w[82], w[14], pars->GC_50, pars->GC_58, amp[1261]); 
  FFV1_0(w[62], w[73], w[8], pars->GC_1, amp[1262]); 
  FFV1_0(w[62], w[121], w[35], pars->GC_11, amp[1263]); 
  FFV2_3_0(w[62], w[73], w[14], pars->GC_50, pars->GC_58, amp[1264]); 
  FFV1_0(w[62], w[122], w[35], pars->GC_11, amp[1265]); 
  FFV1_0(w[111], w[5], w[77], pars->GC_11, amp[1266]); 
  FFV1_0(w[0], w[103], w[77], pars->GC_11, amp[1267]); 
  FFV1_0(w[112], w[5], w[77], pars->GC_11, amp[1268]); 
  FFV1_0(w[0], w[106], w[77], pars->GC_11, amp[1269]); 
  FFV1_0(w[87], w[61], w[8], pars->GC_1, amp[1270]); 
  FFV1_0(w[123], w[61], w[35], pars->GC_11, amp[1271]); 
  FFV2_3_0(w[87], w[61], w[14], pars->GC_50, pars->GC_58, amp[1272]); 
  FFV1_0(w[124], w[61], w[35], pars->GC_11, amp[1273]); 
  FFV1_0(w[111], w[5], w[88], pars->GC_11, amp[1274]); 
  FFV1_0(w[0], w[103], w[88], pars->GC_11, amp[1275]); 
  FFV1_0(w[112], w[5], w[88], pars->GC_11, amp[1276]); 
  FFV1_0(w[0], w[106], w[88], pars->GC_11, amp[1277]); 
  FFV1_0(w[62], w[118], w[49], pars->GC_11, amp[1278]); 
  FFV1_0(w[117], w[61], w[49], pars->GC_11, amp[1279]); 
  FFV1_0(w[117], w[93], w[4], pars->GC_11, amp[1280]); 
  FFV1_0(w[94], w[118], w[4], pars->GC_11, amp[1281]); 
  FFV1_0(w[62], w[120], w[49], pars->GC_11, amp[1282]); 
  FFV1_0(w[119], w[61], w[49], pars->GC_11, amp[1283]); 
  FFV1_0(w[119], w[93], w[4], pars->GC_11, amp[1284]); 
  FFV1_0(w[94], w[120], w[4], pars->GC_11, amp[1285]); 
  FFV1_0(w[111], w[5], w[95], pars->GC_11, amp[1286]); 
  FFV1_0(w[0], w[103], w[95], pars->GC_11, amp[1287]); 
  FFV1_0(w[111], w[96], w[4], pars->GC_11, amp[1288]); 
  FFV1_0(w[97], w[103], w[4], pars->GC_11, amp[1289]); 
  FFV1_0(w[112], w[5], w[95], pars->GC_11, amp[1290]); 
  FFV1_0(w[0], w[106], w[95], pars->GC_11, amp[1291]); 
  FFV1_0(w[112], w[96], w[4], pars->GC_11, amp[1292]); 
  FFV1_0(w[97], w[106], w[4], pars->GC_11, amp[1293]); 
  FFV1_0(w[62], w[183], w[128], pars->GC_11, amp[1294]); 
  FFV1_0(w[182], w[61], w[128], pars->GC_11, amp[1295]); 
  FFV1_0(w[62], w[198], w[129], pars->GC_11, amp[1296]); 
  FFV1_0(w[182], w[125], w[129], pars->GC_11, amp[1297]); 
  FFV1_0(w[199], w[61], w[132], pars->GC_11, amp[1298]); 
  FFV2_5_0(w[134], w[61], w[173], pars->GC_51, pars->GC_58, amp[1299]); 
  FFV1_0(w[199], w[125], w[68], pars->GC_11, amp[1300]); 
  FFV2_5_0(w[136], w[125], w[173], pars->GC_51, pars->GC_58, amp[1301]); 
  FFV2_5_0(w[62], w[139], w[173], pars->GC_51, pars->GC_58, amp[1302]); 
  FFV1_0(w[62], w[200], w[138], pars->GC_11, amp[1303]); 
  FFV1_0(w[62], w[183], w[142], pars->GC_11, amp[1304]); 
  FFV1_0(w[182], w[61], w[142], pars->GC_11, amp[1305]); 
  FFV1_0(w[201], w[61], w[143], pars->GC_11, amp[1306]); 
  FFV1_0(w[126], w[183], w[143], pars->GC_11, amp[1307]); 
  FFV1_0(w[126], w[200], w[68], pars->GC_11, amp[1308]); 
  FFV2_5_0(w[126], w[146], w[173], pars->GC_51, pars->GC_58, amp[1309]); 
  FFV2_5_0(w[62], w[148], w[173], pars->GC_51, pars->GC_58, amp[1310]); 
  FFV1_0(w[62], w[184], w[147], pars->GC_11, amp[1311]); 
  FFV1_0(w[62], w[198], w[149], pars->GC_11, amp[1312]); 
  FFV1_0(w[182], w[125], w[149], pars->GC_11, amp[1313]); 
  FFV1_0(w[201], w[125], w[77], pars->GC_11, amp[1314]); 
  FFV1_0(w[126], w[198], w[77], pars->GC_11, amp[1315]); 
  FFV1_0(w[126], w[184], w[132], pars->GC_11, amp[1316]); 
  FFV2_5_0(w[126], w[150], w[173], pars->GC_51, pars->GC_58, amp[1317]); 
  FFV2_5_0(w[151], w[61], w[173], pars->GC_51, pars->GC_58, amp[1318]); 
  FFV1_0(w[185], w[61], w[147], pars->GC_11, amp[1319]); 
  FFV2_5_0(w[152], w[125], w[173], pars->GC_51, pars->GC_58, amp[1320]); 
  FFV1_0(w[185], w[125], w[138], pars->GC_11, amp[1321]); 
  FFV1_0(w[201], w[61], w[153], pars->GC_11, amp[1322]); 
  FFV1_0(w[126], w[183], w[153], pars->GC_11, amp[1323]); 
  FFV1_0(w[201], w[125], w[88], pars->GC_11, amp[1324]); 
  FFV1_0(w[126], w[198], w[88], pars->GC_11, amp[1325]); 
  FFV1_0(w[62], w[183], w[154], pars->GC_11, amp[1326]); 
  FFV1_0(w[182], w[61], w[154], pars->GC_11, amp[1327]); 
  FFV1_0(w[182], w[155], w[4], pars->GC_11, amp[1328]); 
  FFV1_0(w[156], w[183], w[4], pars->GC_11, amp[1329]); 
  FFV1_0(w[62], w[198], w[157], pars->GC_11, amp[1330]); 
  FFV1_0(w[182], w[125], w[157], pars->GC_11, amp[1331]); 
  FFV1_0(w[182], w[158], w[4], pars->GC_11, amp[1332]); 
  FFV1_0(w[159], w[198], w[4], pars->GC_11, amp[1333]); 
  FFV1_0(w[201], w[61], w[160], pars->GC_11, amp[1334]); 
  FFV1_0(w[126], w[183], w[160], pars->GC_11, amp[1335]); 
  FFV1_0(w[201], w[161], w[4], pars->GC_11, amp[1336]); 
  FFV1_0(w[162], w[183], w[4], pars->GC_11, amp[1337]); 
  FFV1_0(w[201], w[125], w[95], pars->GC_11, amp[1338]); 
  FFV1_0(w[126], w[198], w[95], pars->GC_11, amp[1339]); 
  FFV1_0(w[201], w[163], w[4], pars->GC_11, amp[1340]); 
  FFV1_0(w[164], w[198], w[4], pars->GC_11, amp[1341]); 
  FFV1_0(w[62], w[65], w[128], pars->GC_11, amp[1342]); 
  FFV1_0(w[64], w[61], w[128], pars->GC_11, amp[1343]); 
  FFV1_0(w[62], w[67], w[128], pars->GC_11, amp[1344]); 
  FFV1_0(w[66], w[61], w[128], pars->GC_11, amp[1345]); 
  FFV1_0(w[133], w[125], w[68], pars->GC_11, amp[1346]); 
  FFV1_0(w[136], w[125], w[8], pars->GC_2, amp[1347]); 
  FFV1_0(w[135], w[125], w[68], pars->GC_11, amp[1348]); 
  FFV2_5_0(w[136], w[125], w[14], pars->GC_51, pars->GC_58, amp[1349]); 
  FFV1_0(w[62], w[65], w[142], pars->GC_11, amp[1350]); 
  FFV1_0(w[64], w[61], w[142], pars->GC_11, amp[1351]); 
  FFV1_0(w[62], w[67], w[142], pars->GC_11, amp[1352]); 
  FFV1_0(w[66], w[61], w[142], pars->GC_11, amp[1353]); 
  FFV1_0(w[126], w[140], w[68], pars->GC_11, amp[1354]); 
  FFV1_0(w[126], w[146], w[8], pars->GC_2, amp[1355]); 
  FFV1_0(w[126], w[141], w[68], pars->GC_11, amp[1356]); 
  FFV2_5_0(w[126], w[146], w[14], pars->GC_51, pars->GC_58, amp[1357]); 
  FFV1_0(w[62], w[148], w[8], pars->GC_2, amp[1358]); 
  FFV1_0(w[62], w[74], w[147], pars->GC_11, amp[1359]); 
  FFV2_5_0(w[62], w[148], w[14], pars->GC_51, pars->GC_58, amp[1360]); 
  FFV1_0(w[62], w[75], w[147], pars->GC_11, amp[1361]); 
  FFV1_0(w[144], w[125], w[77], pars->GC_11, amp[1362]); 
  FFV1_0(w[126], w[130], w[77], pars->GC_11, amp[1363]); 
  FFV1_0(w[145], w[125], w[77], pars->GC_11, amp[1364]); 
  FFV1_0(w[126], w[131], w[77], pars->GC_11, amp[1365]); 
  FFV1_0(w[151], w[61], w[8], pars->GC_2, amp[1366]); 
  FFV1_0(w[85], w[61], w[147], pars->GC_11, amp[1367]); 
  FFV2_5_0(w[151], w[61], w[14], pars->GC_51, pars->GC_58, amp[1368]); 
  FFV1_0(w[86], w[61], w[147], pars->GC_11, amp[1369]); 
  FFV1_0(w[144], w[125], w[88], pars->GC_11, amp[1370]); 
  FFV1_0(w[126], w[130], w[88], pars->GC_11, amp[1371]); 
  FFV1_0(w[145], w[125], w[88], pars->GC_11, amp[1372]); 
  FFV1_0(w[126], w[131], w[88], pars->GC_11, amp[1373]); 
  FFV1_0(w[62], w[65], w[154], pars->GC_11, amp[1374]); 
  FFV1_0(w[64], w[61], w[154], pars->GC_11, amp[1375]); 
  FFV1_0(w[64], w[155], w[4], pars->GC_11, amp[1376]); 
  FFV1_0(w[156], w[65], w[4], pars->GC_11, amp[1377]); 
  FFV1_0(w[62], w[67], w[154], pars->GC_11, amp[1378]); 
  FFV1_0(w[66], w[61], w[154], pars->GC_11, amp[1379]); 
  FFV1_0(w[66], w[155], w[4], pars->GC_11, amp[1380]); 
  FFV1_0(w[156], w[67], w[4], pars->GC_11, amp[1381]); 
  FFV1_0(w[144], w[125], w[95], pars->GC_11, amp[1382]); 
  FFV1_0(w[126], w[130], w[95], pars->GC_11, amp[1383]); 
  FFV1_0(w[144], w[163], w[4], pars->GC_11, amp[1384]); 
  FFV1_0(w[164], w[130], w[4], pars->GC_11, amp[1385]); 
  FFV1_0(w[145], w[125], w[95], pars->GC_11, amp[1386]); 
  FFV1_0(w[126], w[131], w[95], pars->GC_11, amp[1387]); 
  FFV1_0(w[145], w[163], w[4], pars->GC_11, amp[1388]); 
  FFV1_0(w[164], w[131], w[4], pars->GC_11, amp[1389]); 
  FFV1_0(w[62], w[118], w[128], pars->GC_11, amp[1390]); 
  FFV1_0(w[117], w[61], w[128], pars->GC_11, amp[1391]); 
  FFV1_0(w[62], w[120], w[128], pars->GC_11, amp[1392]); 
  FFV1_0(w[119], w[61], w[128], pars->GC_11, amp[1393]); 
  FFV1_0(w[133], w[125], w[68], pars->GC_11, amp[1394]); 
  FFV1_0(w[136], w[125], w[8], pars->GC_2, amp[1395]); 
  FFV1_0(w[135], w[125], w[68], pars->GC_11, amp[1396]); 
  FFV2_5_0(w[136], w[125], w[14], pars->GC_51, pars->GC_58, amp[1397]); 
  FFV1_0(w[62], w[118], w[142], pars->GC_11, amp[1398]); 
  FFV1_0(w[117], w[61], w[142], pars->GC_11, amp[1399]); 
  FFV1_0(w[62], w[120], w[142], pars->GC_11, amp[1400]); 
  FFV1_0(w[119], w[61], w[142], pars->GC_11, amp[1401]); 
  FFV1_0(w[126], w[140], w[68], pars->GC_11, amp[1402]); 
  FFV1_0(w[126], w[146], w[8], pars->GC_2, amp[1403]); 
  FFV1_0(w[126], w[141], w[68], pars->GC_11, amp[1404]); 
  FFV2_5_0(w[126], w[146], w[14], pars->GC_51, pars->GC_58, amp[1405]); 
  FFV1_0(w[62], w[148], w[8], pars->GC_1, amp[1406]); 
  FFV1_0(w[62], w[121], w[147], pars->GC_11, amp[1407]); 
  FFV2_3_0(w[62], w[148], w[14], pars->GC_50, pars->GC_58, amp[1408]); 
  FFV1_0(w[62], w[122], w[147], pars->GC_11, amp[1409]); 
  FFV1_0(w[144], w[125], w[77], pars->GC_11, amp[1410]); 
  FFV1_0(w[126], w[130], w[77], pars->GC_11, amp[1411]); 
  FFV1_0(w[145], w[125], w[77], pars->GC_11, amp[1412]); 
  FFV1_0(w[126], w[131], w[77], pars->GC_11, amp[1413]); 
  FFV1_0(w[151], w[61], w[8], pars->GC_1, amp[1414]); 
  FFV1_0(w[123], w[61], w[147], pars->GC_11, amp[1415]); 
  FFV2_3_0(w[151], w[61], w[14], pars->GC_50, pars->GC_58, amp[1416]); 
  FFV1_0(w[124], w[61], w[147], pars->GC_11, amp[1417]); 
  FFV1_0(w[144], w[125], w[88], pars->GC_11, amp[1418]); 
  FFV1_0(w[126], w[130], w[88], pars->GC_11, amp[1419]); 
  FFV1_0(w[145], w[125], w[88], pars->GC_11, amp[1420]); 
  FFV1_0(w[126], w[131], w[88], pars->GC_11, amp[1421]); 
  FFV1_0(w[62], w[118], w[154], pars->GC_11, amp[1422]); 
  FFV1_0(w[117], w[61], w[154], pars->GC_11, amp[1423]); 
  FFV1_0(w[117], w[155], w[4], pars->GC_11, amp[1424]); 
  FFV1_0(w[156], w[118], w[4], pars->GC_11, amp[1425]); 
  FFV1_0(w[62], w[120], w[154], pars->GC_11, amp[1426]); 
  FFV1_0(w[119], w[61], w[154], pars->GC_11, amp[1427]); 
  FFV1_0(w[119], w[155], w[4], pars->GC_11, amp[1428]); 
  FFV1_0(w[156], w[120], w[4], pars->GC_11, amp[1429]); 
  FFV1_0(w[144], w[125], w[95], pars->GC_11, amp[1430]); 
  FFV1_0(w[126], w[130], w[95], pars->GC_11, amp[1431]); 
  FFV1_0(w[144], w[163], w[4], pars->GC_11, amp[1432]); 
  FFV1_0(w[164], w[130], w[4], pars->GC_11, amp[1433]); 
  FFV1_0(w[145], w[125], w[95], pars->GC_11, amp[1434]); 
  FFV1_0(w[126], w[131], w[95], pars->GC_11, amp[1435]); 
  FFV1_0(w[145], w[163], w[4], pars->GC_11, amp[1436]); 
  FFV1_0(w[164], w[131], w[4], pars->GC_11, amp[1437]); 
  FFV1_0(w[62], w[195], w[128], pars->GC_11, amp[1438]); 
  FFV1_0(w[194], w[61], w[128], pars->GC_11, amp[1439]); 
  FFV1_0(w[62], w[202], w[129], pars->GC_11, amp[1440]); 
  FFV1_0(w[194], w[125], w[129], pars->GC_11, amp[1441]); 
  FFV1_0(w[203], w[61], w[132], pars->GC_11, amp[1442]); 
  FFV2_3_0(w[134], w[61], w[173], pars->GC_50, pars->GC_58, amp[1443]); 
  FFV1_0(w[203], w[125], w[68], pars->GC_11, amp[1444]); 
  FFV2_3_0(w[136], w[125], w[173], pars->GC_50, pars->GC_58, amp[1445]); 
  FFV2_3_0(w[62], w[139], w[173], pars->GC_50, pars->GC_58, amp[1446]); 
  FFV1_0(w[62], w[204], w[138], pars->GC_11, amp[1447]); 
  FFV1_0(w[62], w[195], w[142], pars->GC_11, amp[1448]); 
  FFV1_0(w[194], w[61], w[142], pars->GC_11, amp[1449]); 
  FFV1_0(w[205], w[61], w[143], pars->GC_11, amp[1450]); 
  FFV1_0(w[126], w[195], w[143], pars->GC_11, amp[1451]); 
  FFV1_0(w[126], w[204], w[68], pars->GC_11, amp[1452]); 
  FFV2_3_0(w[126], w[146], w[173], pars->GC_50, pars->GC_58, amp[1453]); 
  FFV2_3_0(w[62], w[148], w[173], pars->GC_50, pars->GC_58, amp[1454]); 
  FFV1_0(w[62], w[196], w[147], pars->GC_11, amp[1455]); 
  FFV1_0(w[62], w[202], w[149], pars->GC_11, amp[1456]); 
  FFV1_0(w[194], w[125], w[149], pars->GC_11, amp[1457]); 
  FFV1_0(w[205], w[125], w[77], pars->GC_11, amp[1458]); 
  FFV1_0(w[126], w[202], w[77], pars->GC_11, amp[1459]); 
  FFV1_0(w[126], w[196], w[132], pars->GC_11, amp[1460]); 
  FFV2_3_0(w[126], w[150], w[173], pars->GC_50, pars->GC_58, amp[1461]); 
  FFV2_3_0(w[151], w[61], w[173], pars->GC_50, pars->GC_58, amp[1462]); 
  FFV1_0(w[197], w[61], w[147], pars->GC_11, amp[1463]); 
  FFV2_3_0(w[152], w[125], w[173], pars->GC_50, pars->GC_58, amp[1464]); 
  FFV1_0(w[197], w[125], w[138], pars->GC_11, amp[1465]); 
  FFV1_0(w[205], w[61], w[153], pars->GC_11, amp[1466]); 
  FFV1_0(w[126], w[195], w[153], pars->GC_11, amp[1467]); 
  FFV1_0(w[205], w[125], w[88], pars->GC_11, amp[1468]); 
  FFV1_0(w[126], w[202], w[88], pars->GC_11, amp[1469]); 
  FFV1_0(w[62], w[195], w[154], pars->GC_11, amp[1470]); 
  FFV1_0(w[194], w[61], w[154], pars->GC_11, amp[1471]); 
  FFV1_0(w[194], w[155], w[4], pars->GC_11, amp[1472]); 
  FFV1_0(w[156], w[195], w[4], pars->GC_11, amp[1473]); 
  FFV1_0(w[62], w[202], w[157], pars->GC_11, amp[1474]); 
  FFV1_0(w[194], w[125], w[157], pars->GC_11, amp[1475]); 
  FFV1_0(w[194], w[158], w[4], pars->GC_11, amp[1476]); 
  FFV1_0(w[159], w[202], w[4], pars->GC_11, amp[1477]); 
  FFV1_0(w[205], w[61], w[160], pars->GC_11, amp[1478]); 
  FFV1_0(w[126], w[195], w[160], pars->GC_11, amp[1479]); 
  FFV1_0(w[205], w[161], w[4], pars->GC_11, amp[1480]); 
  FFV1_0(w[162], w[195], w[4], pars->GC_11, amp[1481]); 
  FFV1_0(w[205], w[125], w[95], pars->GC_11, amp[1482]); 
  FFV1_0(w[126], w[202], w[95], pars->GC_11, amp[1483]); 
  FFV1_0(w[205], w[163], w[4], pars->GC_11, amp[1484]); 
  FFV1_0(w[164], w[202], w[4], pars->GC_11, amp[1485]); 
  FFV1_0(w[62], w[118], w[128], pars->GC_11, amp[1486]); 
  FFV1_0(w[117], w[61], w[128], pars->GC_11, amp[1487]); 
  FFV1_0(w[62], w[120], w[128], pars->GC_11, amp[1488]); 
  FFV1_0(w[119], w[61], w[128], pars->GC_11, amp[1489]); 
  FFV1_0(w[167], w[125], w[68], pars->GC_11, amp[1490]); 
  FFV1_0(w[136], w[125], w[8], pars->GC_1, amp[1491]); 
  FFV1_0(w[168], w[125], w[68], pars->GC_11, amp[1492]); 
  FFV2_3_0(w[136], w[125], w[14], pars->GC_50, pars->GC_58, amp[1493]); 
  FFV1_0(w[62], w[118], w[142], pars->GC_11, amp[1494]); 
  FFV1_0(w[117], w[61], w[142], pars->GC_11, amp[1495]); 
  FFV1_0(w[62], w[120], w[142], pars->GC_11, amp[1496]); 
  FFV1_0(w[119], w[61], w[142], pars->GC_11, amp[1497]); 
  FFV1_0(w[126], w[169], w[68], pars->GC_11, amp[1498]); 
  FFV1_0(w[126], w[146], w[8], pars->GC_1, amp[1499]); 
  FFV1_0(w[126], w[170], w[68], pars->GC_11, amp[1500]); 
  FFV2_3_0(w[126], w[146], w[14], pars->GC_50, pars->GC_58, amp[1501]); 
  FFV1_0(w[62], w[148], w[8], pars->GC_1, amp[1502]); 
  FFV1_0(w[62], w[121], w[147], pars->GC_11, amp[1503]); 
  FFV2_3_0(w[62], w[148], w[14], pars->GC_50, pars->GC_58, amp[1504]); 
  FFV1_0(w[62], w[122], w[147], pars->GC_11, amp[1505]); 
  FFV1_0(w[171], w[125], w[77], pars->GC_11, amp[1506]); 
  FFV1_0(w[126], w[165], w[77], pars->GC_11, amp[1507]); 
  FFV1_0(w[172], w[125], w[77], pars->GC_11, amp[1508]); 
  FFV1_0(w[126], w[166], w[77], pars->GC_11, amp[1509]); 
  FFV1_0(w[151], w[61], w[8], pars->GC_1, amp[1510]); 
  FFV1_0(w[123], w[61], w[147], pars->GC_11, amp[1511]); 
  FFV2_3_0(w[151], w[61], w[14], pars->GC_50, pars->GC_58, amp[1512]); 
  FFV1_0(w[124], w[61], w[147], pars->GC_11, amp[1513]); 
  FFV1_0(w[171], w[125], w[88], pars->GC_11, amp[1514]); 
  FFV1_0(w[126], w[165], w[88], pars->GC_11, amp[1515]); 
  FFV1_0(w[172], w[125], w[88], pars->GC_11, amp[1516]); 
  FFV1_0(w[126], w[166], w[88], pars->GC_11, amp[1517]); 
  FFV1_0(w[62], w[118], w[154], pars->GC_11, amp[1518]); 
  FFV1_0(w[117], w[61], w[154], pars->GC_11, amp[1519]); 
  FFV1_0(w[117], w[155], w[4], pars->GC_11, amp[1520]); 
  FFV1_0(w[156], w[118], w[4], pars->GC_11, amp[1521]); 
  FFV1_0(w[62], w[120], w[154], pars->GC_11, amp[1522]); 
  FFV1_0(w[119], w[61], w[154], pars->GC_11, amp[1523]); 
  FFV1_0(w[119], w[155], w[4], pars->GC_11, amp[1524]); 
  FFV1_0(w[156], w[120], w[4], pars->GC_11, amp[1525]); 
  FFV1_0(w[171], w[125], w[95], pars->GC_11, amp[1526]); 
  FFV1_0(w[126], w[165], w[95], pars->GC_11, amp[1527]); 
  FFV1_0(w[171], w[163], w[4], pars->GC_11, amp[1528]); 
  FFV1_0(w[164], w[165], w[4], pars->GC_11, amp[1529]); 
  FFV1_0(w[172], w[125], w[95], pars->GC_11, amp[1530]); 
  FFV1_0(w[126], w[166], w[95], pars->GC_11, amp[1531]); 
  FFV1_0(w[172], w[163], w[4], pars->GC_11, amp[1532]); 
  FFV1_0(w[164], w[166], w[4], pars->GC_11, amp[1533]); 
  FFV1_0(w[1], w[207], w[9], pars->GC_11, amp[1534]); 
  FFV1_0(w[208], w[6], w[9], pars->GC_11, amp[1535]); 
  FFV1_0(w[209], w[6], w[18], pars->GC_11, amp[1536]); 
  FFV2_0(w[20], w[6], w[206], pars->GC_100, amp[1537]); 
  FFV1_0(w[1], w[207], w[29], pars->GC_11, amp[1538]); 
  FFV1_0(w[208], w[6], w[29], pars->GC_11, amp[1539]); 
  FFV1_0(w[210], w[6], w[30], pars->GC_11, amp[1540]); 
  FFV1_0(w[0], w[207], w[30], pars->GC_11, amp[1541]); 
  FFV2_0(w[1], w[36], w[206], pars->GC_100, amp[1542]); 
  FFV1_0(w[1], w[211], w[35], pars->GC_11, amp[1543]); 
  FFV1_0(w[0], w[211], w[18], pars->GC_11, amp[1544]); 
  FFV2_0(w[0], w[41], w[206], pars->GC_100, amp[1545]); 
  FFV2_0(w[43], w[6], w[206], pars->GC_100, amp[1546]); 
  FFV1_0(w[212], w[6], w[35], pars->GC_11, amp[1547]); 
  FFV1_0(w[210], w[6], w[47], pars->GC_11, amp[1548]); 
  FFV1_0(w[0], w[207], w[47], pars->GC_11, amp[1549]); 
  FFV1_0(w[1], w[207], w[49], pars->GC_11, amp[1550]); 
  FFV1_0(w[208], w[6], w[49], pars->GC_11, amp[1551]); 
  FFV1_0(w[208], w[50], w[4], pars->GC_11, amp[1552]); 
  FFV1_0(w[51], w[207], w[4], pars->GC_11, amp[1553]); 
  FFV1_0(w[210], w[6], w[55], pars->GC_11, amp[1554]); 
  FFV1_0(w[0], w[207], w[55], pars->GC_11, amp[1555]); 
  FFV1_0(w[210], w[56], w[4], pars->GC_11, amp[1556]); 
  FFV1_0(w[57], w[207], w[4], pars->GC_11, amp[1557]); 
  FFV1_0(w[1], w[174], w[9], pars->GC_11, amp[1558]); 
  FFV1_0(w[175], w[6], w[9], pars->GC_11, amp[1559]); 
  FFV1_0(w[177], w[5], w[22], pars->GC_11, amp[1560]); 
  FFV2_5_0(w[23], w[5], w[173], pars->GC_51, pars->GC_58, amp[1561]); 
  FFV1_0(w[1], w[174], w[29], pars->GC_11, amp[1562]); 
  FFV1_0(w[175], w[6], w[29], pars->GC_11, amp[1563]); 
  FFV1_0(w[0], w[178], w[22], pars->GC_11, amp[1564]); 
  FFV2_5_0(w[0], w[33], w[173], pars->GC_51, pars->GC_58, amp[1565]); 
  FFV2_5_0(w[1], w[36], w[173], pars->GC_51, pars->GC_58, amp[1566]); 
  FFV1_0(w[1], w[180], w[35], pars->GC_11, amp[1567]); 
  FFV1_0(w[179], w[5], w[40], pars->GC_11, amp[1568]); 
  FFV1_0(w[0], w[176], w[40], pars->GC_11, amp[1569]); 
  FFV2_5_0(w[43], w[6], w[173], pars->GC_51, pars->GC_58, amp[1570]); 
  FFV1_0(w[181], w[6], w[35], pars->GC_11, amp[1571]); 
  FFV1_0(w[179], w[5], w[48], pars->GC_11, amp[1572]); 
  FFV1_0(w[0], w[176], w[48], pars->GC_11, amp[1573]); 
  FFV1_0(w[1], w[174], w[49], pars->GC_11, amp[1574]); 
  FFV1_0(w[175], w[6], w[49], pars->GC_11, amp[1575]); 
  FFV1_0(w[175], w[50], w[4], pars->GC_11, amp[1576]); 
  FFV1_0(w[51], w[174], w[4], pars->GC_11, amp[1577]); 
  FFV1_0(w[179], w[5], w[58], pars->GC_11, amp[1578]); 
  FFV1_0(w[0], w[176], w[58], pars->GC_11, amp[1579]); 
  FFV1_0(w[179], w[59], w[4], pars->GC_11, amp[1580]); 
  FFV1_0(w[60], w[176], w[4], pars->GC_11, amp[1581]); 
  FFV1_0(w[209], w[6], w[18], pars->GC_11, amp[1582]); 
  FFV2_0(w[20], w[6], w[206], pars->GC_100, amp[1583]); 
  FFV1_0(w[209], w[5], w[22], pars->GC_11, amp[1584]); 
  FFV2_0(w[23], w[5], w[206], pars->GC_100, amp[1585]); 
  FFV1_0(w[210], w[6], w[30], pars->GC_11, amp[1586]); 
  FFV1_0(w[0], w[207], w[30], pars->GC_11, amp[1587]); 
  FFV1_0(w[0], w[213], w[22], pars->GC_11, amp[1588]); 
  FFV2_0(w[0], w[33], w[206], pars->GC_100, amp[1589]); 
  FFV1_0(w[210], w[5], w[40], pars->GC_11, amp[1590]); 
  FFV1_0(w[0], w[214], w[40], pars->GC_11, amp[1591]); 
  FFV1_0(w[0], w[211], w[18], pars->GC_11, amp[1592]); 
  FFV2_0(w[0], w[41], w[206], pars->GC_100, amp[1593]); 
  FFV1_0(w[210], w[6], w[47], pars->GC_11, amp[1594]); 
  FFV1_0(w[0], w[207], w[47], pars->GC_11, amp[1595]); 
  FFV1_0(w[210], w[5], w[48], pars->GC_11, amp[1596]); 
  FFV1_0(w[0], w[214], w[48], pars->GC_11, amp[1597]); 
  FFV1_0(w[210], w[6], w[55], pars->GC_11, amp[1598]); 
  FFV1_0(w[0], w[207], w[55], pars->GC_11, amp[1599]); 
  FFV1_0(w[210], w[56], w[4], pars->GC_11, amp[1600]); 
  FFV1_0(w[57], w[207], w[4], pars->GC_11, amp[1601]); 
  FFV1_0(w[210], w[5], w[58], pars->GC_11, amp[1602]); 
  FFV1_0(w[0], w[214], w[58], pars->GC_11, amp[1603]); 
  FFV1_0(w[210], w[59], w[4], pars->GC_11, amp[1604]); 
  FFV1_0(w[60], w[214], w[4], pars->GC_11, amp[1605]); 
  FFV1_0(w[1], w[207], w[9], pars->GC_11, amp[1606]); 
  FFV1_0(w[208], w[6], w[9], pars->GC_11, amp[1607]); 
  FFV1_0(w[1], w[214], w[12], pars->GC_11, amp[1608]); 
  FFV1_0(w[208], w[5], w[12], pars->GC_11, amp[1609]); 
  FFV2_0(w[1], w[26], w[206], pars->GC_100, amp[1610]); 
  FFV1_0(w[1], w[213], w[25], pars->GC_11, amp[1611]); 
  FFV1_0(w[1], w[207], w[29], pars->GC_11, amp[1612]); 
  FFV1_0(w[208], w[6], w[29], pars->GC_11, amp[1613]); 
  FFV2_0(w[1], w[36], w[206], pars->GC_100, amp[1614]); 
  FFV1_0(w[1], w[211], w[35], pars->GC_11, amp[1615]); 
  FFV1_0(w[1], w[214], w[39], pars->GC_11, amp[1616]); 
  FFV1_0(w[208], w[5], w[39], pars->GC_11, amp[1617]); 
  FFV2_0(w[43], w[6], w[206], pars->GC_100, amp[1618]); 
  FFV1_0(w[212], w[6], w[35], pars->GC_11, amp[1619]); 
  FFV2_0(w[46], w[5], w[206], pars->GC_100, amp[1620]); 
  FFV1_0(w[212], w[5], w[25], pars->GC_11, amp[1621]); 
  FFV1_0(w[1], w[207], w[49], pars->GC_11, amp[1622]); 
  FFV1_0(w[208], w[6], w[49], pars->GC_11, amp[1623]); 
  FFV1_0(w[208], w[50], w[4], pars->GC_11, amp[1624]); 
  FFV1_0(w[51], w[207], w[4], pars->GC_11, amp[1625]); 
  FFV1_0(w[1], w[214], w[52], pars->GC_11, amp[1626]); 
  FFV1_0(w[208], w[5], w[52], pars->GC_11, amp[1627]); 
  FFV1_0(w[208], w[53], w[4], pars->GC_11, amp[1628]); 
  FFV1_0(w[54], w[214], w[4], pars->GC_11, amp[1629]); 
  FFV1_0(w[1], w[186], w[9], pars->GC_11, amp[1630]); 
  FFV1_0(w[187], w[6], w[9], pars->GC_11, amp[1631]); 
  FFV1_0(w[177], w[5], w[22], pars->GC_11, amp[1632]); 
  FFV2_5_0(w[23], w[5], w[173], pars->GC_51, pars->GC_58, amp[1633]); 
  FFV1_0(w[1], w[186], w[29], pars->GC_11, amp[1634]); 
  FFV1_0(w[187], w[6], w[29], pars->GC_11, amp[1635]); 
  FFV1_0(w[0], w[178], w[22], pars->GC_11, amp[1636]); 
  FFV2_5_0(w[0], w[33], w[173], pars->GC_51, pars->GC_58, amp[1637]); 
  FFV2_3_0(w[1], w[36], w[173], pars->GC_50, pars->GC_58, amp[1638]); 
  FFV1_0(w[1], w[192], w[35], pars->GC_11, amp[1639]); 
  FFV1_0(w[179], w[5], w[40], pars->GC_11, amp[1640]); 
  FFV1_0(w[0], w[176], w[40], pars->GC_11, amp[1641]); 
  FFV2_3_0(w[43], w[6], w[173], pars->GC_50, pars->GC_58, amp[1642]); 
  FFV1_0(w[193], w[6], w[35], pars->GC_11, amp[1643]); 
  FFV1_0(w[179], w[5], w[48], pars->GC_11, amp[1644]); 
  FFV1_0(w[0], w[176], w[48], pars->GC_11, amp[1645]); 
  FFV1_0(w[1], w[186], w[49], pars->GC_11, amp[1646]); 
  FFV1_0(w[187], w[6], w[49], pars->GC_11, amp[1647]); 
  FFV1_0(w[187], w[50], w[4], pars->GC_11, amp[1648]); 
  FFV1_0(w[51], w[186], w[4], pars->GC_11, amp[1649]); 
  FFV1_0(w[179], w[5], w[58], pars->GC_11, amp[1650]); 
  FFV1_0(w[0], w[176], w[58], pars->GC_11, amp[1651]); 
  FFV1_0(w[179], w[59], w[4], pars->GC_11, amp[1652]); 
  FFV1_0(w[60], w[176], w[4], pars->GC_11, amp[1653]); 
  FFV1_0(w[62], w[214], w[63], pars->GC_11, amp[1654]); 
  FFV1_0(w[215], w[5], w[63], pars->GC_11, amp[1655]); 
  FFV1_0(w[209], w[5], w[68], pars->GC_11, amp[1656]); 
  FFV2_0(w[69], w[5], w[206], pars->GC_100, amp[1657]); 
  FFV1_0(w[62], w[214], w[76], pars->GC_11, amp[1658]); 
  FFV1_0(w[215], w[5], w[76], pars->GC_11, amp[1659]); 
  FFV1_0(w[210], w[5], w[77], pars->GC_11, amp[1660]); 
  FFV1_0(w[0], w[214], w[77], pars->GC_11, amp[1661]); 
  FFV2_0(w[62], w[80], w[206], pars->GC_100, amp[1662]); 
  FFV1_0(w[62], w[213], w[79], pars->GC_11, amp[1663]); 
  FFV1_0(w[0], w[213], w[68], pars->GC_11, amp[1664]); 
  FFV2_0(w[0], w[82], w[206], pars->GC_100, amp[1665]); 
  FFV2_0(w[84], w[5], w[206], pars->GC_100, amp[1666]); 
  FFV1_0(w[216], w[5], w[79], pars->GC_11, amp[1667]); 
  FFV1_0(w[210], w[5], w[88], pars->GC_11, amp[1668]); 
  FFV1_0(w[0], w[214], w[88], pars->GC_11, amp[1669]); 
  FFV1_0(w[62], w[214], w[90], pars->GC_11, amp[1670]); 
  FFV1_0(w[215], w[5], w[90], pars->GC_11, amp[1671]); 
  FFV1_0(w[215], w[91], w[4], pars->GC_11, amp[1672]); 
  FFV1_0(w[92], w[214], w[4], pars->GC_11, amp[1673]); 
  FFV1_0(w[210], w[5], w[95], pars->GC_11, amp[1674]); 
  FFV1_0(w[0], w[214], w[95], pars->GC_11, amp[1675]); 
  FFV1_0(w[210], w[96], w[4], pars->GC_11, amp[1676]); 
  FFV1_0(w[97], w[214], w[4], pars->GC_11, amp[1677]); 
  FFV1_0(w[62], w[214], w[63], pars->GC_11, amp[1678]); 
  FFV1_0(w[215], w[5], w[63], pars->GC_11, amp[1679]); 
  FFV1_0(w[62], w[217], w[9], pars->GC_11, amp[1680]); 
  FFV1_0(w[215], w[61], w[9], pars->GC_11, amp[1681]); 
  FFV2_0(w[62], w[73], w[206], pars->GC_100, amp[1682]); 
  FFV1_0(w[62], w[218], w[35], pars->GC_11, amp[1683]); 
  FFV1_0(w[62], w[214], w[76], pars->GC_11, amp[1684]); 
  FFV1_0(w[215], w[5], w[76], pars->GC_11, amp[1685]); 
  FFV2_0(w[62], w[80], w[206], pars->GC_100, amp[1686]); 
  FFV1_0(w[62], w[213], w[79], pars->GC_11, amp[1687]); 
  FFV1_0(w[62], w[217], w[29], pars->GC_11, amp[1688]); 
  FFV1_0(w[215], w[61], w[29], pars->GC_11, amp[1689]); 
  FFV2_0(w[84], w[5], w[206], pars->GC_100, amp[1690]); 
  FFV1_0(w[216], w[5], w[79], pars->GC_11, amp[1691]); 
  FFV2_0(w[87], w[61], w[206], pars->GC_100, amp[1692]); 
  FFV1_0(w[216], w[61], w[35], pars->GC_11, amp[1693]); 
  FFV1_0(w[62], w[214], w[90], pars->GC_11, amp[1694]); 
  FFV1_0(w[215], w[5], w[90], pars->GC_11, amp[1695]); 
  FFV1_0(w[215], w[91], w[4], pars->GC_11, amp[1696]); 
  FFV1_0(w[92], w[214], w[4], pars->GC_11, amp[1697]); 
  FFV1_0(w[62], w[217], w[49], pars->GC_11, amp[1698]); 
  FFV1_0(w[215], w[61], w[49], pars->GC_11, amp[1699]); 
  FFV1_0(w[215], w[93], w[4], pars->GC_11, amp[1700]); 
  FFV1_0(w[94], w[217], w[4], pars->GC_11, amp[1701]); 
  FFV1_0(w[62], w[176], w[63], pars->GC_11, amp[1702]); 
  FFV1_0(w[182], w[5], w[63], pars->GC_11, amp[1703]); 
  FFV1_0(w[177], w[61], w[70], pars->GC_11, amp[1704]); 
  FFV2_5_0(w[71], w[61], w[173], pars->GC_51, pars->GC_58, amp[1705]); 
  FFV1_0(w[62], w[176], w[76], pars->GC_11, amp[1706]); 
  FFV1_0(w[182], w[5], w[76], pars->GC_11, amp[1707]); 
  FFV1_0(w[0], w[184], w[70], pars->GC_11, amp[1708]); 
  FFV2_5_0(w[0], w[78], w[173], pars->GC_51, pars->GC_58, amp[1709]); 
  FFV2_5_0(w[62], w[80], w[173], pars->GC_51, pars->GC_58, amp[1710]); 
  FFV1_0(w[62], w[178], w[79], pars->GC_11, amp[1711]); 
  FFV1_0(w[179], w[61], w[81], pars->GC_11, amp[1712]); 
  FFV1_0(w[0], w[183], w[81], pars->GC_11, amp[1713]); 
  FFV2_5_0(w[84], w[5], w[173], pars->GC_51, pars->GC_58, amp[1714]); 
  FFV1_0(w[185], w[5], w[79], pars->GC_11, amp[1715]); 
  FFV1_0(w[179], w[61], w[89], pars->GC_11, amp[1716]); 
  FFV1_0(w[0], w[183], w[89], pars->GC_11, amp[1717]); 
  FFV1_0(w[62], w[176], w[90], pars->GC_11, amp[1718]); 
  FFV1_0(w[182], w[5], w[90], pars->GC_11, amp[1719]); 
  FFV1_0(w[182], w[91], w[4], pars->GC_11, amp[1720]); 
  FFV1_0(w[92], w[176], w[4], pars->GC_11, amp[1721]); 
  FFV1_0(w[179], w[61], w[98], pars->GC_11, amp[1722]); 
  FFV1_0(w[0], w[183], w[98], pars->GC_11, amp[1723]); 
  FFV1_0(w[179], w[99], w[4], pars->GC_11, amp[1724]); 
  FFV1_0(w[100], w[183], w[4], pars->GC_11, amp[1725]); 
  FFV1_0(w[62], w[188], w[63], pars->GC_11, amp[1726]); 
  FFV1_0(w[194], w[5], w[63], pars->GC_11, amp[1727]); 
  FFV1_0(w[177], w[61], w[70], pars->GC_11, amp[1728]); 
  FFV2_5_0(w[71], w[61], w[173], pars->GC_51, pars->GC_58, amp[1729]); 
  FFV1_0(w[62], w[188], w[76], pars->GC_11, amp[1730]); 
  FFV1_0(w[194], w[5], w[76], pars->GC_11, amp[1731]); 
  FFV1_0(w[0], w[184], w[70], pars->GC_11, amp[1732]); 
  FFV2_5_0(w[0], w[78], w[173], pars->GC_51, pars->GC_58, amp[1733]); 
  FFV2_3_0(w[62], w[80], w[173], pars->GC_50, pars->GC_58, amp[1734]); 
  FFV1_0(w[62], w[190], w[79], pars->GC_11, amp[1735]); 
  FFV1_0(w[179], w[61], w[81], pars->GC_11, amp[1736]); 
  FFV1_0(w[0], w[183], w[81], pars->GC_11, amp[1737]); 
  FFV2_3_0(w[84], w[5], w[173], pars->GC_50, pars->GC_58, amp[1738]); 
  FFV1_0(w[197], w[5], w[79], pars->GC_11, amp[1739]); 
  FFV1_0(w[179], w[61], w[89], pars->GC_11, amp[1740]); 
  FFV1_0(w[0], w[183], w[89], pars->GC_11, amp[1741]); 
  FFV1_0(w[62], w[188], w[90], pars->GC_11, amp[1742]); 
  FFV1_0(w[194], w[5], w[90], pars->GC_11, amp[1743]); 
  FFV1_0(w[194], w[91], w[4], pars->GC_11, amp[1744]); 
  FFV1_0(w[92], w[188], w[4], pars->GC_11, amp[1745]); 
  FFV1_0(w[179], w[61], w[98], pars->GC_11, amp[1746]); 
  FFV1_0(w[0], w[183], w[98], pars->GC_11, amp[1747]); 
  FFV1_0(w[179], w[99], w[4], pars->GC_11, amp[1748]); 
  FFV1_0(w[100], w[183], w[4], pars->GC_11, amp[1749]); 
  FFV1_0(w[62], w[183], w[9], pars->GC_11, amp[1750]); 
  FFV1_0(w[182], w[61], w[9], pars->GC_11, amp[1751]); 
  FFV1_0(w[177], w[5], w[68], pars->GC_11, amp[1752]); 
  FFV2_5_0(w[69], w[5], w[173], pars->GC_51, pars->GC_58, amp[1753]); 
  FFV1_0(w[62], w[183], w[29], pars->GC_11, amp[1754]); 
  FFV1_0(w[182], w[61], w[29], pars->GC_11, amp[1755]); 
  FFV1_0(w[0], w[178], w[68], pars->GC_11, amp[1756]); 
  FFV2_5_0(w[0], w[82], w[173], pars->GC_51, pars->GC_58, amp[1757]); 
  FFV2_5_0(w[62], w[73], w[173], pars->GC_51, pars->GC_58, amp[1758]); 
  FFV1_0(w[62], w[184], w[35], pars->GC_11, amp[1759]); 
  FFV1_0(w[179], w[5], w[77], pars->GC_11, amp[1760]); 
  FFV1_0(w[0], w[176], w[77], pars->GC_11, amp[1761]); 
  FFV2_5_0(w[87], w[61], w[173], pars->GC_51, pars->GC_58, amp[1762]); 
  FFV1_0(w[185], w[61], w[35], pars->GC_11, amp[1763]); 
  FFV1_0(w[179], w[5], w[88], pars->GC_11, amp[1764]); 
  FFV1_0(w[0], w[176], w[88], pars->GC_11, amp[1765]); 
  FFV1_0(w[62], w[183], w[49], pars->GC_11, amp[1766]); 
  FFV1_0(w[182], w[61], w[49], pars->GC_11, amp[1767]); 
  FFV1_0(w[182], w[93], w[4], pars->GC_11, amp[1768]); 
  FFV1_0(w[94], w[183], w[4], pars->GC_11, amp[1769]); 
  FFV1_0(w[179], w[5], w[95], pars->GC_11, amp[1770]); 
  FFV1_0(w[0], w[176], w[95], pars->GC_11, amp[1771]); 
  FFV1_0(w[179], w[96], w[4], pars->GC_11, amp[1772]); 
  FFV1_0(w[97], w[176], w[4], pars->GC_11, amp[1773]); 
  FFV1_0(w[62], w[217], w[9], pars->GC_11, amp[1774]); 
  FFV1_0(w[215], w[61], w[9], pars->GC_11, amp[1775]); 
  FFV1_0(w[209], w[61], w[70], pars->GC_11, amp[1776]); 
  FFV2_0(w[71], w[61], w[206], pars->GC_100, amp[1777]); 
  FFV1_0(w[62], w[217], w[29], pars->GC_11, amp[1778]); 
  FFV1_0(w[215], w[61], w[29], pars->GC_11, amp[1779]); 
  FFV1_0(w[210], w[61], w[81], pars->GC_11, amp[1780]); 
  FFV1_0(w[0], w[217], w[81], pars->GC_11, amp[1781]); 
  FFV2_0(w[62], w[73], w[206], pars->GC_100, amp[1782]); 
  FFV1_0(w[62], w[218], w[35], pars->GC_11, amp[1783]); 
  FFV1_0(w[0], w[218], w[70], pars->GC_11, amp[1784]); 
  FFV2_0(w[0], w[78], w[206], pars->GC_100, amp[1785]); 
  FFV2_0(w[87], w[61], w[206], pars->GC_100, amp[1786]); 
  FFV1_0(w[216], w[61], w[35], pars->GC_11, amp[1787]); 
  FFV1_0(w[210], w[61], w[89], pars->GC_11, amp[1788]); 
  FFV1_0(w[0], w[217], w[89], pars->GC_11, amp[1789]); 
  FFV1_0(w[62], w[217], w[49], pars->GC_11, amp[1790]); 
  FFV1_0(w[215], w[61], w[49], pars->GC_11, amp[1791]); 
  FFV1_0(w[215], w[93], w[4], pars->GC_11, amp[1792]); 
  FFV1_0(w[94], w[217], w[4], pars->GC_11, amp[1793]); 
  FFV1_0(w[210], w[61], w[98], pars->GC_11, amp[1794]); 
  FFV1_0(w[0], w[217], w[98], pars->GC_11, amp[1795]); 
  FFV1_0(w[210], w[99], w[4], pars->GC_11, amp[1796]); 
  FFV1_0(w[100], w[217], w[4], pars->GC_11, amp[1797]); 
  FFV1_0(w[209], w[5], w[68], pars->GC_11, amp[1798]); 
  FFV2_0(w[69], w[5], w[206], pars->GC_100, amp[1799]); 
  FFV1_0(w[209], w[61], w[70], pars->GC_11, amp[1800]); 
  FFV2_0(w[71], w[61], w[206], pars->GC_100, amp[1801]); 
  FFV1_0(w[210], w[5], w[77], pars->GC_11, amp[1802]); 
  FFV1_0(w[0], w[214], w[77], pars->GC_11, amp[1803]); 
  FFV1_0(w[0], w[218], w[70], pars->GC_11, amp[1804]); 
  FFV2_0(w[0], w[78], w[206], pars->GC_100, amp[1805]); 
  FFV1_0(w[210], w[61], w[81], pars->GC_11, amp[1806]); 
  FFV1_0(w[0], w[217], w[81], pars->GC_11, amp[1807]); 
  FFV1_0(w[0], w[213], w[68], pars->GC_11, amp[1808]); 
  FFV2_0(w[0], w[82], w[206], pars->GC_100, amp[1809]); 
  FFV1_0(w[210], w[5], w[88], pars->GC_11, amp[1810]); 
  FFV1_0(w[0], w[214], w[88], pars->GC_11, amp[1811]); 
  FFV1_0(w[210], w[61], w[89], pars->GC_11, amp[1812]); 
  FFV1_0(w[0], w[217], w[89], pars->GC_11, amp[1813]); 
  FFV1_0(w[210], w[5], w[95], pars->GC_11, amp[1814]); 
  FFV1_0(w[0], w[214], w[95], pars->GC_11, amp[1815]); 
  FFV1_0(w[210], w[96], w[4], pars->GC_11, amp[1816]); 
  FFV1_0(w[97], w[214], w[4], pars->GC_11, amp[1817]); 
  FFV1_0(w[210], w[61], w[98], pars->GC_11, amp[1818]); 
  FFV1_0(w[0], w[217], w[98], pars->GC_11, amp[1819]); 
  FFV1_0(w[210], w[99], w[4], pars->GC_11, amp[1820]); 
  FFV1_0(w[100], w[217], w[4], pars->GC_11, amp[1821]); 
  FFV1_0(w[62], w[195], w[9], pars->GC_11, amp[1822]); 
  FFV1_0(w[194], w[61], w[9], pars->GC_11, amp[1823]); 
  FFV1_0(w[177], w[5], w[68], pars->GC_11, amp[1824]); 
  FFV2_5_0(w[69], w[5], w[173], pars->GC_51, pars->GC_58, amp[1825]); 
  FFV1_0(w[62], w[195], w[29], pars->GC_11, amp[1826]); 
  FFV1_0(w[194], w[61], w[29], pars->GC_11, amp[1827]); 
  FFV1_0(w[0], w[178], w[68], pars->GC_11, amp[1828]); 
  FFV2_5_0(w[0], w[82], w[173], pars->GC_51, pars->GC_58, amp[1829]); 
  FFV2_3_0(w[62], w[73], w[173], pars->GC_50, pars->GC_58, amp[1830]); 
  FFV1_0(w[62], w[196], w[35], pars->GC_11, amp[1831]); 
  FFV1_0(w[179], w[5], w[77], pars->GC_11, amp[1832]); 
  FFV1_0(w[0], w[176], w[77], pars->GC_11, amp[1833]); 
  FFV2_3_0(w[87], w[61], w[173], pars->GC_50, pars->GC_58, amp[1834]); 
  FFV1_0(w[197], w[61], w[35], pars->GC_11, amp[1835]); 
  FFV1_0(w[179], w[5], w[88], pars->GC_11, amp[1836]); 
  FFV1_0(w[0], w[176], w[88], pars->GC_11, amp[1837]); 
  FFV1_0(w[62], w[195], w[49], pars->GC_11, amp[1838]); 
  FFV1_0(w[194], w[61], w[49], pars->GC_11, amp[1839]); 
  FFV1_0(w[194], w[93], w[4], pars->GC_11, amp[1840]); 
  FFV1_0(w[94], w[195], w[4], pars->GC_11, amp[1841]); 
  FFV1_0(w[179], w[5], w[95], pars->GC_11, amp[1842]); 
  FFV1_0(w[0], w[176], w[95], pars->GC_11, amp[1843]); 
  FFV1_0(w[179], w[96], w[4], pars->GC_11, amp[1844]); 
  FFV1_0(w[97], w[176], w[4], pars->GC_11, amp[1845]); 
  FFV1_0(w[1], w[214], w[12], pars->GC_11, amp[1846]); 
  FFV1_0(w[208], w[5], w[12], pars->GC_11, amp[1847]); 
  FFV1_0(w[209], w[5], w[22], pars->GC_11, amp[1848]); 
  FFV2_0(w[23], w[5], w[206], pars->GC_100, amp[1849]); 
  FFV2_0(w[1], w[26], w[206], pars->GC_100, amp[1850]); 
  FFV1_0(w[1], w[213], w[25], pars->GC_11, amp[1851]); 
  FFV1_0(w[0], w[213], w[22], pars->GC_11, amp[1852]); 
  FFV2_0(w[0], w[33], w[206], pars->GC_100, amp[1853]); 
  FFV1_0(w[1], w[214], w[39], pars->GC_11, amp[1854]); 
  FFV1_0(w[208], w[5], w[39], pars->GC_11, amp[1855]); 
  FFV1_0(w[210], w[5], w[40], pars->GC_11, amp[1856]); 
  FFV1_0(w[0], w[214], w[40], pars->GC_11, amp[1857]); 
  FFV2_0(w[46], w[5], w[206], pars->GC_100, amp[1858]); 
  FFV1_0(w[212], w[5], w[25], pars->GC_11, amp[1859]); 
  FFV1_0(w[210], w[5], w[48], pars->GC_11, amp[1860]); 
  FFV1_0(w[0], w[214], w[48], pars->GC_11, amp[1861]); 
  FFV1_0(w[1], w[214], w[52], pars->GC_11, amp[1862]); 
  FFV1_0(w[208], w[5], w[52], pars->GC_11, amp[1863]); 
  FFV1_0(w[208], w[53], w[4], pars->GC_11, amp[1864]); 
  FFV1_0(w[54], w[214], w[4], pars->GC_11, amp[1865]); 
  FFV1_0(w[210], w[5], w[58], pars->GC_11, amp[1866]); 
  FFV1_0(w[0], w[214], w[58], pars->GC_11, amp[1867]); 
  FFV1_0(w[210], w[59], w[4], pars->GC_11, amp[1868]); 
  FFV1_0(w[60], w[214], w[4], pars->GC_11, amp[1869]); 
  FFV1_0(w[1], w[186], w[9], pars->GC_11, amp[1870]); 
  FFV1_0(w[187], w[6], w[9], pars->GC_11, amp[1871]); 
  FFV1_0(w[189], w[5], w[22], pars->GC_11, amp[1872]); 
  FFV2_3_0(w[23], w[5], w[173], pars->GC_50, pars->GC_58, amp[1873]); 
  FFV1_0(w[1], w[186], w[29], pars->GC_11, amp[1874]); 
  FFV1_0(w[187], w[6], w[29], pars->GC_11, amp[1875]); 
  FFV1_0(w[0], w[190], w[22], pars->GC_11, amp[1876]); 
  FFV2_3_0(w[0], w[33], w[173], pars->GC_50, pars->GC_58, amp[1877]); 
  FFV2_3_0(w[1], w[36], w[173], pars->GC_50, pars->GC_58, amp[1878]); 
  FFV1_0(w[1], w[192], w[35], pars->GC_11, amp[1879]); 
  FFV1_0(w[191], w[5], w[40], pars->GC_11, amp[1880]); 
  FFV1_0(w[0], w[188], w[40], pars->GC_11, amp[1881]); 
  FFV2_3_0(w[43], w[6], w[173], pars->GC_50, pars->GC_58, amp[1882]); 
  FFV1_0(w[193], w[6], w[35], pars->GC_11, amp[1883]); 
  FFV1_0(w[191], w[5], w[48], pars->GC_11, amp[1884]); 
  FFV1_0(w[0], w[188], w[48], pars->GC_11, amp[1885]); 
  FFV1_0(w[1], w[186], w[49], pars->GC_11, amp[1886]); 
  FFV1_0(w[187], w[6], w[49], pars->GC_11, amp[1887]); 
  FFV1_0(w[187], w[50], w[4], pars->GC_11, amp[1888]); 
  FFV1_0(w[51], w[186], w[4], pars->GC_11, amp[1889]); 
  FFV1_0(w[191], w[5], w[58], pars->GC_11, amp[1890]); 
  FFV1_0(w[0], w[188], w[58], pars->GC_11, amp[1891]); 
  FFV1_0(w[191], w[59], w[4], pars->GC_11, amp[1892]); 
  FFV1_0(w[60], w[188], w[4], pars->GC_11, amp[1893]); 
  FFV1_0(w[0], w[214], w[88], pars->GC_11, amp[1894]); 
  FFV1_0(w[210], w[5], w[88], pars->GC_11, amp[1895]); 
  FFV1_0(w[0], w[217], w[89], pars->GC_11, amp[1896]); 
  FFV1_0(w[210], w[61], w[89], pars->GC_11, amp[1897]); 
  FFV2_0(w[0], w[78], w[206], pars->GC_100, amp[1898]); 
  FFV1_0(w[0], w[218], w[70], pars->GC_11, amp[1899]); 
  FFV1_0(w[0], w[214], w[77], pars->GC_11, amp[1900]); 
  FFV1_0(w[210], w[5], w[77], pars->GC_11, amp[1901]); 
  FFV2_0(w[0], w[82], w[206], pars->GC_100, amp[1902]); 
  FFV1_0(w[0], w[213], w[68], pars->GC_11, amp[1903]); 
  FFV1_0(w[0], w[217], w[81], pars->GC_11, amp[1904]); 
  FFV1_0(w[210], w[61], w[81], pars->GC_11, amp[1905]); 
  FFV2_0(w[69], w[5], w[206], pars->GC_100, amp[1906]); 
  FFV1_0(w[209], w[5], w[68], pars->GC_11, amp[1907]); 
  FFV2_0(w[71], w[61], w[206], pars->GC_100, amp[1908]); 
  FFV1_0(w[209], w[61], w[70], pars->GC_11, amp[1909]); 
  FFV1_0(w[0], w[214], w[95], pars->GC_11, amp[1910]); 
  FFV1_0(w[210], w[5], w[95], pars->GC_11, amp[1911]); 
  FFV1_0(w[210], w[96], w[4], pars->GC_11, amp[1912]); 
  FFV1_0(w[97], w[214], w[4], pars->GC_11, amp[1913]); 
  FFV1_0(w[0], w[217], w[98], pars->GC_11, amp[1914]); 
  FFV1_0(w[210], w[61], w[98], pars->GC_11, amp[1915]); 
  FFV1_0(w[210], w[99], w[4], pars->GC_11, amp[1916]); 
  FFV1_0(w[100], w[217], w[4], pars->GC_11, amp[1917]); 
  FFV1_0(w[62], w[217], w[9], pars->GC_11, amp[1918]); 
  FFV1_0(w[215], w[61], w[9], pars->GC_11, amp[1919]); 
  FFV1_0(w[209], w[61], w[70], pars->GC_11, amp[1920]); 
  FFV2_0(w[71], w[61], w[206], pars->GC_100, amp[1921]); 
  FFV2_0(w[62], w[73], w[206], pars->GC_100, amp[1922]); 
  FFV1_0(w[62], w[218], w[35], pars->GC_11, amp[1923]); 
  FFV1_0(w[0], w[218], w[70], pars->GC_11, amp[1924]); 
  FFV2_0(w[0], w[78], w[206], pars->GC_100, amp[1925]); 
  FFV1_0(w[62], w[217], w[29], pars->GC_11, amp[1926]); 
  FFV1_0(w[215], w[61], w[29], pars->GC_11, amp[1927]); 
  FFV1_0(w[210], w[61], w[81], pars->GC_11, amp[1928]); 
  FFV1_0(w[0], w[217], w[81], pars->GC_11, amp[1929]); 
  FFV2_0(w[87], w[61], w[206], pars->GC_100, amp[1930]); 
  FFV1_0(w[216], w[61], w[35], pars->GC_11, amp[1931]); 
  FFV1_0(w[210], w[61], w[89], pars->GC_11, amp[1932]); 
  FFV1_0(w[0], w[217], w[89], pars->GC_11, amp[1933]); 
  FFV1_0(w[62], w[217], w[49], pars->GC_11, amp[1934]); 
  FFV1_0(w[215], w[61], w[49], pars->GC_11, amp[1935]); 
  FFV1_0(w[215], w[93], w[4], pars->GC_11, amp[1936]); 
  FFV1_0(w[94], w[217], w[4], pars->GC_11, amp[1937]); 
  FFV1_0(w[210], w[61], w[98], pars->GC_11, amp[1938]); 
  FFV1_0(w[0], w[217], w[98], pars->GC_11, amp[1939]); 
  FFV1_0(w[210], w[99], w[4], pars->GC_11, amp[1940]); 
  FFV1_0(w[100], w[217], w[4], pars->GC_11, amp[1941]); 
  FFV1_0(w[0], w[188], w[88], pars->GC_11, amp[1942]); 
  FFV1_0(w[191], w[5], w[88], pars->GC_11, amp[1943]); 
  FFV1_0(w[185], w[61], w[35], pars->GC_11, amp[1944]); 
  FFV2_5_0(w[87], w[61], w[173], pars->GC_51, pars->GC_58, amp[1945]); 
  FFV1_0(w[0], w[188], w[77], pars->GC_11, amp[1946]); 
  FFV1_0(w[191], w[5], w[77], pars->GC_11, amp[1947]); 
  FFV1_0(w[62], w[184], w[35], pars->GC_11, amp[1948]); 
  FFV2_5_0(w[62], w[73], w[173], pars->GC_51, pars->GC_58, amp[1949]); 
  FFV2_3_0(w[0], w[82], w[173], pars->GC_50, pars->GC_58, amp[1950]); 
  FFV1_0(w[0], w[190], w[68], pars->GC_11, amp[1951]); 
  FFV1_0(w[182], w[61], w[29], pars->GC_11, amp[1952]); 
  FFV1_0(w[62], w[183], w[29], pars->GC_11, amp[1953]); 
  FFV2_3_0(w[69], w[5], w[173], pars->GC_50, pars->GC_58, amp[1954]); 
  FFV1_0(w[189], w[5], w[68], pars->GC_11, amp[1955]); 
  FFV1_0(w[182], w[61], w[9], pars->GC_11, amp[1956]); 
  FFV1_0(w[62], w[183], w[9], pars->GC_11, amp[1957]); 
  FFV1_0(w[0], w[188], w[95], pars->GC_11, amp[1958]); 
  FFV1_0(w[191], w[5], w[95], pars->GC_11, amp[1959]); 
  FFV1_0(w[191], w[96], w[4], pars->GC_11, amp[1960]); 
  FFV1_0(w[97], w[188], w[4], pars->GC_11, amp[1961]); 
  FFV1_0(w[182], w[61], w[49], pars->GC_11, amp[1962]); 
  FFV1_0(w[62], w[183], w[49], pars->GC_11, amp[1963]); 
  FFV1_0(w[182], w[93], w[4], pars->GC_11, amp[1964]); 
  FFV1_0(w[94], w[183], w[4], pars->GC_11, amp[1965]); 
  FFV1_0(w[216], w[5], w[79], pars->GC_11, amp[1966]); 
  FFV2_0(w[84], w[5], w[206], pars->GC_100, amp[1967]); 
  FFV1_0(w[216], w[61], w[35], pars->GC_11, amp[1968]); 
  FFV2_0(w[87], w[61], w[206], pars->GC_100, amp[1969]); 
  FFV1_0(w[215], w[5], w[76], pars->GC_11, amp[1970]); 
  FFV1_0(w[62], w[214], w[76], pars->GC_11, amp[1971]); 
  FFV1_0(w[62], w[218], w[35], pars->GC_11, amp[1972]); 
  FFV2_0(w[62], w[73], w[206], pars->GC_100, amp[1973]); 
  FFV1_0(w[215], w[61], w[29], pars->GC_11, amp[1974]); 
  FFV1_0(w[62], w[217], w[29], pars->GC_11, amp[1975]); 
  FFV1_0(w[62], w[213], w[79], pars->GC_11, amp[1976]); 
  FFV2_0(w[62], w[80], w[206], pars->GC_100, amp[1977]); 
  FFV1_0(w[215], w[5], w[63], pars->GC_11, amp[1978]); 
  FFV1_0(w[62], w[214], w[63], pars->GC_11, amp[1979]); 
  FFV1_0(w[215], w[61], w[9], pars->GC_11, amp[1980]); 
  FFV1_0(w[62], w[217], w[9], pars->GC_11, amp[1981]); 
  FFV1_0(w[215], w[5], w[90], pars->GC_11, amp[1982]); 
  FFV1_0(w[62], w[214], w[90], pars->GC_11, amp[1983]); 
  FFV1_0(w[215], w[91], w[4], pars->GC_11, amp[1984]); 
  FFV1_0(w[92], w[214], w[4], pars->GC_11, amp[1985]); 
  FFV1_0(w[215], w[61], w[49], pars->GC_11, amp[1986]); 
  FFV1_0(w[62], w[217], w[49], pars->GC_11, amp[1987]); 
  FFV1_0(w[215], w[93], w[4], pars->GC_11, amp[1988]); 
  FFV1_0(w[94], w[217], w[4], pars->GC_11, amp[1989]); 
  FFV1_0(w[62], w[214], w[63], pars->GC_11, amp[1990]); 
  FFV1_0(w[215], w[5], w[63], pars->GC_11, amp[1991]); 
  FFV1_0(w[209], w[5], w[68], pars->GC_11, amp[1992]); 
  FFV2_0(w[69], w[5], w[206], pars->GC_100, amp[1993]); 
  FFV2_0(w[62], w[80], w[206], pars->GC_100, amp[1994]); 
  FFV1_0(w[62], w[213], w[79], pars->GC_11, amp[1995]); 
  FFV1_0(w[0], w[213], w[68], pars->GC_11, amp[1996]); 
  FFV2_0(w[0], w[82], w[206], pars->GC_100, amp[1997]); 
  FFV1_0(w[62], w[214], w[76], pars->GC_11, amp[1998]); 
  FFV1_0(w[215], w[5], w[76], pars->GC_11, amp[1999]); 
  FFV1_0(w[210], w[5], w[77], pars->GC_11, amp[2000]); 
  FFV1_0(w[0], w[214], w[77], pars->GC_11, amp[2001]); 
  FFV2_0(w[84], w[5], w[206], pars->GC_100, amp[2002]); 
  FFV1_0(w[216], w[5], w[79], pars->GC_11, amp[2003]); 
  FFV1_0(w[210], w[5], w[88], pars->GC_11, amp[2004]); 
  FFV1_0(w[0], w[214], w[88], pars->GC_11, amp[2005]); 
  FFV1_0(w[62], w[214], w[90], pars->GC_11, amp[2006]); 
  FFV1_0(w[215], w[5], w[90], pars->GC_11, amp[2007]); 
  FFV1_0(w[215], w[91], w[4], pars->GC_11, amp[2008]); 
  FFV1_0(w[92], w[214], w[4], pars->GC_11, amp[2009]); 
  FFV1_0(w[210], w[5], w[95], pars->GC_11, amp[2010]); 
  FFV1_0(w[0], w[214], w[95], pars->GC_11, amp[2011]); 
  FFV1_0(w[210], w[96], w[4], pars->GC_11, amp[2012]); 
  FFV1_0(w[97], w[214], w[4], pars->GC_11, amp[2013]); 
  FFV1_0(w[0], w[195], w[89], pars->GC_11, amp[2014]); 
  FFV1_0(w[191], w[61], w[89], pars->GC_11, amp[2015]); 
  FFV1_0(w[185], w[5], w[79], pars->GC_11, amp[2016]); 
  FFV2_5_0(w[84], w[5], w[173], pars->GC_51, pars->GC_58, amp[2017]); 
  FFV1_0(w[0], w[195], w[81], pars->GC_11, amp[2018]); 
  FFV1_0(w[191], w[61], w[81], pars->GC_11, amp[2019]); 
  FFV1_0(w[62], w[178], w[79], pars->GC_11, amp[2020]); 
  FFV2_5_0(w[62], w[80], w[173], pars->GC_51, pars->GC_58, amp[2021]); 
  FFV2_3_0(w[0], w[78], w[173], pars->GC_50, pars->GC_58, amp[2022]); 
  FFV1_0(w[0], w[196], w[70], pars->GC_11, amp[2023]); 
  FFV1_0(w[182], w[5], w[76], pars->GC_11, amp[2024]); 
  FFV1_0(w[62], w[176], w[76], pars->GC_11, amp[2025]); 
  FFV2_3_0(w[71], w[61], w[173], pars->GC_50, pars->GC_58, amp[2026]); 
  FFV1_0(w[189], w[61], w[70], pars->GC_11, amp[2027]); 
  FFV1_0(w[182], w[5], w[63], pars->GC_11, amp[2028]); 
  FFV1_0(w[62], w[176], w[63], pars->GC_11, amp[2029]); 
  FFV1_0(w[0], w[195], w[98], pars->GC_11, amp[2030]); 
  FFV1_0(w[191], w[61], w[98], pars->GC_11, amp[2031]); 
  FFV1_0(w[191], w[99], w[4], pars->GC_11, amp[2032]); 
  FFV1_0(w[100], w[195], w[4], pars->GC_11, amp[2033]); 
  FFV1_0(w[182], w[5], w[90], pars->GC_11, amp[2034]); 
  FFV1_0(w[62], w[176], w[90], pars->GC_11, amp[2035]); 
  FFV1_0(w[182], w[91], w[4], pars->GC_11, amp[2036]); 
  FFV1_0(w[92], w[176], w[4], pars->GC_11, amp[2037]); 
  FFV1_0(w[62], w[188], w[63], pars->GC_11, amp[2038]); 
  FFV1_0(w[194], w[5], w[63], pars->GC_11, amp[2039]); 
  FFV1_0(w[189], w[61], w[70], pars->GC_11, amp[2040]); 
  FFV2_3_0(w[71], w[61], w[173], pars->GC_50, pars->GC_58, amp[2041]); 
  FFV1_0(w[62], w[188], w[76], pars->GC_11, amp[2042]); 
  FFV1_0(w[194], w[5], w[76], pars->GC_11, amp[2043]); 
  FFV1_0(w[0], w[196], w[70], pars->GC_11, amp[2044]); 
  FFV2_3_0(w[0], w[78], w[173], pars->GC_50, pars->GC_58, amp[2045]); 
  FFV2_3_0(w[62], w[80], w[173], pars->GC_50, pars->GC_58, amp[2046]); 
  FFV1_0(w[62], w[190], w[79], pars->GC_11, amp[2047]); 
  FFV1_0(w[191], w[61], w[81], pars->GC_11, amp[2048]); 
  FFV1_0(w[0], w[195], w[81], pars->GC_11, amp[2049]); 
  FFV2_3_0(w[84], w[5], w[173], pars->GC_50, pars->GC_58, amp[2050]); 
  FFV1_0(w[197], w[5], w[79], pars->GC_11, amp[2051]); 
  FFV1_0(w[191], w[61], w[89], pars->GC_11, amp[2052]); 
  FFV1_0(w[0], w[195], w[89], pars->GC_11, amp[2053]); 
  FFV1_0(w[62], w[188], w[90], pars->GC_11, amp[2054]); 
  FFV1_0(w[194], w[5], w[90], pars->GC_11, amp[2055]); 
  FFV1_0(w[194], w[91], w[4], pars->GC_11, amp[2056]); 
  FFV1_0(w[92], w[188], w[4], pars->GC_11, amp[2057]); 
  FFV1_0(w[191], w[61], w[98], pars->GC_11, amp[2058]); 
  FFV1_0(w[0], w[195], w[98], pars->GC_11, amp[2059]); 
  FFV1_0(w[191], w[99], w[4], pars->GC_11, amp[2060]); 
  FFV1_0(w[100], w[195], w[4], pars->GC_11, amp[2061]); 
  FFV1_0(w[62], w[195], w[9], pars->GC_11, amp[2062]); 
  FFV1_0(w[194], w[61], w[9], pars->GC_11, amp[2063]); 
  FFV1_0(w[189], w[5], w[68], pars->GC_11, amp[2064]); 
  FFV2_3_0(w[69], w[5], w[173], pars->GC_50, pars->GC_58, amp[2065]); 
  FFV1_0(w[62], w[195], w[29], pars->GC_11, amp[2066]); 
  FFV1_0(w[194], w[61], w[29], pars->GC_11, amp[2067]); 
  FFV1_0(w[0], w[190], w[68], pars->GC_11, amp[2068]); 
  FFV2_3_0(w[0], w[82], w[173], pars->GC_50, pars->GC_58, amp[2069]); 
  FFV2_3_0(w[62], w[73], w[173], pars->GC_50, pars->GC_58, amp[2070]); 
  FFV1_0(w[62], w[196], w[35], pars->GC_11, amp[2071]); 
  FFV1_0(w[191], w[5], w[77], pars->GC_11, amp[2072]); 
  FFV1_0(w[0], w[188], w[77], pars->GC_11, amp[2073]); 
  FFV2_3_0(w[87], w[61], w[173], pars->GC_50, pars->GC_58, amp[2074]); 
  FFV1_0(w[197], w[61], w[35], pars->GC_11, amp[2075]); 
  FFV1_0(w[191], w[5], w[88], pars->GC_11, amp[2076]); 
  FFV1_0(w[0], w[188], w[88], pars->GC_11, amp[2077]); 
  FFV1_0(w[62], w[195], w[49], pars->GC_11, amp[2078]); 
  FFV1_0(w[194], w[61], w[49], pars->GC_11, amp[2079]); 
  FFV1_0(w[194], w[93], w[4], pars->GC_11, amp[2080]); 
  FFV1_0(w[94], w[195], w[4], pars->GC_11, amp[2081]); 
  FFV1_0(w[191], w[5], w[95], pars->GC_11, amp[2082]); 
  FFV1_0(w[0], w[188], w[95], pars->GC_11, amp[2083]); 
  FFV1_0(w[191], w[96], w[4], pars->GC_11, amp[2084]); 
  FFV1_0(w[97], w[188], w[4], pars->GC_11, amp[2085]); 
  FFV1_0(w[62], w[217], w[128], pars->GC_11, amp[2086]); 
  FFV1_0(w[215], w[61], w[128], pars->GC_11, amp[2087]); 
  FFV1_0(w[62], w[219], w[129], pars->GC_11, amp[2088]); 
  FFV1_0(w[215], w[125], w[129], pars->GC_11, amp[2089]); 
  FFV2_0(w[62], w[139], w[206], pars->GC_100, amp[2090]); 
  FFV1_0(w[62], w[220], w[138], pars->GC_11, amp[2091]); 
  FFV1_0(w[62], w[217], w[142], pars->GC_11, amp[2092]); 
  FFV1_0(w[215], w[61], w[142], pars->GC_11, amp[2093]); 
  FFV2_0(w[62], w[148], w[206], pars->GC_100, amp[2094]); 
  FFV1_0(w[62], w[218], w[147], pars->GC_11, amp[2095]); 
  FFV1_0(w[62], w[219], w[149], pars->GC_11, amp[2096]); 
  FFV1_0(w[215], w[125], w[149], pars->GC_11, amp[2097]); 
  FFV2_0(w[151], w[61], w[206], pars->GC_100, amp[2098]); 
  FFV1_0(w[216], w[61], w[147], pars->GC_11, amp[2099]); 
  FFV2_0(w[152], w[125], w[206], pars->GC_100, amp[2100]); 
  FFV1_0(w[216], w[125], w[138], pars->GC_11, amp[2101]); 
  FFV1_0(w[62], w[217], w[154], pars->GC_11, amp[2102]); 
  FFV1_0(w[215], w[61], w[154], pars->GC_11, amp[2103]); 
  FFV1_0(w[215], w[155], w[4], pars->GC_11, amp[2104]); 
  FFV1_0(w[156], w[217], w[4], pars->GC_11, amp[2105]); 
  FFV1_0(w[62], w[219], w[157], pars->GC_11, amp[2106]); 
  FFV1_0(w[215], w[125], w[157], pars->GC_11, amp[2107]); 
  FFV1_0(w[215], w[158], w[4], pars->GC_11, amp[2108]); 
  FFV1_0(w[159], w[219], w[4], pars->GC_11, amp[2109]); 
  FFV1_0(w[62], w[183], w[128], pars->GC_11, amp[2110]); 
  FFV1_0(w[182], w[61], w[128], pars->GC_11, amp[2111]); 
  FFV1_0(w[199], w[125], w[68], pars->GC_11, amp[2112]); 
  FFV2_5_0(w[136], w[125], w[173], pars->GC_51, pars->GC_58, amp[2113]); 
  FFV1_0(w[62], w[183], w[142], pars->GC_11, amp[2114]); 
  FFV1_0(w[182], w[61], w[142], pars->GC_11, amp[2115]); 
  FFV1_0(w[126], w[200], w[68], pars->GC_11, amp[2116]); 
  FFV2_5_0(w[126], w[146], w[173], pars->GC_51, pars->GC_58, amp[2117]); 
  FFV2_5_0(w[62], w[148], w[173], pars->GC_51, pars->GC_58, amp[2118]); 
  FFV1_0(w[62], w[184], w[147], pars->GC_11, amp[2119]); 
  FFV1_0(w[201], w[125], w[77], pars->GC_11, amp[2120]); 
  FFV1_0(w[126], w[198], w[77], pars->GC_11, amp[2121]); 
  FFV2_5_0(w[151], w[61], w[173], pars->GC_51, pars->GC_58, amp[2122]); 
  FFV1_0(w[185], w[61], w[147], pars->GC_11, amp[2123]); 
  FFV1_0(w[201], w[125], w[88], pars->GC_11, amp[2124]); 
  FFV1_0(w[126], w[198], w[88], pars->GC_11, amp[2125]); 
  FFV1_0(w[62], w[183], w[154], pars->GC_11, amp[2126]); 
  FFV1_0(w[182], w[61], w[154], pars->GC_11, amp[2127]); 
  FFV1_0(w[182], w[155], w[4], pars->GC_11, amp[2128]); 
  FFV1_0(w[156], w[183], w[4], pars->GC_11, amp[2129]); 
  FFV1_0(w[201], w[125], w[95], pars->GC_11, amp[2130]); 
  FFV1_0(w[126], w[198], w[95], pars->GC_11, amp[2131]); 
  FFV1_0(w[201], w[163], w[4], pars->GC_11, amp[2132]); 
  FFV1_0(w[164], w[198], w[4], pars->GC_11, amp[2133]); 
  FFV1_0(w[62], w[217], w[128], pars->GC_11, amp[2134]); 
  FFV1_0(w[215], w[61], w[128], pars->GC_11, amp[2135]); 
  FFV1_0(w[221], w[61], w[132], pars->GC_11, amp[2136]); 
  FFV2_0(w[134], w[61], w[206], pars->GC_100, amp[2137]); 
  FFV1_0(w[62], w[217], w[142], pars->GC_11, amp[2138]); 
  FFV1_0(w[215], w[61], w[142], pars->GC_11, amp[2139]); 
  FFV1_0(w[222], w[61], w[143], pars->GC_11, amp[2140]); 
  FFV1_0(w[126], w[217], w[143], pars->GC_11, amp[2141]); 
  FFV2_0(w[62], w[148], w[206], pars->GC_100, amp[2142]); 
  FFV1_0(w[62], w[218], w[147], pars->GC_11, amp[2143]); 
  FFV1_0(w[126], w[218], w[132], pars->GC_11, amp[2144]); 
  FFV2_0(w[126], w[150], w[206], pars->GC_100, amp[2145]); 
  FFV2_0(w[151], w[61], w[206], pars->GC_100, amp[2146]); 
  FFV1_0(w[216], w[61], w[147], pars->GC_11, amp[2147]); 
  FFV1_0(w[222], w[61], w[153], pars->GC_11, amp[2148]); 
  FFV1_0(w[126], w[217], w[153], pars->GC_11, amp[2149]); 
  FFV1_0(w[62], w[217], w[154], pars->GC_11, amp[2150]); 
  FFV1_0(w[215], w[61], w[154], pars->GC_11, amp[2151]); 
  FFV1_0(w[215], w[155], w[4], pars->GC_11, amp[2152]); 
  FFV1_0(w[156], w[217], w[4], pars->GC_11, amp[2153]); 
  FFV1_0(w[222], w[61], w[160], pars->GC_11, amp[2154]); 
  FFV1_0(w[126], w[217], w[160], pars->GC_11, amp[2155]); 
  FFV1_0(w[222], w[161], w[4], pars->GC_11, amp[2156]); 
  FFV1_0(w[162], w[217], w[4], pars->GC_11, amp[2157]); 
  FFV1_0(w[62], w[219], w[129], pars->GC_11, amp[2158]); 
  FFV1_0(w[215], w[125], w[129], pars->GC_11, amp[2159]); 
  FFV1_0(w[221], w[125], w[68], pars->GC_11, amp[2160]); 
  FFV2_0(w[136], w[125], w[206], pars->GC_100, amp[2161]); 
  FFV2_0(w[62], w[139], w[206], pars->GC_100, amp[2162]); 
  FFV1_0(w[62], w[220], w[138], pars->GC_11, amp[2163]); 
  FFV1_0(w[126], w[220], w[68], pars->GC_11, amp[2164]); 
  FFV2_0(w[126], w[146], w[206], pars->GC_100, amp[2165]); 
  FFV1_0(w[62], w[219], w[149], pars->GC_11, amp[2166]); 
  FFV1_0(w[215], w[125], w[149], pars->GC_11, amp[2167]); 
  FFV1_0(w[222], w[125], w[77], pars->GC_11, amp[2168]); 
  FFV1_0(w[126], w[219], w[77], pars->GC_11, amp[2169]); 
  FFV2_0(w[152], w[125], w[206], pars->GC_100, amp[2170]); 
  FFV1_0(w[216], w[125], w[138], pars->GC_11, amp[2171]); 
  FFV1_0(w[222], w[125], w[88], pars->GC_11, amp[2172]); 
  FFV1_0(w[126], w[219], w[88], pars->GC_11, amp[2173]); 
  FFV1_0(w[62], w[219], w[157], pars->GC_11, amp[2174]); 
  FFV1_0(w[215], w[125], w[157], pars->GC_11, amp[2175]); 
  FFV1_0(w[215], w[158], w[4], pars->GC_11, amp[2176]); 
  FFV1_0(w[159], w[219], w[4], pars->GC_11, amp[2177]); 
  FFV1_0(w[222], w[125], w[95], pars->GC_11, amp[2178]); 
  FFV1_0(w[126], w[219], w[95], pars->GC_11, amp[2179]); 
  FFV1_0(w[222], w[163], w[4], pars->GC_11, amp[2180]); 
  FFV1_0(w[164], w[219], w[4], pars->GC_11, amp[2181]); 
  FFV1_0(w[62], w[195], w[128], pars->GC_11, amp[2182]); 
  FFV1_0(w[194], w[61], w[128], pars->GC_11, amp[2183]); 
  FFV1_0(w[199], w[125], w[68], pars->GC_11, amp[2184]); 
  FFV2_5_0(w[136], w[125], w[173], pars->GC_51, pars->GC_58, amp[2185]); 
  FFV1_0(w[62], w[195], w[142], pars->GC_11, amp[2186]); 
  FFV1_0(w[194], w[61], w[142], pars->GC_11, amp[2187]); 
  FFV1_0(w[126], w[200], w[68], pars->GC_11, amp[2188]); 
  FFV2_5_0(w[126], w[146], w[173], pars->GC_51, pars->GC_58, amp[2189]); 
  FFV2_3_0(w[62], w[148], w[173], pars->GC_50, pars->GC_58, amp[2190]); 
  FFV1_0(w[62], w[196], w[147], pars->GC_11, amp[2191]); 
  FFV1_0(w[201], w[125], w[77], pars->GC_11, amp[2192]); 
  FFV1_0(w[126], w[198], w[77], pars->GC_11, amp[2193]); 
  FFV2_3_0(w[151], w[61], w[173], pars->GC_50, pars->GC_58, amp[2194]); 
  FFV1_0(w[197], w[61], w[147], pars->GC_11, amp[2195]); 
  FFV1_0(w[201], w[125], w[88], pars->GC_11, amp[2196]); 
  FFV1_0(w[126], w[198], w[88], pars->GC_11, amp[2197]); 
  FFV1_0(w[62], w[195], w[154], pars->GC_11, amp[2198]); 
  FFV1_0(w[194], w[61], w[154], pars->GC_11, amp[2199]); 
  FFV1_0(w[194], w[155], w[4], pars->GC_11, amp[2200]); 
  FFV1_0(w[156], w[195], w[4], pars->GC_11, amp[2201]); 
  FFV1_0(w[201], w[125], w[95], pars->GC_11, amp[2202]); 
  FFV1_0(w[126], w[198], w[95], pars->GC_11, amp[2203]); 
  FFV1_0(w[201], w[163], w[4], pars->GC_11, amp[2204]); 
  FFV1_0(w[164], w[198], w[4], pars->GC_11, amp[2205]); 
  FFV1_0(w[221], w[61], w[132], pars->GC_11, amp[2206]); 
  FFV2_0(w[134], w[61], w[206], pars->GC_100, amp[2207]); 
  FFV1_0(w[221], w[125], w[68], pars->GC_11, amp[2208]); 
  FFV2_0(w[136], w[125], w[206], pars->GC_100, amp[2209]); 
  FFV1_0(w[222], w[61], w[143], pars->GC_11, amp[2210]); 
  FFV1_0(w[126], w[217], w[143], pars->GC_11, amp[2211]); 
  FFV1_0(w[126], w[220], w[68], pars->GC_11, amp[2212]); 
  FFV2_0(w[126], w[146], w[206], pars->GC_100, amp[2213]); 
  FFV1_0(w[222], w[125], w[77], pars->GC_11, amp[2214]); 
  FFV1_0(w[126], w[219], w[77], pars->GC_11, amp[2215]); 
  FFV1_0(w[126], w[218], w[132], pars->GC_11, amp[2216]); 
  FFV2_0(w[126], w[150], w[206], pars->GC_100, amp[2217]); 
  FFV1_0(w[222], w[61], w[153], pars->GC_11, amp[2218]); 
  FFV1_0(w[126], w[217], w[153], pars->GC_11, amp[2219]); 
  FFV1_0(w[222], w[125], w[88], pars->GC_11, amp[2220]); 
  FFV1_0(w[126], w[219], w[88], pars->GC_11, amp[2221]); 
  FFV1_0(w[222], w[61], w[160], pars->GC_11, amp[2222]); 
  FFV1_0(w[126], w[217], w[160], pars->GC_11, amp[2223]); 
  FFV1_0(w[222], w[161], w[4], pars->GC_11, amp[2224]); 
  FFV1_0(w[162], w[217], w[4], pars->GC_11, amp[2225]); 
  FFV1_0(w[222], w[125], w[95], pars->GC_11, amp[2226]); 
  FFV1_0(w[126], w[219], w[95], pars->GC_11, amp[2227]); 
  FFV1_0(w[222], w[163], w[4], pars->GC_11, amp[2228]); 
  FFV1_0(w[164], w[219], w[4], pars->GC_11, amp[2229]); 
  FFV1_0(w[62], w[195], w[128], pars->GC_11, amp[2230]); 
  FFV1_0(w[194], w[61], w[128], pars->GC_11, amp[2231]); 
  FFV1_0(w[203], w[125], w[68], pars->GC_11, amp[2232]); 
  FFV2_3_0(w[136], w[125], w[173], pars->GC_50, pars->GC_58, amp[2233]); 
  FFV1_0(w[62], w[195], w[142], pars->GC_11, amp[2234]); 
  FFV1_0(w[194], w[61], w[142], pars->GC_11, amp[2235]); 
  FFV1_0(w[126], w[204], w[68], pars->GC_11, amp[2236]); 
  FFV2_3_0(w[126], w[146], w[173], pars->GC_50, pars->GC_58, amp[2237]); 
  FFV2_3_0(w[62], w[148], w[173], pars->GC_50, pars->GC_58, amp[2238]); 
  FFV1_0(w[62], w[196], w[147], pars->GC_11, amp[2239]); 
  FFV1_0(w[205], w[125], w[77], pars->GC_11, amp[2240]); 
  FFV1_0(w[126], w[202], w[77], pars->GC_11, amp[2241]); 
  FFV2_3_0(w[151], w[61], w[173], pars->GC_50, pars->GC_58, amp[2242]); 
  FFV1_0(w[197], w[61], w[147], pars->GC_11, amp[2243]); 
  FFV1_0(w[205], w[125], w[88], pars->GC_11, amp[2244]); 
  FFV1_0(w[126], w[202], w[88], pars->GC_11, amp[2245]); 
  FFV1_0(w[62], w[195], w[154], pars->GC_11, amp[2246]); 
  FFV1_0(w[194], w[61], w[154], pars->GC_11, amp[2247]); 
  FFV1_0(w[194], w[155], w[4], pars->GC_11, amp[2248]); 
  FFV1_0(w[156], w[195], w[4], pars->GC_11, amp[2249]); 
  FFV1_0(w[205], w[125], w[95], pars->GC_11, amp[2250]); 
  FFV1_0(w[126], w[202], w[95], pars->GC_11, amp[2251]); 
  FFV1_0(w[205], w[163], w[4], pars->GC_11, amp[2252]); 
  FFV1_0(w[164], w[202], w[4], pars->GC_11, amp[2253]); 
  FFV1_0(w[1], w[207], w[9], pars->GC_11, amp[2254]); 
  FFV1_0(w[208], w[6], w[9], pars->GC_11, amp[2255]); 
  FFV1_0(w[1], w[207], w[29], pars->GC_11, amp[2256]); 
  FFV1_0(w[208], w[6], w[29], pars->GC_11, amp[2257]); 
  FFV2_0(w[1], w[36], w[206], pars->GC_100, amp[2258]); 
  FFV1_0(w[1], w[211], w[35], pars->GC_11, amp[2259]); 
  FFV2_0(w[43], w[6], w[206], pars->GC_100, amp[2260]); 
  FFV1_0(w[212], w[6], w[35], pars->GC_11, amp[2261]); 
  FFV1_0(w[1], w[207], w[49], pars->GC_11, amp[2262]); 
  FFV1_0(w[208], w[6], w[49], pars->GC_11, amp[2263]); 
  FFV1_0(w[208], w[50], w[4], pars->GC_11, amp[2264]); 
  FFV1_0(w[51], w[207], w[4], pars->GC_11, amp[2265]); 
  FFV1_0(w[209], w[6], w[18], pars->GC_11, amp[2266]); 
  FFV2_0(w[20], w[6], w[206], pars->GC_100, amp[2267]); 
  FFV1_0(w[210], w[6], w[30], pars->GC_11, amp[2268]); 
  FFV1_0(w[0], w[207], w[30], pars->GC_11, amp[2269]); 
  FFV1_0(w[0], w[211], w[18], pars->GC_11, amp[2270]); 
  FFV2_0(w[0], w[41], w[206], pars->GC_100, amp[2271]); 
  FFV1_0(w[210], w[6], w[47], pars->GC_11, amp[2272]); 
  FFV1_0(w[0], w[207], w[47], pars->GC_11, amp[2273]); 
  FFV1_0(w[210], w[6], w[55], pars->GC_11, amp[2274]); 
  FFV1_0(w[0], w[207], w[55], pars->GC_11, amp[2275]); 
  FFV1_0(w[210], w[56], w[4], pars->GC_11, amp[2276]); 
  FFV1_0(w[57], w[207], w[4], pars->GC_11, amp[2277]); 
  FFV1_0(w[1], w[207], w[9], pars->GC_11, amp[2278]); 
  FFV1_0(w[208], w[6], w[9], pars->GC_11, amp[2279]); 
  FFV1_0(w[1], w[207], w[29], pars->GC_11, amp[2280]); 
  FFV1_0(w[208], w[6], w[29], pars->GC_11, amp[2281]); 
  FFV2_0(w[1], w[36], w[206], pars->GC_100, amp[2282]); 
  FFV1_0(w[1], w[211], w[35], pars->GC_11, amp[2283]); 
  FFV2_0(w[43], w[6], w[206], pars->GC_100, amp[2284]); 
  FFV1_0(w[212], w[6], w[35], pars->GC_11, amp[2285]); 
  FFV1_0(w[1], w[207], w[49], pars->GC_11, amp[2286]); 
  FFV1_0(w[208], w[6], w[49], pars->GC_11, amp[2287]); 
  FFV1_0(w[208], w[50], w[4], pars->GC_11, amp[2288]); 
  FFV1_0(w[51], w[207], w[4], pars->GC_11, amp[2289]); 
  FFV1_0(w[62], w[214], w[63], pars->GC_11, amp[2290]); 
  FFV1_0(w[215], w[5], w[63], pars->GC_11, amp[2291]); 
  FFV1_0(w[62], w[214], w[76], pars->GC_11, amp[2292]); 
  FFV1_0(w[215], w[5], w[76], pars->GC_11, amp[2293]); 
  FFV2_0(w[62], w[80], w[206], pars->GC_100, amp[2294]); 
  FFV1_0(w[62], w[213], w[79], pars->GC_11, amp[2295]); 
  FFV2_0(w[84], w[5], w[206], pars->GC_100, amp[2296]); 
  FFV1_0(w[216], w[5], w[79], pars->GC_11, amp[2297]); 
  FFV1_0(w[62], w[214], w[90], pars->GC_11, amp[2298]); 
  FFV1_0(w[215], w[5], w[90], pars->GC_11, amp[2299]); 
  FFV1_0(w[215], w[91], w[4], pars->GC_11, amp[2300]); 
  FFV1_0(w[92], w[214], w[4], pars->GC_11, amp[2301]); 
  FFV1_0(w[62], w[214], w[63], pars->GC_11, amp[2302]); 
  FFV1_0(w[215], w[5], w[63], pars->GC_11, amp[2303]); 
  FFV1_0(w[62], w[214], w[76], pars->GC_11, amp[2304]); 
  FFV1_0(w[215], w[5], w[76], pars->GC_11, amp[2305]); 
  FFV2_0(w[62], w[80], w[206], pars->GC_100, amp[2306]); 
  FFV1_0(w[62], w[213], w[79], pars->GC_11, amp[2307]); 
  FFV2_0(w[84], w[5], w[206], pars->GC_100, amp[2308]); 
  FFV1_0(w[216], w[5], w[79], pars->GC_11, amp[2309]); 
  FFV1_0(w[62], w[214], w[90], pars->GC_11, amp[2310]); 
  FFV1_0(w[215], w[5], w[90], pars->GC_11, amp[2311]); 
  FFV1_0(w[215], w[91], w[4], pars->GC_11, amp[2312]); 
  FFV1_0(w[92], w[214], w[4], pars->GC_11, amp[2313]); 
  FFV1_0(w[209], w[5], w[68], pars->GC_11, amp[2314]); 
  FFV2_0(w[69], w[5], w[206], pars->GC_100, amp[2315]); 
  FFV1_0(w[210], w[5], w[77], pars->GC_11, amp[2316]); 
  FFV1_0(w[0], w[214], w[77], pars->GC_11, amp[2317]); 
  FFV1_0(w[0], w[213], w[68], pars->GC_11, amp[2318]); 
  FFV2_0(w[0], w[82], w[206], pars->GC_100, amp[2319]); 
  FFV1_0(w[210], w[5], w[88], pars->GC_11, amp[2320]); 
  FFV1_0(w[0], w[214], w[88], pars->GC_11, amp[2321]); 
  FFV1_0(w[210], w[5], w[95], pars->GC_11, amp[2322]); 
  FFV1_0(w[0], w[214], w[95], pars->GC_11, amp[2323]); 
  FFV1_0(w[210], w[96], w[4], pars->GC_11, amp[2324]); 
  FFV1_0(w[97], w[214], w[4], pars->GC_11, amp[2325]); 
  FFV1_0(w[62], w[217], w[9], pars->GC_11, amp[2326]); 
  FFV1_0(w[215], w[61], w[9], pars->GC_11, amp[2327]); 
  FFV1_0(w[62], w[217], w[29], pars->GC_11, amp[2328]); 
  FFV1_0(w[215], w[61], w[29], pars->GC_11, amp[2329]); 
  FFV2_0(w[62], w[73], w[206], pars->GC_100, amp[2330]); 
  FFV1_0(w[62], w[218], w[35], pars->GC_11, amp[2331]); 
  FFV2_0(w[87], w[61], w[206], pars->GC_100, amp[2332]); 
  FFV1_0(w[216], w[61], w[35], pars->GC_11, amp[2333]); 
  FFV1_0(w[62], w[217], w[49], pars->GC_11, amp[2334]); 
  FFV1_0(w[215], w[61], w[49], pars->GC_11, amp[2335]); 
  FFV1_0(w[215], w[93], w[4], pars->GC_11, amp[2336]); 
  FFV1_0(w[94], w[217], w[4], pars->GC_11, amp[2337]); 
  FFV1_0(w[209], w[61], w[70], pars->GC_11, amp[2338]); 
  FFV2_0(w[71], w[61], w[206], pars->GC_100, amp[2339]); 
  FFV1_0(w[210], w[61], w[81], pars->GC_11, amp[2340]); 
  FFV1_0(w[0], w[217], w[81], pars->GC_11, amp[2341]); 
  FFV1_0(w[0], w[218], w[70], pars->GC_11, amp[2342]); 
  FFV2_0(w[0], w[78], w[206], pars->GC_100, amp[2343]); 
  FFV1_0(w[210], w[61], w[89], pars->GC_11, amp[2344]); 
  FFV1_0(w[0], w[217], w[89], pars->GC_11, amp[2345]); 
  FFV1_0(w[210], w[61], w[98], pars->GC_11, amp[2346]); 
  FFV1_0(w[0], w[217], w[98], pars->GC_11, amp[2347]); 
  FFV1_0(w[210], w[99], w[4], pars->GC_11, amp[2348]); 
  FFV1_0(w[100], w[217], w[4], pars->GC_11, amp[2349]); 
  FFV1_0(w[62], w[217], w[9], pars->GC_11, amp[2350]); 
  FFV1_0(w[215], w[61], w[9], pars->GC_11, amp[2351]); 
  FFV1_0(w[62], w[217], w[29], pars->GC_11, amp[2352]); 
  FFV1_0(w[215], w[61], w[29], pars->GC_11, amp[2353]); 
  FFV2_0(w[62], w[73], w[206], pars->GC_100, amp[2354]); 
  FFV1_0(w[62], w[218], w[35], pars->GC_11, amp[2355]); 
  FFV2_0(w[87], w[61], w[206], pars->GC_100, amp[2356]); 
  FFV1_0(w[216], w[61], w[35], pars->GC_11, amp[2357]); 
  FFV1_0(w[62], w[217], w[49], pars->GC_11, amp[2358]); 
  FFV1_0(w[215], w[61], w[49], pars->GC_11, amp[2359]); 
  FFV1_0(w[215], w[93], w[4], pars->GC_11, amp[2360]); 
  FFV1_0(w[94], w[217], w[4], pars->GC_11, amp[2361]); 
  FFV1_0(w[209], w[5], w[22], pars->GC_11, amp[2362]); 
  FFV2_0(w[23], w[5], w[206], pars->GC_100, amp[2363]); 
  FFV1_0(w[0], w[213], w[22], pars->GC_11, amp[2364]); 
  FFV2_0(w[0], w[33], w[206], pars->GC_100, amp[2365]); 
  FFV1_0(w[210], w[5], w[40], pars->GC_11, amp[2366]); 
  FFV1_0(w[0], w[214], w[40], pars->GC_11, amp[2367]); 
  FFV1_0(w[210], w[5], w[48], pars->GC_11, amp[2368]); 
  FFV1_0(w[0], w[214], w[48], pars->GC_11, amp[2369]); 
  FFV1_0(w[210], w[5], w[58], pars->GC_11, amp[2370]); 
  FFV1_0(w[0], w[214], w[58], pars->GC_11, amp[2371]); 
  FFV1_0(w[210], w[59], w[4], pars->GC_11, amp[2372]); 
  FFV1_0(w[60], w[214], w[4], pars->GC_11, amp[2373]); 
  FFV1_0(w[0], w[217], w[89], pars->GC_11, amp[2374]); 
  FFV1_0(w[210], w[61], w[89], pars->GC_11, amp[2375]); 
  FFV2_0(w[0], w[78], w[206], pars->GC_100, amp[2376]); 
  FFV1_0(w[0], w[218], w[70], pars->GC_11, amp[2377]); 
  FFV1_0(w[0], w[217], w[81], pars->GC_11, amp[2378]); 
  FFV1_0(w[210], w[61], w[81], pars->GC_11, amp[2379]); 
  FFV2_0(w[71], w[61], w[206], pars->GC_100, amp[2380]); 
  FFV1_0(w[209], w[61], w[70], pars->GC_11, amp[2381]); 
  FFV1_0(w[0], w[217], w[98], pars->GC_11, amp[2382]); 
  FFV1_0(w[210], w[61], w[98], pars->GC_11, amp[2383]); 
  FFV1_0(w[210], w[99], w[4], pars->GC_11, amp[2384]); 
  FFV1_0(w[100], w[217], w[4], pars->GC_11, amp[2385]); 
  FFV1_0(w[0], w[214], w[88], pars->GC_11, amp[2386]); 
  FFV1_0(w[210], w[5], w[88], pars->GC_11, amp[2387]); 
  FFV2_0(w[0], w[82], w[206], pars->GC_100, amp[2388]); 
  FFV1_0(w[0], w[213], w[68], pars->GC_11, amp[2389]); 
  FFV1_0(w[0], w[214], w[77], pars->GC_11, amp[2390]); 
  FFV1_0(w[210], w[5], w[77], pars->GC_11, amp[2391]); 
  FFV2_0(w[69], w[5], w[206], pars->GC_100, amp[2392]); 
  FFV1_0(w[209], w[5], w[68], pars->GC_11, amp[2393]); 
  FFV1_0(w[0], w[214], w[95], pars->GC_11, amp[2394]); 
  FFV1_0(w[210], w[5], w[95], pars->GC_11, amp[2395]); 
  FFV1_0(w[210], w[96], w[4], pars->GC_11, amp[2396]); 
  FFV1_0(w[97], w[214], w[4], pars->GC_11, amp[2397]); 
  FFV1_0(w[62], w[217], w[128], pars->GC_11, amp[2398]); 
  FFV1_0(w[215], w[61], w[128], pars->GC_11, amp[2399]); 
  FFV1_0(w[62], w[217], w[142], pars->GC_11, amp[2400]); 
  FFV1_0(w[215], w[61], w[142], pars->GC_11, amp[2401]); 
  FFV2_0(w[62], w[148], w[206], pars->GC_100, amp[2402]); 
  FFV1_0(w[62], w[218], w[147], pars->GC_11, amp[2403]); 
  FFV2_0(w[151], w[61], w[206], pars->GC_100, amp[2404]); 
  FFV1_0(w[216], w[61], w[147], pars->GC_11, amp[2405]); 
  FFV1_0(w[62], w[217], w[154], pars->GC_11, amp[2406]); 
  FFV1_0(w[215], w[61], w[154], pars->GC_11, amp[2407]); 
  FFV1_0(w[215], w[155], w[4], pars->GC_11, amp[2408]); 
  FFV1_0(w[156], w[217], w[4], pars->GC_11, amp[2409]); 
  FFV1_0(w[62], w[219], w[129], pars->GC_11, amp[2410]); 
  FFV1_0(w[215], w[125], w[129], pars->GC_11, amp[2411]); 
  FFV2_0(w[62], w[139], w[206], pars->GC_100, amp[2412]); 
  FFV1_0(w[62], w[220], w[138], pars->GC_11, amp[2413]); 
  FFV1_0(w[62], w[219], w[149], pars->GC_11, amp[2414]); 
  FFV1_0(w[215], w[125], w[149], pars->GC_11, amp[2415]); 
  FFV2_0(w[152], w[125], w[206], pars->GC_100, amp[2416]); 
  FFV1_0(w[216], w[125], w[138], pars->GC_11, amp[2417]); 
  FFV1_0(w[62], w[219], w[157], pars->GC_11, amp[2418]); 
  FFV1_0(w[215], w[125], w[157], pars->GC_11, amp[2419]); 
  FFV1_0(w[215], w[158], w[4], pars->GC_11, amp[2420]); 
  FFV1_0(w[159], w[219], w[4], pars->GC_11, amp[2421]); 
  FFV1_0(w[62], w[217], w[128], pars->GC_11, amp[2422]); 
  FFV1_0(w[215], w[61], w[128], pars->GC_11, amp[2423]); 
  FFV1_0(w[62], w[217], w[142], pars->GC_11, amp[2424]); 
  FFV1_0(w[215], w[61], w[142], pars->GC_11, amp[2425]); 
  FFV2_0(w[62], w[148], w[206], pars->GC_100, amp[2426]); 
  FFV1_0(w[62], w[218], w[147], pars->GC_11, amp[2427]); 
  FFV2_0(w[151], w[61], w[206], pars->GC_100, amp[2428]); 
  FFV1_0(w[216], w[61], w[147], pars->GC_11, amp[2429]); 
  FFV1_0(w[62], w[217], w[154], pars->GC_11, amp[2430]); 
  FFV1_0(w[215], w[61], w[154], pars->GC_11, amp[2431]); 
  FFV1_0(w[215], w[155], w[4], pars->GC_11, amp[2432]); 
  FFV1_0(w[156], w[217], w[4], pars->GC_11, amp[2433]); 
  FFV1_0(w[221], w[125], w[68], pars->GC_11, amp[2434]); 
  FFV2_0(w[136], w[125], w[206], pars->GC_100, amp[2435]); 
  FFV1_0(w[126], w[220], w[68], pars->GC_11, amp[2436]); 
  FFV2_0(w[126], w[146], w[206], pars->GC_100, amp[2437]); 
  FFV1_0(w[222], w[125], w[77], pars->GC_11, amp[2438]); 
  FFV1_0(w[126], w[219], w[77], pars->GC_11, amp[2439]); 
  FFV1_0(w[222], w[125], w[88], pars->GC_11, amp[2440]); 
  FFV1_0(w[126], w[219], w[88], pars->GC_11, amp[2441]); 
  FFV1_0(w[222], w[125], w[95], pars->GC_11, amp[2442]); 
  FFV1_0(w[126], w[219], w[95], pars->GC_11, amp[2443]); 
  FFV1_0(w[222], w[163], w[4], pars->GC_11, amp[2444]); 
  FFV1_0(w[164], w[219], w[4], pars->GC_11, amp[2445]); 


}
double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_uu_epemguu() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 96;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[0] + 1./3. * amp[1] + amp[2] + amp[3] +
      1./3. * amp[4] + 1./3. * amp[5] + amp[6] + amp[7] + amp[8] + amp[9] +
      amp[10] + amp[11] + 1./3. * amp[12] + 1./3. * amp[13] + 1./3. * amp[14] +
      1./3. * amp[15] + amp[16] + amp[17] + amp[18] + amp[19] + 1./3. * amp[20]
      + 1./3. * amp[21] + amp[22] + amp[23] + 1./3. * amp[24] + 1./3. * amp[25]
      + amp[26] + amp[27] + 1./3. * amp[28] + 1./3. * amp[29] + 1./3. * amp[30]
      + 1./3. * amp[31] - Complex<double> (0, 1) * amp[72] - Complex<double>
      (0, 1) * amp[73] + amp[75] - Complex<double> (0, 1) * amp[76] -
      Complex<double> (0, 1) * amp[77] + amp[79] + Complex<double> (0, 1) *
      amp[80] + Complex<double> (0, 1) * amp[81] + amp[82] + Complex<double>
      (0, 1) * amp[84] + Complex<double> (0, 1) * amp[85] + amp[86] + 1./3. *
      amp[90] + 1./3. * amp[91] + 1./3. * amp[94] + 1./3. * amp[95]);
  jamp[1] = +1./2. * (-1./3. * amp[16] - 1./3. * amp[17] - 1./3. * amp[18] -
      1./3. * amp[19] - amp[20] - amp[21] - 1./3. * amp[22] - 1./3. * amp[23] -
      amp[24] - amp[25] - 1./3. * amp[26] - 1./3. * amp[27] - amp[28] - amp[29]
      - amp[30] - amp[31] - amp[48] - amp[49] - amp[50] - amp[51] - 1./3. *
      amp[52] - 1./3. * amp[53] - 1./3. * amp[54] - 1./3. * amp[55] - 1./3. *
      amp[56] - 1./3. * amp[57] - amp[58] - amp[59] - 1./3. * amp[60] - 1./3. *
      amp[61] - amp[62] - amp[63] - Complex<double> (0, 1) * amp[64] -
      Complex<double> (0, 1) * amp[65] - amp[66] - Complex<double> (0, 1) *
      amp[68] - Complex<double> (0, 1) * amp[69] - amp[70] - 1./3. * amp[74] -
      1./3. * amp[75] - 1./3. * amp[78] - 1./3. * amp[79] + Complex<double> (0,
      1) * amp[88] + Complex<double> (0, 1) * amp[89] - amp[91] +
      Complex<double> (0, 1) * amp[92] + Complex<double> (0, 1) * amp[93] -
      amp[95]);
  jamp[2] = +1./2. * (-amp[0] - amp[1] - 1./3. * amp[2] - 1./3. * amp[3] -
      amp[4] - amp[5] - 1./3. * amp[6] - 1./3. * amp[7] - 1./3. * amp[8] -
      1./3. * amp[9] - 1./3. * amp[10] - 1./3. * amp[11] - amp[12] - amp[13] -
      amp[14] - amp[15] - amp[32] - amp[33] - amp[34] - amp[35] - 1./3. *
      amp[36] - 1./3. * amp[37] - amp[38] - amp[39] - 1./3. * amp[40] - 1./3. *
      amp[41] - amp[42] - amp[43] - 1./3. * amp[44] - 1./3. * amp[45] - 1./3. *
      amp[46] - 1./3. * amp[47] + Complex<double> (0, 1) * amp[64] +
      Complex<double> (0, 1) * amp[65] - amp[67] + Complex<double> (0, 1) *
      amp[68] + Complex<double> (0, 1) * amp[69] - amp[71] - 1./3. * amp[82] -
      1./3. * amp[83] - 1./3. * amp[86] - 1./3. * amp[87] - Complex<double> (0,
      1) * amp[88] - Complex<double> (0, 1) * amp[89] - amp[90] -
      Complex<double> (0, 1) * amp[92] - Complex<double> (0, 1) * amp[93] -
      amp[94]);
  jamp[3] = +1./2. * (+1./3. * amp[32] + 1./3. * amp[33] + 1./3. * amp[34] +
      1./3. * amp[35] + amp[36] + amp[37] + 1./3. * amp[38] + 1./3. * amp[39] +
      amp[40] + amp[41] + 1./3. * amp[42] + 1./3. * amp[43] + amp[44] + amp[45]
      + amp[46] + amp[47] + 1./3. * amp[48] + 1./3. * amp[49] + 1./3. * amp[50]
      + 1./3. * amp[51] + amp[52] + amp[53] + amp[54] + amp[55] + amp[56] +
      amp[57] + 1./3. * amp[58] + 1./3. * amp[59] + amp[60] + amp[61] + 1./3. *
      amp[62] + 1./3. * amp[63] + 1./3. * amp[66] + 1./3. * amp[67] + 1./3. *
      amp[70] + 1./3. * amp[71] + Complex<double> (0, 1) * amp[72] +
      Complex<double> (0, 1) * amp[73] + amp[74] + Complex<double> (0, 1) *
      amp[76] + Complex<double> (0, 1) * amp[77] + amp[78] - Complex<double>
      (0, 1) * amp[80] - Complex<double> (0, 1) * amp[81] + amp[83] -
      Complex<double> (0, 1) * amp[84] - Complex<double> (0, 1) * amp[85] +
      amp[87]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[0][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_uux_epemguux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 96;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[128] + 1./3. * amp[129] + 1./3. * amp[130] +
      1./3. * amp[131] + amp[132] + amp[133] + 1./3. * amp[134] + 1./3. *
      amp[135] + amp[136] + amp[137] + 1./3. * amp[138] + 1./3. * amp[139] +
      amp[140] + amp[141] + amp[142] + amp[143] + 1./3. * amp[144] + 1./3. *
      amp[145] + 1./3. * amp[146] + 1./3. * amp[147] + amp[148] + amp[149] +
      amp[150] + amp[151] + amp[152] + amp[153] + 1./3. * amp[154] + 1./3. *
      amp[155] + amp[156] + amp[157] + 1./3. * amp[158] + 1./3. * amp[159] +
      1./3. * amp[162] + 1./3. * amp[163] + 1./3. * amp[166] + 1./3. * amp[167]
      + Complex<double> (0, 1) * amp[168] + Complex<double> (0, 1) * amp[169] +
      amp[170] + Complex<double> (0, 1) * amp[172] + Complex<double> (0, 1) *
      amp[173] + amp[174] - Complex<double> (0, 1) * amp[176] - Complex<double>
      (0, 1) * amp[177] + amp[179] - Complex<double> (0, 1) * amp[180] -
      Complex<double> (0, 1) * amp[181] + amp[183]);
  jamp[1] = +1./2. * (-amp[96] - amp[97] - 1./3. * amp[98] - 1./3. * amp[99] -
      amp[100] - amp[101] - 1./3. * amp[102] - 1./3. * amp[103] - 1./3. *
      amp[104] - 1./3. * amp[105] - 1./3. * amp[106] - 1./3. * amp[107] -
      amp[108] - amp[109] - amp[110] - amp[111] - amp[128] - amp[129] -
      amp[130] - amp[131] - 1./3. * amp[132] - 1./3. * amp[133] - amp[134] -
      amp[135] - 1./3. * amp[136] - 1./3. * amp[137] - amp[138] - amp[139] -
      1./3. * amp[140] - 1./3. * amp[141] - 1./3. * amp[142] - 1./3. * amp[143]
      + Complex<double> (0, 1) * amp[160] + Complex<double> (0, 1) * amp[161] -
      amp[163] + Complex<double> (0, 1) * amp[164] + Complex<double> (0, 1) *
      amp[165] - amp[167] - 1./3. * amp[178] - 1./3. * amp[179] - 1./3. *
      amp[182] - 1./3. * amp[183] - Complex<double> (0, 1) * amp[184] -
      Complex<double> (0, 1) * amp[185] - amp[186] - Complex<double> (0, 1) *
      amp[188] - Complex<double> (0, 1) * amp[189] - amp[190]);
  jamp[2] = +1./2. * (+1./3. * amp[96] + 1./3. * amp[97] + amp[98] + amp[99] +
      1./3. * amp[100] + 1./3. * amp[101] + amp[102] + amp[103] + amp[104] +
      amp[105] + amp[106] + amp[107] + 1./3. * amp[108] + 1./3. * amp[109] +
      1./3. * amp[110] + 1./3. * amp[111] + amp[112] + amp[113] + amp[114] +
      amp[115] + 1./3. * amp[116] + 1./3. * amp[117] + amp[118] + amp[119] +
      1./3. * amp[120] + 1./3. * amp[121] + amp[122] + amp[123] + 1./3. *
      amp[124] + 1./3. * amp[125] + 1./3. * amp[126] + 1./3. * amp[127] -
      Complex<double> (0, 1) * amp[168] - Complex<double> (0, 1) * amp[169] +
      amp[171] - Complex<double> (0, 1) * amp[172] - Complex<double> (0, 1) *
      amp[173] + amp[175] + Complex<double> (0, 1) * amp[176] + Complex<double>
      (0, 1) * amp[177] + amp[178] + Complex<double> (0, 1) * amp[180] +
      Complex<double> (0, 1) * amp[181] + amp[182] + 1./3. * amp[186] + 1./3. *
      amp[187] + 1./3. * amp[190] + 1./3. * amp[191]);
  jamp[3] = +1./2. * (-1./3. * amp[112] - 1./3. * amp[113] - 1./3. * amp[114] -
      1./3. * amp[115] - amp[116] - amp[117] - 1./3. * amp[118] - 1./3. *
      amp[119] - amp[120] - amp[121] - 1./3. * amp[122] - 1./3. * amp[123] -
      amp[124] - amp[125] - amp[126] - amp[127] - amp[144] - amp[145] -
      amp[146] - amp[147] - 1./3. * amp[148] - 1./3. * amp[149] - 1./3. *
      amp[150] - 1./3. * amp[151] - 1./3. * amp[152] - 1./3. * amp[153] -
      amp[154] - amp[155] - 1./3. * amp[156] - 1./3. * amp[157] - amp[158] -
      amp[159] - Complex<double> (0, 1) * amp[160] - Complex<double> (0, 1) *
      amp[161] - amp[162] - Complex<double> (0, 1) * amp[164] - Complex<double>
      (0, 1) * amp[165] - amp[166] - 1./3. * amp[170] - 1./3. * amp[171] -
      1./3. * amp[174] - 1./3. * amp[175] + Complex<double> (0, 1) * amp[184] +
      Complex<double> (0, 1) * amp[185] - amp[187] + Complex<double> (0, 1) *
      amp[188] + Complex<double> (0, 1) * amp[189] - amp[191]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[1][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_dd_epemgdd() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 96;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[192] + 1./3. * amp[193] + amp[194] +
      amp[195] + 1./3. * amp[196] + 1./3. * amp[197] + amp[198] + amp[199] +
      amp[200] + amp[201] + amp[202] + amp[203] + 1./3. * amp[204] + 1./3. *
      amp[205] + 1./3. * amp[206] + 1./3. * amp[207] + amp[208] + amp[209] +
      amp[210] + amp[211] + 1./3. * amp[212] + 1./3. * amp[213] + amp[214] +
      amp[215] + 1./3. * amp[216] + 1./3. * amp[217] + amp[218] + amp[219] +
      1./3. * amp[220] + 1./3. * amp[221] + 1./3. * amp[222] + 1./3. * amp[223]
      - Complex<double> (0, 1) * amp[264] - Complex<double> (0, 1) * amp[265] +
      amp[267] - Complex<double> (0, 1) * amp[268] - Complex<double> (0, 1) *
      amp[269] + amp[271] + Complex<double> (0, 1) * amp[272] + Complex<double>
      (0, 1) * amp[273] + amp[274] + Complex<double> (0, 1) * amp[276] +
      Complex<double> (0, 1) * amp[277] + amp[278] + 1./3. * amp[282] + 1./3. *
      amp[283] + 1./3. * amp[286] + 1./3. * amp[287]);
  jamp[1] = +1./2. * (-1./3. * amp[208] - 1./3. * amp[209] - 1./3. * amp[210] -
      1./3. * amp[211] - amp[212] - amp[213] - 1./3. * amp[214] - 1./3. *
      amp[215] - amp[216] - amp[217] - 1./3. * amp[218] - 1./3. * amp[219] -
      amp[220] - amp[221] - amp[222] - amp[223] - amp[240] - amp[241] -
      amp[242] - amp[243] - 1./3. * amp[244] - 1./3. * amp[245] - 1./3. *
      amp[246] - 1./3. * amp[247] - 1./3. * amp[248] - 1./3. * amp[249] -
      amp[250] - amp[251] - 1./3. * amp[252] - 1./3. * amp[253] - amp[254] -
      amp[255] - Complex<double> (0, 1) * amp[256] - Complex<double> (0, 1) *
      amp[257] - amp[258] - Complex<double> (0, 1) * amp[260] - Complex<double>
      (0, 1) * amp[261] - amp[262] - 1./3. * amp[266] - 1./3. * amp[267] -
      1./3. * amp[270] - 1./3. * amp[271] + Complex<double> (0, 1) * amp[280] +
      Complex<double> (0, 1) * amp[281] - amp[283] + Complex<double> (0, 1) *
      amp[284] + Complex<double> (0, 1) * amp[285] - amp[287]);
  jamp[2] = +1./2. * (-amp[192] - amp[193] - 1./3. * amp[194] - 1./3. *
      amp[195] - amp[196] - amp[197] - 1./3. * amp[198] - 1./3. * amp[199] -
      1./3. * amp[200] - 1./3. * amp[201] - 1./3. * amp[202] - 1./3. * amp[203]
      - amp[204] - amp[205] - amp[206] - amp[207] - amp[224] - amp[225] -
      amp[226] - amp[227] - 1./3. * amp[228] - 1./3. * amp[229] - amp[230] -
      amp[231] - 1./3. * amp[232] - 1./3. * amp[233] - amp[234] - amp[235] -
      1./3. * amp[236] - 1./3. * amp[237] - 1./3. * amp[238] - 1./3. * amp[239]
      + Complex<double> (0, 1) * amp[256] + Complex<double> (0, 1) * amp[257] -
      amp[259] + Complex<double> (0, 1) * amp[260] + Complex<double> (0, 1) *
      amp[261] - amp[263] - 1./3. * amp[274] - 1./3. * amp[275] - 1./3. *
      amp[278] - 1./3. * amp[279] - Complex<double> (0, 1) * amp[280] -
      Complex<double> (0, 1) * amp[281] - amp[282] - Complex<double> (0, 1) *
      amp[284] - Complex<double> (0, 1) * amp[285] - amp[286]);
  jamp[3] = +1./2. * (+1./3. * amp[224] + 1./3. * amp[225] + 1./3. * amp[226] +
      1./3. * amp[227] + amp[228] + amp[229] + 1./3. * amp[230] + 1./3. *
      amp[231] + amp[232] + amp[233] + 1./3. * amp[234] + 1./3. * amp[235] +
      amp[236] + amp[237] + amp[238] + amp[239] + 1./3. * amp[240] + 1./3. *
      amp[241] + 1./3. * amp[242] + 1./3. * amp[243] + amp[244] + amp[245] +
      amp[246] + amp[247] + amp[248] + amp[249] + 1./3. * amp[250] + 1./3. *
      amp[251] + amp[252] + amp[253] + 1./3. * amp[254] + 1./3. * amp[255] +
      1./3. * amp[258] + 1./3. * amp[259] + 1./3. * amp[262] + 1./3. * amp[263]
      + Complex<double> (0, 1) * amp[264] + Complex<double> (0, 1) * amp[265] +
      amp[266] + Complex<double> (0, 1) * amp[268] + Complex<double> (0, 1) *
      amp[269] + amp[270] - Complex<double> (0, 1) * amp[272] - Complex<double>
      (0, 1) * amp[273] + amp[275] - Complex<double> (0, 1) * amp[276] -
      Complex<double> (0, 1) * amp[277] + amp[279]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[2][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_ddx_epemgddx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 96;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[320] + 1./3. * amp[321] + 1./3. * amp[322] +
      1./3. * amp[323] + amp[324] + amp[325] + 1./3. * amp[326] + 1./3. *
      amp[327] + amp[328] + amp[329] + 1./3. * amp[330] + 1./3. * amp[331] +
      amp[332] + amp[333] + amp[334] + amp[335] + 1./3. * amp[336] + 1./3. *
      amp[337] + 1./3. * amp[338] + 1./3. * amp[339] + amp[340] + amp[341] +
      amp[342] + amp[343] + amp[344] + amp[345] + 1./3. * amp[346] + 1./3. *
      amp[347] + amp[348] + amp[349] + 1./3. * amp[350] + 1./3. * amp[351] +
      1./3. * amp[354] + 1./3. * amp[355] + 1./3. * amp[358] + 1./3. * amp[359]
      + Complex<double> (0, 1) * amp[360] + Complex<double> (0, 1) * amp[361] +
      amp[362] + Complex<double> (0, 1) * amp[364] + Complex<double> (0, 1) *
      amp[365] + amp[366] - Complex<double> (0, 1) * amp[368] - Complex<double>
      (0, 1) * amp[369] + amp[371] - Complex<double> (0, 1) * amp[372] -
      Complex<double> (0, 1) * amp[373] + amp[375]);
  jamp[1] = +1./2. * (-amp[288] - amp[289] - 1./3. * amp[290] - 1./3. *
      amp[291] - amp[292] - amp[293] - 1./3. * amp[294] - 1./3. * amp[295] -
      1./3. * amp[296] - 1./3. * amp[297] - 1./3. * amp[298] - 1./3. * amp[299]
      - amp[300] - amp[301] - amp[302] - amp[303] - amp[320] - amp[321] -
      amp[322] - amp[323] - 1./3. * amp[324] - 1./3. * amp[325] - amp[326] -
      amp[327] - 1./3. * amp[328] - 1./3. * amp[329] - amp[330] - amp[331] -
      1./3. * amp[332] - 1./3. * amp[333] - 1./3. * amp[334] - 1./3. * amp[335]
      + Complex<double> (0, 1) * amp[352] + Complex<double> (0, 1) * amp[353] -
      amp[355] + Complex<double> (0, 1) * amp[356] + Complex<double> (0, 1) *
      amp[357] - amp[359] - 1./3. * amp[370] - 1./3. * amp[371] - 1./3. *
      amp[374] - 1./3. * amp[375] - Complex<double> (0, 1) * amp[376] -
      Complex<double> (0, 1) * amp[377] - amp[378] - Complex<double> (0, 1) *
      amp[380] - Complex<double> (0, 1) * amp[381] - amp[382]);
  jamp[2] = +1./2. * (+1./3. * amp[288] + 1./3. * amp[289] + amp[290] +
      amp[291] + 1./3. * amp[292] + 1./3. * amp[293] + amp[294] + amp[295] +
      amp[296] + amp[297] + amp[298] + amp[299] + 1./3. * amp[300] + 1./3. *
      amp[301] + 1./3. * amp[302] + 1./3. * amp[303] + amp[304] + amp[305] +
      amp[306] + amp[307] + 1./3. * amp[308] + 1./3. * amp[309] + amp[310] +
      amp[311] + 1./3. * amp[312] + 1./3. * amp[313] + amp[314] + amp[315] +
      1./3. * amp[316] + 1./3. * amp[317] + 1./3. * amp[318] + 1./3. * amp[319]
      - Complex<double> (0, 1) * amp[360] - Complex<double> (0, 1) * amp[361] +
      amp[363] - Complex<double> (0, 1) * amp[364] - Complex<double> (0, 1) *
      amp[365] + amp[367] + Complex<double> (0, 1) * amp[368] + Complex<double>
      (0, 1) * amp[369] + amp[370] + Complex<double> (0, 1) * amp[372] +
      Complex<double> (0, 1) * amp[373] + amp[374] + 1./3. * amp[378] + 1./3. *
      amp[379] + 1./3. * amp[382] + 1./3. * amp[383]);
  jamp[3] = +1./2. * (-1./3. * amp[304] - 1./3. * amp[305] - 1./3. * amp[306] -
      1./3. * amp[307] - amp[308] - amp[309] - 1./3. * amp[310] - 1./3. *
      amp[311] - amp[312] - amp[313] - 1./3. * amp[314] - 1./3. * amp[315] -
      amp[316] - amp[317] - amp[318] - amp[319] - amp[336] - amp[337] -
      amp[338] - amp[339] - 1./3. * amp[340] - 1./3. * amp[341] - 1./3. *
      amp[342] - 1./3. * amp[343] - 1./3. * amp[344] - 1./3. * amp[345] -
      amp[346] - amp[347] - 1./3. * amp[348] - 1./3. * amp[349] - amp[350] -
      amp[351] - Complex<double> (0, 1) * amp[352] - Complex<double> (0, 1) *
      amp[353] - amp[354] - Complex<double> (0, 1) * amp[356] - Complex<double>
      (0, 1) * amp[357] - amp[358] - 1./3. * amp[362] - 1./3. * amp[363] -
      1./3. * amp[366] - 1./3. * amp[367] + Complex<double> (0, 1) * amp[376] +
      Complex<double> (0, 1) * amp[377] - amp[379] + Complex<double> (0, 1) *
      amp[380] + Complex<double> (0, 1) * amp[381] - amp[383]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[3][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_uxux_epemguxux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 96;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[416] + 1./3. * amp[417] + 1./3. * amp[418] +
      1./3. * amp[419] + amp[420] + amp[421] + 1./3. * amp[422] + 1./3. *
      amp[423] + amp[424] + amp[425] + 1./3. * amp[426] + 1./3. * amp[427] +
      amp[428] + amp[429] + amp[430] + amp[431] + 1./3. * amp[432] + 1./3. *
      amp[433] + 1./3. * amp[434] + 1./3. * amp[435] + amp[436] + amp[437] +
      amp[438] + amp[439] + amp[440] + amp[441] + 1./3. * amp[442] + 1./3. *
      amp[443] + amp[444] + amp[445] + 1./3. * amp[446] + 1./3. * amp[447] +
      1./3. * amp[450] + 1./3. * amp[451] + 1./3. * amp[454] + 1./3. * amp[455]
      + Complex<double> (0, 1) * amp[456] + Complex<double> (0, 1) * amp[457] +
      amp[458] + Complex<double> (0, 1) * amp[460] + Complex<double> (0, 1) *
      amp[461] + amp[462] - Complex<double> (0, 1) * amp[464] - Complex<double>
      (0, 1) * amp[465] + amp[467] - Complex<double> (0, 1) * amp[468] -
      Complex<double> (0, 1) * amp[469] + amp[471]);
  jamp[1] = +1./2. * (-amp[384] - amp[385] - 1./3. * amp[386] - 1./3. *
      amp[387] - amp[388] - amp[389] - 1./3. * amp[390] - 1./3. * amp[391] -
      1./3. * amp[392] - 1./3. * amp[393] - 1./3. * amp[394] - 1./3. * amp[395]
      - amp[396] - amp[397] - amp[398] - amp[399] - amp[416] - amp[417] -
      amp[418] - amp[419] - 1./3. * amp[420] - 1./3. * amp[421] - amp[422] -
      amp[423] - 1./3. * amp[424] - 1./3. * amp[425] - amp[426] - amp[427] -
      1./3. * amp[428] - 1./3. * amp[429] - 1./3. * amp[430] - 1./3. * amp[431]
      + Complex<double> (0, 1) * amp[448] + Complex<double> (0, 1) * amp[449] -
      amp[451] + Complex<double> (0, 1) * amp[452] + Complex<double> (0, 1) *
      amp[453] - amp[455] - 1./3. * amp[466] - 1./3. * amp[467] - 1./3. *
      amp[470] - 1./3. * amp[471] - Complex<double> (0, 1) * amp[472] -
      Complex<double> (0, 1) * amp[473] - amp[474] - Complex<double> (0, 1) *
      amp[476] - Complex<double> (0, 1) * amp[477] - amp[478]);
  jamp[2] = +1./2. * (-1./3. * amp[400] - 1./3. * amp[401] - 1./3. * amp[402] -
      1./3. * amp[403] - amp[404] - amp[405] - 1./3. * amp[406] - 1./3. *
      amp[407] - amp[408] - amp[409] - 1./3. * amp[410] - 1./3. * amp[411] -
      amp[412] - amp[413] - amp[414] - amp[415] - amp[432] - amp[433] -
      amp[434] - amp[435] - 1./3. * amp[436] - 1./3. * amp[437] - 1./3. *
      amp[438] - 1./3. * amp[439] - 1./3. * amp[440] - 1./3. * amp[441] -
      amp[442] - amp[443] - 1./3. * amp[444] - 1./3. * amp[445] - amp[446] -
      amp[447] - Complex<double> (0, 1) * amp[448] - Complex<double> (0, 1) *
      amp[449] - amp[450] - Complex<double> (0, 1) * amp[452] - Complex<double>
      (0, 1) * amp[453] - amp[454] - 1./3. * amp[458] - 1./3. * amp[459] -
      1./3. * amp[462] - 1./3. * amp[463] + Complex<double> (0, 1) * amp[472] +
      Complex<double> (0, 1) * amp[473] - amp[475] + Complex<double> (0, 1) *
      amp[476] + Complex<double> (0, 1) * amp[477] - amp[479]);
  jamp[3] = +1./2. * (+1./3. * amp[384] + 1./3. * amp[385] + amp[386] +
      amp[387] + 1./3. * amp[388] + 1./3. * amp[389] + amp[390] + amp[391] +
      amp[392] + amp[393] + amp[394] + amp[395] + 1./3. * amp[396] + 1./3. *
      amp[397] + 1./3. * amp[398] + 1./3. * amp[399] + amp[400] + amp[401] +
      amp[402] + amp[403] + 1./3. * amp[404] + 1./3. * amp[405] + amp[406] +
      amp[407] + 1./3. * amp[408] + 1./3. * amp[409] + amp[410] + amp[411] +
      1./3. * amp[412] + 1./3. * amp[413] + 1./3. * amp[414] + 1./3. * amp[415]
      - Complex<double> (0, 1) * amp[456] - Complex<double> (0, 1) * amp[457] +
      amp[459] - Complex<double> (0, 1) * amp[460] - Complex<double> (0, 1) *
      amp[461] + amp[463] + Complex<double> (0, 1) * amp[464] + Complex<double>
      (0, 1) * amp[465] + amp[466] + Complex<double> (0, 1) * amp[468] +
      Complex<double> (0, 1) * amp[469] + amp[470] + 1./3. * amp[474] + 1./3. *
      amp[475] + 1./3. * amp[478] + 1./3. * amp[479]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[4][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_dxdx_epemgdxdx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 96;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[512] + 1./3. * amp[513] + 1./3. * amp[514] +
      1./3. * amp[515] + amp[516] + amp[517] + 1./3. * amp[518] + 1./3. *
      amp[519] + amp[520] + amp[521] + 1./3. * amp[522] + 1./3. * amp[523] +
      amp[524] + amp[525] + amp[526] + amp[527] + 1./3. * amp[528] + 1./3. *
      amp[529] + 1./3. * amp[530] + 1./3. * amp[531] + amp[532] + amp[533] +
      amp[534] + amp[535] + amp[536] + amp[537] + 1./3. * amp[538] + 1./3. *
      amp[539] + amp[540] + amp[541] + 1./3. * amp[542] + 1./3. * amp[543] +
      1./3. * amp[546] + 1./3. * amp[547] + 1./3. * amp[550] + 1./3. * amp[551]
      + Complex<double> (0, 1) * amp[552] + Complex<double> (0, 1) * amp[553] +
      amp[554] + Complex<double> (0, 1) * amp[556] + Complex<double> (0, 1) *
      amp[557] + amp[558] - Complex<double> (0, 1) * amp[560] - Complex<double>
      (0, 1) * amp[561] + amp[563] - Complex<double> (0, 1) * amp[564] -
      Complex<double> (0, 1) * amp[565] + amp[567]);
  jamp[1] = +1./2. * (-amp[480] - amp[481] - 1./3. * amp[482] - 1./3. *
      amp[483] - amp[484] - amp[485] - 1./3. * amp[486] - 1./3. * amp[487] -
      1./3. * amp[488] - 1./3. * amp[489] - 1./3. * amp[490] - 1./3. * amp[491]
      - amp[492] - amp[493] - amp[494] - amp[495] - amp[512] - amp[513] -
      amp[514] - amp[515] - 1./3. * amp[516] - 1./3. * amp[517] - amp[518] -
      amp[519] - 1./3. * amp[520] - 1./3. * amp[521] - amp[522] - amp[523] -
      1./3. * amp[524] - 1./3. * amp[525] - 1./3. * amp[526] - 1./3. * amp[527]
      + Complex<double> (0, 1) * amp[544] + Complex<double> (0, 1) * amp[545] -
      amp[547] + Complex<double> (0, 1) * amp[548] + Complex<double> (0, 1) *
      amp[549] - amp[551] - 1./3. * amp[562] - 1./3. * amp[563] - 1./3. *
      amp[566] - 1./3. * amp[567] - Complex<double> (0, 1) * amp[568] -
      Complex<double> (0, 1) * amp[569] - amp[570] - Complex<double> (0, 1) *
      amp[572] - Complex<double> (0, 1) * amp[573] - amp[574]);
  jamp[2] = +1./2. * (-1./3. * amp[496] - 1./3. * amp[497] - 1./3. * amp[498] -
      1./3. * amp[499] - amp[500] - amp[501] - 1./3. * amp[502] - 1./3. *
      amp[503] - amp[504] - amp[505] - 1./3. * amp[506] - 1./3. * amp[507] -
      amp[508] - amp[509] - amp[510] - amp[511] - amp[528] - amp[529] -
      amp[530] - amp[531] - 1./3. * amp[532] - 1./3. * amp[533] - 1./3. *
      amp[534] - 1./3. * amp[535] - 1./3. * amp[536] - 1./3. * amp[537] -
      amp[538] - amp[539] - 1./3. * amp[540] - 1./3. * amp[541] - amp[542] -
      amp[543] - Complex<double> (0, 1) * amp[544] - Complex<double> (0, 1) *
      amp[545] - amp[546] - Complex<double> (0, 1) * amp[548] - Complex<double>
      (0, 1) * amp[549] - amp[550] - 1./3. * amp[554] - 1./3. * amp[555] -
      1./3. * amp[558] - 1./3. * amp[559] + Complex<double> (0, 1) * amp[568] +
      Complex<double> (0, 1) * amp[569] - amp[571] + Complex<double> (0, 1) *
      amp[572] + Complex<double> (0, 1) * amp[573] - amp[575]);
  jamp[3] = +1./2. * (+1./3. * amp[480] + 1./3. * amp[481] + amp[482] +
      amp[483] + 1./3. * amp[484] + 1./3. * amp[485] + amp[486] + amp[487] +
      amp[488] + amp[489] + amp[490] + amp[491] + 1./3. * amp[492] + 1./3. *
      amp[493] + 1./3. * amp[494] + 1./3. * amp[495] + amp[496] + amp[497] +
      amp[498] + amp[499] + 1./3. * amp[500] + 1./3. * amp[501] + amp[502] +
      amp[503] + 1./3. * amp[504] + 1./3. * amp[505] + amp[506] + amp[507] +
      1./3. * amp[508] + 1./3. * amp[509] + 1./3. * amp[510] + 1./3. * amp[511]
      - Complex<double> (0, 1) * amp[552] - Complex<double> (0, 1) * amp[553] +
      amp[555] - Complex<double> (0, 1) * amp[556] - Complex<double> (0, 1) *
      amp[557] + amp[559] + Complex<double> (0, 1) * amp[560] + Complex<double>
      (0, 1) * amp[561] + amp[562] + Complex<double> (0, 1) * amp[564] +
      Complex<double> (0, 1) * amp[565] + amp[566] + 1./3. * amp[570] + 1./3. *
      amp[571] + 1./3. * amp[574] + 1./3. * amp[575]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[5][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_uu_vexveguu() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 48;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[576] + 1./3. * amp[577] + amp[578] +
      amp[579] + amp[580] + amp[581] + 1./3. * amp[582] + 1./3. * amp[583] +
      amp[584] + amp[585] + 1./3. * amp[586] + 1./3. * amp[587] + amp[588] +
      amp[589] + 1./3. * amp[590] + 1./3. * amp[591] - Complex<double> (0, 1) *
      amp[612] - Complex<double> (0, 1) * amp[613] + amp[615] + Complex<double>
      (0, 1) * amp[616] + Complex<double> (0, 1) * amp[617] + amp[618] + 1./3.
      * amp[622] + 1./3. * amp[623]);
  jamp[1] = +1./2. * (-1./3. * amp[584] - 1./3. * amp[585] - amp[586] -
      amp[587] - 1./3. * amp[588] - 1./3. * amp[589] - amp[590] - amp[591] -
      amp[600] - amp[601] - 1./3. * amp[602] - 1./3. * amp[603] - 1./3. *
      amp[604] - 1./3. * amp[605] - amp[606] - amp[607] - Complex<double> (0,
      1) * amp[608] - Complex<double> (0, 1) * amp[609] - amp[610] - 1./3. *
      amp[614] - 1./3. * amp[615] + Complex<double> (0, 1) * amp[620] +
      Complex<double> (0, 1) * amp[621] - amp[623]);
  jamp[2] = +1./2. * (-amp[576] - amp[577] - 1./3. * amp[578] - 1./3. *
      amp[579] - 1./3. * amp[580] - 1./3. * amp[581] - amp[582] - amp[583] -
      amp[592] - amp[593] - 1./3. * amp[594] - 1./3. * amp[595] - amp[596] -
      amp[597] - 1./3. * amp[598] - 1./3. * amp[599] + Complex<double> (0, 1) *
      amp[608] + Complex<double> (0, 1) * amp[609] - amp[611] - 1./3. *
      amp[618] - 1./3. * amp[619] - Complex<double> (0, 1) * amp[620] -
      Complex<double> (0, 1) * amp[621] - amp[622]);
  jamp[3] = +1./2. * (+1./3. * amp[592] + 1./3. * amp[593] + amp[594] +
      amp[595] + 1./3. * amp[596] + 1./3. * amp[597] + amp[598] + amp[599] +
      1./3. * amp[600] + 1./3. * amp[601] + amp[602] + amp[603] + amp[604] +
      amp[605] + 1./3. * amp[606] + 1./3. * amp[607] + 1./3. * amp[610] + 1./3.
      * amp[611] + Complex<double> (0, 1) * amp[612] + Complex<double> (0, 1) *
      amp[613] + amp[614] - Complex<double> (0, 1) * amp[616] - Complex<double>
      (0, 1) * amp[617] + amp[619]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[6][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_uc_epemguc() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 48;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[0] + 1./3. * amp[1] + 1./3. * amp[624] +
      1./3. * amp[625] + 1./3. * amp[626] + 1./3. * amp[627] + 1./3. * amp[628]
      + 1./3. * amp[629] + 1./3. * amp[630] + 1./3. * amp[631] + 1./3. *
      amp[632] + 1./3. * amp[633] + 1./3. * amp[634] + 1./3. * amp[635] + 1./3.
      * amp[636] + 1./3. * amp[637] + 1./3. * amp[664] + 1./3. * amp[665] +
      1./3. * amp[668] + 1./3. * amp[669]);
  jamp[1] = +1./2. * (-amp[630] - amp[631] - amp[632] - amp[633] - amp[634] -
      amp[635] - amp[636] - amp[637] - amp[646] - amp[647] - amp[648] -
      amp[649] - amp[650] - amp[651] - amp[652] - amp[653] - Complex<double>
      (0, 1) * amp[654] - Complex<double> (0, 1) * amp[655] - amp[656] -
      Complex<double> (0, 1) * amp[658] - Complex<double> (0, 1) * amp[659] -
      amp[660] + Complex<double> (0, 1) * amp[662] + Complex<double> (0, 1) *
      amp[663] - amp[665] + Complex<double> (0, 1) * amp[666] + Complex<double>
      (0, 1) * amp[667] - amp[669]);
  jamp[2] = +1./2. * (-amp[0] - amp[1] - amp[624] - amp[625] - amp[626] -
      amp[627] - amp[628] - amp[629] - amp[638] - amp[639] - amp[640] -
      amp[641] - amp[642] - amp[643] - amp[644] - amp[645] + Complex<double>
      (0, 1) * amp[654] + Complex<double> (0, 1) * amp[655] - amp[657] +
      Complex<double> (0, 1) * amp[658] + Complex<double> (0, 1) * amp[659] -
      amp[661] - Complex<double> (0, 1) * amp[662] - Complex<double> (0, 1) *
      amp[663] - amp[664] - Complex<double> (0, 1) * amp[666] - Complex<double>
      (0, 1) * amp[667] - amp[668]);
  jamp[3] = +1./2. * (+1./3. * amp[638] + 1./3. * amp[639] + 1./3. * amp[640] +
      1./3. * amp[641] + 1./3. * amp[642] + 1./3. * amp[643] + 1./3. * amp[644]
      + 1./3. * amp[645] + 1./3. * amp[646] + 1./3. * amp[647] + 1./3. *
      amp[648] + 1./3. * amp[649] + 1./3. * amp[650] + 1./3. * amp[651] + 1./3.
      * amp[652] + 1./3. * amp[653] + 1./3. * amp[656] + 1./3. * amp[657] +
      1./3. * amp[660] + 1./3. * amp[661]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[7][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_ud_epemgud() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 48;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[670] + 1./3. * amp[671] + 1./3. * amp[672] +
      1./3. * amp[673] + 1./3. * amp[674] + 1./3. * amp[675] + 1./3. * amp[676]
      + 1./3. * amp[677] + 1./3. * amp[678] + 1./3. * amp[679] + 1./3. *
      amp[680] + 1./3. * amp[681] + 1./3. * amp[682] + 1./3. * amp[683] + 1./3.
      * amp[684] + 1./3. * amp[685] + 1./3. * amp[712] + 1./3. * amp[713] +
      1./3. * amp[716] + 1./3. * amp[717]);
  jamp[1] = +1./2. * (-amp[678] - amp[679] - amp[680] - amp[681] - amp[682] -
      amp[683] - amp[684] - amp[685] - amp[694] - amp[695] - amp[696] -
      amp[697] - amp[698] - amp[699] - amp[700] - amp[701] - Complex<double>
      (0, 1) * amp[702] - Complex<double> (0, 1) * amp[703] - amp[704] -
      Complex<double> (0, 1) * amp[706] - Complex<double> (0, 1) * amp[707] -
      amp[708] + Complex<double> (0, 1) * amp[710] + Complex<double> (0, 1) *
      amp[711] - amp[713] + Complex<double> (0, 1) * amp[714] + Complex<double>
      (0, 1) * amp[715] - amp[717]);
  jamp[2] = +1./2. * (-amp[670] - amp[671] - amp[672] - amp[673] - amp[674] -
      amp[675] - amp[676] - amp[677] - amp[686] - amp[687] - amp[688] -
      amp[689] - amp[690] - amp[691] - amp[692] - amp[693] + Complex<double>
      (0, 1) * amp[702] + Complex<double> (0, 1) * amp[703] - amp[705] +
      Complex<double> (0, 1) * amp[706] + Complex<double> (0, 1) * amp[707] -
      amp[709] - Complex<double> (0, 1) * amp[710] - Complex<double> (0, 1) *
      amp[711] - amp[712] - Complex<double> (0, 1) * amp[714] - Complex<double>
      (0, 1) * amp[715] - amp[716]);
  jamp[3] = +1./2. * (+1./3. * amp[686] + 1./3. * amp[687] + 1./3. * amp[688] +
      1./3. * amp[689] + 1./3. * amp[690] + 1./3. * amp[691] + 1./3. * amp[692]
      + 1./3. * amp[693] + 1./3. * amp[694] + 1./3. * amp[695] + 1./3. *
      amp[696] + 1./3. * amp[697] + 1./3. * amp[698] + 1./3. * amp[699] + 1./3.
      * amp[700] + 1./3. * amp[701] + 1./3. * amp[704] + 1./3. * amp[705] +
      1./3. * amp[708] + 1./3. * amp[709]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[8][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_uux_epemgccx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 48;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[734] + 1./3. * amp[735] + 1./3. * amp[736] +
      1./3. * amp[737] + 1./3. * amp[738] + 1./3. * amp[739] + 1./3. * amp[740]
      + 1./3. * amp[741] + 1./3. * amp[742] + 1./3. * amp[743] + 1./3. *
      amp[744] + 1./3. * amp[745] + 1./3. * amp[746] + 1./3. * amp[747] + 1./3.
      * amp[748] + 1./3. * amp[749] + 1./3. * amp[752] + 1./3. * amp[753] +
      1./3. * amp[756] + 1./3. * amp[757]);
  jamp[1] = +1./2. * (-amp[718] - amp[719] - amp[720] - amp[721] - amp[722] -
      amp[723] - amp[724] - amp[725] - amp[734] - amp[735] - amp[736] -
      amp[737] - amp[738] - amp[739] - amp[740] - amp[741] + Complex<double>
      (0, 1) * amp[750] + Complex<double> (0, 1) * amp[751] - amp[753] +
      Complex<double> (0, 1) * amp[754] + Complex<double> (0, 1) * amp[755] -
      amp[757] - Complex<double> (0, 1) * amp[758] - Complex<double> (0, 1) *
      amp[759] - amp[760] - Complex<double> (0, 1) * amp[762] - Complex<double>
      (0, 1) * amp[763] - amp[764]);
  jamp[2] = +1./2. * (+1./3. * amp[718] + 1./3. * amp[719] + 1./3. * amp[720] +
      1./3. * amp[721] + 1./3. * amp[722] + 1./3. * amp[723] + 1./3. * amp[724]
      + 1./3. * amp[725] + 1./3. * amp[726] + 1./3. * amp[727] + 1./3. *
      amp[728] + 1./3. * amp[729] + 1./3. * amp[730] + 1./3. * amp[731] + 1./3.
      * amp[732] + 1./3. * amp[733] + 1./3. * amp[760] + 1./3. * amp[761] +
      1./3. * amp[764] + 1./3. * amp[765]);
  jamp[3] = +1./2. * (-amp[726] - amp[727] - amp[728] - amp[729] - amp[730] -
      amp[731] - amp[732] - amp[733] - amp[742] - amp[743] - amp[744] -
      amp[745] - amp[746] - amp[747] - amp[748] - amp[749] - Complex<double>
      (0, 1) * amp[750] - Complex<double> (0, 1) * amp[751] - amp[752] -
      Complex<double> (0, 1) * amp[754] - Complex<double> (0, 1) * amp[755] -
      amp[756] + Complex<double> (0, 1) * amp[758] + Complex<double> (0, 1) *
      amp[759] - amp[761] + Complex<double> (0, 1) * amp[762] + Complex<double>
      (0, 1) * amp[763] - amp[765]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[9][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_uux_epemgddx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 48;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[782] + 1./3. * amp[783] + 1./3. * amp[784] +
      1./3. * amp[785] + 1./3. * amp[786] + 1./3. * amp[787] + 1./3. * amp[788]
      + 1./3. * amp[789] + 1./3. * amp[790] + 1./3. * amp[791] + 1./3. *
      amp[792] + 1./3. * amp[793] + 1./3. * amp[794] + 1./3. * amp[795] + 1./3.
      * amp[796] + 1./3. * amp[797] + 1./3. * amp[800] + 1./3. * amp[801] +
      1./3. * amp[804] + 1./3. * amp[805]);
  jamp[1] = +1./2. * (-amp[766] - amp[767] - amp[768] - amp[769] - amp[770] -
      amp[771] - amp[772] - amp[773] - amp[782] - amp[783] - amp[784] -
      amp[785] - amp[786] - amp[787] - amp[788] - amp[789] + Complex<double>
      (0, 1) * amp[798] + Complex<double> (0, 1) * amp[799] - amp[801] +
      Complex<double> (0, 1) * amp[802] + Complex<double> (0, 1) * amp[803] -
      amp[805] - Complex<double> (0, 1) * amp[806] - Complex<double> (0, 1) *
      amp[807] - amp[808] - Complex<double> (0, 1) * amp[810] - Complex<double>
      (0, 1) * amp[811] - amp[812]);
  jamp[2] = +1./2. * (+1./3. * amp[766] + 1./3. * amp[767] + 1./3. * amp[768] +
      1./3. * amp[769] + 1./3. * amp[770] + 1./3. * amp[771] + 1./3. * amp[772]
      + 1./3. * amp[773] + 1./3. * amp[774] + 1./3. * amp[775] + 1./3. *
      amp[776] + 1./3. * amp[777] + 1./3. * amp[778] + 1./3. * amp[779] + 1./3.
      * amp[780] + 1./3. * amp[781] + 1./3. * amp[808] + 1./3. * amp[809] +
      1./3. * amp[812] + 1./3. * amp[813]);
  jamp[3] = +1./2. * (-amp[774] - amp[775] - amp[776] - amp[777] - amp[778] -
      amp[779] - amp[780] - amp[781] - amp[790] - amp[791] - amp[792] -
      amp[793] - amp[794] - amp[795] - amp[796] - amp[797] - Complex<double>
      (0, 1) * amp[798] - Complex<double> (0, 1) * amp[799] - amp[800] -
      Complex<double> (0, 1) * amp[802] - Complex<double> (0, 1) * amp[803] -
      amp[804] + Complex<double> (0, 1) * amp[806] + Complex<double> (0, 1) *
      amp[807] - amp[809] + Complex<double> (0, 1) * amp[810] + Complex<double>
      (0, 1) * amp[811] - amp[813]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[10][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_uux_vexveguux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 48;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[830] + 1./3. * amp[831] + amp[832] +
      amp[833] + 1./3. * amp[834] + 1./3. * amp[835] + amp[836] + amp[837] +
      1./3. * amp[838] + 1./3. * amp[839] + amp[840] + amp[841] + amp[842] +
      amp[843] + 1./3. * amp[844] + 1./3. * amp[845] + 1./3. * amp[848] + 1./3.
      * amp[849] + Complex<double> (0, 1) * amp[850] + Complex<double> (0, 1) *
      amp[851] + amp[852] - Complex<double> (0, 1) * amp[854] - Complex<double>
      (0, 1) * amp[855] + amp[857]);
  jamp[1] = +1./2. * (-amp[814] - amp[815] - 1./3. * amp[816] - 1./3. *
      amp[817] - 1./3. * amp[818] - 1./3. * amp[819] - amp[820] - amp[821] -
      amp[830] - amp[831] - 1./3. * amp[832] - 1./3. * amp[833] - amp[834] -
      amp[835] - 1./3. * amp[836] - 1./3. * amp[837] + Complex<double> (0, 1) *
      amp[846] + Complex<double> (0, 1) * amp[847] - amp[849] - 1./3. *
      amp[856] - 1./3. * amp[857] - Complex<double> (0, 1) * amp[858] -
      Complex<double> (0, 1) * amp[859] - amp[860]);
  jamp[2] = +1./2. * (+1./3. * amp[814] + 1./3. * amp[815] + amp[816] +
      amp[817] + amp[818] + amp[819] + 1./3. * amp[820] + 1./3. * amp[821] +
      amp[822] + amp[823] + 1./3. * amp[824] + 1./3. * amp[825] + amp[826] +
      amp[827] + 1./3. * amp[828] + 1./3. * amp[829] - Complex<double> (0, 1) *
      amp[850] - Complex<double> (0, 1) * amp[851] + amp[853] + Complex<double>
      (0, 1) * amp[854] + Complex<double> (0, 1) * amp[855] + amp[856] + 1./3.
      * amp[860] + 1./3. * amp[861]);
  jamp[3] = +1./2. * (-1./3. * amp[822] - 1./3. * amp[823] - amp[824] -
      amp[825] - 1./3. * amp[826] - 1./3. * amp[827] - amp[828] - amp[829] -
      amp[838] - amp[839] - 1./3. * amp[840] - 1./3. * amp[841] - 1./3. *
      amp[842] - 1./3. * amp[843] - amp[844] - amp[845] - Complex<double> (0,
      1) * amp[846] - Complex<double> (0, 1) * amp[847] - amp[848] - 1./3. *
      amp[852] - 1./3. * amp[853] + Complex<double> (0, 1) * amp[858] +
      Complex<double> (0, 1) * amp[859] - amp[861]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[11][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_ucx_epemgucx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 48;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[870] + amp[871] + amp[872] + amp[873] + amp[874] +
      amp[875] + amp[876] + amp[877] + amp[886] + amp[887] + amp[888] +
      amp[889] + amp[890] + amp[891] + amp[892] + amp[893] + Complex<double>
      (0, 1) * amp[894] + Complex<double> (0, 1) * amp[895] + amp[896] +
      Complex<double> (0, 1) * amp[898] + Complex<double> (0, 1) * amp[899] +
      amp[900] - Complex<double> (0, 1) * amp[902] - Complex<double> (0, 1) *
      amp[903] + amp[905] - Complex<double> (0, 1) * amp[906] - Complex<double>
      (0, 1) * amp[907] + amp[909]);
  jamp[1] = +1./2. * (-1./3. * amp[862] - 1./3. * amp[863] - 1./3. * amp[864] -
      1./3. * amp[865] - 1./3. * amp[866] - 1./3. * amp[867] - 1./3. * amp[868]
      - 1./3. * amp[869] - 1./3. * amp[870] - 1./3. * amp[871] - 1./3. *
      amp[872] - 1./3. * amp[873] - 1./3. * amp[874] - 1./3. * amp[875] - 1./3.
      * amp[876] - 1./3. * amp[877] - 1./3. * amp[904] - 1./3. * amp[905] -
      1./3. * amp[908] - 1./3. * amp[909]);
  jamp[2] = +1./2. * (+amp[862] + amp[863] + amp[864] + amp[865] + amp[866] +
      amp[867] + amp[868] + amp[869] + amp[878] + amp[879] + amp[880] +
      amp[881] + amp[882] + amp[883] + amp[884] + amp[885] - Complex<double>
      (0, 1) * amp[894] - Complex<double> (0, 1) * amp[895] + amp[897] -
      Complex<double> (0, 1) * amp[898] - Complex<double> (0, 1) * amp[899] +
      amp[901] + Complex<double> (0, 1) * amp[902] + Complex<double> (0, 1) *
      amp[903] + amp[904] + Complex<double> (0, 1) * amp[906] + Complex<double>
      (0, 1) * amp[907] + amp[908]);
  jamp[3] = +1./2. * (-1./3. * amp[878] - 1./3. * amp[879] - 1./3. * amp[880] -
      1./3. * amp[881] - 1./3. * amp[882] - 1./3. * amp[883] - 1./3. * amp[884]
      - 1./3. * amp[885] - 1./3. * amp[886] - 1./3. * amp[887] - 1./3. *
      amp[888] - 1./3. * amp[889] - 1./3. * amp[890] - 1./3. * amp[891] - 1./3.
      * amp[892] - 1./3. * amp[893] - 1./3. * amp[896] - 1./3. * amp[897] -
      1./3. * amp[900] - 1./3. * amp[901]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[12][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_udx_epemgudx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 48;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[918] + amp[919] + amp[920] + amp[921] + amp[922] +
      amp[923] + amp[924] + amp[925] + amp[934] + amp[935] + amp[936] +
      amp[937] + amp[938] + amp[939] + amp[940] + amp[941] + Complex<double>
      (0, 1) * amp[942] + Complex<double> (0, 1) * amp[943] + amp[944] +
      Complex<double> (0, 1) * amp[946] + Complex<double> (0, 1) * amp[947] +
      amp[948] - Complex<double> (0, 1) * amp[950] - Complex<double> (0, 1) *
      amp[951] + amp[953] - Complex<double> (0, 1) * amp[954] - Complex<double>
      (0, 1) * amp[955] + amp[957]);
  jamp[1] = +1./2. * (-1./3. * amp[910] - 1./3. * amp[911] - 1./3. * amp[912] -
      1./3. * amp[913] - 1./3. * amp[914] - 1./3. * amp[915] - 1./3. * amp[916]
      - 1./3. * amp[917] - 1./3. * amp[918] - 1./3. * amp[919] - 1./3. *
      amp[920] - 1./3. * amp[921] - 1./3. * amp[922] - 1./3. * amp[923] - 1./3.
      * amp[924] - 1./3. * amp[925] - 1./3. * amp[952] - 1./3. * amp[953] -
      1./3. * amp[956] - 1./3. * amp[957]);
  jamp[2] = +1./2. * (+amp[910] + amp[911] + amp[912] + amp[913] + amp[914] +
      amp[915] + amp[916] + amp[917] + amp[926] + amp[927] + amp[928] +
      amp[929] + amp[930] + amp[931] + amp[932] + amp[933] - Complex<double>
      (0, 1) * amp[942] - Complex<double> (0, 1) * amp[943] + amp[945] -
      Complex<double> (0, 1) * amp[946] - Complex<double> (0, 1) * amp[947] +
      amp[949] + Complex<double> (0, 1) * amp[950] + Complex<double> (0, 1) *
      amp[951] + amp[952] + Complex<double> (0, 1) * amp[954] + Complex<double>
      (0, 1) * amp[955] + amp[956]);
  jamp[3] = +1./2. * (-1./3. * amp[926] - 1./3. * amp[927] - 1./3. * amp[928] -
      1./3. * amp[929] - 1./3. * amp[930] - 1./3. * amp[931] - 1./3. * amp[932]
      - 1./3. * amp[933] - 1./3. * amp[934] - 1./3. * amp[935] - 1./3. *
      amp[936] - 1./3. * amp[937] - 1./3. * amp[938] - 1./3. * amp[939] - 1./3.
      * amp[940] - 1./3. * amp[941] - 1./3. * amp[944] - 1./3. * amp[945] -
      1./3. * amp[948] - 1./3. * amp[949]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[13][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_dd_vexvegdd() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 48;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[958] + 1./3. * amp[959] + amp[960] +
      amp[961] + amp[962] + amp[963] + 1./3. * amp[964] + 1./3. * amp[965] +
      amp[966] + amp[967] + 1./3. * amp[968] + 1./3. * amp[969] + amp[970] +
      amp[971] + 1./3. * amp[972] + 1./3. * amp[973] - Complex<double> (0, 1) *
      amp[994] - Complex<double> (0, 1) * amp[995] + amp[997] + Complex<double>
      (0, 1) * amp[998] + Complex<double> (0, 1) * amp[999] + amp[1000] + 1./3.
      * amp[1004] + 1./3. * amp[1005]);
  jamp[1] = +1./2. * (-1./3. * amp[966] - 1./3. * amp[967] - amp[968] -
      amp[969] - 1./3. * amp[970] - 1./3. * amp[971] - amp[972] - amp[973] -
      amp[982] - amp[983] - 1./3. * amp[984] - 1./3. * amp[985] - 1./3. *
      amp[986] - 1./3. * amp[987] - amp[988] - amp[989] - Complex<double> (0,
      1) * amp[990] - Complex<double> (0, 1) * amp[991] - amp[992] - 1./3. *
      amp[996] - 1./3. * amp[997] + Complex<double> (0, 1) * amp[1002] +
      Complex<double> (0, 1) * amp[1003] - amp[1005]);
  jamp[2] = +1./2. * (-amp[958] - amp[959] - 1./3. * amp[960] - 1./3. *
      amp[961] - 1./3. * amp[962] - 1./3. * amp[963] - amp[964] - amp[965] -
      amp[974] - amp[975] - 1./3. * amp[976] - 1./3. * amp[977] - amp[978] -
      amp[979] - 1./3. * amp[980] - 1./3. * amp[981] + Complex<double> (0, 1) *
      amp[990] + Complex<double> (0, 1) * amp[991] - amp[993] - 1./3. *
      amp[1000] - 1./3. * amp[1001] - Complex<double> (0, 1) * amp[1002] -
      Complex<double> (0, 1) * amp[1003] - amp[1004]);
  jamp[3] = +1./2. * (+1./3. * amp[974] + 1./3. * amp[975] + amp[976] +
      amp[977] + 1./3. * amp[978] + 1./3. * amp[979] + amp[980] + amp[981] +
      1./3. * amp[982] + 1./3. * amp[983] + amp[984] + amp[985] + amp[986] +
      amp[987] + 1./3. * amp[988] + 1./3. * amp[989] + 1./3. * amp[992] + 1./3.
      * amp[993] + Complex<double> (0, 1) * amp[994] + Complex<double> (0, 1) *
      amp[995] + amp[996] - Complex<double> (0, 1) * amp[998] - Complex<double>
      (0, 1) * amp[999] + amp[1001]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[14][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_ds_epemgds() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 48;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[1006] + 1./3. * amp[1007] + 1./3. *
      amp[1008] + 1./3. * amp[1009] + 1./3. * amp[1010] + 1./3. * amp[1011] +
      1./3. * amp[1012] + 1./3. * amp[1013] + 1./3. * amp[1014] + 1./3. *
      amp[1015] + 1./3. * amp[1016] + 1./3. * amp[1017] + 1./3. * amp[1018] +
      1./3. * amp[1019] + 1./3. * amp[1020] + 1./3. * amp[1021] + 1./3. *
      amp[1048] + 1./3. * amp[1049] + 1./3. * amp[1052] + 1./3. * amp[1053]);
  jamp[1] = +1./2. * (-amp[1014] - amp[1015] - amp[1016] - amp[1017] -
      amp[1018] - amp[1019] - amp[1020] - amp[1021] - amp[1030] - amp[1031] -
      amp[1032] - amp[1033] - amp[1034] - amp[1035] - amp[1036] - amp[1037] -
      Complex<double> (0, 1) * amp[1038] - Complex<double> (0, 1) * amp[1039] -
      amp[1040] - Complex<double> (0, 1) * amp[1042] - Complex<double> (0, 1) *
      amp[1043] - amp[1044] + Complex<double> (0, 1) * amp[1046] +
      Complex<double> (0, 1) * amp[1047] - amp[1049] + Complex<double> (0, 1) *
      amp[1050] + Complex<double> (0, 1) * amp[1051] - amp[1053]);
  jamp[2] = +1./2. * (-amp[1006] - amp[1007] - amp[1008] - amp[1009] -
      amp[1010] - amp[1011] - amp[1012] - amp[1013] - amp[1022] - amp[1023] -
      amp[1024] - amp[1025] - amp[1026] - amp[1027] - amp[1028] - amp[1029] +
      Complex<double> (0, 1) * amp[1038] + Complex<double> (0, 1) * amp[1039] -
      amp[1041] + Complex<double> (0, 1) * amp[1042] + Complex<double> (0, 1) *
      amp[1043] - amp[1045] - Complex<double> (0, 1) * amp[1046] -
      Complex<double> (0, 1) * amp[1047] - amp[1048] - Complex<double> (0, 1) *
      amp[1050] - Complex<double> (0, 1) * amp[1051] - amp[1052]);
  jamp[3] = +1./2. * (+1./3. * amp[1022] + 1./3. * amp[1023] + 1./3. *
      amp[1024] + 1./3. * amp[1025] + 1./3. * amp[1026] + 1./3. * amp[1027] +
      1./3. * amp[1028] + 1./3. * amp[1029] + 1./3. * amp[1030] + 1./3. *
      amp[1031] + 1./3. * amp[1032] + 1./3. * amp[1033] + 1./3. * amp[1034] +
      1./3. * amp[1035] + 1./3. * amp[1036] + 1./3. * amp[1037] + 1./3. *
      amp[1040] + 1./3. * amp[1041] + 1./3. * amp[1044] + 1./3. * amp[1045]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[15][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_dux_epemgdux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 48;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[1054] + amp[1055] + amp[1056] + amp[1057] +
      amp[1058] + amp[1059] + amp[1060] + amp[1061] + amp[1070] + amp[1071] +
      amp[1072] + amp[1073] + amp[1074] + amp[1075] + amp[1076] + amp[1077] -
      Complex<double> (0, 1) * amp[1086] - Complex<double> (0, 1) * amp[1087] +
      amp[1089] - Complex<double> (0, 1) * amp[1090] - Complex<double> (0, 1) *
      amp[1091] + amp[1093] + Complex<double> (0, 1) * amp[1094] +
      Complex<double> (0, 1) * amp[1095] + amp[1096] + Complex<double> (0, 1) *
      amp[1098] + Complex<double> (0, 1) * amp[1099] + amp[1100]);
  jamp[1] = +1./2. * (-1./3. * amp[1070] - 1./3. * amp[1071] - 1./3. *
      amp[1072] - 1./3. * amp[1073] - 1./3. * amp[1074] - 1./3. * amp[1075] -
      1./3. * amp[1076] - 1./3. * amp[1077] - 1./3. * amp[1078] - 1./3. *
      amp[1079] - 1./3. * amp[1080] - 1./3. * amp[1081] - 1./3. * amp[1082] -
      1./3. * amp[1083] - 1./3. * amp[1084] - 1./3. * amp[1085] - 1./3. *
      amp[1088] - 1./3. * amp[1089] - 1./3. * amp[1092] - 1./3. * amp[1093]);
  jamp[2] = +1./2. * (+amp[1062] + amp[1063] + amp[1064] + amp[1065] +
      amp[1066] + amp[1067] + amp[1068] + amp[1069] + amp[1078] + amp[1079] +
      amp[1080] + amp[1081] + amp[1082] + amp[1083] + amp[1084] + amp[1085] +
      Complex<double> (0, 1) * amp[1086] + Complex<double> (0, 1) * amp[1087] +
      amp[1088] + Complex<double> (0, 1) * amp[1090] + Complex<double> (0, 1) *
      amp[1091] + amp[1092] - Complex<double> (0, 1) * amp[1094] -
      Complex<double> (0, 1) * amp[1095] + amp[1097] - Complex<double> (0, 1) *
      amp[1098] - Complex<double> (0, 1) * amp[1099] + amp[1101]);
  jamp[3] = +1./2. * (-1./3. * amp[1054] - 1./3. * amp[1055] - 1./3. *
      amp[1056] - 1./3. * amp[1057] - 1./3. * amp[1058] - 1./3. * amp[1059] -
      1./3. * amp[1060] - 1./3. * amp[1061] - 1./3. * amp[1062] - 1./3. *
      amp[1063] - 1./3. * amp[1064] - 1./3. * amp[1065] - 1./3. * amp[1066] -
      1./3. * amp[1067] - 1./3. * amp[1068] - 1./3. * amp[1069] - 1./3. *
      amp[1096] - 1./3. * amp[1097] - 1./3. * amp[1100] - 1./3. * amp[1101]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[16][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_ddx_epemguux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 48;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[1102] + 1./3. * amp[1103] + 1./3. *
      amp[1104] + 1./3. * amp[1105] + 1./3. * amp[1106] + 1./3. * amp[1107] +
      1./3. * amp[1108] + 1./3. * amp[1109] + 1./3. * amp[1110] + 1./3. *
      amp[1111] + 1./3. * amp[1112] + 1./3. * amp[1113] + 1./3. * amp[1114] +
      1./3. * amp[1115] + 1./3. * amp[1116] + 1./3. * amp[1117] + 1./3. *
      amp[1144] + 1./3. * amp[1145] + 1./3. * amp[1148] + 1./3. * amp[1149]);
  jamp[1] = +1./2. * (-amp[1110] - amp[1111] - amp[1112] - amp[1113] -
      amp[1114] - amp[1115] - amp[1116] - amp[1117] - amp[1126] - amp[1127] -
      amp[1128] - amp[1129] - amp[1130] - amp[1131] - amp[1132] - amp[1133] -
      Complex<double> (0, 1) * amp[1134] - Complex<double> (0, 1) * amp[1135] -
      amp[1136] - Complex<double> (0, 1) * amp[1138] - Complex<double> (0, 1) *
      amp[1139] - amp[1140] + Complex<double> (0, 1) * amp[1142] +
      Complex<double> (0, 1) * amp[1143] - amp[1145] + Complex<double> (0, 1) *
      amp[1146] + Complex<double> (0, 1) * amp[1147] - amp[1149]);
  jamp[2] = +1./2. * (+1./3. * amp[1118] + 1./3. * amp[1119] + 1./3. *
      amp[1120] + 1./3. * amp[1121] + 1./3. * amp[1122] + 1./3. * amp[1123] +
      1./3. * amp[1124] + 1./3. * amp[1125] + 1./3. * amp[1126] + 1./3. *
      amp[1127] + 1./3. * amp[1128] + 1./3. * amp[1129] + 1./3. * amp[1130] +
      1./3. * amp[1131] + 1./3. * amp[1132] + 1./3. * amp[1133] + 1./3. *
      amp[1136] + 1./3. * amp[1137] + 1./3. * amp[1140] + 1./3. * amp[1141]);
  jamp[3] = +1./2. * (-amp[1102] - amp[1103] - amp[1104] - amp[1105] -
      amp[1106] - amp[1107] - amp[1108] - amp[1109] - amp[1118] - amp[1119] -
      amp[1120] - amp[1121] - amp[1122] - amp[1123] - amp[1124] - amp[1125] +
      Complex<double> (0, 1) * amp[1134] + Complex<double> (0, 1) * amp[1135] -
      amp[1137] + Complex<double> (0, 1) * amp[1138] + Complex<double> (0, 1) *
      amp[1139] - amp[1141] - Complex<double> (0, 1) * amp[1142] -
      Complex<double> (0, 1) * amp[1143] - amp[1144] - Complex<double> (0, 1) *
      amp[1146] - Complex<double> (0, 1) * amp[1147] - amp[1148]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[17][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_ddx_epemgssx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 48;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[1166] + 1./3. * amp[1167] + 1./3. *
      amp[1168] + 1./3. * amp[1169] + 1./3. * amp[1170] + 1./3. * amp[1171] +
      1./3. * amp[1172] + 1./3. * amp[1173] + 1./3. * amp[1174] + 1./3. *
      amp[1175] + 1./3. * amp[1176] + 1./3. * amp[1177] + 1./3. * amp[1178] +
      1./3. * amp[1179] + 1./3. * amp[1180] + 1./3. * amp[1181] + 1./3. *
      amp[1184] + 1./3. * amp[1185] + 1./3. * amp[1188] + 1./3. * amp[1189]);
  jamp[1] = +1./2. * (-amp[1150] - amp[1151] - amp[1152] - amp[1153] -
      amp[1154] - amp[1155] - amp[1156] - amp[1157] - amp[1166] - amp[1167] -
      amp[1168] - amp[1169] - amp[1170] - amp[1171] - amp[1172] - amp[1173] +
      Complex<double> (0, 1) * amp[1182] + Complex<double> (0, 1) * amp[1183] -
      amp[1185] + Complex<double> (0, 1) * amp[1186] + Complex<double> (0, 1) *
      amp[1187] - amp[1189] - Complex<double> (0, 1) * amp[1190] -
      Complex<double> (0, 1) * amp[1191] - amp[1192] - Complex<double> (0, 1) *
      amp[1194] - Complex<double> (0, 1) * amp[1195] - amp[1196]);
  jamp[2] = +1./2. * (+1./3. * amp[1150] + 1./3. * amp[1151] + 1./3. *
      amp[1152] + 1./3. * amp[1153] + 1./3. * amp[1154] + 1./3. * amp[1155] +
      1./3. * amp[1156] + 1./3. * amp[1157] + 1./3. * amp[1158] + 1./3. *
      amp[1159] + 1./3. * amp[1160] + 1./3. * amp[1161] + 1./3. * amp[1162] +
      1./3. * amp[1163] + 1./3. * amp[1164] + 1./3. * amp[1165] + 1./3. *
      amp[1192] + 1./3. * amp[1193] + 1./3. * amp[1196] + 1./3. * amp[1197]);
  jamp[3] = +1./2. * (-amp[1158] - amp[1159] - amp[1160] - amp[1161] -
      amp[1162] - amp[1163] - amp[1164] - amp[1165] - amp[1174] - amp[1175] -
      amp[1176] - amp[1177] - amp[1178] - amp[1179] - amp[1180] - amp[1181] -
      Complex<double> (0, 1) * amp[1182] - Complex<double> (0, 1) * amp[1183] -
      amp[1184] - Complex<double> (0, 1) * amp[1186] - Complex<double> (0, 1) *
      amp[1187] - amp[1188] + Complex<double> (0, 1) * amp[1190] +
      Complex<double> (0, 1) * amp[1191] - amp[1193] + Complex<double> (0, 1) *
      amp[1194] + Complex<double> (0, 1) * amp[1195] - amp[1197]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[18][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_ddx_vexvegddx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 48;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[1214] + 1./3. * amp[1215] + amp[1216] +
      amp[1217] + 1./3. * amp[1218] + 1./3. * amp[1219] + amp[1220] + amp[1221]
      + 1./3. * amp[1222] + 1./3. * amp[1223] + amp[1224] + amp[1225] +
      amp[1226] + amp[1227] + 1./3. * amp[1228] + 1./3. * amp[1229] + 1./3. *
      amp[1232] + 1./3. * amp[1233] + Complex<double> (0, 1) * amp[1234] +
      Complex<double> (0, 1) * amp[1235] + amp[1236] - Complex<double> (0, 1) *
      amp[1238] - Complex<double> (0, 1) * amp[1239] + amp[1241]);
  jamp[1] = +1./2. * (-amp[1198] - amp[1199] - 1./3. * amp[1200] - 1./3. *
      amp[1201] - 1./3. * amp[1202] - 1./3. * amp[1203] - amp[1204] - amp[1205]
      - amp[1214] - amp[1215] - 1./3. * amp[1216] - 1./3. * amp[1217] -
      amp[1218] - amp[1219] - 1./3. * amp[1220] - 1./3. * amp[1221] +
      Complex<double> (0, 1) * amp[1230] + Complex<double> (0, 1) * amp[1231] -
      amp[1233] - 1./3. * amp[1240] - 1./3. * amp[1241] - Complex<double> (0,
      1) * amp[1242] - Complex<double> (0, 1) * amp[1243] - amp[1244]);
  jamp[2] = +1./2. * (+1./3. * amp[1198] + 1./3. * amp[1199] + amp[1200] +
      amp[1201] + amp[1202] + amp[1203] + 1./3. * amp[1204] + 1./3. * amp[1205]
      + amp[1206] + amp[1207] + 1./3. * amp[1208] + 1./3. * amp[1209] +
      amp[1210] + amp[1211] + 1./3. * amp[1212] + 1./3. * amp[1213] -
      Complex<double> (0, 1) * amp[1234] - Complex<double> (0, 1) * amp[1235] +
      amp[1237] + Complex<double> (0, 1) * amp[1238] + Complex<double> (0, 1) *
      amp[1239] + amp[1240] + 1./3. * amp[1244] + 1./3. * amp[1245]);
  jamp[3] = +1./2. * (-1./3. * amp[1206] - 1./3. * amp[1207] - amp[1208] -
      amp[1209] - 1./3. * amp[1210] - 1./3. * amp[1211] - amp[1212] - amp[1213]
      - amp[1222] - amp[1223] - 1./3. * amp[1224] - 1./3. * amp[1225] - 1./3. *
      amp[1226] - 1./3. * amp[1227] - amp[1228] - amp[1229] - Complex<double>
      (0, 1) * amp[1230] - Complex<double> (0, 1) * amp[1231] - amp[1232] -
      1./3. * amp[1236] - 1./3. * amp[1237] + Complex<double> (0, 1) *
      amp[1242] + Complex<double> (0, 1) * amp[1243] - amp[1245]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[19][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_dsx_epemgdsx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 48;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[1254] + amp[1255] + amp[1256] + amp[1257] +
      amp[1258] + amp[1259] + amp[1260] + amp[1261] + amp[1270] + amp[1271] +
      amp[1272] + amp[1273] + amp[1274] + amp[1275] + amp[1276] + amp[1277] +
      Complex<double> (0, 1) * amp[1278] + Complex<double> (0, 1) * amp[1279] +
      amp[1280] + Complex<double> (0, 1) * amp[1282] + Complex<double> (0, 1) *
      amp[1283] + amp[1284] - Complex<double> (0, 1) * amp[1286] -
      Complex<double> (0, 1) * amp[1287] + amp[1289] - Complex<double> (0, 1) *
      amp[1290] - Complex<double> (0, 1) * amp[1291] + amp[1293]);
  jamp[1] = +1./2. * (-1./3. * amp[1246] - 1./3. * amp[1247] - 1./3. *
      amp[1248] - 1./3. * amp[1249] - 1./3. * amp[1250] - 1./3. * amp[1251] -
      1./3. * amp[1252] - 1./3. * amp[1253] - 1./3. * amp[1254] - 1./3. *
      amp[1255] - 1./3. * amp[1256] - 1./3. * amp[1257] - 1./3. * amp[1258] -
      1./3. * amp[1259] - 1./3. * amp[1260] - 1./3. * amp[1261] - 1./3. *
      amp[1288] - 1./3. * amp[1289] - 1./3. * amp[1292] - 1./3. * amp[1293]);
  jamp[2] = +1./2. * (+amp[1246] + amp[1247] + amp[1248] + amp[1249] +
      amp[1250] + amp[1251] + amp[1252] + amp[1253] + amp[1262] + amp[1263] +
      amp[1264] + amp[1265] + amp[1266] + amp[1267] + amp[1268] + amp[1269] -
      Complex<double> (0, 1) * amp[1278] - Complex<double> (0, 1) * amp[1279] +
      amp[1281] - Complex<double> (0, 1) * amp[1282] - Complex<double> (0, 1) *
      amp[1283] + amp[1285] + Complex<double> (0, 1) * amp[1286] +
      Complex<double> (0, 1) * amp[1287] + amp[1288] + Complex<double> (0, 1) *
      amp[1290] + Complex<double> (0, 1) * amp[1291] + amp[1292]);
  jamp[3] = +1./2. * (-1./3. * amp[1262] - 1./3. * amp[1263] - 1./3. *
      amp[1264] - 1./3. * amp[1265] - 1./3. * amp[1266] - 1./3. * amp[1267] -
      1./3. * amp[1268] - 1./3. * amp[1269] - 1./3. * amp[1270] - 1./3. *
      amp[1271] - 1./3. * amp[1272] - 1./3. * amp[1273] - 1./3. * amp[1274] -
      1./3. * amp[1275] - 1./3. * amp[1276] - 1./3. * amp[1277] - 1./3. *
      amp[1280] - 1./3. * amp[1281] - 1./3. * amp[1284] - 1./3. * amp[1285]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[20][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_uxux_vexveguxux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 48;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[1310] + 1./3. * amp[1311] + amp[1312] +
      amp[1313] + 1./3. * amp[1314] + 1./3. * amp[1315] + amp[1316] + amp[1317]
      + 1./3. * amp[1318] + 1./3. * amp[1319] + amp[1320] + amp[1321] +
      amp[1322] + amp[1323] + 1./3. * amp[1324] + 1./3. * amp[1325] + 1./3. *
      amp[1328] + 1./3. * amp[1329] + Complex<double> (0, 1) * amp[1330] +
      Complex<double> (0, 1) * amp[1331] + amp[1332] - Complex<double> (0, 1) *
      amp[1334] - Complex<double> (0, 1) * amp[1335] + amp[1337]);
  jamp[1] = +1./2. * (-amp[1294] - amp[1295] - 1./3. * amp[1296] - 1./3. *
      amp[1297] - 1./3. * amp[1298] - 1./3. * amp[1299] - amp[1300] - amp[1301]
      - amp[1310] - amp[1311] - 1./3. * amp[1312] - 1./3. * amp[1313] -
      amp[1314] - amp[1315] - 1./3. * amp[1316] - 1./3. * amp[1317] +
      Complex<double> (0, 1) * amp[1326] + Complex<double> (0, 1) * amp[1327] -
      amp[1329] - 1./3. * amp[1336] - 1./3. * amp[1337] - Complex<double> (0,
      1) * amp[1338] - Complex<double> (0, 1) * amp[1339] - amp[1340]);
  jamp[2] = +1./2. * (-1./3. * amp[1302] - 1./3. * amp[1303] - amp[1304] -
      amp[1305] - 1./3. * amp[1306] - 1./3. * amp[1307] - amp[1308] - amp[1309]
      - amp[1318] - amp[1319] - 1./3. * amp[1320] - 1./3. * amp[1321] - 1./3. *
      amp[1322] - 1./3. * amp[1323] - amp[1324] - amp[1325] - Complex<double>
      (0, 1) * amp[1326] - Complex<double> (0, 1) * amp[1327] - amp[1328] -
      1./3. * amp[1332] - 1./3. * amp[1333] + Complex<double> (0, 1) *
      amp[1338] + Complex<double> (0, 1) * amp[1339] - amp[1341]);
  jamp[3] = +1./2. * (+1./3. * amp[1294] + 1./3. * amp[1295] + amp[1296] +
      amp[1297] + amp[1298] + amp[1299] + 1./3. * amp[1300] + 1./3. * amp[1301]
      + amp[1302] + amp[1303] + 1./3. * amp[1304] + 1./3. * amp[1305] +
      amp[1306] + amp[1307] + 1./3. * amp[1308] + 1./3. * amp[1309] -
      Complex<double> (0, 1) * amp[1330] - Complex<double> (0, 1) * amp[1331] +
      amp[1333] + Complex<double> (0, 1) * amp[1334] + Complex<double> (0, 1) *
      amp[1335] + amp[1336] + 1./3. * amp[1340] + 1./3. * amp[1341]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[21][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_uxcx_epemguxcx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 48;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[1358] + 1./3. * amp[1359] + 1./3. *
      amp[1360] + 1./3. * amp[1361] + 1./3. * amp[1362] + 1./3. * amp[1363] +
      1./3. * amp[1364] + 1./3. * amp[1365] + 1./3. * amp[1366] + 1./3. *
      amp[1367] + 1./3. * amp[1368] + 1./3. * amp[1369] + 1./3. * amp[1370] +
      1./3. * amp[1371] + 1./3. * amp[1372] + 1./3. * amp[1373] + 1./3. *
      amp[1376] + 1./3. * amp[1377] + 1./3. * amp[1380] + 1./3. * amp[1381]);
  jamp[1] = +1./2. * (-amp[1342] - amp[1343] - amp[1344] - amp[1345] -
      amp[1346] - amp[1347] - amp[1348] - amp[1349] - amp[1358] - amp[1359] -
      amp[1360] - amp[1361] - amp[1362] - amp[1363] - amp[1364] - amp[1365] +
      Complex<double> (0, 1) * amp[1374] + Complex<double> (0, 1) * amp[1375] -
      amp[1377] + Complex<double> (0, 1) * amp[1378] + Complex<double> (0, 1) *
      amp[1379] - amp[1381] - Complex<double> (0, 1) * amp[1382] -
      Complex<double> (0, 1) * amp[1383] - amp[1384] - Complex<double> (0, 1) *
      amp[1386] - Complex<double> (0, 1) * amp[1387] - amp[1388]);
  jamp[2] = +1./2. * (-amp[1350] - amp[1351] - amp[1352] - amp[1353] -
      amp[1354] - amp[1355] - amp[1356] - amp[1357] - amp[1366] - amp[1367] -
      amp[1368] - amp[1369] - amp[1370] - amp[1371] - amp[1372] - amp[1373] -
      Complex<double> (0, 1) * amp[1374] - Complex<double> (0, 1) * amp[1375] -
      amp[1376] - Complex<double> (0, 1) * amp[1378] - Complex<double> (0, 1) *
      amp[1379] - amp[1380] + Complex<double> (0, 1) * amp[1382] +
      Complex<double> (0, 1) * amp[1383] - amp[1385] + Complex<double> (0, 1) *
      amp[1386] + Complex<double> (0, 1) * amp[1387] - amp[1389]);
  jamp[3] = +1./2. * (+1./3. * amp[1342] + 1./3. * amp[1343] + 1./3. *
      amp[1344] + 1./3. * amp[1345] + 1./3. * amp[1346] + 1./3. * amp[1347] +
      1./3. * amp[1348] + 1./3. * amp[1349] + 1./3. * amp[1350] + 1./3. *
      amp[1351] + 1./3. * amp[1352] + 1./3. * amp[1353] + 1./3. * amp[1354] +
      1./3. * amp[1355] + 1./3. * amp[1356] + 1./3. * amp[1357] + 1./3. *
      amp[1384] + 1./3. * amp[1385] + 1./3. * amp[1388] + 1./3. * amp[1389]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[22][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_uxdx_epemguxdx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 48;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[1406] + 1./3. * amp[1407] + 1./3. *
      amp[1408] + 1./3. * amp[1409] + 1./3. * amp[1410] + 1./3. * amp[1411] +
      1./3. * amp[1412] + 1./3. * amp[1413] + 1./3. * amp[1414] + 1./3. *
      amp[1415] + 1./3. * amp[1416] + 1./3. * amp[1417] + 1./3. * amp[1418] +
      1./3. * amp[1419] + 1./3. * amp[1420] + 1./3. * amp[1421] + 1./3. *
      amp[1424] + 1./3. * amp[1425] + 1./3. * amp[1428] + 1./3. * amp[1429]);
  jamp[1] = +1./2. * (-amp[1390] - amp[1391] - amp[1392] - amp[1393] -
      amp[1394] - amp[1395] - amp[1396] - amp[1397] - amp[1406] - amp[1407] -
      amp[1408] - amp[1409] - amp[1410] - amp[1411] - amp[1412] - amp[1413] +
      Complex<double> (0, 1) * amp[1422] + Complex<double> (0, 1) * amp[1423] -
      amp[1425] + Complex<double> (0, 1) * amp[1426] + Complex<double> (0, 1) *
      amp[1427] - amp[1429] - Complex<double> (0, 1) * amp[1430] -
      Complex<double> (0, 1) * amp[1431] - amp[1432] - Complex<double> (0, 1) *
      amp[1434] - Complex<double> (0, 1) * amp[1435] - amp[1436]);
  jamp[2] = +1./2. * (-amp[1398] - amp[1399] - amp[1400] - amp[1401] -
      amp[1402] - amp[1403] - amp[1404] - amp[1405] - amp[1414] - amp[1415] -
      amp[1416] - amp[1417] - amp[1418] - amp[1419] - amp[1420] - amp[1421] -
      Complex<double> (0, 1) * amp[1422] - Complex<double> (0, 1) * amp[1423] -
      amp[1424] - Complex<double> (0, 1) * amp[1426] - Complex<double> (0, 1) *
      amp[1427] - amp[1428] + Complex<double> (0, 1) * amp[1430] +
      Complex<double> (0, 1) * amp[1431] - amp[1433] + Complex<double> (0, 1) *
      amp[1434] + Complex<double> (0, 1) * amp[1435] - amp[1437]);
  jamp[3] = +1./2. * (+1./3. * amp[1390] + 1./3. * amp[1391] + 1./3. *
      amp[1392] + 1./3. * amp[1393] + 1./3. * amp[1394] + 1./3. * amp[1395] +
      1./3. * amp[1396] + 1./3. * amp[1397] + 1./3. * amp[1398] + 1./3. *
      amp[1399] + 1./3. * amp[1400] + 1./3. * amp[1401] + 1./3. * amp[1402] +
      1./3. * amp[1403] + 1./3. * amp[1404] + 1./3. * amp[1405] + 1./3. *
      amp[1432] + 1./3. * amp[1433] + 1./3. * amp[1436] + 1./3. * amp[1437]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[23][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_dxdx_vexvegdxdx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 48;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[1454] + 1./3. * amp[1455] + amp[1456] +
      amp[1457] + 1./3. * amp[1458] + 1./3. * amp[1459] + amp[1460] + amp[1461]
      + 1./3. * amp[1462] + 1./3. * amp[1463] + amp[1464] + amp[1465] +
      amp[1466] + amp[1467] + 1./3. * amp[1468] + 1./3. * amp[1469] + 1./3. *
      amp[1472] + 1./3. * amp[1473] + Complex<double> (0, 1) * amp[1474] +
      Complex<double> (0, 1) * amp[1475] + amp[1476] - Complex<double> (0, 1) *
      amp[1478] - Complex<double> (0, 1) * amp[1479] + amp[1481]);
  jamp[1] = +1./2. * (-amp[1438] - amp[1439] - 1./3. * amp[1440] - 1./3. *
      amp[1441] - 1./3. * amp[1442] - 1./3. * amp[1443] - amp[1444] - amp[1445]
      - amp[1454] - amp[1455] - 1./3. * amp[1456] - 1./3. * amp[1457] -
      amp[1458] - amp[1459] - 1./3. * amp[1460] - 1./3. * amp[1461] +
      Complex<double> (0, 1) * amp[1470] + Complex<double> (0, 1) * amp[1471] -
      amp[1473] - 1./3. * amp[1480] - 1./3. * amp[1481] - Complex<double> (0,
      1) * amp[1482] - Complex<double> (0, 1) * amp[1483] - amp[1484]);
  jamp[2] = +1./2. * (-1./3. * amp[1446] - 1./3. * amp[1447] - amp[1448] -
      amp[1449] - 1./3. * amp[1450] - 1./3. * amp[1451] - amp[1452] - amp[1453]
      - amp[1462] - amp[1463] - 1./3. * amp[1464] - 1./3. * amp[1465] - 1./3. *
      amp[1466] - 1./3. * amp[1467] - amp[1468] - amp[1469] - Complex<double>
      (0, 1) * amp[1470] - Complex<double> (0, 1) * amp[1471] - amp[1472] -
      1./3. * amp[1476] - 1./3. * amp[1477] + Complex<double> (0, 1) *
      amp[1482] + Complex<double> (0, 1) * amp[1483] - amp[1485]);
  jamp[3] = +1./2. * (+1./3. * amp[1438] + 1./3. * amp[1439] + amp[1440] +
      amp[1441] + amp[1442] + amp[1443] + 1./3. * amp[1444] + 1./3. * amp[1445]
      + amp[1446] + amp[1447] + 1./3. * amp[1448] + 1./3. * amp[1449] +
      amp[1450] + amp[1451] + 1./3. * amp[1452] + 1./3. * amp[1453] -
      Complex<double> (0, 1) * amp[1474] - Complex<double> (0, 1) * amp[1475] +
      amp[1477] + Complex<double> (0, 1) * amp[1478] + Complex<double> (0, 1) *
      amp[1479] + amp[1480] + 1./3. * amp[1484] + 1./3. * amp[1485]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[24][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_dxsx_epemgdxsx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 48;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[1502] + 1./3. * amp[1503] + 1./3. *
      amp[1504] + 1./3. * amp[1505] + 1./3. * amp[1506] + 1./3. * amp[1507] +
      1./3. * amp[1508] + 1./3. * amp[1509] + 1./3. * amp[1510] + 1./3. *
      amp[1511] + 1./3. * amp[1512] + 1./3. * amp[1513] + 1./3. * amp[1514] +
      1./3. * amp[1515] + 1./3. * amp[1516] + 1./3. * amp[1517] + 1./3. *
      amp[1520] + 1./3. * amp[1521] + 1./3. * amp[1524] + 1./3. * amp[1525]);
  jamp[1] = +1./2. * (-amp[1486] - amp[1487] - amp[1488] - amp[1489] -
      amp[1490] - amp[1491] - amp[1492] - amp[1493] - amp[1502] - amp[1503] -
      amp[1504] - amp[1505] - amp[1506] - amp[1507] - amp[1508] - amp[1509] +
      Complex<double> (0, 1) * amp[1518] + Complex<double> (0, 1) * amp[1519] -
      amp[1521] + Complex<double> (0, 1) * amp[1522] + Complex<double> (0, 1) *
      amp[1523] - amp[1525] - Complex<double> (0, 1) * amp[1526] -
      Complex<double> (0, 1) * amp[1527] - amp[1528] - Complex<double> (0, 1) *
      amp[1530] - Complex<double> (0, 1) * amp[1531] - amp[1532]);
  jamp[2] = +1./2. * (-amp[1494] - amp[1495] - amp[1496] - amp[1497] -
      amp[1498] - amp[1499] - amp[1500] - amp[1501] - amp[1510] - amp[1511] -
      amp[1512] - amp[1513] - amp[1514] - amp[1515] - amp[1516] - amp[1517] -
      Complex<double> (0, 1) * amp[1518] - Complex<double> (0, 1) * amp[1519] -
      amp[1520] - Complex<double> (0, 1) * amp[1522] - Complex<double> (0, 1) *
      amp[1523] - amp[1524] + Complex<double> (0, 1) * amp[1526] +
      Complex<double> (0, 1) * amp[1527] - amp[1529] + Complex<double> (0, 1) *
      amp[1530] + Complex<double> (0, 1) * amp[1531] - amp[1533]);
  jamp[3] = +1./2. * (+1./3. * amp[1486] + 1./3. * amp[1487] + 1./3. *
      amp[1488] + 1./3. * amp[1489] + 1./3. * amp[1490] + 1./3. * amp[1491] +
      1./3. * amp[1492] + 1./3. * amp[1493] + 1./3. * amp[1494] + 1./3. *
      amp[1495] + 1./3. * amp[1496] + 1./3. * amp[1497] + 1./3. * amp[1498] +
      1./3. * amp[1499] + 1./3. * amp[1500] + 1./3. * amp[1501] + 1./3. *
      amp[1528] + 1./3. * amp[1529] + 1./3. * amp[1532] + 1./3. * amp[1533]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[25][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_uu_epvegud() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[1534] + 1./3. * amp[1535] + amp[1536] +
      amp[1537] + 1./3. * amp[1538] + 1./3. * amp[1539] + amp[1540] + amp[1541]
      + Complex<double> (0, 1) * amp[1554] + Complex<double> (0, 1) * amp[1555]
      + amp[1556]);
  jamp[1] = +1./2. * (-amp[1538] - amp[1539] - 1./3. * amp[1540] - 1./3. *
      amp[1541] - amp[1546] - amp[1547] - 1./3. * amp[1548] - 1./3. * amp[1549]
      - Complex<double> (0, 1) * amp[1550] - Complex<double> (0, 1) * amp[1551]
      - amp[1552]);
  jamp[2] = +1./2. * (-amp[1534] - amp[1535] - 1./3. * amp[1536] - 1./3. *
      amp[1537] - amp[1542] - amp[1543] - 1./3. * amp[1544] - 1./3. * amp[1545]
      + Complex<double> (0, 1) * amp[1550] + Complex<double> (0, 1) * amp[1551]
      - amp[1553] - 1./3. * amp[1556] - 1./3. * amp[1557]);
  jamp[3] = +1./2. * (+1./3. * amp[1542] + 1./3. * amp[1543] + amp[1544] +
      amp[1545] + 1./3. * amp[1546] + 1./3. * amp[1547] + amp[1548] + amp[1549]
      + 1./3. * amp[1552] + 1./3. * amp[1553] - Complex<double> (0, 1) *
      amp[1554] - Complex<double> (0, 1) * amp[1555] + amp[1557]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[26][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_uc_vexveguc() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[1558] + 1./3. * amp[1559] + 1./3. *
      amp[1560] + 1./3. * amp[1561] + 1./3. * amp[1562] + 1./3. * amp[1563] +
      1./3. * amp[1564] + 1./3. * amp[1565] + 1./3. * amp[1580] + 1./3. *
      amp[1581]);
  jamp[1] = +1./2. * (-amp[1562] - amp[1563] - amp[1564] - amp[1565] -
      amp[1570] - amp[1571] - amp[1572] - amp[1573] - Complex<double> (0, 1) *
      amp[1574] - Complex<double> (0, 1) * amp[1575] - amp[1576] +
      Complex<double> (0, 1) * amp[1578] + Complex<double> (0, 1) * amp[1579] -
      amp[1581]);
  jamp[2] = +1./2. * (-amp[1558] - amp[1559] - amp[1560] - amp[1561] -
      amp[1566] - amp[1567] - amp[1568] - amp[1569] + Complex<double> (0, 1) *
      amp[1574] + Complex<double> (0, 1) * amp[1575] - amp[1577] -
      Complex<double> (0, 1) * amp[1578] - Complex<double> (0, 1) * amp[1579] -
      amp[1580]);
  jamp[3] = +1./2. * (+1./3. * amp[1566] + 1./3. * amp[1567] + 1./3. *
      amp[1568] + 1./3. * amp[1569] + 1./3. * amp[1570] + 1./3. * amp[1571] +
      1./3. * amp[1572] + 1./3. * amp[1573] + 1./3. * amp[1576] + 1./3. *
      amp[1577]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[27][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_ud_epvegdd() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[1582] + amp[1583] + 1./3. * amp[1584] + 1./3. *
      amp[1585] + amp[1586] + amp[1587] + 1./3. * amp[1588] + 1./3. * amp[1589]
      + Complex<double> (0, 1) * amp[1598] + Complex<double> (0, 1) * amp[1599]
      + amp[1600] + 1./3. * amp[1604] + 1./3. * amp[1605]);
  jamp[1] = +1./2. * (-1./3. * amp[1586] - 1./3. * amp[1587] - amp[1588] -
      amp[1589] - 1./3. * amp[1594] - 1./3. * amp[1595] - amp[1596] - amp[1597]
      + Complex<double> (0, 1) * amp[1602] + Complex<double> (0, 1) * amp[1603]
      - amp[1605]);
  jamp[2] = +1./2. * (-1./3. * amp[1582] - 1./3. * amp[1583] - amp[1584] -
      amp[1585] - amp[1590] - amp[1591] - 1./3. * amp[1592] - 1./3. * amp[1593]
      - 1./3. * amp[1600] - 1./3. * amp[1601] - Complex<double> (0, 1) *
      amp[1602] - Complex<double> (0, 1) * amp[1603] - amp[1604]);
  jamp[3] = +1./2. * (+1./3. * amp[1590] + 1./3. * amp[1591] + amp[1592] +
      amp[1593] + amp[1594] + amp[1595] + 1./3. * amp[1596] + 1./3. * amp[1597]
      - Complex<double> (0, 1) * amp[1598] - Complex<double> (0, 1) * amp[1599]
      + amp[1601]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[28][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_ud_vexemguu() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[1606] + 1./3. * amp[1607] + amp[1608] +
      amp[1609] + amp[1610] + amp[1611] + 1./3. * amp[1612] + 1./3. * amp[1613]
      - Complex<double> (0, 1) * amp[1626] - Complex<double> (0, 1) * amp[1627]
      + amp[1629]);
  jamp[1] = +1./2. * (-1./3. * amp[1610] - 1./3. * amp[1611] - amp[1612] -
      amp[1613] - amp[1618] - amp[1619] - 1./3. * amp[1620] - 1./3. * amp[1621]
      - Complex<double> (0, 1) * amp[1622] - Complex<double> (0, 1) * amp[1623]
      - amp[1624] - 1./3. * amp[1628] - 1./3. * amp[1629]);
  jamp[2] = +1./2. * (-amp[1606] - amp[1607] - 1./3. * amp[1608] - 1./3. *
      amp[1609] - amp[1614] - amp[1615] - 1./3. * amp[1616] - 1./3. * amp[1617]
      + Complex<double> (0, 1) * amp[1622] + Complex<double> (0, 1) * amp[1623]
      - amp[1625]);
  jamp[3] = +1./2. * (+1./3. * amp[1614] + 1./3. * amp[1615] + amp[1616] +
      amp[1617] + 1./3. * amp[1618] + 1./3. * amp[1619] + amp[1620] + amp[1621]
      + 1./3. * amp[1624] + 1./3. * amp[1625] + Complex<double> (0, 1) *
      amp[1626] + Complex<double> (0, 1) * amp[1627] + amp[1628]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[29][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_ud_vexvegud() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[1630] + 1./3. * amp[1631] + 1./3. *
      amp[1632] + 1./3. * amp[1633] + 1./3. * amp[1634] + 1./3. * amp[1635] +
      1./3. * amp[1636] + 1./3. * amp[1637] + 1./3. * amp[1652] + 1./3. *
      amp[1653]);
  jamp[1] = +1./2. * (-amp[1634] - amp[1635] - amp[1636] - amp[1637] -
      amp[1642] - amp[1643] - amp[1644] - amp[1645] - Complex<double> (0, 1) *
      amp[1646] - Complex<double> (0, 1) * amp[1647] - amp[1648] +
      Complex<double> (0, 1) * amp[1650] + Complex<double> (0, 1) * amp[1651] -
      amp[1653]);
  jamp[2] = +1./2. * (-amp[1630] - amp[1631] - amp[1632] - amp[1633] -
      amp[1638] - amp[1639] - amp[1640] - amp[1641] + Complex<double> (0, 1) *
      amp[1646] + Complex<double> (0, 1) * amp[1647] - amp[1649] -
      Complex<double> (0, 1) * amp[1650] - Complex<double> (0, 1) * amp[1651] -
      amp[1652]);
  jamp[3] = +1./2. * (+1./3. * amp[1638] + 1./3. * amp[1639] + 1./3. *
      amp[1640] + 1./3. * amp[1641] + 1./3. * amp[1642] + 1./3. * amp[1643] +
      1./3. * amp[1644] + 1./3. * amp[1645] + 1./3. * amp[1648] + 1./3. *
      amp[1649]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[30][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_uux_epvegdux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[1662] + 1./3. * amp[1663] + amp[1664] +
      amp[1665] + 1./3. * amp[1666] + 1./3. * amp[1667] + amp[1668] + amp[1669]
      + 1./3. * amp[1672] + 1./3. * amp[1673] - Complex<double> (0, 1) *
      amp[1674] - Complex<double> (0, 1) * amp[1675] + amp[1677]);
  jamp[1] = +1./2. * (-amp[1654] - amp[1655] - 1./3. * amp[1656] - 1./3. *
      amp[1657] - amp[1662] - amp[1663] - 1./3. * amp[1664] - 1./3. * amp[1665]
      + Complex<double> (0, 1) * amp[1670] + Complex<double> (0, 1) * amp[1671]
      - amp[1673] - 1./3. * amp[1676] - 1./3. * amp[1677]);
  jamp[2] = +1./2. * (+1./3. * amp[1654] + 1./3. * amp[1655] + amp[1656] +
      amp[1657] + 1./3. * amp[1658] + 1./3. * amp[1659] + amp[1660] + amp[1661]
      + Complex<double> (0, 1) * amp[1674] + Complex<double> (0, 1) * amp[1675]
      + amp[1676]);
  jamp[3] = +1./2. * (-amp[1658] - amp[1659] - 1./3. * amp[1660] - 1./3. *
      amp[1661] - amp[1666] - amp[1667] - 1./3. * amp[1668] - 1./3. * amp[1669]
      - Complex<double> (0, 1) * amp[1670] - Complex<double> (0, 1) * amp[1671]
      - amp[1672]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[31][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_uux_vexemgudx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[1686] + 1./3. * amp[1687] + amp[1688] +
      amp[1689] + 1./3. * amp[1690] + 1./3. * amp[1691] + amp[1692] + amp[1693]
      + 1./3. * amp[1696] + 1./3. * amp[1697] + Complex<double> (0, 1) *
      amp[1698] + Complex<double> (0, 1) * amp[1699] + amp[1700]);
  jamp[1] = +1./2. * (-amp[1678] - amp[1679] - 1./3. * amp[1680] - 1./3. *
      amp[1681] - amp[1686] - amp[1687] - 1./3. * amp[1688] - 1./3. * amp[1689]
      + Complex<double> (0, 1) * amp[1694] + Complex<double> (0, 1) * amp[1695]
      - amp[1697]);
  jamp[2] = +1./2. * (+1./3. * amp[1678] + 1./3. * amp[1679] + amp[1680] +
      amp[1681] + amp[1682] + amp[1683] + 1./3. * amp[1684] + 1./3. * amp[1685]
      - Complex<double> (0, 1) * amp[1698] - Complex<double> (0, 1) * amp[1699]
      + amp[1701]);
  jamp[3] = +1./2. * (-1./3. * amp[1682] - 1./3. * amp[1683] - amp[1684] -
      amp[1685] - amp[1690] - amp[1691] - 1./3. * amp[1692] - 1./3. * amp[1693]
      - Complex<double> (0, 1) * amp[1694] - Complex<double> (0, 1) * amp[1695]
      - amp[1696] - 1./3. * amp[1700] - 1./3. * amp[1701]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[32][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_uux_vexvegccx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[1710] + 1./3. * amp[1711] + 1./3. *
      amp[1712] + 1./3. * amp[1713] + 1./3. * amp[1714] + 1./3. * amp[1715] +
      1./3. * amp[1716] + 1./3. * amp[1717] + 1./3. * amp[1720] + 1./3. *
      amp[1721]);
  jamp[1] = +1./2. * (-amp[1702] - amp[1703] - amp[1704] - amp[1705] -
      amp[1710] - amp[1711] - amp[1712] - amp[1713] + Complex<double> (0, 1) *
      amp[1718] + Complex<double> (0, 1) * amp[1719] - amp[1721] -
      Complex<double> (0, 1) * amp[1722] - Complex<double> (0, 1) * amp[1723] -
      amp[1724]);
  jamp[2] = +1./2. * (+1./3. * amp[1702] + 1./3. * amp[1703] + 1./3. *
      amp[1704] + 1./3. * amp[1705] + 1./3. * amp[1706] + 1./3. * amp[1707] +
      1./3. * amp[1708] + 1./3. * amp[1709] + 1./3. * amp[1724] + 1./3. *
      amp[1725]);
  jamp[3] = +1./2. * (-amp[1706] - amp[1707] - amp[1708] - amp[1709] -
      amp[1714] - amp[1715] - amp[1716] - amp[1717] - Complex<double> (0, 1) *
      amp[1718] - Complex<double> (0, 1) * amp[1719] - amp[1720] +
      Complex<double> (0, 1) * amp[1722] + Complex<double> (0, 1) * amp[1723] -
      amp[1725]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[33][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_uux_vexvegddx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[1734] + 1./3. * amp[1735] + 1./3. *
      amp[1736] + 1./3. * amp[1737] + 1./3. * amp[1738] + 1./3. * amp[1739] +
      1./3. * amp[1740] + 1./3. * amp[1741] + 1./3. * amp[1744] + 1./3. *
      amp[1745]);
  jamp[1] = +1./2. * (-amp[1726] - amp[1727] - amp[1728] - amp[1729] -
      amp[1734] - amp[1735] - amp[1736] - amp[1737] + Complex<double> (0, 1) *
      amp[1742] + Complex<double> (0, 1) * amp[1743] - amp[1745] -
      Complex<double> (0, 1) * amp[1746] - Complex<double> (0, 1) * amp[1747] -
      amp[1748]);
  jamp[2] = +1./2. * (+1./3. * amp[1726] + 1./3. * amp[1727] + 1./3. *
      amp[1728] + 1./3. * amp[1729] + 1./3. * amp[1730] + 1./3. * amp[1731] +
      1./3. * amp[1732] + 1./3. * amp[1733] + 1./3. * amp[1748] + 1./3. *
      amp[1749]);
  jamp[3] = +1./2. * (-amp[1730] - amp[1731] - amp[1732] - amp[1733] -
      amp[1738] - amp[1739] - amp[1740] - amp[1741] - Complex<double> (0, 1) *
      amp[1742] - Complex<double> (0, 1) * amp[1743] - amp[1744] +
      Complex<double> (0, 1) * amp[1746] + Complex<double> (0, 1) * amp[1747] -
      amp[1749]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[34][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_ucx_vexvegucx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[1754] + amp[1755] + amp[1756] + amp[1757] +
      amp[1762] + amp[1763] + amp[1764] + amp[1765] + Complex<double> (0, 1) *
      amp[1766] + Complex<double> (0, 1) * amp[1767] + amp[1768] -
      Complex<double> (0, 1) * amp[1770] - Complex<double> (0, 1) * amp[1771] +
      amp[1773]);
  jamp[1] = +1./2. * (-1./3. * amp[1750] - 1./3. * amp[1751] - 1./3. *
      amp[1752] - 1./3. * amp[1753] - 1./3. * amp[1754] - 1./3. * amp[1755] -
      1./3. * amp[1756] - 1./3. * amp[1757] - 1./3. * amp[1772] - 1./3. *
      amp[1773]);
  jamp[2] = +1./2. * (+amp[1750] + amp[1751] + amp[1752] + amp[1753] +
      amp[1758] + amp[1759] + amp[1760] + amp[1761] - Complex<double> (0, 1) *
      amp[1766] - Complex<double> (0, 1) * amp[1767] + amp[1769] +
      Complex<double> (0, 1) * amp[1770] + Complex<double> (0, 1) * amp[1771] +
      amp[1772]);
  jamp[3] = +1./2. * (-1./3. * amp[1758] - 1./3. * amp[1759] - 1./3. *
      amp[1760] - 1./3. * amp[1761] - 1./3. * amp[1762] - 1./3. * amp[1763] -
      1./3. * amp[1764] - 1./3. * amp[1765] - 1./3. * amp[1768] - 1./3. *
      amp[1769]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[35][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_udx_epveguux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[1778] + amp[1779] + 1./3. * amp[1780] + 1./3. *
      amp[1781] + amp[1786] + amp[1787] + 1./3. * amp[1788] + 1./3. * amp[1789]
      + Complex<double> (0, 1) * amp[1790] + Complex<double> (0, 1) * amp[1791]
      + amp[1792]);
  jamp[1] = +1./2. * (-1./3. * amp[1774] - 1./3. * amp[1775] - amp[1776] -
      amp[1777] - 1./3. * amp[1778] - 1./3. * amp[1779] - amp[1780] - amp[1781]
      - Complex<double> (0, 1) * amp[1794] - Complex<double> (0, 1) * amp[1795]
      - amp[1796]);
  jamp[2] = +1./2. * (+amp[1774] + amp[1775] + 1./3. * amp[1776] + 1./3. *
      amp[1777] + amp[1782] + amp[1783] + 1./3. * amp[1784] + 1./3. * amp[1785]
      - Complex<double> (0, 1) * amp[1790] - Complex<double> (0, 1) * amp[1791]
      + amp[1793] + 1./3. * amp[1796] + 1./3. * amp[1797]);
  jamp[3] = +1./2. * (-1./3. * amp[1782] - 1./3. * amp[1783] - amp[1784] -
      amp[1785] - 1./3. * amp[1786] - 1./3. * amp[1787] - amp[1788] - amp[1789]
      - 1./3. * amp[1792] - 1./3. * amp[1793] + Complex<double> (0, 1) *
      amp[1794] + Complex<double> (0, 1) * amp[1795] - amp[1797]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[36][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_udx_epvegddx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[1806] + 1./3. * amp[1807] + amp[1808] +
      amp[1809] + amp[1810] + amp[1811] + 1./3. * amp[1812] + 1./3. * amp[1813]
      - Complex<double> (0, 1) * amp[1814] - Complex<double> (0, 1) * amp[1815]
      + amp[1817]);
  jamp[1] = +1./2. * (-1./3. * amp[1798] - 1./3. * amp[1799] - amp[1800] -
      amp[1801] - amp[1806] - amp[1807] - 1./3. * amp[1808] - 1./3. * amp[1809]
      - 1./3. * amp[1816] - 1./3. * amp[1817] - Complex<double> (0, 1) *
      amp[1818] - Complex<double> (0, 1) * amp[1819] - amp[1820]);
  jamp[2] = +1./2. * (+amp[1798] + amp[1799] + 1./3. * amp[1800] + 1./3. *
      amp[1801] + amp[1802] + amp[1803] + 1./3. * amp[1804] + 1./3. * amp[1805]
      + Complex<double> (0, 1) * amp[1814] + Complex<double> (0, 1) * amp[1815]
      + amp[1816] + 1./3. * amp[1820] + 1./3. * amp[1821]);
  jamp[3] = +1./2. * (-1./3. * amp[1802] - 1./3. * amp[1803] - amp[1804] -
      amp[1805] - 1./3. * amp[1810] - 1./3. * amp[1811] - amp[1812] - amp[1813]
      + Complex<double> (0, 1) * amp[1818] + Complex<double> (0, 1) * amp[1819]
      - amp[1821]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[37][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_udx_vexvegudx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[1826] + amp[1827] + amp[1828] + amp[1829] +
      amp[1834] + amp[1835] + amp[1836] + amp[1837] + Complex<double> (0, 1) *
      amp[1838] + Complex<double> (0, 1) * amp[1839] + amp[1840] -
      Complex<double> (0, 1) * amp[1842] - Complex<double> (0, 1) * amp[1843] +
      amp[1845]);
  jamp[1] = +1./2. * (-1./3. * amp[1822] - 1./3. * amp[1823] - 1./3. *
      amp[1824] - 1./3. * amp[1825] - 1./3. * amp[1826] - 1./3. * amp[1827] -
      1./3. * amp[1828] - 1./3. * amp[1829] - 1./3. * amp[1844] - 1./3. *
      amp[1845]);
  jamp[2] = +1./2. * (+amp[1822] + amp[1823] + amp[1824] + amp[1825] +
      amp[1830] + amp[1831] + amp[1832] + amp[1833] - Complex<double> (0, 1) *
      amp[1838] - Complex<double> (0, 1) * amp[1839] + amp[1841] +
      Complex<double> (0, 1) * amp[1842] + Complex<double> (0, 1) * amp[1843] +
      amp[1844]);
  jamp[3] = +1./2. * (-1./3. * amp[1830] - 1./3. * amp[1831] - 1./3. *
      amp[1832] - 1./3. * amp[1833] - 1./3. * amp[1834] - 1./3. * amp[1835] -
      1./3. * amp[1836] - 1./3. * amp[1837] - 1./3. * amp[1840] - 1./3. *
      amp[1841]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[38][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_dd_vexemgud() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[1846] + amp[1847] + 1./3. * amp[1848] + 1./3. *
      amp[1849] + amp[1850] + amp[1851] + 1./3. * amp[1852] + 1./3. * amp[1853]
      - Complex<double> (0, 1) * amp[1862] - Complex<double> (0, 1) * amp[1863]
      + amp[1865] + 1./3. * amp[1868] + 1./3. * amp[1869]);
  jamp[1] = +1./2. * (-1./3. * amp[1850] - 1./3. * amp[1851] - amp[1852] -
      amp[1853] - 1./3. * amp[1858] - 1./3. * amp[1859] - amp[1860] - amp[1861]
      - 1./3. * amp[1864] - 1./3. * amp[1865] + Complex<double> (0, 1) *
      amp[1866] + Complex<double> (0, 1) * amp[1867] - amp[1869]);
  jamp[2] = +1./2. * (-1./3. * amp[1846] - 1./3. * amp[1847] - amp[1848] -
      amp[1849] - 1./3. * amp[1854] - 1./3. * amp[1855] - amp[1856] - amp[1857]
      - Complex<double> (0, 1) * amp[1866] - Complex<double> (0, 1) * amp[1867]
      - amp[1868]);
  jamp[3] = +1./2. * (+amp[1854] + amp[1855] + 1./3. * amp[1856] + 1./3. *
      amp[1857] + amp[1858] + amp[1859] + 1./3. * amp[1860] + 1./3. * amp[1861]
      + Complex<double> (0, 1) * amp[1862] + Complex<double> (0, 1) * amp[1863]
      + amp[1864]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[39][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_ds_vexvegds() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[1870] + 1./3. * amp[1871] + 1./3. *
      amp[1872] + 1./3. * amp[1873] + 1./3. * amp[1874] + 1./3. * amp[1875] +
      1./3. * amp[1876] + 1./3. * amp[1877] + 1./3. * amp[1892] + 1./3. *
      amp[1893]);
  jamp[1] = +1./2. * (-amp[1874] - amp[1875] - amp[1876] - amp[1877] -
      amp[1882] - amp[1883] - amp[1884] - amp[1885] - Complex<double> (0, 1) *
      amp[1886] - Complex<double> (0, 1) * amp[1887] - amp[1888] +
      Complex<double> (0, 1) * amp[1890] + Complex<double> (0, 1) * amp[1891] -
      amp[1893]);
  jamp[2] = +1./2. * (-amp[1870] - amp[1871] - amp[1872] - amp[1873] -
      amp[1878] - amp[1879] - amp[1880] - amp[1881] + Complex<double> (0, 1) *
      amp[1886] + Complex<double> (0, 1) * amp[1887] - amp[1889] -
      Complex<double> (0, 1) * amp[1890] - Complex<double> (0, 1) * amp[1891] -
      amp[1892]);
  jamp[3] = +1./2. * (+1./3. * amp[1878] + 1./3. * amp[1879] + 1./3. *
      amp[1880] + 1./3. * amp[1881] + 1./3. * amp[1882] + 1./3. * amp[1883] +
      1./3. * amp[1884] + 1./3. * amp[1885] + 1./3. * amp[1888] + 1./3. *
      amp[1889]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[40][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_dux_vexemguux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[1894] + amp[1895] + 1./3. * amp[1896] + 1./3. *
      amp[1897] + amp[1902] + amp[1903] + 1./3. * amp[1904] + 1./3. * amp[1905]
      - Complex<double> (0, 1) * amp[1910] - Complex<double> (0, 1) * amp[1911]
      + amp[1913]);
  jamp[1] = +1./2. * (-1./3. * amp[1902] - 1./3. * amp[1903] - amp[1904] -
      amp[1905] - 1./3. * amp[1906] - 1./3. * amp[1907] - amp[1908] - amp[1909]
      - 1./3. * amp[1912] - 1./3. * amp[1913] - Complex<double> (0, 1) *
      amp[1914] - Complex<double> (0, 1) * amp[1915] - amp[1916]);
  jamp[2] = +1./2. * (+1./3. * amp[1898] + 1./3. * amp[1899] + amp[1900] +
      amp[1901] + amp[1906] + amp[1907] + 1./3. * amp[1908] + 1./3. * amp[1909]
      + Complex<double> (0, 1) * amp[1910] + Complex<double> (0, 1) * amp[1911]
      + amp[1912] + 1./3. * amp[1916] + 1./3. * amp[1917]);
  jamp[3] = +1./2. * (-1./3. * amp[1894] - 1./3. * amp[1895] - amp[1896] -
      amp[1897] - amp[1898] - amp[1899] - 1./3. * amp[1900] - 1./3. * amp[1901]
      + Complex<double> (0, 1) * amp[1914] + Complex<double> (0, 1) * amp[1915]
      - amp[1917]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[41][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_dux_vexemgddx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[1926] + amp[1927] + 1./3. * amp[1928] + 1./3. *
      amp[1929] + amp[1930] + amp[1931] + 1./3. * amp[1932] + 1./3. * amp[1933]
      + Complex<double> (0, 1) * amp[1934] + Complex<double> (0, 1) * amp[1935]
      + amp[1936]);
  jamp[1] = +1./2. * (-1./3. * amp[1918] - 1./3. * amp[1919] - amp[1920] -
      amp[1921] - 1./3. * amp[1926] - 1./3. * amp[1927] - amp[1928] - amp[1929]
      - Complex<double> (0, 1) * amp[1938] - Complex<double> (0, 1) * amp[1939]
      - amp[1940]);
  jamp[2] = +1./2. * (+amp[1918] + amp[1919] + 1./3. * amp[1920] + 1./3. *
      amp[1921] + amp[1922] + amp[1923] + 1./3. * amp[1924] + 1./3. * amp[1925]
      - Complex<double> (0, 1) * amp[1934] - Complex<double> (0, 1) * amp[1935]
      + amp[1937] + 1./3. * amp[1940] + 1./3. * amp[1941]);
  jamp[3] = +1./2. * (-1./3. * amp[1922] - 1./3. * amp[1923] - amp[1924] -
      amp[1925] - 1./3. * amp[1930] - 1./3. * amp[1931] - amp[1932] - amp[1933]
      - 1./3. * amp[1936] - 1./3. * amp[1937] + Complex<double> (0, 1) *
      amp[1938] + Complex<double> (0, 1) * amp[1939] - amp[1941]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[42][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_dux_vexvegdux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[1942] + amp[1943] + amp[1944] + amp[1945] +
      amp[1950] + amp[1951] + amp[1952] + amp[1953] - Complex<double> (0, 1) *
      amp[1958] - Complex<double> (0, 1) * amp[1959] + amp[1961] +
      Complex<double> (0, 1) * amp[1962] + Complex<double> (0, 1) * amp[1963] +
      amp[1964]);
  jamp[1] = +1./2. * (-1./3. * amp[1950] - 1./3. * amp[1951] - 1./3. *
      amp[1952] - 1./3. * amp[1953] - 1./3. * amp[1954] - 1./3. * amp[1955] -
      1./3. * amp[1956] - 1./3. * amp[1957] - 1./3. * amp[1960] - 1./3. *
      amp[1961]);
  jamp[2] = +1./2. * (+amp[1946] + amp[1947] + amp[1948] + amp[1949] +
      amp[1954] + amp[1955] + amp[1956] + amp[1957] + Complex<double> (0, 1) *
      amp[1958] + Complex<double> (0, 1) * amp[1959] + amp[1960] -
      Complex<double> (0, 1) * amp[1962] - Complex<double> (0, 1) * amp[1963] +
      amp[1965]);
  jamp[3] = +1./2. * (-1./3. * amp[1942] - 1./3. * amp[1943] - 1./3. *
      amp[1944] - 1./3. * amp[1945] - 1./3. * amp[1946] - 1./3. * amp[1947] -
      1./3. * amp[1948] - 1./3. * amp[1949] - 1./3. * amp[1964] - 1./3. *
      amp[1965]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[43][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_ddx_epvegdux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[1966] + 1./3. * amp[1967] + amp[1968] +
      amp[1969] + amp[1974] + amp[1975] + 1./3. * amp[1976] + 1./3. * amp[1977]
      + 1./3. * amp[1984] + 1./3. * amp[1985] + Complex<double> (0, 1) *
      amp[1986] + Complex<double> (0, 1) * amp[1987] + amp[1988]);
  jamp[1] = +1./2. * (-1./3. * amp[1974] - 1./3. * amp[1975] - amp[1976] -
      amp[1977] - amp[1978] - amp[1979] - 1./3. * amp[1980] - 1./3. * amp[1981]
      + Complex<double> (0, 1) * amp[1982] + Complex<double> (0, 1) * amp[1983]
      - amp[1985]);
  jamp[2] = +1./2. * (+1./3. * amp[1970] + 1./3. * amp[1971] + amp[1972] +
      amp[1973] + 1./3. * amp[1978] + 1./3. * amp[1979] + amp[1980] + amp[1981]
      - Complex<double> (0, 1) * amp[1986] - Complex<double> (0, 1) * amp[1987]
      + amp[1989]);
  jamp[3] = +1./2. * (-amp[1966] - amp[1967] - 1./3. * amp[1968] - 1./3. *
      amp[1969] - amp[1970] - amp[1971] - 1./3. * amp[1972] - 1./3. * amp[1973]
      - Complex<double> (0, 1) * amp[1982] - Complex<double> (0, 1) * amp[1983]
      - amp[1984] - 1./3. * amp[1988] - 1./3. * amp[1989]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[44][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_ddx_vexemgudx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[1994] + 1./3. * amp[1995] + amp[1996] +
      amp[1997] + 1./3. * amp[2002] + 1./3. * amp[2003] + amp[2004] + amp[2005]
      + 1./3. * amp[2008] + 1./3. * amp[2009] - Complex<double> (0, 1) *
      amp[2010] - Complex<double> (0, 1) * amp[2011] + amp[2013]);
  jamp[1] = +1./2. * (-amp[1990] - amp[1991] - 1./3. * amp[1992] - 1./3. *
      amp[1993] - amp[1994] - amp[1995] - 1./3. * amp[1996] - 1./3. * amp[1997]
      + Complex<double> (0, 1) * amp[2006] + Complex<double> (0, 1) * amp[2007]
      - amp[2009] - 1./3. * amp[2012] - 1./3. * amp[2013]);
  jamp[2] = +1./2. * (+1./3. * amp[1990] + 1./3. * amp[1991] + amp[1992] +
      amp[1993] + 1./3. * amp[1998] + 1./3. * amp[1999] + amp[2000] + amp[2001]
      + Complex<double> (0, 1) * amp[2010] + Complex<double> (0, 1) * amp[2011]
      + amp[2012]);
  jamp[3] = +1./2. * (-amp[1998] - amp[1999] - 1./3. * amp[2000] - 1./3. *
      amp[2001] - amp[2002] - amp[2003] - 1./3. * amp[2004] - 1./3. * amp[2005]
      - Complex<double> (0, 1) * amp[2006] - Complex<double> (0, 1) * amp[2007]
      - amp[2008]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[45][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_ddx_vexveguux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[2014] + 1./3. * amp[2015] + 1./3. *
      amp[2016] + 1./3. * amp[2017] + 1./3. * amp[2018] + 1./3. * amp[2019] +
      1./3. * amp[2020] + 1./3. * amp[2021] + 1./3. * amp[2036] + 1./3. *
      amp[2037]);
  jamp[1] = +1./2. * (-amp[2018] - amp[2019] - amp[2020] - amp[2021] -
      amp[2026] - amp[2027] - amp[2028] - amp[2029] - Complex<double> (0, 1) *
      amp[2030] - Complex<double> (0, 1) * amp[2031] - amp[2032] +
      Complex<double> (0, 1) * amp[2034] + Complex<double> (0, 1) * amp[2035] -
      amp[2037]);
  jamp[2] = +1./2. * (+1./3. * amp[2022] + 1./3. * amp[2023] + 1./3. *
      amp[2024] + 1./3. * amp[2025] + 1./3. * amp[2026] + 1./3. * amp[2027] +
      1./3. * amp[2028] + 1./3. * amp[2029] + 1./3. * amp[2032] + 1./3. *
      amp[2033]);
  jamp[3] = +1./2. * (-amp[2014] - amp[2015] - amp[2016] - amp[2017] -
      amp[2022] - amp[2023] - amp[2024] - amp[2025] + Complex<double> (0, 1) *
      amp[2030] + Complex<double> (0, 1) * amp[2031] - amp[2033] -
      Complex<double> (0, 1) * amp[2034] - Complex<double> (0, 1) * amp[2035] -
      amp[2036]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[46][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_ddx_vexvegssx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[2046] + 1./3. * amp[2047] + 1./3. *
      amp[2048] + 1./3. * amp[2049] + 1./3. * amp[2050] + 1./3. * amp[2051] +
      1./3. * amp[2052] + 1./3. * amp[2053] + 1./3. * amp[2056] + 1./3. *
      amp[2057]);
  jamp[1] = +1./2. * (-amp[2038] - amp[2039] - amp[2040] - amp[2041] -
      amp[2046] - amp[2047] - amp[2048] - amp[2049] + Complex<double> (0, 1) *
      amp[2054] + Complex<double> (0, 1) * amp[2055] - amp[2057] -
      Complex<double> (0, 1) * amp[2058] - Complex<double> (0, 1) * amp[2059] -
      amp[2060]);
  jamp[2] = +1./2. * (+1./3. * amp[2038] + 1./3. * amp[2039] + 1./3. *
      amp[2040] + 1./3. * amp[2041] + 1./3. * amp[2042] + 1./3. * amp[2043] +
      1./3. * amp[2044] + 1./3. * amp[2045] + 1./3. * amp[2060] + 1./3. *
      amp[2061]);
  jamp[3] = +1./2. * (-amp[2042] - amp[2043] - amp[2044] - amp[2045] -
      amp[2050] - amp[2051] - amp[2052] - amp[2053] - Complex<double> (0, 1) *
      amp[2054] - Complex<double> (0, 1) * amp[2055] - amp[2056] +
      Complex<double> (0, 1) * amp[2058] + Complex<double> (0, 1) * amp[2059] -
      amp[2061]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[47][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_dsx_vexvegdsx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[2066] + amp[2067] + amp[2068] + amp[2069] +
      amp[2074] + amp[2075] + amp[2076] + amp[2077] + Complex<double> (0, 1) *
      amp[2078] + Complex<double> (0, 1) * amp[2079] + amp[2080] -
      Complex<double> (0, 1) * amp[2082] - Complex<double> (0, 1) * amp[2083] +
      amp[2085]);
  jamp[1] = +1./2. * (-1./3. * amp[2062] - 1./3. * amp[2063] - 1./3. *
      amp[2064] - 1./3. * amp[2065] - 1./3. * amp[2066] - 1./3. * amp[2067] -
      1./3. * amp[2068] - 1./3. * amp[2069] - 1./3. * amp[2084] - 1./3. *
      amp[2085]);
  jamp[2] = +1./2. * (+amp[2062] + amp[2063] + amp[2064] + amp[2065] +
      amp[2070] + amp[2071] + amp[2072] + amp[2073] - Complex<double> (0, 1) *
      amp[2078] - Complex<double> (0, 1) * amp[2079] + amp[2081] +
      Complex<double> (0, 1) * amp[2082] + Complex<double> (0, 1) * amp[2083] +
      amp[2084]);
  jamp[3] = +1./2. * (-1./3. * amp[2070] - 1./3. * amp[2071] - 1./3. *
      amp[2072] - 1./3. * amp[2073] - 1./3. * amp[2074] - 1./3. * amp[2075] -
      1./3. * amp[2076] - 1./3. * amp[2077] - 1./3. * amp[2080] - 1./3. *
      amp[2081]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[48][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_uxux_vexemguxdx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[2094] + 1./3. * amp[2095] + amp[2096] +
      amp[2097] + 1./3. * amp[2098] + 1./3. * amp[2099] + amp[2100] + amp[2101]
      + 1./3. * amp[2104] + 1./3. * amp[2105] + Complex<double> (0, 1) *
      amp[2106] + Complex<double> (0, 1) * amp[2107] + amp[2108]);
  jamp[1] = +1./2. * (-amp[2086] - amp[2087] - 1./3. * amp[2088] - 1./3. *
      amp[2089] - amp[2094] - amp[2095] - 1./3. * amp[2096] - 1./3. * amp[2097]
      + Complex<double> (0, 1) * amp[2102] + Complex<double> (0, 1) * amp[2103]
      - amp[2105]);
  jamp[2] = +1./2. * (-1./3. * amp[2090] - 1./3. * amp[2091] - amp[2092] -
      amp[2093] - amp[2098] - amp[2099] - 1./3. * amp[2100] - 1./3. * amp[2101]
      - Complex<double> (0, 1) * amp[2102] - Complex<double> (0, 1) * amp[2103]
      - amp[2104] - 1./3. * amp[2108] - 1./3. * amp[2109]);
  jamp[3] = +1./2. * (+1./3. * amp[2086] + 1./3. * amp[2087] + amp[2088] +
      amp[2089] + amp[2090] + amp[2091] + 1./3. * amp[2092] + 1./3. * amp[2093]
      - Complex<double> (0, 1) * amp[2106] - Complex<double> (0, 1) * amp[2107]
      + amp[2109]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[49][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_uxcx_vexveguxcx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[2118] + 1./3. * amp[2119] + 1./3. *
      amp[2120] + 1./3. * amp[2121] + 1./3. * amp[2122] + 1./3. * amp[2123] +
      1./3. * amp[2124] + 1./3. * amp[2125] + 1./3. * amp[2128] + 1./3. *
      amp[2129]);
  jamp[1] = +1./2. * (-amp[2110] - amp[2111] - amp[2112] - amp[2113] -
      amp[2118] - amp[2119] - amp[2120] - amp[2121] + Complex<double> (0, 1) *
      amp[2126] + Complex<double> (0, 1) * amp[2127] - amp[2129] -
      Complex<double> (0, 1) * amp[2130] - Complex<double> (0, 1) * amp[2131] -
      amp[2132]);
  jamp[2] = +1./2. * (-amp[2114] - amp[2115] - amp[2116] - amp[2117] -
      amp[2122] - amp[2123] - amp[2124] - amp[2125] - Complex<double> (0, 1) *
      amp[2126] - Complex<double> (0, 1) * amp[2127] - amp[2128] +
      Complex<double> (0, 1) * amp[2130] + Complex<double> (0, 1) * amp[2131] -
      amp[2133]);
  jamp[3] = +1./2. * (+1./3. * amp[2110] + 1./3. * amp[2111] + 1./3. *
      amp[2112] + 1./3. * amp[2113] + 1./3. * amp[2114] + 1./3. * amp[2115] +
      1./3. * amp[2116] + 1./3. * amp[2117] + 1./3. * amp[2132] + 1./3. *
      amp[2133]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[50][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_uxdx_epveguxux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[2142] + 1./3. * amp[2143] + amp[2144] +
      amp[2145] + 1./3. * amp[2146] + 1./3. * amp[2147] + amp[2148] + amp[2149]
      + 1./3. * amp[2152] + 1./3. * amp[2153] - Complex<double> (0, 1) *
      amp[2154] - Complex<double> (0, 1) * amp[2155] + amp[2157]);
  jamp[1] = +1./2. * (-amp[2134] - amp[2135] - 1./3. * amp[2136] - 1./3. *
      amp[2137] - amp[2142] - amp[2143] - 1./3. * amp[2144] - 1./3. * amp[2145]
      + Complex<double> (0, 1) * amp[2150] + Complex<double> (0, 1) * amp[2151]
      - amp[2153] - 1./3. * amp[2156] - 1./3. * amp[2157]);
  jamp[2] = +1./2. * (-amp[2138] - amp[2139] - 1./3. * amp[2140] - 1./3. *
      amp[2141] - amp[2146] - amp[2147] - 1./3. * amp[2148] - 1./3. * amp[2149]
      - Complex<double> (0, 1) * amp[2150] - Complex<double> (0, 1) * amp[2151]
      - amp[2152]);
  jamp[3] = +1./2. * (+1./3. * amp[2134] + 1./3. * amp[2135] + amp[2136] +
      amp[2137] + 1./3. * amp[2138] + 1./3. * amp[2139] + amp[2140] + amp[2141]
      + Complex<double> (0, 1) * amp[2154] + Complex<double> (0, 1) * amp[2155]
      + amp[2156]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[51][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_uxdx_vexemgdxdx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[2166] + amp[2167] + 1./3. * amp[2168] + 1./3. *
      amp[2169] + amp[2170] + amp[2171] + 1./3. * amp[2172] + 1./3. * amp[2173]
      + Complex<double> (0, 1) * amp[2174] + Complex<double> (0, 1) * amp[2175]
      + amp[2176]);
  jamp[1] = +1./2. * (-1./3. * amp[2158] - 1./3. * amp[2159] - amp[2160] -
      amp[2161] - 1./3. * amp[2166] - 1./3. * amp[2167] - amp[2168] - amp[2169]
      - Complex<double> (0, 1) * amp[2178] - Complex<double> (0, 1) * amp[2179]
      - amp[2180]);
  jamp[2] = +1./2. * (-1./3. * amp[2162] - 1./3. * amp[2163] - amp[2164] -
      amp[2165] - 1./3. * amp[2170] - 1./3. * amp[2171] - amp[2172] - amp[2173]
      - 1./3. * amp[2176] - 1./3. * amp[2177] + Complex<double> (0, 1) *
      amp[2178] + Complex<double> (0, 1) * amp[2179] - amp[2181]);
  jamp[3] = +1./2. * (+amp[2158] + amp[2159] + 1./3. * amp[2160] + 1./3. *
      amp[2161] + amp[2162] + amp[2163] + 1./3. * amp[2164] + 1./3. * amp[2165]
      - Complex<double> (0, 1) * amp[2174] - Complex<double> (0, 1) * amp[2175]
      + amp[2177] + 1./3. * amp[2180] + 1./3. * amp[2181]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[52][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_uxdx_vexveguxdx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[2190] + 1./3. * amp[2191] + 1./3. *
      amp[2192] + 1./3. * amp[2193] + 1./3. * amp[2194] + 1./3. * amp[2195] +
      1./3. * amp[2196] + 1./3. * amp[2197] + 1./3. * amp[2200] + 1./3. *
      amp[2201]);
  jamp[1] = +1./2. * (-amp[2182] - amp[2183] - amp[2184] - amp[2185] -
      amp[2190] - amp[2191] - amp[2192] - amp[2193] + Complex<double> (0, 1) *
      amp[2198] + Complex<double> (0, 1) * amp[2199] - amp[2201] -
      Complex<double> (0, 1) * amp[2202] - Complex<double> (0, 1) * amp[2203] -
      amp[2204]);
  jamp[2] = +1./2. * (-amp[2186] - amp[2187] - amp[2188] - amp[2189] -
      amp[2194] - amp[2195] - amp[2196] - amp[2197] - Complex<double> (0, 1) *
      amp[2198] - Complex<double> (0, 1) * amp[2199] - amp[2200] +
      Complex<double> (0, 1) * amp[2202] + Complex<double> (0, 1) * amp[2203] -
      amp[2205]);
  jamp[3] = +1./2. * (+1./3. * amp[2182] + 1./3. * amp[2183] + 1./3. *
      amp[2184] + 1./3. * amp[2185] + 1./3. * amp[2186] + 1./3. * amp[2187] +
      1./3. * amp[2188] + 1./3. * amp[2189] + 1./3. * amp[2204] + 1./3. *
      amp[2205]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[53][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_dxdx_epveguxdx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[2214] + 1./3. * amp[2215] + amp[2216] +
      amp[2217] + amp[2218] + amp[2219] + 1./3. * amp[2220] + 1./3. * amp[2221]
      - Complex<double> (0, 1) * amp[2222] - Complex<double> (0, 1) * amp[2223]
      + amp[2225]);
  jamp[1] = +1./2. * (-1./3. * amp[2206] - 1./3. * amp[2207] - amp[2208] -
      amp[2209] - amp[2214] - amp[2215] - 1./3. * amp[2216] - 1./3. * amp[2217]
      - 1./3. * amp[2224] - 1./3. * amp[2225] - Complex<double> (0, 1) *
      amp[2226] - Complex<double> (0, 1) * amp[2227] - amp[2228]);
  jamp[2] = +1./2. * (-1./3. * amp[2210] - 1./3. * amp[2211] - amp[2212] -
      amp[2213] - 1./3. * amp[2218] - 1./3. * amp[2219] - amp[2220] - amp[2221]
      + Complex<double> (0, 1) * amp[2226] + Complex<double> (0, 1) * amp[2227]
      - amp[2229]);
  jamp[3] = +1./2. * (+amp[2206] + amp[2207] + 1./3. * amp[2208] + 1./3. *
      amp[2209] + amp[2210] + amp[2211] + 1./3. * amp[2212] + 1./3. * amp[2213]
      + Complex<double> (0, 1) * amp[2222] + Complex<double> (0, 1) * amp[2223]
      + amp[2224] + 1./3. * amp[2228] + 1./3. * amp[2229]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[54][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_dxsx_vexvegdxsx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[2238] + 1./3. * amp[2239] + 1./3. *
      amp[2240] + 1./3. * amp[2241] + 1./3. * amp[2242] + 1./3. * amp[2243] +
      1./3. * amp[2244] + 1./3. * amp[2245] + 1./3. * amp[2248] + 1./3. *
      amp[2249]);
  jamp[1] = +1./2. * (-amp[2230] - amp[2231] - amp[2232] - amp[2233] -
      amp[2238] - amp[2239] - amp[2240] - amp[2241] + Complex<double> (0, 1) *
      amp[2246] + Complex<double> (0, 1) * amp[2247] - amp[2249] -
      Complex<double> (0, 1) * amp[2250] - Complex<double> (0, 1) * amp[2251] -
      amp[2252]);
  jamp[2] = +1./2. * (-amp[2234] - amp[2235] - amp[2236] - amp[2237] -
      amp[2242] - amp[2243] - amp[2244] - amp[2245] - Complex<double> (0, 1) *
      amp[2246] - Complex<double> (0, 1) * amp[2247] - amp[2248] +
      Complex<double> (0, 1) * amp[2250] + Complex<double> (0, 1) * amp[2251] -
      amp[2253]);
  jamp[3] = +1./2. * (+1./3. * amp[2230] + 1./3. * amp[2231] + 1./3. *
      amp[2232] + 1./3. * amp[2233] + 1./3. * amp[2234] + 1./3. * amp[2235] +
      1./3. * amp[2236] + 1./3. * amp[2237] + 1./3. * amp[2252] + 1./3. *
      amp[2253]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[55][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_uc_epvegus() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 12;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[2254] + 1./3. * amp[2255] + 1./3. *
      amp[2256] + 1./3. * amp[2257]);
  jamp[1] = +1./2. * (-amp[2256] - amp[2257] - amp[2260] - amp[2261] -
      Complex<double> (0, 1) * amp[2262] - Complex<double> (0, 1) * amp[2263] -
      amp[2264]);
  jamp[2] = +1./2. * (-amp[2254] - amp[2255] - amp[2258] - amp[2259] +
      Complex<double> (0, 1) * amp[2262] + Complex<double> (0, 1) * amp[2263] -
      amp[2265]);
  jamp[3] = +1./2. * (+1./3. * amp[2258] + 1./3. * amp[2259] + 1./3. *
      amp[2260] + 1./3. * amp[2261] + 1./3. * amp[2264] + 1./3. * amp[2265]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[56][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_uc_epvegcd() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 12;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[2266] + amp[2267] + amp[2268] + amp[2269] +
      Complex<double> (0, 1) * amp[2274] + Complex<double> (0, 1) * amp[2275] +
      amp[2276]);
  jamp[1] = +1./2. * (-1./3. * amp[2268] - 1./3. * amp[2269] - 1./3. *
      amp[2272] - 1./3. * amp[2273]);
  jamp[2] = +1./2. * (-1./3. * amp[2266] - 1./3. * amp[2267] - 1./3. *
      amp[2270] - 1./3. * amp[2271] - 1./3. * amp[2276] - 1./3. * amp[2277]);
  jamp[3] = +1./2. * (+amp[2270] + amp[2271] + amp[2272] + amp[2273] -
      Complex<double> (0, 1) * amp[2274] - Complex<double> (0, 1) * amp[2275] +
      amp[2277]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[57][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_us_vexemguc() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 12;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[2278] + 1./3. * amp[2279] + 1./3. *
      amp[2280] + 1./3. * amp[2281]);
  jamp[1] = +1./2. * (-amp[2280] - amp[2281] - amp[2284] - amp[2285] -
      Complex<double> (0, 1) * amp[2286] - Complex<double> (0, 1) * amp[2287] -
      amp[2288]);
  jamp[2] = +1./2. * (-amp[2278] - amp[2279] - amp[2282] - amp[2283] +
      Complex<double> (0, 1) * amp[2286] + Complex<double> (0, 1) * amp[2287] -
      amp[2289]);
  jamp[3] = +1./2. * (+1./3. * amp[2282] + 1./3. * amp[2283] + 1./3. *
      amp[2284] + 1./3. * amp[2285] + 1./3. * amp[2288] + 1./3. * amp[2289]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[58][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_uux_epvegscx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 12;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[2294] + 1./3. * amp[2295] + 1./3. *
      amp[2296] + 1./3. * amp[2297] + 1./3. * amp[2300] + 1./3. * amp[2301]);
  jamp[1] = +1./2. * (-amp[2290] - amp[2291] - amp[2294] - amp[2295] +
      Complex<double> (0, 1) * amp[2298] + Complex<double> (0, 1) * amp[2299] -
      amp[2301]);
  jamp[2] = +1./2. * (+1./3. * amp[2290] + 1./3. * amp[2291] + 1./3. *
      amp[2292] + 1./3. * amp[2293]);
  jamp[3] = +1./2. * (-amp[2292] - amp[2293] - amp[2296] - amp[2297] -
      Complex<double> (0, 1) * amp[2298] - Complex<double> (0, 1) * amp[2299] -
      amp[2300]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[59][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_uux_vexemgcsx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 12;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[2306] + 1./3. * amp[2307] + 1./3. *
      amp[2308] + 1./3. * amp[2309] + 1./3. * amp[2312] + 1./3. * amp[2313]);
  jamp[1] = +1./2. * (-amp[2302] - amp[2303] - amp[2306] - amp[2307] +
      Complex<double> (0, 1) * amp[2310] + Complex<double> (0, 1) * amp[2311] -
      amp[2313]);
  jamp[2] = +1./2. * (+1./3. * amp[2302] + 1./3. * amp[2303] + 1./3. *
      amp[2304] + 1./3. * amp[2305]);
  jamp[3] = +1./2. * (-amp[2304] - amp[2305] - amp[2308] - amp[2309] -
      Complex<double> (0, 1) * amp[2310] - Complex<double> (0, 1) * amp[2311] -
      amp[2312]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[60][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_ucx_epvegdcx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 12;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[2318] + amp[2319] + amp[2320] + amp[2321] -
      Complex<double> (0, 1) * amp[2322] - Complex<double> (0, 1) * amp[2323] +
      amp[2325]);
  jamp[1] = +1./2. * (-1./3. * amp[2314] - 1./3. * amp[2315] - 1./3. *
      amp[2318] - 1./3. * amp[2319] - 1./3. * amp[2324] - 1./3. * amp[2325]);
  jamp[2] = +1./2. * (+amp[2314] + amp[2315] + amp[2316] + amp[2317] +
      Complex<double> (0, 1) * amp[2322] + Complex<double> (0, 1) * amp[2323] +
      amp[2324]);
  jamp[3] = +1./2. * (-1./3. * amp[2316] - 1./3. * amp[2317] - 1./3. *
      amp[2320] - 1./3. * amp[2321]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[61][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_ucx_vexemgusx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 12;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[2328] + amp[2329] + amp[2332] + amp[2333] +
      Complex<double> (0, 1) * amp[2334] + Complex<double> (0, 1) * amp[2335] +
      amp[2336]);
  jamp[1] = +1./2. * (-1./3. * amp[2326] - 1./3. * amp[2327] - 1./3. *
      amp[2328] - 1./3. * amp[2329]);
  jamp[2] = +1./2. * (+amp[2326] + amp[2327] + amp[2330] + amp[2331] -
      Complex<double> (0, 1) * amp[2334] - Complex<double> (0, 1) * amp[2335] +
      amp[2337]);
  jamp[3] = +1./2. * (-1./3. * amp[2330] - 1./3. * amp[2331] - 1./3. *
      amp[2332] - 1./3. * amp[2333] - 1./3. * amp[2336] - 1./3. * amp[2337]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[62][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_udx_epvegccx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 12;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[2340] + 1./3. * amp[2341] + 1./3. *
      amp[2344] + 1./3. * amp[2345]);
  jamp[1] = +1./2. * (-amp[2338] - amp[2339] - amp[2340] - amp[2341] -
      Complex<double> (0, 1) * amp[2346] - Complex<double> (0, 1) * amp[2347] -
      amp[2348]);
  jamp[2] = +1./2. * (+1./3. * amp[2338] + 1./3. * amp[2339] + 1./3. *
      amp[2342] + 1./3. * amp[2343] + 1./3. * amp[2348] + 1./3. * amp[2349]);
  jamp[3] = +1./2. * (-amp[2342] - amp[2343] - amp[2344] - amp[2345] +
      Complex<double> (0, 1) * amp[2346] + Complex<double> (0, 1) * amp[2347] -
      amp[2349]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[63][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_usx_epvegucx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 12;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[2352] + amp[2353] + amp[2356] + amp[2357] +
      Complex<double> (0, 1) * amp[2358] + Complex<double> (0, 1) * amp[2359] +
      amp[2360]);
  jamp[1] = +1./2. * (-1./3. * amp[2350] - 1./3. * amp[2351] - 1./3. *
      amp[2352] - 1./3. * amp[2353]);
  jamp[2] = +1./2. * (+amp[2350] + amp[2351] + amp[2354] + amp[2355] -
      Complex<double> (0, 1) * amp[2358] - Complex<double> (0, 1) * amp[2359] +
      amp[2361]);
  jamp[3] = +1./2. * (-1./3. * amp[2354] - 1./3. * amp[2355] - 1./3. *
      amp[2356] - 1./3. * amp[2357] - 1./3. * amp[2360] - 1./3. * amp[2361]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[64][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_ds_vexemgus() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 12;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[2362] + 1./3. * amp[2363] + 1./3. *
      amp[2364] + 1./3. * amp[2365] + 1./3. * amp[2372] + 1./3. * amp[2373]);
  jamp[1] = +1./2. * (-amp[2364] - amp[2365] - amp[2368] - amp[2369] +
      Complex<double> (0, 1) * amp[2370] + Complex<double> (0, 1) * amp[2371] -
      amp[2373]);
  jamp[2] = +1./2. * (-amp[2362] - amp[2363] - amp[2366] - amp[2367] -
      Complex<double> (0, 1) * amp[2370] - Complex<double> (0, 1) * amp[2371] -
      amp[2372]);
  jamp[3] = +1./2. * (+1./3. * amp[2366] + 1./3. * amp[2367] + 1./3. *
      amp[2368] + 1./3. * amp[2369]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[65][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_dux_vexemgccx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 12;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[2374] + 1./3. * amp[2375] + 1./3. *
      amp[2378] + 1./3. * amp[2379]);
  jamp[1] = +1./2. * (-amp[2378] - amp[2379] - amp[2380] - amp[2381] -
      Complex<double> (0, 1) * amp[2382] - Complex<double> (0, 1) * amp[2383] -
      amp[2384]);
  jamp[2] = +1./2. * (+1./3. * amp[2376] + 1./3. * amp[2377] + 1./3. *
      amp[2380] + 1./3. * amp[2381] + 1./3. * amp[2384] + 1./3. * amp[2385]);
  jamp[3] = +1./2. * (-amp[2374] - amp[2375] - amp[2376] - amp[2377] +
      Complex<double> (0, 1) * amp[2382] + Complex<double> (0, 1) * amp[2383] -
      amp[2385]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[66][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_dcx_vexemgucx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 12;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[2386] + amp[2387] + amp[2388] + amp[2389] -
      Complex<double> (0, 1) * amp[2394] - Complex<double> (0, 1) * amp[2395] +
      amp[2397]);
  jamp[1] = +1./2. * (-1./3. * amp[2388] - 1./3. * amp[2389] - 1./3. *
      amp[2392] - 1./3. * amp[2393] - 1./3. * amp[2396] - 1./3. * amp[2397]);
  jamp[2] = +1./2. * (+amp[2390] + amp[2391] + amp[2392] + amp[2393] +
      Complex<double> (0, 1) * amp[2394] + Complex<double> (0, 1) * amp[2395] +
      amp[2396]);
  jamp[3] = +1./2. * (-1./3. * amp[2386] - 1./3. * amp[2387] - 1./3. *
      amp[2390] - 1./3. * amp[2391]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[67][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_uxcx_vexemguxsx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 12;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[2402] + 1./3. * amp[2403] + 1./3. *
      amp[2404] + 1./3. * amp[2405] + 1./3. * amp[2408] + 1./3. * amp[2409]);
  jamp[1] = +1./2. * (-amp[2398] - amp[2399] - amp[2402] - amp[2403] +
      Complex<double> (0, 1) * amp[2406] + Complex<double> (0, 1) * amp[2407] -
      amp[2409]);
  jamp[2] = +1./2. * (-amp[2400] - amp[2401] - amp[2404] - amp[2405] -
      Complex<double> (0, 1) * amp[2406] - Complex<double> (0, 1) * amp[2407] -
      amp[2408]);
  jamp[3] = +1./2. * (+1./3. * amp[2398] + 1./3. * amp[2399] + 1./3. *
      amp[2400] + 1./3. * amp[2401]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[68][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_uxcx_vexemgcxdx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 12;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[2414] + amp[2415] + amp[2416] + amp[2417] +
      Complex<double> (0, 1) * amp[2418] + Complex<double> (0, 1) * amp[2419] +
      amp[2420]);
  jamp[1] = +1./2. * (-1./3. * amp[2410] - 1./3. * amp[2411] - 1./3. *
      amp[2414] - 1./3. * amp[2415]);
  jamp[2] = +1./2. * (-1./3. * amp[2412] - 1./3. * amp[2413] - 1./3. *
      amp[2416] - 1./3. * amp[2417] - 1./3. * amp[2420] - 1./3. * amp[2421]);
  jamp[3] = +1./2. * (+amp[2410] + amp[2411] + amp[2412] + amp[2413] -
      Complex<double> (0, 1) * amp[2418] - Complex<double> (0, 1) * amp[2419] +
      amp[2421]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[69][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_uxsx_epveguxcx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 12;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[2426] + 1./3. * amp[2427] + 1./3. *
      amp[2428] + 1./3. * amp[2429] + 1./3. * amp[2432] + 1./3. * amp[2433]);
  jamp[1] = +1./2. * (-amp[2422] - amp[2423] - amp[2426] - amp[2427] +
      Complex<double> (0, 1) * amp[2430] + Complex<double> (0, 1) * amp[2431] -
      amp[2433]);
  jamp[2] = +1./2. * (-amp[2424] - amp[2425] - amp[2428] - amp[2429] -
      Complex<double> (0, 1) * amp[2430] - Complex<double> (0, 1) * amp[2431] -
      amp[2432]);
  jamp[3] = +1./2. * (+1./3. * amp[2422] + 1./3. * amp[2423] + 1./3. *
      amp[2424] + 1./3. * amp[2425]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[70][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P76_sm_qq_llgqq::matrix_4_dxsx_epveguxsx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 12;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[2438] + 1./3. * amp[2439] + 1./3. *
      amp[2440] + 1./3. * amp[2441]);
  jamp[1] = +1./2. * (-amp[2434] - amp[2435] - amp[2438] - amp[2439] -
      Complex<double> (0, 1) * amp[2442] - Complex<double> (0, 1) * amp[2443] -
      amp[2444]);
  jamp[2] = +1./2. * (-amp[2436] - amp[2437] - amp[2440] - amp[2441] +
      Complex<double> (0, 1) * amp[2442] + Complex<double> (0, 1) * amp[2443] -
      amp[2445]);
  jamp[3] = +1./2. * (+1./3. * amp[2434] + 1./3. * amp[2435] + 1./3. *
      amp[2436] + 1./3. * amp[2437] + 1./3. * amp[2444] + 1./3. * amp[2445]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[71][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}


}  // end namespace PY8MEs_namespace

