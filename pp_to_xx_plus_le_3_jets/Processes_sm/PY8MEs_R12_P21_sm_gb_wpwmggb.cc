//==========================================================================
// This file has been automatically generated for Pythia 8 by
// MadGraph5_aMC@NLO v. 2.6.0, 2017-08-16
// By the MadGraph5_aMC@NLO Development Team
// Visit launchpad.net/madgraph5 and amcatnlo.web.cern.ch
//==========================================================================

#include "PY8MEs_R12_P21_sm_gb_wpwmggb.h"
#include "HelAmps_sm.h"

using namespace Pythia8_sm; 

namespace PY8MEs_namespace 
{
//==========================================================================
// Class member functions for calculating the matrix elements for
// Process: g b > w+ w- g g b WEIGHTED<=7 @12
// Process: g b~ > w+ w- g g b~ WEIGHTED<=7 @12

// Exception class
class PY8MEs_R12_P21_sm_gb_wpwmggbException : public exception
{
  virtual const char * what() const throw()
  {
    return "Exception in class 'PY8MEs_R12_P21_sm_gb_wpwmggb'."; 
  }
}
PY8MEs_R12_P21_sm_gb_wpwmggb_exception; 

std::set<int> PY8MEs_R12_P21_sm_gb_wpwmggb::s_channel_proc = std::set<int>
    (createset<int> ());

int PY8MEs_R12_P21_sm_gb_wpwmggb::helicities[ncomb][nexternal] = {{-1, -1, -1,
    -1, -1, -1, -1}, {-1, -1, -1, -1, -1, -1, 1}, {-1, -1, -1, -1, -1, 1, -1},
    {-1, -1, -1, -1, -1, 1, 1}, {-1, -1, -1, -1, 1, -1, -1}, {-1, -1, -1, -1,
    1, -1, 1}, {-1, -1, -1, -1, 1, 1, -1}, {-1, -1, -1, -1, 1, 1, 1}, {-1, -1,
    -1, 0, -1, -1, -1}, {-1, -1, -1, 0, -1, -1, 1}, {-1, -1, -1, 0, -1, 1, -1},
    {-1, -1, -1, 0, -1, 1, 1}, {-1, -1, -1, 0, 1, -1, -1}, {-1, -1, -1, 0, 1,
    -1, 1}, {-1, -1, -1, 0, 1, 1, -1}, {-1, -1, -1, 0, 1, 1, 1}, {-1, -1, -1,
    1, -1, -1, -1}, {-1, -1, -1, 1, -1, -1, 1}, {-1, -1, -1, 1, -1, 1, -1},
    {-1, -1, -1, 1, -1, 1, 1}, {-1, -1, -1, 1, 1, -1, -1}, {-1, -1, -1, 1, 1,
    -1, 1}, {-1, -1, -1, 1, 1, 1, -1}, {-1, -1, -1, 1, 1, 1, 1}, {-1, -1, 0,
    -1, -1, -1, -1}, {-1, -1, 0, -1, -1, -1, 1}, {-1, -1, 0, -1, -1, 1, -1},
    {-1, -1, 0, -1, -1, 1, 1}, {-1, -1, 0, -1, 1, -1, -1}, {-1, -1, 0, -1, 1,
    -1, 1}, {-1, -1, 0, -1, 1, 1, -1}, {-1, -1, 0, -1, 1, 1, 1}, {-1, -1, 0, 0,
    -1, -1, -1}, {-1, -1, 0, 0, -1, -1, 1}, {-1, -1, 0, 0, -1, 1, -1}, {-1, -1,
    0, 0, -1, 1, 1}, {-1, -1, 0, 0, 1, -1, -1}, {-1, -1, 0, 0, 1, -1, 1}, {-1,
    -1, 0, 0, 1, 1, -1}, {-1, -1, 0, 0, 1, 1, 1}, {-1, -1, 0, 1, -1, -1, -1},
    {-1, -1, 0, 1, -1, -1, 1}, {-1, -1, 0, 1, -1, 1, -1}, {-1, -1, 0, 1, -1, 1,
    1}, {-1, -1, 0, 1, 1, -1, -1}, {-1, -1, 0, 1, 1, -1, 1}, {-1, -1, 0, 1, 1,
    1, -1}, {-1, -1, 0, 1, 1, 1, 1}, {-1, -1, 1, -1, -1, -1, -1}, {-1, -1, 1,
    -1, -1, -1, 1}, {-1, -1, 1, -1, -1, 1, -1}, {-1, -1, 1, -1, -1, 1, 1}, {-1,
    -1, 1, -1, 1, -1, -1}, {-1, -1, 1, -1, 1, -1, 1}, {-1, -1, 1, -1, 1, 1,
    -1}, {-1, -1, 1, -1, 1, 1, 1}, {-1, -1, 1, 0, -1, -1, -1}, {-1, -1, 1, 0,
    -1, -1, 1}, {-1, -1, 1, 0, -1, 1, -1}, {-1, -1, 1, 0, -1, 1, 1}, {-1, -1,
    1, 0, 1, -1, -1}, {-1, -1, 1, 0, 1, -1, 1}, {-1, -1, 1, 0, 1, 1, -1}, {-1,
    -1, 1, 0, 1, 1, 1}, {-1, -1, 1, 1, -1, -1, -1}, {-1, -1, 1, 1, -1, -1, 1},
    {-1, -1, 1, 1, -1, 1, -1}, {-1, -1, 1, 1, -1, 1, 1}, {-1, -1, 1, 1, 1, -1,
    -1}, {-1, -1, 1, 1, 1, -1, 1}, {-1, -1, 1, 1, 1, 1, -1}, {-1, -1, 1, 1, 1,
    1, 1}, {-1, 1, -1, -1, -1, -1, -1}, {-1, 1, -1, -1, -1, -1, 1}, {-1, 1, -1,
    -1, -1, 1, -1}, {-1, 1, -1, -1, -1, 1, 1}, {-1, 1, -1, -1, 1, -1, -1}, {-1,
    1, -1, -1, 1, -1, 1}, {-1, 1, -1, -1, 1, 1, -1}, {-1, 1, -1, -1, 1, 1, 1},
    {-1, 1, -1, 0, -1, -1, -1}, {-1, 1, -1, 0, -1, -1, 1}, {-1, 1, -1, 0, -1,
    1, -1}, {-1, 1, -1, 0, -1, 1, 1}, {-1, 1, -1, 0, 1, -1, -1}, {-1, 1, -1, 0,
    1, -1, 1}, {-1, 1, -1, 0, 1, 1, -1}, {-1, 1, -1, 0, 1, 1, 1}, {-1, 1, -1,
    1, -1, -1, -1}, {-1, 1, -1, 1, -1, -1, 1}, {-1, 1, -1, 1, -1, 1, -1}, {-1,
    1, -1, 1, -1, 1, 1}, {-1, 1, -1, 1, 1, -1, -1}, {-1, 1, -1, 1, 1, -1, 1},
    {-1, 1, -1, 1, 1, 1, -1}, {-1, 1, -1, 1, 1, 1, 1}, {-1, 1, 0, -1, -1, -1,
    -1}, {-1, 1, 0, -1, -1, -1, 1}, {-1, 1, 0, -1, -1, 1, -1}, {-1, 1, 0, -1,
    -1, 1, 1}, {-1, 1, 0, -1, 1, -1, -1}, {-1, 1, 0, -1, 1, -1, 1}, {-1, 1, 0,
    -1, 1, 1, -1}, {-1, 1, 0, -1, 1, 1, 1}, {-1, 1, 0, 0, -1, -1, -1}, {-1, 1,
    0, 0, -1, -1, 1}, {-1, 1, 0, 0, -1, 1, -1}, {-1, 1, 0, 0, -1, 1, 1}, {-1,
    1, 0, 0, 1, -1, -1}, {-1, 1, 0, 0, 1, -1, 1}, {-1, 1, 0, 0, 1, 1, -1}, {-1,
    1, 0, 0, 1, 1, 1}, {-1, 1, 0, 1, -1, -1, -1}, {-1, 1, 0, 1, -1, -1, 1},
    {-1, 1, 0, 1, -1, 1, -1}, {-1, 1, 0, 1, -1, 1, 1}, {-1, 1, 0, 1, 1, -1,
    -1}, {-1, 1, 0, 1, 1, -1, 1}, {-1, 1, 0, 1, 1, 1, -1}, {-1, 1, 0, 1, 1, 1,
    1}, {-1, 1, 1, -1, -1, -1, -1}, {-1, 1, 1, -1, -1, -1, 1}, {-1, 1, 1, -1,
    -1, 1, -1}, {-1, 1, 1, -1, -1, 1, 1}, {-1, 1, 1, -1, 1, -1, -1}, {-1, 1, 1,
    -1, 1, -1, 1}, {-1, 1, 1, -1, 1, 1, -1}, {-1, 1, 1, -1, 1, 1, 1}, {-1, 1,
    1, 0, -1, -1, -1}, {-1, 1, 1, 0, -1, -1, 1}, {-1, 1, 1, 0, -1, 1, -1}, {-1,
    1, 1, 0, -1, 1, 1}, {-1, 1, 1, 0, 1, -1, -1}, {-1, 1, 1, 0, 1, -1, 1}, {-1,
    1, 1, 0, 1, 1, -1}, {-1, 1, 1, 0, 1, 1, 1}, {-1, 1, 1, 1, -1, -1, -1}, {-1,
    1, 1, 1, -1, -1, 1}, {-1, 1, 1, 1, -1, 1, -1}, {-1, 1, 1, 1, -1, 1, 1},
    {-1, 1, 1, 1, 1, -1, -1}, {-1, 1, 1, 1, 1, -1, 1}, {-1, 1, 1, 1, 1, 1, -1},
    {-1, 1, 1, 1, 1, 1, 1}, {1, -1, -1, -1, -1, -1, -1}, {1, -1, -1, -1, -1,
    -1, 1}, {1, -1, -1, -1, -1, 1, -1}, {1, -1, -1, -1, -1, 1, 1}, {1, -1, -1,
    -1, 1, -1, -1}, {1, -1, -1, -1, 1, -1, 1}, {1, -1, -1, -1, 1, 1, -1}, {1,
    -1, -1, -1, 1, 1, 1}, {1, -1, -1, 0, -1, -1, -1}, {1, -1, -1, 0, -1, -1,
    1}, {1, -1, -1, 0, -1, 1, -1}, {1, -1, -1, 0, -1, 1, 1}, {1, -1, -1, 0, 1,
    -1, -1}, {1, -1, -1, 0, 1, -1, 1}, {1, -1, -1, 0, 1, 1, -1}, {1, -1, -1, 0,
    1, 1, 1}, {1, -1, -1, 1, -1, -1, -1}, {1, -1, -1, 1, -1, -1, 1}, {1, -1,
    -1, 1, -1, 1, -1}, {1, -1, -1, 1, -1, 1, 1}, {1, -1, -1, 1, 1, -1, -1}, {1,
    -1, -1, 1, 1, -1, 1}, {1, -1, -1, 1, 1, 1, -1}, {1, -1, -1, 1, 1, 1, 1},
    {1, -1, 0, -1, -1, -1, -1}, {1, -1, 0, -1, -1, -1, 1}, {1, -1, 0, -1, -1,
    1, -1}, {1, -1, 0, -1, -1, 1, 1}, {1, -1, 0, -1, 1, -1, -1}, {1, -1, 0, -1,
    1, -1, 1}, {1, -1, 0, -1, 1, 1, -1}, {1, -1, 0, -1, 1, 1, 1}, {1, -1, 0, 0,
    -1, -1, -1}, {1, -1, 0, 0, -1, -1, 1}, {1, -1, 0, 0, -1, 1, -1}, {1, -1, 0,
    0, -1, 1, 1}, {1, -1, 0, 0, 1, -1, -1}, {1, -1, 0, 0, 1, -1, 1}, {1, -1, 0,
    0, 1, 1, -1}, {1, -1, 0, 0, 1, 1, 1}, {1, -1, 0, 1, -1, -1, -1}, {1, -1, 0,
    1, -1, -1, 1}, {1, -1, 0, 1, -1, 1, -1}, {1, -1, 0, 1, -1, 1, 1}, {1, -1,
    0, 1, 1, -1, -1}, {1, -1, 0, 1, 1, -1, 1}, {1, -1, 0, 1, 1, 1, -1}, {1, -1,
    0, 1, 1, 1, 1}, {1, -1, 1, -1, -1, -1, -1}, {1, -1, 1, -1, -1, -1, 1}, {1,
    -1, 1, -1, -1, 1, -1}, {1, -1, 1, -1, -1, 1, 1}, {1, -1, 1, -1, 1, -1, -1},
    {1, -1, 1, -1, 1, -1, 1}, {1, -1, 1, -1, 1, 1, -1}, {1, -1, 1, -1, 1, 1,
    1}, {1, -1, 1, 0, -1, -1, -1}, {1, -1, 1, 0, -1, -1, 1}, {1, -1, 1, 0, -1,
    1, -1}, {1, -1, 1, 0, -1, 1, 1}, {1, -1, 1, 0, 1, -1, -1}, {1, -1, 1, 0, 1,
    -1, 1}, {1, -1, 1, 0, 1, 1, -1}, {1, -1, 1, 0, 1, 1, 1}, {1, -1, 1, 1, -1,
    -1, -1}, {1, -1, 1, 1, -1, -1, 1}, {1, -1, 1, 1, -1, 1, -1}, {1, -1, 1, 1,
    -1, 1, 1}, {1, -1, 1, 1, 1, -1, -1}, {1, -1, 1, 1, 1, -1, 1}, {1, -1, 1, 1,
    1, 1, -1}, {1, -1, 1, 1, 1, 1, 1}, {1, 1, -1, -1, -1, -1, -1}, {1, 1, -1,
    -1, -1, -1, 1}, {1, 1, -1, -1, -1, 1, -1}, {1, 1, -1, -1, -1, 1, 1}, {1, 1,
    -1, -1, 1, -1, -1}, {1, 1, -1, -1, 1, -1, 1}, {1, 1, -1, -1, 1, 1, -1}, {1,
    1, -1, -1, 1, 1, 1}, {1, 1, -1, 0, -1, -1, -1}, {1, 1, -1, 0, -1, -1, 1},
    {1, 1, -1, 0, -1, 1, -1}, {1, 1, -1, 0, -1, 1, 1}, {1, 1, -1, 0, 1, -1,
    -1}, {1, 1, -1, 0, 1, -1, 1}, {1, 1, -1, 0, 1, 1, -1}, {1, 1, -1, 0, 1, 1,
    1}, {1, 1, -1, 1, -1, -1, -1}, {1, 1, -1, 1, -1, -1, 1}, {1, 1, -1, 1, -1,
    1, -1}, {1, 1, -1, 1, -1, 1, 1}, {1, 1, -1, 1, 1, -1, -1}, {1, 1, -1, 1, 1,
    -1, 1}, {1, 1, -1, 1, 1, 1, -1}, {1, 1, -1, 1, 1, 1, 1}, {1, 1, 0, -1, -1,
    -1, -1}, {1, 1, 0, -1, -1, -1, 1}, {1, 1, 0, -1, -1, 1, -1}, {1, 1, 0, -1,
    -1, 1, 1}, {1, 1, 0, -1, 1, -1, -1}, {1, 1, 0, -1, 1, -1, 1}, {1, 1, 0, -1,
    1, 1, -1}, {1, 1, 0, -1, 1, 1, 1}, {1, 1, 0, 0, -1, -1, -1}, {1, 1, 0, 0,
    -1, -1, 1}, {1, 1, 0, 0, -1, 1, -1}, {1, 1, 0, 0, -1, 1, 1}, {1, 1, 0, 0,
    1, -1, -1}, {1, 1, 0, 0, 1, -1, 1}, {1, 1, 0, 0, 1, 1, -1}, {1, 1, 0, 0, 1,
    1, 1}, {1, 1, 0, 1, -1, -1, -1}, {1, 1, 0, 1, -1, -1, 1}, {1, 1, 0, 1, -1,
    1, -1}, {1, 1, 0, 1, -1, 1, 1}, {1, 1, 0, 1, 1, -1, -1}, {1, 1, 0, 1, 1,
    -1, 1}, {1, 1, 0, 1, 1, 1, -1}, {1, 1, 0, 1, 1, 1, 1}, {1, 1, 1, -1, -1,
    -1, -1}, {1, 1, 1, -1, -1, -1, 1}, {1, 1, 1, -1, -1, 1, -1}, {1, 1, 1, -1,
    -1, 1, 1}, {1, 1, 1, -1, 1, -1, -1}, {1, 1, 1, -1, 1, -1, 1}, {1, 1, 1, -1,
    1, 1, -1}, {1, 1, 1, -1, 1, 1, 1}, {1, 1, 1, 0, -1, -1, -1}, {1, 1, 1, 0,
    -1, -1, 1}, {1, 1, 1, 0, -1, 1, -1}, {1, 1, 1, 0, -1, 1, 1}, {1, 1, 1, 0,
    1, -1, -1}, {1, 1, 1, 0, 1, -1, 1}, {1, 1, 1, 0, 1, 1, -1}, {1, 1, 1, 0, 1,
    1, 1}, {1, 1, 1, 1, -1, -1, -1}, {1, 1, 1, 1, -1, -1, 1}, {1, 1, 1, 1, -1,
    1, -1}, {1, 1, 1, 1, -1, 1, 1}, {1, 1, 1, 1, 1, -1, -1}, {1, 1, 1, 1, 1,
    -1, 1}, {1, 1, 1, 1, 1, 1, -1}, {1, 1, 1, 1, 1, 1, 1}};

// Normalization factors the various processes
// Denominators: spins, colors and identical particles
int PY8MEs_R12_P21_sm_gb_wpwmggb::denom_colors[nprocesses] = {24, 24}; 
int PY8MEs_R12_P21_sm_gb_wpwmggb::denom_hels[nprocesses] = {4, 4}; 
int PY8MEs_R12_P21_sm_gb_wpwmggb::denom_iden[nprocesses] = {2, 2}; 

//--------------------------------------------------------------------------
// Color config initialization
void PY8MEs_R12_P21_sm_gb_wpwmggb::initColorConfigs() 
{
  color_configs = vector < vec_vec_int > (); 
  jamp_nc_relative_power = vector < vec_int > (); 

  // Color flows of process Process: g b > w+ w- g g b WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(2)(4)(0)(0)(0)(0)(0)(3)(2)(4)(3)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #1
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(2)(3)(0)(0)(0)(0)(0)(3)(4)(4)(2)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #2
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(2)(4)(0)(0)(0)(0)(0)(3)(1)(4)(2)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #3
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(2)(2)(0)(0)(0)(0)(0)(3)(1)(4)(3)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #4
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(2)(3)(0)(0)(0)(0)(0)(3)(2)(4)(1)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #5
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(2)(2)(0)(0)(0)(0)(0)(3)(4)(4)(1)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 

  // Color flows of process Process: g b~ > w+ w- g g b~ WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(2)(0)(1)(0)(0)(0)(0)(3)(2)(4)(3)(0)(4)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #1
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(2)(0)(1)(0)(0)(0)(0)(3)(4)(4)(2)(0)(3)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #2
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(2)(0)(1)(0)(0)(0)(0)(3)(1)(4)(2)(0)(4)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #3
  color_configs[1].push_back(vec_int(createvector<int>
      (4)(2)(0)(1)(0)(0)(0)(0)(3)(1)(4)(3)(0)(2)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #4
  color_configs[1].push_back(vec_int(createvector<int>
      (4)(2)(0)(1)(0)(0)(0)(0)(3)(2)(4)(1)(0)(3)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #5
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(2)(0)(1)(0)(0)(0)(0)(3)(4)(4)(1)(0)(2)));
  jamp_nc_relative_power[1].push_back(0); 
}

//--------------------------------------------------------------------------
// Destructor.
PY8MEs_R12_P21_sm_gb_wpwmggb::~PY8MEs_R12_P21_sm_gb_wpwmggb() 
{
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    delete[] p[i]; 
    p[i] = NULL; 
  }
}

//--------------------------------------------------------------------------
// Invert the permutation mapping
vector<int> PY8MEs_R12_P21_sm_gb_wpwmggb::invert_mapping(vector<int> mapping) 
{
  vector<int> inverted_mapping; 
  for (unsigned int i = 0; i < mapping.size(); i++ )
  {
    for (unsigned int j = 0; j < mapping.size(); j++ )
    {
      if (mapping[j] == ((int)i))
      {
        inverted_mapping.push_back(j); 
        break; 
      }
    }
  }
  return inverted_mapping; 
}

//--------------------------------------------------------------------------
// Return the list of possible helicity configurations
vector < vec_int > PY8MEs_R12_P21_sm_gb_wpwmggb::getHelicityConfigs(vector<int>
    permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(ncomb, vector<int> (nexternal, 0)); 
  for (unsigned int ihel = 0; ihel < ncomb; ihel++ )
  {
    for(unsigned int j = 0; j < nexternal; j++ )
    {
      res[ihel][chosenPerm[j]] = helicities[ihel][j]; 
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Return the list of possible color configurations
vector < vec_int > PY8MEs_R12_P21_sm_gb_wpwmggb::getColorConfigs(int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(color_configs[chosenProcID].size(), vector<int>
      (nexternal * 2, 0));
  for (unsigned int icol = 0; icol < color_configs[chosenProcID].size(); icol++
      )
  {
    for(unsigned int j = 0; j < (2 * nexternal); j++ )
    {
      res[icol][chosenPerm[j/2] * 2 + j%2] =
          color_configs[chosenProcID][icol][j];
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Get JAMP relative N_c power
int PY8MEs_R12_P21_sm_gb_wpwmggb::getColorFlowRelativeNCPower(int
    color_flow_ID, int specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return jamp_nc_relative_power[chosenProcID][color_flow_ID]; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity ID -> Helicity Config
vector<int> PY8MEs_R12_P21_sm_gb_wpwmggb::getHelicityConfigForID(int hel_ID,
    vector<int> permutation)
{
  if (hel_ID < 0 || hel_ID >= ncomb)
  {
    cerr <<  "Error in function 'getHelicityConfigForID' of class" << 
    " 'PY8MEs_R12_P21_sm_gb_wpwmggb': Specified helicity ID '" << 
    hel_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R12_P21_sm_gb_wpwmggb_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(nexternal, 0); 
  for (unsigned int j = 0; j < nexternal; j++ )
  {
    res[chosenPerm[j]] = helicities[hel_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity Config -> Helicity ID
int PY8MEs_R12_P21_sm_gb_wpwmggb::getHelicityIDForConfig(vector<int>
    hel_config, vector<int> permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  int user_ihel = -1; 
  if (hel_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < ncomb; i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < nexternal; j++ )
      {
        if (helicities[i][chosenPerm[j]] != hel_config[j])
        {
          found = false; 
          break; 
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_ihel = i; 
        break; 
      }
    }
    if (user_ihel == -1)
    {
      cerr <<  "Error in function 'getHelicityIDForConfig' of class" << 
      " 'PY8MEs_R12_P21_sm_gb_wpwmggb': Specified helicity" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R12_P21_sm_gb_wpwmggb_exception; 
    }
  }
  return user_ihel; 
}


//--------------------------------------------------------------------------
// Implements the map Color ID -> Color Config
vector<int> PY8MEs_R12_P21_sm_gb_wpwmggb::getColorConfigForID(int color_ID, int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < 0 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getColorConfigForID' of class" << 
    " 'PY8MEs_R12_P21_sm_gb_wpwmggb': Specified color ID '" << 
    color_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R12_P21_sm_gb_wpwmggb_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(color_configs[chosenProcID][color_ID].size(), 0); 
  for (unsigned int j = 0; j < (2 * nexternal); j++ )
  {
    res[chosenPerm[j/2] * 2 + j%2] = color_configs[chosenProcID][color_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Color Config -> Color ID
int PY8MEs_R12_P21_sm_gb_wpwmggb::getColorIDForConfig(vector<int> color_config,
    int specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = -1; 
  if (color_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < color_configs[chosenProcID].size(); i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < (nexternal * 2); j++ )
      {

        // If colorless then make sure it matches
        // The little arithmetics in the color index is just
        // the permutation applies on the particle list which is
        // twice smaller since each particle can have two color indices.
        if (color_config[j] == 0)
        {
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] != 0)
          {
            found = false; 
            break; 
          }
          // Otherwise check that the color linked position matches
        }
        else
        {
          int color_linked_pos = -1; 
          // Find the other end of the line in the user color config
          for (unsigned int k = 0; k < (nexternal * 2); k++ )
          {
            if (k == j)
              continue; 
            if (color_config[j] == color_config[k])
            {
              color_linked_pos = k; 
              break; 
            }
          }
          if (color_linked_pos == -1)
          {
            cerr <<  "Error in function 'getColorIDForConfig' of class" << 
            " 'PY8MEs_R12_P21_sm_gb_wpwmggb': A color line could " << 
            " not be closed." << endl; 
            throw PY8MEs_R12_P21_sm_gb_wpwmggb_exception; 
          }
          // Now check whether the color line matches
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] !=
              color_configs[chosenProcID][i][chosenPerm[color_linked_pos/2] * 2
              + color_linked_pos%2])
          {
            found = false; 
            break; 
          }
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_icol = i; 
        break; 
      }
    }

    if (user_icol == -1)
    {
      cerr <<  "Error in function 'getColorIDForConfig' of class" << 
      " 'PY8MEs_R12_P21_sm_gb_wpwmggb': Specified color" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R12_P21_sm_gb_wpwmggb_exception; 
    }
  }
  return user_icol; 
}

//--------------------------------------------------------------------------
// Returns all result previously computed in SigmaKin
vector < vec_double > PY8MEs_R12_P21_sm_gb_wpwmggb::getAllResults(int
    specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return all_results[chosenProcID]; 
}

//--------------------------------------------------------------------------
// Returns a result previously computed in SigmaKin for a specific helicity
// and color ID. -1 means avg and summed over that characteristic.
double PY8MEs_R12_P21_sm_gb_wpwmggb::getResult(int helicity_ID, int color_ID,
    int specify_proc_ID)
{
  if (helicity_ID < - 1 || helicity_ID >= ncomb)
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R12_P21_sm_gb_wpwmggb': Specified helicity ID '" << 
    helicity_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R12_P21_sm_gb_wpwmggb_exception; 
  }
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < - 1 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R12_P21_sm_gb_wpwmggb': Specified color ID '" << 
    color_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R12_P21_sm_gb_wpwmggb_exception; 
  }
  return all_results[chosenProcID][helicity_ID + 1][color_ID + 1]; 
}

//--------------------------------------------------------------------------
// Check for the availability of the requested process and if available,
// If available, this returns the corresponding permutation and Proc_ID to use.
// If not available, this returns a negative Proc_ID.
pair < vector<int> , int > PY8MEs_R12_P21_sm_gb_wpwmggb::static_getPY8ME(vector<int> initial_pdgs, vector<int> final_pdgs, set<int> schannels) 
{

  // Not available return value
  pair < vector<int> , int > NA(vector<int> (), -1); 

  // Check if s-channel requirements match
  if (nreq_s_channels > 0)
  {
    if (schannels != s_channel_proc)
      return NA; 
  }
  else
  {
    if (schannels.size() != 0)
      return NA; 
  }

  // Check number of final state particles
  if (final_pdgs.size() != (nexternal - ninitial))
    return NA; 

  // Check number of initial state particles
  if (initial_pdgs.size() != ninitial)
    return NA; 

  // List of processes available in this class
  const int nprocs = 4; 
  const int proc_IDS[nprocs] = {0, 1, 0, 1}; 
  const int in_pdgs[nprocs][ninitial] = {{21, 5}, {21, -5}, {5, 21}, {-5, 21}}; 
  const int out_pdgs[nprocs][nexternal - ninitial] = {{24, -24, 21, 21, 5},
      {24, -24, 21, 21, -5}, {24, -24, 21, 21, 5}, {24, -24, 21, 21, -5}};

  bool in_pdgs_used[ninitial]; 
  bool out_pdgs_used[nexternal - ninitial]; 
  for(unsigned int i = 0; i < nprocs; i++ )
  {
    int permutations[nexternal]; 

    // Reinitialize initial state look-up variables
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      in_pdgs_used[j] = false; 
      permutations[j] = -1; 
    }
    // Look for initial state matches
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      for(unsigned int k = 0; k < ninitial; k++ )
      {
        // Make sure it has not been used already
        if (in_pdgs_used[k])
          continue; 
        if (initial_pdgs[k] == in_pdgs[i][j])
        {
          permutations[j] = k; 
          in_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[ninitial - 1] == -1)
      continue; 

    // Reinitialize final state look-up variables
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      out_pdgs_used[j] = false; 
      permutations[ninitial + j] = -1; 
    }
    // Look for final state matches
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      for(unsigned int k = 0; k < (nexternal - ninitial); k++ )
      {
        // Make sure it has not been used already
        if (out_pdgs_used[k])
          continue; 
        if (final_pdgs[k] == out_pdgs[i][j])
        {
          permutations[ninitial + j] = ninitial + k; 
          out_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[ninitial + j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[nexternal - 1] == -1)
      continue; 

    // Return process found
    return pair < vector<int> , int > (vector<int> (permutations, permutations
        + nexternal), proc_IDS[i]);
  }

  // No process found
  return NA; 
}

//--------------------------------------------------------------------------
// Set momenta
void PY8MEs_R12_P21_sm_gb_wpwmggb::setMomenta(vector < vec_double >
    momenta_picked)
{
  if (momenta_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setMomenta' of class" << 
    " 'PY8MEs_R12_P21_sm_gb_wpwmggb': Incorrect number of" << 
    " momenta specified." << endl; 
    throw PY8MEs_R12_P21_sm_gb_wpwmggb_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    if (momenta_picked[i].size() != 4)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R12_P21_sm_gb_wpwmggb': Incorrect number of" << 
      " momenta components specified." << endl; 
      throw PY8MEs_R12_P21_sm_gb_wpwmggb_exception; 
    }
    if (momenta_picked[i][0] < 0.0)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R12_P21_sm_gb_wpwmggb': A momentum was specified" << 
      " with negative energy. Check conventions." << endl; 
      throw PY8MEs_R12_P21_sm_gb_wpwmggb_exception; 
    }
    for (unsigned int j = 0; j < 4; j++ )
    {
      p[i][j] = momenta_picked[i][j]; 
    }
  }
}

//--------------------------------------------------------------------------
// Set color configuration to use. An empty vector means sum over all.
void PY8MEs_R12_P21_sm_gb_wpwmggb::setColors(vector<int> colors_picked)
{
  if (colors_picked.size() == 0)
  {
    user_colors = vector<int> (); 
    return; 
  }
  if (colors_picked.size() != (2 * nexternal))
  {
    cerr <<  "Error in function 'setColors' of class" << 
    " 'PY8MEs_R12_P21_sm_gb_wpwmggb': Incorrect number" << 
    " of colors specified." << endl; 
    throw PY8MEs_R12_P21_sm_gb_wpwmggb_exception; 
  }
  user_colors = vector<int> ((2 * nexternal), 0); 
  for(unsigned int i = 0; i < (2 * nexternal); i++ )
  {
    user_colors[i] = colors_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the helicity configuration to use. Am empty vector means sum over all.
void PY8MEs_R12_P21_sm_gb_wpwmggb::setHelicities(vector<int> helicities_picked) 
{
  if (helicities_picked.size() != nexternal)
  {
    if (helicities_picked.size() == 0)
    {
      user_helicities = vector<int> (); 
      return; 
    }
    cerr <<  "Error in function 'setHelicities' of class" << 
    " 'PY8MEs_R12_P21_sm_gb_wpwmggb': Incorrect number" << 
    " of helicities specified." << endl; 
    throw PY8MEs_R12_P21_sm_gb_wpwmggb_exception; 
  }
  user_helicities = vector<int> (nexternal, 0); 
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    user_helicities[i] = helicities_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the permutation to use (will apply to momenta, colors and helicities)
void PY8MEs_R12_P21_sm_gb_wpwmggb::setPermutation(vector<int> perm_picked) 
{
  if (perm_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setPermutations' of class" << 
    " 'PY8MEs_R12_P21_sm_gb_wpwmggb': Incorrect number" << 
    " of permutations specified." << endl; 
    throw PY8MEs_R12_P21_sm_gb_wpwmggb_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    perm[i] = perm_picked[i]; 
  }
}

// Set the proc_ID to use
void PY8MEs_R12_P21_sm_gb_wpwmggb::setProcID(int procID_picked) 
{
  proc_ID = procID_picked; 
}

//--------------------------------------------------------------------------
// Initialize process.

void PY8MEs_R12_P21_sm_gb_wpwmggb::initProc() 
{

  // Initialize flags
  include_symmetry_factors = true; 
  include_helicity_averaging_factors = true; 
  include_color_averaging_factors = true; 

  // Initialize vectors.
  perm = vector<int> (nexternal, 0); 
  user_colors = vector<int> (2 * nexternal, 0); 
  user_helicities = vector<int> (nexternal, 0); 
  p = vector < double * > (); 
  for (unsigned int i = 0; i < nexternal; i++ )
  {
    p.push_back(new double[4]); 
  }
  initColorConfigs(); 
  // Synchronize local variables dependent on the model with the active model.
  mME = vector<double> (nexternal, 0.); 
  syncProcModelParams(); 
  jamp2 = vector < vec_double > (2); 
  jamp2[0] = vector<double> (6, 0.); 
  jamp2[1] = vector<double> (6, 0.); 
  all_results = vector < vec_vec_double > (2); 
  // The first entry is always the color or helicity avg/summed matrix element.
  all_results[0] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[1] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
}

// Synchronize local variables of the process that depend on the model
// parameters
void PY8MEs_R12_P21_sm_gb_wpwmggb::syncProcModelParams() 
{

  // Instantiate the model class and set parameters that stay fixed during run
  mME[0] = pars->ZERO; 
  mME[1] = pars->mdl_MB; 
  mME[2] = pars->mdl_MW; 
  mME[3] = pars->mdl_MW; 
  mME[4] = pars->ZERO; 
  mME[5] = pars->ZERO; 
  mME[6] = pars->mdl_MB; 
}

//--------------------------------------------------------------------------
// Setter allowing to force particular values for the external masses
void PY8MEs_R12_P21_sm_gb_wpwmggb::setMasses(vec_double external_masses) 
{

  if (external_masses.size() != mME.size())
  {
    cerr <<  "Error in function 'setMasses' of class" << 
    " 'PY8MEs_R12_P21_sm_gb_wpwmggb': Incorrect number of" << 
    " masses specified." << endl; 
    throw PY8MEs_R12_P21_sm_gb_wpwmggb_exception; 
  }
  for (unsigned int j = 0; j < mME.size(); j++ )
  {
    mME[j] = external_masses[perm[j]]; 
  }
}

//--------------------------------------------------------------------------
// Getter accessing external masses with the correct ordering
vector<double> PY8MEs_R12_P21_sm_gb_wpwmggb::getMasses() 
{

  vec_double external_masses; 
  vector<int> invertedPerm; 
  invertedPerm = invert_mapping(perm); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    external_masses.push_back(mME[invertedPerm[i]]); 
  }
  return external_masses; 

}


// Set all values of the external masses to float(-mode) where mode can be
// 0 : Mass taken from the model
// 1 : Mass taken from p_i^2 if not massless to begin with
// 2 : Mass always taken from p_i^2.
void PY8MEs_R12_P21_sm_gb_wpwmggb::setExternalMassesMode(int mode) 
{
  if (mode != 0 && mode != 1 && mode != 2)
  {
    cerr <<  "Error in function 'setExternalMassesMode' of class" << 
    " 'PY8MEs_R12_P21_sm_gb_wpwmggb': Incorrect mode selected :" << mode << 
    ". It must be either 0, 1 or 2" << endl; 
    throw PY8MEs_R12_P21_sm_gb_wpwmggb_exception; 
  }
  if (mode == 0)
  {
    syncProcModelParams(); 
  }
  else if (mode == 1)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      if (mME[j] != pars->ZERO)
      {
        mME[j] = -1.0; 
      }
    }
  }
  else if (mode == 2)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      mME[j] = -1.0; 
    }
  }
}

//--------------------------------------------------------------------------
// Evaluate the squared matrix element.

double PY8MEs_R12_P21_sm_gb_wpwmggb::sigmaKin() 
{
  // Set the parameters which change event by event
  pars->setDependentParameters(); 
  pars->setDependentCouplings(); 
  // Reset color flows
  for(int i = 0; i < 6; i++ )
    jamp2[0][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[1][i] = 0.; 

  // Save previous values of mME
  vector<double> saved_mME(mME.size(), 0.0); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (mME[i] < 0.0)
    {
      saved_mME[i] = mME[i]; 
      mME[i] = sqrt(abs(pow(p[perm[i]][0], 2) - 
      (pow(p[perm[i]][1], 2) + pow(p[perm[i]][2], 2) + pow(p[perm[i]][3],
          2))));
    }
  }

  // Local variables and constants
  const int max_tries = 10; 
  vector < vec_bool > goodhel(nprocesses, vec_bool(ncomb, false)); 
  vec_int ntry(nprocesses, 0); 
  double t = 0.; 
  double result = 0.; 

  if (ntry[proc_ID] <= max_tries)
    ntry[proc_ID] = ntry[proc_ID] + 1; 

  // Find which helicity configuration is asked for
  // -1 indicates one wants to sum over helicities
  int user_ihel = getHelicityIDForConfig(user_helicities); 

  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = getColorIDForConfig(user_colors); 

  // Reset the list of results that will be recomputed here
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      all_results[proc_ID][ihel + 1][icolor + 1] = 0.; 
    }
  }

  // Calculate the matrix element for all helicities
  // unless already detected as vanishing
  for(int ihel = 0; ihel < ncomb; ihel++ )
  {
    // Skip helicity if already detected as vanishing
    if ((ntry[proc_ID] >= max_tries) && !goodhel[proc_ID][ihel])
      continue; 

    // Also skip helicity if user asks for a specific one
    if ((ntry[proc_ID] >= max_tries) && user_ihel != -1 && user_ihel != ihel)
      continue; 

    calculate_wavefunctions(helicities[ihel]); 

    // Reset locally computed color flows
    for(int i = 0; i < 6; i++ )
      jamp2[0][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[1][i] = 0.; 

    if (proc_ID == 0)
      t = matrix_12_gb_wpwmggb(); 
    if (proc_ID == 1)
      t = matrix_12_gbx_wpwmggbx(); 

    // Store which helicities give non-zero result
    if ((ntry[proc_ID] < max_tries) && t != 0. && !goodhel[proc_ID][ihel])
      goodhel[proc_ID][ihel] = true; 

    // Aggregate results
    if (user_ihel == -1 || user_ihel == ihel)
    {
      if (user_icol == -1)
      {
        result = result + t; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][0] += t; 
          for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
          {
            all_results[proc_ID][0][i + 1] += jamp2[proc_ID][i]; 
          }
        }
        all_results[proc_ID][ihel + 1][0] += t; 
        for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
        {
          all_results[proc_ID][ihel + 1][i + 1] += jamp2[proc_ID][i]; 
        }
      }
      else
      {
        result = result + jamp2[proc_ID][user_icol]; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][user_icol + 1] += jamp2[proc_ID][user_icol]; 
        }
        all_results[proc_ID][ihel + 1][user_icol + 1] +=
            jamp2[proc_ID][user_icol];
      }
    }

  }

  // Normalize results with the identical particle factor
  if (include_symmetry_factors)
  {
    result = result/denom_iden[proc_ID]; 
  }
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      if (include_symmetry_factors)
      {
        all_results[proc_ID][ihel + 1][icolor + 1] /= denom_iden[proc_ID]; 
      }
    }
  }


  // Normalize when when summing+averaging over helicity configurations
  if (user_ihel == -1 && include_helicity_averaging_factors)
  {
    result /= denom_hels[proc_ID]; 
    if (user_icol == -1)
    {
      all_results[proc_ID][0][0] /= denom_hels[proc_ID]; 
      for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
      {
        all_results[proc_ID][0][i + 1] /= denom_hels[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][0][user_icol + 1] /= denom_hels[proc_ID]; 
    }
  }

  // Normalize when summing+averaging over color configurations
  if (user_icol == -1 && include_color_averaging_factors)
  {
    result /= denom_colors[proc_ID]; 
    if (user_ihel == -1)
    {
      all_results[proc_ID][0][0] /= denom_colors[proc_ID]; 
      for (unsigned int i = 0; i < ncomb; i++ )
      {
        all_results[proc_ID][i + 1][0] /= denom_colors[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][user_ihel + 1][0] /= denom_colors[proc_ID]; 
    }
  }

  // Reinstate previous values of mME
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (saved_mME[i] < 0.0)
    {
      mME[i] = saved_mME[i]; 
    }
  }

  // Finally return it
  return result; 
}

//==========================================================================
// Private class member functions

//--------------------------------------------------------------------------
// Evaluate |M|^2 for each subprocess

void PY8MEs_R12_P21_sm_gb_wpwmggb::calculate_wavefunctions(const int hel[])
{
  // Calculate wavefunctions for all processes
  // Calculate all wavefunctions
  vxxxxx(p[perm[0]], mME[0], hel[0], -1, w[0]); 
  ixxxxx(p[perm[1]], mME[1], hel[1], +1, w[1]); 
  vxxxxx(p[perm[2]], mME[2], hel[2], +1, w[2]); 
  vxxxxx(p[perm[3]], mME[3], hel[3], +1, w[3]); 
  vxxxxx(p[perm[4]], mME[4], hel[4], +1, w[4]); 
  vxxxxx(p[perm[5]], mME[5], hel[5], +1, w[5]); 
  oxxxxx(p[perm[6]], mME[6], hel[6], +1, w[6]); 
  VVV1P0_1(w[0], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[7]); 
  VVV1P0_1(w[3], w[2], pars->GC_4, pars->ZERO, pars->ZERO, w[8]); 
  VVV1P0_1(w[7], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[9]); 
  FFV1_1(w[6], w[8], pars->GC_1, pars->mdl_MB, pars->ZERO, w[10]); 
  FFV1_2(w[1], w[8], pars->GC_1, pars->mdl_MB, pars->ZERO, w[11]); 
  FFV1_1(w[6], w[7], pars->GC_11, pars->mdl_MB, pars->ZERO, w[12]); 
  FFV1_2(w[1], w[7], pars->GC_11, pars->mdl_MB, pars->ZERO, w[13]); 
  VVS1_3(w[3], w[2], pars->GC_72, pars->mdl_MH, pars->mdl_WH, w[14]); 
  FFS4_1(w[6], w[14], pars->GC_83, pars->mdl_MB, pars->ZERO, w[15]); 
  FFS4_2(w[1], w[14], pars->GC_83, pars->mdl_MB, pars->ZERO, w[16]); 
  VVV1_3(w[3], w[2], pars->GC_53, pars->mdl_MZ, pars->mdl_WZ, w[17]); 
  FFV2_3_1(w[6], w[17], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[18]);
  FFV2_3_2(w[1], w[17], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[19]);
  FFV1_1(w[6], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[20]); 
  FFV1_1(w[20], w[7], pars->GC_11, pars->mdl_MB, pars->ZERO, w[21]); 
  FFV1_2(w[1], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[22]); 
  FFV1_2(w[22], w[7], pars->GC_11, pars->mdl_MB, pars->ZERO, w[23]); 
  FFV2_1(w[6], w[2], pars->GC_100, pars->mdl_MT, pars->mdl_WT, w[24]); 
  FFV2_1(w[24], w[3], pars->GC_100, pars->mdl_MB, pars->ZERO, w[25]); 
  FFV1_1(w[24], w[5], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[26]); 
  FFV2_2(w[1], w[3], pars->GC_100, pars->mdl_MT, pars->mdl_WT, w[27]); 
  FFV1_1(w[24], w[7], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[28]); 
  FFV1_2(w[27], w[7], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[29]); 
  FFV2_2(w[27], w[2], pars->GC_100, pars->mdl_MB, pars->ZERO, w[30]); 
  FFV1_2(w[27], w[5], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[31]); 
  FFV2_1(w[20], w[2], pars->GC_100, pars->mdl_MT, pars->mdl_WT, w[32]); 
  FFV2_2(w[22], w[3], pars->GC_100, pars->mdl_MT, pars->mdl_WT, w[33]); 
  VVV1P0_1(w[0], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[34]); 
  FFV1_1(w[6], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[35]); 
  FFV1_2(w[1], w[34], pars->GC_11, pars->mdl_MB, pars->ZERO, w[36]); 
  FFV2_1(w[35], w[2], pars->GC_100, pars->mdl_MT, pars->mdl_WT, w[37]); 
  FFV1_1(w[35], w[34], pars->GC_11, pars->mdl_MB, pars->ZERO, w[38]); 
  FFV1_2(w[27], w[34], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[39]); 
  FFV1_2(w[1], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[40]); 
  FFV1_1(w[6], w[34], pars->GC_11, pars->mdl_MB, pars->ZERO, w[41]); 
  FFV2_2(w[40], w[3], pars->GC_100, pars->mdl_MT, pars->mdl_WT, w[42]); 
  FFV1_2(w[40], w[34], pars->GC_11, pars->mdl_MB, pars->ZERO, w[43]); 
  FFV1_1(w[24], w[34], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[44]); 
  VVV1P0_1(w[34], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[45]); 
  FFV1_1(w[24], w[4], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[46]); 
  FFV1_2(w[27], w[4], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[47]); 
  FFV1_1(w[6], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[48]); 
  VVV1P0_1(w[4], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[49]); 
  FFV2_1(w[48], w[2], pars->GC_100, pars->mdl_MT, pars->mdl_WT, w[50]); 
  FFV1_2(w[1], w[49], pars->GC_11, pars->mdl_MB, pars->ZERO, w[51]); 
  FFV1_1(w[48], w[49], pars->GC_11, pars->mdl_MB, pars->ZERO, w[52]); 
  FFV1_1(w[48], w[8], pars->GC_1, pars->mdl_MB, pars->ZERO, w[53]); 
  FFS4_1(w[48], w[14], pars->GC_83, pars->mdl_MB, pars->ZERO, w[54]); 
  FFV2_3_1(w[48], w[17], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[55]);
  FFV1_2(w[40], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[56]); 
  FFV1_1(w[48], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[57]); 
  FFV1_1(w[48], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[58]); 
  FFV1_2(w[22], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[59]); 
  FFV1_2(w[1], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[60]); 
  FFV2_2(w[60], w[3], pars->GC_100, pars->mdl_MT, pars->mdl_WT, w[61]); 
  FFV1_1(w[6], w[49], pars->GC_11, pars->mdl_MB, pars->ZERO, w[62]); 
  FFV1_2(w[60], w[49], pars->GC_11, pars->mdl_MB, pars->ZERO, w[63]); 
  FFV1_2(w[60], w[8], pars->GC_1, pars->mdl_MB, pars->ZERO, w[64]); 
  FFS4_2(w[60], w[14], pars->GC_83, pars->mdl_MB, pars->ZERO, w[65]); 
  FFV2_3_2(w[60], w[17], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[66]);
  FFV1_1(w[35], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[67]); 
  FFV1_2(w[60], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[68]); 
  FFV1_2(w[60], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[69]); 
  FFV1_1(w[20], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[70]); 
  VVV1P0_1(w[0], w[49], pars->GC_10, pars->ZERO, pars->ZERO, w[71]); 
  FFV1_1(w[24], w[0], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[72]); 
  FFV1_2(w[27], w[0], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[73]); 
  FFV1_1(w[35], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[74]); 
  FFV1_2(w[22], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[75]); 
  FFV1_2(w[40], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[76]); 
  FFV1_1(w[20], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[77]); 
  VVVV1P0_1(w[0], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[78]); 
  VVVV3P0_1(w[0], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[79]); 
  VVVV4P0_1(w[0], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[80]); 
  FFV1_1(w[6], w[78], pars->GC_11, pars->mdl_MB, pars->ZERO, w[81]); 
  FFV1_1(w[6], w[79], pars->GC_11, pars->mdl_MB, pars->ZERO, w[82]); 
  FFV1_1(w[6], w[80], pars->GC_11, pars->mdl_MB, pars->ZERO, w[83]); 
  FFV1_2(w[1], w[78], pars->GC_11, pars->mdl_MB, pars->ZERO, w[84]); 
  FFV1_2(w[1], w[79], pars->GC_11, pars->mdl_MB, pars->ZERO, w[85]); 
  FFV1_2(w[1], w[80], pars->GC_11, pars->mdl_MB, pars->ZERO, w[86]); 
  oxxxxx(p[perm[1]], mME[1], hel[1], -1, w[87]); 
  ixxxxx(p[perm[6]], mME[6], hel[6], -1, w[88]); 
  FFV1_1(w[87], w[8], pars->GC_1, pars->mdl_MB, pars->ZERO, w[89]); 
  FFV1_2(w[88], w[8], pars->GC_1, pars->mdl_MB, pars->ZERO, w[90]); 
  FFV1_1(w[87], w[7], pars->GC_11, pars->mdl_MB, pars->ZERO, w[91]); 
  FFV1_2(w[88], w[7], pars->GC_11, pars->mdl_MB, pars->ZERO, w[92]); 
  FFS4_1(w[87], w[14], pars->GC_83, pars->mdl_MB, pars->ZERO, w[93]); 
  FFS4_2(w[88], w[14], pars->GC_83, pars->mdl_MB, pars->ZERO, w[94]); 
  FFV2_3_1(w[87], w[17], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[95]);
  FFV2_3_2(w[88], w[17], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[96]);
  FFV1_1(w[87], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[97]); 
  FFV1_1(w[97], w[7], pars->GC_11, pars->mdl_MB, pars->ZERO, w[98]); 
  FFV1_2(w[88], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[99]); 
  FFV1_2(w[99], w[7], pars->GC_11, pars->mdl_MB, pars->ZERO, w[100]); 
  FFV2_1(w[87], w[2], pars->GC_100, pars->mdl_MT, pars->mdl_WT, w[101]); 
  FFV2_1(w[101], w[3], pars->GC_100, pars->mdl_MB, pars->ZERO, w[102]); 
  FFV1_1(w[101], w[5], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[103]); 
  FFV2_2(w[88], w[3], pars->GC_100, pars->mdl_MT, pars->mdl_WT, w[104]); 
  FFV1_1(w[101], w[7], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[105]); 
  FFV1_2(w[104], w[7], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[106]); 
  FFV2_2(w[104], w[2], pars->GC_100, pars->mdl_MB, pars->ZERO, w[107]); 
  FFV1_2(w[104], w[5], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[108]); 
  FFV2_1(w[97], w[2], pars->GC_100, pars->mdl_MT, pars->mdl_WT, w[109]); 
  FFV2_2(w[99], w[3], pars->GC_100, pars->mdl_MT, pars->mdl_WT, w[110]); 
  FFV1_1(w[87], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[111]); 
  FFV1_2(w[88], w[34], pars->GC_11, pars->mdl_MB, pars->ZERO, w[112]); 
  FFV2_1(w[111], w[2], pars->GC_100, pars->mdl_MT, pars->mdl_WT, w[113]); 
  FFV1_1(w[111], w[34], pars->GC_11, pars->mdl_MB, pars->ZERO, w[114]); 
  FFV1_2(w[104], w[34], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[115]); 
  FFV1_2(w[88], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[116]); 
  FFV1_1(w[87], w[34], pars->GC_11, pars->mdl_MB, pars->ZERO, w[117]); 
  FFV2_2(w[116], w[3], pars->GC_100, pars->mdl_MT, pars->mdl_WT, w[118]); 
  FFV1_2(w[116], w[34], pars->GC_11, pars->mdl_MB, pars->ZERO, w[119]); 
  FFV1_1(w[101], w[34], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[120]); 
  FFV1_1(w[101], w[4], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[121]); 
  FFV1_2(w[104], w[4], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[122]); 
  FFV1_1(w[87], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[123]); 
  FFV2_1(w[123], w[2], pars->GC_100, pars->mdl_MT, pars->mdl_WT, w[124]); 
  FFV1_2(w[88], w[49], pars->GC_11, pars->mdl_MB, pars->ZERO, w[125]); 
  FFV1_1(w[123], w[49], pars->GC_11, pars->mdl_MB, pars->ZERO, w[126]); 
  FFV1_1(w[123], w[8], pars->GC_1, pars->mdl_MB, pars->ZERO, w[127]); 
  FFS4_1(w[123], w[14], pars->GC_83, pars->mdl_MB, pars->ZERO, w[128]); 
  FFV2_3_1(w[123], w[17], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[129]);
  FFV1_2(w[116], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[130]); 
  FFV1_1(w[123], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[131]); 
  FFV1_1(w[123], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[132]); 
  FFV1_2(w[99], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[133]); 
  FFV1_2(w[88], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[134]); 
  FFV2_2(w[134], w[3], pars->GC_100, pars->mdl_MT, pars->mdl_WT, w[135]); 
  FFV1_1(w[87], w[49], pars->GC_11, pars->mdl_MB, pars->ZERO, w[136]); 
  FFV1_2(w[134], w[49], pars->GC_11, pars->mdl_MB, pars->ZERO, w[137]); 
  FFV1_2(w[134], w[8], pars->GC_1, pars->mdl_MB, pars->ZERO, w[138]); 
  FFS4_2(w[134], w[14], pars->GC_83, pars->mdl_MB, pars->ZERO, w[139]); 
  FFV2_3_2(w[134], w[17], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[140]);
  FFV1_1(w[111], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[141]); 
  FFV1_2(w[134], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[142]); 
  FFV1_2(w[134], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[143]); 
  FFV1_1(w[97], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[144]); 
  FFV1_1(w[101], w[0], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[145]); 
  FFV1_2(w[104], w[0], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[146]); 
  FFV1_1(w[111], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[147]); 
  FFV1_2(w[99], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[148]); 
  FFV1_2(w[116], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[149]); 
  FFV1_1(w[97], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[150]); 
  FFV1_1(w[87], w[78], pars->GC_11, pars->mdl_MB, pars->ZERO, w[151]); 
  FFV1_1(w[87], w[79], pars->GC_11, pars->mdl_MB, pars->ZERO, w[152]); 
  FFV1_1(w[87], w[80], pars->GC_11, pars->mdl_MB, pars->ZERO, w[153]); 
  FFV1_2(w[88], w[78], pars->GC_11, pars->mdl_MB, pars->ZERO, w[154]); 
  FFV1_2(w[88], w[79], pars->GC_11, pars->mdl_MB, pars->ZERO, w[155]); 
  FFV1_2(w[88], w[80], pars->GC_11, pars->mdl_MB, pars->ZERO, w[156]); 

  // Calculate all amplitudes
  // Amplitude(s) for diagram number 0
  FFV1_0(w[1], w[10], w[9], pars->GC_11, amp[0]); 
  FFV1_0(w[11], w[6], w[9], pars->GC_11, amp[1]); 
  FFV1_0(w[11], w[12], w[5], pars->GC_11, amp[2]); 
  FFV1_0(w[13], w[10], w[5], pars->GC_11, amp[3]); 
  FFV1_0(w[1], w[15], w[9], pars->GC_11, amp[4]); 
  FFV1_0(w[16], w[6], w[9], pars->GC_11, amp[5]); 
  FFV1_0(w[16], w[12], w[5], pars->GC_11, amp[6]); 
  FFV1_0(w[13], w[15], w[5], pars->GC_11, amp[7]); 
  FFV1_0(w[1], w[18], w[9], pars->GC_11, amp[8]); 
  FFV1_0(w[19], w[6], w[9], pars->GC_11, amp[9]); 
  FFV1_0(w[19], w[12], w[5], pars->GC_11, amp[10]); 
  FFV1_0(w[13], w[18], w[5], pars->GC_11, amp[11]); 
  FFV1_0(w[1], w[21], w[8], pars->GC_1, amp[12]); 
  FFV1_0(w[13], w[20], w[8], pars->GC_1, amp[13]); 
  FFS4_0(w[1], w[21], w[14], pars->GC_83, amp[14]); 
  FFS4_0(w[13], w[20], w[14], pars->GC_83, amp[15]); 
  FFV2_3_0(w[1], w[21], w[17], pars->GC_50, pars->GC_58, amp[16]); 
  FFV2_3_0(w[13], w[20], w[17], pars->GC_50, pars->GC_58, amp[17]); 
  FFV1_0(w[23], w[6], w[8], pars->GC_1, amp[18]); 
  FFV1_0(w[22], w[12], w[8], pars->GC_1, amp[19]); 
  FFS4_0(w[23], w[6], w[14], pars->GC_83, amp[20]); 
  FFS4_0(w[22], w[12], w[14], pars->GC_83, amp[21]); 
  FFV2_3_0(w[23], w[6], w[17], pars->GC_50, pars->GC_58, amp[22]); 
  FFV2_3_0(w[22], w[12], w[17], pars->GC_50, pars->GC_58, amp[23]); 
  FFV1_0(w[1], w[25], w[9], pars->GC_11, amp[24]); 
  FFV1_0(w[13], w[25], w[5], pars->GC_11, amp[25]); 
  FFV2_0(w[13], w[26], w[3], pars->GC_100, amp[26]); 
  FFV1_0(w[27], w[28], w[5], pars->GC_11, amp[27]); 
  FFV1_0(w[29], w[24], w[5], pars->GC_11, amp[28]); 
  FFV1_0(w[27], w[24], w[9], pars->GC_11, amp[29]); 
  FFV2_0(w[22], w[28], w[3], pars->GC_100, amp[30]); 
  FFV2_0(w[23], w[24], w[3], pars->GC_100, amp[31]); 
  FFV1_0(w[30], w[6], w[9], pars->GC_11, amp[32]); 
  FFV1_0(w[30], w[12], w[5], pars->GC_11, amp[33]); 
  FFV2_0(w[31], w[12], w[2], pars->GC_100, amp[34]); 
  FFV2_0(w[29], w[20], w[2], pars->GC_100, amp[35]); 
  FFV2_0(w[27], w[21], w[2], pars->GC_100, amp[36]); 
  FFV2_0(w[13], w[32], w[3], pars->GC_100, amp[37]); 
  FFV2_0(w[33], w[12], w[2], pars->GC_100, amp[38]); 
  FFV2_0(w[36], w[37], w[3], pars->GC_100, amp[39]); 
  FFV1_0(w[1], w[38], w[8], pars->GC_1, amp[40]); 
  FFV1_0(w[36], w[35], w[8], pars->GC_1, amp[41]); 
  FFS4_0(w[1], w[38], w[14], pars->GC_83, amp[42]); 
  FFS4_0(w[36], w[35], w[14], pars->GC_83, amp[43]); 
  FFV2_3_0(w[1], w[38], w[17], pars->GC_50, pars->GC_58, amp[44]); 
  FFV2_3_0(w[36], w[35], w[17], pars->GC_50, pars->GC_58, amp[45]); 
  FFV2_0(w[27], w[38], w[2], pars->GC_100, amp[46]); 
  FFV2_0(w[39], w[35], w[2], pars->GC_100, amp[47]); 
  FFV2_0(w[42], w[41], w[2], pars->GC_100, amp[48]); 
  FFV1_0(w[43], w[6], w[8], pars->GC_1, amp[49]); 
  FFV1_0(w[40], w[41], w[8], pars->GC_1, amp[50]); 
  FFS4_0(w[43], w[6], w[14], pars->GC_83, amp[51]); 
  FFS4_0(w[40], w[41], w[14], pars->GC_83, amp[52]); 
  FFV2_3_0(w[43], w[6], w[17], pars->GC_50, pars->GC_58, amp[53]); 
  FFV2_3_0(w[40], w[41], w[17], pars->GC_50, pars->GC_58, amp[54]); 
  FFV2_0(w[43], w[24], w[3], pars->GC_100, amp[55]); 
  FFV2_0(w[40], w[44], w[3], pars->GC_100, amp[56]); 
  FFV1_0(w[1], w[10], w[45], pars->GC_11, amp[57]); 
  FFV1_0(w[11], w[6], w[45], pars->GC_11, amp[58]); 
  FFV1_0(w[11], w[41], w[4], pars->GC_11, amp[59]); 
  FFV1_0(w[36], w[10], w[4], pars->GC_11, amp[60]); 
  FFV1_0(w[1], w[15], w[45], pars->GC_11, amp[61]); 
  FFV1_0(w[16], w[6], w[45], pars->GC_11, amp[62]); 
  FFV1_0(w[16], w[41], w[4], pars->GC_11, amp[63]); 
  FFV1_0(w[36], w[15], w[4], pars->GC_11, amp[64]); 
  FFV1_0(w[1], w[18], w[45], pars->GC_11, amp[65]); 
  FFV1_0(w[19], w[6], w[45], pars->GC_11, amp[66]); 
  FFV1_0(w[19], w[41], w[4], pars->GC_11, amp[67]); 
  FFV1_0(w[36], w[18], w[4], pars->GC_11, amp[68]); 
  FFV1_0(w[1], w[25], w[45], pars->GC_11, amp[69]); 
  FFV2_0(w[36], w[46], w[3], pars->GC_100, amp[70]); 
  FFV1_0(w[36], w[25], w[4], pars->GC_11, amp[71]); 
  FFV1_0(w[27], w[24], w[45], pars->GC_11, amp[72]); 
  FFV1_0(w[27], w[44], w[4], pars->GC_11, amp[73]); 
  FFV1_0(w[39], w[24], w[4], pars->GC_11, amp[74]); 
  FFV1_0(w[30], w[6], w[45], pars->GC_11, amp[75]); 
  FFV2_0(w[47], w[41], w[2], pars->GC_100, amp[76]); 
  FFV1_0(w[30], w[41], w[4], pars->GC_11, amp[77]); 
  FFV2_0(w[51], w[50], w[3], pars->GC_100, amp[78]); 
  FFV1_0(w[1], w[52], w[8], pars->GC_1, amp[79]); 
  FFV1_0(w[1], w[53], w[49], pars->GC_11, amp[80]); 
  FFS4_0(w[1], w[52], w[14], pars->GC_83, amp[81]); 
  FFV1_0(w[1], w[54], w[49], pars->GC_11, amp[82]); 
  FFV2_3_0(w[1], w[52], w[17], pars->GC_50, pars->GC_58, amp[83]); 
  FFV1_0(w[1], w[55], w[49], pars->GC_11, amp[84]); 
  FFV2_0(w[27], w[52], w[2], pars->GC_100, amp[85]); 
  FFV1_0(w[27], w[50], w[49], pars->GC_11, amp[86]); 
  FFV1_0(w[42], w[50], w[5], pars->GC_11, amp[87]); 
  FFV2_0(w[56], w[50], w[3], pars->GC_100, amp[88]); 
  FFV2_0(w[42], w[57], w[2], pars->GC_100, amp[89]); 
  FFV1_0(w[40], w[53], w[5], pars->GC_11, amp[90]); 
  FFV1_0(w[40], w[57], w[8], pars->GC_1, amp[91]); 
  FFV1_0(w[40], w[54], w[5], pars->GC_11, amp[92]); 
  FFS4_0(w[40], w[57], w[14], pars->GC_83, amp[93]); 
  FFV1_0(w[40], w[55], w[5], pars->GC_11, amp[94]); 
  FFV2_3_0(w[40], w[57], w[17], pars->GC_50, pars->GC_58, amp[95]); 
  FFV1_0(w[11], w[58], w[5], pars->GC_11, amp[96]); 
  FFV1_0(w[11], w[57], w[4], pars->GC_11, amp[97]); 
  FFV1_0(w[16], w[58], w[5], pars->GC_11, amp[98]); 
  FFV1_0(w[16], w[57], w[4], pars->GC_11, amp[99]); 
  FFV1_0(w[19], w[58], w[5], pars->GC_11, amp[100]); 
  FFV1_0(w[19], w[57], w[4], pars->GC_11, amp[101]); 
  FFV1_0(w[22], w[58], w[8], pars->GC_1, amp[102]); 
  FFV1_0(w[22], w[53], w[4], pars->GC_11, amp[103]); 
  FFS4_0(w[22], w[58], w[14], pars->GC_83, amp[104]); 
  FFV1_0(w[22], w[54], w[4], pars->GC_11, amp[105]); 
  FFV2_3_0(w[22], w[58], w[17], pars->GC_50, pars->GC_58, amp[106]); 
  FFV1_0(w[22], w[55], w[4], pars->GC_11, amp[107]); 
  FFV1_0(w[30], w[58], w[5], pars->GC_11, amp[108]); 
  FFV2_0(w[31], w[58], w[2], pars->GC_100, amp[109]); 
  FFV1_0(w[47], w[50], w[5], pars->GC_11, amp[110]); 
  FFV1_0(w[31], w[50], w[4], pars->GC_11, amp[111]); 
  FFV2_0(w[47], w[57], w[2], pars->GC_100, amp[112]); 
  FFV1_0(w[30], w[57], w[4], pars->GC_11, amp[113]); 
  FFV2_0(w[33], w[58], w[2], pars->GC_100, amp[114]); 
  FFV2_0(w[59], w[50], w[3], pars->GC_100, amp[115]); 
  FFV1_0(w[33], w[50], w[4], pars->GC_11, amp[116]); 
  FFV2_0(w[61], w[62], w[2], pars->GC_100, amp[117]); 
  FFV1_0(w[63], w[6], w[8], pars->GC_1, amp[118]); 
  FFV1_0(w[64], w[6], w[49], pars->GC_11, amp[119]); 
  FFS4_0(w[63], w[6], w[14], pars->GC_83, amp[120]); 
  FFV1_0(w[65], w[6], w[49], pars->GC_11, amp[121]); 
  FFV2_3_0(w[63], w[6], w[17], pars->GC_50, pars->GC_58, amp[122]); 
  FFV1_0(w[66], w[6], w[49], pars->GC_11, amp[123]); 
  FFV2_0(w[63], w[24], w[3], pars->GC_100, amp[124]); 
  FFV1_0(w[61], w[24], w[49], pars->GC_11, amp[125]); 
  FFV1_0(w[61], w[37], w[5], pars->GC_11, amp[126]); 
  FFV2_0(w[61], w[67], w[2], pars->GC_100, amp[127]); 
  FFV2_0(w[68], w[37], w[3], pars->GC_100, amp[128]); 
  FFV1_0(w[64], w[35], w[5], pars->GC_11, amp[129]); 
  FFV1_0(w[68], w[35], w[8], pars->GC_1, amp[130]); 
  FFV1_0(w[65], w[35], w[5], pars->GC_11, amp[131]); 
  FFS4_0(w[68], w[35], w[14], pars->GC_83, amp[132]); 
  FFV1_0(w[66], w[35], w[5], pars->GC_11, amp[133]); 
  FFV2_3_0(w[68], w[35], w[17], pars->GC_50, pars->GC_58, amp[134]); 
  FFV1_0(w[69], w[10], w[5], pars->GC_11, amp[135]); 
  FFV1_0(w[68], w[10], w[4], pars->GC_11, amp[136]); 
  FFV1_0(w[69], w[15], w[5], pars->GC_11, amp[137]); 
  FFV1_0(w[68], w[15], w[4], pars->GC_11, amp[138]); 
  FFV1_0(w[69], w[18], w[5], pars->GC_11, amp[139]); 
  FFV1_0(w[68], w[18], w[4], pars->GC_11, amp[140]); 
  FFV1_0(w[69], w[20], w[8], pars->GC_1, amp[141]); 
  FFV1_0(w[64], w[20], w[4], pars->GC_11, amp[142]); 
  FFS4_0(w[69], w[20], w[14], pars->GC_83, amp[143]); 
  FFV1_0(w[65], w[20], w[4], pars->GC_11, amp[144]); 
  FFV2_3_0(w[69], w[20], w[17], pars->GC_50, pars->GC_58, amp[145]); 
  FFV1_0(w[66], w[20], w[4], pars->GC_11, amp[146]); 
  FFV1_0(w[69], w[25], w[5], pars->GC_11, amp[147]); 
  FFV2_0(w[69], w[26], w[3], pars->GC_100, amp[148]); 
  FFV1_0(w[61], w[46], w[5], pars->GC_11, amp[149]); 
  FFV1_0(w[61], w[26], w[4], pars->GC_11, amp[150]); 
  FFV2_0(w[68], w[46], w[3], pars->GC_100, amp[151]); 
  FFV1_0(w[68], w[25], w[4], pars->GC_11, amp[152]); 
  FFV2_0(w[69], w[32], w[3], pars->GC_100, amp[153]); 
  FFV2_0(w[61], w[70], w[2], pars->GC_100, amp[154]); 
  FFV1_0(w[61], w[32], w[4], pars->GC_11, amp[155]); 
  FFV1_0(w[1], w[10], w[71], pars->GC_11, amp[156]); 
  FFV1_0(w[11], w[6], w[71], pars->GC_11, amp[157]); 
  FFV1_0(w[11], w[62], w[0], pars->GC_11, amp[158]); 
  FFV1_0(w[51], w[10], w[0], pars->GC_11, amp[159]); 
  FFV1_0(w[1], w[15], w[71], pars->GC_11, amp[160]); 
  FFV1_0(w[16], w[6], w[71], pars->GC_11, amp[161]); 
  FFV1_0(w[16], w[62], w[0], pars->GC_11, amp[162]); 
  FFV1_0(w[51], w[15], w[0], pars->GC_11, amp[163]); 
  FFV1_0(w[1], w[18], w[71], pars->GC_11, amp[164]); 
  FFV1_0(w[19], w[6], w[71], pars->GC_11, amp[165]); 
  FFV1_0(w[19], w[62], w[0], pars->GC_11, amp[166]); 
  FFV1_0(w[51], w[18], w[0], pars->GC_11, amp[167]); 
  FFV1_0(w[1], w[25], w[71], pars->GC_11, amp[168]); 
  FFV2_0(w[51], w[72], w[3], pars->GC_100, amp[169]); 
  FFV1_0(w[51], w[25], w[0], pars->GC_11, amp[170]); 
  FFV1_0(w[27], w[24], w[71], pars->GC_11, amp[171]); 
  FFV1_0(w[27], w[72], w[49], pars->GC_11, amp[172]); 
  FFV1_0(w[73], w[24], w[49], pars->GC_11, amp[173]); 
  FFV1_0(w[30], w[6], w[71], pars->GC_11, amp[174]); 
  FFV2_0(w[73], w[62], w[2], pars->GC_100, amp[175]); 
  FFV1_0(w[30], w[62], w[0], pars->GC_11, amp[176]); 
  FFV1_0(w[11], w[74], w[5], pars->GC_11, amp[177]); 
  FFV1_0(w[11], w[67], w[0], pars->GC_11, amp[178]); 
  FFV1_0(w[16], w[74], w[5], pars->GC_11, amp[179]); 
  FFV1_0(w[16], w[67], w[0], pars->GC_11, amp[180]); 
  FFV1_0(w[19], w[74], w[5], pars->GC_11, amp[181]); 
  FFV1_0(w[19], w[67], w[0], pars->GC_11, amp[182]); 
  FFV1_0(w[22], w[74], w[8], pars->GC_1, amp[183]); 
  FFV1_0(w[75], w[35], w[8], pars->GC_1, amp[184]); 
  FFS4_0(w[22], w[74], w[14], pars->GC_83, amp[185]); 
  FFS4_0(w[75], w[35], w[14], pars->GC_83, amp[186]); 
  FFV2_3_0(w[22], w[74], w[17], pars->GC_50, pars->GC_58, amp[187]); 
  FFV2_3_0(w[75], w[35], w[17], pars->GC_50, pars->GC_58, amp[188]); 
  FFV1_0(w[30], w[74], w[5], pars->GC_11, amp[189]); 
  FFV2_0(w[31], w[74], w[2], pars->GC_100, amp[190]); 
  FFV1_0(w[73], w[37], w[5], pars->GC_11, amp[191]); 
  FFV2_0(w[73], w[67], w[2], pars->GC_100, amp[192]); 
  FFV1_0(w[31], w[37], w[0], pars->GC_11, amp[193]); 
  FFV1_0(w[30], w[67], w[0], pars->GC_11, amp[194]); 
  FFV2_0(w[33], w[74], w[2], pars->GC_100, amp[195]); 
  FFV2_0(w[75], w[37], w[3], pars->GC_100, amp[196]); 
  FFV1_0(w[33], w[37], w[0], pars->GC_11, amp[197]); 
  FFV1_0(w[76], w[10], w[5], pars->GC_11, amp[198]); 
  FFV1_0(w[56], w[10], w[0], pars->GC_11, amp[199]); 
  FFV1_0(w[76], w[15], w[5], pars->GC_11, amp[200]); 
  FFV1_0(w[56], w[15], w[0], pars->GC_11, amp[201]); 
  FFV1_0(w[76], w[18], w[5], pars->GC_11, amp[202]); 
  FFV1_0(w[56], w[18], w[0], pars->GC_11, amp[203]); 
  FFV1_0(w[76], w[20], w[8], pars->GC_1, amp[204]); 
  FFV1_0(w[40], w[77], w[8], pars->GC_1, amp[205]); 
  FFS4_0(w[76], w[20], w[14], pars->GC_83, amp[206]); 
  FFS4_0(w[40], w[77], w[14], pars->GC_83, amp[207]); 
  FFV2_3_0(w[76], w[20], w[17], pars->GC_50, pars->GC_58, amp[208]); 
  FFV2_3_0(w[40], w[77], w[17], pars->GC_50, pars->GC_58, amp[209]); 
  FFV1_0(w[76], w[25], w[5], pars->GC_11, amp[210]); 
  FFV2_0(w[76], w[26], w[3], pars->GC_100, amp[211]); 
  FFV1_0(w[42], w[72], w[5], pars->GC_11, amp[212]); 
  FFV2_0(w[56], w[72], w[3], pars->GC_100, amp[213]); 
  FFV1_0(w[42], w[26], w[0], pars->GC_11, amp[214]); 
  FFV1_0(w[56], w[25], w[0], pars->GC_11, amp[215]); 
  FFV2_0(w[76], w[32], w[3], pars->GC_100, amp[216]); 
  FFV2_0(w[42], w[77], w[2], pars->GC_100, amp[217]); 
  FFV1_0(w[42], w[32], w[0], pars->GC_11, amp[218]); 
  FFV1_0(w[11], w[77], w[4], pars->GC_11, amp[219]); 
  FFV1_0(w[11], w[70], w[0], pars->GC_11, amp[220]); 
  FFV1_0(w[16], w[77], w[4], pars->GC_11, amp[221]); 
  FFV1_0(w[16], w[70], w[0], pars->GC_11, amp[222]); 
  FFV1_0(w[19], w[77], w[4], pars->GC_11, amp[223]); 
  FFV1_0(w[19], w[70], w[0], pars->GC_11, amp[224]); 
  FFV1_0(w[75], w[10], w[4], pars->GC_11, amp[225]); 
  FFV1_0(w[59], w[10], w[0], pars->GC_11, amp[226]); 
  FFV1_0(w[75], w[15], w[4], pars->GC_11, amp[227]); 
  FFV1_0(w[59], w[15], w[0], pars->GC_11, amp[228]); 
  FFV1_0(w[75], w[18], w[4], pars->GC_11, amp[229]); 
  FFV1_0(w[59], w[18], w[0], pars->GC_11, amp[230]); 
  FFV1_0(w[47], w[72], w[5], pars->GC_11, amp[231]); 
  FFV1_0(w[31], w[72], w[4], pars->GC_11, amp[232]); 
  FFV1_0(w[73], w[46], w[5], pars->GC_11, amp[233]); 
  FFV1_0(w[73], w[26], w[4], pars->GC_11, amp[234]); 
  FFV1_0(w[31], w[46], w[0], pars->GC_11, amp[235]); 
  FFV1_0(w[47], w[26], w[0], pars->GC_11, amp[236]); 
  FFV2_0(w[59], w[72], w[3], pars->GC_100, amp[237]); 
  FFV1_0(w[33], w[72], w[4], pars->GC_11, amp[238]); 
  FFV2_0(w[75], w[46], w[3], pars->GC_100, amp[239]); 
  FFV1_0(w[75], w[25], w[4], pars->GC_11, amp[240]); 
  FFV1_0(w[33], w[46], w[0], pars->GC_11, amp[241]); 
  FFV1_0(w[59], w[25], w[0], pars->GC_11, amp[242]); 
  FFV2_0(w[73], w[70], w[2], pars->GC_100, amp[243]); 
  FFV1_0(w[73], w[32], w[4], pars->GC_11, amp[244]); 
  FFV2_0(w[47], w[77], w[2], pars->GC_100, amp[245]); 
  FFV1_0(w[30], w[77], w[4], pars->GC_11, amp[246]); 
  FFV1_0(w[47], w[32], w[0], pars->GC_11, amp[247]); 
  FFV1_0(w[30], w[70], w[0], pars->GC_11, amp[248]); 
  FFV1_0(w[1], w[81], w[8], pars->GC_1, amp[249]); 
  FFV1_0(w[1], w[82], w[8], pars->GC_1, amp[250]); 
  FFV1_0(w[1], w[83], w[8], pars->GC_1, amp[251]); 
  FFV1_0(w[84], w[6], w[8], pars->GC_1, amp[252]); 
  FFV1_0(w[85], w[6], w[8], pars->GC_1, amp[253]); 
  FFV1_0(w[86], w[6], w[8], pars->GC_1, amp[254]); 
  FFS4_0(w[1], w[81], w[14], pars->GC_83, amp[255]); 
  FFS4_0(w[1], w[82], w[14], pars->GC_83, amp[256]); 
  FFS4_0(w[1], w[83], w[14], pars->GC_83, amp[257]); 
  FFS4_0(w[84], w[6], w[14], pars->GC_83, amp[258]); 
  FFS4_0(w[85], w[6], w[14], pars->GC_83, amp[259]); 
  FFS4_0(w[86], w[6], w[14], pars->GC_83, amp[260]); 
  FFV2_3_0(w[1], w[81], w[17], pars->GC_50, pars->GC_58, amp[261]); 
  FFV2_3_0(w[1], w[82], w[17], pars->GC_50, pars->GC_58, amp[262]); 
  FFV2_3_0(w[1], w[83], w[17], pars->GC_50, pars->GC_58, amp[263]); 
  FFV2_3_0(w[84], w[6], w[17], pars->GC_50, pars->GC_58, amp[264]); 
  FFV2_3_0(w[85], w[6], w[17], pars->GC_50, pars->GC_58, amp[265]); 
  FFV2_3_0(w[86], w[6], w[17], pars->GC_50, pars->GC_58, amp[266]); 
  FFV2_0(w[84], w[24], w[3], pars->GC_100, amp[267]); 
  FFV2_0(w[85], w[24], w[3], pars->GC_100, amp[268]); 
  FFV2_0(w[86], w[24], w[3], pars->GC_100, amp[269]); 
  FFV1_0(w[27], w[24], w[78], pars->GC_11, amp[270]); 
  FFV1_0(w[27], w[24], w[79], pars->GC_11, amp[271]); 
  FFV1_0(w[27], w[24], w[80], pars->GC_11, amp[272]); 
  FFV2_0(w[27], w[81], w[2], pars->GC_100, amp[273]); 
  FFV2_0(w[27], w[82], w[2], pars->GC_100, amp[274]); 
  FFV2_0(w[27], w[83], w[2], pars->GC_100, amp[275]); 
  FFV1_0(w[88], w[89], w[9], pars->GC_11, amp[276]); 
  FFV1_0(w[90], w[87], w[9], pars->GC_11, amp[277]); 
  FFV1_0(w[90], w[91], w[5], pars->GC_11, amp[278]); 
  FFV1_0(w[92], w[89], w[5], pars->GC_11, amp[279]); 
  FFV1_0(w[88], w[93], w[9], pars->GC_11, amp[280]); 
  FFV1_0(w[94], w[87], w[9], pars->GC_11, amp[281]); 
  FFV1_0(w[94], w[91], w[5], pars->GC_11, amp[282]); 
  FFV1_0(w[92], w[93], w[5], pars->GC_11, amp[283]); 
  FFV1_0(w[88], w[95], w[9], pars->GC_11, amp[284]); 
  FFV1_0(w[96], w[87], w[9], pars->GC_11, amp[285]); 
  FFV1_0(w[96], w[91], w[5], pars->GC_11, amp[286]); 
  FFV1_0(w[92], w[95], w[5], pars->GC_11, amp[287]); 
  FFV1_0(w[88], w[98], w[8], pars->GC_1, amp[288]); 
  FFV1_0(w[92], w[97], w[8], pars->GC_1, amp[289]); 
  FFS4_0(w[88], w[98], w[14], pars->GC_83, amp[290]); 
  FFS4_0(w[92], w[97], w[14], pars->GC_83, amp[291]); 
  FFV2_3_0(w[88], w[98], w[17], pars->GC_50, pars->GC_58, amp[292]); 
  FFV2_3_0(w[92], w[97], w[17], pars->GC_50, pars->GC_58, amp[293]); 
  FFV1_0(w[100], w[87], w[8], pars->GC_1, amp[294]); 
  FFV1_0(w[99], w[91], w[8], pars->GC_1, amp[295]); 
  FFS4_0(w[100], w[87], w[14], pars->GC_83, amp[296]); 
  FFS4_0(w[99], w[91], w[14], pars->GC_83, amp[297]); 
  FFV2_3_0(w[100], w[87], w[17], pars->GC_50, pars->GC_58, amp[298]); 
  FFV2_3_0(w[99], w[91], w[17], pars->GC_50, pars->GC_58, amp[299]); 
  FFV1_0(w[88], w[102], w[9], pars->GC_11, amp[300]); 
  FFV1_0(w[92], w[102], w[5], pars->GC_11, amp[301]); 
  FFV2_0(w[92], w[103], w[3], pars->GC_100, amp[302]); 
  FFV1_0(w[104], w[105], w[5], pars->GC_11, amp[303]); 
  FFV1_0(w[106], w[101], w[5], pars->GC_11, amp[304]); 
  FFV1_0(w[104], w[101], w[9], pars->GC_11, amp[305]); 
  FFV2_0(w[99], w[105], w[3], pars->GC_100, amp[306]); 
  FFV2_0(w[100], w[101], w[3], pars->GC_100, amp[307]); 
  FFV1_0(w[107], w[87], w[9], pars->GC_11, amp[308]); 
  FFV1_0(w[107], w[91], w[5], pars->GC_11, amp[309]); 
  FFV2_0(w[108], w[91], w[2], pars->GC_100, amp[310]); 
  FFV2_0(w[106], w[97], w[2], pars->GC_100, amp[311]); 
  FFV2_0(w[104], w[98], w[2], pars->GC_100, amp[312]); 
  FFV2_0(w[92], w[109], w[3], pars->GC_100, amp[313]); 
  FFV2_0(w[110], w[91], w[2], pars->GC_100, amp[314]); 
  FFV2_0(w[112], w[113], w[3], pars->GC_100, amp[315]); 
  FFV1_0(w[88], w[114], w[8], pars->GC_1, amp[316]); 
  FFV1_0(w[112], w[111], w[8], pars->GC_1, amp[317]); 
  FFS4_0(w[88], w[114], w[14], pars->GC_83, amp[318]); 
  FFS4_0(w[112], w[111], w[14], pars->GC_83, amp[319]); 
  FFV2_3_0(w[88], w[114], w[17], pars->GC_50, pars->GC_58, amp[320]); 
  FFV2_3_0(w[112], w[111], w[17], pars->GC_50, pars->GC_58, amp[321]); 
  FFV2_0(w[104], w[114], w[2], pars->GC_100, amp[322]); 
  FFV2_0(w[115], w[111], w[2], pars->GC_100, amp[323]); 
  FFV2_0(w[118], w[117], w[2], pars->GC_100, amp[324]); 
  FFV1_0(w[119], w[87], w[8], pars->GC_1, amp[325]); 
  FFV1_0(w[116], w[117], w[8], pars->GC_1, amp[326]); 
  FFS4_0(w[119], w[87], w[14], pars->GC_83, amp[327]); 
  FFS4_0(w[116], w[117], w[14], pars->GC_83, amp[328]); 
  FFV2_3_0(w[119], w[87], w[17], pars->GC_50, pars->GC_58, amp[329]); 
  FFV2_3_0(w[116], w[117], w[17], pars->GC_50, pars->GC_58, amp[330]); 
  FFV2_0(w[119], w[101], w[3], pars->GC_100, amp[331]); 
  FFV2_0(w[116], w[120], w[3], pars->GC_100, amp[332]); 
  FFV1_0(w[88], w[89], w[45], pars->GC_11, amp[333]); 
  FFV1_0(w[90], w[87], w[45], pars->GC_11, amp[334]); 
  FFV1_0(w[90], w[117], w[4], pars->GC_11, amp[335]); 
  FFV1_0(w[112], w[89], w[4], pars->GC_11, amp[336]); 
  FFV1_0(w[88], w[93], w[45], pars->GC_11, amp[337]); 
  FFV1_0(w[94], w[87], w[45], pars->GC_11, amp[338]); 
  FFV1_0(w[94], w[117], w[4], pars->GC_11, amp[339]); 
  FFV1_0(w[112], w[93], w[4], pars->GC_11, amp[340]); 
  FFV1_0(w[88], w[95], w[45], pars->GC_11, amp[341]); 
  FFV1_0(w[96], w[87], w[45], pars->GC_11, amp[342]); 
  FFV1_0(w[96], w[117], w[4], pars->GC_11, amp[343]); 
  FFV1_0(w[112], w[95], w[4], pars->GC_11, amp[344]); 
  FFV1_0(w[88], w[102], w[45], pars->GC_11, amp[345]); 
  FFV2_0(w[112], w[121], w[3], pars->GC_100, amp[346]); 
  FFV1_0(w[112], w[102], w[4], pars->GC_11, amp[347]); 
  FFV1_0(w[104], w[101], w[45], pars->GC_11, amp[348]); 
  FFV1_0(w[104], w[120], w[4], pars->GC_11, amp[349]); 
  FFV1_0(w[115], w[101], w[4], pars->GC_11, amp[350]); 
  FFV1_0(w[107], w[87], w[45], pars->GC_11, amp[351]); 
  FFV2_0(w[122], w[117], w[2], pars->GC_100, amp[352]); 
  FFV1_0(w[107], w[117], w[4], pars->GC_11, amp[353]); 
  FFV2_0(w[125], w[124], w[3], pars->GC_100, amp[354]); 
  FFV1_0(w[88], w[126], w[8], pars->GC_1, amp[355]); 
  FFV1_0(w[88], w[127], w[49], pars->GC_11, amp[356]); 
  FFS4_0(w[88], w[126], w[14], pars->GC_83, amp[357]); 
  FFV1_0(w[88], w[128], w[49], pars->GC_11, amp[358]); 
  FFV2_3_0(w[88], w[126], w[17], pars->GC_50, pars->GC_58, amp[359]); 
  FFV1_0(w[88], w[129], w[49], pars->GC_11, amp[360]); 
  FFV2_0(w[104], w[126], w[2], pars->GC_100, amp[361]); 
  FFV1_0(w[104], w[124], w[49], pars->GC_11, amp[362]); 
  FFV1_0(w[118], w[124], w[5], pars->GC_11, amp[363]); 
  FFV2_0(w[130], w[124], w[3], pars->GC_100, amp[364]); 
  FFV2_0(w[118], w[131], w[2], pars->GC_100, amp[365]); 
  FFV1_0(w[116], w[127], w[5], pars->GC_11, amp[366]); 
  FFV1_0(w[116], w[131], w[8], pars->GC_1, amp[367]); 
  FFV1_0(w[116], w[128], w[5], pars->GC_11, amp[368]); 
  FFS4_0(w[116], w[131], w[14], pars->GC_83, amp[369]); 
  FFV1_0(w[116], w[129], w[5], pars->GC_11, amp[370]); 
  FFV2_3_0(w[116], w[131], w[17], pars->GC_50, pars->GC_58, amp[371]); 
  FFV1_0(w[90], w[132], w[5], pars->GC_11, amp[372]); 
  FFV1_0(w[90], w[131], w[4], pars->GC_11, amp[373]); 
  FFV1_0(w[94], w[132], w[5], pars->GC_11, amp[374]); 
  FFV1_0(w[94], w[131], w[4], pars->GC_11, amp[375]); 
  FFV1_0(w[96], w[132], w[5], pars->GC_11, amp[376]); 
  FFV1_0(w[96], w[131], w[4], pars->GC_11, amp[377]); 
  FFV1_0(w[99], w[132], w[8], pars->GC_1, amp[378]); 
  FFV1_0(w[99], w[127], w[4], pars->GC_11, amp[379]); 
  FFS4_0(w[99], w[132], w[14], pars->GC_83, amp[380]); 
  FFV1_0(w[99], w[128], w[4], pars->GC_11, amp[381]); 
  FFV2_3_0(w[99], w[132], w[17], pars->GC_50, pars->GC_58, amp[382]); 
  FFV1_0(w[99], w[129], w[4], pars->GC_11, amp[383]); 
  FFV1_0(w[107], w[132], w[5], pars->GC_11, amp[384]); 
  FFV2_0(w[108], w[132], w[2], pars->GC_100, amp[385]); 
  FFV1_0(w[122], w[124], w[5], pars->GC_11, amp[386]); 
  FFV1_0(w[108], w[124], w[4], pars->GC_11, amp[387]); 
  FFV2_0(w[122], w[131], w[2], pars->GC_100, amp[388]); 
  FFV1_0(w[107], w[131], w[4], pars->GC_11, amp[389]); 
  FFV2_0(w[110], w[132], w[2], pars->GC_100, amp[390]); 
  FFV2_0(w[133], w[124], w[3], pars->GC_100, amp[391]); 
  FFV1_0(w[110], w[124], w[4], pars->GC_11, amp[392]); 
  FFV2_0(w[135], w[136], w[2], pars->GC_100, amp[393]); 
  FFV1_0(w[137], w[87], w[8], pars->GC_1, amp[394]); 
  FFV1_0(w[138], w[87], w[49], pars->GC_11, amp[395]); 
  FFS4_0(w[137], w[87], w[14], pars->GC_83, amp[396]); 
  FFV1_0(w[139], w[87], w[49], pars->GC_11, amp[397]); 
  FFV2_3_0(w[137], w[87], w[17], pars->GC_50, pars->GC_58, amp[398]); 
  FFV1_0(w[140], w[87], w[49], pars->GC_11, amp[399]); 
  FFV2_0(w[137], w[101], w[3], pars->GC_100, amp[400]); 
  FFV1_0(w[135], w[101], w[49], pars->GC_11, amp[401]); 
  FFV1_0(w[135], w[113], w[5], pars->GC_11, amp[402]); 
  FFV2_0(w[135], w[141], w[2], pars->GC_100, amp[403]); 
  FFV2_0(w[142], w[113], w[3], pars->GC_100, amp[404]); 
  FFV1_0(w[138], w[111], w[5], pars->GC_11, amp[405]); 
  FFV1_0(w[142], w[111], w[8], pars->GC_1, amp[406]); 
  FFV1_0(w[139], w[111], w[5], pars->GC_11, amp[407]); 
  FFS4_0(w[142], w[111], w[14], pars->GC_83, amp[408]); 
  FFV1_0(w[140], w[111], w[5], pars->GC_11, amp[409]); 
  FFV2_3_0(w[142], w[111], w[17], pars->GC_50, pars->GC_58, amp[410]); 
  FFV1_0(w[143], w[89], w[5], pars->GC_11, amp[411]); 
  FFV1_0(w[142], w[89], w[4], pars->GC_11, amp[412]); 
  FFV1_0(w[143], w[93], w[5], pars->GC_11, amp[413]); 
  FFV1_0(w[142], w[93], w[4], pars->GC_11, amp[414]); 
  FFV1_0(w[143], w[95], w[5], pars->GC_11, amp[415]); 
  FFV1_0(w[142], w[95], w[4], pars->GC_11, amp[416]); 
  FFV1_0(w[143], w[97], w[8], pars->GC_1, amp[417]); 
  FFV1_0(w[138], w[97], w[4], pars->GC_11, amp[418]); 
  FFS4_0(w[143], w[97], w[14], pars->GC_83, amp[419]); 
  FFV1_0(w[139], w[97], w[4], pars->GC_11, amp[420]); 
  FFV2_3_0(w[143], w[97], w[17], pars->GC_50, pars->GC_58, amp[421]); 
  FFV1_0(w[140], w[97], w[4], pars->GC_11, amp[422]); 
  FFV1_0(w[143], w[102], w[5], pars->GC_11, amp[423]); 
  FFV2_0(w[143], w[103], w[3], pars->GC_100, amp[424]); 
  FFV1_0(w[135], w[121], w[5], pars->GC_11, amp[425]); 
  FFV1_0(w[135], w[103], w[4], pars->GC_11, amp[426]); 
  FFV2_0(w[142], w[121], w[3], pars->GC_100, amp[427]); 
  FFV1_0(w[142], w[102], w[4], pars->GC_11, amp[428]); 
  FFV2_0(w[143], w[109], w[3], pars->GC_100, amp[429]); 
  FFV2_0(w[135], w[144], w[2], pars->GC_100, amp[430]); 
  FFV1_0(w[135], w[109], w[4], pars->GC_11, amp[431]); 
  FFV1_0(w[88], w[89], w[71], pars->GC_11, amp[432]); 
  FFV1_0(w[90], w[87], w[71], pars->GC_11, amp[433]); 
  FFV1_0(w[90], w[136], w[0], pars->GC_11, amp[434]); 
  FFV1_0(w[125], w[89], w[0], pars->GC_11, amp[435]); 
  FFV1_0(w[88], w[93], w[71], pars->GC_11, amp[436]); 
  FFV1_0(w[94], w[87], w[71], pars->GC_11, amp[437]); 
  FFV1_0(w[94], w[136], w[0], pars->GC_11, amp[438]); 
  FFV1_0(w[125], w[93], w[0], pars->GC_11, amp[439]); 
  FFV1_0(w[88], w[95], w[71], pars->GC_11, amp[440]); 
  FFV1_0(w[96], w[87], w[71], pars->GC_11, amp[441]); 
  FFV1_0(w[96], w[136], w[0], pars->GC_11, amp[442]); 
  FFV1_0(w[125], w[95], w[0], pars->GC_11, amp[443]); 
  FFV1_0(w[88], w[102], w[71], pars->GC_11, amp[444]); 
  FFV2_0(w[125], w[145], w[3], pars->GC_100, amp[445]); 
  FFV1_0(w[125], w[102], w[0], pars->GC_11, amp[446]); 
  FFV1_0(w[104], w[101], w[71], pars->GC_11, amp[447]); 
  FFV1_0(w[104], w[145], w[49], pars->GC_11, amp[448]); 
  FFV1_0(w[146], w[101], w[49], pars->GC_11, amp[449]); 
  FFV1_0(w[107], w[87], w[71], pars->GC_11, amp[450]); 
  FFV2_0(w[146], w[136], w[2], pars->GC_100, amp[451]); 
  FFV1_0(w[107], w[136], w[0], pars->GC_11, amp[452]); 
  FFV1_0(w[90], w[147], w[5], pars->GC_11, amp[453]); 
  FFV1_0(w[90], w[141], w[0], pars->GC_11, amp[454]); 
  FFV1_0(w[94], w[147], w[5], pars->GC_11, amp[455]); 
  FFV1_0(w[94], w[141], w[0], pars->GC_11, amp[456]); 
  FFV1_0(w[96], w[147], w[5], pars->GC_11, amp[457]); 
  FFV1_0(w[96], w[141], w[0], pars->GC_11, amp[458]); 
  FFV1_0(w[99], w[147], w[8], pars->GC_1, amp[459]); 
  FFV1_0(w[148], w[111], w[8], pars->GC_1, amp[460]); 
  FFS4_0(w[99], w[147], w[14], pars->GC_83, amp[461]); 
  FFS4_0(w[148], w[111], w[14], pars->GC_83, amp[462]); 
  FFV2_3_0(w[99], w[147], w[17], pars->GC_50, pars->GC_58, amp[463]); 
  FFV2_3_0(w[148], w[111], w[17], pars->GC_50, pars->GC_58, amp[464]); 
  FFV1_0(w[107], w[147], w[5], pars->GC_11, amp[465]); 
  FFV2_0(w[108], w[147], w[2], pars->GC_100, amp[466]); 
  FFV1_0(w[146], w[113], w[5], pars->GC_11, amp[467]); 
  FFV2_0(w[146], w[141], w[2], pars->GC_100, amp[468]); 
  FFV1_0(w[108], w[113], w[0], pars->GC_11, amp[469]); 
  FFV1_0(w[107], w[141], w[0], pars->GC_11, amp[470]); 
  FFV2_0(w[110], w[147], w[2], pars->GC_100, amp[471]); 
  FFV2_0(w[148], w[113], w[3], pars->GC_100, amp[472]); 
  FFV1_0(w[110], w[113], w[0], pars->GC_11, amp[473]); 
  FFV1_0(w[149], w[89], w[5], pars->GC_11, amp[474]); 
  FFV1_0(w[130], w[89], w[0], pars->GC_11, amp[475]); 
  FFV1_0(w[149], w[93], w[5], pars->GC_11, amp[476]); 
  FFV1_0(w[130], w[93], w[0], pars->GC_11, amp[477]); 
  FFV1_0(w[149], w[95], w[5], pars->GC_11, amp[478]); 
  FFV1_0(w[130], w[95], w[0], pars->GC_11, amp[479]); 
  FFV1_0(w[149], w[97], w[8], pars->GC_1, amp[480]); 
  FFV1_0(w[116], w[150], w[8], pars->GC_1, amp[481]); 
  FFS4_0(w[149], w[97], w[14], pars->GC_83, amp[482]); 
  FFS4_0(w[116], w[150], w[14], pars->GC_83, amp[483]); 
  FFV2_3_0(w[149], w[97], w[17], pars->GC_50, pars->GC_58, amp[484]); 
  FFV2_3_0(w[116], w[150], w[17], pars->GC_50, pars->GC_58, amp[485]); 
  FFV1_0(w[149], w[102], w[5], pars->GC_11, amp[486]); 
  FFV2_0(w[149], w[103], w[3], pars->GC_100, amp[487]); 
  FFV1_0(w[118], w[145], w[5], pars->GC_11, amp[488]); 
  FFV2_0(w[130], w[145], w[3], pars->GC_100, amp[489]); 
  FFV1_0(w[118], w[103], w[0], pars->GC_11, amp[490]); 
  FFV1_0(w[130], w[102], w[0], pars->GC_11, amp[491]); 
  FFV2_0(w[149], w[109], w[3], pars->GC_100, amp[492]); 
  FFV2_0(w[118], w[150], w[2], pars->GC_100, amp[493]); 
  FFV1_0(w[118], w[109], w[0], pars->GC_11, amp[494]); 
  FFV1_0(w[90], w[150], w[4], pars->GC_11, amp[495]); 
  FFV1_0(w[90], w[144], w[0], pars->GC_11, amp[496]); 
  FFV1_0(w[94], w[150], w[4], pars->GC_11, amp[497]); 
  FFV1_0(w[94], w[144], w[0], pars->GC_11, amp[498]); 
  FFV1_0(w[96], w[150], w[4], pars->GC_11, amp[499]); 
  FFV1_0(w[96], w[144], w[0], pars->GC_11, amp[500]); 
  FFV1_0(w[148], w[89], w[4], pars->GC_11, amp[501]); 
  FFV1_0(w[133], w[89], w[0], pars->GC_11, amp[502]); 
  FFV1_0(w[148], w[93], w[4], pars->GC_11, amp[503]); 
  FFV1_0(w[133], w[93], w[0], pars->GC_11, amp[504]); 
  FFV1_0(w[148], w[95], w[4], pars->GC_11, amp[505]); 
  FFV1_0(w[133], w[95], w[0], pars->GC_11, amp[506]); 
  FFV1_0(w[122], w[145], w[5], pars->GC_11, amp[507]); 
  FFV1_0(w[108], w[145], w[4], pars->GC_11, amp[508]); 
  FFV1_0(w[146], w[121], w[5], pars->GC_11, amp[509]); 
  FFV1_0(w[146], w[103], w[4], pars->GC_11, amp[510]); 
  FFV1_0(w[108], w[121], w[0], pars->GC_11, amp[511]); 
  FFV1_0(w[122], w[103], w[0], pars->GC_11, amp[512]); 
  FFV2_0(w[133], w[145], w[3], pars->GC_100, amp[513]); 
  FFV1_0(w[110], w[145], w[4], pars->GC_11, amp[514]); 
  FFV2_0(w[148], w[121], w[3], pars->GC_100, amp[515]); 
  FFV1_0(w[148], w[102], w[4], pars->GC_11, amp[516]); 
  FFV1_0(w[110], w[121], w[0], pars->GC_11, amp[517]); 
  FFV1_0(w[133], w[102], w[0], pars->GC_11, amp[518]); 
  FFV2_0(w[146], w[144], w[2], pars->GC_100, amp[519]); 
  FFV1_0(w[146], w[109], w[4], pars->GC_11, amp[520]); 
  FFV2_0(w[122], w[150], w[2], pars->GC_100, amp[521]); 
  FFV1_0(w[107], w[150], w[4], pars->GC_11, amp[522]); 
  FFV1_0(w[122], w[109], w[0], pars->GC_11, amp[523]); 
  FFV1_0(w[107], w[144], w[0], pars->GC_11, amp[524]); 
  FFV1_0(w[88], w[151], w[8], pars->GC_1, amp[525]); 
  FFV1_0(w[88], w[152], w[8], pars->GC_1, amp[526]); 
  FFV1_0(w[88], w[153], w[8], pars->GC_1, amp[527]); 
  FFV1_0(w[154], w[87], w[8], pars->GC_1, amp[528]); 
  FFV1_0(w[155], w[87], w[8], pars->GC_1, amp[529]); 
  FFV1_0(w[156], w[87], w[8], pars->GC_1, amp[530]); 
  FFS4_0(w[88], w[151], w[14], pars->GC_83, amp[531]); 
  FFS4_0(w[88], w[152], w[14], pars->GC_83, amp[532]); 
  FFS4_0(w[88], w[153], w[14], pars->GC_83, amp[533]); 
  FFS4_0(w[154], w[87], w[14], pars->GC_83, amp[534]); 
  FFS4_0(w[155], w[87], w[14], pars->GC_83, amp[535]); 
  FFS4_0(w[156], w[87], w[14], pars->GC_83, amp[536]); 
  FFV2_3_0(w[88], w[151], w[17], pars->GC_50, pars->GC_58, amp[537]); 
  FFV2_3_0(w[88], w[152], w[17], pars->GC_50, pars->GC_58, amp[538]); 
  FFV2_3_0(w[88], w[153], w[17], pars->GC_50, pars->GC_58, amp[539]); 
  FFV2_3_0(w[154], w[87], w[17], pars->GC_50, pars->GC_58, amp[540]); 
  FFV2_3_0(w[155], w[87], w[17], pars->GC_50, pars->GC_58, amp[541]); 
  FFV2_3_0(w[156], w[87], w[17], pars->GC_50, pars->GC_58, amp[542]); 
  FFV2_0(w[154], w[101], w[3], pars->GC_100, amp[543]); 
  FFV2_0(w[155], w[101], w[3], pars->GC_100, amp[544]); 
  FFV2_0(w[156], w[101], w[3], pars->GC_100, amp[545]); 
  FFV1_0(w[104], w[101], w[78], pars->GC_11, amp[546]); 
  FFV1_0(w[104], w[101], w[79], pars->GC_11, amp[547]); 
  FFV1_0(w[104], w[101], w[80], pars->GC_11, amp[548]); 
  FFV2_0(w[104], w[151], w[2], pars->GC_100, amp[549]); 
  FFV2_0(w[104], w[152], w[2], pars->GC_100, amp[550]); 
  FFV2_0(w[104], w[153], w[2], pars->GC_100, amp[551]); 


}
double PY8MEs_R12_P21_sm_gb_wpwmggb::matrix_12_gb_wpwmggb() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 276;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = -amp[0] - amp[1] - Complex<double> (0, 1) * amp[2] - amp[4] -
      amp[5] - Complex<double> (0, 1) * amp[6] - amp[8] - amp[9] -
      Complex<double> (0, 1) * amp[10] - Complex<double> (0, 1) * amp[18] -
      Complex<double> (0, 1) * amp[19] - Complex<double> (0, 1) * amp[20] -
      Complex<double> (0, 1) * amp[21] - Complex<double> (0, 1) * amp[22] -
      Complex<double> (0, 1) * amp[23] - amp[24] - Complex<double> (0, 1) *
      amp[27] - amp[29] - Complex<double> (0, 1) * amp[30] - Complex<double>
      (0, 1) * amp[31] - amp[32] - Complex<double> (0, 1) * amp[33] -
      Complex<double> (0, 1) * amp[34] - Complex<double> (0, 1) * amp[38] -
      Complex<double> (0, 1) * amp[78] - Complex<double> (0, 1) * amp[79] -
      Complex<double> (0, 1) * amp[80] - Complex<double> (0, 1) * amp[81] -
      Complex<double> (0, 1) * amp[82] - Complex<double> (0, 1) * amp[83] -
      Complex<double> (0, 1) * amp[84] - Complex<double> (0, 1) * amp[85] -
      Complex<double> (0, 1) * amp[86] + amp[96] + amp[98] + amp[100] +
      amp[102] + amp[103] + amp[104] + amp[105] + amp[106] + amp[107] +
      amp[108] + amp[109] + amp[111] + amp[114] + amp[115] + amp[116] -
      amp[156] - amp[157] - Complex<double> (0, 1) * amp[159] - amp[160] -
      amp[161] - Complex<double> (0, 1) * amp[163] - amp[164] - amp[165] -
      Complex<double> (0, 1) * amp[167] - amp[168] - Complex<double> (0, 1) *
      amp[169] - Complex<double> (0, 1) * amp[170] - amp[171] - Complex<double>
      (0, 1) * amp[172] - amp[174] + amp[226] + amp[228] + amp[230] + amp[232]
      + amp[237] + amp[238] + amp[242] - amp[249] + amp[251] - amp[252] +
      amp[254] - amp[255] + amp[257] - amp[258] + amp[260] - amp[261] +
      amp[263] - amp[264] + amp[266] - amp[267] + amp[269] + amp[272] -
      amp[270] + amp[275] - amp[273];
  jamp[1] = -Complex<double> (0, 1) * amp[48] - Complex<double> (0, 1) *
      amp[49] - Complex<double> (0, 1) * amp[50] - Complex<double> (0, 1) *
      amp[51] - Complex<double> (0, 1) * amp[52] - Complex<double> (0, 1) *
      amp[53] - Complex<double> (0, 1) * amp[54] - Complex<double> (0, 1) *
      amp[55] - Complex<double> (0, 1) * amp[56] - amp[57] - amp[58] -
      Complex<double> (0, 1) * amp[59] - amp[61] - amp[62] - Complex<double>
      (0, 1) * amp[63] - amp[65] - amp[66] - Complex<double> (0, 1) * amp[67] -
      amp[69] - amp[72] - Complex<double> (0, 1) * amp[73] - amp[75] -
      Complex<double> (0, 1) * amp[76] - Complex<double> (0, 1) * amp[77] +
      Complex<double> (0, 1) * amp[78] + Complex<double> (0, 1) * amp[79] +
      Complex<double> (0, 1) * amp[80] + Complex<double> (0, 1) * amp[81] +
      Complex<double> (0, 1) * amp[82] + Complex<double> (0, 1) * amp[83] +
      Complex<double> (0, 1) * amp[84] + Complex<double> (0, 1) * amp[85] +
      Complex<double> (0, 1) * amp[86] + amp[87] + amp[88] + amp[89] + amp[90]
      + amp[91] + amp[92] + amp[93] + amp[94] + amp[95] + amp[97] + amp[99] +
      amp[101] + amp[110] + amp[112] + amp[113] + amp[156] + amp[157] +
      Complex<double> (0, 1) * amp[159] + amp[160] + amp[161] + Complex<double>
      (0, 1) * amp[163] + amp[164] + amp[165] + Complex<double> (0, 1) *
      amp[167] + amp[168] + Complex<double> (0, 1) * amp[169] + Complex<double>
      (0, 1) * amp[170] + amp[171] + Complex<double> (0, 1) * amp[172] +
      amp[174] + amp[199] + amp[201] + amp[203] + amp[212] + amp[213] +
      amp[215] + amp[231] + amp[249] + amp[250] + amp[252] + amp[253] +
      amp[255] + amp[256] + amp[258] + amp[259] + amp[261] + amp[262] +
      amp[264] + amp[265] + amp[267] + amp[268] + amp[270] + amp[271] +
      amp[273] + amp[274];
  jamp[2] = +amp[0] + amp[1] + Complex<double> (0, 1) * amp[2] + amp[4] +
      amp[5] + Complex<double> (0, 1) * amp[6] + amp[8] + amp[9] +
      Complex<double> (0, 1) * amp[10] + Complex<double> (0, 1) * amp[18] +
      Complex<double> (0, 1) * amp[19] + Complex<double> (0, 1) * amp[20] +
      Complex<double> (0, 1) * amp[21] + Complex<double> (0, 1) * amp[22] +
      Complex<double> (0, 1) * amp[23] + amp[24] + Complex<double> (0, 1) *
      amp[27] + amp[29] + Complex<double> (0, 1) * amp[30] + Complex<double>
      (0, 1) * amp[31] + amp[32] + Complex<double> (0, 1) * amp[33] +
      Complex<double> (0, 1) * amp[34] + Complex<double> (0, 1) * amp[38] -
      Complex<double> (0, 1) * amp[39] - Complex<double> (0, 1) * amp[40] -
      Complex<double> (0, 1) * amp[41] - Complex<double> (0, 1) * amp[42] -
      Complex<double> (0, 1) * amp[43] - Complex<double> (0, 1) * amp[44] -
      Complex<double> (0, 1) * amp[45] - Complex<double> (0, 1) * amp[46] -
      Complex<double> (0, 1) * amp[47] + amp[57] + amp[58] - Complex<double>
      (0, 1) * amp[60] + amp[61] + amp[62] - Complex<double> (0, 1) * amp[64] +
      amp[65] + amp[66] - Complex<double> (0, 1) * amp[68] + amp[69] -
      Complex<double> (0, 1) * amp[70] - Complex<double> (0, 1) * amp[71] +
      amp[72] - Complex<double> (0, 1) * amp[74] + amp[75] + amp[177] +
      amp[179] + amp[181] + amp[183] + amp[184] + amp[185] + amp[186] +
      amp[187] + amp[188] + amp[189] + amp[190] + amp[193] + amp[195] +
      amp[196] + amp[197] + amp[225] + amp[227] + amp[229] + amp[235] +
      amp[239] + amp[240] + amp[241] - amp[250] - amp[251] - amp[253] -
      amp[254] - amp[256] - amp[257] - amp[259] - amp[260] - amp[262] -
      amp[263] - amp[265] - amp[266] - amp[268] - amp[269] - amp[272] -
      amp[271] - amp[275] - amp[274];
  jamp[3] = +Complex<double> (0, 1) * amp[39] + Complex<double> (0, 1) *
      amp[40] + Complex<double> (0, 1) * amp[41] + Complex<double> (0, 1) *
      amp[42] + Complex<double> (0, 1) * amp[43] + Complex<double> (0, 1) *
      amp[44] + Complex<double> (0, 1) * amp[45] + Complex<double> (0, 1) *
      amp[46] + Complex<double> (0, 1) * amp[47] - amp[57] - amp[58] +
      Complex<double> (0, 1) * amp[60] - amp[61] - amp[62] + Complex<double>
      (0, 1) * amp[64] - amp[65] - amp[66] + Complex<double> (0, 1) * amp[68] -
      amp[69] + Complex<double> (0, 1) * amp[70] + Complex<double> (0, 1) *
      amp[71] - amp[72] + Complex<double> (0, 1) * amp[74] - amp[75] -
      Complex<double> (0, 1) * amp[117] - Complex<double> (0, 1) * amp[118] -
      Complex<double> (0, 1) * amp[119] - Complex<double> (0, 1) * amp[120] -
      Complex<double> (0, 1) * amp[121] - Complex<double> (0, 1) * amp[122] -
      Complex<double> (0, 1) * amp[123] - Complex<double> (0, 1) * amp[124] -
      Complex<double> (0, 1) * amp[125] + amp[126] + amp[127] + amp[128] +
      amp[129] + amp[130] + amp[131] + amp[132] + amp[133] + amp[134] +
      amp[136] + amp[138] + amp[140] + amp[149] + amp[151] + amp[152] +
      amp[156] + amp[157] - Complex<double> (0, 1) * amp[158] + amp[160] +
      amp[161] - Complex<double> (0, 1) * amp[162] + amp[164] + amp[165] -
      Complex<double> (0, 1) * amp[166] + amp[168] + amp[171] - Complex<double>
      (0, 1) * amp[173] + amp[174] - Complex<double> (0, 1) * amp[175] -
      Complex<double> (0, 1) * amp[176] + amp[178] + amp[180] + amp[182] +
      amp[191] + amp[192] + amp[194] + amp[233] + amp[249] + amp[250] +
      amp[252] + amp[253] + amp[255] + amp[256] + amp[258] + amp[259] +
      amp[261] + amp[262] + amp[264] + amp[265] + amp[267] + amp[268] +
      amp[270] + amp[271] + amp[273] + amp[274];
  jamp[4] = +amp[0] + amp[1] - Complex<double> (0, 1) * amp[3] + amp[4] +
      amp[5] - Complex<double> (0, 1) * amp[7] + amp[8] + amp[9] -
      Complex<double> (0, 1) * amp[11] - Complex<double> (0, 1) * amp[12] -
      Complex<double> (0, 1) * amp[13] - Complex<double> (0, 1) * amp[14] -
      Complex<double> (0, 1) * amp[15] - Complex<double> (0, 1) * amp[16] -
      Complex<double> (0, 1) * amp[17] + amp[24] - Complex<double> (0, 1) *
      amp[25] - Complex<double> (0, 1) * amp[26] - Complex<double> (0, 1) *
      amp[28] + amp[29] + amp[32] - Complex<double> (0, 1) * amp[35] -
      Complex<double> (0, 1) * amp[36] - Complex<double> (0, 1) * amp[37] +
      Complex<double> (0, 1) * amp[48] + Complex<double> (0, 1) * amp[49] +
      Complex<double> (0, 1) * amp[50] + Complex<double> (0, 1) * amp[51] +
      Complex<double> (0, 1) * amp[52] + Complex<double> (0, 1) * amp[53] +
      Complex<double> (0, 1) * amp[54] + Complex<double> (0, 1) * amp[55] +
      Complex<double> (0, 1) * amp[56] + amp[57] + amp[58] + Complex<double>
      (0, 1) * amp[59] + amp[61] + amp[62] + Complex<double> (0, 1) * amp[63] +
      amp[65] + amp[66] + Complex<double> (0, 1) * amp[67] + amp[69] + amp[72]
      + Complex<double> (0, 1) * amp[73] + amp[75] + Complex<double> (0, 1) *
      amp[76] + Complex<double> (0, 1) * amp[77] + amp[198] + amp[200] +
      amp[202] + amp[204] + amp[205] + amp[206] + amp[207] + amp[208] +
      amp[209] + amp[210] + amp[211] + amp[214] + amp[216] + amp[217] +
      amp[218] + amp[219] + amp[221] + amp[223] + amp[236] + amp[245] +
      amp[246] + amp[247] - amp[250] - amp[251] - amp[253] - amp[254] -
      amp[256] - amp[257] - amp[259] - amp[260] - amp[262] - amp[263] -
      amp[265] - amp[266] - amp[268] - amp[269] - amp[272] - amp[271] -
      amp[275] - amp[274];
  jamp[5] = -amp[0] - amp[1] + Complex<double> (0, 1) * amp[3] - amp[4] -
      amp[5] + Complex<double> (0, 1) * amp[7] - amp[8] - amp[9] +
      Complex<double> (0, 1) * amp[11] + Complex<double> (0, 1) * amp[12] +
      Complex<double> (0, 1) * amp[13] + Complex<double> (0, 1) * amp[14] +
      Complex<double> (0, 1) * amp[15] + Complex<double> (0, 1) * amp[16] +
      Complex<double> (0, 1) * amp[17] - amp[24] + Complex<double> (0, 1) *
      amp[25] + Complex<double> (0, 1) * amp[26] + Complex<double> (0, 1) *
      amp[28] - amp[29] - amp[32] + Complex<double> (0, 1) * amp[35] +
      Complex<double> (0, 1) * amp[36] + Complex<double> (0, 1) * amp[37] +
      Complex<double> (0, 1) * amp[117] + Complex<double> (0, 1) * amp[118] +
      Complex<double> (0, 1) * amp[119] + Complex<double> (0, 1) * amp[120] +
      Complex<double> (0, 1) * amp[121] + Complex<double> (0, 1) * amp[122] +
      Complex<double> (0, 1) * amp[123] + Complex<double> (0, 1) * amp[124] +
      Complex<double> (0, 1) * amp[125] + amp[135] + amp[137] + amp[139] +
      amp[141] + amp[142] + amp[143] + amp[144] + amp[145] + amp[146] +
      amp[147] + amp[148] + amp[150] + amp[153] + amp[154] + amp[155] -
      amp[156] - amp[157] + Complex<double> (0, 1) * amp[158] - amp[160] -
      amp[161] + Complex<double> (0, 1) * amp[162] - amp[164] - amp[165] +
      Complex<double> (0, 1) * amp[166] - amp[168] - amp[171] + Complex<double>
      (0, 1) * amp[173] - amp[174] + Complex<double> (0, 1) * amp[175] +
      Complex<double> (0, 1) * amp[176] + amp[220] + amp[222] + amp[224] +
      amp[234] + amp[243] + amp[244] + amp[248] - amp[249] + amp[251] -
      amp[252] + amp[254] - amp[255] + amp[257] - amp[258] + amp[260] -
      amp[261] + amp[263] - amp[264] + amp[266] - amp[267] + amp[269] +
      amp[272] - amp[270] + amp[275] - amp[273];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[0][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P21_sm_gb_wpwmggb::matrix_12_gbx_wpwmggbx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 276;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = +amp[276] + amp[277] + Complex<double> (0, 1) * amp[278] + amp[280]
      + amp[281] + Complex<double> (0, 1) * amp[282] + amp[284] + amp[285] +
      Complex<double> (0, 1) * amp[286] + Complex<double> (0, 1) * amp[294] +
      Complex<double> (0, 1) * amp[295] + Complex<double> (0, 1) * amp[296] +
      Complex<double> (0, 1) * amp[297] + Complex<double> (0, 1) * amp[298] +
      Complex<double> (0, 1) * amp[299] + amp[300] + Complex<double> (0, 1) *
      amp[303] + amp[305] + Complex<double> (0, 1) * amp[306] + Complex<double>
      (0, 1) * amp[307] + amp[308] + Complex<double> (0, 1) * amp[309] +
      Complex<double> (0, 1) * amp[310] + Complex<double> (0, 1) * amp[314] +
      Complex<double> (0, 1) * amp[354] + Complex<double> (0, 1) * amp[355] +
      Complex<double> (0, 1) * amp[356] + Complex<double> (0, 1) * amp[357] +
      Complex<double> (0, 1) * amp[358] + Complex<double> (0, 1) * amp[359] +
      Complex<double> (0, 1) * amp[360] + Complex<double> (0, 1) * amp[361] +
      Complex<double> (0, 1) * amp[362] - amp[372] - amp[374] - amp[376] -
      amp[378] - amp[379] - amp[380] - amp[381] - amp[382] - amp[383] -
      amp[384] - amp[385] - amp[387] - amp[390] - amp[391] - amp[392] +
      amp[432] + amp[433] + Complex<double> (0, 1) * amp[435] + amp[436] +
      amp[437] + Complex<double> (0, 1) * amp[439] + amp[440] + amp[441] +
      Complex<double> (0, 1) * amp[443] + amp[444] + Complex<double> (0, 1) *
      amp[445] + Complex<double> (0, 1) * amp[446] + amp[447] + Complex<double>
      (0, 1) * amp[448] + amp[450] - amp[502] - amp[504] - amp[506] - amp[508]
      - amp[513] - amp[514] - amp[518] + amp[525] - amp[527] + amp[528] -
      amp[530] + amp[531] - amp[533] + amp[534] - amp[536] + amp[537] -
      amp[539] + amp[540] - amp[542] + amp[543] - amp[545] - amp[548] +
      amp[546] - amp[551] + amp[549];
  jamp[1] = +Complex<double> (0, 1) * amp[324] + Complex<double> (0, 1) *
      amp[325] + Complex<double> (0, 1) * amp[326] + Complex<double> (0, 1) *
      amp[327] + Complex<double> (0, 1) * amp[328] + Complex<double> (0, 1) *
      amp[329] + Complex<double> (0, 1) * amp[330] + Complex<double> (0, 1) *
      amp[331] + Complex<double> (0, 1) * amp[332] + amp[333] + amp[334] +
      Complex<double> (0, 1) * amp[335] + amp[337] + amp[338] + Complex<double>
      (0, 1) * amp[339] + amp[341] + amp[342] + Complex<double> (0, 1) *
      amp[343] + amp[345] + amp[348] + Complex<double> (0, 1) * amp[349] +
      amp[351] + Complex<double> (0, 1) * amp[352] + Complex<double> (0, 1) *
      amp[353] - Complex<double> (0, 1) * amp[354] - Complex<double> (0, 1) *
      amp[355] - Complex<double> (0, 1) * amp[356] - Complex<double> (0, 1) *
      amp[357] - Complex<double> (0, 1) * amp[358] - Complex<double> (0, 1) *
      amp[359] - Complex<double> (0, 1) * amp[360] - Complex<double> (0, 1) *
      amp[361] - Complex<double> (0, 1) * amp[362] - amp[363] - amp[364] -
      amp[365] - amp[366] - amp[367] - amp[368] - amp[369] - amp[370] -
      amp[371] - amp[373] - amp[375] - amp[377] - amp[386] - amp[388] -
      amp[389] - amp[432] - amp[433] - Complex<double> (0, 1) * amp[435] -
      amp[436] - amp[437] - Complex<double> (0, 1) * amp[439] - amp[440] -
      amp[441] - Complex<double> (0, 1) * amp[443] - amp[444] - Complex<double>
      (0, 1) * amp[445] - Complex<double> (0, 1) * amp[446] - amp[447] -
      Complex<double> (0, 1) * amp[448] - amp[450] - amp[475] - amp[477] -
      amp[479] - amp[488] - amp[489] - amp[491] - amp[507] - amp[525] -
      amp[526] - amp[528] - amp[529] - amp[531] - amp[532] - amp[534] -
      amp[535] - amp[537] - amp[538] - amp[540] - amp[541] - amp[543] -
      amp[544] - amp[546] - amp[547] - amp[549] - amp[550];
  jamp[2] = -amp[276] - amp[277] - Complex<double> (0, 1) * amp[278] - amp[280]
      - amp[281] - Complex<double> (0, 1) * amp[282] - amp[284] - amp[285] -
      Complex<double> (0, 1) * amp[286] - Complex<double> (0, 1) * amp[294] -
      Complex<double> (0, 1) * amp[295] - Complex<double> (0, 1) * amp[296] -
      Complex<double> (0, 1) * amp[297] - Complex<double> (0, 1) * amp[298] -
      Complex<double> (0, 1) * amp[299] - amp[300] - Complex<double> (0, 1) *
      amp[303] - amp[305] - Complex<double> (0, 1) * amp[306] - Complex<double>
      (0, 1) * amp[307] - amp[308] - Complex<double> (0, 1) * amp[309] -
      Complex<double> (0, 1) * amp[310] - Complex<double> (0, 1) * amp[314] +
      Complex<double> (0, 1) * amp[315] + Complex<double> (0, 1) * amp[316] +
      Complex<double> (0, 1) * amp[317] + Complex<double> (0, 1) * amp[318] +
      Complex<double> (0, 1) * amp[319] + Complex<double> (0, 1) * amp[320] +
      Complex<double> (0, 1) * amp[321] + Complex<double> (0, 1) * amp[322] +
      Complex<double> (0, 1) * amp[323] - amp[333] - amp[334] + Complex<double>
      (0, 1) * amp[336] - amp[337] - amp[338] + Complex<double> (0, 1) *
      amp[340] - amp[341] - amp[342] + Complex<double> (0, 1) * amp[344] -
      amp[345] + Complex<double> (0, 1) * amp[346] + Complex<double> (0, 1) *
      amp[347] - amp[348] + Complex<double> (0, 1) * amp[350] - amp[351] -
      amp[453] - amp[455] - amp[457] - amp[459] - amp[460] - amp[461] -
      amp[462] - amp[463] - amp[464] - amp[465] - amp[466] - amp[469] -
      amp[471] - amp[472] - amp[473] - amp[501] - amp[503] - amp[505] -
      amp[511] - amp[515] - amp[516] - amp[517] + amp[526] + amp[527] +
      amp[529] + amp[530] + amp[532] + amp[533] + amp[535] + amp[536] +
      amp[538] + amp[539] + amp[541] + amp[542] + amp[544] + amp[545] +
      amp[548] + amp[547] + amp[551] + amp[550];
  jamp[3] = -Complex<double> (0, 1) * amp[315] - Complex<double> (0, 1) *
      amp[316] - Complex<double> (0, 1) * amp[317] - Complex<double> (0, 1) *
      amp[318] - Complex<double> (0, 1) * amp[319] - Complex<double> (0, 1) *
      amp[320] - Complex<double> (0, 1) * amp[321] - Complex<double> (0, 1) *
      amp[322] - Complex<double> (0, 1) * amp[323] + amp[333] + amp[334] -
      Complex<double> (0, 1) * amp[336] + amp[337] + amp[338] - Complex<double>
      (0, 1) * amp[340] + amp[341] + amp[342] - Complex<double> (0, 1) *
      amp[344] + amp[345] - Complex<double> (0, 1) * amp[346] - Complex<double>
      (0, 1) * amp[347] + amp[348] - Complex<double> (0, 1) * amp[350] +
      amp[351] + Complex<double> (0, 1) * amp[393] + Complex<double> (0, 1) *
      amp[394] + Complex<double> (0, 1) * amp[395] + Complex<double> (0, 1) *
      amp[396] + Complex<double> (0, 1) * amp[397] + Complex<double> (0, 1) *
      amp[398] + Complex<double> (0, 1) * amp[399] + Complex<double> (0, 1) *
      amp[400] + Complex<double> (0, 1) * amp[401] - amp[402] - amp[403] -
      amp[404] - amp[405] - amp[406] - amp[407] - amp[408] - amp[409] -
      amp[410] - amp[412] - amp[414] - amp[416] - amp[425] - amp[427] -
      amp[428] - amp[432] - amp[433] + Complex<double> (0, 1) * amp[434] -
      amp[436] - amp[437] + Complex<double> (0, 1) * amp[438] - amp[440] -
      amp[441] + Complex<double> (0, 1) * amp[442] - amp[444] - amp[447] +
      Complex<double> (0, 1) * amp[449] - amp[450] + Complex<double> (0, 1) *
      amp[451] + Complex<double> (0, 1) * amp[452] - amp[454] - amp[456] -
      amp[458] - amp[467] - amp[468] - amp[470] - amp[509] - amp[525] -
      amp[526] - amp[528] - amp[529] - amp[531] - amp[532] - amp[534] -
      amp[535] - amp[537] - amp[538] - amp[540] - amp[541] - amp[543] -
      amp[544] - amp[546] - amp[547] - amp[549] - amp[550];
  jamp[4] = -amp[276] - amp[277] + Complex<double> (0, 1) * amp[279] - amp[280]
      - amp[281] + Complex<double> (0, 1) * amp[283] - amp[284] - amp[285] +
      Complex<double> (0, 1) * amp[287] + Complex<double> (0, 1) * amp[288] +
      Complex<double> (0, 1) * amp[289] + Complex<double> (0, 1) * amp[290] +
      Complex<double> (0, 1) * amp[291] + Complex<double> (0, 1) * amp[292] +
      Complex<double> (0, 1) * amp[293] - amp[300] + Complex<double> (0, 1) *
      amp[301] + Complex<double> (0, 1) * amp[302] + Complex<double> (0, 1) *
      amp[304] - amp[305] - amp[308] + Complex<double> (0, 1) * amp[311] +
      Complex<double> (0, 1) * amp[312] + Complex<double> (0, 1) * amp[313] -
      Complex<double> (0, 1) * amp[324] - Complex<double> (0, 1) * amp[325] -
      Complex<double> (0, 1) * amp[326] - Complex<double> (0, 1) * amp[327] -
      Complex<double> (0, 1) * amp[328] - Complex<double> (0, 1) * amp[329] -
      Complex<double> (0, 1) * amp[330] - Complex<double> (0, 1) * amp[331] -
      Complex<double> (0, 1) * amp[332] - amp[333] - amp[334] - Complex<double>
      (0, 1) * amp[335] - amp[337] - amp[338] - Complex<double> (0, 1) *
      amp[339] - amp[341] - amp[342] - Complex<double> (0, 1) * amp[343] -
      amp[345] - amp[348] - Complex<double> (0, 1) * amp[349] - amp[351] -
      Complex<double> (0, 1) * amp[352] - Complex<double> (0, 1) * amp[353] -
      amp[474] - amp[476] - amp[478] - amp[480] - amp[481] - amp[482] -
      amp[483] - amp[484] - amp[485] - amp[486] - amp[487] - amp[490] -
      amp[492] - amp[493] - amp[494] - amp[495] - amp[497] - amp[499] -
      amp[512] - amp[521] - amp[522] - amp[523] + amp[526] + amp[527] +
      amp[529] + amp[530] + amp[532] + amp[533] + amp[535] + amp[536] +
      amp[538] + amp[539] + amp[541] + amp[542] + amp[544] + amp[545] +
      amp[548] + amp[547] + amp[551] + amp[550];
  jamp[5] = +amp[276] + amp[277] - Complex<double> (0, 1) * amp[279] + amp[280]
      + amp[281] - Complex<double> (0, 1) * amp[283] + amp[284] + amp[285] -
      Complex<double> (0, 1) * amp[287] - Complex<double> (0, 1) * amp[288] -
      Complex<double> (0, 1) * amp[289] - Complex<double> (0, 1) * amp[290] -
      Complex<double> (0, 1) * amp[291] - Complex<double> (0, 1) * amp[292] -
      Complex<double> (0, 1) * amp[293] + amp[300] - Complex<double> (0, 1) *
      amp[301] - Complex<double> (0, 1) * amp[302] - Complex<double> (0, 1) *
      amp[304] + amp[305] + amp[308] - Complex<double> (0, 1) * amp[311] -
      Complex<double> (0, 1) * amp[312] - Complex<double> (0, 1) * amp[313] -
      Complex<double> (0, 1) * amp[393] - Complex<double> (0, 1) * amp[394] -
      Complex<double> (0, 1) * amp[395] - Complex<double> (0, 1) * amp[396] -
      Complex<double> (0, 1) * amp[397] - Complex<double> (0, 1) * amp[398] -
      Complex<double> (0, 1) * amp[399] - Complex<double> (0, 1) * amp[400] -
      Complex<double> (0, 1) * amp[401] - amp[411] - amp[413] - amp[415] -
      amp[417] - amp[418] - amp[419] - amp[420] - amp[421] - amp[422] -
      amp[423] - amp[424] - amp[426] - amp[429] - amp[430] - amp[431] +
      amp[432] + amp[433] - Complex<double> (0, 1) * amp[434] + amp[436] +
      amp[437] - Complex<double> (0, 1) * amp[438] + amp[440] + amp[441] -
      Complex<double> (0, 1) * amp[442] + amp[444] + amp[447] - Complex<double>
      (0, 1) * amp[449] + amp[450] - Complex<double> (0, 1) * amp[451] -
      Complex<double> (0, 1) * amp[452] - amp[496] - amp[498] - amp[500] -
      amp[510] - amp[519] - amp[520] - amp[524] + amp[525] - amp[527] +
      amp[528] - amp[530] + amp[531] - amp[533] + amp[534] - amp[536] +
      amp[537] - amp[539] + amp[540] - amp[542] + amp[543] - amp[545] -
      amp[548] + amp[546] - amp[551] + amp[549];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[1][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}


}  // end namespace PY8MEs_namespace

