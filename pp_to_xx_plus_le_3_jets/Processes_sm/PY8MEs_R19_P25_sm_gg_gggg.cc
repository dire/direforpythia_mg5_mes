//==========================================================================
// This file has been automatically generated for Pythia 8 by
// MadGraph5_aMC@NLO v. 2.6.0, 2017-08-16
// By the MadGraph5_aMC@NLO Development Team
// Visit launchpad.net/madgraph5 and amcatnlo.web.cern.ch
//==========================================================================

#include "PY8MEs_R19_P25_sm_gg_gggg.h"
#include "HelAmps_sm.h"

using namespace Pythia8_sm; 

namespace PY8MEs_namespace 
{
//==========================================================================
// Class member functions for calculating the matrix elements for
// Process: g g > g g g g WEIGHTED<=4 @19

// Exception class
class PY8MEs_R19_P25_sm_gg_ggggException : public exception
{
  virtual const char * what() const throw()
  {
    return "Exception in class 'PY8MEs_R19_P25_sm_gg_gggg'."; 
  }
}
PY8MEs_R19_P25_sm_gg_gggg_exception; 

std::set<int> PY8MEs_R19_P25_sm_gg_gggg::s_channel_proc = std::set<int>
    (createset<int> ());

int PY8MEs_R19_P25_sm_gg_gggg::helicities[ncomb][nexternal] = {{-1, -1, -1, -1,
    -1, -1}, {-1, -1, -1, -1, -1, 1}, {-1, -1, -1, -1, 1, -1}, {-1, -1, -1, -1,
    1, 1}, {-1, -1, -1, 1, -1, -1}, {-1, -1, -1, 1, -1, 1}, {-1, -1, -1, 1, 1,
    -1}, {-1, -1, -1, 1, 1, 1}, {-1, -1, 1, -1, -1, -1}, {-1, -1, 1, -1, -1,
    1}, {-1, -1, 1, -1, 1, -1}, {-1, -1, 1, -1, 1, 1}, {-1, -1, 1, 1, -1, -1},
    {-1, -1, 1, 1, -1, 1}, {-1, -1, 1, 1, 1, -1}, {-1, -1, 1, 1, 1, 1}, {-1, 1,
    -1, -1, -1, -1}, {-1, 1, -1, -1, -1, 1}, {-1, 1, -1, -1, 1, -1}, {-1, 1,
    -1, -1, 1, 1}, {-1, 1, -1, 1, -1, -1}, {-1, 1, -1, 1, -1, 1}, {-1, 1, -1,
    1, 1, -1}, {-1, 1, -1, 1, 1, 1}, {-1, 1, 1, -1, -1, -1}, {-1, 1, 1, -1, -1,
    1}, {-1, 1, 1, -1, 1, -1}, {-1, 1, 1, -1, 1, 1}, {-1, 1, 1, 1, -1, -1},
    {-1, 1, 1, 1, -1, 1}, {-1, 1, 1, 1, 1, -1}, {-1, 1, 1, 1, 1, 1}, {1, -1,
    -1, -1, -1, -1}, {1, -1, -1, -1, -1, 1}, {1, -1, -1, -1, 1, -1}, {1, -1,
    -1, -1, 1, 1}, {1, -1, -1, 1, -1, -1}, {1, -1, -1, 1, -1, 1}, {1, -1, -1,
    1, 1, -1}, {1, -1, -1, 1, 1, 1}, {1, -1, 1, -1, -1, -1}, {1, -1, 1, -1, -1,
    1}, {1, -1, 1, -1, 1, -1}, {1, -1, 1, -1, 1, 1}, {1, -1, 1, 1, -1, -1}, {1,
    -1, 1, 1, -1, 1}, {1, -1, 1, 1, 1, -1}, {1, -1, 1, 1, 1, 1}, {1, 1, -1, -1,
    -1, -1}, {1, 1, -1, -1, -1, 1}, {1, 1, -1, -1, 1, -1}, {1, 1, -1, -1, 1,
    1}, {1, 1, -1, 1, -1, -1}, {1, 1, -1, 1, -1, 1}, {1, 1, -1, 1, 1, -1}, {1,
    1, -1, 1, 1, 1}, {1, 1, 1, -1, -1, -1}, {1, 1, 1, -1, -1, 1}, {1, 1, 1, -1,
    1, -1}, {1, 1, 1, -1, 1, 1}, {1, 1, 1, 1, -1, -1}, {1, 1, 1, 1, -1, 1}, {1,
    1, 1, 1, 1, -1}, {1, 1, 1, 1, 1, 1}};

// Normalization factors the various processes
// Denominators: spins, colors and identical particles
int PY8MEs_R19_P25_sm_gg_gggg::denom_colors[nprocesses] = {64}; 
int PY8MEs_R19_P25_sm_gg_gggg::denom_hels[nprocesses] = {4}; 
int PY8MEs_R19_P25_sm_gg_gggg::denom_iden[nprocesses] = {24}; 

//--------------------------------------------------------------------------
// Color config initialization
void PY8MEs_R19_P25_sm_gg_gggg::initColorConfigs() 
{
  color_configs = vector < vec_vec_int > (); 
  jamp_nc_relative_power = vector < vec_int > (); 

  // Color flows of process Process: g g > g g g g WEIGHTED<=4 @19
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[0].push_back(vec_int(createvector<int>
      (6)(1)(1)(2)(3)(2)(4)(3)(5)(4)(6)(5)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #1
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(1)(1)(2)(3)(2)(4)(3)(5)(6)(6)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #2
  color_configs[0].push_back(vec_int(createvector<int>
      (6)(1)(1)(2)(3)(2)(4)(5)(5)(3)(6)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #3
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(1)(1)(2)(3)(2)(4)(6)(5)(3)(6)(5)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #4
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(1)(1)(2)(3)(2)(4)(6)(5)(4)(6)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #5
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(1)(1)(2)(3)(2)(4)(5)(5)(6)(6)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #6
  color_configs[0].push_back(vec_int(createvector<int>
      (6)(1)(1)(2)(3)(4)(4)(2)(5)(3)(6)(5)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #7
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(1)(1)(2)(3)(4)(4)(2)(5)(6)(6)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #8
  color_configs[0].push_back(vec_int(createvector<int>
      (6)(1)(1)(2)(3)(5)(4)(2)(5)(4)(6)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #9
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(1)(1)(2)(3)(6)(4)(2)(5)(4)(6)(5)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #10
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(1)(1)(2)(3)(6)(4)(2)(5)(3)(6)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #11
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(1)(1)(2)(3)(5)(4)(2)(5)(6)(6)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #12
  color_configs[0].push_back(vec_int(createvector<int>
      (6)(1)(1)(2)(3)(5)(4)(3)(5)(2)(6)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #13
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(1)(1)(2)(3)(5)(4)(6)(5)(2)(6)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #14
  color_configs[0].push_back(vec_int(createvector<int>
      (6)(1)(1)(2)(3)(4)(4)(5)(5)(2)(6)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #15
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(1)(1)(2)(3)(6)(4)(5)(5)(2)(6)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #16
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(1)(1)(2)(3)(6)(4)(3)(5)(2)(6)(5)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #17
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(1)(1)(2)(3)(4)(4)(6)(5)(2)(6)(5)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #18
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(1)(1)(2)(3)(6)(4)(3)(5)(4)(6)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #19
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(1)(1)(2)(3)(6)(4)(5)(5)(3)(6)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #20
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(1)(1)(2)(3)(4)(4)(6)(5)(3)(6)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #21
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(1)(1)(2)(3)(5)(4)(6)(5)(4)(6)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #22
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(1)(1)(2)(3)(5)(4)(3)(5)(6)(6)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #23
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(1)(1)(2)(3)(4)(4)(5)(5)(6)(6)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #24
  color_configs[0].push_back(vec_int(createvector<int>
      (6)(1)(3)(2)(3)(1)(4)(2)(5)(4)(6)(5)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #25
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(1)(3)(2)(3)(1)(4)(2)(5)(6)(6)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #26
  color_configs[0].push_back(vec_int(createvector<int>
      (6)(1)(3)(2)(3)(1)(4)(5)(5)(2)(6)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #27
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(1)(3)(2)(3)(1)(4)(6)(5)(2)(6)(5)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #28
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(1)(3)(2)(3)(1)(4)(6)(5)(4)(6)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #29
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(1)(3)(2)(3)(1)(4)(5)(5)(6)(6)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #30
  color_configs[0].push_back(vec_int(createvector<int>
      (6)(1)(4)(2)(3)(1)(4)(3)(5)(2)(6)(5)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #31
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(1)(4)(2)(3)(1)(4)(3)(5)(6)(6)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #32
  color_configs[0].push_back(vec_int(createvector<int>
      (6)(1)(5)(2)(3)(1)(4)(3)(5)(4)(6)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #33
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(1)(6)(2)(3)(1)(4)(3)(5)(4)(6)(5)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #34
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(1)(6)(2)(3)(1)(4)(3)(5)(2)(6)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #35
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(1)(5)(2)(3)(1)(4)(3)(5)(6)(6)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #36
  color_configs[0].push_back(vec_int(createvector<int>
      (6)(1)(5)(2)(3)(1)(4)(2)(5)(3)(6)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #37
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(1)(5)(2)(3)(1)(4)(6)(5)(3)(6)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #38
  color_configs[0].push_back(vec_int(createvector<int>
      (6)(1)(4)(2)(3)(1)(4)(5)(5)(3)(6)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #39
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(1)(6)(2)(3)(1)(4)(5)(5)(3)(6)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #40
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(1)(6)(2)(3)(1)(4)(2)(5)(3)(6)(5)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #41
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(1)(4)(2)(3)(1)(4)(6)(5)(3)(6)(5)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #42
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(1)(6)(2)(3)(1)(4)(2)(5)(4)(6)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #43
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(1)(6)(2)(3)(1)(4)(5)(5)(2)(6)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #44
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(1)(4)(2)(3)(1)(4)(6)(5)(2)(6)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #45
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(1)(5)(2)(3)(1)(4)(6)(5)(4)(6)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #46
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(1)(5)(2)(3)(1)(4)(2)(5)(6)(6)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #47
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(1)(4)(2)(3)(1)(4)(5)(5)(6)(6)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #48
  color_configs[0].push_back(vec_int(createvector<int>
      (6)(1)(4)(2)(3)(2)(4)(1)(5)(3)(6)(5)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #49
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(1)(4)(2)(3)(2)(4)(1)(5)(6)(6)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #50
  color_configs[0].push_back(vec_int(createvector<int>
      (6)(1)(4)(2)(3)(5)(4)(1)(5)(2)(6)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #51
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(1)(4)(2)(3)(6)(4)(1)(5)(2)(6)(5)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #52
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(1)(4)(2)(3)(6)(4)(1)(5)(3)(6)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #53
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(1)(4)(2)(3)(5)(4)(1)(5)(6)(6)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #54
  color_configs[0].push_back(vec_int(createvector<int>
      (6)(1)(3)(2)(3)(4)(4)(1)(5)(2)(6)(5)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #55
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(1)(3)(2)(3)(4)(4)(1)(5)(6)(6)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #56
  color_configs[0].push_back(vec_int(createvector<int>
      (6)(1)(5)(2)(3)(4)(4)(1)(5)(3)(6)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #57
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(1)(6)(2)(3)(4)(4)(1)(5)(3)(6)(5)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #58
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(1)(6)(2)(3)(4)(4)(1)(5)(2)(6)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #59
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(1)(5)(2)(3)(4)(4)(1)(5)(6)(6)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #60
  color_configs[0].push_back(vec_int(createvector<int>
      (6)(1)(5)(2)(3)(2)(4)(1)(5)(4)(6)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #61
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(1)(5)(2)(3)(6)(4)(1)(5)(4)(6)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #62
  color_configs[0].push_back(vec_int(createvector<int>
      (6)(1)(3)(2)(3)(5)(4)(1)(5)(4)(6)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #63
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(1)(6)(2)(3)(5)(4)(1)(5)(4)(6)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #64
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(1)(6)(2)(3)(2)(4)(1)(5)(4)(6)(5)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #65
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(1)(3)(2)(3)(6)(4)(1)(5)(4)(6)(5)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #66
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(1)(6)(2)(3)(2)(4)(1)(5)(3)(6)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #67
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(1)(6)(2)(3)(5)(4)(1)(5)(2)(6)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #68
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(1)(3)(2)(3)(6)(4)(1)(5)(2)(6)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #69
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(1)(5)(2)(3)(6)(4)(1)(5)(3)(6)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #70
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(1)(5)(2)(3)(2)(4)(1)(5)(6)(6)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #71
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(1)(3)(2)(3)(5)(4)(1)(5)(6)(6)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #72
  color_configs[0].push_back(vec_int(createvector<int>
      (6)(1)(5)(2)(3)(2)(4)(3)(5)(1)(6)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #73
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(1)(5)(2)(3)(2)(4)(6)(5)(1)(6)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #74
  color_configs[0].push_back(vec_int(createvector<int>
      (6)(1)(5)(2)(3)(4)(4)(2)(5)(1)(6)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #75
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(1)(5)(2)(3)(6)(4)(2)(5)(1)(6)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #76
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(1)(5)(2)(3)(6)(4)(3)(5)(1)(6)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #77
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(1)(5)(2)(3)(4)(4)(6)(5)(1)(6)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #78
  color_configs[0].push_back(vec_int(createvector<int>
      (6)(1)(3)(2)(3)(5)(4)(2)(5)(1)(6)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #79
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(1)(3)(2)(3)(5)(4)(6)(5)(1)(6)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #80
  color_configs[0].push_back(vec_int(createvector<int>
      (6)(1)(4)(2)(3)(5)(4)(3)(5)(1)(6)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #81
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(1)(6)(2)(3)(5)(4)(3)(5)(1)(6)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #82
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(1)(6)(2)(3)(5)(4)(2)(5)(1)(6)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #83
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(1)(4)(2)(3)(5)(4)(6)(5)(1)(6)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #84
  color_configs[0].push_back(vec_int(createvector<int>
      (6)(1)(4)(2)(3)(2)(4)(5)(5)(1)(6)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #85
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(1)(4)(2)(3)(6)(4)(5)(5)(1)(6)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #86
  color_configs[0].push_back(vec_int(createvector<int>
      (6)(1)(3)(2)(3)(4)(4)(5)(5)(1)(6)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #87
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(1)(6)(2)(3)(4)(4)(5)(5)(1)(6)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #88
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(1)(6)(2)(3)(2)(4)(5)(5)(1)(6)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #89
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(1)(3)(2)(3)(6)(4)(5)(5)(1)(6)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #90
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(1)(6)(2)(3)(2)(4)(3)(5)(1)(6)(5)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #91
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(1)(6)(2)(3)(4)(4)(2)(5)(1)(6)(5)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #92
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(1)(3)(2)(3)(6)(4)(2)(5)(1)(6)(5)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #93
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(1)(4)(2)(3)(6)(4)(3)(5)(1)(6)(5)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #94
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(1)(4)(2)(3)(2)(4)(6)(5)(1)(6)(5)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #95
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(1)(3)(2)(3)(4)(4)(6)(5)(1)(6)(5)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #96
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(1)(6)(2)(3)(2)(4)(3)(5)(4)(6)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #97
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(1)(6)(2)(3)(2)(4)(5)(5)(3)(6)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #98
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(1)(6)(2)(3)(4)(4)(2)(5)(3)(6)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #99
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(1)(6)(2)(3)(5)(4)(2)(5)(4)(6)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #100
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(1)(6)(2)(3)(5)(4)(3)(5)(2)(6)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #101
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(1)(6)(2)(3)(4)(4)(5)(5)(2)(6)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #102
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(1)(3)(2)(3)(6)(4)(2)(5)(4)(6)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #103
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(1)(3)(2)(3)(6)(4)(5)(5)(2)(6)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #104
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(1)(4)(2)(3)(6)(4)(3)(5)(2)(6)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #105
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(1)(5)(2)(3)(6)(4)(3)(5)(4)(6)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #106
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(1)(5)(2)(3)(6)(4)(2)(5)(3)(6)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #107
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(1)(4)(2)(3)(6)(4)(5)(5)(3)(6)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #108
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(1)(4)(2)(3)(2)(4)(6)(5)(3)(6)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #109
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(1)(4)(2)(3)(5)(4)(6)(5)(2)(6)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #110
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(1)(3)(2)(3)(4)(4)(6)(5)(2)(6)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #111
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(1)(5)(2)(3)(4)(4)(6)(5)(3)(6)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #112
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(1)(5)(2)(3)(2)(4)(6)(5)(4)(6)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #113
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(1)(3)(2)(3)(5)(4)(6)(5)(4)(6)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #114
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(1)(5)(2)(3)(2)(4)(3)(5)(6)(6)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #115
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(1)(5)(2)(3)(4)(4)(2)(5)(6)(6)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #116
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(1)(3)(2)(3)(5)(4)(2)(5)(6)(6)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #117
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(1)(4)(2)(3)(5)(4)(3)(5)(6)(6)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #118
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(1)(4)(2)(3)(2)(4)(5)(5)(6)(6)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #119
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(1)(3)(2)(3)(4)(4)(5)(5)(6)(6)(1)));
  jamp_nc_relative_power[0].push_back(0); 
}

//--------------------------------------------------------------------------
// Destructor.
PY8MEs_R19_P25_sm_gg_gggg::~PY8MEs_R19_P25_sm_gg_gggg() 
{
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    delete[] p[i]; 
    p[i] = NULL; 
  }
}

//--------------------------------------------------------------------------
// Invert the permutation mapping
vector<int> PY8MEs_R19_P25_sm_gg_gggg::invert_mapping(vector<int> mapping) 
{
  vector<int> inverted_mapping; 
  for (unsigned int i = 0; i < mapping.size(); i++ )
  {
    for (unsigned int j = 0; j < mapping.size(); j++ )
    {
      if (mapping[j] == ((int)i))
      {
        inverted_mapping.push_back(j); 
        break; 
      }
    }
  }
  return inverted_mapping; 
}

//--------------------------------------------------------------------------
// Return the list of possible helicity configurations
vector < vec_int > PY8MEs_R19_P25_sm_gg_gggg::getHelicityConfigs(vector<int>
    permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(ncomb, vector<int> (nexternal, 0)); 
  for (unsigned int ihel = 0; ihel < ncomb; ihel++ )
  {
    for(unsigned int j = 0; j < nexternal; j++ )
    {
      res[ihel][chosenPerm[j]] = helicities[ihel][j]; 
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Return the list of possible color configurations
vector < vec_int > PY8MEs_R19_P25_sm_gg_gggg::getColorConfigs(int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(color_configs[chosenProcID].size(), vector<int>
      (nexternal * 2, 0));
  for (unsigned int icol = 0; icol < color_configs[chosenProcID].size(); icol++
      )
  {
    for(unsigned int j = 0; j < (2 * nexternal); j++ )
    {
      res[icol][chosenPerm[j/2] * 2 + j%2] =
          color_configs[chosenProcID][icol][j];
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Get JAMP relative N_c power
int PY8MEs_R19_P25_sm_gg_gggg::getColorFlowRelativeNCPower(int color_flow_ID,
    int specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return jamp_nc_relative_power[chosenProcID][color_flow_ID]; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity ID -> Helicity Config
vector<int> PY8MEs_R19_P25_sm_gg_gggg::getHelicityConfigForID(int hel_ID,
    vector<int> permutation)
{
  if (hel_ID < 0 || hel_ID >= ncomb)
  {
    cerr <<  "Error in function 'getHelicityConfigForID' of class" << 
    " 'PY8MEs_R19_P25_sm_gg_gggg': Specified helicity ID '" << 
    hel_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R19_P25_sm_gg_gggg_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(nexternal, 0); 
  for (unsigned int j = 0; j < nexternal; j++ )
  {
    res[chosenPerm[j]] = helicities[hel_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity Config -> Helicity ID
int PY8MEs_R19_P25_sm_gg_gggg::getHelicityIDForConfig(vector<int> hel_config,
    vector<int> permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  int user_ihel = -1; 
  if (hel_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < ncomb; i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < nexternal; j++ )
      {
        if (helicities[i][chosenPerm[j]] != hel_config[j])
        {
          found = false; 
          break; 
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_ihel = i; 
        break; 
      }
    }
    if (user_ihel == -1)
    {
      cerr <<  "Error in function 'getHelicityIDForConfig' of class" << 
      " 'PY8MEs_R19_P25_sm_gg_gggg': Specified helicity" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R19_P25_sm_gg_gggg_exception; 
    }
  }
  return user_ihel; 
}


//--------------------------------------------------------------------------
// Implements the map Color ID -> Color Config
vector<int> PY8MEs_R19_P25_sm_gg_gggg::getColorConfigForID(int color_ID, int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < 0 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getColorConfigForID' of class" << 
    " 'PY8MEs_R19_P25_sm_gg_gggg': Specified color ID '" << 
    color_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R19_P25_sm_gg_gggg_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(color_configs[chosenProcID][color_ID].size(), 0); 
  for (unsigned int j = 0; j < (2 * nexternal); j++ )
  {
    res[chosenPerm[j/2] * 2 + j%2] = color_configs[chosenProcID][color_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Color Config -> Color ID
int PY8MEs_R19_P25_sm_gg_gggg::getColorIDForConfig(vector<int> color_config,
    int specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = -1; 
  if (color_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < color_configs[chosenProcID].size(); i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < (nexternal * 2); j++ )
      {

        // If colorless then make sure it matches
        // The little arithmetics in the color index is just
        // the permutation applies on the particle list which is
        // twice smaller since each particle can have two color indices.
        if (color_config[j] == 0)
        {
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] != 0)
          {
            found = false; 
            break; 
          }
          // Otherwise check that the color linked position matches
        }
        else
        {
          int color_linked_pos = -1; 
          // Find the other end of the line in the user color config
          for (unsigned int k = 0; k < (nexternal * 2); k++ )
          {
            if (k == j)
              continue; 
            if (color_config[j] == color_config[k])
            {
              color_linked_pos = k; 
              break; 
            }
          }
          if (color_linked_pos == -1)
          {
            cerr <<  "Error in function 'getColorIDForConfig' of class" << 
            " 'PY8MEs_R19_P25_sm_gg_gggg': A color line could " << 
            " not be closed." << endl; 
            throw PY8MEs_R19_P25_sm_gg_gggg_exception; 
          }
          // Now check whether the color line matches
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] !=
              color_configs[chosenProcID][i][chosenPerm[color_linked_pos/2] * 2
              + color_linked_pos%2])
          {
            found = false; 
            break; 
          }
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_icol = i; 
        break; 
      }
    }

    if (user_icol == -1)
    {
      cerr <<  "Error in function 'getColorIDForConfig' of class" << 
      " 'PY8MEs_R19_P25_sm_gg_gggg': Specified color" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R19_P25_sm_gg_gggg_exception; 
    }
  }
  return user_icol; 
}

//--------------------------------------------------------------------------
// Returns all result previously computed in SigmaKin
vector < vec_double > PY8MEs_R19_P25_sm_gg_gggg::getAllResults(int
    specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return all_results[chosenProcID]; 
}

//--------------------------------------------------------------------------
// Returns a result previously computed in SigmaKin for a specific helicity
// and color ID. -1 means avg and summed over that characteristic.
double PY8MEs_R19_P25_sm_gg_gggg::getResult(int helicity_ID, int color_ID, int
    specify_proc_ID)
{
  if (helicity_ID < - 1 || helicity_ID >= ncomb)
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R19_P25_sm_gg_gggg': Specified helicity ID '" << 
    helicity_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R19_P25_sm_gg_gggg_exception; 
  }
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < - 1 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R19_P25_sm_gg_gggg': Specified color ID '" << 
    color_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R19_P25_sm_gg_gggg_exception; 
  }
  return all_results[chosenProcID][helicity_ID + 1][color_ID + 1]; 
}

//--------------------------------------------------------------------------
// Check for the availability of the requested process and if available,
// If available, this returns the corresponding permutation and Proc_ID to use.
// If not available, this returns a negative Proc_ID.
pair < vector<int> , int > PY8MEs_R19_P25_sm_gg_gggg::static_getPY8ME(vector<int> initial_pdgs, vector<int> final_pdgs, set<int> schannels) 
{

  // Not available return value
  pair < vector<int> , int > NA(vector<int> (), -1); 

  // Check if s-channel requirements match
  if (nreq_s_channels > 0)
  {
    if (schannels != s_channel_proc)
      return NA; 
  }
  else
  {
    if (schannels.size() != 0)
      return NA; 
  }

  // Check number of final state particles
  if (final_pdgs.size() != (nexternal - ninitial))
    return NA; 

  // Check number of initial state particles
  if (initial_pdgs.size() != ninitial)
    return NA; 

  // List of processes available in this class
  const int nprocs = 1; 
  const int proc_IDS[nprocs] = {0}; 
  const int in_pdgs[nprocs][ninitial] = {{21, 21}}; 
  const int out_pdgs[nprocs][nexternal - ninitial] = {{21, 21, 21, 21}}; 

  bool in_pdgs_used[ninitial]; 
  bool out_pdgs_used[nexternal - ninitial]; 
  for(unsigned int i = 0; i < nprocs; i++ )
  {
    int permutations[nexternal]; 

    // Reinitialize initial state look-up variables
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      in_pdgs_used[j] = false; 
      permutations[j] = -1; 
    }
    // Look for initial state matches
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      for(unsigned int k = 0; k < ninitial; k++ )
      {
        // Make sure it has not been used already
        if (in_pdgs_used[k])
          continue; 
        if (initial_pdgs[k] == in_pdgs[i][j])
        {
          permutations[j] = k; 
          in_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[ninitial - 1] == -1)
      continue; 

    // Reinitialize final state look-up variables
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      out_pdgs_used[j] = false; 
      permutations[ninitial + j] = -1; 
    }
    // Look for final state matches
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      for(unsigned int k = 0; k < (nexternal - ninitial); k++ )
      {
        // Make sure it has not been used already
        if (out_pdgs_used[k])
          continue; 
        if (final_pdgs[k] == out_pdgs[i][j])
        {
          permutations[ninitial + j] = ninitial + k; 
          out_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[ninitial + j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[nexternal - 1] == -1)
      continue; 

    // Return process found
    return pair < vector<int> , int > (vector<int> (permutations, permutations
        + nexternal), proc_IDS[i]);
  }

  // No process found
  return NA; 
}

//--------------------------------------------------------------------------
// Set momenta
void PY8MEs_R19_P25_sm_gg_gggg::setMomenta(vector < vec_double > momenta_picked)
{
  if (momenta_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setMomenta' of class" << 
    " 'PY8MEs_R19_P25_sm_gg_gggg': Incorrect number of" << 
    " momenta specified." << endl; 
    throw PY8MEs_R19_P25_sm_gg_gggg_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    if (momenta_picked[i].size() != 4)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R19_P25_sm_gg_gggg': Incorrect number of" << 
      " momenta components specified." << endl; 
      throw PY8MEs_R19_P25_sm_gg_gggg_exception; 
    }
    if (momenta_picked[i][0] < 0.0)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R19_P25_sm_gg_gggg': A momentum was specified" << 
      " with negative energy. Check conventions." << endl; 
      throw PY8MEs_R19_P25_sm_gg_gggg_exception; 
    }
    for (unsigned int j = 0; j < 4; j++ )
    {
      p[i][j] = momenta_picked[i][j]; 
    }
  }
}

//--------------------------------------------------------------------------
// Set color configuration to use. An empty vector means sum over all.
void PY8MEs_R19_P25_sm_gg_gggg::setColors(vector<int> colors_picked)
{
  if (colors_picked.size() == 0)
  {
    user_colors = vector<int> (); 
    return; 
  }
  if (colors_picked.size() != (2 * nexternal))
  {
    cerr <<  "Error in function 'setColors' of class" << 
    " 'PY8MEs_R19_P25_sm_gg_gggg': Incorrect number" << 
    " of colors specified." << endl; 
    throw PY8MEs_R19_P25_sm_gg_gggg_exception; 
  }
  user_colors = vector<int> ((2 * nexternal), 0); 
  for(unsigned int i = 0; i < (2 * nexternal); i++ )
  {
    user_colors[i] = colors_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the helicity configuration to use. Am empty vector means sum over all.
void PY8MEs_R19_P25_sm_gg_gggg::setHelicities(vector<int> helicities_picked) 
{
  if (helicities_picked.size() != nexternal)
  {
    if (helicities_picked.size() == 0)
    {
      user_helicities = vector<int> (); 
      return; 
    }
    cerr <<  "Error in function 'setHelicities' of class" << 
    " 'PY8MEs_R19_P25_sm_gg_gggg': Incorrect number" << 
    " of helicities specified." << endl; 
    throw PY8MEs_R19_P25_sm_gg_gggg_exception; 
  }
  user_helicities = vector<int> (nexternal, 0); 
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    user_helicities[i] = helicities_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the permutation to use (will apply to momenta, colors and helicities)
void PY8MEs_R19_P25_sm_gg_gggg::setPermutation(vector<int> perm_picked) 
{
  if (perm_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setPermutations' of class" << 
    " 'PY8MEs_R19_P25_sm_gg_gggg': Incorrect number" << 
    " of permutations specified." << endl; 
    throw PY8MEs_R19_P25_sm_gg_gggg_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    perm[i] = perm_picked[i]; 
  }
}

// Set the proc_ID to use
void PY8MEs_R19_P25_sm_gg_gggg::setProcID(int procID_picked) 
{
  proc_ID = procID_picked; 
}

//--------------------------------------------------------------------------
// Initialize process.

void PY8MEs_R19_P25_sm_gg_gggg::initProc() 
{

  // Initialize flags
  include_symmetry_factors = true; 
  include_helicity_averaging_factors = true; 
  include_color_averaging_factors = true; 

  // Initialize vectors.
  perm = vector<int> (nexternal, 0); 
  user_colors = vector<int> (2 * nexternal, 0); 
  user_helicities = vector<int> (nexternal, 0); 
  p = vector < double * > (); 
  for (unsigned int i = 0; i < nexternal; i++ )
  {
    p.push_back(new double[4]); 
  }
  initColorConfigs(); 
  // Synchronize local variables dependent on the model with the active model.
  mME = vector<double> (nexternal, 0.); 
  syncProcModelParams(); 
  jamp2 = vector < vec_double > (1); 
  jamp2[0] = vector<double> (120, 0.); 
  all_results = vector < vec_vec_double > (1); 
  // The first entry is always the color or helicity avg/summed matrix element.
  all_results[0] = vector < vec_double > (ncomb + 1, vector<double> (120 + 1,
      0.));
}

// Synchronize local variables of the process that depend on the model
// parameters
void PY8MEs_R19_P25_sm_gg_gggg::syncProcModelParams() 
{

  // Instantiate the model class and set parameters that stay fixed during run
  mME[0] = pars->ZERO; 
  mME[1] = pars->ZERO; 
  mME[2] = pars->ZERO; 
  mME[3] = pars->ZERO; 
  mME[4] = pars->ZERO; 
  mME[5] = pars->ZERO; 
}

//--------------------------------------------------------------------------
// Setter allowing to force particular values for the external masses
void PY8MEs_R19_P25_sm_gg_gggg::setMasses(vec_double external_masses) 
{

  if (external_masses.size() != mME.size())
  {
    cerr <<  "Error in function 'setMasses' of class" << 
    " 'PY8MEs_R19_P25_sm_gg_gggg': Incorrect number of" << 
    " masses specified." << endl; 
    throw PY8MEs_R19_P25_sm_gg_gggg_exception; 
  }
  for (unsigned int j = 0; j < mME.size(); j++ )
  {
    mME[j] = external_masses[perm[j]]; 
  }
}

//--------------------------------------------------------------------------
// Getter accessing external masses with the correct ordering
vector<double> PY8MEs_R19_P25_sm_gg_gggg::getMasses() 
{

  vec_double external_masses; 
  vector<int> invertedPerm; 
  invertedPerm = invert_mapping(perm); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    external_masses.push_back(mME[invertedPerm[i]]); 
  }
  return external_masses; 

}


// Set all values of the external masses to float(-mode) where mode can be
// 0 : Mass taken from the model
// 1 : Mass taken from p_i^2 if not massless to begin with
// 2 : Mass always taken from p_i^2.
void PY8MEs_R19_P25_sm_gg_gggg::setExternalMassesMode(int mode) 
{
  if (mode != 0 && mode != 1 && mode != 2)
  {
    cerr <<  "Error in function 'setExternalMassesMode' of class" << 
    " 'PY8MEs_R19_P25_sm_gg_gggg': Incorrect mode selected :" << mode << 
    ". It must be either 0, 1 or 2" << endl; 
    throw PY8MEs_R19_P25_sm_gg_gggg_exception; 
  }
  if (mode == 0)
  {
    syncProcModelParams(); 
  }
  else if (mode == 1)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      if (mME[j] != pars->ZERO)
      {
        mME[j] = -1.0; 
      }
    }
  }
  else if (mode == 2)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      mME[j] = -1.0; 
    }
  }
}

//--------------------------------------------------------------------------
// Evaluate the squared matrix element.

double PY8MEs_R19_P25_sm_gg_gggg::sigmaKin() 
{
  // Set the parameters which change event by event
  pars->setDependentParameters(); 
  pars->setDependentCouplings(); 
  // Reset color flows
  for(int i = 0; i < 120; i++ )
    jamp2[0][i] = 0.; 

  // Save previous values of mME
  vector<double> saved_mME(mME.size(), 0.0); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (mME[i] < 0.0)
    {
      saved_mME[i] = mME[i]; 
      mME[i] = sqrt(abs(pow(p[perm[i]][0], 2) - 
      (pow(p[perm[i]][1], 2) + pow(p[perm[i]][2], 2) + pow(p[perm[i]][3],
          2))));
    }
  }

  // Local variables and constants
  const int max_tries = 10; 
  vector < vec_bool > goodhel(nprocesses, vec_bool(ncomb, false)); 
  vec_int ntry(nprocesses, 0); 
  double t = 0.; 
  double result = 0.; 

  if (ntry[proc_ID] <= max_tries)
    ntry[proc_ID] = ntry[proc_ID] + 1; 

  // Find which helicity configuration is asked for
  // -1 indicates one wants to sum over helicities
  int user_ihel = getHelicityIDForConfig(user_helicities); 

  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = getColorIDForConfig(user_colors); 

  // Reset the list of results that will be recomputed here
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      all_results[proc_ID][ihel + 1][icolor + 1] = 0.; 
    }
  }

  // Calculate the matrix element for all helicities
  // unless already detected as vanishing
  for(int ihel = 0; ihel < ncomb; ihel++ )
  {
    // Skip helicity if already detected as vanishing
    if ((ntry[proc_ID] >= max_tries) && !goodhel[proc_ID][ihel])
      continue; 

    // Also skip helicity if user asks for a specific one
    if ((ntry[proc_ID] >= max_tries) && user_ihel != -1 && user_ihel != ihel)
      continue; 

    calculate_wavefunctions(helicities[ihel]); 

    // Reset locally computed color flows
    for(int i = 0; i < 120; i++ )
      jamp2[0][i] = 0.; 

    if (proc_ID == 0)
      t = matrix_19_gg_gggg(); 

    // Store which helicities give non-zero result
    if ((ntry[proc_ID] < max_tries) && t != 0. && !goodhel[proc_ID][ihel])
      goodhel[proc_ID][ihel] = true; 

    // Aggregate results
    if (user_ihel == -1 || user_ihel == ihel)
    {
      if (user_icol == -1)
      {
        result = result + t; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][0] += t; 
          for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
          {
            all_results[proc_ID][0][i + 1] += jamp2[proc_ID][i]; 
          }
        }
        all_results[proc_ID][ihel + 1][0] += t; 
        for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
        {
          all_results[proc_ID][ihel + 1][i + 1] += jamp2[proc_ID][i]; 
        }
      }
      else
      {
        result = result + jamp2[proc_ID][user_icol]; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][user_icol + 1] += jamp2[proc_ID][user_icol]; 
        }
        all_results[proc_ID][ihel + 1][user_icol + 1] +=
            jamp2[proc_ID][user_icol];
      }
    }

  }

  // Normalize results with the identical particle factor
  if (include_symmetry_factors)
  {
    result = result/denom_iden[proc_ID]; 
  }
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      if (include_symmetry_factors)
      {
        all_results[proc_ID][ihel + 1][icolor + 1] /= denom_iden[proc_ID]; 
      }
    }
  }


  // Normalize when when summing+averaging over helicity configurations
  if (user_ihel == -1 && include_helicity_averaging_factors)
  {
    result /= denom_hels[proc_ID]; 
    if (user_icol == -1)
    {
      all_results[proc_ID][0][0] /= denom_hels[proc_ID]; 
      for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
      {
        all_results[proc_ID][0][i + 1] /= denom_hels[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][0][user_icol + 1] /= denom_hels[proc_ID]; 
    }
  }

  // Normalize when summing+averaging over color configurations
  if (user_icol == -1 && include_color_averaging_factors)
  {
    result /= denom_colors[proc_ID]; 
    if (user_ihel == -1)
    {
      all_results[proc_ID][0][0] /= denom_colors[proc_ID]; 
      for (unsigned int i = 0; i < ncomb; i++ )
      {
        all_results[proc_ID][i + 1][0] /= denom_colors[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][user_ihel + 1][0] /= denom_colors[proc_ID]; 
    }
  }

  // Reinstate previous values of mME
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (saved_mME[i] < 0.0)
    {
      mME[i] = saved_mME[i]; 
    }
  }

  // Finally return it
  return result; 
}

//==========================================================================
// Private class member functions

//--------------------------------------------------------------------------
// Evaluate |M|^2 for each subprocess

void PY8MEs_R19_P25_sm_gg_gggg::calculate_wavefunctions(const int hel[])
{
  // Calculate wavefunctions for all processes
  // Calculate all wavefunctions
  vxxxxx(p[perm[0]], mME[0], hel[0], -1, w[0]); 
  vxxxxx(p[perm[1]], mME[1], hel[1], -1, w[1]); 
  vxxxxx(p[perm[2]], mME[2], hel[2], +1, w[2]); 
  vxxxxx(p[perm[3]], mME[3], hel[3], +1, w[3]); 
  vxxxxx(p[perm[4]], mME[4], hel[4], +1, w[4]); 
  vxxxxx(p[perm[5]], mME[5], hel[5], +1, w[5]); 
  VVV1P0_1(w[0], w[1], pars->GC_10, pars->ZERO, pars->ZERO, w[6]); 
  VVV1P0_1(w[2], w[3], pars->GC_10, pars->ZERO, pars->ZERO, w[7]); 
  VVV1P0_1(w[6], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[8]); 
  VVV1P0_1(w[6], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[9]); 
  VVV1P0_1(w[4], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[10]); 
  VVV1P0_1(w[2], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[11]); 
  VVV1P0_1(w[6], w[3], pars->GC_10, pars->ZERO, pars->ZERO, w[12]); 
  VVV1P0_1(w[3], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[13]); 
  VVV1P0_1(w[2], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[14]); 
  VVV1P0_1(w[3], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[15]); 
  VVV1P0_1(w[6], w[2], pars->GC_10, pars->ZERO, pars->ZERO, w[16]); 
  VVVV1P0_1(w[2], w[3], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[17]); 
  VVVV3P0_1(w[2], w[3], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[18]); 
  VVVV4P0_1(w[2], w[3], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[19]); 
  VVVV1P0_1(w[2], w[3], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[20]); 
  VVVV3P0_1(w[2], w[3], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[21]); 
  VVVV4P0_1(w[2], w[3], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[22]); 
  VVVV1P0_1(w[2], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[23]); 
  VVVV3P0_1(w[2], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[24]); 
  VVVV4P0_1(w[2], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[25]); 
  VVVV1P0_1(w[3], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[26]); 
  VVVV3P0_1(w[3], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[27]); 
  VVVV4P0_1(w[3], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[28]); 
  VVV1P0_1(w[0], w[2], pars->GC_10, pars->ZERO, pars->ZERO, w[29]); 
  VVV1P0_1(w[1], w[3], pars->GC_10, pars->ZERO, pars->ZERO, w[30]); 
  VVV1P0_1(w[29], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[31]); 
  VVV1P0_1(w[29], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[32]); 
  VVV1P0_1(w[1], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[33]); 
  VVV1P0_1(w[29], w[3], pars->GC_10, pars->ZERO, pars->ZERO, w[34]); 
  VVV1P0_1(w[1], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[35]); 
  VVV1P0_1(w[29], w[1], pars->GC_10, pars->ZERO, pars->ZERO, w[36]); 
  VVVV1P0_1(w[1], w[3], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[37]); 
  VVVV3P0_1(w[1], w[3], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[38]); 
  VVVV4P0_1(w[1], w[3], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[39]); 
  VVVV1P0_1(w[1], w[3], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[40]); 
  VVVV3P0_1(w[1], w[3], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[41]); 
  VVVV4P0_1(w[1], w[3], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[42]); 
  VVVV1P0_1(w[1], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[43]); 
  VVVV3P0_1(w[1], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[44]); 
  VVVV4P0_1(w[1], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[45]); 
  VVV1P0_1(w[0], w[3], pars->GC_10, pars->ZERO, pars->ZERO, w[46]); 
  VVV1P0_1(w[1], w[2], pars->GC_10, pars->ZERO, pars->ZERO, w[47]); 
  VVV1P0_1(w[46], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[48]); 
  VVV1P0_1(w[46], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[49]); 
  VVV1P0_1(w[46], w[2], pars->GC_10, pars->ZERO, pars->ZERO, w[50]); 
  VVV1P0_1(w[46], w[1], pars->GC_10, pars->ZERO, pars->ZERO, w[51]); 
  VVVV1P0_1(w[1], w[2], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[52]); 
  VVVV3P0_1(w[1], w[2], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[53]); 
  VVVV4P0_1(w[1], w[2], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[54]); 
  VVVV1P0_1(w[1], w[2], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[55]); 
  VVVV3P0_1(w[1], w[2], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[56]); 
  VVVV4P0_1(w[1], w[2], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[57]); 
  VVV1P0_1(w[0], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[58]); 
  VVV1P0_1(w[58], w[3], pars->GC_10, pars->ZERO, pars->ZERO, w[59]); 
  VVV1P0_1(w[58], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[60]); 
  VVV1P0_1(w[58], w[2], pars->GC_10, pars->ZERO, pars->ZERO, w[61]); 
  VVV1P0_1(w[58], w[1], pars->GC_10, pars->ZERO, pars->ZERO, w[62]); 
  VVVV1P0_1(w[1], w[2], w[3], pars->GC_12, pars->ZERO, pars->ZERO, w[63]); 
  VVVV3P0_1(w[1], w[2], w[3], pars->GC_12, pars->ZERO, pars->ZERO, w[64]); 
  VVVV4P0_1(w[1], w[2], w[3], pars->GC_12, pars->ZERO, pars->ZERO, w[65]); 
  VVV1P0_1(w[0], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[66]); 
  VVV1P0_1(w[66], w[3], pars->GC_10, pars->ZERO, pars->ZERO, w[67]); 
  VVV1P0_1(w[66], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[68]); 
  VVV1P0_1(w[66], w[2], pars->GC_10, pars->ZERO, pars->ZERO, w[69]); 
  VVV1P0_1(w[66], w[1], pars->GC_10, pars->ZERO, pars->ZERO, w[70]); 
  VVV1P0_1(w[0], w[47], pars->GC_10, pars->ZERO, pars->ZERO, w[71]); 
  VVV1P0_1(w[0], w[15], pars->GC_10, pars->ZERO, pars->ZERO, w[72]); 
  VVV1P0_1(w[0], w[13], pars->GC_10, pars->ZERO, pars->ZERO, w[73]); 
  VVV1P0_1(w[0], w[10], pars->GC_10, pars->ZERO, pars->ZERO, w[74]); 
  VVV1P0_1(w[0], w[30], pars->GC_10, pars->ZERO, pars->ZERO, w[75]); 
  VVV1P0_1(w[0], w[11], pars->GC_10, pars->ZERO, pars->ZERO, w[76]); 
  VVV1P0_1(w[0], w[14], pars->GC_10, pars->ZERO, pars->ZERO, w[77]); 
  VVV1P0_1(w[0], w[33], pars->GC_10, pars->ZERO, pars->ZERO, w[78]); 
  VVV1P0_1(w[0], w[7], pars->GC_10, pars->ZERO, pars->ZERO, w[79]); 
  VVV1P0_1(w[0], w[35], pars->GC_10, pars->ZERO, pars->ZERO, w[80]); 
  VVVV1P0_1(w[0], w[1], w[2], pars->GC_12, pars->ZERO, pars->ZERO, w[81]); 
  VVVV3P0_1(w[0], w[1], w[2], pars->GC_12, pars->ZERO, pars->ZERO, w[82]); 
  VVVV4P0_1(w[0], w[1], w[2], pars->GC_12, pars->ZERO, pars->ZERO, w[83]); 
  VVVV1P0_1(w[0], w[1], w[3], pars->GC_12, pars->ZERO, pars->ZERO, w[84]); 
  VVVV3P0_1(w[0], w[1], w[3], pars->GC_12, pars->ZERO, pars->ZERO, w[85]); 
  VVVV4P0_1(w[0], w[1], w[3], pars->GC_12, pars->ZERO, pars->ZERO, w[86]); 
  VVVV1P0_1(w[0], w[1], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[87]); 
  VVVV3P0_1(w[0], w[1], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[88]); 
  VVVV4P0_1(w[0], w[1], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[89]); 
  VVVV1P0_1(w[0], w[1], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[90]); 
  VVVV3P0_1(w[0], w[1], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[91]); 
  VVVV4P0_1(w[0], w[1], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[92]); 
  VVVV1P0_1(w[0], w[2], w[3], pars->GC_12, pars->ZERO, pars->ZERO, w[93]); 
  VVVV3P0_1(w[0], w[2], w[3], pars->GC_12, pars->ZERO, pars->ZERO, w[94]); 
  VVVV4P0_1(w[0], w[2], w[3], pars->GC_12, pars->ZERO, pars->ZERO, w[95]); 
  VVVV1P0_1(w[0], w[2], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[96]); 
  VVVV3P0_1(w[0], w[2], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[97]); 
  VVVV4P0_1(w[0], w[2], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[98]); 
  VVVV1P0_1(w[0], w[2], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[99]); 
  VVVV3P0_1(w[0], w[2], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[100]); 
  VVVV4P0_1(w[0], w[2], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[101]); 
  VVVV1P0_1(w[0], w[3], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[102]); 
  VVVV3P0_1(w[0], w[3], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[103]); 
  VVVV4P0_1(w[0], w[3], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[104]); 
  VVVV1P0_1(w[0], w[3], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[105]); 
  VVVV3P0_1(w[0], w[3], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[106]); 
  VVVV4P0_1(w[0], w[3], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[107]); 
  VVVV1P0_1(w[0], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[108]); 
  VVVV3P0_1(w[0], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[109]); 
  VVVV4P0_1(w[0], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[110]); 

  // Calculate all amplitudes
  // Amplitude(s) for diagram number 0
  VVVV1_0(w[6], w[7], w[4], w[5], pars->GC_12, amp[0]); 
  VVVV3_0(w[6], w[7], w[4], w[5], pars->GC_12, amp[1]); 
  VVVV4_0(w[6], w[7], w[4], w[5], pars->GC_12, amp[2]); 
  VVV1_0(w[7], w[5], w[8], pars->GC_10, amp[3]); 
  VVV1_0(w[7], w[4], w[9], pars->GC_10, amp[4]); 
  VVV1_0(w[6], w[7], w[10], pars->GC_10, amp[5]); 
  VVVV1_0(w[6], w[11], w[3], w[5], pars->GC_12, amp[6]); 
  VVVV3_0(w[6], w[11], w[3], w[5], pars->GC_12, amp[7]); 
  VVVV4_0(w[6], w[11], w[3], w[5], pars->GC_12, amp[8]); 
  VVV1_0(w[11], w[5], w[12], pars->GC_10, amp[9]); 
  VVV1_0(w[11], w[3], w[9], pars->GC_10, amp[10]); 
  VVV1_0(w[6], w[11], w[13], pars->GC_10, amp[11]); 
  VVVV1_0(w[6], w[14], w[3], w[4], pars->GC_12, amp[12]); 
  VVVV3_0(w[6], w[14], w[3], w[4], pars->GC_12, amp[13]); 
  VVVV4_0(w[6], w[14], w[3], w[4], pars->GC_12, amp[14]); 
  VVV1_0(w[14], w[4], w[12], pars->GC_10, amp[15]); 
  VVV1_0(w[14], w[3], w[8], pars->GC_10, amp[16]); 
  VVV1_0(w[6], w[14], w[15], pars->GC_10, amp[17]); 
  VVVV1_0(w[6], w[2], w[15], w[5], pars->GC_12, amp[18]); 
  VVVV3_0(w[6], w[2], w[15], w[5], pars->GC_12, amp[19]); 
  VVVV4_0(w[6], w[2], w[15], w[5], pars->GC_12, amp[20]); 
  VVV1_0(w[15], w[5], w[16], pars->GC_10, amp[21]); 
  VVV1_0(w[2], w[15], w[9], pars->GC_10, amp[22]); 
  VVVV1_0(w[6], w[2], w[13], w[4], pars->GC_12, amp[23]); 
  VVVV3_0(w[6], w[2], w[13], w[4], pars->GC_12, amp[24]); 
  VVVV4_0(w[6], w[2], w[13], w[4], pars->GC_12, amp[25]); 
  VVV1_0(w[13], w[4], w[16], pars->GC_10, amp[26]); 
  VVV1_0(w[2], w[13], w[8], pars->GC_10, amp[27]); 
  VVVV1_0(w[6], w[2], w[3], w[10], pars->GC_12, amp[28]); 
  VVVV3_0(w[6], w[2], w[3], w[10], pars->GC_12, amp[29]); 
  VVVV4_0(w[6], w[2], w[3], w[10], pars->GC_12, amp[30]); 
  VVV1_0(w[3], w[10], w[16], pars->GC_10, amp[31]); 
  VVV1_0(w[2], w[10], w[12], pars->GC_10, amp[32]); 
  VVV1_0(w[6], w[17], w[5], pars->GC_10, amp[33]); 
  VVV1_0(w[6], w[18], w[5], pars->GC_10, amp[34]); 
  VVV1_0(w[6], w[19], w[5], pars->GC_10, amp[35]); 
  VVV1_0(w[6], w[20], w[4], pars->GC_10, amp[36]); 
  VVV1_0(w[6], w[21], w[4], pars->GC_10, amp[37]); 
  VVV1_0(w[6], w[22], w[4], pars->GC_10, amp[38]); 
  VVV1_0(w[6], w[23], w[3], pars->GC_10, amp[39]); 
  VVV1_0(w[6], w[24], w[3], pars->GC_10, amp[40]); 
  VVV1_0(w[6], w[25], w[3], pars->GC_10, amp[41]); 
  VVV1_0(w[6], w[2], w[26], pars->GC_10, amp[42]); 
  VVV1_0(w[6], w[2], w[27], pars->GC_10, amp[43]); 
  VVV1_0(w[6], w[2], w[28], pars->GC_10, amp[44]); 
  VVVV1_0(w[29], w[30], w[4], w[5], pars->GC_12, amp[45]); 
  VVVV3_0(w[29], w[30], w[4], w[5], pars->GC_12, amp[46]); 
  VVVV4_0(w[29], w[30], w[4], w[5], pars->GC_12, amp[47]); 
  VVV1_0(w[30], w[5], w[31], pars->GC_10, amp[48]); 
  VVV1_0(w[30], w[4], w[32], pars->GC_10, amp[49]); 
  VVV1_0(w[29], w[30], w[10], pars->GC_10, amp[50]); 
  VVVV1_0(w[29], w[33], w[3], w[5], pars->GC_12, amp[51]); 
  VVVV3_0(w[29], w[33], w[3], w[5], pars->GC_12, amp[52]); 
  VVVV4_0(w[29], w[33], w[3], w[5], pars->GC_12, amp[53]); 
  VVV1_0(w[33], w[5], w[34], pars->GC_10, amp[54]); 
  VVV1_0(w[33], w[3], w[32], pars->GC_10, amp[55]); 
  VVV1_0(w[29], w[33], w[13], pars->GC_10, amp[56]); 
  VVVV1_0(w[29], w[35], w[3], w[4], pars->GC_12, amp[57]); 
  VVVV3_0(w[29], w[35], w[3], w[4], pars->GC_12, amp[58]); 
  VVVV4_0(w[29], w[35], w[3], w[4], pars->GC_12, amp[59]); 
  VVV1_0(w[35], w[4], w[34], pars->GC_10, amp[60]); 
  VVV1_0(w[35], w[3], w[31], pars->GC_10, amp[61]); 
  VVV1_0(w[29], w[35], w[15], pars->GC_10, amp[62]); 
  VVVV1_0(w[29], w[1], w[15], w[5], pars->GC_12, amp[63]); 
  VVVV3_0(w[29], w[1], w[15], w[5], pars->GC_12, amp[64]); 
  VVVV4_0(w[29], w[1], w[15], w[5], pars->GC_12, amp[65]); 
  VVV1_0(w[15], w[5], w[36], pars->GC_10, amp[66]); 
  VVV1_0(w[1], w[15], w[32], pars->GC_10, amp[67]); 
  VVVV1_0(w[29], w[1], w[13], w[4], pars->GC_12, amp[68]); 
  VVVV3_0(w[29], w[1], w[13], w[4], pars->GC_12, amp[69]); 
  VVVV4_0(w[29], w[1], w[13], w[4], pars->GC_12, amp[70]); 
  VVV1_0(w[13], w[4], w[36], pars->GC_10, amp[71]); 
  VVV1_0(w[1], w[13], w[31], pars->GC_10, amp[72]); 
  VVVV1_0(w[29], w[1], w[3], w[10], pars->GC_12, amp[73]); 
  VVVV3_0(w[29], w[1], w[3], w[10], pars->GC_12, amp[74]); 
  VVVV4_0(w[29], w[1], w[3], w[10], pars->GC_12, amp[75]); 
  VVV1_0(w[3], w[10], w[36], pars->GC_10, amp[76]); 
  VVV1_0(w[1], w[10], w[34], pars->GC_10, amp[77]); 
  VVV1_0(w[29], w[37], w[5], pars->GC_10, amp[78]); 
  VVV1_0(w[29], w[38], w[5], pars->GC_10, amp[79]); 
  VVV1_0(w[29], w[39], w[5], pars->GC_10, amp[80]); 
  VVV1_0(w[29], w[40], w[4], pars->GC_10, amp[81]); 
  VVV1_0(w[29], w[41], w[4], pars->GC_10, amp[82]); 
  VVV1_0(w[29], w[42], w[4], pars->GC_10, amp[83]); 
  VVV1_0(w[29], w[43], w[3], pars->GC_10, amp[84]); 
  VVV1_0(w[29], w[44], w[3], pars->GC_10, amp[85]); 
  VVV1_0(w[29], w[45], w[3], pars->GC_10, amp[86]); 
  VVV1_0(w[29], w[1], w[26], pars->GC_10, amp[87]); 
  VVV1_0(w[29], w[1], w[27], pars->GC_10, amp[88]); 
  VVV1_0(w[29], w[1], w[28], pars->GC_10, amp[89]); 
  VVVV1_0(w[46], w[47], w[4], w[5], pars->GC_12, amp[90]); 
  VVVV3_0(w[46], w[47], w[4], w[5], pars->GC_12, amp[91]); 
  VVVV4_0(w[46], w[47], w[4], w[5], pars->GC_12, amp[92]); 
  VVV1_0(w[47], w[5], w[48], pars->GC_10, amp[93]); 
  VVV1_0(w[47], w[4], w[49], pars->GC_10, amp[94]); 
  VVV1_0(w[46], w[47], w[10], pars->GC_10, amp[95]); 
  VVVV1_0(w[46], w[33], w[2], w[5], pars->GC_12, amp[96]); 
  VVVV3_0(w[46], w[33], w[2], w[5], pars->GC_12, amp[97]); 
  VVVV4_0(w[46], w[33], w[2], w[5], pars->GC_12, amp[98]); 
  VVV1_0(w[33], w[5], w[50], pars->GC_10, amp[99]); 
  VVV1_0(w[33], w[2], w[49], pars->GC_10, amp[100]); 
  VVV1_0(w[46], w[33], w[14], pars->GC_10, amp[101]); 
  VVVV1_0(w[46], w[35], w[2], w[4], pars->GC_12, amp[102]); 
  VVVV3_0(w[46], w[35], w[2], w[4], pars->GC_12, amp[103]); 
  VVVV4_0(w[46], w[35], w[2], w[4], pars->GC_12, amp[104]); 
  VVV1_0(w[35], w[4], w[50], pars->GC_10, amp[105]); 
  VVV1_0(w[35], w[2], w[48], pars->GC_10, amp[106]); 
  VVV1_0(w[46], w[35], w[11], pars->GC_10, amp[107]); 
  VVVV1_0(w[46], w[1], w[11], w[5], pars->GC_12, amp[108]); 
  VVVV3_0(w[46], w[1], w[11], w[5], pars->GC_12, amp[109]); 
  VVVV4_0(w[46], w[1], w[11], w[5], pars->GC_12, amp[110]); 
  VVV1_0(w[11], w[5], w[51], pars->GC_10, amp[111]); 
  VVV1_0(w[1], w[11], w[49], pars->GC_10, amp[112]); 
  VVVV1_0(w[46], w[1], w[14], w[4], pars->GC_12, amp[113]); 
  VVVV3_0(w[46], w[1], w[14], w[4], pars->GC_12, amp[114]); 
  VVVV4_0(w[46], w[1], w[14], w[4], pars->GC_12, amp[115]); 
  VVV1_0(w[14], w[4], w[51], pars->GC_10, amp[116]); 
  VVV1_0(w[1], w[14], w[48], pars->GC_10, amp[117]); 
  VVVV1_0(w[46], w[1], w[2], w[10], pars->GC_12, amp[118]); 
  VVVV3_0(w[46], w[1], w[2], w[10], pars->GC_12, amp[119]); 
  VVVV4_0(w[46], w[1], w[2], w[10], pars->GC_12, amp[120]); 
  VVV1_0(w[2], w[10], w[51], pars->GC_10, amp[121]); 
  VVV1_0(w[1], w[10], w[50], pars->GC_10, amp[122]); 
  VVV1_0(w[46], w[52], w[5], pars->GC_10, amp[123]); 
  VVV1_0(w[46], w[53], w[5], pars->GC_10, amp[124]); 
  VVV1_0(w[46], w[54], w[5], pars->GC_10, amp[125]); 
  VVV1_0(w[46], w[55], w[4], pars->GC_10, amp[126]); 
  VVV1_0(w[46], w[56], w[4], pars->GC_10, amp[127]); 
  VVV1_0(w[46], w[57], w[4], pars->GC_10, amp[128]); 
  VVV1_0(w[46], w[43], w[2], pars->GC_10, amp[129]); 
  VVV1_0(w[46], w[44], w[2], pars->GC_10, amp[130]); 
  VVV1_0(w[46], w[45], w[2], pars->GC_10, amp[131]); 
  VVV1_0(w[46], w[1], w[23], pars->GC_10, amp[132]); 
  VVV1_0(w[46], w[1], w[24], pars->GC_10, amp[133]); 
  VVV1_0(w[46], w[1], w[25], pars->GC_10, amp[134]); 
  VVVV1_0(w[58], w[47], w[3], w[5], pars->GC_12, amp[135]); 
  VVVV3_0(w[58], w[47], w[3], w[5], pars->GC_12, amp[136]); 
  VVVV4_0(w[58], w[47], w[3], w[5], pars->GC_12, amp[137]); 
  VVV1_0(w[47], w[5], w[59], pars->GC_10, amp[138]); 
  VVV1_0(w[47], w[3], w[60], pars->GC_10, amp[139]); 
  VVV1_0(w[58], w[47], w[13], pars->GC_10, amp[140]); 
  VVVV1_0(w[58], w[30], w[2], w[5], pars->GC_12, amp[141]); 
  VVVV3_0(w[58], w[30], w[2], w[5], pars->GC_12, amp[142]); 
  VVVV4_0(w[58], w[30], w[2], w[5], pars->GC_12, amp[143]); 
  VVV1_0(w[30], w[5], w[61], pars->GC_10, amp[144]); 
  VVV1_0(w[30], w[2], w[60], pars->GC_10, amp[145]); 
  VVV1_0(w[58], w[30], w[14], pars->GC_10, amp[146]); 
  VVVV1_0(w[58], w[35], w[2], w[3], pars->GC_12, amp[147]); 
  VVVV3_0(w[58], w[35], w[2], w[3], pars->GC_12, amp[148]); 
  VVVV4_0(w[58], w[35], w[2], w[3], pars->GC_12, amp[149]); 
  VVV1_0(w[35], w[3], w[61], pars->GC_10, amp[150]); 
  VVV1_0(w[35], w[2], w[59], pars->GC_10, amp[151]); 
  VVV1_0(w[58], w[35], w[7], pars->GC_10, amp[152]); 
  VVVV1_0(w[58], w[1], w[7], w[5], pars->GC_12, amp[153]); 
  VVVV3_0(w[58], w[1], w[7], w[5], pars->GC_12, amp[154]); 
  VVVV4_0(w[58], w[1], w[7], w[5], pars->GC_12, amp[155]); 
  VVV1_0(w[7], w[5], w[62], pars->GC_10, amp[156]); 
  VVV1_0(w[1], w[7], w[60], pars->GC_10, amp[157]); 
  VVVV1_0(w[58], w[1], w[14], w[3], pars->GC_12, amp[158]); 
  VVVV3_0(w[58], w[1], w[14], w[3], pars->GC_12, amp[159]); 
  VVVV4_0(w[58], w[1], w[14], w[3], pars->GC_12, amp[160]); 
  VVV1_0(w[14], w[3], w[62], pars->GC_10, amp[161]); 
  VVV1_0(w[1], w[14], w[59], pars->GC_10, amp[162]); 
  VVVV1_0(w[58], w[1], w[2], w[13], pars->GC_12, amp[163]); 
  VVVV3_0(w[58], w[1], w[2], w[13], pars->GC_12, amp[164]); 
  VVVV4_0(w[58], w[1], w[2], w[13], pars->GC_12, amp[165]); 
  VVV1_0(w[2], w[13], w[62], pars->GC_10, amp[166]); 
  VVV1_0(w[1], w[13], w[61], pars->GC_10, amp[167]); 
  VVV1_0(w[58], w[63], w[5], pars->GC_10, amp[168]); 
  VVV1_0(w[58], w[64], w[5], pars->GC_10, amp[169]); 
  VVV1_0(w[58], w[65], w[5], pars->GC_10, amp[170]); 
  VVV1_0(w[58], w[55], w[3], pars->GC_10, amp[171]); 
  VVV1_0(w[58], w[56], w[3], pars->GC_10, amp[172]); 
  VVV1_0(w[58], w[57], w[3], pars->GC_10, amp[173]); 
  VVV1_0(w[58], w[40], w[2], pars->GC_10, amp[174]); 
  VVV1_0(w[58], w[41], w[2], pars->GC_10, amp[175]); 
  VVV1_0(w[58], w[42], w[2], pars->GC_10, amp[176]); 
  VVV1_0(w[58], w[1], w[20], pars->GC_10, amp[177]); 
  VVV1_0(w[58], w[1], w[21], pars->GC_10, amp[178]); 
  VVV1_0(w[58], w[1], w[22], pars->GC_10, amp[179]); 
  VVVV1_0(w[66], w[47], w[3], w[4], pars->GC_12, amp[180]); 
  VVVV3_0(w[66], w[47], w[3], w[4], pars->GC_12, amp[181]); 
  VVVV4_0(w[66], w[47], w[3], w[4], pars->GC_12, amp[182]); 
  VVV1_0(w[47], w[4], w[67], pars->GC_10, amp[183]); 
  VVV1_0(w[47], w[3], w[68], pars->GC_10, amp[184]); 
  VVV1_0(w[66], w[47], w[15], pars->GC_10, amp[185]); 
  VVVV1_0(w[66], w[30], w[2], w[4], pars->GC_12, amp[186]); 
  VVVV3_0(w[66], w[30], w[2], w[4], pars->GC_12, amp[187]); 
  VVVV4_0(w[66], w[30], w[2], w[4], pars->GC_12, amp[188]); 
  VVV1_0(w[30], w[4], w[69], pars->GC_10, amp[189]); 
  VVV1_0(w[30], w[2], w[68], pars->GC_10, amp[190]); 
  VVV1_0(w[66], w[30], w[11], pars->GC_10, amp[191]); 
  VVVV1_0(w[66], w[33], w[2], w[3], pars->GC_12, amp[192]); 
  VVVV3_0(w[66], w[33], w[2], w[3], pars->GC_12, amp[193]); 
  VVVV4_0(w[66], w[33], w[2], w[3], pars->GC_12, amp[194]); 
  VVV1_0(w[33], w[3], w[69], pars->GC_10, amp[195]); 
  VVV1_0(w[33], w[2], w[67], pars->GC_10, amp[196]); 
  VVV1_0(w[66], w[33], w[7], pars->GC_10, amp[197]); 
  VVVV1_0(w[66], w[1], w[7], w[4], pars->GC_12, amp[198]); 
  VVVV3_0(w[66], w[1], w[7], w[4], pars->GC_12, amp[199]); 
  VVVV4_0(w[66], w[1], w[7], w[4], pars->GC_12, amp[200]); 
  VVV1_0(w[7], w[4], w[70], pars->GC_10, amp[201]); 
  VVV1_0(w[1], w[7], w[68], pars->GC_10, amp[202]); 
  VVVV1_0(w[66], w[1], w[11], w[3], pars->GC_12, amp[203]); 
  VVVV3_0(w[66], w[1], w[11], w[3], pars->GC_12, amp[204]); 
  VVVV4_0(w[66], w[1], w[11], w[3], pars->GC_12, amp[205]); 
  VVV1_0(w[11], w[3], w[70], pars->GC_10, amp[206]); 
  VVV1_0(w[1], w[11], w[67], pars->GC_10, amp[207]); 
  VVVV1_0(w[66], w[1], w[2], w[15], pars->GC_12, amp[208]); 
  VVVV3_0(w[66], w[1], w[2], w[15], pars->GC_12, amp[209]); 
  VVVV4_0(w[66], w[1], w[2], w[15], pars->GC_12, amp[210]); 
  VVV1_0(w[2], w[15], w[70], pars->GC_10, amp[211]); 
  VVV1_0(w[1], w[15], w[69], pars->GC_10, amp[212]); 
  VVV1_0(w[66], w[63], w[4], pars->GC_10, amp[213]); 
  VVV1_0(w[66], w[64], w[4], pars->GC_10, amp[214]); 
  VVV1_0(w[66], w[65], w[4], pars->GC_10, amp[215]); 
  VVV1_0(w[66], w[52], w[3], pars->GC_10, amp[216]); 
  VVV1_0(w[66], w[53], w[3], pars->GC_10, amp[217]); 
  VVV1_0(w[66], w[54], w[3], pars->GC_10, amp[218]); 
  VVV1_0(w[66], w[37], w[2], pars->GC_10, amp[219]); 
  VVV1_0(w[66], w[38], w[2], pars->GC_10, amp[220]); 
  VVV1_0(w[66], w[39], w[2], pars->GC_10, amp[221]); 
  VVV1_0(w[66], w[1], w[17], pars->GC_10, amp[222]); 
  VVV1_0(w[66], w[1], w[18], pars->GC_10, amp[223]); 
  VVV1_0(w[66], w[1], w[19], pars->GC_10, amp[224]); 
  VVVV1_0(w[0], w[47], w[15], w[5], pars->GC_12, amp[225]); 
  VVVV3_0(w[0], w[47], w[15], w[5], pars->GC_12, amp[226]); 
  VVVV4_0(w[0], w[47], w[15], w[5], pars->GC_12, amp[227]); 
  VVV1_0(w[15], w[5], w[71], pars->GC_10, amp[228]); 
  VVV1_0(w[47], w[5], w[72], pars->GC_10, amp[229]); 
  VVVV1_0(w[0], w[47], w[13], w[4], pars->GC_12, amp[230]); 
  VVVV3_0(w[0], w[47], w[13], w[4], pars->GC_12, amp[231]); 
  VVVV4_0(w[0], w[47], w[13], w[4], pars->GC_12, amp[232]); 
  VVV1_0(w[13], w[4], w[71], pars->GC_10, amp[233]); 
  VVV1_0(w[47], w[4], w[73], pars->GC_10, amp[234]); 
  VVVV1_0(w[0], w[47], w[3], w[10], pars->GC_12, amp[235]); 
  VVVV3_0(w[0], w[47], w[3], w[10], pars->GC_12, amp[236]); 
  VVVV4_0(w[0], w[47], w[3], w[10], pars->GC_12, amp[237]); 
  VVV1_0(w[3], w[10], w[71], pars->GC_10, amp[238]); 
  VVV1_0(w[47], w[3], w[74], pars->GC_10, amp[239]); 
  VVV1_0(w[0], w[47], w[26], pars->GC_10, amp[240]); 
  VVV1_0(w[0], w[47], w[27], pars->GC_10, amp[241]); 
  VVV1_0(w[0], w[47], w[28], pars->GC_10, amp[242]); 
  VVVV1_0(w[0], w[30], w[11], w[5], pars->GC_12, amp[243]); 
  VVVV3_0(w[0], w[30], w[11], w[5], pars->GC_12, amp[244]); 
  VVVV4_0(w[0], w[30], w[11], w[5], pars->GC_12, amp[245]); 
  VVV1_0(w[11], w[5], w[75], pars->GC_10, amp[246]); 
  VVV1_0(w[30], w[5], w[76], pars->GC_10, amp[247]); 
  VVVV1_0(w[0], w[30], w[14], w[4], pars->GC_12, amp[248]); 
  VVVV3_0(w[0], w[30], w[14], w[4], pars->GC_12, amp[249]); 
  VVVV4_0(w[0], w[30], w[14], w[4], pars->GC_12, amp[250]); 
  VVV1_0(w[14], w[4], w[75], pars->GC_10, amp[251]); 
  VVV1_0(w[30], w[4], w[77], pars->GC_10, amp[252]); 
  VVVV1_0(w[0], w[30], w[2], w[10], pars->GC_12, amp[253]); 
  VVVV3_0(w[0], w[30], w[2], w[10], pars->GC_12, amp[254]); 
  VVVV4_0(w[0], w[30], w[2], w[10], pars->GC_12, amp[255]); 
  VVV1_0(w[2], w[10], w[75], pars->GC_10, amp[256]); 
  VVV1_0(w[30], w[2], w[74], pars->GC_10, amp[257]); 
  VVV1_0(w[0], w[30], w[23], pars->GC_10, amp[258]); 
  VVV1_0(w[0], w[30], w[24], pars->GC_10, amp[259]); 
  VVV1_0(w[0], w[30], w[25], pars->GC_10, amp[260]); 
  VVVV1_0(w[0], w[33], w[7], w[5], pars->GC_12, amp[261]); 
  VVVV3_0(w[0], w[33], w[7], w[5], pars->GC_12, amp[262]); 
  VVVV4_0(w[0], w[33], w[7], w[5], pars->GC_12, amp[263]); 
  VVV1_0(w[7], w[5], w[78], pars->GC_10, amp[264]); 
  VVV1_0(w[33], w[5], w[79], pars->GC_10, amp[265]); 
  VVVV1_0(w[0], w[33], w[14], w[3], pars->GC_12, amp[266]); 
  VVVV3_0(w[0], w[33], w[14], w[3], pars->GC_12, amp[267]); 
  VVVV4_0(w[0], w[33], w[14], w[3], pars->GC_12, amp[268]); 
  VVV1_0(w[14], w[3], w[78], pars->GC_10, amp[269]); 
  VVV1_0(w[33], w[3], w[77], pars->GC_10, amp[270]); 
  VVVV1_0(w[0], w[33], w[2], w[13], pars->GC_12, amp[271]); 
  VVVV3_0(w[0], w[33], w[2], w[13], pars->GC_12, amp[272]); 
  VVVV4_0(w[0], w[33], w[2], w[13], pars->GC_12, amp[273]); 
  VVV1_0(w[2], w[13], w[78], pars->GC_10, amp[274]); 
  VVV1_0(w[33], w[2], w[73], pars->GC_10, amp[275]); 
  VVV1_0(w[0], w[33], w[20], pars->GC_10, amp[276]); 
  VVV1_0(w[0], w[33], w[21], pars->GC_10, amp[277]); 
  VVV1_0(w[0], w[33], w[22], pars->GC_10, amp[278]); 
  VVVV1_0(w[0], w[35], w[7], w[4], pars->GC_12, amp[279]); 
  VVVV3_0(w[0], w[35], w[7], w[4], pars->GC_12, amp[280]); 
  VVVV4_0(w[0], w[35], w[7], w[4], pars->GC_12, amp[281]); 
  VVV1_0(w[7], w[4], w[80], pars->GC_10, amp[282]); 
  VVV1_0(w[35], w[4], w[79], pars->GC_10, amp[283]); 
  VVVV1_0(w[0], w[35], w[11], w[3], pars->GC_12, amp[284]); 
  VVVV3_0(w[0], w[35], w[11], w[3], pars->GC_12, amp[285]); 
  VVVV4_0(w[0], w[35], w[11], w[3], pars->GC_12, amp[286]); 
  VVV1_0(w[11], w[3], w[80], pars->GC_10, amp[287]); 
  VVV1_0(w[35], w[3], w[76], pars->GC_10, amp[288]); 
  VVVV1_0(w[0], w[35], w[2], w[15], pars->GC_12, amp[289]); 
  VVVV3_0(w[0], w[35], w[2], w[15], pars->GC_12, amp[290]); 
  VVVV4_0(w[0], w[35], w[2], w[15], pars->GC_12, amp[291]); 
  VVV1_0(w[2], w[15], w[80], pars->GC_10, amp[292]); 
  VVV1_0(w[35], w[2], w[72], pars->GC_10, amp[293]); 
  VVV1_0(w[0], w[35], w[17], pars->GC_10, amp[294]); 
  VVV1_0(w[0], w[35], w[18], pars->GC_10, amp[295]); 
  VVV1_0(w[0], w[35], w[19], pars->GC_10, amp[296]); 
  VVVV1_0(w[0], w[1], w[7], w[10], pars->GC_12, amp[297]); 
  VVVV3_0(w[0], w[1], w[7], w[10], pars->GC_12, amp[298]); 
  VVVV4_0(w[0], w[1], w[7], w[10], pars->GC_12, amp[299]); 
  VVV1_0(w[1], w[10], w[79], pars->GC_10, amp[300]); 
  VVV1_0(w[1], w[7], w[74], pars->GC_10, amp[301]); 
  VVVV1_0(w[0], w[1], w[11], w[13], pars->GC_12, amp[302]); 
  VVVV3_0(w[0], w[1], w[11], w[13], pars->GC_12, amp[303]); 
  VVVV4_0(w[0], w[1], w[11], w[13], pars->GC_12, amp[304]); 
  VVV1_0(w[1], w[13], w[76], pars->GC_10, amp[305]); 
  VVV1_0(w[1], w[11], w[73], pars->GC_10, amp[306]); 
  VVVV1_0(w[0], w[1], w[14], w[15], pars->GC_12, amp[307]); 
  VVVV3_0(w[0], w[1], w[14], w[15], pars->GC_12, amp[308]); 
  VVVV4_0(w[0], w[1], w[14], w[15], pars->GC_12, amp[309]); 
  VVV1_0(w[1], w[15], w[77], pars->GC_10, amp[310]); 
  VVV1_0(w[1], w[14], w[72], pars->GC_10, amp[311]); 
  VVV1_0(w[81], w[15], w[5], pars->GC_10, amp[312]); 
  VVV1_0(w[82], w[15], w[5], pars->GC_10, amp[313]); 
  VVV1_0(w[83], w[15], w[5], pars->GC_10, amp[314]); 
  VVV1_0(w[81], w[13], w[4], pars->GC_10, amp[315]); 
  VVV1_0(w[82], w[13], w[4], pars->GC_10, amp[316]); 
  VVV1_0(w[83], w[13], w[4], pars->GC_10, amp[317]); 
  VVV1_0(w[81], w[3], w[10], pars->GC_10, amp[318]); 
  VVV1_0(w[82], w[3], w[10], pars->GC_10, amp[319]); 
  VVV1_0(w[83], w[3], w[10], pars->GC_10, amp[320]); 
  VVVV1_0(w[3], w[4], w[5], w[81], pars->GC_12, amp[321]); 
  VVVV3_0(w[3], w[4], w[5], w[81], pars->GC_12, amp[322]); 
  VVVV4_0(w[3], w[4], w[5], w[81], pars->GC_12, amp[323]); 
  VVVV1_0(w[3], w[4], w[5], w[82], pars->GC_12, amp[324]); 
  VVVV3_0(w[3], w[4], w[5], w[82], pars->GC_12, amp[325]); 
  VVVV4_0(w[3], w[4], w[5], w[82], pars->GC_12, amp[326]); 
  VVVV1_0(w[3], w[4], w[5], w[83], pars->GC_12, amp[327]); 
  VVVV3_0(w[3], w[4], w[5], w[83], pars->GC_12, amp[328]); 
  VVVV4_0(w[3], w[4], w[5], w[83], pars->GC_12, amp[329]); 
  VVV1_0(w[84], w[11], w[5], pars->GC_10, amp[330]); 
  VVV1_0(w[85], w[11], w[5], pars->GC_10, amp[331]); 
  VVV1_0(w[86], w[11], w[5], pars->GC_10, amp[332]); 
  VVV1_0(w[84], w[14], w[4], pars->GC_10, amp[333]); 
  VVV1_0(w[85], w[14], w[4], pars->GC_10, amp[334]); 
  VVV1_0(w[86], w[14], w[4], pars->GC_10, amp[335]); 
  VVV1_0(w[84], w[2], w[10], pars->GC_10, amp[336]); 
  VVV1_0(w[85], w[2], w[10], pars->GC_10, amp[337]); 
  VVV1_0(w[86], w[2], w[10], pars->GC_10, amp[338]); 
  VVVV1_0(w[2], w[4], w[5], w[84], pars->GC_12, amp[339]); 
  VVVV3_0(w[2], w[4], w[5], w[84], pars->GC_12, amp[340]); 
  VVVV4_0(w[2], w[4], w[5], w[84], pars->GC_12, amp[341]); 
  VVVV1_0(w[2], w[4], w[5], w[85], pars->GC_12, amp[342]); 
  VVVV3_0(w[2], w[4], w[5], w[85], pars->GC_12, amp[343]); 
  VVVV4_0(w[2], w[4], w[5], w[85], pars->GC_12, amp[344]); 
  VVVV1_0(w[2], w[4], w[5], w[86], pars->GC_12, amp[345]); 
  VVVV3_0(w[2], w[4], w[5], w[86], pars->GC_12, amp[346]); 
  VVVV4_0(w[2], w[4], w[5], w[86], pars->GC_12, amp[347]); 
  VVV1_0(w[87], w[7], w[5], pars->GC_10, amp[348]); 
  VVV1_0(w[88], w[7], w[5], pars->GC_10, amp[349]); 
  VVV1_0(w[89], w[7], w[5], pars->GC_10, amp[350]); 
  VVV1_0(w[87], w[14], w[3], pars->GC_10, amp[351]); 
  VVV1_0(w[88], w[14], w[3], pars->GC_10, amp[352]); 
  VVV1_0(w[89], w[14], w[3], pars->GC_10, amp[353]); 
  VVV1_0(w[87], w[2], w[13], pars->GC_10, amp[354]); 
  VVV1_0(w[88], w[2], w[13], pars->GC_10, amp[355]); 
  VVV1_0(w[89], w[2], w[13], pars->GC_10, amp[356]); 
  VVVV1_0(w[2], w[3], w[5], w[87], pars->GC_12, amp[357]); 
  VVVV3_0(w[2], w[3], w[5], w[87], pars->GC_12, amp[358]); 
  VVVV4_0(w[2], w[3], w[5], w[87], pars->GC_12, amp[359]); 
  VVVV1_0(w[2], w[3], w[5], w[88], pars->GC_12, amp[360]); 
  VVVV3_0(w[2], w[3], w[5], w[88], pars->GC_12, amp[361]); 
  VVVV4_0(w[2], w[3], w[5], w[88], pars->GC_12, amp[362]); 
  VVVV1_0(w[2], w[3], w[5], w[89], pars->GC_12, amp[363]); 
  VVVV3_0(w[2], w[3], w[5], w[89], pars->GC_12, amp[364]); 
  VVVV4_0(w[2], w[3], w[5], w[89], pars->GC_12, amp[365]); 
  VVV1_0(w[90], w[7], w[4], pars->GC_10, amp[366]); 
  VVV1_0(w[91], w[7], w[4], pars->GC_10, amp[367]); 
  VVV1_0(w[92], w[7], w[4], pars->GC_10, amp[368]); 
  VVV1_0(w[90], w[11], w[3], pars->GC_10, amp[369]); 
  VVV1_0(w[91], w[11], w[3], pars->GC_10, amp[370]); 
  VVV1_0(w[92], w[11], w[3], pars->GC_10, amp[371]); 
  VVV1_0(w[90], w[2], w[15], pars->GC_10, amp[372]); 
  VVV1_0(w[91], w[2], w[15], pars->GC_10, amp[373]); 
  VVV1_0(w[92], w[2], w[15], pars->GC_10, amp[374]); 
  VVVV1_0(w[2], w[3], w[4], w[90], pars->GC_12, amp[375]); 
  VVVV3_0(w[2], w[3], w[4], w[90], pars->GC_12, amp[376]); 
  VVVV4_0(w[2], w[3], w[4], w[90], pars->GC_12, amp[377]); 
  VVVV1_0(w[2], w[3], w[4], w[91], pars->GC_12, amp[378]); 
  VVVV3_0(w[2], w[3], w[4], w[91], pars->GC_12, amp[379]); 
  VVVV4_0(w[2], w[3], w[4], w[91], pars->GC_12, amp[380]); 
  VVVV1_0(w[2], w[3], w[4], w[92], pars->GC_12, amp[381]); 
  VVVV3_0(w[2], w[3], w[4], w[92], pars->GC_12, amp[382]); 
  VVVV4_0(w[2], w[3], w[4], w[92], pars->GC_12, amp[383]); 
  VVV1_0(w[93], w[33], w[5], pars->GC_10, amp[384]); 
  VVV1_0(w[94], w[33], w[5], pars->GC_10, amp[385]); 
  VVV1_0(w[95], w[33], w[5], pars->GC_10, amp[386]); 
  VVV1_0(w[93], w[35], w[4], pars->GC_10, amp[387]); 
  VVV1_0(w[94], w[35], w[4], pars->GC_10, amp[388]); 
  VVV1_0(w[95], w[35], w[4], pars->GC_10, amp[389]); 
  VVV1_0(w[93], w[1], w[10], pars->GC_10, amp[390]); 
  VVV1_0(w[94], w[1], w[10], pars->GC_10, amp[391]); 
  VVV1_0(w[95], w[1], w[10], pars->GC_10, amp[392]); 
  VVVV1_0(w[1], w[4], w[5], w[93], pars->GC_12, amp[393]); 
  VVVV3_0(w[1], w[4], w[5], w[93], pars->GC_12, amp[394]); 
  VVVV4_0(w[1], w[4], w[5], w[93], pars->GC_12, amp[395]); 
  VVVV1_0(w[1], w[4], w[5], w[94], pars->GC_12, amp[396]); 
  VVVV3_0(w[1], w[4], w[5], w[94], pars->GC_12, amp[397]); 
  VVVV4_0(w[1], w[4], w[5], w[94], pars->GC_12, amp[398]); 
  VVVV1_0(w[1], w[4], w[5], w[95], pars->GC_12, amp[399]); 
  VVVV3_0(w[1], w[4], w[5], w[95], pars->GC_12, amp[400]); 
  VVVV4_0(w[1], w[4], w[5], w[95], pars->GC_12, amp[401]); 
  VVV1_0(w[96], w[30], w[5], pars->GC_10, amp[402]); 
  VVV1_0(w[97], w[30], w[5], pars->GC_10, amp[403]); 
  VVV1_0(w[98], w[30], w[5], pars->GC_10, amp[404]); 
  VVV1_0(w[96], w[35], w[3], pars->GC_10, amp[405]); 
  VVV1_0(w[97], w[35], w[3], pars->GC_10, amp[406]); 
  VVV1_0(w[98], w[35], w[3], pars->GC_10, amp[407]); 
  VVV1_0(w[96], w[1], w[13], pars->GC_10, amp[408]); 
  VVV1_0(w[97], w[1], w[13], pars->GC_10, amp[409]); 
  VVV1_0(w[98], w[1], w[13], pars->GC_10, amp[410]); 
  VVVV1_0(w[1], w[3], w[5], w[96], pars->GC_12, amp[411]); 
  VVVV3_0(w[1], w[3], w[5], w[96], pars->GC_12, amp[412]); 
  VVVV4_0(w[1], w[3], w[5], w[96], pars->GC_12, amp[413]); 
  VVVV1_0(w[1], w[3], w[5], w[97], pars->GC_12, amp[414]); 
  VVVV3_0(w[1], w[3], w[5], w[97], pars->GC_12, amp[415]); 
  VVVV4_0(w[1], w[3], w[5], w[97], pars->GC_12, amp[416]); 
  VVVV1_0(w[1], w[3], w[5], w[98], pars->GC_12, amp[417]); 
  VVVV3_0(w[1], w[3], w[5], w[98], pars->GC_12, amp[418]); 
  VVVV4_0(w[1], w[3], w[5], w[98], pars->GC_12, amp[419]); 
  VVV1_0(w[99], w[30], w[4], pars->GC_10, amp[420]); 
  VVV1_0(w[100], w[30], w[4], pars->GC_10, amp[421]); 
  VVV1_0(w[101], w[30], w[4], pars->GC_10, amp[422]); 
  VVV1_0(w[99], w[33], w[3], pars->GC_10, amp[423]); 
  VVV1_0(w[100], w[33], w[3], pars->GC_10, amp[424]); 
  VVV1_0(w[101], w[33], w[3], pars->GC_10, amp[425]); 
  VVV1_0(w[99], w[1], w[15], pars->GC_10, amp[426]); 
  VVV1_0(w[100], w[1], w[15], pars->GC_10, amp[427]); 
  VVV1_0(w[101], w[1], w[15], pars->GC_10, amp[428]); 
  VVVV1_0(w[1], w[3], w[4], w[99], pars->GC_12, amp[429]); 
  VVVV3_0(w[1], w[3], w[4], w[99], pars->GC_12, amp[430]); 
  VVVV4_0(w[1], w[3], w[4], w[99], pars->GC_12, amp[431]); 
  VVVV1_0(w[1], w[3], w[4], w[100], pars->GC_12, amp[432]); 
  VVVV3_0(w[1], w[3], w[4], w[100], pars->GC_12, amp[433]); 
  VVVV4_0(w[1], w[3], w[4], w[100], pars->GC_12, amp[434]); 
  VVVV1_0(w[1], w[3], w[4], w[101], pars->GC_12, amp[435]); 
  VVVV3_0(w[1], w[3], w[4], w[101], pars->GC_12, amp[436]); 
  VVVV4_0(w[1], w[3], w[4], w[101], pars->GC_12, amp[437]); 
  VVV1_0(w[102], w[47], w[5], pars->GC_10, amp[438]); 
  VVV1_0(w[103], w[47], w[5], pars->GC_10, amp[439]); 
  VVV1_0(w[104], w[47], w[5], pars->GC_10, amp[440]); 
  VVV1_0(w[102], w[35], w[2], pars->GC_10, amp[441]); 
  VVV1_0(w[103], w[35], w[2], pars->GC_10, amp[442]); 
  VVV1_0(w[104], w[35], w[2], pars->GC_10, amp[443]); 
  VVV1_0(w[102], w[1], w[14], pars->GC_10, amp[444]); 
  VVV1_0(w[103], w[1], w[14], pars->GC_10, amp[445]); 
  VVV1_0(w[104], w[1], w[14], pars->GC_10, amp[446]); 
  VVVV1_0(w[1], w[2], w[5], w[102], pars->GC_12, amp[447]); 
  VVVV3_0(w[1], w[2], w[5], w[102], pars->GC_12, amp[448]); 
  VVVV4_0(w[1], w[2], w[5], w[102], pars->GC_12, amp[449]); 
  VVVV1_0(w[1], w[2], w[5], w[103], pars->GC_12, amp[450]); 
  VVVV3_0(w[1], w[2], w[5], w[103], pars->GC_12, amp[451]); 
  VVVV4_0(w[1], w[2], w[5], w[103], pars->GC_12, amp[452]); 
  VVVV1_0(w[1], w[2], w[5], w[104], pars->GC_12, amp[453]); 
  VVVV3_0(w[1], w[2], w[5], w[104], pars->GC_12, amp[454]); 
  VVVV4_0(w[1], w[2], w[5], w[104], pars->GC_12, amp[455]); 
  VVV1_0(w[105], w[47], w[4], pars->GC_10, amp[456]); 
  VVV1_0(w[106], w[47], w[4], pars->GC_10, amp[457]); 
  VVV1_0(w[107], w[47], w[4], pars->GC_10, amp[458]); 
  VVV1_0(w[105], w[33], w[2], pars->GC_10, amp[459]); 
  VVV1_0(w[106], w[33], w[2], pars->GC_10, amp[460]); 
  VVV1_0(w[107], w[33], w[2], pars->GC_10, amp[461]); 
  VVV1_0(w[105], w[1], w[11], pars->GC_10, amp[462]); 
  VVV1_0(w[106], w[1], w[11], pars->GC_10, amp[463]); 
  VVV1_0(w[107], w[1], w[11], pars->GC_10, amp[464]); 
  VVVV1_0(w[1], w[2], w[4], w[105], pars->GC_12, amp[465]); 
  VVVV3_0(w[1], w[2], w[4], w[105], pars->GC_12, amp[466]); 
  VVVV4_0(w[1], w[2], w[4], w[105], pars->GC_12, amp[467]); 
  VVVV1_0(w[1], w[2], w[4], w[106], pars->GC_12, amp[468]); 
  VVVV3_0(w[1], w[2], w[4], w[106], pars->GC_12, amp[469]); 
  VVVV4_0(w[1], w[2], w[4], w[106], pars->GC_12, amp[470]); 
  VVVV1_0(w[1], w[2], w[4], w[107], pars->GC_12, amp[471]); 
  VVVV3_0(w[1], w[2], w[4], w[107], pars->GC_12, amp[472]); 
  VVVV4_0(w[1], w[2], w[4], w[107], pars->GC_12, amp[473]); 
  VVV1_0(w[108], w[47], w[3], pars->GC_10, amp[474]); 
  VVV1_0(w[109], w[47], w[3], pars->GC_10, amp[475]); 
  VVV1_0(w[110], w[47], w[3], pars->GC_10, amp[476]); 
  VVV1_0(w[108], w[30], w[2], pars->GC_10, amp[477]); 
  VVV1_0(w[109], w[30], w[2], pars->GC_10, amp[478]); 
  VVV1_0(w[110], w[30], w[2], pars->GC_10, amp[479]); 
  VVV1_0(w[108], w[1], w[7], pars->GC_10, amp[480]); 
  VVV1_0(w[109], w[1], w[7], pars->GC_10, amp[481]); 
  VVV1_0(w[110], w[1], w[7], pars->GC_10, amp[482]); 
  VVVV1_0(w[1], w[2], w[3], w[108], pars->GC_12, amp[483]); 
  VVVV3_0(w[1], w[2], w[3], w[108], pars->GC_12, amp[484]); 
  VVVV4_0(w[1], w[2], w[3], w[108], pars->GC_12, amp[485]); 
  VVVV1_0(w[1], w[2], w[3], w[109], pars->GC_12, amp[486]); 
  VVVV3_0(w[1], w[2], w[3], w[109], pars->GC_12, amp[487]); 
  VVVV4_0(w[1], w[2], w[3], w[109], pars->GC_12, amp[488]); 
  VVVV1_0(w[1], w[2], w[3], w[110], pars->GC_12, amp[489]); 
  VVVV3_0(w[1], w[2], w[3], w[110], pars->GC_12, amp[490]); 
  VVVV4_0(w[1], w[2], w[3], w[110], pars->GC_12, amp[491]); 
  VVV1_0(w[0], w[63], w[10], pars->GC_10, amp[492]); 
  VVV1_0(w[0], w[64], w[10], pars->GC_10, amp[493]); 
  VVV1_0(w[0], w[65], w[10], pars->GC_10, amp[494]); 
  VVV1_0(w[0], w[52], w[13], pars->GC_10, amp[495]); 
  VVV1_0(w[0], w[53], w[13], pars->GC_10, amp[496]); 
  VVV1_0(w[0], w[54], w[13], pars->GC_10, amp[497]); 
  VVV1_0(w[0], w[55], w[15], pars->GC_10, amp[498]); 
  VVV1_0(w[0], w[56], w[15], pars->GC_10, amp[499]); 
  VVV1_0(w[0], w[57], w[15], pars->GC_10, amp[500]); 
  VVV1_0(w[0], w[37], w[14], pars->GC_10, amp[501]); 
  VVV1_0(w[0], w[38], w[14], pars->GC_10, amp[502]); 
  VVV1_0(w[0], w[39], w[14], pars->GC_10, amp[503]); 
  VVV1_0(w[0], w[40], w[11], pars->GC_10, amp[504]); 
  VVV1_0(w[0], w[41], w[11], pars->GC_10, amp[505]); 
  VVV1_0(w[0], w[42], w[11], pars->GC_10, amp[506]); 
  VVV1_0(w[0], w[43], w[7], pars->GC_10, amp[507]); 
  VVV1_0(w[0], w[44], w[7], pars->GC_10, amp[508]); 
  VVV1_0(w[0], w[45], w[7], pars->GC_10, amp[509]); 


}
double PY8MEs_R19_P25_sm_gg_gggg::matrix_19_gg_gggg() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 510;
  const int ncolor = 120; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {648, 648, 648, 648, 648, 648, 648, 648,
      648, 648, 648, 648, 648, 648, 648, 648, 648, 648, 648, 648, 648, 648,
      648, 648, 648, 648, 648, 648, 648, 648, 648, 648, 648, 648, 648, 648,
      648, 648, 648, 648, 648, 648, 648, 648, 648, 648, 648, 648, 648, 648,
      648, 648, 648, 648, 648, 648, 648, 648, 648, 648, 648, 648, 648, 648,
      648, 648, 648, 648, 648, 648, 648, 648, 648, 648, 648, 648, 648, 648,
      648, 648, 648, 648, 648, 648, 648, 648, 648, 648, 648, 648, 648, 648,
      648, 648, 648, 648, 648, 648, 648, 648, 648, 648, 648, 648, 648, 648,
      648, 648, 648, 648, 648, 648, 648, 648, 648, 648, 648, 648, 648, 648};
  static const double cf[ncolor][ncolor] = {{3641, -454, -454, 50, 50, 572,
      -454, 68, 50, 50, -13, -76, 50, -13, 572, -76, 554, 428, 50, -76, -76,
      428, 428, -184, -454, 68, 68, -76, -76, -40, 50, -76, 50, -454, -13, 68,
      -13, 5, -76, 68, -13, -76, -13, -58, 5, -76, -58, -40, 50, -76, -13, -13,
      5, -58, 572, -40, -76, 68, -58, 104, 554, -13, 428, -76, 50, 572, -13, 5,
      -58, -58, -76, -40, 50, -13, -76, 5, -13, -58, -76, -58, 428, -76, 5,
      -58, 428, -58, -184, -40, -76, -40, 50, -76, -76, 428, 428, -184, -454,
      68, 68, -76, -76, -40, 68, 104, -76, 572, -58, -40, -76, -58, -40, -40,
      428, -184, 572, -40, -40, -184, -184, 176}, {-454, 3641, 50, 572, -454,
      50, 68, -454, -13, -76, 50, 50, 50, -76, -76, 428, 428, -184, 50, -13,
      572, -76, 554, 428, 68, -454, -76, -40, 68, -76, -76, 50, -13, 68, 50,
      -454, -13, -58, 5, -76, -58, -40, -13, 5, -76, 68, -13, -76, -76, 50, 5,
      -58, -13, -13, -40, 572, -58, 104, -76, 68, -13, 5, -58, -58, -76, -40,
      554, -13, 428, -76, 50, 572, -454, 68, 68, -76, -76, -40, 68, 104, -76,
      572, -58, -40, -76, -58, -40, -40, 428, -184, 572, -40, -40, -184, -184,
      176, 50, -13, -76, 5, -13, -58, -76, -58, 428, -76, 5, -58, 428, -58,
      -184, -40, -76, -40, 50, -76, -76, 428, 428, -184}, {-454, 50, 3641,
      -454, 572, 50, 50, -13, 572, -76, 554, 428, -454, 68, 50, 50, -13, -76,
      -76, 50, 428, -184, -76, 428, 68, -76, -454, 68, -40, -76, -13, 5, -76,
      68, -13, -76, 50, -76, 50, -454, -13, 68, -58, -13, -58, -40, 5, -76, 50,
      -13, -76, 5, -13, -58, -76, -58, 428, -76, 5, -58, 428, -58, -184, -40,
      -76, -40, 50, -76, -76, 428, 428, -184, 50, -76, -13, -13, 5, -58, 572,
      -40, -76, 68, -58, 104, 554, -13, 428, -76, 50, 572, -13, 5, -58, -58,
      -76, -40, 68, -454, -76, -40, 68, -76, 104, 68, -58, -40, -76, 572, 572,
      -40, -40, -184, -184, 176, -76, -58, -40, -40, 428, -184}, {50, 572,
      -454, 3641, 50, -454, 50, -76, -76, 428, 428, -184, 68, -454, -13, -76,
      50, 50, -13, 50, 554, 428, 572, -76, -76, -40, 68, -454, -76, 68, -13,
      -58, 5, -76, -58, -40, -76, 50, -13, 68, 50, -454, 5, -13, -13, -76, -76,
      68, -454, 68, 68, -76, -76, -40, 68, 104, -76, 572, -58, -40, -76, -58,
      -40, -40, 428, -184, 572, -40, -40, -184, -184, 176, -76, 50, 5, -58,
      -13, -13, -40, 572, -58, 104, -76, 68, -13, 5, -58, -58, -76, -40, 554,
      -13, 428, -76, 50, 572, -13, 50, -13, -58, -76, 5, -58, -76, 5, -58, 428,
      -76, 50, -76, -76, 428, 428, -184, 428, -58, -184, -40, -76, -40}, {50,
      -454, 572, 50, 3641, -454, -13, 50, 554, 428, 572, -76, -76, 50, 428,
      -184, -76, 428, -454, 68, 50, 50, -13, -76, -76, 68, -40, -76, -454, 68,
      5, -13, -13, -76, -76, 68, -58, -13, -58, -40, 5, -76, 50, -76, 50, -454,
      -13, 68, -13, 50, -13, -58, -76, 5, -58, -76, 5, -58, 428, -76, 50, -76,
      -76, 428, 428, -184, 428, -58, -184, -40, -76, -40, 68, -454, -76, -40,
      68, -76, 104, 68, -58, -40, -76, 572, 572, -40, -40, -184, -184, 176,
      -76, -58, -40, -40, 428, -184, 50, -76, -13, -13, 5, -58, 572, -40, -76,
      68, -58, 104, 554, -13, 428, -76, 50, 572, -13, 5, -58, -58, -76, -40},
      {572, 50, 50, -454, -454, 3641, -76, 50, 428, -184, -76, 428, -13, 50,
      554, 428, 572, -76, 68, -454, -13, -76, 50, 50, -40, -76, -76, 68, 68,
      -454, -58, -13, -58, -40, 5, -76, 5, -13, -13, -76, -76, 68, -76, 50,
      -13, 68, 50, -454, 68, -454, -76, -40, 68, -76, 104, 68, -58, -40, -76,
      572, 572, -40, -40, -184, -184, 176, -76, -58, -40, -40, 428, -184, -13,
      50, -13, -58, -76, 5, -58, -76, 5, -58, 428, -76, 50, -76, -76, 428, 428,
      -184, 428, -58, -184, -40, -76, -40, -76, 50, 5, -58, -13, -13, -40, 572,
      -58, 104, -76, 68, -13, 5, -58, -58, -76, -40, 554, -13, 428, -76, 50,
      572}, {-454, 68, 50, 50, -13, -76, 3641, -454, -454, 50, 50, 572, 572,
      -76, 50, -13, 428, 554, -76, 428, 50, -76, -184, 428, 50, -76, -13, -13,
      5, -58, 572, -40, -76, 68, -58, 104, 554, -13, 428, -76, 50, 572, -13, 5,
      -58, -58, -76, -40, -454, 68, 68, -76, -76, -40, 50, -76, 50, -454, -13,
      68, -13, 5, -76, 68, -13, -76, -13, -58, 5, -76, -58, -40, -76, 5, 50,
      -13, -58, -13, 428, -58, -184, -40, -76, -40, -76, -58, 428, -76, 5, -58,
      -76, 50, 428, -184, -76, 428, 68, -76, -454, 68, -40, -76, -76, -58, -40,
      -40, 428, -184, 68, 104, -76, 572, -58, -40, -40, 572, -184, 176, -40,
      -184}, {68, -454, -13, -76, 50, 50, -454, 3641, 50, 572, -454, 50, -76,
      428, 50, -76, -184, 428, 572, -76, 50, -13, 428, 554, -76, 50, 5, -58,
      -13, -13, -40, 572, -58, 104, -76, 68, -13, 5, -58, -58, -76, -40, 554,
      -13, 428, -76, 50, 572, 68, -454, -76, -40, 68, -76, -76, 50, -13, 68,
      50, -454, -13, -58, 5, -76, -58, -40, -13, 5, -76, 68, -13, -76, 68, -76,
      -454, 68, -40, -76, -76, -58, -40, -40, 428, -184, 68, 104, -76, 572,
      -58, -40, -40, 572, -184, 176, -40, -184, -76, 5, 50, -13, -58, -13, 428,
      -58, -184, -40, -76, -40, -76, -58, 428, -76, 5, -58, -76, 50, 428, -184,
      -76, 428}, {50, -13, 572, -76, 554, 428, -454, 50, 3641, -454, 572, 50,
      50, 50, -454, 68, -76, -13, 428, -184, -76, 50, 428, -76, 50, -13, -76,
      5, -13, -58, -76, -58, 428, -76, 5, -58, 428, -58, -184, -40, -76, -40,
      50, -76, -76, 428, 428, -184, 68, -76, -454, 68, -40, -76, -13, 5, -76,
      68, -13, -76, 50, -76, 50, -454, -13, 68, -58, -13, -58, -40, 5, -76,
      -13, -13, 50, -76, -58, 5, 554, -13, 428, -76, 50, 572, 572, -40, -76,
      68, -58, 104, 5, -13, -76, -40, -58, -58, -76, -40, 68, -454, -76, 68,
      572, -40, -40, -184, -184, 176, 104, 68, -58, -40, -76, 572, -58, -76,
      428, -184, -40, -40}, {50, -76, -76, 428, 428, -184, 50, 572, -454, 3641,
      50, -454, -13, -76, 68, -454, 50, 50, 554, 428, -13, 50, -76, 572, -454,
      68, 68, -76, -76, -40, 68, 104, -76, 572, -58, -40, -76, -58, -40, -40,
      428, -184, 572, -40, -40, -184, -184, 176, -76, -40, 68, -454, -76, 68,
      -13, -58, 5, -76, -58, -40, -76, 50, -13, 68, 50, -454, 5, -13, -13, -76,
      -76, 68, 5, -58, -76, 50, -13, -13, -13, 5, -58, -58, -76, -40, -40, 572,
      -58, 104, -76, 68, -13, 554, 50, 572, 428, -76, -13, -58, -13, 50, 5,
      -76, 50, -76, -76, 428, 428, -184, -58, -76, 5, -58, 428, -76, -58, 428,
      -76, -40, -184, -40}, {-13, 50, 554, 428, 572, -76, 50, -454, 572, 50,
      3641, -454, 428, -184, -76, 50, 428, -76, 50, 50, -454, 68, -76, -13,
      -13, 50, -13, -58, -76, 5, -58, -76, 5, -58, 428, -76, 50, -76, -76, 428,
      428, -184, 428, -58, -184, -40, -76, -40, -76, 68, -40, -76, -454, 68, 5,
      -13, -13, -76, -76, 68, -58, -13, -58, -40, 5, -76, 50, -76, 50, -454,
      -13, 68, -76, -40, 68, -454, -76, 68, 572, -40, -40, -184, -184, 176,
      104, 68, -58, -40, -76, 572, -58, -76, 428, -184, -40, -40, -13, -13, 50,
      -76, -58, 5, 554, -13, 428, -76, 50, 572, 572, -40, -76, 68, -58, 104, 5,
      -13, -76, -40, -58, -58}, {-76, 50, 428, -184, -76, 428, 572, 50, 50,
      -454, -454, 3641, 554, 428, -13, 50, -76, 572, -13, -76, 68, -454, 50,
      50, 68, -454, -76, -40, 68, -76, 104, 68, -58, -40, -76, 572, 572, -40,
      -40, -184, -184, 176, -76, -58, -40, -40, 428, -184, -40, -76, -76, 68,
      68, -454, -58, -13, -58, -40, 5, -76, 5, -13, -13, -76, -76, 68, -76, 50,
      -13, 68, 50, -454, -13, -58, -13, 50, 5, -76, 50, -76, -76, 428, 428,
      -184, -58, -76, 5, -58, 428, -76, -58, 428, -76, -40, -184, -40, 5, -58,
      -76, 50, -13, -13, -13, 5, -58, -58, -76, -40, -40, 572, -58, 104, -76,
      68, -13, 554, 50, 572, 428, -76}, {50, 50, -454, 68, -76, -13, 572, -76,
      50, -13, 428, 554, 3641, -454, -454, 50, 50, 572, 428, -76, -184, 428,
      50, -76, -13, -13, 50, -76, -58, 5, 554, -13, 428, -76, 50, 572, 572,
      -40, -76, 68, -58, 104, 5, -13, -76, -40, -58, -58, -76, 5, 50, -13, -58,
      -13, 428, -58, -184, -40, -76, -40, -76, -58, 428, -76, 5, -58, -76, 50,
      428, -184, -76, 428, -454, 68, 68, -76, -76, -40, 50, -76, 50, -454, -13,
      68, -13, 5, -76, 68, -13, -76, -13, -58, 5, -76, -58, -40, -76, 68, -40,
      -76, -454, 68, -58, -76, 428, -184, -40, -40, -40, 572, -184, 176, -40,
      -184, 68, 104, -76, 572, -58, -40}, {-13, -76, 68, -454, 50, 50, -76,
      428, 50, -76, -184, 428, -454, 3641, 50, 572, -454, 50, -76, 572, 428,
      554, 50, -13, 5, -58, -76, 50, -13, -13, -13, 5, -58, -58, -76, -40, -40,
      572, -58, 104, -76, 68, -13, 554, 50, 572, 428, -76, 68, -76, -454, 68,
      -40, -76, -76, -58, -40, -40, 428, -184, 68, 104, -76, 572, -58, -40,
      -40, 572, -184, 176, -40, -184, 68, -454, -76, -40, 68, -76, -76, 50,
      -13, 68, 50, -454, -13, -58, 5, -76, -58, -40, -13, 5, -76, 68, -13, -76,
      5, -76, -58, -13, 50, -13, -58, 428, -76, -40, -184, -40, -76, 50, 428,
      -184, -76, 428, -76, -58, 428, -76, 5, -58}, {572, -76, 50, -13, 428,
      554, 50, 50, -454, 68, -76, -13, -454, 50, 3641, -454, 572, 50, -184,
      428, 428, -76, -76, 50, -76, 5, 50, -13, -58, -13, 428, -58, -184, -40,
      -76, -40, -76, -58, 428, -76, 5, -58, -76, 50, 428, -184, -76, 428, -13,
      -13, 50, -76, -58, 5, 554, -13, 428, -76, 50, 572, 572, -40, -76, 68,
      -58, 104, 5, -13, -76, -40, -58, -58, 68, -76, -454, 68, -40, -76, -13,
      5, -76, 68, -13, -76, 50, -76, 50, -454, -13, 68, -58, -13, -58, -40, 5,
      -76, -40, -76, -76, 68, 68, -454, -40, 572, -184, 176, -40, -184, -58,
      -76, 428, -184, -40, -40, 104, 68, -58, -40, -76, 572}, {-76, 428, 50,
      -76, -184, 428, -13, -76, 68, -454, 50, 50, 50, 572, -454, 3641, 50,
      -454, 428, 554, -76, 572, -13, 50, 68, -76, -454, 68, -40, -76, -76, -58,
      -40, -40, 428, -184, 68, 104, -76, 572, -58, -40, -40, 572, -184, 176,
      -40, -184, 5, -58, -76, 50, -13, -13, -13, 5, -58, -58, -76, -40, -40,
      572, -58, 104, -76, 68, -13, 554, 50, 572, 428, -76, -76, -40, 68, -454,
      -76, 68, -13, -58, 5, -76, -58, -40, -76, 50, -13, 68, 50, -454, 5, -13,
      -13, -76, -76, 68, -58, -13, 5, -76, -13, 50, -76, 50, 428, -184, -76,
      428, -58, 428, -76, -40, -184, -40, -58, -76, 5, -58, 428, -76}, {554,
      428, -13, 50, -76, 572, 428, -184, -76, 50, 428, -76, 50, -454, 572, 50,
      3641, -454, 50, 50, -76, -13, -454, 68, -13, -58, -13, 50, 5, -76, 50,
      -76, -76, 428, 428, -184, -58, -76, 5, -58, 428, -76, -58, 428, -76, -40,
      -184, -40, -76, -40, 68, -454, -76, 68, 572, -40, -40, -184, -184, 176,
      104, 68, -58, -40, -76, 572, -58, -76, 428, -184, -40, -40, -76, 68, -40,
      -76, -454, 68, 5, -13, -13, -76, -76, 68, -58, -13, -58, -40, 5, -76, 50,
      -76, 50, -454, -13, 68, -13, -13, -58, 5, 50, -76, -13, 554, 50, 572,
      428, -76, 5, -13, -76, -40, -58, -58, 572, -40, -76, 68, -58, 104}, {428,
      -184, -76, 50, 428, -76, 554, 428, -13, 50, -76, 572, 572, 50, 50, -454,
      -454, 3641, -76, -13, 50, 50, 68, -454, -76, -40, 68, -454, -76, 68, 572,
      -40, -40, -184, -184, 176, 104, 68, -58, -40, -76, 572, -58, -76, 428,
      -184, -40, -40, -13, -58, -13, 50, 5, -76, 50, -76, -76, 428, 428, -184,
      -58, -76, 5, -58, 428, -76, -58, 428, -76, -40, -184, -40, -40, -76, -76,
      68, 68, -454, -58, -13, -58, -40, 5, -76, 5, -13, -13, -76, -76, 68, -76,
      50, -13, 68, 50, -454, -58, 5, -13, -13, -76, 50, 5, -13, -76, -40, -58,
      -58, -13, 554, 50, 572, 428, -76, -40, 572, -58, 104, -76, 68}, {50, 50,
      -76, -13, -454, 68, -76, 572, 428, 554, 50, -13, 428, -76, -184, 428, 50,
      -76, 3641, -454, -454, 50, 50, 572, -13, -13, -58, 5, 50, -76, -13, 554,
      50, 572, 428, -76, 5, -13, -76, -40, -58, -58, 572, -40, -76, 68, -58,
      104, 5, -76, -58, -13, 50, -13, -58, 428, -76, -40, -184, -40, -76, 50,
      428, -184, -76, 428, -76, -58, 428, -76, 5, -58, -76, 68, -40, -76, -454,
      68, -58, -76, 428, -184, -40, -40, -40, 572, -184, 176, -40, -184, 68,
      104, -76, 572, -58, -40, -454, 68, 68, -76, -76, -40, 50, -76, 50, -454,
      -13, 68, -13, 5, -76, 68, -13, -76, -13, -58, 5, -76, -58, -40}, {-76,
      -13, 50, 50, 68, -454, 428, -76, -184, 428, 50, -76, -76, 572, 428, 554,
      50, -13, -454, 3641, 50, 572, -454, 50, -58, 5, -13, -13, -76, 50, 5,
      -13, -76, -40, -58, -58, -13, 554, 50, 572, 428, -76, -40, 572, -58, 104,
      -76, 68, -76, 68, -40, -76, -454, 68, -58, -76, 428, -184, -40, -40, -40,
      572, -184, 176, -40, -184, 68, 104, -76, 572, -58, -40, 5, -76, -58, -13,
      50, -13, -58, 428, -76, -40, -184, -40, -76, 50, 428, -184, -76, 428,
      -76, -58, 428, -76, 5, -58, 68, -454, -76, -40, 68, -76, -76, 50, -13,
      68, 50, -454, -13, -58, 5, -76, -58, -40, -13, 5, -76, 68, -13, -76},
      {-76, 572, 428, 554, 50, -13, 50, 50, -76, -13, -454, 68, -184, 428, 428,
      -76, -76, 50, -454, 50, 3641, -454, 572, 50, 5, -76, -58, -13, 50, -13,
      -58, 428, -76, -40, -184, -40, -76, 50, 428, -184, -76, 428, -76, -58,
      428, -76, 5, -58, -13, -13, -58, 5, 50, -76, -13, 554, 50, 572, 428, -76,
      5, -13, -76, -40, -58, -58, 572, -40, -76, 68, -58, 104, -40, -76, -76,
      68, 68, -454, -40, 572, -184, 176, -40, -184, -58, -76, 428, -184, -40,
      -40, 104, 68, -58, -40, -76, 572, 68, -76, -454, 68, -40, -76, -13, 5,
      -76, 68, -13, -76, 50, -76, 50, -454, -13, 68, -58, -13, -58, -40, 5,
      -76}, {428, -76, -184, 428, 50, -76, -76, -13, 50, 50, 68, -454, 428,
      554, -76, 572, -13, 50, 50, 572, -454, 3641, 50, -454, -76, 68, -40, -76,
      -454, 68, -58, -76, 428, -184, -40, -40, -40, 572, -184, 176, -40, -184,
      68, 104, -76, 572, -58, -40, -58, 5, -13, -13, -76, 50, 5, -13, -76, -40,
      -58, -58, -13, 554, 50, 572, 428, -76, -40, 572, -58, 104, -76, 68, -58,
      -13, 5, -76, -13, 50, -76, 50, 428, -184, -76, 428, -58, 428, -76, -40,
      -184, -40, -58, -76, 5, -58, 428, -76, -76, -40, 68, -454, -76, 68, -13,
      -58, 5, -76, -58, -40, -76, 50, -13, 68, 50, -454, 5, -13, -13, -76, -76,
      68}, {428, 554, -76, 572, -13, 50, -184, 428, 428, -76, -76, 50, 50, 50,
      -76, -13, -454, 68, 50, -454, 572, 50, 3641, -454, -58, -13, 5, -76, -13,
      50, -76, 50, 428, -184, -76, 428, -58, 428, -76, -40, -184, -40, -58,
      -76, 5, -58, 428, -76, -40, -76, -76, 68, 68, -454, -40, 572, -184, 176,
      -40, -184, -58, -76, 428, -184, -40, -40, 104, 68, -58, -40, -76, 572,
      -13, -13, -58, 5, 50, -76, -13, 554, 50, 572, 428, -76, 5, -13, -76, -40,
      -58, -58, 572, -40, -76, 68, -58, 104, -76, 68, -40, -76, -454, 68, 5,
      -13, -13, -76, -76, 68, -58, -13, -58, -40, 5, -76, 50, -76, 50, -454,
      -13, 68}, {-184, 428, 428, -76, -76, 50, 428, 554, -76, 572, -13, 50,
      -76, -13, 50, 50, 68, -454, 572, 50, 50, -454, -454, 3641, -40, -76, -76,
      68, 68, -454, -40, 572, -184, 176, -40, -184, -58, -76, 428, -184, -40,
      -40, 104, 68, -58, -40, -76, 572, -58, -13, 5, -76, -13, 50, -76, 50,
      428, -184, -76, 428, -58, 428, -76, -40, -184, -40, -58, -76, 5, -58,
      428, -76, -58, 5, -13, -13, -76, 50, 5, -13, -76, -40, -58, -58, -13,
      554, 50, 572, 428, -76, -40, 572, -58, 104, -76, 68, -40, -76, -76, 68,
      68, -454, -58, -13, -58, -40, 5, -76, 5, -13, -13, -76, -76, 68, -76, 50,
      -13, 68, 50, -454}, {-454, 68, 68, -76, -76, -40, 50, -76, 50, -454, -13,
      68, -13, 5, -76, 68, -13, -76, -13, -58, 5, -76, -58, -40, 3641, -454,
      -454, 50, 50, 572, -454, 68, 50, 50, -13, -76, 50, -13, 572, -76, 554,
      428, 50, -76, -76, 428, 428, -184, 572, -40, -76, 68, -58, 104, 50, -76,
      -13, -13, 5, -58, 428, -76, 554, -13, 572, 50, -58, -58, -13, 5, -40,
      -76, -76, -58, 428, -76, 5, -58, 50, -13, -76, 5, -13, -58, -184, -40,
      428, -58, -40, -76, -76, 428, 50, -76, -184, 428, 68, 104, -76, 572, -58,
      -40, -454, 68, 68, -76, -76, -40, -40, -40, -76, -58, -184, 428, -40,
      -184, 572, -40, 176, -184}, {68, -454, -76, -40, 68, -76, -76, 50, -13,
      68, 50, -454, -13, -58, 5, -76, -58, -40, -13, 5, -76, 68, -13, -76,
      -454, 3641, 50, 572, -454, 50, 68, -454, -13, -76, 50, 50, 50, -76, -76,
      428, 428, -184, 50, -13, 572, -76, 554, 428, -40, 572, -58, 104, -76, 68,
      -76, 50, 5, -58, -13, -13, -58, -58, -13, 5, -40, -76, 428, -76, 554,
      -13, 572, 50, 68, 104, -76, 572, -58, -40, -454, 68, 68, -76, -76, -40,
      -40, -40, -76, -58, -184, 428, -40, -184, 572, -40, 176, -184, -76, -58,
      428, -76, 5, -58, 50, -13, -76, 5, -13, -58, -184, -40, 428, -58, -40,
      -76, -76, 428, 50, -76, -184, 428}, {68, -76, -454, 68, -40, -76, -13, 5,
      -76, 68, -13, -76, 50, -76, 50, -454, -13, 68, -58, -13, -58, -40, 5,
      -76, -454, 50, 3641, -454, 572, 50, 50, -13, 572, -76, 554, 428, -454,
      68, 50, 50, -13, -76, -76, 50, 428, -184, -76, 428, -76, -58, 428, -76,
      5, -58, 50, -13, -76, 5, -13, -58, -184, -40, 428, -58, -40, -76, -76,
      428, 50, -76, -184, 428, 572, -40, -76, 68, -58, 104, 50, -76, -13, -13,
      5, -58, 428, -76, 554, -13, 572, 50, -58, -58, -13, 5, -40, -76, 104, 68,
      -58, -40, -76, 572, 68, -454, -76, -40, 68, -76, -40, -184, 572, -40,
      176, -184, -40, -40, -76, -58, -184, 428}, {-76, -40, 68, -454, -76, 68,
      -13, -58, 5, -76, -58, -40, -76, 50, -13, 68, 50, -454, 5, -13, -13, -76,
      -76, 68, 50, 572, -454, 3641, 50, -454, 50, -76, -76, 428, 428, -184, 68,
      -454, -13, -76, 50, 50, -13, 50, 554, 428, 572, -76, 68, 104, -76, 572,
      -58, -40, -454, 68, 68, -76, -76, -40, -40, -40, -76, -58, -184, 428,
      -40, -184, 572, -40, 176, -184, -40, 572, -58, 104, -76, 68, -76, 50, 5,
      -58, -13, -13, -58, -58, -13, 5, -40, -76, 428, -76, 554, -13, 572, 50,
      -58, -76, 5, -58, 428, -76, -13, 50, -13, -58, -76, 5, -76, 428, 50, -76,
      -184, 428, -184, -40, 428, -58, -40, -76}, {-76, 68, -40, -76, -454, 68,
      5, -13, -13, -76, -76, 68, -58, -13, -58, -40, 5, -76, 50, -76, 50, -454,
      -13, 68, 50, -454, 572, 50, 3641, -454, -13, 50, 554, 428, 572, -76, -76,
      50, 428, -184, -76, 428, -454, 68, 50, 50, -13, -76, -58, -76, 5, -58,
      428, -76, -13, 50, -13, -58, -76, 5, -76, 428, 50, -76, -184, 428, -184,
      -40, 428, -58, -40, -76, 104, 68, -58, -40, -76, 572, 68, -454, -76, -40,
      68, -76, -40, -184, 572, -40, 176, -184, -40, -40, -76, -58, -184, 428,
      572, -40, -76, 68, -58, 104, 50, -76, -13, -13, 5, -58, 428, -76, 554,
      -13, 572, 50, -58, -58, -13, 5, -40, -76}, {-40, -76, -76, 68, 68, -454,
      -58, -13, -58, -40, 5, -76, 5, -13, -13, -76, -76, 68, -76, 50, -13, 68,
      50, -454, 572, 50, 50, -454, -454, 3641, -76, 50, 428, -184, -76, 428,
      -13, 50, 554, 428, 572, -76, 68, -454, -13, -76, 50, 50, 104, 68, -58,
      -40, -76, 572, 68, -454, -76, -40, 68, -76, -40, -184, 572, -40, 176,
      -184, -40, -40, -76, -58, -184, 428, -58, -76, 5, -58, 428, -76, -13, 50,
      -13, -58, -76, 5, -76, 428, 50, -76, -184, 428, -184, -40, 428, -58, -40,
      -76, -40, 572, -58, 104, -76, 68, -76, 50, 5, -58, -13, -13, -58, -58,
      -13, 5, -40, -76, 428, -76, 554, -13, 572, 50}, {50, -76, -13, -13, 5,
      -58, 572, -40, -76, 68, -58, 104, 554, -13, 428, -76, 50, 572, -13, 5,
      -58, -58, -76, -40, -454, 68, 50, 50, -13, -76, 3641, -454, -454, 50, 50,
      572, 572, -76, 50, -13, 428, 554, -76, 428, 50, -76, -184, 428, 50, -76,
      50, -454, -13, 68, -454, 68, 68, -76, -76, -40, -76, 68, -13, 5, -76,
      -13, 5, -76, -13, -58, -40, -58, 428, -58, -184, -40, -76, -40, -76, 5,
      50, -13, -58, -13, 428, -76, -76, -58, -58, 5, 428, -184, -76, 50, 428,
      -76, -76, -58, -40, -40, 428, -184, 68, -76, -454, 68, -40, -76, -76,
      572, 68, 104, -40, -58, -184, 176, -40, 572, -184, -40}, {-76, 50, 5,
      -58, -13, -13, -40, 572, -58, 104, -76, 68, -13, 5, -58, -58, -76, -40,
      554, -13, 428, -76, 50, 572, 68, -454, -13, -76, 50, 50, -454, 3641, 50,
      572, -454, 50, -76, 428, 50, -76, -184, 428, 572, -76, 50, -13, 428, 554,
      -76, 50, -13, 68, 50, -454, 68, -454, -76, -40, 68, -76, 5, -76, -13,
      -58, -40, -58, -76, 68, -13, 5, -76, -13, -76, -58, -40, -40, 428, -184,
      68, -76, -454, 68, -40, -76, -76, 572, 68, 104, -40, -58, -184, 176, -40,
      572, -184, -40, 428, -58, -184, -40, -76, -40, -76, 5, 50, -13, -58, -13,
      428, -76, -76, -58, -58, 5, 428, -184, -76, 50, 428, -76}, {50, -13, -76,
      5, -13, -58, -76, -58, 428, -76, 5, -58, 428, -58, -184, -40, -76, -40,
      50, -76, -76, 428, 428, -184, 50, -13, 572, -76, 554, 428, -454, 50,
      3641, -454, 572, 50, 50, 50, -454, 68, -76, -13, 428, -184, -76, 50, 428,
      -76, -13, 5, -76, 68, -13, -76, 68, -76, -454, 68, -40, -76, 50, -454,
      50, -76, 68, -13, -58, -40, -58, -13, -76, 5, 554, -13, 428, -76, 50,
      572, -13, -13, 50, -76, -58, 5, -76, 68, 572, -40, 104, -58, -76, -40, 5,
      -13, -58, -58, 572, -40, -40, -184, -184, 176, -76, -40, 68, -454, -76,
      68, -58, -40, 104, 68, 572, -76, 428, -184, -58, -76, -40, -40}, {-454,
      68, 68, -76, -76, -40, 68, 104, -76, 572, -58, -40, -76, -58, -40, -40,
      428, -184, 572, -40, -40, -184, -184, 176, 50, -76, -76, 428, 428, -184,
      50, 572, -454, 3641, 50, -454, -13, -76, 68, -454, 50, 50, 554, 428, -13,
      50, -76, 572, -13, -58, 5, -76, -58, -40, -76, -40, 68, -454, -76, 68,
      -13, 68, -76, 50, -454, 50, -13, -76, 5, -13, 68, -76, -13, 5, -58, -58,
      -76, -40, 5, -58, -76, 50, -13, -13, -58, 104, -40, 572, 68, -76, 50,
      572, -13, 554, -76, 428, 50, -76, -76, 428, 428, -184, -13, -58, -13, 50,
      5, -76, 5, -58, -58, -76, -76, 428, -76, -40, -58, 428, -40, -184}, {-13,
      50, -13, -58, -76, 5, -58, -76, 5, -58, 428, -76, 50, -76, -76, 428, 428,
      -184, 428, -58, -184, -40, -76, -40, -13, 50, 554, 428, 572, -76, 50,
      -454, 572, 50, 3641, -454, 428, -184, -76, 50, 428, -76, 50, 50, -454,
      68, -76, -13, 5, -13, -13, -76, -76, 68, -76, 68, -40, -76, -454, 68,
      -58, -40, -58, -13, -76, 5, 50, -454, 50, -76, 68, -13, 572, -40, -40,
      -184, -184, 176, -76, -40, 68, -454, -76, 68, -58, -40, 104, 68, 572,
      -76, 428, -184, -58, -76, -40, -40, 554, -13, 428, -76, 50, 572, -13,
      -13, 50, -76, -58, 5, -76, 68, 572, -40, 104, -58, -76, -40, 5, -13, -58,
      -58}, {68, -454, -76, -40, 68, -76, 104, 68, -58, -40, -76, 572, 572,
      -40, -40, -184, -184, 176, -76, -58, -40, -40, 428, -184, -76, 50, 428,
      -184, -76, 428, 572, 50, 50, -454, -454, 3641, 554, 428, -13, 50, -76,
      572, -13, -76, 68, -454, 50, 50, -58, -13, -58, -40, 5, -76, -40, -76,
      -76, 68, 68, -454, -13, -76, 5, -13, 68, -76, -13, 68, -76, 50, -454, 50,
      50, -76, -76, 428, 428, -184, -13, -58, -13, 50, 5, -76, 5, -58, -58,
      -76, -76, 428, -76, -40, -58, 428, -40, -184, -13, 5, -58, -58, -76, -40,
      5, -58, -76, 50, -13, -13, -58, 104, -40, 572, 68, -76, 50, 572, -13,
      554, -76, 428}, {-13, -13, 50, -76, -58, 5, 554, -13, 428, -76, 50, 572,
      572, -40, -76, 68, -58, 104, 5, -13, -76, -40, -58, -58, 50, 50, -454,
      68, -76, -13, 572, -76, 50, -13, 428, 554, 3641, -454, -454, 50, 50, 572,
      428, -76, -184, 428, 50, -76, 428, -58, -184, -40, -76, -40, -76, 5, 50,
      -13, -58, -13, 428, -76, -76, -58, -58, 5, 428, -184, -76, 50, 428, -76,
      50, -76, 50, -454, -13, 68, -454, 68, 68, -76, -76, -40, -76, 68, -13, 5,
      -76, -13, 5, -76, -13, -58, -40, -58, -58, -76, 428, -184, -40, -40, -76,
      68, -40, -76, -454, 68, -184, 176, -40, 572, -184, -40, -76, 572, 68,
      104, -40, -58}, {5, -58, -76, 50, -13, -13, -13, 5, -58, -58, -76, -40,
      -40, 572, -58, 104, -76, 68, -13, 554, 50, 572, 428, -76, -13, -76, 68,
      -454, 50, 50, -76, 428, 50, -76, -184, 428, -454, 3641, 50, 572, -454,
      50, -76, 572, 428, 554, 50, -13, -76, -58, -40, -40, 428, -184, 68, -76,
      -454, 68, -40, -76, -76, 572, 68, 104, -40, -58, -184, 176, -40, 572,
      -184, -40, -76, 50, -13, 68, 50, -454, 68, -454, -76, -40, 68, -76, 5,
      -76, -13, -58, -40, -58, -76, 68, -13, 5, -76, -13, -58, 428, -76, -40,
      -184, -40, 5, -76, -58, -13, 50, -13, 428, -184, -76, 50, 428, -76, 428,
      -76, -76, -58, -58, 5}, {-76, 5, 50, -13, -58, -13, 428, -58, -184, -40,
      -76, -40, -76, -58, 428, -76, 5, -58, -76, 50, 428, -184, -76, 428, 572,
      -76, 50, -13, 428, 554, 50, 50, -454, 68, -76, -13, -454, 50, 3641, -454,
      572, 50, -184, 428, 428, -76, -76, 50, 554, -13, 428, -76, 50, 572, -13,
      -13, 50, -76, -58, 5, -76, 68, 572, -40, 104, -58, -76, -40, 5, -13, -58,
      -58, -13, 5, -76, 68, -13, -76, 68, -76, -454, 68, -40, -76, 50, -454,
      50, -76, 68, -13, -58, -40, -58, -13, -76, 5, -40, 572, -184, 176, -40,
      -184, -40, -76, -76, 68, 68, -454, 428, -184, -58, -76, -40, -40, -58,
      -40, 104, 68, 572, -76}, {68, -76, -454, 68, -40, -76, -76, -58, -40,
      -40, 428, -184, 68, 104, -76, 572, -58, -40, -40, 572, -184, 176, -40,
      -184, -76, 428, 50, -76, -184, 428, -13, -76, 68, -454, 50, 50, 50, 572,
      -454, 3641, 50, -454, 428, 554, -76, 572, -13, 50, -13, 5, -58, -58, -76,
      -40, 5, -58, -76, 50, -13, -13, -58, 104, -40, 572, 68, -76, 50, 572,
      -13, 554, -76, 428, -13, -58, 5, -76, -58, -40, -76, -40, 68, -454, -76,
      68, -13, 68, -76, 50, -454, 50, -13, -76, 5, -13, 68, -76, -76, 50, 428,
      -184, -76, 428, -58, -13, 5, -76, -13, 50, -76, -40, -58, 428, -40, -184,
      5, -58, -58, -76, -76, 428}, {-13, -58, -13, 50, 5, -76, 50, -76, -76,
      428, 428, -184, -58, -76, 5, -58, 428, -76, -58, 428, -76, -40, -184,
      -40, 554, 428, -13, 50, -76, 572, 428, -184, -76, 50, 428, -76, 50, -454,
      572, 50, 3641, -454, 50, 50, -76, -13, -454, 68, 572, -40, -40, -184,
      -184, 176, -76, -40, 68, -454, -76, 68, -58, -40, 104, 68, 572, -76, 428,
      -184, -58, -76, -40, -40, 5, -13, -13, -76, -76, 68, -76, 68, -40, -76,
      -454, 68, -58, -40, -58, -13, -76, 5, 50, -454, 50, -76, 68, -13, -13,
      554, 50, 572, 428, -76, -13, -13, -58, 5, 50, -76, -76, -40, 5, -13, -58,
      -58, -76, 68, 572, -40, 104, -58}, {-76, -40, 68, -454, -76, 68, 572,
      -40, -40, -184, -184, 176, 104, 68, -58, -40, -76, 572, -58, -76, 428,
      -184, -40, -40, 428, -184, -76, 50, 428, -76, 554, 428, -13, 50, -76,
      572, 572, 50, 50, -454, -454, 3641, -76, -13, 50, 50, 68, -454, 50, -76,
      -76, 428, 428, -184, -13, -58, -13, 50, 5, -76, 5, -58, -58, -76, -76,
      428, -76, -40, -58, 428, -40, -184, -58, -13, -58, -40, 5, -76, -40, -76,
      -76, 68, 68, -454, -13, -76, 5, -13, 68, -76, -13, 68, -76, 50, -454, 50,
      5, -13, -76, -40, -58, -58, -58, 5, -13, -13, -76, 50, 50, 572, -13, 554,
      -76, 428, -58, 104, -40, 572, 68, -76}, {-13, -13, -58, 5, 50, -76, -13,
      554, 50, 572, 428, -76, 5, -13, -76, -40, -58, -58, 572, -40, -76, 68,
      -58, 104, 50, 50, -76, -13, -454, 68, -76, 572, 428, 554, 50, -13, 428,
      -76, -184, 428, 50, -76, 3641, -454, -454, 50, 50, 572, -58, 428, -76,
      -40, -184, -40, 5, -76, -58, -13, 50, -13, 428, -184, -76, 50, 428, -76,
      428, -76, -76, -58, -58, 5, -58, -76, 428, -184, -40, -40, -76, 68, -40,
      -76, -454, 68, -184, 176, -40, 572, -184, -40, -76, 572, 68, 104, -40,
      -58, 50, -76, 50, -454, -13, 68, -454, 68, 68, -76, -76, -40, -76, 68,
      -13, 5, -76, -13, 5, -76, -13, -58, -40, -58}, {-58, 5, -13, -13, -76,
      50, 5, -13, -76, -40, -58, -58, -13, 554, 50, 572, 428, -76, -40, 572,
      -58, 104, -76, 68, -76, -13, 50, 50, 68, -454, 428, -76, -184, 428, 50,
      -76, -76, 572, 428, 554, 50, -13, -454, 3641, 50, 572, -454, 50, -58,
      -76, 428, -184, -40, -40, -76, 68, -40, -76, -454, 68, -184, 176, -40,
      572, -184, -40, -76, 572, 68, 104, -40, -58, -58, 428, -76, -40, -184,
      -40, 5, -76, -58, -13, 50, -13, 428, -184, -76, 50, 428, -76, 428, -76,
      -76, -58, -58, 5, -76, 50, -13, 68, 50, -454, 68, -454, -76, -40, 68,
      -76, 5, -76, -13, -58, -40, -58, -76, 68, -13, 5, -76, -13}, {5, -76,
      -58, -13, 50, -13, -58, 428, -76, -40, -184, -40, -76, 50, 428, -184,
      -76, 428, -76, -58, 428, -76, 5, -58, -76, 572, 428, 554, 50, -13, 50,
      50, -76, -13, -454, 68, -184, 428, 428, -76, -76, 50, -454, 50, 3641,
      -454, 572, 50, -13, 554, 50, 572, 428, -76, -13, -13, -58, 5, 50, -76,
      -76, -40, 5, -13, -58, -58, -76, 68, 572, -40, 104, -58, -40, 572, -184,
      176, -40, -184, -40, -76, -76, 68, 68, -454, 428, -184, -58, -76, -40,
      -40, -58, -40, 104, 68, 572, -76, -13, 5, -76, 68, -13, -76, 68, -76,
      -454, 68, -40, -76, 50, -454, 50, -76, 68, -13, -58, -40, -58, -13, -76,
      5}, {-76, 68, -40, -76, -454, 68, -58, -76, 428, -184, -40, -40, -40,
      572, -184, 176, -40, -184, 68, 104, -76, 572, -58, -40, 428, -76, -184,
      428, 50, -76, -76, -13, 50, 50, 68, -454, 428, 554, -76, 572, -13, 50,
      50, 572, -454, 3641, 50, -454, 5, -13, -76, -40, -58, -58, -58, 5, -13,
      -13, -76, 50, 50, 572, -13, 554, -76, 428, -58, 104, -40, 572, 68, -76,
      -76, 50, 428, -184, -76, 428, -58, -13, 5, -76, -13, 50, -76, -40, -58,
      428, -40, -184, 5, -58, -58, -76, -76, 428, -13, -58, 5, -76, -58, -40,
      -76, -40, 68, -454, -76, 68, -13, 68, -76, 50, -454, 50, -13, -76, 5,
      -13, 68, -76}, {-58, -13, 5, -76, -13, 50, -76, 50, 428, -184, -76, 428,
      -58, 428, -76, -40, -184, -40, -58, -76, 5, -58, 428, -76, 428, 554, -76,
      572, -13, 50, -184, 428, 428, -76, -76, 50, 50, 50, -76, -13, -454, 68,
      50, -454, 572, 50, 3641, -454, -40, 572, -184, 176, -40, -184, -40, -76,
      -76, 68, 68, -454, 428, -184, -58, -76, -40, -40, -58, -40, 104, 68, 572,
      -76, -13, 554, 50, 572, 428, -76, -13, -13, -58, 5, 50, -76, -76, -40, 5,
      -13, -58, -58, -76, 68, 572, -40, 104, -58, 5, -13, -13, -76, -76, 68,
      -76, 68, -40, -76, -454, 68, -58, -40, -58, -13, -76, 5, 50, -454, 50,
      -76, 68, -13}, {-40, -76, -76, 68, 68, -454, -40, 572, -184, 176, -40,
      -184, -58, -76, 428, -184, -40, -40, 104, 68, -58, -40, -76, 572, -184,
      428, 428, -76, -76, 50, 428, 554, -76, 572, -13, 50, -76, -13, 50, 50,
      68, -454, 572, 50, 50, -454, -454, 3641, -76, 50, 428, -184, -76, 428,
      -58, -13, 5, -76, -13, 50, -76, -40, -58, 428, -40, -184, 5, -58, -58,
      -76, -76, 428, 5, -13, -76, -40, -58, -58, -58, 5, -13, -13, -76, 50, 50,
      572, -13, 554, -76, 428, -58, 104, -40, 572, 68, -76, -58, -13, -58, -40,
      5, -76, -40, -76, -76, 68, 68, -454, -13, -76, 5, -13, 68, -76, -13, 68,
      -76, 50, -454, 50}, {50, -76, 50, -454, -13, 68, -454, 68, 68, -76, -76,
      -40, -76, 68, -13, 5, -76, -13, 5, -76, -13, -58, -40, -58, 572, -40,
      -76, 68, -58, 104, 50, -76, -13, -13, 5, -58, 428, -76, 554, -13, 572,
      50, -58, -58, -13, 5, -40, -76, 3641, -454, -454, 50, 50, 572, -454, 68,
      50, 50, -13, -76, 50, -13, 572, -76, 554, 428, 50, -76, -76, 428, 428,
      -184, 428, -76, -76, -58, -58, 5, -184, -40, 428, -58, -40, -76, 50, -13,
      -76, 5, -13, -58, 428, -76, -184, 428, 50, -76, -76, 572, 68, 104, -40,
      -58, -40, -40, -76, -58, -184, 428, -454, 68, 68, -76, -76, -40, -184,
      -40, 176, -184, 572, -40}, {-76, 50, -13, 68, 50, -454, 68, -454, -76,
      -40, 68, -76, 5, -76, -13, -58, -40, -58, -76, 68, -13, 5, -76, -13, -40,
      572, -58, 104, -76, 68, -76, 50, 5, -58, -13, -13, -58, -58, -13, 5, -40,
      -76, 428, -76, 554, -13, 572, 50, -454, 3641, 50, 572, -454, 50, 68,
      -454, -13, -76, 50, 50, 50, -76, -76, 428, 428, -184, 50, -13, 572, -76,
      554, 428, -76, 572, 68, 104, -40, -58, -40, -40, -76, -58, -184, 428,
      -454, 68, 68, -76, -76, -40, -184, -40, 176, -184, 572, -40, 428, -76,
      -76, -58, -58, 5, -184, -40, 428, -58, -40, -76, 50, -13, -76, 5, -13,
      -58, 428, -76, -184, 428, 50, -76}, {-13, 5, -76, 68, -13, -76, 68, -76,
      -454, 68, -40, -76, 50, -454, 50, -76, 68, -13, -58, -40, -58, -13, -76,
      5, -76, -58, 428, -76, 5, -58, 50, -13, -76, 5, -13, -58, -184, -40, 428,
      -58, -40, -76, -76, 428, 50, -76, -184, 428, -454, 50, 3641, -454, 572,
      50, 50, -13, 572, -76, 554, 428, -454, 68, 50, 50, -13, -76, -76, 50,
      428, -184, -76, 428, -76, 68, 572, -40, 104, -58, 428, -76, 554, -13,
      572, 50, 50, -76, -13, -13, 5, -58, -58, -58, -40, -76, -13, 5, -58, -40,
      104, 68, 572, -76, -40, -184, 572, -40, 176, -184, 68, -454, -76, -40,
      68, -76, -40, -40, -184, 428, -76, -58}, {-13, -58, 5, -76, -58, -40,
      -76, -40, 68, -454, -76, 68, -13, 68, -76, 50, -454, 50, -13, -76, 5,
      -13, 68, -76, 68, 104, -76, 572, -58, -40, -454, 68, 68, -76, -76, -40,
      -40, -40, -76, -58, -184, 428, -40, -184, 572, -40, 176, -184, 50, 572,
      -454, 3641, 50, -454, 50, -76, -76, 428, 428, -184, 68, -454, -13, -76,
      50, 50, -13, 50, 554, 428, 572, -76, -58, 104, -40, 572, 68, -76, -58,
      -58, -13, 5, -40, -76, -76, 50, 5, -58, -13, -13, -76, 428, 572, 50, 554,
      -13, 5, -58, -58, -76, -76, 428, -76, 428, 50, -76, -184, 428, -13, 50,
      -13, -58, -76, 5, -40, -184, -40, -76, 428, -58}, {5, -13, -13, -76, -76,
      68, -76, 68, -40, -76, -454, 68, -58, -40, -58, -13, -76, 5, 50, -454,
      50, -76, 68, -13, -58, -76, 5, -58, 428, -76, -13, 50, -13, -58, -76, 5,
      -76, 428, 50, -76, -184, 428, -184, -40, 428, -58, -40, -76, 50, -454,
      572, 50, 3641, -454, -13, 50, 554, 428, 572, -76, -76, 50, 428, -184,
      -76, 428, -454, 68, 50, 50, -13, -76, -58, -40, 104, 68, 572, -76, -40,
      -184, 572, -40, 176, -184, 68, -454, -76, -40, 68, -76, -40, -40, -184,
      428, -76, -58, -76, 68, 572, -40, 104, -58, 428, -76, 554, -13, 572, 50,
      50, -76, -13, -13, 5, -58, -58, -58, -40, -76, -13, 5}, {-58, -13, -58,
      -40, 5, -76, -40, -76, -76, 68, 68, -454, -13, -76, 5, -13, 68, -76, -13,
      68, -76, 50, -454, 50, 104, 68, -58, -40, -76, 572, 68, -454, -76, -40,
      68, -76, -40, -184, 572, -40, 176, -184, -40, -40, -76, -58, -184, 428,
      572, 50, 50, -454, -454, 3641, -76, 50, 428, -184, -76, 428, -13, 50,
      554, 428, 572, -76, 68, -454, -13, -76, 50, 50, 5, -58, -58, -76, -76,
      428, -76, 428, 50, -76, -184, 428, -13, 50, -13, -58, -76, 5, -40, -184,
      -40, -76, 428, -58, -58, 104, -40, 572, 68, -76, -58, -58, -13, 5, -40,
      -76, -76, 50, 5, -58, -13, -13, -76, 428, 572, 50, 554, -13}, {572, -40,
      -76, 68, -58, 104, 50, -76, -13, -13, 5, -58, 428, -76, 554, -13, 572,
      50, -58, -58, -13, 5, -40, -76, 50, -76, 50, -454, -13, 68, -454, 68, 68,
      -76, -76, -40, -76, 68, -13, 5, -76, -13, 5, -76, -13, -58, -40, -58,
      -454, 68, 50, 50, -13, -76, 3641, -454, -454, 50, 50, 572, 572, -76, 50,
      -13, 428, 554, -76, 428, 50, -76, -184, 428, -184, -40, 428, -58, -40,
      -76, 428, -76, -76, -58, -58, 5, -76, 5, 50, -13, -58, -13, -184, 428,
      428, -76, -76, 50, -40, -40, -76, -58, -184, 428, -76, 572, 68, 104, -40,
      -58, 68, -76, -454, 68, -40, -76, 176, -184, -184, -40, -40, 572}, {-40,
      572, -58, 104, -76, 68, -76, 50, 5, -58, -13, -13, -58, -58, -13, 5, -40,
      -76, 428, -76, 554, -13, 572, 50, -76, 50, -13, 68, 50, -454, 68, -454,
      -76, -40, 68, -76, 5, -76, -13, -58, -40, -58, -76, 68, -13, 5, -76, -13,
      68, -454, -13, -76, 50, 50, -454, 3641, 50, 572, -454, 50, -76, 428, 50,
      -76, -184, 428, 572, -76, 50, -13, 428, 554, -40, -40, -76, -58, -184,
      428, -76, 572, 68, 104, -40, -58, 68, -76, -454, 68, -40, -76, 176, -184,
      -184, -40, -40, 572, -184, -40, 428, -58, -40, -76, 428, -76, -76, -58,
      -58, 5, -76, 5, 50, -13, -58, -13, -184, 428, 428, -76, -76, 50}, {-76,
      -58, 428, -76, 5, -58, 50, -13, -76, 5, -13, -58, -184, -40, 428, -58,
      -40, -76, -76, 428, 50, -76, -184, 428, -13, 5, -76, 68, -13, -76, 68,
      -76, -454, 68, -40, -76, 50, -454, 50, -76, 68, -13, -58, -40, -58, -13,
      -76, 5, 50, -13, 572, -76, 554, 428, -454, 50, 3641, -454, 572, 50, 50,
      50, -454, 68, -76, -13, 428, -184, -76, 50, 428, -76, 428, -76, 554, -13,
      572, 50, -76, 68, 572, -40, 104, -58, -13, -13, 50, -76, -58, 5, -40,
      -76, -58, -58, 5, -13, -40, -184, 572, -40, 176, -184, -58, -40, 104, 68,
      572, -76, -76, -40, 68, -454, -76, 68, -184, 428, -40, -40, -58, -76},
      {68, 104, -76, 572, -58, -40, -454, 68, 68, -76, -76, -40, -40, -40, -76,
      -58, -184, 428, -40, -184, 572, -40, 176, -184, -13, -58, 5, -76, -58,
      -40, -76, -40, 68, -454, -76, 68, -13, 68, -76, 50, -454, 50, -13, -76,
      5, -13, 68, -76, 50, -76, -76, 428, 428, -184, 50, 572, -454, 3641, 50,
      -454, -13, -76, 68, -454, 50, 50, 554, 428, -13, 50, -76, 572, -58, -58,
      -13, 5, -40, -76, -58, 104, -40, 572, 68, -76, 5, -58, -76, 50, -13, -13,
      572, 50, -76, 428, -13, 554, -76, 428, 50, -76, -184, 428, 5, -58, -58,
      -76, -76, 428, -13, -58, -13, 50, 5, -76, -40, -76, -40, -184, -58, 428},
      {-58, -76, 5, -58, 428, -76, -13, 50, -13, -58, -76, 5, -76, 428, 50,
      -76, -184, 428, -184, -40, 428, -58, -40, -76, 5, -13, -13, -76, -76, 68,
      -76, 68, -40, -76, -454, 68, -58, -40, -58, -13, -76, 5, 50, -454, 50,
      -76, 68, -13, -13, 50, 554, 428, 572, -76, 50, -454, 572, 50, 3641, -454,
      428, -184, -76, 50, 428, -76, 50, 50, -454, 68, -76, -13, -40, -184, 572,
      -40, 176, -184, -58, -40, 104, 68, 572, -76, -76, -40, 68, -454, -76, 68,
      -184, 428, -40, -40, -58, -76, 428, -76, 554, -13, 572, 50, -76, 68, 572,
      -40, 104, -58, -13, -13, 50, -76, -58, 5, -40, -76, -58, -58, 5, -13},
      {104, 68, -58, -40, -76, 572, 68, -454, -76, -40, 68, -76, -40, -184,
      572, -40, 176, -184, -40, -40, -76, -58, -184, 428, -58, -13, -58, -40,
      5, -76, -40, -76, -76, 68, 68, -454, -13, -76, 5, -13, 68, -76, -13, 68,
      -76, 50, -454, 50, -76, 50, 428, -184, -76, 428, 572, 50, 50, -454, -454,
      3641, 554, 428, -13, 50, -76, 572, -13, -76, 68, -454, 50, 50, -76, 428,
      50, -76, -184, 428, 5, -58, -58, -76, -76, 428, -13, -58, -13, 50, 5,
      -76, -40, -76, -40, -184, -58, 428, -58, -58, -13, 5, -40, -76, -58, 104,
      -40, 572, 68, -76, 5, -58, -76, 50, -13, -13, 572, 50, -76, 428, -13,
      554}, {554, -13, 428, -76, 50, 572, -13, -13, 50, -76, -58, 5, -76, 68,
      572, -40, 104, -58, -76, -40, 5, -13, -58, -58, 428, -58, -184, -40, -76,
      -40, -76, 5, 50, -13, -58, -13, 428, -76, -76, -58, -58, 5, 428, -184,
      -76, 50, 428, -76, 50, 50, -454, 68, -76, -13, 572, -76, 50, -13, 428,
      554, 3641, -454, -454, 50, 50, 572, 428, -76, -184, 428, 50, -76, 50,
      -454, 50, -76, 68, -13, -76, 68, -13, 5, -76, -13, -454, 68, 68, -76,
      -76, -40, -76, 5, -40, -58, -13, -58, 428, -184, -58, -76, -40, -40,
      -184, 176, -40, 572, -184, -40, -76, 68, -40, -76, -454, 68, 572, -76,
      -40, -58, 68, 104}, {-13, 5, -58, -58, -76, -40, 5, -58, -76, 50, -13,
      -13, -58, 104, -40, 572, 68, -76, 50, 572, -13, 554, -76, 428, -76, -58,
      -40, -40, 428, -184, 68, -76, -454, 68, -40, -76, -76, 572, 68, 104, -40,
      -58, -184, 176, -40, 572, -184, -40, -13, -76, 68, -454, 50, 50, -76,
      428, 50, -76, -184, 428, -454, 3641, 50, 572, -454, 50, -76, 572, 428,
      554, 50, -13, -13, 68, -76, 50, -454, 50, 5, -76, -13, -58, -40, -58, 68,
      -454, -76, -40, 68, -76, 68, -76, -76, -13, -13, 5, -76, -40, -58, 428,
      -40, -184, 428, -184, -76, 50, 428, -76, 5, -76, -58, -13, 50, -13, -76,
      428, -58, 5, -76, -58}, {428, -58, -184, -40, -76, -40, -76, 5, 50, -13,
      -58, -13, 428, -76, -76, -58, -58, 5, 428, -184, -76, 50, 428, -76, 554,
      -13, 428, -76, 50, 572, -13, -13, 50, -76, -58, 5, -76, 68, 572, -40,
      104, -58, -76, -40, 5, -13, -58, -58, 572, -76, 50, -13, 428, 554, 50,
      50, -454, 68, -76, -13, -454, 50, 3641, -454, 572, 50, -184, 428, 428,
      -76, -76, 50, -76, 68, -13, 5, -76, -13, 50, -454, 50, -76, 68, -13, 68,
      -76, -454, 68, -40, -76, -40, -58, -76, 5, -58, -13, -184, 176, -40, 572,
      -184, -40, 428, -184, -58, -76, -40, -40, -40, -76, -76, 68, 68, -454,
      -40, -58, 572, -76, 104, 68}, {-76, -58, -40, -40, 428, -184, 68, -76,
      -454, 68, -40, -76, -76, 572, 68, 104, -40, -58, -184, 176, -40, 572,
      -184, -40, -13, 5, -58, -58, -76, -40, 5, -58, -76, 50, -13, -13, -58,
      104, -40, 572, 68, -76, 50, 572, -13, 554, -76, 428, -76, 428, 50, -76,
      -184, 428, -13, -76, 68, -454, 50, 50, 50, 572, -454, 3641, 50, -454,
      428, 554, -76, 572, -13, 50, 5, -76, -13, -58, -40, -58, -13, 68, -76,
      50, -454, 50, -76, -40, 68, -454, -76, 68, -76, -13, 68, -76, 5, -13,
      428, -184, -76, 50, 428, -76, -76, -40, -58, 428, -40, -184, -58, -13, 5,
      -76, -13, 50, -58, 5, -76, 428, -58, -76}, {50, -76, -76, 428, 428, -184,
      -13, -58, -13, 50, 5, -76, 5, -58, -58, -76, -76, 428, -76, -40, -58,
      428, -40, -184, 572, -40, -40, -184, -184, 176, -76, -40, 68, -454, -76,
      68, -58, -40, 104, 68, 572, -76, 428, -184, -58, -76, -40, -40, 554, 428,
      -13, 50, -76, 572, 428, -184, -76, 50, 428, -76, 50, -454, 572, 50, 3641,
      -454, 50, 50, -76, -13, -454, 68, -13, -76, 5, -13, 68, -76, -58, -40,
      -58, -13, -76, 5, -76, 68, -40, -76, -454, 68, -454, 50, 68, -13, 50,
      -76, 50, 572, -13, 554, -76, 428, -76, -40, 5, -13, -58, -58, -13, -13,
      -58, 5, 50, -76, 68, -76, 104, -58, 572, -40}, {572, -40, -40, -184,
      -184, 176, -76, -40, 68, -454, -76, 68, -58, -40, 104, 68, 572, -76, 428,
      -184, -58, -76, -40, -40, 50, -76, -76, 428, 428, -184, -13, -58, -13,
      50, 5, -76, 5, -58, -58, -76, -76, 428, -76, -40, -58, 428, -40, -184,
      428, -184, -76, 50, 428, -76, 554, 428, -13, 50, -76, 572, 572, 50, 50,
      -454, -454, 3641, -76, -13, 50, 50, 68, -454, -58, -40, -58, -13, -76, 5,
      -13, -76, 5, -13, 68, -76, -40, -76, -76, 68, 68, -454, 68, -13, -454,
      50, -76, 50, -76, -40, 5, -13, -58, -58, 50, 572, -13, 554, -76, 428,
      -58, 5, -13, -13, -76, 50, 104, -58, 68, -76, -40, 572}, {-13, 554, 50,
      572, 428, -76, -13, -13, -58, 5, 50, -76, -76, -40, 5, -13, -58, -58,
      -76, 68, 572, -40, 104, -58, -58, 428, -76, -40, -184, -40, 5, -76, -58,
      -13, 50, -13, 428, -184, -76, 50, 428, -76, 428, -76, -76, -58, -58, 5,
      50, 50, -76, -13, -454, 68, -76, 572, 428, 554, 50, -13, 428, -76, -184,
      428, 50, -76, 3641, -454, -454, 50, 50, 572, 428, -184, -58, -76, -40,
      -40, -184, 176, -40, 572, -184, -40, -76, 68, -40, -76, -454, 68, 572,
      -76, -40, -58, 68, 104, 50, -454, 50, -76, 68, -13, -76, 68, -13, 5, -76,
      -13, -454, 68, 68, -76, -76, -40, -76, 5, -40, -58, -13, -58}, {5, -13,
      -76, -40, -58, -58, -58, 5, -13, -13, -76, 50, 50, 572, -13, 554, -76,
      428, -58, 104, -40, 572, 68, -76, -58, -76, 428, -184, -40, -40, -76, 68,
      -40, -76, -454, 68, -184, 176, -40, 572, -184, -40, -76, 572, 68, 104,
      -40, -58, -76, -13, 50, 50, 68, -454, 428, -76, -184, 428, 50, -76, -76,
      572, 428, 554, 50, -13, -454, 3641, 50, 572, -454, 50, -76, -40, -58,
      428, -40, -184, 428, -184, -76, 50, 428, -76, 5, -76, -58, -13, 50, -13,
      -76, 428, -58, 5, -76, -58, -13, 68, -76, 50, -454, 50, 5, -76, -13, -58,
      -40, -58, 68, -454, -76, -40, 68, -76, 68, -76, -76, -13, -13, 5}, {-58,
      428, -76, -40, -184, -40, 5, -76, -58, -13, 50, -13, 428, -184, -76, 50,
      428, -76, 428, -76, -76, -58, -58, 5, -13, 554, 50, 572, 428, -76, -13,
      -13, -58, 5, 50, -76, -76, -40, 5, -13, -58, -58, -76, 68, 572, -40, 104,
      -58, -76, 572, 428, 554, 50, -13, 50, 50, -76, -13, -454, 68, -184, 428,
      428, -76, -76, 50, -454, 50, 3641, -454, 572, 50, -184, 176, -40, 572,
      -184, -40, 428, -184, -58, -76, -40, -40, -40, -76, -76, 68, 68, -454,
      -40, -58, 572, -76, 104, 68, -76, 68, -13, 5, -76, -13, 50, -454, 50,
      -76, 68, -13, 68, -76, -454, 68, -40, -76, -40, -58, -76, 5, -58, -13},
      {-58, -76, 428, -184, -40, -40, -76, 68, -40, -76, -454, 68, -184, 176,
      -40, 572, -184, -40, -76, 572, 68, 104, -40, -58, 5, -13, -76, -40, -58,
      -58, -58, 5, -13, -13, -76, 50, 50, 572, -13, 554, -76, 428, -58, 104,
      -40, 572, 68, -76, 428, -76, -184, 428, 50, -76, -76, -13, 50, 50, 68,
      -454, 428, 554, -76, 572, -13, 50, 50, 572, -454, 3641, 50, -454, 428,
      -184, -76, 50, 428, -76, -76, -40, -58, 428, -40, -184, -58, -13, 5, -76,
      -13, 50, -58, 5, -76, 428, -58, -76, 5, -76, -13, -58, -40, -58, -13, 68,
      -76, 50, -454, 50, -76, -40, 68, -454, -76, 68, -76, -13, 68, -76, 5,
      -13}, {-76, 50, 428, -184, -76, 428, -58, -13, 5, -76, -13, 50, -76, -40,
      -58, 428, -40, -184, 5, -58, -58, -76, -76, 428, -40, 572, -184, 176,
      -40, -184, -40, -76, -76, 68, 68, -454, 428, -184, -58, -76, -40, -40,
      -58, -40, 104, 68, 572, -76, 428, 554, -76, 572, -13, 50, -184, 428, 428,
      -76, -76, 50, 50, 50, -76, -13, -454, 68, 50, -454, 572, 50, 3641, -454,
      50, 572, -13, 554, -76, 428, -76, -40, 5, -13, -58, -58, -13, -13, -58,
      5, 50, -76, 68, -76, 104, -58, 572, -40, -13, -76, 5, -13, 68, -76, -58,
      -40, -58, -13, -76, 5, -76, 68, -40, -76, -454, 68, -454, 50, 68, -13,
      50, -76}, {-40, 572, -184, 176, -40, -184, -40, -76, -76, 68, 68, -454,
      428, -184, -58, -76, -40, -40, -58, -40, 104, 68, 572, -76, -76, 50, 428,
      -184, -76, 428, -58, -13, 5, -76, -13, 50, -76, -40, -58, 428, -40, -184,
      5, -58, -58, -76, -76, 428, -184, 428, 428, -76, -76, 50, 428, 554, -76,
      572, -13, 50, -76, -13, 50, 50, 68, -454, 572, 50, 50, -454, -454, 3641,
      -76, -40, 5, -13, -58, -58, 50, 572, -13, 554, -76, 428, -58, 5, -13,
      -13, -76, 50, 104, -58, 68, -76, -40, 572, -58, -40, -58, -13, -76, 5,
      -13, -76, 5, -13, 68, -76, -40, -76, -76, 68, 68, -454, 68, -13, -454,
      50, -76, 50}, {50, -454, 50, -76, 68, -13, -76, 68, -13, 5, -76, -13,
      -454, 68, 68, -76, -76, -40, -76, 5, -40, -58, -13, -58, -76, 68, 572,
      -40, 104, -58, 428, -76, 554, -13, 572, 50, 50, -76, -13, -13, 5, -58,
      -58, -58, -40, -76, -13, 5, 428, -76, -76, -58, -58, 5, -184, -40, 428,
      -58, -40, -76, 50, -13, -76, 5, -13, -58, 428, -76, -184, 428, 50, -76,
      3641, -454, -454, 50, 50, 572, -454, 68, 50, 50, -13, -76, 50, -13, 572,
      -76, 554, 428, 50, -76, -76, 428, 428, -184, 572, -76, -40, -58, 68, 104,
      -40, -40, -184, 428, -76, -58, -184, -40, 176, -184, 572, -40, -454, 68,
      68, -76, -76, -40}, {-13, 68, -76, 50, -454, 50, 5, -76, -13, -58, -40,
      -58, 68, -454, -76, -40, 68, -76, 68, -76, -76, -13, -13, 5, -58, 104,
      -40, 572, 68, -76, -58, -58, -13, 5, -40, -76, -76, 50, 5, -58, -13, -13,
      -76, 428, 572, 50, 554, -13, -76, 572, 68, 104, -40, -58, -40, -40, -76,
      -58, -184, 428, -454, 68, 68, -76, -76, -40, -184, -40, 176, -184, 572,
      -40, -454, 3641, 50, 572, -454, 50, 68, -454, -13, -76, 50, 50, 50, -76,
      -76, 428, 428, -184, 50, -13, 572, -76, 554, 428, -76, 428, -58, 5, -76,
      -58, -40, -184, -40, -76, 428, -58, 428, -76, -184, 428, 50, -76, 50,
      -13, -76, 5, -13, -58}, {-76, 68, -13, 5, -76, -13, 50, -454, 50, -76,
      68, -13, 68, -76, -454, 68, -40, -76, -40, -58, -76, 5, -58, -13, 428,
      -76, -76, -58, -58, 5, -184, -40, 428, -58, -40, -76, 50, -13, -76, 5,
      -13, -58, 428, -76, -184, 428, 50, -76, -76, 68, 572, -40, 104, -58, 428,
      -76, 554, -13, 572, 50, 50, -76, -13, -13, 5, -58, -58, -58, -40, -76,
      -13, 5, -454, 50, 3641, -454, 572, 50, 50, -13, 572, -76, 554, 428, -454,
      68, 50, 50, -13, -76, -76, 50, 428, -184, -76, 428, -40, -58, 572, -76,
      104, 68, -184, -40, 176, -184, 572, -40, -40, -40, -184, 428, -76, -58,
      68, -454, -76, -40, 68, -76}, {5, -76, -13, -58, -40, -58, -13, 68, -76,
      50, -454, 50, -76, -40, 68, -454, -76, 68, -76, -13, 68, -76, 5, -13,
      -76, 572, 68, 104, -40, -58, -40, -40, -76, -58, -184, 428, -454, 68, 68,
      -76, -76, -40, -184, -40, 176, -184, 572, -40, -58, 104, -40, 572, 68,
      -76, -58, -58, -13, 5, -40, -76, -76, 50, 5, -58, -13, -13, -76, 428,
      572, 50, 554, -13, 50, 572, -454, 3641, 50, -454, 50, -76, -76, 428, 428,
      -184, 68, -454, -13, -76, 50, 50, -13, 50, 554, 428, 572, -76, -58, 5,
      -76, 428, -58, -76, 428, -76, -184, 428, 50, -76, -40, -184, -40, -76,
      428, -58, -13, 50, -13, -58, -76, 5}, {-13, -76, 5, -13, 68, -76, -58,
      -40, -58, -13, -76, 5, -76, 68, -40, -76, -454, 68, -454, 50, 68, -13,
      50, -76, 5, -58, -58, -76, -76, 428, -76, 428, 50, -76, -184, 428, -13,
      50, -13, -58, -76, 5, -40, -184, -40, -76, 428, -58, -58, -40, 104, 68,
      572, -76, -40, -184, 572, -40, 176, -184, 68, -454, -76, -40, 68, -76,
      -40, -40, -184, 428, -76, -58, 50, -454, 572, 50, 3641, -454, -13, 50,
      554, 428, 572, -76, -76, 50, 428, -184, -76, 428, -454, 68, 50, 50, -13,
      -76, 68, -76, 104, -58, 572, -40, -76, 428, 572, 50, 554, -13, -58, -58,
      -40, -76, -13, 5, 50, -76, -13, -13, 5, -58}, {-58, -40, -58, -13, -76,
      5, -13, -76, 5, -13, 68, -76, -40, -76, -76, 68, 68, -454, 68, -13, -454,
      50, -76, 50, -58, -40, 104, 68, 572, -76, -40, -184, 572, -40, 176, -184,
      68, -454, -76, -40, 68, -76, -40, -40, -184, 428, -76, -58, 5, -58, -58,
      -76, -76, 428, -76, 428, 50, -76, -184, 428, -13, 50, -13, -58, -76, 5,
      -40, -184, -40, -76, 428, -58, 572, 50, 50, -454, -454, 3641, -76, 50,
      428, -184, -76, 428, -13, 50, 554, 428, 572, -76, 68, -454, -13, -76, 50,
      50, 104, -58, 68, -76, -40, 572, -58, -58, -40, -76, -13, 5, -76, 428,
      572, 50, 554, -13, -76, 50, 5, -58, -13, -13}, {-76, 68, 572, -40, 104,
      -58, 428, -76, 554, -13, 572, 50, 50, -76, -13, -13, 5, -58, -58, -58,
      -40, -76, -13, 5, 50, -454, 50, -76, 68, -13, -76, 68, -13, 5, -76, -13,
      -454, 68, 68, -76, -76, -40, -76, 5, -40, -58, -13, -58, -184, -40, 428,
      -58, -40, -76, 428, -76, -76, -58, -58, 5, -76, 5, 50, -13, -58, -13,
      -184, 428, 428, -76, -76, 50, -454, 68, 50, 50, -13, -76, 3641, -454,
      -454, 50, 50, 572, 572, -76, 50, -13, 428, 554, -76, 428, 50, -76, -184,
      428, -40, -40, -184, 428, -76, -58, 572, -76, -40, -58, 68, 104, 176,
      -184, -184, -40, -40, 572, 68, -76, -454, 68, -40, -76}, {-58, 104, -40,
      572, 68, -76, -58, -58, -13, 5, -40, -76, -76, 50, 5, -58, -13, -13, -76,
      428, 572, 50, 554, -13, -13, 68, -76, 50, -454, 50, 5, -76, -13, -58,
      -40, -58, 68, -454, -76, -40, 68, -76, 68, -76, -76, -13, -13, 5, -40,
      -40, -76, -58, -184, 428, -76, 572, 68, 104, -40, -58, 68, -76, -454, 68,
      -40, -76, 176, -184, -184, -40, -40, 572, 68, -454, -13, -76, 50, 50,
      -454, 3641, 50, 572, -454, 50, -76, 428, 50, -76, -184, 428, 572, -76,
      50, -13, 428, 554, -40, -184, -40, -76, 428, -58, -76, 428, -58, 5, -76,
      -58, -184, 428, 428, -76, -76, 50, -76, 5, 50, -13, -58, -13}, {428, -76,
      -76, -58, -58, 5, -184, -40, 428, -58, -40, -76, 50, -13, -76, 5, -13,
      -58, 428, -76, -184, 428, 50, -76, -76, 68, -13, 5, -76, -13, 50, -454,
      50, -76, 68, -13, 68, -76, -454, 68, -40, -76, -40, -58, -76, 5, -58,
      -13, 428, -76, 554, -13, 572, 50, -76, 68, 572, -40, 104, -58, -13, -13,
      50, -76, -58, 5, -40, -76, -58, -58, 5, -13, 50, -13, 572, -76, 554, 428,
      -454, 50, 3641, -454, 572, 50, 50, 50, -454, 68, -76, -13, 428, -184,
      -76, 50, 428, -76, -184, -40, 176, -184, 572, -40, -40, -58, 572, -76,
      104, 68, -184, 428, -40, -40, -58, -76, -76, -40, 68, -454, -76, 68},
      {-76, 572, 68, 104, -40, -58, -40, -40, -76, -58, -184, 428, -454, 68,
      68, -76, -76, -40, -184, -40, 176, -184, 572, -40, 5, -76, -13, -58, -40,
      -58, -13, 68, -76, 50, -454, 50, -76, -40, 68, -454, -76, 68, -76, -13,
      68, -76, 5, -13, -58, -58, -13, 5, -40, -76, -58, 104, -40, 572, 68, -76,
      5, -58, -76, 50, -13, -13, 572, 50, -76, 428, -13, 554, 50, -76, -76,
      428, 428, -184, 50, 572, -454, 3641, 50, -454, -13, -76, 68, -454, 50,
      50, 554, 428, -13, 50, -76, 572, 428, -76, -184, 428, 50, -76, -58, 5,
      -76, 428, -58, -76, -40, -76, -40, -184, -58, 428, -13, -58, -13, 50, 5,
      -76}, {5, -58, -58, -76, -76, 428, -76, 428, 50, -76, -184, 428, -13, 50,
      -13, -58, -76, 5, -40, -184, -40, -76, 428, -58, -13, -76, 5, -13, 68,
      -76, -58, -40, -58, -13, -76, 5, -76, 68, -40, -76, -454, 68, -454, 50,
      68, -13, 50, -76, -40, -184, 572, -40, 176, -184, -58, -40, 104, 68, 572,
      -76, -76, -40, 68, -454, -76, 68, -184, 428, -40, -40, -58, -76, -13, 50,
      554, 428, 572, -76, 50, -454, 572, 50, 3641, -454, 428, -184, -76, 50,
      428, -76, 50, 50, -454, 68, -76, -13, -76, 428, 572, 50, 554, -13, 68,
      -76, 104, -58, 572, -40, -40, -76, -58, -58, 5, -13, -13, -13, 50, -76,
      -58, 5}, {-58, -40, 104, 68, 572, -76, -40, -184, 572, -40, 176, -184,
      68, -454, -76, -40, 68, -76, -40, -40, -184, 428, -76, -58, -58, -40,
      -58, -13, -76, 5, -13, -76, 5, -13, 68, -76, -40, -76, -76, 68, 68, -454,
      68, -13, -454, 50, -76, 50, -76, 428, 50, -76, -184, 428, 5, -58, -58,
      -76, -76, 428, -13, -58, -13, 50, 5, -76, -40, -76, -40, -184, -58, 428,
      -76, 50, 428, -184, -76, 428, 572, 50, 50, -454, -454, 3641, 554, 428,
      -13, 50, -76, 572, -13, -76, 68, -454, 50, 50, -58, -58, -40, -76, -13,
      5, 104, -58, 68, -76, -40, 572, 572, 50, -76, 428, -13, 554, 5, -58, -76,
      50, -13, -13}, {428, -76, 554, -13, 572, 50, -76, 68, 572, -40, 104, -58,
      -13, -13, 50, -76, -58, 5, -40, -76, -58, -58, 5, -13, -184, -40, 428,
      -58, -40, -76, 428, -76, -76, -58, -58, 5, -76, 5, 50, -13, -58, -13,
      -184, 428, 428, -76, -76, 50, 50, -454, 50, -76, 68, -13, -76, 68, -13,
      5, -76, -13, -454, 68, 68, -76, -76, -40, -76, 5, -40, -58, -13, -58, 50,
      50, -454, 68, -76, -13, 572, -76, 50, -13, 428, 554, 3641, -454, -454,
      50, 50, 572, 428, -76, -184, 428, 50, -76, -184, 428, -40, -40, -58, -76,
      176, -184, -184, -40, -40, 572, 572, -76, -40, -58, 68, 104, -76, 68,
      -40, -76, -454, 68}, {-58, -58, -13, 5, -40, -76, -58, 104, -40, 572, 68,
      -76, 5, -58, -76, 50, -13, -13, 572, 50, -76, 428, -13, 554, -40, -40,
      -76, -58, -184, 428, -76, 572, 68, 104, -40, -58, 68, -76, -454, 68, -40,
      -76, 176, -184, -184, -40, -40, 572, -13, 68, -76, 50, -454, 50, 5, -76,
      -13, -58, -40, -58, 68, -454, -76, -40, 68, -76, 68, -76, -76, -13, -13,
      5, -13, -76, 68, -454, 50, 50, -76, 428, 50, -76, -184, 428, -454, 3641,
      50, 572, -454, 50, -76, 572, 428, 554, 50, -13, -40, -76, -40, -184, -58,
      428, -184, 428, 428, -76, -76, 50, -76, 428, -58, 5, -76, -58, 5, -76,
      -58, -13, 50, -13}, {-184, -40, 428, -58, -40, -76, 428, -76, -76, -58,
      -58, 5, -76, 5, 50, -13, -58, -13, -184, 428, 428, -76, -76, 50, 428,
      -76, 554, -13, 572, 50, -76, 68, 572, -40, 104, -58, -13, -13, 50, -76,
      -58, 5, -40, -76, -58, -58, 5, -13, -76, 68, -13, 5, -76, -13, 50, -454,
      50, -76, 68, -13, 68, -76, -454, 68, -40, -76, -40, -58, -76, 5, -58,
      -13, 572, -76, 50, -13, 428, 554, 50, 50, -454, 68, -76, -13, -454, 50,
      3641, -454, 572, 50, -184, 428, 428, -76, -76, 50, 176, -184, -184, -40,
      -40, 572, -184, 428, -40, -40, -58, -76, -40, -58, 572, -76, 104, 68,
      -40, -76, -76, 68, 68, -454}, {-40, -40, -76, -58, -184, 428, -76, 572,
      68, 104, -40, -58, 68, -76, -454, 68, -40, -76, 176, -184, -184, -40,
      -40, 572, -58, -58, -13, 5, -40, -76, -58, 104, -40, 572, 68, -76, 5,
      -58, -76, 50, -13, -13, 572, 50, -76, 428, -13, 554, 5, -76, -13, -58,
      -40, -58, -13, 68, -76, 50, -454, 50, -76, -40, 68, -454, -76, 68, -76,
      -13, 68, -76, 5, -13, -76, 428, 50, -76, -184, 428, -13, -76, 68, -454,
      50, 50, 50, 572, -454, 3641, 50, -454, 428, 554, -76, 572, -13, 50, -184,
      428, 428, -76, -76, 50, -40, -76, -40, -184, -58, 428, -58, 5, -76, 428,
      -58, -76, -58, -13, 5, -76, -13, 50}, {-76, 428, 50, -76, -184, 428, 5,
      -58, -58, -76, -76, 428, -13, -58, -13, 50, 5, -76, -40, -76, -40, -184,
      -58, 428, -40, -184, 572, -40, 176, -184, -58, -40, 104, 68, 572, -76,
      -76, -40, 68, -454, -76, 68, -184, 428, -40, -40, -58, -76, -13, -76, 5,
      -13, 68, -76, -58, -40, -58, -13, -76, 5, -76, 68, -40, -76, -454, 68,
      -454, 50, 68, -13, 50, -76, 554, 428, -13, 50, -76, 572, 428, -184, -76,
      50, 428, -76, 50, -454, 572, 50, 3641, -454, 50, 50, -76, -13, -454, 68,
      572, 50, -76, 428, -13, 554, -40, -76, -58, -58, 5, -13, 68, -76, 104,
      -58, 572, -40, -13, -13, -58, 5, 50, -76}, {-40, -184, 572, -40, 176,
      -184, -58, -40, 104, 68, 572, -76, -76, -40, 68, -454, -76, 68, -184,
      428, -40, -40, -58, -76, -76, 428, 50, -76, -184, 428, 5, -58, -58, -76,
      -76, 428, -13, -58, -13, 50, 5, -76, -40, -76, -40, -184, -58, 428, -58,
      -40, -58, -13, -76, 5, -13, -76, 5, -13, 68, -76, -40, -76, -76, 68, 68,
      -454, 68, -13, -454, 50, -76, 50, 428, -184, -76, 50, 428, -76, 554, 428,
      -13, 50, -76, 572, 572, 50, 50, -454, -454, 3641, -76, -13, 50, 50, 68,
      -454, -40, -76, -58, -58, 5, -13, 572, 50, -76, 428, -13, 554, 104, -58,
      68, -76, -40, 572, -58, 5, -13, -13, -76, 50}, {50, 572, -13, 554, -76,
      428, -76, -40, 5, -13, -58, -58, -13, -13, -58, 5, 50, -76, 68, -76, 104,
      -58, 572, -40, -76, -40, -58, 428, -40, -184, 428, -184, -76, 50, 428,
      -76, 5, -76, -58, -13, 50, -13, -76, 428, -58, 5, -76, -58, 428, -184,
      -58, -76, -40, -40, -184, 176, -40, 572, -184, -40, -76, 68, -40, -76,
      -454, 68, 572, -76, -40, -58, 68, 104, 50, 50, -76, -13, -454, 68, -76,
      572, 428, 554, 50, -13, 428, -76, -184, 428, 50, -76, 3641, -454, -454,
      50, 50, 572, -454, 50, 68, -13, 50, -76, 68, -76, -76, -13, -13, 5, -76,
      5, -40, -58, -13, -58, -454, 68, 68, -76, -76, -40}, {-76, -40, 5, -13,
      -58, -58, 50, 572, -13, 554, -76, 428, -58, 5, -13, -13, -76, 50, 104,
      -58, 68, -76, -40, 572, 428, -184, -58, -76, -40, -40, -184, 176, -40,
      572, -184, -40, -76, 68, -40, -76, -454, 68, 572, -76, -40, -58, 68, 104,
      -76, -40, -58, 428, -40, -184, 428, -184, -76, 50, 428, -76, 5, -76, -58,
      -13, 50, -13, -76, 428, -58, 5, -76, -58, -76, -13, 50, 50, 68, -454,
      428, -76, -184, 428, 50, -76, -76, 572, 428, 554, 50, -13, -454, 3641,
      50, 572, -454, 50, 68, -13, -454, 50, -76, 50, -76, 5, -40, -58, -13,
      -58, 68, -76, -76, -13, -13, 5, 68, -454, -76, -40, 68, -76}, {-76, -40,
      -58, 428, -40, -184, 428, -184, -76, 50, 428, -76, 5, -76, -58, -13, 50,
      -13, -76, 428, -58, 5, -76, -58, 50, 572, -13, 554, -76, 428, -76, -40,
      5, -13, -58, -58, -13, -13, -58, 5, 50, -76, 68, -76, 104, -58, 572, -40,
      -184, 176, -40, 572, -184, -40, 428, -184, -58, -76, -40, -40, -40, -76,
      -76, 68, 68, -454, -40, -58, 572, -76, 104, 68, -76, 572, 428, 554, 50,
      -13, 50, 50, -76, -13, -454, 68, -184, 428, 428, -76, -76, 50, -454, 50,
      3641, -454, 572, 50, 68, -76, -76, -13, -13, 5, -454, 50, 68, -13, 50,
      -76, -40, -58, -76, 5, -58, -13, 68, -76, -454, 68, -40, -76}, {428,
      -184, -58, -76, -40, -40, -184, 176, -40, 572, -184, -40, -76, 68, -40,
      -76, -454, 68, 572, -76, -40, -58, 68, 104, -76, -40, 5, -13, -58, -58,
      50, 572, -13, 554, -76, 428, -58, 5, -13, -13, -76, 50, 104, -58, 68,
      -76, -40, 572, 428, -184, -76, 50, 428, -76, -76, -40, -58, 428, -40,
      -184, -58, -13, 5, -76, -13, 50, -58, 5, -76, 428, -58, -76, 428, -76,
      -184, 428, 50, -76, -76, -13, 50, 50, 68, -454, 428, 554, -76, 572, -13,
      50, 50, 572, -454, 3641, 50, -454, -76, 5, -40, -58, -13, -58, 68, -13,
      -454, 50, -76, 50, -76, -13, 68, -76, 5, -13, -76, -40, 68, -454, -76,
      68}, {428, -184, -76, 50, 428, -76, -76, -40, -58, 428, -40, -184, -58,
      -13, 5, -76, -13, 50, -58, 5, -76, 428, -58, -76, -184, 176, -40, 572,
      -184, -40, 428, -184, -58, -76, -40, -40, -40, -76, -76, 68, 68, -454,
      -40, -58, 572, -76, 104, 68, 50, 572, -13, 554, -76, 428, -76, -40, 5,
      -13, -58, -58, -13, -13, -58, 5, 50, -76, 68, -76, 104, -58, 572, -40,
      428, 554, -76, 572, -13, 50, -184, 428, 428, -76, -76, 50, 50, 50, -76,
      -13, -454, 68, 50, -454, 572, 50, 3641, -454, -76, -13, 68, -76, 5, -13,
      -40, -58, -76, 5, -58, -13, -454, 50, 68, -13, 50, -76, -76, 68, -40,
      -76, -454, 68}, {-184, 176, -40, 572, -184, -40, 428, -184, -58, -76,
      -40, -40, -40, -76, -76, 68, 68, -454, -40, -58, 572, -76, 104, 68, 428,
      -184, -76, 50, 428, -76, -76, -40, -58, 428, -40, -184, -58, -13, 5, -76,
      -13, 50, -58, 5, -76, 428, -58, -76, -76, -40, 5, -13, -58, -58, 50, 572,
      -13, 554, -76, 428, -58, 5, -13, -13, -76, 50, 104, -58, 68, -76, -40,
      572, -184, 428, 428, -76, -76, 50, 428, 554, -76, 572, -13, 50, -76, -13,
      50, 50, 68, -454, 572, 50, 50, -454, -454, 3641, -40, -58, -76, 5, -58,
      -13, -76, -13, 68, -76, 5, -13, 68, -13, -454, 50, -76, 50, -40, -76,
      -76, 68, 68, -454}, {-454, 50, 68, -13, 50, -76, 68, -76, -76, -13, -13,
      5, -76, 5, -40, -58, -13, -58, -454, 68, 68, -76, -76, -40, 68, -76, 104,
      -58, 572, -40, -76, 428, 572, 50, 554, -13, -58, -58, -40, -76, -13, 5,
      50, -76, -13, -13, 5, -58, -76, 428, -58, 5, -76, -58, -40, -184, -40,
      -76, 428, -58, 428, -76, -184, 428, 50, -76, 50, -13, -76, 5, -13, -58,
      572, -76, -40, -58, 68, 104, -40, -40, -184, 428, -76, -58, -184, -40,
      176, -184, 572, -40, -454, 68, 68, -76, -76, -40, 3641, -454, -454, 50,
      50, 572, -454, 68, 50, 50, -13, -76, 50, -13, 572, -76, 554, 428, 50,
      -76, -76, 428, 428, -184}, {68, -13, -454, 50, -76, 50, -76, 5, -40, -58,
      -13, -58, 68, -76, -76, -13, -13, 5, 68, -454, -76, -40, 68, -76, 104,
      -58, 68, -76, -40, 572, -58, -58, -40, -76, -13, 5, -76, 428, 572, 50,
      554, -13, -76, 50, 5, -58, -13, -13, 572, -76, -40, -58, 68, 104, -40,
      -40, -184, 428, -76, -58, -184, -40, 176, -184, 572, -40, -454, 68, 68,
      -76, -76, -40, -76, 428, -58, 5, -76, -58, -40, -184, -40, -76, 428, -58,
      428, -76, -184, 428, 50, -76, 50, -13, -76, 5, -13, -58, -454, 3641, 50,
      572, -454, 50, 68, -454, -13, -76, 50, 50, 50, -76, -76, 428, 428, -184,
      50, -13, 572, -76, 554, 428}, {68, -76, -76, -13, -13, 5, -454, 50, 68,
      -13, 50, -76, -40, -58, -76, 5, -58, -13, 68, -76, -454, 68, -40, -76,
      -76, 428, -58, 5, -76, -58, -40, -184, -40, -76, 428, -58, 428, -76,
      -184, 428, 50, -76, 50, -13, -76, 5, -13, -58, 68, -76, 104, -58, 572,
      -40, -76, 428, 572, 50, 554, -13, -58, -58, -40, -76, -13, 5, 50, -76,
      -13, -13, 5, -58, -40, -58, 572, -76, 104, 68, -184, -40, 176, -184, 572,
      -40, -40, -40, -184, 428, -76, -58, 68, -454, -76, -40, 68, -76, -454,
      50, 3641, -454, 572, 50, 50, -13, 572, -76, 554, 428, -454, 68, 50, 50,
      -13, -76, -76, 50, 428, -184, -76, 428}, {-76, 5, -40, -58, -13, -58, 68,
      -13, -454, 50, -76, 50, -76, -13, 68, -76, 5, -13, -76, -40, 68, -454,
      -76, 68, 572, -76, -40, -58, 68, 104, -40, -40, -184, 428, -76, -58,
      -184, -40, 176, -184, 572, -40, -454, 68, 68, -76, -76, -40, 104, -58,
      68, -76, -40, 572, -58, -58, -40, -76, -13, 5, -76, 428, 572, 50, 554,
      -13, -76, 50, 5, -58, -13, -13, -58, 5, -76, 428, -58, -76, 428, -76,
      -184, 428, 50, -76, -40, -184, -40, -76, 428, -58, -13, 50, -13, -58,
      -76, 5, 50, 572, -454, 3641, 50, -454, 50, -76, -76, 428, 428, -184, 68,
      -454, -13, -76, 50, 50, -13, 50, 554, 428, 572, -76}, {-76, -13, 68, -76,
      5, -13, -40, -58, -76, 5, -58, -13, -454, 50, 68, -13, 50, -76, -76, 68,
      -40, -76, -454, 68, -58, 5, -76, 428, -58, -76, 428, -76, -184, 428, 50,
      -76, -40, -184, -40, -76, 428, -58, -13, 50, -13, -58, -76, 5, -40, -58,
      572, -76, 104, 68, -184, -40, 176, -184, 572, -40, -40, -40, -184, 428,
      -76, -58, 68, -454, -76, -40, 68, -76, 68, -76, 104, -58, 572, -40, -76,
      428, 572, 50, 554, -13, -58, -58, -40, -76, -13, 5, 50, -76, -13, -13, 5,
      -58, 50, -454, 572, 50, 3641, -454, -13, 50, 554, 428, 572, -76, -76, 50,
      428, -184, -76, 428, -454, 68, 50, 50, -13, -76}, {-40, -58, -76, 5, -58,
      -13, -76, -13, 68, -76, 5, -13, 68, -13, -454, 50, -76, 50, -40, -76,
      -76, 68, 68, -454, -40, -58, 572, -76, 104, 68, -184, -40, 176, -184,
      572, -40, -40, -40, -184, 428, -76, -58, 68, -454, -76, -40, 68, -76,
      -58, 5, -76, 428, -58, -76, 428, -76, -184, 428, 50, -76, -40, -184, -40,
      -76, 428, -58, -13, 50, -13, -58, -76, 5, 104, -58, 68, -76, -40, 572,
      -58, -58, -40, -76, -13, 5, -76, 428, 572, 50, 554, -13, -76, 50, 5, -58,
      -13, -13, 572, 50, 50, -454, -454, 3641, -76, 50, 428, -184, -76, 428,
      -13, 50, 554, 428, 572, -76, 68, -454, -13, -76, 50, 50}, {68, -76, 104,
      -58, 572, -40, -76, 428, 572, 50, 554, -13, -58, -58, -40, -76, -13, 5,
      50, -76, -13, -13, 5, -58, -454, 50, 68, -13, 50, -76, 68, -76, -76, -13,
      -13, 5, -76, 5, -40, -58, -13, -58, -454, 68, 68, -76, -76, -40, -40,
      -184, -40, -76, 428, -58, -76, 428, -58, 5, -76, -58, -184, 428, 428,
      -76, -76, 50, -76, 5, 50, -13, -58, -13, -40, -40, -184, 428, -76, -58,
      572, -76, -40, -58, 68, 104, 176, -184, -184, -40, -40, 572, 68, -76,
      -454, 68, -40, -76, -454, 68, 50, 50, -13, -76, 3641, -454, -454, 50, 50,
      572, 572, -76, 50, -13, 428, 554, -76, 428, 50, -76, -184, 428}, {104,
      -58, 68, -76, -40, 572, -58, -58, -40, -76, -13, 5, -76, 428, 572, 50,
      554, -13, -76, 50, 5, -58, -13, -13, 68, -13, -454, 50, -76, 50, -76, 5,
      -40, -58, -13, -58, 68, -76, -76, -13, -13, 5, 68, -454, -76, -40, 68,
      -76, -40, -40, -184, 428, -76, -58, 572, -76, -40, -58, 68, 104, 176,
      -184, -184, -40, -40, 572, 68, -76, -454, 68, -40, -76, -40, -184, -40,
      -76, 428, -58, -76, 428, -58, 5, -76, -58, -184, 428, 428, -76, -76, 50,
      -76, 5, 50, -13, -58, -13, 68, -454, -13, -76, 50, 50, -454, 3641, 50,
      572, -454, 50, -76, 428, 50, -76, -184, 428, 572, -76, 50, -13, 428,
      554}, {-76, 428, -58, 5, -76, -58, -40, -184, -40, -76, 428, -58, 428,
      -76, -184, 428, 50, -76, 50, -13, -76, 5, -13, -58, 68, -76, -76, -13,
      -13, 5, -454, 50, 68, -13, 50, -76, -40, -58, -76, 5, -58, -13, 68, -76,
      -454, 68, -40, -76, -76, 428, 572, 50, 554, -13, 68, -76, 104, -58, 572,
      -40, -40, -76, -58, -58, 5, -13, -13, -13, 50, -76, -58, 5, -184, -40,
      176, -184, 572, -40, -40, -58, 572, -76, 104, 68, -184, 428, -40, -40,
      -58, -76, -76, -40, 68, -454, -76, 68, 50, -13, 572, -76, 554, 428, -454,
      50, 3641, -454, 572, 50, 50, 50, -454, 68, -76, -13, 428, -184, -76, 50,
      428, -76}, {572, -76, -40, -58, 68, 104, -40, -40, -184, 428, -76, -58,
      -184, -40, 176, -184, 572, -40, -454, 68, 68, -76, -76, -40, -76, 5, -40,
      -58, -13, -58, 68, -13, -454, 50, -76, 50, -76, -13, 68, -76, 5, -13,
      -76, -40, 68, -454, -76, 68, -58, -58, -40, -76, -13, 5, 104, -58, 68,
      -76, -40, 572, 572, 50, -76, 428, -13, 554, 5, -58, -76, 50, -13, -13,
      428, -76, -184, 428, 50, -76, -58, 5, -76, 428, -58, -76, -40, -76, -40,
      -184, -58, 428, -13, -58, -13, 50, 5, -76, 50, -76, -76, 428, 428, -184,
      50, 572, -454, 3641, 50, -454, -13, -76, 68, -454, 50, 50, 554, 428, -13,
      50, -76, 572}, {-58, 5, -76, 428, -58, -76, 428, -76, -184, 428, 50, -76,
      -40, -184, -40, -76, 428, -58, -13, 50, -13, -58, -76, 5, -76, -13, 68,
      -76, 5, -13, -40, -58, -76, 5, -58, -13, -454, 50, 68, -13, 50, -76, -76,
      68, -40, -76, -454, 68, -184, -40, 176, -184, 572, -40, -40, -58, 572,
      -76, 104, 68, -184, 428, -40, -40, -58, -76, -76, -40, 68, -454, -76, 68,
      -76, 428, 572, 50, 554, -13, 68, -76, 104, -58, 572, -40, -40, -76, -58,
      -58, 5, -13, -13, -13, 50, -76, -58, 5, -13, 50, 554, 428, 572, -76, 50,
      -454, 572, 50, 3641, -454, 428, -184, -76, 50, 428, -76, 50, 50, -454,
      68, -76, -13}, {-40, -58, 572, -76, 104, 68, -184, -40, 176, -184, 572,
      -40, -40, -40, -184, 428, -76, -58, 68, -454, -76, -40, 68, -76, -40,
      -58, -76, 5, -58, -13, -76, -13, 68, -76, 5, -13, 68, -13, -454, 50, -76,
      50, -40, -76, -76, 68, 68, -454, 428, -76, -184, 428, 50, -76, -58, 5,
      -76, 428, -58, -76, -40, -76, -40, -184, -58, 428, -13, -58, -13, 50, 5,
      -76, -58, -58, -40, -76, -13, 5, 104, -58, 68, -76, -40, 572, 572, 50,
      -76, 428, -13, 554, 5, -58, -76, 50, -13, -13, -76, 50, 428, -184, -76,
      428, 572, 50, 50, -454, -454, 3641, 554, 428, -13, 50, -76, 572, -13,
      -76, 68, -454, 50, 50}, {-76, 428, 572, 50, 554, -13, 68, -76, 104, -58,
      572, -40, -40, -76, -58, -58, 5, -13, -13, -13, 50, -76, -58, 5, -40,
      -184, -40, -76, 428, -58, -76, 428, -58, 5, -76, -58, -184, 428, 428,
      -76, -76, 50, -76, 5, 50, -13, -58, -13, -454, 50, 68, -13, 50, -76, 68,
      -76, -76, -13, -13, 5, -76, 5, -40, -58, -13, -58, -454, 68, 68, -76,
      -76, -40, -184, 428, -40, -40, -58, -76, 176, -184, -184, -40, -40, 572,
      572, -76, -40, -58, 68, 104, -76, 68, -40, -76, -454, 68, 50, 50, -454,
      68, -76, -13, 572, -76, 50, -13, 428, 554, 3641, -454, -454, 50, 50, 572,
      428, -76, -184, 428, 50, -76}, {-58, -58, -40, -76, -13, 5, 104, -58, 68,
      -76, -40, 572, 572, 50, -76, 428, -13, 554, 5, -58, -76, 50, -13, -13,
      -40, -40, -184, 428, -76, -58, 572, -76, -40, -58, 68, 104, 176, -184,
      -184, -40, -40, 572, 68, -76, -454, 68, -40, -76, 68, -13, -454, 50, -76,
      50, -76, 5, -40, -58, -13, -58, 68, -76, -76, -13, -13, 5, 68, -454, -76,
      -40, 68, -76, -40, -76, -40, -184, -58, 428, -184, 428, 428, -76, -76,
      50, -76, 428, -58, 5, -76, -58, 5, -76, -58, -13, 50, -13, -13, -76, 68,
      -454, 50, 50, -76, 428, 50, -76, -184, 428, -454, 3641, 50, 572, -454,
      50, -76, 572, 428, 554, 50, -13}, {-40, -184, -40, -76, 428, -58, -76,
      428, -58, 5, -76, -58, -184, 428, 428, -76, -76, 50, -76, 5, 50, -13,
      -58, -13, -76, 428, 572, 50, 554, -13, 68, -76, 104, -58, 572, -40, -40,
      -76, -58, -58, 5, -13, -13, -13, 50, -76, -58, 5, 68, -76, -76, -13, -13,
      5, -454, 50, 68, -13, 50, -76, -40, -58, -76, 5, -58, -13, 68, -76, -454,
      68, -40, -76, 176, -184, -184, -40, -40, 572, -184, 428, -40, -40, -58,
      -76, -40, -58, 572, -76, 104, 68, -40, -76, -76, 68, 68, -454, 572, -76,
      50, -13, 428, 554, 50, 50, -454, 68, -76, -13, -454, 50, 3641, -454, 572,
      50, -184, 428, 428, -76, -76, 50}, {-40, -40, -184, 428, -76, -58, 572,
      -76, -40, -58, 68, 104, 176, -184, -184, -40, -40, 572, 68, -76, -454,
      68, -40, -76, -58, -58, -40, -76, -13, 5, 104, -58, 68, -76, -40, 572,
      572, 50, -76, 428, -13, 554, 5, -58, -76, 50, -13, -13, -76, 5, -40, -58,
      -13, -58, 68, -13, -454, 50, -76, 50, -76, -13, 68, -76, 5, -13, -76,
      -40, 68, -454, -76, 68, -184, 428, 428, -76, -76, 50, -40, -76, -40,
      -184, -58, 428, -58, 5, -76, 428, -58, -76, -58, -13, 5, -76, -13, 50,
      -76, 428, 50, -76, -184, 428, -13, -76, 68, -454, 50, 50, 50, 572, -454,
      3641, 50, -454, 428, 554, -76, 572, -13, 50}, {428, -76, -184, 428, 50,
      -76, -58, 5, -76, 428, -58, -76, -40, -76, -40, -184, -58, 428, -13, -58,
      -13, 50, 5, -76, -184, -40, 176, -184, 572, -40, -40, -58, 572, -76, 104,
      68, -184, 428, -40, -40, -58, -76, -76, -40, 68, -454, -76, 68, -76, -13,
      68, -76, 5, -13, -40, -58, -76, 5, -58, -13, -454, 50, 68, -13, 50, -76,
      -76, 68, -40, -76, -454, 68, 572, 50, -76, 428, -13, 554, -40, -76, -58,
      -58, 5, -13, 68, -76, 104, -58, 572, -40, -13, -13, -58, 5, 50, -76, 554,
      428, -13, 50, -76, 572, 428, -184, -76, 50, 428, -76, 50, -454, 572, 50,
      3641, -454, 50, 50, -76, -13, -454, 68}, {-184, -40, 176, -184, 572, -40,
      -40, -58, 572, -76, 104, 68, -184, 428, -40, -40, -58, -76, -76, -40, 68,
      -454, -76, 68, 428, -76, -184, 428, 50, -76, -58, 5, -76, 428, -58, -76,
      -40, -76, -40, -184, -58, 428, -13, -58, -13, 50, 5, -76, -40, -58, -76,
      5, -58, -13, -76, -13, 68, -76, 5, -13, 68, -13, -454, 50, -76, 50, -40,
      -76, -76, 68, 68, -454, -40, -76, -58, -58, 5, -13, 572, 50, -76, 428,
      -13, 554, 104, -58, 68, -76, -40, 572, -58, 5, -13, -13, -76, 50, 428,
      -184, -76, 50, 428, -76, 554, 428, -13, 50, -76, 572, 572, 50, 50, -454,
      -454, 3641, -76, -13, 50, 50, 68, -454}, {572, 50, -76, 428, -13, 554,
      -40, -76, -58, -58, 5, -13, 68, -76, 104, -58, 572, -40, -13, -13, -58,
      5, 50, -76, -40, -76, -40, -184, -58, 428, -184, 428, 428, -76, -76, 50,
      -76, 428, -58, 5, -76, -58, 5, -76, -58, -13, 50, -13, -184, 428, -40,
      -40, -58, -76, 176, -184, -184, -40, -40, 572, 572, -76, -40, -58, 68,
      104, -76, 68, -40, -76, -454, 68, -454, 50, 68, -13, 50, -76, 68, -76,
      -76, -13, -13, 5, -76, 5, -40, -58, -13, -58, -454, 68, 68, -76, -76,
      -40, 50, 50, -76, -13, -454, 68, -76, 572, 428, 554, 50, -13, 428, -76,
      -184, 428, 50, -76, 3641, -454, -454, 50, 50, 572}, {-40, -76, -58, -58,
      5, -13, 572, 50, -76, 428, -13, 554, 104, -58, 68, -76, -40, 572, -58, 5,
      -13, -13, -76, 50, -184, 428, -40, -40, -58, -76, 176, -184, -184, -40,
      -40, 572, 572, -76, -40, -58, 68, 104, -76, 68, -40, -76, -454, 68, -40,
      -76, -40, -184, -58, 428, -184, 428, 428, -76, -76, 50, -76, 428, -58, 5,
      -76, -58, 5, -76, -58, -13, 50, -13, 68, -13, -454, 50, -76, 50, -76, 5,
      -40, -58, -13, -58, 68, -76, -76, -13, -13, 5, 68, -454, -76, -40, 68,
      -76, -76, -13, 50, 50, 68, -454, 428, -76, -184, 428, 50, -76, -76, 572,
      428, 554, 50, -13, -454, 3641, 50, 572, -454, 50}, {-40, -76, -40, -184,
      -58, 428, -184, 428, 428, -76, -76, 50, -76, 428, -58, 5, -76, -58, 5,
      -76, -58, -13, 50, -13, 572, 50, -76, 428, -13, 554, -40, -76, -58, -58,
      5, -13, 68, -76, 104, -58, 572, -40, -13, -13, -58, 5, 50, -76, 176,
      -184, -184, -40, -40, 572, -184, 428, -40, -40, -58, -76, -40, -58, 572,
      -76, 104, 68, -40, -76, -76, 68, 68, -454, 68, -76, -76, -13, -13, 5,
      -454, 50, 68, -13, 50, -76, -40, -58, -76, 5, -58, -13, 68, -76, -454,
      68, -40, -76, -76, 572, 428, 554, 50, -13, 50, 50, -76, -13, -454, 68,
      -184, 428, 428, -76, -76, 50, -454, 50, 3641, -454, 572, 50}, {-184, 428,
      -40, -40, -58, -76, 176, -184, -184, -40, -40, 572, 572, -76, -40, -58,
      68, 104, -76, 68, -40, -76, -454, 68, -40, -76, -58, -58, 5, -13, 572,
      50, -76, 428, -13, 554, 104, -58, 68, -76, -40, 572, -58, 5, -13, -13,
      -76, 50, -184, 428, 428, -76, -76, 50, -40, -76, -40, -184, -58, 428,
      -58, 5, -76, 428, -58, -76, -58, -13, 5, -76, -13, 50, -76, 5, -40, -58,
      -13, -58, 68, -13, -454, 50, -76, 50, -76, -13, 68, -76, 5, -13, -76,
      -40, 68, -454, -76, 68, 428, -76, -184, 428, 50, -76, -76, -13, 50, 50,
      68, -454, 428, 554, -76, 572, -13, 50, 50, 572, -454, 3641, 50, -454},
      {-184, 428, 428, -76, -76, 50, -40, -76, -40, -184, -58, 428, -58, 5,
      -76, 428, -58, -76, -58, -13, 5, -76, -13, 50, 176, -184, -184, -40, -40,
      572, -184, 428, -40, -40, -58, -76, -40, -58, 572, -76, 104, 68, -40,
      -76, -76, 68, 68, -454, 572, 50, -76, 428, -13, 554, -40, -76, -58, -58,
      5, -13, 68, -76, 104, -58, 572, -40, -13, -13, -58, 5, 50, -76, -76, -13,
      68, -76, 5, -13, -40, -58, -76, 5, -58, -13, -454, 50, 68, -13, 50, -76,
      -76, 68, -40, -76, -454, 68, 428, 554, -76, 572, -13, 50, -184, 428, 428,
      -76, -76, 50, 50, 50, -76, -13, -454, 68, 50, -454, 572, 50, 3641, -454},
      {176, -184, -184, -40, -40, 572, -184, 428, -40, -40, -58, -76, -40, -58,
      572, -76, 104, 68, -40, -76, -76, 68, 68, -454, -184, 428, 428, -76, -76,
      50, -40, -76, -40, -184, -58, 428, -58, 5, -76, 428, -58, -76, -58, -13,
      5, -76, -13, 50, -40, -76, -58, -58, 5, -13, 572, 50, -76, 428, -13, 554,
      104, -58, 68, -76, -40, 572, -58, 5, -13, -13, -76, 50, -40, -58, -76, 5,
      -58, -13, -76, -13, 68, -76, 5, -13, 68, -13, -454, 50, -76, 50, -40,
      -76, -76, 68, 68, -454, -184, 428, 428, -76, -76, 50, 428, 554, -76, 572,
      -13, 50, -76, -13, 50, 50, 68, -454, 572, 50, 50, -454, -454, 3641}};

  // Calculate color flows
  jamp[0] = +2. * (+amp[0] - amp[2] - amp[4] + amp[5] + amp[18] - amp[20] +
      amp[21] - amp[22] + amp[28] - amp[30] + amp[31] - amp[35] + amp[33] -
      amp[44] + amp[42] - amp[180] + amp[182] + amp[184] - amp[185] - amp[198]
      + amp[200] - amp[201] + amp[202] - amp[208] + amp[210] - amp[211] +
      amp[215] - amp[213] + amp[224] - amp[222] + amp[225] - amp[227] +
      amp[228] + amp[235] - amp[237] + amp[238] - amp[239] - amp[242] +
      amp[240] + amp[297] - amp[299] - amp[301] + amp[312] - amp[314] +
      amp[318] - amp[320] + amp[321] - amp[323] - amp[327] + amp[329] +
      amp[367] + amp[368] + amp[373] + amp[374] - amp[380] + amp[381] -
      amp[383] + amp[378] - amp[475] - amp[474] - amp[481] - amp[480] +
      amp[488] - amp[483] + amp[485] - amp[486] + amp[492] - amp[494]);
  jamp[1] = +2. * (-amp[0] - amp[1] - amp[3] - amp[5] + amp[23] - amp[25] +
      amp[26] - amp[27] - amp[28] + amp[30] - amp[31] - amp[38] + amp[36] -
      amp[43] - amp[42] - amp[135] + amp[137] + amp[139] - amp[140] - amp[153]
      + amp[155] - amp[156] + amp[157] - amp[163] + amp[165] - amp[166] +
      amp[170] - amp[168] + amp[179] - amp[177] + amp[230] - amp[232] +
      amp[233] - amp[235] + amp[237] - amp[238] + amp[239] - amp[241] -
      amp[240] - amp[297] + amp[299] + amp[301] + amp[315] - amp[317] -
      amp[318] + amp[320] + amp[322] - amp[328] + amp[323] - amp[329] +
      amp[349] + amp[350] + amp[355] + amp[356] - amp[362] + amp[363] -
      amp[365] + amp[360] + amp[474] - amp[476] + amp[480] - amp[482] +
      amp[483] - amp[485] - amp[489] + amp[491] - amp[492] + amp[494]);
  jamp[2] = +2. * (+amp[6] - amp[8] - amp[10] + amp[11] - amp[18] + amp[20] -
      amp[21] + amp[22] - amp[23] - amp[24] - amp[26] - amp[34] - amp[33] +
      amp[44] + amp[43] + amp[180] + amp[181] + amp[183] + amp[185] - amp[203]
      + amp[205] - amp[206] + amp[207] + amp[208] - amp[210] + amp[211] +
      amp[218] - amp[216] + amp[223] + amp[222] - amp[225] + amp[227] -
      amp[228] - amp[230] - amp[231] - amp[233] - amp[234] + amp[242] +
      amp[241] + amp[302] - amp[304] - amp[306] - amp[312] + amp[314] -
      amp[315] + amp[317] - amp[322] - amp[321] + amp[328] + amp[327] +
      amp[370] + amp[371] - amp[373] - amp[374] + amp[380] + amp[382] +
      amp[383] + amp[379] - amp[457] - amp[456] - amp[463] - amp[462] +
      amp[470] - amp[465] + amp[467] - amp[468] + amp[495] - amp[497]);
  jamp[3] = +2. * (-amp[6] - amp[7] - amp[9] - amp[11] + amp[23] + amp[24] +
      amp[26] - amp[28] - amp[29] - amp[31] - amp[32] - amp[41] + amp[39] -
      amp[43] - amp[42] - amp[90] + amp[92] + amp[94] - amp[95] - amp[108] +
      amp[110] - amp[111] + amp[112] - amp[118] + amp[120] - amp[121] +
      amp[125] - amp[123] + amp[134] - amp[132] + amp[230] + amp[231] +
      amp[233] + amp[234] - amp[235] - amp[236] - amp[238] - amp[241] -
      amp[240] - amp[302] + amp[304] + amp[306] + amp[315] - amp[317] -
      amp[318] + amp[320] + amp[322] - amp[328] + amp[323] - amp[329] +
      amp[331] + amp[332] + amp[337] + amp[338] - amp[344] + amp[345] -
      amp[347] + amp[342] + amp[456] - amp[458] + amp[462] - amp[464] +
      amp[465] - amp[467] - amp[471] + amp[473] - amp[495] + amp[497]);
  jamp[4] = +2. * (+amp[12] - amp[14] - amp[16] + amp[17] - amp[18] - amp[19] -
      amp[21] - amp[23] + amp[25] - amp[26] + amp[27] - amp[37] - amp[36] +
      amp[44] + amp[43] + amp[135] + amp[136] + amp[138] + amp[140] - amp[158]
      + amp[160] - amp[161] + amp[162] + amp[163] - amp[165] + amp[166] +
      amp[173] - amp[171] + amp[178] + amp[177] - amp[225] - amp[226] -
      amp[228] - amp[229] - amp[230] + amp[232] - amp[233] + amp[242] +
      amp[241] + amp[307] - amp[309] - amp[311] - amp[312] + amp[314] -
      amp[315] + amp[317] - amp[322] - amp[321] + amp[328] + amp[327] +
      amp[352] + amp[353] - amp[355] - amp[356] + amp[362] + amp[364] +
      amp[365] + amp[361] - amp[439] - amp[438] - amp[445] - amp[444] +
      amp[452] - amp[447] + amp[449] - amp[450] + amp[498] - amp[500]);
  jamp[5] = +2. * (-amp[12] - amp[13] - amp[15] - amp[17] + amp[18] + amp[19] +
      amp[21] + amp[28] + amp[29] + amp[31] + amp[32] - amp[40] - amp[39] -
      amp[44] + amp[42] + amp[90] + amp[91] + amp[93] + amp[95] - amp[113] +
      amp[115] - amp[116] + amp[117] + amp[118] - amp[120] + amp[121] +
      amp[128] - amp[126] + amp[133] + amp[132] + amp[225] + amp[226] +
      amp[228] + amp[229] + amp[235] + amp[236] + amp[238] - amp[242] +
      amp[240] - amp[307] + amp[309] + amp[311] + amp[312] - amp[314] +
      amp[318] - amp[320] + amp[321] - amp[323] - amp[327] + amp[329] +
      amp[334] + amp[335] - amp[337] - amp[338] + amp[344] + amp[346] +
      amp[347] + amp[343] + amp[438] - amp[440] + amp[444] - amp[446] +
      amp[447] - amp[449] - amp[453] + amp[455] - amp[498] + amp[500]);
  jamp[6] = +2. * (-amp[0] + amp[2] + amp[4] - amp[5] + amp[7] + amp[8] +
      amp[9] + amp[10] + amp[29] + amp[30] + amp[32] + amp[35] + amp[34] +
      amp[41] - amp[39] - amp[186] + amp[188] + amp[190] - amp[191] + amp[198]
      - amp[200] + amp[201] - amp[202] + amp[203] + amp[204] + amp[206] +
      amp[214] + amp[213] - amp[224] - amp[223] + amp[243] - amp[245] +
      amp[246] + amp[253] - amp[255] + amp[256] - amp[257] - amp[260] +
      amp[258] - amp[297] + amp[299] + amp[301] + amp[330] - amp[332] +
      amp[336] - amp[338] + amp[339] - amp[341] - amp[345] + amp[347] -
      amp[367] - amp[368] - amp[370] - amp[371] - amp[382] - amp[381] -
      amp[378] - amp[379] - amp[478] - amp[477] + amp[481] + amp[480] -
      amp[484] - amp[488] - amp[485] - amp[487] - amp[493] - amp[492]);
  jamp[7] = +2. * (+amp[0] + amp[1] + amp[3] + amp[5] + amp[13] + amp[14] +
      amp[15] + amp[16] - amp[29] - amp[30] - amp[32] + amp[38] + amp[37] +
      amp[40] + amp[39] - amp[141] + amp[143] + amp[145] - amp[146] + amp[153]
      - amp[155] + amp[156] - amp[157] + amp[158] + amp[159] + amp[161] +
      amp[169] + amp[168] - amp[179] - amp[178] + amp[248] - amp[250] +
      amp[251] - amp[253] + amp[255] - amp[256] + amp[257] - amp[259] -
      amp[258] + amp[297] - amp[299] - amp[301] + amp[333] - amp[335] -
      amp[336] + amp[338] + amp[340] - amp[346] + amp[341] - amp[347] -
      amp[349] - amp[350] - amp[352] - amp[353] - amp[364] - amp[363] -
      amp[360] - amp[361] + amp[477] - amp[479] - amp[480] + amp[482] +
      amp[484] - amp[490] + amp[485] - amp[491] + amp[493] + amp[492]);
  jamp[8] = +2. * (-amp[7] - amp[8] - amp[9] - amp[10] - amp[12] - amp[13] -
      amp[15] - amp[17] + amp[19] + amp[20] + amp[22] - amp[34] - amp[33] -
      amp[41] - amp[40] + amp[186] + amp[187] + amp[189] + amp[191] - amp[203]
      - amp[204] - amp[206] + amp[208] + amp[209] + amp[211] + amp[212] +
      amp[221] - amp[219] + amp[223] + amp[222] - amp[243] + amp[245] -
      amp[246] - amp[248] - amp[249] - amp[251] - amp[252] + amp[260] +
      amp[259] - amp[307] - amp[308] - amp[310] - amp[330] + amp[332] -
      amp[333] + amp[335] - amp[340] - amp[339] + amp[346] + amp[345] +
      amp[370] + amp[371] - amp[373] - amp[374] + amp[380] + amp[382] +
      amp[383] + amp[379] - amp[421] - amp[420] - amp[427] - amp[426] +
      amp[434] - amp[429] + amp[431] - amp[432] + amp[501] - amp[503]);
  jamp[9] = +2. * (+amp[12] + amp[13] + amp[15] + amp[17] - amp[18] - amp[19] -
      amp[21] - amp[28] - amp[29] - amp[31] - amp[32] + amp[40] + amp[39] +
      amp[44] - amp[42] - amp[45] + amp[47] + amp[49] - amp[50] - amp[63] +
      amp[65] - amp[66] + amp[67] - amp[73] + amp[75] - amp[76] + amp[80] -
      amp[78] + amp[89] - amp[87] + amp[248] + amp[249] + amp[251] + amp[252] -
      amp[253] - amp[254] - amp[256] - amp[259] - amp[258] + amp[307] +
      amp[308] + amp[310] + amp[313] + amp[314] + amp[319] + amp[320] -
      amp[326] + amp[327] - amp[329] + amp[324] + amp[333] - amp[335] -
      amp[336] + amp[338] + amp[340] - amp[346] + amp[341] - amp[347] +
      amp[420] - amp[422] + amp[426] - amp[428] + amp[429] - amp[431] -
      amp[435] + amp[437] - amp[501] + amp[503]);
  jamp[10] = +2. * (-amp[6] - amp[7] - amp[9] - amp[11] - amp[13] - amp[14] -
      amp[15] - amp[16] + amp[24] + amp[25] + amp[27] - amp[37] - amp[36] -
      amp[41] - amp[40] + amp[141] + amp[142] + amp[144] + amp[146] - amp[158]
      - amp[159] - amp[161] + amp[163] + amp[164] + amp[166] + amp[167] +
      amp[176] - amp[174] + amp[178] + amp[177] - amp[243] - amp[244] -
      amp[246] - amp[247] - amp[248] + amp[250] - amp[251] + amp[260] +
      amp[259] - amp[302] - amp[303] - amp[305] - amp[330] + amp[332] -
      amp[333] + amp[335] - amp[340] - amp[339] + amp[346] + amp[345] +
      amp[352] + amp[353] - amp[355] - amp[356] + amp[362] + amp[364] +
      amp[365] + amp[361] - amp[403] - amp[402] - amp[409] - amp[408] +
      amp[416] - amp[411] + amp[413] - amp[414] + amp[504] - amp[506]);
  jamp[11] = +2. * (+amp[6] + amp[7] + amp[9] + amp[11] - amp[23] - amp[24] -
      amp[26] + amp[28] + amp[29] + amp[31] + amp[32] + amp[41] - amp[39] +
      amp[43] + amp[42] + amp[45] + amp[46] + amp[48] + amp[50] - amp[68] +
      amp[70] - amp[71] + amp[72] + amp[73] - amp[75] + amp[76] + amp[83] -
      amp[81] + amp[88] + amp[87] + amp[243] + amp[244] + amp[246] + amp[247] +
      amp[253] + amp[254] + amp[256] - amp[260] + amp[258] + amp[302] +
      amp[303] + amp[305] + amp[316] + amp[317] - amp[319] - amp[320] +
      amp[326] + amp[328] + amp[329] + amp[325] + amp[330] - amp[332] +
      amp[336] - amp[338] + amp[339] - amp[341] - amp[345] + amp[347] +
      amp[402] - amp[404] + amp[408] - amp[410] + amp[411] - amp[413] -
      amp[417] + amp[419] - amp[504] + amp[506]);
  jamp[12] = +2. * (+amp[1] + amp[2] + amp[3] + amp[4] - amp[6] + amp[8] +
      amp[10] - amp[11] + amp[24] + amp[25] + amp[27] + amp[35] + amp[34] +
      amp[38] - amp[36] - amp[192] + amp[194] + amp[196] - amp[197] + amp[198]
      + amp[199] + amp[201] + amp[203] - amp[205] + amp[206] - amp[207] +
      amp[217] + amp[216] - amp[224] - amp[223] + amp[261] - amp[263] +
      amp[264] + amp[271] - amp[273] + amp[274] - amp[275] - amp[278] +
      amp[276] - amp[302] + amp[304] + amp[306] + amp[348] - amp[350] +
      amp[354] - amp[356] + amp[357] - amp[359] - amp[363] + amp[365] -
      amp[367] - amp[368] - amp[370] - amp[371] - amp[382] - amp[381] -
      amp[378] - amp[379] - amp[460] - amp[459] + amp[463] + amp[462] -
      amp[466] - amp[470] - amp[467] - amp[469] - amp[496] - amp[495]);
  jamp[13] = +2. * (+amp[6] + amp[7] + amp[9] + amp[11] + amp[13] + amp[14] +
      amp[15] + amp[16] - amp[24] - amp[25] - amp[27] + amp[37] + amp[36] +
      amp[41] + amp[40] - amp[96] + amp[98] + amp[100] - amp[101] + amp[108] -
      amp[110] + amp[111] - amp[112] + amp[113] + amp[114] + amp[116] +
      amp[124] + amp[123] - amp[134] - amp[133] + amp[266] - amp[268] +
      amp[269] - amp[271] + amp[273] - amp[274] + amp[275] - amp[277] -
      amp[276] + amp[302] - amp[304] - amp[306] - amp[331] - amp[332] -
      amp[334] - amp[335] - amp[346] - amp[345] - amp[342] - amp[343] +
      amp[351] - amp[353] - amp[354] + amp[356] + amp[358] - amp[364] +
      amp[359] - amp[365] + amp[459] - amp[461] - amp[462] + amp[464] +
      amp[466] - amp[472] + amp[467] - amp[473] + amp[496] + amp[495]);
  jamp[14] = +2. * (-amp[1] - amp[2] - amp[3] - amp[4] + amp[12] - amp[14] -
      amp[16] + amp[17] - amp[19] - amp[20] - amp[22] - amp[35] + amp[33] -
      amp[38] - amp[37] + amp[192] + amp[193] + amp[195] + amp[197] - amp[198]
      - amp[199] - amp[201] - amp[208] - amp[209] - amp[211] - amp[212] +
      amp[220] + amp[219] + amp[224] - amp[222] - amp[261] + amp[263] -
      amp[264] - amp[266] - amp[267] - amp[269] - amp[270] + amp[278] +
      amp[277] + amp[307] + amp[308] + amp[310] - amp[348] + amp[350] -
      amp[351] + amp[353] - amp[358] - amp[357] + amp[364] + amp[363] +
      amp[367] + amp[368] + amp[373] + amp[374] - amp[380] + amp[381] -
      amp[383] + amp[378] - amp[424] - amp[423] + amp[427] + amp[426] -
      amp[430] - amp[434] - amp[431] - amp[433] - amp[502] - amp[501]);
  jamp[15] = +2. * (-amp[12] + amp[14] + amp[16] - amp[17] + amp[18] + amp[19]
      + amp[21] + amp[23] - amp[25] + amp[26] - amp[27] + amp[37] + amp[36] -
      amp[44] - amp[43] - amp[51] + amp[53] + amp[55] - amp[56] + amp[63] -
      amp[65] + amp[66] - amp[67] + amp[68] + amp[69] + amp[71] + amp[79] +
      amp[78] - amp[89] - amp[88] + amp[266] + amp[267] + amp[269] + amp[270] -
      amp[271] - amp[272] - amp[274] - amp[277] - amp[276] - amp[307] -
      amp[308] - amp[310] - amp[313] - amp[314] - amp[316] - amp[317] -
      amp[328] - amp[327] - amp[324] - amp[325] + amp[351] - amp[353] -
      amp[354] + amp[356] + amp[358] - amp[364] + amp[359] - amp[365] +
      amp[423] - amp[425] - amp[426] + amp[428] + amp[430] - amp[436] +
      amp[431] - amp[437] + amp[502] + amp[501]);
  jamp[16] = +2. * (-amp[0] - amp[1] - amp[3] - amp[5] - amp[13] - amp[14] -
      amp[15] - amp[16] + amp[29] + amp[30] + amp[32] - amp[38] - amp[37] -
      amp[40] - amp[39] + amp[96] + amp[97] + amp[99] + amp[101] - amp[113] -
      amp[114] - amp[116] + amp[118] + amp[119] + amp[121] + amp[122] +
      amp[131] - amp[129] + amp[133] + amp[132] - amp[261] - amp[262] -
      amp[264] - amp[265] - amp[266] + amp[268] - amp[269] + amp[278] +
      amp[277] - amp[297] - amp[298] - amp[300] + amp[334] + amp[335] -
      amp[337] - amp[338] + amp[344] + amp[346] + amp[347] + amp[343] -
      amp[348] + amp[350] - amp[351] + amp[353] - amp[358] - amp[357] +
      amp[364] + amp[363] - amp[385] - amp[384] - amp[391] - amp[390] +
      amp[398] - amp[393] + amp[395] - amp[396] + amp[507] - amp[509]);
  jamp[17] = +2. * (+amp[0] + amp[1] + amp[3] + amp[5] - amp[23] + amp[25] -
      amp[26] + amp[27] + amp[28] - amp[30] + amp[31] + amp[38] - amp[36] +
      amp[43] + amp[42] + amp[51] + amp[52] + amp[54] + amp[56] - amp[68] -
      amp[69] - amp[71] + amp[73] + amp[74] + amp[76] + amp[77] + amp[86] -
      amp[84] + amp[88] + amp[87] + amp[261] + amp[262] + amp[264] + amp[265] +
      amp[271] + amp[272] + amp[274] - amp[278] + amp[276] + amp[297] +
      amp[298] + amp[300] + amp[316] + amp[317] - amp[319] - amp[320] +
      amp[326] + amp[328] + amp[329] + amp[325] + amp[348] - amp[350] +
      amp[354] - amp[356] + amp[357] - amp[359] - amp[363] + amp[365] +
      amp[384] - amp[386] + amp[390] - amp[392] + amp[393] - amp[395] -
      amp[399] + amp[401] - amp[507] + amp[509]);
  jamp[18] = +2. * (+amp[1] + amp[2] + amp[3] + amp[4] - amp[12] + amp[14] +
      amp[16] - amp[17] + amp[19] + amp[20] + amp[22] + amp[35] - amp[33] +
      amp[38] + amp[37] - amp[147] + amp[149] + amp[151] - amp[152] + amp[153]
      + amp[154] + amp[156] + amp[158] - amp[160] + amp[161] - amp[162] +
      amp[172] + amp[171] - amp[179] - amp[178] + amp[279] - amp[281] +
      amp[282] + amp[289] - amp[291] + amp[292] - amp[293] - amp[296] +
      amp[294] - amp[307] + amp[309] + amp[311] - amp[349] - amp[350] -
      amp[352] - amp[353] - amp[364] - amp[363] - amp[360] - amp[361] +
      amp[366] - amp[368] + amp[372] - amp[374] + amp[375] - amp[377] -
      amp[381] + amp[383] - amp[442] - amp[441] + amp[445] + amp[444] -
      amp[448] - amp[452] - amp[449] - amp[451] - amp[499] - amp[498]);
  jamp[19] = +2. * (+amp[7] + amp[8] + amp[9] + amp[10] + amp[12] + amp[13] +
      amp[15] + amp[17] - amp[19] - amp[20] - amp[22] + amp[34] + amp[33] +
      amp[41] + amp[40] - amp[102] + amp[104] + amp[106] - amp[107] + amp[108]
      + amp[109] + amp[111] + amp[113] - amp[115] + amp[116] - amp[117] +
      amp[127] + amp[126] - amp[134] - amp[133] + amp[284] - amp[286] +
      amp[287] - amp[289] + amp[291] - amp[292] + amp[293] - amp[295] -
      amp[294] + amp[307] - amp[309] - amp[311] - amp[331] - amp[332] -
      amp[334] - amp[335] - amp[346] - amp[345] - amp[342] - amp[343] +
      amp[369] - amp[371] - amp[372] + amp[374] + amp[376] - amp[382] +
      amp[377] - amp[383] + amp[441] - amp[443] - amp[444] + amp[446] +
      amp[448] - amp[454] + amp[449] - amp[455] + amp[499] + amp[498]);
  jamp[20] = +2. * (-amp[1] - amp[2] - amp[3] - amp[4] + amp[6] - amp[8] -
      amp[10] + amp[11] - amp[24] - amp[25] - amp[27] - amp[35] - amp[34] -
      amp[38] + amp[36] + amp[147] + amp[148] + amp[150] + amp[152] - amp[153]
      - amp[154] - amp[156] - amp[163] - amp[164] - amp[166] - amp[167] +
      amp[175] + amp[174] + amp[179] - amp[177] - amp[279] + amp[281] -
      amp[282] - amp[284] - amp[285] - amp[287] - amp[288] + amp[296] +
      amp[295] + amp[302] + amp[303] + amp[305] + amp[349] + amp[350] +
      amp[355] + amp[356] - amp[362] + amp[363] - amp[365] + amp[360] -
      amp[366] + amp[368] - amp[369] + amp[371] - amp[376] - amp[375] +
      amp[382] + amp[381] - amp[406] - amp[405] + amp[409] + amp[408] -
      amp[412] - amp[416] - amp[413] - amp[415] - amp[505] - amp[504]);
  jamp[21] = +2. * (-amp[6] + amp[8] + amp[10] - amp[11] + amp[18] - amp[20] +
      amp[21] - amp[22] + amp[23] + amp[24] + amp[26] + amp[34] + amp[33] -
      amp[44] - amp[43] - amp[57] + amp[59] + amp[61] - amp[62] + amp[63] +
      amp[64] + amp[66] + amp[68] - amp[70] + amp[71] - amp[72] + amp[82] +
      amp[81] - amp[89] - amp[88] + amp[284] + amp[285] + amp[287] + amp[288] -
      amp[289] - amp[290] - amp[292] - amp[295] - amp[294] - amp[302] -
      amp[303] - amp[305] - amp[313] - amp[314] - amp[316] - amp[317] -
      amp[328] - amp[327] - amp[324] - amp[325] + amp[369] - amp[371] -
      amp[372] + amp[374] + amp[376] - amp[382] + amp[377] - amp[383] +
      amp[405] - amp[407] - amp[408] + amp[410] + amp[412] - amp[418] +
      amp[413] - amp[419] + amp[505] + amp[504]);
  jamp[22] = +2. * (+amp[0] - amp[2] - amp[4] + amp[5] - amp[7] - amp[8] -
      amp[9] - amp[10] - amp[29] - amp[30] - amp[32] - amp[35] - amp[34] -
      amp[41] + amp[39] + amp[102] + amp[103] + amp[105] + amp[107] - amp[108]
      - amp[109] - amp[111] - amp[118] - amp[119] - amp[121] - amp[122] +
      amp[130] + amp[129] + amp[134] - amp[132] - amp[279] - amp[280] -
      amp[282] - amp[283] - amp[284] + amp[286] - amp[287] + amp[296] +
      amp[295] + amp[297] + amp[298] + amp[300] + amp[331] + amp[332] +
      amp[337] + amp[338] - amp[344] + amp[345] - amp[347] + amp[342] -
      amp[366] + amp[368] - amp[369] + amp[371] - amp[376] - amp[375] +
      amp[382] + amp[381] - amp[388] - amp[387] + amp[391] + amp[390] -
      amp[394] - amp[398] - amp[395] - amp[397] - amp[508] - amp[507]);
  jamp[23] = +2. * (-amp[0] + amp[2] + amp[4] - amp[5] - amp[18] + amp[20] -
      amp[21] + amp[22] - amp[28] + amp[30] - amp[31] + amp[35] - amp[33] +
      amp[44] - amp[42] + amp[57] + amp[58] + amp[60] + amp[62] - amp[63] -
      amp[64] - amp[66] - amp[73] - amp[74] - amp[76] - amp[77] + amp[85] +
      amp[84] + amp[89] - amp[87] + amp[279] + amp[280] + amp[282] + amp[283] +
      amp[289] + amp[290] + amp[292] - amp[296] + amp[294] - amp[297] -
      amp[298] - amp[300] + amp[313] + amp[314] + amp[319] + amp[320] -
      amp[326] + amp[327] - amp[329] + amp[324] + amp[366] - amp[368] +
      amp[372] - amp[374] + amp[375] - amp[377] - amp[381] + amp[383] +
      amp[387] - amp[389] - amp[390] + amp[392] + amp[394] - amp[400] +
      amp[395] - amp[401] + amp[508] + amp[507]);
  jamp[24] = +2. * (+amp[45] - amp[47] - amp[49] + amp[50] + amp[63] - amp[65]
      + amp[66] - amp[67] + amp[73] - amp[75] + amp[76] - amp[80] + amp[78] -
      amp[89] + amp[87] + amp[180] - amp[182] - amp[184] + amp[185] - amp[187]
      - amp[188] - amp[189] - amp[190] - amp[209] - amp[210] - amp[212] -
      amp[215] - amp[214] - amp[221] + amp[219] - amp[225] + amp[227] -
      amp[228] - amp[235] + amp[237] - amp[238] + amp[239] + amp[242] -
      amp[240] + amp[254] + amp[255] + amp[257] - amp[313] - amp[312] -
      amp[319] - amp[318] + amp[326] - amp[321] + amp[323] - amp[324] +
      amp[421] + amp[422] + amp[427] + amp[428] - amp[434] + amp[435] -
      amp[437] + amp[432] + amp[475] + amp[474] + amp[478] + amp[477] +
      amp[484] + amp[483] + amp[486] + amp[487] + amp[493] + amp[494]);
  jamp[25] = +2. * (-amp[45] - amp[46] - amp[48] - amp[50] + amp[68] - amp[70]
      + amp[71] - amp[72] - amp[73] + amp[75] - amp[76] - amp[83] + amp[81] -
      amp[88] - amp[87] + amp[135] - amp[137] - amp[139] + amp[140] - amp[142]
      - amp[143] - amp[144] - amp[145] - amp[164] - amp[165] - amp[167] -
      amp[170] - amp[169] - amp[176] + amp[174] - amp[230] + amp[232] -
      amp[233] + amp[235] - amp[237] + amp[238] - amp[239] + amp[241] +
      amp[240] - amp[254] - amp[255] - amp[257] - amp[316] - amp[315] +
      amp[319] + amp[318] - amp[322] - amp[326] - amp[323] - amp[325] +
      amp[403] + amp[404] + amp[409] + amp[410] - amp[416] + amp[417] -
      amp[419] + amp[414] - amp[474] + amp[476] - amp[477] + amp[479] -
      amp[484] - amp[483] + amp[490] + amp[489] - amp[493] - amp[494]);
  jamp[26] = +2. * (+amp[51] - amp[53] - amp[55] + amp[56] - amp[63] + amp[65]
      - amp[66] + amp[67] - amp[68] - amp[69] - amp[71] - amp[79] - amp[78] +
      amp[89] + amp[88] - amp[180] - amp[181] - amp[183] - amp[185] - amp[193]
      - amp[194] - amp[195] - amp[196] + amp[209] + amp[210] + amp[212] -
      amp[218] - amp[217] - amp[220] - amp[219] + amp[225] - amp[227] +
      amp[228] + amp[230] + amp[231] + amp[233] + amp[234] - amp[242] -
      amp[241] + amp[272] + amp[273] + amp[275] + amp[313] + amp[312] +
      amp[316] + amp[315] + amp[322] + amp[321] + amp[324] + amp[325] +
      amp[424] + amp[425] - amp[427] - amp[428] + amp[434] + amp[436] +
      amp[437] + amp[433] + amp[457] + amp[456] + amp[460] + amp[459] +
      amp[466] + amp[465] + amp[468] + amp[469] + amp[496] + amp[497]);
  jamp[27] = +2. * (-amp[51] - amp[52] - amp[54] - amp[56] + amp[68] + amp[69]
      + amp[71] - amp[73] - amp[74] - amp[76] - amp[77] - amp[86] + amp[84] -
      amp[88] - amp[87] + amp[90] - amp[92] - amp[94] + amp[95] - amp[97] -
      amp[98] - amp[99] - amp[100] - amp[119] - amp[120] - amp[122] - amp[125]
      - amp[124] - amp[131] + amp[129] - amp[230] - amp[231] - amp[233] -
      amp[234] + amp[235] + amp[236] + amp[238] + amp[241] + amp[240] -
      amp[272] - amp[273] - amp[275] - amp[316] - amp[315] + amp[319] +
      amp[318] - amp[322] - amp[326] - amp[323] - amp[325] + amp[385] +
      amp[386] + amp[391] + amp[392] - amp[398] + amp[399] - amp[401] +
      amp[396] - amp[456] + amp[458] - amp[459] + amp[461] - amp[466] -
      amp[465] + amp[472] + amp[471] - amp[496] - amp[497]);
  jamp[28] = +2. * (+amp[57] - amp[59] - amp[61] + amp[62] - amp[63] - amp[64]
      - amp[66] - amp[68] + amp[70] - amp[71] + amp[72] - amp[82] - amp[81] +
      amp[89] + amp[88] - amp[135] - amp[136] - amp[138] - amp[140] - amp[148]
      - amp[149] - amp[150] - amp[151] + amp[164] + amp[165] + amp[167] -
      amp[173] - amp[172] - amp[175] - amp[174] + amp[225] + amp[226] +
      amp[228] + amp[229] + amp[230] - amp[232] + amp[233] - amp[242] -
      amp[241] + amp[290] + amp[291] + amp[293] + amp[313] + amp[312] +
      amp[316] + amp[315] + amp[322] + amp[321] + amp[324] + amp[325] +
      amp[406] + amp[407] - amp[409] - amp[410] + amp[416] + amp[418] +
      amp[419] + amp[415] + amp[439] + amp[438] + amp[442] + amp[441] +
      amp[448] + amp[447] + amp[450] + amp[451] + amp[499] + amp[500]);
  jamp[29] = +2. * (-amp[57] - amp[58] - amp[60] - amp[62] + amp[63] + amp[64]
      + amp[66] + amp[73] + amp[74] + amp[76] + amp[77] - amp[85] - amp[84] -
      amp[89] + amp[87] - amp[90] - amp[91] - amp[93] - amp[95] - amp[103] -
      amp[104] - amp[105] - amp[106] + amp[119] + amp[120] + amp[122] -
      amp[128] - amp[127] - amp[130] - amp[129] - amp[225] - amp[226] -
      amp[228] - amp[229] - amp[235] - amp[236] - amp[238] + amp[242] -
      amp[240] - amp[290] - amp[291] - amp[293] - amp[313] - amp[312] -
      amp[319] - amp[318] + amp[326] - amp[321] + amp[323] - amp[324] +
      amp[388] + amp[389] - amp[391] - amp[392] + amp[398] + amp[400] +
      amp[401] + amp[397] - amp[438] + amp[440] - amp[441] + amp[443] -
      amp[448] - amp[447] + amp[454] + amp[453] - amp[499] - amp[500]);
  jamp[30] = +2. * (-amp[45] + amp[47] + amp[49] - amp[50] + amp[52] + amp[53]
      + amp[54] + amp[55] + amp[74] + amp[75] + amp[77] + amp[80] + amp[79] +
      amp[86] - amp[84] + amp[187] + amp[188] + amp[189] + amp[190] + amp[192]
      + amp[193] + amp[195] + amp[197] - amp[199] - amp[200] - amp[202] +
      amp[214] + amp[213] + amp[221] + amp[220] - amp[254] - amp[255] -
      amp[257] + amp[262] + amp[263] + amp[265] + amp[298] + amp[299] +
      amp[300] + amp[301] + amp[384] - amp[386] + amp[390] - amp[392] +
      amp[393] - amp[395] - amp[399] + amp[401] - amp[421] - amp[422] -
      amp[424] - amp[425] - amp[436] - amp[435] - amp[432] - amp[433] -
      amp[478] - amp[477] + amp[481] + amp[480] - amp[484] - amp[488] -
      amp[485] - amp[487] - amp[493] - amp[492] - amp[507] + amp[509]);
  jamp[31] = +2. * (+amp[45] + amp[46] + amp[48] + amp[50] + amp[58] + amp[59]
      + amp[60] + amp[61] - amp[74] - amp[75] - amp[77] + amp[83] + amp[82] +
      amp[85] + amp[84] + amp[142] + amp[143] + amp[144] + amp[145] + amp[147]
      + amp[148] + amp[150] + amp[152] - amp[154] - amp[155] - amp[157] +
      amp[169] + amp[168] + amp[176] + amp[175] + amp[254] + amp[255] +
      amp[257] + amp[280] + amp[281] + amp[283] - amp[298] - amp[299] -
      amp[300] - amp[301] + amp[387] - amp[389] - amp[390] + amp[392] +
      amp[394] - amp[400] + amp[395] - amp[401] - amp[403] - amp[404] -
      amp[406] - amp[407] - amp[418] - amp[417] - amp[414] - amp[415] +
      amp[477] - amp[479] - amp[480] + amp[482] + amp[484] - amp[490] +
      amp[485] - amp[491] + amp[493] + amp[492] + amp[508] + amp[507]);
  jamp[32] = +2. * (-amp[52] - amp[53] - amp[54] - amp[55] - amp[57] - amp[58]
      - amp[60] - amp[62] + amp[64] + amp[65] + amp[67] - amp[79] - amp[78] -
      amp[86] - amp[85] - amp[192] - amp[193] - amp[195] - amp[197] + amp[198]
      + amp[199] + amp[201] + amp[208] + amp[209] + amp[211] + amp[212] -
      amp[220] - amp[219] - amp[224] + amp[222] - amp[262] - amp[263] -
      amp[265] - amp[279] - amp[280] - amp[282] - amp[283] - amp[289] -
      amp[290] - amp[292] + amp[296] - amp[294] - amp[367] - amp[366] -
      amp[373] - amp[372] + amp[380] - amp[375] + amp[377] - amp[378] -
      amp[384] + amp[386] - amp[387] + amp[389] - amp[394] - amp[393] +
      amp[400] + amp[399] + amp[424] + amp[425] - amp[427] - amp[428] +
      amp[434] + amp[436] + amp[437] + amp[433] - amp[508] - amp[509]);
  jamp[33] = +2. * (-amp[0] + amp[2] + amp[4] - amp[5] - amp[18] + amp[20] -
      amp[21] + amp[22] - amp[28] + amp[30] - amp[31] + amp[35] - amp[33] +
      amp[44] - amp[42] + amp[57] + amp[58] + amp[60] + amp[62] - amp[63] -
      amp[64] - amp[66] - amp[73] - amp[74] - amp[76] - amp[77] + amp[85] +
      amp[84] + amp[89] - amp[87] + amp[279] + amp[280] + amp[282] + amp[283] +
      amp[289] + amp[290] + amp[292] - amp[296] + amp[294] - amp[297] -
      amp[298] - amp[300] + amp[313] + amp[314] + amp[319] + amp[320] -
      amp[326] + amp[327] - amp[329] + amp[324] + amp[366] - amp[368] +
      amp[372] - amp[374] + amp[375] - amp[377] - amp[381] + amp[383] +
      amp[387] - amp[389] - amp[390] + amp[392] + amp[394] - amp[400] +
      amp[395] - amp[401] + amp[508] + amp[507]);
  jamp[34] = +2. * (-amp[51] - amp[52] - amp[54] - amp[56] - amp[58] - amp[59]
      - amp[60] - amp[61] + amp[69] + amp[70] + amp[72] - amp[82] - amp[81] -
      amp[86] - amp[85] - amp[147] - amp[148] - amp[150] - amp[152] + amp[153]
      + amp[154] + amp[156] + amp[163] + amp[164] + amp[166] + amp[167] -
      amp[175] - amp[174] - amp[179] + amp[177] - amp[261] - amp[262] -
      amp[264] - amp[265] - amp[271] - amp[272] - amp[274] + amp[278] -
      amp[276] - amp[280] - amp[281] - amp[283] - amp[349] - amp[348] -
      amp[355] - amp[354] + amp[362] - amp[357] + amp[359] - amp[360] -
      amp[384] + amp[386] - amp[387] + amp[389] - amp[394] - amp[393] +
      amp[400] + amp[399] + amp[406] + amp[407] - amp[409] - amp[410] +
      amp[416] + amp[418] + amp[419] + amp[415] - amp[508] - amp[509]);
  jamp[35] = +2. * (+amp[0] + amp[1] + amp[3] + amp[5] - amp[23] + amp[25] -
      amp[26] + amp[27] + amp[28] - amp[30] + amp[31] + amp[38] - amp[36] +
      amp[43] + amp[42] + amp[51] + amp[52] + amp[54] + amp[56] - amp[68] -
      amp[69] - amp[71] + amp[73] + amp[74] + amp[76] + amp[77] + amp[86] -
      amp[84] + amp[88] + amp[87] + amp[261] + amp[262] + amp[264] + amp[265] +
      amp[271] + amp[272] + amp[274] - amp[278] + amp[276] + amp[297] +
      amp[298] + amp[300] + amp[316] + amp[317] - amp[319] - amp[320] +
      amp[326] + amp[328] + amp[329] + amp[325] + amp[348] - amp[350] +
      amp[354] - amp[356] + amp[357] - amp[359] - amp[363] + amp[365] +
      amp[384] - amp[386] + amp[390] - amp[392] + amp[393] - amp[395] -
      amp[399] + amp[401] - amp[507] + amp[509]);
  jamp[36] = +2. * (+amp[46] + amp[47] + amp[48] + amp[49] - amp[51] + amp[53]
      + amp[55] - amp[56] + amp[69] + amp[70] + amp[72] + amp[80] + amp[79] +
      amp[83] - amp[81] + amp[186] + amp[187] + amp[189] + amp[191] + amp[193]
      + amp[194] + amp[195] + amp[196] - amp[204] - amp[205] - amp[207] +
      amp[217] + amp[216] + amp[221] + amp[220] + amp[244] + amp[245] +
      amp[247] - amp[272] - amp[273] - amp[275] + amp[303] + amp[304] +
      amp[305] + amp[306] + amp[402] - amp[404] + amp[408] - amp[410] +
      amp[411] - amp[413] - amp[417] + amp[419] - amp[421] - amp[422] -
      amp[424] - amp[425] - amp[436] - amp[435] - amp[432] - amp[433] -
      amp[460] - amp[459] + amp[463] + amp[462] - amp[466] - amp[470] -
      amp[467] - amp[469] - amp[496] - amp[495] - amp[504] + amp[506]);
  jamp[37] = +2. * (+amp[51] + amp[52] + amp[54] + amp[56] + amp[58] + amp[59]
      + amp[60] + amp[61] - amp[69] - amp[70] - amp[72] + amp[82] + amp[81] +
      amp[86] + amp[85] + amp[97] + amp[98] + amp[99] + amp[100] + amp[102] +
      amp[103] + amp[105] + amp[107] - amp[109] - amp[110] - amp[112] +
      amp[124] + amp[123] + amp[131] + amp[130] + amp[272] + amp[273] +
      amp[275] + amp[285] + amp[286] + amp[288] - amp[303] - amp[304] -
      amp[305] - amp[306] - amp[385] - amp[386] - amp[388] - amp[389] -
      amp[400] - amp[399] - amp[396] - amp[397] + amp[405] - amp[407] -
      amp[408] + amp[410] + amp[412] - amp[418] + amp[413] - amp[419] +
      amp[459] - amp[461] - amp[462] + amp[464] + amp[466] - amp[472] +
      amp[467] - amp[473] + amp[496] + amp[495] + amp[505] + amp[504]);
  jamp[38] = +2. * (-amp[46] - amp[47] - amp[48] - amp[49] + amp[57] - amp[59]
      - amp[61] + amp[62] - amp[64] - amp[65] - amp[67] - amp[80] + amp[78] -
      amp[83] - amp[82] - amp[186] - amp[187] - amp[189] - amp[191] + amp[203]
      + amp[204] + amp[206] - amp[208] - amp[209] - amp[211] - amp[212] -
      amp[221] + amp[219] - amp[223] - amp[222] - amp[244] - amp[245] -
      amp[247] - amp[284] - amp[285] - amp[287] - amp[288] + amp[289] +
      amp[290] + amp[292] + amp[295] + amp[294] - amp[370] - amp[369] +
      amp[373] + amp[372] - amp[376] - amp[380] - amp[377] - amp[379] -
      amp[402] + amp[404] - amp[405] + amp[407] - amp[412] - amp[411] +
      amp[418] + amp[417] + amp[421] + amp[422] + amp[427] + amp[428] -
      amp[434] + amp[435] - amp[437] + amp[432] - amp[505] - amp[506]);
  jamp[39] = +2. * (-amp[6] + amp[8] + amp[10] - amp[11] + amp[18] - amp[20] +
      amp[21] - amp[22] + amp[23] + amp[24] + amp[26] + amp[34] + amp[33] -
      amp[44] - amp[43] - amp[57] + amp[59] + amp[61] - amp[62] + amp[63] +
      amp[64] + amp[66] + amp[68] - amp[70] + amp[71] - amp[72] + amp[82] +
      amp[81] - amp[89] - amp[88] + amp[284] + amp[285] + amp[287] + amp[288] -
      amp[289] - amp[290] - amp[292] - amp[295] - amp[294] - amp[302] -
      amp[303] - amp[305] - amp[313] - amp[314] - amp[316] - amp[317] -
      amp[328] - amp[327] - amp[324] - amp[325] + amp[369] - amp[371] -
      amp[372] + amp[374] + amp[376] - amp[382] + amp[377] - amp[383] +
      amp[405] - amp[407] - amp[408] + amp[410] + amp[412] - amp[418] +
      amp[413] - amp[419] + amp[505] + amp[504]);
  jamp[40] = +2. * (-amp[45] - amp[46] - amp[48] - amp[50] - amp[58] - amp[59]
      - amp[60] - amp[61] + amp[74] + amp[75] + amp[77] - amp[83] - amp[82] -
      amp[85] - amp[84] - amp[102] - amp[103] - amp[105] - amp[107] + amp[108]
      + amp[109] + amp[111] + amp[118] + amp[119] + amp[121] + amp[122] -
      amp[130] - amp[129] - amp[134] + amp[132] - amp[243] - amp[244] -
      amp[246] - amp[247] - amp[253] - amp[254] - amp[256] + amp[260] -
      amp[258] - amp[285] - amp[286] - amp[288] - amp[331] - amp[330] -
      amp[337] - amp[336] + amp[344] - amp[339] + amp[341] - amp[342] +
      amp[388] + amp[389] - amp[391] - amp[392] + amp[398] + amp[400] +
      amp[401] + amp[397] - amp[402] + amp[404] - amp[405] + amp[407] -
      amp[412] - amp[411] + amp[418] + amp[417] - amp[505] - amp[506]);
  jamp[41] = +2. * (+amp[6] + amp[7] + amp[9] + amp[11] - amp[23] - amp[24] -
      amp[26] + amp[28] + amp[29] + amp[31] + amp[32] + amp[41] - amp[39] +
      amp[43] + amp[42] + amp[45] + amp[46] + amp[48] + amp[50] - amp[68] +
      amp[70] - amp[71] + amp[72] + amp[73] - amp[75] + amp[76] + amp[83] -
      amp[81] + amp[88] + amp[87] + amp[243] + amp[244] + amp[246] + amp[247] +
      amp[253] + amp[254] + amp[256] - amp[260] + amp[258] + amp[302] +
      amp[303] + amp[305] + amp[316] + amp[317] - amp[319] - amp[320] +
      amp[326] + amp[328] + amp[329] + amp[325] + amp[330] - amp[332] +
      amp[336] - amp[338] + amp[339] - amp[341] - amp[345] + amp[347] +
      amp[402] - amp[404] + amp[408] - amp[410] + amp[411] - amp[413] -
      amp[417] + amp[419] - amp[504] + amp[506]);
  jamp[42] = +2. * (+amp[46] + amp[47] + amp[48] + amp[49] - amp[57] + amp[59]
      + amp[61] - amp[62] + amp[64] + amp[65] + amp[67] + amp[80] - amp[78] +
      amp[83] + amp[82] + amp[141] + amp[142] + amp[144] + amp[146] + amp[148]
      + amp[149] + amp[150] + amp[151] - amp[159] - amp[160] - amp[162] +
      amp[172] + amp[171] + amp[176] + amp[175] + amp[249] + amp[250] +
      amp[252] - amp[290] - amp[291] - amp[293] + amp[308] + amp[309] +
      amp[310] + amp[311] - amp[403] - amp[404] - amp[406] - amp[407] -
      amp[418] - amp[417] - amp[414] - amp[415] + amp[420] - amp[422] +
      amp[426] - amp[428] + amp[429] - amp[431] - amp[435] + amp[437] -
      amp[442] - amp[441] + amp[445] + amp[444] - amp[448] - amp[452] -
      amp[449] - amp[451] - amp[499] - amp[498] - amp[501] + amp[503]);
  jamp[43] = +2. * (+amp[52] + amp[53] + amp[54] + amp[55] + amp[57] + amp[58]
      + amp[60] + amp[62] - amp[64] - amp[65] - amp[67] + amp[79] + amp[78] +
      amp[86] + amp[85] + amp[96] + amp[97] + amp[99] + amp[101] + amp[103] +
      amp[104] + amp[105] + amp[106] - amp[114] - amp[115] - amp[117] +
      amp[127] + amp[126] + amp[131] + amp[130] + amp[267] + amp[268] +
      amp[270] + amp[290] + amp[291] + amp[293] - amp[308] - amp[309] -
      amp[310] - amp[311] - amp[385] - amp[386] - amp[388] - amp[389] -
      amp[400] - amp[399] - amp[396] - amp[397] + amp[423] - amp[425] -
      amp[426] + amp[428] + amp[430] - amp[436] + amp[431] - amp[437] +
      amp[441] - amp[443] - amp[444] + amp[446] + amp[448] - amp[454] +
      amp[449] - amp[455] + amp[499] + amp[498] + amp[502] + amp[501]);
  jamp[44] = +2. * (-amp[46] - amp[47] - amp[48] - amp[49] + amp[51] - amp[53]
      - amp[55] + amp[56] - amp[69] - amp[70] - amp[72] - amp[80] - amp[79] -
      amp[83] + amp[81] - amp[141] - amp[142] - amp[144] - amp[146] + amp[158]
      + amp[159] + amp[161] - amp[163] - amp[164] - amp[166] - amp[167] -
      amp[176] + amp[174] - amp[178] - amp[177] - amp[249] - amp[250] -
      amp[252] - amp[266] - amp[267] - amp[269] - amp[270] + amp[271] +
      amp[272] + amp[274] + amp[277] + amp[276] - amp[352] - amp[351] +
      amp[355] + amp[354] - amp[358] - amp[362] - amp[359] - amp[361] +
      amp[403] + amp[404] + amp[409] + amp[410] - amp[416] + amp[417] -
      amp[419] + amp[414] - amp[420] + amp[422] - amp[423] + amp[425] -
      amp[430] - amp[429] + amp[436] + amp[435] - amp[502] - amp[503]);
  jamp[45] = +2. * (-amp[12] + amp[14] + amp[16] - amp[17] + amp[18] + amp[19]
      + amp[21] + amp[23] - amp[25] + amp[26] - amp[27] + amp[37] + amp[36] -
      amp[44] - amp[43] - amp[51] + amp[53] + amp[55] - amp[56] + amp[63] -
      amp[65] + amp[66] - amp[67] + amp[68] + amp[69] + amp[71] + amp[79] +
      amp[78] - amp[89] - amp[88] + amp[266] + amp[267] + amp[269] + amp[270] -
      amp[271] - amp[272] - amp[274] - amp[277] - amp[276] - amp[307] -
      amp[308] - amp[310] - amp[313] - amp[314] - amp[316] - amp[317] -
      amp[328] - amp[327] - amp[324] - amp[325] + amp[351] - amp[353] -
      amp[354] + amp[356] + amp[358] - amp[364] + amp[359] - amp[365] +
      amp[423] - amp[425] - amp[426] + amp[428] + amp[430] - amp[436] +
      amp[431] - amp[437] + amp[502] + amp[501]);
  jamp[46] = +2. * (+amp[45] - amp[47] - amp[49] + amp[50] - amp[52] - amp[53]
      - amp[54] - amp[55] - amp[74] - amp[75] - amp[77] - amp[80] - amp[79] -
      amp[86] + amp[84] - amp[96] - amp[97] - amp[99] - amp[101] + amp[113] +
      amp[114] + amp[116] - amp[118] - amp[119] - amp[121] - amp[122] -
      amp[131] + amp[129] - amp[133] - amp[132] - amp[248] - amp[249] -
      amp[251] - amp[252] + amp[253] + amp[254] + amp[256] + amp[259] +
      amp[258] - amp[267] - amp[268] - amp[270] - amp[334] - amp[333] +
      amp[337] + amp[336] - amp[340] - amp[344] - amp[341] - amp[343] +
      amp[385] + amp[386] + amp[391] + amp[392] - amp[398] + amp[399] -
      amp[401] + amp[396] - amp[420] + amp[422] - amp[423] + amp[425] -
      amp[430] - amp[429] + amp[436] + amp[435] - amp[502] - amp[503]);
  jamp[47] = +2. * (+amp[12] + amp[13] + amp[15] + amp[17] - amp[18] - amp[19]
      - amp[21] - amp[28] - amp[29] - amp[31] - amp[32] + amp[40] + amp[39] +
      amp[44] - amp[42] - amp[45] + amp[47] + amp[49] - amp[50] - amp[63] +
      amp[65] - amp[66] + amp[67] - amp[73] + amp[75] - amp[76] + amp[80] -
      amp[78] + amp[89] - amp[87] + amp[248] + amp[249] + amp[251] + amp[252] -
      amp[253] - amp[254] - amp[256] - amp[259] - amp[258] + amp[307] +
      amp[308] + amp[310] + amp[313] + amp[314] + amp[319] + amp[320] -
      amp[326] + amp[327] - amp[329] + amp[324] + amp[333] - amp[335] -
      amp[336] + amp[338] + amp[340] - amp[346] + amp[341] - amp[347] +
      amp[420] - amp[422] + amp[426] - amp[428] + amp[429] - amp[431] -
      amp[435] + amp[437] - amp[501] + amp[503]);
  jamp[48] = +2. * (+amp[90] - amp[92] - amp[94] + amp[95] + amp[108] -
      amp[110] + amp[111] - amp[112] + amp[118] - amp[120] + amp[121] -
      amp[125] + amp[123] - amp[134] + amp[132] - amp[181] - amp[182] -
      amp[183] - amp[184] + amp[186] - amp[188] - amp[190] + amp[191] -
      amp[204] - amp[205] - amp[207] - amp[215] - amp[214] - amp[218] +
      amp[216] + amp[236] + amp[237] + amp[239] - amp[243] + amp[245] -
      amp[246] - amp[253] + amp[255] - amp[256] + amp[257] + amp[260] -
      amp[258] - amp[331] - amp[330] - amp[337] - amp[336] + amp[344] -
      amp[339] + amp[341] - amp[342] + amp[457] + amp[458] + amp[463] +
      amp[464] - amp[470] + amp[471] - amp[473] + amp[468] + amp[475] +
      amp[474] + amp[478] + amp[477] + amp[484] + amp[483] + amp[486] +
      amp[487] + amp[493] + amp[494]);
  jamp[49] = +2. * (-amp[90] - amp[91] - amp[93] - amp[95] + amp[113] -
      amp[115] + amp[116] - amp[117] - amp[118] + amp[120] - amp[121] -
      amp[128] + amp[126] - amp[133] - amp[132] - amp[136] - amp[137] -
      amp[138] - amp[139] + amp[141] - amp[143] - amp[145] + amp[146] -
      amp[159] - amp[160] - amp[162] - amp[170] - amp[169] - amp[173] +
      amp[171] - amp[236] - amp[237] - amp[239] - amp[248] + amp[250] -
      amp[251] + amp[253] - amp[255] + amp[256] - amp[257] + amp[259] +
      amp[258] - amp[334] - amp[333] + amp[337] + amp[336] - amp[340] -
      amp[344] - amp[341] - amp[343] + amp[439] + amp[440] + amp[445] +
      amp[446] - amp[452] + amp[453] - amp[455] + amp[450] - amp[474] +
      amp[476] - amp[477] + amp[479] - amp[484] - amp[483] + amp[490] +
      amp[489] - amp[493] - amp[494]);
  jamp[50] = +2. * (+amp[96] - amp[98] - amp[100] + amp[101] - amp[108] +
      amp[110] - amp[111] + amp[112] - amp[113] - amp[114] - amp[116] -
      amp[124] - amp[123] + amp[134] + amp[133] - amp[186] - amp[187] -
      amp[189] - amp[191] - amp[193] - amp[194] - amp[195] - amp[196] +
      amp[204] + amp[205] + amp[207] - amp[217] - amp[216] - amp[221] -
      amp[220] + amp[243] - amp[245] + amp[246] + amp[248] + amp[249] +
      amp[251] + amp[252] - amp[260] - amp[259] + amp[267] + amp[268] +
      amp[270] + amp[331] + amp[330] + amp[334] + amp[333] + amp[340] +
      amp[339] + amp[342] + amp[343] + amp[421] + amp[420] + amp[424] +
      amp[423] + amp[430] + amp[429] + amp[432] + amp[433] + amp[460] +
      amp[461] - amp[463] - amp[464] + amp[470] + amp[472] + amp[473] +
      amp[469] + amp[502] + amp[503]);
  jamp[51] = +2. * (+amp[45] - amp[47] - amp[49] + amp[50] - amp[52] - amp[53]
      - amp[54] - amp[55] - amp[74] - amp[75] - amp[77] - amp[80] - amp[79] -
      amp[86] + amp[84] - amp[96] - amp[97] - amp[99] - amp[101] + amp[113] +
      amp[114] + amp[116] - amp[118] - amp[119] - amp[121] - amp[122] -
      amp[131] + amp[129] - amp[133] - amp[132] - amp[248] - amp[249] -
      amp[251] - amp[252] + amp[253] + amp[254] + amp[256] + amp[259] +
      amp[258] - amp[267] - amp[268] - amp[270] - amp[334] - amp[333] +
      amp[337] + amp[336] - amp[340] - amp[344] - amp[341] - amp[343] +
      amp[385] + amp[386] + amp[391] + amp[392] - amp[398] + amp[399] -
      amp[401] + amp[396] - amp[420] + amp[422] - amp[423] + amp[425] -
      amp[430] - amp[429] + amp[436] + amp[435] - amp[502] - amp[503]);
  jamp[52] = +2. * (+amp[102] - amp[104] - amp[106] + amp[107] - amp[108] -
      amp[109] - amp[111] - amp[113] + amp[115] - amp[116] + amp[117] -
      amp[127] - amp[126] + amp[134] + amp[133] - amp[141] - amp[142] -
      amp[144] - amp[146] - amp[148] - amp[149] - amp[150] - amp[151] +
      amp[159] + amp[160] + amp[162] - amp[172] - amp[171] - amp[176] -
      amp[175] + amp[243] + amp[244] + amp[246] + amp[247] + amp[248] -
      amp[250] + amp[251] - amp[260] - amp[259] + amp[285] + amp[286] +
      amp[288] + amp[331] + amp[330] + amp[334] + amp[333] + amp[340] +
      amp[339] + amp[342] + amp[343] + amp[403] + amp[402] + amp[406] +
      amp[405] + amp[412] + amp[411] + amp[414] + amp[415] + amp[442] +
      amp[443] - amp[445] - amp[446] + amp[452] + amp[454] + amp[455] +
      amp[451] + amp[505] + amp[506]);
  jamp[53] = +2. * (-amp[45] - amp[46] - amp[48] - amp[50] - amp[58] - amp[59]
      - amp[60] - amp[61] + amp[74] + amp[75] + amp[77] - amp[83] - amp[82] -
      amp[85] - amp[84] - amp[102] - amp[103] - amp[105] - amp[107] + amp[108]
      + amp[109] + amp[111] + amp[118] + amp[119] + amp[121] + amp[122] -
      amp[130] - amp[129] - amp[134] + amp[132] - amp[243] - amp[244] -
      amp[246] - amp[247] - amp[253] - amp[254] - amp[256] + amp[260] -
      amp[258] - amp[285] - amp[286] - amp[288] - amp[331] - amp[330] -
      amp[337] - amp[336] + amp[344] - amp[339] + amp[341] - amp[342] +
      amp[388] + amp[389] - amp[391] - amp[392] + amp[398] + amp[400] +
      amp[401] + amp[397] - amp[402] + amp[404] - amp[405] + amp[407] -
      amp[412] - amp[411] + amp[418] + amp[417] - amp[505] - amp[506]);
  jamp[54] = +2. * (-amp[90] + amp[92] + amp[94] - amp[95] + amp[97] + amp[98]
      + amp[99] + amp[100] + amp[119] + amp[120] + amp[122] + amp[125] +
      amp[124] + amp[131] - amp[129] + amp[181] + amp[182] + amp[183] +
      amp[184] - amp[192] + amp[194] + amp[196] - amp[197] + amp[199] +
      amp[200] + amp[202] + amp[215] - amp[213] + amp[218] + amp[217] -
      amp[236] - amp[237] - amp[239] - amp[262] - amp[263] - amp[265] -
      amp[298] - amp[299] - amp[300] - amp[301] - amp[385] - amp[384] -
      amp[391] - amp[390] + amp[398] - amp[393] + amp[395] - amp[396] -
      amp[457] - amp[458] - amp[460] - amp[461] - amp[472] - amp[471] -
      amp[468] - amp[469] - amp[475] - amp[474] - amp[481] - amp[480] +
      amp[488] - amp[483] + amp[485] - amp[486] + amp[492] - amp[494] +
      amp[507] - amp[509]);
  jamp[55] = +2. * (+amp[90] + amp[91] + amp[93] + amp[95] + amp[103] +
      amp[104] + amp[105] + amp[106] - amp[119] - amp[120] - amp[122] +
      amp[128] + amp[127] + amp[130] + amp[129] + amp[136] + amp[137] +
      amp[138] + amp[139] - amp[147] + amp[149] + amp[151] - amp[152] +
      amp[154] + amp[155] + amp[157] + amp[170] - amp[168] + amp[173] +
      amp[172] + amp[236] + amp[237] + amp[239] - amp[280] - amp[281] -
      amp[283] + amp[298] + amp[299] + amp[300] + amp[301] - amp[388] -
      amp[387] + amp[391] + amp[390] - amp[394] - amp[398] - amp[395] -
      amp[397] - amp[439] - amp[440] - amp[442] - amp[443] - amp[454] -
      amp[453] - amp[450] - amp[451] + amp[474] - amp[476] + amp[480] -
      amp[482] + amp[483] - amp[485] - amp[489] + amp[491] - amp[492] +
      amp[494] - amp[508] - amp[507]);
  jamp[56] = +2. * (-amp[97] - amp[98] - amp[99] - amp[100] - amp[102] -
      amp[103] - amp[105] - amp[107] + amp[109] + amp[110] + amp[112] -
      amp[124] - amp[123] - amp[131] - amp[130] + amp[192] - amp[194] -
      amp[196] + amp[197] - amp[198] - amp[199] - amp[201] - amp[203] +
      amp[205] - amp[206] + amp[207] - amp[217] - amp[216] + amp[224] +
      amp[223] + amp[262] + amp[263] + amp[265] + amp[279] + amp[280] +
      amp[282] + amp[283] + amp[284] - amp[286] + amp[287] - amp[296] -
      amp[295] + amp[367] + amp[366] + amp[370] + amp[369] + amp[376] +
      amp[375] + amp[378] + amp[379] + amp[385] + amp[384] + amp[388] +
      amp[387] + amp[394] + amp[393] + amp[396] + amp[397] + amp[460] +
      amp[461] - amp[463] - amp[464] + amp[470] + amp[472] + amp[473] +
      amp[469] + amp[508] + amp[509]);
  jamp[57] = +2. * (+amp[0] - amp[2] - amp[4] + amp[5] - amp[7] - amp[8] -
      amp[9] - amp[10] - amp[29] - amp[30] - amp[32] - amp[35] - amp[34] -
      amp[41] + amp[39] + amp[102] + amp[103] + amp[105] + amp[107] - amp[108]
      - amp[109] - amp[111] - amp[118] - amp[119] - amp[121] - amp[122] +
      amp[130] + amp[129] + amp[134] - amp[132] - amp[279] - amp[280] -
      amp[282] - amp[283] - amp[284] + amp[286] - amp[287] + amp[296] +
      amp[295] + amp[297] + amp[298] + amp[300] + amp[331] + amp[332] +
      amp[337] + amp[338] - amp[344] + amp[345] - amp[347] + amp[342] -
      amp[366] + amp[368] - amp[369] + amp[371] - amp[376] - amp[375] +
      amp[382] + amp[381] - amp[388] - amp[387] + amp[391] + amp[390] -
      amp[394] - amp[398] - amp[395] - amp[397] - amp[508] - amp[507]);
  jamp[58] = +2. * (-amp[96] - amp[97] - amp[99] - amp[101] - amp[103] -
      amp[104] - amp[105] - amp[106] + amp[114] + amp[115] + amp[117] -
      amp[127] - amp[126] - amp[131] - amp[130] + amp[147] - amp[149] -
      amp[151] + amp[152] - amp[153] - amp[154] - amp[156] - amp[158] +
      amp[160] - amp[161] + amp[162] - amp[172] - amp[171] + amp[179] +
      amp[178] + amp[261] + amp[262] + amp[264] + amp[265] + amp[266] -
      amp[268] + amp[269] - amp[278] - amp[277] + amp[280] + amp[281] +
      amp[283] + amp[349] + amp[348] + amp[352] + amp[351] + amp[358] +
      amp[357] + amp[360] + amp[361] + amp[385] + amp[384] + amp[388] +
      amp[387] + amp[394] + amp[393] + amp[396] + amp[397] + amp[442] +
      amp[443] - amp[445] - amp[446] + amp[452] + amp[454] + amp[455] +
      amp[451] + amp[508] + amp[509]);
  jamp[59] = +2. * (-amp[0] - amp[1] - amp[3] - amp[5] - amp[13] - amp[14] -
      amp[15] - amp[16] + amp[29] + amp[30] + amp[32] - amp[38] - amp[37] -
      amp[40] - amp[39] + amp[96] + amp[97] + amp[99] + amp[101] - amp[113] -
      amp[114] - amp[116] + amp[118] + amp[119] + amp[121] + amp[122] +
      amp[131] - amp[129] + amp[133] + amp[132] - amp[261] - amp[262] -
      amp[264] - amp[265] - amp[266] + amp[268] - amp[269] + amp[278] +
      amp[277] - amp[297] - amp[298] - amp[300] + amp[334] + amp[335] -
      amp[337] - amp[338] + amp[344] + amp[346] + amp[347] + amp[343] -
      amp[348] + amp[350] - amp[351] + amp[353] - amp[358] - amp[357] +
      amp[364] + amp[363] - amp[385] - amp[384] - amp[391] - amp[390] +
      amp[398] - amp[393] + amp[395] - amp[396] + amp[507] - amp[509]);
  jamp[60] = +2. * (+amp[91] + amp[92] + amp[93] + amp[94] - amp[96] + amp[98]
      + amp[100] - amp[101] + amp[114] + amp[115] + amp[117] + amp[125] +
      amp[124] + amp[128] - amp[126] + amp[180] + amp[181] + amp[183] +
      amp[185] + amp[193] + amp[194] + amp[195] + amp[196] - amp[209] -
      amp[210] - amp[212] + amp[218] + amp[217] + amp[220] + amp[219] +
      amp[226] + amp[227] + amp[229] - amp[267] - amp[268] - amp[270] +
      amp[308] + amp[309] + amp[310] + amp[311] - amp[424] - amp[423] +
      amp[427] + amp[426] - amp[430] - amp[434] - amp[431] - amp[433] +
      amp[438] - amp[440] + amp[444] - amp[446] + amp[447] - amp[449] -
      amp[453] + amp[455] - amp[457] - amp[458] - amp[460] - amp[461] -
      amp[472] - amp[471] - amp[468] - amp[469] - amp[498] + amp[500] -
      amp[502] - amp[501]);
  jamp[61] = +2. * (+amp[52] + amp[53] + amp[54] + amp[55] + amp[57] + amp[58]
      + amp[60] + amp[62] - amp[64] - amp[65] - amp[67] + amp[79] + amp[78] +
      amp[86] + amp[85] + amp[96] + amp[97] + amp[99] + amp[101] + amp[103] +
      amp[104] + amp[105] + amp[106] - amp[114] - amp[115] - amp[117] +
      amp[127] + amp[126] + amp[131] + amp[130] + amp[267] + amp[268] +
      amp[270] + amp[290] + amp[291] + amp[293] - amp[308] - amp[309] -
      amp[310] - amp[311] - amp[385] - amp[386] - amp[388] - amp[389] -
      amp[400] - amp[399] - amp[396] - amp[397] + amp[423] - amp[425] -
      amp[426] + amp[428] + amp[430] - amp[436] + amp[431] - amp[437] +
      amp[441] - amp[443] - amp[444] + amp[446] + amp[448] - amp[454] +
      amp[449] - amp[455] + amp[499] + amp[498] + amp[502] + amp[501]);
  jamp[62] = +2. * (-amp[91] - amp[92] - amp[93] - amp[94] + amp[102] -
      amp[104] - amp[106] + amp[107] - amp[109] - amp[110] - amp[112] -
      amp[125] + amp[123] - amp[128] - amp[127] - amp[180] - amp[181] -
      amp[183] - amp[185] + amp[203] - amp[205] + amp[206] - amp[207] -
      amp[208] + amp[210] - amp[211] - amp[218] + amp[216] - amp[223] -
      amp[222] - amp[226] - amp[227] - amp[229] - amp[284] + amp[286] -
      amp[287] + amp[289] - amp[291] + amp[292] - amp[293] + amp[295] +
      amp[294] - amp[370] - amp[369] + amp[373] + amp[372] - amp[376] -
      amp[380] - amp[377] - amp[379] - amp[438] + amp[440] - amp[441] +
      amp[443] - amp[448] - amp[447] + amp[454] + amp[453] + amp[457] +
      amp[458] + amp[463] + amp[464] - amp[470] + amp[471] - amp[473] +
      amp[468] - amp[499] - amp[500]);
  jamp[63] = +2. * (+amp[7] + amp[8] + amp[9] + amp[10] + amp[12] + amp[13] +
      amp[15] + amp[17] - amp[19] - amp[20] - amp[22] + amp[34] + amp[33] +
      amp[41] + amp[40] - amp[102] + amp[104] + amp[106] - amp[107] + amp[108]
      + amp[109] + amp[111] + amp[113] - amp[115] + amp[116] - amp[117] +
      amp[127] + amp[126] - amp[134] - amp[133] + amp[284] - amp[286] +
      amp[287] - amp[289] + amp[291] - amp[292] + amp[293] - amp[295] -
      amp[294] + amp[307] - amp[309] - amp[311] - amp[331] - amp[332] -
      amp[334] - amp[335] - amp[346] - amp[345] - amp[342] - amp[343] +
      amp[369] - amp[371] - amp[372] + amp[374] + amp[376] - amp[382] +
      amp[377] - amp[383] + amp[441] - amp[443] - amp[444] + amp[446] +
      amp[448] - amp[454] + amp[449] - amp[455] + amp[499] + amp[498]);
  jamp[64] = +2. * (-amp[57] - amp[58] - amp[60] - amp[62] + amp[63] + amp[64]
      + amp[66] + amp[73] + amp[74] + amp[76] + amp[77] - amp[85] - amp[84] -
      amp[89] + amp[87] - amp[90] - amp[91] - amp[93] - amp[95] - amp[103] -
      amp[104] - amp[105] - amp[106] + amp[119] + amp[120] + amp[122] -
      amp[128] - amp[127] - amp[130] - amp[129] - amp[225] - amp[226] -
      amp[228] - amp[229] - amp[235] - amp[236] - amp[238] + amp[242] -
      amp[240] - amp[290] - amp[291] - amp[293] - amp[313] - amp[312] -
      amp[319] - amp[318] + amp[326] - amp[321] + amp[323] - amp[324] +
      amp[388] + amp[389] - amp[391] - amp[392] + amp[398] + amp[400] +
      amp[401] + amp[397] - amp[438] + amp[440] - amp[441] + amp[443] -
      amp[448] - amp[447] + amp[454] + amp[453] - amp[499] - amp[500]);
  jamp[65] = +2. * (-amp[12] - amp[13] - amp[15] - amp[17] + amp[18] + amp[19]
      + amp[21] + amp[28] + amp[29] + amp[31] + amp[32] - amp[40] - amp[39] -
      amp[44] + amp[42] + amp[90] + amp[91] + amp[93] + amp[95] - amp[113] +
      amp[115] - amp[116] + amp[117] + amp[118] - amp[120] + amp[121] +
      amp[128] - amp[126] + amp[133] + amp[132] + amp[225] + amp[226] +
      amp[228] + amp[229] + amp[235] + amp[236] + amp[238] - amp[242] +
      amp[240] - amp[307] + amp[309] + amp[311] + amp[312] - amp[314] +
      amp[318] - amp[320] + amp[321] - amp[323] - amp[327] + amp[329] +
      amp[334] + amp[335] - amp[337] - amp[338] + amp[344] + amp[346] +
      amp[347] + amp[343] + amp[438] - amp[440] + amp[444] - amp[446] +
      amp[447] - amp[449] - amp[453] + amp[455] - amp[498] + amp[500]);
  jamp[66] = +2. * (+amp[91] + amp[92] + amp[93] + amp[94] - amp[102] +
      amp[104] + amp[106] - amp[107] + amp[109] + amp[110] + amp[112] +
      amp[125] - amp[123] + amp[128] + amp[127] + amp[135] + amp[136] +
      amp[138] + amp[140] + amp[148] + amp[149] + amp[150] + amp[151] -
      amp[164] - amp[165] - amp[167] + amp[173] + amp[172] + amp[175] +
      amp[174] + amp[231] + amp[232] + amp[234] - amp[285] - amp[286] -
      amp[288] + amp[303] + amp[304] + amp[305] + amp[306] - amp[406] -
      amp[405] + amp[409] + amp[408] - amp[412] - amp[416] - amp[413] -
      amp[415] - amp[439] - amp[440] - amp[442] - amp[443] - amp[454] -
      amp[453] - amp[450] - amp[451] + amp[456] - amp[458] + amp[462] -
      amp[464] + amp[465] - amp[467] - amp[471] + amp[473] - amp[495] +
      amp[497] - amp[505] - amp[504]);
  jamp[67] = +2. * (+amp[51] + amp[52] + amp[54] + amp[56] + amp[58] + amp[59]
      + amp[60] + amp[61] - amp[69] - amp[70] - amp[72] + amp[82] + amp[81] +
      amp[86] + amp[85] + amp[97] + amp[98] + amp[99] + amp[100] + amp[102] +
      amp[103] + amp[105] + amp[107] - amp[109] - amp[110] - amp[112] +
      amp[124] + amp[123] + amp[131] + amp[130] + amp[272] + amp[273] +
      amp[275] + amp[285] + amp[286] + amp[288] - amp[303] - amp[304] -
      amp[305] - amp[306] - amp[385] - amp[386] - amp[388] - amp[389] -
      amp[400] - amp[399] - amp[396] - amp[397] + amp[405] - amp[407] -
      amp[408] + amp[410] + amp[412] - amp[418] + amp[413] - amp[419] +
      amp[459] - amp[461] - amp[462] + amp[464] + amp[466] - amp[472] +
      amp[467] - amp[473] + amp[496] + amp[495] + amp[505] + amp[504]);
  jamp[68] = +2. * (-amp[91] - amp[92] - amp[93] - amp[94] + amp[96] - amp[98]
      - amp[100] + amp[101] - amp[114] - amp[115] - amp[117] - amp[125] -
      amp[124] - amp[128] + amp[126] - amp[135] - amp[136] - amp[138] -
      amp[140] + amp[158] - amp[160] + amp[161] - amp[162] - amp[163] +
      amp[165] - amp[166] - amp[173] + amp[171] - amp[178] - amp[177] -
      amp[231] - amp[232] - amp[234] - amp[266] + amp[268] - amp[269] +
      amp[271] - amp[273] + amp[274] - amp[275] + amp[277] + amp[276] -
      amp[352] - amp[351] + amp[355] + amp[354] - amp[358] - amp[362] -
      amp[359] - amp[361] + amp[439] + amp[440] + amp[445] + amp[446] -
      amp[452] + amp[453] - amp[455] + amp[450] - amp[456] + amp[458] -
      amp[459] + amp[461] - amp[466] - amp[465] + amp[472] + amp[471] -
      amp[496] - amp[497]);
  jamp[69] = +2. * (+amp[6] + amp[7] + amp[9] + amp[11] + amp[13] + amp[14] +
      amp[15] + amp[16] - amp[24] - amp[25] - amp[27] + amp[37] + amp[36] +
      amp[41] + amp[40] - amp[96] + amp[98] + amp[100] - amp[101] + amp[108] -
      amp[110] + amp[111] - amp[112] + amp[113] + amp[114] + amp[116] +
      amp[124] + amp[123] - amp[134] - amp[133] + amp[266] - amp[268] +
      amp[269] - amp[271] + amp[273] - amp[274] + amp[275] - amp[277] -
      amp[276] + amp[302] - amp[304] - amp[306] - amp[331] - amp[332] -
      amp[334] - amp[335] - amp[346] - amp[345] - amp[342] - amp[343] +
      amp[351] - amp[353] - amp[354] + amp[356] + amp[358] - amp[364] +
      amp[359] - amp[365] + amp[459] - amp[461] - amp[462] + amp[464] +
      amp[466] - amp[472] + amp[467] - amp[473] + amp[496] + amp[495]);
  jamp[70] = +2. * (-amp[51] - amp[52] - amp[54] - amp[56] + amp[68] + amp[69]
      + amp[71] - amp[73] - amp[74] - amp[76] - amp[77] - amp[86] + amp[84] -
      amp[88] - amp[87] + amp[90] - amp[92] - amp[94] + amp[95] - amp[97] -
      amp[98] - amp[99] - amp[100] - amp[119] - amp[120] - amp[122] - amp[125]
      - amp[124] - amp[131] + amp[129] - amp[230] - amp[231] - amp[233] -
      amp[234] + amp[235] + amp[236] + amp[238] + amp[241] + amp[240] -
      amp[272] - amp[273] - amp[275] - amp[316] - amp[315] + amp[319] +
      amp[318] - amp[322] - amp[326] - amp[323] - amp[325] + amp[385] +
      amp[386] + amp[391] + amp[392] - amp[398] + amp[399] - amp[401] +
      amp[396] - amp[456] + amp[458] - amp[459] + amp[461] - amp[466] -
      amp[465] + amp[472] + amp[471] - amp[496] - amp[497]);
  jamp[71] = +2. * (-amp[6] - amp[7] - amp[9] - amp[11] + amp[23] + amp[24] +
      amp[26] - amp[28] - amp[29] - amp[31] - amp[32] - amp[41] + amp[39] -
      amp[43] - amp[42] - amp[90] + amp[92] + amp[94] - amp[95] - amp[108] +
      amp[110] - amp[111] + amp[112] - amp[118] + amp[120] - amp[121] +
      amp[125] - amp[123] + amp[134] - amp[132] + amp[230] + amp[231] +
      amp[233] + amp[234] - amp[235] - amp[236] - amp[238] - amp[241] -
      amp[240] - amp[302] + amp[304] + amp[306] + amp[315] - amp[317] -
      amp[318] + amp[320] + amp[322] - amp[328] + amp[323] - amp[329] +
      amp[331] + amp[332] + amp[337] + amp[338] - amp[344] + amp[345] -
      amp[347] + amp[342] + amp[456] - amp[458] + amp[462] - amp[464] +
      amp[465] - amp[467] - amp[471] + amp[473] - amp[495] + amp[497]);
  jamp[72] = +2. * (+amp[135] - amp[137] - amp[139] + amp[140] + amp[153] -
      amp[155] + amp[156] - amp[157] + amp[163] - amp[165] + amp[166] -
      amp[170] + amp[168] - amp[179] + amp[177] - amp[181] - amp[182] -
      amp[183] - amp[184] + amp[192] - amp[194] - amp[196] + amp[197] -
      amp[199] - amp[200] - amp[202] - amp[215] + amp[213] - amp[218] -
      amp[217] + amp[231] + amp[232] + amp[234] - amp[261] + amp[263] -
      amp[264] - amp[271] + amp[273] - amp[274] + amp[275] + amp[278] -
      amp[276] - amp[349] - amp[348] - amp[355] - amp[354] + amp[362] -
      amp[357] + amp[359] - amp[360] + amp[457] + amp[456] + amp[460] +
      amp[459] + amp[466] + amp[465] + amp[468] + amp[469] + amp[475] +
      amp[476] + amp[481] + amp[482] - amp[488] + amp[489] - amp[491] +
      amp[486] + amp[496] + amp[497]);
  jamp[73] = +2. * (-amp[91] - amp[92] - amp[93] - amp[94] + amp[96] - amp[98]
      - amp[100] + amp[101] - amp[114] - amp[115] - amp[117] - amp[125] -
      amp[124] - amp[128] + amp[126] - amp[135] - amp[136] - amp[138] -
      amp[140] + amp[158] - amp[160] + amp[161] - amp[162] - amp[163] +
      amp[165] - amp[166] - amp[173] + amp[171] - amp[178] - amp[177] -
      amp[231] - amp[232] - amp[234] - amp[266] + amp[268] - amp[269] +
      amp[271] - amp[273] + amp[274] - amp[275] + amp[277] + amp[276] -
      amp[352] - amp[351] + amp[355] + amp[354] - amp[358] - amp[362] -
      amp[359] - amp[361] + amp[439] + amp[440] + amp[445] + amp[446] -
      amp[452] + amp[453] - amp[455] + amp[450] - amp[456] + amp[458] -
      amp[459] + amp[461] - amp[466] - amp[465] + amp[472] + amp[471] -
      amp[496] - amp[497]);
  jamp[74] = +2. * (+amp[141] - amp[143] - amp[145] + amp[146] - amp[153] +
      amp[155] - amp[156] + amp[157] - amp[158] - amp[159] - amp[161] -
      amp[169] - amp[168] + amp[179] + amp[178] - amp[187] - amp[188] -
      amp[189] - amp[190] - amp[192] - amp[193] - amp[195] - amp[197] +
      amp[199] + amp[200] + amp[202] - amp[214] - amp[213] - amp[221] -
      amp[220] + amp[249] + amp[250] + amp[252] + amp[261] - amp[263] +
      amp[264] + amp[266] + amp[267] + amp[269] + amp[270] - amp[278] -
      amp[277] + amp[349] + amp[348] + amp[352] + amp[351] + amp[358] +
      amp[357] + amp[360] + amp[361] + amp[421] + amp[420] + amp[424] +
      amp[423] + amp[430] + amp[429] + amp[432] + amp[433] + amp[478] +
      amp[479] - amp[481] - amp[482] + amp[488] + amp[490] + amp[491] +
      amp[487] + amp[502] + amp[503]);
  jamp[75] = +2. * (-amp[46] - amp[47] - amp[48] - amp[49] + amp[51] - amp[53]
      - amp[55] + amp[56] - amp[69] - amp[70] - amp[72] - amp[80] - amp[79] -
      amp[83] + amp[81] - amp[141] - amp[142] - amp[144] - amp[146] + amp[158]
      + amp[159] + amp[161] - amp[163] - amp[164] - amp[166] - amp[167] -
      amp[176] + amp[174] - amp[178] - amp[177] - amp[249] - amp[250] -
      amp[252] - amp[266] - amp[267] - amp[269] - amp[270] + amp[271] +
      amp[272] + amp[274] + amp[277] + amp[276] - amp[352] - amp[351] +
      amp[355] + amp[354] - amp[358] - amp[362] - amp[359] - amp[361] +
      amp[403] + amp[404] + amp[409] + amp[410] - amp[416] + amp[417] -
      amp[419] + amp[414] - amp[420] + amp[422] - amp[423] + amp[425] -
      amp[430] - amp[429] + amp[436] + amp[435] - amp[502] - amp[503]);
  jamp[76] = +2. * (-amp[96] - amp[97] - amp[99] - amp[101] - amp[103] -
      amp[104] - amp[105] - amp[106] + amp[114] + amp[115] + amp[117] -
      amp[127] - amp[126] - amp[131] - amp[130] + amp[147] - amp[149] -
      amp[151] + amp[152] - amp[153] - amp[154] - amp[156] - amp[158] +
      amp[160] - amp[161] + amp[162] - amp[172] - amp[171] + amp[179] +
      amp[178] + amp[261] + amp[262] + amp[264] + amp[265] + amp[266] -
      amp[268] + amp[269] - amp[278] - amp[277] + amp[280] + amp[281] +
      amp[283] + amp[349] + amp[348] + amp[352] + amp[351] + amp[358] +
      amp[357] + amp[360] + amp[361] + amp[385] + amp[384] + amp[388] +
      amp[387] + amp[394] + amp[393] + amp[396] + amp[397] + amp[442] +
      amp[443] - amp[445] - amp[446] + amp[452] + amp[454] + amp[455] +
      amp[451] + amp[508] + amp[509]);
  jamp[77] = +2. * (-amp[51] - amp[52] - amp[54] - amp[56] - amp[58] - amp[59]
      - amp[60] - amp[61] + amp[69] + amp[70] + amp[72] - amp[82] - amp[81] -
      amp[86] - amp[85] - amp[147] - amp[148] - amp[150] - amp[152] + amp[153]
      + amp[154] + amp[156] + amp[163] + amp[164] + amp[166] + amp[167] -
      amp[175] - amp[174] - amp[179] + amp[177] - amp[261] - amp[262] -
      amp[264] - amp[265] - amp[271] - amp[272] - amp[274] + amp[278] -
      amp[276] - amp[280] - amp[281] - amp[283] - amp[349] - amp[348] -
      amp[355] - amp[354] + amp[362] - amp[357] + amp[359] - amp[360] -
      amp[384] + amp[386] - amp[387] + amp[389] - amp[394] - amp[393] +
      amp[400] + amp[399] + amp[406] + amp[407] - amp[409] - amp[410] +
      amp[416] + amp[418] + amp[419] + amp[415] - amp[508] - amp[509]);
  jamp[78] = +2. * (-amp[135] + amp[137] + amp[139] - amp[140] + amp[142] +
      amp[143] + amp[144] + amp[145] + amp[164] + amp[165] + amp[167] +
      amp[170] + amp[169] + amp[176] - amp[174] + amp[181] + amp[182] +
      amp[183] + amp[184] - amp[186] + amp[188] + amp[190] - amp[191] +
      amp[204] + amp[205] + amp[207] + amp[215] + amp[214] + amp[218] -
      amp[216] - amp[231] - amp[232] - amp[234] - amp[244] - amp[245] -
      amp[247] - amp[303] - amp[304] - amp[305] - amp[306] - amp[403] -
      amp[402] - amp[409] - amp[408] + amp[416] - amp[411] + amp[413] -
      amp[414] - amp[457] - amp[456] - amp[463] - amp[462] + amp[470] -
      amp[465] + amp[467] - amp[468] - amp[475] - amp[476] - amp[478] -
      amp[479] - amp[490] - amp[489] - amp[486] - amp[487] + amp[495] -
      amp[497] + amp[504] - amp[506]);
  jamp[79] = +2. * (+amp[91] + amp[92] + amp[93] + amp[94] - amp[102] +
      amp[104] + amp[106] - amp[107] + amp[109] + amp[110] + amp[112] +
      amp[125] - amp[123] + amp[128] + amp[127] + amp[135] + amp[136] +
      amp[138] + amp[140] + amp[148] + amp[149] + amp[150] + amp[151] -
      amp[164] - amp[165] - amp[167] + amp[173] + amp[172] + amp[175] +
      amp[174] + amp[231] + amp[232] + amp[234] - amp[285] - amp[286] -
      amp[288] + amp[303] + amp[304] + amp[305] + amp[306] - amp[406] -
      amp[405] + amp[409] + amp[408] - amp[412] - amp[416] - amp[413] -
      amp[415] - amp[439] - amp[440] - amp[442] - amp[443] - amp[454] -
      amp[453] - amp[450] - amp[451] + amp[456] - amp[458] + amp[462] -
      amp[464] + amp[465] - amp[467] - amp[471] + amp[473] - amp[495] +
      amp[497] - amp[505] - amp[504]);
  jamp[80] = +2. * (-amp[142] - amp[143] - amp[144] - amp[145] - amp[147] -
      amp[148] - amp[150] - amp[152] + amp[154] + amp[155] + amp[157] -
      amp[169] - amp[168] - amp[176] - amp[175] + amp[186] - amp[188] -
      amp[190] + amp[191] - amp[198] + amp[200] - amp[201] + amp[202] -
      amp[203] - amp[204] - amp[206] - amp[214] - amp[213] + amp[224] +
      amp[223] + amp[244] + amp[245] + amp[247] + amp[279] - amp[281] +
      amp[282] + amp[284] + amp[285] + amp[287] + amp[288] - amp[296] -
      amp[295] + amp[367] + amp[366] + amp[370] + amp[369] + amp[376] +
      amp[375] + amp[378] + amp[379] + amp[403] + amp[402] + amp[406] +
      amp[405] + amp[412] + amp[411] + amp[414] + amp[415] + amp[478] +
      amp[479] - amp[481] - amp[482] + amp[488] + amp[490] + amp[491] +
      amp[487] + amp[505] + amp[506]);
  jamp[81] = +2. * (-amp[1] - amp[2] - amp[3] - amp[4] + amp[6] - amp[8] -
      amp[10] + amp[11] - amp[24] - amp[25] - amp[27] - amp[35] - amp[34] -
      amp[38] + amp[36] + amp[147] + amp[148] + amp[150] + amp[152] - amp[153]
      - amp[154] - amp[156] - amp[163] - amp[164] - amp[166] - amp[167] +
      amp[175] + amp[174] + amp[179] - amp[177] - amp[279] + amp[281] -
      amp[282] - amp[284] - amp[285] - amp[287] - amp[288] + amp[296] +
      amp[295] + amp[302] + amp[303] + amp[305] + amp[349] + amp[350] +
      amp[355] + amp[356] - amp[362] + amp[363] - amp[365] + amp[360] -
      amp[366] + amp[368] - amp[369] + amp[371] - amp[376] - amp[375] +
      amp[382] + amp[381] - amp[406] - amp[405] + amp[409] + amp[408] -
      amp[412] - amp[416] - amp[413] - amp[415] - amp[505] - amp[504]);
  jamp[82] = +2. * (+amp[102] - amp[104] - amp[106] + amp[107] - amp[108] -
      amp[109] - amp[111] - amp[113] + amp[115] - amp[116] + amp[117] -
      amp[127] - amp[126] + amp[134] + amp[133] - amp[141] - amp[142] -
      amp[144] - amp[146] - amp[148] - amp[149] - amp[150] - amp[151] +
      amp[159] + amp[160] + amp[162] - amp[172] - amp[171] - amp[176] -
      amp[175] + amp[243] + amp[244] + amp[246] + amp[247] + amp[248] -
      amp[250] + amp[251] - amp[260] - amp[259] + amp[285] + amp[286] +
      amp[288] + amp[331] + amp[330] + amp[334] + amp[333] + amp[340] +
      amp[339] + amp[342] + amp[343] + amp[403] + amp[402] + amp[406] +
      amp[405] + amp[412] + amp[411] + amp[414] + amp[415] + amp[442] +
      amp[443] - amp[445] - amp[446] + amp[452] + amp[454] + amp[455] +
      amp[451] + amp[505] + amp[506]);
  jamp[83] = +2. * (-amp[6] - amp[7] - amp[9] - amp[11] - amp[13] - amp[14] -
      amp[15] - amp[16] + amp[24] + amp[25] + amp[27] - amp[37] - amp[36] -
      amp[41] - amp[40] + amp[141] + amp[142] + amp[144] + amp[146] - amp[158]
      - amp[159] - amp[161] + amp[163] + amp[164] + amp[166] + amp[167] +
      amp[176] - amp[174] + amp[178] + amp[177] - amp[243] - amp[244] -
      amp[246] - amp[247] - amp[248] + amp[250] - amp[251] + amp[260] +
      amp[259] - amp[302] - amp[303] - amp[305] - amp[330] + amp[332] -
      amp[333] + amp[335] - amp[340] - amp[339] + amp[346] + amp[345] +
      amp[352] + amp[353] - amp[355] - amp[356] + amp[362] + amp[364] +
      amp[365] + amp[361] - amp[403] - amp[402] - amp[409] - amp[408] +
      amp[416] - amp[411] + amp[413] - amp[414] + amp[504] - amp[506]);
  jamp[84] = +2. * (+amp[136] + amp[137] + amp[138] + amp[139] - amp[141] +
      amp[143] + amp[145] - amp[146] + amp[159] + amp[160] + amp[162] +
      amp[170] + amp[169] + amp[173] - amp[171] - amp[180] + amp[182] +
      amp[184] - amp[185] + amp[187] + amp[188] + amp[189] + amp[190] +
      amp[209] + amp[210] + amp[212] + amp[215] + amp[214] + amp[221] -
      amp[219] - amp[226] - amp[227] - amp[229] - amp[249] - amp[250] -
      amp[252] - amp[308] - amp[309] - amp[310] - amp[311] - amp[421] -
      amp[420] - amp[427] - amp[426] + amp[434] - amp[429] + amp[431] -
      amp[432] - amp[439] - amp[438] - amp[445] - amp[444] + amp[452] -
      amp[447] + amp[449] - amp[450] - amp[475] - amp[476] - amp[478] -
      amp[479] - amp[490] - amp[489] - amp[486] - amp[487] + amp[498] -
      amp[500] + amp[501] - amp[503]);
  jamp[85] = +2. * (+amp[46] + amp[47] + amp[48] + amp[49] - amp[57] + amp[59]
      + amp[61] - amp[62] + amp[64] + amp[65] + amp[67] + amp[80] - amp[78] +
      amp[83] + amp[82] + amp[141] + amp[142] + amp[144] + amp[146] + amp[148]
      + amp[149] + amp[150] + amp[151] - amp[159] - amp[160] - amp[162] +
      amp[172] + amp[171] + amp[176] + amp[175] + amp[249] + amp[250] +
      amp[252] - amp[290] - amp[291] - amp[293] + amp[308] + amp[309] +
      amp[310] + amp[311] - amp[403] - amp[404] - amp[406] - amp[407] -
      amp[418] - amp[417] - amp[414] - amp[415] + amp[420] - amp[422] +
      amp[426] - amp[428] + amp[429] - amp[431] - amp[435] + amp[437] -
      amp[442] - amp[441] + amp[445] + amp[444] - amp[448] - amp[452] -
      amp[449] - amp[451] - amp[499] - amp[498] - amp[501] + amp[503]);
  jamp[86] = +2. * (-amp[136] - amp[137] - amp[138] - amp[139] + amp[147] -
      amp[149] - amp[151] + amp[152] - amp[154] - amp[155] - amp[157] -
      amp[170] + amp[168] - amp[173] - amp[172] + amp[180] - amp[182] -
      amp[184] + amp[185] + amp[198] - amp[200] + amp[201] - amp[202] +
      amp[208] - amp[210] + amp[211] - amp[215] + amp[213] - amp[224] +
      amp[222] + amp[226] + amp[227] + amp[229] - amp[279] + amp[281] -
      amp[282] - amp[289] + amp[291] - amp[292] + amp[293] + amp[296] -
      amp[294] - amp[367] - amp[366] - amp[373] - amp[372] + amp[380] -
      amp[375] + amp[377] - amp[378] + amp[439] + amp[438] + amp[442] +
      amp[441] + amp[448] + amp[447] + amp[450] + amp[451] + amp[475] +
      amp[476] + amp[481] + amp[482] - amp[488] + amp[489] - amp[491] +
      amp[486] + amp[499] + amp[500]);
  jamp[87] = +2. * (+amp[1] + amp[2] + amp[3] + amp[4] - amp[12] + amp[14] +
      amp[16] - amp[17] + amp[19] + amp[20] + amp[22] + amp[35] - amp[33] +
      amp[38] + amp[37] - amp[147] + amp[149] + amp[151] - amp[152] + amp[153]
      + amp[154] + amp[156] + amp[158] - amp[160] + amp[161] - amp[162] +
      amp[172] + amp[171] - amp[179] - amp[178] + amp[279] - amp[281] +
      amp[282] + amp[289] - amp[291] + amp[292] - amp[293] - amp[296] +
      amp[294] - amp[307] + amp[309] + amp[311] - amp[349] - amp[350] -
      amp[352] - amp[353] - amp[364] - amp[363] - amp[360] - amp[361] +
      amp[366] - amp[368] + amp[372] - amp[374] + amp[375] - amp[377] -
      amp[381] + amp[383] - amp[442] - amp[441] + amp[445] + amp[444] -
      amp[448] - amp[452] - amp[449] - amp[451] - amp[499] - amp[498]);
  jamp[88] = +2. * (+amp[57] - amp[59] - amp[61] + amp[62] - amp[63] - amp[64]
      - amp[66] - amp[68] + amp[70] - amp[71] + amp[72] - amp[82] - amp[81] +
      amp[89] + amp[88] - amp[135] - amp[136] - amp[138] - amp[140] - amp[148]
      - amp[149] - amp[150] - amp[151] + amp[164] + amp[165] + amp[167] -
      amp[173] - amp[172] - amp[175] - amp[174] + amp[225] + amp[226] +
      amp[228] + amp[229] + amp[230] - amp[232] + amp[233] - amp[242] -
      amp[241] + amp[290] + amp[291] + amp[293] + amp[313] + amp[312] +
      amp[316] + amp[315] + amp[322] + amp[321] + amp[324] + amp[325] +
      amp[406] + amp[407] - amp[409] - amp[410] + amp[416] + amp[418] +
      amp[419] + amp[415] + amp[439] + amp[438] + amp[442] + amp[441] +
      amp[448] + amp[447] + amp[450] + amp[451] + amp[499] + amp[500]);
  jamp[89] = +2. * (+amp[12] - amp[14] - amp[16] + amp[17] - amp[18] - amp[19]
      - amp[21] - amp[23] + amp[25] - amp[26] + amp[27] - amp[37] - amp[36] +
      amp[44] + amp[43] + amp[135] + amp[136] + amp[138] + amp[140] - amp[158]
      + amp[160] - amp[161] + amp[162] + amp[163] - amp[165] + amp[166] +
      amp[173] - amp[171] + amp[178] + amp[177] - amp[225] - amp[226] -
      amp[228] - amp[229] - amp[230] + amp[232] - amp[233] + amp[242] +
      amp[241] + amp[307] - amp[309] - amp[311] - amp[312] + amp[314] -
      amp[315] + amp[317] - amp[322] - amp[321] + amp[328] + amp[327] +
      amp[352] + amp[353] - amp[355] - amp[356] + amp[362] + amp[364] +
      amp[365] + amp[361] - amp[439] - amp[438] - amp[445] - amp[444] +
      amp[452] - amp[447] + amp[449] - amp[450] + amp[498] - amp[500]);
  jamp[90] = +2. * (+amp[90] + amp[91] + amp[93] + amp[95] + amp[103] +
      amp[104] + amp[105] + amp[106] - amp[119] - amp[120] - amp[122] +
      amp[128] + amp[127] + amp[130] + amp[129] + amp[136] + amp[137] +
      amp[138] + amp[139] - amp[147] + amp[149] + amp[151] - amp[152] +
      amp[154] + amp[155] + amp[157] + amp[170] - amp[168] + amp[173] +
      amp[172] + amp[236] + amp[237] + amp[239] - amp[280] - amp[281] -
      amp[283] + amp[298] + amp[299] + amp[300] + amp[301] - amp[388] -
      amp[387] + amp[391] + amp[390] - amp[394] - amp[398] - amp[395] -
      amp[397] - amp[439] - amp[440] - amp[442] - amp[443] - amp[454] -
      amp[453] - amp[450] - amp[451] + amp[474] - amp[476] + amp[480] -
      amp[482] + amp[483] - amp[485] - amp[489] + amp[491] - amp[492] +
      amp[494] - amp[508] - amp[507]);
  jamp[91] = +2. * (+amp[45] + amp[46] + amp[48] + amp[50] + amp[58] + amp[59]
      + amp[60] + amp[61] - amp[74] - amp[75] - amp[77] + amp[83] + amp[82] +
      amp[85] + amp[84] + amp[142] + amp[143] + amp[144] + amp[145] + amp[147]
      + amp[148] + amp[150] + amp[152] - amp[154] - amp[155] - amp[157] +
      amp[169] + amp[168] + amp[176] + amp[175] + amp[254] + amp[255] +
      amp[257] + amp[280] + amp[281] + amp[283] - amp[298] - amp[299] -
      amp[300] - amp[301] + amp[387] - amp[389] - amp[390] + amp[392] +
      amp[394] - amp[400] + amp[395] - amp[401] - amp[403] - amp[404] -
      amp[406] - amp[407] - amp[418] - amp[417] - amp[414] - amp[415] +
      amp[477] - amp[479] - amp[480] + amp[482] + amp[484] - amp[490] +
      amp[485] - amp[491] + amp[493] + amp[492] + amp[508] + amp[507]);
  jamp[92] = +2. * (-amp[90] - amp[91] - amp[93] - amp[95] + amp[113] -
      amp[115] + amp[116] - amp[117] - amp[118] + amp[120] - amp[121] -
      amp[128] + amp[126] - amp[133] - amp[132] - amp[136] - amp[137] -
      amp[138] - amp[139] + amp[141] - amp[143] - amp[145] + amp[146] -
      amp[159] - amp[160] - amp[162] - amp[170] - amp[169] - amp[173] +
      amp[171] - amp[236] - amp[237] - amp[239] - amp[248] + amp[250] -
      amp[251] + amp[253] - amp[255] + amp[256] - amp[257] + amp[259] +
      amp[258] - amp[334] - amp[333] + amp[337] + amp[336] - amp[340] -
      amp[344] - amp[341] - amp[343] + amp[439] + amp[440] + amp[445] +
      amp[446] - amp[452] + amp[453] - amp[455] + amp[450] - amp[474] +
      amp[476] - amp[477] + amp[479] - amp[484] - amp[483] + amp[490] +
      amp[489] - amp[493] - amp[494]);
  jamp[93] = +2. * (+amp[0] + amp[1] + amp[3] + amp[5] + amp[13] + amp[14] +
      amp[15] + amp[16] - amp[29] - amp[30] - amp[32] + amp[38] + amp[37] +
      amp[40] + amp[39] - amp[141] + amp[143] + amp[145] - amp[146] + amp[153]
      - amp[155] + amp[156] - amp[157] + amp[158] + amp[159] + amp[161] +
      amp[169] + amp[168] - amp[179] - amp[178] + amp[248] - amp[250] +
      amp[251] - amp[253] + amp[255] - amp[256] + amp[257] - amp[259] -
      amp[258] + amp[297] - amp[299] - amp[301] + amp[333] - amp[335] -
      amp[336] + amp[338] + amp[340] - amp[346] + amp[341] - amp[347] -
      amp[349] - amp[350] - amp[352] - amp[353] - amp[364] - amp[363] -
      amp[360] - amp[361] + amp[477] - amp[479] - amp[480] + amp[482] +
      amp[484] - amp[490] + amp[485] - amp[491] + amp[493] + amp[492]);
  jamp[94] = +2. * (-amp[45] - amp[46] - amp[48] - amp[50] + amp[68] - amp[70]
      + amp[71] - amp[72] - amp[73] + amp[75] - amp[76] - amp[83] + amp[81] -
      amp[88] - amp[87] + amp[135] - amp[137] - amp[139] + amp[140] - amp[142]
      - amp[143] - amp[144] - amp[145] - amp[164] - amp[165] - amp[167] -
      amp[170] - amp[169] - amp[176] + amp[174] - amp[230] + amp[232] -
      amp[233] + amp[235] - amp[237] + amp[238] - amp[239] + amp[241] +
      amp[240] - amp[254] - amp[255] - amp[257] - amp[316] - amp[315] +
      amp[319] + amp[318] - amp[322] - amp[326] - amp[323] - amp[325] +
      amp[403] + amp[404] + amp[409] + amp[410] - amp[416] + amp[417] -
      amp[419] + amp[414] - amp[474] + amp[476] - amp[477] + amp[479] -
      amp[484] - amp[483] + amp[490] + amp[489] - amp[493] - amp[494]);
  jamp[95] = +2. * (-amp[0] - amp[1] - amp[3] - amp[5] + amp[23] - amp[25] +
      amp[26] - amp[27] - amp[28] + amp[30] - amp[31] - amp[38] + amp[36] -
      amp[43] - amp[42] - amp[135] + amp[137] + amp[139] - amp[140] - amp[153]
      + amp[155] - amp[156] + amp[157] - amp[163] + amp[165] - amp[166] +
      amp[170] - amp[168] + amp[179] - amp[177] + amp[230] - amp[232] +
      amp[233] - amp[235] + amp[237] - amp[238] + amp[239] - amp[241] -
      amp[240] - amp[297] + amp[299] + amp[301] + amp[315] - amp[317] -
      amp[318] + amp[320] + amp[322] - amp[328] + amp[323] - amp[329] +
      amp[349] + amp[350] + amp[355] + amp[356] - amp[362] + amp[363] -
      amp[365] + amp[360] + amp[474] - amp[476] + amp[480] - amp[482] +
      amp[483] - amp[485] - amp[489] + amp[491] - amp[492] + amp[494]);
  jamp[96] = +2. * (-amp[136] - amp[137] - amp[138] - amp[139] + amp[147] -
      amp[149] - amp[151] + amp[152] - amp[154] - amp[155] - amp[157] -
      amp[170] + amp[168] - amp[173] - amp[172] + amp[180] - amp[182] -
      amp[184] + amp[185] + amp[198] - amp[200] + amp[201] - amp[202] +
      amp[208] - amp[210] + amp[211] - amp[215] + amp[213] - amp[224] +
      amp[222] + amp[226] + amp[227] + amp[229] - amp[279] + amp[281] -
      amp[282] - amp[289] + amp[291] - amp[292] + amp[293] + amp[296] -
      amp[294] - amp[367] - amp[366] - amp[373] - amp[372] + amp[380] -
      amp[375] + amp[377] - amp[378] + amp[439] + amp[438] + amp[442] +
      amp[441] + amp[448] + amp[447] + amp[450] + amp[451] + amp[475] +
      amp[476] + amp[481] + amp[482] - amp[488] + amp[489] - amp[491] +
      amp[486] + amp[499] + amp[500]);
  jamp[97] = +2. * (-amp[91] - amp[92] - amp[93] - amp[94] + amp[102] -
      amp[104] - amp[106] + amp[107] - amp[109] - amp[110] - amp[112] -
      amp[125] + amp[123] - amp[128] - amp[127] - amp[180] - amp[181] -
      amp[183] - amp[185] + amp[203] - amp[205] + amp[206] - amp[207] -
      amp[208] + amp[210] - amp[211] - amp[218] + amp[216] - amp[223] -
      amp[222] - amp[226] - amp[227] - amp[229] - amp[284] + amp[286] -
      amp[287] + amp[289] - amp[291] + amp[292] - amp[293] + amp[295] +
      amp[294] - amp[370] - amp[369] + amp[373] + amp[372] - amp[376] -
      amp[380] - amp[377] - amp[379] - amp[438] + amp[440] - amp[441] +
      amp[443] - amp[448] - amp[447] + amp[454] + amp[453] + amp[457] +
      amp[458] + amp[463] + amp[464] - amp[470] + amp[471] - amp[473] +
      amp[468] - amp[499] - amp[500]);
  jamp[98] = +2. * (-amp[142] - amp[143] - amp[144] - amp[145] - amp[147] -
      amp[148] - amp[150] - amp[152] + amp[154] + amp[155] + amp[157] -
      amp[169] - amp[168] - amp[176] - amp[175] + amp[186] - amp[188] -
      amp[190] + amp[191] - amp[198] + amp[200] - amp[201] + amp[202] -
      amp[203] - amp[204] - amp[206] - amp[214] - amp[213] + amp[224] +
      amp[223] + amp[244] + amp[245] + amp[247] + amp[279] - amp[281] +
      amp[282] + amp[284] + amp[285] + amp[287] + amp[288] - amp[296] -
      amp[295] + amp[367] + amp[366] + amp[370] + amp[369] + amp[376] +
      amp[375] + amp[378] + amp[379] + amp[403] + amp[402] + amp[406] +
      amp[405] + amp[412] + amp[411] + amp[414] + amp[415] + amp[478] +
      amp[479] - amp[481] - amp[482] + amp[488] + amp[490] + amp[491] +
      amp[487] + amp[505] + amp[506]);
  jamp[99] = +2. * (-amp[46] - amp[47] - amp[48] - amp[49] + amp[57] - amp[59]
      - amp[61] + amp[62] - amp[64] - amp[65] - amp[67] - amp[80] + amp[78] -
      amp[83] - amp[82] - amp[186] - amp[187] - amp[189] - amp[191] + amp[203]
      + amp[204] + amp[206] - amp[208] - amp[209] - amp[211] - amp[212] -
      amp[221] + amp[219] - amp[223] - amp[222] - amp[244] - amp[245] -
      amp[247] - amp[284] - amp[285] - amp[287] - amp[288] + amp[289] +
      amp[290] + amp[292] + amp[295] + amp[294] - amp[370] - amp[369] +
      amp[373] + amp[372] - amp[376] - amp[380] - amp[377] - amp[379] -
      amp[402] + amp[404] - amp[405] + amp[407] - amp[412] - amp[411] +
      amp[418] + amp[417] + amp[421] + amp[422] + amp[427] + amp[428] -
      amp[434] + amp[435] - amp[437] + amp[432] - amp[505] - amp[506]);
  jamp[100] = +2. * (-amp[97] - amp[98] - amp[99] - amp[100] - amp[102] -
      amp[103] - amp[105] - amp[107] + amp[109] + amp[110] + amp[112] -
      amp[124] - amp[123] - amp[131] - amp[130] + amp[192] - amp[194] -
      amp[196] + amp[197] - amp[198] - amp[199] - amp[201] - amp[203] +
      amp[205] - amp[206] + amp[207] - amp[217] - amp[216] + amp[224] +
      amp[223] + amp[262] + amp[263] + amp[265] + amp[279] + amp[280] +
      amp[282] + amp[283] + amp[284] - amp[286] + amp[287] - amp[296] -
      amp[295] + amp[367] + amp[366] + amp[370] + amp[369] + amp[376] +
      amp[375] + amp[378] + amp[379] + amp[385] + amp[384] + amp[388] +
      amp[387] + amp[394] + amp[393] + amp[396] + amp[397] + amp[460] +
      amp[461] - amp[463] - amp[464] + amp[470] + amp[472] + amp[473] +
      amp[469] + amp[508] + amp[509]);
  jamp[101] = +2. * (-amp[52] - amp[53] - amp[54] - amp[55] - amp[57] - amp[58]
      - amp[60] - amp[62] + amp[64] + amp[65] + amp[67] - amp[79] - amp[78] -
      amp[86] - amp[85] - amp[192] - amp[193] - amp[195] - amp[197] + amp[198]
      + amp[199] + amp[201] + amp[208] + amp[209] + amp[211] + amp[212] -
      amp[220] - amp[219] - amp[224] + amp[222] - amp[262] - amp[263] -
      amp[265] - amp[279] - amp[280] - amp[282] - amp[283] - amp[289] -
      amp[290] - amp[292] + amp[296] - amp[294] - amp[367] - amp[366] -
      amp[373] - amp[372] + amp[380] - amp[375] + amp[377] - amp[378] -
      amp[384] + amp[386] - amp[387] + amp[389] - amp[394] - amp[393] +
      amp[400] + amp[399] + amp[424] + amp[425] - amp[427] - amp[428] +
      amp[434] + amp[436] + amp[437] + amp[433] - amp[508] - amp[509]);
  jamp[102] = +2. * (+amp[136] + amp[137] + amp[138] + amp[139] - amp[141] +
      amp[143] + amp[145] - amp[146] + amp[159] + amp[160] + amp[162] +
      amp[170] + amp[169] + amp[173] - amp[171] - amp[180] + amp[182] +
      amp[184] - amp[185] + amp[187] + amp[188] + amp[189] + amp[190] +
      amp[209] + amp[210] + amp[212] + amp[215] + amp[214] + amp[221] -
      amp[219] - amp[226] - amp[227] - amp[229] - amp[249] - amp[250] -
      amp[252] - amp[308] - amp[309] - amp[310] - amp[311] - amp[421] -
      amp[420] - amp[427] - amp[426] + amp[434] - amp[429] + amp[431] -
      amp[432] - amp[439] - amp[438] - amp[445] - amp[444] + amp[452] -
      amp[447] + amp[449] - amp[450] - amp[475] - amp[476] - amp[478] -
      amp[479] - amp[490] - amp[489] - amp[486] - amp[487] + amp[498] -
      amp[500] + amp[501] - amp[503]);
  jamp[103] = +2. * (+amp[91] + amp[92] + amp[93] + amp[94] - amp[96] + amp[98]
      + amp[100] - amp[101] + amp[114] + amp[115] + amp[117] + amp[125] +
      amp[124] + amp[128] - amp[126] + amp[180] + amp[181] + amp[183] +
      amp[185] + amp[193] + amp[194] + amp[195] + amp[196] - amp[209] -
      amp[210] - amp[212] + amp[218] + amp[217] + amp[220] + amp[219] +
      amp[226] + amp[227] + amp[229] - amp[267] - amp[268] - amp[270] +
      amp[308] + amp[309] + amp[310] + amp[311] - amp[424] - amp[423] +
      amp[427] + amp[426] - amp[430] - amp[434] - amp[431] - amp[433] +
      amp[438] - amp[440] + amp[444] - amp[446] + amp[447] - amp[449] -
      amp[453] + amp[455] - amp[457] - amp[458] - amp[460] - amp[461] -
      amp[472] - amp[471] - amp[468] - amp[469] - amp[498] + amp[500] -
      amp[502] - amp[501]);
  jamp[104] = +2. * (+amp[141] - amp[143] - amp[145] + amp[146] - amp[153] +
      amp[155] - amp[156] + amp[157] - amp[158] - amp[159] - amp[161] -
      amp[169] - amp[168] + amp[179] + amp[178] - amp[187] - amp[188] -
      amp[189] - amp[190] - amp[192] - amp[193] - amp[195] - amp[197] +
      amp[199] + amp[200] + amp[202] - amp[214] - amp[213] - amp[221] -
      amp[220] + amp[249] + amp[250] + amp[252] + amp[261] - amp[263] +
      amp[264] + amp[266] + amp[267] + amp[269] + amp[270] - amp[278] -
      amp[277] + amp[349] + amp[348] + amp[352] + amp[351] + amp[358] +
      amp[357] + amp[360] + amp[361] + amp[421] + amp[420] + amp[424] +
      amp[423] + amp[430] + amp[429] + amp[432] + amp[433] + amp[478] +
      amp[479] - amp[481] - amp[482] + amp[488] + amp[490] + amp[491] +
      amp[487] + amp[502] + amp[503]);
  jamp[105] = +2. * (-amp[1] - amp[2] - amp[3] - amp[4] + amp[12] - amp[14] -
      amp[16] + amp[17] - amp[19] - amp[20] - amp[22] - amp[35] + amp[33] -
      amp[38] - amp[37] + amp[192] + amp[193] + amp[195] + amp[197] - amp[198]
      - amp[199] - amp[201] - amp[208] - amp[209] - amp[211] - amp[212] +
      amp[220] + amp[219] + amp[224] - amp[222] - amp[261] + amp[263] -
      amp[264] - amp[266] - amp[267] - amp[269] - amp[270] + amp[278] +
      amp[277] + amp[307] + amp[308] + amp[310] - amp[348] + amp[350] -
      amp[351] + amp[353] - amp[358] - amp[357] + amp[364] + amp[363] +
      amp[367] + amp[368] + amp[373] + amp[374] - amp[380] + amp[381] -
      amp[383] + amp[378] - amp[424] - amp[423] + amp[427] + amp[426] -
      amp[430] - amp[434] - amp[431] - amp[433] - amp[502] - amp[501]);
  jamp[106] = +2. * (+amp[96] - amp[98] - amp[100] + amp[101] - amp[108] +
      amp[110] - amp[111] + amp[112] - amp[113] - amp[114] - amp[116] -
      amp[124] - amp[123] + amp[134] + amp[133] - amp[186] - amp[187] -
      amp[189] - amp[191] - amp[193] - amp[194] - amp[195] - amp[196] +
      amp[204] + amp[205] + amp[207] - amp[217] - amp[216] - amp[221] -
      amp[220] + amp[243] - amp[245] + amp[246] + amp[248] + amp[249] +
      amp[251] + amp[252] - amp[260] - amp[259] + amp[267] + amp[268] +
      amp[270] + amp[331] + amp[330] + amp[334] + amp[333] + amp[340] +
      amp[339] + amp[342] + amp[343] + amp[421] + amp[420] + amp[424] +
      amp[423] + amp[430] + amp[429] + amp[432] + amp[433] + amp[460] +
      amp[461] - amp[463] - amp[464] + amp[470] + amp[472] + amp[473] +
      amp[469] + amp[502] + amp[503]);
  jamp[107] = +2. * (-amp[7] - amp[8] - amp[9] - amp[10] - amp[12] - amp[13] -
      amp[15] - amp[17] + amp[19] + amp[20] + amp[22] - amp[34] - amp[33] -
      amp[41] - amp[40] + amp[186] + amp[187] + amp[189] + amp[191] - amp[203]
      - amp[204] - amp[206] + amp[208] + amp[209] + amp[211] + amp[212] +
      amp[221] - amp[219] + amp[223] + amp[222] - amp[243] + amp[245] -
      amp[246] - amp[248] - amp[249] - amp[251] - amp[252] + amp[260] +
      amp[259] - amp[307] - amp[308] - amp[310] - amp[330] + amp[332] -
      amp[333] + amp[335] - amp[340] - amp[339] + amp[346] + amp[345] +
      amp[370] + amp[371] - amp[373] - amp[374] + amp[380] + amp[382] +
      amp[383] + amp[379] - amp[421] - amp[420] - amp[427] - amp[426] +
      amp[434] - amp[429] + amp[431] - amp[432] + amp[501] - amp[503]);
  jamp[108] = +2. * (-amp[135] + amp[137] + amp[139] - amp[140] + amp[142] +
      amp[143] + amp[144] + amp[145] + amp[164] + amp[165] + amp[167] +
      amp[170] + amp[169] + amp[176] - amp[174] + amp[181] + amp[182] +
      amp[183] + amp[184] - amp[186] + amp[188] + amp[190] - amp[191] +
      amp[204] + amp[205] + amp[207] + amp[215] + amp[214] + amp[218] -
      amp[216] - amp[231] - amp[232] - amp[234] - amp[244] - amp[245] -
      amp[247] - amp[303] - amp[304] - amp[305] - amp[306] - amp[403] -
      amp[402] - amp[409] - amp[408] + amp[416] - amp[411] + amp[413] -
      amp[414] - amp[457] - amp[456] - amp[463] - amp[462] + amp[470] -
      amp[465] + amp[467] - amp[468] - amp[475] - amp[476] - amp[478] -
      amp[479] - amp[490] - amp[489] - amp[486] - amp[487] + amp[495] -
      amp[497] + amp[504] - amp[506]);
  jamp[109] = +2. * (+amp[46] + amp[47] + amp[48] + amp[49] - amp[51] + amp[53]
      + amp[55] - amp[56] + amp[69] + amp[70] + amp[72] + amp[80] + amp[79] +
      amp[83] - amp[81] + amp[186] + amp[187] + amp[189] + amp[191] + amp[193]
      + amp[194] + amp[195] + amp[196] - amp[204] - amp[205] - amp[207] +
      amp[217] + amp[216] + amp[221] + amp[220] + amp[244] + amp[245] +
      amp[247] - amp[272] - amp[273] - amp[275] + amp[303] + amp[304] +
      amp[305] + amp[306] + amp[402] - amp[404] + amp[408] - amp[410] +
      amp[411] - amp[413] - amp[417] + amp[419] - amp[421] - amp[422] -
      amp[424] - amp[425] - amp[436] - amp[435] - amp[432] - amp[433] -
      amp[460] - amp[459] + amp[463] + amp[462] - amp[466] - amp[470] -
      amp[467] - amp[469] - amp[496] - amp[495] - amp[504] + amp[506]);
  jamp[110] = +2. * (+amp[135] - amp[137] - amp[139] + amp[140] + amp[153] -
      amp[155] + amp[156] - amp[157] + amp[163] - amp[165] + amp[166] -
      amp[170] + amp[168] - amp[179] + amp[177] - amp[181] - amp[182] -
      amp[183] - amp[184] + amp[192] - amp[194] - amp[196] + amp[197] -
      amp[199] - amp[200] - amp[202] - amp[215] + amp[213] - amp[218] -
      amp[217] + amp[231] + amp[232] + amp[234] - amp[261] + amp[263] -
      amp[264] - amp[271] + amp[273] - amp[274] + amp[275] + amp[278] -
      amp[276] - amp[349] - amp[348] - amp[355] - amp[354] + amp[362] -
      amp[357] + amp[359] - amp[360] + amp[457] + amp[456] + amp[460] +
      amp[459] + amp[466] + amp[465] + amp[468] + amp[469] + amp[475] +
      amp[476] + amp[481] + amp[482] - amp[488] + amp[489] - amp[491] +
      amp[486] + amp[496] + amp[497]);
  jamp[111] = +2. * (+amp[1] + amp[2] + amp[3] + amp[4] - amp[6] + amp[8] +
      amp[10] - amp[11] + amp[24] + amp[25] + amp[27] + amp[35] + amp[34] +
      amp[38] - amp[36] - amp[192] + amp[194] + amp[196] - amp[197] + amp[198]
      + amp[199] + amp[201] + amp[203] - amp[205] + amp[206] - amp[207] +
      amp[217] + amp[216] - amp[224] - amp[223] + amp[261] - amp[263] +
      amp[264] + amp[271] - amp[273] + amp[274] - amp[275] - amp[278] +
      amp[276] - amp[302] + amp[304] + amp[306] + amp[348] - amp[350] +
      amp[354] - amp[356] + amp[357] - amp[359] - amp[363] + amp[365] -
      amp[367] - amp[368] - amp[370] - amp[371] - amp[382] - amp[381] -
      amp[378] - amp[379] - amp[460] - amp[459] + amp[463] + amp[462] -
      amp[466] - amp[470] - amp[467] - amp[469] - amp[496] - amp[495]);
  jamp[112] = +2. * (+amp[51] - amp[53] - amp[55] + amp[56] - amp[63] + amp[65]
      - amp[66] + amp[67] - amp[68] - amp[69] - amp[71] - amp[79] - amp[78] +
      amp[89] + amp[88] - amp[180] - amp[181] - amp[183] - amp[185] - amp[193]
      - amp[194] - amp[195] - amp[196] + amp[209] + amp[210] + amp[212] -
      amp[218] - amp[217] - amp[220] - amp[219] + amp[225] - amp[227] +
      amp[228] + amp[230] + amp[231] + amp[233] + amp[234] - amp[242] -
      amp[241] + amp[272] + amp[273] + amp[275] + amp[313] + amp[312] +
      amp[316] + amp[315] + amp[322] + amp[321] + amp[324] + amp[325] +
      amp[424] + amp[425] - amp[427] - amp[428] + amp[434] + amp[436] +
      amp[437] + amp[433] + amp[457] + amp[456] + amp[460] + amp[459] +
      amp[466] + amp[465] + amp[468] + amp[469] + amp[496] + amp[497]);
  jamp[113] = +2. * (+amp[6] - amp[8] - amp[10] + amp[11] - amp[18] + amp[20] -
      amp[21] + amp[22] - amp[23] - amp[24] - amp[26] - amp[34] - amp[33] +
      amp[44] + amp[43] + amp[180] + amp[181] + amp[183] + amp[185] - amp[203]
      + amp[205] - amp[206] + amp[207] + amp[208] - amp[210] + amp[211] +
      amp[218] - amp[216] + amp[223] + amp[222] - amp[225] + amp[227] -
      amp[228] - amp[230] - amp[231] - amp[233] - amp[234] + amp[242] +
      amp[241] + amp[302] - amp[304] - amp[306] - amp[312] + amp[314] -
      amp[315] + amp[317] - amp[322] - amp[321] + amp[328] + amp[327] +
      amp[370] + amp[371] - amp[373] - amp[374] + amp[380] + amp[382] +
      amp[383] + amp[379] - amp[457] - amp[456] - amp[463] - amp[462] +
      amp[470] - amp[465] + amp[467] - amp[468] + amp[495] - amp[497]);
  jamp[114] = +2. * (-amp[90] + amp[92] + amp[94] - amp[95] + amp[97] + amp[98]
      + amp[99] + amp[100] + amp[119] + amp[120] + amp[122] + amp[125] +
      amp[124] + amp[131] - amp[129] + amp[181] + amp[182] + amp[183] +
      amp[184] - amp[192] + amp[194] + amp[196] - amp[197] + amp[199] +
      amp[200] + amp[202] + amp[215] - amp[213] + amp[218] + amp[217] -
      amp[236] - amp[237] - amp[239] - amp[262] - amp[263] - amp[265] -
      amp[298] - amp[299] - amp[300] - amp[301] - amp[385] - amp[384] -
      amp[391] - amp[390] + amp[398] - amp[393] + amp[395] - amp[396] -
      amp[457] - amp[458] - amp[460] - amp[461] - amp[472] - amp[471] -
      amp[468] - amp[469] - amp[475] - amp[474] - amp[481] - amp[480] +
      amp[488] - amp[483] + amp[485] - amp[486] + amp[492] - amp[494] +
      amp[507] - amp[509]);
  jamp[115] = +2. * (-amp[45] + amp[47] + amp[49] - amp[50] + amp[52] + amp[53]
      + amp[54] + amp[55] + amp[74] + amp[75] + amp[77] + amp[80] + amp[79] +
      amp[86] - amp[84] + amp[187] + amp[188] + amp[189] + amp[190] + amp[192]
      + amp[193] + amp[195] + amp[197] - amp[199] - amp[200] - amp[202] +
      amp[214] + amp[213] + amp[221] + amp[220] - amp[254] - amp[255] -
      amp[257] + amp[262] + amp[263] + amp[265] + amp[298] + amp[299] +
      amp[300] + amp[301] + amp[384] - amp[386] + amp[390] - amp[392] +
      amp[393] - amp[395] - amp[399] + amp[401] - amp[421] - amp[422] -
      amp[424] - amp[425] - amp[436] - amp[435] - amp[432] - amp[433] -
      amp[478] - amp[477] + amp[481] + amp[480] - amp[484] - amp[488] -
      amp[485] - amp[487] - amp[493] - amp[492] - amp[507] + amp[509]);
  jamp[116] = +2. * (+amp[90] - amp[92] - amp[94] + amp[95] + amp[108] -
      amp[110] + amp[111] - amp[112] + amp[118] - amp[120] + amp[121] -
      amp[125] + amp[123] - amp[134] + amp[132] - amp[181] - amp[182] -
      amp[183] - amp[184] + amp[186] - amp[188] - amp[190] + amp[191] -
      amp[204] - amp[205] - amp[207] - amp[215] - amp[214] - amp[218] +
      amp[216] + amp[236] + amp[237] + amp[239] - amp[243] + amp[245] -
      amp[246] - amp[253] + amp[255] - amp[256] + amp[257] + amp[260] -
      amp[258] - amp[331] - amp[330] - amp[337] - amp[336] + amp[344] -
      amp[339] + amp[341] - amp[342] + amp[457] + amp[458] + amp[463] +
      amp[464] - amp[470] + amp[471] - amp[473] + amp[468] + amp[475] +
      amp[474] + amp[478] + amp[477] + amp[484] + amp[483] + amp[486] +
      amp[487] + amp[493] + amp[494]);
  jamp[117] = +2. * (-amp[0] + amp[2] + amp[4] - amp[5] + amp[7] + amp[8] +
      amp[9] + amp[10] + amp[29] + amp[30] + amp[32] + amp[35] + amp[34] +
      amp[41] - amp[39] - amp[186] + amp[188] + amp[190] - amp[191] + amp[198]
      - amp[200] + amp[201] - amp[202] + amp[203] + amp[204] + amp[206] +
      amp[214] + amp[213] - amp[224] - amp[223] + amp[243] - amp[245] +
      amp[246] + amp[253] - amp[255] + amp[256] - amp[257] - amp[260] +
      amp[258] - amp[297] + amp[299] + amp[301] + amp[330] - amp[332] +
      amp[336] - amp[338] + amp[339] - amp[341] - amp[345] + amp[347] -
      amp[367] - amp[368] - amp[370] - amp[371] - amp[382] - amp[381] -
      amp[378] - amp[379] - amp[478] - amp[477] + amp[481] + amp[480] -
      amp[484] - amp[488] - amp[485] - amp[487] - amp[493] - amp[492]);
  jamp[118] = +2. * (+amp[45] - amp[47] - amp[49] + amp[50] + amp[63] - amp[65]
      + amp[66] - amp[67] + amp[73] - amp[75] + amp[76] - amp[80] + amp[78] -
      amp[89] + amp[87] + amp[180] - amp[182] - amp[184] + amp[185] - amp[187]
      - amp[188] - amp[189] - amp[190] - amp[209] - amp[210] - amp[212] -
      amp[215] - amp[214] - amp[221] + amp[219] - amp[225] + amp[227] -
      amp[228] - amp[235] + amp[237] - amp[238] + amp[239] + amp[242] -
      amp[240] + amp[254] + amp[255] + amp[257] - amp[313] - amp[312] -
      amp[319] - amp[318] + amp[326] - amp[321] + amp[323] - amp[324] +
      amp[421] + amp[422] + amp[427] + amp[428] - amp[434] + amp[435] -
      amp[437] + amp[432] + amp[475] + amp[474] + amp[478] + amp[477] +
      amp[484] + amp[483] + amp[486] + amp[487] + amp[493] + amp[494]);
  jamp[119] = +2. * (+amp[0] - amp[2] - amp[4] + amp[5] + amp[18] - amp[20] +
      amp[21] - amp[22] + amp[28] - amp[30] + amp[31] - amp[35] + amp[33] -
      amp[44] + amp[42] - amp[180] + amp[182] + amp[184] - amp[185] - amp[198]
      + amp[200] - amp[201] + amp[202] - amp[208] + amp[210] - amp[211] +
      amp[215] - amp[213] + amp[224] - amp[222] + amp[225] - amp[227] +
      amp[228] + amp[235] - amp[237] + amp[238] - amp[239] - amp[242] +
      amp[240] + amp[297] - amp[299] - amp[301] + amp[312] - amp[314] +
      amp[318] - amp[320] + amp[321] - amp[323] - amp[327] + amp[329] +
      amp[367] + amp[368] + amp[373] + amp[374] - amp[380] + amp[381] -
      amp[383] + amp[378] - amp[475] - amp[474] - amp[481] - amp[480] +
      amp[488] - amp[483] + amp[485] - amp[486] + amp[492] - amp[494]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[0][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}


}  // end namespace PY8MEs_namespace

