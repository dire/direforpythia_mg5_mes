//==========================================================================
// This file has been automatically generated for Pythia 8 by
// MadGraph5_aMC@NLO v. 2.6.0, 2017-08-16
// By the MadGraph5_aMC@NLO Development Team
// Visit launchpad.net/madgraph5 and amcatnlo.web.cern.ch
//==========================================================================

#include "PY8MEs_R16_P0_sm_gg_ttxggg.h"
#include "HelAmps_sm.h"

using namespace Pythia8_sm; 

namespace PY8MEs_namespace 
{
//==========================================================================
// Class member functions for calculating the matrix elements for
// Process: g g > t t~ g g g WEIGHTED<=5 @16

// Exception class
class PY8MEs_R16_P0_sm_gg_ttxgggException : public exception
{
  virtual const char * what() const throw()
  {
    return "Exception in class 'PY8MEs_R16_P0_sm_gg_ttxggg'."; 
  }
}
PY8MEs_R16_P0_sm_gg_ttxggg_exception; 

std::set<int> PY8MEs_R16_P0_sm_gg_ttxggg::s_channel_proc = std::set<int>
    (createset<int> ());

int PY8MEs_R16_P0_sm_gg_ttxggg::helicities[ncomb][nexternal] = {{-1, -1, -1,
    -1, -1, -1, -1}, {-1, -1, -1, -1, -1, -1, 1}, {-1, -1, -1, -1, -1, 1, -1},
    {-1, -1, -1, -1, -1, 1, 1}, {-1, -1, -1, -1, 1, -1, -1}, {-1, -1, -1, -1,
    1, -1, 1}, {-1, -1, -1, -1, 1, 1, -1}, {-1, -1, -1, -1, 1, 1, 1}, {-1, -1,
    -1, 1, -1, -1, -1}, {-1, -1, -1, 1, -1, -1, 1}, {-1, -1, -1, 1, -1, 1, -1},
    {-1, -1, -1, 1, -1, 1, 1}, {-1, -1, -1, 1, 1, -1, -1}, {-1, -1, -1, 1, 1,
    -1, 1}, {-1, -1, -1, 1, 1, 1, -1}, {-1, -1, -1, 1, 1, 1, 1}, {-1, -1, 1,
    -1, -1, -1, -1}, {-1, -1, 1, -1, -1, -1, 1}, {-1, -1, 1, -1, -1, 1, -1},
    {-1, -1, 1, -1, -1, 1, 1}, {-1, -1, 1, -1, 1, -1, -1}, {-1, -1, 1, -1, 1,
    -1, 1}, {-1, -1, 1, -1, 1, 1, -1}, {-1, -1, 1, -1, 1, 1, 1}, {-1, -1, 1, 1,
    -1, -1, -1}, {-1, -1, 1, 1, -1, -1, 1}, {-1, -1, 1, 1, -1, 1, -1}, {-1, -1,
    1, 1, -1, 1, 1}, {-1, -1, 1, 1, 1, -1, -1}, {-1, -1, 1, 1, 1, -1, 1}, {-1,
    -1, 1, 1, 1, 1, -1}, {-1, -1, 1, 1, 1, 1, 1}, {-1, 1, -1, -1, -1, -1, -1},
    {-1, 1, -1, -1, -1, -1, 1}, {-1, 1, -1, -1, -1, 1, -1}, {-1, 1, -1, -1, -1,
    1, 1}, {-1, 1, -1, -1, 1, -1, -1}, {-1, 1, -1, -1, 1, -1, 1}, {-1, 1, -1,
    -1, 1, 1, -1}, {-1, 1, -1, -1, 1, 1, 1}, {-1, 1, -1, 1, -1, -1, -1}, {-1,
    1, -1, 1, -1, -1, 1}, {-1, 1, -1, 1, -1, 1, -1}, {-1, 1, -1, 1, -1, 1, 1},
    {-1, 1, -1, 1, 1, -1, -1}, {-1, 1, -1, 1, 1, -1, 1}, {-1, 1, -1, 1, 1, 1,
    -1}, {-1, 1, -1, 1, 1, 1, 1}, {-1, 1, 1, -1, -1, -1, -1}, {-1, 1, 1, -1,
    -1, -1, 1}, {-1, 1, 1, -1, -1, 1, -1}, {-1, 1, 1, -1, -1, 1, 1}, {-1, 1, 1,
    -1, 1, -1, -1}, {-1, 1, 1, -1, 1, -1, 1}, {-1, 1, 1, -1, 1, 1, -1}, {-1, 1,
    1, -1, 1, 1, 1}, {-1, 1, 1, 1, -1, -1, -1}, {-1, 1, 1, 1, -1, -1, 1}, {-1,
    1, 1, 1, -1, 1, -1}, {-1, 1, 1, 1, -1, 1, 1}, {-1, 1, 1, 1, 1, -1, -1},
    {-1, 1, 1, 1, 1, -1, 1}, {-1, 1, 1, 1, 1, 1, -1}, {-1, 1, 1, 1, 1, 1, 1},
    {1, -1, -1, -1, -1, -1, -1}, {1, -1, -1, -1, -1, -1, 1}, {1, -1, -1, -1,
    -1, 1, -1}, {1, -1, -1, -1, -1, 1, 1}, {1, -1, -1, -1, 1, -1, -1}, {1, -1,
    -1, -1, 1, -1, 1}, {1, -1, -1, -1, 1, 1, -1}, {1, -1, -1, -1, 1, 1, 1}, {1,
    -1, -1, 1, -1, -1, -1}, {1, -1, -1, 1, -1, -1, 1}, {1, -1, -1, 1, -1, 1,
    -1}, {1, -1, -1, 1, -1, 1, 1}, {1, -1, -1, 1, 1, -1, -1}, {1, -1, -1, 1, 1,
    -1, 1}, {1, -1, -1, 1, 1, 1, -1}, {1, -1, -1, 1, 1, 1, 1}, {1, -1, 1, -1,
    -1, -1, -1}, {1, -1, 1, -1, -1, -1, 1}, {1, -1, 1, -1, -1, 1, -1}, {1, -1,
    1, -1, -1, 1, 1}, {1, -1, 1, -1, 1, -1, -1}, {1, -1, 1, -1, 1, -1, 1}, {1,
    -1, 1, -1, 1, 1, -1}, {1, -1, 1, -1, 1, 1, 1}, {1, -1, 1, 1, -1, -1, -1},
    {1, -1, 1, 1, -1, -1, 1}, {1, -1, 1, 1, -1, 1, -1}, {1, -1, 1, 1, -1, 1,
    1}, {1, -1, 1, 1, 1, -1, -1}, {1, -1, 1, 1, 1, -1, 1}, {1, -1, 1, 1, 1, 1,
    -1}, {1, -1, 1, 1, 1, 1, 1}, {1, 1, -1, -1, -1, -1, -1}, {1, 1, -1, -1, -1,
    -1, 1}, {1, 1, -1, -1, -1, 1, -1}, {1, 1, -1, -1, -1, 1, 1}, {1, 1, -1, -1,
    1, -1, -1}, {1, 1, -1, -1, 1, -1, 1}, {1, 1, -1, -1, 1, 1, -1}, {1, 1, -1,
    -1, 1, 1, 1}, {1, 1, -1, 1, -1, -1, -1}, {1, 1, -1, 1, -1, -1, 1}, {1, 1,
    -1, 1, -1, 1, -1}, {1, 1, -1, 1, -1, 1, 1}, {1, 1, -1, 1, 1, -1, -1}, {1,
    1, -1, 1, 1, -1, 1}, {1, 1, -1, 1, 1, 1, -1}, {1, 1, -1, 1, 1, 1, 1}, {1,
    1, 1, -1, -1, -1, -1}, {1, 1, 1, -1, -1, -1, 1}, {1, 1, 1, -1, -1, 1, -1},
    {1, 1, 1, -1, -1, 1, 1}, {1, 1, 1, -1, 1, -1, -1}, {1, 1, 1, -1, 1, -1, 1},
    {1, 1, 1, -1, 1, 1, -1}, {1, 1, 1, -1, 1, 1, 1}, {1, 1, 1, 1, -1, -1, -1},
    {1, 1, 1, 1, -1, -1, 1}, {1, 1, 1, 1, -1, 1, -1}, {1, 1, 1, 1, -1, 1, 1},
    {1, 1, 1, 1, 1, -1, -1}, {1, 1, 1, 1, 1, -1, 1}, {1, 1, 1, 1, 1, 1, -1},
    {1, 1, 1, 1, 1, 1, 1}};

// Normalization factors the various processes
// Denominators: spins, colors and identical particles
int PY8MEs_R16_P0_sm_gg_ttxggg::denom_colors[nprocesses] = {64}; 
int PY8MEs_R16_P0_sm_gg_ttxggg::denom_hels[nprocesses] = {4}; 
int PY8MEs_R16_P0_sm_gg_ttxggg::denom_iden[nprocesses] = {6}; 

//--------------------------------------------------------------------------
// Color config initialization
void PY8MEs_R16_P0_sm_gg_ttxggg::initColorConfigs() 
{
  color_configs = vector < vec_vec_int > (); 
  jamp_nc_relative_power = vector < vec_int > (); 

  // Color flows of process Process: g g > t t~ g g g WEIGHTED<=5 @16
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(2)(2)(3)(1)(0)(0)(6)(4)(3)(5)(4)(6)(5)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #1
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(2)(2)(3)(1)(0)(0)(5)(4)(3)(5)(6)(6)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #2
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(2)(2)(3)(1)(0)(0)(6)(4)(5)(5)(3)(6)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #3
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(2)(2)(3)(1)(0)(0)(4)(4)(6)(5)(3)(6)(5)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #4
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(2)(2)(3)(1)(0)(0)(5)(4)(6)(5)(4)(6)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #5
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(2)(2)(3)(1)(0)(0)(4)(4)(5)(5)(6)(6)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #6
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(2)(4)(3)(1)(0)(0)(6)(4)(2)(5)(3)(6)(5)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #7
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(2)(4)(3)(1)(0)(0)(5)(4)(2)(5)(6)(6)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #8
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(2)(5)(3)(1)(0)(0)(6)(4)(2)(5)(4)(6)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #9
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(2)(6)(3)(1)(0)(0)(3)(4)(2)(5)(4)(6)(5)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #10
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(2)(6)(3)(1)(0)(0)(5)(4)(2)(5)(3)(6)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #11
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(2)(5)(3)(1)(0)(0)(3)(4)(2)(5)(6)(6)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #12
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(2)(5)(3)(1)(0)(0)(6)(4)(3)(5)(2)(6)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #13
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(2)(5)(3)(1)(0)(0)(4)(4)(6)(5)(2)(6)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #14
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(2)(4)(3)(1)(0)(0)(6)(4)(5)(5)(2)(6)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #15
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(2)(6)(3)(1)(0)(0)(3)(4)(5)(5)(2)(6)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #16
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(2)(6)(3)(1)(0)(0)(4)(4)(3)(5)(2)(6)(5)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #17
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(2)(4)(3)(1)(0)(0)(3)(4)(6)(5)(2)(6)(5)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #18
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(2)(6)(3)(1)(0)(0)(5)(4)(3)(5)(4)(6)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #19
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(2)(6)(3)(1)(0)(0)(4)(4)(5)(5)(3)(6)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #20
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(2)(4)(3)(1)(0)(0)(5)(4)(6)(5)(3)(6)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #21
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(2)(5)(3)(1)(0)(0)(3)(4)(6)(5)(4)(6)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #22
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(2)(5)(3)(1)(0)(0)(4)(4)(3)(5)(6)(6)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #23
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(2)(4)(3)(1)(0)(0)(3)(4)(5)(5)(6)(6)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #24
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(2)(1)(3)(1)(0)(0)(6)(4)(2)(5)(4)(6)(5)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #25
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(2)(1)(3)(1)(0)(0)(5)(4)(2)(5)(6)(6)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #26
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(2)(1)(3)(1)(0)(0)(6)(4)(5)(5)(2)(6)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #27
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(2)(1)(3)(1)(0)(0)(4)(4)(6)(5)(2)(6)(5)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #28
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(2)(1)(3)(1)(0)(0)(5)(4)(6)(5)(4)(6)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #29
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(2)(1)(3)(1)(0)(0)(4)(4)(5)(5)(6)(6)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #30
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(2)(1)(3)(1)(0)(0)(6)(4)(3)(5)(2)(6)(5)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #31
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(2)(1)(3)(1)(0)(0)(5)(4)(3)(5)(6)(6)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #32
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(2)(1)(3)(1)(0)(0)(6)(4)(3)(5)(4)(6)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #33
  color_configs[0].push_back(vec_int(createvector<int>
      (6)(2)(1)(3)(1)(0)(0)(2)(4)(3)(5)(4)(6)(5)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #34
  color_configs[0].push_back(vec_int(createvector<int>
      (6)(2)(1)(3)(1)(0)(0)(5)(4)(3)(5)(2)(6)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #35
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(2)(1)(3)(1)(0)(0)(2)(4)(3)(5)(6)(6)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #36
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(2)(1)(3)(1)(0)(0)(6)(4)(2)(5)(3)(6)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #37
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(2)(1)(3)(1)(0)(0)(4)(4)(6)(5)(3)(6)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #38
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(2)(1)(3)(1)(0)(0)(6)(4)(5)(5)(3)(6)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #39
  color_configs[0].push_back(vec_int(createvector<int>
      (6)(2)(1)(3)(1)(0)(0)(2)(4)(5)(5)(3)(6)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #40
  color_configs[0].push_back(vec_int(createvector<int>
      (6)(2)(1)(3)(1)(0)(0)(4)(4)(2)(5)(3)(6)(5)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #41
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(2)(1)(3)(1)(0)(0)(2)(4)(6)(5)(3)(6)(5)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #42
  color_configs[0].push_back(vec_int(createvector<int>
      (6)(2)(1)(3)(1)(0)(0)(5)(4)(2)(5)(4)(6)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #43
  color_configs[0].push_back(vec_int(createvector<int>
      (6)(2)(1)(3)(1)(0)(0)(4)(4)(5)(5)(2)(6)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #44
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(2)(1)(3)(1)(0)(0)(5)(4)(6)(5)(2)(6)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #45
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(2)(1)(3)(1)(0)(0)(2)(4)(6)(5)(4)(6)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #46
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(2)(1)(3)(1)(0)(0)(4)(4)(2)(5)(6)(6)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #47
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(2)(1)(3)(1)(0)(0)(2)(4)(5)(5)(6)(6)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #48
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(2)(2)(3)(1)(0)(0)(6)(4)(1)(5)(3)(6)(5)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #49
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(2)(2)(3)(1)(0)(0)(5)(4)(1)(5)(6)(6)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #50
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(2)(5)(3)(1)(0)(0)(6)(4)(1)(5)(2)(6)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #51
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(2)(6)(3)(1)(0)(0)(3)(4)(1)(5)(2)(6)(5)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #52
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(2)(6)(3)(1)(0)(0)(5)(4)(1)(5)(3)(6)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #53
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(2)(5)(3)(1)(0)(0)(3)(4)(1)(5)(6)(6)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #54
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(2)(4)(3)(1)(0)(0)(6)(4)(1)(5)(2)(6)(5)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #55
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(2)(4)(3)(1)(0)(0)(5)(4)(1)(5)(6)(6)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #56
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(2)(4)(3)(1)(0)(0)(6)(4)(1)(5)(3)(6)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #57
  color_configs[0].push_back(vec_int(createvector<int>
      (6)(2)(4)(3)(1)(0)(0)(2)(4)(1)(5)(3)(6)(5)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #58
  color_configs[0].push_back(vec_int(createvector<int>
      (6)(2)(4)(3)(1)(0)(0)(5)(4)(1)(5)(2)(6)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #59
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(2)(4)(3)(1)(0)(0)(2)(4)(1)(5)(6)(6)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #60
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(2)(2)(3)(1)(0)(0)(6)(4)(1)(5)(4)(6)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #61
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(2)(6)(3)(1)(0)(0)(3)(4)(1)(5)(4)(6)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #62
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(2)(5)(3)(1)(0)(0)(6)(4)(1)(5)(4)(6)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #63
  color_configs[0].push_back(vec_int(createvector<int>
      (6)(2)(5)(3)(1)(0)(0)(2)(4)(1)(5)(4)(6)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #64
  color_configs[0].push_back(vec_int(createvector<int>
      (6)(2)(2)(3)(1)(0)(0)(3)(4)(1)(5)(4)(6)(5)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #65
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(2)(6)(3)(1)(0)(0)(2)(4)(1)(5)(4)(6)(5)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #66
  color_configs[0].push_back(vec_int(createvector<int>
      (6)(2)(2)(3)(1)(0)(0)(5)(4)(1)(5)(3)(6)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #67
  color_configs[0].push_back(vec_int(createvector<int>
      (6)(2)(5)(3)(1)(0)(0)(3)(4)(1)(5)(2)(6)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #68
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(2)(6)(3)(1)(0)(0)(5)(4)(1)(5)(2)(6)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #69
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(2)(6)(3)(1)(0)(0)(2)(4)(1)(5)(3)(6)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #70
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(2)(2)(3)(1)(0)(0)(3)(4)(1)(5)(6)(6)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #71
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(2)(5)(3)(1)(0)(0)(2)(4)(1)(5)(6)(6)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #72
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(2)(2)(3)(1)(0)(0)(6)(4)(3)(5)(1)(6)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #73
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(2)(2)(3)(1)(0)(0)(4)(4)(6)(5)(1)(6)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #74
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(2)(4)(3)(1)(0)(0)(6)(4)(2)(5)(1)(6)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #75
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(2)(6)(3)(1)(0)(0)(3)(4)(2)(5)(1)(6)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #76
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(2)(6)(3)(1)(0)(0)(4)(4)(3)(5)(1)(6)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #77
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(2)(4)(3)(1)(0)(0)(3)(4)(6)(5)(1)(6)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #78
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(2)(5)(3)(1)(0)(0)(6)(4)(2)(5)(1)(6)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #79
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(2)(5)(3)(1)(0)(0)(4)(4)(6)(5)(1)(6)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #80
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(2)(5)(3)(1)(0)(0)(6)(4)(3)(5)(1)(6)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #81
  color_configs[0].push_back(vec_int(createvector<int>
      (6)(2)(5)(3)(1)(0)(0)(2)(4)(3)(5)(1)(6)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #82
  color_configs[0].push_back(vec_int(createvector<int>
      (6)(2)(5)(3)(1)(0)(0)(4)(4)(2)(5)(1)(6)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #83
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(2)(5)(3)(1)(0)(0)(2)(4)(6)(5)(1)(6)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #84
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(2)(2)(3)(1)(0)(0)(6)(4)(5)(5)(1)(6)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #85
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(2)(6)(3)(1)(0)(0)(3)(4)(5)(5)(1)(6)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #86
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(2)(4)(3)(1)(0)(0)(6)(4)(5)(5)(1)(6)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #87
  color_configs[0].push_back(vec_int(createvector<int>
      (6)(2)(4)(3)(1)(0)(0)(2)(4)(5)(5)(1)(6)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #88
  color_configs[0].push_back(vec_int(createvector<int>
      (6)(2)(2)(3)(1)(0)(0)(3)(4)(5)(5)(1)(6)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #89
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(2)(6)(3)(1)(0)(0)(2)(4)(5)(5)(1)(6)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #90
  color_configs[0].push_back(vec_int(createvector<int>
      (6)(2)(2)(3)(1)(0)(0)(4)(4)(3)(5)(1)(6)(5)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #91
  color_configs[0].push_back(vec_int(createvector<int>
      (6)(2)(4)(3)(1)(0)(0)(3)(4)(2)(5)(1)(6)(5)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #92
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(2)(6)(3)(1)(0)(0)(4)(4)(2)(5)(1)(6)(5)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #93
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(2)(6)(3)(1)(0)(0)(2)(4)(3)(5)(1)(6)(5)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #94
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(2)(2)(3)(1)(0)(0)(3)(4)(6)(5)(1)(6)(5)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #95
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(2)(4)(3)(1)(0)(0)(2)(4)(6)(5)(1)(6)(5)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #96
  color_configs[0].push_back(vec_int(createvector<int>
      (6)(2)(2)(3)(1)(0)(0)(5)(4)(3)(5)(4)(6)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #97
  color_configs[0].push_back(vec_int(createvector<int>
      (6)(2)(2)(3)(1)(0)(0)(4)(4)(5)(5)(3)(6)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #98
  color_configs[0].push_back(vec_int(createvector<int>
      (6)(2)(4)(3)(1)(0)(0)(5)(4)(2)(5)(3)(6)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #99
  color_configs[0].push_back(vec_int(createvector<int>
      (6)(2)(5)(3)(1)(0)(0)(3)(4)(2)(5)(4)(6)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #100
  color_configs[0].push_back(vec_int(createvector<int>
      (6)(2)(5)(3)(1)(0)(0)(4)(4)(3)(5)(2)(6)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #101
  color_configs[0].push_back(vec_int(createvector<int>
      (6)(2)(4)(3)(1)(0)(0)(3)(4)(5)(5)(2)(6)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #102
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(2)(6)(3)(1)(0)(0)(5)(4)(2)(5)(4)(6)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #103
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(2)(6)(3)(1)(0)(0)(4)(4)(5)(5)(2)(6)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #104
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(2)(6)(3)(1)(0)(0)(5)(4)(3)(5)(2)(6)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #105
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(2)(6)(3)(1)(0)(0)(2)(4)(3)(5)(4)(6)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #106
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(2)(6)(3)(1)(0)(0)(4)(4)(2)(5)(3)(6)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #107
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(2)(6)(3)(1)(0)(0)(2)(4)(5)(5)(3)(6)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #108
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(2)(2)(3)(1)(0)(0)(5)(4)(6)(5)(3)(6)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #109
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(2)(5)(3)(1)(0)(0)(3)(4)(6)(5)(2)(6)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #110
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(2)(4)(3)(1)(0)(0)(5)(4)(6)(5)(2)(6)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #111
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(2)(4)(3)(1)(0)(0)(2)(4)(6)(5)(3)(6)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #112
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(2)(2)(3)(1)(0)(0)(3)(4)(6)(5)(4)(6)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #113
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(2)(5)(3)(1)(0)(0)(2)(4)(6)(5)(4)(6)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #114
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(2)(2)(3)(1)(0)(0)(4)(4)(3)(5)(6)(6)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #115
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(2)(4)(3)(1)(0)(0)(3)(4)(2)(5)(6)(6)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #116
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(2)(5)(3)(1)(0)(0)(4)(4)(2)(5)(6)(6)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #117
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(2)(5)(3)(1)(0)(0)(2)(4)(3)(5)(6)(6)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #118
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(2)(2)(3)(1)(0)(0)(3)(4)(5)(5)(6)(6)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #119
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(2)(4)(3)(1)(0)(0)(2)(4)(5)(5)(6)(6)(1)));
  jamp_nc_relative_power[0].push_back(0); 
}

//--------------------------------------------------------------------------
// Destructor.
PY8MEs_R16_P0_sm_gg_ttxggg::~PY8MEs_R16_P0_sm_gg_ttxggg() 
{
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    delete[] p[i]; 
    p[i] = NULL; 
  }
}

//--------------------------------------------------------------------------
// Invert the permutation mapping
vector<int> PY8MEs_R16_P0_sm_gg_ttxggg::invert_mapping(vector<int> mapping) 
{
  vector<int> inverted_mapping; 
  for (unsigned int i = 0; i < mapping.size(); i++ )
  {
    for (unsigned int j = 0; j < mapping.size(); j++ )
    {
      if (mapping[j] == ((int)i))
      {
        inverted_mapping.push_back(j); 
        break; 
      }
    }
  }
  return inverted_mapping; 
}

//--------------------------------------------------------------------------
// Return the list of possible helicity configurations
vector < vec_int > PY8MEs_R16_P0_sm_gg_ttxggg::getHelicityConfigs(vector<int>
    permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(ncomb, vector<int> (nexternal, 0)); 
  for (unsigned int ihel = 0; ihel < ncomb; ihel++ )
  {
    for(unsigned int j = 0; j < nexternal; j++ )
    {
      res[ihel][chosenPerm[j]] = helicities[ihel][j]; 
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Return the list of possible color configurations
vector < vec_int > PY8MEs_R16_P0_sm_gg_ttxggg::getColorConfigs(int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(color_configs[chosenProcID].size(), vector<int>
      (nexternal * 2, 0));
  for (unsigned int icol = 0; icol < color_configs[chosenProcID].size(); icol++
      )
  {
    for(unsigned int j = 0; j < (2 * nexternal); j++ )
    {
      res[icol][chosenPerm[j/2] * 2 + j%2] =
          color_configs[chosenProcID][icol][j];
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Get JAMP relative N_c power
int PY8MEs_R16_P0_sm_gg_ttxggg::getColorFlowRelativeNCPower(int color_flow_ID,
    int specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return jamp_nc_relative_power[chosenProcID][color_flow_ID]; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity ID -> Helicity Config
vector<int> PY8MEs_R16_P0_sm_gg_ttxggg::getHelicityConfigForID(int hel_ID,
    vector<int> permutation)
{
  if (hel_ID < 0 || hel_ID >= ncomb)
  {
    cerr <<  "Error in function 'getHelicityConfigForID' of class" << 
    " 'PY8MEs_R16_P0_sm_gg_ttxggg': Specified helicity ID '" << 
    hel_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R16_P0_sm_gg_ttxggg_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(nexternal, 0); 
  for (unsigned int j = 0; j < nexternal; j++ )
  {
    res[chosenPerm[j]] = helicities[hel_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity Config -> Helicity ID
int PY8MEs_R16_P0_sm_gg_ttxggg::getHelicityIDForConfig(vector<int> hel_config,
    vector<int> permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  int user_ihel = -1; 
  if (hel_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < ncomb; i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < nexternal; j++ )
      {
        if (helicities[i][chosenPerm[j]] != hel_config[j])
        {
          found = false; 
          break; 
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_ihel = i; 
        break; 
      }
    }
    if (user_ihel == -1)
    {
      cerr <<  "Error in function 'getHelicityIDForConfig' of class" << 
      " 'PY8MEs_R16_P0_sm_gg_ttxggg': Specified helicity" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R16_P0_sm_gg_ttxggg_exception; 
    }
  }
  return user_ihel; 
}


//--------------------------------------------------------------------------
// Implements the map Color ID -> Color Config
vector<int> PY8MEs_R16_P0_sm_gg_ttxggg::getColorConfigForID(int color_ID, int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < 0 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getColorConfigForID' of class" << 
    " 'PY8MEs_R16_P0_sm_gg_ttxggg': Specified color ID '" << 
    color_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R16_P0_sm_gg_ttxggg_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(color_configs[chosenProcID][color_ID].size(), 0); 
  for (unsigned int j = 0; j < (2 * nexternal); j++ )
  {
    res[chosenPerm[j/2] * 2 + j%2] = color_configs[chosenProcID][color_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Color Config -> Color ID
int PY8MEs_R16_P0_sm_gg_ttxggg::getColorIDForConfig(vector<int> color_config,
    int specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = -1; 
  if (color_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < color_configs[chosenProcID].size(); i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < (nexternal * 2); j++ )
      {

        // If colorless then make sure it matches
        // The little arithmetics in the color index is just
        // the permutation applies on the particle list which is
        // twice smaller since each particle can have two color indices.
        if (color_config[j] == 0)
        {
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] != 0)
          {
            found = false; 
            break; 
          }
          // Otherwise check that the color linked position matches
        }
        else
        {
          int color_linked_pos = -1; 
          // Find the other end of the line in the user color config
          for (unsigned int k = 0; k < (nexternal * 2); k++ )
          {
            if (k == j)
              continue; 
            if (color_config[j] == color_config[k])
            {
              color_linked_pos = k; 
              break; 
            }
          }
          if (color_linked_pos == -1)
          {
            cerr <<  "Error in function 'getColorIDForConfig' of class" << 
            " 'PY8MEs_R16_P0_sm_gg_ttxggg': A color line could " << 
            " not be closed." << endl; 
            throw PY8MEs_R16_P0_sm_gg_ttxggg_exception; 
          }
          // Now check whether the color line matches
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] !=
              color_configs[chosenProcID][i][chosenPerm[color_linked_pos/2] * 2
              + color_linked_pos%2])
          {
            found = false; 
            break; 
          }
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_icol = i; 
        break; 
      }
    }

    if (user_icol == -1)
    {
      cerr <<  "Error in function 'getColorIDForConfig' of class" << 
      " 'PY8MEs_R16_P0_sm_gg_ttxggg': Specified color" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R16_P0_sm_gg_ttxggg_exception; 
    }
  }
  return user_icol; 
}

//--------------------------------------------------------------------------
// Returns all result previously computed in SigmaKin
vector < vec_double > PY8MEs_R16_P0_sm_gg_ttxggg::getAllResults(int
    specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return all_results[chosenProcID]; 
}

//--------------------------------------------------------------------------
// Returns a result previously computed in SigmaKin for a specific helicity
// and color ID. -1 means avg and summed over that characteristic.
double PY8MEs_R16_P0_sm_gg_ttxggg::getResult(int helicity_ID, int color_ID, int
    specify_proc_ID)
{
  if (helicity_ID < - 1 || helicity_ID >= ncomb)
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R16_P0_sm_gg_ttxggg': Specified helicity ID '" << 
    helicity_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R16_P0_sm_gg_ttxggg_exception; 
  }
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < - 1 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R16_P0_sm_gg_ttxggg': Specified color ID '" << 
    color_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R16_P0_sm_gg_ttxggg_exception; 
  }
  return all_results[chosenProcID][helicity_ID + 1][color_ID + 1]; 
}

//--------------------------------------------------------------------------
// Check for the availability of the requested process and if available,
// If available, this returns the corresponding permutation and Proc_ID to use.
// If not available, this returns a negative Proc_ID.
pair < vector<int> , int > PY8MEs_R16_P0_sm_gg_ttxggg::static_getPY8ME(vector<int> initial_pdgs, vector<int> final_pdgs, set<int> schannels) 
{

  // Not available return value
  pair < vector<int> , int > NA(vector<int> (), -1); 

  // Check if s-channel requirements match
  if (nreq_s_channels > 0)
  {
    if (schannels != s_channel_proc)
      return NA; 
  }
  else
  {
    if (schannels.size() != 0)
      return NA; 
  }

  // Check number of final state particles
  if (final_pdgs.size() != (nexternal - ninitial))
    return NA; 

  // Check number of initial state particles
  if (initial_pdgs.size() != ninitial)
    return NA; 

  // List of processes available in this class
  const int nprocs = 1; 
  const int proc_IDS[nprocs] = {0}; 
  const int in_pdgs[nprocs][ninitial] = {{21, 21}}; 
  const int out_pdgs[nprocs][nexternal - ninitial] = {{6, -6, 21, 21, 21}}; 

  bool in_pdgs_used[ninitial]; 
  bool out_pdgs_used[nexternal - ninitial]; 
  for(unsigned int i = 0; i < nprocs; i++ )
  {
    int permutations[nexternal]; 

    // Reinitialize initial state look-up variables
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      in_pdgs_used[j] = false; 
      permutations[j] = -1; 
    }
    // Look for initial state matches
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      for(unsigned int k = 0; k < ninitial; k++ )
      {
        // Make sure it has not been used already
        if (in_pdgs_used[k])
          continue; 
        if (initial_pdgs[k] == in_pdgs[i][j])
        {
          permutations[j] = k; 
          in_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[ninitial - 1] == -1)
      continue; 

    // Reinitialize final state look-up variables
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      out_pdgs_used[j] = false; 
      permutations[ninitial + j] = -1; 
    }
    // Look for final state matches
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      for(unsigned int k = 0; k < (nexternal - ninitial); k++ )
      {
        // Make sure it has not been used already
        if (out_pdgs_used[k])
          continue; 
        if (final_pdgs[k] == out_pdgs[i][j])
        {
          permutations[ninitial + j] = ninitial + k; 
          out_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[ninitial + j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[nexternal - 1] == -1)
      continue; 

    // Return process found
    return pair < vector<int> , int > (vector<int> (permutations, permutations
        + nexternal), proc_IDS[i]);
  }

  // No process found
  return NA; 
}

//--------------------------------------------------------------------------
// Set momenta
void PY8MEs_R16_P0_sm_gg_ttxggg::setMomenta(vector < vec_double >
    momenta_picked)
{
  if (momenta_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setMomenta' of class" << 
    " 'PY8MEs_R16_P0_sm_gg_ttxggg': Incorrect number of" << 
    " momenta specified." << endl; 
    throw PY8MEs_R16_P0_sm_gg_ttxggg_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    if (momenta_picked[i].size() != 4)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R16_P0_sm_gg_ttxggg': Incorrect number of" << 
      " momenta components specified." << endl; 
      throw PY8MEs_R16_P0_sm_gg_ttxggg_exception; 
    }
    if (momenta_picked[i][0] < 0.0)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R16_P0_sm_gg_ttxggg': A momentum was specified" << 
      " with negative energy. Check conventions." << endl; 
      throw PY8MEs_R16_P0_sm_gg_ttxggg_exception; 
    }
    for (unsigned int j = 0; j < 4; j++ )
    {
      p[i][j] = momenta_picked[i][j]; 
    }
  }
}

//--------------------------------------------------------------------------
// Set color configuration to use. An empty vector means sum over all.
void PY8MEs_R16_P0_sm_gg_ttxggg::setColors(vector<int> colors_picked)
{
  if (colors_picked.size() == 0)
  {
    user_colors = vector<int> (); 
    return; 
  }
  if (colors_picked.size() != (2 * nexternal))
  {
    cerr <<  "Error in function 'setColors' of class" << 
    " 'PY8MEs_R16_P0_sm_gg_ttxggg': Incorrect number" << 
    " of colors specified." << endl; 
    throw PY8MEs_R16_P0_sm_gg_ttxggg_exception; 
  }
  user_colors = vector<int> ((2 * nexternal), 0); 
  for(unsigned int i = 0; i < (2 * nexternal); i++ )
  {
    user_colors[i] = colors_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the helicity configuration to use. Am empty vector means sum over all.
void PY8MEs_R16_P0_sm_gg_ttxggg::setHelicities(vector<int> helicities_picked) 
{
  if (helicities_picked.size() != nexternal)
  {
    if (helicities_picked.size() == 0)
    {
      user_helicities = vector<int> (); 
      return; 
    }
    cerr <<  "Error in function 'setHelicities' of class" << 
    " 'PY8MEs_R16_P0_sm_gg_ttxggg': Incorrect number" << 
    " of helicities specified." << endl; 
    throw PY8MEs_R16_P0_sm_gg_ttxggg_exception; 
  }
  user_helicities = vector<int> (nexternal, 0); 
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    user_helicities[i] = helicities_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the permutation to use (will apply to momenta, colors and helicities)
void PY8MEs_R16_P0_sm_gg_ttxggg::setPermutation(vector<int> perm_picked) 
{
  if (perm_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setPermutations' of class" << 
    " 'PY8MEs_R16_P0_sm_gg_ttxggg': Incorrect number" << 
    " of permutations specified." << endl; 
    throw PY8MEs_R16_P0_sm_gg_ttxggg_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    perm[i] = perm_picked[i]; 
  }
}

// Set the proc_ID to use
void PY8MEs_R16_P0_sm_gg_ttxggg::setProcID(int procID_picked) 
{
  proc_ID = procID_picked; 
}

//--------------------------------------------------------------------------
// Initialize process.

void PY8MEs_R16_P0_sm_gg_ttxggg::initProc() 
{

  // Initialize flags
  include_symmetry_factors = true; 
  include_helicity_averaging_factors = true; 
  include_color_averaging_factors = true; 

  // Initialize vectors.
  perm = vector<int> (nexternal, 0); 
  user_colors = vector<int> (2 * nexternal, 0); 
  user_helicities = vector<int> (nexternal, 0); 
  p = vector < double * > (); 
  for (unsigned int i = 0; i < nexternal; i++ )
  {
    p.push_back(new double[4]); 
  }
  initColorConfigs(); 
  // Synchronize local variables dependent on the model with the active model.
  mME = vector<double> (nexternal, 0.); 
  syncProcModelParams(); 
  jamp2 = vector < vec_double > (1); 
  jamp2[0] = vector<double> (120, 0.); 
  all_results = vector < vec_vec_double > (1); 
  // The first entry is always the color or helicity avg/summed matrix element.
  all_results[0] = vector < vec_double > (ncomb + 1, vector<double> (120 + 1,
      0.));
}

// Synchronize local variables of the process that depend on the model
// parameters
void PY8MEs_R16_P0_sm_gg_ttxggg::syncProcModelParams() 
{

  // Instantiate the model class and set parameters that stay fixed during run
  mME[0] = pars->ZERO; 
  mME[1] = pars->ZERO; 
  mME[2] = pars->mdl_MT; 
  mME[3] = pars->mdl_MT; 
  mME[4] = pars->ZERO; 
  mME[5] = pars->ZERO; 
  mME[6] = pars->ZERO; 
}

//--------------------------------------------------------------------------
// Setter allowing to force particular values for the external masses
void PY8MEs_R16_P0_sm_gg_ttxggg::setMasses(vec_double external_masses) 
{

  if (external_masses.size() != mME.size())
  {
    cerr <<  "Error in function 'setMasses' of class" << 
    " 'PY8MEs_R16_P0_sm_gg_ttxggg': Incorrect number of" << 
    " masses specified." << endl; 
    throw PY8MEs_R16_P0_sm_gg_ttxggg_exception; 
  }
  for (unsigned int j = 0; j < mME.size(); j++ )
  {
    mME[j] = external_masses[perm[j]]; 
  }
}

//--------------------------------------------------------------------------
// Getter accessing external masses with the correct ordering
vector<double> PY8MEs_R16_P0_sm_gg_ttxggg::getMasses() 
{

  vec_double external_masses; 
  vector<int> invertedPerm; 
  invertedPerm = invert_mapping(perm); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    external_masses.push_back(mME[invertedPerm[i]]); 
  }
  return external_masses; 

}


// Set all values of the external masses to float(-mode) where mode can be
// 0 : Mass taken from the model
// 1 : Mass taken from p_i^2 if not massless to begin with
// 2 : Mass always taken from p_i^2.
void PY8MEs_R16_P0_sm_gg_ttxggg::setExternalMassesMode(int mode) 
{
  if (mode != 0 && mode != 1 && mode != 2)
  {
    cerr <<  "Error in function 'setExternalMassesMode' of class" << 
    " 'PY8MEs_R16_P0_sm_gg_ttxggg': Incorrect mode selected :" << mode << 
    ". It must be either 0, 1 or 2" << endl; 
    throw PY8MEs_R16_P0_sm_gg_ttxggg_exception; 
  }
  if (mode == 0)
  {
    syncProcModelParams(); 
  }
  else if (mode == 1)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      if (mME[j] != pars->ZERO)
      {
        mME[j] = -1.0; 
      }
    }
  }
  else if (mode == 2)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      mME[j] = -1.0; 
    }
  }
}

//--------------------------------------------------------------------------
// Evaluate the squared matrix element.

double PY8MEs_R16_P0_sm_gg_ttxggg::sigmaKin() 
{
  // Set the parameters which change event by event
  pars->setDependentParameters(); 
  pars->setDependentCouplings(); 
  // Reset color flows
  for(int i = 0; i < 120; i++ )
    jamp2[0][i] = 0.; 

  // Save previous values of mME
  vector<double> saved_mME(mME.size(), 0.0); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (mME[i] < 0.0)
    {
      saved_mME[i] = mME[i]; 
      mME[i] = sqrt(abs(pow(p[perm[i]][0], 2) - 
      (pow(p[perm[i]][1], 2) + pow(p[perm[i]][2], 2) + pow(p[perm[i]][3],
          2))));
    }
  }

  // Local variables and constants
  const int max_tries = 10; 
  vector < vec_bool > goodhel(nprocesses, vec_bool(ncomb, false)); 
  vec_int ntry(nprocesses, 0); 
  double t = 0.; 
  double result = 0.; 

  if (ntry[proc_ID] <= max_tries)
    ntry[proc_ID] = ntry[proc_ID] + 1; 

  // Find which helicity configuration is asked for
  // -1 indicates one wants to sum over helicities
  int user_ihel = getHelicityIDForConfig(user_helicities); 

  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = getColorIDForConfig(user_colors); 

  // Reset the list of results that will be recomputed here
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      all_results[proc_ID][ihel + 1][icolor + 1] = 0.; 
    }
  }

  // Calculate the matrix element for all helicities
  // unless already detected as vanishing
  for(int ihel = 0; ihel < ncomb; ihel++ )
  {
    // Skip helicity if already detected as vanishing
    if ((ntry[proc_ID] >= max_tries) && !goodhel[proc_ID][ihel])
      continue; 

    // Also skip helicity if user asks for a specific one
    if ((ntry[proc_ID] >= max_tries) && user_ihel != -1 && user_ihel != ihel)
      continue; 

    calculate_wavefunctions(helicities[ihel]); 

    // Reset locally computed color flows
    for(int i = 0; i < 120; i++ )
      jamp2[0][i] = 0.; 

    if (proc_ID == 0)
      t = matrix_16_gg_ttxggg(); 

    // Store which helicities give non-zero result
    if ((ntry[proc_ID] < max_tries) && t != 0. && !goodhel[proc_ID][ihel])
      goodhel[proc_ID][ihel] = true; 

    // Aggregate results
    if (user_ihel == -1 || user_ihel == ihel)
    {
      if (user_icol == -1)
      {
        result = result + t; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][0] += t; 
          for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
          {
            all_results[proc_ID][0][i + 1] += jamp2[proc_ID][i]; 
          }
        }
        all_results[proc_ID][ihel + 1][0] += t; 
        for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
        {
          all_results[proc_ID][ihel + 1][i + 1] += jamp2[proc_ID][i]; 
        }
      }
      else
      {
        result = result + jamp2[proc_ID][user_icol]; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][user_icol + 1] += jamp2[proc_ID][user_icol]; 
        }
        all_results[proc_ID][ihel + 1][user_icol + 1] +=
            jamp2[proc_ID][user_icol];
      }
    }

  }

  // Normalize results with the identical particle factor
  if (include_symmetry_factors)
  {
    result = result/denom_iden[proc_ID]; 
  }
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      if (include_symmetry_factors)
      {
        all_results[proc_ID][ihel + 1][icolor + 1] /= denom_iden[proc_ID]; 
      }
    }
  }


  // Normalize when when summing+averaging over helicity configurations
  if (user_ihel == -1 && include_helicity_averaging_factors)
  {
    result /= denom_hels[proc_ID]; 
    if (user_icol == -1)
    {
      all_results[proc_ID][0][0] /= denom_hels[proc_ID]; 
      for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
      {
        all_results[proc_ID][0][i + 1] /= denom_hels[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][0][user_icol + 1] /= denom_hels[proc_ID]; 
    }
  }

  // Normalize when summing+averaging over color configurations
  if (user_icol == -1 && include_color_averaging_factors)
  {
    result /= denom_colors[proc_ID]; 
    if (user_ihel == -1)
    {
      all_results[proc_ID][0][0] /= denom_colors[proc_ID]; 
      for (unsigned int i = 0; i < ncomb; i++ )
      {
        all_results[proc_ID][i + 1][0] /= denom_colors[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][user_ihel + 1][0] /= denom_colors[proc_ID]; 
    }
  }

  // Reinstate previous values of mME
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (saved_mME[i] < 0.0)
    {
      mME[i] = saved_mME[i]; 
    }
  }

  // Finally return it
  return result; 
}

//==========================================================================
// Private class member functions

//--------------------------------------------------------------------------
// Evaluate |M|^2 for each subprocess

void PY8MEs_R16_P0_sm_gg_ttxggg::calculate_wavefunctions(const int hel[])
{
  // Calculate wavefunctions for all processes
  // Calculate all wavefunctions
  vxxxxx(p[perm[0]], mME[0], hel[0], -1, w[0]); 
  vxxxxx(p[perm[1]], mME[1], hel[1], -1, w[1]); 
  oxxxxx(p[perm[2]], mME[2], hel[2], +1, w[2]); 
  ixxxxx(p[perm[3]], mME[3], hel[3], -1, w[3]); 
  vxxxxx(p[perm[4]], mME[4], hel[4], +1, w[4]); 
  vxxxxx(p[perm[5]], mME[5], hel[5], +1, w[5]); 
  vxxxxx(p[perm[6]], mME[6], hel[6], +1, w[6]); 
  VVV1P0_1(w[0], w[1], pars->GC_10, pars->ZERO, pars->ZERO, w[7]); 
  FFV1P0_3(w[3], w[2], pars->GC_11, pars->ZERO, pars->ZERO, w[8]); 
  VVV1P0_1(w[7], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[9]); 
  VVV1P0_1(w[8], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[10]); 
  VVV1P0_1(w[8], w[6], pars->GC_10, pars->ZERO, pars->ZERO, w[11]); 
  VVV1P0_1(w[7], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[12]); 
  VVV1P0_1(w[8], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[13]); 
  VVV1P0_1(w[7], w[6], pars->GC_10, pars->ZERO, pars->ZERO, w[14]); 
  VVVV1P0_1(w[7], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[15]); 
  VVVV3P0_1(w[7], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[16]); 
  VVVV4P0_1(w[7], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[17]); 
  VVVV1P0_1(w[7], w[4], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[18]); 
  VVVV3P0_1(w[7], w[4], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[19]); 
  VVVV4P0_1(w[7], w[4], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[20]); 
  VVVV1P0_1(w[7], w[5], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[21]); 
  VVVV3P0_1(w[7], w[5], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[22]); 
  VVVV4P0_1(w[7], w[5], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[23]); 
  VVV1P0_1(w[4], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[24]); 
  VVV1P0_1(w[7], w[8], pars->GC_10, pars->ZERO, pars->ZERO, w[25]); 
  VVV1P0_1(w[7], w[24], pars->GC_10, pars->ZERO, pars->ZERO, w[26]); 
  VVV1P0_1(w[4], w[6], pars->GC_10, pars->ZERO, pars->ZERO, w[27]); 
  VVV1P0_1(w[7], w[27], pars->GC_10, pars->ZERO, pars->ZERO, w[28]); 
  VVV1P0_1(w[5], w[6], pars->GC_10, pars->ZERO, pars->ZERO, w[29]); 
  VVV1P0_1(w[7], w[29], pars->GC_10, pars->ZERO, pars->ZERO, w[30]); 
  VVVV1P0_1(w[4], w[5], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[31]); 
  VVVV3P0_1(w[4], w[5], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[32]); 
  VVVV4P0_1(w[4], w[5], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[33]); 
  FFV1_1(w[2], w[4], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[34]); 
  FFV1_2(w[3], w[7], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[35]); 
  FFV1_1(w[34], w[5], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[36]); 
  FFV1_1(w[34], w[6], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[37]); 
  FFV1P0_3(w[3], w[34], pars->GC_11, pars->ZERO, pars->ZERO, w[38]); 
  FFV1_2(w[3], w[5], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[39]); 
  FFV1_1(w[34], w[7], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[40]); 
  FFV1_2(w[39], w[7], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[41]); 
  FFV1_2(w[3], w[6], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[42]); 
  FFV1_2(w[42], w[7], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[43]); 
  FFV1_1(w[2], w[5], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[44]); 
  FFV1_1(w[44], w[4], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[45]); 
  FFV1_1(w[44], w[6], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[46]); 
  FFV1P0_3(w[3], w[44], pars->GC_11, pars->ZERO, pars->ZERO, w[47]); 
  FFV1_2(w[3], w[4], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[48]); 
  FFV1_1(w[44], w[7], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[49]); 
  FFV1_2(w[48], w[7], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[50]); 
  FFV1_1(w[2], w[6], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[51]); 
  FFV1_1(w[51], w[4], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[52]); 
  FFV1_1(w[51], w[5], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[53]); 
  FFV1P0_3(w[3], w[51], pars->GC_11, pars->ZERO, pars->ZERO, w[54]); 
  FFV1_1(w[51], w[7], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[55]); 
  FFV1_1(w[2], w[7], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[56]); 
  FFV1_2(w[48], w[5], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[57]); 
  FFV1_2(w[48], w[6], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[58]); 
  FFV1P0_3(w[48], w[2], pars->GC_11, pars->ZERO, pars->ZERO, w[59]); 
  FFV1_2(w[39], w[4], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[60]); 
  FFV1_2(w[39], w[6], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[61]); 
  FFV1P0_3(w[39], w[2], pars->GC_11, pars->ZERO, pars->ZERO, w[62]); 
  FFV1_2(w[42], w[4], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[63]); 
  FFV1_2(w[42], w[5], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[64]); 
  FFV1P0_3(w[42], w[2], pars->GC_11, pars->ZERO, pars->ZERO, w[65]); 
  FFV1_2(w[3], w[24], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[66]); 
  VVV1P0_1(w[24], w[6], pars->GC_10, pars->ZERO, pars->ZERO, w[67]); 
  FFV1_1(w[2], w[24], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[68]); 
  FFV1_2(w[3], w[27], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[69]); 
  VVV1P0_1(w[27], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[70]); 
  FFV1_1(w[2], w[27], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[71]); 
  FFV1_2(w[3], w[29], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[72]); 
  VVV1P0_1(w[4], w[29], pars->GC_10, pars->ZERO, pars->ZERO, w[73]); 
  FFV1_1(w[2], w[29], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[74]); 
  FFV1_1(w[2], w[0], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[75]); 
  FFV1_2(w[3], w[1], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[76]); 
  FFV1_1(w[75], w[4], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[77]); 
  FFV1_2(w[76], w[5], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[78]); 
  FFV1_2(w[76], w[6], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[79]); 
  FFV1_1(w[75], w[5], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[80]); 
  FFV1_2(w[76], w[4], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[81]); 
  FFV1_1(w[75], w[6], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[82]); 
  FFV1P0_3(w[76], w[75], pars->GC_11, pars->ZERO, pars->ZERO, w[83]); 
  FFV1_1(w[75], w[24], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[84]); 
  FFV1_1(w[75], w[27], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[85]); 
  FFV1_1(w[75], w[29], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[86]); 
  VVV1P0_1(w[1], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[87]); 
  FFV1P0_3(w[3], w[75], pars->GC_11, pars->ZERO, pars->ZERO, w[88]); 
  VVV1P0_1(w[87], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[89]); 
  VVV1P0_1(w[87], w[6], pars->GC_10, pars->ZERO, pars->ZERO, w[90]); 
  FFV1_2(w[3], w[87], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[91]); 
  FFV1_1(w[75], w[87], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[92]); 
  FFV1P0_3(w[39], w[75], pars->GC_11, pars->ZERO, pars->ZERO, w[93]); 
  FFV1P0_3(w[42], w[75], pars->GC_11, pars->ZERO, pars->ZERO, w[94]); 
  VVV1P0_1(w[1], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[95]); 
  VVV1P0_1(w[95], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[96]); 
  VVV1P0_1(w[95], w[6], pars->GC_10, pars->ZERO, pars->ZERO, w[97]); 
  FFV1_2(w[3], w[95], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[98]); 
  FFV1_1(w[75], w[95], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[99]); 
  FFV1P0_3(w[48], w[75], pars->GC_11, pars->ZERO, pars->ZERO, w[100]); 
  VVV1P0_1(w[1], w[6], pars->GC_10, pars->ZERO, pars->ZERO, w[101]); 
  VVV1P0_1(w[101], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[102]); 
  VVV1P0_1(w[101], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[103]); 
  FFV1_2(w[3], w[101], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[104]); 
  FFV1_1(w[75], w[101], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[105]); 
  FFV1_1(w[75], w[1], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[106]); 
  FFV1_2(w[48], w[1], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[107]); 
  FFV1_2(w[39], w[1], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[108]); 
  FFV1_2(w[42], w[1], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[109]); 
  VVV1P0_1(w[1], w[24], pars->GC_10, pars->ZERO, pars->ZERO, w[110]); 
  VVV1P0_1(w[1], w[27], pars->GC_10, pars->ZERO, pars->ZERO, w[111]); 
  VVV1P0_1(w[1], w[29], pars->GC_10, pars->ZERO, pars->ZERO, w[112]); 
  VVVV1P0_1(w[1], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[113]); 
  VVVV3P0_1(w[1], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[114]); 
  VVVV4P0_1(w[1], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[115]); 
  VVVV1P0_1(w[1], w[4], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[116]); 
  VVVV3P0_1(w[1], w[4], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[117]); 
  VVVV4P0_1(w[1], w[4], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[118]); 
  VVVV1P0_1(w[1], w[5], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[119]); 
  VVVV3P0_1(w[1], w[5], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[120]); 
  VVVV4P0_1(w[1], w[5], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[121]); 
  FFV1_2(w[3], w[0], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[122]); 
  FFV1_1(w[2], w[1], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[123]); 
  FFV1_2(w[122], w[4], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[124]); 
  FFV1_1(w[123], w[5], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[125]); 
  FFV1_1(w[123], w[6], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[126]); 
  FFV1_2(w[122], w[5], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[127]); 
  FFV1_1(w[123], w[4], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[128]); 
  FFV1_2(w[122], w[6], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[129]); 
  FFV1P0_3(w[122], w[123], pars->GC_11, pars->ZERO, pars->ZERO, w[130]); 
  FFV1_2(w[122], w[24], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[131]); 
  FFV1_2(w[122], w[27], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[132]); 
  FFV1_2(w[122], w[29], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[133]); 
  FFV1P0_3(w[122], w[2], pars->GC_11, pars->ZERO, pars->ZERO, w[134]); 
  FFV1_1(w[2], w[87], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[135]); 
  FFV1_2(w[122], w[87], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[136]); 
  FFV1P0_3(w[122], w[44], pars->GC_11, pars->ZERO, pars->ZERO, w[137]); 
  FFV1P0_3(w[122], w[51], pars->GC_11, pars->ZERO, pars->ZERO, w[138]); 
  FFV1_1(w[2], w[95], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[139]); 
  FFV1_2(w[122], w[95], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[140]); 
  FFV1P0_3(w[122], w[34], pars->GC_11, pars->ZERO, pars->ZERO, w[141]); 
  FFV1_1(w[2], w[101], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[142]); 
  FFV1_2(w[122], w[101], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[143]); 
  FFV1_2(w[122], w[1], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[144]); 
  FFV1_1(w[34], w[1], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[145]); 
  FFV1_1(w[44], w[1], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[146]); 
  FFV1_1(w[51], w[1], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[147]); 
  VVV1P0_1(w[0], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[148]); 
  FFV1_2(w[3], w[148], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[149]); 
  VVV1P0_1(w[148], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[150]); 
  FFV1P0_3(w[3], w[123], pars->GC_11, pars->ZERO, pars->ZERO, w[151]); 
  VVV1P0_1(w[148], w[6], pars->GC_10, pars->ZERO, pars->ZERO, w[152]); 
  VVVV1P0_1(w[148], w[5], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[153]); 
  VVVV3P0_1(w[148], w[5], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[154]); 
  VVVV4P0_1(w[148], w[5], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[155]); 
  FFV1_1(w[123], w[148], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[156]); 
  FFV1_2(w[39], w[148], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[157]); 
  FFV1_2(w[42], w[148], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[158]); 
  VVV1P0_1(w[148], w[29], pars->GC_10, pars->ZERO, pars->ZERO, w[159]); 
  FFV1_1(w[2], w[148], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[160]); 
  FFV1P0_3(w[76], w[2], pars->GC_11, pars->ZERO, pars->ZERO, w[161]); 
  FFV1_2(w[76], w[148], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[162]); 
  FFV1_1(w[44], w[148], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[163]); 
  FFV1_1(w[51], w[148], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[164]); 
  VVV1P0_1(w[148], w[95], pars->GC_10, pars->ZERO, pars->ZERO, w[165]); 
  VVV1P0_1(w[148], w[8], pars->GC_10, pars->ZERO, pars->ZERO, w[166]); 
  VVV1P0_1(w[148], w[101], pars->GC_10, pars->ZERO, pars->ZERO, w[167]); 
  VVV1P0_1(w[148], w[1], pars->GC_10, pars->ZERO, pars->ZERO, w[168]); 
  VVV1P0_1(w[1], w[8], pars->GC_10, pars->ZERO, pars->ZERO, w[169]); 
  VVVV1P0_1(w[148], w[1], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[170]); 
  VVVV3P0_1(w[148], w[1], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[171]); 
  VVVV4P0_1(w[148], w[1], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[172]); 
  VVVV1P0_1(w[148], w[1], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[173]); 
  VVVV3P0_1(w[148], w[1], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[174]); 
  VVVV4P0_1(w[148], w[1], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[175]); 
  VVV1P0_1(w[0], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[176]); 
  FFV1_2(w[3], w[176], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[177]); 
  VVV1P0_1(w[176], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[178]); 
  VVV1P0_1(w[176], w[6], pars->GC_10, pars->ZERO, pars->ZERO, w[179]); 
  VVVV1P0_1(w[176], w[4], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[180]); 
  VVVV3P0_1(w[176], w[4], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[181]); 
  VVVV4P0_1(w[176], w[4], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[182]); 
  FFV1_1(w[123], w[176], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[183]); 
  FFV1_2(w[48], w[176], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[184]); 
  FFV1_2(w[42], w[176], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[185]); 
  VVV1P0_1(w[176], w[27], pars->GC_10, pars->ZERO, pars->ZERO, w[186]); 
  FFV1_1(w[2], w[176], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[187]); 
  FFV1_2(w[76], w[176], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[188]); 
  FFV1_1(w[34], w[176], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[189]); 
  FFV1_1(w[51], w[176], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[190]); 
  VVV1P0_1(w[176], w[87], pars->GC_10, pars->ZERO, pars->ZERO, w[191]); 
  VVV1P0_1(w[176], w[8], pars->GC_10, pars->ZERO, pars->ZERO, w[192]); 
  VVV1P0_1(w[176], w[101], pars->GC_10, pars->ZERO, pars->ZERO, w[193]); 
  VVV1P0_1(w[176], w[1], pars->GC_10, pars->ZERO, pars->ZERO, w[194]); 
  VVVV1P0_1(w[176], w[1], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[195]); 
  VVVV3P0_1(w[176], w[1], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[196]); 
  VVVV4P0_1(w[176], w[1], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[197]); 
  VVVV1P0_1(w[176], w[1], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[198]); 
  VVVV3P0_1(w[176], w[1], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[199]); 
  VVVV4P0_1(w[176], w[1], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[200]); 
  VVV1P0_1(w[0], w[6], pars->GC_10, pars->ZERO, pars->ZERO, w[201]); 
  FFV1_2(w[3], w[201], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[202]); 
  VVV1P0_1(w[201], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[203]); 
  VVV1P0_1(w[201], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[204]); 
  VVVV1P0_1(w[201], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[205]); 
  VVVV3P0_1(w[201], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[206]); 
  VVVV4P0_1(w[201], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[207]); 
  FFV1_1(w[123], w[201], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[208]); 
  FFV1_2(w[48], w[201], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[209]); 
  FFV1_2(w[39], w[201], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[210]); 
  VVV1P0_1(w[201], w[24], pars->GC_10, pars->ZERO, pars->ZERO, w[211]); 
  FFV1_1(w[2], w[201], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[212]); 
  FFV1_2(w[76], w[201], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[213]); 
  FFV1_1(w[34], w[201], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[214]); 
  FFV1_1(w[44], w[201], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[215]); 
  VVV1P0_1(w[201], w[87], pars->GC_10, pars->ZERO, pars->ZERO, w[216]); 
  VVV1P0_1(w[201], w[8], pars->GC_10, pars->ZERO, pars->ZERO, w[217]); 
  VVV1P0_1(w[201], w[95], pars->GC_10, pars->ZERO, pars->ZERO, w[218]); 
  VVV1P0_1(w[201], w[1], pars->GC_10, pars->ZERO, pars->ZERO, w[219]); 
  VVVV1P0_1(w[201], w[1], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[220]); 
  VVVV3P0_1(w[201], w[1], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[221]); 
  VVVV4P0_1(w[201], w[1], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[222]); 
  VVVV1P0_1(w[201], w[1], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[223]); 
  VVVV3P0_1(w[201], w[1], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[224]); 
  VVVV4P0_1(w[201], w[1], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[225]); 
  FFV1_1(w[123], w[0], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[226]); 
  FFV1_2(w[48], w[0], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[227]); 
  VVV1P0_1(w[0], w[29], pars->GC_10, pars->ZERO, pars->ZERO, w[228]); 
  FFV1_2(w[39], w[0], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[229]); 
  VVV1P0_1(w[0], w[27], pars->GC_10, pars->ZERO, pars->ZERO, w[230]); 
  FFV1_2(w[42], w[0], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[231]); 
  VVV1P0_1(w[0], w[24], pars->GC_10, pars->ZERO, pars->ZERO, w[232]); 
  VVVV1P0_1(w[0], w[24], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[233]); 
  VVVV3P0_1(w[0], w[24], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[234]); 
  VVVV4P0_1(w[0], w[24], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[235]); 
  VVVV1P0_1(w[0], w[27], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[236]); 
  VVVV3P0_1(w[0], w[27], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[237]); 
  VVVV4P0_1(w[0], w[27], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[238]); 
  VVVV1P0_1(w[0], w[4], w[29], pars->GC_12, pars->ZERO, pars->ZERO, w[239]); 
  VVVV3P0_1(w[0], w[4], w[29], pars->GC_12, pars->ZERO, pars->ZERO, w[240]); 
  VVVV4P0_1(w[0], w[4], w[29], pars->GC_12, pars->ZERO, pars->ZERO, w[241]); 
  VVV1P0_1(w[0], w[31], pars->GC_10, pars->ZERO, pars->ZERO, w[242]); 
  VVV1P0_1(w[0], w[32], pars->GC_10, pars->ZERO, pars->ZERO, w[243]); 
  VVV1P0_1(w[0], w[33], pars->GC_10, pars->ZERO, pars->ZERO, w[244]); 
  FFV1_2(w[76], w[0], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[245]); 
  FFV1_1(w[34], w[0], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[246]); 
  FFV1_1(w[44], w[0], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[247]); 
  FFV1_1(w[51], w[0], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[248]); 
  VVV1P0_1(w[0], w[87], pars->GC_10, pars->ZERO, pars->ZERO, w[249]); 
  VVV1P0_1(w[0], w[8], pars->GC_10, pars->ZERO, pars->ZERO, w[250]); 
  VVVV1P0_1(w[0], w[87], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[251]); 
  VVVV3P0_1(w[0], w[87], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[252]); 
  VVVV4P0_1(w[0], w[87], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[253]); 
  VVVV1P0_1(w[0], w[87], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[254]); 
  VVVV3P0_1(w[0], w[87], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[255]); 
  VVVV4P0_1(w[0], w[87], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[256]); 
  VVVV1P0_1(w[0], w[8], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[257]); 
  VVVV3P0_1(w[0], w[8], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[258]); 
  VVVV4P0_1(w[0], w[8], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[259]); 
  VVVV1P0_1(w[0], w[8], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[260]); 
  VVVV3P0_1(w[0], w[8], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[261]); 
  VVVV4P0_1(w[0], w[8], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[262]); 
  VVV1P0_1(w[0], w[95], pars->GC_10, pars->ZERO, pars->ZERO, w[263]); 
  VVVV1P0_1(w[0], w[95], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[264]); 
  VVVV3P0_1(w[0], w[95], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[265]); 
  VVVV4P0_1(w[0], w[95], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[266]); 
  VVVV1P0_1(w[0], w[95], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[267]); 
  VVVV3P0_1(w[0], w[95], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[268]); 
  VVVV4P0_1(w[0], w[95], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[269]); 
  VVVV1P0_1(w[0], w[8], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[270]); 
  VVVV3P0_1(w[0], w[8], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[271]); 
  VVVV4P0_1(w[0], w[8], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[272]); 
  VVV1P0_1(w[0], w[101], pars->GC_10, pars->ZERO, pars->ZERO, w[273]); 
  VVVV1P0_1(w[0], w[101], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[274]); 
  VVVV3P0_1(w[0], w[101], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[275]); 
  VVVV4P0_1(w[0], w[101], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[276]); 
  VVVV1P0_1(w[0], w[101], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[277]); 
  VVVV3P0_1(w[0], w[101], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[278]); 
  VVVV4P0_1(w[0], w[101], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[279]); 
  VVVV1P0_1(w[0], w[1], w[8], pars->GC_12, pars->ZERO, pars->ZERO, w[280]); 
  VVVV3P0_1(w[0], w[1], w[8], pars->GC_12, pars->ZERO, pars->ZERO, w[281]); 
  VVVV4P0_1(w[0], w[1], w[8], pars->GC_12, pars->ZERO, pars->ZERO, w[282]); 
  VVVV1P0_1(w[0], w[1], w[24], pars->GC_12, pars->ZERO, pars->ZERO, w[283]); 
  VVVV3P0_1(w[0], w[1], w[24], pars->GC_12, pars->ZERO, pars->ZERO, w[284]); 
  VVVV4P0_1(w[0], w[1], w[24], pars->GC_12, pars->ZERO, pars->ZERO, w[285]); 
  VVVV1P0_1(w[0], w[1], w[27], pars->GC_12, pars->ZERO, pars->ZERO, w[286]); 
  VVVV3P0_1(w[0], w[1], w[27], pars->GC_12, pars->ZERO, pars->ZERO, w[287]); 
  VVVV4P0_1(w[0], w[1], w[27], pars->GC_12, pars->ZERO, pars->ZERO, w[288]); 
  VVVV1P0_1(w[0], w[1], w[29], pars->GC_12, pars->ZERO, pars->ZERO, w[289]); 
  VVVV3P0_1(w[0], w[1], w[29], pars->GC_12, pars->ZERO, pars->ZERO, w[290]); 
  VVVV4P0_1(w[0], w[1], w[29], pars->GC_12, pars->ZERO, pars->ZERO, w[291]); 
  VVVV1P0_1(w[0], w[1], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[292]); 
  VVVV3P0_1(w[0], w[1], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[293]); 
  VVVV4P0_1(w[0], w[1], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[294]); 
  VVV1P0_1(w[292], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[295]); 
  VVV1P0_1(w[293], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[296]); 
  VVV1P0_1(w[294], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[297]); 
  VVV1P0_1(w[292], w[6], pars->GC_10, pars->ZERO, pars->ZERO, w[298]); 
  VVV1P0_1(w[293], w[6], pars->GC_10, pars->ZERO, pars->ZERO, w[299]); 
  VVV1P0_1(w[294], w[6], pars->GC_10, pars->ZERO, pars->ZERO, w[300]); 
  FFV1_2(w[3], w[292], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[301]); 
  FFV1_2(w[3], w[293], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[302]); 
  FFV1_2(w[3], w[294], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[303]); 
  FFV1_1(w[2], w[292], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[304]); 
  FFV1_1(w[2], w[293], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[305]); 
  FFV1_1(w[2], w[294], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[306]); 
  VVVV1P0_1(w[0], w[1], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[307]); 
  VVVV3P0_1(w[0], w[1], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[308]); 
  VVVV4P0_1(w[0], w[1], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[309]); 
  VVV1P0_1(w[307], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[310]); 
  VVV1P0_1(w[308], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[311]); 
  VVV1P0_1(w[309], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[312]); 
  VVV1P0_1(w[307], w[6], pars->GC_10, pars->ZERO, pars->ZERO, w[313]); 
  VVV1P0_1(w[308], w[6], pars->GC_10, pars->ZERO, pars->ZERO, w[314]); 
  VVV1P0_1(w[309], w[6], pars->GC_10, pars->ZERO, pars->ZERO, w[315]); 
  FFV1_2(w[3], w[307], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[316]); 
  FFV1_2(w[3], w[308], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[317]); 
  FFV1_2(w[3], w[309], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[318]); 
  FFV1_1(w[2], w[307], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[319]); 
  FFV1_1(w[2], w[308], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[320]); 
  FFV1_1(w[2], w[309], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[321]); 
  VVVV1P0_1(w[0], w[1], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[322]); 
  VVVV3P0_1(w[0], w[1], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[323]); 
  VVVV4P0_1(w[0], w[1], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[324]); 
  VVV1P0_1(w[322], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[325]); 
  VVV1P0_1(w[323], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[326]); 
  VVV1P0_1(w[324], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[327]); 
  VVV1P0_1(w[322], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[328]); 
  VVV1P0_1(w[323], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[329]); 
  VVV1P0_1(w[324], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[330]); 
  FFV1_2(w[3], w[322], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[331]); 
  FFV1_2(w[3], w[323], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[332]); 
  FFV1_2(w[3], w[324], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[333]); 
  FFV1_1(w[2], w[322], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[334]); 
  FFV1_1(w[2], w[323], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[335]); 
  FFV1_1(w[2], w[324], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[336]); 
  VVVV1P0_1(w[0], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[337]); 
  VVVV3P0_1(w[0], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[338]); 
  VVVV4P0_1(w[0], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[339]); 
  FFV1_2(w[3], w[337], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[340]); 
  FFV1_2(w[3], w[338], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[341]); 
  FFV1_2(w[3], w[339], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[342]); 
  VVV1P0_1(w[337], w[6], pars->GC_10, pars->ZERO, pars->ZERO, w[343]); 
  VVV1P0_1(w[338], w[6], pars->GC_10, pars->ZERO, pars->ZERO, w[344]); 
  VVV1P0_1(w[339], w[6], pars->GC_10, pars->ZERO, pars->ZERO, w[345]); 
  FFV1_1(w[2], w[337], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[346]); 
  FFV1_1(w[2], w[338], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[347]); 
  FFV1_1(w[2], w[339], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[348]); 
  VVV1P0_1(w[337], w[1], pars->GC_10, pars->ZERO, pars->ZERO, w[349]); 
  VVV1P0_1(w[338], w[1], pars->GC_10, pars->ZERO, pars->ZERO, w[350]); 
  VVV1P0_1(w[339], w[1], pars->GC_10, pars->ZERO, pars->ZERO, w[351]); 
  VVVV1P0_1(w[0], w[4], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[352]); 
  VVVV3P0_1(w[0], w[4], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[353]); 
  VVVV4P0_1(w[0], w[4], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[354]); 
  FFV1_2(w[3], w[352], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[355]); 
  FFV1_2(w[3], w[353], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[356]); 
  FFV1_2(w[3], w[354], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[357]); 
  VVV1P0_1(w[352], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[358]); 
  VVV1P0_1(w[353], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[359]); 
  VVV1P0_1(w[354], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[360]); 
  FFV1_1(w[2], w[352], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[361]); 
  FFV1_1(w[2], w[353], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[362]); 
  FFV1_1(w[2], w[354], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[363]); 
  VVV1P0_1(w[352], w[1], pars->GC_10, pars->ZERO, pars->ZERO, w[364]); 
  VVV1P0_1(w[353], w[1], pars->GC_10, pars->ZERO, pars->ZERO, w[365]); 
  VVV1P0_1(w[354], w[1], pars->GC_10, pars->ZERO, pars->ZERO, w[366]); 
  VVVV1P0_1(w[0], w[5], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[367]); 
  VVVV3P0_1(w[0], w[5], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[368]); 
  VVVV4P0_1(w[0], w[5], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[369]); 
  FFV1_2(w[3], w[367], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[370]); 
  FFV1_2(w[3], w[368], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[371]); 
  FFV1_2(w[3], w[369], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[372]); 
  VVV1P0_1(w[367], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[373]); 
  VVV1P0_1(w[368], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[374]); 
  VVV1P0_1(w[369], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[375]); 
  FFV1_1(w[2], w[367], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[376]); 
  FFV1_1(w[2], w[368], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[377]); 
  FFV1_1(w[2], w[369], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[378]); 
  VVV1P0_1(w[367], w[1], pars->GC_10, pars->ZERO, pars->ZERO, w[379]); 
  VVV1P0_1(w[368], w[1], pars->GC_10, pars->ZERO, pars->ZERO, w[380]); 
  VVV1P0_1(w[369], w[1], pars->GC_10, pars->ZERO, pars->ZERO, w[381]); 
  VVV1P0_1(w[0], w[113], pars->GC_10, pars->ZERO, pars->ZERO, w[382]); 
  VVV1P0_1(w[0], w[114], pars->GC_10, pars->ZERO, pars->ZERO, w[383]); 
  VVV1P0_1(w[0], w[115], pars->GC_10, pars->ZERO, pars->ZERO, w[384]); 
  VVV1P0_1(w[0], w[116], pars->GC_10, pars->ZERO, pars->ZERO, w[385]); 
  VVV1P0_1(w[0], w[117], pars->GC_10, pars->ZERO, pars->ZERO, w[386]); 
  VVV1P0_1(w[0], w[118], pars->GC_10, pars->ZERO, pars->ZERO, w[387]); 
  VVV1P0_1(w[0], w[119], pars->GC_10, pars->ZERO, pars->ZERO, w[388]); 
  VVV1P0_1(w[0], w[120], pars->GC_10, pars->ZERO, pars->ZERO, w[389]); 
  VVV1P0_1(w[0], w[121], pars->GC_10, pars->ZERO, pars->ZERO, w[390]); 

  // Calculate all amplitudes
  // Amplitude(s) for diagram number 0
  VVV1_0(w[9], w[10], w[6], pars->GC_10, amp[0]); 
  VVV1_0(w[9], w[11], w[5], pars->GC_10, amp[1]); 
  VVVV1_0(w[8], w[5], w[6], w[9], pars->GC_12, amp[2]); 
  VVVV3_0(w[8], w[5], w[6], w[9], pars->GC_12, amp[3]); 
  VVVV4_0(w[8], w[5], w[6], w[9], pars->GC_12, amp[4]); 
  VVV1_0(w[12], w[13], w[6], pars->GC_10, amp[5]); 
  VVV1_0(w[12], w[11], w[4], pars->GC_10, amp[6]); 
  VVVV1_0(w[8], w[4], w[6], w[12], pars->GC_12, amp[7]); 
  VVVV3_0(w[8], w[4], w[6], w[12], pars->GC_12, amp[8]); 
  VVVV4_0(w[8], w[4], w[6], w[12], pars->GC_12, amp[9]); 
  VVV1_0(w[14], w[13], w[5], pars->GC_10, amp[10]); 
  VVV1_0(w[14], w[10], w[4], pars->GC_10, amp[11]); 
  VVVV1_0(w[8], w[4], w[5], w[14], pars->GC_12, amp[12]); 
  VVVV3_0(w[8], w[4], w[5], w[14], pars->GC_12, amp[13]); 
  VVVV4_0(w[8], w[4], w[5], w[14], pars->GC_12, amp[14]); 
  VVV1_0(w[8], w[6], w[15], pars->GC_10, amp[15]); 
  VVV1_0(w[8], w[6], w[16], pars->GC_10, amp[16]); 
  VVV1_0(w[8], w[6], w[17], pars->GC_10, amp[17]); 
  VVV1_0(w[8], w[5], w[18], pars->GC_10, amp[18]); 
  VVV1_0(w[8], w[5], w[19], pars->GC_10, amp[19]); 
  VVV1_0(w[8], w[5], w[20], pars->GC_10, amp[20]); 
  VVV1_0(w[8], w[4], w[21], pars->GC_10, amp[21]); 
  VVV1_0(w[8], w[4], w[22], pars->GC_10, amp[22]); 
  VVV1_0(w[8], w[4], w[23], pars->GC_10, amp[23]); 
  VVVV1_0(w[7], w[8], w[24], w[6], pars->GC_12, amp[24]); 
  VVVV3_0(w[7], w[8], w[24], w[6], pars->GC_12, amp[25]); 
  VVVV4_0(w[7], w[8], w[24], w[6], pars->GC_12, amp[26]); 
  VVV1_0(w[24], w[6], w[25], pars->GC_10, amp[27]); 
  VVV1_0(w[8], w[6], w[26], pars->GC_10, amp[28]); 
  VVV1_0(w[8], w[24], w[14], pars->GC_10, amp[29]); 
  VVVV1_0(w[7], w[8], w[27], w[5], pars->GC_12, amp[30]); 
  VVVV3_0(w[7], w[8], w[27], w[5], pars->GC_12, amp[31]); 
  VVVV4_0(w[7], w[8], w[27], w[5], pars->GC_12, amp[32]); 
  VVV1_0(w[27], w[5], w[25], pars->GC_10, amp[33]); 
  VVV1_0(w[8], w[5], w[28], pars->GC_10, amp[34]); 
  VVV1_0(w[8], w[27], w[12], pars->GC_10, amp[35]); 
  VVVV1_0(w[7], w[8], w[4], w[29], pars->GC_12, amp[36]); 
  VVVV3_0(w[7], w[8], w[4], w[29], pars->GC_12, amp[37]); 
  VVVV4_0(w[7], w[8], w[4], w[29], pars->GC_12, amp[38]); 
  VVV1_0(w[4], w[29], w[25], pars->GC_10, amp[39]); 
  VVV1_0(w[8], w[29], w[9], pars->GC_10, amp[40]); 
  VVV1_0(w[8], w[4], w[30], pars->GC_10, amp[41]); 
  VVV1_0(w[7], w[8], w[31], pars->GC_10, amp[42]); 
  VVV1_0(w[7], w[8], w[32], pars->GC_10, amp[43]); 
  VVV1_0(w[7], w[8], w[33], pars->GC_10, amp[44]); 
  FFV1_0(w[35], w[36], w[6], pars->GC_11, amp[45]); 
  FFV1_0(w[35], w[37], w[5], pars->GC_11, amp[46]); 
  VVV1_0(w[12], w[38], w[6], pars->GC_10, amp[47]); 
  FFV1_0(w[3], w[37], w[12], pars->GC_11, amp[48]); 
  VVV1_0(w[14], w[38], w[5], pars->GC_10, amp[49]); 
  FFV1_0(w[3], w[36], w[14], pars->GC_11, amp[50]); 
  FFV1_0(w[3], w[34], w[21], pars->GC_11, amp[51]); 
  FFV1_0(w[3], w[34], w[22], pars->GC_11, amp[52]); 
  FFV1_0(w[3], w[34], w[23], pars->GC_11, amp[53]); 
  FFV1_0(w[39], w[40], w[6], pars->GC_11, amp[54]); 
  FFV1_0(w[41], w[34], w[6], pars->GC_11, amp[55]); 
  FFV1_0(w[39], w[34], w[14], pars->GC_11, amp[56]); 
  FFV1_0(w[42], w[40], w[5], pars->GC_11, amp[57]); 
  FFV1_0(w[43], w[34], w[5], pars->GC_11, amp[58]); 
  FFV1_0(w[42], w[34], w[12], pars->GC_11, amp[59]); 
  FFV1_0(w[3], w[40], w[29], pars->GC_11, amp[60]); 
  FFV1_0(w[35], w[34], w[29], pars->GC_11, amp[61]); 
  FFV1_0(w[3], w[34], w[30], pars->GC_11, amp[62]); 
  FFV1_0(w[35], w[45], w[6], pars->GC_11, amp[63]); 
  FFV1_0(w[35], w[46], w[4], pars->GC_11, amp[64]); 
  VVV1_0(w[9], w[47], w[6], pars->GC_10, amp[65]); 
  FFV1_0(w[3], w[46], w[9], pars->GC_11, amp[66]); 
  VVV1_0(w[14], w[47], w[4], pars->GC_10, amp[67]); 
  FFV1_0(w[3], w[45], w[14], pars->GC_11, amp[68]); 
  FFV1_0(w[3], w[44], w[18], pars->GC_11, amp[69]); 
  FFV1_0(w[3], w[44], w[19], pars->GC_11, amp[70]); 
  FFV1_0(w[3], w[44], w[20], pars->GC_11, amp[71]); 
  FFV1_0(w[48], w[49], w[6], pars->GC_11, amp[72]); 
  FFV1_0(w[50], w[44], w[6], pars->GC_11, amp[73]); 
  FFV1_0(w[48], w[44], w[14], pars->GC_11, amp[74]); 
  FFV1_0(w[42], w[49], w[4], pars->GC_11, amp[75]); 
  FFV1_0(w[43], w[44], w[4], pars->GC_11, amp[76]); 
  FFV1_0(w[42], w[44], w[9], pars->GC_11, amp[77]); 
  FFV1_0(w[3], w[49], w[27], pars->GC_11, amp[78]); 
  FFV1_0(w[35], w[44], w[27], pars->GC_11, amp[79]); 
  FFV1_0(w[3], w[44], w[28], pars->GC_11, amp[80]); 
  FFV1_0(w[35], w[52], w[5], pars->GC_11, amp[81]); 
  FFV1_0(w[35], w[53], w[4], pars->GC_11, amp[82]); 
  VVV1_0(w[9], w[54], w[5], pars->GC_10, amp[83]); 
  FFV1_0(w[3], w[53], w[9], pars->GC_11, amp[84]); 
  VVV1_0(w[12], w[54], w[4], pars->GC_10, amp[85]); 
  FFV1_0(w[3], w[52], w[12], pars->GC_11, amp[86]); 
  FFV1_0(w[3], w[51], w[15], pars->GC_11, amp[87]); 
  FFV1_0(w[3], w[51], w[16], pars->GC_11, amp[88]); 
  FFV1_0(w[3], w[51], w[17], pars->GC_11, amp[89]); 
  FFV1_0(w[48], w[55], w[5], pars->GC_11, amp[90]); 
  FFV1_0(w[50], w[51], w[5], pars->GC_11, amp[91]); 
  FFV1_0(w[48], w[51], w[12], pars->GC_11, amp[92]); 
  FFV1_0(w[39], w[55], w[4], pars->GC_11, amp[93]); 
  FFV1_0(w[41], w[51], w[4], pars->GC_11, amp[94]); 
  FFV1_0(w[39], w[51], w[9], pars->GC_11, amp[95]); 
  FFV1_0(w[3], w[55], w[24], pars->GC_11, amp[96]); 
  FFV1_0(w[35], w[51], w[24], pars->GC_11, amp[97]); 
  FFV1_0(w[3], w[51], w[26], pars->GC_11, amp[98]); 
  FFV1_0(w[57], w[56], w[6], pars->GC_11, amp[99]); 
  FFV1_0(w[58], w[56], w[5], pars->GC_11, amp[100]); 
  VVV1_0(w[12], w[59], w[6], pars->GC_10, amp[101]); 
  FFV1_0(w[58], w[2], w[12], pars->GC_11, amp[102]); 
  VVV1_0(w[14], w[59], w[5], pars->GC_10, amp[103]); 
  FFV1_0(w[57], w[2], w[14], pars->GC_11, amp[104]); 
  FFV1_0(w[48], w[2], w[21], pars->GC_11, amp[105]); 
  FFV1_0(w[48], w[2], w[22], pars->GC_11, amp[106]); 
  FFV1_0(w[48], w[2], w[23], pars->GC_11, amp[107]); 
  FFV1_0(w[48], w[56], w[29], pars->GC_11, amp[108]); 
  FFV1_0(w[50], w[2], w[29], pars->GC_11, amp[109]); 
  FFV1_0(w[48], w[2], w[30], pars->GC_11, amp[110]); 
  FFV1_0(w[60], w[56], w[6], pars->GC_11, amp[111]); 
  FFV1_0(w[61], w[56], w[4], pars->GC_11, amp[112]); 
  VVV1_0(w[9], w[62], w[6], pars->GC_10, amp[113]); 
  FFV1_0(w[61], w[2], w[9], pars->GC_11, amp[114]); 
  VVV1_0(w[14], w[62], w[4], pars->GC_10, amp[115]); 
  FFV1_0(w[60], w[2], w[14], pars->GC_11, amp[116]); 
  FFV1_0(w[39], w[2], w[18], pars->GC_11, amp[117]); 
  FFV1_0(w[39], w[2], w[19], pars->GC_11, amp[118]); 
  FFV1_0(w[39], w[2], w[20], pars->GC_11, amp[119]); 
  FFV1_0(w[39], w[56], w[27], pars->GC_11, amp[120]); 
  FFV1_0(w[41], w[2], w[27], pars->GC_11, amp[121]); 
  FFV1_0(w[39], w[2], w[28], pars->GC_11, amp[122]); 
  FFV1_0(w[63], w[56], w[5], pars->GC_11, amp[123]); 
  FFV1_0(w[64], w[56], w[4], pars->GC_11, amp[124]); 
  VVV1_0(w[9], w[65], w[5], pars->GC_10, amp[125]); 
  FFV1_0(w[64], w[2], w[9], pars->GC_11, amp[126]); 
  VVV1_0(w[12], w[65], w[4], pars->GC_10, amp[127]); 
  FFV1_0(w[63], w[2], w[12], pars->GC_11, amp[128]); 
  FFV1_0(w[42], w[2], w[15], pars->GC_11, amp[129]); 
  FFV1_0(w[42], w[2], w[16], pars->GC_11, amp[130]); 
  FFV1_0(w[42], w[2], w[17], pars->GC_11, amp[131]); 
  FFV1_0(w[42], w[56], w[24], pars->GC_11, amp[132]); 
  FFV1_0(w[43], w[2], w[24], pars->GC_11, amp[133]); 
  FFV1_0(w[42], w[2], w[26], pars->GC_11, amp[134]); 
  FFV1_0(w[66], w[56], w[6], pars->GC_11, amp[135]); 
  FFV1_0(w[3], w[56], w[67], pars->GC_11, amp[136]); 
  FFV1_0(w[35], w[68], w[6], pars->GC_11, amp[137]); 
  FFV1_0(w[35], w[2], w[67], pars->GC_11, amp[138]); 
  FFV1_0(w[3], w[68], w[14], pars->GC_11, amp[139]); 
  FFV1_0(w[66], w[2], w[14], pars->GC_11, amp[140]); 
  FFV1_0(w[69], w[56], w[5], pars->GC_11, amp[141]); 
  FFV1_0(w[3], w[56], w[70], pars->GC_11, amp[142]); 
  FFV1_0(w[35], w[71], w[5], pars->GC_11, amp[143]); 
  FFV1_0(w[35], w[2], w[70], pars->GC_11, amp[144]); 
  FFV1_0(w[3], w[71], w[12], pars->GC_11, amp[145]); 
  FFV1_0(w[69], w[2], w[12], pars->GC_11, amp[146]); 
  FFV1_0(w[72], w[56], w[4], pars->GC_11, amp[147]); 
  FFV1_0(w[3], w[56], w[73], pars->GC_11, amp[148]); 
  FFV1_0(w[35], w[74], w[4], pars->GC_11, amp[149]); 
  FFV1_0(w[35], w[2], w[73], pars->GC_11, amp[150]); 
  FFV1_0(w[3], w[74], w[9], pars->GC_11, amp[151]); 
  FFV1_0(w[72], w[2], w[9], pars->GC_11, amp[152]); 
  FFV1_0(w[3], w[56], w[31], pars->GC_11, amp[153]); 
  FFV1_0(w[3], w[56], w[32], pars->GC_11, amp[154]); 
  FFV1_0(w[3], w[56], w[33], pars->GC_11, amp[155]); 
  FFV1_0(w[35], w[2], w[31], pars->GC_11, amp[156]); 
  FFV1_0(w[35], w[2], w[32], pars->GC_11, amp[157]); 
  FFV1_0(w[35], w[2], w[33], pars->GC_11, amp[158]); 
  FFV1_0(w[78], w[77], w[6], pars->GC_11, amp[159]); 
  FFV1_0(w[79], w[77], w[5], pars->GC_11, amp[160]); 
  FFV1_0(w[81], w[80], w[6], pars->GC_11, amp[161]); 
  FFV1_0(w[79], w[80], w[4], pars->GC_11, amp[162]); 
  FFV1_0(w[81], w[82], w[5], pars->GC_11, amp[163]); 
  FFV1_0(w[78], w[82], w[4], pars->GC_11, amp[164]); 
  VVV1_0(w[24], w[6], w[83], pars->GC_10, amp[165]); 
  FFV1_0(w[76], w[84], w[6], pars->GC_11, amp[166]); 
  FFV1_0(w[76], w[82], w[24], pars->GC_11, amp[167]); 
  VVV1_0(w[27], w[5], w[83], pars->GC_10, amp[168]); 
  FFV1_0(w[76], w[85], w[5], pars->GC_11, amp[169]); 
  FFV1_0(w[76], w[80], w[27], pars->GC_11, amp[170]); 
  VVV1_0(w[4], w[29], w[83], pars->GC_10, amp[171]); 
  FFV1_0(w[76], w[77], w[29], pars->GC_11, amp[172]); 
  FFV1_0(w[76], w[86], w[4], pars->GC_11, amp[173]); 
  FFV1_0(w[76], w[75], w[31], pars->GC_11, amp[174]); 
  FFV1_0(w[76], w[75], w[32], pars->GC_11, amp[175]); 
  FFV1_0(w[76], w[75], w[33], pars->GC_11, amp[176]); 
  VVV1_0(w[88], w[89], w[6], pars->GC_10, amp[177]); 
  VVV1_0(w[88], w[90], w[5], pars->GC_10, amp[178]); 
  VVVV1_0(w[87], w[5], w[6], w[88], pars->GC_12, amp[179]); 
  VVVV3_0(w[87], w[5], w[6], w[88], pars->GC_12, amp[180]); 
  VVVV4_0(w[87], w[5], w[6], w[88], pars->GC_12, amp[181]); 
  FFV1_0(w[91], w[80], w[6], pars->GC_11, amp[182]); 
  FFV1_0(w[3], w[80], w[90], pars->GC_11, amp[183]); 
  FFV1_0(w[91], w[82], w[5], pars->GC_11, amp[184]); 
  FFV1_0(w[3], w[82], w[89], pars->GC_11, amp[185]); 
  FFV1_0(w[39], w[92], w[6], pars->GC_11, amp[186]); 
  VVV1_0(w[87], w[6], w[93], pars->GC_10, amp[187]); 
  FFV1_0(w[39], w[82], w[87], pars->GC_11, amp[188]); 
  FFV1_0(w[42], w[92], w[5], pars->GC_11, amp[189]); 
  VVV1_0(w[87], w[5], w[94], pars->GC_10, amp[190]); 
  FFV1_0(w[42], w[80], w[87], pars->GC_11, amp[191]); 
  FFV1_0(w[3], w[92], w[29], pars->GC_11, amp[192]); 
  VVV1_0(w[87], w[29], w[88], pars->GC_10, amp[193]); 
  FFV1_0(w[3], w[86], w[87], pars->GC_11, amp[194]); 
  VVV1_0(w[88], w[96], w[6], pars->GC_10, amp[195]); 
  VVV1_0(w[88], w[97], w[4], pars->GC_10, amp[196]); 
  VVVV1_0(w[95], w[4], w[6], w[88], pars->GC_12, amp[197]); 
  VVVV3_0(w[95], w[4], w[6], w[88], pars->GC_12, amp[198]); 
  VVVV4_0(w[95], w[4], w[6], w[88], pars->GC_12, amp[199]); 
  FFV1_0(w[98], w[77], w[6], pars->GC_11, amp[200]); 
  FFV1_0(w[3], w[77], w[97], pars->GC_11, amp[201]); 
  FFV1_0(w[98], w[82], w[4], pars->GC_11, amp[202]); 
  FFV1_0(w[3], w[82], w[96], pars->GC_11, amp[203]); 
  FFV1_0(w[48], w[99], w[6], pars->GC_11, amp[204]); 
  VVV1_0(w[95], w[6], w[100], pars->GC_10, amp[205]); 
  FFV1_0(w[48], w[82], w[95], pars->GC_11, amp[206]); 
  FFV1_0(w[42], w[99], w[4], pars->GC_11, amp[207]); 
  VVV1_0(w[95], w[4], w[94], pars->GC_10, amp[208]); 
  FFV1_0(w[42], w[77], w[95], pars->GC_11, amp[209]); 
  FFV1_0(w[3], w[99], w[27], pars->GC_11, amp[210]); 
  VVV1_0(w[95], w[27], w[88], pars->GC_10, amp[211]); 
  FFV1_0(w[3], w[85], w[95], pars->GC_11, amp[212]); 
  VVV1_0(w[88], w[102], w[5], pars->GC_10, amp[213]); 
  VVV1_0(w[88], w[103], w[4], pars->GC_10, amp[214]); 
  VVVV1_0(w[101], w[4], w[5], w[88], pars->GC_12, amp[215]); 
  VVVV3_0(w[101], w[4], w[5], w[88], pars->GC_12, amp[216]); 
  VVVV4_0(w[101], w[4], w[5], w[88], pars->GC_12, amp[217]); 
  FFV1_0(w[104], w[77], w[5], pars->GC_11, amp[218]); 
  FFV1_0(w[3], w[77], w[103], pars->GC_11, amp[219]); 
  FFV1_0(w[104], w[80], w[4], pars->GC_11, amp[220]); 
  FFV1_0(w[3], w[80], w[102], pars->GC_11, amp[221]); 
  FFV1_0(w[48], w[105], w[5], pars->GC_11, amp[222]); 
  VVV1_0(w[101], w[5], w[100], pars->GC_10, amp[223]); 
  FFV1_0(w[48], w[80], w[101], pars->GC_11, amp[224]); 
  FFV1_0(w[39], w[105], w[4], pars->GC_11, amp[225]); 
  VVV1_0(w[101], w[4], w[93], pars->GC_10, amp[226]); 
  FFV1_0(w[39], w[77], w[101], pars->GC_11, amp[227]); 
  FFV1_0(w[3], w[105], w[24], pars->GC_11, amp[228]); 
  VVV1_0(w[101], w[24], w[88], pars->GC_10, amp[229]); 
  FFV1_0(w[3], w[84], w[101], pars->GC_11, amp[230]); 
  FFV1_0(w[57], w[106], w[6], pars->GC_11, amp[231]); 
  FFV1_0(w[58], w[106], w[5], pars->GC_11, amp[232]); 
  FFV1_0(w[107], w[80], w[6], pars->GC_11, amp[233]); 
  FFV1_0(w[58], w[80], w[1], pars->GC_11, amp[234]); 
  FFV1_0(w[107], w[82], w[5], pars->GC_11, amp[235]); 
  FFV1_0(w[57], w[82], w[1], pars->GC_11, amp[236]); 
  FFV1_0(w[48], w[106], w[29], pars->GC_11, amp[237]); 
  VVV1_0(w[1], w[29], w[100], pars->GC_10, amp[238]); 
  FFV1_0(w[48], w[86], w[1], pars->GC_11, amp[239]); 
  FFV1_0(w[60], w[106], w[6], pars->GC_11, amp[240]); 
  FFV1_0(w[61], w[106], w[4], pars->GC_11, amp[241]); 
  FFV1_0(w[108], w[77], w[6], pars->GC_11, amp[242]); 
  FFV1_0(w[61], w[77], w[1], pars->GC_11, amp[243]); 
  FFV1_0(w[108], w[82], w[4], pars->GC_11, amp[244]); 
  FFV1_0(w[60], w[82], w[1], pars->GC_11, amp[245]); 
  FFV1_0(w[39], w[106], w[27], pars->GC_11, amp[246]); 
  VVV1_0(w[1], w[27], w[93], pars->GC_10, amp[247]); 
  FFV1_0(w[39], w[85], w[1], pars->GC_11, amp[248]); 
  FFV1_0(w[63], w[106], w[5], pars->GC_11, amp[249]); 
  FFV1_0(w[64], w[106], w[4], pars->GC_11, amp[250]); 
  FFV1_0(w[109], w[77], w[5], pars->GC_11, amp[251]); 
  FFV1_0(w[64], w[77], w[1], pars->GC_11, amp[252]); 
  FFV1_0(w[109], w[80], w[4], pars->GC_11, amp[253]); 
  FFV1_0(w[63], w[80], w[1], pars->GC_11, amp[254]); 
  FFV1_0(w[42], w[106], w[24], pars->GC_11, amp[255]); 
  VVV1_0(w[1], w[24], w[94], pars->GC_10, amp[256]); 
  FFV1_0(w[42], w[84], w[1], pars->GC_11, amp[257]); 
  FFV1_0(w[66], w[106], w[6], pars->GC_11, amp[258]); 
  FFV1_0(w[3], w[106], w[67], pars->GC_11, amp[259]); 
  VVV1_0(w[88], w[110], w[6], pars->GC_10, amp[260]); 
  VVV1_0(w[88], w[1], w[67], pars->GC_10, amp[261]); 
  VVVV1_0(w[1], w[24], w[6], w[88], pars->GC_12, amp[262]); 
  VVVV3_0(w[1], w[24], w[6], w[88], pars->GC_12, amp[263]); 
  VVVV4_0(w[1], w[24], w[6], w[88], pars->GC_12, amp[264]); 
  FFV1_0(w[3], w[82], w[110], pars->GC_11, amp[265]); 
  FFV1_0(w[66], w[82], w[1], pars->GC_11, amp[266]); 
  FFV1_0(w[69], w[106], w[5], pars->GC_11, amp[267]); 
  FFV1_0(w[3], w[106], w[70], pars->GC_11, amp[268]); 
  VVV1_0(w[88], w[111], w[5], pars->GC_10, amp[269]); 
  VVV1_0(w[88], w[1], w[70], pars->GC_10, amp[270]); 
  VVVV1_0(w[1], w[27], w[5], w[88], pars->GC_12, amp[271]); 
  VVVV3_0(w[1], w[27], w[5], w[88], pars->GC_12, amp[272]); 
  VVVV4_0(w[1], w[27], w[5], w[88], pars->GC_12, amp[273]); 
  FFV1_0(w[3], w[80], w[111], pars->GC_11, amp[274]); 
  FFV1_0(w[69], w[80], w[1], pars->GC_11, amp[275]); 
  FFV1_0(w[72], w[106], w[4], pars->GC_11, amp[276]); 
  FFV1_0(w[3], w[106], w[73], pars->GC_11, amp[277]); 
  VVV1_0(w[88], w[112], w[4], pars->GC_10, amp[278]); 
  VVV1_0(w[88], w[1], w[73], pars->GC_10, amp[279]); 
  VVVV1_0(w[1], w[4], w[29], w[88], pars->GC_12, amp[280]); 
  VVVV3_0(w[1], w[4], w[29], w[88], pars->GC_12, amp[281]); 
  VVVV4_0(w[1], w[4], w[29], w[88], pars->GC_12, amp[282]); 
  FFV1_0(w[3], w[77], w[112], pars->GC_11, amp[283]); 
  FFV1_0(w[72], w[77], w[1], pars->GC_11, amp[284]); 
  VVV1_0(w[113], w[6], w[88], pars->GC_10, amp[285]); 
  VVV1_0(w[114], w[6], w[88], pars->GC_10, amp[286]); 
  VVV1_0(w[115], w[6], w[88], pars->GC_10, amp[287]); 
  FFV1_0(w[3], w[82], w[113], pars->GC_11, amp[288]); 
  FFV1_0(w[3], w[82], w[114], pars->GC_11, amp[289]); 
  FFV1_0(w[3], w[82], w[115], pars->GC_11, amp[290]); 
  FFV1_0(w[42], w[75], w[113], pars->GC_11, amp[291]); 
  FFV1_0(w[42], w[75], w[114], pars->GC_11, amp[292]); 
  FFV1_0(w[42], w[75], w[115], pars->GC_11, amp[293]); 
  VVV1_0(w[116], w[5], w[88], pars->GC_10, amp[294]); 
  VVV1_0(w[117], w[5], w[88], pars->GC_10, amp[295]); 
  VVV1_0(w[118], w[5], w[88], pars->GC_10, amp[296]); 
  FFV1_0(w[3], w[80], w[116], pars->GC_11, amp[297]); 
  FFV1_0(w[3], w[80], w[117], pars->GC_11, amp[298]); 
  FFV1_0(w[3], w[80], w[118], pars->GC_11, amp[299]); 
  FFV1_0(w[39], w[75], w[116], pars->GC_11, amp[300]); 
  FFV1_0(w[39], w[75], w[117], pars->GC_11, amp[301]); 
  FFV1_0(w[39], w[75], w[118], pars->GC_11, amp[302]); 
  VVV1_0(w[119], w[4], w[88], pars->GC_10, amp[303]); 
  VVV1_0(w[120], w[4], w[88], pars->GC_10, amp[304]); 
  VVV1_0(w[121], w[4], w[88], pars->GC_10, amp[305]); 
  FFV1_0(w[3], w[77], w[119], pars->GC_11, amp[306]); 
  FFV1_0(w[3], w[77], w[120], pars->GC_11, amp[307]); 
  FFV1_0(w[3], w[77], w[121], pars->GC_11, amp[308]); 
  FFV1_0(w[48], w[75], w[119], pars->GC_11, amp[309]); 
  FFV1_0(w[48], w[75], w[120], pars->GC_11, amp[310]); 
  FFV1_0(w[48], w[75], w[121], pars->GC_11, amp[311]); 
  FFV1_0(w[3], w[106], w[31], pars->GC_11, amp[312]); 
  FFV1_0(w[3], w[106], w[32], pars->GC_11, amp[313]); 
  FFV1_0(w[3], w[106], w[33], pars->GC_11, amp[314]); 
  VVV1_0(w[1], w[31], w[88], pars->GC_10, amp[315]); 
  VVV1_0(w[1], w[32], w[88], pars->GC_10, amp[316]); 
  VVV1_0(w[1], w[33], w[88], pars->GC_10, amp[317]); 
  FFV1_0(w[124], w[125], w[6], pars->GC_11, amp[318]); 
  FFV1_0(w[124], w[126], w[5], pars->GC_11, amp[319]); 
  FFV1_0(w[127], w[128], w[6], pars->GC_11, amp[320]); 
  FFV1_0(w[127], w[126], w[4], pars->GC_11, amp[321]); 
  FFV1_0(w[129], w[128], w[5], pars->GC_11, amp[322]); 
  FFV1_0(w[129], w[125], w[4], pars->GC_11, amp[323]); 
  VVV1_0(w[24], w[6], w[130], pars->GC_10, amp[324]); 
  FFV1_0(w[131], w[123], w[6], pars->GC_11, amp[325]); 
  FFV1_0(w[129], w[123], w[24], pars->GC_11, amp[326]); 
  VVV1_0(w[27], w[5], w[130], pars->GC_10, amp[327]); 
  FFV1_0(w[132], w[123], w[5], pars->GC_11, amp[328]); 
  FFV1_0(w[127], w[123], w[27], pars->GC_11, amp[329]); 
  VVV1_0(w[4], w[29], w[130], pars->GC_10, amp[330]); 
  FFV1_0(w[124], w[123], w[29], pars->GC_11, amp[331]); 
  FFV1_0(w[133], w[123], w[4], pars->GC_11, amp[332]); 
  FFV1_0(w[122], w[123], w[31], pars->GC_11, amp[333]); 
  FFV1_0(w[122], w[123], w[32], pars->GC_11, amp[334]); 
  FFV1_0(w[122], w[123], w[33], pars->GC_11, amp[335]); 
  VVV1_0(w[134], w[89], w[6], pars->GC_10, amp[336]); 
  VVV1_0(w[134], w[90], w[5], pars->GC_10, amp[337]); 
  VVVV1_0(w[87], w[5], w[6], w[134], pars->GC_12, amp[338]); 
  VVVV3_0(w[87], w[5], w[6], w[134], pars->GC_12, amp[339]); 
  VVVV4_0(w[87], w[5], w[6], w[134], pars->GC_12, amp[340]); 
  FFV1_0(w[127], w[135], w[6], pars->GC_11, amp[341]); 
  FFV1_0(w[127], w[2], w[90], pars->GC_11, amp[342]); 
  FFV1_0(w[129], w[135], w[5], pars->GC_11, amp[343]); 
  FFV1_0(w[129], w[2], w[89], pars->GC_11, amp[344]); 
  FFV1_0(w[136], w[44], w[6], pars->GC_11, amp[345]); 
  VVV1_0(w[87], w[6], w[137], pars->GC_10, amp[346]); 
  FFV1_0(w[129], w[44], w[87], pars->GC_11, amp[347]); 
  FFV1_0(w[136], w[51], w[5], pars->GC_11, amp[348]); 
  VVV1_0(w[87], w[5], w[138], pars->GC_10, amp[349]); 
  FFV1_0(w[127], w[51], w[87], pars->GC_11, amp[350]); 
  FFV1_0(w[136], w[2], w[29], pars->GC_11, amp[351]); 
  VVV1_0(w[87], w[29], w[134], pars->GC_10, amp[352]); 
  FFV1_0(w[133], w[2], w[87], pars->GC_11, amp[353]); 
  VVV1_0(w[134], w[96], w[6], pars->GC_10, amp[354]); 
  VVV1_0(w[134], w[97], w[4], pars->GC_10, amp[355]); 
  VVVV1_0(w[95], w[4], w[6], w[134], pars->GC_12, amp[356]); 
  VVVV3_0(w[95], w[4], w[6], w[134], pars->GC_12, amp[357]); 
  VVVV4_0(w[95], w[4], w[6], w[134], pars->GC_12, amp[358]); 
  FFV1_0(w[124], w[139], w[6], pars->GC_11, amp[359]); 
  FFV1_0(w[124], w[2], w[97], pars->GC_11, amp[360]); 
  FFV1_0(w[129], w[139], w[4], pars->GC_11, amp[361]); 
  FFV1_0(w[129], w[2], w[96], pars->GC_11, amp[362]); 
  FFV1_0(w[140], w[34], w[6], pars->GC_11, amp[363]); 
  VVV1_0(w[95], w[6], w[141], pars->GC_10, amp[364]); 
  FFV1_0(w[129], w[34], w[95], pars->GC_11, amp[365]); 
  FFV1_0(w[140], w[51], w[4], pars->GC_11, amp[366]); 
  VVV1_0(w[95], w[4], w[138], pars->GC_10, amp[367]); 
  FFV1_0(w[124], w[51], w[95], pars->GC_11, amp[368]); 
  FFV1_0(w[140], w[2], w[27], pars->GC_11, amp[369]); 
  VVV1_0(w[95], w[27], w[134], pars->GC_10, amp[370]); 
  FFV1_0(w[132], w[2], w[95], pars->GC_11, amp[371]); 
  VVV1_0(w[134], w[102], w[5], pars->GC_10, amp[372]); 
  VVV1_0(w[134], w[103], w[4], pars->GC_10, amp[373]); 
  VVVV1_0(w[101], w[4], w[5], w[134], pars->GC_12, amp[374]); 
  VVVV3_0(w[101], w[4], w[5], w[134], pars->GC_12, amp[375]); 
  VVVV4_0(w[101], w[4], w[5], w[134], pars->GC_12, amp[376]); 
  FFV1_0(w[124], w[142], w[5], pars->GC_11, amp[377]); 
  FFV1_0(w[124], w[2], w[103], pars->GC_11, amp[378]); 
  FFV1_0(w[127], w[142], w[4], pars->GC_11, amp[379]); 
  FFV1_0(w[127], w[2], w[102], pars->GC_11, amp[380]); 
  FFV1_0(w[143], w[34], w[5], pars->GC_11, amp[381]); 
  VVV1_0(w[101], w[5], w[141], pars->GC_10, amp[382]); 
  FFV1_0(w[127], w[34], w[101], pars->GC_11, amp[383]); 
  FFV1_0(w[143], w[44], w[4], pars->GC_11, amp[384]); 
  VVV1_0(w[101], w[4], w[137], pars->GC_10, amp[385]); 
  FFV1_0(w[124], w[44], w[101], pars->GC_11, amp[386]); 
  FFV1_0(w[143], w[2], w[24], pars->GC_11, amp[387]); 
  VVV1_0(w[101], w[24], w[134], pars->GC_10, amp[388]); 
  FFV1_0(w[131], w[2], w[101], pars->GC_11, amp[389]); 
  FFV1_0(w[144], w[36], w[6], pars->GC_11, amp[390]); 
  FFV1_0(w[144], w[37], w[5], pars->GC_11, amp[391]); 
  FFV1_0(w[127], w[145], w[6], pars->GC_11, amp[392]); 
  FFV1_0(w[127], w[37], w[1], pars->GC_11, amp[393]); 
  FFV1_0(w[129], w[145], w[5], pars->GC_11, amp[394]); 
  FFV1_0(w[129], w[36], w[1], pars->GC_11, amp[395]); 
  FFV1_0(w[144], w[34], w[29], pars->GC_11, amp[396]); 
  VVV1_0(w[1], w[29], w[141], pars->GC_10, amp[397]); 
  FFV1_0(w[133], w[34], w[1], pars->GC_11, amp[398]); 
  FFV1_0(w[144], w[45], w[6], pars->GC_11, amp[399]); 
  FFV1_0(w[144], w[46], w[4], pars->GC_11, amp[400]); 
  FFV1_0(w[124], w[146], w[6], pars->GC_11, amp[401]); 
  FFV1_0(w[124], w[46], w[1], pars->GC_11, amp[402]); 
  FFV1_0(w[129], w[146], w[4], pars->GC_11, amp[403]); 
  FFV1_0(w[129], w[45], w[1], pars->GC_11, amp[404]); 
  FFV1_0(w[144], w[44], w[27], pars->GC_11, amp[405]); 
  VVV1_0(w[1], w[27], w[137], pars->GC_10, amp[406]); 
  FFV1_0(w[132], w[44], w[1], pars->GC_11, amp[407]); 
  FFV1_0(w[144], w[52], w[5], pars->GC_11, amp[408]); 
  FFV1_0(w[144], w[53], w[4], pars->GC_11, amp[409]); 
  FFV1_0(w[124], w[147], w[5], pars->GC_11, amp[410]); 
  FFV1_0(w[124], w[53], w[1], pars->GC_11, amp[411]); 
  FFV1_0(w[127], w[147], w[4], pars->GC_11, amp[412]); 
  FFV1_0(w[127], w[52], w[1], pars->GC_11, amp[413]); 
  FFV1_0(w[144], w[51], w[24], pars->GC_11, amp[414]); 
  VVV1_0(w[1], w[24], w[138], pars->GC_10, amp[415]); 
  FFV1_0(w[131], w[51], w[1], pars->GC_11, amp[416]); 
  FFV1_0(w[144], w[68], w[6], pars->GC_11, amp[417]); 
  FFV1_0(w[144], w[2], w[67], pars->GC_11, amp[418]); 
  VVV1_0(w[134], w[110], w[6], pars->GC_10, amp[419]); 
  VVV1_0(w[134], w[1], w[67], pars->GC_10, amp[420]); 
  VVVV1_0(w[1], w[24], w[6], w[134], pars->GC_12, amp[421]); 
  VVVV3_0(w[1], w[24], w[6], w[134], pars->GC_12, amp[422]); 
  VVVV4_0(w[1], w[24], w[6], w[134], pars->GC_12, amp[423]); 
  FFV1_0(w[129], w[2], w[110], pars->GC_11, amp[424]); 
  FFV1_0(w[129], w[68], w[1], pars->GC_11, amp[425]); 
  FFV1_0(w[144], w[71], w[5], pars->GC_11, amp[426]); 
  FFV1_0(w[144], w[2], w[70], pars->GC_11, amp[427]); 
  VVV1_0(w[134], w[111], w[5], pars->GC_10, amp[428]); 
  VVV1_0(w[134], w[1], w[70], pars->GC_10, amp[429]); 
  VVVV1_0(w[1], w[27], w[5], w[134], pars->GC_12, amp[430]); 
  VVVV3_0(w[1], w[27], w[5], w[134], pars->GC_12, amp[431]); 
  VVVV4_0(w[1], w[27], w[5], w[134], pars->GC_12, amp[432]); 
  FFV1_0(w[127], w[2], w[111], pars->GC_11, amp[433]); 
  FFV1_0(w[127], w[71], w[1], pars->GC_11, amp[434]); 
  FFV1_0(w[144], w[74], w[4], pars->GC_11, amp[435]); 
  FFV1_0(w[144], w[2], w[73], pars->GC_11, amp[436]); 
  VVV1_0(w[134], w[112], w[4], pars->GC_10, amp[437]); 
  VVV1_0(w[134], w[1], w[73], pars->GC_10, amp[438]); 
  VVVV1_0(w[1], w[4], w[29], w[134], pars->GC_12, amp[439]); 
  VVVV3_0(w[1], w[4], w[29], w[134], pars->GC_12, amp[440]); 
  VVVV4_0(w[1], w[4], w[29], w[134], pars->GC_12, amp[441]); 
  FFV1_0(w[124], w[2], w[112], pars->GC_11, amp[442]); 
  FFV1_0(w[124], w[74], w[1], pars->GC_11, amp[443]); 
  VVV1_0(w[113], w[6], w[134], pars->GC_10, amp[444]); 
  VVV1_0(w[114], w[6], w[134], pars->GC_10, amp[445]); 
  VVV1_0(w[115], w[6], w[134], pars->GC_10, amp[446]); 
  FFV1_0(w[129], w[2], w[113], pars->GC_11, amp[447]); 
  FFV1_0(w[129], w[2], w[114], pars->GC_11, amp[448]); 
  FFV1_0(w[129], w[2], w[115], pars->GC_11, amp[449]); 
  FFV1_0(w[122], w[51], w[113], pars->GC_11, amp[450]); 
  FFV1_0(w[122], w[51], w[114], pars->GC_11, amp[451]); 
  FFV1_0(w[122], w[51], w[115], pars->GC_11, amp[452]); 
  VVV1_0(w[116], w[5], w[134], pars->GC_10, amp[453]); 
  VVV1_0(w[117], w[5], w[134], pars->GC_10, amp[454]); 
  VVV1_0(w[118], w[5], w[134], pars->GC_10, amp[455]); 
  FFV1_0(w[127], w[2], w[116], pars->GC_11, amp[456]); 
  FFV1_0(w[127], w[2], w[117], pars->GC_11, amp[457]); 
  FFV1_0(w[127], w[2], w[118], pars->GC_11, amp[458]); 
  FFV1_0(w[122], w[44], w[116], pars->GC_11, amp[459]); 
  FFV1_0(w[122], w[44], w[117], pars->GC_11, amp[460]); 
  FFV1_0(w[122], w[44], w[118], pars->GC_11, amp[461]); 
  VVV1_0(w[119], w[4], w[134], pars->GC_10, amp[462]); 
  VVV1_0(w[120], w[4], w[134], pars->GC_10, amp[463]); 
  VVV1_0(w[121], w[4], w[134], pars->GC_10, amp[464]); 
  FFV1_0(w[124], w[2], w[119], pars->GC_11, amp[465]); 
  FFV1_0(w[124], w[2], w[120], pars->GC_11, amp[466]); 
  FFV1_0(w[124], w[2], w[121], pars->GC_11, amp[467]); 
  FFV1_0(w[122], w[34], w[119], pars->GC_11, amp[468]); 
  FFV1_0(w[122], w[34], w[120], pars->GC_11, amp[469]); 
  FFV1_0(w[122], w[34], w[121], pars->GC_11, amp[470]); 
  FFV1_0(w[144], w[2], w[31], pars->GC_11, amp[471]); 
  FFV1_0(w[144], w[2], w[32], pars->GC_11, amp[472]); 
  FFV1_0(w[144], w[2], w[33], pars->GC_11, amp[473]); 
  VVV1_0(w[1], w[31], w[134], pars->GC_10, amp[474]); 
  VVV1_0(w[1], w[32], w[134], pars->GC_10, amp[475]); 
  VVV1_0(w[1], w[33], w[134], pars->GC_10, amp[476]); 
  FFV1_0(w[149], w[125], w[6], pars->GC_11, amp[477]); 
  FFV1_0(w[149], w[126], w[5], pars->GC_11, amp[478]); 
  VVV1_0(w[150], w[151], w[6], pars->GC_10, amp[479]); 
  FFV1_0(w[3], w[126], w[150], pars->GC_11, amp[480]); 
  VVV1_0(w[152], w[151], w[5], pars->GC_10, amp[481]); 
  FFV1_0(w[3], w[125], w[152], pars->GC_11, amp[482]); 
  FFV1_0(w[3], w[123], w[153], pars->GC_11, amp[483]); 
  FFV1_0(w[3], w[123], w[154], pars->GC_11, amp[484]); 
  FFV1_0(w[3], w[123], w[155], pars->GC_11, amp[485]); 
  FFV1_0(w[39], w[156], w[6], pars->GC_11, amp[486]); 
  FFV1_0(w[157], w[123], w[6], pars->GC_11, amp[487]); 
  FFV1_0(w[39], w[123], w[152], pars->GC_11, amp[488]); 
  FFV1_0(w[42], w[156], w[5], pars->GC_11, amp[489]); 
  FFV1_0(w[158], w[123], w[5], pars->GC_11, amp[490]); 
  FFV1_0(w[42], w[123], w[150], pars->GC_11, amp[491]); 
  FFV1_0(w[3], w[156], w[29], pars->GC_11, amp[492]); 
  FFV1_0(w[149], w[123], w[29], pars->GC_11, amp[493]); 
  FFV1_0(w[3], w[123], w[159], pars->GC_11, amp[494]); 
  FFV1_0(w[78], w[160], w[6], pars->GC_11, amp[495]); 
  FFV1_0(w[79], w[160], w[5], pars->GC_11, amp[496]); 
  VVV1_0(w[150], w[161], w[6], pars->GC_10, amp[497]); 
  FFV1_0(w[79], w[2], w[150], pars->GC_11, amp[498]); 
  VVV1_0(w[152], w[161], w[5], pars->GC_10, amp[499]); 
  FFV1_0(w[78], w[2], w[152], pars->GC_11, amp[500]); 
  FFV1_0(w[76], w[2], w[153], pars->GC_11, amp[501]); 
  FFV1_0(w[76], w[2], w[154], pars->GC_11, amp[502]); 
  FFV1_0(w[76], w[2], w[155], pars->GC_11, amp[503]); 
  FFV1_0(w[162], w[44], w[6], pars->GC_11, amp[504]); 
  FFV1_0(w[76], w[163], w[6], pars->GC_11, amp[505]); 
  FFV1_0(w[76], w[44], w[152], pars->GC_11, amp[506]); 
  FFV1_0(w[162], w[51], w[5], pars->GC_11, amp[507]); 
  FFV1_0(w[76], w[164], w[5], pars->GC_11, amp[508]); 
  FFV1_0(w[76], w[51], w[150], pars->GC_11, amp[509]); 
  FFV1_0(w[162], w[2], w[29], pars->GC_11, amp[510]); 
  FFV1_0(w[76], w[160], w[29], pars->GC_11, amp[511]); 
  FFV1_0(w[76], w[2], w[159], pars->GC_11, amp[512]); 
  FFV1_0(w[98], w[160], w[6], pars->GC_11, amp[513]); 
  FFV1_0(w[3], w[160], w[97], pars->GC_11, amp[514]); 
  FFV1_0(w[149], w[139], w[6], pars->GC_11, amp[515]); 
  FFV1_0(w[149], w[2], w[97], pars->GC_11, amp[516]); 
  FFV1_0(w[3], w[139], w[152], pars->GC_11, amp[517]); 
  FFV1_0(w[98], w[2], w[152], pars->GC_11, amp[518]); 
  VVVV1_0(w[148], w[95], w[8], w[6], pars->GC_12, amp[519]); 
  VVVV3_0(w[148], w[95], w[8], w[6], pars->GC_12, amp[520]); 
  VVVV4_0(w[148], w[95], w[8], w[6], pars->GC_12, amp[521]); 
  VVV1_0(w[8], w[6], w[165], pars->GC_10, amp[522]); 
  VVV1_0(w[95], w[6], w[166], pars->GC_10, amp[523]); 
  VVV1_0(w[95], w[8], w[152], pars->GC_10, amp[524]); 
  FFV1_0(w[3], w[51], w[165], pars->GC_11, amp[525]); 
  FFV1_0(w[3], w[164], w[95], pars->GC_11, amp[526]); 
  FFV1_0(w[149], w[51], w[95], pars->GC_11, amp[527]); 
  FFV1_0(w[42], w[2], w[165], pars->GC_11, amp[528]); 
  FFV1_0(w[42], w[160], w[95], pars->GC_11, amp[529]); 
  FFV1_0(w[158], w[2], w[95], pars->GC_11, amp[530]); 
  FFV1_0(w[104], w[160], w[5], pars->GC_11, amp[531]); 
  FFV1_0(w[3], w[160], w[103], pars->GC_11, amp[532]); 
  FFV1_0(w[149], w[142], w[5], pars->GC_11, amp[533]); 
  FFV1_0(w[149], w[2], w[103], pars->GC_11, amp[534]); 
  FFV1_0(w[3], w[142], w[150], pars->GC_11, amp[535]); 
  FFV1_0(w[104], w[2], w[150], pars->GC_11, amp[536]); 
  VVVV1_0(w[148], w[101], w[8], w[5], pars->GC_12, amp[537]); 
  VVVV3_0(w[148], w[101], w[8], w[5], pars->GC_12, amp[538]); 
  VVVV4_0(w[148], w[101], w[8], w[5], pars->GC_12, amp[539]); 
  VVV1_0(w[8], w[5], w[167], pars->GC_10, amp[540]); 
  VVV1_0(w[101], w[5], w[166], pars->GC_10, amp[541]); 
  VVV1_0(w[101], w[8], w[150], pars->GC_10, amp[542]); 
  FFV1_0(w[3], w[44], w[167], pars->GC_11, amp[543]); 
  FFV1_0(w[3], w[163], w[101], pars->GC_11, amp[544]); 
  FFV1_0(w[149], w[44], w[101], pars->GC_11, amp[545]); 
  FFV1_0(w[39], w[2], w[167], pars->GC_11, amp[546]); 
  FFV1_0(w[39], w[160], w[101], pars->GC_11, amp[547]); 
  FFV1_0(w[157], w[2], w[101], pars->GC_11, amp[548]); 
  VVV1_0(w[168], w[10], w[6], pars->GC_10, amp[549]); 
  VVV1_0(w[168], w[11], w[5], pars->GC_10, amp[550]); 
  VVVV1_0(w[8], w[5], w[6], w[168], pars->GC_12, amp[551]); 
  VVVV3_0(w[8], w[5], w[6], w[168], pars->GC_12, amp[552]); 
  VVVV4_0(w[8], w[5], w[6], w[168], pars->GC_12, amp[553]); 
  VVV1_0(w[150], w[169], w[6], pars->GC_10, amp[554]); 
  VVV1_0(w[150], w[1], w[11], pars->GC_10, amp[555]); 
  VVVV1_0(w[1], w[8], w[6], w[150], pars->GC_12, amp[556]); 
  VVVV3_0(w[1], w[8], w[6], w[150], pars->GC_12, amp[557]); 
  VVVV4_0(w[1], w[8], w[6], w[150], pars->GC_12, amp[558]); 
  VVV1_0(w[152], w[169], w[5], pars->GC_10, amp[559]); 
  VVV1_0(w[152], w[1], w[10], pars->GC_10, amp[560]); 
  VVVV1_0(w[1], w[8], w[5], w[152], pars->GC_12, amp[561]); 
  VVVV3_0(w[1], w[8], w[5], w[152], pars->GC_12, amp[562]); 
  VVVV4_0(w[1], w[8], w[5], w[152], pars->GC_12, amp[563]); 
  VVV1_0(w[8], w[6], w[170], pars->GC_10, amp[564]); 
  VVV1_0(w[8], w[6], w[171], pars->GC_10, amp[565]); 
  VVV1_0(w[8], w[6], w[172], pars->GC_10, amp[566]); 
  VVV1_0(w[8], w[5], w[173], pars->GC_10, amp[567]); 
  VVV1_0(w[8], w[5], w[174], pars->GC_10, amp[568]); 
  VVV1_0(w[8], w[5], w[175], pars->GC_10, amp[569]); 
  VVV1_0(w[1], w[8], w[153], pars->GC_10, amp[570]); 
  VVV1_0(w[1], w[8], w[154], pars->GC_10, amp[571]); 
  VVV1_0(w[1], w[8], w[155], pars->GC_10, amp[572]); 
  VVVV1_0(w[148], w[1], w[8], w[29], pars->GC_12, amp[573]); 
  VVVV3_0(w[148], w[1], w[8], w[29], pars->GC_12, amp[574]); 
  VVVV4_0(w[148], w[1], w[8], w[29], pars->GC_12, amp[575]); 
  VVV1_0(w[8], w[29], w[168], pars->GC_10, amp[576]); 
  VVV1_0(w[1], w[29], w[166], pars->GC_10, amp[577]); 
  VVV1_0(w[1], w[8], w[159], pars->GC_10, amp[578]); 
  VVV1_0(w[168], w[47], w[6], pars->GC_10, amp[579]); 
  FFV1_0(w[3], w[46], w[168], pars->GC_11, amp[580]); 
  FFV1_0(w[149], w[146], w[6], pars->GC_11, amp[581]); 
  FFV1_0(w[149], w[46], w[1], pars->GC_11, amp[582]); 
  FFV1_0(w[3], w[146], w[152], pars->GC_11, amp[583]); 
  VVV1_0(w[152], w[1], w[47], pars->GC_10, amp[584]); 
  FFV1_0(w[3], w[44], w[173], pars->GC_11, amp[585]); 
  FFV1_0(w[3], w[44], w[174], pars->GC_11, amp[586]); 
  FFV1_0(w[3], w[44], w[175], pars->GC_11, amp[587]); 
  FFV1_0(w[42], w[44], w[168], pars->GC_11, amp[588]); 
  FFV1_0(w[42], w[163], w[1], pars->GC_11, amp[589]); 
  FFV1_0(w[158], w[44], w[1], pars->GC_11, amp[590]); 
  VVV1_0(w[168], w[54], w[5], pars->GC_10, amp[591]); 
  FFV1_0(w[3], w[53], w[168], pars->GC_11, amp[592]); 
  FFV1_0(w[149], w[147], w[5], pars->GC_11, amp[593]); 
  FFV1_0(w[149], w[53], w[1], pars->GC_11, amp[594]); 
  FFV1_0(w[3], w[147], w[150], pars->GC_11, amp[595]); 
  VVV1_0(w[150], w[1], w[54], pars->GC_10, amp[596]); 
  FFV1_0(w[3], w[51], w[170], pars->GC_11, amp[597]); 
  FFV1_0(w[3], w[51], w[171], pars->GC_11, amp[598]); 
  FFV1_0(w[3], w[51], w[172], pars->GC_11, amp[599]); 
  FFV1_0(w[39], w[51], w[168], pars->GC_11, amp[600]); 
  FFV1_0(w[39], w[164], w[1], pars->GC_11, amp[601]); 
  FFV1_0(w[157], w[51], w[1], pars->GC_11, amp[602]); 
  VVV1_0(w[168], w[62], w[6], pars->GC_10, amp[603]); 
  FFV1_0(w[61], w[2], w[168], pars->GC_11, amp[604]); 
  FFV1_0(w[108], w[160], w[6], pars->GC_11, amp[605]); 
  FFV1_0(w[61], w[160], w[1], pars->GC_11, amp[606]); 
  FFV1_0(w[108], w[2], w[152], pars->GC_11, amp[607]); 
  VVV1_0(w[152], w[1], w[62], pars->GC_10, amp[608]); 
  FFV1_0(w[39], w[2], w[173], pars->GC_11, amp[609]); 
  FFV1_0(w[39], w[2], w[174], pars->GC_11, amp[610]); 
  FFV1_0(w[39], w[2], w[175], pars->GC_11, amp[611]); 
  VVV1_0(w[168], w[65], w[5], pars->GC_10, amp[612]); 
  FFV1_0(w[64], w[2], w[168], pars->GC_11, amp[613]); 
  FFV1_0(w[109], w[160], w[5], pars->GC_11, amp[614]); 
  FFV1_0(w[64], w[160], w[1], pars->GC_11, amp[615]); 
  FFV1_0(w[109], w[2], w[150], pars->GC_11, amp[616]); 
  VVV1_0(w[150], w[1], w[65], pars->GC_10, amp[617]); 
  FFV1_0(w[42], w[2], w[170], pars->GC_11, amp[618]); 
  FFV1_0(w[42], w[2], w[171], pars->GC_11, amp[619]); 
  FFV1_0(w[42], w[2], w[172], pars->GC_11, amp[620]); 
  FFV1_0(w[3], w[74], w[168], pars->GC_11, amp[621]); 
  FFV1_0(w[72], w[2], w[168], pars->GC_11, amp[622]); 
  FFV1_0(w[3], w[160], w[112], pars->GC_11, amp[623]); 
  FFV1_0(w[72], w[160], w[1], pars->GC_11, amp[624]); 
  FFV1_0(w[149], w[2], w[112], pars->GC_11, amp[625]); 
  FFV1_0(w[149], w[74], w[1], pars->GC_11, amp[626]); 
  FFV1_0(w[3], w[160], w[119], pars->GC_11, amp[627]); 
  FFV1_0(w[3], w[160], w[120], pars->GC_11, amp[628]); 
  FFV1_0(w[3], w[160], w[121], pars->GC_11, amp[629]); 
  FFV1_0(w[149], w[2], w[119], pars->GC_11, amp[630]); 
  FFV1_0(w[149], w[2], w[120], pars->GC_11, amp[631]); 
  FFV1_0(w[149], w[2], w[121], pars->GC_11, amp[632]); 
  VVV1_0(w[148], w[119], w[8], pars->GC_10, amp[633]); 
  VVV1_0(w[148], w[120], w[8], pars->GC_10, amp[634]); 
  VVV1_0(w[148], w[121], w[8], pars->GC_10, amp[635]); 
  FFV1_0(w[177], w[128], w[6], pars->GC_11, amp[636]); 
  FFV1_0(w[177], w[126], w[4], pars->GC_11, amp[637]); 
  VVV1_0(w[178], w[151], w[6], pars->GC_10, amp[638]); 
  FFV1_0(w[3], w[126], w[178], pars->GC_11, amp[639]); 
  VVV1_0(w[179], w[151], w[4], pars->GC_10, amp[640]); 
  FFV1_0(w[3], w[128], w[179], pars->GC_11, amp[641]); 
  FFV1_0(w[3], w[123], w[180], pars->GC_11, amp[642]); 
  FFV1_0(w[3], w[123], w[181], pars->GC_11, amp[643]); 
  FFV1_0(w[3], w[123], w[182], pars->GC_11, amp[644]); 
  FFV1_0(w[48], w[183], w[6], pars->GC_11, amp[645]); 
  FFV1_0(w[184], w[123], w[6], pars->GC_11, amp[646]); 
  FFV1_0(w[48], w[123], w[179], pars->GC_11, amp[647]); 
  FFV1_0(w[42], w[183], w[4], pars->GC_11, amp[648]); 
  FFV1_0(w[185], w[123], w[4], pars->GC_11, amp[649]); 
  FFV1_0(w[42], w[123], w[178], pars->GC_11, amp[650]); 
  FFV1_0(w[3], w[183], w[27], pars->GC_11, amp[651]); 
  FFV1_0(w[177], w[123], w[27], pars->GC_11, amp[652]); 
  FFV1_0(w[3], w[123], w[186], pars->GC_11, amp[653]); 
  FFV1_0(w[81], w[187], w[6], pars->GC_11, amp[654]); 
  FFV1_0(w[79], w[187], w[4], pars->GC_11, amp[655]); 
  VVV1_0(w[178], w[161], w[6], pars->GC_10, amp[656]); 
  FFV1_0(w[79], w[2], w[178], pars->GC_11, amp[657]); 
  VVV1_0(w[179], w[161], w[4], pars->GC_10, amp[658]); 
  FFV1_0(w[81], w[2], w[179], pars->GC_11, amp[659]); 
  FFV1_0(w[76], w[2], w[180], pars->GC_11, amp[660]); 
  FFV1_0(w[76], w[2], w[181], pars->GC_11, amp[661]); 
  FFV1_0(w[76], w[2], w[182], pars->GC_11, amp[662]); 
  FFV1_0(w[188], w[34], w[6], pars->GC_11, amp[663]); 
  FFV1_0(w[76], w[189], w[6], pars->GC_11, amp[664]); 
  FFV1_0(w[76], w[34], w[179], pars->GC_11, amp[665]); 
  FFV1_0(w[188], w[51], w[4], pars->GC_11, amp[666]); 
  FFV1_0(w[76], w[190], w[4], pars->GC_11, amp[667]); 
  FFV1_0(w[76], w[51], w[178], pars->GC_11, amp[668]); 
  FFV1_0(w[188], w[2], w[27], pars->GC_11, amp[669]); 
  FFV1_0(w[76], w[187], w[27], pars->GC_11, amp[670]); 
  FFV1_0(w[76], w[2], w[186], pars->GC_11, amp[671]); 
  FFV1_0(w[91], w[187], w[6], pars->GC_11, amp[672]); 
  FFV1_0(w[3], w[187], w[90], pars->GC_11, amp[673]); 
  FFV1_0(w[177], w[135], w[6], pars->GC_11, amp[674]); 
  FFV1_0(w[177], w[2], w[90], pars->GC_11, amp[675]); 
  FFV1_0(w[3], w[135], w[179], pars->GC_11, amp[676]); 
  FFV1_0(w[91], w[2], w[179], pars->GC_11, amp[677]); 
  VVVV1_0(w[176], w[87], w[8], w[6], pars->GC_12, amp[678]); 
  VVVV3_0(w[176], w[87], w[8], w[6], pars->GC_12, amp[679]); 
  VVVV4_0(w[176], w[87], w[8], w[6], pars->GC_12, amp[680]); 
  VVV1_0(w[8], w[6], w[191], pars->GC_10, amp[681]); 
  VVV1_0(w[87], w[6], w[192], pars->GC_10, amp[682]); 
  VVV1_0(w[87], w[8], w[179], pars->GC_10, amp[683]); 
  FFV1_0(w[3], w[51], w[191], pars->GC_11, amp[684]); 
  FFV1_0(w[3], w[190], w[87], pars->GC_11, amp[685]); 
  FFV1_0(w[177], w[51], w[87], pars->GC_11, amp[686]); 
  FFV1_0(w[42], w[2], w[191], pars->GC_11, amp[687]); 
  FFV1_0(w[42], w[187], w[87], pars->GC_11, amp[688]); 
  FFV1_0(w[185], w[2], w[87], pars->GC_11, amp[689]); 
  FFV1_0(w[104], w[187], w[4], pars->GC_11, amp[690]); 
  FFV1_0(w[3], w[187], w[102], pars->GC_11, amp[691]); 
  FFV1_0(w[177], w[142], w[4], pars->GC_11, amp[692]); 
  FFV1_0(w[177], w[2], w[102], pars->GC_11, amp[693]); 
  FFV1_0(w[3], w[142], w[178], pars->GC_11, amp[694]); 
  FFV1_0(w[104], w[2], w[178], pars->GC_11, amp[695]); 
  VVVV1_0(w[176], w[101], w[8], w[4], pars->GC_12, amp[696]); 
  VVVV3_0(w[176], w[101], w[8], w[4], pars->GC_12, amp[697]); 
  VVVV4_0(w[176], w[101], w[8], w[4], pars->GC_12, amp[698]); 
  VVV1_0(w[8], w[4], w[193], pars->GC_10, amp[699]); 
  VVV1_0(w[101], w[4], w[192], pars->GC_10, amp[700]); 
  VVV1_0(w[101], w[8], w[178], pars->GC_10, amp[701]); 
  FFV1_0(w[3], w[34], w[193], pars->GC_11, amp[702]); 
  FFV1_0(w[3], w[189], w[101], pars->GC_11, amp[703]); 
  FFV1_0(w[177], w[34], w[101], pars->GC_11, amp[704]); 
  FFV1_0(w[48], w[2], w[193], pars->GC_11, amp[705]); 
  FFV1_0(w[48], w[187], w[101], pars->GC_11, amp[706]); 
  FFV1_0(w[184], w[2], w[101], pars->GC_11, amp[707]); 
  VVV1_0(w[194], w[13], w[6], pars->GC_10, amp[708]); 
  VVV1_0(w[194], w[11], w[4], pars->GC_10, amp[709]); 
  VVVV1_0(w[8], w[4], w[6], w[194], pars->GC_12, amp[710]); 
  VVVV3_0(w[8], w[4], w[6], w[194], pars->GC_12, amp[711]); 
  VVVV4_0(w[8], w[4], w[6], w[194], pars->GC_12, amp[712]); 
  VVV1_0(w[178], w[169], w[6], pars->GC_10, amp[713]); 
  VVV1_0(w[178], w[1], w[11], pars->GC_10, amp[714]); 
  VVVV1_0(w[1], w[8], w[6], w[178], pars->GC_12, amp[715]); 
  VVVV3_0(w[1], w[8], w[6], w[178], pars->GC_12, amp[716]); 
  VVVV4_0(w[1], w[8], w[6], w[178], pars->GC_12, amp[717]); 
  VVV1_0(w[179], w[169], w[4], pars->GC_10, amp[718]); 
  VVV1_0(w[179], w[1], w[13], pars->GC_10, amp[719]); 
  VVVV1_0(w[1], w[8], w[4], w[179], pars->GC_12, amp[720]); 
  VVVV3_0(w[1], w[8], w[4], w[179], pars->GC_12, amp[721]); 
  VVVV4_0(w[1], w[8], w[4], w[179], pars->GC_12, amp[722]); 
  VVV1_0(w[8], w[6], w[195], pars->GC_10, amp[723]); 
  VVV1_0(w[8], w[6], w[196], pars->GC_10, amp[724]); 
  VVV1_0(w[8], w[6], w[197], pars->GC_10, amp[725]); 
  VVV1_0(w[8], w[4], w[198], pars->GC_10, amp[726]); 
  VVV1_0(w[8], w[4], w[199], pars->GC_10, amp[727]); 
  VVV1_0(w[8], w[4], w[200], pars->GC_10, amp[728]); 
  VVV1_0(w[1], w[8], w[180], pars->GC_10, amp[729]); 
  VVV1_0(w[1], w[8], w[181], pars->GC_10, amp[730]); 
  VVV1_0(w[1], w[8], w[182], pars->GC_10, amp[731]); 
  VVVV1_0(w[176], w[1], w[8], w[27], pars->GC_12, amp[732]); 
  VVVV3_0(w[176], w[1], w[8], w[27], pars->GC_12, amp[733]); 
  VVVV4_0(w[176], w[1], w[8], w[27], pars->GC_12, amp[734]); 
  VVV1_0(w[8], w[27], w[194], pars->GC_10, amp[735]); 
  VVV1_0(w[1], w[27], w[192], pars->GC_10, amp[736]); 
  VVV1_0(w[1], w[8], w[186], pars->GC_10, amp[737]); 
  VVV1_0(w[194], w[38], w[6], pars->GC_10, amp[738]); 
  FFV1_0(w[3], w[37], w[194], pars->GC_11, amp[739]); 
  FFV1_0(w[177], w[145], w[6], pars->GC_11, amp[740]); 
  FFV1_0(w[177], w[37], w[1], pars->GC_11, amp[741]); 
  FFV1_0(w[3], w[145], w[179], pars->GC_11, amp[742]); 
  VVV1_0(w[179], w[1], w[38], pars->GC_10, amp[743]); 
  FFV1_0(w[3], w[34], w[198], pars->GC_11, amp[744]); 
  FFV1_0(w[3], w[34], w[199], pars->GC_11, amp[745]); 
  FFV1_0(w[3], w[34], w[200], pars->GC_11, amp[746]); 
  FFV1_0(w[42], w[34], w[194], pars->GC_11, amp[747]); 
  FFV1_0(w[42], w[189], w[1], pars->GC_11, amp[748]); 
  FFV1_0(w[185], w[34], w[1], pars->GC_11, amp[749]); 
  VVV1_0(w[194], w[54], w[4], pars->GC_10, amp[750]); 
  FFV1_0(w[3], w[52], w[194], pars->GC_11, amp[751]); 
  FFV1_0(w[177], w[147], w[4], pars->GC_11, amp[752]); 
  FFV1_0(w[177], w[52], w[1], pars->GC_11, amp[753]); 
  FFV1_0(w[3], w[147], w[178], pars->GC_11, amp[754]); 
  VVV1_0(w[178], w[1], w[54], pars->GC_10, amp[755]); 
  FFV1_0(w[3], w[51], w[195], pars->GC_11, amp[756]); 
  FFV1_0(w[3], w[51], w[196], pars->GC_11, amp[757]); 
  FFV1_0(w[3], w[51], w[197], pars->GC_11, amp[758]); 
  FFV1_0(w[48], w[51], w[194], pars->GC_11, amp[759]); 
  FFV1_0(w[48], w[190], w[1], pars->GC_11, amp[760]); 
  FFV1_0(w[184], w[51], w[1], pars->GC_11, amp[761]); 
  VVV1_0(w[194], w[59], w[6], pars->GC_10, amp[762]); 
  FFV1_0(w[58], w[2], w[194], pars->GC_11, amp[763]); 
  FFV1_0(w[107], w[187], w[6], pars->GC_11, amp[764]); 
  FFV1_0(w[58], w[187], w[1], pars->GC_11, amp[765]); 
  FFV1_0(w[107], w[2], w[179], pars->GC_11, amp[766]); 
  VVV1_0(w[179], w[1], w[59], pars->GC_10, amp[767]); 
  FFV1_0(w[48], w[2], w[198], pars->GC_11, amp[768]); 
  FFV1_0(w[48], w[2], w[199], pars->GC_11, amp[769]); 
  FFV1_0(w[48], w[2], w[200], pars->GC_11, amp[770]); 
  VVV1_0(w[194], w[65], w[4], pars->GC_10, amp[771]); 
  FFV1_0(w[63], w[2], w[194], pars->GC_11, amp[772]); 
  FFV1_0(w[109], w[187], w[4], pars->GC_11, amp[773]); 
  FFV1_0(w[63], w[187], w[1], pars->GC_11, amp[774]); 
  FFV1_0(w[109], w[2], w[178], pars->GC_11, amp[775]); 
  VVV1_0(w[178], w[1], w[65], pars->GC_10, amp[776]); 
  FFV1_0(w[42], w[2], w[195], pars->GC_11, amp[777]); 
  FFV1_0(w[42], w[2], w[196], pars->GC_11, amp[778]); 
  FFV1_0(w[42], w[2], w[197], pars->GC_11, amp[779]); 
  FFV1_0(w[3], w[71], w[194], pars->GC_11, amp[780]); 
  FFV1_0(w[69], w[2], w[194], pars->GC_11, amp[781]); 
  FFV1_0(w[3], w[187], w[111], pars->GC_11, amp[782]); 
  FFV1_0(w[69], w[187], w[1], pars->GC_11, amp[783]); 
  FFV1_0(w[177], w[2], w[111], pars->GC_11, amp[784]); 
  FFV1_0(w[177], w[71], w[1], pars->GC_11, amp[785]); 
  FFV1_0(w[3], w[187], w[116], pars->GC_11, amp[786]); 
  FFV1_0(w[3], w[187], w[117], pars->GC_11, amp[787]); 
  FFV1_0(w[3], w[187], w[118], pars->GC_11, amp[788]); 
  FFV1_0(w[177], w[2], w[116], pars->GC_11, amp[789]); 
  FFV1_0(w[177], w[2], w[117], pars->GC_11, amp[790]); 
  FFV1_0(w[177], w[2], w[118], pars->GC_11, amp[791]); 
  VVV1_0(w[176], w[116], w[8], pars->GC_10, amp[792]); 
  VVV1_0(w[176], w[117], w[8], pars->GC_10, amp[793]); 
  VVV1_0(w[176], w[118], w[8], pars->GC_10, amp[794]); 
  FFV1_0(w[202], w[128], w[5], pars->GC_11, amp[795]); 
  FFV1_0(w[202], w[125], w[4], pars->GC_11, amp[796]); 
  VVV1_0(w[203], w[151], w[5], pars->GC_10, amp[797]); 
  FFV1_0(w[3], w[125], w[203], pars->GC_11, amp[798]); 
  VVV1_0(w[204], w[151], w[4], pars->GC_10, amp[799]); 
  FFV1_0(w[3], w[128], w[204], pars->GC_11, amp[800]); 
  FFV1_0(w[3], w[123], w[205], pars->GC_11, amp[801]); 
  FFV1_0(w[3], w[123], w[206], pars->GC_11, amp[802]); 
  FFV1_0(w[3], w[123], w[207], pars->GC_11, amp[803]); 
  FFV1_0(w[48], w[208], w[5], pars->GC_11, amp[804]); 
  FFV1_0(w[209], w[123], w[5], pars->GC_11, amp[805]); 
  FFV1_0(w[48], w[123], w[204], pars->GC_11, amp[806]); 
  FFV1_0(w[39], w[208], w[4], pars->GC_11, amp[807]); 
  FFV1_0(w[210], w[123], w[4], pars->GC_11, amp[808]); 
  FFV1_0(w[39], w[123], w[203], pars->GC_11, amp[809]); 
  FFV1_0(w[3], w[208], w[24], pars->GC_11, amp[810]); 
  FFV1_0(w[202], w[123], w[24], pars->GC_11, amp[811]); 
  FFV1_0(w[3], w[123], w[211], pars->GC_11, amp[812]); 
  FFV1_0(w[81], w[212], w[5], pars->GC_11, amp[813]); 
  FFV1_0(w[78], w[212], w[4], pars->GC_11, amp[814]); 
  VVV1_0(w[203], w[161], w[5], pars->GC_10, amp[815]); 
  FFV1_0(w[78], w[2], w[203], pars->GC_11, amp[816]); 
  VVV1_0(w[204], w[161], w[4], pars->GC_10, amp[817]); 
  FFV1_0(w[81], w[2], w[204], pars->GC_11, amp[818]); 
  FFV1_0(w[76], w[2], w[205], pars->GC_11, amp[819]); 
  FFV1_0(w[76], w[2], w[206], pars->GC_11, amp[820]); 
  FFV1_0(w[76], w[2], w[207], pars->GC_11, amp[821]); 
  FFV1_0(w[213], w[34], w[5], pars->GC_11, amp[822]); 
  FFV1_0(w[76], w[214], w[5], pars->GC_11, amp[823]); 
  FFV1_0(w[76], w[34], w[204], pars->GC_11, amp[824]); 
  FFV1_0(w[213], w[44], w[4], pars->GC_11, amp[825]); 
  FFV1_0(w[76], w[215], w[4], pars->GC_11, amp[826]); 
  FFV1_0(w[76], w[44], w[203], pars->GC_11, amp[827]); 
  FFV1_0(w[213], w[2], w[24], pars->GC_11, amp[828]); 
  FFV1_0(w[76], w[212], w[24], pars->GC_11, amp[829]); 
  FFV1_0(w[76], w[2], w[211], pars->GC_11, amp[830]); 
  FFV1_0(w[91], w[212], w[5], pars->GC_11, amp[831]); 
  FFV1_0(w[3], w[212], w[89], pars->GC_11, amp[832]); 
  FFV1_0(w[202], w[135], w[5], pars->GC_11, amp[833]); 
  FFV1_0(w[202], w[2], w[89], pars->GC_11, amp[834]); 
  FFV1_0(w[3], w[135], w[204], pars->GC_11, amp[835]); 
  FFV1_0(w[91], w[2], w[204], pars->GC_11, amp[836]); 
  VVVV1_0(w[201], w[87], w[8], w[5], pars->GC_12, amp[837]); 
  VVVV3_0(w[201], w[87], w[8], w[5], pars->GC_12, amp[838]); 
  VVVV4_0(w[201], w[87], w[8], w[5], pars->GC_12, amp[839]); 
  VVV1_0(w[8], w[5], w[216], pars->GC_10, amp[840]); 
  VVV1_0(w[87], w[5], w[217], pars->GC_10, amp[841]); 
  VVV1_0(w[87], w[8], w[204], pars->GC_10, amp[842]); 
  FFV1_0(w[3], w[44], w[216], pars->GC_11, amp[843]); 
  FFV1_0(w[3], w[215], w[87], pars->GC_11, amp[844]); 
  FFV1_0(w[202], w[44], w[87], pars->GC_11, amp[845]); 
  FFV1_0(w[39], w[2], w[216], pars->GC_11, amp[846]); 
  FFV1_0(w[39], w[212], w[87], pars->GC_11, amp[847]); 
  FFV1_0(w[210], w[2], w[87], pars->GC_11, amp[848]); 
  FFV1_0(w[98], w[212], w[4], pars->GC_11, amp[849]); 
  FFV1_0(w[3], w[212], w[96], pars->GC_11, amp[850]); 
  FFV1_0(w[202], w[139], w[4], pars->GC_11, amp[851]); 
  FFV1_0(w[202], w[2], w[96], pars->GC_11, amp[852]); 
  FFV1_0(w[3], w[139], w[203], pars->GC_11, amp[853]); 
  FFV1_0(w[98], w[2], w[203], pars->GC_11, amp[854]); 
  VVVV1_0(w[201], w[95], w[8], w[4], pars->GC_12, amp[855]); 
  VVVV3_0(w[201], w[95], w[8], w[4], pars->GC_12, amp[856]); 
  VVVV4_0(w[201], w[95], w[8], w[4], pars->GC_12, amp[857]); 
  VVV1_0(w[8], w[4], w[218], pars->GC_10, amp[858]); 
  VVV1_0(w[95], w[4], w[217], pars->GC_10, amp[859]); 
  VVV1_0(w[95], w[8], w[203], pars->GC_10, amp[860]); 
  FFV1_0(w[3], w[34], w[218], pars->GC_11, amp[861]); 
  FFV1_0(w[3], w[214], w[95], pars->GC_11, amp[862]); 
  FFV1_0(w[202], w[34], w[95], pars->GC_11, amp[863]); 
  FFV1_0(w[48], w[2], w[218], pars->GC_11, amp[864]); 
  FFV1_0(w[48], w[212], w[95], pars->GC_11, amp[865]); 
  FFV1_0(w[209], w[2], w[95], pars->GC_11, amp[866]); 
  VVV1_0(w[219], w[13], w[5], pars->GC_10, amp[867]); 
  VVV1_0(w[219], w[10], w[4], pars->GC_10, amp[868]); 
  VVVV1_0(w[8], w[4], w[5], w[219], pars->GC_12, amp[869]); 
  VVVV3_0(w[8], w[4], w[5], w[219], pars->GC_12, amp[870]); 
  VVVV4_0(w[8], w[4], w[5], w[219], pars->GC_12, amp[871]); 
  VVV1_0(w[203], w[169], w[5], pars->GC_10, amp[872]); 
  VVV1_0(w[203], w[1], w[10], pars->GC_10, amp[873]); 
  VVVV1_0(w[1], w[8], w[5], w[203], pars->GC_12, amp[874]); 
  VVVV3_0(w[1], w[8], w[5], w[203], pars->GC_12, amp[875]); 
  VVVV4_0(w[1], w[8], w[5], w[203], pars->GC_12, amp[876]); 
  VVV1_0(w[204], w[169], w[4], pars->GC_10, amp[877]); 
  VVV1_0(w[204], w[1], w[13], pars->GC_10, amp[878]); 
  VVVV1_0(w[1], w[8], w[4], w[204], pars->GC_12, amp[879]); 
  VVVV3_0(w[1], w[8], w[4], w[204], pars->GC_12, amp[880]); 
  VVVV4_0(w[1], w[8], w[4], w[204], pars->GC_12, amp[881]); 
  VVV1_0(w[8], w[5], w[220], pars->GC_10, amp[882]); 
  VVV1_0(w[8], w[5], w[221], pars->GC_10, amp[883]); 
  VVV1_0(w[8], w[5], w[222], pars->GC_10, amp[884]); 
  VVV1_0(w[8], w[4], w[223], pars->GC_10, amp[885]); 
  VVV1_0(w[8], w[4], w[224], pars->GC_10, amp[886]); 
  VVV1_0(w[8], w[4], w[225], pars->GC_10, amp[887]); 
  VVV1_0(w[1], w[8], w[205], pars->GC_10, amp[888]); 
  VVV1_0(w[1], w[8], w[206], pars->GC_10, amp[889]); 
  VVV1_0(w[1], w[8], w[207], pars->GC_10, amp[890]); 
  VVVV1_0(w[201], w[1], w[8], w[24], pars->GC_12, amp[891]); 
  VVVV3_0(w[201], w[1], w[8], w[24], pars->GC_12, amp[892]); 
  VVVV4_0(w[201], w[1], w[8], w[24], pars->GC_12, amp[893]); 
  VVV1_0(w[8], w[24], w[219], pars->GC_10, amp[894]); 
  VVV1_0(w[1], w[24], w[217], pars->GC_10, amp[895]); 
  VVV1_0(w[1], w[8], w[211], pars->GC_10, amp[896]); 
  VVV1_0(w[219], w[38], w[5], pars->GC_10, amp[897]); 
  FFV1_0(w[3], w[36], w[219], pars->GC_11, amp[898]); 
  FFV1_0(w[202], w[145], w[5], pars->GC_11, amp[899]); 
  FFV1_0(w[202], w[36], w[1], pars->GC_11, amp[900]); 
  FFV1_0(w[3], w[145], w[204], pars->GC_11, amp[901]); 
  VVV1_0(w[204], w[1], w[38], pars->GC_10, amp[902]); 
  FFV1_0(w[3], w[34], w[223], pars->GC_11, amp[903]); 
  FFV1_0(w[3], w[34], w[224], pars->GC_11, amp[904]); 
  FFV1_0(w[3], w[34], w[225], pars->GC_11, amp[905]); 
  FFV1_0(w[39], w[34], w[219], pars->GC_11, amp[906]); 
  FFV1_0(w[39], w[214], w[1], pars->GC_11, amp[907]); 
  FFV1_0(w[210], w[34], w[1], pars->GC_11, amp[908]); 
  VVV1_0(w[219], w[47], w[4], pars->GC_10, amp[909]); 
  FFV1_0(w[3], w[45], w[219], pars->GC_11, amp[910]); 
  FFV1_0(w[202], w[146], w[4], pars->GC_11, amp[911]); 
  FFV1_0(w[202], w[45], w[1], pars->GC_11, amp[912]); 
  FFV1_0(w[3], w[146], w[203], pars->GC_11, amp[913]); 
  VVV1_0(w[203], w[1], w[47], pars->GC_10, amp[914]); 
  FFV1_0(w[3], w[44], w[220], pars->GC_11, amp[915]); 
  FFV1_0(w[3], w[44], w[221], pars->GC_11, amp[916]); 
  FFV1_0(w[3], w[44], w[222], pars->GC_11, amp[917]); 
  FFV1_0(w[48], w[44], w[219], pars->GC_11, amp[918]); 
  FFV1_0(w[48], w[215], w[1], pars->GC_11, amp[919]); 
  FFV1_0(w[209], w[44], w[1], pars->GC_11, amp[920]); 
  VVV1_0(w[219], w[59], w[5], pars->GC_10, amp[921]); 
  FFV1_0(w[57], w[2], w[219], pars->GC_11, amp[922]); 
  FFV1_0(w[107], w[212], w[5], pars->GC_11, amp[923]); 
  FFV1_0(w[57], w[212], w[1], pars->GC_11, amp[924]); 
  FFV1_0(w[107], w[2], w[204], pars->GC_11, amp[925]); 
  VVV1_0(w[204], w[1], w[59], pars->GC_10, amp[926]); 
  FFV1_0(w[48], w[2], w[223], pars->GC_11, amp[927]); 
  FFV1_0(w[48], w[2], w[224], pars->GC_11, amp[928]); 
  FFV1_0(w[48], w[2], w[225], pars->GC_11, amp[929]); 
  VVV1_0(w[219], w[62], w[4], pars->GC_10, amp[930]); 
  FFV1_0(w[60], w[2], w[219], pars->GC_11, amp[931]); 
  FFV1_0(w[108], w[212], w[4], pars->GC_11, amp[932]); 
  FFV1_0(w[60], w[212], w[1], pars->GC_11, amp[933]); 
  FFV1_0(w[108], w[2], w[203], pars->GC_11, amp[934]); 
  VVV1_0(w[203], w[1], w[62], pars->GC_10, amp[935]); 
  FFV1_0(w[39], w[2], w[220], pars->GC_11, amp[936]); 
  FFV1_0(w[39], w[2], w[221], pars->GC_11, amp[937]); 
  FFV1_0(w[39], w[2], w[222], pars->GC_11, amp[938]); 
  FFV1_0(w[3], w[68], w[219], pars->GC_11, amp[939]); 
  FFV1_0(w[66], w[2], w[219], pars->GC_11, amp[940]); 
  FFV1_0(w[3], w[212], w[110], pars->GC_11, amp[941]); 
  FFV1_0(w[66], w[212], w[1], pars->GC_11, amp[942]); 
  FFV1_0(w[202], w[2], w[110], pars->GC_11, amp[943]); 
  FFV1_0(w[202], w[68], w[1], pars->GC_11, amp[944]); 
  FFV1_0(w[3], w[212], w[113], pars->GC_11, amp[945]); 
  FFV1_0(w[3], w[212], w[114], pars->GC_11, amp[946]); 
  FFV1_0(w[3], w[212], w[115], pars->GC_11, amp[947]); 
  FFV1_0(w[202], w[2], w[113], pars->GC_11, amp[948]); 
  FFV1_0(w[202], w[2], w[114], pars->GC_11, amp[949]); 
  FFV1_0(w[202], w[2], w[115], pars->GC_11, amp[950]); 
  VVV1_0(w[201], w[113], w[8], pars->GC_10, amp[951]); 
  VVV1_0(w[201], w[114], w[8], pars->GC_10, amp[952]); 
  VVV1_0(w[201], w[115], w[8], pars->GC_10, amp[953]); 
  FFV1_0(w[57], w[226], w[6], pars->GC_11, amp[954]); 
  FFV1_0(w[58], w[226], w[5], pars->GC_11, amp[955]); 
  FFV1_0(w[227], w[125], w[6], pars->GC_11, amp[956]); 
  FFV1_0(w[227], w[126], w[5], pars->GC_11, amp[957]); 
  FFV1_0(w[58], w[125], w[0], pars->GC_11, amp[958]); 
  FFV1_0(w[57], w[126], w[0], pars->GC_11, amp[959]); 
  FFV1_0(w[48], w[226], w[29], pars->GC_11, amp[960]); 
  FFV1_0(w[227], w[123], w[29], pars->GC_11, amp[961]); 
  FFV1_0(w[48], w[123], w[228], pars->GC_11, amp[962]); 
  FFV1_0(w[60], w[226], w[6], pars->GC_11, amp[963]); 
  FFV1_0(w[61], w[226], w[4], pars->GC_11, amp[964]); 
  FFV1_0(w[229], w[128], w[6], pars->GC_11, amp[965]); 
  FFV1_0(w[229], w[126], w[4], pars->GC_11, amp[966]); 
  FFV1_0(w[61], w[128], w[0], pars->GC_11, amp[967]); 
  FFV1_0(w[60], w[126], w[0], pars->GC_11, amp[968]); 
  FFV1_0(w[39], w[226], w[27], pars->GC_11, amp[969]); 
  FFV1_0(w[229], w[123], w[27], pars->GC_11, amp[970]); 
  FFV1_0(w[39], w[123], w[230], pars->GC_11, amp[971]); 
  FFV1_0(w[63], w[226], w[5], pars->GC_11, amp[972]); 
  FFV1_0(w[64], w[226], w[4], pars->GC_11, amp[973]); 
  FFV1_0(w[231], w[128], w[5], pars->GC_11, amp[974]); 
  FFV1_0(w[231], w[125], w[4], pars->GC_11, amp[975]); 
  FFV1_0(w[64], w[128], w[0], pars->GC_11, amp[976]); 
  FFV1_0(w[63], w[125], w[0], pars->GC_11, amp[977]); 
  FFV1_0(w[42], w[226], w[24], pars->GC_11, amp[978]); 
  FFV1_0(w[231], w[123], w[24], pars->GC_11, amp[979]); 
  FFV1_0(w[42], w[123], w[232], pars->GC_11, amp[980]); 
  FFV1_0(w[66], w[226], w[6], pars->GC_11, amp[981]); 
  FFV1_0(w[3], w[226], w[67], pars->GC_11, amp[982]); 
  VVV1_0(w[232], w[151], w[6], pars->GC_10, amp[983]); 
  FFV1_0(w[3], w[126], w[232], pars->GC_11, amp[984]); 
  VVV1_0(w[0], w[151], w[67], pars->GC_10, amp[985]); 
  FFV1_0(w[66], w[126], w[0], pars->GC_11, amp[986]); 
  FFV1_0(w[3], w[123], w[233], pars->GC_11, amp[987]); 
  FFV1_0(w[3], w[123], w[234], pars->GC_11, amp[988]); 
  FFV1_0(w[3], w[123], w[235], pars->GC_11, amp[989]); 
  FFV1_0(w[69], w[226], w[5], pars->GC_11, amp[990]); 
  FFV1_0(w[3], w[226], w[70], pars->GC_11, amp[991]); 
  VVV1_0(w[230], w[151], w[5], pars->GC_10, amp[992]); 
  FFV1_0(w[3], w[125], w[230], pars->GC_11, amp[993]); 
  VVV1_0(w[0], w[151], w[70], pars->GC_10, amp[994]); 
  FFV1_0(w[69], w[125], w[0], pars->GC_11, amp[995]); 
  FFV1_0(w[3], w[123], w[236], pars->GC_11, amp[996]); 
  FFV1_0(w[3], w[123], w[237], pars->GC_11, amp[997]); 
  FFV1_0(w[3], w[123], w[238], pars->GC_11, amp[998]); 
  FFV1_0(w[72], w[226], w[4], pars->GC_11, amp[999]); 
  FFV1_0(w[3], w[226], w[73], pars->GC_11, amp[1000]); 
  VVV1_0(w[228], w[151], w[4], pars->GC_10, amp[1001]); 
  FFV1_0(w[3], w[128], w[228], pars->GC_11, amp[1002]); 
  VVV1_0(w[0], w[151], w[73], pars->GC_10, amp[1003]); 
  FFV1_0(w[72], w[128], w[0], pars->GC_11, amp[1004]); 
  FFV1_0(w[3], w[123], w[239], pars->GC_11, amp[1005]); 
  FFV1_0(w[3], w[123], w[240], pars->GC_11, amp[1006]); 
  FFV1_0(w[3], w[123], w[241], pars->GC_11, amp[1007]); 
  FFV1_0(w[3], w[226], w[31], pars->GC_11, amp[1008]); 
  FFV1_0(w[3], w[226], w[32], pars->GC_11, amp[1009]); 
  FFV1_0(w[3], w[226], w[33], pars->GC_11, amp[1010]); 
  FFV1_0(w[3], w[123], w[242], pars->GC_11, amp[1011]); 
  FFV1_0(w[3], w[123], w[243], pars->GC_11, amp[1012]); 
  FFV1_0(w[3], w[123], w[244], pars->GC_11, amp[1013]); 
  FFV1_0(w[245], w[36], w[6], pars->GC_11, amp[1014]); 
  FFV1_0(w[245], w[37], w[5], pars->GC_11, amp[1015]); 
  FFV1_0(w[78], w[246], w[6], pars->GC_11, amp[1016]); 
  FFV1_0(w[79], w[246], w[5], pars->GC_11, amp[1017]); 
  FFV1_0(w[78], w[37], w[0], pars->GC_11, amp[1018]); 
  FFV1_0(w[79], w[36], w[0], pars->GC_11, amp[1019]); 
  FFV1_0(w[245], w[34], w[29], pars->GC_11, amp[1020]); 
  FFV1_0(w[76], w[246], w[29], pars->GC_11, amp[1021]); 
  FFV1_0(w[76], w[34], w[228], pars->GC_11, amp[1022]); 
  FFV1_0(w[245], w[45], w[6], pars->GC_11, amp[1023]); 
  FFV1_0(w[245], w[46], w[4], pars->GC_11, amp[1024]); 
  FFV1_0(w[81], w[247], w[6], pars->GC_11, amp[1025]); 
  FFV1_0(w[79], w[247], w[4], pars->GC_11, amp[1026]); 
  FFV1_0(w[81], w[46], w[0], pars->GC_11, amp[1027]); 
  FFV1_0(w[79], w[45], w[0], pars->GC_11, amp[1028]); 
  FFV1_0(w[245], w[44], w[27], pars->GC_11, amp[1029]); 
  FFV1_0(w[76], w[247], w[27], pars->GC_11, amp[1030]); 
  FFV1_0(w[76], w[44], w[230], pars->GC_11, amp[1031]); 
  FFV1_0(w[245], w[52], w[5], pars->GC_11, amp[1032]); 
  FFV1_0(w[245], w[53], w[4], pars->GC_11, amp[1033]); 
  FFV1_0(w[81], w[248], w[5], pars->GC_11, amp[1034]); 
  FFV1_0(w[78], w[248], w[4], pars->GC_11, amp[1035]); 
  FFV1_0(w[81], w[53], w[0], pars->GC_11, amp[1036]); 
  FFV1_0(w[78], w[52], w[0], pars->GC_11, amp[1037]); 
  FFV1_0(w[245], w[51], w[24], pars->GC_11, amp[1038]); 
  FFV1_0(w[76], w[248], w[24], pars->GC_11, amp[1039]); 
  FFV1_0(w[76], w[51], w[232], pars->GC_11, amp[1040]); 
  FFV1_0(w[245], w[68], w[6], pars->GC_11, amp[1041]); 
  FFV1_0(w[245], w[2], w[67], pars->GC_11, amp[1042]); 
  VVV1_0(w[232], w[161], w[6], pars->GC_10, amp[1043]); 
  FFV1_0(w[79], w[2], w[232], pars->GC_11, amp[1044]); 
  VVV1_0(w[0], w[161], w[67], pars->GC_10, amp[1045]); 
  FFV1_0(w[79], w[68], w[0], pars->GC_11, amp[1046]); 
  FFV1_0(w[76], w[2], w[233], pars->GC_11, amp[1047]); 
  FFV1_0(w[76], w[2], w[234], pars->GC_11, amp[1048]); 
  FFV1_0(w[76], w[2], w[235], pars->GC_11, amp[1049]); 
  FFV1_0(w[245], w[71], w[5], pars->GC_11, amp[1050]); 
  FFV1_0(w[245], w[2], w[70], pars->GC_11, amp[1051]); 
  VVV1_0(w[230], w[161], w[5], pars->GC_10, amp[1052]); 
  FFV1_0(w[78], w[2], w[230], pars->GC_11, amp[1053]); 
  VVV1_0(w[0], w[161], w[70], pars->GC_10, amp[1054]); 
  FFV1_0(w[78], w[71], w[0], pars->GC_11, amp[1055]); 
  FFV1_0(w[76], w[2], w[236], pars->GC_11, amp[1056]); 
  FFV1_0(w[76], w[2], w[237], pars->GC_11, amp[1057]); 
  FFV1_0(w[76], w[2], w[238], pars->GC_11, amp[1058]); 
  FFV1_0(w[245], w[74], w[4], pars->GC_11, amp[1059]); 
  FFV1_0(w[245], w[2], w[73], pars->GC_11, amp[1060]); 
  VVV1_0(w[228], w[161], w[4], pars->GC_10, amp[1061]); 
  FFV1_0(w[81], w[2], w[228], pars->GC_11, amp[1062]); 
  VVV1_0(w[0], w[161], w[73], pars->GC_10, amp[1063]); 
  FFV1_0(w[81], w[74], w[0], pars->GC_11, amp[1064]); 
  FFV1_0(w[76], w[2], w[239], pars->GC_11, amp[1065]); 
  FFV1_0(w[76], w[2], w[240], pars->GC_11, amp[1066]); 
  FFV1_0(w[76], w[2], w[241], pars->GC_11, amp[1067]); 
  FFV1_0(w[245], w[2], w[31], pars->GC_11, amp[1068]); 
  FFV1_0(w[245], w[2], w[32], pars->GC_11, amp[1069]); 
  FFV1_0(w[245], w[2], w[33], pars->GC_11, amp[1070]); 
  FFV1_0(w[76], w[2], w[242], pars->GC_11, amp[1071]); 
  FFV1_0(w[76], w[2], w[243], pars->GC_11, amp[1072]); 
  FFV1_0(w[76], w[2], w[244], pars->GC_11, amp[1073]); 
  VVV1_0(w[249], w[10], w[6], pars->GC_10, amp[1074]); 
  VVV1_0(w[249], w[11], w[5], pars->GC_10, amp[1075]); 
  VVVV1_0(w[8], w[5], w[6], w[249], pars->GC_12, amp[1076]); 
  VVVV3_0(w[8], w[5], w[6], w[249], pars->GC_12, amp[1077]); 
  VVVV4_0(w[8], w[5], w[6], w[249], pars->GC_12, amp[1078]); 
  VVV1_0(w[250], w[89], w[6], pars->GC_10, amp[1079]); 
  VVV1_0(w[250], w[90], w[5], pars->GC_10, amp[1080]); 
  VVVV1_0(w[87], w[5], w[6], w[250], pars->GC_12, amp[1081]); 
  VVVV3_0(w[87], w[5], w[6], w[250], pars->GC_12, amp[1082]); 
  VVVV4_0(w[87], w[5], w[6], w[250], pars->GC_12, amp[1083]); 
  VVV1_0(w[0], w[89], w[11], pars->GC_10, amp[1084]); 
  VVV1_0(w[0], w[90], w[10], pars->GC_10, amp[1085]); 
  VVV1_0(w[8], w[6], w[251], pars->GC_10, amp[1086]); 
  VVV1_0(w[8], w[6], w[252], pars->GC_10, amp[1087]); 
  VVV1_0(w[8], w[6], w[253], pars->GC_10, amp[1088]); 
  VVV1_0(w[8], w[5], w[254], pars->GC_10, amp[1089]); 
  VVV1_0(w[8], w[5], w[255], pars->GC_10, amp[1090]); 
  VVV1_0(w[8], w[5], w[256], pars->GC_10, amp[1091]); 
  VVV1_0(w[87], w[6], w[257], pars->GC_10, amp[1092]); 
  VVV1_0(w[87], w[6], w[258], pars->GC_10, amp[1093]); 
  VVV1_0(w[87], w[6], w[259], pars->GC_10, amp[1094]); 
  VVV1_0(w[87], w[5], w[260], pars->GC_10, amp[1095]); 
  VVV1_0(w[87], w[5], w[261], pars->GC_10, amp[1096]); 
  VVV1_0(w[87], w[5], w[262], pars->GC_10, amp[1097]); 
  VVVV1_0(w[0], w[87], w[8], w[29], pars->GC_12, amp[1098]); 
  VVVV3_0(w[0], w[87], w[8], w[29], pars->GC_12, amp[1099]); 
  VVVV4_0(w[0], w[87], w[8], w[29], pars->GC_12, amp[1100]); 
  VVV1_0(w[8], w[29], w[249], pars->GC_10, amp[1101]); 
  VVV1_0(w[87], w[29], w[250], pars->GC_10, amp[1102]); 
  VVV1_0(w[87], w[8], w[228], pars->GC_10, amp[1103]); 
  VVV1_0(w[249], w[47], w[6], pars->GC_10, amp[1104]); 
  FFV1_0(w[3], w[46], w[249], pars->GC_11, amp[1105]); 
  FFV1_0(w[91], w[247], w[6], pars->GC_11, amp[1106]); 
  FFV1_0(w[3], w[247], w[90], pars->GC_11, amp[1107]); 
  FFV1_0(w[91], w[46], w[0], pars->GC_11, amp[1108]); 
  VVV1_0(w[0], w[90], w[47], pars->GC_10, amp[1109]); 
  FFV1_0(w[3], w[44], w[254], pars->GC_11, amp[1110]); 
  FFV1_0(w[3], w[44], w[255], pars->GC_11, amp[1111]); 
  FFV1_0(w[3], w[44], w[256], pars->GC_11, amp[1112]); 
  FFV1_0(w[42], w[44], w[249], pars->GC_11, amp[1113]); 
  FFV1_0(w[42], w[247], w[87], pars->GC_11, amp[1114]); 
  FFV1_0(w[231], w[44], w[87], pars->GC_11, amp[1115]); 
  VVV1_0(w[249], w[54], w[5], pars->GC_10, amp[1116]); 
  FFV1_0(w[3], w[53], w[249], pars->GC_11, amp[1117]); 
  FFV1_0(w[91], w[248], w[5], pars->GC_11, amp[1118]); 
  FFV1_0(w[3], w[248], w[89], pars->GC_11, amp[1119]); 
  FFV1_0(w[91], w[53], w[0], pars->GC_11, amp[1120]); 
  VVV1_0(w[0], w[89], w[54], pars->GC_10, amp[1121]); 
  FFV1_0(w[3], w[51], w[251], pars->GC_11, amp[1122]); 
  FFV1_0(w[3], w[51], w[252], pars->GC_11, amp[1123]); 
  FFV1_0(w[3], w[51], w[253], pars->GC_11, amp[1124]); 
  FFV1_0(w[39], w[51], w[249], pars->GC_11, amp[1125]); 
  FFV1_0(w[39], w[248], w[87], pars->GC_11, amp[1126]); 
  FFV1_0(w[229], w[51], w[87], pars->GC_11, amp[1127]); 
  VVV1_0(w[249], w[62], w[6], pars->GC_10, amp[1128]); 
  FFV1_0(w[61], w[2], w[249], pars->GC_11, amp[1129]); 
  FFV1_0(w[229], w[135], w[6], pars->GC_11, amp[1130]); 
  FFV1_0(w[229], w[2], w[90], pars->GC_11, amp[1131]); 
  FFV1_0(w[61], w[135], w[0], pars->GC_11, amp[1132]); 
  VVV1_0(w[0], w[90], w[62], pars->GC_10, amp[1133]); 
  FFV1_0(w[39], w[2], w[254], pars->GC_11, amp[1134]); 
  FFV1_0(w[39], w[2], w[255], pars->GC_11, amp[1135]); 
  FFV1_0(w[39], w[2], w[256], pars->GC_11, amp[1136]); 
  VVV1_0(w[249], w[65], w[5], pars->GC_10, amp[1137]); 
  FFV1_0(w[64], w[2], w[249], pars->GC_11, amp[1138]); 
  FFV1_0(w[231], w[135], w[5], pars->GC_11, amp[1139]); 
  FFV1_0(w[231], w[2], w[89], pars->GC_11, amp[1140]); 
  FFV1_0(w[64], w[135], w[0], pars->GC_11, amp[1141]); 
  VVV1_0(w[0], w[89], w[65], pars->GC_10, amp[1142]); 
  FFV1_0(w[42], w[2], w[251], pars->GC_11, amp[1143]); 
  FFV1_0(w[42], w[2], w[252], pars->GC_11, amp[1144]); 
  FFV1_0(w[42], w[2], w[253], pars->GC_11, amp[1145]); 
  FFV1_0(w[3], w[74], w[249], pars->GC_11, amp[1146]); 
  FFV1_0(w[72], w[2], w[249], pars->GC_11, amp[1147]); 
  FFV1_0(w[3], w[135], w[228], pars->GC_11, amp[1148]); 
  FFV1_0(w[91], w[2], w[228], pars->GC_11, amp[1149]); 
  FFV1_0(w[72], w[135], w[0], pars->GC_11, amp[1150]); 
  FFV1_0(w[91], w[74], w[0], pars->GC_11, amp[1151]); 
  VVV1_0(w[263], w[13], w[6], pars->GC_10, amp[1152]); 
  VVV1_0(w[263], w[11], w[4], pars->GC_10, amp[1153]); 
  VVVV1_0(w[8], w[4], w[6], w[263], pars->GC_12, amp[1154]); 
  VVVV3_0(w[8], w[4], w[6], w[263], pars->GC_12, amp[1155]); 
  VVVV4_0(w[8], w[4], w[6], w[263], pars->GC_12, amp[1156]); 
  VVV1_0(w[250], w[96], w[6], pars->GC_10, amp[1157]); 
  VVV1_0(w[250], w[97], w[4], pars->GC_10, amp[1158]); 
  VVVV1_0(w[95], w[4], w[6], w[250], pars->GC_12, amp[1159]); 
  VVVV3_0(w[95], w[4], w[6], w[250], pars->GC_12, amp[1160]); 
  VVVV4_0(w[95], w[4], w[6], w[250], pars->GC_12, amp[1161]); 
  VVV1_0(w[0], w[96], w[11], pars->GC_10, amp[1162]); 
  VVV1_0(w[0], w[97], w[13], pars->GC_10, amp[1163]); 
  VVV1_0(w[8], w[6], w[264], pars->GC_10, amp[1164]); 
  VVV1_0(w[8], w[6], w[265], pars->GC_10, amp[1165]); 
  VVV1_0(w[8], w[6], w[266], pars->GC_10, amp[1166]); 
  VVV1_0(w[8], w[4], w[267], pars->GC_10, amp[1167]); 
  VVV1_0(w[8], w[4], w[268], pars->GC_10, amp[1168]); 
  VVV1_0(w[8], w[4], w[269], pars->GC_10, amp[1169]); 
  VVV1_0(w[95], w[6], w[270], pars->GC_10, amp[1170]); 
  VVV1_0(w[95], w[6], w[271], pars->GC_10, amp[1171]); 
  VVV1_0(w[95], w[6], w[272], pars->GC_10, amp[1172]); 
  VVV1_0(w[95], w[4], w[260], pars->GC_10, amp[1173]); 
  VVV1_0(w[95], w[4], w[261], pars->GC_10, amp[1174]); 
  VVV1_0(w[95], w[4], w[262], pars->GC_10, amp[1175]); 
  VVVV1_0(w[0], w[95], w[8], w[27], pars->GC_12, amp[1176]); 
  VVVV3_0(w[0], w[95], w[8], w[27], pars->GC_12, amp[1177]); 
  VVVV4_0(w[0], w[95], w[8], w[27], pars->GC_12, amp[1178]); 
  VVV1_0(w[8], w[27], w[263], pars->GC_10, amp[1179]); 
  VVV1_0(w[95], w[27], w[250], pars->GC_10, amp[1180]); 
  VVV1_0(w[95], w[8], w[230], pars->GC_10, amp[1181]); 
  VVV1_0(w[263], w[38], w[6], pars->GC_10, amp[1182]); 
  FFV1_0(w[3], w[37], w[263], pars->GC_11, amp[1183]); 
  FFV1_0(w[98], w[246], w[6], pars->GC_11, amp[1184]); 
  FFV1_0(w[3], w[246], w[97], pars->GC_11, amp[1185]); 
  FFV1_0(w[98], w[37], w[0], pars->GC_11, amp[1186]); 
  VVV1_0(w[0], w[97], w[38], pars->GC_10, amp[1187]); 
  FFV1_0(w[3], w[34], w[267], pars->GC_11, amp[1188]); 
  FFV1_0(w[3], w[34], w[268], pars->GC_11, amp[1189]); 
  FFV1_0(w[3], w[34], w[269], pars->GC_11, amp[1190]); 
  FFV1_0(w[42], w[34], w[263], pars->GC_11, amp[1191]); 
  FFV1_0(w[42], w[246], w[95], pars->GC_11, amp[1192]); 
  FFV1_0(w[231], w[34], w[95], pars->GC_11, amp[1193]); 
  VVV1_0(w[263], w[54], w[4], pars->GC_10, amp[1194]); 
  FFV1_0(w[3], w[52], w[263], pars->GC_11, amp[1195]); 
  FFV1_0(w[98], w[248], w[4], pars->GC_11, amp[1196]); 
  FFV1_0(w[3], w[248], w[96], pars->GC_11, amp[1197]); 
  FFV1_0(w[98], w[52], w[0], pars->GC_11, amp[1198]); 
  VVV1_0(w[0], w[96], w[54], pars->GC_10, amp[1199]); 
  FFV1_0(w[3], w[51], w[264], pars->GC_11, amp[1200]); 
  FFV1_0(w[3], w[51], w[265], pars->GC_11, amp[1201]); 
  FFV1_0(w[3], w[51], w[266], pars->GC_11, amp[1202]); 
  FFV1_0(w[48], w[51], w[263], pars->GC_11, amp[1203]); 
  FFV1_0(w[48], w[248], w[95], pars->GC_11, amp[1204]); 
  FFV1_0(w[227], w[51], w[95], pars->GC_11, amp[1205]); 
  VVV1_0(w[263], w[59], w[6], pars->GC_10, amp[1206]); 
  FFV1_0(w[58], w[2], w[263], pars->GC_11, amp[1207]); 
  FFV1_0(w[227], w[139], w[6], pars->GC_11, amp[1208]); 
  FFV1_0(w[227], w[2], w[97], pars->GC_11, amp[1209]); 
  FFV1_0(w[58], w[139], w[0], pars->GC_11, amp[1210]); 
  VVV1_0(w[0], w[97], w[59], pars->GC_10, amp[1211]); 
  FFV1_0(w[48], w[2], w[267], pars->GC_11, amp[1212]); 
  FFV1_0(w[48], w[2], w[268], pars->GC_11, amp[1213]); 
  FFV1_0(w[48], w[2], w[269], pars->GC_11, amp[1214]); 
  VVV1_0(w[263], w[65], w[4], pars->GC_10, amp[1215]); 
  FFV1_0(w[63], w[2], w[263], pars->GC_11, amp[1216]); 
  FFV1_0(w[231], w[139], w[4], pars->GC_11, amp[1217]); 
  FFV1_0(w[231], w[2], w[96], pars->GC_11, amp[1218]); 
  FFV1_0(w[63], w[139], w[0], pars->GC_11, amp[1219]); 
  VVV1_0(w[0], w[96], w[65], pars->GC_10, amp[1220]); 
  FFV1_0(w[42], w[2], w[264], pars->GC_11, amp[1221]); 
  FFV1_0(w[42], w[2], w[265], pars->GC_11, amp[1222]); 
  FFV1_0(w[42], w[2], w[266], pars->GC_11, amp[1223]); 
  FFV1_0(w[3], w[71], w[263], pars->GC_11, amp[1224]); 
  FFV1_0(w[69], w[2], w[263], pars->GC_11, amp[1225]); 
  FFV1_0(w[3], w[139], w[230], pars->GC_11, amp[1226]); 
  FFV1_0(w[98], w[2], w[230], pars->GC_11, amp[1227]); 
  FFV1_0(w[69], w[139], w[0], pars->GC_11, amp[1228]); 
  FFV1_0(w[98], w[71], w[0], pars->GC_11, amp[1229]); 
  VVV1_0(w[273], w[13], w[5], pars->GC_10, amp[1230]); 
  VVV1_0(w[273], w[10], w[4], pars->GC_10, amp[1231]); 
  VVVV1_0(w[8], w[4], w[5], w[273], pars->GC_12, amp[1232]); 
  VVVV3_0(w[8], w[4], w[5], w[273], pars->GC_12, amp[1233]); 
  VVVV4_0(w[8], w[4], w[5], w[273], pars->GC_12, amp[1234]); 
  VVV1_0(w[250], w[102], w[5], pars->GC_10, amp[1235]); 
  VVV1_0(w[250], w[103], w[4], pars->GC_10, amp[1236]); 
  VVVV1_0(w[101], w[4], w[5], w[250], pars->GC_12, amp[1237]); 
  VVVV3_0(w[101], w[4], w[5], w[250], pars->GC_12, amp[1238]); 
  VVVV4_0(w[101], w[4], w[5], w[250], pars->GC_12, amp[1239]); 
  VVV1_0(w[0], w[102], w[10], pars->GC_10, amp[1240]); 
  VVV1_0(w[0], w[103], w[13], pars->GC_10, amp[1241]); 
  VVV1_0(w[8], w[5], w[274], pars->GC_10, amp[1242]); 
  VVV1_0(w[8], w[5], w[275], pars->GC_10, amp[1243]); 
  VVV1_0(w[8], w[5], w[276], pars->GC_10, amp[1244]); 
  VVV1_0(w[8], w[4], w[277], pars->GC_10, amp[1245]); 
  VVV1_0(w[8], w[4], w[278], pars->GC_10, amp[1246]); 
  VVV1_0(w[8], w[4], w[279], pars->GC_10, amp[1247]); 
  VVV1_0(w[101], w[5], w[270], pars->GC_10, amp[1248]); 
  VVV1_0(w[101], w[5], w[271], pars->GC_10, amp[1249]); 
  VVV1_0(w[101], w[5], w[272], pars->GC_10, amp[1250]); 
  VVV1_0(w[101], w[4], w[257], pars->GC_10, amp[1251]); 
  VVV1_0(w[101], w[4], w[258], pars->GC_10, amp[1252]); 
  VVV1_0(w[101], w[4], w[259], pars->GC_10, amp[1253]); 
  VVVV1_0(w[0], w[101], w[8], w[24], pars->GC_12, amp[1254]); 
  VVVV3_0(w[0], w[101], w[8], w[24], pars->GC_12, amp[1255]); 
  VVVV4_0(w[0], w[101], w[8], w[24], pars->GC_12, amp[1256]); 
  VVV1_0(w[8], w[24], w[273], pars->GC_10, amp[1257]); 
  VVV1_0(w[101], w[24], w[250], pars->GC_10, amp[1258]); 
  VVV1_0(w[101], w[8], w[232], pars->GC_10, amp[1259]); 
  VVV1_0(w[273], w[38], w[5], pars->GC_10, amp[1260]); 
  FFV1_0(w[3], w[36], w[273], pars->GC_11, amp[1261]); 
  FFV1_0(w[104], w[246], w[5], pars->GC_11, amp[1262]); 
  FFV1_0(w[3], w[246], w[103], pars->GC_11, amp[1263]); 
  FFV1_0(w[104], w[36], w[0], pars->GC_11, amp[1264]); 
  VVV1_0(w[0], w[103], w[38], pars->GC_10, amp[1265]); 
  FFV1_0(w[3], w[34], w[277], pars->GC_11, amp[1266]); 
  FFV1_0(w[3], w[34], w[278], pars->GC_11, amp[1267]); 
  FFV1_0(w[3], w[34], w[279], pars->GC_11, amp[1268]); 
  FFV1_0(w[39], w[34], w[273], pars->GC_11, amp[1269]); 
  FFV1_0(w[39], w[246], w[101], pars->GC_11, amp[1270]); 
  FFV1_0(w[229], w[34], w[101], pars->GC_11, amp[1271]); 
  VVV1_0(w[273], w[47], w[4], pars->GC_10, amp[1272]); 
  FFV1_0(w[3], w[45], w[273], pars->GC_11, amp[1273]); 
  FFV1_0(w[104], w[247], w[4], pars->GC_11, amp[1274]); 
  FFV1_0(w[3], w[247], w[102], pars->GC_11, amp[1275]); 
  FFV1_0(w[104], w[45], w[0], pars->GC_11, amp[1276]); 
  VVV1_0(w[0], w[102], w[47], pars->GC_10, amp[1277]); 
  FFV1_0(w[3], w[44], w[274], pars->GC_11, amp[1278]); 
  FFV1_0(w[3], w[44], w[275], pars->GC_11, amp[1279]); 
  FFV1_0(w[3], w[44], w[276], pars->GC_11, amp[1280]); 
  FFV1_0(w[48], w[44], w[273], pars->GC_11, amp[1281]); 
  FFV1_0(w[48], w[247], w[101], pars->GC_11, amp[1282]); 
  FFV1_0(w[227], w[44], w[101], pars->GC_11, amp[1283]); 
  VVV1_0(w[273], w[59], w[5], pars->GC_10, amp[1284]); 
  FFV1_0(w[57], w[2], w[273], pars->GC_11, amp[1285]); 
  FFV1_0(w[227], w[142], w[5], pars->GC_11, amp[1286]); 
  FFV1_0(w[227], w[2], w[103], pars->GC_11, amp[1287]); 
  FFV1_0(w[57], w[142], w[0], pars->GC_11, amp[1288]); 
  VVV1_0(w[0], w[103], w[59], pars->GC_10, amp[1289]); 
  FFV1_0(w[48], w[2], w[277], pars->GC_11, amp[1290]); 
  FFV1_0(w[48], w[2], w[278], pars->GC_11, amp[1291]); 
  FFV1_0(w[48], w[2], w[279], pars->GC_11, amp[1292]); 
  VVV1_0(w[273], w[62], w[4], pars->GC_10, amp[1293]); 
  FFV1_0(w[60], w[2], w[273], pars->GC_11, amp[1294]); 
  FFV1_0(w[229], w[142], w[4], pars->GC_11, amp[1295]); 
  FFV1_0(w[229], w[2], w[102], pars->GC_11, amp[1296]); 
  FFV1_0(w[60], w[142], w[0], pars->GC_11, amp[1297]); 
  VVV1_0(w[0], w[102], w[62], pars->GC_10, amp[1298]); 
  FFV1_0(w[39], w[2], w[274], pars->GC_11, amp[1299]); 
  FFV1_0(w[39], w[2], w[275], pars->GC_11, amp[1300]); 
  FFV1_0(w[39], w[2], w[276], pars->GC_11, amp[1301]); 
  FFV1_0(w[3], w[68], w[273], pars->GC_11, amp[1302]); 
  FFV1_0(w[66], w[2], w[273], pars->GC_11, amp[1303]); 
  FFV1_0(w[3], w[142], w[232], pars->GC_11, amp[1304]); 
  FFV1_0(w[104], w[2], w[232], pars->GC_11, amp[1305]); 
  FFV1_0(w[66], w[142], w[0], pars->GC_11, amp[1306]); 
  FFV1_0(w[104], w[68], w[0], pars->GC_11, amp[1307]); 
  VVV1_0(w[250], w[110], w[6], pars->GC_10, amp[1308]); 
  VVV1_0(w[250], w[1], w[67], pars->GC_10, amp[1309]); 
  VVVV1_0(w[1], w[24], w[6], w[250], pars->GC_12, amp[1310]); 
  VVVV3_0(w[1], w[24], w[6], w[250], pars->GC_12, amp[1311]); 
  VVVV4_0(w[1], w[24], w[6], w[250], pars->GC_12, amp[1312]); 
  VVV1_0(w[232], w[169], w[6], pars->GC_10, amp[1313]); 
  VVV1_0(w[232], w[1], w[11], pars->GC_10, amp[1314]); 
  VVVV1_0(w[1], w[8], w[6], w[232], pars->GC_12, amp[1315]); 
  VVVV3_0(w[1], w[8], w[6], w[232], pars->GC_12, amp[1316]); 
  VVVV4_0(w[1], w[8], w[6], w[232], pars->GC_12, amp[1317]); 
  VVV1_0(w[0], w[169], w[67], pars->GC_10, amp[1318]); 
  VVV1_0(w[0], w[110], w[11], pars->GC_10, amp[1319]); 
  VVV1_0(w[24], w[6], w[280], pars->GC_10, amp[1320]); 
  VVV1_0(w[24], w[6], w[281], pars->GC_10, amp[1321]); 
  VVV1_0(w[24], w[6], w[282], pars->GC_10, amp[1322]); 
  VVV1_0(w[8], w[6], w[283], pars->GC_10, amp[1323]); 
  VVV1_0(w[8], w[6], w[284], pars->GC_10, amp[1324]); 
  VVV1_0(w[8], w[6], w[285], pars->GC_10, amp[1325]); 
  VVV1_0(w[1], w[24], w[260], pars->GC_10, amp[1326]); 
  VVV1_0(w[1], w[24], w[261], pars->GC_10, amp[1327]); 
  VVV1_0(w[1], w[24], w[262], pars->GC_10, amp[1328]); 
  VVV1_0(w[1], w[8], w[233], pars->GC_10, amp[1329]); 
  VVV1_0(w[1], w[8], w[234], pars->GC_10, amp[1330]); 
  VVV1_0(w[1], w[8], w[235], pars->GC_10, amp[1331]); 
  VVV1_0(w[250], w[111], w[5], pars->GC_10, amp[1332]); 
  VVV1_0(w[250], w[1], w[70], pars->GC_10, amp[1333]); 
  VVVV1_0(w[1], w[27], w[5], w[250], pars->GC_12, amp[1334]); 
  VVVV3_0(w[1], w[27], w[5], w[250], pars->GC_12, amp[1335]); 
  VVVV4_0(w[1], w[27], w[5], w[250], pars->GC_12, amp[1336]); 
  VVV1_0(w[230], w[169], w[5], pars->GC_10, amp[1337]); 
  VVV1_0(w[230], w[1], w[10], pars->GC_10, amp[1338]); 
  VVVV1_0(w[1], w[8], w[5], w[230], pars->GC_12, amp[1339]); 
  VVVV3_0(w[1], w[8], w[5], w[230], pars->GC_12, amp[1340]); 
  VVVV4_0(w[1], w[8], w[5], w[230], pars->GC_12, amp[1341]); 
  VVV1_0(w[0], w[169], w[70], pars->GC_10, amp[1342]); 
  VVV1_0(w[0], w[111], w[10], pars->GC_10, amp[1343]); 
  VVV1_0(w[27], w[5], w[280], pars->GC_10, amp[1344]); 
  VVV1_0(w[27], w[5], w[281], pars->GC_10, amp[1345]); 
  VVV1_0(w[27], w[5], w[282], pars->GC_10, amp[1346]); 
  VVV1_0(w[8], w[5], w[286], pars->GC_10, amp[1347]); 
  VVV1_0(w[8], w[5], w[287], pars->GC_10, amp[1348]); 
  VVV1_0(w[8], w[5], w[288], pars->GC_10, amp[1349]); 
  VVV1_0(w[1], w[27], w[257], pars->GC_10, amp[1350]); 
  VVV1_0(w[1], w[27], w[258], pars->GC_10, amp[1351]); 
  VVV1_0(w[1], w[27], w[259], pars->GC_10, amp[1352]); 
  VVV1_0(w[1], w[8], w[236], pars->GC_10, amp[1353]); 
  VVV1_0(w[1], w[8], w[237], pars->GC_10, amp[1354]); 
  VVV1_0(w[1], w[8], w[238], pars->GC_10, amp[1355]); 
  VVV1_0(w[250], w[112], w[4], pars->GC_10, amp[1356]); 
  VVV1_0(w[250], w[1], w[73], pars->GC_10, amp[1357]); 
  VVVV1_0(w[1], w[4], w[29], w[250], pars->GC_12, amp[1358]); 
  VVVV3_0(w[1], w[4], w[29], w[250], pars->GC_12, amp[1359]); 
  VVVV4_0(w[1], w[4], w[29], w[250], pars->GC_12, amp[1360]); 
  VVV1_0(w[228], w[169], w[4], pars->GC_10, amp[1361]); 
  VVV1_0(w[228], w[1], w[13], pars->GC_10, amp[1362]); 
  VVVV1_0(w[1], w[8], w[4], w[228], pars->GC_12, amp[1363]); 
  VVVV3_0(w[1], w[8], w[4], w[228], pars->GC_12, amp[1364]); 
  VVVV4_0(w[1], w[8], w[4], w[228], pars->GC_12, amp[1365]); 
  VVV1_0(w[0], w[169], w[73], pars->GC_10, amp[1366]); 
  VVV1_0(w[0], w[112], w[13], pars->GC_10, amp[1367]); 
  VVV1_0(w[4], w[29], w[280], pars->GC_10, amp[1368]); 
  VVV1_0(w[4], w[29], w[281], pars->GC_10, amp[1369]); 
  VVV1_0(w[4], w[29], w[282], pars->GC_10, amp[1370]); 
  VVV1_0(w[8], w[4], w[289], pars->GC_10, amp[1371]); 
  VVV1_0(w[8], w[4], w[290], pars->GC_10, amp[1372]); 
  VVV1_0(w[8], w[4], w[291], pars->GC_10, amp[1373]); 
  VVV1_0(w[1], w[29], w[270], pars->GC_10, amp[1374]); 
  VVV1_0(w[1], w[29], w[271], pars->GC_10, amp[1375]); 
  VVV1_0(w[1], w[29], w[272], pars->GC_10, amp[1376]); 
  VVV1_0(w[1], w[8], w[239], pars->GC_10, amp[1377]); 
  VVV1_0(w[1], w[8], w[240], pars->GC_10, amp[1378]); 
  VVV1_0(w[1], w[8], w[241], pars->GC_10, amp[1379]); 
  VVVV1_0(w[0], w[1], w[8], w[31], pars->GC_12, amp[1380]); 
  VVVV3_0(w[0], w[1], w[8], w[31], pars->GC_12, amp[1381]); 
  VVVV4_0(w[0], w[1], w[8], w[31], pars->GC_12, amp[1382]); 
  VVVV1_0(w[0], w[1], w[8], w[32], pars->GC_12, amp[1383]); 
  VVVV3_0(w[0], w[1], w[8], w[32], pars->GC_12, amp[1384]); 
  VVVV4_0(w[0], w[1], w[8], w[32], pars->GC_12, amp[1385]); 
  VVVV1_0(w[0], w[1], w[8], w[33], pars->GC_12, amp[1386]); 
  VVVV3_0(w[0], w[1], w[8], w[33], pars->GC_12, amp[1387]); 
  VVVV4_0(w[0], w[1], w[8], w[33], pars->GC_12, amp[1388]); 
  VVV1_0(w[1], w[31], w[250], pars->GC_10, amp[1389]); 
  VVV1_0(w[1], w[32], w[250], pars->GC_10, amp[1390]); 
  VVV1_0(w[1], w[33], w[250], pars->GC_10, amp[1391]); 
  VVV1_0(w[1], w[8], w[242], pars->GC_10, amp[1392]); 
  VVV1_0(w[1], w[8], w[243], pars->GC_10, amp[1393]); 
  VVV1_0(w[1], w[8], w[244], pars->GC_10, amp[1394]); 
  FFV1_0(w[108], w[246], w[6], pars->GC_11, amp[1395]); 
  FFV1_0(w[61], w[246], w[1], pars->GC_11, amp[1396]); 
  FFV1_0(w[229], w[145], w[6], pars->GC_11, amp[1397]); 
  FFV1_0(w[229], w[37], w[1], pars->GC_11, amp[1398]); 
  FFV1_0(w[61], w[145], w[0], pars->GC_11, amp[1399]); 
  FFV1_0(w[108], w[37], w[0], pars->GC_11, amp[1400]); 
  FFV1_0(w[109], w[246], w[5], pars->GC_11, amp[1401]); 
  FFV1_0(w[64], w[246], w[1], pars->GC_11, amp[1402]); 
  FFV1_0(w[231], w[145], w[5], pars->GC_11, amp[1403]); 
  FFV1_0(w[231], w[36], w[1], pars->GC_11, amp[1404]); 
  FFV1_0(w[64], w[145], w[0], pars->GC_11, amp[1405]); 
  FFV1_0(w[109], w[36], w[0], pars->GC_11, amp[1406]); 
  FFV1_0(w[3], w[246], w[112], pars->GC_11, amp[1407]); 
  FFV1_0(w[72], w[246], w[1], pars->GC_11, amp[1408]); 
  FFV1_0(w[3], w[145], w[228], pars->GC_11, amp[1409]); 
  VVV1_0(w[228], w[1], w[38], pars->GC_10, amp[1410]); 
  FFV1_0(w[72], w[145], w[0], pars->GC_11, amp[1411]); 
  VVV1_0(w[0], w[112], w[38], pars->GC_10, amp[1412]); 
  FFV1_0(w[3], w[34], w[289], pars->GC_11, amp[1413]); 
  FFV1_0(w[3], w[34], w[290], pars->GC_11, amp[1414]); 
  FFV1_0(w[3], w[34], w[291], pars->GC_11, amp[1415]); 
  FFV1_0(w[107], w[247], w[6], pars->GC_11, amp[1416]); 
  FFV1_0(w[58], w[247], w[1], pars->GC_11, amp[1417]); 
  FFV1_0(w[227], w[146], w[6], pars->GC_11, amp[1418]); 
  FFV1_0(w[227], w[46], w[1], pars->GC_11, amp[1419]); 
  FFV1_0(w[58], w[146], w[0], pars->GC_11, amp[1420]); 
  FFV1_0(w[107], w[46], w[0], pars->GC_11, amp[1421]); 
  FFV1_0(w[109], w[247], w[4], pars->GC_11, amp[1422]); 
  FFV1_0(w[63], w[247], w[1], pars->GC_11, amp[1423]); 
  FFV1_0(w[231], w[146], w[4], pars->GC_11, amp[1424]); 
  FFV1_0(w[231], w[45], w[1], pars->GC_11, amp[1425]); 
  FFV1_0(w[63], w[146], w[0], pars->GC_11, amp[1426]); 
  FFV1_0(w[109], w[45], w[0], pars->GC_11, amp[1427]); 
  FFV1_0(w[3], w[247], w[111], pars->GC_11, amp[1428]); 
  FFV1_0(w[69], w[247], w[1], pars->GC_11, amp[1429]); 
  FFV1_0(w[3], w[146], w[230], pars->GC_11, amp[1430]); 
  VVV1_0(w[230], w[1], w[47], pars->GC_10, amp[1431]); 
  FFV1_0(w[69], w[146], w[0], pars->GC_11, amp[1432]); 
  VVV1_0(w[0], w[111], w[47], pars->GC_10, amp[1433]); 
  FFV1_0(w[3], w[44], w[286], pars->GC_11, amp[1434]); 
  FFV1_0(w[3], w[44], w[287], pars->GC_11, amp[1435]); 
  FFV1_0(w[3], w[44], w[288], pars->GC_11, amp[1436]); 
  FFV1_0(w[107], w[248], w[5], pars->GC_11, amp[1437]); 
  FFV1_0(w[57], w[248], w[1], pars->GC_11, amp[1438]); 
  FFV1_0(w[227], w[147], w[5], pars->GC_11, amp[1439]); 
  FFV1_0(w[227], w[53], w[1], pars->GC_11, amp[1440]); 
  FFV1_0(w[57], w[147], w[0], pars->GC_11, amp[1441]); 
  FFV1_0(w[107], w[53], w[0], pars->GC_11, amp[1442]); 
  FFV1_0(w[108], w[248], w[4], pars->GC_11, amp[1443]); 
  FFV1_0(w[60], w[248], w[1], pars->GC_11, amp[1444]); 
  FFV1_0(w[229], w[147], w[4], pars->GC_11, amp[1445]); 
  FFV1_0(w[229], w[52], w[1], pars->GC_11, amp[1446]); 
  FFV1_0(w[60], w[147], w[0], pars->GC_11, amp[1447]); 
  FFV1_0(w[108], w[52], w[0], pars->GC_11, amp[1448]); 
  FFV1_0(w[3], w[248], w[110], pars->GC_11, amp[1449]); 
  FFV1_0(w[66], w[248], w[1], pars->GC_11, amp[1450]); 
  FFV1_0(w[3], w[147], w[232], pars->GC_11, amp[1451]); 
  VVV1_0(w[232], w[1], w[54], pars->GC_10, amp[1452]); 
  FFV1_0(w[66], w[147], w[0], pars->GC_11, amp[1453]); 
  VVV1_0(w[0], w[110], w[54], pars->GC_10, amp[1454]); 
  FFV1_0(w[3], w[51], w[283], pars->GC_11, amp[1455]); 
  FFV1_0(w[3], w[51], w[284], pars->GC_11, amp[1456]); 
  FFV1_0(w[3], w[51], w[285], pars->GC_11, amp[1457]); 
  FFV1_0(w[227], w[2], w[112], pars->GC_11, amp[1458]); 
  FFV1_0(w[227], w[74], w[1], pars->GC_11, amp[1459]); 
  FFV1_0(w[107], w[2], w[228], pars->GC_11, amp[1460]); 
  VVV1_0(w[228], w[1], w[59], pars->GC_10, amp[1461]); 
  FFV1_0(w[107], w[74], w[0], pars->GC_11, amp[1462]); 
  VVV1_0(w[0], w[112], w[59], pars->GC_10, amp[1463]); 
  FFV1_0(w[48], w[2], w[289], pars->GC_11, amp[1464]); 
  FFV1_0(w[48], w[2], w[290], pars->GC_11, amp[1465]); 
  FFV1_0(w[48], w[2], w[291], pars->GC_11, amp[1466]); 
  FFV1_0(w[229], w[2], w[111], pars->GC_11, amp[1467]); 
  FFV1_0(w[229], w[71], w[1], pars->GC_11, amp[1468]); 
  FFV1_0(w[108], w[2], w[230], pars->GC_11, amp[1469]); 
  VVV1_0(w[230], w[1], w[62], pars->GC_10, amp[1470]); 
  FFV1_0(w[108], w[71], w[0], pars->GC_11, amp[1471]); 
  VVV1_0(w[0], w[111], w[62], pars->GC_10, amp[1472]); 
  FFV1_0(w[39], w[2], w[286], pars->GC_11, amp[1473]); 
  FFV1_0(w[39], w[2], w[287], pars->GC_11, amp[1474]); 
  FFV1_0(w[39], w[2], w[288], pars->GC_11, amp[1475]); 
  FFV1_0(w[231], w[2], w[110], pars->GC_11, amp[1476]); 
  FFV1_0(w[231], w[68], w[1], pars->GC_11, amp[1477]); 
  FFV1_0(w[109], w[2], w[232], pars->GC_11, amp[1478]); 
  VVV1_0(w[232], w[1], w[65], pars->GC_10, amp[1479]); 
  FFV1_0(w[109], w[68], w[0], pars->GC_11, amp[1480]); 
  VVV1_0(w[0], w[110], w[65], pars->GC_10, amp[1481]); 
  FFV1_0(w[42], w[2], w[283], pars->GC_11, amp[1482]); 
  FFV1_0(w[42], w[2], w[284], pars->GC_11, amp[1483]); 
  FFV1_0(w[42], w[2], w[285], pars->GC_11, amp[1484]); 
  VVVV1_0(w[292], w[8], w[5], w[6], pars->GC_12, amp[1485]); 
  VVVV3_0(w[292], w[8], w[5], w[6], pars->GC_12, amp[1486]); 
  VVVV4_0(w[292], w[8], w[5], w[6], pars->GC_12, amp[1487]); 
  VVVV1_0(w[293], w[8], w[5], w[6], pars->GC_12, amp[1488]); 
  VVVV3_0(w[293], w[8], w[5], w[6], pars->GC_12, amp[1489]); 
  VVVV4_0(w[293], w[8], w[5], w[6], pars->GC_12, amp[1490]); 
  VVVV1_0(w[294], w[8], w[5], w[6], pars->GC_12, amp[1491]); 
  VVVV3_0(w[294], w[8], w[5], w[6], pars->GC_12, amp[1492]); 
  VVVV4_0(w[294], w[8], w[5], w[6], pars->GC_12, amp[1493]); 
  VVV1_0(w[8], w[6], w[295], pars->GC_10, amp[1494]); 
  VVV1_0(w[8], w[6], w[296], pars->GC_10, amp[1495]); 
  VVV1_0(w[8], w[6], w[297], pars->GC_10, amp[1496]); 
  VVV1_0(w[8], w[5], w[298], pars->GC_10, amp[1497]); 
  VVV1_0(w[8], w[5], w[299], pars->GC_10, amp[1498]); 
  VVV1_0(w[8], w[5], w[300], pars->GC_10, amp[1499]); 
  VVV1_0(w[292], w[8], w[29], pars->GC_10, amp[1500]); 
  VVV1_0(w[293], w[8], w[29], pars->GC_10, amp[1501]); 
  VVV1_0(w[294], w[8], w[29], pars->GC_10, amp[1502]); 
  FFV1_0(w[301], w[44], w[6], pars->GC_11, amp[1503]); 
  FFV1_0(w[302], w[44], w[6], pars->GC_11, amp[1504]); 
  FFV1_0(w[303], w[44], w[6], pars->GC_11, amp[1505]); 
  FFV1_0(w[3], w[44], w[298], pars->GC_11, amp[1506]); 
  FFV1_0(w[3], w[44], w[299], pars->GC_11, amp[1507]); 
  FFV1_0(w[3], w[44], w[300], pars->GC_11, amp[1508]); 
  FFV1_0(w[42], w[44], w[292], pars->GC_11, amp[1509]); 
  FFV1_0(w[42], w[44], w[293], pars->GC_11, amp[1510]); 
  FFV1_0(w[42], w[44], w[294], pars->GC_11, amp[1511]); 
  FFV1_0(w[301], w[51], w[5], pars->GC_11, amp[1512]); 
  FFV1_0(w[302], w[51], w[5], pars->GC_11, amp[1513]); 
  FFV1_0(w[303], w[51], w[5], pars->GC_11, amp[1514]); 
  FFV1_0(w[3], w[51], w[295], pars->GC_11, amp[1515]); 
  FFV1_0(w[3], w[51], w[296], pars->GC_11, amp[1516]); 
  FFV1_0(w[3], w[51], w[297], pars->GC_11, amp[1517]); 
  FFV1_0(w[39], w[51], w[292], pars->GC_11, amp[1518]); 
  FFV1_0(w[39], w[51], w[293], pars->GC_11, amp[1519]); 
  FFV1_0(w[39], w[51], w[294], pars->GC_11, amp[1520]); 
  FFV1_0(w[39], w[304], w[6], pars->GC_11, amp[1521]); 
  FFV1_0(w[39], w[305], w[6], pars->GC_11, amp[1522]); 
  FFV1_0(w[39], w[306], w[6], pars->GC_11, amp[1523]); 
  FFV1_0(w[39], w[2], w[298], pars->GC_11, amp[1524]); 
  FFV1_0(w[39], w[2], w[299], pars->GC_11, amp[1525]); 
  FFV1_0(w[39], w[2], w[300], pars->GC_11, amp[1526]); 
  FFV1_0(w[42], w[304], w[5], pars->GC_11, amp[1527]); 
  FFV1_0(w[42], w[305], w[5], pars->GC_11, amp[1528]); 
  FFV1_0(w[42], w[306], w[5], pars->GC_11, amp[1529]); 
  FFV1_0(w[42], w[2], w[295], pars->GC_11, amp[1530]); 
  FFV1_0(w[42], w[2], w[296], pars->GC_11, amp[1531]); 
  FFV1_0(w[42], w[2], w[297], pars->GC_11, amp[1532]); 
  FFV1_0(w[3], w[304], w[29], pars->GC_11, amp[1533]); 
  FFV1_0(w[3], w[305], w[29], pars->GC_11, amp[1534]); 
  FFV1_0(w[3], w[306], w[29], pars->GC_11, amp[1535]); 
  FFV1_0(w[301], w[2], w[29], pars->GC_11, amp[1536]); 
  FFV1_0(w[302], w[2], w[29], pars->GC_11, amp[1537]); 
  FFV1_0(w[303], w[2], w[29], pars->GC_11, amp[1538]); 
  VVVV1_0(w[307], w[8], w[4], w[6], pars->GC_12, amp[1539]); 
  VVVV3_0(w[307], w[8], w[4], w[6], pars->GC_12, amp[1540]); 
  VVVV4_0(w[307], w[8], w[4], w[6], pars->GC_12, amp[1541]); 
  VVVV1_0(w[308], w[8], w[4], w[6], pars->GC_12, amp[1542]); 
  VVVV3_0(w[308], w[8], w[4], w[6], pars->GC_12, amp[1543]); 
  VVVV4_0(w[308], w[8], w[4], w[6], pars->GC_12, amp[1544]); 
  VVVV1_0(w[309], w[8], w[4], w[6], pars->GC_12, amp[1545]); 
  VVVV3_0(w[309], w[8], w[4], w[6], pars->GC_12, amp[1546]); 
  VVVV4_0(w[309], w[8], w[4], w[6], pars->GC_12, amp[1547]); 
  VVV1_0(w[8], w[6], w[310], pars->GC_10, amp[1548]); 
  VVV1_0(w[8], w[6], w[311], pars->GC_10, amp[1549]); 
  VVV1_0(w[8], w[6], w[312], pars->GC_10, amp[1550]); 
  VVV1_0(w[8], w[4], w[313], pars->GC_10, amp[1551]); 
  VVV1_0(w[8], w[4], w[314], pars->GC_10, amp[1552]); 
  VVV1_0(w[8], w[4], w[315], pars->GC_10, amp[1553]); 
  VVV1_0(w[307], w[8], w[27], pars->GC_10, amp[1554]); 
  VVV1_0(w[308], w[8], w[27], pars->GC_10, amp[1555]); 
  VVV1_0(w[309], w[8], w[27], pars->GC_10, amp[1556]); 
  FFV1_0(w[316], w[34], w[6], pars->GC_11, amp[1557]); 
  FFV1_0(w[317], w[34], w[6], pars->GC_11, amp[1558]); 
  FFV1_0(w[318], w[34], w[6], pars->GC_11, amp[1559]); 
  FFV1_0(w[3], w[34], w[313], pars->GC_11, amp[1560]); 
  FFV1_0(w[3], w[34], w[314], pars->GC_11, amp[1561]); 
  FFV1_0(w[3], w[34], w[315], pars->GC_11, amp[1562]); 
  FFV1_0(w[42], w[34], w[307], pars->GC_11, amp[1563]); 
  FFV1_0(w[42], w[34], w[308], pars->GC_11, amp[1564]); 
  FFV1_0(w[42], w[34], w[309], pars->GC_11, amp[1565]); 
  FFV1_0(w[316], w[51], w[4], pars->GC_11, amp[1566]); 
  FFV1_0(w[317], w[51], w[4], pars->GC_11, amp[1567]); 
  FFV1_0(w[318], w[51], w[4], pars->GC_11, amp[1568]); 
  FFV1_0(w[3], w[51], w[310], pars->GC_11, amp[1569]); 
  FFV1_0(w[3], w[51], w[311], pars->GC_11, amp[1570]); 
  FFV1_0(w[3], w[51], w[312], pars->GC_11, amp[1571]); 
  FFV1_0(w[48], w[51], w[307], pars->GC_11, amp[1572]); 
  FFV1_0(w[48], w[51], w[308], pars->GC_11, amp[1573]); 
  FFV1_0(w[48], w[51], w[309], pars->GC_11, amp[1574]); 
  FFV1_0(w[48], w[319], w[6], pars->GC_11, amp[1575]); 
  FFV1_0(w[48], w[320], w[6], pars->GC_11, amp[1576]); 
  FFV1_0(w[48], w[321], w[6], pars->GC_11, amp[1577]); 
  FFV1_0(w[48], w[2], w[313], pars->GC_11, amp[1578]); 
  FFV1_0(w[48], w[2], w[314], pars->GC_11, amp[1579]); 
  FFV1_0(w[48], w[2], w[315], pars->GC_11, amp[1580]); 
  FFV1_0(w[42], w[319], w[4], pars->GC_11, amp[1581]); 
  FFV1_0(w[42], w[320], w[4], pars->GC_11, amp[1582]); 
  FFV1_0(w[42], w[321], w[4], pars->GC_11, amp[1583]); 
  FFV1_0(w[42], w[2], w[310], pars->GC_11, amp[1584]); 
  FFV1_0(w[42], w[2], w[311], pars->GC_11, amp[1585]); 
  FFV1_0(w[42], w[2], w[312], pars->GC_11, amp[1586]); 
  FFV1_0(w[3], w[319], w[27], pars->GC_11, amp[1587]); 
  FFV1_0(w[3], w[320], w[27], pars->GC_11, amp[1588]); 
  FFV1_0(w[3], w[321], w[27], pars->GC_11, amp[1589]); 
  FFV1_0(w[316], w[2], w[27], pars->GC_11, amp[1590]); 
  FFV1_0(w[317], w[2], w[27], pars->GC_11, amp[1591]); 
  FFV1_0(w[318], w[2], w[27], pars->GC_11, amp[1592]); 
  VVVV1_0(w[322], w[8], w[4], w[5], pars->GC_12, amp[1593]); 
  VVVV3_0(w[322], w[8], w[4], w[5], pars->GC_12, amp[1594]); 
  VVVV4_0(w[322], w[8], w[4], w[5], pars->GC_12, amp[1595]); 
  VVVV1_0(w[323], w[8], w[4], w[5], pars->GC_12, amp[1596]); 
  VVVV3_0(w[323], w[8], w[4], w[5], pars->GC_12, amp[1597]); 
  VVVV4_0(w[323], w[8], w[4], w[5], pars->GC_12, amp[1598]); 
  VVVV1_0(w[324], w[8], w[4], w[5], pars->GC_12, amp[1599]); 
  VVVV3_0(w[324], w[8], w[4], w[5], pars->GC_12, amp[1600]); 
  VVVV4_0(w[324], w[8], w[4], w[5], pars->GC_12, amp[1601]); 
  VVV1_0(w[8], w[5], w[325], pars->GC_10, amp[1602]); 
  VVV1_0(w[8], w[5], w[326], pars->GC_10, amp[1603]); 
  VVV1_0(w[8], w[5], w[327], pars->GC_10, amp[1604]); 
  VVV1_0(w[8], w[4], w[328], pars->GC_10, amp[1605]); 
  VVV1_0(w[8], w[4], w[329], pars->GC_10, amp[1606]); 
  VVV1_0(w[8], w[4], w[330], pars->GC_10, amp[1607]); 
  VVV1_0(w[322], w[8], w[24], pars->GC_10, amp[1608]); 
  VVV1_0(w[323], w[8], w[24], pars->GC_10, amp[1609]); 
  VVV1_0(w[324], w[8], w[24], pars->GC_10, amp[1610]); 
  FFV1_0(w[331], w[34], w[5], pars->GC_11, amp[1611]); 
  FFV1_0(w[332], w[34], w[5], pars->GC_11, amp[1612]); 
  FFV1_0(w[333], w[34], w[5], pars->GC_11, amp[1613]); 
  FFV1_0(w[3], w[34], w[328], pars->GC_11, amp[1614]); 
  FFV1_0(w[3], w[34], w[329], pars->GC_11, amp[1615]); 
  FFV1_0(w[3], w[34], w[330], pars->GC_11, amp[1616]); 
  FFV1_0(w[39], w[34], w[322], pars->GC_11, amp[1617]); 
  FFV1_0(w[39], w[34], w[323], pars->GC_11, amp[1618]); 
  FFV1_0(w[39], w[34], w[324], pars->GC_11, amp[1619]); 
  FFV1_0(w[331], w[44], w[4], pars->GC_11, amp[1620]); 
  FFV1_0(w[332], w[44], w[4], pars->GC_11, amp[1621]); 
  FFV1_0(w[333], w[44], w[4], pars->GC_11, amp[1622]); 
  FFV1_0(w[3], w[44], w[325], pars->GC_11, amp[1623]); 
  FFV1_0(w[3], w[44], w[326], pars->GC_11, amp[1624]); 
  FFV1_0(w[3], w[44], w[327], pars->GC_11, amp[1625]); 
  FFV1_0(w[48], w[44], w[322], pars->GC_11, amp[1626]); 
  FFV1_0(w[48], w[44], w[323], pars->GC_11, amp[1627]); 
  FFV1_0(w[48], w[44], w[324], pars->GC_11, amp[1628]); 
  FFV1_0(w[48], w[334], w[5], pars->GC_11, amp[1629]); 
  FFV1_0(w[48], w[335], w[5], pars->GC_11, amp[1630]); 
  FFV1_0(w[48], w[336], w[5], pars->GC_11, amp[1631]); 
  FFV1_0(w[48], w[2], w[328], pars->GC_11, amp[1632]); 
  FFV1_0(w[48], w[2], w[329], pars->GC_11, amp[1633]); 
  FFV1_0(w[48], w[2], w[330], pars->GC_11, amp[1634]); 
  FFV1_0(w[39], w[334], w[4], pars->GC_11, amp[1635]); 
  FFV1_0(w[39], w[335], w[4], pars->GC_11, amp[1636]); 
  FFV1_0(w[39], w[336], w[4], pars->GC_11, amp[1637]); 
  FFV1_0(w[39], w[2], w[325], pars->GC_11, amp[1638]); 
  FFV1_0(w[39], w[2], w[326], pars->GC_11, amp[1639]); 
  FFV1_0(w[39], w[2], w[327], pars->GC_11, amp[1640]); 
  FFV1_0(w[3], w[334], w[24], pars->GC_11, amp[1641]); 
  FFV1_0(w[3], w[335], w[24], pars->GC_11, amp[1642]); 
  FFV1_0(w[3], w[336], w[24], pars->GC_11, amp[1643]); 
  FFV1_0(w[331], w[2], w[24], pars->GC_11, amp[1644]); 
  FFV1_0(w[332], w[2], w[24], pars->GC_11, amp[1645]); 
  FFV1_0(w[333], w[2], w[24], pars->GC_11, amp[1646]); 
  FFV1_0(w[340], w[123], w[6], pars->GC_11, amp[1647]); 
  FFV1_0(w[341], w[123], w[6], pars->GC_11, amp[1648]); 
  FFV1_0(w[342], w[123], w[6], pars->GC_11, amp[1649]); 
  FFV1_0(w[3], w[123], w[343], pars->GC_11, amp[1650]); 
  FFV1_0(w[3], w[123], w[344], pars->GC_11, amp[1651]); 
  FFV1_0(w[3], w[123], w[345], pars->GC_11, amp[1652]); 
  FFV1_0(w[42], w[123], w[337], pars->GC_11, amp[1653]); 
  FFV1_0(w[42], w[123], w[338], pars->GC_11, amp[1654]); 
  FFV1_0(w[42], w[123], w[339], pars->GC_11, amp[1655]); 
  FFV1_0(w[76], w[346], w[6], pars->GC_11, amp[1656]); 
  FFV1_0(w[76], w[347], w[6], pars->GC_11, amp[1657]); 
  FFV1_0(w[76], w[348], w[6], pars->GC_11, amp[1658]); 
  FFV1_0(w[76], w[2], w[343], pars->GC_11, amp[1659]); 
  FFV1_0(w[76], w[2], w[344], pars->GC_11, amp[1660]); 
  FFV1_0(w[76], w[2], w[345], pars->GC_11, amp[1661]); 
  FFV1_0(w[76], w[51], w[337], pars->GC_11, amp[1662]); 
  FFV1_0(w[76], w[51], w[338], pars->GC_11, amp[1663]); 
  FFV1_0(w[76], w[51], w[339], pars->GC_11, amp[1664]); 
  FFV1_0(w[3], w[346], w[101], pars->GC_11, amp[1665]); 
  FFV1_0(w[3], w[347], w[101], pars->GC_11, amp[1666]); 
  FFV1_0(w[3], w[348], w[101], pars->GC_11, amp[1667]); 
  FFV1_0(w[340], w[2], w[101], pars->GC_11, amp[1668]); 
  FFV1_0(w[341], w[2], w[101], pars->GC_11, amp[1669]); 
  FFV1_0(w[342], w[2], w[101], pars->GC_11, amp[1670]); 
  VVV1_0(w[337], w[101], w[8], pars->GC_10, amp[1671]); 
  VVV1_0(w[338], w[101], w[8], pars->GC_10, amp[1672]); 
  VVV1_0(w[339], w[101], w[8], pars->GC_10, amp[1673]); 
  VVVV1_0(w[337], w[1], w[8], w[6], pars->GC_12, amp[1674]); 
  VVVV3_0(w[337], w[1], w[8], w[6], pars->GC_12, amp[1675]); 
  VVVV4_0(w[337], w[1], w[8], w[6], pars->GC_12, amp[1676]); 
  VVVV1_0(w[338], w[1], w[8], w[6], pars->GC_12, amp[1677]); 
  VVVV3_0(w[338], w[1], w[8], w[6], pars->GC_12, amp[1678]); 
  VVVV4_0(w[338], w[1], w[8], w[6], pars->GC_12, amp[1679]); 
  VVVV1_0(w[339], w[1], w[8], w[6], pars->GC_12, amp[1680]); 
  VVVV3_0(w[339], w[1], w[8], w[6], pars->GC_12, amp[1681]); 
  VVVV4_0(w[339], w[1], w[8], w[6], pars->GC_12, amp[1682]); 
  VVV1_0(w[8], w[6], w[349], pars->GC_10, amp[1683]); 
  VVV1_0(w[8], w[6], w[350], pars->GC_10, amp[1684]); 
  VVV1_0(w[8], w[6], w[351], pars->GC_10, amp[1685]); 
  VVV1_0(w[1], w[8], w[343], pars->GC_10, amp[1686]); 
  VVV1_0(w[1], w[8], w[344], pars->GC_10, amp[1687]); 
  VVV1_0(w[1], w[8], w[345], pars->GC_10, amp[1688]); 
  FFV1_0(w[3], w[51], w[349], pars->GC_11, amp[1689]); 
  FFV1_0(w[3], w[51], w[350], pars->GC_11, amp[1690]); 
  FFV1_0(w[3], w[51], w[351], pars->GC_11, amp[1691]); 
  FFV1_0(w[340], w[51], w[1], pars->GC_11, amp[1692]); 
  FFV1_0(w[341], w[51], w[1], pars->GC_11, amp[1693]); 
  FFV1_0(w[342], w[51], w[1], pars->GC_11, amp[1694]); 
  FFV1_0(w[42], w[2], w[349], pars->GC_11, amp[1695]); 
  FFV1_0(w[42], w[2], w[350], pars->GC_11, amp[1696]); 
  FFV1_0(w[42], w[2], w[351], pars->GC_11, amp[1697]); 
  FFV1_0(w[42], w[346], w[1], pars->GC_11, amp[1698]); 
  FFV1_0(w[42], w[347], w[1], pars->GC_11, amp[1699]); 
  FFV1_0(w[42], w[348], w[1], pars->GC_11, amp[1700]); 
  FFV1_0(w[355], w[123], w[5], pars->GC_11, amp[1701]); 
  FFV1_0(w[356], w[123], w[5], pars->GC_11, amp[1702]); 
  FFV1_0(w[357], w[123], w[5], pars->GC_11, amp[1703]); 
  FFV1_0(w[3], w[123], w[358], pars->GC_11, amp[1704]); 
  FFV1_0(w[3], w[123], w[359], pars->GC_11, amp[1705]); 
  FFV1_0(w[3], w[123], w[360], pars->GC_11, amp[1706]); 
  FFV1_0(w[39], w[123], w[352], pars->GC_11, amp[1707]); 
  FFV1_0(w[39], w[123], w[353], pars->GC_11, amp[1708]); 
  FFV1_0(w[39], w[123], w[354], pars->GC_11, amp[1709]); 
  FFV1_0(w[76], w[361], w[5], pars->GC_11, amp[1710]); 
  FFV1_0(w[76], w[362], w[5], pars->GC_11, amp[1711]); 
  FFV1_0(w[76], w[363], w[5], pars->GC_11, amp[1712]); 
  FFV1_0(w[76], w[2], w[358], pars->GC_11, amp[1713]); 
  FFV1_0(w[76], w[2], w[359], pars->GC_11, amp[1714]); 
  FFV1_0(w[76], w[2], w[360], pars->GC_11, amp[1715]); 
  FFV1_0(w[76], w[44], w[352], pars->GC_11, amp[1716]); 
  FFV1_0(w[76], w[44], w[353], pars->GC_11, amp[1717]); 
  FFV1_0(w[76], w[44], w[354], pars->GC_11, amp[1718]); 
  FFV1_0(w[3], w[361], w[95], pars->GC_11, amp[1719]); 
  FFV1_0(w[3], w[362], w[95], pars->GC_11, amp[1720]); 
  FFV1_0(w[3], w[363], w[95], pars->GC_11, amp[1721]); 
  FFV1_0(w[355], w[2], w[95], pars->GC_11, amp[1722]); 
  FFV1_0(w[356], w[2], w[95], pars->GC_11, amp[1723]); 
  FFV1_0(w[357], w[2], w[95], pars->GC_11, amp[1724]); 
  VVV1_0(w[352], w[95], w[8], pars->GC_10, amp[1725]); 
  VVV1_0(w[353], w[95], w[8], pars->GC_10, amp[1726]); 
  VVV1_0(w[354], w[95], w[8], pars->GC_10, amp[1727]); 
  VVVV1_0(w[352], w[1], w[8], w[5], pars->GC_12, amp[1728]); 
  VVVV3_0(w[352], w[1], w[8], w[5], pars->GC_12, amp[1729]); 
  VVVV4_0(w[352], w[1], w[8], w[5], pars->GC_12, amp[1730]); 
  VVVV1_0(w[353], w[1], w[8], w[5], pars->GC_12, amp[1731]); 
  VVVV3_0(w[353], w[1], w[8], w[5], pars->GC_12, amp[1732]); 
  VVVV4_0(w[353], w[1], w[8], w[5], pars->GC_12, amp[1733]); 
  VVVV1_0(w[354], w[1], w[8], w[5], pars->GC_12, amp[1734]); 
  VVVV3_0(w[354], w[1], w[8], w[5], pars->GC_12, amp[1735]); 
  VVVV4_0(w[354], w[1], w[8], w[5], pars->GC_12, amp[1736]); 
  VVV1_0(w[8], w[5], w[364], pars->GC_10, amp[1737]); 
  VVV1_0(w[8], w[5], w[365], pars->GC_10, amp[1738]); 
  VVV1_0(w[8], w[5], w[366], pars->GC_10, amp[1739]); 
  VVV1_0(w[1], w[8], w[358], pars->GC_10, amp[1740]); 
  VVV1_0(w[1], w[8], w[359], pars->GC_10, amp[1741]); 
  VVV1_0(w[1], w[8], w[360], pars->GC_10, amp[1742]); 
  FFV1_0(w[3], w[44], w[364], pars->GC_11, amp[1743]); 
  FFV1_0(w[3], w[44], w[365], pars->GC_11, amp[1744]); 
  FFV1_0(w[3], w[44], w[366], pars->GC_11, amp[1745]); 
  FFV1_0(w[355], w[44], w[1], pars->GC_11, amp[1746]); 
  FFV1_0(w[356], w[44], w[1], pars->GC_11, amp[1747]); 
  FFV1_0(w[357], w[44], w[1], pars->GC_11, amp[1748]); 
  FFV1_0(w[39], w[2], w[364], pars->GC_11, amp[1749]); 
  FFV1_0(w[39], w[2], w[365], pars->GC_11, amp[1750]); 
  FFV1_0(w[39], w[2], w[366], pars->GC_11, amp[1751]); 
  FFV1_0(w[39], w[361], w[1], pars->GC_11, amp[1752]); 
  FFV1_0(w[39], w[362], w[1], pars->GC_11, amp[1753]); 
  FFV1_0(w[39], w[363], w[1], pars->GC_11, amp[1754]); 
  FFV1_0(w[370], w[123], w[4], pars->GC_11, amp[1755]); 
  FFV1_0(w[371], w[123], w[4], pars->GC_11, amp[1756]); 
  FFV1_0(w[372], w[123], w[4], pars->GC_11, amp[1757]); 
  FFV1_0(w[3], w[123], w[373], pars->GC_11, amp[1758]); 
  FFV1_0(w[3], w[123], w[374], pars->GC_11, amp[1759]); 
  FFV1_0(w[3], w[123], w[375], pars->GC_11, amp[1760]); 
  FFV1_0(w[48], w[123], w[367], pars->GC_11, amp[1761]); 
  FFV1_0(w[48], w[123], w[368], pars->GC_11, amp[1762]); 
  FFV1_0(w[48], w[123], w[369], pars->GC_11, amp[1763]); 
  FFV1_0(w[76], w[376], w[4], pars->GC_11, amp[1764]); 
  FFV1_0(w[76], w[377], w[4], pars->GC_11, amp[1765]); 
  FFV1_0(w[76], w[378], w[4], pars->GC_11, amp[1766]); 
  FFV1_0(w[76], w[2], w[373], pars->GC_11, amp[1767]); 
  FFV1_0(w[76], w[2], w[374], pars->GC_11, amp[1768]); 
  FFV1_0(w[76], w[2], w[375], pars->GC_11, amp[1769]); 
  FFV1_0(w[76], w[34], w[367], pars->GC_11, amp[1770]); 
  FFV1_0(w[76], w[34], w[368], pars->GC_11, amp[1771]); 
  FFV1_0(w[76], w[34], w[369], pars->GC_11, amp[1772]); 
  FFV1_0(w[3], w[376], w[87], pars->GC_11, amp[1773]); 
  FFV1_0(w[3], w[377], w[87], pars->GC_11, amp[1774]); 
  FFV1_0(w[3], w[378], w[87], pars->GC_11, amp[1775]); 
  FFV1_0(w[370], w[2], w[87], pars->GC_11, amp[1776]); 
  FFV1_0(w[371], w[2], w[87], pars->GC_11, amp[1777]); 
  FFV1_0(w[372], w[2], w[87], pars->GC_11, amp[1778]); 
  VVV1_0(w[367], w[87], w[8], pars->GC_10, amp[1779]); 
  VVV1_0(w[368], w[87], w[8], pars->GC_10, amp[1780]); 
  VVV1_0(w[369], w[87], w[8], pars->GC_10, amp[1781]); 
  VVVV1_0(w[367], w[1], w[8], w[4], pars->GC_12, amp[1782]); 
  VVVV3_0(w[367], w[1], w[8], w[4], pars->GC_12, amp[1783]); 
  VVVV4_0(w[367], w[1], w[8], w[4], pars->GC_12, amp[1784]); 
  VVVV1_0(w[368], w[1], w[8], w[4], pars->GC_12, amp[1785]); 
  VVVV3_0(w[368], w[1], w[8], w[4], pars->GC_12, amp[1786]); 
  VVVV4_0(w[368], w[1], w[8], w[4], pars->GC_12, amp[1787]); 
  VVVV1_0(w[369], w[1], w[8], w[4], pars->GC_12, amp[1788]); 
  VVVV3_0(w[369], w[1], w[8], w[4], pars->GC_12, amp[1789]); 
  VVVV4_0(w[369], w[1], w[8], w[4], pars->GC_12, amp[1790]); 
  VVV1_0(w[8], w[4], w[379], pars->GC_10, amp[1791]); 
  VVV1_0(w[8], w[4], w[380], pars->GC_10, amp[1792]); 
  VVV1_0(w[8], w[4], w[381], pars->GC_10, amp[1793]); 
  VVV1_0(w[1], w[8], w[373], pars->GC_10, amp[1794]); 
  VVV1_0(w[1], w[8], w[374], pars->GC_10, amp[1795]); 
  VVV1_0(w[1], w[8], w[375], pars->GC_10, amp[1796]); 
  FFV1_0(w[3], w[34], w[379], pars->GC_11, amp[1797]); 
  FFV1_0(w[3], w[34], w[380], pars->GC_11, amp[1798]); 
  FFV1_0(w[3], w[34], w[381], pars->GC_11, amp[1799]); 
  FFV1_0(w[370], w[34], w[1], pars->GC_11, amp[1800]); 
  FFV1_0(w[371], w[34], w[1], pars->GC_11, amp[1801]); 
  FFV1_0(w[372], w[34], w[1], pars->GC_11, amp[1802]); 
  FFV1_0(w[48], w[2], w[379], pars->GC_11, amp[1803]); 
  FFV1_0(w[48], w[2], w[380], pars->GC_11, amp[1804]); 
  FFV1_0(w[48], w[2], w[381], pars->GC_11, amp[1805]); 
  FFV1_0(w[48], w[376], w[1], pars->GC_11, amp[1806]); 
  FFV1_0(w[48], w[377], w[1], pars->GC_11, amp[1807]); 
  FFV1_0(w[48], w[378], w[1], pars->GC_11, amp[1808]); 
  VVVV1_0(w[0], w[113], w[8], w[6], pars->GC_12, amp[1809]); 
  VVVV3_0(w[0], w[113], w[8], w[6], pars->GC_12, amp[1810]); 
  VVVV4_0(w[0], w[113], w[8], w[6], pars->GC_12, amp[1811]); 
  VVVV1_0(w[0], w[114], w[8], w[6], pars->GC_12, amp[1812]); 
  VVVV3_0(w[0], w[114], w[8], w[6], pars->GC_12, amp[1813]); 
  VVVV4_0(w[0], w[114], w[8], w[6], pars->GC_12, amp[1814]); 
  VVVV1_0(w[0], w[115], w[8], w[6], pars->GC_12, amp[1815]); 
  VVVV3_0(w[0], w[115], w[8], w[6], pars->GC_12, amp[1816]); 
  VVVV4_0(w[0], w[115], w[8], w[6], pars->GC_12, amp[1817]); 
  VVV1_0(w[8], w[6], w[382], pars->GC_10, amp[1818]); 
  VVV1_0(w[8], w[6], w[383], pars->GC_10, amp[1819]); 
  VVV1_0(w[8], w[6], w[384], pars->GC_10, amp[1820]); 
  VVV1_0(w[113], w[6], w[250], pars->GC_10, amp[1821]); 
  VVV1_0(w[114], w[6], w[250], pars->GC_10, amp[1822]); 
  VVV1_0(w[115], w[6], w[250], pars->GC_10, amp[1823]); 
  FFV1_0(w[3], w[51], w[382], pars->GC_11, amp[1824]); 
  FFV1_0(w[3], w[51], w[383], pars->GC_11, amp[1825]); 
  FFV1_0(w[3], w[51], w[384], pars->GC_11, amp[1826]); 
  FFV1_0(w[3], w[248], w[113], pars->GC_11, amp[1827]); 
  FFV1_0(w[3], w[248], w[114], pars->GC_11, amp[1828]); 
  FFV1_0(w[3], w[248], w[115], pars->GC_11, amp[1829]); 
  FFV1_0(w[42], w[2], w[382], pars->GC_11, amp[1830]); 
  FFV1_0(w[42], w[2], w[383], pars->GC_11, amp[1831]); 
  FFV1_0(w[42], w[2], w[384], pars->GC_11, amp[1832]); 
  FFV1_0(w[231], w[2], w[113], pars->GC_11, amp[1833]); 
  FFV1_0(w[231], w[2], w[114], pars->GC_11, amp[1834]); 
  FFV1_0(w[231], w[2], w[115], pars->GC_11, amp[1835]); 
  VVVV1_0(w[0], w[116], w[8], w[5], pars->GC_12, amp[1836]); 
  VVVV3_0(w[0], w[116], w[8], w[5], pars->GC_12, amp[1837]); 
  VVVV4_0(w[0], w[116], w[8], w[5], pars->GC_12, amp[1838]); 
  VVVV1_0(w[0], w[117], w[8], w[5], pars->GC_12, amp[1839]); 
  VVVV3_0(w[0], w[117], w[8], w[5], pars->GC_12, amp[1840]); 
  VVVV4_0(w[0], w[117], w[8], w[5], pars->GC_12, amp[1841]); 
  VVVV1_0(w[0], w[118], w[8], w[5], pars->GC_12, amp[1842]); 
  VVVV3_0(w[0], w[118], w[8], w[5], pars->GC_12, amp[1843]); 
  VVVV4_0(w[0], w[118], w[8], w[5], pars->GC_12, amp[1844]); 
  VVV1_0(w[8], w[5], w[385], pars->GC_10, amp[1845]); 
  VVV1_0(w[8], w[5], w[386], pars->GC_10, amp[1846]); 
  VVV1_0(w[8], w[5], w[387], pars->GC_10, amp[1847]); 
  VVV1_0(w[116], w[5], w[250], pars->GC_10, amp[1848]); 
  VVV1_0(w[117], w[5], w[250], pars->GC_10, amp[1849]); 
  VVV1_0(w[118], w[5], w[250], pars->GC_10, amp[1850]); 
  FFV1_0(w[3], w[44], w[385], pars->GC_11, amp[1851]); 
  FFV1_0(w[3], w[44], w[386], pars->GC_11, amp[1852]); 
  FFV1_0(w[3], w[44], w[387], pars->GC_11, amp[1853]); 
  FFV1_0(w[3], w[247], w[116], pars->GC_11, amp[1854]); 
  FFV1_0(w[3], w[247], w[117], pars->GC_11, amp[1855]); 
  FFV1_0(w[3], w[247], w[118], pars->GC_11, amp[1856]); 
  FFV1_0(w[39], w[2], w[385], pars->GC_11, amp[1857]); 
  FFV1_0(w[39], w[2], w[386], pars->GC_11, amp[1858]); 
  FFV1_0(w[39], w[2], w[387], pars->GC_11, amp[1859]); 
  FFV1_0(w[229], w[2], w[116], pars->GC_11, amp[1860]); 
  FFV1_0(w[229], w[2], w[117], pars->GC_11, amp[1861]); 
  FFV1_0(w[229], w[2], w[118], pars->GC_11, amp[1862]); 
  VVVV1_0(w[0], w[119], w[8], w[4], pars->GC_12, amp[1863]); 
  VVVV3_0(w[0], w[119], w[8], w[4], pars->GC_12, amp[1864]); 
  VVVV4_0(w[0], w[119], w[8], w[4], pars->GC_12, amp[1865]); 
  VVVV1_0(w[0], w[120], w[8], w[4], pars->GC_12, amp[1866]); 
  VVVV3_0(w[0], w[120], w[8], w[4], pars->GC_12, amp[1867]); 
  VVVV4_0(w[0], w[120], w[8], w[4], pars->GC_12, amp[1868]); 
  VVVV1_0(w[0], w[121], w[8], w[4], pars->GC_12, amp[1869]); 
  VVVV3_0(w[0], w[121], w[8], w[4], pars->GC_12, amp[1870]); 
  VVVV4_0(w[0], w[121], w[8], w[4], pars->GC_12, amp[1871]); 
  VVV1_0(w[8], w[4], w[388], pars->GC_10, amp[1872]); 
  VVV1_0(w[8], w[4], w[389], pars->GC_10, amp[1873]); 
  VVV1_0(w[8], w[4], w[390], pars->GC_10, amp[1874]); 
  VVV1_0(w[119], w[4], w[250], pars->GC_10, amp[1875]); 
  VVV1_0(w[120], w[4], w[250], pars->GC_10, amp[1876]); 
  VVV1_0(w[121], w[4], w[250], pars->GC_10, amp[1877]); 
  FFV1_0(w[3], w[34], w[388], pars->GC_11, amp[1878]); 
  FFV1_0(w[3], w[34], w[389], pars->GC_11, amp[1879]); 
  FFV1_0(w[3], w[34], w[390], pars->GC_11, amp[1880]); 
  FFV1_0(w[3], w[246], w[119], pars->GC_11, amp[1881]); 
  FFV1_0(w[3], w[246], w[120], pars->GC_11, amp[1882]); 
  FFV1_0(w[3], w[246], w[121], pars->GC_11, amp[1883]); 
  FFV1_0(w[48], w[2], w[388], pars->GC_11, amp[1884]); 
  FFV1_0(w[48], w[2], w[389], pars->GC_11, amp[1885]); 
  FFV1_0(w[48], w[2], w[390], pars->GC_11, amp[1886]); 
  FFV1_0(w[227], w[2], w[119], pars->GC_11, amp[1887]); 
  FFV1_0(w[227], w[2], w[120], pars->GC_11, amp[1888]); 
  FFV1_0(w[227], w[2], w[121], pars->GC_11, amp[1889]); 


}
double PY8MEs_R16_P0_sm_gg_ttxggg::matrix_16_gg_ttxggg() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 1890;
  const int ncolor = 120; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {324, 324, 324, 324, 324, 324, 324, 324,
      324, 324, 324, 324, 324, 324, 324, 324, 324, 324, 324, 324, 324, 324,
      324, 324, 324, 324, 324, 324, 324, 324, 324, 324, 324, 324, 324, 324,
      324, 324, 324, 324, 324, 324, 324, 324, 324, 324, 324, 324, 324, 324,
      324, 324, 324, 324, 324, 324, 324, 324, 324, 324, 324, 324, 324, 324,
      324, 324, 324, 324, 324, 324, 324, 324, 324, 324, 324, 324, 324, 324,
      324, 324, 324, 324, 324, 324, 324, 324, 324, 324, 324, 324, 324, 324,
      324, 324, 324, 324, 324, 324, 324, 324, 324, 324, 324, 324, 324, 324,
      324, 324, 324, 324, 324, 324, 324, 324, 324, 324, 324, 324, 324, 324};
  static const double cf[ncolor][ncolor] = {{4096, -512, -512, 64, 64, 640,
      -512, 64, 64, -8, -8, -80, 64, -8, 640, -80, 568, 496, -8, -80, -80, 496,
      496, -224, -512, 64, 64, -8, -8, -80, 64, -8, -8, 1, 1, 10, -8, 1, -80,
      10, -71, -62, 1, 10, 10, -62, -62, 28, 64, -8, -8, 1, 1, 10, 640, -80,
      -80, 10, 10, 100, 568, -71, 496, -62, 505, 514, -71, 19, -62, -53, -134,
      -44, -8, 1, -80, 10, -71, -62, -80, 10, 496, -62, 19, -53, 496, -62,
      -224, 28, -134, -44, 505, -134, -134, 442, 442, -116, 1, 10, 10, -62,
      -62, 28, 10, 100, -62, 514, -53, -44, -62, -53, 28, -44, 442, -116, 514,
      -44, -44, -116, -116, 136}, {-512, 4096, 64, 640, -512, 64, 64, -512, -8,
      -80, 64, -8, -8, -80, -80, 496, 496, -224, 64, -8, 640, -80, 568, 496,
      64, -512, -8, -80, 64, -8, -8, 64, 1, 10, -8, 1, 1, 10, 10, -62, -62, 28,
      -8, 1, -80, 10, -71, -62, -8, 64, 1, 10, -8, 1, -80, 640, 10, 100, -80,
      10, -71, 19, -62, -53, -134, -44, 568, -71, 496, -62, 505, 514, 1, 10,
      10, -62, -62, 28, 10, 100, -62, 514, -53, -44, -62, -53, 28, -44, 442,
      -116, 514, -44, -44, -116, -116, 136, -8, 1, -80, 10, -71, -62, -80, 10,
      496, -62, 19, -53, 496, -62, -224, 28, -134, -44, 505, -134, -134, 442,
      442, -116}, {-512, 64, 4096, -512, 640, 64, 64, -8, 640, -80, 568, 496,
      -512, 64, 64, -8, -8, -80, -80, -8, 496, -224, -80, 496, 64, -8, -512,
      64, -80, -8, -8, 1, -80, 10, -71, -62, 64, -8, -8, 1, 1, 10, 10, 1, -62,
      28, 10, -62, -8, 1, -80, 10, -71, -62, -80, 10, 496, -62, 19, -53, 496,
      -62, -224, 28, -134, -44, 505, -134, -134, 442, 442, -116, 64, -8, -8, 1,
      1, 10, 640, -80, -80, 10, 10, 100, 568, -71, 496, -62, 505, 514, -71, 19,
      -62, -53, -134, -44, 10, 1, -62, 28, 10, -62, 100, 10, -53, -44, -62,
      514, 514, -44, -44, -116, -116, 136, -62, -53, 28, -44, 442, -116}, {64,
      640, -512, 4096, 64, -512, -8, -80, -80, 496, 496, -224, 64, -512, -8,
      -80, 64, -8, -8, 64, 568, 496, 640, -80, -8, -80, 64, -512, -8, 64, 1,
      10, 10, -62, -62, 28, -8, 64, 1, 10, -8, 1, 1, -8, -71, -62, -80, 10, 1,
      10, 10, -62, -62, 28, 10, 100, -62, 514, -53, -44, -62, -53, 28, -44,
      442, -116, 514, -44, -44, -116, -116, 136, -8, 64, 1, 10, -8, 1, -80,
      640, 10, 100, -80, 10, -71, 19, -62, -53, -134, -44, 568, -71, 496, -62,
      505, 514, 1, -8, -71, -62, -80, 10, 10, -80, 19, -53, 496, -62, 505,
      -134, -134, 442, 442, -116, 496, -62, -224, 28, -134, -44}, {64, -512,
      640, 64, 4096, -512, -8, 64, 568, 496, 640, -80, -80, -8, 496, -224, -80,
      496, -512, 64, 64, -8, -8, -80, -8, 64, -80, -8, -512, 64, 1, -8, -71,
      -62, -80, 10, 10, 1, -62, 28, 10, -62, 64, -8, -8, 1, 1, 10, 1, -8, -71,
      -62, -80, 10, 10, -80, 19, -53, 496, -62, 505, -134, -134, 442, 442,
      -116, 496, -62, -224, 28, -134, -44, 10, 1, -62, 28, 10, -62, 100, 10,
      -53, -44, -62, 514, 514, -44, -44, -116, -116, 136, -62, -53, 28, -44,
      442, -116, 64, -8, -8, 1, 1, 10, 640, -80, -80, 10, 10, 100, 568, -71,
      496, -62, 505, 514, -71, 19, -62, -53, -134, -44}, {640, 64, 64, -512,
      -512, 4096, -80, -8, 496, -224, -80, 496, -8, 64, 568, 496, 640, -80, 64,
      -512, -8, -80, 64, -8, -80, -8, -8, 64, 64, -512, 10, 1, -62, 28, 10,
      -62, 1, -8, -71, -62, -80, 10, -8, 64, 1, 10, -8, 1, 10, 1, -62, 28, 10,
      -62, 100, 10, -53, -44, -62, 514, 514, -44, -44, -116, -116, 136, -62,
      -53, 28, -44, 442, -116, 1, -8, -71, -62, -80, 10, 10, -80, 19, -53, 496,
      -62, 505, -134, -134, 442, 442, -116, 496, -62, -224, 28, -134, -44, -8,
      64, 1, 10, -8, 1, -80, 640, 10, 100, -80, 10, -71, 19, -62, -53, -134,
      -44, 568, -71, 496, -62, 505, 514}, {-512, 64, 64, -8, -8, -80, 4096,
      -512, -512, 64, 64, 640, 640, -80, 64, -8, 496, 568, -80, 496, -8, -80,
      -224, 496, 64, -8, -8, 1, 1, 10, 640, -80, -80, 10, 10, 100, 568, -71,
      496, -62, 505, 514, -71, 19, -62, -53, -134, -44, -512, 64, 64, -8, -8,
      -80, 64, -8, -8, 1, 1, 10, -8, 1, -80, 10, -71, -62, 1, 10, 10, -62, -62,
      28, -80, 10, -8, 1, -62, -71, 496, -62, -224, 28, -134, -44, -80, 10,
      496, -62, 19, -53, -134, 505, 442, -116, -134, 442, 10, -62, 1, 10, 28,
      -62, -62, -53, 28, -44, 442, -116, 10, 100, -62, 514, -53, -44, -44, 514,
      -116, 136, -44, -116}, {64, -512, -8, -80, 64, -8, -512, 4096, 64, 640,
      -512, 64, -80, 496, -8, -80, -224, 496, 640, -80, 64, -8, 496, 568, -8,
      64, 1, 10, -8, 1, -80, 640, 10, 100, -80, 10, -71, 19, -62, -53, -134,
      -44, 568, -71, 496, -62, 505, 514, 64, -512, -8, -80, 64, -8, -8, 64, 1,
      10, -8, 1, 1, 10, 10, -62, -62, 28, -8, 1, -80, 10, -71, -62, 10, -62, 1,
      10, 28, -62, -62, -53, 28, -44, 442, -116, 10, 100, -62, 514, -53, -44,
      -44, 514, -116, 136, -44, -116, -80, 10, -8, 1, -62, -71, 496, -62, -224,
      28, -134, -44, -80, 10, 496, -62, 19, -53, -134, 505, 442, -116, -134,
      442}, {64, -8, 640, -80, 568, 496, -512, 64, 4096, -512, 640, 64, 64, -8,
      -512, 64, -80, -8, 496, -224, -80, -8, 496, -80, -8, 1, -80, 10, -71,
      -62, -80, 10, 496, -62, 19, -53, 496, -62, -224, 28, -134, -44, 505,
      -134, -134, 442, 442, -116, 64, -8, -512, 64, -80, -8, -8, 1, -80, 10,
      -71, -62, 64, -8, -8, 1, 1, 10, 10, 1, -62, 28, 10, -62, -8, 1, 64, -8,
      10, 1, 568, -71, 496, -62, 505, 514, 640, -80, -80, 10, 10, 100, 19, -71,
      -134, -44, -62, -53, -62, 28, 10, 1, -62, 10, 514, -44, -44, -116, -116,
      136, 100, 10, -53, -44, -62, 514, -53, -62, 442, -116, 28, -44}, {-8,
      -80, -80, 496, 496, -224, 64, 640, -512, 4096, 64, -512, -8, -80, 64,
      -512, -8, 64, 568, 496, -8, 64, -80, 640, 1, 10, 10, -62, -62, 28, 10,
      100, -62, 514, -53, -44, -62, -53, 28, -44, 442, -116, 514, -44, -44,
      -116, -116, 136, -8, -80, 64, -512, -8, 64, 1, 10, 10, -62, -62, 28, -8,
      64, 1, 10, -8, 1, 1, -8, -71, -62, -80, 10, 1, 10, -8, 64, 1, -8, -71,
      19, -62, -53, -134, -44, -80, 640, 10, 100, -80, 10, -71, 568, 505, 514,
      496, -62, -71, -62, 1, -8, 10, -80, 505, -134, -134, 442, 442, -116, 10,
      -80, 19, -53, 496, -62, -62, 496, -134, -44, -224, 28}, {-8, 64, 568,
      496, 640, -80, 64, -512, 640, 64, 4096, -512, 496, -224, -80, -8, 496,
      -80, 64, -8, -512, 64, -80, -8, 1, -8, -71, -62, -80, 10, 10, -80, 19,
      -53, 496, -62, 505, -134, -134, 442, 442, -116, 496, -62, -224, 28, -134,
      -44, -8, 64, -80, -8, -512, 64, 1, -8, -71, -62, -80, 10, 10, 1, -62, 28,
      10, -62, 64, -8, -8, 1, 1, 10, -62, 28, 10, 1, -62, 10, 514, -44, -44,
      -116, -116, 136, 100, 10, -53, -44, -62, 514, -53, -62, 442, -116, 28,
      -44, -8, 1, 64, -8, 10, 1, 568, -71, 496, -62, 505, 514, 640, -80, -80,
      10, 10, 100, 19, -71, -134, -44, -62, -53}, {-80, -8, 496, -224, -80,
      496, 640, 64, 64, -512, -512, 4096, 568, 496, -8, 64, -80, 640, -8, -80,
      64, -512, -8, 64, 10, 1, -62, 28, 10, -62, 100, 10, -53, -44, -62, 514,
      514, -44, -44, -116, -116, 136, -62, -53, 28, -44, 442, -116, -80, -8,
      -8, 64, 64, -512, 10, 1, -62, 28, 10, -62, 1, -8, -71, -62, -80, 10, -8,
      64, 1, 10, -8, 1, -71, -62, 1, -8, 10, -80, 505, -134, -134, 442, 442,
      -116, 10, -80, 19, -53, 496, -62, -62, 496, -134, -44, -224, 28, 1, 10,
      -8, 64, 1, -8, -71, 19, -62, -53, -134, -44, -80, 640, 10, 100, -80, 10,
      -71, 568, 505, 514, 496, -62}, {64, -8, -512, 64, -80, -8, 640, -80, 64,
      -8, 496, 568, 4096, -512, -512, 64, 64, 640, 496, -80, -224, 496, -8,
      -80, -8, 1, 64, -8, 10, 1, 568, -71, 496, -62, 505, 514, 640, -80, -80,
      10, 10, 100, 19, -71, -134, -44, -62, -53, -80, 10, -8, 1, -62, -71, 496,
      -62, -224, 28, -134, -44, -80, 10, 496, -62, 19, -53, -134, 505, 442,
      -116, -134, 442, -512, 64, 64, -8, -8, -80, 64, -8, -8, 1, 1, 10, -8, 1,
      -80, 10, -71, -62, 1, 10, 10, -62, -62, 28, -62, 10, 28, -62, 1, 10, -53,
      -62, 442, -116, 28, -44, -44, 514, -116, 136, -44, -116, 10, 100, -62,
      514, -53, -44}, {-8, -80, 64, -512, -8, 64, -80, 496, -8, -80, -224, 496,
      -512, 4096, 64, 640, -512, 64, -80, 640, 496, 568, 64, -8, 1, 10, -8, 64,
      1, -8, -71, 19, -62, -53, -134, -44, -80, 640, 10, 100, -80, 10, -71,
      568, 505, 514, 496, -62, 10, -62, 1, 10, 28, -62, -62, -53, 28, -44, 442,
      -116, 10, 100, -62, 514, -53, -44, -44, 514, -116, 136, -44, -116, 64,
      -512, -8, -80, 64, -8, -8, 64, 1, 10, -8, 1, 1, 10, 10, -62, -62, 28, -8,
      1, -80, 10, -71, -62, 10, -80, -62, -71, -8, 1, -62, 496, -134, -44,
      -224, 28, -134, 505, 442, -116, -134, 442, -80, 10, 496, -62, 19, -53},
      {640, -80, 64, -8, 496, 568, 64, -8, -512, 64, -80, -8, -512, 64, 4096,
      -512, 640, 64, -224, 496, 496, -80, -80, -8, -80, 10, -8, 1, -62, -71,
      496, -62, -224, 28, -134, -44, -80, 10, 496, -62, 19, -53, -134, 505,
      442, -116, -134, 442, -8, 1, 64, -8, 10, 1, 568, -71, 496, -62, 505, 514,
      640, -80, -80, 10, 10, 100, 19, -71, -134, -44, -62, -53, 64, -8, -512,
      64, -80, -8, -8, 1, -80, 10, -71, -62, 64, -8, -8, 1, 1, 10, 10, 1, -62,
      28, 10, -62, 28, -62, -62, 10, 10, 1, -44, 514, -116, 136, -44, -116,
      -53, -62, 442, -116, 28, -44, 100, 10, -53, -44, -62, 514}, {-80, 496,
      -8, -80, -224, 496, -8, -80, 64, -512, -8, 64, 64, 640, -512, 4096, 64,
      -512, 496, 568, -80, 640, -8, 64, 10, -62, 1, 10, 28, -62, -62, -53, 28,
      -44, 442, -116, 10, 100, -62, 514, -53, -44, -44, 514, -116, 136, -44,
      -116, 1, 10, -8, 64, 1, -8, -71, 19, -62, -53, -134, -44, -80, 640, 10,
      100, -80, 10, -71, 568, 505, 514, 496, -62, -8, -80, 64, -512, -8, 64, 1,
      10, 10, -62, -62, 28, -8, 64, 1, 10, -8, 1, 1, -8, -71, -62, -80, 10,
      -62, -71, 10, -80, 1, -8, -134, 505, 442, -116, -134, 442, -62, 496,
      -134, -44, -224, 28, 10, -80, 19, -53, 496, -62}, {568, 496, -8, 64, -80,
      640, 496, -224, -80, -8, 496, -80, 64, -512, 640, 64, 4096, -512, -8, 64,
      -80, -8, -512, 64, -71, -62, 1, -8, 10, -80, 505, -134, -134, 442, 442,
      -116, 10, -80, 19, -53, 496, -62, -62, 496, -134, -44, -224, 28, -62, 28,
      10, 1, -62, 10, 514, -44, -44, -116, -116, 136, 100, 10, -53, -44, -62,
      514, -53, -62, 442, -116, 28, -44, -8, 64, -80, -8, -512, 64, 1, -8, -71,
      -62, -80, 10, 10, 1, -62, 28, 10, -62, 64, -8, -8, 1, 1, 10, 1, -8, 10,
      1, 64, -8, -71, 568, 505, 514, 496, -62, 19, -71, -134, -44, -62, -53,
      640, -80, -80, 10, 10, 100}, {496, -224, -80, -8, 496, -80, 568, 496, -8,
      64, -80, 640, 640, 64, 64, -512, -512, 4096, -80, -8, -8, 64, 64, -512,
      -62, 28, 10, 1, -62, 10, 514, -44, -44, -116, -116, 136, 100, 10, -53,
      -44, -62, 514, -53, -62, 442, -116, 28, -44, -71, -62, 1, -8, 10, -80,
      505, -134, -134, 442, 442, -116, 10, -80, 19, -53, 496, -62, -62, 496,
      -134, -44, -224, 28, -80, -8, -8, 64, 64, -512, 10, 1, -62, 28, 10, -62,
      1, -8, -71, -62, -80, 10, -8, 64, 1, 10, -8, 1, 10, 1, 1, -8, -8, 64, 19,
      -71, -134, -44, -62, -53, -71, 568, 505, 514, 496, -62, -80, 640, 10,
      100, -80, 10}, {-8, 64, -80, -8, -512, 64, -80, 640, 496, 568, 64, -8,
      496, -80, -224, 496, -8, -80, 4096, -512, -512, 64, 64, 640, 1, -8, 10,
      1, 64, -8, -71, 568, 505, 514, 496, -62, 19, -71, -134, -44, -62, -53,
      640, -80, -80, 10, 10, 100, 10, -80, -62, -71, -8, 1, -62, 496, -134,
      -44, -224, 28, -134, 505, 442, -116, -134, 442, -80, 10, 496, -62, 19,
      -53, -62, 10, 28, -62, 1, 10, -53, -62, 442, -116, 28, -44, -44, 514,
      -116, 136, -44, -116, 10, 100, -62, 514, -53, -44, -512, 64, 64, -8, -8,
      -80, 64, -8, -8, 1, 1, 10, -8, 1, -80, 10, -71, -62, 1, 10, 10, -62, -62,
      28}, {-80, -8, -8, 64, 64, -512, 496, -80, -224, 496, -8, -80, -80, 640,
      496, 568, 64, -8, -512, 4096, 64, 640, -512, 64, 10, 1, 1, -8, -8, 64,
      19, -71, -134, -44, -62, -53, -71, 568, 505, 514, 496, -62, -80, 640, 10,
      100, -80, 10, -62, 10, 28, -62, 1, 10, -53, -62, 442, -116, 28, -44, -44,
      514, -116, 136, -44, -116, 10, 100, -62, 514, -53, -44, 10, -80, -62,
      -71, -8, 1, -62, 496, -134, -44, -224, 28, -134, 505, 442, -116, -134,
      442, -80, 10, 496, -62, 19, -53, 64, -512, -8, -80, 64, -8, -8, 64, 1,
      10, -8, 1, 1, 10, 10, -62, -62, 28, -8, 1, -80, 10, -71, -62}, {-80, 640,
      496, 568, 64, -8, -8, 64, -80, -8, -512, 64, -224, 496, 496, -80, -80,
      -8, -512, 64, 4096, -512, 640, 64, 10, -80, -62, -71, -8, 1, -62, 496,
      -134, -44, -224, 28, -134, 505, 442, -116, -134, 442, -80, 10, 496, -62,
      19, -53, 1, -8, 10, 1, 64, -8, -71, 568, 505, 514, 496, -62, 19, -71,
      -134, -44, -62, -53, 640, -80, -80, 10, 10, 100, 28, -62, -62, 10, 10, 1,
      -44, 514, -116, 136, -44, -116, -53, -62, 442, -116, 28, -44, 100, 10,
      -53, -44, -62, 514, 64, -8, -512, 64, -80, -8, -8, 1, -80, 10, -71, -62,
      64, -8, -8, 1, 1, 10, 10, 1, -62, 28, 10, -62}, {496, -80, -224, 496, -8,
      -80, -80, -8, -8, 64, 64, -512, 496, 568, -80, 640, -8, 64, 64, 640,
      -512, 4096, 64, -512, -62, 10, 28, -62, 1, 10, -53, -62, 442, -116, 28,
      -44, -44, 514, -116, 136, -44, -116, 10, 100, -62, 514, -53, -44, 10, 1,
      1, -8, -8, 64, 19, -71, -134, -44, -62, -53, -71, 568, 505, 514, 496,
      -62, -80, 640, 10, 100, -80, 10, -62, -71, 10, -80, 1, -8, -134, 505,
      442, -116, -134, 442, -62, 496, -134, -44, -224, 28, 10, -80, 19, -53,
      496, -62, -8, -80, 64, -512, -8, 64, 1, 10, 10, -62, -62, 28, -8, 64, 1,
      10, -8, 1, 1, -8, -71, -62, -80, 10}, {496, 568, -80, 640, -8, 64, -224,
      496, 496, -80, -80, -8, -8, 64, -80, -8, -512, 64, 64, -512, 640, 64,
      4096, -512, -62, -71, 10, -80, 1, -8, -134, 505, 442, -116, -134, 442,
      -62, 496, -134, -44, -224, 28, 10, -80, 19, -53, 496, -62, 28, -62, -62,
      10, 10, 1, -44, 514, -116, 136, -44, -116, -53, -62, 442, -116, 28, -44,
      100, 10, -53, -44, -62, 514, 1, -8, 10, 1, 64, -8, -71, 568, 505, 514,
      496, -62, 19, -71, -134, -44, -62, -53, 640, -80, -80, 10, 10, 100, -8,
      64, -80, -8, -512, 64, 1, -8, -71, -62, -80, 10, 10, 1, -62, 28, 10, -62,
      64, -8, -8, 1, 1, 10}, {-224, 496, 496, -80, -80, -8, 496, 568, -80, 640,
      -8, 64, -80, -8, -8, 64, 64, -512, 640, 64, 64, -512, -512, 4096, 28,
      -62, -62, 10, 10, 1, -44, 514, -116, 136, -44, -116, -53, -62, 442, -116,
      28, -44, 100, 10, -53, -44, -62, 514, -62, -71, 10, -80, 1, -8, -134,
      505, 442, -116, -134, 442, -62, 496, -134, -44, -224, 28, 10, -80, 19,
      -53, 496, -62, 10, 1, 1, -8, -8, 64, 19, -71, -134, -44, -62, -53, -71,
      568, 505, 514, 496, -62, -80, 640, 10, 100, -80, 10, -80, -8, -8, 64, 64,
      -512, 10, 1, -62, 28, 10, -62, 1, -8, -71, -62, -80, 10, -8, 64, 1, 10,
      -8, 1}, {-512, 64, 64, -8, -8, -80, 64, -8, -8, 1, 1, 10, -8, 1, -80, 10,
      -71, -62, 1, 10, 10, -62, -62, 28, 4096, -512, -512, 64, 64, 640, -512,
      64, 64, -8, -8, -80, 64, -8, 640, -80, 568, 496, -8, -80, -80, 496, 496,
      -224, 640, -80, -80, 10, 10, 100, 64, -8, -8, 1, 1, 10, 496, -62, 568,
      -71, 514, 505, -62, -53, -71, 19, -44, -134, -80, 10, 496, -62, 19, -53,
      -8, 1, -80, 10, -71, -62, -224, 28, 496, -62, -44, -134, -134, 442, 505,
      -134, -116, 442, 10, 100, -62, 514, -53, -44, 1, 10, 10, -62, -62, 28,
      28, -44, -62, -53, -116, 442, -44, -116, 514, -44, 136, -116}, {64, -512,
      -8, -80, 64, -8, -8, 64, 1, 10, -8, 1, 1, 10, 10, -62, -62, 28, -8, 1,
      -80, 10, -71, -62, -512, 4096, 64, 640, -512, 64, 64, -512, -8, -80, 64,
      -8, -8, -80, -80, 496, 496, -224, 64, -8, 640, -80, 568, 496, -80, 640,
      10, 100, -80, 10, -8, 64, 1, 10, -8, 1, -62, -53, -71, 19, -44, -134,
      496, -62, 568, -71, 514, 505, 10, 100, -62, 514, -53, -44, 1, 10, 10,
      -62, -62, 28, 28, -44, -62, -53, -116, 442, -44, -116, 514, -44, 136,
      -116, -80, 10, 496, -62, 19, -53, -8, 1, -80, 10, -71, -62, -224, 28,
      496, -62, -44, -134, -134, 442, 505, -134, -116, 442}, {64, -8, -512, 64,
      -80, -8, -8, 1, -80, 10, -71, -62, 64, -8, -8, 1, 1, 10, 10, 1, -62, 28,
      10, -62, -512, 64, 4096, -512, 640, 64, 64, -8, 640, -80, 568, 496, -512,
      64, 64, -8, -8, -80, -80, -8, 496, -224, -80, 496, -80, 10, 496, -62, 19,
      -53, -8, 1, -80, 10, -71, -62, -224, 28, 496, -62, -44, -134, -134, 442,
      505, -134, -116, 442, 640, -80, -80, 10, 10, 100, 64, -8, -8, 1, 1, 10,
      496, -62, 568, -71, 514, 505, -62, -53, -71, 19, -44, -134, 100, 10, -53,
      -44, -62, 514, 10, 1, -62, 28, 10, -62, -44, -116, 514, -44, 136, -116,
      28, -44, -62, -53, -116, 442}, {-8, -80, 64, -512, -8, 64, 1, 10, 10,
      -62, -62, 28, -8, 64, 1, 10, -8, 1, 1, -8, -71, -62, -80, 10, 64, 640,
      -512, 4096, 64, -512, -8, -80, -80, 496, 496, -224, 64, -512, -8, -80,
      64, -8, -8, 64, 568, 496, 640, -80, 10, 100, -62, 514, -53, -44, 1, 10,
      10, -62, -62, 28, 28, -44, -62, -53, -116, 442, -44, -116, 514, -44, 136,
      -116, -80, 640, 10, 100, -80, 10, -8, 64, 1, 10, -8, 1, -62, -53, -71,
      19, -44, -134, 496, -62, 568, -71, 514, 505, 10, -80, 19, -53, 496, -62,
      1, -8, -71, -62, -80, 10, -134, 442, 505, -134, -116, 442, -224, 28, 496,
      -62, -44, -134}, {-8, 64, -80, -8, -512, 64, 1, -8, -71, -62, -80, 10,
      10, 1, -62, 28, 10, -62, 64, -8, -8, 1, 1, 10, 64, -512, 640, 64, 4096,
      -512, -8, 64, 568, 496, 640, -80, -80, -8, 496, -224, -80, 496, -512, 64,
      64, -8, -8, -80, 10, -80, 19, -53, 496, -62, 1, -8, -71, -62, -80, 10,
      -134, 442, 505, -134, -116, 442, -224, 28, 496, -62, -44, -134, 100, 10,
      -53, -44, -62, 514, 10, 1, -62, 28, 10, -62, -44, -116, 514, -44, 136,
      -116, 28, -44, -62, -53, -116, 442, 640, -80, -80, 10, 10, 100, 64, -8,
      -8, 1, 1, 10, 496, -62, 568, -71, 514, 505, -62, -53, -71, 19, -44,
      -134}, {-80, -8, -8, 64, 64, -512, 10, 1, -62, 28, 10, -62, 1, -8, -71,
      -62, -80, 10, -8, 64, 1, 10, -8, 1, 640, 64, 64, -512, -512, 4096, -80,
      -8, 496, -224, -80, 496, -8, 64, 568, 496, 640, -80, 64, -512, -8, -80,
      64, -8, 100, 10, -53, -44, -62, 514, 10, 1, -62, 28, 10, -62, -44, -116,
      514, -44, 136, -116, 28, -44, -62, -53, -116, 442, 10, -80, 19, -53, 496,
      -62, 1, -8, -71, -62, -80, 10, -134, 442, 505, -134, -116, 442, -224, 28,
      496, -62, -44, -134, -80, 640, 10, 100, -80, 10, -8, 64, 1, 10, -8, 1,
      -62, -53, -71, 19, -44, -134, 496, -62, 568, -71, 514, 505}, {64, -8, -8,
      1, 1, 10, 640, -80, -80, 10, 10, 100, 568, -71, 496, -62, 505, 514, -71,
      19, -62, -53, -134, -44, -512, 64, 64, -8, -8, -80, 4096, -512, -512, 64,
      64, 640, 640, -80, 64, -8, 496, 568, -80, 496, -8, -80, -224, 496, 64,
      -8, -8, 1, 1, 10, -512, 64, 64, -8, -8, -80, -80, 10, -8, 1, -62, -71,
      10, -62, 1, 10, 28, -62, 496, -62, -224, 28, -134, -44, -80, 10, -8, 1,
      -62, -71, 496, -62, -80, 10, -53, 19, 442, -116, -134, 505, 442, -134,
      -62, -53, 28, -44, 442, -116, 10, -62, 1, 10, 28, -62, -62, 514, 10, 100,
      -44, -53, -116, 136, -44, 514, -116, -44}, {-8, 64, 1, 10, -8, 1, -80,
      640, 10, 100, -80, 10, -71, 19, -62, -53, -134, -44, 568, -71, 496, -62,
      505, 514, 64, -512, -8, -80, 64, -8, -512, 4096, 64, 640, -512, 64, -80,
      496, -8, -80, -224, 496, 640, -80, 64, -8, 496, 568, -8, 64, 1, 10, -8,
      1, 64, -512, -8, -80, 64, -8, 10, -62, 1, 10, 28, -62, -80, 10, -8, 1,
      -62, -71, -62, -53, 28, -44, 442, -116, 10, -62, 1, 10, 28, -62, -62,
      514, 10, 100, -44, -53, -116, 136, -44, 514, -116, -44, 496, -62, -224,
      28, -134, -44, -80, 10, -8, 1, -62, -71, 496, -62, -80, 10, -53, 19, 442,
      -116, -134, 505, 442, -134}, {-8, 1, -80, 10, -71, -62, -80, 10, 496,
      -62, 19, -53, 496, -62, -224, 28, -134, -44, 505, -134, -134, 442, 442,
      -116, 64, -8, 640, -80, 568, 496, -512, 64, 4096, -512, 640, 64, 64, -8,
      -512, 64, -80, -8, 496, -224, -80, -8, 496, -80, -8, 1, -80, 10, -71,
      -62, 64, -8, -512, 64, -80, -8, -8, 1, 64, -8, 10, 1, -62, 28, 10, 1,
      -62, 10, 568, -71, 496, -62, 505, 514, -8, 1, 64, -8, 10, 1, -80, 10,
      640, -80, 100, 10, -134, -44, 19, -71, -53, -62, 514, -44, -44, -116,
      -116, 136, -62, 28, 10, 1, -62, 10, -53, -44, 100, 10, 514, -62, 442,
      -116, -53, -62, -44, 28}, {1, 10, 10, -62, -62, 28, 10, 100, -62, 514,
      -53, -44, -62, -53, 28, -44, 442, -116, 514, -44, -44, -116, -116, 136,
      -8, -80, -80, 496, 496, -224, 64, 640, -512, 4096, 64, -512, -8, -80, 64,
      -512, -8, 64, 568, 496, -8, 64, -80, 640, 1, 10, 10, -62, -62, 28, -8,
      -80, 64, -512, -8, 64, 1, 10, -8, 64, 1, -8, -71, -62, 1, -8, 10, -80,
      -71, 19, -62, -53, -134, -44, 1, 10, -8, 64, 1, -8, 10, 100, -80, 640,
      10, -80, 505, 514, -71, 568, -62, 496, 505, -134, -134, 442, 442, -116,
      -71, -62, 1, -8, 10, -80, 19, -53, 10, -80, -62, 496, -134, -44, -62,
      496, 28, -224}, {1, -8, -71, -62, -80, 10, 10, -80, 19, -53, 496, -62,
      505, -134, -134, 442, 442, -116, 496, -62, -224, 28, -134, -44, -8, 64,
      568, 496, 640, -80, 64, -512, 640, 64, 4096, -512, 496, -224, -80, -8,
      496, -80, 64, -8, -512, 64, -80, -8, 1, -8, -71, -62, -80, 10, -8, 64,
      -80, -8, -512, 64, -62, 28, 10, 1, -62, 10, -8, 1, 64, -8, 10, 1, 514,
      -44, -44, -116, -116, 136, -62, 28, 10, 1, -62, 10, -53, -44, 100, 10,
      514, -62, 442, -116, -53, -62, -44, 28, 568, -71, 496, -62, 505, 514, -8,
      1, 64, -8, 10, 1, -80, 10, 640, -80, 100, 10, -134, -44, 19, -71, -53,
      -62}, {10, 1, -62, 28, 10, -62, 100, 10, -53, -44, -62, 514, 514, -44,
      -44, -116, -116, 136, -62, -53, 28, -44, 442, -116, -80, -8, 496, -224,
      -80, 496, 640, 64, 64, -512, -512, 4096, 568, 496, -8, 64, -80, 640, -8,
      -80, 64, -512, -8, 64, 10, 1, -62, 28, 10, -62, -80, -8, -8, 64, 64,
      -512, -71, -62, 1, -8, 10, -80, 1, 10, -8, 64, 1, -8, 505, -134, -134,
      442, 442, -116, -71, -62, 1, -8, 10, -80, 19, -53, 10, -80, -62, 496,
      -134, -44, -62, 496, 28, -224, -71, 19, -62, -53, -134, -44, 1, 10, -8,
      64, 1, -8, 10, 100, -80, 640, 10, -80, 505, 514, -71, 568, -62, 496},
      {-8, 1, 64, -8, 10, 1, 568, -71, 496, -62, 505, 514, 640, -80, -80, 10,
      10, 100, 19, -71, -134, -44, -62, -53, 64, -8, -512, 64, -80, -8, 640,
      -80, 64, -8, 496, 568, 4096, -512, -512, 64, 64, 640, 496, -80, -224,
      496, -8, -80, 496, -62, -224, 28, -134, -44, -80, 10, -8, 1, -62, -71,
      496, -62, -80, 10, -53, 19, 442, -116, -134, 505, 442, -134, 64, -8, -8,
      1, 1, 10, -512, 64, 64, -8, -8, -80, -80, 10, -8, 1, -62, -71, 10, -62,
      1, 10, 28, -62, -53, -62, 442, -116, 28, -44, -62, 10, 28, -62, 1, 10,
      -116, 136, -44, 514, -116, -44, -62, 514, 10, 100, -44, -53}, {1, 10, -8,
      64, 1, -8, -71, 19, -62, -53, -134, -44, -80, 640, 10, 100, -80, 10, -71,
      568, 505, 514, 496, -62, -8, -80, 64, -512, -8, 64, -80, 496, -8, -80,
      -224, 496, -512, 4096, 64, 640, -512, 64, -80, 640, 496, 568, 64, -8,
      -62, -53, 28, -44, 442, -116, 10, -62, 1, 10, 28, -62, -62, 514, 10, 100,
      -44, -53, -116, 136, -44, 514, -116, -44, -8, 64, 1, 10, -8, 1, 64, -512,
      -8, -80, 64, -8, 10, -62, 1, 10, 28, -62, -80, 10, -8, 1, -62, -71, -62,
      496, -134, -44, -224, 28, 10, -80, -62, -71, -8, 1, 442, -116, -134, 505,
      442, -134, 496, -62, -80, 10, -53, 19}, {-80, 10, -8, 1, -62, -71, 496,
      -62, -224, 28, -134, -44, -80, 10, 496, -62, 19, -53, -134, 505, 442,
      -116, -134, 442, 640, -80, 64, -8, 496, 568, 64, -8, -512, 64, -80, -8,
      -512, 64, 4096, -512, 640, 64, -224, 496, 496, -80, -80, -8, 568, -71,
      496, -62, 505, 514, -8, 1, 64, -8, 10, 1, -80, 10, 640, -80, 100, 10,
      -134, -44, 19, -71, -53, -62, -8, 1, -80, 10, -71, -62, 64, -8, -512, 64,
      -80, -8, -8, 1, 64, -8, 10, 1, -62, 28, 10, 1, -62, 10, -44, 514, -116,
      136, -44, -116, 28, -62, -62, 10, 10, 1, 442, -116, -53, -62, -44, 28,
      -53, -44, 100, 10, 514, -62}, {10, -62, 1, 10, 28, -62, -62, -53, 28,
      -44, 442, -116, 10, 100, -62, 514, -53, -44, -44, 514, -116, 136, -44,
      -116, -80, 496, -8, -80, -224, 496, -8, -80, 64, -512, -8, 64, 64, 640,
      -512, 4096, 64, -512, 496, 568, -80, 640, -8, 64, -71, 19, -62, -53,
      -134, -44, 1, 10, -8, 64, 1, -8, 10, 100, -80, 640, 10, -80, 505, 514,
      -71, 568, -62, 496, 1, 10, 10, -62, -62, 28, -8, -80, 64, -512, -8, 64,
      1, 10, -8, 64, 1, -8, -71, -62, 1, -8, 10, -80, -134, 505, 442, -116,
      -134, 442, -62, -71, 10, -80, 1, -8, -134, -44, -62, 496, 28, -224, 19,
      -53, 10, -80, -62, 496}, {-71, -62, 1, -8, 10, -80, 505, -134, -134, 442,
      442, -116, 10, -80, 19, -53, 496, -62, -62, 496, -134, -44, -224, 28,
      568, 496, -8, 64, -80, 640, 496, -224, -80, -8, 496, -80, 64, -512, 640,
      64, 4096, -512, -8, 64, -80, -8, -512, 64, 514, -44, -44, -116, -116,
      136, -62, 28, 10, 1, -62, 10, -53, -44, 100, 10, 514, -62, 442, -116,
      -53, -62, -44, 28, 1, -8, -71, -62, -80, 10, -8, 64, -80, -8, -512, 64,
      -62, 28, 10, 1, -62, 10, -8, 1, 64, -8, 10, 1, -71, 568, 505, 514, 496,
      -62, 1, -8, 10, 1, 64, -8, -134, -44, 19, -71, -53, -62, -80, 10, 640,
      -80, 100, 10}, {-62, 28, 10, 1, -62, 10, 514, -44, -44, -116, -116, 136,
      100, 10, -53, -44, -62, 514, -53, -62, 442, -116, 28, -44, 496, -224,
      -80, -8, 496, -80, 568, 496, -8, 64, -80, 640, 640, 64, 64, -512, -512,
      4096, -80, -8, -8, 64, 64, -512, 505, -134, -134, 442, 442, -116, -71,
      -62, 1, -8, 10, -80, 19, -53, 10, -80, -62, 496, -134, -44, -62, 496, 28,
      -224, 10, 1, -62, 28, 10, -62, -80, -8, -8, 64, 64, -512, -71, -62, 1,
      -8, 10, -80, 1, 10, -8, 64, 1, -8, 19, -71, -134, -44, -62, -53, 10, 1,
      1, -8, -8, 64, 505, 514, -71, 568, -62, 496, 10, 100, -80, 640, 10, -80},
      {1, -8, 10, 1, 64, -8, -71, 568, 505, 514, 496, -62, 19, -71, -134, -44,
      -62, -53, 640, -80, -80, 10, 10, 100, -8, 64, -80, -8, -512, 64, -80,
      640, 496, 568, 64, -8, 496, -80, -224, 496, -8, -80, 4096, -512, -512,
      64, 64, 640, -62, 496, -134, -44, -224, 28, 10, -80, -62, -71, -8, 1,
      442, -116, -134, 505, 442, -134, 496, -62, -80, 10, -53, 19, -53, -62,
      442, -116, 28, -44, -62, 10, 28, -62, 1, 10, -116, 136, -44, 514, -116,
      -44, -62, 514, 10, 100, -44, -53, 64, -8, -8, 1, 1, 10, -512, 64, 64, -8,
      -8, -80, -80, 10, -8, 1, -62, -71, 10, -62, 1, 10, 28, -62}, {10, 1, 1,
      -8, -8, 64, 19, -71, -134, -44, -62, -53, -71, 568, 505, 514, 496, -62,
      -80, 640, 10, 100, -80, 10, -80, -8, -8, 64, 64, -512, 496, -80, -224,
      496, -8, -80, -80, 640, 496, 568, 64, -8, -512, 4096, 64, 640, -512, 64,
      -53, -62, 442, -116, 28, -44, -62, 10, 28, -62, 1, 10, -116, 136, -44,
      514, -116, -44, -62, 514, 10, 100, -44, -53, -62, 496, -134, -44, -224,
      28, 10, -80, -62, -71, -8, 1, 442, -116, -134, 505, 442, -134, 496, -62,
      -80, 10, -53, 19, -8, 64, 1, 10, -8, 1, 64, -512, -8, -80, 64, -8, 10,
      -62, 1, 10, 28, -62, -80, 10, -8, 1, -62, -71}, {10, -80, -62, -71, -8,
      1, -62, 496, -134, -44, -224, 28, -134, 505, 442, -116, -134, 442, -80,
      10, 496, -62, 19, -53, -80, 640, 496, 568, 64, -8, -8, 64, -80, -8, -512,
      64, -224, 496, 496, -80, -80, -8, -512, 64, 4096, -512, 640, 64, -71,
      568, 505, 514, 496, -62, 1, -8, 10, 1, 64, -8, -134, -44, 19, -71, -53,
      -62, -80, 10, 640, -80, 100, 10, -44, 514, -116, 136, -44, -116, 28, -62,
      -62, 10, 10, 1, 442, -116, -53, -62, -44, 28, -53, -44, 100, 10, 514,
      -62, -8, 1, -80, 10, -71, -62, 64, -8, -512, 64, -80, -8, -8, 1, 64, -8,
      10, 1, -62, 28, 10, 1, -62, 10}, {-62, 10, 28, -62, 1, 10, -53, -62, 442,
      -116, 28, -44, -44, 514, -116, 136, -44, -116, 10, 100, -62, 514, -53,
      -44, 496, -80, -224, 496, -8, -80, -80, -8, -8, 64, 64, -512, 496, 568,
      -80, 640, -8, 64, 64, 640, -512, 4096, 64, -512, 19, -71, -134, -44, -62,
      -53, 10, 1, 1, -8, -8, 64, 505, 514, -71, 568, -62, 496, 10, 100, -80,
      640, 10, -80, -134, 505, 442, -116, -134, 442, -62, -71, 10, -80, 1, -8,
      -134, -44, -62, 496, 28, -224, 19, -53, 10, -80, -62, 496, 1, 10, 10,
      -62, -62, 28, -8, -80, 64, -512, -8, 64, 1, 10, -8, 64, 1, -8, -71, -62,
      1, -8, 10, -80}, {-62, -71, 10, -80, 1, -8, -134, 505, 442, -116, -134,
      442, -62, 496, -134, -44, -224, 28, 10, -80, 19, -53, 496, -62, 496, 568,
      -80, 640, -8, 64, -224, 496, 496, -80, -80, -8, -8, 64, -80, -8, -512,
      64, 64, -512, 640, 64, 4096, -512, -44, 514, -116, 136, -44, -116, 28,
      -62, -62, 10, 10, 1, 442, -116, -53, -62, -44, 28, -53, -44, 100, 10,
      514, -62, -71, 568, 505, 514, 496, -62, 1, -8, 10, 1, 64, -8, -134, -44,
      19, -71, -53, -62, -80, 10, 640, -80, 100, 10, 1, -8, -71, -62, -80, 10,
      -8, 64, -80, -8, -512, 64, -62, 28, 10, 1, -62, 10, -8, 1, 64, -8, 10,
      1}, {28, -62, -62, 10, 10, 1, -44, 514, -116, 136, -44, -116, -53, -62,
      442, -116, 28, -44, 100, 10, -53, -44, -62, 514, -224, 496, 496, -80,
      -80, -8, 496, 568, -80, 640, -8, 64, -80, -8, -8, 64, 64, -512, 640, 64,
      64, -512, -512, 4096, -134, 505, 442, -116, -134, 442, -62, -71, 10, -80,
      1, -8, -134, -44, -62, 496, 28, -224, 19, -53, 10, -80, -62, 496, 19,
      -71, -134, -44, -62, -53, 10, 1, 1, -8, -8, 64, 505, 514, -71, 568, -62,
      496, 10, 100, -80, 640, 10, -80, 10, 1, -62, 28, 10, -62, -80, -8, -8,
      64, 64, -512, -71, -62, 1, -8, 10, -80, 1, 10, -8, 64, 1, -8}, {64, -8,
      -8, 1, 1, 10, -512, 64, 64, -8, -8, -80, -80, 10, -8, 1, -62, -71, 10,
      -62, 1, 10, 28, -62, 640, -80, -80, 10, 10, 100, 64, -8, -8, 1, 1, 10,
      496, -62, 568, -71, 514, 505, -62, -53, -71, 19, -44, -134, 4096, -512,
      -512, 64, 64, 640, -512, 64, 64, -8, -8, -80, 64, -8, 640, -80, 568, 496,
      -8, -80, -80, 496, 496, -224, 496, -62, -80, 10, -53, 19, -224, 28, 496,
      -62, -44, -134, -8, 1, -80, 10, -71, -62, 442, -134, -116, 442, 505,
      -134, -62, 514, 10, 100, -44, -53, 28, -44, -62, -53, -116, 442, 1, 10,
      10, -62, -62, 28, -116, -44, 136, -116, 514, -44}, {-8, 64, 1, 10, -8, 1,
      64, -512, -8, -80, 64, -8, 10, -62, 1, 10, 28, -62, -80, 10, -8, 1, -62,
      -71, -80, 640, 10, 100, -80, 10, -8, 64, 1, 10, -8, 1, -62, -53, -71, 19,
      -44, -134, 496, -62, 568, -71, 514, 505, -512, 4096, 64, 640, -512, 64,
      64, -512, -8, -80, 64, -8, -8, -80, -80, 496, 496, -224, 64, -8, 640,
      -80, 568, 496, -62, 514, 10, 100, -44, -53, 28, -44, -62, -53, -116, 442,
      1, 10, 10, -62, -62, 28, -116, -44, 136, -116, 514, -44, 496, -62, -80,
      10, -53, 19, -224, 28, 496, -62, -44, -134, -8, 1, -80, 10, -71, -62,
      442, -134, -116, 442, 505, -134}, {-8, 1, -80, 10, -71, -62, 64, -8,
      -512, 64, -80, -8, -8, 1, 64, -8, 10, 1, -62, 28, 10, 1, -62, 10, -80,
      10, 496, -62, 19, -53, -8, 1, -80, 10, -71, -62, -224, 28, 496, -62, -44,
      -134, -134, 442, 505, -134, -116, 442, -512, 64, 4096, -512, 640, 64, 64,
      -8, 640, -80, 568, 496, -512, 64, 64, -8, -8, -80, -80, -8, 496, -224,
      -80, 496, -80, 10, 640, -80, 100, 10, 496, -62, 568, -71, 514, 505, 64,
      -8, -8, 1, 1, 10, -53, -62, -44, -134, -71, 19, -53, -44, 100, 10, 514,
      -62, -44, -116, 514, -44, 136, -116, 10, 1, -62, 28, 10, -62, -44, 28,
      -116, 442, -62, -53}, {1, 10, 10, -62, -62, 28, -8, -80, 64, -512, -8,
      64, 1, 10, -8, 64, 1, -8, -71, -62, 1, -8, 10, -80, 10, 100, -62, 514,
      -53, -44, 1, 10, 10, -62, -62, 28, 28, -44, -62, -53, -116, 442, -44,
      -116, 514, -44, 136, -116, 64, 640, -512, 4096, 64, -512, -8, -80, -80,
      496, 496, -224, 64, -512, -8, -80, 64, -8, -8, 64, 568, 496, 640, -80,
      10, 100, -80, 640, 10, -80, -62, -53, -71, 19, -44, -134, -8, 64, 1, 10,
      -8, 1, -62, 496, 514, 505, 568, -71, 19, -53, 10, -80, -62, 496, -134,
      442, 505, -134, -116, 442, 1, -8, -71, -62, -80, 10, 28, -224, -44, -134,
      496, -62}, {1, -8, -71, -62, -80, 10, -8, 64, -80, -8, -512, 64, -62, 28,
      10, 1, -62, 10, -8, 1, 64, -8, 10, 1, 10, -80, 19, -53, 496, -62, 1, -8,
      -71, -62, -80, 10, -134, 442, 505, -134, -116, 442, -224, 28, 496, -62,
      -44, -134, 64, -512, 640, 64, 4096, -512, -8, 64, 568, 496, 640, -80,
      -80, -8, 496, -224, -80, 496, -512, 64, 64, -8, -8, -80, -53, -44, 100,
      10, 514, -62, -44, -116, 514, -44, 136, -116, 10, 1, -62, 28, 10, -62,
      -44, 28, -116, 442, -62, -53, -80, 10, 640, -80, 100, 10, 496, -62, 568,
      -71, 514, 505, 64, -8, -8, 1, 1, 10, -53, -62, -44, -134, -71, 19}, {10,
      1, -62, 28, 10, -62, -80, -8, -8, 64, 64, -512, -71, -62, 1, -8, 10, -80,
      1, 10, -8, 64, 1, -8, 100, 10, -53, -44, -62, 514, 10, 1, -62, 28, 10,
      -62, -44, -116, 514, -44, 136, -116, 28, -44, -62, -53, -116, 442, 640,
      64, 64, -512, -512, 4096, -80, -8, 496, -224, -80, 496, -8, 64, 568, 496,
      640, -80, 64, -512, -8, -80, 64, -8, 19, -53, 10, -80, -62, 496, -134,
      442, 505, -134, -116, 442, 1, -8, -71, -62, -80, 10, 28, -224, -44, -134,
      496, -62, 10, 100, -80, 640, 10, -80, -62, -53, -71, 19, -44, -134, -8,
      64, 1, 10, -8, 1, -62, 496, 514, 505, 568, -71}, {640, -80, -80, 10, 10,
      100, 64, -8, -8, 1, 1, 10, 496, -62, 568, -71, 514, 505, -62, -53, -71,
      19, -44, -134, 64, -8, -8, 1, 1, 10, -512, 64, 64, -8, -8, -80, -80, 10,
      -8, 1, -62, -71, 10, -62, 1, 10, 28, -62, -512, 64, 64, -8, -8, -80,
      4096, -512, -512, 64, 64, 640, 640, -80, 64, -8, 496, 568, -80, 496, -8,
      -80, -224, 496, -224, 28, 496, -62, -44, -134, 496, -62, -80, 10, -53,
      19, -80, 10, -8, 1, -62, -71, -116, 442, 442, -134, -134, 505, 28, -44,
      -62, -53, -116, 442, -62, 514, 10, 100, -44, -53, 10, -62, 1, 10, 28,
      -62, 136, -116, -116, -44, -44, 514}, {-80, 640, 10, 100, -80, 10, -8,
      64, 1, 10, -8, 1, -62, -53, -71, 19, -44, -134, 496, -62, 568, -71, 514,
      505, -8, 64, 1, 10, -8, 1, 64, -512, -8, -80, 64, -8, 10, -62, 1, 10, 28,
      -62, -80, 10, -8, 1, -62, -71, 64, -512, -8, -80, 64, -8, -512, 4096, 64,
      640, -512, 64, -80, 496, -8, -80, -224, 496, 640, -80, 64, -8, 496, 568,
      28, -44, -62, -53, -116, 442, -62, 514, 10, 100, -44, -53, 10, -62, 1,
      10, 28, -62, 136, -116, -116, -44, -44, 514, -224, 28, 496, -62, -44,
      -134, 496, -62, -80, 10, -53, 19, -80, 10, -8, 1, -62, -71, -116, 442,
      442, -134, -134, 505}, {-80, 10, 496, -62, 19, -53, -8, 1, -80, 10, -71,
      -62, -224, 28, 496, -62, -44, -134, -134, 442, 505, -134, -116, 442, -8,
      1, -80, 10, -71, -62, 64, -8, -512, 64, -80, -8, -8, 1, 64, -8, 10, 1,
      -62, 28, 10, 1, -62, 10, 64, -8, 640, -80, 568, 496, -512, 64, 4096,
      -512, 640, 64, 64, -8, -512, 64, -80, -8, 496, -224, -80, -8, 496, -80,
      496, -62, 568, -71, 514, 505, -80, 10, 640, -80, 100, 10, -8, 1, 64, -8,
      10, 1, -44, -134, -53, -62, 19, -71, -44, -116, 514, -44, 136, -116, -53,
      -44, 100, 10, 514, -62, -62, 28, 10, 1, -62, 10, -116, 442, -44, 28, -53,
      -62}, {10, 100, -62, 514, -53, -44, 1, 10, 10, -62, -62, 28, 28, -44,
      -62, -53, -116, 442, -44, -116, 514, -44, 136, -116, 1, 10, 10, -62, -62,
      28, -8, -80, 64, -512, -8, 64, 1, 10, -8, 64, 1, -8, -71, -62, 1, -8, 10,
      -80, -8, -80, -80, 496, 496, -224, 64, 640, -512, 4096, 64, -512, -8,
      -80, 64, -512, -8, 64, 568, 496, -8, 64, -80, 640, -62, -53, -71, 19,
      -44, -134, 10, 100, -80, 640, 10, -80, 1, 10, -8, 64, 1, -8, 514, 505,
      -62, 496, -71, 568, -134, 442, 505, -134, -116, 442, 19, -53, 10, -80,
      -62, 496, -71, -62, 1, -8, 10, -80, -44, -134, 28, -224, -62, 496}, {10,
      -80, 19, -53, 496, -62, 1, -8, -71, -62, -80, 10, -134, 442, 505, -134,
      -116, 442, -224, 28, 496, -62, -44, -134, 1, -8, -71, -62, -80, 10, -8,
      64, -80, -8, -512, 64, -62, 28, 10, 1, -62, 10, -8, 1, 64, -8, 10, 1, -8,
      64, 568, 496, 640, -80, 64, -512, 640, 64, 4096, -512, 496, -224, -80,
      -8, 496, -80, 64, -8, -512, 64, -80, -8, -44, -116, 514, -44, 136, -116,
      -53, -44, 100, 10, 514, -62, -62, 28, 10, 1, -62, 10, -116, 442, -44, 28,
      -53, -62, 496, -62, 568, -71, 514, 505, -80, 10, 640, -80, 100, 10, -8,
      1, 64, -8, 10, 1, -44, -134, -53, -62, 19, -71}, {100, 10, -53, -44, -62,
      514, 10, 1, -62, 28, 10, -62, -44, -116, 514, -44, 136, -116, 28, -44,
      -62, -53, -116, 442, 10, 1, -62, 28, 10, -62, -80, -8, -8, 64, 64, -512,
      -71, -62, 1, -8, 10, -80, 1, 10, -8, 64, 1, -8, -80, -8, 496, -224, -80,
      496, 640, 64, 64, -512, -512, 4096, 568, 496, -8, 64, -80, 640, -8, -80,
      64, -512, -8, 64, -134, 442, 505, -134, -116, 442, 19, -53, 10, -80, -62,
      496, -71, -62, 1, -8, 10, -80, -44, -134, 28, -224, -62, 496, -62, -53,
      -71, 19, -44, -134, 10, 100, -80, 640, 10, -80, 1, 10, -8, 64, 1, -8,
      514, 505, -62, 496, -71, 568}, {568, -71, 496, -62, 505, 514, -8, 1, 64,
      -8, 10, 1, -80, 10, 640, -80, 100, 10, -134, -44, 19, -71, -53, -62, 496,
      -62, -224, 28, -134, -44, -80, 10, -8, 1, -62, -71, 496, -62, -80, 10,
      -53, 19, 442, -116, -134, 505, 442, -134, 64, -8, -512, 64, -80, -8, 640,
      -80, 64, -8, 496, 568, 4096, -512, -512, 64, 64, 640, 496, -80, -224,
      496, -8, -80, -8, 1, 64, -8, 10, 1, -80, 10, -8, 1, -62, -71, -512, 64,
      64, -8, -8, -80, -62, 10, 28, -62, 1, 10, 442, -116, -53, -62, -44, 28,
      -116, 136, -44, 514, -116, -44, -62, 10, 28, -62, 1, 10, 514, -62, -44,
      -53, 10, 100}, {-71, 19, -62, -53, -134, -44, 1, 10, -8, 64, 1, -8, 10,
      100, -80, 640, 10, -80, 505, 514, -71, 568, -62, 496, -62, -53, 28, -44,
      442, -116, 10, -62, 1, 10, 28, -62, -62, 514, 10, 100, -44, -53, -116,
      136, -44, 514, -116, -44, -8, -80, 64, -512, -8, 64, -80, 496, -8, -80,
      -224, 496, -512, 4096, 64, 640, -512, 64, -80, 640, 496, 568, 64, -8, 1,
      10, -8, 64, 1, -8, 10, -62, 1, 10, 28, -62, 64, -512, -8, -80, 64, -8,
      10, -80, -62, -71, -8, 1, -134, -44, -62, 496, 28, -224, 442, -116, -134,
      505, 442, -134, 10, -80, -62, -71, -8, 1, -62, 496, -53, 19, -80, 10},
      {496, -62, -224, 28, -134, -44, -80, 10, -8, 1, -62, -71, 496, -62, -80,
      10, -53, 19, 442, -116, -134, 505, 442, -134, 568, -71, 496, -62, 505,
      514, -8, 1, 64, -8, 10, 1, -80, 10, 640, -80, 100, 10, -134, -44, 19,
      -71, -53, -62, 640, -80, 64, -8, 496, 568, 64, -8, -512, 64, -80, -8,
      -512, 64, 4096, -512, 640, 64, -224, 496, 496, -80, -80, -8, -80, 10, -8,
      1, -62, -71, -8, 1, 64, -8, 10, 1, 64, -8, -512, 64, -80, -8, 28, -62,
      -62, 10, 10, 1, -116, 136, -44, 514, -116, -44, 442, -116, -53, -62, -44,
      28, 28, -62, -62, 10, 10, 1, -44, -53, 514, -62, 100, 10}, {-62, -53, 28,
      -44, 442, -116, 10, -62, 1, 10, 28, -62, -62, 514, 10, 100, -44, -53,
      -116, 136, -44, 514, -116, -44, -71, 19, -62, -53, -134, -44, 1, 10, -8,
      64, 1, -8, 10, 100, -80, 640, 10, -80, 505, 514, -71, 568, -62, 496, -80,
      496, -8, -80, -224, 496, -8, -80, 64, -512, -8, 64, 64, 640, -512, 4096,
      64, -512, 496, 568, -80, 640, -8, 64, 10, -62, 1, 10, 28, -62, 1, 10, -8,
      64, 1, -8, -8, -80, 64, -512, -8, 64, -62, -71, 10, -80, 1, -8, 442,
      -116, -134, 505, 442, -134, -134, -44, -62, 496, 28, -224, -62, -71, 10,
      -80, 1, -8, -53, 19, -62, 496, 10, -80}, {505, -134, -134, 442, 442,
      -116, -71, -62, 1, -8, 10, -80, 19, -53, 10, -80, -62, 496, -134, -44,
      -62, 496, 28, -224, 514, -44, -44, -116, -116, 136, -62, 28, 10, 1, -62,
      10, -53, -44, 100, 10, 514, -62, 442, -116, -53, -62, -44, 28, 568, 496,
      -8, 64, -80, 640, 496, -224, -80, -8, 496, -80, 64, -512, 640, 64, 4096,
      -512, -8, 64, -80, -8, -512, 64, -71, -62, 1, -8, 10, -80, -62, 28, 10,
      1, -62, 10, -8, 64, -80, -8, -512, 64, 1, -8, 10, 1, 64, -8, 505, 514,
      -71, 568, -62, 496, -134, -44, 19, -71, -53, -62, 1, -8, 10, 1, 64, -8,
      10, -80, 100, 10, 640, -80}, {514, -44, -44, -116, -116, 136, -62, 28,
      10, 1, -62, 10, -53, -44, 100, 10, 514, -62, 442, -116, -53, -62, -44,
      28, 505, -134, -134, 442, 442, -116, -71, -62, 1, -8, 10, -80, 19, -53,
      10, -80, -62, 496, -134, -44, -62, 496, 28, -224, 496, -224, -80, -8,
      496, -80, 568, 496, -8, 64, -80, 640, 640, 64, 64, -512, -512, 4096, -80,
      -8, -8, 64, 64, -512, -62, 28, 10, 1, -62, 10, -71, -62, 1, -8, 10, -80,
      -80, -8, -8, 64, 64, -512, 10, 1, 1, -8, -8, 64, -134, -44, 19, -71, -53,
      -62, 505, 514, -71, 568, -62, 496, 10, 1, 1, -8, -8, 64, 100, 10, 10,
      -80, -80, 640}, {-71, 568, 505, 514, 496, -62, 1, -8, 10, 1, 64, -8,
      -134, -44, 19, -71, -53, -62, -80, 10, 640, -80, 100, 10, -62, 496, -134,
      -44, -224, 28, 10, -80, -62, -71, -8, 1, 442, -116, -134, 505, 442, -134,
      496, -62, -80, 10, -53, 19, -8, 64, -80, -8, -512, 64, -80, 640, 496,
      568, 64, -8, 496, -80, -224, 496, -8, -80, 4096, -512, -512, 64, 64, 640,
      442, -116, -53, -62, -44, 28, -116, 136, -44, 514, -116, -44, -62, 10,
      28, -62, 1, 10, 514, -62, -44, -53, 10, 100, -8, 1, 64, -8, 10, 1, -80,
      10, -8, 1, -62, -71, -512, 64, 64, -8, -8, -80, -62, 10, 28, -62, 1, 10},
      {19, -71, -134, -44, -62, -53, 10, 1, 1, -8, -8, 64, 505, 514, -71, 568,
      -62, 496, 10, 100, -80, 640, 10, -80, -53, -62, 442, -116, 28, -44, -62,
      10, 28, -62, 1, 10, -116, 136, -44, 514, -116, -44, -62, 514, 10, 100,
      -44, -53, -80, -8, -8, 64, 64, -512, 496, -80, -224, 496, -8, -80, -80,
      640, 496, 568, 64, -8, -512, 4096, 64, 640, -512, 64, -134, -44, -62,
      496, 28, -224, 442, -116, -134, 505, 442, -134, 10, -80, -62, -71, -8, 1,
      -62, 496, -53, 19, -80, 10, 1, 10, -8, 64, 1, -8, 10, -62, 1, 10, 28,
      -62, 64, -512, -8, -80, 64, -8, 10, -80, -62, -71, -8, 1}, {-62, 496,
      -134, -44, -224, 28, 10, -80, -62, -71, -8, 1, 442, -116, -134, 505, 442,
      -134, 496, -62, -80, 10, -53, 19, -71, 568, 505, 514, 496, -62, 1, -8,
      10, 1, 64, -8, -134, -44, 19, -71, -53, -62, -80, 10, 640, -80, 100, 10,
      -80, 640, 496, 568, 64, -8, -8, 64, -80, -8, -512, 64, -224, 496, 496,
      -80, -80, -8, -512, 64, 4096, -512, 640, 64, -116, 136, -44, 514, -116,
      -44, 442, -116, -53, -62, -44, 28, 28, -62, -62, 10, 10, 1, -44, -53,
      514, -62, 100, 10, -80, 10, -8, 1, -62, -71, -8, 1, 64, -8, 10, 1, 64,
      -8, -512, 64, -80, -8, 28, -62, -62, 10, 10, 1}, {-53, -62, 442, -116,
      28, -44, -62, 10, 28, -62, 1, 10, -116, 136, -44, 514, -116, -44, -62,
      514, 10, 100, -44, -53, 19, -71, -134, -44, -62, -53, 10, 1, 1, -8, -8,
      64, 505, 514, -71, 568, -62, 496, 10, 100, -80, 640, 10, -80, 496, -80,
      -224, 496, -8, -80, -80, -8, -8, 64, 64, -512, 496, 568, -80, 640, -8,
      64, 64, 640, -512, 4096, 64, -512, 442, -116, -134, 505, 442, -134, -134,
      -44, -62, 496, 28, -224, -62, -71, 10, -80, 1, -8, -53, 19, -62, 496, 10,
      -80, 10, -62, 1, 10, 28, -62, 1, 10, -8, 64, 1, -8, -8, -80, 64, -512,
      -8, 64, -62, -71, 10, -80, 1, -8}, {-134, 505, 442, -116, -134, 442, -62,
      -71, 10, -80, 1, -8, -134, -44, -62, 496, 28, -224, 19, -53, 10, -80,
      -62, 496, -44, 514, -116, 136, -44, -116, 28, -62, -62, 10, 10, 1, 442,
      -116, -53, -62, -44, 28, -53, -44, 100, 10, 514, -62, 496, 568, -80, 640,
      -8, 64, -224, 496, 496, -80, -80, -8, -8, 64, -80, -8, -512, 64, 64,
      -512, 640, 64, 4096, -512, 505, 514, -71, 568, -62, 496, -134, -44, 19,
      -71, -53, -62, 1, -8, 10, 1, 64, -8, 10, -80, 100, 10, 640, -80, -71,
      -62, 1, -8, 10, -80, -62, 28, 10, 1, -62, 10, -8, 64, -80, -8, -512, 64,
      1, -8, 10, 1, 64, -8}, {-44, 514, -116, 136, -44, -116, 28, -62, -62, 10,
      10, 1, 442, -116, -53, -62, -44, 28, -53, -44, 100, 10, 514, -62, -134,
      505, 442, -116, -134, 442, -62, -71, 10, -80, 1, -8, -134, -44, -62, 496,
      28, -224, 19, -53, 10, -80, -62, 496, -224, 496, 496, -80, -80, -8, 496,
      568, -80, 640, -8, 64, -80, -8, -8, 64, 64, -512, 640, 64, 64, -512,
      -512, 4096, -134, -44, 19, -71, -53, -62, 505, 514, -71, 568, -62, 496,
      10, 1, 1, -8, -8, 64, 100, 10, 10, -80, -80, 640, -62, 28, 10, 1, -62,
      10, -71, -62, 1, -8, 10, -80, -80, -8, -8, 64, 64, -512, 10, 1, 1, -8,
      -8, 64}, {-8, 1, 64, -8, 10, 1, -80, 10, -8, 1, -62, -71, -512, 64, 64,
      -8, -8, -80, -62, 10, 28, -62, 1, 10, -80, 10, 640, -80, 100, 10, 496,
      -62, 568, -71, 514, 505, 64, -8, -8, 1, 1, 10, -53, -62, -44, -134, -71,
      19, 496, -62, -80, 10, -53, 19, -224, 28, 496, -62, -44, -134, -8, 1,
      -80, 10, -71, -62, 442, -134, -116, 442, 505, -134, 4096, -512, -512, 64,
      64, 640, -512, 64, 64, -8, -8, -80, 64, -8, 640, -80, 568, 496, -8, -80,
      -80, 496, 496, -224, 514, -62, -44, -53, 10, 100, -44, 28, -116, 442,
      -62, -53, -116, -44, 136, -116, 514, -44, 1, 10, 10, -62, -62, 28}, {1,
      10, -8, 64, 1, -8, 10, -62, 1, 10, 28, -62, 64, -512, -8, -80, 64, -8,
      10, -80, -62, -71, -8, 1, 10, 100, -80, 640, 10, -80, -62, -53, -71, 19,
      -44, -134, -8, 64, 1, 10, -8, 1, -62, 496, 514, 505, 568, -71, -62, 514,
      10, 100, -44, -53, 28, -44, -62, -53, -116, 442, 1, 10, 10, -62, -62, 28,
      -116, -44, 136, -116, 514, -44, -512, 4096, 64, 640, -512, 64, 64, -512,
      -8, -80, 64, -8, -8, -80, -80, 496, 496, -224, 64, -8, 640, -80, 568,
      496, -62, 496, -53, 19, -80, 10, 28, -224, -44, -134, 496, -62, 442,
      -134, -116, 442, 505, -134, -8, 1, -80, 10, -71, -62}, {-80, 10, -8, 1,
      -62, -71, -8, 1, 64, -8, 10, 1, 64, -8, -512, 64, -80, -8, 28, -62, -62,
      10, 10, 1, 496, -62, -80, 10, -53, 19, -224, 28, 496, -62, -44, -134, -8,
      1, -80, 10, -71, -62, 442, -134, -116, 442, 505, -134, -80, 10, 640, -80,
      100, 10, 496, -62, 568, -71, 514, 505, 64, -8, -8, 1, 1, 10, -53, -62,
      -44, -134, -71, 19, -512, 64, 4096, -512, 640, 64, 64, -8, 640, -80, 568,
      496, -512, 64, 64, -8, -8, -80, -80, -8, 496, -224, -80, 496, -44, -53,
      514, -62, 100, 10, -116, -44, 136, -116, 514, -44, -44, 28, -116, 442,
      -62, -53, 10, 1, -62, 28, 10, -62}, {10, -62, 1, 10, 28, -62, 1, 10, -8,
      64, 1, -8, -8, -80, 64, -512, -8, 64, -62, -71, 10, -80, 1, -8, -62, 514,
      10, 100, -44, -53, 28, -44, -62, -53, -116, 442, 1, 10, 10, -62, -62, 28,
      -116, -44, 136, -116, 514, -44, 10, 100, -80, 640, 10, -80, -62, -53,
      -71, 19, -44, -134, -8, 64, 1, 10, -8, 1, -62, 496, 514, 505, 568, -71,
      64, 640, -512, 4096, 64, -512, -8, -80, -80, 496, 496, -224, 64, -512,
      -8, -80, 64, -8, -8, 64, 568, 496, 640, -80, -53, 19, -62, 496, 10, -80,
      442, -134, -116, 442, 505, -134, 28, -224, -44, -134, 496, -62, 1, -8,
      -71, -62, -80, 10}, {-71, -62, 1, -8, 10, -80, -62, 28, 10, 1, -62, 10,
      -8, 64, -80, -8, -512, 64, 1, -8, 10, 1, 64, -8, 19, -53, 10, -80, -62,
      496, -134, 442, 505, -134, -116, 442, 1, -8, -71, -62, -80, 10, 28, -224,
      -44, -134, 496, -62, -53, -44, 100, 10, 514, -62, -44, -116, 514, -44,
      136, -116, 10, 1, -62, 28, 10, -62, -44, 28, -116, 442, -62, -53, 64,
      -512, 640, 64, 4096, -512, -8, 64, 568, 496, 640, -80, -80, -8, 496,
      -224, -80, 496, -512, 64, 64, -8, -8, -80, 10, -80, 100, 10, 640, -80,
      -62, 496, 514, 505, 568, -71, -53, -62, -44, -134, -71, 19, 64, -8, -8,
      1, 1, 10}, {-62, 28, 10, 1, -62, 10, -71, -62, 1, -8, 10, -80, -80, -8,
      -8, 64, 64, -512, 10, 1, 1, -8, -8, 64, -53, -44, 100, 10, 514, -62, -44,
      -116, 514, -44, 136, -116, 10, 1, -62, 28, 10, -62, -44, 28, -116, 442,
      -62, -53, 19, -53, 10, -80, -62, 496, -134, 442, 505, -134, -116, 442, 1,
      -8, -71, -62, -80, 10, 28, -224, -44, -134, 496, -62, 640, 64, 64, -512,
      -512, 4096, -80, -8, 496, -224, -80, 496, -8, 64, 568, 496, 640, -80, 64,
      -512, -8, -80, 64, -8, 100, 10, 10, -80, -80, 640, -53, -62, -44, -134,
      -71, 19, -62, 496, 514, 505, 568, -71, -8, 64, 1, 10, -8, 1}, {-80, 10,
      640, -80, 100, 10, 496, -62, 568, -71, 514, 505, 64, -8, -8, 1, 1, 10,
      -53, -62, -44, -134, -71, 19, -8, 1, 64, -8, 10, 1, -80, 10, -8, 1, -62,
      -71, -512, 64, 64, -8, -8, -80, -62, 10, 28, -62, 1, 10, -224, 28, 496,
      -62, -44, -134, 496, -62, -80, 10, -53, 19, -80, 10, -8, 1, -62, -71,
      -116, 442, 442, -134, -134, 505, -512, 64, 64, -8, -8, -80, 4096, -512,
      -512, 64, 64, 640, 640, -80, 64, -8, 496, 568, -80, 496, -8, -80, -224,
      496, -44, 28, -116, 442, -62, -53, 514, -62, -44, -53, 10, 100, 136,
      -116, -116, -44, -44, 514, 10, -62, 1, 10, 28, -62}, {10, 100, -80, 640,
      10, -80, -62, -53, -71, 19, -44, -134, -8, 64, 1, 10, -8, 1, -62, 496,
      514, 505, 568, -71, 1, 10, -8, 64, 1, -8, 10, -62, 1, 10, 28, -62, 64,
      -512, -8, -80, 64, -8, 10, -80, -62, -71, -8, 1, 28, -44, -62, -53, -116,
      442, -62, 514, 10, 100, -44, -53, 10, -62, 1, 10, 28, -62, 136, -116,
      -116, -44, -44, 514, 64, -512, -8, -80, 64, -8, -512, 4096, 64, 640,
      -512, 64, -80, 496, -8, -80, -224, 496, 640, -80, 64, -8, 496, 568, 28,
      -224, -44, -134, 496, -62, -62, 496, -53, 19, -80, 10, -116, 442, 442,
      -134, -134, 505, -80, 10, -8, 1, -62, -71}, {496, -62, -80, 10, -53, 19,
      -224, 28, 496, -62, -44, -134, -8, 1, -80, 10, -71, -62, 442, -134, -116,
      442, 505, -134, -80, 10, -8, 1, -62, -71, -8, 1, 64, -8, 10, 1, 64, -8,
      -512, 64, -80, -8, 28, -62, -62, 10, 10, 1, 496, -62, 568, -71, 514, 505,
      -80, 10, 640, -80, 100, 10, -8, 1, 64, -8, 10, 1, -44, -134, -53, -62,
      19, -71, 64, -8, 640, -80, 568, 496, -512, 64, 4096, -512, 640, 64, 64,
      -8, -512, 64, -80, -8, 496, -224, -80, -8, 496, -80, -116, -44, 136,
      -116, 514, -44, -44, -53, 514, -62, 100, 10, -116, 442, -44, 28, -53,
      -62, -62, 28, 10, 1, -62, 10}, {-62, 514, 10, 100, -44, -53, 28, -44,
      -62, -53, -116, 442, 1, 10, 10, -62, -62, 28, -116, -44, 136, -116, 514,
      -44, 10, -62, 1, 10, 28, -62, 1, 10, -8, 64, 1, -8, -8, -80, 64, -512,
      -8, 64, -62, -71, 10, -80, 1, -8, -62, -53, -71, 19, -44, -134, 10, 100,
      -80, 640, 10, -80, 1, 10, -8, 64, 1, -8, 514, 505, -62, 496, -71, 568,
      -8, -80, -80, 496, 496, -224, 64, 640, -512, 4096, 64, -512, -8, -80, 64,
      -512, -8, 64, 568, 496, -8, 64, -80, 640, 442, -134, -116, 442, 505,
      -134, -53, 19, -62, 496, 10, -80, -44, -134, 28, -224, -62, 496, -71,
      -62, 1, -8, 10, -80}, {19, -53, 10, -80, -62, 496, -134, 442, 505, -134,
      -116, 442, 1, -8, -71, -62, -80, 10, 28, -224, -44, -134, 496, -62, -71,
      -62, 1, -8, 10, -80, -62, 28, 10, 1, -62, 10, -8, 64, -80, -8, -512, 64,
      1, -8, 10, 1, 64, -8, -44, -116, 514, -44, 136, -116, -53, -44, 100, 10,
      514, -62, -62, 28, 10, 1, -62, 10, -116, 442, -44, 28, -53, -62, -8, 64,
      568, 496, 640, -80, 64, -512, 640, 64, 4096, -512, 496, -224, -80, -8,
      496, -80, 64, -8, -512, 64, -80, -8, -62, 496, 514, 505, 568, -71, 10,
      -80, 100, 10, 640, -80, -44, -134, -53, -62, 19, -71, -8, 1, 64, -8, 10,
      1}, {-53, -44, 100, 10, 514, -62, -44, -116, 514, -44, 136, -116, 10, 1,
      -62, 28, 10, -62, -44, 28, -116, 442, -62, -53, -62, 28, 10, 1, -62, 10,
      -71, -62, 1, -8, 10, -80, -80, -8, -8, 64, 64, -512, 10, 1, 1, -8, -8,
      64, -134, 442, 505, -134, -116, 442, 19, -53, 10, -80, -62, 496, -71,
      -62, 1, -8, 10, -80, -44, -134, 28, -224, -62, 496, -80, -8, 496, -224,
      -80, 496, 640, 64, 64, -512, -512, 4096, 568, 496, -8, 64, -80, 640, -8,
      -80, 64, -512, -8, 64, -53, -62, -44, -134, -71, 19, 100, 10, 10, -80,
      -80, 640, 514, 505, -62, 496, -71, 568, 1, 10, -8, 64, 1, -8}, {496, -62,
      568, -71, 514, 505, -80, 10, 640, -80, 100, 10, -8, 1, 64, -8, 10, 1,
      -44, -134, -53, -62, 19, -71, -224, 28, 496, -62, -44, -134, 496, -62,
      -80, 10, -53, 19, -80, 10, -8, 1, -62, -71, -116, 442, 442, -134, -134,
      505, -8, 1, 64, -8, 10, 1, -80, 10, -8, 1, -62, -71, -512, 64, 64, -8,
      -8, -80, -62, 10, 28, -62, 1, 10, 64, -8, -512, 64, -80, -8, 640, -80,
      64, -8, 496, 568, 4096, -512, -512, 64, 64, 640, 496, -80, -224, 496, -8,
      -80, -116, 442, -44, 28, -53, -62, 136, -116, -116, -44, -44, 514, 514,
      -62, -44, -53, 10, 100, -62, 10, 28, -62, 1, 10}, {-62, -53, -71, 19,
      -44, -134, 10, 100, -80, 640, 10, -80, 1, 10, -8, 64, 1, -8, 514, 505,
      -62, 496, -71, 568, 28, -44, -62, -53, -116, 442, -62, 514, 10, 100, -44,
      -53, 10, -62, 1, 10, 28, -62, 136, -116, -116, -44, -44, 514, 1, 10, -8,
      64, 1, -8, 10, -62, 1, 10, 28, -62, 64, -512, -8, -80, 64, -8, 10, -80,
      -62, -71, -8, 1, -8, -80, 64, -512, -8, 64, -80, 496, -8, -80, -224, 496,
      -512, 4096, 64, 640, -512, 64, -80, 640, 496, 568, 64, -8, -44, -134, 28,
      -224, -62, 496, -116, 442, 442, -134, -134, 505, -62, 496, -53, 19, -80,
      10, 10, -80, -62, -71, -8, 1}, {-224, 28, 496, -62, -44, -134, 496, -62,
      -80, 10, -53, 19, -80, 10, -8, 1, -62, -71, -116, 442, 442, -134, -134,
      505, 496, -62, 568, -71, 514, 505, -80, 10, 640, -80, 100, 10, -8, 1, 64,
      -8, 10, 1, -44, -134, -53, -62, 19, -71, -80, 10, -8, 1, -62, -71, -8, 1,
      64, -8, 10, 1, 64, -8, -512, 64, -80, -8, 28, -62, -62, 10, 10, 1, 640,
      -80, 64, -8, 496, 568, 64, -8, -512, 64, -80, -8, -512, 64, 4096, -512,
      640, 64, -224, 496, 496, -80, -80, -8, 136, -116, -116, -44, -44, 514,
      -116, 442, -44, 28, -53, -62, -44, -53, 514, -62, 100, 10, 28, -62, -62,
      10, 10, 1}, {28, -44, -62, -53, -116, 442, -62, 514, 10, 100, -44, -53,
      10, -62, 1, 10, 28, -62, 136, -116, -116, -44, -44, 514, -62, -53, -71,
      19, -44, -134, 10, 100, -80, 640, 10, -80, 1, 10, -8, 64, 1, -8, 514,
      505, -62, 496, -71, 568, 10, -62, 1, 10, 28, -62, 1, 10, -8, 64, 1, -8,
      -8, -80, 64, -512, -8, 64, -62, -71, 10, -80, 1, -8, -80, 496, -8, -80,
      -224, 496, -8, -80, 64, -512, -8, 64, 64, 640, -512, 4096, 64, -512, 496,
      568, -80, 640, -8, 64, -116, 442, 442, -134, -134, 505, -44, -134, 28,
      -224, -62, 496, -53, 19, -62, 496, 10, -80, -62, -71, 10, -80, 1, -8},
      {-134, 442, 505, -134, -116, 442, 19, -53, 10, -80, -62, 496, -71, -62,
      1, -8, 10, -80, -44, -134, 28, -224, -62, 496, -44, -116, 514, -44, 136,
      -116, -53, -44, 100, 10, 514, -62, -62, 28, 10, 1, -62, 10, -116, 442,
      -44, 28, -53, -62, -71, -62, 1, -8, 10, -80, -62, 28, 10, 1, -62, 10, -8,
      64, -80, -8, -512, 64, 1, -8, 10, 1, 64, -8, 568, 496, -8, 64, -80, 640,
      496, -224, -80, -8, 496, -80, 64, -512, 640, 64, 4096, -512, -8, 64, -80,
      -8, -512, 64, 514, 505, -62, 496, -71, 568, -44, -134, -53, -62, 19, -71,
      10, -80, 100, 10, 640, -80, 1, -8, 10, 1, 64, -8}, {-44, -116, 514, -44,
      136, -116, -53, -44, 100, 10, 514, -62, -62, 28, 10, 1, -62, 10, -116,
      442, -44, 28, -53, -62, -134, 442, 505, -134, -116, 442, 19, -53, 10,
      -80, -62, 496, -71, -62, 1, -8, 10, -80, -44, -134, 28, -224, -62, 496,
      -62, 28, 10, 1, -62, 10, -71, -62, 1, -8, 10, -80, -80, -8, -8, 64, 64,
      -512, 10, 1, 1, -8, -8, 64, 496, -224, -80, -8, 496, -80, 568, 496, -8,
      64, -80, 640, 640, 64, 64, -512, -512, 4096, -80, -8, -8, 64, 64, -512,
      -44, -134, -53, -62, 19, -71, 514, 505, -62, 496, -71, 568, 100, 10, 10,
      -80, -80, 640, 10, 1, 1, -8, -8, 64}, {505, 514, -71, 568, -62, 496,
      -134, -44, 19, -71, -53, -62, 1, -8, 10, 1, 64, -8, 10, -80, 100, 10,
      640, -80, -134, -44, -62, 496, 28, -224, 442, -116, -134, 505, 442, -134,
      10, -80, -62, -71, -8, 1, -62, 496, -53, 19, -80, 10, 442, -116, -53,
      -62, -44, 28, -116, 136, -44, 514, -116, -44, -62, 10, 28, -62, 1, 10,
      514, -62, -44, -53, 10, 100, -8, 64, -80, -8, -512, 64, -80, 640, 496,
      568, 64, -8, 496, -80, -224, 496, -8, -80, 4096, -512, -512, 64, 64, 640,
      1, -8, 10, 1, 64, -8, 10, -80, -62, -71, -8, 1, -62, 10, 28, -62, 1, 10,
      -512, 64, 64, -8, -8, -80}, {-134, -44, 19, -71, -53, -62, 505, 514, -71,
      568, -62, 496, 10, 1, 1, -8, -8, 64, 100, 10, 10, -80, -80, 640, 442,
      -116, -53, -62, -44, 28, -116, 136, -44, 514, -116, -44, -62, 10, 28,
      -62, 1, 10, 514, -62, -44, -53, 10, 100, -134, -44, -62, 496, 28, -224,
      442, -116, -134, 505, 442, -134, 10, -80, -62, -71, -8, 1, -62, 496, -53,
      19, -80, 10, -80, -8, -8, 64, 64, -512, 496, -80, -224, 496, -8, -80,
      -80, 640, 496, 568, 64, -8, -512, 4096, 64, 640, -512, 64, 10, 1, 1, -8,
      -8, 64, -62, 10, 28, -62, 1, 10, 10, -80, -62, -71, -8, 1, 64, -512, -8,
      -80, 64, -8}, {-134, -44, -62, 496, 28, -224, 442, -116, -134, 505, 442,
      -134, 10, -80, -62, -71, -8, 1, -62, 496, -53, 19, -80, 10, 505, 514,
      -71, 568, -62, 496, -134, -44, 19, -71, -53, -62, 1, -8, 10, 1, 64, -8,
      10, -80, 100, 10, 640, -80, -116, 136, -44, 514, -116, -44, 442, -116,
      -53, -62, -44, 28, 28, -62, -62, 10, 10, 1, -44, -53, 514, -62, 100, 10,
      -80, 640, 496, 568, 64, -8, -8, 64, -80, -8, -512, 64, -224, 496, 496,
      -80, -80, -8, -512, 64, 4096, -512, 640, 64, 10, -80, -62, -71, -8, 1, 1,
      -8, 10, 1, 64, -8, 28, -62, -62, 10, 10, 1, 64, -8, -512, 64, -80, -8},
      {442, -116, -53, -62, -44, 28, -116, 136, -44, 514, -116, -44, -62, 10,
      28, -62, 1, 10, 514, -62, -44, -53, 10, 100, -134, -44, 19, -71, -53,
      -62, 505, 514, -71, 568, -62, 496, 10, 1, 1, -8, -8, 64, 100, 10, 10,
      -80, -80, 640, 442, -116, -134, 505, 442, -134, -134, -44, -62, 496, 28,
      -224, -62, -71, 10, -80, 1, -8, -53, 19, -62, 496, 10, -80, 496, -80,
      -224, 496, -8, -80, -80, -8, -8, 64, 64, -512, 496, 568, -80, 640, -8,
      64, 64, 640, -512, 4096, 64, -512, -62, 10, 28, -62, 1, 10, 10, 1, 1, -8,
      -8, 64, -62, -71, 10, -80, 1, -8, -8, -80, 64, -512, -8, 64}, {442, -116,
      -134, 505, 442, -134, -134, -44, -62, 496, 28, -224, -62, -71, 10, -80,
      1, -8, -53, 19, -62, 496, 10, -80, -116, 136, -44, 514, -116, -44, 442,
      -116, -53, -62, -44, 28, 28, -62, -62, 10, 10, 1, -44, -53, 514, -62,
      100, 10, 505, 514, -71, 568, -62, 496, -134, -44, 19, -71, -53, -62, 1,
      -8, 10, 1, 64, -8, 10, -80, 100, 10, 640, -80, 496, 568, -80, 640, -8,
      64, -224, 496, 496, -80, -80, -8, -8, 64, -80, -8, -512, 64, 64, -512,
      640, 64, 4096, -512, -62, -71, 10, -80, 1, -8, 28, -62, -62, 10, 10, 1,
      1, -8, 10, 1, 64, -8, -8, 64, -80, -8, -512, 64}, {-116, 136, -44, 514,
      -116, -44, 442, -116, -53, -62, -44, 28, 28, -62, -62, 10, 10, 1, -44,
      -53, 514, -62, 100, 10, 442, -116, -134, 505, 442, -134, -134, -44, -62,
      496, 28, -224, -62, -71, 10, -80, 1, -8, -53, 19, -62, 496, 10, -80,
      -134, -44, 19, -71, -53, -62, 505, 514, -71, 568, -62, 496, 10, 1, 1, -8,
      -8, 64, 100, 10, 10, -80, -80, 640, -224, 496, 496, -80, -80, -8, 496,
      568, -80, 640, -8, 64, -80, -8, -8, 64, 64, -512, 640, 64, 64, -512,
      -512, 4096, 28, -62, -62, 10, 10, 1, -62, -71, 10, -80, 1, -8, 10, 1, 1,
      -8, -8, 64, -80, -8, -8, 64, 64, -512}, {1, -8, 10, 1, 64, -8, 10, -80,
      -62, -71, -8, 1, -62, 10, 28, -62, 1, 10, -512, 64, 64, -8, -8, -80, 10,
      -80, 100, 10, 640, -80, -62, 496, 514, 505, 568, -71, -53, -62, -44,
      -134, -71, 19, 64, -8, -8, 1, 1, 10, -62, 496, -53, 19, -80, 10, 28,
      -224, -44, -134, 496, -62, 442, -134, -116, 442, 505, -134, -8, 1, -80,
      10, -71, -62, 514, -62, -44, -53, 10, 100, -44, 28, -116, 442, -62, -53,
      -116, -44, 136, -116, 514, -44, 1, 10, 10, -62, -62, 28, 4096, -512,
      -512, 64, 64, 640, -512, 64, 64, -8, -8, -80, 64, -8, 640, -80, 568, 496,
      -8, -80, -80, 496, 496, -224}, {10, 1, 1, -8, -8, 64, -62, 10, 28, -62,
      1, 10, 10, -80, -62, -71, -8, 1, 64, -512, -8, -80, 64, -8, 100, 10, 10,
      -80, -80, 640, -53, -62, -44, -134, -71, 19, -62, 496, 514, 505, 568,
      -71, -8, 64, 1, 10, -8, 1, 514, -62, -44, -53, 10, 100, -44, 28, -116,
      442, -62, -53, -116, -44, 136, -116, 514, -44, 1, 10, 10, -62, -62, 28,
      -62, 496, -53, 19, -80, 10, 28, -224, -44, -134, 496, -62, 442, -134,
      -116, 442, 505, -134, -8, 1, -80, 10, -71, -62, -512, 4096, 64, 640,
      -512, 64, 64, -512, -8, -80, 64, -8, -8, -80, -80, 496, 496, -224, 64,
      -8, 640, -80, 568, 496}, {10, -80, -62, -71, -8, 1, 1, -8, 10, 1, 64, -8,
      28, -62, -62, 10, 10, 1, 64, -8, -512, 64, -80, -8, -62, 496, -53, 19,
      -80, 10, 28, -224, -44, -134, 496, -62, 442, -134, -116, 442, 505, -134,
      -8, 1, -80, 10, -71, -62, 10, -80, 100, 10, 640, -80, -62, 496, 514, 505,
      568, -71, -53, -62, -44, -134, -71, 19, 64, -8, -8, 1, 1, 10, -44, -53,
      514, -62, 100, 10, -116, -44, 136, -116, 514, -44, -44, 28, -116, 442,
      -62, -53, 10, 1, -62, 28, 10, -62, -512, 64, 4096, -512, 640, 64, 64, -8,
      640, -80, 568, 496, -512, 64, 64, -8, -8, -80, -80, -8, 496, -224, -80,
      496}, {-62, 10, 28, -62, 1, 10, 10, 1, 1, -8, -8, 64, -62, -71, 10, -80,
      1, -8, -8, -80, 64, -512, -8, 64, 514, -62, -44, -53, 10, 100, -44, 28,
      -116, 442, -62, -53, -116, -44, 136, -116, 514, -44, 1, 10, 10, -62, -62,
      28, 100, 10, 10, -80, -80, 640, -53, -62, -44, -134, -71, 19, -62, 496,
      514, 505, 568, -71, -8, 64, 1, 10, -8, 1, -53, 19, -62, 496, 10, -80,
      442, -134, -116, 442, 505, -134, 28, -224, -44, -134, 496, -62, 1, -8,
      -71, -62, -80, 10, 64, 640, -512, 4096, 64, -512, -8, -80, -80, 496, 496,
      -224, 64, -512, -8, -80, 64, -8, -8, 64, 568, 496, 640, -80}, {-62, -71,
      10, -80, 1, -8, 28, -62, -62, 10, 10, 1, 1, -8, 10, 1, 64, -8, -8, 64,
      -80, -8, -512, 64, -53, 19, -62, 496, 10, -80, 442, -134, -116, 442, 505,
      -134, 28, -224, -44, -134, 496, -62, 1, -8, -71, -62, -80, 10, -44, -53,
      514, -62, 100, 10, -116, -44, 136, -116, 514, -44, -44, 28, -116, 442,
      -62, -53, 10, 1, -62, 28, 10, -62, 10, -80, 100, 10, 640, -80, -62, 496,
      514, 505, 568, -71, -53, -62, -44, -134, -71, 19, 64, -8, -8, 1, 1, 10,
      64, -512, 640, 64, 4096, -512, -8, 64, 568, 496, 640, -80, -80, -8, 496,
      -224, -80, 496, -512, 64, 64, -8, -8, -80}, {28, -62, -62, 10, 10, 1,
      -62, -71, 10, -80, 1, -8, 10, 1, 1, -8, -8, 64, -80, -8, -8, 64, 64,
      -512, -44, -53, 514, -62, 100, 10, -116, -44, 136, -116, 514, -44, -44,
      28, -116, 442, -62, -53, 10, 1, -62, 28, 10, -62, -53, 19, -62, 496, 10,
      -80, 442, -134, -116, 442, 505, -134, 28, -224, -44, -134, 496, -62, 1,
      -8, -71, -62, -80, 10, 100, 10, 10, -80, -80, 640, -53, -62, -44, -134,
      -71, 19, -62, 496, 514, 505, 568, -71, -8, 64, 1, 10, -8, 1, 640, 64, 64,
      -512, -512, 4096, -80, -8, 496, -224, -80, 496, -8, 64, 568, 496, 640,
      -80, 64, -512, -8, -80, 64, -8}, {10, -80, 100, 10, 640, -80, -62, 496,
      514, 505, 568, -71, -53, -62, -44, -134, -71, 19, 64, -8, -8, 1, 1, 10,
      1, -8, 10, 1, 64, -8, 10, -80, -62, -71, -8, 1, -62, 10, 28, -62, 1, 10,
      -512, 64, 64, -8, -8, -80, 28, -224, -44, -134, 496, -62, -62, 496, -53,
      19, -80, 10, -116, 442, 442, -134, -134, 505, -80, 10, -8, 1, -62, -71,
      -44, 28, -116, 442, -62, -53, 514, -62, -44, -53, 10, 100, 136, -116,
      -116, -44, -44, 514, 10, -62, 1, 10, 28, -62, -512, 64, 64, -8, -8, -80,
      4096, -512, -512, 64, 64, 640, 640, -80, 64, -8, 496, 568, -80, 496, -8,
      -80, -224, 496}, {100, 10, 10, -80, -80, 640, -53, -62, -44, -134, -71,
      19, -62, 496, 514, 505, 568, -71, -8, 64, 1, 10, -8, 1, 10, 1, 1, -8, -8,
      64, -62, 10, 28, -62, 1, 10, 10, -80, -62, -71, -8, 1, 64, -512, -8, -80,
      64, -8, -44, 28, -116, 442, -62, -53, 514, -62, -44, -53, 10, 100, 136,
      -116, -116, -44, -44, 514, 10, -62, 1, 10, 28, -62, 28, -224, -44, -134,
      496, -62, -62, 496, -53, 19, -80, 10, -116, 442, 442, -134, -134, 505,
      -80, 10, -8, 1, -62, -71, 64, -512, -8, -80, 64, -8, -512, 4096, 64, 640,
      -512, 64, -80, 496, -8, -80, -224, 496, 640, -80, 64, -8, 496, 568},
      {-62, 496, -53, 19, -80, 10, 28, -224, -44, -134, 496, -62, 442, -134,
      -116, 442, 505, -134, -8, 1, -80, 10, -71, -62, 10, -80, -62, -71, -8, 1,
      1, -8, 10, 1, 64, -8, 28, -62, -62, 10, 10, 1, 64, -8, -512, 64, -80, -8,
      -62, 496, 514, 505, 568, -71, 10, -80, 100, 10, 640, -80, -44, -134, -53,
      -62, 19, -71, -8, 1, 64, -8, 10, 1, -116, -44, 136, -116, 514, -44, -44,
      -53, 514, -62, 100, 10, -116, 442, -44, 28, -53, -62, -62, 28, 10, 1,
      -62, 10, 64, -8, 640, -80, 568, 496, -512, 64, 4096, -512, 640, 64, 64,
      -8, -512, 64, -80, -8, 496, -224, -80, -8, 496, -80}, {514, -62, -44,
      -53, 10, 100, -44, 28, -116, 442, -62, -53, -116, -44, 136, -116, 514,
      -44, 1, 10, 10, -62, -62, 28, -62, 10, 28, -62, 1, 10, 10, 1, 1, -8, -8,
      64, -62, -71, 10, -80, 1, -8, -8, -80, 64, -512, -8, 64, -53, -62, -44,
      -134, -71, 19, 100, 10, 10, -80, -80, 640, 514, 505, -62, 496, -71, 568,
      1, 10, -8, 64, 1, -8, 442, -134, -116, 442, 505, -134, -53, 19, -62, 496,
      10, -80, -44, -134, 28, -224, -62, 496, -71, -62, 1, -8, 10, -80, -8,
      -80, -80, 496, 496, -224, 64, 640, -512, 4096, 64, -512, -8, -80, 64,
      -512, -8, 64, 568, 496, -8, 64, -80, 640}, {-53, 19, -62, 496, 10, -80,
      442, -134, -116, 442, 505, -134, 28, -224, -44, -134, 496, -62, 1, -8,
      -71, -62, -80, 10, -62, -71, 10, -80, 1, -8, 28, -62, -62, 10, 10, 1, 1,
      -8, 10, 1, 64, -8, -8, 64, -80, -8, -512, 64, -116, -44, 136, -116, 514,
      -44, -44, -53, 514, -62, 100, 10, -116, 442, -44, 28, -53, -62, -62, 28,
      10, 1, -62, 10, -62, 496, 514, 505, 568, -71, 10, -80, 100, 10, 640, -80,
      -44, -134, -53, -62, 19, -71, -8, 1, 64, -8, 10, 1, -8, 64, 568, 496,
      640, -80, 64, -512, 640, 64, 4096, -512, 496, -224, -80, -8, 496, -80,
      64, -8, -512, 64, -80, -8}, {-44, -53, 514, -62, 100, 10, -116, -44, 136,
      -116, 514, -44, -44, 28, -116, 442, -62, -53, 10, 1, -62, 28, 10, -62,
      28, -62, -62, 10, 10, 1, -62, -71, 10, -80, 1, -8, 10, 1, 1, -8, -8, 64,
      -80, -8, -8, 64, 64, -512, 442, -134, -116, 442, 505, -134, -53, 19, -62,
      496, 10, -80, -44, -134, 28, -224, -62, 496, -71, -62, 1, -8, 10, -80,
      -53, -62, -44, -134, -71, 19, 100, 10, 10, -80, -80, 640, 514, 505, -62,
      496, -71, 568, 1, 10, -8, 64, 1, -8, -80, -8, 496, -224, -80, 496, 640,
      64, 64, -512, -512, 4096, 568, 496, -8, 64, -80, 640, -8, -80, 64, -512,
      -8, 64}, {-62, 496, 514, 505, 568, -71, 10, -80, 100, 10, 640, -80, -44,
      -134, -53, -62, 19, -71, -8, 1, 64, -8, 10, 1, 28, -224, -44, -134, 496,
      -62, -62, 496, -53, 19, -80, 10, -116, 442, 442, -134, -134, 505, -80,
      10, -8, 1, -62, -71, 1, -8, 10, 1, 64, -8, 10, -80, -62, -71, -8, 1, -62,
      10, 28, -62, 1, 10, -512, 64, 64, -8, -8, -80, -116, 442, -44, 28, -53,
      -62, 136, -116, -116, -44, -44, 514, 514, -62, -44, -53, 10, 100, -62,
      10, 28, -62, 1, 10, 64, -8, -512, 64, -80, -8, 640, -80, 64, -8, 496,
      568, 4096, -512, -512, 64, 64, 640, 496, -80, -224, 496, -8, -80}, {-53,
      -62, -44, -134, -71, 19, 100, 10, 10, -80, -80, 640, 514, 505, -62, 496,
      -71, 568, 1, 10, -8, 64, 1, -8, -44, 28, -116, 442, -62, -53, 514, -62,
      -44, -53, 10, 100, 136, -116, -116, -44, -44, 514, 10, -62, 1, 10, 28,
      -62, 10, 1, 1, -8, -8, 64, -62, 10, 28, -62, 1, 10, 10, -80, -62, -71,
      -8, 1, 64, -512, -8, -80, 64, -8, -44, -134, 28, -224, -62, 496, -116,
      442, 442, -134, -134, 505, -62, 496, -53, 19, -80, 10, 10, -80, -62, -71,
      -8, 1, -8, -80, 64, -512, -8, 64, -80, 496, -8, -80, -224, 496, -512,
      4096, 64, 640, -512, 64, -80, 640, 496, 568, 64, -8}, {28, -224, -44,
      -134, 496, -62, -62, 496, -53, 19, -80, 10, -116, 442, 442, -134, -134,
      505, -80, 10, -8, 1, -62, -71, -62, 496, 514, 505, 568, -71, 10, -80,
      100, 10, 640, -80, -44, -134, -53, -62, 19, -71, -8, 1, 64, -8, 10, 1,
      10, -80, -62, -71, -8, 1, 1, -8, 10, 1, 64, -8, 28, -62, -62, 10, 10, 1,
      64, -8, -512, 64, -80, -8, 136, -116, -116, -44, -44, 514, -116, 442,
      -44, 28, -53, -62, -44, -53, 514, -62, 100, 10, 28, -62, -62, 10, 10, 1,
      640, -80, 64, -8, 496, 568, 64, -8, -512, 64, -80, -8, -512, 64, 4096,
      -512, 640, 64, -224, 496, 496, -80, -80, -8}, {-44, 28, -116, 442, -62,
      -53, 514, -62, -44, -53, 10, 100, 136, -116, -116, -44, -44, 514, 10,
      -62, 1, 10, 28, -62, -53, -62, -44, -134, -71, 19, 100, 10, 10, -80, -80,
      640, 514, 505, -62, 496, -71, 568, 1, 10, -8, 64, 1, -8, -62, 10, 28,
      -62, 1, 10, 10, 1, 1, -8, -8, 64, -62, -71, 10, -80, 1, -8, -8, -80, 64,
      -512, -8, 64, -116, 442, 442, -134, -134, 505, -44, -134, 28, -224, -62,
      496, -53, 19, -62, 496, 10, -80, -62, -71, 10, -80, 1, -8, -80, 496, -8,
      -80, -224, 496, -8, -80, 64, -512, -8, 64, 64, 640, -512, 4096, 64, -512,
      496, 568, -80, 640, -8, 64}, {442, -134, -116, 442, 505, -134, -53, 19,
      -62, 496, 10, -80, -44, -134, 28, -224, -62, 496, -71, -62, 1, -8, 10,
      -80, -116, -44, 136, -116, 514, -44, -44, -53, 514, -62, 100, 10, -116,
      442, -44, 28, -53, -62, -62, 28, 10, 1, -62, 10, -62, -71, 10, -80, 1,
      -8, 28, -62, -62, 10, 10, 1, 1, -8, 10, 1, 64, -8, -8, 64, -80, -8, -512,
      64, 514, 505, -62, 496, -71, 568, -44, -134, -53, -62, 19, -71, 10, -80,
      100, 10, 640, -80, 1, -8, 10, 1, 64, -8, 568, 496, -8, 64, -80, 640, 496,
      -224, -80, -8, 496, -80, 64, -512, 640, 64, 4096, -512, -8, 64, -80, -8,
      -512, 64}, {-116, -44, 136, -116, 514, -44, -44, -53, 514, -62, 100, 10,
      -116, 442, -44, 28, -53, -62, -62, 28, 10, 1, -62, 10, 442, -134, -116,
      442, 505, -134, -53, 19, -62, 496, 10, -80, -44, -134, 28, -224, -62,
      496, -71, -62, 1, -8, 10, -80, 28, -62, -62, 10, 10, 1, -62, -71, 10,
      -80, 1, -8, 10, 1, 1, -8, -8, 64, -80, -8, -8, 64, 64, -512, -44, -134,
      -53, -62, 19, -71, 514, 505, -62, 496, -71, 568, 100, 10, 10, -80, -80,
      640, 10, 1, 1, -8, -8, 64, 496, -224, -80, -8, 496, -80, 568, 496, -8,
      64, -80, 640, 640, 64, 64, -512, -512, 4096, -80, -8, -8, 64, 64, -512},
      {514, 505, -62, 496, -71, 568, -44, -134, -53, -62, 19, -71, 10, -80,
      100, 10, 640, -80, 1, -8, 10, 1, 64, -8, -44, -134, 28, -224, -62, 496,
      -116, 442, 442, -134, -134, 505, -62, 496, -53, 19, -80, 10, 10, -80,
      -62, -71, -8, 1, -116, 442, -44, 28, -53, -62, 136, -116, -116, -44, -44,
      514, 514, -62, -44, -53, 10, 100, -62, 10, 28, -62, 1, 10, 1, -8, 10, 1,
      64, -8, 10, -80, -62, -71, -8, 1, -62, 10, 28, -62, 1, 10, -512, 64, 64,
      -8, -8, -80, -8, 64, -80, -8, -512, 64, -80, 640, 496, 568, 64, -8, 496,
      -80, -224, 496, -8, -80, 4096, -512, -512, 64, 64, 640}, {-44, -134, -53,
      -62, 19, -71, 514, 505, -62, 496, -71, 568, 100, 10, 10, -80, -80, 640,
      10, 1, 1, -8, -8, 64, -116, 442, -44, 28, -53, -62, 136, -116, -116, -44,
      -44, 514, 514, -62, -44, -53, 10, 100, -62, 10, 28, -62, 1, 10, -44,
      -134, 28, -224, -62, 496, -116, 442, 442, -134, -134, 505, -62, 496, -53,
      19, -80, 10, 10, -80, -62, -71, -8, 1, 10, 1, 1, -8, -8, 64, -62, 10, 28,
      -62, 1, 10, 10, -80, -62, -71, -8, 1, 64, -512, -8, -80, 64, -8, -80, -8,
      -8, 64, 64, -512, 496, -80, -224, 496, -8, -80, -80, 640, 496, 568, 64,
      -8, -512, 4096, 64, 640, -512, 64}, {-44, -134, 28, -224, -62, 496, -116,
      442, 442, -134, -134, 505, -62, 496, -53, 19, -80, 10, 10, -80, -62, -71,
      -8, 1, 514, 505, -62, 496, -71, 568, -44, -134, -53, -62, 19, -71, 10,
      -80, 100, 10, 640, -80, 1, -8, 10, 1, 64, -8, 136, -116, -116, -44, -44,
      514, -116, 442, -44, 28, -53, -62, -44, -53, 514, -62, 100, 10, 28, -62,
      -62, 10, 10, 1, 10, -80, -62, -71, -8, 1, 1, -8, 10, 1, 64, -8, 28, -62,
      -62, 10, 10, 1, 64, -8, -512, 64, -80, -8, -80, 640, 496, 568, 64, -8,
      -8, 64, -80, -8, -512, 64, -224, 496, 496, -80, -80, -8, -512, 64, 4096,
      -512, 640, 64}, {-116, 442, -44, 28, -53, -62, 136, -116, -116, -44, -44,
      514, 514, -62, -44, -53, 10, 100, -62, 10, 28, -62, 1, 10, -44, -134,
      -53, -62, 19, -71, 514, 505, -62, 496, -71, 568, 100, 10, 10, -80, -80,
      640, 10, 1, 1, -8, -8, 64, -116, 442, 442, -134, -134, 505, -44, -134,
      28, -224, -62, 496, -53, 19, -62, 496, 10, -80, -62, -71, 10, -80, 1, -8,
      -62, 10, 28, -62, 1, 10, 10, 1, 1, -8, -8, 64, -62, -71, 10, -80, 1, -8,
      -8, -80, 64, -512, -8, 64, 496, -80, -224, 496, -8, -80, -80, -8, -8, 64,
      64, -512, 496, 568, -80, 640, -8, 64, 64, 640, -512, 4096, 64, -512},
      {-116, 442, 442, -134, -134, 505, -44, -134, 28, -224, -62, 496, -53, 19,
      -62, 496, 10, -80, -62, -71, 10, -80, 1, -8, 136, -116, -116, -44, -44,
      514, -116, 442, -44, 28, -53, -62, -44, -53, 514, -62, 100, 10, 28, -62,
      -62, 10, 10, 1, 514, 505, -62, 496, -71, 568, -44, -134, -53, -62, 19,
      -71, 10, -80, 100, 10, 640, -80, 1, -8, 10, 1, 64, -8, -62, -71, 10, -80,
      1, -8, 28, -62, -62, 10, 10, 1, 1, -8, 10, 1, 64, -8, -8, 64, -80, -8,
      -512, 64, 496, 568, -80, 640, -8, 64, -224, 496, 496, -80, -80, -8, -8,
      64, -80, -8, -512, 64, 64, -512, 640, 64, 4096, -512}, {136, -116, -116,
      -44, -44, 514, -116, 442, -44, 28, -53, -62, -44, -53, 514, -62, 100, 10,
      28, -62, -62, 10, 10, 1, -116, 442, 442, -134, -134, 505, -44, -134, 28,
      -224, -62, 496, -53, 19, -62, 496, 10, -80, -62, -71, 10, -80, 1, -8,
      -44, -134, -53, -62, 19, -71, 514, 505, -62, 496, -71, 568, 100, 10, 10,
      -80, -80, 640, 10, 1, 1, -8, -8, 64, 28, -62, -62, 10, 10, 1, -62, -71,
      10, -80, 1, -8, 10, 1, 1, -8, -8, 64, -80, -8, -8, 64, 64, -512, -224,
      496, 496, -80, -80, -8, 496, 568, -80, 640, -8, 64, -80, -8, -8, 64, 64,
      -512, 640, 64, 64, -512, -512, 4096}};

  // Calculate color flows
  jamp[0] = -amp[1] - amp[4] - amp[3] - amp[17] + amp[15] + amp[25] + amp[24] +
      amp[27] + amp[28] + amp[37] + amp[36] + amp[39] + amp[40] - amp[44] +
      amp[42] + Complex<double> (0, 1) * amp[124] + Complex<double> (0, 1) *
      amp[125] + amp[126] + Complex<double> (0, 1) * amp[131] - Complex<double>
      (0, 1) * amp[129] + amp[132] - Complex<double> (0, 1) * amp[134] -
      Complex<double> (0, 1) * amp[136] + amp[147] - Complex<double> (0, 1) *
      amp[148] - Complex<double> (0, 1) * amp[152] + Complex<double> (0, 1) *
      amp[155] - Complex<double> (0, 1) * amp[153] - Complex<double> (0, 1) *
      amp[177] + Complex<double> (0, 1) * amp[181] - Complex<double> (0, 1) *
      amp[179] + Complex<double> (0, 1) * amp[189] + amp[190] + amp[192] -
      Complex<double> (0, 1) * amp[193] - amp[250] + Complex<double> (0, 1) *
      amp[255] + amp[256] + amp[259] - Complex<double> (0, 1) * amp[260] -
      Complex<double> (0, 1) * amp[261] + Complex<double> (0, 1) * amp[264] -
      Complex<double> (0, 1) * amp[262] + Complex<double> (0, 1) * amp[276] +
      amp[277] - Complex<double> (0, 1) * amp[279] + Complex<double> (0, 1) *
      amp[282] - Complex<double> (0, 1) * amp[280] - Complex<double> (0, 1) *
      amp[285] + Complex<double> (0, 1) * amp[287] - amp[293] + amp[291] -
      amp[314] + amp[312] - Complex<double> (0, 1) * amp[315] + Complex<double>
      (0, 1) * amp[317] - amp[1075] - amp[1078] - amp[1077] + amp[1079] -
      amp[1083] + amp[1081] + amp[1084] - amp[1088] + amp[1086] + amp[1097] -
      amp[1095] + amp[1099] + amp[1098] + amp[1101] + amp[1102] +
      Complex<double> (0, 1) * amp[1137] + amp[1138] - Complex<double> (0, 1) *
      amp[1142] + Complex<double> (0, 1) * amp[1145] - Complex<double> (0, 1) *
      amp[1143] - Complex<double> (0, 1) * amp[1147] + amp[1308] + amp[1309] -
      amp[1312] + amp[1310] + amp[1319] - amp[1322] - amp[1321] - amp[1325] +
      amp[1323] + amp[1328] - amp[1326] + amp[1357] - amp[1360] + amp[1358] -
      amp[1370] - amp[1369] - amp[1386] + amp[1381] + amp[1380] - amp[1387] +
      amp[1389] - amp[1391] - Complex<double> (0, 1) * amp[1481] +
      Complex<double> (0, 1) * amp[1484] - Complex<double> (0, 1) * amp[1482] +
      amp[1486] + amp[1485] - amp[1492] - amp[1491] - amp[1496] + amp[1494] +
      amp[1500] - amp[1502] - amp[1529] + amp[1527] + Complex<double> (0, 1) *
      amp[1532] - Complex<double> (0, 1) * amp[1530] - Complex<double> (0, 1) *
      amp[1533] + Complex<double> (0, 1) * amp[1535] + amp[1810] + amp[1809] -
      amp[1816] - amp[1815] - amp[1820] + amp[1818] + amp[1821] - amp[1823] +
      Complex<double> (0, 1) * amp[1832] - Complex<double> (0, 1) * amp[1830];
  jamp[1] = -amp[0] + amp[4] - amp[2] - amp[20] + amp[18] + amp[31] + amp[30] +
      amp[33] + amp[34] - amp[37] - amp[36] - amp[39] - amp[40] - amp[42] -
      amp[43] + Complex<double> (0, 1) * amp[112] + Complex<double> (0, 1) *
      amp[113] + amp[114] + Complex<double> (0, 1) * amp[119] - Complex<double>
      (0, 1) * amp[117] + amp[120] - Complex<double> (0, 1) * amp[122] -
      Complex<double> (0, 1) * amp[142] - amp[147] + Complex<double> (0, 1) *
      amp[148] + Complex<double> (0, 1) * amp[152] + Complex<double> (0, 1) *
      amp[153] + Complex<double> (0, 1) * amp[154] - Complex<double> (0, 1) *
      amp[178] - Complex<double> (0, 1) * amp[181] - Complex<double> (0, 1) *
      amp[180] + Complex<double> (0, 1) * amp[186] + amp[187] - amp[192] +
      Complex<double> (0, 1) * amp[193] - amp[241] + Complex<double> (0, 1) *
      amp[246] + amp[247] + amp[268] - Complex<double> (0, 1) * amp[269] -
      Complex<double> (0, 1) * amp[270] + Complex<double> (0, 1) * amp[273] -
      Complex<double> (0, 1) * amp[271] - Complex<double> (0, 1) * amp[276] -
      amp[277] + Complex<double> (0, 1) * amp[279] - Complex<double> (0, 1) *
      amp[282] + Complex<double> (0, 1) * amp[280] - Complex<double> (0, 1) *
      amp[294] + Complex<double> (0, 1) * amp[296] - amp[302] + amp[300] -
      amp[312] - amp[313] + Complex<double> (0, 1) * amp[315] + Complex<double>
      (0, 1) * amp[316] - amp[1074] + amp[1078] - amp[1076] + amp[1080] +
      amp[1083] + amp[1082] + amp[1085] - amp[1091] + amp[1089] + amp[1094] -
      amp[1092] - amp[1099] - amp[1098] - amp[1101] - amp[1102] +
      Complex<double> (0, 1) * amp[1128] + amp[1129] - Complex<double> (0, 1) *
      amp[1133] + Complex<double> (0, 1) * amp[1136] - Complex<double> (0, 1) *
      amp[1134] + Complex<double> (0, 1) * amp[1147] + amp[1332] + amp[1333] -
      amp[1336] + amp[1334] + amp[1343] - amp[1346] - amp[1345] - amp[1349] +
      amp[1347] + amp[1352] - amp[1350] - amp[1357] + amp[1360] - amp[1358] +
      amp[1370] + amp[1369] - amp[1384] - amp[1381] - amp[1380] - amp[1383] -
      amp[1389] - amp[1390] - Complex<double> (0, 1) * amp[1472] +
      Complex<double> (0, 1) * amp[1475] - Complex<double> (0, 1) * amp[1473] +
      amp[1487] - amp[1493] - amp[1485] + amp[1491] - amp[1499] + amp[1497] -
      amp[1500] + amp[1502] - amp[1523] + amp[1521] + Complex<double> (0, 1) *
      amp[1526] - Complex<double> (0, 1) * amp[1524] + Complex<double> (0, 1) *
      amp[1533] - Complex<double> (0, 1) * amp[1535] + amp[1837] + amp[1836] -
      amp[1843] - amp[1842] - amp[1847] + amp[1845] + amp[1848] - amp[1850] +
      Complex<double> (0, 1) * amp[1859] - Complex<double> (0, 1) * amp[1857];
  jamp[2] = -amp[6] - amp[9] - amp[8] - amp[15] - amp[16] - amp[25] - amp[24] -
      amp[27] - amp[28] + amp[32] - amp[30] - amp[33] + amp[35] + amp[44] +
      amp[43] + Complex<double> (0, 1) * amp[123] + Complex<double> (0, 1) *
      amp[127] + amp[128] + Complex<double> (0, 1) * amp[129] + Complex<double>
      (0, 1) * amp[130] - amp[132] + Complex<double> (0, 1) * amp[134] +
      Complex<double> (0, 1) * amp[136] + amp[141] + Complex<double> (0, 1) *
      amp[142] - Complex<double> (0, 1) * amp[146] - Complex<double> (0, 1) *
      amp[155] - Complex<double> (0, 1) * amp[154] - Complex<double> (0, 1) *
      amp[195] + Complex<double> (0, 1) * amp[199] - Complex<double> (0, 1) *
      amp[197] + Complex<double> (0, 1) * amp[207] + amp[208] + amp[210] -
      Complex<double> (0, 1) * amp[211] - amp[249] - Complex<double> (0, 1) *
      amp[255] - amp[256] - amp[259] + Complex<double> (0, 1) * amp[260] +
      Complex<double> (0, 1) * amp[261] - Complex<double> (0, 1) * amp[264] +
      Complex<double> (0, 1) * amp[262] + Complex<double> (0, 1) * amp[267] -
      amp[268] + Complex<double> (0, 1) * amp[270] - Complex<double> (0, 1) *
      amp[273] - Complex<double> (0, 1) * amp[272] + Complex<double> (0, 1) *
      amp[285] + Complex<double> (0, 1) * amp[286] - amp[291] - amp[292] +
      amp[314] + amp[313] - Complex<double> (0, 1) * amp[316] - Complex<double>
      (0, 1) * amp[317] - amp[1153] - amp[1156] - amp[1155] + amp[1157] -
      amp[1161] + amp[1159] + amp[1162] - amp[1166] + amp[1164] + amp[1175] -
      amp[1173] + amp[1177] + amp[1176] + amp[1179] + amp[1180] +
      Complex<double> (0, 1) * amp[1215] + amp[1216] - Complex<double> (0, 1) *
      amp[1220] + Complex<double> (0, 1) * amp[1223] - Complex<double> (0, 1) *
      amp[1221] - Complex<double> (0, 1) * amp[1225] - amp[1308] - amp[1309] +
      amp[1312] - amp[1310] - amp[1319] + amp[1322] + amp[1321] + amp[1325] -
      amp[1323] - amp[1328] + amp[1326] - amp[1333] + amp[1336] + amp[1335] +
      amp[1346] + amp[1345] + amp[1386] + amp[1384] + amp[1383] + amp[1387] +
      amp[1390] + amp[1391] + Complex<double> (0, 1) * amp[1481] -
      Complex<double> (0, 1) * amp[1484] + Complex<double> (0, 1) * amp[1482] +
      amp[1540] + amp[1539] - amp[1546] - amp[1545] - amp[1550] + amp[1548] +
      amp[1554] - amp[1556] - amp[1583] + amp[1581] + Complex<double> (0, 1) *
      amp[1586] - Complex<double> (0, 1) * amp[1584] - Complex<double> (0, 1) *
      amp[1587] + Complex<double> (0, 1) * amp[1589] - amp[1812] - amp[1810] -
      amp[1813] - amp[1809] - amp[1818] - amp[1819] - amp[1821] - amp[1822] +
      Complex<double> (0, 1) * amp[1830] + Complex<double> (0, 1) * amp[1831];
  jamp[3] = -amp[5] + amp[9] - amp[7] - amp[23] + amp[21] - amp[32] + amp[30] +
      amp[33] - amp[35] + amp[38] - amp[36] - amp[39] + amp[41] - amp[42] -
      amp[43] + Complex<double> (0, 1) * amp[100] + Complex<double> (0, 1) *
      amp[101] + amp[102] + Complex<double> (0, 1) * amp[107] - Complex<double>
      (0, 1) * amp[105] + amp[108] - Complex<double> (0, 1) * amp[110] -
      amp[141] - Complex<double> (0, 1) * amp[142] + Complex<double> (0, 1) *
      amp[146] + Complex<double> (0, 1) * amp[148] + Complex<double> (0, 1) *
      amp[153] + Complex<double> (0, 1) * amp[154] - Complex<double> (0, 1) *
      amp[196] - Complex<double> (0, 1) * amp[199] - Complex<double> (0, 1) *
      amp[198] + Complex<double> (0, 1) * amp[204] + amp[205] - amp[210] +
      Complex<double> (0, 1) * amp[211] - amp[232] + Complex<double> (0, 1) *
      amp[237] + amp[238] - Complex<double> (0, 1) * amp[267] + amp[268] -
      Complex<double> (0, 1) * amp[270] + Complex<double> (0, 1) * amp[273] +
      Complex<double> (0, 1) * amp[272] - amp[277] - Complex<double> (0, 1) *
      amp[278] + Complex<double> (0, 1) * amp[279] - Complex<double> (0, 1) *
      amp[282] - Complex<double> (0, 1) * amp[281] - Complex<double> (0, 1) *
      amp[303] + Complex<double> (0, 1) * amp[305] - amp[311] + amp[309] -
      amp[312] - amp[313] + Complex<double> (0, 1) * amp[315] + Complex<double>
      (0, 1) * amp[316] - amp[1152] + amp[1156] - amp[1154] + amp[1158] +
      amp[1161] + amp[1160] + amp[1163] - amp[1169] + amp[1167] + amp[1172] -
      amp[1170] - amp[1177] - amp[1176] - amp[1179] - amp[1180] +
      Complex<double> (0, 1) * amp[1206] + amp[1207] - Complex<double> (0, 1) *
      amp[1211] + Complex<double> (0, 1) * amp[1214] - Complex<double> (0, 1) *
      amp[1212] + Complex<double> (0, 1) * amp[1225] + amp[1333] - amp[1336] -
      amp[1335] - amp[1346] - amp[1345] + amp[1356] - amp[1357] + amp[1360] +
      amp[1359] + amp[1367] + amp[1370] + amp[1369] - amp[1373] + amp[1371] +
      amp[1376] - amp[1374] - amp[1384] - amp[1381] - amp[1380] - amp[1383] -
      amp[1389] - amp[1390] - Complex<double> (0, 1) * amp[1463] +
      Complex<double> (0, 1) * amp[1466] - Complex<double> (0, 1) * amp[1464] +
      amp[1541] - amp[1547] - amp[1539] + amp[1545] - amp[1553] + amp[1551] -
      amp[1554] + amp[1556] - amp[1577] + amp[1575] + Complex<double> (0, 1) *
      amp[1580] - Complex<double> (0, 1) * amp[1578] + Complex<double> (0, 1) *
      amp[1587] - Complex<double> (0, 1) * amp[1589] + amp[1864] + amp[1863] -
      amp[1870] - amp[1869] - amp[1874] + amp[1872] + amp[1875] - amp[1877] +
      Complex<double> (0, 1) * amp[1886] - Complex<double> (0, 1) * amp[1884];
  jamp[4] = -amp[11] - amp[14] - amp[13] - amp[18] - amp[19] + amp[26] -
      amp[24] - amp[27] + amp[29] - amp[31] - amp[30] - amp[33] - amp[34] +
      amp[44] + amp[43] + Complex<double> (0, 1) * amp[111] + Complex<double>
      (0, 1) * amp[115] + amp[116] + Complex<double> (0, 1) * amp[117] +
      Complex<double> (0, 1) * amp[118] - amp[120] + Complex<double> (0, 1) *
      amp[122] + amp[135] + Complex<double> (0, 1) * amp[136] - Complex<double>
      (0, 1) * amp[140] + Complex<double> (0, 1) * amp[142] - Complex<double>
      (0, 1) * amp[155] - Complex<double> (0, 1) * amp[154] - Complex<double>
      (0, 1) * amp[213] + Complex<double> (0, 1) * amp[217] - Complex<double>
      (0, 1) * amp[215] + Complex<double> (0, 1) * amp[225] + amp[226] +
      amp[228] - Complex<double> (0, 1) * amp[229] - amp[240] - Complex<double>
      (0, 1) * amp[246] - amp[247] + Complex<double> (0, 1) * amp[258] -
      amp[259] + Complex<double> (0, 1) * amp[261] - Complex<double> (0, 1) *
      amp[264] - Complex<double> (0, 1) * amp[263] - amp[268] + Complex<double>
      (0, 1) * amp[269] + Complex<double> (0, 1) * amp[270] - Complex<double>
      (0, 1) * amp[273] + Complex<double> (0, 1) * amp[271] + Complex<double>
      (0, 1) * amp[294] + Complex<double> (0, 1) * amp[295] - amp[300] -
      amp[301] + amp[314] + amp[313] - Complex<double> (0, 1) * amp[316] -
      Complex<double> (0, 1) * amp[317] - amp[1231] - amp[1234] - amp[1233] +
      amp[1235] - amp[1239] + amp[1237] + amp[1240] - amp[1244] + amp[1242] +
      amp[1253] - amp[1251] + amp[1255] + amp[1254] + amp[1257] + amp[1258] +
      Complex<double> (0, 1) * amp[1293] + amp[1294] - Complex<double> (0, 1) *
      amp[1298] + Complex<double> (0, 1) * amp[1301] - Complex<double> (0, 1) *
      amp[1299] - Complex<double> (0, 1) * amp[1303] - amp[1309] + amp[1312] +
      amp[1311] + amp[1322] + amp[1321] - amp[1332] - amp[1333] + amp[1336] -
      amp[1334] - amp[1343] + amp[1346] + amp[1345] + amp[1349] - amp[1347] -
      amp[1352] + amp[1350] + amp[1386] + amp[1384] + amp[1383] + amp[1387] +
      amp[1390] + amp[1391] + Complex<double> (0, 1) * amp[1472] -
      Complex<double> (0, 1) * amp[1475] + Complex<double> (0, 1) * amp[1473] +
      amp[1594] + amp[1593] - amp[1600] - amp[1599] - amp[1604] + amp[1602] +
      amp[1608] - amp[1610] - amp[1637] + amp[1635] + Complex<double> (0, 1) *
      amp[1640] - Complex<double> (0, 1) * amp[1638] - Complex<double> (0, 1) *
      amp[1641] + Complex<double> (0, 1) * amp[1643] - amp[1839] - amp[1837] -
      amp[1840] - amp[1836] - amp[1845] - amp[1846] - amp[1848] - amp[1849] +
      Complex<double> (0, 1) * amp[1857] + Complex<double> (0, 1) * amp[1858];
  jamp[5] = -amp[10] + amp[14] - amp[12] - amp[21] - amp[22] - amp[26] +
      amp[24] + amp[27] - amp[29] - amp[38] + amp[36] + amp[39] - amp[41] -
      amp[44] + amp[42] + Complex<double> (0, 1) * amp[99] + Complex<double>
      (0, 1) * amp[103] + amp[104] + Complex<double> (0, 1) * amp[105] +
      Complex<double> (0, 1) * amp[106] - amp[108] + Complex<double> (0, 1) *
      amp[110] - amp[135] - Complex<double> (0, 1) * amp[136] + Complex<double>
      (0, 1) * amp[140] - Complex<double> (0, 1) * amp[148] + Complex<double>
      (0, 1) * amp[155] - Complex<double> (0, 1) * amp[153] - Complex<double>
      (0, 1) * amp[214] - Complex<double> (0, 1) * amp[217] - Complex<double>
      (0, 1) * amp[216] + Complex<double> (0, 1) * amp[222] + amp[223] -
      amp[228] + Complex<double> (0, 1) * amp[229] - amp[231] - Complex<double>
      (0, 1) * amp[237] - amp[238] - Complex<double> (0, 1) * amp[258] +
      amp[259] - Complex<double> (0, 1) * amp[261] + Complex<double> (0, 1) *
      amp[264] + Complex<double> (0, 1) * amp[263] + amp[277] + Complex<double>
      (0, 1) * amp[278] - Complex<double> (0, 1) * amp[279] + Complex<double>
      (0, 1) * amp[282] + Complex<double> (0, 1) * amp[281] + Complex<double>
      (0, 1) * amp[303] + Complex<double> (0, 1) * amp[304] - amp[309] -
      amp[310] - amp[314] + amp[312] - Complex<double> (0, 1) * amp[315] +
      Complex<double> (0, 1) * amp[317] - amp[1230] + amp[1234] - amp[1232] +
      amp[1236] + amp[1239] + amp[1238] + amp[1241] - amp[1247] + amp[1245] +
      amp[1250] - amp[1248] - amp[1255] - amp[1254] - amp[1257] - amp[1258] +
      Complex<double> (0, 1) * amp[1284] + amp[1285] - Complex<double> (0, 1) *
      amp[1289] + Complex<double> (0, 1) * amp[1292] - Complex<double> (0, 1) *
      amp[1290] + Complex<double> (0, 1) * amp[1303] + amp[1309] - amp[1312] -
      amp[1311] - amp[1322] - amp[1321] - amp[1356] + amp[1357] - amp[1360] -
      amp[1359] - amp[1367] - amp[1370] - amp[1369] + amp[1373] - amp[1371] -
      amp[1376] + amp[1374] - amp[1386] + amp[1381] + amp[1380] - amp[1387] +
      amp[1389] - amp[1391] + Complex<double> (0, 1) * amp[1463] -
      Complex<double> (0, 1) * amp[1466] + Complex<double> (0, 1) * amp[1464] +
      amp[1595] - amp[1601] - amp[1593] + amp[1599] - amp[1607] + amp[1605] -
      amp[1608] + amp[1610] - amp[1631] + amp[1629] + Complex<double> (0, 1) *
      amp[1634] - Complex<double> (0, 1) * amp[1632] + Complex<double> (0, 1) *
      amp[1641] - Complex<double> (0, 1) * amp[1643] - amp[1866] - amp[1864] -
      amp[1867] - amp[1863] - amp[1872] - amp[1873] - amp[1875] - amp[1876] +
      Complex<double> (0, 1) * amp[1884] + Complex<double> (0, 1) * amp[1885];
  jamp[6] = +Complex<double> (0, 1) * amp[177] - Complex<double> (0, 1) *
      amp[181] + Complex<double> (0, 1) * amp[179] - Complex<double> (0, 1) *
      amp[189] - amp[190] - amp[192] + Complex<double> (0, 1) * amp[193] +
      Complex<double> (0, 1) * amp[195] + Complex<double> (0, 1) * amp[196] +
      Complex<double> (0, 1) * amp[198] + Complex<double> (0, 1) * amp[197] +
      amp[201] - amp[208] + Complex<double> (0, 1) * amp[209] - amp[252] +
      Complex<double> (0, 1) * amp[278] + Complex<double> (0, 1) * amp[281] +
      Complex<double> (0, 1) * amp[280] + amp[283] + Complex<double> (0, 1) *
      amp[284] - Complex<double> (0, 1) * amp[286] - Complex<double> (0, 1) *
      amp[287] + amp[293] + amp[292] + Complex<double> (0, 1) * amp[303] -
      Complex<double> (0, 1) * amp[305] - amp[308] + amp[306] - Complex<double>
      (0, 1) * amp[514] + amp[520] + amp[519] + amp[522] + amp[523] -
      Complex<double> (0, 1) * amp[528] + amp[529] - amp[550] - amp[553] -
      amp[552] - amp[566] + amp[564] + amp[574] + amp[573] + amp[576] +
      amp[577] + Complex<double> (0, 1) * amp[612] + amp[613] + Complex<double>
      (0, 1) * amp[615] + Complex<double> (0, 1) * amp[620] - Complex<double>
      (0, 1) * amp[618] - Complex<double> (0, 1) * amp[622] - Complex<double>
      (0, 1) * amp[623] + amp[624] + Complex<double> (0, 1) * amp[629] -
      Complex<double> (0, 1) * amp[627] + amp[635] - amp[633] + amp[1075] +
      amp[1078] + amp[1077] - amp[1079] + amp[1083] - amp[1081] - amp[1084] +
      amp[1088] - amp[1086] - amp[1097] + amp[1095] - amp[1099] - amp[1098] -
      amp[1101] - amp[1102] - Complex<double> (0, 1) * amp[1137] - amp[1138] +
      Complex<double> (0, 1) * amp[1142] - Complex<double> (0, 1) * amp[1145] +
      Complex<double> (0, 1) * amp[1143] + Complex<double> (0, 1) * amp[1147] -
      amp[1157] - amp[1158] - amp[1160] - amp[1159] - amp[1162] - amp[1164] -
      amp[1165] - amp[1172] - amp[1171] - amp[1175] + amp[1173] +
      Complex<double> (0, 1) * amp[1220] + Complex<double> (0, 1) * amp[1221] +
      Complex<double> (0, 1) * amp[1222] - amp[1356] - amp[1359] - amp[1358] -
      amp[1376] - amp[1375] - amp[1488] - amp[1486] - amp[1489] - amp[1485] -
      amp[1494] - amp[1495] - amp[1500] - amp[1501] - amp[1527] - amp[1528] +
      Complex<double> (0, 1) * amp[1530] + Complex<double> (0, 1) * amp[1531] +
      Complex<double> (0, 1) * amp[1533] + Complex<double> (0, 1) * amp[1534] +
      amp[1812] + amp[1813] + amp[1816] + amp[1815] + amp[1820] + amp[1819] +
      amp[1822] + amp[1823] - Complex<double> (0, 1) * amp[1832] -
      Complex<double> (0, 1) * amp[1831] - amp[1865] + amp[1871] - amp[1864] +
      amp[1870] - amp[1875] + amp[1877];
  jamp[7] = +Complex<double> (0, 1) * amp[178] + Complex<double> (0, 1) *
      amp[181] + Complex<double> (0, 1) * amp[180] - Complex<double> (0, 1) *
      amp[186] - amp[187] + amp[192] - Complex<double> (0, 1) * amp[193] +
      Complex<double> (0, 1) * amp[213] + Complex<double> (0, 1) * amp[214] +
      Complex<double> (0, 1) * amp[216] + Complex<double> (0, 1) * amp[215] +
      amp[219] - amp[226] + Complex<double> (0, 1) * amp[227] - amp[243] -
      Complex<double> (0, 1) * amp[278] - Complex<double> (0, 1) * amp[281] -
      Complex<double> (0, 1) * amp[280] - amp[283] - Complex<double> (0, 1) *
      amp[284] - Complex<double> (0, 1) * amp[295] - Complex<double> (0, 1) *
      amp[296] + amp[302] + amp[301] - Complex<double> (0, 1) * amp[303] -
      Complex<double> (0, 1) * amp[304] - amp[306] - amp[307] - Complex<double>
      (0, 1) * amp[532] + amp[538] + amp[537] + amp[540] + amp[541] -
      Complex<double> (0, 1) * amp[546] + amp[547] - amp[549] + amp[553] -
      amp[551] - amp[569] + amp[567] - amp[574] - amp[573] - amp[576] -
      amp[577] + Complex<double> (0, 1) * amp[603] + amp[604] + Complex<double>
      (0, 1) * amp[606] + Complex<double> (0, 1) * amp[611] - Complex<double>
      (0, 1) * amp[609] + Complex<double> (0, 1) * amp[622] + Complex<double>
      (0, 1) * amp[623] - amp[624] + Complex<double> (0, 1) * amp[627] +
      Complex<double> (0, 1) * amp[628] + amp[633] + amp[634] + amp[1074] -
      amp[1078] + amp[1076] - amp[1080] - amp[1083] - amp[1082] - amp[1085] +
      amp[1091] - amp[1089] - amp[1094] + amp[1092] + amp[1099] + amp[1098] +
      amp[1101] + amp[1102] - Complex<double> (0, 1) * amp[1128] - amp[1129] +
      Complex<double> (0, 1) * amp[1133] - Complex<double> (0, 1) * amp[1136] +
      Complex<double> (0, 1) * amp[1134] - Complex<double> (0, 1) * amp[1147] -
      amp[1235] - amp[1236] - amp[1238] - amp[1237] - amp[1240] - amp[1242] -
      amp[1243] - amp[1250] - amp[1249] - amp[1253] + amp[1251] +
      Complex<double> (0, 1) * amp[1298] + Complex<double> (0, 1) * amp[1299] +
      Complex<double> (0, 1) * amp[1300] + amp[1356] + amp[1359] + amp[1358] +
      amp[1376] + amp[1375] - amp[1487] + amp[1488] + amp[1485] - amp[1490] -
      amp[1497] - amp[1498] + amp[1500] + amp[1501] - amp[1521] - amp[1522] +
      Complex<double> (0, 1) * amp[1524] + Complex<double> (0, 1) * amp[1525] -
      Complex<double> (0, 1) * amp[1533] - Complex<double> (0, 1) * amp[1534] +
      amp[1839] + amp[1840] + amp[1843] + amp[1842] + amp[1847] + amp[1846] +
      amp[1849] + amp[1850] - Complex<double> (0, 1) * amp[1859] -
      Complex<double> (0, 1) * amp[1858] + amp[1865] + amp[1864] + amp[1867] +
      amp[1868] + amp[1875] + amp[1876];
  jamp[8] = -Complex<double> (0, 1) * amp[195] - Complex<double> (0, 1) *
      amp[196] - Complex<double> (0, 1) * amp[198] - Complex<double> (0, 1) *
      amp[197] - amp[201] + amp[208] - Complex<double> (0, 1) * amp[209] -
      Complex<double> (0, 1) * amp[214] - Complex<double> (0, 1) * amp[217] -
      Complex<double> (0, 1) * amp[216] + Complex<double> (0, 1) * amp[218] -
      amp[219] + Complex<double> (0, 1) * amp[229] + amp[230] - amp[251] -
      amp[256] + Complex<double> (0, 1) * amp[257] + Complex<double> (0, 1) *
      amp[260] + Complex<double> (0, 1) * amp[263] + Complex<double> (0, 1) *
      amp[262] + Complex<double> (0, 1) * amp[285] + Complex<double> (0, 1) *
      amp[286] - amp[291] - amp[292] + Complex<double> (0, 1) * amp[304] +
      Complex<double> (0, 1) * amp[305] + amp[308] + amp[307] + Complex<double>
      (0, 1) * amp[514] - amp[520] - amp[519] - amp[522] - amp[523] +
      Complex<double> (0, 1) * amp[528] - amp[529] + amp[531] + Complex<double>
      (0, 1) * amp[532] - Complex<double> (0, 1) * amp[536] - amp[539] -
      amp[538] - amp[541] - amp[542] + amp[555] - amp[558] - amp[557] -
      amp[564] - amp[565] + Complex<double> (0, 1) * amp[614] + amp[616] -
      Complex<double> (0, 1) * amp[617] + Complex<double> (0, 1) * amp[618] +
      Complex<double> (0, 1) * amp[619] - Complex<double> (0, 1) * amp[629] -
      Complex<double> (0, 1) * amp[628] - amp[635] - amp[634] + amp[1157] +
      amp[1158] + amp[1160] + amp[1159] + amp[1162] + amp[1164] + amp[1165] +
      amp[1172] + amp[1171] + amp[1175] - amp[1173] - Complex<double> (0, 1) *
      amp[1220] - Complex<double> (0, 1) * amp[1221] - Complex<double> (0, 1) *
      amp[1222] + amp[1236] + amp[1239] + amp[1238] + amp[1250] + amp[1249] -
      amp[1256] - amp[1255] - amp[1258] - amp[1259] - Complex<double> (0, 1) *
      amp[1305] - amp[1308] - amp[1311] - amp[1310] + amp[1314] - amp[1317] -
      amp[1316] - amp[1319] - amp[1323] - amp[1324] - amp[1328] + amp[1326] +
      amp[1478] - Complex<double> (0, 1) * amp[1479] + Complex<double> (0, 1) *
      amp[1481] + Complex<double> (0, 1) * amp[1482] + Complex<double> (0, 1) *
      amp[1483] - Complex<double> (0, 1) * amp[1665] + Complex<double> (0, 1) *
      amp[1667] - amp[1671] + amp[1673] + amp[1675] + amp[1674] - amp[1681] -
      amp[1680] - amp[1685] + amp[1683] + Complex<double> (0, 1) * amp[1697] -
      Complex<double> (0, 1) * amp[1695] - amp[1700] + amp[1698] - amp[1812] -
      amp[1810] - amp[1813] - amp[1809] - amp[1818] - amp[1819] - amp[1821] -
      amp[1822] + Complex<double> (0, 1) * amp[1830] + Complex<double> (0, 1) *
      amp[1831] - amp[1871] - amp[1867] - amp[1870] - amp[1868] - amp[1876] -
      amp[1877];
  jamp[9] = -amp[160] + amp[165] + Complex<double> (0, 1) * amp[166] + amp[171]
      + Complex<double> (0, 1) * amp[172] - amp[176] + amp[174] +
      Complex<double> (0, 1) * amp[214] + Complex<double> (0, 1) * amp[217] +
      Complex<double> (0, 1) * amp[216] - Complex<double> (0, 1) * amp[218] +
      amp[219] - Complex<double> (0, 1) * amp[229] - amp[230] + Complex<double>
      (0, 1) * amp[261] - Complex<double> (0, 1) * amp[264] - Complex<double>
      (0, 1) * amp[263] - Complex<double> (0, 1) * amp[278] + Complex<double>
      (0, 1) * amp[279] - Complex<double> (0, 1) * amp[282] - Complex<double>
      (0, 1) * amp[281] - amp[283] - Complex<double> (0, 1) * amp[303] -
      Complex<double> (0, 1) * amp[304] - amp[306] - amp[307] + Complex<double>
      (0, 1) * amp[315] - Complex<double> (0, 1) * amp[317] + Complex<double>
      (0, 1) * amp[496] + Complex<double> (0, 1) * amp[497] + amp[498] +
      Complex<double> (0, 1) * amp[503] - Complex<double> (0, 1) * amp[501] +
      amp[511] - Complex<double> (0, 1) * amp[512] - amp[531] - Complex<double>
      (0, 1) * amp[532] + Complex<double> (0, 1) * amp[536] + amp[539] +
      amp[538] + amp[541] + amp[542] + amp[554] + amp[557] + amp[556] +
      amp[572] - amp[570] - amp[575] - amp[574] - amp[577] - amp[578] +
      Complex<double> (0, 1) * amp[623] + Complex<double> (0, 1) * amp[627] +
      Complex<double> (0, 1) * amp[628] + amp[633] + amp[634] + Complex<double>
      (0, 1) * amp[1043] + amp[1044] + Complex<double> (0, 1) * amp[1045] +
      Complex<double> (0, 1) * amp[1049] - Complex<double> (0, 1) * amp[1047] +
      Complex<double> (0, 1) * amp[1063] + Complex<double> (0, 1) * amp[1067] -
      Complex<double> (0, 1) * amp[1065] + Complex<double> (0, 1) * amp[1073] -
      Complex<double> (0, 1) * amp[1071] - amp[1236] - amp[1239] - amp[1238] -
      amp[1250] - amp[1249] + amp[1256] + amp[1255] + amp[1258] + amp[1259] +
      Complex<double> (0, 1) * amp[1305] - amp[1309] + amp[1312] + amp[1311] +
      amp[1313] + amp[1316] + amp[1315] + amp[1318] + amp[1320] + amp[1321] +
      amp[1331] - amp[1329] + amp[1356] - amp[1357] + amp[1360] + amp[1359] +
      amp[1366] + amp[1368] + amp[1369] + amp[1376] + amp[1375] + amp[1379] -
      amp[1377] - amp[1382] - amp[1381] + amp[1388] + amp[1387] - amp[1389] +
      amp[1391] + amp[1394] - amp[1392] - amp[1658] + amp[1656] +
      Complex<double> (0, 1) * amp[1661] - Complex<double> (0, 1) * amp[1659] +
      Complex<double> (0, 1) * amp[1665] - Complex<double> (0, 1) * amp[1667] +
      amp[1671] - amp[1673] - amp[1676] + amp[1682] - amp[1675] + amp[1681] +
      amp[1688] - amp[1686] + amp[1865] + amp[1864] + amp[1867] + amp[1868] +
      amp[1875] + amp[1876];
  jamp[10] = -Complex<double> (0, 1) * amp[196] - Complex<double> (0, 1) *
      amp[199] - Complex<double> (0, 1) * amp[198] + Complex<double> (0, 1) *
      amp[200] - amp[201] + Complex<double> (0, 1) * amp[211] + amp[212] -
      Complex<double> (0, 1) * amp[213] - Complex<double> (0, 1) * amp[214] -
      Complex<double> (0, 1) * amp[216] - Complex<double> (0, 1) * amp[215] -
      amp[219] + amp[226] - Complex<double> (0, 1) * amp[227] - amp[242] -
      amp[247] + Complex<double> (0, 1) * amp[248] + Complex<double> (0, 1) *
      amp[269] + Complex<double> (0, 1) * amp[272] + Complex<double> (0, 1) *
      amp[271] + Complex<double> (0, 1) * amp[294] + Complex<double> (0, 1) *
      amp[295] - amp[300] - amp[301] + Complex<double> (0, 1) * amp[304] +
      Complex<double> (0, 1) * amp[305] + amp[308] + amp[307] + amp[513] +
      Complex<double> (0, 1) * amp[514] - Complex<double> (0, 1) * amp[518] -
      amp[521] - amp[520] - amp[523] - amp[524] + Complex<double> (0, 1) *
      amp[532] - amp[538] - amp[537] - amp[540] - amp[541] + Complex<double>
      (0, 1) * amp[546] - amp[547] + amp[560] - amp[563] - amp[562] - amp[567]
      - amp[568] + Complex<double> (0, 1) * amp[605] + amp[607] -
      Complex<double> (0, 1) * amp[608] + Complex<double> (0, 1) * amp[609] +
      Complex<double> (0, 1) * amp[610] - Complex<double> (0, 1) * amp[629] -
      Complex<double> (0, 1) * amp[628] - amp[635] - amp[634] + amp[1158] +
      amp[1161] + amp[1160] + amp[1172] + amp[1171] - amp[1178] - amp[1177] -
      amp[1180] - amp[1181] - Complex<double> (0, 1) * amp[1227] + amp[1235] +
      amp[1236] + amp[1238] + amp[1237] + amp[1240] + amp[1242] + amp[1243] +
      amp[1250] + amp[1249] + amp[1253] - amp[1251] - Complex<double> (0, 1) *
      amp[1298] - Complex<double> (0, 1) * amp[1299] - Complex<double> (0, 1) *
      amp[1300] - amp[1332] - amp[1335] - amp[1334] + amp[1338] - amp[1341] -
      amp[1340] - amp[1343] - amp[1347] - amp[1348] - amp[1352] + amp[1350] +
      amp[1469] - Complex<double> (0, 1) * amp[1470] + Complex<double> (0, 1) *
      amp[1472] + Complex<double> (0, 1) * amp[1473] + Complex<double> (0, 1) *
      amp[1474] - Complex<double> (0, 1) * amp[1719] + Complex<double> (0, 1) *
      amp[1721] - amp[1725] + amp[1727] + amp[1729] + amp[1728] - amp[1735] -
      amp[1734] - amp[1739] + amp[1737] + Complex<double> (0, 1) * amp[1751] -
      Complex<double> (0, 1) * amp[1749] - amp[1754] + amp[1752] - amp[1839] -
      amp[1837] - amp[1840] - amp[1836] - amp[1845] - amp[1846] - amp[1848] -
      amp[1849] + Complex<double> (0, 1) * amp[1857] + Complex<double> (0, 1) *
      amp[1858] - amp[1871] - amp[1867] - amp[1870] - amp[1868] - amp[1876] -
      amp[1877];
  jamp[11] = -amp[159] + amp[168] + Complex<double> (0, 1) * amp[169] -
      amp[171] - Complex<double> (0, 1) * amp[172] - amp[174] - amp[175] +
      Complex<double> (0, 1) * amp[196] + Complex<double> (0, 1) * amp[199] +
      Complex<double> (0, 1) * amp[198] - Complex<double> (0, 1) * amp[200] +
      amp[201] - Complex<double> (0, 1) * amp[211] - amp[212] + Complex<double>
      (0, 1) * amp[270] - Complex<double> (0, 1) * amp[273] - Complex<double>
      (0, 1) * amp[272] + Complex<double> (0, 1) * amp[278] - Complex<double>
      (0, 1) * amp[279] + Complex<double> (0, 1) * amp[282] + Complex<double>
      (0, 1) * amp[281] + amp[283] + Complex<double> (0, 1) * amp[303] -
      Complex<double> (0, 1) * amp[305] - amp[308] + amp[306] - Complex<double>
      (0, 1) * amp[315] - Complex<double> (0, 1) * amp[316] + Complex<double>
      (0, 1) * amp[495] + Complex<double> (0, 1) * amp[499] + amp[500] +
      Complex<double> (0, 1) * amp[501] + Complex<double> (0, 1) * amp[502] -
      amp[511] + Complex<double> (0, 1) * amp[512] - amp[513] - Complex<double>
      (0, 1) * amp[514] + Complex<double> (0, 1) * amp[518] + amp[521] +
      amp[520] + amp[523] + amp[524] + amp[559] + amp[562] + amp[561] +
      amp[570] + amp[571] + amp[575] + amp[574] + amp[577] + amp[578] -
      Complex<double> (0, 1) * amp[623] + Complex<double> (0, 1) * amp[629] -
      Complex<double> (0, 1) * amp[627] + amp[635] - amp[633] + Complex<double>
      (0, 1) * amp[1052] + amp[1053] + Complex<double> (0, 1) * amp[1054] +
      Complex<double> (0, 1) * amp[1058] - Complex<double> (0, 1) * amp[1056] -
      Complex<double> (0, 1) * amp[1063] - Complex<double> (0, 1) * amp[1067] +
      Complex<double> (0, 1) * amp[1065] + Complex<double> (0, 1) * amp[1071] +
      Complex<double> (0, 1) * amp[1072] - amp[1158] - amp[1161] - amp[1160] -
      amp[1172] - amp[1171] + amp[1178] + amp[1177] + amp[1180] + amp[1181] +
      Complex<double> (0, 1) * amp[1227] - amp[1333] + amp[1336] + amp[1335] +
      amp[1337] + amp[1340] + amp[1339] + amp[1342] + amp[1344] + amp[1345] +
      amp[1355] - amp[1353] - amp[1356] + amp[1357] - amp[1360] - amp[1359] -
      amp[1366] - amp[1368] - amp[1369] - amp[1376] - amp[1375] - amp[1379] +
      amp[1377] + amp[1382] + amp[1384] + amp[1385] + amp[1381] + amp[1389] +
      amp[1390] + amp[1392] + amp[1393] - amp[1712] + amp[1710] +
      Complex<double> (0, 1) * amp[1715] - Complex<double> (0, 1) * amp[1713] +
      Complex<double> (0, 1) * amp[1719] - Complex<double> (0, 1) * amp[1721] +
      amp[1725] - amp[1727] - amp[1730] + amp[1736] - amp[1729] + amp[1735] +
      amp[1742] - amp[1740] - amp[1865] + amp[1871] - amp[1864] + amp[1870] -
      amp[1875] + amp[1877];
  jamp[12] = +Complex<double> (0, 1) * amp[177] + Complex<double> (0, 1) *
      amp[178] + Complex<double> (0, 1) * amp[180] + Complex<double> (0, 1) *
      amp[179] + amp[183] - amp[190] + Complex<double> (0, 1) * amp[191] +
      Complex<double> (0, 1) * amp[195] - Complex<double> (0, 1) * amp[199] +
      Complex<double> (0, 1) * amp[197] - Complex<double> (0, 1) * amp[207] -
      amp[208] - amp[210] + Complex<double> (0, 1) * amp[211] - amp[254] +
      Complex<double> (0, 1) * amp[269] + Complex<double> (0, 1) * amp[272] +
      Complex<double> (0, 1) * amp[271] + amp[274] + Complex<double> (0, 1) *
      amp[275] - Complex<double> (0, 1) * amp[286] - Complex<double> (0, 1) *
      amp[287] + amp[293] + amp[292] + Complex<double> (0, 1) * amp[294] -
      Complex<double> (0, 1) * amp[296] - amp[299] + amp[297] - Complex<double>
      (0, 1) * amp[673] + amp[679] + amp[678] + amp[681] + amp[682] -
      Complex<double> (0, 1) * amp[687] + amp[688] - amp[709] - amp[712] -
      amp[711] - amp[725] + amp[723] + amp[733] + amp[732] + amp[735] +
      amp[736] + Complex<double> (0, 1) * amp[771] + amp[772] + Complex<double>
      (0, 1) * amp[774] + Complex<double> (0, 1) * amp[779] - Complex<double>
      (0, 1) * amp[777] - Complex<double> (0, 1) * amp[781] - Complex<double>
      (0, 1) * amp[782] + amp[783] + Complex<double> (0, 1) * amp[788] -
      Complex<double> (0, 1) * amp[786] + amp[794] - amp[792] - amp[1079] -
      amp[1080] - amp[1082] - amp[1081] - amp[1084] - amp[1086] - amp[1087] -
      amp[1094] - amp[1093] - amp[1097] + amp[1095] + Complex<double> (0, 1) *
      amp[1142] + Complex<double> (0, 1) * amp[1143] + Complex<double> (0, 1) *
      amp[1144] + amp[1153] + amp[1156] + amp[1155] - amp[1157] + amp[1161] -
      amp[1159] - amp[1162] + amp[1166] - amp[1164] - amp[1175] + amp[1173] -
      amp[1177] - amp[1176] - amp[1179] - amp[1180] - Complex<double> (0, 1) *
      amp[1215] - amp[1216] + Complex<double> (0, 1) * amp[1220] -
      Complex<double> (0, 1) * amp[1223] + Complex<double> (0, 1) * amp[1221] +
      Complex<double> (0, 1) * amp[1225] - amp[1332] - amp[1335] - amp[1334] -
      amp[1352] - amp[1351] - amp[1542] - amp[1540] - amp[1543] - amp[1539] -
      amp[1548] - amp[1549] - amp[1554] - amp[1555] - amp[1581] - amp[1582] +
      Complex<double> (0, 1) * amp[1584] + Complex<double> (0, 1) * amp[1585] +
      Complex<double> (0, 1) * amp[1587] + Complex<double> (0, 1) * amp[1588] +
      amp[1812] + amp[1813] + amp[1816] + amp[1815] + amp[1820] + amp[1819] +
      amp[1822] + amp[1823] - Complex<double> (0, 1) * amp[1832] -
      Complex<double> (0, 1) * amp[1831] - amp[1838] + amp[1844] - amp[1837] +
      amp[1843] - amp[1848] + amp[1850];
  jamp[13] = +Complex<double> (0, 1) * amp[196] + Complex<double> (0, 1) *
      amp[199] + Complex<double> (0, 1) * amp[198] - Complex<double> (0, 1) *
      amp[204] - amp[205] + amp[210] - Complex<double> (0, 1) * amp[211] +
      Complex<double> (0, 1) * amp[213] + Complex<double> (0, 1) * amp[214] +
      Complex<double> (0, 1) * amp[216] + Complex<double> (0, 1) * amp[215] +
      amp[221] - amp[223] + Complex<double> (0, 1) * amp[224] - amp[234] -
      Complex<double> (0, 1) * amp[269] - Complex<double> (0, 1) * amp[272] -
      Complex<double> (0, 1) * amp[271] - amp[274] - Complex<double> (0, 1) *
      amp[275] - Complex<double> (0, 1) * amp[294] - Complex<double> (0, 1) *
      amp[295] - amp[297] - amp[298] - Complex<double> (0, 1) * amp[304] -
      Complex<double> (0, 1) * amp[305] + amp[311] + amp[310] - Complex<double>
      (0, 1) * amp[691] + amp[697] + amp[696] + amp[699] + amp[700] -
      Complex<double> (0, 1) * amp[705] + amp[706] - amp[708] + amp[712] -
      amp[710] - amp[728] + amp[726] - amp[733] - amp[732] - amp[735] -
      amp[736] + Complex<double> (0, 1) * amp[762] + amp[763] + Complex<double>
      (0, 1) * amp[765] + Complex<double> (0, 1) * amp[770] - Complex<double>
      (0, 1) * amp[768] + Complex<double> (0, 1) * amp[781] + Complex<double>
      (0, 1) * amp[782] - amp[783] + Complex<double> (0, 1) * amp[786] +
      Complex<double> (0, 1) * amp[787] + amp[792] + amp[793] + amp[1152] -
      amp[1156] + amp[1154] - amp[1158] - amp[1161] - amp[1160] - amp[1163] +
      amp[1169] - amp[1167] - amp[1172] + amp[1170] + amp[1177] + amp[1176] +
      amp[1179] + amp[1180] - Complex<double> (0, 1) * amp[1206] - amp[1207] +
      Complex<double> (0, 1) * amp[1211] - Complex<double> (0, 1) * amp[1214] +
      Complex<double> (0, 1) * amp[1212] - Complex<double> (0, 1) * amp[1225] -
      amp[1235] - amp[1236] - amp[1238] - amp[1237] - amp[1241] - amp[1245] -
      amp[1246] - amp[1250] + amp[1248] - amp[1253] - amp[1252] +
      Complex<double> (0, 1) * amp[1289] + Complex<double> (0, 1) * amp[1290] +
      Complex<double> (0, 1) * amp[1291] + amp[1332] + amp[1335] + amp[1334] +
      amp[1352] + amp[1351] - amp[1541] + amp[1542] + amp[1539] - amp[1544] -
      amp[1551] - amp[1552] + amp[1554] + amp[1555] - amp[1575] - amp[1576] +
      Complex<double> (0, 1) * amp[1578] + Complex<double> (0, 1) * amp[1579] -
      Complex<double> (0, 1) * amp[1587] - Complex<double> (0, 1) * amp[1588] +
      amp[1838] + amp[1837] + amp[1840] + amp[1841] + amp[1848] + amp[1849] +
      amp[1866] + amp[1867] + amp[1870] + amp[1869] + amp[1874] + amp[1873] +
      amp[1876] + amp[1877] - Complex<double> (0, 1) * amp[1886] -
      Complex<double> (0, 1) * amp[1885];
  jamp[14] = -Complex<double> (0, 1) * amp[177] - Complex<double> (0, 1) *
      amp[178] - Complex<double> (0, 1) * amp[180] - Complex<double> (0, 1) *
      amp[179] - amp[183] + amp[190] - Complex<double> (0, 1) * amp[191] -
      Complex<double> (0, 1) * amp[213] + Complex<double> (0, 1) * amp[217] -
      Complex<double> (0, 1) * amp[215] + Complex<double> (0, 1) * amp[220] -
      amp[221] - Complex<double> (0, 1) * amp[229] - amp[230] - amp[253] +
      amp[256] - Complex<double> (0, 1) * amp[257] - Complex<double> (0, 1) *
      amp[260] - Complex<double> (0, 1) * amp[263] - Complex<double> (0, 1) *
      amp[262] - Complex<double> (0, 1) * amp[285] + Complex<double> (0, 1) *
      amp[287] - amp[293] + amp[291] + Complex<double> (0, 1) * amp[295] +
      Complex<double> (0, 1) * amp[296] + amp[299] + amp[298] + Complex<double>
      (0, 1) * amp[673] - amp[679] - amp[678] - amp[681] - amp[682] +
      Complex<double> (0, 1) * amp[687] - amp[688] + amp[690] + Complex<double>
      (0, 1) * amp[691] - Complex<double> (0, 1) * amp[695] - amp[698] -
      amp[697] - amp[700] - amp[701] + amp[714] - amp[717] - amp[716] -
      amp[723] - amp[724] + Complex<double> (0, 1) * amp[773] + amp[775] -
      Complex<double> (0, 1) * amp[776] + Complex<double> (0, 1) * amp[777] +
      Complex<double> (0, 1) * amp[778] - Complex<double> (0, 1) * amp[788] -
      Complex<double> (0, 1) * amp[787] - amp[794] - amp[793] + amp[1079] +
      amp[1080] + amp[1082] + amp[1081] + amp[1084] + amp[1086] + amp[1087] +
      amp[1094] + amp[1093] + amp[1097] - amp[1095] - Complex<double> (0, 1) *
      amp[1142] - Complex<double> (0, 1) * amp[1143] - Complex<double> (0, 1) *
      amp[1144] + amp[1235] - amp[1239] + amp[1237] + amp[1253] + amp[1252] +
      amp[1256] + amp[1255] + amp[1258] + amp[1259] + Complex<double> (0, 1) *
      amp[1305] + amp[1308] + amp[1311] + amp[1310] - amp[1314] + amp[1317] +
      amp[1316] + amp[1319] + amp[1323] + amp[1324] + amp[1328] - amp[1326] -
      amp[1478] + Complex<double> (0, 1) * amp[1479] - Complex<double> (0, 1) *
      amp[1481] - Complex<double> (0, 1) * amp[1482] - Complex<double> (0, 1) *
      amp[1483] + Complex<double> (0, 1) * amp[1665] + Complex<double> (0, 1) *
      amp[1666] + amp[1671] + amp[1672] - amp[1677] - amp[1675] - amp[1678] -
      amp[1674] - amp[1683] - amp[1684] + Complex<double> (0, 1) * amp[1695] +
      Complex<double> (0, 1) * amp[1696] - amp[1698] - amp[1699] + amp[1810] +
      amp[1809] - amp[1816] - amp[1815] - amp[1820] + amp[1818] + amp[1821] -
      amp[1823] + Complex<double> (0, 1) * amp[1832] - Complex<double> (0, 1) *
      amp[1830] - amp[1844] - amp[1840] - amp[1843] - amp[1841] - amp[1849] -
      amp[1850];
  jamp[15] = -amp[162] - amp[165] - Complex<double> (0, 1) * amp[166] -
      amp[168] + Complex<double> (0, 1) * amp[170] + amp[176] + amp[175] +
      Complex<double> (0, 1) * amp[213] - Complex<double> (0, 1) * amp[217] +
      Complex<double> (0, 1) * amp[215] - Complex<double> (0, 1) * amp[220] +
      amp[221] + Complex<double> (0, 1) * amp[229] + amp[230] - Complex<double>
      (0, 1) * amp[261] + Complex<double> (0, 1) * amp[264] + Complex<double>
      (0, 1) * amp[263] - Complex<double> (0, 1) * amp[269] - Complex<double>
      (0, 1) * amp[270] + Complex<double> (0, 1) * amp[273] - Complex<double>
      (0, 1) * amp[271] - amp[274] - Complex<double> (0, 1) * amp[294] -
      Complex<double> (0, 1) * amp[295] - amp[297] - amp[298] + Complex<double>
      (0, 1) * amp[316] + Complex<double> (0, 1) * amp[317] + Complex<double>
      (0, 1) * amp[655] + Complex<double> (0, 1) * amp[656] + amp[657] +
      Complex<double> (0, 1) * amp[662] - Complex<double> (0, 1) * amp[660] +
      amp[670] - Complex<double> (0, 1) * amp[671] - amp[690] - Complex<double>
      (0, 1) * amp[691] + Complex<double> (0, 1) * amp[695] + amp[698] +
      amp[697] + amp[700] + amp[701] + amp[713] + amp[716] + amp[715] +
      amp[731] - amp[729] - amp[734] - amp[733] - amp[736] - amp[737] +
      Complex<double> (0, 1) * amp[782] + Complex<double> (0, 1) * amp[786] +
      Complex<double> (0, 1) * amp[787] + amp[792] + amp[793] - Complex<double>
      (0, 1) * amp[1043] - amp[1044] - Complex<double> (0, 1) * amp[1045] -
      Complex<double> (0, 1) * amp[1049] + Complex<double> (0, 1) * amp[1047] -
      Complex<double> (0, 1) * amp[1054] + Complex<double> (0, 1) * amp[1056] +
      Complex<double> (0, 1) * amp[1057] - Complex<double> (0, 1) * amp[1073] -
      Complex<double> (0, 1) * amp[1072] - amp[1235] + amp[1239] - amp[1237] -
      amp[1253] - amp[1252] - amp[1256] - amp[1255] - amp[1258] - amp[1259] -
      Complex<double> (0, 1) * amp[1305] + amp[1309] - amp[1312] - amp[1311] -
      amp[1313] - amp[1316] - amp[1315] - amp[1318] - amp[1320] - amp[1321] -
      amp[1331] + amp[1329] + amp[1332] + amp[1333] - amp[1336] + amp[1334] -
      amp[1342] - amp[1344] - amp[1345] + amp[1352] + amp[1351] + amp[1353] +
      amp[1354] - amp[1384] - amp[1385] - amp[1388] - amp[1387] - amp[1390] -
      amp[1391] - amp[1394] - amp[1393] - amp[1656] - amp[1657] +
      Complex<double> (0, 1) * amp[1659] + Complex<double> (0, 1) * amp[1660] -
      Complex<double> (0, 1) * amp[1665] - Complex<double> (0, 1) * amp[1666] -
      amp[1671] - amp[1672] + amp[1676] + amp[1675] + amp[1678] + amp[1679] +
      amp[1686] + amp[1687] + amp[1838] + amp[1837] + amp[1840] + amp[1841] +
      amp[1848] + amp[1849];
  jamp[16] = -Complex<double> (0, 1) * amp[178] - Complex<double> (0, 1) *
      amp[181] - Complex<double> (0, 1) * amp[180] + Complex<double> (0, 1) *
      amp[182] - amp[183] + Complex<double> (0, 1) * amp[193] + amp[194] -
      Complex<double> (0, 1) * amp[213] - Complex<double> (0, 1) * amp[214] -
      Complex<double> (0, 1) * amp[216] - Complex<double> (0, 1) * amp[215] -
      amp[221] + amp[223] - Complex<double> (0, 1) * amp[224] - amp[233] -
      amp[238] + Complex<double> (0, 1) * amp[239] + Complex<double> (0, 1) *
      amp[278] + Complex<double> (0, 1) * amp[281] + Complex<double> (0, 1) *
      amp[280] + Complex<double> (0, 1) * amp[295] + Complex<double> (0, 1) *
      amp[296] + amp[299] + amp[298] + Complex<double> (0, 1) * amp[303] +
      Complex<double> (0, 1) * amp[304] - amp[309] - amp[310] + amp[672] +
      Complex<double> (0, 1) * amp[673] - Complex<double> (0, 1) * amp[677] -
      amp[680] - amp[679] - amp[682] - amp[683] + Complex<double> (0, 1) *
      amp[691] - amp[697] - amp[696] - amp[699] - amp[700] + Complex<double>
      (0, 1) * amp[705] - amp[706] + amp[719] - amp[722] - amp[721] - amp[726]
      - amp[727] + Complex<double> (0, 1) * amp[764] + amp[766] -
      Complex<double> (0, 1) * amp[767] + Complex<double> (0, 1) * amp[768] +
      Complex<double> (0, 1) * amp[769] - Complex<double> (0, 1) * amp[788] -
      Complex<double> (0, 1) * amp[787] - amp[794] - amp[793] + amp[1080] +
      amp[1083] + amp[1082] + amp[1094] + amp[1093] - amp[1100] - amp[1099] -
      amp[1102] - amp[1103] - Complex<double> (0, 1) * amp[1149] + amp[1235] +
      amp[1236] + amp[1238] + amp[1237] + amp[1241] + amp[1245] + amp[1246] +
      amp[1250] - amp[1248] + amp[1253] + amp[1252] - Complex<double> (0, 1) *
      amp[1289] - Complex<double> (0, 1) * amp[1290] - Complex<double> (0, 1) *
      amp[1291] - amp[1356] - amp[1359] - amp[1358] + amp[1362] - amp[1365] -
      amp[1364] - amp[1367] - amp[1371] - amp[1372] - amp[1376] + amp[1374] +
      amp[1460] - Complex<double> (0, 1) * amp[1461] + Complex<double> (0, 1) *
      amp[1463] + Complex<double> (0, 1) * amp[1464] + Complex<double> (0, 1) *
      amp[1465] - Complex<double> (0, 1) * amp[1773] + Complex<double> (0, 1) *
      amp[1775] - amp[1779] + amp[1781] + amp[1783] + amp[1782] - amp[1789] -
      amp[1788] - amp[1793] + amp[1791] + Complex<double> (0, 1) * amp[1805] -
      Complex<double> (0, 1) * amp[1803] - amp[1808] + amp[1806] - amp[1844] -
      amp[1840] - amp[1843] - amp[1841] - amp[1849] - amp[1850] - amp[1866] -
      amp[1864] - amp[1867] - amp[1863] - amp[1872] - amp[1873] - amp[1875] -
      amp[1876] + Complex<double> (0, 1) * amp[1884] + Complex<double> (0, 1) *
      amp[1885];
  jamp[17] = -amp[161] + amp[168] - Complex<double> (0, 1) * amp[170] -
      amp[171] + Complex<double> (0, 1) * amp[173] - amp[174] - amp[175] +
      Complex<double> (0, 1) * amp[178] + Complex<double> (0, 1) * amp[181] +
      Complex<double> (0, 1) * amp[180] - Complex<double> (0, 1) * amp[182] +
      amp[183] - Complex<double> (0, 1) * amp[193] - amp[194] + Complex<double>
      (0, 1) * amp[269] + Complex<double> (0, 1) * amp[270] - Complex<double>
      (0, 1) * amp[273] + Complex<double> (0, 1) * amp[271] + amp[274] -
      Complex<double> (0, 1) * amp[279] + Complex<double> (0, 1) * amp[282] -
      Complex<double> (0, 1) * amp[280] + Complex<double> (0, 1) * amp[294] -
      Complex<double> (0, 1) * amp[296] - amp[299] + amp[297] - Complex<double>
      (0, 1) * amp[315] - Complex<double> (0, 1) * amp[316] + Complex<double>
      (0, 1) * amp[654] + Complex<double> (0, 1) * amp[658] + amp[659] +
      Complex<double> (0, 1) * amp[660] + Complex<double> (0, 1) * amp[661] -
      amp[670] + Complex<double> (0, 1) * amp[671] - amp[672] - Complex<double>
      (0, 1) * amp[673] + Complex<double> (0, 1) * amp[677] + amp[680] +
      amp[679] + amp[682] + amp[683] + amp[718] + amp[721] + amp[720] +
      amp[729] + amp[730] + amp[734] + amp[733] + amp[736] + amp[737] -
      Complex<double> (0, 1) * amp[782] + Complex<double> (0, 1) * amp[788] -
      Complex<double> (0, 1) * amp[786] + amp[794] - amp[792] + Complex<double>
      (0, 1) * amp[1054] - Complex<double> (0, 1) * amp[1056] - Complex<double>
      (0, 1) * amp[1057] + Complex<double> (0, 1) * amp[1061] + amp[1062] -
      Complex<double> (0, 1) * amp[1063] + Complex<double> (0, 1) * amp[1065] +
      Complex<double> (0, 1) * amp[1066] + Complex<double> (0, 1) * amp[1071] +
      Complex<double> (0, 1) * amp[1072] - amp[1080] - amp[1083] - amp[1082] -
      amp[1094] - amp[1093] + amp[1100] + amp[1099] + amp[1102] + amp[1103] +
      Complex<double> (0, 1) * amp[1149] - amp[1332] - amp[1333] + amp[1336] -
      amp[1334] + amp[1342] + amp[1344] + amp[1345] - amp[1352] - amp[1351] -
      amp[1353] - amp[1354] + amp[1357] - amp[1360] + amp[1358] + amp[1361] +
      amp[1364] + amp[1363] - amp[1366] - amp[1368] - amp[1369] + amp[1377] +
      amp[1378] + amp[1382] + amp[1384] + amp[1385] + amp[1381] + amp[1389] +
      amp[1390] + amp[1392] + amp[1393] - amp[1766] + amp[1764] +
      Complex<double> (0, 1) * amp[1769] - Complex<double> (0, 1) * amp[1767] +
      Complex<double> (0, 1) * amp[1773] - Complex<double> (0, 1) * amp[1775] +
      amp[1779] - amp[1781] - amp[1784] + amp[1790] - amp[1783] + amp[1789] +
      amp[1796] - amp[1794] - amp[1838] + amp[1844] - amp[1837] + amp[1843] -
      amp[1848] + amp[1850];
  jamp[18] = +Complex<double> (0, 1) * amp[177] + Complex<double> (0, 1) *
      amp[178] + Complex<double> (0, 1) * amp[180] + Complex<double> (0, 1) *
      amp[179] + amp[185] - amp[187] + Complex<double> (0, 1) * amp[188] +
      Complex<double> (0, 1) * amp[213] - Complex<double> (0, 1) * amp[217] +
      Complex<double> (0, 1) * amp[215] - Complex<double> (0, 1) * amp[225] -
      amp[226] - amp[228] + Complex<double> (0, 1) * amp[229] - amp[245] +
      Complex<double> (0, 1) * amp[260] + Complex<double> (0, 1) * amp[263] +
      Complex<double> (0, 1) * amp[262] + amp[265] + Complex<double> (0, 1) *
      amp[266] + Complex<double> (0, 1) * amp[285] - Complex<double> (0, 1) *
      amp[287] - amp[290] + amp[288] - Complex<double> (0, 1) * amp[295] -
      Complex<double> (0, 1) * amp[296] + amp[302] + amp[301] - Complex<double>
      (0, 1) * amp[832] + amp[838] + amp[837] + amp[840] + amp[841] -
      Complex<double> (0, 1) * amp[846] + amp[847] - amp[868] - amp[871] -
      amp[870] - amp[884] + amp[882] + amp[892] + amp[891] + amp[894] +
      amp[895] + Complex<double> (0, 1) * amp[930] + amp[931] + Complex<double>
      (0, 1) * amp[933] + Complex<double> (0, 1) * amp[938] - Complex<double>
      (0, 1) * amp[936] - Complex<double> (0, 1) * amp[940] - Complex<double>
      (0, 1) * amp[941] + amp[942] + Complex<double> (0, 1) * amp[947] -
      Complex<double> (0, 1) * amp[945] + amp[953] - amp[951] - amp[1079] -
      amp[1080] - amp[1082] - amp[1081] - amp[1085] - amp[1089] - amp[1090] -
      amp[1094] + amp[1092] - amp[1097] - amp[1096] + Complex<double> (0, 1) *
      amp[1133] + Complex<double> (0, 1) * amp[1134] + Complex<double> (0, 1) *
      amp[1135] + amp[1231] + amp[1234] + amp[1233] - amp[1235] + amp[1239] -
      amp[1237] - amp[1240] + amp[1244] - amp[1242] - amp[1253] + amp[1251] -
      amp[1255] - amp[1254] - amp[1257] - amp[1258] - Complex<double> (0, 1) *
      amp[1293] - amp[1294] + Complex<double> (0, 1) * amp[1298] -
      Complex<double> (0, 1) * amp[1301] + Complex<double> (0, 1) * amp[1299] +
      Complex<double> (0, 1) * amp[1303] - amp[1308] - amp[1311] - amp[1310] -
      amp[1328] - amp[1327] - amp[1596] - amp[1594] - amp[1597] - amp[1593] -
      amp[1602] - amp[1603] - amp[1608] - amp[1609] - amp[1635] - amp[1636] +
      Complex<double> (0, 1) * amp[1638] + Complex<double> (0, 1) * amp[1639] +
      Complex<double> (0, 1) * amp[1641] + Complex<double> (0, 1) * amp[1642] -
      amp[1811] + amp[1817] - amp[1810] + amp[1816] - amp[1821] + amp[1823] +
      amp[1839] + amp[1840] + amp[1843] + amp[1842] + amp[1847] + amp[1846] +
      amp[1849] + amp[1850] - Complex<double> (0, 1) * amp[1859] -
      Complex<double> (0, 1) * amp[1858];
  jamp[19] = +Complex<double> (0, 1) * amp[195] + Complex<double> (0, 1) *
      amp[196] + Complex<double> (0, 1) * amp[198] + Complex<double> (0, 1) *
      amp[197] + amp[203] - amp[205] + Complex<double> (0, 1) * amp[206] +
      Complex<double> (0, 1) * amp[214] + Complex<double> (0, 1) * amp[217] +
      Complex<double> (0, 1) * amp[216] - Complex<double> (0, 1) * amp[222] -
      amp[223] + amp[228] - Complex<double> (0, 1) * amp[229] - amp[236] -
      Complex<double> (0, 1) * amp[260] - Complex<double> (0, 1) * amp[263] -
      Complex<double> (0, 1) * amp[262] - amp[265] - Complex<double> (0, 1) *
      amp[266] - Complex<double> (0, 1) * amp[285] - Complex<double> (0, 1) *
      amp[286] - amp[288] - amp[289] - Complex<double> (0, 1) * amp[304] -
      Complex<double> (0, 1) * amp[305] + amp[311] + amp[310] - Complex<double>
      (0, 1) * amp[850] + amp[856] + amp[855] + amp[858] + amp[859] -
      Complex<double> (0, 1) * amp[864] + amp[865] - amp[867] + amp[871] -
      amp[869] - amp[887] + amp[885] - amp[892] - amp[891] - amp[894] -
      amp[895] + Complex<double> (0, 1) * amp[921] + amp[922] + Complex<double>
      (0, 1) * amp[924] + Complex<double> (0, 1) * amp[929] - Complex<double>
      (0, 1) * amp[927] + Complex<double> (0, 1) * amp[940] + Complex<double>
      (0, 1) * amp[941] - amp[942] + Complex<double> (0, 1) * amp[945] +
      Complex<double> (0, 1) * amp[946] + amp[951] + amp[952] - amp[1157] -
      amp[1158] - amp[1160] - amp[1159] - amp[1163] - amp[1167] - amp[1168] -
      amp[1172] + amp[1170] - amp[1175] - amp[1174] + Complex<double> (0, 1) *
      amp[1211] + Complex<double> (0, 1) * amp[1212] + Complex<double> (0, 1) *
      amp[1213] + amp[1230] - amp[1234] + amp[1232] - amp[1236] - amp[1239] -
      amp[1238] - amp[1241] + amp[1247] - amp[1245] - amp[1250] + amp[1248] +
      amp[1255] + amp[1254] + amp[1257] + amp[1258] - Complex<double> (0, 1) *
      amp[1284] - amp[1285] + Complex<double> (0, 1) * amp[1289] -
      Complex<double> (0, 1) * amp[1292] + Complex<double> (0, 1) * amp[1290] -
      Complex<double> (0, 1) * amp[1303] + amp[1308] + amp[1311] + amp[1310] +
      amp[1328] + amp[1327] - amp[1595] + amp[1596] + amp[1593] - amp[1598] -
      amp[1605] - amp[1606] + amp[1608] + amp[1609] - amp[1629] - amp[1630] +
      Complex<double> (0, 1) * amp[1632] + Complex<double> (0, 1) * amp[1633] -
      Complex<double> (0, 1) * amp[1641] - Complex<double> (0, 1) * amp[1642] +
      amp[1811] + amp[1810] + amp[1813] + amp[1814] + amp[1821] + amp[1822] +
      amp[1866] + amp[1867] + amp[1870] + amp[1869] + amp[1874] + amp[1873] +
      amp[1876] + amp[1877] - Complex<double> (0, 1) * amp[1886] -
      Complex<double> (0, 1) * amp[1885];
  jamp[20] = -Complex<double> (0, 1) * amp[177] - Complex<double> (0, 1) *
      amp[178] - Complex<double> (0, 1) * amp[180] - Complex<double> (0, 1) *
      amp[179] - amp[185] + amp[187] - Complex<double> (0, 1) * amp[188] -
      Complex<double> (0, 1) * amp[195] + Complex<double> (0, 1) * amp[199] -
      Complex<double> (0, 1) * amp[197] + Complex<double> (0, 1) * amp[202] -
      amp[203] - Complex<double> (0, 1) * amp[211] - amp[212] - amp[244] +
      amp[247] - Complex<double> (0, 1) * amp[248] - Complex<double> (0, 1) *
      amp[269] - Complex<double> (0, 1) * amp[272] - Complex<double> (0, 1) *
      amp[271] + Complex<double> (0, 1) * amp[286] + Complex<double> (0, 1) *
      amp[287] + amp[290] + amp[289] - Complex<double> (0, 1) * amp[294] +
      Complex<double> (0, 1) * amp[296] - amp[302] + amp[300] + Complex<double>
      (0, 1) * amp[832] - amp[838] - amp[837] - amp[840] - amp[841] +
      Complex<double> (0, 1) * amp[846] - amp[847] + amp[849] + Complex<double>
      (0, 1) * amp[850] - Complex<double> (0, 1) * amp[854] - amp[857] -
      amp[856] - amp[859] - amp[860] + amp[873] - amp[876] - amp[875] -
      amp[882] - amp[883] + Complex<double> (0, 1) * amp[932] + amp[934] -
      Complex<double> (0, 1) * amp[935] + Complex<double> (0, 1) * amp[936] +
      Complex<double> (0, 1) * amp[937] - Complex<double> (0, 1) * amp[947] -
      Complex<double> (0, 1) * amp[946] - amp[953] - amp[952] + amp[1079] +
      amp[1080] + amp[1082] + amp[1081] + amp[1085] + amp[1089] + amp[1090] +
      amp[1094] - amp[1092] + amp[1097] + amp[1096] - Complex<double> (0, 1) *
      amp[1133] - Complex<double> (0, 1) * amp[1134] - Complex<double> (0, 1) *
      amp[1135] + amp[1157] - amp[1161] + amp[1159] + amp[1175] + amp[1174] +
      amp[1178] + amp[1177] + amp[1180] + amp[1181] + Complex<double> (0, 1) *
      amp[1227] + amp[1332] + amp[1335] + amp[1334] - amp[1338] + amp[1341] +
      amp[1340] + amp[1343] + amp[1347] + amp[1348] + amp[1352] - amp[1350] -
      amp[1469] + Complex<double> (0, 1) * amp[1470] - Complex<double> (0, 1) *
      amp[1472] - Complex<double> (0, 1) * amp[1473] - Complex<double> (0, 1) *
      amp[1474] + Complex<double> (0, 1) * amp[1719] + Complex<double> (0, 1) *
      amp[1720] + amp[1725] + amp[1726] - amp[1731] - amp[1729] - amp[1732] -
      amp[1728] - amp[1737] - amp[1738] + Complex<double> (0, 1) * amp[1749] +
      Complex<double> (0, 1) * amp[1750] - amp[1752] - amp[1753] - amp[1817] -
      amp[1813] - amp[1816] - amp[1814] - amp[1822] - amp[1823] + amp[1837] +
      amp[1836] - amp[1843] - amp[1842] - amp[1847] + amp[1845] + amp[1848] -
      amp[1850] + Complex<double> (0, 1) * amp[1859] - Complex<double> (0, 1) *
      amp[1857];
  jamp[21] = -amp[164] - amp[165] + Complex<double> (0, 1) * amp[167] -
      amp[168] - Complex<double> (0, 1) * amp[169] + amp[176] + amp[175] +
      Complex<double> (0, 1) * amp[195] - Complex<double> (0, 1) * amp[199] +
      Complex<double> (0, 1) * amp[197] - Complex<double> (0, 1) * amp[202] +
      amp[203] + Complex<double> (0, 1) * amp[211] + amp[212] - Complex<double>
      (0, 1) * amp[260] - Complex<double> (0, 1) * amp[261] + Complex<double>
      (0, 1) * amp[264] - Complex<double> (0, 1) * amp[262] - amp[265] -
      Complex<double> (0, 1) * amp[270] + Complex<double> (0, 1) * amp[273] +
      Complex<double> (0, 1) * amp[272] - Complex<double> (0, 1) * amp[285] -
      Complex<double> (0, 1) * amp[286] - amp[288] - amp[289] + Complex<double>
      (0, 1) * amp[316] + Complex<double> (0, 1) * amp[317] + Complex<double>
      (0, 1) * amp[814] + Complex<double> (0, 1) * amp[815] + amp[816] +
      Complex<double> (0, 1) * amp[821] - Complex<double> (0, 1) * amp[819] +
      amp[829] - Complex<double> (0, 1) * amp[830] - amp[849] - Complex<double>
      (0, 1) * amp[850] + Complex<double> (0, 1) * amp[854] + amp[857] +
      amp[856] + amp[859] + amp[860] + amp[872] + amp[875] + amp[874] +
      amp[890] - amp[888] - amp[893] - amp[892] - amp[895] - amp[896] +
      Complex<double> (0, 1) * amp[941] + Complex<double> (0, 1) * amp[945] +
      Complex<double> (0, 1) * amp[946] + amp[951] + amp[952] - Complex<double>
      (0, 1) * amp[1045] + Complex<double> (0, 1) * amp[1047] + Complex<double>
      (0, 1) * amp[1048] - Complex<double> (0, 1) * amp[1052] - amp[1053] -
      Complex<double> (0, 1) * amp[1054] - Complex<double> (0, 1) * amp[1058] +
      Complex<double> (0, 1) * amp[1056] - Complex<double> (0, 1) * amp[1073] -
      Complex<double> (0, 1) * amp[1072] - amp[1157] + amp[1161] - amp[1159] -
      amp[1175] - amp[1174] - amp[1178] - amp[1177] - amp[1180] - amp[1181] -
      Complex<double> (0, 1) * amp[1227] + amp[1308] + amp[1309] - amp[1312] +
      amp[1310] - amp[1318] - amp[1320] - amp[1321] + amp[1328] + amp[1327] +
      amp[1329] + amp[1330] + amp[1333] - amp[1336] - amp[1335] - amp[1337] -
      amp[1340] - amp[1339] - amp[1342] - amp[1344] - amp[1345] - amp[1355] +
      amp[1353] - amp[1384] - amp[1385] - amp[1388] - amp[1387] - amp[1390] -
      amp[1391] - amp[1394] - amp[1393] - amp[1710] - amp[1711] +
      Complex<double> (0, 1) * amp[1713] + Complex<double> (0, 1) * amp[1714] -
      Complex<double> (0, 1) * amp[1719] - Complex<double> (0, 1) * amp[1720] -
      amp[1725] - amp[1726] + amp[1730] + amp[1729] + amp[1732] + amp[1733] +
      amp[1740] + amp[1741] + amp[1811] + amp[1810] + amp[1813] + amp[1814] +
      amp[1821] + amp[1822];
  jamp[22] = -Complex<double> (0, 1) * amp[177] + Complex<double> (0, 1) *
      amp[181] - Complex<double> (0, 1) * amp[179] + Complex<double> (0, 1) *
      amp[184] - amp[185] - Complex<double> (0, 1) * amp[193] - amp[194] -
      Complex<double> (0, 1) * amp[195] - Complex<double> (0, 1) * amp[196] -
      Complex<double> (0, 1) * amp[198] - Complex<double> (0, 1) * amp[197] -
      amp[203] + amp[205] - Complex<double> (0, 1) * amp[206] - amp[235] +
      amp[238] - Complex<double> (0, 1) * amp[239] - Complex<double> (0, 1) *
      amp[278] - Complex<double> (0, 1) * amp[281] - Complex<double> (0, 1) *
      amp[280] + Complex<double> (0, 1) * amp[286] + Complex<double> (0, 1) *
      amp[287] + amp[290] + amp[289] - Complex<double> (0, 1) * amp[303] +
      Complex<double> (0, 1) * amp[305] - amp[311] + amp[309] + amp[831] +
      Complex<double> (0, 1) * amp[832] - Complex<double> (0, 1) * amp[836] -
      amp[839] - amp[838] - amp[841] - amp[842] + Complex<double> (0, 1) *
      amp[850] - amp[856] - amp[855] - amp[858] - amp[859] + Complex<double>
      (0, 1) * amp[864] - amp[865] + amp[878] - amp[881] - amp[880] - amp[885]
      - amp[886] + Complex<double> (0, 1) * amp[923] + amp[925] -
      Complex<double> (0, 1) * amp[926] + Complex<double> (0, 1) * amp[927] +
      Complex<double> (0, 1) * amp[928] - Complex<double> (0, 1) * amp[947] -
      Complex<double> (0, 1) * amp[946] - amp[953] - amp[952] + amp[1079] -
      amp[1083] + amp[1081] + amp[1097] + amp[1096] + amp[1100] + amp[1099] +
      amp[1102] + amp[1103] + Complex<double> (0, 1) * amp[1149] + amp[1157] +
      amp[1158] + amp[1160] + amp[1159] + amp[1163] + amp[1167] + amp[1168] +
      amp[1172] - amp[1170] + amp[1175] + amp[1174] - Complex<double> (0, 1) *
      amp[1211] - Complex<double> (0, 1) * amp[1212] - Complex<double> (0, 1) *
      amp[1213] + amp[1356] + amp[1359] + amp[1358] - amp[1362] + amp[1365] +
      amp[1364] + amp[1367] + amp[1371] + amp[1372] + amp[1376] - amp[1374] -
      amp[1460] + Complex<double> (0, 1) * amp[1461] - Complex<double> (0, 1) *
      amp[1463] - Complex<double> (0, 1) * amp[1464] - Complex<double> (0, 1) *
      amp[1465] + Complex<double> (0, 1) * amp[1773] + Complex<double> (0, 1) *
      amp[1774] + amp[1779] + amp[1780] - amp[1785] - amp[1783] - amp[1786] -
      amp[1782] - amp[1791] - amp[1792] + Complex<double> (0, 1) * amp[1803] +
      Complex<double> (0, 1) * amp[1804] - amp[1806] - amp[1807] - amp[1817] -
      amp[1813] - amp[1816] - amp[1814] - amp[1822] - amp[1823] + amp[1864] +
      amp[1863] - amp[1870] - amp[1869] - amp[1874] + amp[1872] + amp[1875] -
      amp[1877] + Complex<double> (0, 1) * amp[1886] - Complex<double> (0, 1) *
      amp[1884];
  jamp[23] = -amp[163] + amp[165] - Complex<double> (0, 1) * amp[167] +
      amp[171] - Complex<double> (0, 1) * amp[173] - amp[176] + amp[174] +
      Complex<double> (0, 1) * amp[177] - Complex<double> (0, 1) * amp[181] +
      Complex<double> (0, 1) * amp[179] - Complex<double> (0, 1) * amp[184] +
      amp[185] + Complex<double> (0, 1) * amp[193] + amp[194] + Complex<double>
      (0, 1) * amp[260] + Complex<double> (0, 1) * amp[261] - Complex<double>
      (0, 1) * amp[264] + Complex<double> (0, 1) * amp[262] + amp[265] +
      Complex<double> (0, 1) * amp[279] - Complex<double> (0, 1) * amp[282] +
      Complex<double> (0, 1) * amp[280] + Complex<double> (0, 1) * amp[285] -
      Complex<double> (0, 1) * amp[287] - amp[290] + amp[288] + Complex<double>
      (0, 1) * amp[315] - Complex<double> (0, 1) * amp[317] + Complex<double>
      (0, 1) * amp[813] + Complex<double> (0, 1) * amp[817] + amp[818] +
      Complex<double> (0, 1) * amp[819] + Complex<double> (0, 1) * amp[820] -
      amp[829] + Complex<double> (0, 1) * amp[830] - amp[831] - Complex<double>
      (0, 1) * amp[832] + Complex<double> (0, 1) * amp[836] + amp[839] +
      amp[838] + amp[841] + amp[842] + amp[877] + amp[880] + amp[879] +
      amp[888] + amp[889] + amp[893] + amp[892] + amp[895] + amp[896] -
      Complex<double> (0, 1) * amp[941] + Complex<double> (0, 1) * amp[947] -
      Complex<double> (0, 1) * amp[945] + amp[953] - amp[951] + Complex<double>
      (0, 1) * amp[1045] - Complex<double> (0, 1) * amp[1047] - Complex<double>
      (0, 1) * amp[1048] - Complex<double> (0, 1) * amp[1061] - amp[1062] +
      Complex<double> (0, 1) * amp[1063] - Complex<double> (0, 1) * amp[1065] -
      Complex<double> (0, 1) * amp[1066] + Complex<double> (0, 1) * amp[1073] -
      Complex<double> (0, 1) * amp[1071] - amp[1079] + amp[1083] - amp[1081] -
      amp[1097] - amp[1096] - amp[1100] - amp[1099] - amp[1102] - amp[1103] -
      Complex<double> (0, 1) * amp[1149] - amp[1308] - amp[1309] + amp[1312] -
      amp[1310] + amp[1318] + amp[1320] + amp[1321] - amp[1328] - amp[1327] -
      amp[1329] - amp[1330] - amp[1357] + amp[1360] - amp[1358] - amp[1361] -
      amp[1364] - amp[1363] + amp[1366] + amp[1368] + amp[1369] - amp[1377] -
      amp[1378] - amp[1382] - amp[1381] + amp[1388] + amp[1387] - amp[1389] +
      amp[1391] + amp[1394] - amp[1392] - amp[1764] - amp[1765] +
      Complex<double> (0, 1) * amp[1767] + Complex<double> (0, 1) * amp[1768] -
      Complex<double> (0, 1) * amp[1773] - Complex<double> (0, 1) * amp[1774] -
      amp[1779] - amp[1780] + amp[1784] + amp[1783] + amp[1786] + amp[1787] +
      amp[1794] + amp[1795] - amp[1811] + amp[1817] - amp[1810] + amp[1816] -
      amp[1821] + amp[1823];
  jamp[24] = +amp[1] + amp[4] + amp[3] + amp[17] - amp[15] - amp[25] - amp[24]
      - amp[27] - amp[28] - amp[37] - amp[36] - amp[39] - amp[40] + amp[44] -
      amp[42] - Complex<double> (0, 1) * amp[124] - Complex<double> (0, 1) *
      amp[125] - amp[126] - Complex<double> (0, 1) * amp[131] + Complex<double>
      (0, 1) * amp[129] - amp[132] + Complex<double> (0, 1) * amp[134] +
      Complex<double> (0, 1) * amp[136] - amp[147] + Complex<double> (0, 1) *
      amp[148] + Complex<double> (0, 1) * amp[152] - Complex<double> (0, 1) *
      amp[155] + Complex<double> (0, 1) * amp[153] + Complex<double> (0, 1) *
      amp[479] + Complex<double> (0, 1) * amp[485] - Complex<double> (0, 1) *
      amp[483] + Complex<double> (0, 1) * amp[489] + amp[491] + amp[492] -
      Complex<double> (0, 1) * amp[494] + amp[550] + amp[553] + amp[552] -
      amp[554] - amp[555] + amp[558] - amp[556] + amp[566] + amp[565] -
      amp[572] + amp[570] + amp[575] - amp[573] - amp[576] + amp[578] -
      Complex<double> (0, 1) * amp[612] - amp[613] + Complex<double> (0, 1) *
      amp[617] - Complex<double> (0, 1) * amp[620] - Complex<double> (0, 1) *
      amp[619] + Complex<double> (0, 1) * amp[622] - amp[973] + Complex<double>
      (0, 1) * amp[978] + amp[980] + amp[982] + Complex<double> (0, 1) *
      amp[983] + Complex<double> (0, 1) * amp[985] + Complex<double> (0, 1) *
      amp[989] - Complex<double> (0, 1) * amp[987] + Complex<double> (0, 1) *
      amp[999] + amp[1000] + Complex<double> (0, 1) * amp[1003] +
      Complex<double> (0, 1) * amp[1007] - Complex<double> (0, 1) * amp[1005] -
      amp[1010] + amp[1008] + Complex<double> (0, 1) * amp[1013] -
      Complex<double> (0, 1) * amp[1011] - amp[1313] - amp[1314] + amp[1317] -
      amp[1315] - amp[1318] + amp[1322] - amp[1320] + amp[1325] + amp[1324] -
      amp[1331] + amp[1329] - amp[1366] + amp[1370] - amp[1368] - amp[1379] +
      amp[1377] + amp[1386] + amp[1382] - amp[1388] - amp[1380] - amp[1394] +
      amp[1392] + Complex<double> (0, 1) * amp[1479] - Complex<double> (0, 1) *
      amp[1484] - Complex<double> (0, 1) * amp[1483] + amp[1488] + amp[1489] +
      amp[1492] + amp[1491] + amp[1496] + amp[1495] + amp[1501] + amp[1502] +
      amp[1529] + amp[1528] - Complex<double> (0, 1) * amp[1532] -
      Complex<double> (0, 1) * amp[1531] - Complex<double> (0, 1) * amp[1534] -
      Complex<double> (0, 1) * amp[1535] + Complex<double> (0, 1) * amp[1652] -
      Complex<double> (0, 1) * amp[1650] - amp[1655] + amp[1653] + amp[1676] -
      amp[1682] - amp[1674] + amp[1680] + amp[1685] - amp[1683] - amp[1688] +
      amp[1686] - Complex<double> (0, 1) * amp[1697] + Complex<double> (0, 1) *
      amp[1695];
  jamp[25] = +amp[0] - amp[4] + amp[2] + amp[20] - amp[18] - amp[31] - amp[30]
      - amp[33] - amp[34] + amp[37] + amp[36] + amp[39] + amp[40] + amp[42] +
      amp[43] - Complex<double> (0, 1) * amp[112] - Complex<double> (0, 1) *
      amp[113] - amp[114] - Complex<double> (0, 1) * amp[119] + Complex<double>
      (0, 1) * amp[117] - amp[120] + Complex<double> (0, 1) * amp[122] +
      Complex<double> (0, 1) * amp[142] + amp[147] - Complex<double> (0, 1) *
      amp[148] - Complex<double> (0, 1) * amp[152] - Complex<double> (0, 1) *
      amp[153] - Complex<double> (0, 1) * amp[154] + Complex<double> (0, 1) *
      amp[481] + Complex<double> (0, 1) * amp[483] + Complex<double> (0, 1) *
      amp[484] + Complex<double> (0, 1) * amp[486] + amp[488] - amp[492] +
      Complex<double> (0, 1) * amp[494] + amp[549] - amp[553] + amp[551] -
      amp[559] - amp[560] + amp[563] - amp[561] + amp[569] + amp[568] -
      amp[570] - amp[571] - amp[575] + amp[573] + amp[576] - amp[578] -
      Complex<double> (0, 1) * amp[603] - amp[604] + Complex<double> (0, 1) *
      amp[608] - Complex<double> (0, 1) * amp[611] - Complex<double> (0, 1) *
      amp[610] - Complex<double> (0, 1) * amp[622] - amp[964] + Complex<double>
      (0, 1) * amp[969] + amp[971] + amp[991] + Complex<double> (0, 1) *
      amp[992] + Complex<double> (0, 1) * amp[994] + Complex<double> (0, 1) *
      amp[998] - Complex<double> (0, 1) * amp[996] - Complex<double> (0, 1) *
      amp[999] - amp[1000] - Complex<double> (0, 1) * amp[1003] -
      Complex<double> (0, 1) * amp[1007] + Complex<double> (0, 1) * amp[1005] -
      amp[1008] - amp[1009] + Complex<double> (0, 1) * amp[1011] +
      Complex<double> (0, 1) * amp[1012] - amp[1337] - amp[1338] + amp[1341] -
      amp[1339] - amp[1342] + amp[1346] - amp[1344] + amp[1349] + amp[1348] -
      amp[1355] + amp[1353] + amp[1366] - amp[1370] + amp[1368] + amp[1379] -
      amp[1377] - amp[1382] - amp[1385] + amp[1380] + amp[1383] - amp[1392] -
      amp[1393] + Complex<double> (0, 1) * amp[1470] - Complex<double> (0, 1) *
      amp[1475] - Complex<double> (0, 1) * amp[1474] - amp[1488] + amp[1493] +
      amp[1490] - amp[1491] + amp[1499] + amp[1498] - amp[1501] - amp[1502] +
      amp[1523] + amp[1522] - Complex<double> (0, 1) * amp[1526] -
      Complex<double> (0, 1) * amp[1525] + Complex<double> (0, 1) * amp[1534] +
      Complex<double> (0, 1) * amp[1535] + Complex<double> (0, 1) * amp[1706] -
      Complex<double> (0, 1) * amp[1704] - amp[1709] + amp[1707] + amp[1730] -
      amp[1736] - amp[1728] + amp[1734] + amp[1739] - amp[1737] - amp[1742] +
      amp[1740] - Complex<double> (0, 1) * amp[1751] + Complex<double> (0, 1) *
      amp[1749];
  jamp[26] = +amp[6] + amp[9] + amp[8] + amp[15] + amp[16] + amp[25] + amp[24]
      + amp[27] + amp[28] - amp[32] + amp[30] + amp[33] - amp[35] - amp[44] -
      amp[43] - Complex<double> (0, 1) * amp[123] - Complex<double> (0, 1) *
      amp[127] - amp[128] - Complex<double> (0, 1) * amp[129] - Complex<double>
      (0, 1) * amp[130] + amp[132] - Complex<double> (0, 1) * amp[134] -
      Complex<double> (0, 1) * amp[136] - amp[141] - Complex<double> (0, 1) *
      amp[142] + Complex<double> (0, 1) * amp[146] + Complex<double> (0, 1) *
      amp[155] + Complex<double> (0, 1) * amp[154] + Complex<double> (0, 1) *
      amp[638] + Complex<double> (0, 1) * amp[644] - Complex<double> (0, 1) *
      amp[642] + Complex<double> (0, 1) * amp[648] + amp[650] + amp[651] -
      Complex<double> (0, 1) * amp[653] + amp[709] + amp[712] + amp[711] -
      amp[713] - amp[714] + amp[717] - amp[715] + amp[725] + amp[724] -
      amp[731] + amp[729] + amp[734] - amp[732] - amp[735] + amp[737] -
      Complex<double> (0, 1) * amp[771] - amp[772] + Complex<double> (0, 1) *
      amp[776] - Complex<double> (0, 1) * amp[779] - Complex<double> (0, 1) *
      amp[778] + Complex<double> (0, 1) * amp[781] - amp[972] - Complex<double>
      (0, 1) * amp[978] - amp[980] - amp[982] - Complex<double> (0, 1) *
      amp[983] - Complex<double> (0, 1) * amp[985] - Complex<double> (0, 1) *
      amp[989] + Complex<double> (0, 1) * amp[987] + Complex<double> (0, 1) *
      amp[990] - amp[991] - Complex<double> (0, 1) * amp[994] + Complex<double>
      (0, 1) * amp[996] + Complex<double> (0, 1) * amp[997] + amp[1010] +
      amp[1009] - Complex<double> (0, 1) * amp[1013] - Complex<double> (0, 1) *
      amp[1012] + amp[1313] + amp[1314] - amp[1317] + amp[1315] + amp[1318] -
      amp[1322] + amp[1320] - amp[1325] - amp[1324] + amp[1331] - amp[1329] +
      amp[1342] - amp[1346] + amp[1344] - amp[1353] - amp[1354] - amp[1386] +
      amp[1385] + amp[1388] - amp[1383] + amp[1394] + amp[1393] -
      Complex<double> (0, 1) * amp[1479] + Complex<double> (0, 1) * amp[1484] +
      Complex<double> (0, 1) * amp[1483] + amp[1542] + amp[1543] + amp[1546] +
      amp[1545] + amp[1550] + amp[1549] + amp[1555] + amp[1556] + amp[1583] +
      amp[1582] - Complex<double> (0, 1) * amp[1586] - Complex<double> (0, 1) *
      amp[1585] - Complex<double> (0, 1) * amp[1588] - Complex<double> (0, 1) *
      amp[1589] + Complex<double> (0, 1) * amp[1650] + Complex<double> (0, 1) *
      amp[1651] - amp[1653] - amp[1654] - amp[1676] + amp[1677] + amp[1674] -
      amp[1679] + amp[1683] + amp[1684] - amp[1686] - amp[1687] -
      Complex<double> (0, 1) * amp[1695] - Complex<double> (0, 1) * amp[1696];
  jamp[27] = +amp[5] - amp[9] + amp[7] + amp[23] - amp[21] + amp[32] - amp[30]
      - amp[33] + amp[35] - amp[38] + amp[36] + amp[39] - amp[41] + amp[42] +
      amp[43] - Complex<double> (0, 1) * amp[100] - Complex<double> (0, 1) *
      amp[101] - amp[102] - Complex<double> (0, 1) * amp[107] + Complex<double>
      (0, 1) * amp[105] - amp[108] + Complex<double> (0, 1) * amp[110] +
      amp[141] + Complex<double> (0, 1) * amp[142] - Complex<double> (0, 1) *
      amp[146] - Complex<double> (0, 1) * amp[148] - Complex<double> (0, 1) *
      amp[153] - Complex<double> (0, 1) * amp[154] + Complex<double> (0, 1) *
      amp[640] + Complex<double> (0, 1) * amp[642] + Complex<double> (0, 1) *
      amp[643] + Complex<double> (0, 1) * amp[645] + amp[647] - amp[651] +
      Complex<double> (0, 1) * amp[653] + amp[708] - amp[712] + amp[710] -
      amp[718] - amp[719] + amp[722] - amp[720] + amp[728] + amp[727] -
      amp[729] - amp[730] - amp[734] + amp[732] + amp[735] - amp[737] -
      Complex<double> (0, 1) * amp[762] - amp[763] + Complex<double> (0, 1) *
      amp[767] - Complex<double> (0, 1) * amp[770] - Complex<double> (0, 1) *
      amp[769] - Complex<double> (0, 1) * amp[781] - amp[955] + Complex<double>
      (0, 1) * amp[960] + amp[962] - Complex<double> (0, 1) * amp[990] +
      amp[991] + Complex<double> (0, 1) * amp[994] - Complex<double> (0, 1) *
      amp[996] - Complex<double> (0, 1) * amp[997] - amp[1000] +
      Complex<double> (0, 1) * amp[1001] - Complex<double> (0, 1) * amp[1003] +
      Complex<double> (0, 1) * amp[1005] + Complex<double> (0, 1) * amp[1006] -
      amp[1008] - amp[1009] + Complex<double> (0, 1) * amp[1011] +
      Complex<double> (0, 1) * amp[1012] - amp[1342] + amp[1346] - amp[1344] +
      amp[1353] + amp[1354] - amp[1361] - amp[1362] + amp[1365] - amp[1363] +
      amp[1366] - amp[1370] + amp[1368] + amp[1373] + amp[1372] - amp[1377] -
      amp[1378] - amp[1382] - amp[1385] + amp[1380] + amp[1383] - amp[1392] -
      amp[1393] + Complex<double> (0, 1) * amp[1461] - Complex<double> (0, 1) *
      amp[1466] - Complex<double> (0, 1) * amp[1465] - amp[1542] + amp[1547] +
      amp[1544] - amp[1545] + amp[1553] + amp[1552] - amp[1555] - amp[1556] +
      amp[1577] + amp[1576] - Complex<double> (0, 1) * amp[1580] -
      Complex<double> (0, 1) * amp[1579] + Complex<double> (0, 1) * amp[1588] +
      Complex<double> (0, 1) * amp[1589] + Complex<double> (0, 1) * amp[1760] -
      Complex<double> (0, 1) * amp[1758] - amp[1763] + amp[1761] + amp[1784] -
      amp[1790] - amp[1782] + amp[1788] + amp[1793] - amp[1791] - amp[1796] +
      amp[1794] - Complex<double> (0, 1) * amp[1805] + Complex<double> (0, 1) *
      amp[1803];
  jamp[28] = +amp[11] + amp[14] + amp[13] + amp[18] + amp[19] - amp[26] +
      amp[24] + amp[27] - amp[29] + amp[31] + amp[30] + amp[33] + amp[34] -
      amp[44] - amp[43] - Complex<double> (0, 1) * amp[111] - Complex<double>
      (0, 1) * amp[115] - amp[116] - Complex<double> (0, 1) * amp[117] -
      Complex<double> (0, 1) * amp[118] + amp[120] - Complex<double> (0, 1) *
      amp[122] - amp[135] - Complex<double> (0, 1) * amp[136] + Complex<double>
      (0, 1) * amp[140] - Complex<double> (0, 1) * amp[142] + Complex<double>
      (0, 1) * amp[155] + Complex<double> (0, 1) * amp[154] + Complex<double>
      (0, 1) * amp[797] + Complex<double> (0, 1) * amp[803] - Complex<double>
      (0, 1) * amp[801] + Complex<double> (0, 1) * amp[807] + amp[809] +
      amp[810] - Complex<double> (0, 1) * amp[812] + amp[868] + amp[871] +
      amp[870] - amp[872] - amp[873] + amp[876] - amp[874] + amp[884] +
      amp[883] - amp[890] + amp[888] + amp[893] - amp[891] - amp[894] +
      amp[896] - Complex<double> (0, 1) * amp[930] - amp[931] + Complex<double>
      (0, 1) * amp[935] - Complex<double> (0, 1) * amp[938] - Complex<double>
      (0, 1) * amp[937] + Complex<double> (0, 1) * amp[940] - amp[963] -
      Complex<double> (0, 1) * amp[969] - amp[971] + Complex<double> (0, 1) *
      amp[981] - amp[982] - Complex<double> (0, 1) * amp[985] + Complex<double>
      (0, 1) * amp[987] + Complex<double> (0, 1) * amp[988] - amp[991] -
      Complex<double> (0, 1) * amp[992] - Complex<double> (0, 1) * amp[994] -
      Complex<double> (0, 1) * amp[998] + Complex<double> (0, 1) * amp[996] +
      amp[1010] + amp[1009] - Complex<double> (0, 1) * amp[1013] -
      Complex<double> (0, 1) * amp[1012] + amp[1318] - amp[1322] + amp[1320] -
      amp[1329] - amp[1330] + amp[1337] + amp[1338] - amp[1341] + amp[1339] +
      amp[1342] - amp[1346] + amp[1344] - amp[1349] - amp[1348] + amp[1355] -
      amp[1353] - amp[1386] + amp[1385] + amp[1388] - amp[1383] + amp[1394] +
      amp[1393] - Complex<double> (0, 1) * amp[1470] + Complex<double> (0, 1) *
      amp[1475] + Complex<double> (0, 1) * amp[1474] + amp[1596] + amp[1597] +
      amp[1600] + amp[1599] + amp[1604] + amp[1603] + amp[1609] + amp[1610] +
      amp[1637] + amp[1636] - Complex<double> (0, 1) * amp[1640] -
      Complex<double> (0, 1) * amp[1639] - Complex<double> (0, 1) * amp[1642] -
      Complex<double> (0, 1) * amp[1643] + Complex<double> (0, 1) * amp[1704] +
      Complex<double> (0, 1) * amp[1705] - amp[1707] - amp[1708] - amp[1730] +
      amp[1731] + amp[1728] - amp[1733] + amp[1737] + amp[1738] - amp[1740] -
      amp[1741] - Complex<double> (0, 1) * amp[1749] - Complex<double> (0, 1) *
      amp[1750];
  jamp[29] = +amp[10] - amp[14] + amp[12] + amp[21] + amp[22] + amp[26] -
      amp[24] - amp[27] + amp[29] + amp[38] - amp[36] - amp[39] + amp[41] +
      amp[44] - amp[42] - Complex<double> (0, 1) * amp[99] - Complex<double>
      (0, 1) * amp[103] - amp[104] - Complex<double> (0, 1) * amp[105] -
      Complex<double> (0, 1) * amp[106] + amp[108] - Complex<double> (0, 1) *
      amp[110] + amp[135] + Complex<double> (0, 1) * amp[136] - Complex<double>
      (0, 1) * amp[140] + Complex<double> (0, 1) * amp[148] - Complex<double>
      (0, 1) * amp[155] + Complex<double> (0, 1) * amp[153] + Complex<double>
      (0, 1) * amp[799] + Complex<double> (0, 1) * amp[801] + Complex<double>
      (0, 1) * amp[802] + Complex<double> (0, 1) * amp[804] + amp[806] -
      amp[810] + Complex<double> (0, 1) * amp[812] + amp[867] - amp[871] +
      amp[869] - amp[877] - amp[878] + amp[881] - amp[879] + amp[887] +
      amp[886] - amp[888] - amp[889] - amp[893] + amp[891] + amp[894] -
      amp[896] - Complex<double> (0, 1) * amp[921] - amp[922] + Complex<double>
      (0, 1) * amp[926] - Complex<double> (0, 1) * amp[929] - Complex<double>
      (0, 1) * amp[928] - Complex<double> (0, 1) * amp[940] - amp[954] -
      Complex<double> (0, 1) * amp[960] - amp[962] - Complex<double> (0, 1) *
      amp[981] + amp[982] + Complex<double> (0, 1) * amp[985] - Complex<double>
      (0, 1) * amp[987] - Complex<double> (0, 1) * amp[988] + amp[1000] -
      Complex<double> (0, 1) * amp[1001] + Complex<double> (0, 1) * amp[1003] -
      Complex<double> (0, 1) * amp[1005] - Complex<double> (0, 1) * amp[1006] -
      amp[1010] + amp[1008] + Complex<double> (0, 1) * amp[1013] -
      Complex<double> (0, 1) * amp[1011] - amp[1318] + amp[1322] - amp[1320] +
      amp[1329] + amp[1330] + amp[1361] + amp[1362] - amp[1365] + amp[1363] -
      amp[1366] + amp[1370] - amp[1368] - amp[1373] - amp[1372] + amp[1377] +
      amp[1378] + amp[1386] + amp[1382] - amp[1388] - amp[1380] - amp[1394] +
      amp[1392] - Complex<double> (0, 1) * amp[1461] + Complex<double> (0, 1) *
      amp[1466] + Complex<double> (0, 1) * amp[1465] - amp[1596] + amp[1601] +
      amp[1598] - amp[1599] + amp[1607] + amp[1606] - amp[1609] - amp[1610] +
      amp[1631] + amp[1630] - Complex<double> (0, 1) * amp[1634] -
      Complex<double> (0, 1) * amp[1633] + Complex<double> (0, 1) * amp[1642] +
      Complex<double> (0, 1) * amp[1643] + Complex<double> (0, 1) * amp[1758] +
      Complex<double> (0, 1) * amp[1759] - amp[1761] - amp[1762] - amp[1784] +
      amp[1785] + amp[1782] - amp[1787] + amp[1791] + amp[1792] - amp[1794] -
      amp[1795] - Complex<double> (0, 1) * amp[1803] - Complex<double> (0, 1) *
      amp[1804];
  jamp[30] = -Complex<double> (0, 1) * amp[479] - Complex<double> (0, 1) *
      amp[485] + Complex<double> (0, 1) * amp[483] - Complex<double> (0, 1) *
      amp[489] - amp[491] - amp[492] + Complex<double> (0, 1) * amp[494] -
      amp[550] - amp[553] - amp[552] + amp[554] + amp[555] - amp[558] +
      amp[556] - amp[566] - amp[565] + amp[572] - amp[570] - amp[575] +
      amp[573] + amp[576] - amp[578] + Complex<double> (0, 1) * amp[612] +
      amp[613] - Complex<double> (0, 1) * amp[617] + Complex<double> (0, 1) *
      amp[620] + Complex<double> (0, 1) * amp[619] - Complex<double> (0, 1) *
      amp[622] - Complex<double> (0, 1) * amp[638] - Complex<double> (0, 1) *
      amp[640] + amp[641] - Complex<double> (0, 1) * amp[644] - Complex<double>
      (0, 1) * amp[643] + Complex<double> (0, 1) * amp[649] - amp[650] -
      Complex<double> (0, 1) * amp[676] + amp[680] - amp[678] - amp[681] +
      amp[683] + Complex<double> (0, 1) * amp[687] + amp[689] + amp[713] +
      amp[714] - amp[717] + amp[715] + amp[718] + amp[721] + amp[720] -
      amp[723] - amp[724] + amp[731] + amp[730] - Complex<double> (0, 1) *
      amp[776] + Complex<double> (0, 1) * amp[777] + Complex<double> (0, 1) *
      amp[778] - amp[976] - Complex<double> (0, 1) * amp[1001] + amp[1002] +
      Complex<double> (0, 1) * amp[1004] - Complex<double> (0, 1) * amp[1007] -
      Complex<double> (0, 1) * amp[1006] + amp[1075] + amp[1078] + amp[1077] +
      amp[1088] + amp[1087] + amp[1100] - amp[1098] - amp[1101] + amp[1103] -
      Complex<double> (0, 1) * amp[1137] - amp[1138] + Complex<double> (0, 1) *
      amp[1141] - Complex<double> (0, 1) * amp[1145] - Complex<double> (0, 1) *
      amp[1144] + Complex<double> (0, 1) * amp[1147] - Complex<double> (0, 1) *
      amp[1148] + amp[1150] + amp[1361] + amp[1364] + amp[1363] + amp[1379] +
      amp[1378] - amp[1488] - amp[1486] - amp[1489] - amp[1485] - amp[1494] -
      amp[1495] - amp[1500] - amp[1501] - amp[1527] - amp[1528] +
      Complex<double> (0, 1) * amp[1530] + Complex<double> (0, 1) * amp[1531] +
      Complex<double> (0, 1) * amp[1533] + Complex<double> (0, 1) * amp[1534] -
      Complex<double> (0, 1) * amp[1652] - Complex<double> (0, 1) * amp[1651] +
      amp[1655] + amp[1654] - amp[1677] + amp[1682] + amp[1679] - amp[1680] -
      amp[1685] - amp[1684] + amp[1688] + amp[1687] + Complex<double> (0, 1) *
      amp[1697] + Complex<double> (0, 1) * amp[1696] + amp[1755] - amp[1757] -
      Complex<double> (0, 1) * amp[1760] + Complex<double> (0, 1) * amp[1758] -
      Complex<double> (0, 1) * amp[1776] + Complex<double> (0, 1) * amp[1778] +
      amp[1779] - amp[1781] - amp[1784] + amp[1790] - amp[1783] + amp[1789] +
      amp[1796] - amp[1794];
  jamp[31] = -Complex<double> (0, 1) * amp[481] - Complex<double> (0, 1) *
      amp[483] - Complex<double> (0, 1) * amp[484] - Complex<double> (0, 1) *
      amp[486] - amp[488] + amp[492] - Complex<double> (0, 1) * amp[494] -
      amp[549] + amp[553] - amp[551] + amp[559] + amp[560] - amp[563] +
      amp[561] - amp[569] - amp[568] + amp[570] + amp[571] + amp[575] -
      amp[573] - amp[576] + amp[578] + Complex<double> (0, 1) * amp[603] +
      amp[604] - Complex<double> (0, 1) * amp[608] + Complex<double> (0, 1) *
      amp[611] + Complex<double> (0, 1) * amp[610] + Complex<double> (0, 1) *
      amp[622] - Complex<double> (0, 1) * amp[797] - Complex<double> (0, 1) *
      amp[799] + amp[800] - Complex<double> (0, 1) * amp[803] - Complex<double>
      (0, 1) * amp[802] + Complex<double> (0, 1) * amp[808] - amp[809] -
      Complex<double> (0, 1) * amp[835] + amp[839] - amp[837] - amp[840] +
      amp[842] + Complex<double> (0, 1) * amp[846] + amp[848] + amp[872] +
      amp[873] - amp[876] + amp[874] + amp[877] + amp[880] + amp[879] -
      amp[882] - amp[883] + amp[890] + amp[889] - Complex<double> (0, 1) *
      amp[935] + Complex<double> (0, 1) * amp[936] + Complex<double> (0, 1) *
      amp[937] - amp[967] + Complex<double> (0, 1) * amp[1001] - amp[1002] -
      Complex<double> (0, 1) * amp[1004] + Complex<double> (0, 1) * amp[1007] +
      Complex<double> (0, 1) * amp[1006] + amp[1074] - amp[1078] + amp[1076] +
      amp[1091] + amp[1090] - amp[1100] + amp[1098] + amp[1101] - amp[1103] -
      Complex<double> (0, 1) * amp[1128] - amp[1129] + Complex<double> (0, 1) *
      amp[1132] - Complex<double> (0, 1) * amp[1136] - Complex<double> (0, 1) *
      amp[1135] - Complex<double> (0, 1) * amp[1147] + Complex<double> (0, 1) *
      amp[1148] - amp[1150] - amp[1361] - amp[1364] - amp[1363] - amp[1379] -
      amp[1378] - amp[1487] + amp[1488] + amp[1485] - amp[1490] - amp[1497] -
      amp[1498] + amp[1500] + amp[1501] - amp[1521] - amp[1522] +
      Complex<double> (0, 1) * amp[1524] + Complex<double> (0, 1) * amp[1525] -
      Complex<double> (0, 1) * amp[1533] - Complex<double> (0, 1) * amp[1534] -
      Complex<double> (0, 1) * amp[1706] - Complex<double> (0, 1) * amp[1705] +
      amp[1709] + amp[1708] - amp[1731] + amp[1736] + amp[1733] - amp[1734] -
      amp[1739] - amp[1738] + amp[1742] + amp[1741] + Complex<double> (0, 1) *
      amp[1751] + Complex<double> (0, 1) * amp[1750] - amp[1755] - amp[1756] -
      Complex<double> (0, 1) * amp[1758] - Complex<double> (0, 1) * amp[1759] +
      Complex<double> (0, 1) * amp[1776] + Complex<double> (0, 1) * amp[1777] -
      amp[1779] - amp[1780] + amp[1784] + amp[1783] + amp[1786] + amp[1787] +
      amp[1794] + amp[1795];
  jamp[32] = +Complex<double> (0, 1) * amp[638] + Complex<double> (0, 1) *
      amp[640] - amp[641] + Complex<double> (0, 1) * amp[644] + Complex<double>
      (0, 1) * amp[643] - Complex<double> (0, 1) * amp[649] + amp[650] +
      Complex<double> (0, 1) * amp[676] - amp[680] + amp[678] + amp[681] -
      amp[683] - Complex<double> (0, 1) * amp[687] - amp[689] - amp[713] -
      amp[714] + amp[717] - amp[715] - amp[718] - amp[721] - amp[720] +
      amp[723] + amp[724] - amp[731] - amp[730] + Complex<double> (0, 1) *
      amp[776] - Complex<double> (0, 1) * amp[777] - Complex<double> (0, 1) *
      amp[778] + Complex<double> (0, 1) * amp[795] + Complex<double> (0, 1) *
      amp[799] - amp[800] + Complex<double> (0, 1) * amp[801] + Complex<double>
      (0, 1) * amp[802] + amp[811] + Complex<double> (0, 1) * amp[812] +
      amp[833] - Complex<double> (0, 1) * amp[834] + Complex<double> (0, 1) *
      amp[835] - amp[839] - amp[838] - amp[841] - amp[842] - amp[877] -
      amp[880] - amp[879] - amp[888] - amp[889] - amp[893] - amp[892] -
      amp[895] - amp[896] - Complex<double> (0, 1) * amp[943] + Complex<double>
      (0, 1) * amp[950] - Complex<double> (0, 1) * amp[948] - amp[953] +
      amp[951] - amp[974] + Complex<double> (0, 1) * amp[979] - amp[980] -
      Complex<double> (0, 1) * amp[983] - Complex<double> (0, 1) * amp[989] -
      Complex<double> (0, 1) * amp[988] - amp[1084] - amp[1086] - amp[1087] +
      amp[1095] + amp[1096] + Complex<double> (0, 1) * amp[1139] + amp[1140] +
      Complex<double> (0, 1) * amp[1142] + Complex<double> (0, 1) * amp[1143] +
      Complex<double> (0, 1) * amp[1144] + amp[1313] + amp[1314] - amp[1317] +
      amp[1315] - amp[1319] - amp[1323] - amp[1324] + amp[1326] + amp[1327] +
      amp[1331] + amp[1330] + amp[1476] - Complex<double> (0, 1) * amp[1479] +
      Complex<double> (0, 1) * amp[1481] + Complex<double> (0, 1) * amp[1482] +
      Complex<double> (0, 1) * amp[1483] + Complex<double> (0, 1) * amp[1650] +
      Complex<double> (0, 1) * amp[1651] - amp[1653] - amp[1654] - amp[1676] +
      amp[1677] + amp[1674] - amp[1679] + amp[1683] + amp[1684] - amp[1686] -
      amp[1687] - Complex<double> (0, 1) * amp[1695] - Complex<double> (0, 1) *
      amp[1696] + amp[1756] + amp[1757] + Complex<double> (0, 1) * amp[1760] +
      Complex<double> (0, 1) * amp[1759] - Complex<double> (0, 1) * amp[1777] -
      Complex<double> (0, 1) * amp[1778] + amp[1780] + amp[1781] - amp[1790] -
      amp[1786] - amp[1789] - amp[1787] - amp[1796] - amp[1795] + amp[1811] -
      amp[1817] - amp[1809] + amp[1815] + amp[1820] - amp[1818] -
      Complex<double> (0, 1) * amp[1832] + Complex<double> (0, 1) * amp[1830] -
      amp[1835] + amp[1833];
  jamp[33] = -amp[322] + amp[324] + Complex<double> (0, 1) * amp[326] +
      amp[330] + Complex<double> (0, 1) * amp[332] - amp[335] + amp[333] -
      Complex<double> (0, 1) * amp[336] + Complex<double> (0, 1) * amp[340] -
      Complex<double> (0, 1) * amp[338] + Complex<double> (0, 1) * amp[343] +
      amp[344] - Complex<double> (0, 1) * amp[352] + amp[353] - Complex<double>
      (0, 1) * amp[419] - Complex<double> (0, 1) * amp[420] + Complex<double>
      (0, 1) * amp[423] - Complex<double> (0, 1) * amp[421] + amp[424] -
      Complex<double> (0, 1) * amp[438] + Complex<double> (0, 1) * amp[441] -
      Complex<double> (0, 1) * amp[439] - Complex<double> (0, 1) * amp[444] +
      Complex<double> (0, 1) * amp[446] - amp[449] + amp[447] - Complex<double>
      (0, 1) * amp[474] + Complex<double> (0, 1) * amp[476] - Complex<double>
      (0, 1) * amp[795] - Complex<double> (0, 1) * amp[799] + amp[800] -
      Complex<double> (0, 1) * amp[801] - Complex<double> (0, 1) * amp[802] -
      amp[811] - Complex<double> (0, 1) * amp[812] - amp[833] + Complex<double>
      (0, 1) * amp[834] - Complex<double> (0, 1) * amp[835] + amp[839] +
      amp[838] + amp[841] + amp[842] + amp[877] + amp[880] + amp[879] +
      amp[888] + amp[889] + amp[893] + amp[892] + amp[895] + amp[896] +
      Complex<double> (0, 1) * amp[943] - Complex<double> (0, 1) * amp[950] +
      Complex<double> (0, 1) * amp[948] + amp[953] - amp[951] - Complex<double>
      (0, 1) * amp[985] + Complex<double> (0, 1) * amp[987] + Complex<double>
      (0, 1) * amp[988] + Complex<double> (0, 1) * amp[1001] - amp[1002] -
      Complex<double> (0, 1) * amp[1003] + Complex<double> (0, 1) * amp[1005] +
      Complex<double> (0, 1) * amp[1006] - Complex<double> (0, 1) * amp[1013] +
      Complex<double> (0, 1) * amp[1011] - amp[1079] + amp[1083] - amp[1081] -
      amp[1097] - amp[1096] - amp[1100] - amp[1099] - amp[1102] - amp[1103] +
      Complex<double> (0, 1) * amp[1148] - amp[1308] - amp[1309] + amp[1312] -
      amp[1310] + amp[1318] + amp[1320] + amp[1321] - amp[1328] - amp[1327] -
      amp[1329] - amp[1330] - amp[1357] + amp[1360] - amp[1358] - amp[1361] -
      amp[1364] - amp[1363] + amp[1366] + amp[1368] + amp[1369] - amp[1377] -
      amp[1378] - amp[1382] - amp[1381] + amp[1388] + amp[1387] - amp[1389] +
      amp[1391] + amp[1394] - amp[1392] - amp[1755] - amp[1756] -
      Complex<double> (0, 1) * amp[1758] - Complex<double> (0, 1) * amp[1759] +
      Complex<double> (0, 1) * amp[1776] + Complex<double> (0, 1) * amp[1777] -
      amp[1779] - amp[1780] + amp[1784] + amp[1783] + amp[1786] + amp[1787] +
      amp[1794] + amp[1795] - amp[1811] + amp[1817] - amp[1810] + amp[1816] -
      amp[1821] + amp[1823];
  jamp[34] = +Complex<double> (0, 1) * amp[636] + Complex<double> (0, 1) *
      amp[640] - amp[641] + Complex<double> (0, 1) * amp[642] + Complex<double>
      (0, 1) * amp[643] + amp[652] + Complex<double> (0, 1) * amp[653] +
      amp[674] - Complex<double> (0, 1) * amp[675] + Complex<double> (0, 1) *
      amp[676] - amp[680] - amp[679] - amp[682] - amp[683] - amp[718] -
      amp[721] - amp[720] - amp[729] - amp[730] - amp[734] - amp[733] -
      amp[736] - amp[737] - Complex<double> (0, 1) * amp[784] + Complex<double>
      (0, 1) * amp[791] - Complex<double> (0, 1) * amp[789] - amp[794] +
      amp[792] + Complex<double> (0, 1) * amp[797] + Complex<double> (0, 1) *
      amp[799] - amp[800] + Complex<double> (0, 1) * amp[803] + Complex<double>
      (0, 1) * amp[802] - Complex<double> (0, 1) * amp[808] + amp[809] +
      Complex<double> (0, 1) * amp[835] - amp[839] + amp[837] + amp[840] -
      amp[842] - Complex<double> (0, 1) * amp[846] - amp[848] - amp[872] -
      amp[873] + amp[876] - amp[874] - amp[877] - amp[880] - amp[879] +
      amp[882] + amp[883] - amp[890] - amp[889] + Complex<double> (0, 1) *
      amp[935] - Complex<double> (0, 1) * amp[936] - Complex<double> (0, 1) *
      amp[937] - amp[965] + Complex<double> (0, 1) * amp[970] - amp[971] -
      Complex<double> (0, 1) * amp[992] - Complex<double> (0, 1) * amp[998] -
      Complex<double> (0, 1) * amp[997] - amp[1085] - amp[1089] - amp[1090] +
      amp[1092] + amp[1093] + Complex<double> (0, 1) * amp[1130] + amp[1131] +
      Complex<double> (0, 1) * amp[1133] + Complex<double> (0, 1) * amp[1134] +
      Complex<double> (0, 1) * amp[1135] + amp[1337] + amp[1338] - amp[1341] +
      amp[1339] - amp[1343] - amp[1347] - amp[1348] + amp[1350] + amp[1351] +
      amp[1355] + amp[1354] + amp[1467] - Complex<double> (0, 1) * amp[1470] +
      Complex<double> (0, 1) * amp[1472] + Complex<double> (0, 1) * amp[1473] +
      Complex<double> (0, 1) * amp[1474] + Complex<double> (0, 1) * amp[1704] +
      Complex<double> (0, 1) * amp[1705] - amp[1707] - amp[1708] - amp[1730] +
      amp[1731] + amp[1728] - amp[1733] + amp[1737] + amp[1738] - amp[1740] -
      amp[1741] - Complex<double> (0, 1) * amp[1749] - Complex<double> (0, 1) *
      amp[1750] + amp[1756] + amp[1757] + Complex<double> (0, 1) * amp[1760] +
      Complex<double> (0, 1) * amp[1759] - Complex<double> (0, 1) * amp[1777] -
      Complex<double> (0, 1) * amp[1778] + amp[1780] + amp[1781] - amp[1790] -
      amp[1786] - amp[1789] - amp[1787] - amp[1796] - amp[1795] + amp[1838] -
      amp[1844] - amp[1836] + amp[1842] + amp[1847] - amp[1845] -
      Complex<double> (0, 1) * amp[1859] + Complex<double> (0, 1) * amp[1857] -
      amp[1862] + amp[1860];
  jamp[35] = -amp[320] + amp[327] + Complex<double> (0, 1) * amp[329] -
      amp[330] - Complex<double> (0, 1) * amp[332] - amp[333] - amp[334] -
      Complex<double> (0, 1) * amp[337] - Complex<double> (0, 1) * amp[340] -
      Complex<double> (0, 1) * amp[339] + Complex<double> (0, 1) * amp[341] +
      amp[342] + Complex<double> (0, 1) * amp[352] - amp[353] - Complex<double>
      (0, 1) * amp[428] - Complex<double> (0, 1) * amp[429] + Complex<double>
      (0, 1) * amp[432] - Complex<double> (0, 1) * amp[430] + amp[433] +
      Complex<double> (0, 1) * amp[438] - Complex<double> (0, 1) * amp[441] +
      Complex<double> (0, 1) * amp[439] - Complex<double> (0, 1) * amp[453] +
      Complex<double> (0, 1) * amp[455] - amp[458] + amp[456] + Complex<double>
      (0, 1) * amp[474] + Complex<double> (0, 1) * amp[475] - Complex<double>
      (0, 1) * amp[636] - Complex<double> (0, 1) * amp[640] + amp[641] -
      Complex<double> (0, 1) * amp[642] - Complex<double> (0, 1) * amp[643] -
      amp[652] - Complex<double> (0, 1) * amp[653] - amp[674] + Complex<double>
      (0, 1) * amp[675] - Complex<double> (0, 1) * amp[676] + amp[680] +
      amp[679] + amp[682] + amp[683] + amp[718] + amp[721] + amp[720] +
      amp[729] + amp[730] + amp[734] + amp[733] + amp[736] + amp[737] +
      Complex<double> (0, 1) * amp[784] - Complex<double> (0, 1) * amp[791] +
      Complex<double> (0, 1) * amp[789] + amp[794] - amp[792] - Complex<double>
      (0, 1) * amp[994] + Complex<double> (0, 1) * amp[996] + Complex<double>
      (0, 1) * amp[997] - Complex<double> (0, 1) * amp[1001] + amp[1002] +
      Complex<double> (0, 1) * amp[1003] - Complex<double> (0, 1) * amp[1005] -
      Complex<double> (0, 1) * amp[1006] - Complex<double> (0, 1) * amp[1011] -
      Complex<double> (0, 1) * amp[1012] - amp[1080] - amp[1083] - amp[1082] -
      amp[1094] - amp[1093] + amp[1100] + amp[1099] + amp[1102] + amp[1103] -
      Complex<double> (0, 1) * amp[1148] - amp[1332] - amp[1333] + amp[1336] -
      amp[1334] + amp[1342] + amp[1344] + amp[1345] - amp[1352] - amp[1351] -
      amp[1353] - amp[1354] + amp[1357] - amp[1360] + amp[1358] + amp[1361] +
      amp[1364] + amp[1363] - amp[1366] - amp[1368] - amp[1369] + amp[1377] +
      amp[1378] + amp[1382] + amp[1384] + amp[1385] + amp[1381] + amp[1389] +
      amp[1390] + amp[1392] + amp[1393] + amp[1755] - amp[1757] -
      Complex<double> (0, 1) * amp[1760] + Complex<double> (0, 1) * amp[1758] -
      Complex<double> (0, 1) * amp[1776] + Complex<double> (0, 1) * amp[1778] +
      amp[1779] - amp[1781] - amp[1784] + amp[1790] - amp[1783] + amp[1789] +
      amp[1796] - amp[1794] - amp[1838] + amp[1844] - amp[1837] + amp[1843] -
      amp[1848] + amp[1850];
  jamp[36] = -Complex<double> (0, 1) * amp[479] - Complex<double> (0, 1) *
      amp[481] + amp[482] - Complex<double> (0, 1) * amp[485] - Complex<double>
      (0, 1) * amp[484] + Complex<double> (0, 1) * amp[490] - amp[491] -
      Complex<double> (0, 1) * amp[517] + amp[521] - amp[519] - amp[522] +
      amp[524] + Complex<double> (0, 1) * amp[528] + amp[530] + amp[554] +
      amp[555] - amp[558] + amp[556] + amp[559] + amp[562] + amp[561] -
      amp[564] - amp[565] + amp[572] + amp[571] - Complex<double> (0, 1) *
      amp[617] + Complex<double> (0, 1) * amp[618] + Complex<double> (0, 1) *
      amp[619] - Complex<double> (0, 1) * amp[638] - Complex<double> (0, 1) *
      amp[644] + Complex<double> (0, 1) * amp[642] - Complex<double> (0, 1) *
      amp[648] - amp[650] - amp[651] + Complex<double> (0, 1) * amp[653] -
      amp[709] - amp[712] - amp[711] + amp[713] + amp[714] - amp[717] +
      amp[715] - amp[725] - amp[724] + amp[731] - amp[729] - amp[734] +
      amp[732] + amp[735] - amp[737] + Complex<double> (0, 1) * amp[771] +
      amp[772] - Complex<double> (0, 1) * amp[776] + Complex<double> (0, 1) *
      amp[779] + Complex<double> (0, 1) * amp[778] - Complex<double> (0, 1) *
      amp[781] - amp[977] - Complex<double> (0, 1) * amp[992] + amp[993] +
      Complex<double> (0, 1) * amp[995] - Complex<double> (0, 1) * amp[998] -
      Complex<double> (0, 1) * amp[997] + amp[1153] + amp[1156] + amp[1155] +
      amp[1166] + amp[1165] + amp[1178] - amp[1176] - amp[1179] + amp[1181] -
      Complex<double> (0, 1) * amp[1215] - amp[1216] + Complex<double> (0, 1) *
      amp[1219] - Complex<double> (0, 1) * amp[1223] - Complex<double> (0, 1) *
      amp[1222] + Complex<double> (0, 1) * amp[1225] - Complex<double> (0, 1) *
      amp[1226] + amp[1228] + amp[1337] + amp[1340] + amp[1339] + amp[1355] +
      amp[1354] - amp[1542] - amp[1540] - amp[1543] - amp[1539] - amp[1548] -
      amp[1549] - amp[1554] - amp[1555] - amp[1581] - amp[1582] +
      Complex<double> (0, 1) * amp[1584] + Complex<double> (0, 1) * amp[1585] +
      Complex<double> (0, 1) * amp[1587] + Complex<double> (0, 1) * amp[1588] -
      Complex<double> (0, 1) * amp[1652] - Complex<double> (0, 1) * amp[1651] +
      amp[1655] + amp[1654] - amp[1677] + amp[1682] + amp[1679] - amp[1680] -
      amp[1685] - amp[1684] + amp[1688] + amp[1687] + Complex<double> (0, 1) *
      amp[1697] + Complex<double> (0, 1) * amp[1696] + amp[1701] - amp[1703] -
      Complex<double> (0, 1) * amp[1706] + Complex<double> (0, 1) * amp[1704] -
      Complex<double> (0, 1) * amp[1722] + Complex<double> (0, 1) * amp[1724] +
      amp[1725] - amp[1727] - amp[1730] + amp[1736] - amp[1729] + amp[1735] +
      amp[1742] - amp[1740];
  jamp[37] = -Complex<double> (0, 1) * amp[640] - Complex<double> (0, 1) *
      amp[642] - Complex<double> (0, 1) * amp[643] - Complex<double> (0, 1) *
      amp[645] - amp[647] + amp[651] - Complex<double> (0, 1) * amp[653] -
      amp[708] + amp[712] - amp[710] + amp[718] + amp[719] - amp[722] +
      amp[720] - amp[728] - amp[727] + amp[729] + amp[730] + amp[734] -
      amp[732] - amp[735] + amp[737] + Complex<double> (0, 1) * amp[762] +
      amp[763] - Complex<double> (0, 1) * amp[767] + Complex<double> (0, 1) *
      amp[770] + Complex<double> (0, 1) * amp[769] + Complex<double> (0, 1) *
      amp[781] - Complex<double> (0, 1) * amp[797] + amp[798] - Complex<double>
      (0, 1) * amp[799] - Complex<double> (0, 1) * amp[803] - Complex<double>
      (0, 1) * amp[802] + Complex<double> (0, 1) * amp[805] - amp[806] -
      Complex<double> (0, 1) * amp[853] + amp[857] - amp[855] - amp[858] +
      amp[860] + Complex<double> (0, 1) * amp[864] + amp[866] + amp[872] +
      amp[875] + amp[874] + amp[877] + amp[878] - amp[881] + amp[879] -
      amp[885] - amp[886] + amp[890] + amp[889] - Complex<double> (0, 1) *
      amp[926] + Complex<double> (0, 1) * amp[927] + Complex<double> (0, 1) *
      amp[928] - amp[958] + Complex<double> (0, 1) * amp[992] - amp[993] -
      Complex<double> (0, 1) * amp[995] + Complex<double> (0, 1) * amp[998] +
      Complex<double> (0, 1) * amp[997] + amp[1152] - amp[1156] + amp[1154] +
      amp[1169] + amp[1168] - amp[1178] + amp[1176] + amp[1179] - amp[1181] -
      Complex<double> (0, 1) * amp[1206] - amp[1207] + Complex<double> (0, 1) *
      amp[1210] - Complex<double> (0, 1) * amp[1214] - Complex<double> (0, 1) *
      amp[1213] - Complex<double> (0, 1) * amp[1225] + Complex<double> (0, 1) *
      amp[1226] - amp[1228] - amp[1337] - amp[1340] - amp[1339] - amp[1355] -
      amp[1354] - amp[1541] + amp[1542] + amp[1539] - amp[1544] - amp[1551] -
      amp[1552] + amp[1554] + amp[1555] - amp[1575] - amp[1576] +
      Complex<double> (0, 1) * amp[1578] + Complex<double> (0, 1) * amp[1579] -
      Complex<double> (0, 1) * amp[1587] - Complex<double> (0, 1) * amp[1588] -
      amp[1701] - amp[1702] - Complex<double> (0, 1) * amp[1704] -
      Complex<double> (0, 1) * amp[1705] + Complex<double> (0, 1) * amp[1722] +
      Complex<double> (0, 1) * amp[1723] - amp[1725] - amp[1726] + amp[1730] +
      amp[1729] + amp[1732] + amp[1733] + amp[1740] + amp[1741] -
      Complex<double> (0, 1) * amp[1760] - Complex<double> (0, 1) * amp[1759] +
      amp[1763] + amp[1762] - amp[1785] + amp[1790] + amp[1787] - amp[1788] -
      amp[1793] - amp[1792] + amp[1796] + amp[1795] + Complex<double> (0, 1) *
      amp[1805] + Complex<double> (0, 1) * amp[1804];
  jamp[38] = +Complex<double> (0, 1) * amp[479] + Complex<double> (0, 1) *
      amp[481] - amp[482] + Complex<double> (0, 1) * amp[485] + Complex<double>
      (0, 1) * amp[484] - Complex<double> (0, 1) * amp[490] + amp[491] +
      Complex<double> (0, 1) * amp[517] - amp[521] + amp[519] + amp[522] -
      amp[524] - Complex<double> (0, 1) * amp[528] - amp[530] - amp[554] -
      amp[555] + amp[558] - amp[556] - amp[559] - amp[562] - amp[561] +
      amp[564] + amp[565] - amp[572] - amp[571] + Complex<double> (0, 1) *
      amp[617] - Complex<double> (0, 1) * amp[618] - Complex<double> (0, 1) *
      amp[619] + Complex<double> (0, 1) * amp[796] + Complex<double> (0, 1) *
      amp[797] - amp[798] + Complex<double> (0, 1) * amp[803] - Complex<double>
      (0, 1) * amp[801] - amp[811] - Complex<double> (0, 1) * amp[812] +
      amp[851] - Complex<double> (0, 1) * amp[852] + Complex<double> (0, 1) *
      amp[853] - amp[857] - amp[856] - amp[859] - amp[860] - amp[872] -
      amp[875] - amp[874] - amp[890] + amp[888] + amp[893] + amp[892] +
      amp[895] + amp[896] + Complex<double> (0, 1) * amp[943] + Complex<double>
      (0, 1) * amp[948] + Complex<double> (0, 1) * amp[949] - amp[951] -
      amp[952] - amp[975] - Complex<double> (0, 1) * amp[979] + amp[980] +
      Complex<double> (0, 1) * amp[983] + Complex<double> (0, 1) * amp[989] +
      Complex<double> (0, 1) * amp[988] - amp[1162] - amp[1164] - amp[1165] +
      amp[1173] + amp[1174] + Complex<double> (0, 1) * amp[1217] + amp[1218] +
      Complex<double> (0, 1) * amp[1220] + Complex<double> (0, 1) * amp[1221] +
      Complex<double> (0, 1) * amp[1222] - amp[1313] - amp[1314] + amp[1317] -
      amp[1315] + amp[1319] + amp[1323] + amp[1324] - amp[1326] - amp[1327] -
      amp[1331] - amp[1330] - amp[1476] + Complex<double> (0, 1) * amp[1479] -
      Complex<double> (0, 1) * amp[1481] - Complex<double> (0, 1) * amp[1482] -
      Complex<double> (0, 1) * amp[1483] + Complex<double> (0, 1) * amp[1652] -
      Complex<double> (0, 1) * amp[1650] - amp[1655] + amp[1653] + amp[1676] -
      amp[1682] - amp[1674] + amp[1680] + amp[1685] - amp[1683] - amp[1688] +
      amp[1686] - Complex<double> (0, 1) * amp[1697] + Complex<double> (0, 1) *
      amp[1695] + amp[1702] + amp[1703] + Complex<double> (0, 1) * amp[1706] +
      Complex<double> (0, 1) * amp[1705] - Complex<double> (0, 1) * amp[1723] -
      Complex<double> (0, 1) * amp[1724] + amp[1726] + amp[1727] - amp[1736] -
      amp[1732] - amp[1735] - amp[1733] - amp[1742] - amp[1741] - amp[1811] +
      amp[1812] + amp[1809] - amp[1814] + amp[1818] + amp[1819] -
      Complex<double> (0, 1) * amp[1830] - Complex<double> (0, 1) * amp[1831] -
      amp[1833] - amp[1834];
  jamp[39] = -amp[323] - amp[324] - Complex<double> (0, 1) * amp[326] -
      amp[327] + Complex<double> (0, 1) * amp[328] + amp[335] + amp[334] -
      Complex<double> (0, 1) * amp[354] + Complex<double> (0, 1) * amp[358] -
      Complex<double> (0, 1) * amp[356] + Complex<double> (0, 1) * amp[361] +
      amp[362] - Complex<double> (0, 1) * amp[370] + amp[371] + Complex<double>
      (0, 1) * amp[419] + Complex<double> (0, 1) * amp[420] - Complex<double>
      (0, 1) * amp[423] + Complex<double> (0, 1) * amp[421] - amp[424] +
      Complex<double> (0, 1) * amp[429] - Complex<double> (0, 1) * amp[432] -
      Complex<double> (0, 1) * amp[431] + Complex<double> (0, 1) * amp[444] +
      Complex<double> (0, 1) * amp[445] - amp[447] - amp[448] - Complex<double>
      (0, 1) * amp[475] - Complex<double> (0, 1) * amp[476] - Complex<double>
      (0, 1) * amp[796] - Complex<double> (0, 1) * amp[797] + amp[798] -
      Complex<double> (0, 1) * amp[803] + Complex<double> (0, 1) * amp[801] +
      amp[811] + Complex<double> (0, 1) * amp[812] - amp[851] + Complex<double>
      (0, 1) * amp[852] - Complex<double> (0, 1) * amp[853] + amp[857] +
      amp[856] + amp[859] + amp[860] + amp[872] + amp[875] + amp[874] +
      amp[890] - amp[888] - amp[893] - amp[892] - amp[895] - amp[896] -
      Complex<double> (0, 1) * amp[943] - Complex<double> (0, 1) * amp[948] -
      Complex<double> (0, 1) * amp[949] + amp[951] + amp[952] + Complex<double>
      (0, 1) * amp[985] - Complex<double> (0, 1) * amp[987] - Complex<double>
      (0, 1) * amp[988] + Complex<double> (0, 1) * amp[992] - amp[993] +
      Complex<double> (0, 1) * amp[994] + Complex<double> (0, 1) * amp[998] -
      Complex<double> (0, 1) * amp[996] + Complex<double> (0, 1) * amp[1013] +
      Complex<double> (0, 1) * amp[1012] - amp[1157] + amp[1161] - amp[1159] -
      amp[1175] - amp[1174] - amp[1178] - amp[1177] - amp[1180] - amp[1181] +
      Complex<double> (0, 1) * amp[1226] + amp[1308] + amp[1309] - amp[1312] +
      amp[1310] - amp[1318] - amp[1320] - amp[1321] + amp[1328] + amp[1327] +
      amp[1329] + amp[1330] + amp[1333] - amp[1336] - amp[1335] - amp[1337] -
      amp[1340] - amp[1339] - amp[1342] - amp[1344] - amp[1345] - amp[1355] +
      amp[1353] - amp[1384] - amp[1385] - amp[1388] - amp[1387] - amp[1390] -
      amp[1391] - amp[1394] - amp[1393] - amp[1701] - amp[1702] -
      Complex<double> (0, 1) * amp[1704] - Complex<double> (0, 1) * amp[1705] +
      Complex<double> (0, 1) * amp[1722] + Complex<double> (0, 1) * amp[1723] -
      amp[1725] - amp[1726] + amp[1730] + amp[1729] + amp[1732] + amp[1733] +
      amp[1740] + amp[1741] + amp[1811] + amp[1810] + amp[1813] + amp[1814] +
      amp[1821] + amp[1822];
  jamp[40] = +Complex<double> (0, 1) * amp[477] + Complex<double> (0, 1) *
      amp[481] - amp[482] + Complex<double> (0, 1) * amp[483] + Complex<double>
      (0, 1) * amp[484] + amp[493] + Complex<double> (0, 1) * amp[494] +
      amp[515] - Complex<double> (0, 1) * amp[516] + Complex<double> (0, 1) *
      amp[517] - amp[521] - amp[520] - amp[523] - amp[524] - amp[559] -
      amp[562] - amp[561] - amp[570] - amp[571] - amp[575] - amp[574] -
      amp[577] - amp[578] - Complex<double> (0, 1) * amp[625] + Complex<double>
      (0, 1) * amp[632] - Complex<double> (0, 1) * amp[630] - amp[635] +
      amp[633] + Complex<double> (0, 1) * amp[797] - amp[798] + Complex<double>
      (0, 1) * amp[799] + Complex<double> (0, 1) * amp[803] + Complex<double>
      (0, 1) * amp[802] - Complex<double> (0, 1) * amp[805] + amp[806] +
      Complex<double> (0, 1) * amp[853] - amp[857] + amp[855] + amp[858] -
      amp[860] - Complex<double> (0, 1) * amp[864] - amp[866] - amp[872] -
      amp[875] - amp[874] - amp[877] - amp[878] + amp[881] - amp[879] +
      amp[885] + amp[886] - amp[890] - amp[889] + Complex<double> (0, 1) *
      amp[926] - Complex<double> (0, 1) * amp[927] - Complex<double> (0, 1) *
      amp[928] - amp[956] + Complex<double> (0, 1) * amp[961] - amp[962] -
      Complex<double> (0, 1) * amp[1001] - Complex<double> (0, 1) * amp[1007] -
      Complex<double> (0, 1) * amp[1006] - amp[1163] - amp[1167] - amp[1168] +
      amp[1170] + amp[1171] + Complex<double> (0, 1) * amp[1208] + amp[1209] +
      Complex<double> (0, 1) * amp[1211] + Complex<double> (0, 1) * amp[1212] +
      Complex<double> (0, 1) * amp[1213] + amp[1361] + amp[1362] - amp[1365] +
      amp[1363] - amp[1367] - amp[1371] - amp[1372] + amp[1374] + amp[1375] +
      amp[1379] + amp[1378] + amp[1458] - Complex<double> (0, 1) * amp[1461] +
      Complex<double> (0, 1) * amp[1463] + Complex<double> (0, 1) * amp[1464] +
      Complex<double> (0, 1) * amp[1465] + amp[1702] + amp[1703] +
      Complex<double> (0, 1) * amp[1706] + Complex<double> (0, 1) * amp[1705] -
      Complex<double> (0, 1) * amp[1723] - Complex<double> (0, 1) * amp[1724] +
      amp[1726] + amp[1727] - amp[1736] - amp[1732] - amp[1735] - amp[1733] -
      amp[1742] - amp[1741] + Complex<double> (0, 1) * amp[1758] +
      Complex<double> (0, 1) * amp[1759] - amp[1761] - amp[1762] - amp[1784] +
      amp[1785] + amp[1782] - amp[1787] + amp[1791] + amp[1792] - amp[1794] -
      amp[1795] - Complex<double> (0, 1) * amp[1803] - Complex<double> (0, 1) *
      amp[1804] + amp[1865] - amp[1871] - amp[1863] + amp[1869] + amp[1874] -
      amp[1872] - Complex<double> (0, 1) * amp[1886] + Complex<double> (0, 1) *
      amp[1884] - amp[1889] + amp[1887];
  jamp[41] = -amp[318] + amp[327] - Complex<double> (0, 1) * amp[328] -
      amp[330] + Complex<double> (0, 1) * amp[331] - amp[333] - amp[334] -
      Complex<double> (0, 1) * amp[355] - Complex<double> (0, 1) * amp[358] -
      Complex<double> (0, 1) * amp[357] + Complex<double> (0, 1) * amp[359] +
      amp[360] + Complex<double> (0, 1) * amp[370] - amp[371] - Complex<double>
      (0, 1) * amp[429] + Complex<double> (0, 1) * amp[432] + Complex<double>
      (0, 1) * amp[431] - Complex<double> (0, 1) * amp[437] + Complex<double>
      (0, 1) * amp[438] - Complex<double> (0, 1) * amp[441] - Complex<double>
      (0, 1) * amp[440] + amp[442] - Complex<double> (0, 1) * amp[462] +
      Complex<double> (0, 1) * amp[464] - amp[467] + amp[465] + Complex<double>
      (0, 1) * amp[474] + Complex<double> (0, 1) * amp[475] - Complex<double>
      (0, 1) * amp[477] - Complex<double> (0, 1) * amp[481] + amp[482] -
      Complex<double> (0, 1) * amp[483] - Complex<double> (0, 1) * amp[484] -
      amp[493] - Complex<double> (0, 1) * amp[494] - amp[515] + Complex<double>
      (0, 1) * amp[516] - Complex<double> (0, 1) * amp[517] + amp[521] +
      amp[520] + amp[523] + amp[524] + amp[559] + amp[562] + amp[561] +
      amp[570] + amp[571] + amp[575] + amp[574] + amp[577] + amp[578] +
      Complex<double> (0, 1) * amp[625] - Complex<double> (0, 1) * amp[632] +
      Complex<double> (0, 1) * amp[630] + amp[635] - amp[633] - Complex<double>
      (0, 1) * amp[992] + amp[993] - Complex<double> (0, 1) * amp[994] -
      Complex<double> (0, 1) * amp[998] + Complex<double> (0, 1) * amp[996] +
      Complex<double> (0, 1) * amp[1003] + Complex<double> (0, 1) * amp[1007] -
      Complex<double> (0, 1) * amp[1005] - Complex<double> (0, 1) * amp[1011] -
      Complex<double> (0, 1) * amp[1012] - amp[1158] - amp[1161] - amp[1160] -
      amp[1172] - amp[1171] + amp[1178] + amp[1177] + amp[1180] + amp[1181] -
      Complex<double> (0, 1) * amp[1226] - amp[1333] + amp[1336] + amp[1335] +
      amp[1337] + amp[1340] + amp[1339] + amp[1342] + amp[1344] + amp[1345] +
      amp[1355] - amp[1353] - amp[1356] + amp[1357] - amp[1360] - amp[1359] -
      amp[1366] - amp[1368] - amp[1369] - amp[1376] - amp[1375] - amp[1379] +
      amp[1377] + amp[1382] + amp[1384] + amp[1385] + amp[1381] + amp[1389] +
      amp[1390] + amp[1392] + amp[1393] + amp[1701] - amp[1703] -
      Complex<double> (0, 1) * amp[1706] + Complex<double> (0, 1) * amp[1704] -
      Complex<double> (0, 1) * amp[1722] + Complex<double> (0, 1) * amp[1724] +
      amp[1725] - amp[1727] - amp[1730] + amp[1736] - amp[1729] + amp[1735] +
      amp[1742] - amp[1740] - amp[1865] + amp[1871] - amp[1864] + amp[1870] -
      amp[1875] + amp[1877];
  jamp[42] = -Complex<double> (0, 1) * amp[479] + amp[480] - Complex<double>
      (0, 1) * amp[481] - Complex<double> (0, 1) * amp[485] - Complex<double>
      (0, 1) * amp[484] + Complex<double> (0, 1) * amp[487] - amp[488] -
      Complex<double> (0, 1) * amp[535] + amp[539] - amp[537] - amp[540] +
      amp[542] + Complex<double> (0, 1) * amp[546] + amp[548] + amp[554] +
      amp[557] + amp[556] + amp[559] + amp[560] - amp[563] + amp[561] -
      amp[567] - amp[568] + amp[572] + amp[571] - Complex<double> (0, 1) *
      amp[608] + Complex<double> (0, 1) * amp[609] + Complex<double> (0, 1) *
      amp[610] - Complex<double> (0, 1) * amp[797] - Complex<double> (0, 1) *
      amp[803] + Complex<double> (0, 1) * amp[801] - Complex<double> (0, 1) *
      amp[807] - amp[809] - amp[810] + Complex<double> (0, 1) * amp[812] -
      amp[868] - amp[871] - amp[870] + amp[872] + amp[873] - amp[876] +
      amp[874] - amp[884] - amp[883] + amp[890] - amp[888] - amp[893] +
      amp[891] + amp[894] - amp[896] + Complex<double> (0, 1) * amp[930] +
      amp[931] - Complex<double> (0, 1) * amp[935] + Complex<double> (0, 1) *
      amp[938] + Complex<double> (0, 1) * amp[937] - Complex<double> (0, 1) *
      amp[940] - amp[968] - Complex<double> (0, 1) * amp[983] + amp[984] +
      Complex<double> (0, 1) * amp[986] - Complex<double> (0, 1) * amp[989] -
      Complex<double> (0, 1) * amp[988] + amp[1231] + amp[1234] + amp[1233] +
      amp[1244] + amp[1243] + amp[1256] - amp[1254] - amp[1257] + amp[1259] -
      Complex<double> (0, 1) * amp[1293] - amp[1294] + Complex<double> (0, 1) *
      amp[1297] - Complex<double> (0, 1) * amp[1301] - Complex<double> (0, 1) *
      amp[1300] + Complex<double> (0, 1) * amp[1303] - Complex<double> (0, 1) *
      amp[1304] + amp[1306] + amp[1313] + amp[1316] + amp[1315] + amp[1331] +
      amp[1330] - amp[1596] - amp[1594] - amp[1597] - amp[1593] - amp[1602] -
      amp[1603] - amp[1608] - amp[1609] - amp[1635] - amp[1636] +
      Complex<double> (0, 1) * amp[1638] + Complex<double> (0, 1) * amp[1639] +
      Complex<double> (0, 1) * amp[1641] + Complex<double> (0, 1) * amp[1642] +
      amp[1647] - amp[1649] - Complex<double> (0, 1) * amp[1652] +
      Complex<double> (0, 1) * amp[1650] - Complex<double> (0, 1) * amp[1668] +
      Complex<double> (0, 1) * amp[1670] + amp[1671] - amp[1673] - amp[1676] +
      amp[1682] - amp[1675] + amp[1681] + amp[1688] - amp[1686] -
      Complex<double> (0, 1) * amp[1706] - Complex<double> (0, 1) * amp[1705] +
      amp[1709] + amp[1708] - amp[1731] + amp[1736] + amp[1733] - amp[1734] -
      amp[1739] - amp[1738] + amp[1742] + amp[1741] + Complex<double> (0, 1) *
      amp[1751] + Complex<double> (0, 1) * amp[1750];
  jamp[43] = -Complex<double> (0, 1) * amp[638] + amp[639] - Complex<double>
      (0, 1) * amp[640] - Complex<double> (0, 1) * amp[644] - Complex<double>
      (0, 1) * amp[643] + Complex<double> (0, 1) * amp[646] - amp[647] -
      Complex<double> (0, 1) * amp[694] + amp[698] - amp[696] - amp[699] +
      amp[701] + Complex<double> (0, 1) * amp[705] + amp[707] + amp[713] +
      amp[716] + amp[715] + amp[718] + amp[719] - amp[722] + amp[720] -
      amp[726] - amp[727] + amp[731] + amp[730] - Complex<double> (0, 1) *
      amp[767] + Complex<double> (0, 1) * amp[768] + Complex<double> (0, 1) *
      amp[769] - Complex<double> (0, 1) * amp[799] - Complex<double> (0, 1) *
      amp[801] - Complex<double> (0, 1) * amp[802] - Complex<double> (0, 1) *
      amp[804] - amp[806] + amp[810] - Complex<double> (0, 1) * amp[812] -
      amp[867] + amp[871] - amp[869] + amp[877] + amp[878] - amp[881] +
      amp[879] - amp[887] - amp[886] + amp[888] + amp[889] + amp[893] -
      amp[891] - amp[894] + amp[896] + Complex<double> (0, 1) * amp[921] +
      amp[922] - Complex<double> (0, 1) * amp[926] + Complex<double> (0, 1) *
      amp[929] + Complex<double> (0, 1) * amp[928] + Complex<double> (0, 1) *
      amp[940] - amp[959] + Complex<double> (0, 1) * amp[983] - amp[984] -
      Complex<double> (0, 1) * amp[986] + Complex<double> (0, 1) * amp[989] +
      Complex<double> (0, 1) * amp[988] + amp[1230] - amp[1234] + amp[1232] +
      amp[1247] + amp[1246] - amp[1256] + amp[1254] + amp[1257] - amp[1259] -
      Complex<double> (0, 1) * amp[1284] - amp[1285] + Complex<double> (0, 1) *
      amp[1288] - Complex<double> (0, 1) * amp[1292] - Complex<double> (0, 1) *
      amp[1291] - Complex<double> (0, 1) * amp[1303] + Complex<double> (0, 1) *
      amp[1304] - amp[1306] - amp[1313] - amp[1316] - amp[1315] - amp[1331] -
      amp[1330] - amp[1595] + amp[1596] + amp[1593] - amp[1598] - amp[1605] -
      amp[1606] + amp[1608] + amp[1609] - amp[1629] - amp[1630] +
      Complex<double> (0, 1) * amp[1632] + Complex<double> (0, 1) * amp[1633] -
      Complex<double> (0, 1) * amp[1641] - Complex<double> (0, 1) * amp[1642] -
      amp[1647] - amp[1648] - Complex<double> (0, 1) * amp[1650] -
      Complex<double> (0, 1) * amp[1651] + Complex<double> (0, 1) * amp[1668] +
      Complex<double> (0, 1) * amp[1669] - amp[1671] - amp[1672] + amp[1676] +
      amp[1675] + amp[1678] + amp[1679] + amp[1686] + amp[1687] -
      Complex<double> (0, 1) * amp[1760] - Complex<double> (0, 1) * amp[1759] +
      amp[1763] + amp[1762] - amp[1785] + amp[1790] + amp[1787] - amp[1788] -
      amp[1793] - amp[1792] + amp[1796] + amp[1795] + Complex<double> (0, 1) *
      amp[1805] + Complex<double> (0, 1) * amp[1804];
  jamp[44] = +Complex<double> (0, 1) * amp[479] - amp[480] + Complex<double>
      (0, 1) * amp[481] + Complex<double> (0, 1) * amp[485] + Complex<double>
      (0, 1) * amp[484] - Complex<double> (0, 1) * amp[487] + amp[488] +
      Complex<double> (0, 1) * amp[535] - amp[539] + amp[537] + amp[540] -
      amp[542] - Complex<double> (0, 1) * amp[546] - amp[548] - amp[554] -
      amp[557] - amp[556] - amp[559] - amp[560] + amp[563] - amp[561] +
      amp[567] + amp[568] - amp[572] - amp[571] + Complex<double> (0, 1) *
      amp[608] - Complex<double> (0, 1) * amp[609] - Complex<double> (0, 1) *
      amp[610] + Complex<double> (0, 1) * amp[637] + Complex<double> (0, 1) *
      amp[638] - amp[639] + Complex<double> (0, 1) * amp[644] - Complex<double>
      (0, 1) * amp[642] - amp[652] - Complex<double> (0, 1) * amp[653] +
      amp[692] - Complex<double> (0, 1) * amp[693] + Complex<double> (0, 1) *
      amp[694] - amp[698] - amp[697] - amp[700] - amp[701] - amp[713] -
      amp[716] - amp[715] - amp[731] + amp[729] + amp[734] + amp[733] +
      amp[736] + amp[737] + Complex<double> (0, 1) * amp[784] + Complex<double>
      (0, 1) * amp[789] + Complex<double> (0, 1) * amp[790] - amp[792] -
      amp[793] - amp[966] - Complex<double> (0, 1) * amp[970] + amp[971] +
      Complex<double> (0, 1) * amp[992] + Complex<double> (0, 1) * amp[998] +
      Complex<double> (0, 1) * amp[997] - amp[1240] - amp[1242] - amp[1243] +
      amp[1251] + amp[1252] + Complex<double> (0, 1) * amp[1295] + amp[1296] +
      Complex<double> (0, 1) * amp[1298] + Complex<double> (0, 1) * amp[1299] +
      Complex<double> (0, 1) * amp[1300] - amp[1337] - amp[1338] + amp[1341] -
      amp[1339] + amp[1343] + amp[1347] + amp[1348] - amp[1350] - amp[1351] -
      amp[1355] - amp[1354] - amp[1467] + Complex<double> (0, 1) * amp[1470] -
      Complex<double> (0, 1) * amp[1472] - Complex<double> (0, 1) * amp[1473] -
      Complex<double> (0, 1) * amp[1474] + amp[1648] + amp[1649] +
      Complex<double> (0, 1) * amp[1652] + Complex<double> (0, 1) * amp[1651] -
      Complex<double> (0, 1) * amp[1669] - Complex<double> (0, 1) * amp[1670] +
      amp[1672] + amp[1673] - amp[1682] - amp[1678] - amp[1681] - amp[1679] -
      amp[1688] - amp[1687] + Complex<double> (0, 1) * amp[1706] -
      Complex<double> (0, 1) * amp[1704] - amp[1709] + amp[1707] + amp[1730] -
      amp[1736] - amp[1728] + amp[1734] + amp[1739] - amp[1737] - amp[1742] +
      amp[1740] - Complex<double> (0, 1) * amp[1751] + Complex<double> (0, 1) *
      amp[1749] - amp[1838] + amp[1839] + amp[1836] - amp[1841] + amp[1845] +
      amp[1846] - Complex<double> (0, 1) * amp[1857] - Complex<double> (0, 1) *
      amp[1858] - amp[1860] - amp[1861];
  jamp[45] = -amp[321] - amp[324] + Complex<double> (0, 1) * amp[325] -
      amp[327] - Complex<double> (0, 1) * amp[329] + amp[335] + amp[334] -
      Complex<double> (0, 1) * amp[372] + Complex<double> (0, 1) * amp[376] -
      Complex<double> (0, 1) * amp[374] + Complex<double> (0, 1) * amp[379] +
      amp[380] - Complex<double> (0, 1) * amp[388] + amp[389] + Complex<double>
      (0, 1) * amp[420] - Complex<double> (0, 1) * amp[423] - Complex<double>
      (0, 1) * amp[422] + Complex<double> (0, 1) * amp[428] + Complex<double>
      (0, 1) * amp[429] - Complex<double> (0, 1) * amp[432] + Complex<double>
      (0, 1) * amp[430] - amp[433] + Complex<double> (0, 1) * amp[453] +
      Complex<double> (0, 1) * amp[454] - amp[456] - amp[457] - Complex<double>
      (0, 1) * amp[475] - Complex<double> (0, 1) * amp[476] - Complex<double>
      (0, 1) * amp[637] - Complex<double> (0, 1) * amp[638] + amp[639] -
      Complex<double> (0, 1) * amp[644] + Complex<double> (0, 1) * amp[642] +
      amp[652] + Complex<double> (0, 1) * amp[653] - amp[692] + Complex<double>
      (0, 1) * amp[693] - Complex<double> (0, 1) * amp[694] + amp[698] +
      amp[697] + amp[700] + amp[701] + amp[713] + amp[716] + amp[715] +
      amp[731] - amp[729] - amp[734] - amp[733] - amp[736] - amp[737] -
      Complex<double> (0, 1) * amp[784] - Complex<double> (0, 1) * amp[789] -
      Complex<double> (0, 1) * amp[790] + amp[792] + amp[793] + Complex<double>
      (0, 1) * amp[983] - amp[984] + Complex<double> (0, 1) * amp[985] +
      Complex<double> (0, 1) * amp[989] - Complex<double> (0, 1) * amp[987] +
      Complex<double> (0, 1) * amp[994] - Complex<double> (0, 1) * amp[996] -
      Complex<double> (0, 1) * amp[997] + Complex<double> (0, 1) * amp[1013] +
      Complex<double> (0, 1) * amp[1012] - amp[1235] + amp[1239] - amp[1237] -
      amp[1253] - amp[1252] - amp[1256] - amp[1255] - amp[1258] - amp[1259] +
      Complex<double> (0, 1) * amp[1304] + amp[1309] - amp[1312] - amp[1311] -
      amp[1313] - amp[1316] - amp[1315] - amp[1318] - amp[1320] - amp[1321] -
      amp[1331] + amp[1329] + amp[1332] + amp[1333] - amp[1336] + amp[1334] -
      amp[1342] - amp[1344] - amp[1345] + amp[1352] + amp[1351] + amp[1353] +
      amp[1354] - amp[1384] - amp[1385] - amp[1388] - amp[1387] - amp[1390] -
      amp[1391] - amp[1394] - amp[1393] - amp[1647] - amp[1648] -
      Complex<double> (0, 1) * amp[1650] - Complex<double> (0, 1) * amp[1651] +
      Complex<double> (0, 1) * amp[1668] + Complex<double> (0, 1) * amp[1669] -
      amp[1671] - amp[1672] + amp[1676] + amp[1675] + amp[1678] + amp[1679] +
      amp[1686] + amp[1687] + amp[1838] + amp[1837] + amp[1840] + amp[1841] +
      amp[1848] + amp[1849];
  jamp[46] = +Complex<double> (0, 1) * amp[478] + Complex<double> (0, 1) *
      amp[479] - amp[480] + Complex<double> (0, 1) * amp[485] - Complex<double>
      (0, 1) * amp[483] - amp[493] - Complex<double> (0, 1) * amp[494] +
      amp[533] - Complex<double> (0, 1) * amp[534] + Complex<double> (0, 1) *
      amp[535] - amp[539] - amp[538] - amp[541] - amp[542] - amp[554] -
      amp[557] - amp[556] - amp[572] + amp[570] + amp[575] + amp[574] +
      amp[577] + amp[578] + Complex<double> (0, 1) * amp[625] + Complex<double>
      (0, 1) * amp[630] + Complex<double> (0, 1) * amp[631] - amp[633] -
      amp[634] + Complex<double> (0, 1) * amp[638] - amp[639] + Complex<double>
      (0, 1) * amp[640] + Complex<double> (0, 1) * amp[644] + Complex<double>
      (0, 1) * amp[643] - Complex<double> (0, 1) * amp[646] + amp[647] +
      Complex<double> (0, 1) * amp[694] - amp[698] + amp[696] + amp[699] -
      amp[701] - Complex<double> (0, 1) * amp[705] - amp[707] - amp[713] -
      amp[716] - amp[715] - amp[718] - amp[719] + amp[722] - amp[720] +
      amp[726] + amp[727] - amp[731] - amp[730] + Complex<double> (0, 1) *
      amp[767] - Complex<double> (0, 1) * amp[768] - Complex<double> (0, 1) *
      amp[769] - amp[957] - Complex<double> (0, 1) * amp[961] + amp[962] +
      Complex<double> (0, 1) * amp[1001] + Complex<double> (0, 1) * amp[1007] +
      Complex<double> (0, 1) * amp[1006] - amp[1241] - amp[1245] - amp[1246] +
      amp[1248] + amp[1249] + Complex<double> (0, 1) * amp[1286] + amp[1287] +
      Complex<double> (0, 1) * amp[1289] + Complex<double> (0, 1) * amp[1290] +
      Complex<double> (0, 1) * amp[1291] - amp[1361] - amp[1362] + amp[1365] -
      amp[1363] + amp[1367] + amp[1371] + amp[1372] - amp[1374] - amp[1375] -
      amp[1379] - amp[1378] - amp[1458] + Complex<double> (0, 1) * amp[1461] -
      Complex<double> (0, 1) * amp[1463] - Complex<double> (0, 1) * amp[1464] -
      Complex<double> (0, 1) * amp[1465] + amp[1648] + amp[1649] +
      Complex<double> (0, 1) * amp[1652] + Complex<double> (0, 1) * amp[1651] -
      Complex<double> (0, 1) * amp[1669] - Complex<double> (0, 1) * amp[1670] +
      amp[1672] + amp[1673] - amp[1682] - amp[1678] - amp[1681] - amp[1679] -
      amp[1688] - amp[1687] + Complex<double> (0, 1) * amp[1760] -
      Complex<double> (0, 1) * amp[1758] - amp[1763] + amp[1761] + amp[1784] -
      amp[1790] - amp[1782] + amp[1788] + amp[1793] - amp[1791] - amp[1796] +
      amp[1794] - Complex<double> (0, 1) * amp[1805] + Complex<double> (0, 1) *
      amp[1803] - amp[1865] + amp[1866] + amp[1863] - amp[1868] + amp[1872] +
      amp[1873] - Complex<double> (0, 1) * amp[1884] - Complex<double> (0, 1) *
      amp[1885] - amp[1887] - amp[1888];
  jamp[47] = -amp[319] + amp[324] - Complex<double> (0, 1) * amp[325] +
      amp[330] - Complex<double> (0, 1) * amp[331] - amp[335] + amp[333] -
      Complex<double> (0, 1) * amp[373] - Complex<double> (0, 1) * amp[376] -
      Complex<double> (0, 1) * amp[375] + Complex<double> (0, 1) * amp[377] +
      amp[378] + Complex<double> (0, 1) * amp[388] - amp[389] - Complex<double>
      (0, 1) * amp[420] + Complex<double> (0, 1) * amp[423] + Complex<double>
      (0, 1) * amp[422] + Complex<double> (0, 1) * amp[437] - Complex<double>
      (0, 1) * amp[438] + Complex<double> (0, 1) * amp[441] + Complex<double>
      (0, 1) * amp[440] - amp[442] + Complex<double> (0, 1) * amp[462] +
      Complex<double> (0, 1) * amp[463] - amp[465] - amp[466] - Complex<double>
      (0, 1) * amp[474] + Complex<double> (0, 1) * amp[476] - Complex<double>
      (0, 1) * amp[478] - Complex<double> (0, 1) * amp[479] + amp[480] -
      Complex<double> (0, 1) * amp[485] + Complex<double> (0, 1) * amp[483] +
      amp[493] + Complex<double> (0, 1) * amp[494] - amp[533] + Complex<double>
      (0, 1) * amp[534] - Complex<double> (0, 1) * amp[535] + amp[539] +
      amp[538] + amp[541] + amp[542] + amp[554] + amp[557] + amp[556] +
      amp[572] - amp[570] - amp[575] - amp[574] - amp[577] - amp[578] -
      Complex<double> (0, 1) * amp[625] - Complex<double> (0, 1) * amp[630] -
      Complex<double> (0, 1) * amp[631] + amp[633] + amp[634] - Complex<double>
      (0, 1) * amp[983] + amp[984] - Complex<double> (0, 1) * amp[985] -
      Complex<double> (0, 1) * amp[989] + Complex<double> (0, 1) * amp[987] -
      Complex<double> (0, 1) * amp[1003] - Complex<double> (0, 1) * amp[1007] +
      Complex<double> (0, 1) * amp[1005] - Complex<double> (0, 1) * amp[1013] +
      Complex<double> (0, 1) * amp[1011] - amp[1236] - amp[1239] - amp[1238] -
      amp[1250] - amp[1249] + amp[1256] + amp[1255] + amp[1258] + amp[1259] -
      Complex<double> (0, 1) * amp[1304] - amp[1309] + amp[1312] + amp[1311] +
      amp[1313] + amp[1316] + amp[1315] + amp[1318] + amp[1320] + amp[1321] +
      amp[1331] - amp[1329] + amp[1356] - amp[1357] + amp[1360] + amp[1359] +
      amp[1366] + amp[1368] + amp[1369] + amp[1376] + amp[1375] + amp[1379] -
      amp[1377] - amp[1382] - amp[1381] + amp[1388] + amp[1387] - amp[1389] +
      amp[1391] + amp[1394] - amp[1392] + amp[1647] - amp[1649] -
      Complex<double> (0, 1) * amp[1652] + Complex<double> (0, 1) * amp[1650] -
      Complex<double> (0, 1) * amp[1668] + Complex<double> (0, 1) * amp[1670] +
      amp[1671] - amp[1673] - amp[1676] + amp[1682] - amp[1675] + amp[1681] +
      amp[1688] - amp[1686] + amp[1865] + amp[1864] + amp[1867] + amp[1868] +
      amp[1875] + amp[1876];
  jamp[48] = +amp[1] + amp[4] + amp[3] + amp[5] + amp[6] + amp[8] + amp[7] +
      amp[17] + amp[16] + amp[23] - amp[21] - amp[38] - amp[37] - amp[40] -
      amp[41] + Complex<double> (0, 1) * amp[47] + Complex<double> (0, 1) *
      amp[53] - Complex<double> (0, 1) * amp[51] + Complex<double> (0, 1) *
      amp[57] + amp[59] + amp[60] - Complex<double> (0, 1) * amp[62] -
      Complex<double> (0, 1) * amp[125] - amp[126] - Complex<double> (0, 1) *
      amp[127] - Complex<double> (0, 1) * amp[131] - Complex<double> (0, 1) *
      amp[130] + Complex<double> (0, 1) * amp[152] + Complex<double> (0, 1) *
      amp[514] - amp[520] - amp[519] - amp[522] - amp[523] + Complex<double>
      (0, 1) * amp[528] - amp[529] + amp[550] + amp[553] + amp[552] + amp[566]
      - amp[564] - amp[574] - amp[573] - amp[576] - amp[577] - Complex<double>
      (0, 1) * amp[612] - amp[613] - Complex<double> (0, 1) * amp[615] -
      Complex<double> (0, 1) * amp[620] + Complex<double> (0, 1) * amp[618] +
      Complex<double> (0, 1) * amp[622] + Complex<double> (0, 1) * amp[623] -
      amp[624] - Complex<double> (0, 1) * amp[629] + Complex<double> (0, 1) *
      amp[627] - amp[635] + amp[633] + amp[1152] + amp[1153] + amp[1155] +
      amp[1154] - amp[1163] + amp[1166] + amp[1165] + amp[1169] - amp[1167] +
      amp[1170] + amp[1171] + Complex<double> (0, 1) * amp[1182] + amp[1185] -
      Complex<double> (0, 1) * amp[1187] + Complex<double> (0, 1) * amp[1190] -
      Complex<double> (0, 1) * amp[1188] + amp[1191] + Complex<double> (0, 1) *
      amp[1192] - Complex<double> (0, 1) * amp[1215] - Complex<double> (0, 1) *
      amp[1223] - Complex<double> (0, 1) * amp[1222] - amp[1367] + amp[1373] -
      amp[1371] + amp[1374] + amp[1375] - amp[1402] + amp[1407] +
      Complex<double> (0, 1) * amp[1408] - Complex<double> (0, 1) * amp[1412] +
      Complex<double> (0, 1) * amp[1415] - Complex<double> (0, 1) * amp[1413] +
      amp[1488] + amp[1489] + amp[1492] + amp[1491] + amp[1496] + amp[1495] +
      amp[1501] + amp[1502] + amp[1529] + amp[1528] - Complex<double> (0, 1) *
      amp[1532] - Complex<double> (0, 1) * amp[1531] - Complex<double> (0, 1) *
      amp[1534] - Complex<double> (0, 1) * amp[1535] - amp[1541] + amp[1547] -
      amp[1540] + amp[1546] + amp[1550] - amp[1548] + amp[1553] - amp[1551] +
      Complex<double> (0, 1) * amp[1562] - Complex<double> (0, 1) * amp[1560] -
      amp[1565] + amp[1563] - Complex<double> (0, 1) * amp[1586] +
      Complex<double> (0, 1) * amp[1584] + amp[1865] - amp[1871] - amp[1863] +
      amp[1869] + amp[1874] - amp[1872] + Complex<double> (0, 1) * amp[1880] -
      Complex<double> (0, 1) * amp[1878] - amp[1883] + amp[1881];
  jamp[49] = +amp[0] - amp[4] + amp[2] + amp[10] + amp[11] + amp[13] + amp[12]
      + amp[20] + amp[19] + amp[21] + amp[22] + amp[38] + amp[37] + amp[40] +
      amp[41] + Complex<double> (0, 1) * amp[49] + Complex<double> (0, 1) *
      amp[51] + Complex<double> (0, 1) * amp[52] + Complex<double> (0, 1) *
      amp[54] + amp[56] - amp[60] + Complex<double> (0, 1) * amp[62] -
      Complex<double> (0, 1) * amp[113] - amp[114] - Complex<double> (0, 1) *
      amp[115] - Complex<double> (0, 1) * amp[119] - Complex<double> (0, 1) *
      amp[118] - Complex<double> (0, 1) * amp[152] + Complex<double> (0, 1) *
      amp[532] - amp[538] - amp[537] - amp[540] - amp[541] + Complex<double>
      (0, 1) * amp[546] - amp[547] + amp[549] - amp[553] + amp[551] + amp[569]
      - amp[567] + amp[574] + amp[573] + amp[576] + amp[577] - Complex<double>
      (0, 1) * amp[603] - amp[604] - Complex<double> (0, 1) * amp[606] -
      Complex<double> (0, 1) * amp[611] + Complex<double> (0, 1) * amp[609] -
      Complex<double> (0, 1) * amp[622] - Complex<double> (0, 1) * amp[623] +
      amp[624] - Complex<double> (0, 1) * amp[627] - Complex<double> (0, 1) *
      amp[628] - amp[633] - amp[634] + amp[1230] + amp[1231] + amp[1233] +
      amp[1232] - amp[1241] + amp[1244] + amp[1243] + amp[1247] - amp[1245] +
      amp[1248] + amp[1249] + Complex<double> (0, 1) * amp[1260] + amp[1263] -
      Complex<double> (0, 1) * amp[1265] + Complex<double> (0, 1) * amp[1268] -
      Complex<double> (0, 1) * amp[1266] + amp[1269] + Complex<double> (0, 1) *
      amp[1270] - Complex<double> (0, 1) * amp[1293] - Complex<double> (0, 1) *
      amp[1301] - Complex<double> (0, 1) * amp[1300] + amp[1367] - amp[1373] +
      amp[1371] - amp[1374] - amp[1375] - amp[1396] - amp[1407] -
      Complex<double> (0, 1) * amp[1408] + Complex<double> (0, 1) * amp[1412] -
      Complex<double> (0, 1) * amp[1415] + Complex<double> (0, 1) * amp[1413] -
      amp[1488] + amp[1493] + amp[1490] - amp[1491] + amp[1499] + amp[1498] -
      amp[1501] - amp[1502] + amp[1523] + amp[1522] - Complex<double> (0, 1) *
      amp[1526] - Complex<double> (0, 1) * amp[1525] + Complex<double> (0, 1) *
      amp[1534] + Complex<double> (0, 1) * amp[1535] - amp[1595] + amp[1601] -
      amp[1594] + amp[1600] + amp[1604] - amp[1602] + amp[1607] - amp[1605] +
      Complex<double> (0, 1) * amp[1616] - Complex<double> (0, 1) * amp[1614] -
      amp[1619] + amp[1617] - Complex<double> (0, 1) * amp[1640] +
      Complex<double> (0, 1) * amp[1638] - amp[1865] + amp[1866] + amp[1863] -
      amp[1868] + amp[1872] + amp[1873] + Complex<double> (0, 1) * amp[1878] +
      Complex<double> (0, 1) * amp[1879] - amp[1881] - amp[1882];
  jamp[50] = -Complex<double> (0, 1) * amp[514] + amp[520] + amp[519] +
      amp[522] + amp[523] - Complex<double> (0, 1) * amp[528] + amp[529] -
      amp[531] - Complex<double> (0, 1) * amp[532] + Complex<double> (0, 1) *
      amp[536] + amp[539] + amp[538] + amp[541] + amp[542] - amp[555] +
      amp[558] + amp[557] + amp[564] + amp[565] - Complex<double> (0, 1) *
      amp[614] - amp[616] + Complex<double> (0, 1) * amp[617] - Complex<double>
      (0, 1) * amp[618] - Complex<double> (0, 1) * amp[619] + Complex<double>
      (0, 1) * amp[629] + Complex<double> (0, 1) * amp[628] + amp[635] +
      amp[634] + Complex<double> (0, 1) * amp[695] + amp[698] - amp[696] -
      amp[699] + amp[701] - Complex<double> (0, 1) * amp[702] + amp[703] +
      amp[708] + amp[709] + amp[711] + amp[710] - amp[714] + amp[717] +
      amp[716] + amp[725] + amp[724] + amp[728] - amp[726] + Complex<double>
      (0, 1) * amp[738] + Complex<double> (0, 1) * amp[746] - Complex<double>
      (0, 1) * amp[744] + amp[747] + Complex<double> (0, 1) * amp[748] -
      Complex<double> (0, 1) * amp[771] - amp[775] + Complex<double> (0, 1) *
      amp[776] - Complex<double> (0, 1) * amp[779] - Complex<double> (0, 1) *
      amp[778] - amp[1152] - amp[1153] - amp[1155] - amp[1154] + amp[1163] -
      amp[1166] - amp[1165] - amp[1169] + amp[1167] - amp[1170] - amp[1171] -
      Complex<double> (0, 1) * amp[1182] - amp[1185] + Complex<double> (0, 1) *
      amp[1187] - Complex<double> (0, 1) * amp[1190] + Complex<double> (0, 1) *
      amp[1188] - amp[1191] - Complex<double> (0, 1) * amp[1192] +
      Complex<double> (0, 1) * amp[1215] + Complex<double> (0, 1) * amp[1223] +
      Complex<double> (0, 1) * amp[1222] + amp[1241] + amp[1245] + amp[1246] -
      amp[1248] - amp[1249] + Complex<double> (0, 1) * amp[1262] - amp[1263] +
      Complex<double> (0, 1) * amp[1265] + Complex<double> (0, 1) * amp[1266] +
      Complex<double> (0, 1) * amp[1267] - amp[1401] + amp[1541] + amp[1540] +
      amp[1543] + amp[1544] + amp[1548] + amp[1549] + amp[1551] + amp[1552] +
      Complex<double> (0, 1) * amp[1560] + Complex<double> (0, 1) * amp[1561] -
      amp[1563] - amp[1564] - Complex<double> (0, 1) * amp[1584] -
      Complex<double> (0, 1) * amp[1585] - Complex<double> (0, 1) * amp[1666] -
      Complex<double> (0, 1) * amp[1667] - amp[1672] - amp[1673] + amp[1677] +
      amp[1678] + amp[1681] + amp[1680] + amp[1685] + amp[1684] -
      Complex<double> (0, 1) * amp[1697] - Complex<double> (0, 1) * amp[1696] +
      amp[1700] + amp[1699] - amp[1866] + amp[1871] + amp[1868] - amp[1869] -
      amp[1874] - amp[1873] - Complex<double> (0, 1) * amp[1880] -
      Complex<double> (0, 1) * amp[1879] + amp[1883] + amp[1882];
  jamp[51] = -Complex<double> (0, 1) * amp[496] - Complex<double> (0, 1) *
      amp[497] - amp[498] - Complex<double> (0, 1) * amp[503] + Complex<double>
      (0, 1) * amp[501] - amp[511] + Complex<double> (0, 1) * amp[512] +
      amp[531] + Complex<double> (0, 1) * amp[532] - Complex<double> (0, 1) *
      amp[536] - amp[539] - amp[538] - amp[541] - amp[542] - amp[554] -
      amp[557] - amp[556] - amp[572] + amp[570] + amp[575] + amp[574] +
      amp[577] + amp[578] - Complex<double> (0, 1) * amp[623] - Complex<double>
      (0, 1) * amp[627] - Complex<double> (0, 1) * amp[628] - amp[633] -
      amp[634] - Complex<double> (0, 1) * amp[656] - amp[657] - Complex<double>
      (0, 1) * amp[658] - Complex<double> (0, 1) * amp[662] - Complex<double>
      (0, 1) * amp[661] + Complex<double> (0, 1) * amp[664] + amp[665] -
      Complex<double> (0, 1) * amp[695] - amp[698] + amp[696] + amp[699] -
      amp[701] + Complex<double> (0, 1) * amp[702] - amp[703] - amp[713] -
      amp[716] - amp[715] - amp[718] - amp[719] + amp[722] - amp[720] +
      amp[726] + amp[727] - amp[731] - amp[730] - Complex<double> (0, 1) *
      amp[743] + Complex<double> (0, 1) * amp[744] + Complex<double> (0, 1) *
      amp[745] - amp[1017] + Complex<double> (0, 1) * amp[1021] + amp[1022] -
      Complex<double> (0, 1) * amp[1061] - Complex<double> (0, 1) * amp[1067] -
      Complex<double> (0, 1) * amp[1066] - amp[1241] - amp[1245] - amp[1246] +
      amp[1248] + amp[1249] - Complex<double> (0, 1) * amp[1262] + amp[1263] -
      Complex<double> (0, 1) * amp[1265] - Complex<double> (0, 1) * amp[1266] -
      Complex<double> (0, 1) * amp[1267] - amp[1361] - amp[1362] + amp[1365] -
      amp[1363] + amp[1367] + amp[1371] + amp[1372] - amp[1374] - amp[1375] -
      amp[1379] - amp[1378] - amp[1407] - Complex<double> (0, 1) * amp[1410] +
      Complex<double> (0, 1) * amp[1412] + Complex<double> (0, 1) * amp[1413] +
      Complex<double> (0, 1) * amp[1414] + amp[1658] + amp[1657] -
      Complex<double> (0, 1) * amp[1661] - Complex<double> (0, 1) * amp[1660] +
      Complex<double> (0, 1) * amp[1666] + Complex<double> (0, 1) * amp[1667] +
      amp[1672] + amp[1673] - amp[1682] - amp[1678] - amp[1681] - amp[1679] -
      amp[1688] - amp[1687] - Complex<double> (0, 1) * amp[1769] +
      Complex<double> (0, 1) * amp[1767] - amp[1772] + amp[1770] + amp[1784] -
      amp[1790] - amp[1782] + amp[1788] + amp[1793] - amp[1791] - amp[1796] +
      amp[1794] + Complex<double> (0, 1) * amp[1799] - Complex<double> (0, 1) *
      amp[1797] - amp[1865] + amp[1866] + amp[1863] - amp[1868] + amp[1872] +
      amp[1873] + Complex<double> (0, 1) * amp[1878] + Complex<double> (0, 1) *
      amp[1879] - amp[1881] - amp[1882];
  jamp[52] = -amp[513] - Complex<double> (0, 1) * amp[514] + Complex<double>
      (0, 1) * amp[518] + amp[521] + amp[520] + amp[523] + amp[524] -
      Complex<double> (0, 1) * amp[532] + amp[538] + amp[537] + amp[540] +
      amp[541] - Complex<double> (0, 1) * amp[546] + amp[547] - amp[560] +
      amp[563] + amp[562] + amp[567] + amp[568] - Complex<double> (0, 1) *
      amp[605] - amp[607] + Complex<double> (0, 1) * amp[608] - Complex<double>
      (0, 1) * amp[609] - Complex<double> (0, 1) * amp[610] + Complex<double>
      (0, 1) * amp[629] + Complex<double> (0, 1) * amp[628] + amp[635] +
      amp[634] + Complex<double> (0, 1) * amp[854] + amp[857] - amp[855] -
      amp[858] + amp[860] - Complex<double> (0, 1) * amp[861] + amp[862] +
      amp[867] + amp[868] + amp[870] + amp[869] - amp[873] + amp[876] +
      amp[875] + amp[884] + amp[883] + amp[887] - amp[885] + Complex<double>
      (0, 1) * amp[897] + Complex<double> (0, 1) * amp[905] - Complex<double>
      (0, 1) * amp[903] + amp[906] + Complex<double> (0, 1) * amp[907] -
      Complex<double> (0, 1) * amp[930] - amp[934] + Complex<double> (0, 1) *
      amp[935] - Complex<double> (0, 1) * amp[938] - Complex<double> (0, 1) *
      amp[937] + amp[1163] + amp[1167] + amp[1168] - amp[1170] - amp[1171] +
      Complex<double> (0, 1) * amp[1184] - amp[1185] + Complex<double> (0, 1) *
      amp[1187] + Complex<double> (0, 1) * amp[1188] + Complex<double> (0, 1) *
      amp[1189] - amp[1230] - amp[1231] - amp[1233] - amp[1232] + amp[1241] -
      amp[1244] - amp[1243] - amp[1247] + amp[1245] - amp[1248] - amp[1249] -
      Complex<double> (0, 1) * amp[1260] - amp[1263] + Complex<double> (0, 1) *
      amp[1265] - Complex<double> (0, 1) * amp[1268] + Complex<double> (0, 1) *
      amp[1266] - amp[1269] - Complex<double> (0, 1) * amp[1270] +
      Complex<double> (0, 1) * amp[1293] + Complex<double> (0, 1) * amp[1301] +
      Complex<double> (0, 1) * amp[1300] - amp[1395] + amp[1595] + amp[1594] +
      amp[1597] + amp[1598] + amp[1602] + amp[1603] + amp[1605] + amp[1606] +
      Complex<double> (0, 1) * amp[1614] + Complex<double> (0, 1) * amp[1615] -
      amp[1617] - amp[1618] - Complex<double> (0, 1) * amp[1638] -
      Complex<double> (0, 1) * amp[1639] - Complex<double> (0, 1) * amp[1720] -
      Complex<double> (0, 1) * amp[1721] - amp[1726] - amp[1727] + amp[1731] +
      amp[1732] + amp[1735] + amp[1734] + amp[1739] + amp[1738] -
      Complex<double> (0, 1) * amp[1751] - Complex<double> (0, 1) * amp[1750] +
      amp[1754] + amp[1753] - amp[1866] + amp[1871] + amp[1868] - amp[1869] -
      amp[1874] - amp[1873] - Complex<double> (0, 1) * amp[1880] -
      Complex<double> (0, 1) * amp[1879] + amp[1883] + amp[1882];
  jamp[53] = -Complex<double> (0, 1) * amp[495] - Complex<double> (0, 1) *
      amp[499] - amp[500] - Complex<double> (0, 1) * amp[501] - Complex<double>
      (0, 1) * amp[502] + amp[511] - Complex<double> (0, 1) * amp[512] +
      amp[513] + Complex<double> (0, 1) * amp[514] - Complex<double> (0, 1) *
      amp[518] - amp[521] - amp[520] - amp[523] - amp[524] - amp[559] -
      amp[562] - amp[561] - amp[570] - amp[571] - amp[575] - amp[574] -
      amp[577] - amp[578] + Complex<double> (0, 1) * amp[623] - Complex<double>
      (0, 1) * amp[629] + Complex<double> (0, 1) * amp[627] - amp[635] +
      amp[633] - Complex<double> (0, 1) * amp[815] - amp[816] - Complex<double>
      (0, 1) * amp[817] - Complex<double> (0, 1) * amp[821] - Complex<double>
      (0, 1) * amp[820] + Complex<double> (0, 1) * amp[823] + amp[824] -
      Complex<double> (0, 1) * amp[854] - amp[857] + amp[855] + amp[858] -
      amp[860] + Complex<double> (0, 1) * amp[861] - amp[862] - amp[872] -
      amp[875] - amp[874] - amp[877] - amp[878] + amp[881] - amp[879] +
      amp[885] + amp[886] - amp[890] - amp[889] - Complex<double> (0, 1) *
      amp[902] + Complex<double> (0, 1) * amp[903] + Complex<double> (0, 1) *
      amp[904] - amp[1016] - Complex<double> (0, 1) * amp[1021] - amp[1022] +
      Complex<double> (0, 1) * amp[1061] + Complex<double> (0, 1) * amp[1067] +
      Complex<double> (0, 1) * amp[1066] - amp[1163] - amp[1167] - amp[1168] +
      amp[1170] + amp[1171] - Complex<double> (0, 1) * amp[1184] + amp[1185] -
      Complex<double> (0, 1) * amp[1187] - Complex<double> (0, 1) * amp[1188] -
      Complex<double> (0, 1) * amp[1189] + amp[1361] + amp[1362] - amp[1365] +
      amp[1363] - amp[1367] - amp[1371] - amp[1372] + amp[1374] + amp[1375] +
      amp[1379] + amp[1378] + amp[1407] + Complex<double> (0, 1) * amp[1410] -
      Complex<double> (0, 1) * amp[1412] - Complex<double> (0, 1) * amp[1413] -
      Complex<double> (0, 1) * amp[1414] + amp[1712] + amp[1711] -
      Complex<double> (0, 1) * amp[1715] - Complex<double> (0, 1) * amp[1714] +
      Complex<double> (0, 1) * amp[1720] + Complex<double> (0, 1) * amp[1721] +
      amp[1726] + amp[1727] - amp[1736] - amp[1732] - amp[1735] - amp[1733] -
      amp[1742] - amp[1741] - Complex<double> (0, 1) * amp[1767] -
      Complex<double> (0, 1) * amp[1768] - amp[1770] - amp[1771] - amp[1784] +
      amp[1785] + amp[1782] - amp[1787] + amp[1791] + amp[1792] - amp[1794] -
      amp[1795] + Complex<double> (0, 1) * amp[1797] + Complex<double> (0, 1) *
      amp[1798] + amp[1865] - amp[1871] - amp[1863] + amp[1869] + amp[1874] -
      amp[1872] + Complex<double> (0, 1) * amp[1880] - Complex<double> (0, 1) *
      amp[1878] - amp[1883] + amp[1881];
  jamp[54] = -amp[1] - amp[4] - amp[3] - amp[5] - amp[6] - amp[8] - amp[7] -
      amp[17] - amp[16] - amp[23] + amp[21] + amp[38] + amp[37] + amp[40] +
      amp[41] - Complex<double> (0, 1) * amp[47] - Complex<double> (0, 1) *
      amp[53] + Complex<double> (0, 1) * amp[51] - Complex<double> (0, 1) *
      amp[57] - amp[59] - amp[60] + Complex<double> (0, 1) * amp[62] +
      Complex<double> (0, 1) * amp[125] + amp[126] + Complex<double> (0, 1) *
      amp[127] + Complex<double> (0, 1) * amp[131] + Complex<double> (0, 1) *
      amp[130] - Complex<double> (0, 1) * amp[152] + Complex<double> (0, 1) *
      amp[676] - amp[680] + amp[678] + amp[681] - amp[683] - Complex<double>
      (0, 1) * amp[687] - amp[689] - amp[708] - amp[709] - amp[711] - amp[710]
      + amp[719] - amp[722] - amp[721] - amp[725] + amp[723] - amp[728] -
      amp[727] - Complex<double> (0, 1) * amp[738] + amp[742] + Complex<double>
      (0, 1) * amp[743] - Complex<double> (0, 1) * amp[746] - Complex<double>
      (0, 1) * amp[745] - amp[747] + Complex<double> (0, 1) * amp[749] +
      Complex<double> (0, 1) * amp[771] + Complex<double> (0, 1) * amp[779] -
      Complex<double> (0, 1) * amp[777] - amp[1075] - amp[1078] - amp[1077] -
      amp[1088] - amp[1087] - amp[1100] + amp[1098] + amp[1101] - amp[1103] +
      Complex<double> (0, 1) * amp[1137] + amp[1138] - Complex<double> (0, 1) *
      amp[1141] + Complex<double> (0, 1) * amp[1145] + Complex<double> (0, 1) *
      amp[1144] - Complex<double> (0, 1) * amp[1147] + Complex<double> (0, 1) *
      amp[1148] - amp[1150] + amp[1362] - amp[1365] - amp[1364] - amp[1373] -
      amp[1372] - amp[1405] + amp[1409] + Complex<double> (0, 1) * amp[1410] +
      Complex<double> (0, 1) * amp[1411] - Complex<double> (0, 1) * amp[1415] -
      Complex<double> (0, 1) * amp[1414] + amp[1486] + amp[1485] - amp[1492] -
      amp[1491] - amp[1496] + amp[1494] + amp[1500] - amp[1502] - amp[1529] +
      amp[1527] + Complex<double> (0, 1) * amp[1532] - Complex<double> (0, 1) *
      amp[1530] - Complex<double> (0, 1) * amp[1533] + Complex<double> (0, 1) *
      amp[1535] - amp[1547] - amp[1543] - amp[1546] - amp[1544] - amp[1550] -
      amp[1549] - amp[1553] - amp[1552] - Complex<double> (0, 1) * amp[1562] -
      Complex<double> (0, 1) * amp[1561] + amp[1565] + amp[1564] +
      Complex<double> (0, 1) * amp[1586] + Complex<double> (0, 1) * amp[1585] +
      Complex<double> (0, 1) * amp[1776] - Complex<double> (0, 1) * amp[1778] -
      amp[1779] + amp[1781] + amp[1783] + amp[1782] - amp[1789] - amp[1788] -
      amp[1793] + amp[1791] - Complex<double> (0, 1) * amp[1799] +
      Complex<double> (0, 1) * amp[1797] + amp[1800] - amp[1802];
  jamp[55] = -amp[0] + amp[4] - amp[2] - amp[10] - amp[11] - amp[13] - amp[12]
      - amp[20] - amp[19] - amp[21] - amp[22] - amp[38] - amp[37] - amp[40] -
      amp[41] - Complex<double> (0, 1) * amp[49] - Complex<double> (0, 1) *
      amp[51] - Complex<double> (0, 1) * amp[52] - Complex<double> (0, 1) *
      amp[54] - amp[56] + amp[60] - Complex<double> (0, 1) * amp[62] +
      Complex<double> (0, 1) * amp[113] + amp[114] + Complex<double> (0, 1) *
      amp[115] + Complex<double> (0, 1) * amp[119] + Complex<double> (0, 1) *
      amp[118] + Complex<double> (0, 1) * amp[152] + Complex<double> (0, 1) *
      amp[835] - amp[839] + amp[837] + amp[840] - amp[842] - Complex<double>
      (0, 1) * amp[846] - amp[848] - amp[867] - amp[868] - amp[870] - amp[869]
      + amp[878] - amp[881] - amp[880] - amp[884] + amp[882] - amp[887] -
      amp[886] - Complex<double> (0, 1) * amp[897] + amp[901] + Complex<double>
      (0, 1) * amp[902] - Complex<double> (0, 1) * amp[905] - Complex<double>
      (0, 1) * amp[904] - amp[906] + Complex<double> (0, 1) * amp[908] +
      Complex<double> (0, 1) * amp[930] + Complex<double> (0, 1) * amp[938] -
      Complex<double> (0, 1) * amp[936] - amp[1074] + amp[1078] - amp[1076] -
      amp[1091] - amp[1090] + amp[1100] - amp[1098] - amp[1101] + amp[1103] +
      Complex<double> (0, 1) * amp[1128] + amp[1129] - Complex<double> (0, 1) *
      amp[1132] + Complex<double> (0, 1) * amp[1136] + Complex<double> (0, 1) *
      amp[1135] + Complex<double> (0, 1) * amp[1147] - Complex<double> (0, 1) *
      amp[1148] + amp[1150] - amp[1362] + amp[1365] + amp[1364] + amp[1373] +
      amp[1372] - amp[1399] - amp[1409] - Complex<double> (0, 1) * amp[1410] -
      Complex<double> (0, 1) * amp[1411] + Complex<double> (0, 1) * amp[1415] +
      Complex<double> (0, 1) * amp[1414] + amp[1487] - amp[1493] - amp[1485] +
      amp[1491] - amp[1499] + amp[1497] - amp[1500] + amp[1502] - amp[1523] +
      amp[1521] + Complex<double> (0, 1) * amp[1526] - Complex<double> (0, 1) *
      amp[1524] + Complex<double> (0, 1) * amp[1533] - Complex<double> (0, 1) *
      amp[1535] - amp[1601] - amp[1597] - amp[1600] - amp[1598] - amp[1604] -
      amp[1603] - amp[1607] - amp[1606] - Complex<double> (0, 1) * amp[1616] -
      Complex<double> (0, 1) * amp[1615] + amp[1619] + amp[1618] +
      Complex<double> (0, 1) * amp[1640] + Complex<double> (0, 1) * amp[1639] -
      Complex<double> (0, 1) * amp[1776] - Complex<double> (0, 1) * amp[1777] +
      amp[1779] + amp[1780] - amp[1785] - amp[1783] - amp[1786] - amp[1782] -
      amp[1791] - amp[1792] - Complex<double> (0, 1) * amp[1797] -
      Complex<double> (0, 1) * amp[1798] - amp[1800] - amp[1801];
  jamp[56] = -Complex<double> (0, 1) * amp[676] + amp[680] - amp[678] -
      amp[681] + amp[683] + Complex<double> (0, 1) * amp[687] + amp[689] +
      amp[708] + amp[709] + amp[711] + amp[710] - amp[719] + amp[722] +
      amp[721] + amp[725] - amp[723] + amp[728] + amp[727] + Complex<double>
      (0, 1) * amp[738] - amp[742] - Complex<double> (0, 1) * amp[743] +
      Complex<double> (0, 1) * amp[746] + Complex<double> (0, 1) * amp[745] +
      amp[747] - Complex<double> (0, 1) * amp[749] - Complex<double> (0, 1) *
      amp[771] - Complex<double> (0, 1) * amp[779] + Complex<double> (0, 1) *
      amp[777] - amp[833] + Complex<double> (0, 1) * amp[834] - Complex<double>
      (0, 1) * amp[835] + amp[839] + amp[838] + amp[841] + amp[842] +
      Complex<double> (0, 1) * amp[852] + amp[856] + amp[855] + amp[858] +
      amp[859] + Complex<double> (0, 1) * amp[861] + amp[863] - amp[878] +
      amp[881] + amp[880] + amp[885] + amp[886] + Complex<double> (0, 1) *
      amp[899] - amp[901] - Complex<double> (0, 1) * amp[902] + Complex<double>
      (0, 1) * amp[903] + Complex<double> (0, 1) * amp[904] - Complex<double>
      (0, 1) * amp[950] - Complex<double> (0, 1) * amp[949] + amp[953] +
      amp[952] + amp[1084] + amp[1086] + amp[1087] - amp[1095] - amp[1096] -
      Complex<double> (0, 1) * amp[1139] - amp[1140] - Complex<double> (0, 1) *
      amp[1142] - Complex<double> (0, 1) * amp[1143] - Complex<double> (0, 1) *
      amp[1144] - amp[1152] - amp[1153] - amp[1155] - amp[1154] + amp[1162] -
      amp[1166] + amp[1164] - amp[1169] - amp[1168] - amp[1173] - amp[1174] -
      Complex<double> (0, 1) * amp[1182] - Complex<double> (0, 1) * amp[1190] -
      Complex<double> (0, 1) * amp[1189] - amp[1191] + Complex<double> (0, 1) *
      amp[1193] + Complex<double> (0, 1) * amp[1215] - amp[1218] -
      Complex<double> (0, 1) * amp[1220] + Complex<double> (0, 1) * amp[1223] -
      Complex<double> (0, 1) * amp[1221] - amp[1403] + amp[1541] + amp[1540] +
      amp[1543] + amp[1544] + amp[1548] + amp[1549] + amp[1551] + amp[1552] +
      Complex<double> (0, 1) * amp[1560] + Complex<double> (0, 1) * amp[1561] -
      amp[1563] - amp[1564] - Complex<double> (0, 1) * amp[1584] -
      Complex<double> (0, 1) * amp[1585] + Complex<double> (0, 1) * amp[1777] +
      Complex<double> (0, 1) * amp[1778] - amp[1780] - amp[1781] + amp[1785] +
      amp[1786] + amp[1789] + amp[1788] + amp[1793] + amp[1792] +
      Complex<double> (0, 1) * amp[1799] + Complex<double> (0, 1) * amp[1798] +
      amp[1801] + amp[1802] - amp[1812] + amp[1817] + amp[1814] - amp[1815] -
      amp[1820] - amp[1819] + Complex<double> (0, 1) * amp[1832] +
      Complex<double> (0, 1) * amp[1831] + amp[1835] + amp[1834];
  jamp[57] = +Complex<double> (0, 1) * amp[336] - Complex<double> (0, 1) *
      amp[340] + Complex<double> (0, 1) * amp[338] - Complex<double> (0, 1) *
      amp[343] - amp[344] + Complex<double> (0, 1) * amp[352] - amp[353] +
      Complex<double> (0, 1) * amp[354] + Complex<double> (0, 1) * amp[355] +
      Complex<double> (0, 1) * amp[357] + Complex<double> (0, 1) * amp[356] -
      amp[362] + amp[364] + Complex<double> (0, 1) * amp[365] - amp[394] +
      amp[397] + Complex<double> (0, 1) * amp[398] + Complex<double> (0, 1) *
      amp[437] + Complex<double> (0, 1) * amp[440] + Complex<double> (0, 1) *
      amp[439] - Complex<double> (0, 1) * amp[445] - Complex<double> (0, 1) *
      amp[446] + amp[449] + amp[448] + Complex<double> (0, 1) * amp[462] -
      Complex<double> (0, 1) * amp[464] - amp[470] + amp[468] + amp[833] -
      Complex<double> (0, 1) * amp[834] + Complex<double> (0, 1) * amp[835] -
      amp[839] - amp[838] - amp[841] - amp[842] - Complex<double> (0, 1) *
      amp[852] - amp[856] - amp[855] - amp[858] - amp[859] - Complex<double>
      (0, 1) * amp[861] - amp[863] + amp[878] - amp[881] - amp[880] - amp[885]
      - amp[886] - Complex<double> (0, 1) * amp[899] + amp[901] +
      Complex<double> (0, 1) * amp[902] - Complex<double> (0, 1) * amp[903] -
      Complex<double> (0, 1) * amp[904] + Complex<double> (0, 1) * amp[950] +
      Complex<double> (0, 1) * amp[949] - amp[953] - amp[952] + amp[1079] -
      amp[1083] + amp[1081] + amp[1097] + amp[1096] + amp[1100] + amp[1099] +
      amp[1102] + amp[1103] - Complex<double> (0, 1) * amp[1148] + amp[1157] +
      amp[1158] + amp[1160] + amp[1159] + amp[1163] + amp[1167] + amp[1168] +
      amp[1172] - amp[1170] + amp[1175] + amp[1174] + Complex<double> (0, 1) *
      amp[1187] + Complex<double> (0, 1) * amp[1188] + Complex<double> (0, 1) *
      amp[1189] + amp[1356] + amp[1359] + amp[1358] - amp[1362] + amp[1365] +
      amp[1364] + amp[1367] + amp[1371] + amp[1372] + amp[1376] - amp[1374] -
      amp[1409] - Complex<double> (0, 1) * amp[1410] + Complex<double> (0, 1) *
      amp[1412] + Complex<double> (0, 1) * amp[1413] + Complex<double> (0, 1) *
      amp[1414] - Complex<double> (0, 1) * amp[1776] - Complex<double> (0, 1) *
      amp[1777] + amp[1779] + amp[1780] - amp[1785] - amp[1783] - amp[1786] -
      amp[1782] - amp[1791] - amp[1792] - Complex<double> (0, 1) * amp[1797] -
      Complex<double> (0, 1) * amp[1798] - amp[1800] - amp[1801] - amp[1817] -
      amp[1813] - amp[1816] - amp[1814] - amp[1822] - amp[1823] + amp[1864] +
      amp[1863] - amp[1870] - amp[1869] - amp[1874] + amp[1872] + amp[1875] -
      amp[1877] - Complex<double> (0, 1) * amp[1880] + Complex<double> (0, 1) *
      amp[1878];
  jamp[58] = -amp[674] + Complex<double> (0, 1) * amp[675] - Complex<double>
      (0, 1) * amp[676] + amp[680] + amp[679] + amp[682] + amp[683] +
      Complex<double> (0, 1) * amp[693] + amp[697] + amp[696] + amp[699] +
      amp[700] + Complex<double> (0, 1) * amp[702] + amp[704] - amp[719] +
      amp[722] + amp[721] + amp[726] + amp[727] + Complex<double> (0, 1) *
      amp[740] - amp[742] - Complex<double> (0, 1) * amp[743] + Complex<double>
      (0, 1) * amp[744] + Complex<double> (0, 1) * amp[745] - Complex<double>
      (0, 1) * amp[791] - Complex<double> (0, 1) * amp[790] + amp[794] +
      amp[793] - Complex<double> (0, 1) * amp[835] + amp[839] - amp[837] -
      amp[840] + amp[842] + Complex<double> (0, 1) * amp[846] + amp[848] +
      amp[867] + amp[868] + amp[870] + amp[869] - amp[878] + amp[881] +
      amp[880] + amp[884] - amp[882] + amp[887] + amp[886] + Complex<double>
      (0, 1) * amp[897] - amp[901] - Complex<double> (0, 1) * amp[902] +
      Complex<double> (0, 1) * amp[905] + Complex<double> (0, 1) * amp[904] +
      amp[906] - Complex<double> (0, 1) * amp[908] - Complex<double> (0, 1) *
      amp[930] - Complex<double> (0, 1) * amp[938] + Complex<double> (0, 1) *
      amp[936] + amp[1085] + amp[1089] + amp[1090] - amp[1092] - amp[1093] -
      Complex<double> (0, 1) * amp[1130] - amp[1131] - Complex<double> (0, 1) *
      amp[1133] - Complex<double> (0, 1) * amp[1134] - Complex<double> (0, 1) *
      amp[1135] - amp[1230] - amp[1231] - amp[1233] - amp[1232] + amp[1240] -
      amp[1244] + amp[1242] - amp[1247] - amp[1246] - amp[1251] - amp[1252] -
      Complex<double> (0, 1) * amp[1260] - Complex<double> (0, 1) * amp[1268] -
      Complex<double> (0, 1) * amp[1267] - amp[1269] + Complex<double> (0, 1) *
      amp[1271] + Complex<double> (0, 1) * amp[1293] - amp[1296] -
      Complex<double> (0, 1) * amp[1298] + Complex<double> (0, 1) * amp[1301] -
      Complex<double> (0, 1) * amp[1299] - amp[1397] + amp[1595] + amp[1594] +
      amp[1597] + amp[1598] + amp[1602] + amp[1603] + amp[1605] + amp[1606] +
      Complex<double> (0, 1) * amp[1614] + Complex<double> (0, 1) * amp[1615] -
      amp[1617] - amp[1618] - Complex<double> (0, 1) * amp[1638] -
      Complex<double> (0, 1) * amp[1639] + Complex<double> (0, 1) * amp[1777] +
      Complex<double> (0, 1) * amp[1778] - amp[1780] - amp[1781] + amp[1785] +
      amp[1786] + amp[1789] + amp[1788] + amp[1793] + amp[1792] +
      Complex<double> (0, 1) * amp[1799] + Complex<double> (0, 1) * amp[1798] +
      amp[1801] + amp[1802] - amp[1839] + amp[1844] + amp[1841] - amp[1842] -
      amp[1847] - amp[1846] + Complex<double> (0, 1) * amp[1859] +
      Complex<double> (0, 1) * amp[1858] + amp[1862] + amp[1861];
  jamp[59] = +Complex<double> (0, 1) * amp[337] + Complex<double> (0, 1) *
      amp[340] + Complex<double> (0, 1) * amp[339] - Complex<double> (0, 1) *
      amp[341] - amp[342] - Complex<double> (0, 1) * amp[352] + amp[353] +
      Complex<double> (0, 1) * amp[372] + Complex<double> (0, 1) * amp[373] +
      Complex<double> (0, 1) * amp[375] + Complex<double> (0, 1) * amp[374] -
      amp[380] + amp[382] + Complex<double> (0, 1) * amp[383] - amp[392] -
      amp[397] - Complex<double> (0, 1) * amp[398] - Complex<double> (0, 1) *
      amp[437] - Complex<double> (0, 1) * amp[440] - Complex<double> (0, 1) *
      amp[439] - Complex<double> (0, 1) * amp[454] - Complex<double> (0, 1) *
      amp[455] + amp[458] + amp[457] - Complex<double> (0, 1) * amp[462] -
      Complex<double> (0, 1) * amp[463] - amp[468] - amp[469] + amp[674] -
      Complex<double> (0, 1) * amp[675] + Complex<double> (0, 1) * amp[676] -
      amp[680] - amp[679] - amp[682] - amp[683] - Complex<double> (0, 1) *
      amp[693] - amp[697] - amp[696] - amp[699] - amp[700] - Complex<double>
      (0, 1) * amp[702] - amp[704] + amp[719] - amp[722] - amp[721] - amp[726]
      - amp[727] - Complex<double> (0, 1) * amp[740] + amp[742] +
      Complex<double> (0, 1) * amp[743] - Complex<double> (0, 1) * amp[744] -
      Complex<double> (0, 1) * amp[745] + Complex<double> (0, 1) * amp[791] +
      Complex<double> (0, 1) * amp[790] - amp[794] - amp[793] + amp[1080] +
      amp[1083] + amp[1082] + amp[1094] + amp[1093] - amp[1100] - amp[1099] -
      amp[1102] - amp[1103] + Complex<double> (0, 1) * amp[1148] + amp[1235] +
      amp[1236] + amp[1238] + amp[1237] + amp[1241] + amp[1245] + amp[1246] +
      amp[1250] - amp[1248] + amp[1253] + amp[1252] + Complex<double> (0, 1) *
      amp[1265] + Complex<double> (0, 1) * amp[1266] + Complex<double> (0, 1) *
      amp[1267] - amp[1356] - amp[1359] - amp[1358] + amp[1362] - amp[1365] -
      amp[1364] - amp[1367] - amp[1371] - amp[1372] - amp[1376] + amp[1374] +
      amp[1409] + Complex<double> (0, 1) * amp[1410] - Complex<double> (0, 1) *
      amp[1412] - Complex<double> (0, 1) * amp[1413] - Complex<double> (0, 1) *
      amp[1414] + Complex<double> (0, 1) * amp[1776] - Complex<double> (0, 1) *
      amp[1778] - amp[1779] + amp[1781] + amp[1783] + amp[1782] - amp[1789] -
      amp[1788] - amp[1793] + amp[1791] - Complex<double> (0, 1) * amp[1799] +
      Complex<double> (0, 1) * amp[1797] + amp[1800] - amp[1802] - amp[1844] -
      amp[1840] - amp[1843] - amp[1841] - amp[1849] - amp[1850] - amp[1866] -
      amp[1864] - amp[1867] - amp[1863] - amp[1872] - amp[1873] - amp[1875] -
      amp[1876] - Complex<double> (0, 1) * amp[1878] - Complex<double> (0, 1) *
      amp[1879];
  jamp[60] = -amp[5] - amp[6] - amp[8] - amp[7] - amp[10] + amp[14] - amp[12] -
      amp[15] - amp[16] - amp[23] - amp[22] - amp[26] - amp[25] - amp[28] -
      amp[29] - Complex<double> (0, 1) * amp[47] - Complex<double> (0, 1) *
      amp[49] + amp[50] - Complex<double> (0, 1) * amp[53] - Complex<double>
      (0, 1) * amp[52] + Complex<double> (0, 1) * amp[58] - amp[59] +
      Complex<double> (0, 1) * amp[127] + Complex<double> (0, 1) * amp[129] +
      Complex<double> (0, 1) * amp[130] + amp[133] + Complex<double> (0, 1) *
      amp[134] - Complex<double> (0, 1) * amp[139] - Complex<double> (0, 1) *
      amp[695] - amp[698] + amp[696] + amp[699] - amp[701] + Complex<double>
      (0, 1) * amp[702] - amp[703] - amp[708] - amp[709] - amp[711] - amp[710]
      + amp[714] - amp[717] - amp[716] - amp[725] - amp[724] - amp[728] +
      amp[726] - Complex<double> (0, 1) * amp[738] - Complex<double> (0, 1) *
      amp[746] + Complex<double> (0, 1) * amp[744] - amp[747] - Complex<double>
      (0, 1) * amp[748] + Complex<double> (0, 1) * amp[771] + amp[775] -
      Complex<double> (0, 1) * amp[776] + Complex<double> (0, 1) * amp[779] +
      Complex<double> (0, 1) * amp[778] - amp[1230] + amp[1234] - amp[1232] -
      amp[1247] - amp[1246] + amp[1256] - amp[1254] - amp[1257] + amp[1259] -
      Complex<double> (0, 1) * amp[1260] + amp[1261] + Complex<double> (0, 1) *
      amp[1264] - Complex<double> (0, 1) * amp[1268] - Complex<double> (0, 1) *
      amp[1267] - Complex<double> (0, 1) * amp[1302] + Complex<double> (0, 1) *
      amp[1305] + amp[1307] - amp[1314] + amp[1317] + amp[1316] + amp[1325] +
      amp[1324] - amp[1406] - amp[1478] + Complex<double> (0, 1) * amp[1479] +
      Complex<double> (0, 1) * amp[1480] - Complex<double> (0, 1) * amp[1484] -
      Complex<double> (0, 1) * amp[1483] - amp[1547] - amp[1543] - amp[1546] -
      amp[1544] - amp[1550] - amp[1549] - amp[1553] - amp[1552] -
      Complex<double> (0, 1) * amp[1562] - Complex<double> (0, 1) * amp[1561] +
      amp[1565] + amp[1564] + Complex<double> (0, 1) * amp[1586] +
      Complex<double> (0, 1) * amp[1585] + amp[1595] - amp[1601] - amp[1593] +
      amp[1599] - amp[1607] + amp[1605] - amp[1608] + amp[1610] + amp[1611] -
      amp[1613] - Complex<double> (0, 1) * amp[1616] + Complex<double> (0, 1) *
      amp[1614] - Complex<double> (0, 1) * amp[1644] + Complex<double> (0, 1) *
      amp[1646] + Complex<double> (0, 1) * amp[1665] + Complex<double> (0, 1) *
      amp[1666] + amp[1671] + amp[1672] - amp[1677] - amp[1675] - amp[1678] -
      amp[1674] - amp[1683] - amp[1684] + Complex<double> (0, 1) * amp[1695] +
      Complex<double> (0, 1) * amp[1696] - amp[1698] - amp[1699];
  jamp[61] = +Complex<double> (0, 1) * amp[656] + amp[657] + Complex<double>
      (0, 1) * amp[658] + Complex<double> (0, 1) * amp[662] + Complex<double>
      (0, 1) * amp[661] - Complex<double> (0, 1) * amp[664] - amp[665] +
      Complex<double> (0, 1) * amp[695] + amp[698] - amp[696] - amp[699] +
      amp[701] - Complex<double> (0, 1) * amp[702] + amp[703] + amp[713] +
      amp[716] + amp[715] + amp[718] + amp[719] - amp[722] + amp[720] -
      amp[726] - amp[727] + amp[731] + amp[730] + Complex<double> (0, 1) *
      amp[743] - Complex<double> (0, 1) * amp[744] - Complex<double> (0, 1) *
      amp[745] + Complex<double> (0, 1) * amp[817] + Complex<double> (0, 1) *
      amp[819] + Complex<double> (0, 1) * amp[820] + Complex<double> (0, 1) *
      amp[822] - amp[824] + amp[828] + Complex<double> (0, 1) * amp[830] -
      amp[867] + amp[871] - amp[869] + amp[877] + amp[878] - amp[881] +
      amp[879] - amp[887] - amp[886] + amp[888] + amp[889] + amp[893] -
      amp[891] - amp[894] + amp[896] - Complex<double> (0, 1) * amp[897] +
      amp[898] + Complex<double> (0, 1) * amp[902] - Complex<double> (0, 1) *
      amp[905] - Complex<double> (0, 1) * amp[904] - Complex<double> (0, 1) *
      amp[939] - amp[1019] - Complex<double> (0, 1) * amp[1043] - amp[1044] +
      Complex<double> (0, 1) * amp[1046] - Complex<double> (0, 1) * amp[1049] -
      Complex<double> (0, 1) * amp[1048] + amp[1230] - amp[1234] + amp[1232] +
      amp[1247] + amp[1246] - amp[1256] + amp[1254] + amp[1257] - amp[1259] +
      Complex<double> (0, 1) * amp[1260] - amp[1261] - Complex<double> (0, 1) *
      amp[1264] + Complex<double> (0, 1) * amp[1268] + Complex<double> (0, 1) *
      amp[1267] + Complex<double> (0, 1) * amp[1302] - Complex<double> (0, 1) *
      amp[1305] - amp[1307] - amp[1313] - amp[1316] - amp[1315] - amp[1331] -
      amp[1330] - amp[1595] + amp[1596] + amp[1593] - amp[1598] - amp[1605] -
      amp[1606] + amp[1608] + amp[1609] - amp[1611] - amp[1612] -
      Complex<double> (0, 1) * amp[1614] - Complex<double> (0, 1) * amp[1615] +
      Complex<double> (0, 1) * amp[1644] + Complex<double> (0, 1) * amp[1645] -
      amp[1656] - amp[1657] + Complex<double> (0, 1) * amp[1659] +
      Complex<double> (0, 1) * amp[1660] - Complex<double> (0, 1) * amp[1665] -
      Complex<double> (0, 1) * amp[1666] - amp[1671] - amp[1672] + amp[1676] +
      amp[1675] + amp[1678] + amp[1679] + amp[1686] + amp[1687] +
      Complex<double> (0, 1) * amp[1769] + Complex<double> (0, 1) * amp[1768] +
      amp[1772] + amp[1771] - amp[1785] + amp[1790] + amp[1787] - amp[1788] -
      amp[1793] - amp[1792] + amp[1796] + amp[1795] - Complex<double> (0, 1) *
      amp[1799] - Complex<double> (0, 1) * amp[1798];
  jamp[62] = +amp[5] + amp[6] + amp[8] + amp[7] + amp[10] - amp[14] + amp[12] +
      amp[15] + amp[16] + amp[23] + amp[22] + amp[26] + amp[25] + amp[28] +
      amp[29] + Complex<double> (0, 1) * amp[47] + Complex<double> (0, 1) *
      amp[49] - amp[50] + Complex<double> (0, 1) * amp[53] + Complex<double>
      (0, 1) * amp[52] - Complex<double> (0, 1) * amp[58] + amp[59] -
      Complex<double> (0, 1) * amp[127] - Complex<double> (0, 1) * amp[129] -
      Complex<double> (0, 1) * amp[130] - amp[133] - Complex<double> (0, 1) *
      amp[134] + Complex<double> (0, 1) * amp[139] - Complex<double> (0, 1) *
      amp[852] - amp[856] - amp[855] - amp[858] - amp[859] - Complex<double>
      (0, 1) * amp[861] - amp[863] + amp[867] - amp[871] + amp[869] + amp[887]
      - amp[885] + amp[892] + amp[891] + amp[894] + amp[895] + Complex<double>
      (0, 1) * amp[897] - amp[898] + Complex<double> (0, 1) * amp[900] +
      Complex<double> (0, 1) * amp[905] - Complex<double> (0, 1) * amp[903] +
      Complex<double> (0, 1) * amp[939] + Complex<double> (0, 1) * amp[943] +
      amp[944] + Complex<double> (0, 1) * amp[948] + Complex<double> (0, 1) *
      amp[949] - amp[951] - amp[952] + amp[1152] + amp[1153] + amp[1155] +
      amp[1154] - amp[1162] + amp[1166] - amp[1164] + amp[1169] + amp[1168] +
      amp[1173] + amp[1174] + Complex<double> (0, 1) * amp[1182] +
      Complex<double> (0, 1) * amp[1190] + Complex<double> (0, 1) * amp[1189] +
      amp[1191] - Complex<double> (0, 1) * amp[1193] - Complex<double> (0, 1) *
      amp[1215] + amp[1218] + Complex<double> (0, 1) * amp[1220] -
      Complex<double> (0, 1) * amp[1223] + Complex<double> (0, 1) * amp[1221] +
      amp[1319] - amp[1325] + amp[1323] - amp[1326] - amp[1327] - amp[1404] -
      amp[1476] + Complex<double> (0, 1) * amp[1477] - Complex<double> (0, 1) *
      amp[1481] + Complex<double> (0, 1) * amp[1484] - Complex<double> (0, 1) *
      amp[1482] - amp[1541] + amp[1547] - amp[1540] + amp[1546] + amp[1550] -
      amp[1548] + amp[1553] - amp[1551] + Complex<double> (0, 1) * amp[1562] -
      Complex<double> (0, 1) * amp[1560] - amp[1565] + amp[1563] -
      Complex<double> (0, 1) * amp[1586] + Complex<double> (0, 1) * amp[1584] -
      amp[1596] + amp[1601] + amp[1598] - amp[1599] + amp[1607] + amp[1606] -
      amp[1609] - amp[1610] + amp[1612] + amp[1613] + Complex<double> (0, 1) *
      amp[1616] + Complex<double> (0, 1) * amp[1615] - Complex<double> (0, 1) *
      amp[1645] - Complex<double> (0, 1) * amp[1646] - amp[1811] + amp[1812] +
      amp[1809] - amp[1814] + amp[1818] + amp[1819] - Complex<double> (0, 1) *
      amp[1830] - Complex<double> (0, 1) * amp[1831] - amp[1833] - amp[1834];
  jamp[63] = -Complex<double> (0, 1) * amp[354] - Complex<double> (0, 1) *
      amp[355] - Complex<double> (0, 1) * amp[357] - Complex<double> (0, 1) *
      amp[356] + amp[362] - amp[364] - Complex<double> (0, 1) * amp[365] -
      Complex<double> (0, 1) * amp[373] - Complex<double> (0, 1) * amp[376] -
      Complex<double> (0, 1) * amp[375] + Complex<double> (0, 1) * amp[381] -
      amp[382] + amp[387] + Complex<double> (0, 1) * amp[388] - amp[395] +
      Complex<double> (0, 1) * amp[419] + Complex<double> (0, 1) * amp[422] +
      Complex<double> (0, 1) * amp[421] - amp[424] + Complex<double> (0, 1) *
      amp[425] + Complex<double> (0, 1) * amp[444] + Complex<double> (0, 1) *
      amp[445] - amp[447] - amp[448] + Complex<double> (0, 1) * amp[463] +
      Complex<double> (0, 1) * amp[464] + amp[470] + amp[469] + Complex<double>
      (0, 1) * amp[852] + amp[856] + amp[855] + amp[858] + amp[859] +
      Complex<double> (0, 1) * amp[861] + amp[863] - amp[867] + amp[871] -
      amp[869] - amp[887] + amp[885] - amp[892] - amp[891] - amp[894] -
      amp[895] - Complex<double> (0, 1) * amp[897] + amp[898] - Complex<double>
      (0, 1) * amp[900] - Complex<double> (0, 1) * amp[905] + Complex<double>
      (0, 1) * amp[903] - Complex<double> (0, 1) * amp[939] - Complex<double>
      (0, 1) * amp[943] - amp[944] - Complex<double> (0, 1) * amp[948] -
      Complex<double> (0, 1) * amp[949] + amp[951] + amp[952] - amp[1157] -
      amp[1158] - amp[1160] - amp[1159] - amp[1163] - amp[1167] - amp[1168] -
      amp[1172] + amp[1170] - amp[1175] - amp[1174] - Complex<double> (0, 1) *
      amp[1187] - Complex<double> (0, 1) * amp[1188] - Complex<double> (0, 1) *
      amp[1189] + amp[1230] - amp[1234] + amp[1232] - amp[1236] - amp[1239] -
      amp[1238] - amp[1241] + amp[1247] - amp[1245] - amp[1250] + amp[1248] +
      amp[1255] + amp[1254] + amp[1257] + amp[1258] + Complex<double> (0, 1) *
      amp[1260] - amp[1261] - Complex<double> (0, 1) * amp[1265] +
      Complex<double> (0, 1) * amp[1268] - Complex<double> (0, 1) * amp[1266] +
      Complex<double> (0, 1) * amp[1302] + amp[1308] + amp[1311] + amp[1310] +
      amp[1328] + amp[1327] - amp[1595] + amp[1596] + amp[1593] - amp[1598] -
      amp[1605] - amp[1606] + amp[1608] + amp[1609] - amp[1611] - amp[1612] -
      Complex<double> (0, 1) * amp[1614] - Complex<double> (0, 1) * amp[1615] +
      Complex<double> (0, 1) * amp[1644] + Complex<double> (0, 1) * amp[1645] +
      amp[1811] + amp[1810] + amp[1813] + amp[1814] + amp[1821] + amp[1822] +
      amp[1866] + amp[1867] + amp[1870] + amp[1869] + amp[1874] + amp[1873] +
      amp[1876] + amp[1877] + Complex<double> (0, 1) * amp[1880] +
      Complex<double> (0, 1) * amp[1879];
  jamp[64] = +amp[10] - amp[14] + amp[12] + amp[21] + amp[22] + amp[26] -
      amp[24] - amp[27] + amp[29] + amp[38] - amp[36] - amp[39] + amp[41] +
      amp[44] - amp[42] + Complex<double> (0, 1) * amp[45] + Complex<double>
      (0, 1) * amp[49] - amp[50] + Complex<double> (0, 1) * amp[51] +
      Complex<double> (0, 1) * amp[52] + amp[61] + Complex<double> (0, 1) *
      amp[62] + amp[137] - Complex<double> (0, 1) * amp[138] + Complex<double>
      (0, 1) * amp[139] - Complex<double> (0, 1) * amp[150] + Complex<double>
      (0, 1) * amp[158] - Complex<double> (0, 1) * amp[156] - Complex<double>
      (0, 1) * amp[817] - Complex<double> (0, 1) * amp[819] - Complex<double>
      (0, 1) * amp[820] - Complex<double> (0, 1) * amp[822] + amp[824] -
      amp[828] - Complex<double> (0, 1) * amp[830] + amp[867] - amp[871] +
      amp[869] - amp[877] - amp[878] + amp[881] - amp[879] + amp[887] +
      amp[886] - amp[888] - amp[889] - amp[893] + amp[891] + amp[894] -
      amp[896] + Complex<double> (0, 1) * amp[897] - amp[898] - Complex<double>
      (0, 1) * amp[902] + Complex<double> (0, 1) * amp[905] + Complex<double>
      (0, 1) * amp[904] + Complex<double> (0, 1) * amp[939] - amp[1014] +
      Complex<double> (0, 1) * amp[1020] - amp[1022] + Complex<double> (0, 1) *
      amp[1041] + amp[1042] - Complex<double> (0, 1) * amp[1045] +
      Complex<double> (0, 1) * amp[1047] + Complex<double> (0, 1) * amp[1048] +
      amp[1060] + Complex<double> (0, 1) * amp[1061] - Complex<double> (0, 1) *
      amp[1063] + Complex<double> (0, 1) * amp[1065] + Complex<double> (0, 1) *
      amp[1066] - amp[1070] + amp[1068] - Complex<double> (0, 1) * amp[1073] +
      Complex<double> (0, 1) * amp[1071] - amp[1318] + amp[1322] - amp[1320] +
      amp[1329] + amp[1330] + amp[1361] + amp[1362] - amp[1365] + amp[1363] -
      amp[1366] + amp[1370] - amp[1368] - amp[1373] - amp[1372] + amp[1377] +
      amp[1378] + amp[1386] + amp[1382] - amp[1388] - amp[1380] - amp[1394] +
      amp[1392] + Complex<double> (0, 1) * amp[1410] - Complex<double> (0, 1) *
      amp[1415] - Complex<double> (0, 1) * amp[1414] - amp[1596] + amp[1601] +
      amp[1598] - amp[1599] + amp[1607] + amp[1606] - amp[1609] - amp[1610] +
      amp[1612] + amp[1613] + Complex<double> (0, 1) * amp[1616] +
      Complex<double> (0, 1) * amp[1615] - Complex<double> (0, 1) * amp[1645] -
      Complex<double> (0, 1) * amp[1646] - Complex<double> (0, 1) * amp[1767] -
      Complex<double> (0, 1) * amp[1768] - amp[1770] - amp[1771] - amp[1784] +
      amp[1785] + amp[1782] - amp[1787] + amp[1791] + amp[1792] - amp[1794] -
      amp[1795] + Complex<double> (0, 1) * amp[1797] + Complex<double> (0, 1) *
      amp[1798];
  jamp[65] = -amp[10] + amp[14] - amp[12] - amp[21] - amp[22] - amp[26] +
      amp[24] + amp[27] - amp[29] - amp[38] + amp[36] + amp[39] - amp[41] -
      amp[44] + amp[42] - Complex<double> (0, 1) * amp[45] - Complex<double>
      (0, 1) * amp[49] + amp[50] - Complex<double> (0, 1) * amp[51] -
      Complex<double> (0, 1) * amp[52] - amp[61] - Complex<double> (0, 1) *
      amp[62] - amp[137] + Complex<double> (0, 1) * amp[138] - Complex<double>
      (0, 1) * amp[139] + Complex<double> (0, 1) * amp[150] - Complex<double>
      (0, 1) * amp[158] + Complex<double> (0, 1) * amp[156] + Complex<double>
      (0, 1) * amp[373] + Complex<double> (0, 1) * amp[376] + Complex<double>
      (0, 1) * amp[375] - Complex<double> (0, 1) * amp[381] + amp[382] -
      amp[387] - Complex<double> (0, 1) * amp[388] - amp[390] + Complex<double>
      (0, 1) * amp[396] - amp[397] + Complex<double> (0, 1) * amp[417] +
      amp[418] + Complex<double> (0, 1) * amp[420] - Complex<double> (0, 1) *
      amp[423] - Complex<double> (0, 1) * amp[422] + amp[436] - Complex<double>
      (0, 1) * amp[437] + Complex<double> (0, 1) * amp[438] - Complex<double>
      (0, 1) * amp[441] - Complex<double> (0, 1) * amp[440] - Complex<double>
      (0, 1) * amp[462] - Complex<double> (0, 1) * amp[463] - amp[468] -
      amp[469] - amp[473] + amp[471] + Complex<double> (0, 1) * amp[474] -
      Complex<double> (0, 1) * amp[476] - amp[1230] + amp[1234] - amp[1232] +
      amp[1236] + amp[1239] + amp[1238] + amp[1241] - amp[1247] + amp[1245] +
      amp[1250] - amp[1248] - amp[1255] - amp[1254] - amp[1257] - amp[1258] -
      Complex<double> (0, 1) * amp[1260] + amp[1261] + Complex<double> (0, 1) *
      amp[1265] - Complex<double> (0, 1) * amp[1268] + Complex<double> (0, 1) *
      amp[1266] - Complex<double> (0, 1) * amp[1302] + amp[1309] - amp[1312] -
      amp[1311] - amp[1322] - amp[1321] - amp[1356] + amp[1357] - amp[1360] -
      amp[1359] - amp[1367] - amp[1370] - amp[1369] + amp[1373] - amp[1371] -
      amp[1376] + amp[1374] - amp[1386] + amp[1381] + amp[1380] - amp[1387] +
      amp[1389] - amp[1391] - Complex<double> (0, 1) * amp[1412] +
      Complex<double> (0, 1) * amp[1415] - Complex<double> (0, 1) * amp[1413] +
      amp[1595] - amp[1601] - amp[1593] + amp[1599] - amp[1607] + amp[1605] -
      amp[1608] + amp[1610] + amp[1611] - amp[1613] - Complex<double> (0, 1) *
      amp[1616] + Complex<double> (0, 1) * amp[1614] - Complex<double> (0, 1) *
      amp[1644] + Complex<double> (0, 1) * amp[1646] - amp[1866] - amp[1864] -
      amp[1867] - amp[1863] - amp[1872] - amp[1873] - amp[1875] - amp[1876] -
      Complex<double> (0, 1) * amp[1878] - Complex<double> (0, 1) * amp[1879];
  jamp[66] = -amp[5] + amp[9] - amp[7] - amp[10] - amp[11] - amp[13] - amp[12]
      - amp[18] - amp[19] - amp[23] - amp[22] - amp[32] - amp[31] - amp[34] -
      amp[35] - Complex<double> (0, 1) * amp[47] + amp[48] - Complex<double>
      (0, 1) * amp[49] - Complex<double> (0, 1) * amp[53] - Complex<double> (0,
      1) * amp[52] + Complex<double> (0, 1) * amp[55] - amp[56] +
      Complex<double> (0, 1) * amp[115] + Complex<double> (0, 1) * amp[117] +
      Complex<double> (0, 1) * amp[118] + amp[121] + Complex<double> (0, 1) *
      amp[122] - Complex<double> (0, 1) * amp[145] - Complex<double> (0, 1) *
      amp[854] - amp[857] + amp[855] + amp[858] - amp[860] + Complex<double>
      (0, 1) * amp[861] - amp[862] - amp[867] - amp[868] - amp[870] - amp[869]
      + amp[873] - amp[876] - amp[875] - amp[884] - amp[883] - amp[887] +
      amp[885] - Complex<double> (0, 1) * amp[897] - Complex<double> (0, 1) *
      amp[905] + Complex<double> (0, 1) * amp[903] - amp[906] - Complex<double>
      (0, 1) * amp[907] + Complex<double> (0, 1) * amp[930] + amp[934] -
      Complex<double> (0, 1) * amp[935] + Complex<double> (0, 1) * amp[938] +
      Complex<double> (0, 1) * amp[937] - amp[1152] + amp[1156] - amp[1154] -
      amp[1169] - amp[1168] + amp[1178] - amp[1176] - amp[1179] + amp[1181] -
      Complex<double> (0, 1) * amp[1182] + amp[1183] + Complex<double> (0, 1) *
      amp[1186] - Complex<double> (0, 1) * amp[1190] - Complex<double> (0, 1) *
      amp[1189] - Complex<double> (0, 1) * amp[1224] + Complex<double> (0, 1) *
      amp[1227] + amp[1229] - amp[1338] + amp[1341] + amp[1340] + amp[1349] +
      amp[1348] - amp[1400] - amp[1469] + Complex<double> (0, 1) * amp[1470] +
      Complex<double> (0, 1) * amp[1471] - Complex<double> (0, 1) * amp[1475] -
      Complex<double> (0, 1) * amp[1474] + amp[1541] - amp[1547] - amp[1539] +
      amp[1545] - amp[1553] + amp[1551] - amp[1554] + amp[1556] + amp[1557] -
      amp[1559] - Complex<double> (0, 1) * amp[1562] + Complex<double> (0, 1) *
      amp[1560] - Complex<double> (0, 1) * amp[1590] + Complex<double> (0, 1) *
      amp[1592] - amp[1601] - amp[1597] - amp[1600] - amp[1598] - amp[1604] -
      amp[1603] - amp[1607] - amp[1606] - Complex<double> (0, 1) * amp[1616] -
      Complex<double> (0, 1) * amp[1615] + amp[1619] + amp[1618] +
      Complex<double> (0, 1) * amp[1640] + Complex<double> (0, 1) * amp[1639] +
      Complex<double> (0, 1) * amp[1719] + Complex<double> (0, 1) * amp[1720] +
      amp[1725] + amp[1726] - amp[1731] - amp[1729] - amp[1732] - amp[1728] -
      amp[1737] - amp[1738] + Complex<double> (0, 1) * amp[1749] +
      Complex<double> (0, 1) * amp[1750] - amp[1752] - amp[1753];
  jamp[67] = +Complex<double> (0, 1) * amp[658] + Complex<double> (0, 1) *
      amp[660] + Complex<double> (0, 1) * amp[661] + Complex<double> (0, 1) *
      amp[663] - amp[665] + amp[669] + Complex<double> (0, 1) * amp[671] -
      amp[708] + amp[712] - amp[710] + amp[718] + amp[719] - amp[722] +
      amp[720] - amp[728] - amp[727] + amp[729] + amp[730] + amp[734] -
      amp[732] - amp[735] + amp[737] - Complex<double> (0, 1) * amp[738] +
      amp[739] + Complex<double> (0, 1) * amp[743] - Complex<double> (0, 1) *
      amp[746] - Complex<double> (0, 1) * amp[745] - Complex<double> (0, 1) *
      amp[780] + Complex<double> (0, 1) * amp[815] + amp[816] + Complex<double>
      (0, 1) * amp[817] + Complex<double> (0, 1) * amp[821] + Complex<double>
      (0, 1) * amp[820] - Complex<double> (0, 1) * amp[823] - amp[824] +
      Complex<double> (0, 1) * amp[854] + amp[857] - amp[855] - amp[858] +
      amp[860] - Complex<double> (0, 1) * amp[861] + amp[862] + amp[872] +
      amp[875] + amp[874] + amp[877] + amp[878] - amp[881] + amp[879] -
      amp[885] - amp[886] + amp[890] + amp[889] + Complex<double> (0, 1) *
      amp[902] - Complex<double> (0, 1) * amp[903] - Complex<double> (0, 1) *
      amp[904] - amp[1018] - Complex<double> (0, 1) * amp[1052] - amp[1053] +
      Complex<double> (0, 1) * amp[1055] - Complex<double> (0, 1) * amp[1058] -
      Complex<double> (0, 1) * amp[1057] + amp[1152] - amp[1156] + amp[1154] +
      amp[1169] + amp[1168] - amp[1178] + amp[1176] + amp[1179] - amp[1181] +
      Complex<double> (0, 1) * amp[1182] - amp[1183] - Complex<double> (0, 1) *
      amp[1186] + Complex<double> (0, 1) * amp[1190] + Complex<double> (0, 1) *
      amp[1189] + Complex<double> (0, 1) * amp[1224] - Complex<double> (0, 1) *
      amp[1227] - amp[1229] - amp[1337] - amp[1340] - amp[1339] - amp[1355] -
      amp[1354] - amp[1541] + amp[1542] + amp[1539] - amp[1544] - amp[1551] -
      amp[1552] + amp[1554] + amp[1555] - amp[1557] - amp[1558] -
      Complex<double> (0, 1) * amp[1560] - Complex<double> (0, 1) * amp[1561] +
      Complex<double> (0, 1) * amp[1590] + Complex<double> (0, 1) * amp[1591] -
      amp[1710] - amp[1711] + Complex<double> (0, 1) * amp[1713] +
      Complex<double> (0, 1) * amp[1714] - Complex<double> (0, 1) * amp[1719] -
      Complex<double> (0, 1) * amp[1720] - amp[1725] - amp[1726] + amp[1730] +
      amp[1729] + amp[1732] + amp[1733] + amp[1740] + amp[1741] +
      Complex<double> (0, 1) * amp[1769] + Complex<double> (0, 1) * amp[1768] +
      amp[1772] + amp[1771] - amp[1785] + amp[1790] + amp[1787] - amp[1788] -
      amp[1793] - amp[1792] + amp[1796] + amp[1795] - Complex<double> (0, 1) *
      amp[1799] - Complex<double> (0, 1) * amp[1798];
  jamp[68] = +amp[5] - amp[9] + amp[7] + amp[10] + amp[11] + amp[13] + amp[12]
      + amp[18] + amp[19] + amp[23] + amp[22] + amp[32] + amp[31] + amp[34] +
      amp[35] + Complex<double> (0, 1) * amp[47] - amp[48] + Complex<double>
      (0, 1) * amp[49] + Complex<double> (0, 1) * amp[53] + Complex<double> (0,
      1) * amp[52] - Complex<double> (0, 1) * amp[55] + amp[56] -
      Complex<double> (0, 1) * amp[115] - Complex<double> (0, 1) * amp[117] -
      Complex<double> (0, 1) * amp[118] - amp[121] - Complex<double> (0, 1) *
      amp[122] + Complex<double> (0, 1) * amp[145] - Complex<double> (0, 1) *
      amp[693] - amp[697] - amp[696] - amp[699] - amp[700] - Complex<double>
      (0, 1) * amp[702] - amp[704] + amp[708] - amp[712] + amp[710] + amp[728]
      - amp[726] + amp[733] + amp[732] + amp[735] + amp[736] + Complex<double>
      (0, 1) * amp[738] - amp[739] + Complex<double> (0, 1) * amp[741] +
      Complex<double> (0, 1) * amp[746] - Complex<double> (0, 1) * amp[744] +
      Complex<double> (0, 1) * amp[780] + Complex<double> (0, 1) * amp[784] +
      amp[785] + Complex<double> (0, 1) * amp[789] + Complex<double> (0, 1) *
      amp[790] - amp[792] - amp[793] + amp[1230] + amp[1231] + amp[1233] +
      amp[1232] - amp[1240] + amp[1244] - amp[1242] + amp[1247] + amp[1246] +
      amp[1251] + amp[1252] + Complex<double> (0, 1) * amp[1260] +
      Complex<double> (0, 1) * amp[1268] + Complex<double> (0, 1) * amp[1267] +
      amp[1269] - Complex<double> (0, 1) * amp[1271] - Complex<double> (0, 1) *
      amp[1293] + amp[1296] + Complex<double> (0, 1) * amp[1298] -
      Complex<double> (0, 1) * amp[1301] + Complex<double> (0, 1) * amp[1299] +
      amp[1343] - amp[1349] + amp[1347] - amp[1350] - amp[1351] - amp[1398] -
      amp[1467] + Complex<double> (0, 1) * amp[1468] - Complex<double> (0, 1) *
      amp[1472] + Complex<double> (0, 1) * amp[1475] - Complex<double> (0, 1) *
      amp[1473] - amp[1542] + amp[1547] + amp[1544] - amp[1545] + amp[1553] +
      amp[1552] - amp[1555] - amp[1556] + amp[1558] + amp[1559] +
      Complex<double> (0, 1) * amp[1562] + Complex<double> (0, 1) * amp[1561] -
      Complex<double> (0, 1) * amp[1591] - Complex<double> (0, 1) * amp[1592] -
      amp[1595] + amp[1601] - amp[1594] + amp[1600] + amp[1604] - amp[1602] +
      amp[1607] - amp[1605] + Complex<double> (0, 1) * amp[1616] -
      Complex<double> (0, 1) * amp[1614] - amp[1619] + amp[1617] -
      Complex<double> (0, 1) * amp[1640] + Complex<double> (0, 1) * amp[1638] -
      amp[1838] + amp[1839] + amp[1836] - amp[1841] + amp[1845] + amp[1846] -
      Complex<double> (0, 1) * amp[1857] - Complex<double> (0, 1) * amp[1858] -
      amp[1860] - amp[1861];
  jamp[69] = -Complex<double> (0, 1) * amp[355] - Complex<double> (0, 1) *
      amp[358] - Complex<double> (0, 1) * amp[357] + Complex<double> (0, 1) *
      amp[363] - amp[364] + amp[369] + Complex<double> (0, 1) * amp[370] -
      Complex<double> (0, 1) * amp[372] - Complex<double> (0, 1) * amp[373] -
      Complex<double> (0, 1) * amp[375] - Complex<double> (0, 1) * amp[374] +
      amp[380] - amp[382] - Complex<double> (0, 1) * amp[383] - amp[393] +
      Complex<double> (0, 1) * amp[428] + Complex<double> (0, 1) * amp[431] +
      Complex<double> (0, 1) * amp[430] - amp[433] + Complex<double> (0, 1) *
      amp[434] + Complex<double> (0, 1) * amp[453] + Complex<double> (0, 1) *
      amp[454] - amp[456] - amp[457] + Complex<double> (0, 1) * amp[463] +
      Complex<double> (0, 1) * amp[464] + amp[470] + amp[469] + Complex<double>
      (0, 1) * amp[693] + amp[697] + amp[696] + amp[699] + amp[700] +
      Complex<double> (0, 1) * amp[702] + amp[704] - amp[708] + amp[712] -
      amp[710] - amp[728] + amp[726] - amp[733] - amp[732] - amp[735] -
      amp[736] - Complex<double> (0, 1) * amp[738] + amp[739] - Complex<double>
      (0, 1) * amp[741] - Complex<double> (0, 1) * amp[746] + Complex<double>
      (0, 1) * amp[744] - Complex<double> (0, 1) * amp[780] - Complex<double>
      (0, 1) * amp[784] - amp[785] - Complex<double> (0, 1) * amp[789] -
      Complex<double> (0, 1) * amp[790] + amp[792] + amp[793] + amp[1152] -
      amp[1156] + amp[1154] - amp[1158] - amp[1161] - amp[1160] - amp[1163] +
      amp[1169] - amp[1167] - amp[1172] + amp[1170] + amp[1177] + amp[1176] +
      amp[1179] + amp[1180] + Complex<double> (0, 1) * amp[1182] - amp[1183] -
      Complex<double> (0, 1) * amp[1187] + Complex<double> (0, 1) * amp[1190] -
      Complex<double> (0, 1) * amp[1188] + Complex<double> (0, 1) * amp[1224] -
      amp[1235] - amp[1236] - amp[1238] - amp[1237] - amp[1241] - amp[1245] -
      amp[1246] - amp[1250] + amp[1248] - amp[1253] - amp[1252] -
      Complex<double> (0, 1) * amp[1265] - Complex<double> (0, 1) * amp[1266] -
      Complex<double> (0, 1) * amp[1267] + amp[1332] + amp[1335] + amp[1334] +
      amp[1352] + amp[1351] - amp[1541] + amp[1542] + amp[1539] - amp[1544] -
      amp[1551] - amp[1552] + amp[1554] + amp[1555] - amp[1557] - amp[1558] -
      Complex<double> (0, 1) * amp[1560] - Complex<double> (0, 1) * amp[1561] +
      Complex<double> (0, 1) * amp[1590] + Complex<double> (0, 1) * amp[1591] +
      amp[1838] + amp[1837] + amp[1840] + amp[1841] + amp[1848] + amp[1849] +
      amp[1866] + amp[1867] + amp[1870] + amp[1869] + amp[1874] + amp[1873] +
      amp[1876] + amp[1877] + Complex<double> (0, 1) * amp[1880] +
      Complex<double> (0, 1) * amp[1879];
  jamp[70] = +amp[5] - amp[9] + amp[7] + amp[23] - amp[21] + amp[32] - amp[30]
      - amp[33] + amp[35] - amp[38] + amp[36] + amp[39] - amp[41] + amp[42] +
      amp[43] + Complex<double> (0, 1) * amp[46] + Complex<double> (0, 1) *
      amp[47] - amp[48] + Complex<double> (0, 1) * amp[53] - Complex<double>
      (0, 1) * amp[51] - amp[61] - Complex<double> (0, 1) * amp[62] + amp[143]
      - Complex<double> (0, 1) * amp[144] + Complex<double> (0, 1) * amp[145] +
      Complex<double> (0, 1) * amp[150] + Complex<double> (0, 1) * amp[156] +
      Complex<double> (0, 1) * amp[157] - Complex<double> (0, 1) * amp[658] -
      Complex<double> (0, 1) * amp[660] - Complex<double> (0, 1) * amp[661] -
      Complex<double> (0, 1) * amp[663] + amp[665] - amp[669] - Complex<double>
      (0, 1) * amp[671] + amp[708] - amp[712] + amp[710] - amp[718] - amp[719]
      + amp[722] - amp[720] + amp[728] + amp[727] - amp[729] - amp[730] -
      amp[734] + amp[732] + amp[735] - amp[737] + Complex<double> (0, 1) *
      amp[738] - amp[739] - Complex<double> (0, 1) * amp[743] + Complex<double>
      (0, 1) * amp[746] + Complex<double> (0, 1) * amp[745] + Complex<double>
      (0, 1) * amp[780] - amp[1015] - Complex<double> (0, 1) * amp[1020] +
      amp[1022] + Complex<double> (0, 1) * amp[1050] + amp[1051] -
      Complex<double> (0, 1) * amp[1054] + Complex<double> (0, 1) * amp[1056] +
      Complex<double> (0, 1) * amp[1057] - amp[1060] - Complex<double> (0, 1) *
      amp[1061] + Complex<double> (0, 1) * amp[1063] - Complex<double> (0, 1) *
      amp[1065] - Complex<double> (0, 1) * amp[1066] - amp[1068] - amp[1069] -
      Complex<double> (0, 1) * amp[1071] - Complex<double> (0, 1) * amp[1072] -
      amp[1342] + amp[1346] - amp[1344] + amp[1353] + amp[1354] - amp[1361] -
      amp[1362] + amp[1365] - amp[1363] + amp[1366] - amp[1370] + amp[1368] +
      amp[1373] + amp[1372] - amp[1377] - amp[1378] - amp[1382] - amp[1385] +
      amp[1380] + amp[1383] - amp[1392] - amp[1393] - Complex<double> (0, 1) *
      amp[1410] + Complex<double> (0, 1) * amp[1415] + Complex<double> (0, 1) *
      amp[1414] - amp[1542] + amp[1547] + amp[1544] - amp[1545] + amp[1553] +
      amp[1552] - amp[1555] - amp[1556] + amp[1558] + amp[1559] +
      Complex<double> (0, 1) * amp[1562] + Complex<double> (0, 1) * amp[1561] -
      Complex<double> (0, 1) * amp[1591] - Complex<double> (0, 1) * amp[1592] -
      Complex<double> (0, 1) * amp[1769] + Complex<double> (0, 1) * amp[1767] -
      amp[1772] + amp[1770] + amp[1784] - amp[1790] - amp[1782] + amp[1788] +
      amp[1793] - amp[1791] - amp[1796] + amp[1794] + Complex<double> (0, 1) *
      amp[1799] - Complex<double> (0, 1) * amp[1797];
  jamp[71] = -amp[5] + amp[9] - amp[7] - amp[23] + amp[21] - amp[32] + amp[30]
      + amp[33] - amp[35] + amp[38] - amp[36] - amp[39] + amp[41] - amp[42] -
      amp[43] - Complex<double> (0, 1) * amp[46] - Complex<double> (0, 1) *
      amp[47] + amp[48] - Complex<double> (0, 1) * amp[53] + Complex<double>
      (0, 1) * amp[51] + amp[61] + Complex<double> (0, 1) * amp[62] - amp[143]
      + Complex<double> (0, 1) * amp[144] - Complex<double> (0, 1) * amp[145] -
      Complex<double> (0, 1) * amp[150] - Complex<double> (0, 1) * amp[156] -
      Complex<double> (0, 1) * amp[157] + Complex<double> (0, 1) * amp[355] +
      Complex<double> (0, 1) * amp[358] + Complex<double> (0, 1) * amp[357] -
      Complex<double> (0, 1) * amp[363] + amp[364] - amp[369] - Complex<double>
      (0, 1) * amp[370] - amp[391] - Complex<double> (0, 1) * amp[396] +
      amp[397] + Complex<double> (0, 1) * amp[426] + amp[427] + Complex<double>
      (0, 1) * amp[429] - Complex<double> (0, 1) * amp[432] - Complex<double>
      (0, 1) * amp[431] - amp[436] + Complex<double> (0, 1) * amp[437] -
      Complex<double> (0, 1) * amp[438] + Complex<double> (0, 1) * amp[441] +
      Complex<double> (0, 1) * amp[440] + Complex<double> (0, 1) * amp[462] -
      Complex<double> (0, 1) * amp[464] - amp[470] + amp[468] - amp[471] -
      amp[472] - Complex<double> (0, 1) * amp[474] - Complex<double> (0, 1) *
      amp[475] - amp[1152] + amp[1156] - amp[1154] + amp[1158] + amp[1161] +
      amp[1160] + amp[1163] - amp[1169] + amp[1167] + amp[1172] - amp[1170] -
      amp[1177] - amp[1176] - amp[1179] - amp[1180] - Complex<double> (0, 1) *
      amp[1182] + amp[1183] + Complex<double> (0, 1) * amp[1187] -
      Complex<double> (0, 1) * amp[1190] + Complex<double> (0, 1) * amp[1188] -
      Complex<double> (0, 1) * amp[1224] + amp[1333] - amp[1336] - amp[1335] -
      amp[1346] - amp[1345] + amp[1356] - amp[1357] + amp[1360] + amp[1359] +
      amp[1367] + amp[1370] + amp[1369] - amp[1373] + amp[1371] + amp[1376] -
      amp[1374] - amp[1384] - amp[1381] - amp[1380] - amp[1383] - amp[1389] -
      amp[1390] + Complex<double> (0, 1) * amp[1412] - Complex<double> (0, 1) *
      amp[1415] + Complex<double> (0, 1) * amp[1413] + amp[1541] - amp[1547] -
      amp[1539] + amp[1545] - amp[1553] + amp[1551] - amp[1554] + amp[1556] +
      amp[1557] - amp[1559] - Complex<double> (0, 1) * amp[1562] +
      Complex<double> (0, 1) * amp[1560] - Complex<double> (0, 1) * amp[1590] +
      Complex<double> (0, 1) * amp[1592] + amp[1864] + amp[1863] - amp[1870] -
      amp[1869] - amp[1874] + amp[1872] + amp[1875] - amp[1877] -
      Complex<double> (0, 1) * amp[1880] + Complex<double> (0, 1) * amp[1878];
  jamp[72] = +amp[0] + amp[1] + amp[3] + amp[2] + amp[6] + amp[9] + amp[8] +
      amp[17] + amp[16] + amp[20] - amp[18] - amp[32] - amp[31] - amp[34] -
      amp[35] + Complex<double> (0, 1) * amp[65] + Complex<double> (0, 1) *
      amp[71] - Complex<double> (0, 1) * amp[69] + Complex<double> (0, 1) *
      amp[75] + amp[77] + amp[78] - Complex<double> (0, 1) * amp[80] -
      Complex<double> (0, 1) * amp[125] - Complex<double> (0, 1) * amp[127] -
      amp[128] - Complex<double> (0, 1) * amp[131] - Complex<double> (0, 1) *
      amp[130] + Complex<double> (0, 1) * amp[146] + Complex<double> (0, 1) *
      amp[673] - amp[679] - amp[678] - amp[681] - amp[682] + Complex<double>
      (0, 1) * amp[687] - amp[688] + amp[709] + amp[712] + amp[711] + amp[725]
      - amp[723] - amp[733] - amp[732] - amp[735] - amp[736] - Complex<double>
      (0, 1) * amp[771] - amp[772] - Complex<double> (0, 1) * amp[774] -
      Complex<double> (0, 1) * amp[779] + Complex<double> (0, 1) * amp[777] +
      Complex<double> (0, 1) * amp[781] + Complex<double> (0, 1) * amp[782] -
      amp[783] - Complex<double> (0, 1) * amp[788] + Complex<double> (0, 1) *
      amp[786] - amp[794] + amp[792] + amp[1074] + amp[1075] + amp[1077] +
      amp[1076] - amp[1085] + amp[1088] + amp[1087] + amp[1091] - amp[1089] +
      amp[1092] + amp[1093] + Complex<double> (0, 1) * amp[1104] + amp[1107] -
      Complex<double> (0, 1) * amp[1109] + Complex<double> (0, 1) * amp[1112] -
      Complex<double> (0, 1) * amp[1110] + amp[1113] + Complex<double> (0, 1) *
      amp[1114] - Complex<double> (0, 1) * amp[1137] - Complex<double> (0, 1) *
      amp[1145] - Complex<double> (0, 1) * amp[1144] - amp[1343] + amp[1349] -
      amp[1347] + amp[1350] + amp[1351] - amp[1423] + amp[1428] +
      Complex<double> (0, 1) * amp[1429] - Complex<double> (0, 1) * amp[1433] +
      Complex<double> (0, 1) * amp[1436] - Complex<double> (0, 1) * amp[1434] -
      amp[1487] + amp[1493] - amp[1486] + amp[1492] + amp[1496] - amp[1494] +
      amp[1499] - amp[1497] + Complex<double> (0, 1) * amp[1508] -
      Complex<double> (0, 1) * amp[1506] - amp[1511] + amp[1509] -
      Complex<double> (0, 1) * amp[1532] + Complex<double> (0, 1) * amp[1530] +
      amp[1542] + amp[1543] + amp[1546] + amp[1545] + amp[1550] + amp[1549] +
      amp[1555] + amp[1556] + amp[1583] + amp[1582] - Complex<double> (0, 1) *
      amp[1586] - Complex<double> (0, 1) * amp[1585] - Complex<double> (0, 1) *
      amp[1588] - Complex<double> (0, 1) * amp[1589] + amp[1838] - amp[1844] -
      amp[1836] + amp[1842] + amp[1847] - amp[1845] + Complex<double> (0, 1) *
      amp[1853] - Complex<double> (0, 1) * amp[1851] - amp[1856] + amp[1854];
  jamp[73] = +amp[5] - amp[9] + amp[7] + amp[10] + amp[11] + amp[13] + amp[12]
      + amp[18] + amp[19] + amp[23] + amp[22] + amp[32] + amp[31] + amp[34] +
      amp[35] + Complex<double> (0, 1) * amp[67] + Complex<double> (0, 1) *
      amp[69] + Complex<double> (0, 1) * amp[70] + Complex<double> (0, 1) *
      amp[72] + amp[74] - amp[78] + Complex<double> (0, 1) * amp[80] -
      Complex<double> (0, 1) * amp[101] - amp[102] - Complex<double> (0, 1) *
      amp[103] - Complex<double> (0, 1) * amp[107] - Complex<double> (0, 1) *
      amp[106] - Complex<double> (0, 1) * amp[146] + Complex<double> (0, 1) *
      amp[691] - amp[697] - amp[696] - amp[699] - amp[700] + Complex<double>
      (0, 1) * amp[705] - amp[706] + amp[708] - amp[712] + amp[710] + amp[728]
      - amp[726] + amp[733] + amp[732] + amp[735] + amp[736] - Complex<double>
      (0, 1) * amp[762] - amp[763] - Complex<double> (0, 1) * amp[765] -
      Complex<double> (0, 1) * amp[770] + Complex<double> (0, 1) * amp[768] -
      Complex<double> (0, 1) * amp[781] - Complex<double> (0, 1) * amp[782] +
      amp[783] - Complex<double> (0, 1) * amp[786] - Complex<double> (0, 1) *
      amp[787] - amp[792] - amp[793] + amp[1230] + amp[1231] + amp[1233] +
      amp[1232] - amp[1240] + amp[1244] - amp[1242] + amp[1247] + amp[1246] +
      amp[1251] + amp[1252] + Complex<double> (0, 1) * amp[1272] + amp[1275] -
      Complex<double> (0, 1) * amp[1277] + Complex<double> (0, 1) * amp[1280] -
      Complex<double> (0, 1) * amp[1278] + amp[1281] + Complex<double> (0, 1) *
      amp[1282] - Complex<double> (0, 1) * amp[1284] - Complex<double> (0, 1) *
      amp[1292] - Complex<double> (0, 1) * amp[1291] + amp[1343] - amp[1349] +
      amp[1347] - amp[1350] - amp[1351] - amp[1417] - amp[1428] -
      Complex<double> (0, 1) * amp[1429] + Complex<double> (0, 1) * amp[1433] -
      Complex<double> (0, 1) * amp[1436] + Complex<double> (0, 1) * amp[1434] -
      amp[1542] + amp[1547] + amp[1544] - amp[1545] + amp[1553] + amp[1552] -
      amp[1555] - amp[1556] + amp[1577] + amp[1576] - Complex<double> (0, 1) *
      amp[1580] - Complex<double> (0, 1) * amp[1579] + Complex<double> (0, 1) *
      amp[1588] + Complex<double> (0, 1) * amp[1589] - amp[1595] + amp[1601] -
      amp[1594] + amp[1600] + amp[1604] - amp[1602] + amp[1607] - amp[1605] +
      Complex<double> (0, 1) * amp[1625] - Complex<double> (0, 1) * amp[1623] -
      amp[1628] + amp[1626] - Complex<double> (0, 1) * amp[1634] +
      Complex<double> (0, 1) * amp[1632] - amp[1838] + amp[1839] + amp[1836] -
      amp[1841] + amp[1845] + amp[1846] + Complex<double> (0, 1) * amp[1851] +
      Complex<double> (0, 1) * amp[1852] - amp[1854] - amp[1855];
  jamp[74] = +Complex<double> (0, 1) * amp[536] + amp[539] - amp[537] -
      amp[540] + amp[542] - Complex<double> (0, 1) * amp[543] + amp[544] +
      amp[549] + amp[550] + amp[552] + amp[551] - amp[555] + amp[558] +
      amp[557] + amp[566] + amp[565] + amp[569] - amp[567] + Complex<double>
      (0, 1) * amp[579] + Complex<double> (0, 1) * amp[587] - Complex<double>
      (0, 1) * amp[585] + amp[588] + Complex<double> (0, 1) * amp[589] -
      Complex<double> (0, 1) * amp[612] - amp[616] + Complex<double> (0, 1) *
      amp[617] - Complex<double> (0, 1) * amp[620] - Complex<double> (0, 1) *
      amp[619] - Complex<double> (0, 1) * amp[673] + amp[679] + amp[678] +
      amp[681] + amp[682] - Complex<double> (0, 1) * amp[687] + amp[688] -
      amp[690] - Complex<double> (0, 1) * amp[691] + Complex<double> (0, 1) *
      amp[695] + amp[698] + amp[697] + amp[700] + amp[701] - amp[714] +
      amp[717] + amp[716] + amp[723] + amp[724] - Complex<double> (0, 1) *
      amp[773] - amp[775] + Complex<double> (0, 1) * amp[776] - Complex<double>
      (0, 1) * amp[777] - Complex<double> (0, 1) * amp[778] + Complex<double>
      (0, 1) * amp[788] + Complex<double> (0, 1) * amp[787] + amp[794] +
      amp[793] - amp[1074] - amp[1075] - amp[1077] - amp[1076] + amp[1085] -
      amp[1088] - amp[1087] - amp[1091] + amp[1089] - amp[1092] - amp[1093] -
      Complex<double> (0, 1) * amp[1104] - amp[1107] + Complex<double> (0, 1) *
      amp[1109] - Complex<double> (0, 1) * amp[1112] + Complex<double> (0, 1) *
      amp[1110] - amp[1113] - Complex<double> (0, 1) * amp[1114] +
      Complex<double> (0, 1) * amp[1137] + Complex<double> (0, 1) * amp[1145] +
      Complex<double> (0, 1) * amp[1144] + amp[1240] + amp[1242] + amp[1243] -
      amp[1251] - amp[1252] + Complex<double> (0, 1) * amp[1274] - amp[1275] +
      Complex<double> (0, 1) * amp[1277] + Complex<double> (0, 1) * amp[1278] +
      Complex<double> (0, 1) * amp[1279] - amp[1422] + amp[1487] + amp[1486] +
      amp[1489] + amp[1490] + amp[1494] + amp[1495] + amp[1497] + amp[1498] +
      Complex<double> (0, 1) * amp[1506] + Complex<double> (0, 1) * amp[1507] -
      amp[1509] - amp[1510] - Complex<double> (0, 1) * amp[1530] -
      Complex<double> (0, 1) * amp[1531] - Complex<double> (0, 1) * amp[1666] -
      Complex<double> (0, 1) * amp[1667] - amp[1672] - amp[1673] + amp[1677] +
      amp[1678] + amp[1681] + amp[1680] + amp[1685] + amp[1684] -
      Complex<double> (0, 1) * amp[1697] - Complex<double> (0, 1) * amp[1696] +
      amp[1700] + amp[1699] - amp[1839] + amp[1844] + amp[1841] - amp[1842] -
      amp[1847] - amp[1846] - Complex<double> (0, 1) * amp[1853] -
      Complex<double> (0, 1) * amp[1852] + amp[1856] + amp[1855];
  jamp[75] = -Complex<double> (0, 1) * amp[497] - amp[498] - Complex<double>
      (0, 1) * amp[499] - Complex<double> (0, 1) * amp[503] - Complex<double>
      (0, 1) * amp[502] + Complex<double> (0, 1) * amp[505] + amp[506] -
      Complex<double> (0, 1) * amp[536] - amp[539] + amp[537] + amp[540] -
      amp[542] + Complex<double> (0, 1) * amp[543] - amp[544] - amp[554] -
      amp[557] - amp[556] - amp[559] - amp[560] + amp[563] - amp[561] +
      amp[567] + amp[568] - amp[572] - amp[571] - Complex<double> (0, 1) *
      amp[584] + Complex<double> (0, 1) * amp[585] + Complex<double> (0, 1) *
      amp[586] - Complex<double> (0, 1) * amp[655] - Complex<double> (0, 1) *
      amp[656] - amp[657] - Complex<double> (0, 1) * amp[662] + Complex<double>
      (0, 1) * amp[660] - amp[670] + Complex<double> (0, 1) * amp[671] +
      amp[690] + Complex<double> (0, 1) * amp[691] - Complex<double> (0, 1) *
      amp[695] - amp[698] - amp[697] - amp[700] - amp[701] - amp[713] -
      amp[716] - amp[715] - amp[731] + amp[729] + amp[734] + amp[733] +
      amp[736] + amp[737] - Complex<double> (0, 1) * amp[782] - Complex<double>
      (0, 1) * amp[786] - Complex<double> (0, 1) * amp[787] - amp[792] -
      amp[793] - amp[1026] + Complex<double> (0, 1) * amp[1030] + amp[1031] -
      Complex<double> (0, 1) * amp[1052] - Complex<double> (0, 1) * amp[1058] -
      Complex<double> (0, 1) * amp[1057] - amp[1240] - amp[1242] - amp[1243] +
      amp[1251] + amp[1252] - Complex<double> (0, 1) * amp[1274] + amp[1275] -
      Complex<double> (0, 1) * amp[1277] - Complex<double> (0, 1) * amp[1278] -
      Complex<double> (0, 1) * amp[1279] - amp[1337] - amp[1338] + amp[1341] -
      amp[1339] + amp[1343] + amp[1347] + amp[1348] - amp[1350] - amp[1351] -
      amp[1355] - amp[1354] - amp[1428] - Complex<double> (0, 1) * amp[1431] +
      Complex<double> (0, 1) * amp[1433] + Complex<double> (0, 1) * amp[1434] +
      Complex<double> (0, 1) * amp[1435] + amp[1658] + amp[1657] -
      Complex<double> (0, 1) * amp[1661] - Complex<double> (0, 1) * amp[1660] +
      Complex<double> (0, 1) * amp[1666] + Complex<double> (0, 1) * amp[1667] +
      amp[1672] + amp[1673] - amp[1682] - amp[1678] - amp[1681] - amp[1679] -
      amp[1688] - amp[1687] - Complex<double> (0, 1) * amp[1715] +
      Complex<double> (0, 1) * amp[1713] - amp[1718] + amp[1716] + amp[1730] -
      amp[1736] - amp[1728] + amp[1734] + amp[1739] - amp[1737] - amp[1742] +
      amp[1740] + Complex<double> (0, 1) * amp[1745] - Complex<double> (0, 1) *
      amp[1743] - amp[1838] + amp[1839] + amp[1836] - amp[1841] + amp[1845] +
      amp[1846] + Complex<double> (0, 1) * amp[1851] + Complex<double> (0, 1) *
      amp[1852] - amp[1854] - amp[1855];
  jamp[76] = -amp[672] - Complex<double> (0, 1) * amp[673] + Complex<double>
      (0, 1) * amp[677] + amp[680] + amp[679] + amp[682] + amp[683] -
      Complex<double> (0, 1) * amp[691] + amp[697] + amp[696] + amp[699] +
      amp[700] - Complex<double> (0, 1) * amp[705] + amp[706] - amp[719] +
      amp[722] + amp[721] + amp[726] + amp[727] - Complex<double> (0, 1) *
      amp[764] - amp[766] + Complex<double> (0, 1) * amp[767] - Complex<double>
      (0, 1) * amp[768] - Complex<double> (0, 1) * amp[769] + Complex<double>
      (0, 1) * amp[788] + Complex<double> (0, 1) * amp[787] + amp[794] +
      amp[793] + Complex<double> (0, 1) * amp[836] + amp[839] - amp[837] -
      amp[840] + amp[842] - Complex<double> (0, 1) * amp[843] + amp[844] +
      amp[867] + amp[868] + amp[870] + amp[869] - amp[878] + amp[881] +
      amp[880] + amp[884] - amp[882] + amp[887] + amp[886] + Complex<double>
      (0, 1) * amp[909] + Complex<double> (0, 1) * amp[917] - Complex<double>
      (0, 1) * amp[915] + amp[918] + Complex<double> (0, 1) * amp[919] -
      Complex<double> (0, 1) * amp[921] - amp[925] + Complex<double> (0, 1) *
      amp[926] - Complex<double> (0, 1) * amp[929] - Complex<double> (0, 1) *
      amp[928] + amp[1085] + amp[1089] + amp[1090] - amp[1092] - amp[1093] +
      Complex<double> (0, 1) * amp[1106] - amp[1107] + Complex<double> (0, 1) *
      amp[1109] + Complex<double> (0, 1) * amp[1110] + Complex<double> (0, 1) *
      amp[1111] - amp[1230] - amp[1231] - amp[1233] - amp[1232] + amp[1240] -
      amp[1244] + amp[1242] - amp[1247] - amp[1246] - amp[1251] - amp[1252] -
      Complex<double> (0, 1) * amp[1272] - amp[1275] + Complex<double> (0, 1) *
      amp[1277] - Complex<double> (0, 1) * amp[1280] + Complex<double> (0, 1) *
      amp[1278] - amp[1281] - Complex<double> (0, 1) * amp[1282] +
      Complex<double> (0, 1) * amp[1284] + Complex<double> (0, 1) * amp[1292] +
      Complex<double> (0, 1) * amp[1291] - amp[1416] + amp[1595] + amp[1594] +
      amp[1597] + amp[1598] + amp[1602] + amp[1603] + amp[1605] + amp[1606] +
      Complex<double> (0, 1) * amp[1623] + Complex<double> (0, 1) * amp[1624] -
      amp[1626] - amp[1627] - Complex<double> (0, 1) * amp[1632] -
      Complex<double> (0, 1) * amp[1633] - Complex<double> (0, 1) * amp[1774] -
      Complex<double> (0, 1) * amp[1775] - amp[1780] - amp[1781] + amp[1785] +
      amp[1786] + amp[1789] + amp[1788] + amp[1793] + amp[1792] -
      Complex<double> (0, 1) * amp[1805] - Complex<double> (0, 1) * amp[1804] +
      amp[1808] + amp[1807] - amp[1839] + amp[1844] + amp[1841] - amp[1842] -
      amp[1847] - amp[1846] - Complex<double> (0, 1) * amp[1853] -
      Complex<double> (0, 1) * amp[1852] + amp[1856] + amp[1855];
  jamp[77] = -Complex<double> (0, 1) * amp[654] - Complex<double> (0, 1) *
      amp[658] - amp[659] - Complex<double> (0, 1) * amp[660] - Complex<double>
      (0, 1) * amp[661] + amp[670] - Complex<double> (0, 1) * amp[671] +
      amp[672] + Complex<double> (0, 1) * amp[673] - Complex<double> (0, 1) *
      amp[677] - amp[680] - amp[679] - amp[682] - amp[683] - amp[718] -
      amp[721] - amp[720] - amp[729] - amp[730] - amp[734] - amp[733] -
      amp[736] - amp[737] + Complex<double> (0, 1) * amp[782] - Complex<double>
      (0, 1) * amp[788] + Complex<double> (0, 1) * amp[786] - amp[794] +
      amp[792] - Complex<double> (0, 1) * amp[815] - Complex<double> (0, 1) *
      amp[817] - amp[818] - Complex<double> (0, 1) * amp[821] - Complex<double>
      (0, 1) * amp[820] + Complex<double> (0, 1) * amp[826] + amp[827] -
      Complex<double> (0, 1) * amp[836] - amp[839] + amp[837] + amp[840] -
      amp[842] + Complex<double> (0, 1) * amp[843] - amp[844] - amp[872] -
      amp[873] + amp[876] - amp[874] - amp[877] - amp[880] - amp[879] +
      amp[882] + amp[883] - amp[890] - amp[889] - Complex<double> (0, 1) *
      amp[914] + Complex<double> (0, 1) * amp[915] + Complex<double> (0, 1) *
      amp[916] - amp[1025] - Complex<double> (0, 1) * amp[1030] - amp[1031] +
      Complex<double> (0, 1) * amp[1052] + Complex<double> (0, 1) * amp[1058] +
      Complex<double> (0, 1) * amp[1057] - amp[1085] - amp[1089] - amp[1090] +
      amp[1092] + amp[1093] - Complex<double> (0, 1) * amp[1106] + amp[1107] -
      Complex<double> (0, 1) * amp[1109] - Complex<double> (0, 1) * amp[1110] -
      Complex<double> (0, 1) * amp[1111] + amp[1337] + amp[1338] - amp[1341] +
      amp[1339] - amp[1343] - amp[1347] - amp[1348] + amp[1350] + amp[1351] +
      amp[1355] + amp[1354] + amp[1428] + Complex<double> (0, 1) * amp[1431] -
      Complex<double> (0, 1) * amp[1433] - Complex<double> (0, 1) * amp[1434] -
      Complex<double> (0, 1) * amp[1435] - Complex<double> (0, 1) * amp[1713] -
      Complex<double> (0, 1) * amp[1714] - amp[1716] - amp[1717] - amp[1730] +
      amp[1731] + amp[1728] - amp[1733] + amp[1737] + amp[1738] - amp[1740] -
      amp[1741] + Complex<double> (0, 1) * amp[1743] + Complex<double> (0, 1) *
      amp[1744] + amp[1766] + amp[1765] - Complex<double> (0, 1) * amp[1769] -
      Complex<double> (0, 1) * amp[1768] + Complex<double> (0, 1) * amp[1774] +
      Complex<double> (0, 1) * amp[1775] + amp[1780] + amp[1781] - amp[1790] -
      amp[1786] - amp[1789] - amp[1787] - amp[1796] - amp[1795] + amp[1838] -
      amp[1844] - amp[1836] + amp[1842] + amp[1847] - amp[1845] +
      Complex<double> (0, 1) * amp[1853] - Complex<double> (0, 1) * amp[1851] -
      amp[1856] + amp[1854];
  jamp[78] = -amp[0] - amp[1] - amp[3] - amp[2] - amp[6] - amp[9] - amp[8] -
      amp[17] - amp[16] - amp[20] + amp[18] + amp[32] + amp[31] + amp[34] +
      amp[35] - Complex<double> (0, 1) * amp[65] - Complex<double> (0, 1) *
      amp[71] + Complex<double> (0, 1) * amp[69] - Complex<double> (0, 1) *
      amp[75] - amp[77] - amp[78] + Complex<double> (0, 1) * amp[80] +
      Complex<double> (0, 1) * amp[125] + Complex<double> (0, 1) * amp[127] +
      amp[128] + Complex<double> (0, 1) * amp[131] + Complex<double> (0, 1) *
      amp[130] - Complex<double> (0, 1) * amp[146] + Complex<double> (0, 1) *
      amp[517] - amp[521] + amp[519] + amp[522] - amp[524] - Complex<double>
      (0, 1) * amp[528] - amp[530] - amp[549] - amp[550] - amp[552] - amp[551]
      + amp[560] - amp[563] - amp[562] - amp[566] + amp[564] - amp[569] -
      amp[568] - Complex<double> (0, 1) * amp[579] + amp[583] + Complex<double>
      (0, 1) * amp[584] - Complex<double> (0, 1) * amp[587] - Complex<double>
      (0, 1) * amp[586] - amp[588] + Complex<double> (0, 1) * amp[590] +
      Complex<double> (0, 1) * amp[612] + Complex<double> (0, 1) * amp[620] -
      Complex<double> (0, 1) * amp[618] - amp[1153] - amp[1156] - amp[1155] -
      amp[1166] - amp[1165] - amp[1178] + amp[1176] + amp[1179] - amp[1181] +
      Complex<double> (0, 1) * amp[1215] + amp[1216] - Complex<double> (0, 1) *
      amp[1219] + Complex<double> (0, 1) * amp[1223] + Complex<double> (0, 1) *
      amp[1222] - Complex<double> (0, 1) * amp[1225] + Complex<double> (0, 1) *
      amp[1226] - amp[1228] + amp[1338] - amp[1341] - amp[1340] - amp[1349] -
      amp[1348] - amp[1426] + amp[1430] + Complex<double> (0, 1) * amp[1431] +
      Complex<double> (0, 1) * amp[1432] - Complex<double> (0, 1) * amp[1436] -
      Complex<double> (0, 1) * amp[1435] - amp[1493] - amp[1489] - amp[1492] -
      amp[1490] - amp[1496] - amp[1495] - amp[1499] - amp[1498] -
      Complex<double> (0, 1) * amp[1508] - Complex<double> (0, 1) * amp[1507] +
      amp[1511] + amp[1510] + Complex<double> (0, 1) * amp[1532] +
      Complex<double> (0, 1) * amp[1531] + amp[1540] + amp[1539] - amp[1546] -
      amp[1545] - amp[1550] + amp[1548] + amp[1554] - amp[1556] - amp[1583] +
      amp[1581] + Complex<double> (0, 1) * amp[1586] - Complex<double> (0, 1) *
      amp[1584] - Complex<double> (0, 1) * amp[1587] + Complex<double> (0, 1) *
      amp[1589] + Complex<double> (0, 1) * amp[1722] - Complex<double> (0, 1) *
      amp[1724] - amp[1725] + amp[1727] + amp[1729] + amp[1728] - amp[1735] -
      amp[1734] - amp[1739] + amp[1737] - Complex<double> (0, 1) * amp[1745] +
      Complex<double> (0, 1) * amp[1743] + amp[1746] - amp[1748];
  jamp[79] = -amp[5] + amp[9] - amp[7] - amp[10] - amp[11] - amp[13] - amp[12]
      - amp[18] - amp[19] - amp[23] - amp[22] - amp[32] - amp[31] - amp[34] -
      amp[35] - Complex<double> (0, 1) * amp[67] - Complex<double> (0, 1) *
      amp[69] - Complex<double> (0, 1) * amp[70] - Complex<double> (0, 1) *
      amp[72] - amp[74] + amp[78] - Complex<double> (0, 1) * amp[80] +
      Complex<double> (0, 1) * amp[101] + amp[102] + Complex<double> (0, 1) *
      amp[103] + Complex<double> (0, 1) * amp[107] + Complex<double> (0, 1) *
      amp[106] + Complex<double> (0, 1) * amp[146] + Complex<double> (0, 1) *
      amp[853] - amp[857] + amp[855] + amp[858] - amp[860] - Complex<double>
      (0, 1) * amp[864] - amp[866] - amp[867] - amp[868] - amp[870] - amp[869]
      + amp[873] - amp[876] - amp[875] - amp[884] - amp[883] - amp[887] +
      amp[885] - Complex<double> (0, 1) * amp[909] + amp[913] + Complex<double>
      (0, 1) * amp[914] - Complex<double> (0, 1) * amp[917] - Complex<double>
      (0, 1) * amp[916] - amp[918] + Complex<double> (0, 1) * amp[920] +
      Complex<double> (0, 1) * amp[921] + Complex<double> (0, 1) * amp[929] -
      Complex<double> (0, 1) * amp[927] - amp[1152] + amp[1156] - amp[1154] -
      amp[1169] - amp[1168] + amp[1178] - amp[1176] - amp[1179] + amp[1181] +
      Complex<double> (0, 1) * amp[1206] + amp[1207] - Complex<double> (0, 1) *
      amp[1210] + Complex<double> (0, 1) * amp[1214] + Complex<double> (0, 1) *
      amp[1213] + Complex<double> (0, 1) * amp[1225] - Complex<double> (0, 1) *
      amp[1226] + amp[1228] - amp[1338] + amp[1341] + amp[1340] + amp[1349] +
      amp[1348] - amp[1420] - amp[1430] - Complex<double> (0, 1) * amp[1431] -
      Complex<double> (0, 1) * amp[1432] + Complex<double> (0, 1) * amp[1436] +
      Complex<double> (0, 1) * amp[1435] + amp[1541] - amp[1547] - amp[1539] +
      amp[1545] - amp[1553] + amp[1551] - amp[1554] + amp[1556] - amp[1577] +
      amp[1575] + Complex<double> (0, 1) * amp[1580] - Complex<double> (0, 1) *
      amp[1578] + Complex<double> (0, 1) * amp[1587] - Complex<double> (0, 1) *
      amp[1589] - amp[1601] - amp[1597] - amp[1600] - amp[1598] - amp[1604] -
      amp[1603] - amp[1607] - amp[1606] - Complex<double> (0, 1) * amp[1625] -
      Complex<double> (0, 1) * amp[1624] + amp[1628] + amp[1627] +
      Complex<double> (0, 1) * amp[1634] + Complex<double> (0, 1) * amp[1633] -
      Complex<double> (0, 1) * amp[1722] - Complex<double> (0, 1) * amp[1723] +
      amp[1725] + amp[1726] - amp[1731] - amp[1729] - amp[1732] - amp[1728] -
      amp[1737] - amp[1738] - Complex<double> (0, 1) * amp[1743] -
      Complex<double> (0, 1) * amp[1744] - amp[1746] - amp[1747];
  jamp[80] = -Complex<double> (0, 1) * amp[517] + amp[521] - amp[519] -
      amp[522] + amp[524] + Complex<double> (0, 1) * amp[528] + amp[530] +
      amp[549] + amp[550] + amp[552] + amp[551] - amp[560] + amp[563] +
      amp[562] + amp[566] - amp[564] + amp[569] + amp[568] + Complex<double>
      (0, 1) * amp[579] - amp[583] - Complex<double> (0, 1) * amp[584] +
      Complex<double> (0, 1) * amp[587] + Complex<double> (0, 1) * amp[586] +
      amp[588] - Complex<double> (0, 1) * amp[590] - Complex<double> (0, 1) *
      amp[612] - Complex<double> (0, 1) * amp[620] + Complex<double> (0, 1) *
      amp[618] + Complex<double> (0, 1) * amp[834] + amp[838] + amp[837] +
      amp[840] + amp[841] + Complex<double> (0, 1) * amp[843] + amp[845] -
      amp[851] + Complex<double> (0, 1) * amp[852] - Complex<double> (0, 1) *
      amp[853] + amp[857] + amp[856] + amp[859] + amp[860] - amp[873] +
      amp[876] + amp[875] + amp[882] + amp[883] + Complex<double> (0, 1) *
      amp[911] - amp[913] - Complex<double> (0, 1) * amp[914] + Complex<double>
      (0, 1) * amp[915] + Complex<double> (0, 1) * amp[916] - Complex<double>
      (0, 1) * amp[950] - Complex<double> (0, 1) * amp[949] + amp[953] +
      amp[952] - amp[1074] - amp[1075] - amp[1077] - amp[1076] + amp[1084] -
      amp[1088] + amp[1086] - amp[1091] - amp[1090] - amp[1095] - amp[1096] -
      Complex<double> (0, 1) * amp[1104] - Complex<double> (0, 1) * amp[1112] -
      Complex<double> (0, 1) * amp[1111] - amp[1113] + Complex<double> (0, 1) *
      amp[1115] + Complex<double> (0, 1) * amp[1137] - amp[1140] -
      Complex<double> (0, 1) * amp[1142] + Complex<double> (0, 1) * amp[1145] -
      Complex<double> (0, 1) * amp[1143] + amp[1162] + amp[1164] + amp[1165] -
      amp[1173] - amp[1174] - Complex<double> (0, 1) * amp[1217] - amp[1218] -
      Complex<double> (0, 1) * amp[1220] - Complex<double> (0, 1) * amp[1221] -
      Complex<double> (0, 1) * amp[1222] - amp[1424] + amp[1487] + amp[1486] +
      amp[1489] + amp[1490] + amp[1494] + amp[1495] + amp[1497] + amp[1498] +
      Complex<double> (0, 1) * amp[1506] + Complex<double> (0, 1) * amp[1507] -
      amp[1509] - amp[1510] - Complex<double> (0, 1) * amp[1530] -
      Complex<double> (0, 1) * amp[1531] + Complex<double> (0, 1) * amp[1723] +
      Complex<double> (0, 1) * amp[1724] - amp[1726] - amp[1727] + amp[1731] +
      amp[1732] + amp[1735] + amp[1734] + amp[1739] + amp[1738] +
      Complex<double> (0, 1) * amp[1745] + Complex<double> (0, 1) * amp[1744] +
      amp[1747] + amp[1748] - amp[1812] + amp[1817] + amp[1814] - amp[1815] -
      amp[1820] - amp[1819] + Complex<double> (0, 1) * amp[1832] +
      Complex<double> (0, 1) * amp[1831] + amp[1835] + amp[1834];
  jamp[81] = +Complex<double> (0, 1) * amp[336] + Complex<double> (0, 1) *
      amp[337] + Complex<double> (0, 1) * amp[339] + Complex<double> (0, 1) *
      amp[338] - amp[344] + amp[346] + Complex<double> (0, 1) * amp[347] +
      Complex<double> (0, 1) * amp[354] - Complex<double> (0, 1) * amp[358] +
      Complex<double> (0, 1) * amp[356] - Complex<double> (0, 1) * amp[361] -
      amp[362] + Complex<double> (0, 1) * amp[370] - amp[371] - amp[403] +
      amp[406] + Complex<double> (0, 1) * amp[407] + Complex<double> (0, 1) *
      amp[428] + Complex<double> (0, 1) * amp[431] + Complex<double> (0, 1) *
      amp[430] - Complex<double> (0, 1) * amp[445] - Complex<double> (0, 1) *
      amp[446] + amp[449] + amp[448] + Complex<double> (0, 1) * amp[453] -
      Complex<double> (0, 1) * amp[455] - amp[461] + amp[459] - Complex<double>
      (0, 1) * amp[834] - amp[838] - amp[837] - amp[840] - amp[841] -
      Complex<double> (0, 1) * amp[843] - amp[845] + amp[851] - Complex<double>
      (0, 1) * amp[852] + Complex<double> (0, 1) * amp[853] - amp[857] -
      amp[856] - amp[859] - amp[860] + amp[873] - amp[876] - amp[875] -
      amp[882] - amp[883] - Complex<double> (0, 1) * amp[911] + amp[913] +
      Complex<double> (0, 1) * amp[914] - Complex<double> (0, 1) * amp[915] -
      Complex<double> (0, 1) * amp[916] + Complex<double> (0, 1) * amp[950] +
      Complex<double> (0, 1) * amp[949] - amp[953] - amp[952] + amp[1079] +
      amp[1080] + amp[1082] + amp[1081] + amp[1085] + amp[1089] + amp[1090] +
      amp[1094] - amp[1092] + amp[1097] + amp[1096] + Complex<double> (0, 1) *
      amp[1109] + Complex<double> (0, 1) * amp[1110] + Complex<double> (0, 1) *
      amp[1111] + amp[1157] - amp[1161] + amp[1159] + amp[1175] + amp[1174] +
      amp[1178] + amp[1177] + amp[1180] + amp[1181] - Complex<double> (0, 1) *
      amp[1226] + amp[1332] + amp[1335] + amp[1334] - amp[1338] + amp[1341] +
      amp[1340] + amp[1343] + amp[1347] + amp[1348] + amp[1352] - amp[1350] -
      amp[1430] - Complex<double> (0, 1) * amp[1431] + Complex<double> (0, 1) *
      amp[1433] + Complex<double> (0, 1) * amp[1434] + Complex<double> (0, 1) *
      amp[1435] - Complex<double> (0, 1) * amp[1722] - Complex<double> (0, 1) *
      amp[1723] + amp[1725] + amp[1726] - amp[1731] - amp[1729] - amp[1732] -
      amp[1728] - amp[1737] - amp[1738] - Complex<double> (0, 1) * amp[1743] -
      Complex<double> (0, 1) * amp[1744] - amp[1746] - amp[1747] - amp[1817] -
      amp[1813] - amp[1816] - amp[1814] - amp[1822] - amp[1823] + amp[1837] +
      amp[1836] - amp[1843] - amp[1842] - amp[1847] + amp[1845] + amp[1848] -
      amp[1850] - Complex<double> (0, 1) * amp[1853] + Complex<double> (0, 1) *
      amp[1851];
  jamp[82] = -amp[515] + Complex<double> (0, 1) * amp[516] - Complex<double>
      (0, 1) * amp[517] + amp[521] + amp[520] + amp[523] + amp[524] +
      Complex<double> (0, 1) * amp[534] + amp[538] + amp[537] + amp[540] +
      amp[541] + Complex<double> (0, 1) * amp[543] + amp[545] - amp[560] +
      amp[563] + amp[562] + amp[567] + amp[568] + Complex<double> (0, 1) *
      amp[581] - amp[583] - Complex<double> (0, 1) * amp[584] + Complex<double>
      (0, 1) * amp[585] + Complex<double> (0, 1) * amp[586] - Complex<double>
      (0, 1) * amp[632] - Complex<double> (0, 1) * amp[631] + amp[635] +
      amp[634] - Complex<double> (0, 1) * amp[853] + amp[857] - amp[855] -
      amp[858] + amp[860] + Complex<double> (0, 1) * amp[864] + amp[866] +
      amp[867] + amp[868] + amp[870] + amp[869] - amp[873] + amp[876] +
      amp[875] + amp[884] + amp[883] + amp[887] - amp[885] + Complex<double>
      (0, 1) * amp[909] - amp[913] - Complex<double> (0, 1) * amp[914] +
      Complex<double> (0, 1) * amp[917] + Complex<double> (0, 1) * amp[916] +
      amp[918] - Complex<double> (0, 1) * amp[920] - Complex<double> (0, 1) *
      amp[921] - Complex<double> (0, 1) * amp[929] + Complex<double> (0, 1) *
      amp[927] + amp[1163] + amp[1167] + amp[1168] - amp[1170] - amp[1171] -
      Complex<double> (0, 1) * amp[1208] - amp[1209] - Complex<double> (0, 1) *
      amp[1211] - Complex<double> (0, 1) * amp[1212] - Complex<double> (0, 1) *
      amp[1213] - amp[1230] - amp[1231] - amp[1233] - amp[1232] + amp[1241] -
      amp[1244] - amp[1243] - amp[1247] + amp[1245] - amp[1248] - amp[1249] -
      Complex<double> (0, 1) * amp[1272] - Complex<double> (0, 1) * amp[1280] -
      Complex<double> (0, 1) * amp[1279] - amp[1281] + Complex<double> (0, 1) *
      amp[1283] + Complex<double> (0, 1) * amp[1284] - amp[1287] -
      Complex<double> (0, 1) * amp[1289] + Complex<double> (0, 1) * amp[1292] -
      Complex<double> (0, 1) * amp[1290] - amp[1418] + amp[1595] + amp[1594] +
      amp[1597] + amp[1598] + amp[1602] + amp[1603] + amp[1605] + amp[1606] +
      Complex<double> (0, 1) * amp[1623] + Complex<double> (0, 1) * amp[1624] -
      amp[1626] - amp[1627] - Complex<double> (0, 1) * amp[1632] -
      Complex<double> (0, 1) * amp[1633] + Complex<double> (0, 1) * amp[1723] +
      Complex<double> (0, 1) * amp[1724] - amp[1726] - amp[1727] + amp[1731] +
      amp[1732] + amp[1735] + amp[1734] + amp[1739] + amp[1738] +
      Complex<double> (0, 1) * amp[1745] + Complex<double> (0, 1) * amp[1744] +
      amp[1747] + amp[1748] - amp[1866] + amp[1871] + amp[1868] - amp[1869] -
      amp[1874] - amp[1873] + Complex<double> (0, 1) * amp[1886] +
      Complex<double> (0, 1) * amp[1885] + amp[1889] + amp[1888];
  jamp[83] = +Complex<double> (0, 1) * amp[355] + Complex<double> (0, 1) *
      amp[358] + Complex<double> (0, 1) * amp[357] - Complex<double> (0, 1) *
      amp[359] - amp[360] - Complex<double> (0, 1) * amp[370] + amp[371] +
      Complex<double> (0, 1) * amp[372] + Complex<double> (0, 1) * amp[373] +
      Complex<double> (0, 1) * amp[375] + Complex<double> (0, 1) * amp[374] -
      amp[378] + amp[385] + Complex<double> (0, 1) * amp[386] - amp[401] -
      amp[406] - Complex<double> (0, 1) * amp[407] - Complex<double> (0, 1) *
      amp[428] - Complex<double> (0, 1) * amp[431] - Complex<double> (0, 1) *
      amp[430] - Complex<double> (0, 1) * amp[453] - Complex<double> (0, 1) *
      amp[454] - amp[459] - amp[460] - Complex<double> (0, 1) * amp[463] -
      Complex<double> (0, 1) * amp[464] + amp[467] + amp[466] + amp[515] -
      Complex<double> (0, 1) * amp[516] + Complex<double> (0, 1) * amp[517] -
      amp[521] - amp[520] - amp[523] - amp[524] - Complex<double> (0, 1) *
      amp[534] - amp[538] - amp[537] - amp[540] - amp[541] - Complex<double>
      (0, 1) * amp[543] - amp[545] + amp[560] - amp[563] - amp[562] - amp[567]
      - amp[568] - Complex<double> (0, 1) * amp[581] + amp[583] +
      Complex<double> (0, 1) * amp[584] - Complex<double> (0, 1) * amp[585] -
      Complex<double> (0, 1) * amp[586] + Complex<double> (0, 1) * amp[632] +
      Complex<double> (0, 1) * amp[631] - amp[635] - amp[634] + amp[1158] +
      amp[1161] + amp[1160] + amp[1172] + amp[1171] - amp[1178] - amp[1177] -
      amp[1180] - amp[1181] + Complex<double> (0, 1) * amp[1226] + amp[1235] +
      amp[1236] + amp[1238] + amp[1237] + amp[1240] + amp[1242] + amp[1243] +
      amp[1250] + amp[1249] + amp[1253] - amp[1251] + Complex<double> (0, 1) *
      amp[1277] + Complex<double> (0, 1) * amp[1278] + Complex<double> (0, 1) *
      amp[1279] - amp[1332] - amp[1335] - amp[1334] + amp[1338] - amp[1341] -
      amp[1340] - amp[1343] - amp[1347] - amp[1348] - amp[1352] + amp[1350] +
      amp[1430] + Complex<double> (0, 1) * amp[1431] - Complex<double> (0, 1) *
      amp[1433] - Complex<double> (0, 1) * amp[1434] - Complex<double> (0, 1) *
      amp[1435] + Complex<double> (0, 1) * amp[1722] - Complex<double> (0, 1) *
      amp[1724] - amp[1725] + amp[1727] + amp[1729] + amp[1728] - amp[1735] -
      amp[1734] - amp[1739] + amp[1737] - Complex<double> (0, 1) * amp[1745] +
      Complex<double> (0, 1) * amp[1743] + amp[1746] - amp[1748] - amp[1839] -
      amp[1837] - amp[1840] - amp[1836] - amp[1845] - amp[1846] - amp[1848] -
      amp[1849] - Complex<double> (0, 1) * amp[1851] - Complex<double> (0, 1) *
      amp[1852] - amp[1871] - amp[1867] - amp[1870] - amp[1868] - amp[1876] -
      amp[1877];
  jamp[84] = -amp[0] - amp[1] - amp[3] - amp[2] - amp[11] - amp[14] - amp[13] -
      amp[17] + amp[15] - amp[20] - amp[19] + amp[26] + amp[25] + amp[28] +
      amp[29] - Complex<double> (0, 1) * amp[65] - Complex<double> (0, 1) *
      amp[67] + amp[68] - Complex<double> (0, 1) * amp[71] - Complex<double>
      (0, 1) * amp[70] + Complex<double> (0, 1) * amp[76] - amp[77] +
      Complex<double> (0, 1) * amp[125] + Complex<double> (0, 1) * amp[131] -
      Complex<double> (0, 1) * amp[129] - amp[133] - Complex<double> (0, 1) *
      amp[134] + Complex<double> (0, 1) * amp[139] - Complex<double> (0, 1) *
      amp[536] - amp[539] + amp[537] + amp[540] - amp[542] + Complex<double>
      (0, 1) * amp[543] - amp[544] - amp[549] - amp[550] - amp[552] - amp[551]
      + amp[555] - amp[558] - amp[557] - amp[566] - amp[565] - amp[569] +
      amp[567] - Complex<double> (0, 1) * amp[579] - Complex<double> (0, 1) *
      amp[587] + Complex<double> (0, 1) * amp[585] - amp[588] - Complex<double>
      (0, 1) * amp[589] + Complex<double> (0, 1) * amp[612] + amp[616] -
      Complex<double> (0, 1) * amp[617] + Complex<double> (0, 1) * amp[620] +
      Complex<double> (0, 1) * amp[619] - amp[1231] - amp[1234] - amp[1233] -
      amp[1244] - amp[1243] - amp[1256] + amp[1254] + amp[1257] - amp[1259] -
      Complex<double> (0, 1) * amp[1272] + amp[1273] + Complex<double> (0, 1) *
      amp[1276] - Complex<double> (0, 1) * amp[1280] - Complex<double> (0, 1) *
      amp[1279] + Complex<double> (0, 1) * amp[1302] - Complex<double> (0, 1) *
      amp[1305] - amp[1307] + amp[1314] - amp[1317] - amp[1316] - amp[1325] -
      amp[1324] - amp[1427] + amp[1478] - Complex<double> (0, 1) * amp[1479] -
      Complex<double> (0, 1) * amp[1480] + Complex<double> (0, 1) * amp[1484] +
      Complex<double> (0, 1) * amp[1483] - amp[1493] - amp[1489] - amp[1492] -
      amp[1490] - amp[1496] - amp[1495] - amp[1499] - amp[1498] -
      Complex<double> (0, 1) * amp[1508] - Complex<double> (0, 1) * amp[1507] +
      amp[1511] + amp[1510] + Complex<double> (0, 1) * amp[1532] +
      Complex<double> (0, 1) * amp[1531] + amp[1594] + amp[1593] - amp[1600] -
      amp[1599] - amp[1604] + amp[1602] + amp[1608] - amp[1610] + amp[1620] -
      amp[1622] - Complex<double> (0, 1) * amp[1625] + Complex<double> (0, 1) *
      amp[1623] + Complex<double> (0, 1) * amp[1644] - Complex<double> (0, 1) *
      amp[1646] - Complex<double> (0, 1) * amp[1665] + Complex<double> (0, 1) *
      amp[1667] - amp[1671] + amp[1673] + amp[1675] + amp[1674] - amp[1681] -
      amp[1680] - amp[1685] + amp[1683] + Complex<double> (0, 1) * amp[1697] -
      Complex<double> (0, 1) * amp[1695] - amp[1700] + amp[1698];
  jamp[85] = +Complex<double> (0, 1) * amp[497] + amp[498] + Complex<double>
      (0, 1) * amp[499] + Complex<double> (0, 1) * amp[503] + Complex<double>
      (0, 1) * amp[502] - Complex<double> (0, 1) * amp[505] - amp[506] +
      Complex<double> (0, 1) * amp[536] + amp[539] - amp[537] - amp[540] +
      amp[542] - Complex<double> (0, 1) * amp[543] + amp[544] + amp[554] +
      amp[557] + amp[556] + amp[559] + amp[560] - amp[563] + amp[561] -
      amp[567] - amp[568] + amp[572] + amp[571] + Complex<double> (0, 1) *
      amp[584] - Complex<double> (0, 1) * amp[585] - Complex<double> (0, 1) *
      amp[586] + Complex<double> (0, 1) * amp[815] + Complex<double> (0, 1) *
      amp[821] - Complex<double> (0, 1) * amp[819] + Complex<double> (0, 1) *
      amp[825] - amp[827] - amp[828] - Complex<double> (0, 1) * amp[830] -
      amp[868] - amp[871] - amp[870] + amp[872] + amp[873] - amp[876] +
      amp[874] - amp[884] - amp[883] + amp[890] - amp[888] - amp[893] +
      amp[891] + amp[894] - amp[896] - Complex<double> (0, 1) * amp[909] +
      amp[910] + Complex<double> (0, 1) * amp[914] - Complex<double> (0, 1) *
      amp[917] - Complex<double> (0, 1) * amp[916] + Complex<double> (0, 1) *
      amp[939] - amp[1028] + Complex<double> (0, 1) * amp[1043] + amp[1044] -
      Complex<double> (0, 1) * amp[1046] + Complex<double> (0, 1) * amp[1049] +
      Complex<double> (0, 1) * amp[1048] + amp[1231] + amp[1234] + amp[1233] +
      amp[1244] + amp[1243] + amp[1256] - amp[1254] - amp[1257] + amp[1259] +
      Complex<double> (0, 1) * amp[1272] - amp[1273] - Complex<double> (0, 1) *
      amp[1276] + Complex<double> (0, 1) * amp[1280] + Complex<double> (0, 1) *
      amp[1279] - Complex<double> (0, 1) * amp[1302] + Complex<double> (0, 1) *
      amp[1305] + amp[1307] + amp[1313] + amp[1316] + amp[1315] + amp[1331] +
      amp[1330] - amp[1596] - amp[1594] - amp[1597] - amp[1593] - amp[1602] -
      amp[1603] - amp[1608] - amp[1609] - amp[1620] - amp[1621] -
      Complex<double> (0, 1) * amp[1623] - Complex<double> (0, 1) * amp[1624] -
      Complex<double> (0, 1) * amp[1644] - Complex<double> (0, 1) * amp[1645] -
      amp[1658] + amp[1656] + Complex<double> (0, 1) * amp[1661] -
      Complex<double> (0, 1) * amp[1659] + Complex<double> (0, 1) * amp[1665] -
      Complex<double> (0, 1) * amp[1667] + amp[1671] - amp[1673] - amp[1676] +
      amp[1682] - amp[1675] + amp[1681] + amp[1688] - amp[1686] +
      Complex<double> (0, 1) * amp[1715] + Complex<double> (0, 1) * amp[1714] +
      amp[1718] + amp[1717] - amp[1731] + amp[1736] + amp[1733] - amp[1734] -
      amp[1739] - amp[1738] + amp[1742] + amp[1741] - Complex<double> (0, 1) *
      amp[1745] - Complex<double> (0, 1) * amp[1744];
  jamp[86] = +amp[0] + amp[1] + amp[3] + amp[2] + amp[11] + amp[14] + amp[13] +
      amp[17] - amp[15] + amp[20] + amp[19] - amp[26] - amp[25] - amp[28] -
      amp[29] + Complex<double> (0, 1) * amp[65] + Complex<double> (0, 1) *
      amp[67] - amp[68] + Complex<double> (0, 1) * amp[71] + Complex<double>
      (0, 1) * amp[70] - Complex<double> (0, 1) * amp[76] + amp[77] -
      Complex<double> (0, 1) * amp[125] - Complex<double> (0, 1) * amp[131] +
      Complex<double> (0, 1) * amp[129] + amp[133] + Complex<double> (0, 1) *
      amp[134] - Complex<double> (0, 1) * amp[139] - Complex<double> (0, 1) *
      amp[834] - amp[838] - amp[837] - amp[840] - amp[841] - Complex<double>
      (0, 1) * amp[843] - amp[845] + amp[868] + amp[871] + amp[870] + amp[884]
      - amp[882] - amp[892] - amp[891] - amp[894] - amp[895] + Complex<double>
      (0, 1) * amp[909] - amp[910] + Complex<double> (0, 1) * amp[912] +
      Complex<double> (0, 1) * amp[917] - Complex<double> (0, 1) * amp[915] -
      Complex<double> (0, 1) * amp[939] - Complex<double> (0, 1) * amp[943] -
      amp[944] + Complex<double> (0, 1) * amp[950] - Complex<double> (0, 1) *
      amp[948] - amp[953] + amp[951] + amp[1074] + amp[1075] + amp[1077] +
      amp[1076] - amp[1084] + amp[1088] - amp[1086] + amp[1091] + amp[1090] +
      amp[1095] + amp[1096] + Complex<double> (0, 1) * amp[1104] +
      Complex<double> (0, 1) * amp[1112] + Complex<double> (0, 1) * amp[1111] +
      amp[1113] - Complex<double> (0, 1) * amp[1115] - Complex<double> (0, 1) *
      amp[1137] + amp[1140] + Complex<double> (0, 1) * amp[1142] -
      Complex<double> (0, 1) * amp[1145] + Complex<double> (0, 1) * amp[1143] -
      amp[1319] + amp[1325] - amp[1323] + amp[1326] + amp[1327] - amp[1425] +
      amp[1476] - Complex<double> (0, 1) * amp[1477] + Complex<double> (0, 1) *
      amp[1481] - Complex<double> (0, 1) * amp[1484] + Complex<double> (0, 1) *
      amp[1482] - amp[1487] + amp[1493] - amp[1486] + amp[1492] + amp[1496] -
      amp[1494] + amp[1499] - amp[1497] + Complex<double> (0, 1) * amp[1508] -
      Complex<double> (0, 1) * amp[1506] - amp[1511] + amp[1509] -
      Complex<double> (0, 1) * amp[1532] + Complex<double> (0, 1) * amp[1530] +
      amp[1596] + amp[1597] + amp[1600] + amp[1599] + amp[1604] + amp[1603] +
      amp[1609] + amp[1610] + amp[1621] + amp[1622] + Complex<double> (0, 1) *
      amp[1625] + Complex<double> (0, 1) * amp[1624] + Complex<double> (0, 1) *
      amp[1645] + Complex<double> (0, 1) * amp[1646] + amp[1811] - amp[1817] -
      amp[1809] + amp[1815] + amp[1820] - amp[1818] - Complex<double> (0, 1) *
      amp[1832] + Complex<double> (0, 1) * amp[1830] - amp[1835] + amp[1833];
  jamp[87] = -Complex<double> (0, 1) * amp[336] - Complex<double> (0, 1) *
      amp[337] - Complex<double> (0, 1) * amp[339] - Complex<double> (0, 1) *
      amp[338] + amp[344] - amp[346] - Complex<double> (0, 1) * amp[347] -
      Complex<double> (0, 1) * amp[372] + Complex<double> (0, 1) * amp[376] -
      Complex<double> (0, 1) * amp[374] + Complex<double> (0, 1) * amp[384] -
      amp[385] - amp[387] - Complex<double> (0, 1) * amp[388] - amp[404] -
      Complex<double> (0, 1) * amp[419] - Complex<double> (0, 1) * amp[422] -
      Complex<double> (0, 1) * amp[421] + amp[424] - Complex<double> (0, 1) *
      amp[425] - Complex<double> (0, 1) * amp[444] + Complex<double> (0, 1) *
      amp[446] - amp[449] + amp[447] + Complex<double> (0, 1) * amp[454] +
      Complex<double> (0, 1) * amp[455] + amp[461] + amp[460] + Complex<double>
      (0, 1) * amp[834] + amp[838] + amp[837] + amp[840] + amp[841] +
      Complex<double> (0, 1) * amp[843] + amp[845] - amp[868] - amp[871] -
      amp[870] - amp[884] + amp[882] + amp[892] + amp[891] + amp[894] +
      amp[895] - Complex<double> (0, 1) * amp[909] + amp[910] - Complex<double>
      (0, 1) * amp[912] - Complex<double> (0, 1) * amp[917] + Complex<double>
      (0, 1) * amp[915] + Complex<double> (0, 1) * amp[939] + Complex<double>
      (0, 1) * amp[943] + amp[944] - Complex<double> (0, 1) * amp[950] +
      Complex<double> (0, 1) * amp[948] + amp[953] - amp[951] - amp[1079] -
      amp[1080] - amp[1082] - amp[1081] - amp[1085] - amp[1089] - amp[1090] -
      amp[1094] + amp[1092] - amp[1097] - amp[1096] - Complex<double> (0, 1) *
      amp[1109] - Complex<double> (0, 1) * amp[1110] - Complex<double> (0, 1) *
      amp[1111] + amp[1231] + amp[1234] + amp[1233] - amp[1235] + amp[1239] -
      amp[1237] - amp[1240] + amp[1244] - amp[1242] - amp[1253] + amp[1251] -
      amp[1255] - amp[1254] - amp[1257] - amp[1258] + Complex<double> (0, 1) *
      amp[1272] - amp[1273] - Complex<double> (0, 1) * amp[1277] +
      Complex<double> (0, 1) * amp[1280] - Complex<double> (0, 1) * amp[1278] -
      Complex<double> (0, 1) * amp[1302] - amp[1308] - amp[1311] - amp[1310] -
      amp[1328] - amp[1327] - amp[1596] - amp[1594] - amp[1597] - amp[1593] -
      amp[1602] - amp[1603] - amp[1608] - amp[1609] - amp[1620] - amp[1621] -
      Complex<double> (0, 1) * amp[1623] - Complex<double> (0, 1) * amp[1624] -
      Complex<double> (0, 1) * amp[1644] - Complex<double> (0, 1) * amp[1645] -
      amp[1811] + amp[1817] - amp[1810] + amp[1816] - amp[1821] + amp[1823] +
      amp[1839] + amp[1840] + amp[1843] + amp[1842] + amp[1847] + amp[1846] +
      amp[1849] + amp[1850] + Complex<double> (0, 1) * amp[1853] +
      Complex<double> (0, 1) * amp[1852];
  jamp[88] = +amp[11] + amp[14] + amp[13] + amp[18] + amp[19] - amp[26] +
      amp[24] + amp[27] - amp[29] + amp[31] + amp[30] + amp[33] + amp[34] -
      amp[44] - amp[43] + Complex<double> (0, 1) * amp[63] + Complex<double>
      (0, 1) * amp[67] - amp[68] + Complex<double> (0, 1) * amp[69] +
      Complex<double> (0, 1) * amp[70] + amp[79] + Complex<double> (0, 1) *
      amp[80] - amp[137] + Complex<double> (0, 1) * amp[138] - Complex<double>
      (0, 1) * amp[139] + Complex<double> (0, 1) * amp[144] - Complex<double>
      (0, 1) * amp[158] - Complex<double> (0, 1) * amp[157] - Complex<double>
      (0, 1) * amp[815] - Complex<double> (0, 1) * amp[821] + Complex<double>
      (0, 1) * amp[819] - Complex<double> (0, 1) * amp[825] + amp[827] +
      amp[828] + Complex<double> (0, 1) * amp[830] + amp[868] + amp[871] +
      amp[870] - amp[872] - amp[873] + amp[876] - amp[874] + amp[884] +
      amp[883] - amp[890] + amp[888] + amp[893] - amp[891] - amp[894] +
      amp[896] + Complex<double> (0, 1) * amp[909] - amp[910] - Complex<double>
      (0, 1) * amp[914] + Complex<double> (0, 1) * amp[917] + Complex<double>
      (0, 1) * amp[916] - Complex<double> (0, 1) * amp[939] - amp[1023] +
      Complex<double> (0, 1) * amp[1029] - amp[1031] - Complex<double> (0, 1) *
      amp[1041] - amp[1042] + Complex<double> (0, 1) * amp[1045] -
      Complex<double> (0, 1) * amp[1047] - Complex<double> (0, 1) * amp[1048] -
      amp[1051] + Complex<double> (0, 1) * amp[1052] + Complex<double> (0, 1) *
      amp[1054] + Complex<double> (0, 1) * amp[1058] - Complex<double> (0, 1) *
      amp[1056] + amp[1070] + amp[1069] + Complex<double> (0, 1) * amp[1073] +
      Complex<double> (0, 1) * amp[1072] + amp[1318] - amp[1322] + amp[1320] -
      amp[1329] - amp[1330] + amp[1337] + amp[1338] - amp[1341] + amp[1339] +
      amp[1342] - amp[1346] + amp[1344] - amp[1349] - amp[1348] + amp[1355] -
      amp[1353] - amp[1386] + amp[1385] + amp[1388] - amp[1383] + amp[1394] +
      amp[1393] + Complex<double> (0, 1) * amp[1431] - Complex<double> (0, 1) *
      amp[1436] - Complex<double> (0, 1) * amp[1435] + amp[1596] + amp[1597] +
      amp[1600] + amp[1599] + amp[1604] + amp[1603] + amp[1609] + amp[1610] +
      amp[1621] + amp[1622] + Complex<double> (0, 1) * amp[1625] +
      Complex<double> (0, 1) * amp[1624] + Complex<double> (0, 1) * amp[1645] +
      Complex<double> (0, 1) * amp[1646] - Complex<double> (0, 1) * amp[1713] -
      Complex<double> (0, 1) * amp[1714] - amp[1716] - amp[1717] - amp[1730] +
      amp[1731] + amp[1728] - amp[1733] + amp[1737] + amp[1738] - amp[1740] -
      amp[1741] + Complex<double> (0, 1) * amp[1743] + Complex<double> (0, 1) *
      amp[1744];
  jamp[89] = -amp[11] - amp[14] - amp[13] - amp[18] - amp[19] + amp[26] -
      amp[24] - amp[27] + amp[29] - amp[31] - amp[30] - amp[33] - amp[34] +
      amp[44] + amp[43] - Complex<double> (0, 1) * amp[63] - Complex<double>
      (0, 1) * amp[67] + amp[68] - Complex<double> (0, 1) * amp[69] -
      Complex<double> (0, 1) * amp[70] - amp[79] - Complex<double> (0, 1) *
      amp[80] + amp[137] - Complex<double> (0, 1) * amp[138] + Complex<double>
      (0, 1) * amp[139] - Complex<double> (0, 1) * amp[144] + Complex<double>
      (0, 1) * amp[158] + Complex<double> (0, 1) * amp[157] + Complex<double>
      (0, 1) * amp[372] - Complex<double> (0, 1) * amp[376] + Complex<double>
      (0, 1) * amp[374] - Complex<double> (0, 1) * amp[384] + amp[385] +
      amp[387] + Complex<double> (0, 1) * amp[388] - amp[399] + Complex<double>
      (0, 1) * amp[405] - amp[406] - Complex<double> (0, 1) * amp[417] -
      amp[418] - Complex<double> (0, 1) * amp[420] + Complex<double> (0, 1) *
      amp[423] + Complex<double> (0, 1) * amp[422] - amp[427] - Complex<double>
      (0, 1) * amp[428] - Complex<double> (0, 1) * amp[429] + Complex<double>
      (0, 1) * amp[432] - Complex<double> (0, 1) * amp[430] - Complex<double>
      (0, 1) * amp[453] - Complex<double> (0, 1) * amp[454] - amp[459] -
      amp[460] + amp[473] + amp[472] + Complex<double> (0, 1) * amp[475] +
      Complex<double> (0, 1) * amp[476] - amp[1231] - amp[1234] - amp[1233] +
      amp[1235] - amp[1239] + amp[1237] + amp[1240] - amp[1244] + amp[1242] +
      amp[1253] - amp[1251] + amp[1255] + amp[1254] + amp[1257] + amp[1258] -
      Complex<double> (0, 1) * amp[1272] + amp[1273] + Complex<double> (0, 1) *
      amp[1277] - Complex<double> (0, 1) * amp[1280] + Complex<double> (0, 1) *
      amp[1278] + Complex<double> (0, 1) * amp[1302] - amp[1309] + amp[1312] +
      amp[1311] + amp[1322] + amp[1321] - amp[1332] - amp[1333] + amp[1336] -
      amp[1334] - amp[1343] + amp[1346] + amp[1345] + amp[1349] - amp[1347] -
      amp[1352] + amp[1350] + amp[1386] + amp[1384] + amp[1383] + amp[1387] +
      amp[1390] + amp[1391] - Complex<double> (0, 1) * amp[1433] +
      Complex<double> (0, 1) * amp[1436] - Complex<double> (0, 1) * amp[1434] +
      amp[1594] + amp[1593] - amp[1600] - amp[1599] - amp[1604] + amp[1602] +
      amp[1608] - amp[1610] + amp[1620] - amp[1622] - Complex<double> (0, 1) *
      amp[1625] + Complex<double> (0, 1) * amp[1623] + Complex<double> (0, 1) *
      amp[1644] - Complex<double> (0, 1) * amp[1646] - amp[1839] - amp[1837] -
      amp[1840] - amp[1836] - amp[1845] - amp[1846] - amp[1848] - amp[1849] -
      Complex<double> (0, 1) * amp[1851] - Complex<double> (0, 1) * amp[1852];
  jamp[90] = -amp[0] + amp[4] - amp[2] - amp[10] - amp[11] - amp[13] - amp[12]
      - amp[20] - amp[19] - amp[21] - amp[22] - amp[38] - amp[37] - amp[40] -
      amp[41] - Complex<double> (0, 1) * amp[65] + amp[66] - Complex<double>
      (0, 1) * amp[67] - Complex<double> (0, 1) * amp[71] - Complex<double> (0,
      1) * amp[70] + Complex<double> (0, 1) * amp[73] - amp[74] +
      Complex<double> (0, 1) * amp[103] + Complex<double> (0, 1) * amp[105] +
      Complex<double> (0, 1) * amp[106] + amp[109] + Complex<double> (0, 1) *
      amp[110] - Complex<double> (0, 1) * amp[151] - Complex<double> (0, 1) *
      amp[836] - amp[839] + amp[837] + amp[840] - amp[842] + Complex<double>
      (0, 1) * amp[843] - amp[844] - amp[867] - amp[868] - amp[870] - amp[869]
      + amp[878] - amp[881] - amp[880] - amp[884] + amp[882] - amp[887] -
      amp[886] - Complex<double> (0, 1) * amp[909] - Complex<double> (0, 1) *
      amp[917] + Complex<double> (0, 1) * amp[915] - amp[918] - Complex<double>
      (0, 1) * amp[919] + Complex<double> (0, 1) * amp[921] + amp[925] -
      Complex<double> (0, 1) * amp[926] + Complex<double> (0, 1) * amp[929] +
      Complex<double> (0, 1) * amp[928] - amp[1074] + amp[1078] - amp[1076] -
      amp[1091] - amp[1090] + amp[1100] - amp[1098] - amp[1101] + amp[1103] -
      Complex<double> (0, 1) * amp[1104] + amp[1105] + Complex<double> (0, 1) *
      amp[1108] - Complex<double> (0, 1) * amp[1112] - Complex<double> (0, 1) *
      amp[1111] - Complex<double> (0, 1) * amp[1146] + Complex<double> (0, 1) *
      amp[1149] + amp[1151] - amp[1362] + amp[1365] + amp[1364] + amp[1373] +
      amp[1372] - amp[1421] - amp[1460] + Complex<double> (0, 1) * amp[1461] +
      Complex<double> (0, 1) * amp[1462] - Complex<double> (0, 1) * amp[1466] -
      Complex<double> (0, 1) * amp[1465] + amp[1487] - amp[1493] - amp[1485] +
      amp[1491] - amp[1499] + amp[1497] - amp[1500] + amp[1502] + amp[1503] -
      amp[1505] - Complex<double> (0, 1) * amp[1508] + Complex<double> (0, 1) *
      amp[1506] - Complex<double> (0, 1) * amp[1536] + Complex<double> (0, 1) *
      amp[1538] - amp[1601] - amp[1597] - amp[1600] - amp[1598] - amp[1604] -
      amp[1603] - amp[1607] - amp[1606] - Complex<double> (0, 1) * amp[1625] -
      Complex<double> (0, 1) * amp[1624] + amp[1628] + amp[1627] +
      Complex<double> (0, 1) * amp[1634] + Complex<double> (0, 1) * amp[1633] +
      Complex<double> (0, 1) * amp[1773] + Complex<double> (0, 1) * amp[1774] +
      amp[1779] + amp[1780] - amp[1785] - amp[1783] - amp[1786] - amp[1782] -
      amp[1791] - amp[1792] + Complex<double> (0, 1) * amp[1803] +
      Complex<double> (0, 1) * amp[1804] - amp[1806] - amp[1807];
  jamp[91] = +Complex<double> (0, 1) * amp[499] + Complex<double> (0, 1) *
      amp[501] + Complex<double> (0, 1) * amp[502] + Complex<double> (0, 1) *
      amp[504] - amp[506] + amp[510] + Complex<double> (0, 1) * amp[512] -
      amp[549] + amp[553] - amp[551] + amp[559] + amp[560] - amp[563] +
      amp[561] - amp[569] - amp[568] + amp[570] + amp[571] + amp[575] -
      amp[573] - amp[576] + amp[578] - Complex<double> (0, 1) * amp[579] +
      amp[580] + Complex<double> (0, 1) * amp[584] - Complex<double> (0, 1) *
      amp[587] - Complex<double> (0, 1) * amp[586] - Complex<double> (0, 1) *
      amp[621] + Complex<double> (0, 1) * amp[815] + Complex<double> (0, 1) *
      amp[817] + amp[818] + Complex<double> (0, 1) * amp[821] + Complex<double>
      (0, 1) * amp[820] - Complex<double> (0, 1) * amp[826] - amp[827] +
      Complex<double> (0, 1) * amp[836] + amp[839] - amp[837] - amp[840] +
      amp[842] - Complex<double> (0, 1) * amp[843] + amp[844] + amp[872] +
      amp[873] - amp[876] + amp[874] + amp[877] + amp[880] + amp[879] -
      amp[882] - amp[883] + amp[890] + amp[889] + Complex<double> (0, 1) *
      amp[914] - Complex<double> (0, 1) * amp[915] - Complex<double> (0, 1) *
      amp[916] - amp[1027] - Complex<double> (0, 1) * amp[1061] - amp[1062] +
      Complex<double> (0, 1) * amp[1064] - Complex<double> (0, 1) * amp[1067] -
      Complex<double> (0, 1) * amp[1066] + amp[1074] - amp[1078] + amp[1076] +
      amp[1091] + amp[1090] - amp[1100] + amp[1098] + amp[1101] - amp[1103] +
      Complex<double> (0, 1) * amp[1104] - amp[1105] - Complex<double> (0, 1) *
      amp[1108] + Complex<double> (0, 1) * amp[1112] + Complex<double> (0, 1) *
      amp[1111] + Complex<double> (0, 1) * amp[1146] - Complex<double> (0, 1) *
      amp[1149] - amp[1151] - amp[1361] - amp[1364] - amp[1363] - amp[1379] -
      amp[1378] - amp[1487] + amp[1488] + amp[1485] - amp[1490] - amp[1497] -
      amp[1498] + amp[1500] + amp[1501] - amp[1503] - amp[1504] -
      Complex<double> (0, 1) * amp[1506] - Complex<double> (0, 1) * amp[1507] +
      Complex<double> (0, 1) * amp[1536] + Complex<double> (0, 1) * amp[1537] +
      Complex<double> (0, 1) * amp[1715] + Complex<double> (0, 1) * amp[1714] +
      amp[1718] + amp[1717] - amp[1731] + amp[1736] + amp[1733] - amp[1734] -
      amp[1739] - amp[1738] + amp[1742] + amp[1741] - Complex<double> (0, 1) *
      amp[1745] - Complex<double> (0, 1) * amp[1744] - amp[1764] - amp[1765] +
      Complex<double> (0, 1) * amp[1767] + Complex<double> (0, 1) * amp[1768] -
      Complex<double> (0, 1) * amp[1773] - Complex<double> (0, 1) * amp[1774] -
      amp[1779] - amp[1780] + amp[1784] + amp[1783] + amp[1786] + amp[1787] +
      amp[1794] + amp[1795];
  jamp[92] = +amp[0] - amp[4] + amp[2] + amp[10] + amp[11] + amp[13] + amp[12]
      + amp[20] + amp[19] + amp[21] + amp[22] + amp[38] + amp[37] + amp[40] +
      amp[41] + Complex<double> (0, 1) * amp[65] - amp[66] + Complex<double>
      (0, 1) * amp[67] + Complex<double> (0, 1) * amp[71] + Complex<double> (0,
      1) * amp[70] - Complex<double> (0, 1) * amp[73] + amp[74] -
      Complex<double> (0, 1) * amp[103] - Complex<double> (0, 1) * amp[105] -
      Complex<double> (0, 1) * amp[106] - amp[109] - Complex<double> (0, 1) *
      amp[110] + Complex<double> (0, 1) * amp[151] - Complex<double> (0, 1) *
      amp[534] - amp[538] - amp[537] - amp[540] - amp[541] - Complex<double>
      (0, 1) * amp[543] - amp[545] + amp[549] - amp[553] + amp[551] + amp[569]
      - amp[567] + amp[574] + amp[573] + amp[576] + amp[577] + Complex<double>
      (0, 1) * amp[579] - amp[580] + Complex<double> (0, 1) * amp[582] +
      Complex<double> (0, 1) * amp[587] - Complex<double> (0, 1) * amp[585] +
      Complex<double> (0, 1) * amp[621] + Complex<double> (0, 1) * amp[625] +
      amp[626] + Complex<double> (0, 1) * amp[630] + Complex<double> (0, 1) *
      amp[631] - amp[633] - amp[634] + amp[1230] + amp[1231] + amp[1233] +
      amp[1232] - amp[1241] + amp[1244] + amp[1243] + amp[1247] - amp[1245] +
      amp[1248] + amp[1249] + Complex<double> (0, 1) * amp[1272] +
      Complex<double> (0, 1) * amp[1280] + Complex<double> (0, 1) * amp[1279] +
      amp[1281] - Complex<double> (0, 1) * amp[1283] - Complex<double> (0, 1) *
      amp[1284] + amp[1287] + Complex<double> (0, 1) * amp[1289] -
      Complex<double> (0, 1) * amp[1292] + Complex<double> (0, 1) * amp[1290] +
      amp[1367] - amp[1373] + amp[1371] - amp[1374] - amp[1375] - amp[1419] -
      amp[1458] + Complex<double> (0, 1) * amp[1459] - Complex<double> (0, 1) *
      amp[1463] + Complex<double> (0, 1) * amp[1466] - Complex<double> (0, 1) *
      amp[1464] - amp[1488] + amp[1493] + amp[1490] - amp[1491] + amp[1499] +
      amp[1498] - amp[1501] - amp[1502] + amp[1504] + amp[1505] +
      Complex<double> (0, 1) * amp[1508] + Complex<double> (0, 1) * amp[1507] -
      Complex<double> (0, 1) * amp[1537] - Complex<double> (0, 1) * amp[1538] -
      amp[1595] + amp[1601] - amp[1594] + amp[1600] + amp[1604] - amp[1602] +
      amp[1607] - amp[1605] + Complex<double> (0, 1) * amp[1625] -
      Complex<double> (0, 1) * amp[1623] - amp[1628] + amp[1626] -
      Complex<double> (0, 1) * amp[1634] + Complex<double> (0, 1) * amp[1632] -
      amp[1865] + amp[1866] + amp[1863] - amp[1868] + amp[1872] + amp[1873] -
      Complex<double> (0, 1) * amp[1884] - Complex<double> (0, 1) * amp[1885] -
      amp[1887] - amp[1888];
  jamp[93] = -Complex<double> (0, 1) * amp[337] - Complex<double> (0, 1) *
      amp[340] - Complex<double> (0, 1) * amp[339] + Complex<double> (0, 1) *
      amp[345] - amp[346] + amp[351] + Complex<double> (0, 1) * amp[352] -
      Complex<double> (0, 1) * amp[372] - Complex<double> (0, 1) * amp[373] -
      Complex<double> (0, 1) * amp[375] - Complex<double> (0, 1) * amp[374] +
      amp[378] - amp[385] - Complex<double> (0, 1) * amp[386] - amp[402] +
      Complex<double> (0, 1) * amp[437] + Complex<double> (0, 1) * amp[440] +
      Complex<double> (0, 1) * amp[439] - amp[442] + Complex<double> (0, 1) *
      amp[443] + Complex<double> (0, 1) * amp[454] + Complex<double> (0, 1) *
      amp[455] + amp[461] + amp[460] + Complex<double> (0, 1) * amp[462] +
      Complex<double> (0, 1) * amp[463] - amp[465] - amp[466] + Complex<double>
      (0, 1) * amp[534] + amp[538] + amp[537] + amp[540] + amp[541] +
      Complex<double> (0, 1) * amp[543] + amp[545] - amp[549] + amp[553] -
      amp[551] - amp[569] + amp[567] - amp[574] - amp[573] - amp[576] -
      amp[577] - Complex<double> (0, 1) * amp[579] + amp[580] - Complex<double>
      (0, 1) * amp[582] - Complex<double> (0, 1) * amp[587] + Complex<double>
      (0, 1) * amp[585] - Complex<double> (0, 1) * amp[621] - Complex<double>
      (0, 1) * amp[625] - amp[626] - Complex<double> (0, 1) * amp[630] -
      Complex<double> (0, 1) * amp[631] + amp[633] + amp[634] + amp[1074] -
      amp[1078] + amp[1076] - amp[1080] - amp[1083] - amp[1082] - amp[1085] +
      amp[1091] - amp[1089] - amp[1094] + amp[1092] + amp[1099] + amp[1098] +
      amp[1101] + amp[1102] + Complex<double> (0, 1) * amp[1104] - amp[1105] -
      Complex<double> (0, 1) * amp[1109] + Complex<double> (0, 1) * amp[1112] -
      Complex<double> (0, 1) * amp[1110] + Complex<double> (0, 1) * amp[1146] -
      amp[1235] - amp[1236] - amp[1238] - amp[1237] - amp[1240] - amp[1242] -
      amp[1243] - amp[1250] - amp[1249] - amp[1253] + amp[1251] -
      Complex<double> (0, 1) * amp[1277] - Complex<double> (0, 1) * amp[1278] -
      Complex<double> (0, 1) * amp[1279] + amp[1356] + amp[1359] + amp[1358] +
      amp[1376] + amp[1375] - amp[1487] + amp[1488] + amp[1485] - amp[1490] -
      amp[1497] - amp[1498] + amp[1500] + amp[1501] - amp[1503] - amp[1504] -
      Complex<double> (0, 1) * amp[1506] - Complex<double> (0, 1) * amp[1507] +
      Complex<double> (0, 1) * amp[1536] + Complex<double> (0, 1) * amp[1537] +
      amp[1839] + amp[1840] + amp[1843] + amp[1842] + amp[1847] + amp[1846] +
      amp[1849] + amp[1850] + Complex<double> (0, 1) * amp[1853] +
      Complex<double> (0, 1) * amp[1852] + amp[1865] + amp[1864] + amp[1867] +
      amp[1868] + amp[1875] + amp[1876];
  jamp[94] = +amp[0] - amp[4] + amp[2] + amp[20] - amp[18] - amp[31] - amp[30]
      - amp[33] - amp[34] + amp[37] + amp[36] + amp[39] + amp[40] + amp[42] +
      amp[43] + Complex<double> (0, 1) * amp[64] + Complex<double> (0, 1) *
      amp[65] - amp[66] + Complex<double> (0, 1) * amp[71] - Complex<double>
      (0, 1) * amp[69] - amp[79] - Complex<double> (0, 1) * amp[80] -
      Complex<double> (0, 1) * amp[144] + amp[149] + Complex<double> (0, 1) *
      amp[150] + Complex<double> (0, 1) * amp[151] + Complex<double> (0, 1) *
      amp[156] + Complex<double> (0, 1) * amp[157] - Complex<double> (0, 1) *
      amp[499] - Complex<double> (0, 1) * amp[501] - Complex<double> (0, 1) *
      amp[502] - Complex<double> (0, 1) * amp[504] + amp[506] - amp[510] -
      Complex<double> (0, 1) * amp[512] + amp[549] - amp[553] + amp[551] -
      amp[559] - amp[560] + amp[563] - amp[561] + amp[569] + amp[568] -
      amp[570] - amp[571] - amp[575] + amp[573] + amp[576] - amp[578] +
      Complex<double> (0, 1) * amp[579] - amp[580] - Complex<double> (0, 1) *
      amp[584] + Complex<double> (0, 1) * amp[587] + Complex<double> (0, 1) *
      amp[586] + Complex<double> (0, 1) * amp[621] - amp[1024] -
      Complex<double> (0, 1) * amp[1029] + amp[1031] + amp[1051] -
      Complex<double> (0, 1) * amp[1052] - Complex<double> (0, 1) * amp[1054] -
      Complex<double> (0, 1) * amp[1058] + Complex<double> (0, 1) * amp[1056] +
      Complex<double> (0, 1) * amp[1059] - amp[1060] + Complex<double> (0, 1) *
      amp[1063] + Complex<double> (0, 1) * amp[1067] - Complex<double> (0, 1) *
      amp[1065] - amp[1068] - amp[1069] - Complex<double> (0, 1) * amp[1071] -
      Complex<double> (0, 1) * amp[1072] - amp[1337] - amp[1338] + amp[1341] -
      amp[1339] - amp[1342] + amp[1346] - amp[1344] + amp[1349] + amp[1348] -
      amp[1355] + amp[1353] + amp[1366] - amp[1370] + amp[1368] + amp[1379] -
      amp[1377] - amp[1382] - amp[1385] + amp[1380] + amp[1383] - amp[1392] -
      amp[1393] - Complex<double> (0, 1) * amp[1431] + Complex<double> (0, 1) *
      amp[1436] + Complex<double> (0, 1) * amp[1435] - amp[1488] + amp[1493] +
      amp[1490] - amp[1491] + amp[1499] + amp[1498] - amp[1501] - amp[1502] +
      amp[1504] + amp[1505] + Complex<double> (0, 1) * amp[1508] +
      Complex<double> (0, 1) * amp[1507] - Complex<double> (0, 1) * amp[1537] -
      Complex<double> (0, 1) * amp[1538] - Complex<double> (0, 1) * amp[1715] +
      Complex<double> (0, 1) * amp[1713] - amp[1718] + amp[1716] + amp[1730] -
      amp[1736] - amp[1728] + amp[1734] + amp[1739] - amp[1737] - amp[1742] +
      amp[1740] + Complex<double> (0, 1) * amp[1745] - Complex<double> (0, 1) *
      amp[1743];
  jamp[95] = -amp[0] + amp[4] - amp[2] - amp[20] + amp[18] + amp[31] + amp[30]
      + amp[33] + amp[34] - amp[37] - amp[36] - amp[39] - amp[40] - amp[42] -
      amp[43] - Complex<double> (0, 1) * amp[64] - Complex<double> (0, 1) *
      amp[65] + amp[66] - Complex<double> (0, 1) * amp[71] + Complex<double>
      (0, 1) * amp[69] + amp[79] + Complex<double> (0, 1) * amp[80] +
      Complex<double> (0, 1) * amp[144] - amp[149] - Complex<double> (0, 1) *
      amp[150] - Complex<double> (0, 1) * amp[151] - Complex<double> (0, 1) *
      amp[156] - Complex<double> (0, 1) * amp[157] + Complex<double> (0, 1) *
      amp[337] + Complex<double> (0, 1) * amp[340] + Complex<double> (0, 1) *
      amp[339] - Complex<double> (0, 1) * amp[345] + amp[346] - amp[351] -
      Complex<double> (0, 1) * amp[352] - amp[400] - Complex<double> (0, 1) *
      amp[405] + amp[406] + amp[427] + Complex<double> (0, 1) * amp[428] +
      Complex<double> (0, 1) * amp[429] - Complex<double> (0, 1) * amp[432] +
      Complex<double> (0, 1) * amp[430] + Complex<double> (0, 1) * amp[435] -
      amp[436] - Complex<double> (0, 1) * amp[438] + Complex<double> (0, 1) *
      amp[441] - Complex<double> (0, 1) * amp[439] + Complex<double> (0, 1) *
      amp[453] - Complex<double> (0, 1) * amp[455] - amp[461] + amp[459] -
      amp[471] - amp[472] - Complex<double> (0, 1) * amp[474] - Complex<double>
      (0, 1) * amp[475] - amp[1074] + amp[1078] - amp[1076] + amp[1080] +
      amp[1083] + amp[1082] + amp[1085] - amp[1091] + amp[1089] + amp[1094] -
      amp[1092] - amp[1099] - amp[1098] - amp[1101] - amp[1102] -
      Complex<double> (0, 1) * amp[1104] + amp[1105] + Complex<double> (0, 1) *
      amp[1109] - Complex<double> (0, 1) * amp[1112] + Complex<double> (0, 1) *
      amp[1110] - Complex<double> (0, 1) * amp[1146] + amp[1332] + amp[1333] -
      amp[1336] + amp[1334] + amp[1343] - amp[1346] - amp[1345] - amp[1349] +
      amp[1347] + amp[1352] - amp[1350] - amp[1357] + amp[1360] - amp[1358] +
      amp[1370] + amp[1369] - amp[1384] - amp[1381] - amp[1380] - amp[1383] -
      amp[1389] - amp[1390] + Complex<double> (0, 1) * amp[1433] -
      Complex<double> (0, 1) * amp[1436] + Complex<double> (0, 1) * amp[1434] +
      amp[1487] - amp[1493] - amp[1485] + amp[1491] - amp[1499] + amp[1497] -
      amp[1500] + amp[1502] + amp[1503] - amp[1505] - Complex<double> (0, 1) *
      amp[1508] + Complex<double> (0, 1) * amp[1506] - Complex<double> (0, 1) *
      amp[1536] + Complex<double> (0, 1) * amp[1538] + amp[1837] + amp[1836] -
      amp[1843] - amp[1842] - amp[1847] + amp[1845] + amp[1848] - amp[1850] -
      Complex<double> (0, 1) * amp[1853] + Complex<double> (0, 1) * amp[1851];
  jamp[96] = +amp[0] + amp[1] + amp[3] + amp[2] + amp[11] + amp[14] + amp[13] +
      amp[17] - amp[15] + amp[20] + amp[19] - amp[26] - amp[25] - amp[28] -
      amp[29] + Complex<double> (0, 1) * amp[83] + Complex<double> (0, 1) *
      amp[89] - Complex<double> (0, 1) * amp[87] + Complex<double> (0, 1) *
      amp[93] + amp[95] + amp[96] - Complex<double> (0, 1) * amp[98] -
      Complex<double> (0, 1) * amp[113] - Complex<double> (0, 1) * amp[115] -
      amp[116] - Complex<double> (0, 1) * amp[119] - Complex<double> (0, 1) *
      amp[118] + Complex<double> (0, 1) * amp[140] + Complex<double> (0, 1) *
      amp[832] - amp[838] - amp[837] - amp[840] - amp[841] + Complex<double>
      (0, 1) * amp[846] - amp[847] + amp[868] + amp[871] + amp[870] + amp[884]
      - amp[882] - amp[892] - amp[891] - amp[894] - amp[895] - Complex<double>
      (0, 1) * amp[930] - amp[931] - Complex<double> (0, 1) * amp[933] -
      Complex<double> (0, 1) * amp[938] + Complex<double> (0, 1) * amp[936] +
      Complex<double> (0, 1) * amp[940] + Complex<double> (0, 1) * amp[941] -
      amp[942] - Complex<double> (0, 1) * amp[947] + Complex<double> (0, 1) *
      amp[945] - amp[953] + amp[951] + amp[1074] + amp[1075] + amp[1077] +
      amp[1076] - amp[1084] + amp[1088] - amp[1086] + amp[1091] + amp[1090] +
      amp[1095] + amp[1096] + Complex<double> (0, 1) * amp[1116] + amp[1119] -
      Complex<double> (0, 1) * amp[1121] + Complex<double> (0, 1) * amp[1124] -
      Complex<double> (0, 1) * amp[1122] + amp[1125] + Complex<double> (0, 1) *
      amp[1126] - Complex<double> (0, 1) * amp[1128] - Complex<double> (0, 1) *
      amp[1136] - Complex<double> (0, 1) * amp[1135] - amp[1319] + amp[1325] -
      amp[1323] + amp[1326] + amp[1327] - amp[1444] + amp[1449] +
      Complex<double> (0, 1) * amp[1450] - Complex<double> (0, 1) * amp[1454] +
      Complex<double> (0, 1) * amp[1457] - Complex<double> (0, 1) * amp[1455] -
      amp[1487] + amp[1493] - amp[1486] + amp[1492] + amp[1496] - amp[1494] +
      amp[1499] - amp[1497] + Complex<double> (0, 1) * amp[1517] -
      Complex<double> (0, 1) * amp[1515] - amp[1520] + amp[1518] -
      Complex<double> (0, 1) * amp[1526] + Complex<double> (0, 1) * amp[1524] +
      amp[1596] + amp[1597] + amp[1600] + amp[1599] + amp[1604] + amp[1603] +
      amp[1609] + amp[1610] + amp[1637] + amp[1636] - Complex<double> (0, 1) *
      amp[1640] - Complex<double> (0, 1) * amp[1639] - Complex<double> (0, 1) *
      amp[1642] - Complex<double> (0, 1) * amp[1643] + amp[1811] - amp[1817] -
      amp[1809] + amp[1815] + amp[1820] - amp[1818] + Complex<double> (0, 1) *
      amp[1826] - Complex<double> (0, 1) * amp[1824] - amp[1829] + amp[1827];
  jamp[97] = +amp[5] + amp[6] + amp[8] + amp[7] + amp[10] - amp[14] + amp[12] +
      amp[15] + amp[16] + amp[23] + amp[22] + amp[26] + amp[25] + amp[28] +
      amp[29] + Complex<double> (0, 1) * amp[85] + Complex<double> (0, 1) *
      amp[87] + Complex<double> (0, 1) * amp[88] + Complex<double> (0, 1) *
      amp[90] + amp[92] - amp[96] + Complex<double> (0, 1) * amp[98] -
      Complex<double> (0, 1) * amp[101] - Complex<double> (0, 1) * amp[103] -
      amp[104] - Complex<double> (0, 1) * amp[107] - Complex<double> (0, 1) *
      amp[106] - Complex<double> (0, 1) * amp[140] + Complex<double> (0, 1) *
      amp[850] - amp[856] - amp[855] - amp[858] - amp[859] + Complex<double>
      (0, 1) * amp[864] - amp[865] + amp[867] - amp[871] + amp[869] + amp[887]
      - amp[885] + amp[892] + amp[891] + amp[894] + amp[895] - Complex<double>
      (0, 1) * amp[921] - amp[922] - Complex<double> (0, 1) * amp[924] -
      Complex<double> (0, 1) * amp[929] + Complex<double> (0, 1) * amp[927] -
      Complex<double> (0, 1) * amp[940] - Complex<double> (0, 1) * amp[941] +
      amp[942] - Complex<double> (0, 1) * amp[945] - Complex<double> (0, 1) *
      amp[946] - amp[951] - amp[952] + amp[1152] + amp[1153] + amp[1155] +
      amp[1154] - amp[1162] + amp[1166] - amp[1164] + amp[1169] + amp[1168] +
      amp[1173] + amp[1174] + Complex<double> (0, 1) * amp[1194] + amp[1197] -
      Complex<double> (0, 1) * amp[1199] + Complex<double> (0, 1) * amp[1202] -
      Complex<double> (0, 1) * amp[1200] + amp[1203] + Complex<double> (0, 1) *
      amp[1204] - Complex<double> (0, 1) * amp[1206] - Complex<double> (0, 1) *
      amp[1214] - Complex<double> (0, 1) * amp[1213] + amp[1319] - amp[1325] +
      amp[1323] - amp[1326] - amp[1327] - amp[1438] - amp[1449] -
      Complex<double> (0, 1) * amp[1450] + Complex<double> (0, 1) * amp[1454] -
      Complex<double> (0, 1) * amp[1457] + Complex<double> (0, 1) * amp[1455] -
      amp[1541] + amp[1547] - amp[1540] + amp[1546] + amp[1550] - amp[1548] +
      amp[1553] - amp[1551] + Complex<double> (0, 1) * amp[1571] -
      Complex<double> (0, 1) * amp[1569] - amp[1574] + amp[1572] -
      Complex<double> (0, 1) * amp[1580] + Complex<double> (0, 1) * amp[1578] -
      amp[1596] + amp[1601] + amp[1598] - amp[1599] + amp[1607] + amp[1606] -
      amp[1609] - amp[1610] + amp[1631] + amp[1630] - Complex<double> (0, 1) *
      amp[1634] - Complex<double> (0, 1) * amp[1633] + Complex<double> (0, 1) *
      amp[1642] + Complex<double> (0, 1) * amp[1643] - amp[1811] + amp[1812] +
      amp[1809] - amp[1814] + amp[1818] + amp[1819] + Complex<double> (0, 1) *
      amp[1824] + Complex<double> (0, 1) * amp[1825] - amp[1827] - amp[1828];
  jamp[98] = +Complex<double> (0, 1) * amp[518] + amp[521] - amp[519] -
      amp[522] + amp[524] - Complex<double> (0, 1) * amp[525] + amp[526] +
      amp[549] + amp[550] + amp[552] + amp[551] - amp[560] + amp[563] +
      amp[562] + amp[566] - amp[564] + amp[569] + amp[568] + Complex<double>
      (0, 1) * amp[591] + Complex<double> (0, 1) * amp[599] - Complex<double>
      (0, 1) * amp[597] + amp[600] + Complex<double> (0, 1) * amp[601] -
      Complex<double> (0, 1) * amp[603] - amp[607] + Complex<double> (0, 1) *
      amp[608] - Complex<double> (0, 1) * amp[611] - Complex<double> (0, 1) *
      amp[610] - Complex<double> (0, 1) * amp[832] + amp[838] + amp[837] +
      amp[840] + amp[841] - Complex<double> (0, 1) * amp[846] + amp[847] -
      amp[849] - Complex<double> (0, 1) * amp[850] + Complex<double> (0, 1) *
      amp[854] + amp[857] + amp[856] + amp[859] + amp[860] - amp[873] +
      amp[876] + amp[875] + amp[882] + amp[883] - Complex<double> (0, 1) *
      amp[932] - amp[934] + Complex<double> (0, 1) * amp[935] - Complex<double>
      (0, 1) * amp[936] - Complex<double> (0, 1) * amp[937] + Complex<double>
      (0, 1) * amp[947] + Complex<double> (0, 1) * amp[946] + amp[953] +
      amp[952] - amp[1074] - amp[1075] - amp[1077] - amp[1076] + amp[1084] -
      amp[1088] + amp[1086] - amp[1091] - amp[1090] - amp[1095] - amp[1096] -
      Complex<double> (0, 1) * amp[1116] - amp[1119] + Complex<double> (0, 1) *
      amp[1121] - Complex<double> (0, 1) * amp[1124] + Complex<double> (0, 1) *
      amp[1122] - amp[1125] - Complex<double> (0, 1) * amp[1126] +
      Complex<double> (0, 1) * amp[1128] + Complex<double> (0, 1) * amp[1136] +
      Complex<double> (0, 1) * amp[1135] + amp[1162] + amp[1164] + amp[1165] -
      amp[1173] - amp[1174] + Complex<double> (0, 1) * amp[1196] - amp[1197] +
      Complex<double> (0, 1) * amp[1199] + Complex<double> (0, 1) * amp[1200] +
      Complex<double> (0, 1) * amp[1201] - amp[1443] + amp[1487] + amp[1486] +
      amp[1489] + amp[1490] + amp[1494] + amp[1495] + amp[1497] + amp[1498] +
      Complex<double> (0, 1) * amp[1515] + Complex<double> (0, 1) * amp[1516] -
      amp[1518] - amp[1519] - Complex<double> (0, 1) * amp[1524] -
      Complex<double> (0, 1) * amp[1525] - Complex<double> (0, 1) * amp[1720] -
      Complex<double> (0, 1) * amp[1721] - amp[1726] - amp[1727] + amp[1731] +
      amp[1732] + amp[1735] + amp[1734] + amp[1739] + amp[1738] -
      Complex<double> (0, 1) * amp[1751] - Complex<double> (0, 1) * amp[1750] +
      amp[1754] + amp[1753] - amp[1812] + amp[1817] + amp[1814] - amp[1815] -
      amp[1820] - amp[1819] - Complex<double> (0, 1) * amp[1826] -
      Complex<double> (0, 1) * amp[1825] + amp[1829] + amp[1828];
  jamp[99] = -Complex<double> (0, 1) * amp[497] - Complex<double> (0, 1) *
      amp[499] - amp[500] - Complex<double> (0, 1) * amp[503] - Complex<double>
      (0, 1) * amp[502] + Complex<double> (0, 1) * amp[508] + amp[509] -
      Complex<double> (0, 1) * amp[518] - amp[521] + amp[519] + amp[522] -
      amp[524] + Complex<double> (0, 1) * amp[525] - amp[526] - amp[554] -
      amp[555] + amp[558] - amp[556] - amp[559] - amp[562] - amp[561] +
      amp[564] + amp[565] - amp[572] - amp[571] - Complex<double> (0, 1) *
      amp[596] + Complex<double> (0, 1) * amp[597] + Complex<double> (0, 1) *
      amp[598] - Complex<double> (0, 1) * amp[814] - Complex<double> (0, 1) *
      amp[815] - amp[816] - Complex<double> (0, 1) * amp[821] + Complex<double>
      (0, 1) * amp[819] - amp[829] + Complex<double> (0, 1) * amp[830] +
      amp[849] + Complex<double> (0, 1) * amp[850] - Complex<double> (0, 1) *
      amp[854] - amp[857] - amp[856] - amp[859] - amp[860] - amp[872] -
      amp[875] - amp[874] - amp[890] + amp[888] + amp[893] + amp[892] +
      amp[895] + amp[896] - Complex<double> (0, 1) * amp[941] - Complex<double>
      (0, 1) * amp[945] - Complex<double> (0, 1) * amp[946] - amp[951] -
      amp[952] - amp[1035] + Complex<double> (0, 1) * amp[1039] + amp[1040] -
      Complex<double> (0, 1) * amp[1043] - Complex<double> (0, 1) * amp[1049] -
      Complex<double> (0, 1) * amp[1048] - amp[1162] - amp[1164] - amp[1165] +
      amp[1173] + amp[1174] - Complex<double> (0, 1) * amp[1196] + amp[1197] -
      Complex<double> (0, 1) * amp[1199] - Complex<double> (0, 1) * amp[1200] -
      Complex<double> (0, 1) * amp[1201] - amp[1313] - amp[1314] + amp[1317] -
      amp[1315] + amp[1319] + amp[1323] + amp[1324] - amp[1326] - amp[1327] -
      amp[1331] - amp[1330] - amp[1449] - Complex<double> (0, 1) * amp[1452] +
      Complex<double> (0, 1) * amp[1454] + Complex<double> (0, 1) * amp[1455] +
      Complex<double> (0, 1) * amp[1456] - Complex<double> (0, 1) * amp[1661] +
      Complex<double> (0, 1) * amp[1659] - amp[1664] + amp[1662] + amp[1676] -
      amp[1682] - amp[1674] + amp[1680] + amp[1685] - amp[1683] - amp[1688] +
      amp[1686] + Complex<double> (0, 1) * amp[1691] - Complex<double> (0, 1) *
      amp[1689] + amp[1712] + amp[1711] - Complex<double> (0, 1) * amp[1715] -
      Complex<double> (0, 1) * amp[1714] + Complex<double> (0, 1) * amp[1720] +
      Complex<double> (0, 1) * amp[1721] + amp[1726] + amp[1727] - amp[1736] -
      amp[1732] - amp[1735] - amp[1733] - amp[1742] - amp[1741] - amp[1811] +
      amp[1812] + amp[1809] - amp[1814] + amp[1818] + amp[1819] +
      Complex<double> (0, 1) * amp[1824] + Complex<double> (0, 1) * amp[1825] -
      amp[1827] - amp[1828];
  jamp[100] = +Complex<double> (0, 1) * amp[677] + amp[680] - amp[678] -
      amp[681] + amp[683] - Complex<double> (0, 1) * amp[684] + amp[685] +
      amp[708] + amp[709] + amp[711] + amp[710] - amp[719] + amp[722] +
      amp[721] + amp[725] - amp[723] + amp[728] + amp[727] + Complex<double>
      (0, 1) * amp[750] + Complex<double> (0, 1) * amp[758] - Complex<double>
      (0, 1) * amp[756] + amp[759] + Complex<double> (0, 1) * amp[760] -
      Complex<double> (0, 1) * amp[762] - amp[766] + Complex<double> (0, 1) *
      amp[767] - Complex<double> (0, 1) * amp[770] - Complex<double> (0, 1) *
      amp[769] - amp[831] - Complex<double> (0, 1) * amp[832] + Complex<double>
      (0, 1) * amp[836] + amp[839] + amp[838] + amp[841] + amp[842] -
      Complex<double> (0, 1) * amp[850] + amp[856] + amp[855] + amp[858] +
      amp[859] - Complex<double> (0, 1) * amp[864] + amp[865] - amp[878] +
      amp[881] + amp[880] + amp[885] + amp[886] - Complex<double> (0, 1) *
      amp[923] - amp[925] + Complex<double> (0, 1) * amp[926] - Complex<double>
      (0, 1) * amp[927] - Complex<double> (0, 1) * amp[928] + Complex<double>
      (0, 1) * amp[947] + Complex<double> (0, 1) * amp[946] + amp[953] +
      amp[952] + amp[1084] + amp[1086] + amp[1087] - amp[1095] - amp[1096] +
      Complex<double> (0, 1) * amp[1118] - amp[1119] + Complex<double> (0, 1) *
      amp[1121] + Complex<double> (0, 1) * amp[1122] + Complex<double> (0, 1) *
      amp[1123] - amp[1152] - amp[1153] - amp[1155] - amp[1154] + amp[1162] -
      amp[1166] + amp[1164] - amp[1169] - amp[1168] - amp[1173] - amp[1174] -
      Complex<double> (0, 1) * amp[1194] - amp[1197] + Complex<double> (0, 1) *
      amp[1199] - Complex<double> (0, 1) * amp[1202] + Complex<double> (0, 1) *
      amp[1200] - amp[1203] - Complex<double> (0, 1) * amp[1204] +
      Complex<double> (0, 1) * amp[1206] + Complex<double> (0, 1) * amp[1214] +
      Complex<double> (0, 1) * amp[1213] - amp[1437] + amp[1541] + amp[1540] +
      amp[1543] + amp[1544] + amp[1548] + amp[1549] + amp[1551] + amp[1552] +
      Complex<double> (0, 1) * amp[1569] + Complex<double> (0, 1) * amp[1570] -
      amp[1572] - amp[1573] - Complex<double> (0, 1) * amp[1578] -
      Complex<double> (0, 1) * amp[1579] - Complex<double> (0, 1) * amp[1774] -
      Complex<double> (0, 1) * amp[1775] - amp[1780] - amp[1781] + amp[1785] +
      amp[1786] + amp[1789] + amp[1788] + amp[1793] + amp[1792] -
      Complex<double> (0, 1) * amp[1805] - Complex<double> (0, 1) * amp[1804] +
      amp[1808] + amp[1807] - amp[1812] + amp[1817] + amp[1814] - amp[1815] -
      amp[1820] - amp[1819] - Complex<double> (0, 1) * amp[1826] -
      Complex<double> (0, 1) * amp[1825] + amp[1829] + amp[1828];
  jamp[101] = -Complex<double> (0, 1) * amp[656] - Complex<double> (0, 1) *
      amp[658] - amp[659] - Complex<double> (0, 1) * amp[662] - Complex<double>
      (0, 1) * amp[661] + Complex<double> (0, 1) * amp[667] + amp[668] -
      Complex<double> (0, 1) * amp[677] - amp[680] + amp[678] + amp[681] -
      amp[683] + Complex<double> (0, 1) * amp[684] - amp[685] - amp[713] -
      amp[714] + amp[717] - amp[715] - amp[718] - amp[721] - amp[720] +
      amp[723] + amp[724] - amp[731] - amp[730] - Complex<double> (0, 1) *
      amp[755] + Complex<double> (0, 1) * amp[756] + Complex<double> (0, 1) *
      amp[757] - Complex<double> (0, 1) * amp[813] - Complex<double> (0, 1) *
      amp[817] - amp[818] - Complex<double> (0, 1) * amp[819] - Complex<double>
      (0, 1) * amp[820] + amp[829] - Complex<double> (0, 1) * amp[830] +
      amp[831] + Complex<double> (0, 1) * amp[832] - Complex<double> (0, 1) *
      amp[836] - amp[839] - amp[838] - amp[841] - amp[842] - amp[877] -
      amp[880] - amp[879] - amp[888] - amp[889] - amp[893] - amp[892] -
      amp[895] - amp[896] + Complex<double> (0, 1) * amp[941] - Complex<double>
      (0, 1) * amp[947] + Complex<double> (0, 1) * amp[945] - amp[953] +
      amp[951] - amp[1034] - Complex<double> (0, 1) * amp[1039] - amp[1040] +
      Complex<double> (0, 1) * amp[1043] + Complex<double> (0, 1) * amp[1049] +
      Complex<double> (0, 1) * amp[1048] - amp[1084] - amp[1086] - amp[1087] +
      amp[1095] + amp[1096] - Complex<double> (0, 1) * amp[1118] + amp[1119] -
      Complex<double> (0, 1) * amp[1121] - Complex<double> (0, 1) * amp[1122] -
      Complex<double> (0, 1) * amp[1123] + amp[1313] + amp[1314] - amp[1317] +
      amp[1315] - amp[1319] - amp[1323] - amp[1324] + amp[1326] + amp[1327] +
      amp[1331] + amp[1330] + amp[1449] + Complex<double> (0, 1) * amp[1452] -
      Complex<double> (0, 1) * amp[1454] - Complex<double> (0, 1) * amp[1455] -
      Complex<double> (0, 1) * amp[1456] - Complex<double> (0, 1) * amp[1659] -
      Complex<double> (0, 1) * amp[1660] - amp[1662] - amp[1663] - amp[1676] +
      amp[1677] + amp[1674] - amp[1679] + amp[1683] + amp[1684] - amp[1686] -
      amp[1687] + Complex<double> (0, 1) * amp[1689] + Complex<double> (0, 1) *
      amp[1690] + amp[1766] + amp[1765] - Complex<double> (0, 1) * amp[1769] -
      Complex<double> (0, 1) * amp[1768] + Complex<double> (0, 1) * amp[1774] +
      Complex<double> (0, 1) * amp[1775] + amp[1780] + amp[1781] - amp[1790] -
      amp[1786] - amp[1789] - amp[1787] - amp[1796] - amp[1795] + amp[1811] -
      amp[1817] - amp[1809] + amp[1815] + amp[1820] - amp[1818] +
      Complex<double> (0, 1) * amp[1826] - Complex<double> (0, 1) * amp[1824] -
      amp[1829] + amp[1827];
  jamp[102] = -amp[0] - amp[1] - amp[3] - amp[2] - amp[11] - amp[14] - amp[13]
      - amp[17] + amp[15] - amp[20] - amp[19] + amp[26] + amp[25] + amp[28] +
      amp[29] - Complex<double> (0, 1) * amp[83] - Complex<double> (0, 1) *
      amp[89] + Complex<double> (0, 1) * amp[87] - Complex<double> (0, 1) *
      amp[93] - amp[95] - amp[96] + Complex<double> (0, 1) * amp[98] +
      Complex<double> (0, 1) * amp[113] + Complex<double> (0, 1) * amp[115] +
      amp[116] + Complex<double> (0, 1) * amp[119] + Complex<double> (0, 1) *
      amp[118] - Complex<double> (0, 1) * amp[140] + Complex<double> (0, 1) *
      amp[535] - amp[539] + amp[537] + amp[540] - amp[542] - Complex<double>
      (0, 1) * amp[546] - amp[548] - amp[549] - amp[550] - amp[552] - amp[551]
      + amp[555] - amp[558] - amp[557] - amp[566] - amp[565] - amp[569] +
      amp[567] - Complex<double> (0, 1) * amp[591] + amp[595] + Complex<double>
      (0, 1) * amp[596] - Complex<double> (0, 1) * amp[599] - Complex<double>
      (0, 1) * amp[598] - amp[600] + Complex<double> (0, 1) * amp[602] +
      Complex<double> (0, 1) * amp[603] + Complex<double> (0, 1) * amp[611] -
      Complex<double> (0, 1) * amp[609] - amp[1231] - amp[1234] - amp[1233] -
      amp[1244] - amp[1243] - amp[1256] + amp[1254] + amp[1257] - amp[1259] +
      Complex<double> (0, 1) * amp[1293] + amp[1294] - Complex<double> (0, 1) *
      amp[1297] + Complex<double> (0, 1) * amp[1301] + Complex<double> (0, 1) *
      amp[1300] - Complex<double> (0, 1) * amp[1303] + Complex<double> (0, 1) *
      amp[1304] - amp[1306] + amp[1314] - amp[1317] - amp[1316] - amp[1325] -
      amp[1324] - amp[1447] + amp[1451] + Complex<double> (0, 1) * amp[1452] +
      Complex<double> (0, 1) * amp[1453] - Complex<double> (0, 1) * amp[1457] -
      Complex<double> (0, 1) * amp[1456] - amp[1493] - amp[1489] - amp[1492] -
      amp[1490] - amp[1496] - amp[1495] - amp[1499] - amp[1498] -
      Complex<double> (0, 1) * amp[1517] - Complex<double> (0, 1) * amp[1516] +
      amp[1520] + amp[1519] + Complex<double> (0, 1) * amp[1526] +
      Complex<double> (0, 1) * amp[1525] + amp[1594] + amp[1593] - amp[1600] -
      amp[1599] - amp[1604] + amp[1602] + amp[1608] - amp[1610] - amp[1637] +
      amp[1635] + Complex<double> (0, 1) * amp[1640] - Complex<double> (0, 1) *
      amp[1638] - Complex<double> (0, 1) * amp[1641] + Complex<double> (0, 1) *
      amp[1643] + Complex<double> (0, 1) * amp[1668] - Complex<double> (0, 1) *
      amp[1670] - amp[1671] + amp[1673] + amp[1675] + amp[1674] - amp[1681] -
      amp[1680] - amp[1685] + amp[1683] - Complex<double> (0, 1) * amp[1691] +
      Complex<double> (0, 1) * amp[1689] + amp[1692] - amp[1694];
  jamp[103] = -amp[5] - amp[6] - amp[8] - amp[7] - amp[10] + amp[14] - amp[12]
      - amp[15] - amp[16] - amp[23] - amp[22] - amp[26] - amp[25] - amp[28] -
      amp[29] - Complex<double> (0, 1) * amp[85] - Complex<double> (0, 1) *
      amp[87] - Complex<double> (0, 1) * amp[88] - Complex<double> (0, 1) *
      amp[90] - amp[92] + amp[96] - Complex<double> (0, 1) * amp[98] +
      Complex<double> (0, 1) * amp[101] + Complex<double> (0, 1) * amp[103] +
      amp[104] + Complex<double> (0, 1) * amp[107] + Complex<double> (0, 1) *
      amp[106] + Complex<double> (0, 1) * amp[140] + Complex<double> (0, 1) *
      amp[694] - amp[698] + amp[696] + amp[699] - amp[701] - Complex<double>
      (0, 1) * amp[705] - amp[707] - amp[708] - amp[709] - amp[711] - amp[710]
      + amp[714] - amp[717] - amp[716] - amp[725] - amp[724] - amp[728] +
      amp[726] - Complex<double> (0, 1) * amp[750] + amp[754] + Complex<double>
      (0, 1) * amp[755] - Complex<double> (0, 1) * amp[758] - Complex<double>
      (0, 1) * amp[757] - amp[759] + Complex<double> (0, 1) * amp[761] +
      Complex<double> (0, 1) * amp[762] + Complex<double> (0, 1) * amp[770] -
      Complex<double> (0, 1) * amp[768] - amp[1230] + amp[1234] - amp[1232] -
      amp[1247] - amp[1246] + amp[1256] - amp[1254] - amp[1257] + amp[1259] +
      Complex<double> (0, 1) * amp[1284] + amp[1285] - Complex<double> (0, 1) *
      amp[1288] + Complex<double> (0, 1) * amp[1292] + Complex<double> (0, 1) *
      amp[1291] + Complex<double> (0, 1) * amp[1303] - Complex<double> (0, 1) *
      amp[1304] + amp[1306] - amp[1314] + amp[1317] + amp[1316] + amp[1325] +
      amp[1324] - amp[1441] - amp[1451] - Complex<double> (0, 1) * amp[1452] -
      Complex<double> (0, 1) * amp[1453] + Complex<double> (0, 1) * amp[1457] +
      Complex<double> (0, 1) * amp[1456] - amp[1547] - amp[1543] - amp[1546] -
      amp[1544] - amp[1550] - amp[1549] - amp[1553] - amp[1552] -
      Complex<double> (0, 1) * amp[1571] - Complex<double> (0, 1) * amp[1570] +
      amp[1574] + amp[1573] + Complex<double> (0, 1) * amp[1580] +
      Complex<double> (0, 1) * amp[1579] + amp[1595] - amp[1601] - amp[1593] +
      amp[1599] - amp[1607] + amp[1605] - amp[1608] + amp[1610] - amp[1631] +
      amp[1629] + Complex<double> (0, 1) * amp[1634] - Complex<double> (0, 1) *
      amp[1632] + Complex<double> (0, 1) * amp[1641] - Complex<double> (0, 1) *
      amp[1643] - Complex<double> (0, 1) * amp[1668] - Complex<double> (0, 1) *
      amp[1669] + amp[1671] + amp[1672] - amp[1677] - amp[1675] - amp[1678] -
      amp[1674] - amp[1683] - amp[1684] - Complex<double> (0, 1) * amp[1689] -
      Complex<double> (0, 1) * amp[1690] - amp[1692] - amp[1693];
  jamp[104] = -Complex<double> (0, 1) * amp[535] + amp[539] - amp[537] -
      amp[540] + amp[542] + Complex<double> (0, 1) * amp[546] + amp[548] +
      amp[549] + amp[550] + amp[552] + amp[551] - amp[555] + amp[558] +
      amp[557] + amp[566] + amp[565] + amp[569] - amp[567] + Complex<double>
      (0, 1) * amp[591] - amp[595] - Complex<double> (0, 1) * amp[596] +
      Complex<double> (0, 1) * amp[599] + Complex<double> (0, 1) * amp[598] +
      amp[600] - Complex<double> (0, 1) * amp[602] - Complex<double> (0, 1) *
      amp[603] - Complex<double> (0, 1) * amp[611] + Complex<double> (0, 1) *
      amp[609] + Complex<double> (0, 1) * amp[675] + amp[679] + amp[678] +
      amp[681] + amp[682] + Complex<double> (0, 1) * amp[684] + amp[686] -
      amp[692] + Complex<double> (0, 1) * amp[693] - Complex<double> (0, 1) *
      amp[694] + amp[698] + amp[697] + amp[700] + amp[701] - amp[714] +
      amp[717] + amp[716] + amp[723] + amp[724] + Complex<double> (0, 1) *
      amp[752] - amp[754] - Complex<double> (0, 1) * amp[755] + Complex<double>
      (0, 1) * amp[756] + Complex<double> (0, 1) * amp[757] - Complex<double>
      (0, 1) * amp[791] - Complex<double> (0, 1) * amp[790] + amp[794] +
      amp[793] - amp[1074] - amp[1075] - amp[1077] - amp[1076] + amp[1085] -
      amp[1088] - amp[1087] - amp[1091] + amp[1089] - amp[1092] - amp[1093] -
      Complex<double> (0, 1) * amp[1116] - Complex<double> (0, 1) * amp[1124] -
      Complex<double> (0, 1) * amp[1123] - amp[1125] + Complex<double> (0, 1) *
      amp[1127] + Complex<double> (0, 1) * amp[1128] - amp[1131] -
      Complex<double> (0, 1) * amp[1133] + Complex<double> (0, 1) * amp[1136] -
      Complex<double> (0, 1) * amp[1134] + amp[1240] + amp[1242] + amp[1243] -
      amp[1251] - amp[1252] - Complex<double> (0, 1) * amp[1295] - amp[1296] -
      Complex<double> (0, 1) * amp[1298] - Complex<double> (0, 1) * amp[1299] -
      Complex<double> (0, 1) * amp[1300] - amp[1445] + amp[1487] + amp[1486] +
      amp[1489] + amp[1490] + amp[1494] + amp[1495] + amp[1497] + amp[1498] +
      Complex<double> (0, 1) * amp[1515] + Complex<double> (0, 1) * amp[1516] -
      amp[1518] - amp[1519] - Complex<double> (0, 1) * amp[1524] -
      Complex<double> (0, 1) * amp[1525] + Complex<double> (0, 1) * amp[1669] +
      Complex<double> (0, 1) * amp[1670] - amp[1672] - amp[1673] + amp[1677] +
      amp[1678] + amp[1681] + amp[1680] + amp[1685] + amp[1684] +
      Complex<double> (0, 1) * amp[1691] + Complex<double> (0, 1) * amp[1690] +
      amp[1693] + amp[1694] - amp[1839] + amp[1844] + amp[1841] - amp[1842] -
      amp[1847] - amp[1846] + Complex<double> (0, 1) * amp[1859] +
      Complex<double> (0, 1) * amp[1858] + amp[1862] + amp[1861];
  jamp[105] = +Complex<double> (0, 1) * amp[336] + Complex<double> (0, 1) *
      amp[337] + Complex<double> (0, 1) * amp[339] + Complex<double> (0, 1) *
      amp[338] - amp[342] + amp[349] + Complex<double> (0, 1) * amp[350] +
      Complex<double> (0, 1) * amp[372] - Complex<double> (0, 1) * amp[376] +
      Complex<double> (0, 1) * amp[374] - Complex<double> (0, 1) * amp[379] -
      amp[380] + Complex<double> (0, 1) * amp[388] - amp[389] - amp[412] +
      amp[415] + Complex<double> (0, 1) * amp[416] + Complex<double> (0, 1) *
      amp[419] + Complex<double> (0, 1) * amp[422] + Complex<double> (0, 1) *
      amp[421] + Complex<double> (0, 1) * amp[444] - Complex<double> (0, 1) *
      amp[446] - amp[452] + amp[450] - Complex<double> (0, 1) * amp[454] -
      Complex<double> (0, 1) * amp[455] + amp[458] + amp[457] - Complex<double>
      (0, 1) * amp[675] - amp[679] - amp[678] - amp[681] - amp[682] -
      Complex<double> (0, 1) * amp[684] - amp[686] + amp[692] - Complex<double>
      (0, 1) * amp[693] + Complex<double> (0, 1) * amp[694] - amp[698] -
      amp[697] - amp[700] - amp[701] + amp[714] - amp[717] - amp[716] -
      amp[723] - amp[724] - Complex<double> (0, 1) * amp[752] + amp[754] +
      Complex<double> (0, 1) * amp[755] - Complex<double> (0, 1) * amp[756] -
      Complex<double> (0, 1) * amp[757] + Complex<double> (0, 1) * amp[791] +
      Complex<double> (0, 1) * amp[790] - amp[794] - amp[793] + amp[1079] +
      amp[1080] + amp[1082] + amp[1081] + amp[1084] + amp[1086] + amp[1087] +
      amp[1094] + amp[1093] + amp[1097] - amp[1095] + Complex<double> (0, 1) *
      amp[1121] + Complex<double> (0, 1) * amp[1122] + Complex<double> (0, 1) *
      amp[1123] + amp[1235] - amp[1239] + amp[1237] + amp[1253] + amp[1252] +
      amp[1256] + amp[1255] + amp[1258] + amp[1259] - Complex<double> (0, 1) *
      amp[1304] + amp[1308] + amp[1311] + amp[1310] - amp[1314] + amp[1317] +
      amp[1316] + amp[1319] + amp[1323] + amp[1324] + amp[1328] - amp[1326] -
      amp[1451] - Complex<double> (0, 1) * amp[1452] + Complex<double> (0, 1) *
      amp[1454] + Complex<double> (0, 1) * amp[1455] + Complex<double> (0, 1) *
      amp[1456] - Complex<double> (0, 1) * amp[1668] - Complex<double> (0, 1) *
      amp[1669] + amp[1671] + amp[1672] - amp[1677] - amp[1675] - amp[1678] -
      amp[1674] - amp[1683] - amp[1684] - Complex<double> (0, 1) * amp[1689] -
      Complex<double> (0, 1) * amp[1690] - amp[1692] - amp[1693] + amp[1810] +
      amp[1809] - amp[1816] - amp[1815] - amp[1820] + amp[1818] + amp[1821] -
      amp[1823] - Complex<double> (0, 1) * amp[1826] + Complex<double> (0, 1) *
      amp[1824] - amp[1844] - amp[1840] - amp[1843] - amp[1841] - amp[1849] -
      amp[1850];
  jamp[106] = +Complex<double> (0, 1) * amp[516] + amp[520] + amp[519] +
      amp[522] + amp[523] + Complex<double> (0, 1) * amp[525] + amp[527] -
      amp[533] + Complex<double> (0, 1) * amp[534] - Complex<double> (0, 1) *
      amp[535] + amp[539] + amp[538] + amp[541] + amp[542] - amp[555] +
      amp[558] + amp[557] + amp[564] + amp[565] + Complex<double> (0, 1) *
      amp[593] - amp[595] - Complex<double> (0, 1) * amp[596] + Complex<double>
      (0, 1) * amp[597] + Complex<double> (0, 1) * amp[598] - Complex<double>
      (0, 1) * amp[632] - Complex<double> (0, 1) * amp[631] + amp[635] +
      amp[634] - Complex<double> (0, 1) * amp[694] + amp[698] - amp[696] -
      amp[699] + amp[701] + Complex<double> (0, 1) * amp[705] + amp[707] +
      amp[708] + amp[709] + amp[711] + amp[710] - amp[714] + amp[717] +
      amp[716] + amp[725] + amp[724] + amp[728] - amp[726] + Complex<double>
      (0, 1) * amp[750] - amp[754] - Complex<double> (0, 1) * amp[755] +
      Complex<double> (0, 1) * amp[758] + Complex<double> (0, 1) * amp[757] +
      amp[759] - Complex<double> (0, 1) * amp[761] - Complex<double> (0, 1) *
      amp[762] - Complex<double> (0, 1) * amp[770] + Complex<double> (0, 1) *
      amp[768] - amp[1152] - amp[1153] - amp[1155] - amp[1154] + amp[1163] -
      amp[1166] - amp[1165] - amp[1169] + amp[1167] - amp[1170] - amp[1171] -
      Complex<double> (0, 1) * amp[1194] - Complex<double> (0, 1) * amp[1202] -
      Complex<double> (0, 1) * amp[1201] - amp[1203] + Complex<double> (0, 1) *
      amp[1205] + Complex<double> (0, 1) * amp[1206] - amp[1209] -
      Complex<double> (0, 1) * amp[1211] + Complex<double> (0, 1) * amp[1214] -
      Complex<double> (0, 1) * amp[1212] + amp[1241] + amp[1245] + amp[1246] -
      amp[1248] - amp[1249] - Complex<double> (0, 1) * amp[1286] - amp[1287] -
      Complex<double> (0, 1) * amp[1289] - Complex<double> (0, 1) * amp[1290] -
      Complex<double> (0, 1) * amp[1291] - amp[1439] + amp[1541] + amp[1540] +
      amp[1543] + amp[1544] + amp[1548] + amp[1549] + amp[1551] + amp[1552] +
      Complex<double> (0, 1) * amp[1569] + Complex<double> (0, 1) * amp[1570] -
      amp[1572] - amp[1573] - Complex<double> (0, 1) * amp[1578] -
      Complex<double> (0, 1) * amp[1579] + Complex<double> (0, 1) * amp[1669] +
      Complex<double> (0, 1) * amp[1670] - amp[1672] - amp[1673] + amp[1677] +
      amp[1678] + amp[1681] + amp[1680] + amp[1685] + amp[1684] +
      Complex<double> (0, 1) * amp[1691] + Complex<double> (0, 1) * amp[1690] +
      amp[1693] + amp[1694] - amp[1866] + amp[1871] + amp[1868] - amp[1869] -
      amp[1874] - amp[1873] + Complex<double> (0, 1) * amp[1886] +
      Complex<double> (0, 1) * amp[1885] + amp[1889] + amp[1888];
  jamp[107] = +Complex<double> (0, 1) * amp[354] + Complex<double> (0, 1) *
      amp[355] + Complex<double> (0, 1) * amp[357] + Complex<double> (0, 1) *
      amp[356] - amp[360] + amp[367] + Complex<double> (0, 1) * amp[368] +
      Complex<double> (0, 1) * amp[373] + Complex<double> (0, 1) * amp[376] +
      Complex<double> (0, 1) * amp[375] - Complex<double> (0, 1) * amp[377] -
      amp[378] - Complex<double> (0, 1) * amp[388] + amp[389] - amp[410] -
      amp[415] - Complex<double> (0, 1) * amp[416] - Complex<double> (0, 1) *
      amp[419] - Complex<double> (0, 1) * amp[422] - Complex<double> (0, 1) *
      amp[421] - Complex<double> (0, 1) * amp[444] - Complex<double> (0, 1) *
      amp[445] - amp[450] - amp[451] - Complex<double> (0, 1) * amp[463] -
      Complex<double> (0, 1) * amp[464] + amp[467] + amp[466] - Complex<double>
      (0, 1) * amp[516] - amp[520] - amp[519] - amp[522] - amp[523] -
      Complex<double> (0, 1) * amp[525] - amp[527] + amp[533] - Complex<double>
      (0, 1) * amp[534] + Complex<double> (0, 1) * amp[535] - amp[539] -
      amp[538] - amp[541] - amp[542] + amp[555] - amp[558] - amp[557] -
      amp[564] - amp[565] - Complex<double> (0, 1) * amp[593] + amp[595] +
      Complex<double> (0, 1) * amp[596] - Complex<double> (0, 1) * amp[597] -
      Complex<double> (0, 1) * amp[598] + Complex<double> (0, 1) * amp[632] +
      Complex<double> (0, 1) * amp[631] - amp[635] - amp[634] + amp[1157] +
      amp[1158] + amp[1160] + amp[1159] + amp[1162] + amp[1164] + amp[1165] +
      amp[1172] + amp[1171] + amp[1175] - amp[1173] + Complex<double> (0, 1) *
      amp[1199] + Complex<double> (0, 1) * amp[1200] + Complex<double> (0, 1) *
      amp[1201] + amp[1236] + amp[1239] + amp[1238] + amp[1250] + amp[1249] -
      amp[1256] - amp[1255] - amp[1258] - amp[1259] + Complex<double> (0, 1) *
      amp[1304] - amp[1308] - amp[1311] - amp[1310] + amp[1314] - amp[1317] -
      amp[1316] - amp[1319] - amp[1323] - amp[1324] - amp[1328] + amp[1326] +
      amp[1451] + Complex<double> (0, 1) * amp[1452] - Complex<double> (0, 1) *
      amp[1454] - Complex<double> (0, 1) * amp[1455] - Complex<double> (0, 1) *
      amp[1456] + Complex<double> (0, 1) * amp[1668] - Complex<double> (0, 1) *
      amp[1670] - amp[1671] + amp[1673] + amp[1675] + amp[1674] - amp[1681] -
      amp[1680] - amp[1685] + amp[1683] - Complex<double> (0, 1) * amp[1691] +
      Complex<double> (0, 1) * amp[1689] + amp[1692] - amp[1694] - amp[1812] -
      amp[1810] - amp[1813] - amp[1809] - amp[1818] - amp[1819] - amp[1821] -
      amp[1822] - Complex<double> (0, 1) * amp[1824] - Complex<double> (0, 1) *
      amp[1825] - amp[1871] - amp[1867] - amp[1870] - amp[1868] - amp[1876] -
      amp[1877];
  jamp[108] = -amp[0] - amp[1] - amp[3] - amp[2] - amp[6] - amp[9] - amp[8] -
      amp[17] - amp[16] - amp[20] + amp[18] + amp[32] + amp[31] + amp[34] +
      amp[35] - Complex<double> (0, 1) * amp[83] - Complex<double> (0, 1) *
      amp[85] + amp[86] - Complex<double> (0, 1) * amp[89] - Complex<double>
      (0, 1) * amp[88] + Complex<double> (0, 1) * amp[94] - amp[95] +
      Complex<double> (0, 1) * amp[113] + Complex<double> (0, 1) * amp[119] -
      Complex<double> (0, 1) * amp[117] - amp[121] - Complex<double> (0, 1) *
      amp[122] + Complex<double> (0, 1) * amp[145] - Complex<double> (0, 1) *
      amp[518] - amp[521] + amp[519] + amp[522] - amp[524] + Complex<double>
      (0, 1) * amp[525] - amp[526] - amp[549] - amp[550] - amp[552] - amp[551]
      + amp[560] - amp[563] - amp[562] - amp[566] + amp[564] - amp[569] -
      amp[568] - Complex<double> (0, 1) * amp[591] - Complex<double> (0, 1) *
      amp[599] + Complex<double> (0, 1) * amp[597] - amp[600] - Complex<double>
      (0, 1) * amp[601] + Complex<double> (0, 1) * amp[603] + amp[607] -
      Complex<double> (0, 1) * amp[608] + Complex<double> (0, 1) * amp[611] +
      Complex<double> (0, 1) * amp[610] - amp[1153] - amp[1156] - amp[1155] -
      amp[1166] - amp[1165] - amp[1178] + amp[1176] + amp[1179] - amp[1181] -
      Complex<double> (0, 1) * amp[1194] + amp[1195] + Complex<double> (0, 1) *
      amp[1198] - Complex<double> (0, 1) * amp[1202] - Complex<double> (0, 1) *
      amp[1201] + Complex<double> (0, 1) * amp[1224] - Complex<double> (0, 1) *
      amp[1227] - amp[1229] + amp[1338] - amp[1341] - amp[1340] - amp[1349] -
      amp[1348] - amp[1448] + amp[1469] - Complex<double> (0, 1) * amp[1470] -
      Complex<double> (0, 1) * amp[1471] + Complex<double> (0, 1) * amp[1475] +
      Complex<double> (0, 1) * amp[1474] - amp[1493] - amp[1489] - amp[1492] -
      amp[1490] - amp[1496] - amp[1495] - amp[1499] - amp[1498] -
      Complex<double> (0, 1) * amp[1517] - Complex<double> (0, 1) * amp[1516] +
      amp[1520] + amp[1519] + Complex<double> (0, 1) * amp[1526] +
      Complex<double> (0, 1) * amp[1525] + amp[1540] + amp[1539] - amp[1546] -
      amp[1545] - amp[1550] + amp[1548] + amp[1554] - amp[1556] + amp[1566] -
      amp[1568] - Complex<double> (0, 1) * amp[1571] + Complex<double> (0, 1) *
      amp[1569] + Complex<double> (0, 1) * amp[1590] - Complex<double> (0, 1) *
      amp[1592] - Complex<double> (0, 1) * amp[1719] + Complex<double> (0, 1) *
      amp[1721] - amp[1725] + amp[1727] + amp[1729] + amp[1728] - amp[1735] -
      amp[1734] - amp[1739] + amp[1737] + Complex<double> (0, 1) * amp[1751] -
      Complex<double> (0, 1) * amp[1749] - amp[1754] + amp[1752];
  jamp[109] = +Complex<double> (0, 1) * amp[497] + Complex<double> (0, 1) *
      amp[499] + amp[500] + Complex<double> (0, 1) * amp[503] + Complex<double>
      (0, 1) * amp[502] - Complex<double> (0, 1) * amp[508] - amp[509] +
      Complex<double> (0, 1) * amp[518] + amp[521] - amp[519] - amp[522] +
      amp[524] - Complex<double> (0, 1) * amp[525] + amp[526] + amp[554] +
      amp[555] - amp[558] + amp[556] + amp[559] + amp[562] + amp[561] -
      amp[564] - amp[565] + amp[572] + amp[571] + Complex<double> (0, 1) *
      amp[596] - Complex<double> (0, 1) * amp[597] - Complex<double> (0, 1) *
      amp[598] + Complex<double> (0, 1) * amp[656] + Complex<double> (0, 1) *
      amp[662] - Complex<double> (0, 1) * amp[660] + Complex<double> (0, 1) *
      amp[666] - amp[668] - amp[669] - Complex<double> (0, 1) * amp[671] -
      amp[709] - amp[712] - amp[711] + amp[713] + amp[714] - amp[717] +
      amp[715] - amp[725] - amp[724] + amp[731] - amp[729] - amp[734] +
      amp[732] + amp[735] - amp[737] - Complex<double> (0, 1) * amp[750] +
      amp[751] + Complex<double> (0, 1) * amp[755] - Complex<double> (0, 1) *
      amp[758] - Complex<double> (0, 1) * amp[757] + Complex<double> (0, 1) *
      amp[780] - amp[1037] + Complex<double> (0, 1) * amp[1052] + amp[1053] -
      Complex<double> (0, 1) * amp[1055] + Complex<double> (0, 1) * amp[1058] +
      Complex<double> (0, 1) * amp[1057] + amp[1153] + amp[1156] + amp[1155] +
      amp[1166] + amp[1165] + amp[1178] - amp[1176] - amp[1179] + amp[1181] +
      Complex<double> (0, 1) * amp[1194] - amp[1195] - Complex<double> (0, 1) *
      amp[1198] + Complex<double> (0, 1) * amp[1202] + Complex<double> (0, 1) *
      amp[1201] - Complex<double> (0, 1) * amp[1224] + Complex<double> (0, 1) *
      amp[1227] + amp[1229] + amp[1337] + amp[1340] + amp[1339] + amp[1355] +
      amp[1354] - amp[1542] - amp[1540] - amp[1543] - amp[1539] - amp[1548] -
      amp[1549] - amp[1554] - amp[1555] - amp[1566] - amp[1567] -
      Complex<double> (0, 1) * amp[1569] - Complex<double> (0, 1) * amp[1570] -
      Complex<double> (0, 1) * amp[1590] - Complex<double> (0, 1) * amp[1591] +
      Complex<double> (0, 1) * amp[1661] + Complex<double> (0, 1) * amp[1660] +
      amp[1664] + amp[1663] - amp[1677] + amp[1682] + amp[1679] - amp[1680] -
      amp[1685] - amp[1684] + amp[1688] + amp[1687] - Complex<double> (0, 1) *
      amp[1691] - Complex<double> (0, 1) * amp[1690] - amp[1712] + amp[1710] +
      Complex<double> (0, 1) * amp[1715] - Complex<double> (0, 1) * amp[1713] +
      Complex<double> (0, 1) * amp[1719] - Complex<double> (0, 1) * amp[1721] +
      amp[1725] - amp[1727] - amp[1730] + amp[1736] - amp[1729] + amp[1735] +
      amp[1742] - amp[1740];
  jamp[110] = +amp[0] + amp[1] + amp[3] + amp[2] + amp[6] + amp[9] + amp[8] +
      amp[17] + amp[16] + amp[20] - amp[18] - amp[32] - amp[31] - amp[34] -
      amp[35] + Complex<double> (0, 1) * amp[83] + Complex<double> (0, 1) *
      amp[85] - amp[86] + Complex<double> (0, 1) * amp[89] + Complex<double>
      (0, 1) * amp[88] - Complex<double> (0, 1) * amp[94] + amp[95] -
      Complex<double> (0, 1) * amp[113] - Complex<double> (0, 1) * amp[119] +
      Complex<double> (0, 1) * amp[117] + amp[121] + Complex<double> (0, 1) *
      amp[122] - Complex<double> (0, 1) * amp[145] - Complex<double> (0, 1) *
      amp[675] - amp[679] - amp[678] - amp[681] - amp[682] - Complex<double>
      (0, 1) * amp[684] - amp[686] + amp[709] + amp[712] + amp[711] + amp[725]
      - amp[723] - amp[733] - amp[732] - amp[735] - amp[736] + Complex<double>
      (0, 1) * amp[750] - amp[751] + Complex<double> (0, 1) * amp[753] +
      Complex<double> (0, 1) * amp[758] - Complex<double> (0, 1) * amp[756] -
      Complex<double> (0, 1) * amp[780] - Complex<double> (0, 1) * amp[784] -
      amp[785] + Complex<double> (0, 1) * amp[791] - Complex<double> (0, 1) *
      amp[789] - amp[794] + amp[792] + amp[1074] + amp[1075] + amp[1077] +
      amp[1076] - amp[1085] + amp[1088] + amp[1087] + amp[1091] - amp[1089] +
      amp[1092] + amp[1093] + Complex<double> (0, 1) * amp[1116] +
      Complex<double> (0, 1) * amp[1124] + Complex<double> (0, 1) * amp[1123] +
      amp[1125] - Complex<double> (0, 1) * amp[1127] - Complex<double> (0, 1) *
      amp[1128] + amp[1131] + Complex<double> (0, 1) * amp[1133] -
      Complex<double> (0, 1) * amp[1136] + Complex<double> (0, 1) * amp[1134] -
      amp[1343] + amp[1349] - amp[1347] + amp[1350] + amp[1351] - amp[1446] +
      amp[1467] - Complex<double> (0, 1) * amp[1468] + Complex<double> (0, 1) *
      amp[1472] - Complex<double> (0, 1) * amp[1475] + Complex<double> (0, 1) *
      amp[1473] - amp[1487] + amp[1493] - amp[1486] + amp[1492] + amp[1496] -
      amp[1494] + amp[1499] - amp[1497] + Complex<double> (0, 1) * amp[1517] -
      Complex<double> (0, 1) * amp[1515] - amp[1520] + amp[1518] -
      Complex<double> (0, 1) * amp[1526] + Complex<double> (0, 1) * amp[1524] +
      amp[1542] + amp[1543] + amp[1546] + amp[1545] + amp[1550] + amp[1549] +
      amp[1555] + amp[1556] + amp[1567] + amp[1568] + Complex<double> (0, 1) *
      amp[1571] + Complex<double> (0, 1) * amp[1570] + Complex<double> (0, 1) *
      amp[1591] + Complex<double> (0, 1) * amp[1592] + amp[1838] - amp[1844] -
      amp[1836] + amp[1842] + amp[1847] - amp[1845] - Complex<double> (0, 1) *
      amp[1859] + Complex<double> (0, 1) * amp[1857] - amp[1862] + amp[1860];
  jamp[111] = -Complex<double> (0, 1) * amp[336] - Complex<double> (0, 1) *
      amp[337] - Complex<double> (0, 1) * amp[339] - Complex<double> (0, 1) *
      amp[338] + amp[342] - amp[349] - Complex<double> (0, 1) * amp[350] -
      Complex<double> (0, 1) * amp[354] + Complex<double> (0, 1) * amp[358] -
      Complex<double> (0, 1) * amp[356] + Complex<double> (0, 1) * amp[366] -
      amp[367] - amp[369] - Complex<double> (0, 1) * amp[370] - amp[413] -
      Complex<double> (0, 1) * amp[428] - Complex<double> (0, 1) * amp[431] -
      Complex<double> (0, 1) * amp[430] + amp[433] - Complex<double> (0, 1) *
      amp[434] + Complex<double> (0, 1) * amp[445] + Complex<double> (0, 1) *
      amp[446] + amp[452] + amp[451] - Complex<double> (0, 1) * amp[453] +
      Complex<double> (0, 1) * amp[455] - amp[458] + amp[456] + Complex<double>
      (0, 1) * amp[675] + amp[679] + amp[678] + amp[681] + amp[682] +
      Complex<double> (0, 1) * amp[684] + amp[686] - amp[709] - amp[712] -
      amp[711] - amp[725] + amp[723] + amp[733] + amp[732] + amp[735] +
      amp[736] - Complex<double> (0, 1) * amp[750] + amp[751] - Complex<double>
      (0, 1) * amp[753] - Complex<double> (0, 1) * amp[758] + Complex<double>
      (0, 1) * amp[756] + Complex<double> (0, 1) * amp[780] + Complex<double>
      (0, 1) * amp[784] + amp[785] - Complex<double> (0, 1) * amp[791] +
      Complex<double> (0, 1) * amp[789] + amp[794] - amp[792] - amp[1079] -
      amp[1080] - amp[1082] - amp[1081] - amp[1084] - amp[1086] - amp[1087] -
      amp[1094] - amp[1093] - amp[1097] + amp[1095] - Complex<double> (0, 1) *
      amp[1121] - Complex<double> (0, 1) * amp[1122] - Complex<double> (0, 1) *
      amp[1123] + amp[1153] + amp[1156] + amp[1155] - amp[1157] + amp[1161] -
      amp[1159] - amp[1162] + amp[1166] - amp[1164] - amp[1175] + amp[1173] -
      amp[1177] - amp[1176] - amp[1179] - amp[1180] + Complex<double> (0, 1) *
      amp[1194] - amp[1195] - Complex<double> (0, 1) * amp[1199] +
      Complex<double> (0, 1) * amp[1202] - Complex<double> (0, 1) * amp[1200] -
      Complex<double> (0, 1) * amp[1224] - amp[1332] - amp[1335] - amp[1334] -
      amp[1352] - amp[1351] - amp[1542] - amp[1540] - amp[1543] - amp[1539] -
      amp[1548] - amp[1549] - amp[1554] - amp[1555] - amp[1566] - amp[1567] -
      Complex<double> (0, 1) * amp[1569] - Complex<double> (0, 1) * amp[1570] -
      Complex<double> (0, 1) * amp[1590] - Complex<double> (0, 1) * amp[1591] +
      amp[1812] + amp[1813] + amp[1816] + amp[1815] + amp[1820] + amp[1819] +
      amp[1822] + amp[1823] + Complex<double> (0, 1) * amp[1826] +
      Complex<double> (0, 1) * amp[1825] - amp[1838] + amp[1844] - amp[1837] +
      amp[1843] - amp[1848] + amp[1850];
  jamp[112] = +amp[6] + amp[9] + amp[8] + amp[15] + amp[16] + amp[25] + amp[24]
      + amp[27] + amp[28] - amp[32] + amp[30] + amp[33] - amp[35] - amp[44] -
      amp[43] + Complex<double> (0, 1) * amp[81] + Complex<double> (0, 1) *
      amp[85] - amp[86] + Complex<double> (0, 1) * amp[87] + Complex<double>
      (0, 1) * amp[88] + amp[97] + Complex<double> (0, 1) * amp[98] +
      Complex<double> (0, 1) * amp[138] - amp[143] + Complex<double> (0, 1) *
      amp[144] - Complex<double> (0, 1) * amp[145] - Complex<double> (0, 1) *
      amp[158] - Complex<double> (0, 1) * amp[157] - Complex<double> (0, 1) *
      amp[656] - Complex<double> (0, 1) * amp[662] + Complex<double> (0, 1) *
      amp[660] - Complex<double> (0, 1) * amp[666] + amp[668] + amp[669] +
      Complex<double> (0, 1) * amp[671] + amp[709] + amp[712] + amp[711] -
      amp[713] - amp[714] + amp[717] - amp[715] + amp[725] + amp[724] -
      amp[731] + amp[729] + amp[734] - amp[732] - amp[735] + amp[737] +
      Complex<double> (0, 1) * amp[750] - amp[751] - Complex<double> (0, 1) *
      amp[755] + Complex<double> (0, 1) * amp[758] + Complex<double> (0, 1) *
      amp[757] - Complex<double> (0, 1) * amp[780] - amp[1032] +
      Complex<double> (0, 1) * amp[1038] - amp[1040] - amp[1042] +
      Complex<double> (0, 1) * amp[1043] + Complex<double> (0, 1) * amp[1045] +
      Complex<double> (0, 1) * amp[1049] - Complex<double> (0, 1) * amp[1047] -
      Complex<double> (0, 1) * amp[1050] - amp[1051] + Complex<double> (0, 1) *
      amp[1054] - Complex<double> (0, 1) * amp[1056] - Complex<double> (0, 1) *
      amp[1057] + amp[1070] + amp[1069] + Complex<double> (0, 1) * amp[1073] +
      Complex<double> (0, 1) * amp[1072] + amp[1313] + amp[1314] - amp[1317] +
      amp[1315] + amp[1318] - amp[1322] + amp[1320] - amp[1325] - amp[1324] +
      amp[1331] - amp[1329] + amp[1342] - amp[1346] + amp[1344] - amp[1353] -
      amp[1354] - amp[1386] + amp[1385] + amp[1388] - amp[1383] + amp[1394] +
      amp[1393] + Complex<double> (0, 1) * amp[1452] - Complex<double> (0, 1) *
      amp[1457] - Complex<double> (0, 1) * amp[1456] + amp[1542] + amp[1543] +
      amp[1546] + amp[1545] + amp[1550] + amp[1549] + amp[1555] + amp[1556] +
      amp[1567] + amp[1568] + Complex<double> (0, 1) * amp[1571] +
      Complex<double> (0, 1) * amp[1570] + Complex<double> (0, 1) * amp[1591] +
      Complex<double> (0, 1) * amp[1592] - Complex<double> (0, 1) * amp[1659] -
      Complex<double> (0, 1) * amp[1660] - amp[1662] - amp[1663] - amp[1676] +
      amp[1677] + amp[1674] - amp[1679] + amp[1683] + amp[1684] - amp[1686] -
      amp[1687] + Complex<double> (0, 1) * amp[1689] + Complex<double> (0, 1) *
      amp[1690];
  jamp[113] = -amp[6] - amp[9] - amp[8] - amp[15] - amp[16] - amp[25] - amp[24]
      - amp[27] - amp[28] + amp[32] - amp[30] - amp[33] + amp[35] + amp[44] +
      amp[43] - Complex<double> (0, 1) * amp[81] - Complex<double> (0, 1) *
      amp[85] + amp[86] - Complex<double> (0, 1) * amp[87] - Complex<double>
      (0, 1) * amp[88] - amp[97] - Complex<double> (0, 1) * amp[98] -
      Complex<double> (0, 1) * amp[138] + amp[143] - Complex<double> (0, 1) *
      amp[144] + Complex<double> (0, 1) * amp[145] + Complex<double> (0, 1) *
      amp[158] + Complex<double> (0, 1) * amp[157] + Complex<double> (0, 1) *
      amp[354] - Complex<double> (0, 1) * amp[358] + Complex<double> (0, 1) *
      amp[356] - Complex<double> (0, 1) * amp[366] + amp[367] + amp[369] +
      Complex<double> (0, 1) * amp[370] - amp[408] + Complex<double> (0, 1) *
      amp[414] - amp[415] - amp[418] - Complex<double> (0, 1) * amp[419] -
      Complex<double> (0, 1) * amp[420] + Complex<double> (0, 1) * amp[423] -
      Complex<double> (0, 1) * amp[421] - Complex<double> (0, 1) * amp[426] -
      amp[427] - Complex<double> (0, 1) * amp[429] + Complex<double> (0, 1) *
      amp[432] + Complex<double> (0, 1) * amp[431] - Complex<double> (0, 1) *
      amp[444] - Complex<double> (0, 1) * amp[445] - amp[450] - amp[451] +
      amp[473] + amp[472] + Complex<double> (0, 1) * amp[475] + Complex<double>
      (0, 1) * amp[476] - amp[1153] - amp[1156] - amp[1155] + amp[1157] -
      amp[1161] + amp[1159] + amp[1162] - amp[1166] + amp[1164] + amp[1175] -
      amp[1173] + amp[1177] + amp[1176] + amp[1179] + amp[1180] -
      Complex<double> (0, 1) * amp[1194] + amp[1195] + Complex<double> (0, 1) *
      amp[1199] - Complex<double> (0, 1) * amp[1202] + Complex<double> (0, 1) *
      amp[1200] + Complex<double> (0, 1) * amp[1224] - amp[1308] - amp[1309] +
      amp[1312] - amp[1310] - amp[1319] + amp[1322] + amp[1321] + amp[1325] -
      amp[1323] - amp[1328] + amp[1326] - amp[1333] + amp[1336] + amp[1335] +
      amp[1346] + amp[1345] + amp[1386] + amp[1384] + amp[1383] + amp[1387] +
      amp[1390] + amp[1391] - Complex<double> (0, 1) * amp[1454] +
      Complex<double> (0, 1) * amp[1457] - Complex<double> (0, 1) * amp[1455] +
      amp[1540] + amp[1539] - amp[1546] - amp[1545] - amp[1550] + amp[1548] +
      amp[1554] - amp[1556] + amp[1566] - amp[1568] - Complex<double> (0, 1) *
      amp[1571] + Complex<double> (0, 1) * amp[1569] + Complex<double> (0, 1) *
      amp[1590] - Complex<double> (0, 1) * amp[1592] - amp[1812] - amp[1810] -
      amp[1813] - amp[1809] - amp[1818] - amp[1819] - amp[1821] - amp[1822] -
      Complex<double> (0, 1) * amp[1824] - Complex<double> (0, 1) * amp[1825];
  jamp[114] = -amp[1] - amp[4] - amp[3] - amp[5] - amp[6] - amp[8] - amp[7] -
      amp[17] - amp[16] - amp[23] + amp[21] + amp[38] + amp[37] + amp[40] +
      amp[41] - Complex<double> (0, 1) * amp[83] + amp[84] - Complex<double>
      (0, 1) * amp[85] - Complex<double> (0, 1) * amp[89] - Complex<double> (0,
      1) * amp[88] + Complex<double> (0, 1) * amp[91] - amp[92] +
      Complex<double> (0, 1) * amp[101] + Complex<double> (0, 1) * amp[107] -
      Complex<double> (0, 1) * amp[105] - amp[109] - Complex<double> (0, 1) *
      amp[110] + Complex<double> (0, 1) * amp[151] - Complex<double> (0, 1) *
      amp[677] - amp[680] + amp[678] + amp[681] - amp[683] + Complex<double>
      (0, 1) * amp[684] - amp[685] - amp[708] - amp[709] - amp[711] - amp[710]
      + amp[719] - amp[722] - amp[721] - amp[725] + amp[723] - amp[728] -
      amp[727] - Complex<double> (0, 1) * amp[750] - Complex<double> (0, 1) *
      amp[758] + Complex<double> (0, 1) * amp[756] - amp[759] - Complex<double>
      (0, 1) * amp[760] + Complex<double> (0, 1) * amp[762] + amp[766] -
      Complex<double> (0, 1) * amp[767] + Complex<double> (0, 1) * amp[770] +
      Complex<double> (0, 1) * amp[769] - amp[1075] - amp[1078] - amp[1077] -
      amp[1088] - amp[1087] - amp[1100] + amp[1098] + amp[1101] - amp[1103] -
      Complex<double> (0, 1) * amp[1116] + amp[1117] + Complex<double> (0, 1) *
      amp[1120] - Complex<double> (0, 1) * amp[1124] - Complex<double> (0, 1) *
      amp[1123] + Complex<double> (0, 1) * amp[1146] - Complex<double> (0, 1) *
      amp[1149] - amp[1151] + amp[1362] - amp[1365] - amp[1364] - amp[1373] -
      amp[1372] - amp[1442] + amp[1460] - Complex<double> (0, 1) * amp[1461] -
      Complex<double> (0, 1) * amp[1462] + Complex<double> (0, 1) * amp[1466] +
      Complex<double> (0, 1) * amp[1465] + amp[1486] + amp[1485] - amp[1492] -
      amp[1491] - amp[1496] + amp[1494] + amp[1500] - amp[1502] + amp[1512] -
      amp[1514] - Complex<double> (0, 1) * amp[1517] + Complex<double> (0, 1) *
      amp[1515] + Complex<double> (0, 1) * amp[1536] - Complex<double> (0, 1) *
      amp[1538] - amp[1547] - amp[1543] - amp[1546] - amp[1544] - amp[1550] -
      amp[1549] - amp[1553] - amp[1552] - Complex<double> (0, 1) * amp[1571] -
      Complex<double> (0, 1) * amp[1570] + amp[1574] + amp[1573] +
      Complex<double> (0, 1) * amp[1580] + Complex<double> (0, 1) * amp[1579] -
      Complex<double> (0, 1) * amp[1773] + Complex<double> (0, 1) * amp[1775] -
      amp[1779] + amp[1781] + amp[1783] + amp[1782] - amp[1789] - amp[1788] -
      amp[1793] + amp[1791] + Complex<double> (0, 1) * amp[1805] -
      Complex<double> (0, 1) * amp[1803] - amp[1808] + amp[1806];
  jamp[115] = +Complex<double> (0, 1) * amp[497] + Complex<double> (0, 1) *
      amp[503] - Complex<double> (0, 1) * amp[501] + Complex<double> (0, 1) *
      amp[507] - amp[509] - amp[510] - Complex<double> (0, 1) * amp[512] -
      amp[550] - amp[553] - amp[552] + amp[554] + amp[555] - amp[558] +
      amp[556] - amp[566] - amp[565] + amp[572] - amp[570] - amp[575] +
      amp[573] + amp[576] - amp[578] - Complex<double> (0, 1) * amp[591] +
      amp[592] + Complex<double> (0, 1) * amp[596] - Complex<double> (0, 1) *
      amp[599] - Complex<double> (0, 1) * amp[598] + Complex<double> (0, 1) *
      amp[621] + Complex<double> (0, 1) * amp[656] + Complex<double> (0, 1) *
      amp[658] + amp[659] + Complex<double> (0, 1) * amp[662] + Complex<double>
      (0, 1) * amp[661] - Complex<double> (0, 1) * amp[667] - amp[668] +
      Complex<double> (0, 1) * amp[677] + amp[680] - amp[678] - amp[681] +
      amp[683] - Complex<double> (0, 1) * amp[684] + amp[685] + amp[713] +
      amp[714] - amp[717] + amp[715] + amp[718] + amp[721] + amp[720] -
      amp[723] - amp[724] + amp[731] + amp[730] + Complex<double> (0, 1) *
      amp[755] - Complex<double> (0, 1) * amp[756] - Complex<double> (0, 1) *
      amp[757] - amp[1036] + Complex<double> (0, 1) * amp[1061] + amp[1062] -
      Complex<double> (0, 1) * amp[1064] + Complex<double> (0, 1) * amp[1067] +
      Complex<double> (0, 1) * amp[1066] + amp[1075] + amp[1078] + amp[1077] +
      amp[1088] + amp[1087] + amp[1100] - amp[1098] - amp[1101] + amp[1103] +
      Complex<double> (0, 1) * amp[1116] - amp[1117] - Complex<double> (0, 1) *
      amp[1120] + Complex<double> (0, 1) * amp[1124] + Complex<double> (0, 1) *
      amp[1123] - Complex<double> (0, 1) * amp[1146] + Complex<double> (0, 1) *
      amp[1149] + amp[1151] + amp[1361] + amp[1364] + amp[1363] + amp[1379] +
      amp[1378] - amp[1488] - amp[1486] - amp[1489] - amp[1485] - amp[1494] -
      amp[1495] - amp[1500] - amp[1501] - amp[1512] - amp[1513] -
      Complex<double> (0, 1) * amp[1515] - Complex<double> (0, 1) * amp[1516] -
      Complex<double> (0, 1) * amp[1536] - Complex<double> (0, 1) * amp[1537] +
      Complex<double> (0, 1) * amp[1661] + Complex<double> (0, 1) * amp[1660] +
      amp[1664] + amp[1663] - amp[1677] + amp[1682] + amp[1679] - amp[1680] -
      amp[1685] - amp[1684] + amp[1688] + amp[1687] - Complex<double> (0, 1) *
      amp[1691] - Complex<double> (0, 1) * amp[1690] - amp[1766] + amp[1764] +
      Complex<double> (0, 1) * amp[1769] - Complex<double> (0, 1) * amp[1767] +
      Complex<double> (0, 1) * amp[1773] - Complex<double> (0, 1) * amp[1775] +
      amp[1779] - amp[1781] - amp[1784] + amp[1790] - amp[1783] + amp[1789] +
      amp[1796] - amp[1794];
  jamp[116] = +amp[1] + amp[4] + amp[3] + amp[5] + amp[6] + amp[8] + amp[7] +
      amp[17] + amp[16] + amp[23] - amp[21] - amp[38] - amp[37] - amp[40] -
      amp[41] + Complex<double> (0, 1) * amp[83] - amp[84] + Complex<double>
      (0, 1) * amp[85] + Complex<double> (0, 1) * amp[89] + Complex<double> (0,
      1) * amp[88] - Complex<double> (0, 1) * amp[91] + amp[92] -
      Complex<double> (0, 1) * amp[101] - Complex<double> (0, 1) * amp[107] +
      Complex<double> (0, 1) * amp[105] + amp[109] + Complex<double> (0, 1) *
      amp[110] - Complex<double> (0, 1) * amp[151] - Complex<double> (0, 1) *
      amp[516] - amp[520] - amp[519] - amp[522] - amp[523] - Complex<double>
      (0, 1) * amp[525] - amp[527] + amp[550] + amp[553] + amp[552] + amp[566]
      - amp[564] - amp[574] - amp[573] - amp[576] - amp[577] + Complex<double>
      (0, 1) * amp[591] - amp[592] + Complex<double> (0, 1) * amp[594] +
      Complex<double> (0, 1) * amp[599] - Complex<double> (0, 1) * amp[597] -
      Complex<double> (0, 1) * amp[621] - Complex<double> (0, 1) * amp[625] -
      amp[626] + Complex<double> (0, 1) * amp[632] - Complex<double> (0, 1) *
      amp[630] - amp[635] + amp[633] + amp[1152] + amp[1153] + amp[1155] +
      amp[1154] - amp[1163] + amp[1166] + amp[1165] + amp[1169] - amp[1167] +
      amp[1170] + amp[1171] + Complex<double> (0, 1) * amp[1194] +
      Complex<double> (0, 1) * amp[1202] + Complex<double> (0, 1) * amp[1201] +
      amp[1203] - Complex<double> (0, 1) * amp[1205] - Complex<double> (0, 1) *
      amp[1206] + amp[1209] + Complex<double> (0, 1) * amp[1211] -
      Complex<double> (0, 1) * amp[1214] + Complex<double> (0, 1) * amp[1212] -
      amp[1367] + amp[1373] - amp[1371] + amp[1374] + amp[1375] - amp[1440] +
      amp[1458] - Complex<double> (0, 1) * amp[1459] + Complex<double> (0, 1) *
      amp[1463] - Complex<double> (0, 1) * amp[1466] + Complex<double> (0, 1) *
      amp[1464] + amp[1488] + amp[1489] + amp[1492] + amp[1491] + amp[1496] +
      amp[1495] + amp[1501] + amp[1502] + amp[1513] + amp[1514] +
      Complex<double> (0, 1) * amp[1517] + Complex<double> (0, 1) * amp[1516] +
      Complex<double> (0, 1) * amp[1537] + Complex<double> (0, 1) * amp[1538] -
      amp[1541] + amp[1547] - amp[1540] + amp[1546] + amp[1550] - amp[1548] +
      amp[1553] - amp[1551] + Complex<double> (0, 1) * amp[1571] -
      Complex<double> (0, 1) * amp[1569] - amp[1574] + amp[1572] -
      Complex<double> (0, 1) * amp[1580] + Complex<double> (0, 1) * amp[1578] +
      amp[1865] - amp[1871] - amp[1863] + amp[1869] + amp[1874] - amp[1872] -
      Complex<double> (0, 1) * amp[1886] + Complex<double> (0, 1) * amp[1884] -
      amp[1889] + amp[1887];
  jamp[117] = -Complex<double> (0, 1) * amp[336] + Complex<double> (0, 1) *
      amp[340] - Complex<double> (0, 1) * amp[338] + Complex<double> (0, 1) *
      amp[348] - amp[349] - amp[351] - Complex<double> (0, 1) * amp[352] -
      Complex<double> (0, 1) * amp[354] - Complex<double> (0, 1) * amp[355] -
      Complex<double> (0, 1) * amp[357] - Complex<double> (0, 1) * amp[356] +
      amp[360] - amp[367] - Complex<double> (0, 1) * amp[368] - amp[411] -
      Complex<double> (0, 1) * amp[437] - Complex<double> (0, 1) * amp[440] -
      Complex<double> (0, 1) * amp[439] + amp[442] - Complex<double> (0, 1) *
      amp[443] + Complex<double> (0, 1) * amp[445] + Complex<double> (0, 1) *
      amp[446] + amp[452] + amp[451] - Complex<double> (0, 1) * amp[462] +
      Complex<double> (0, 1) * amp[464] - amp[467] + amp[465] + Complex<double>
      (0, 1) * amp[516] + amp[520] + amp[519] + amp[522] + amp[523] +
      Complex<double> (0, 1) * amp[525] + amp[527] - amp[550] - amp[553] -
      amp[552] - amp[566] + amp[564] + amp[574] + amp[573] + amp[576] +
      amp[577] - Complex<double> (0, 1) * amp[591] + amp[592] - Complex<double>
      (0, 1) * amp[594] - Complex<double> (0, 1) * amp[599] + Complex<double>
      (0, 1) * amp[597] + Complex<double> (0, 1) * amp[621] + Complex<double>
      (0, 1) * amp[625] + amp[626] - Complex<double> (0, 1) * amp[632] +
      Complex<double> (0, 1) * amp[630] + amp[635] - amp[633] + amp[1075] +
      amp[1078] + amp[1077] - amp[1079] + amp[1083] - amp[1081] - amp[1084] +
      amp[1088] - amp[1086] - amp[1097] + amp[1095] - amp[1099] - amp[1098] -
      amp[1101] - amp[1102] + Complex<double> (0, 1) * amp[1116] - amp[1117] -
      Complex<double> (0, 1) * amp[1121] + Complex<double> (0, 1) * amp[1124] -
      Complex<double> (0, 1) * amp[1122] - Complex<double> (0, 1) * amp[1146] -
      amp[1157] - amp[1158] - amp[1160] - amp[1159] - amp[1162] - amp[1164] -
      amp[1165] - amp[1172] - amp[1171] - amp[1175] + amp[1173] -
      Complex<double> (0, 1) * amp[1199] - Complex<double> (0, 1) * amp[1200] -
      Complex<double> (0, 1) * amp[1201] - amp[1356] - amp[1359] - amp[1358] -
      amp[1376] - amp[1375] - amp[1488] - amp[1486] - amp[1489] - amp[1485] -
      amp[1494] - amp[1495] - amp[1500] - amp[1501] - amp[1512] - amp[1513] -
      Complex<double> (0, 1) * amp[1515] - Complex<double> (0, 1) * amp[1516] -
      Complex<double> (0, 1) * amp[1536] - Complex<double> (0, 1) * amp[1537] +
      amp[1812] + amp[1813] + amp[1816] + amp[1815] + amp[1820] + amp[1819] +
      amp[1822] + amp[1823] + Complex<double> (0, 1) * amp[1826] +
      Complex<double> (0, 1) * amp[1825] - amp[1865] + amp[1871] - amp[1864] +
      amp[1870] - amp[1875] + amp[1877];
  jamp[118] = +amp[1] + amp[4] + amp[3] + amp[17] - amp[15] - amp[25] - amp[24]
      - amp[27] - amp[28] - amp[37] - amp[36] - amp[39] - amp[40] + amp[44] -
      amp[42] + Complex<double> (0, 1) * amp[82] + Complex<double> (0, 1) *
      amp[83] - amp[84] + Complex<double> (0, 1) * amp[89] - Complex<double>
      (0, 1) * amp[87] - amp[97] - Complex<double> (0, 1) * amp[98] -
      Complex<double> (0, 1) * amp[138] - amp[149] - Complex<double> (0, 1) *
      amp[150] - Complex<double> (0, 1) * amp[151] + Complex<double> (0, 1) *
      amp[158] - Complex<double> (0, 1) * amp[156] - Complex<double> (0, 1) *
      amp[497] - Complex<double> (0, 1) * amp[503] + Complex<double> (0, 1) *
      amp[501] - Complex<double> (0, 1) * amp[507] + amp[509] + amp[510] +
      Complex<double> (0, 1) * amp[512] + amp[550] + amp[553] + amp[552] -
      amp[554] - amp[555] + amp[558] - amp[556] + amp[566] + amp[565] -
      amp[572] + amp[570] + amp[575] - amp[573] - amp[576] + amp[578] +
      Complex<double> (0, 1) * amp[591] - amp[592] - Complex<double> (0, 1) *
      amp[596] + Complex<double> (0, 1) * amp[599] + Complex<double> (0, 1) *
      amp[598] - Complex<double> (0, 1) * amp[621] - amp[1033] -
      Complex<double> (0, 1) * amp[1038] + amp[1040] + amp[1042] -
      Complex<double> (0, 1) * amp[1043] - Complex<double> (0, 1) * amp[1045] -
      Complex<double> (0, 1) * amp[1049] + Complex<double> (0, 1) * amp[1047] -
      Complex<double> (0, 1) * amp[1059] + amp[1060] - Complex<double> (0, 1) *
      amp[1063] - Complex<double> (0, 1) * amp[1067] + Complex<double> (0, 1) *
      amp[1065] - amp[1070] + amp[1068] - Complex<double> (0, 1) * amp[1073] +
      Complex<double> (0, 1) * amp[1071] - amp[1313] - amp[1314] + amp[1317] -
      amp[1315] - amp[1318] + amp[1322] - amp[1320] + amp[1325] + amp[1324] -
      amp[1331] + amp[1329] - amp[1366] + amp[1370] - amp[1368] - amp[1379] +
      amp[1377] + amp[1386] + amp[1382] - amp[1388] - amp[1380] - amp[1394] +
      amp[1392] - Complex<double> (0, 1) * amp[1452] + Complex<double> (0, 1) *
      amp[1457] + Complex<double> (0, 1) * amp[1456] + amp[1488] + amp[1489] +
      amp[1492] + amp[1491] + amp[1496] + amp[1495] + amp[1501] + amp[1502] +
      amp[1513] + amp[1514] + Complex<double> (0, 1) * amp[1517] +
      Complex<double> (0, 1) * amp[1516] + Complex<double> (0, 1) * amp[1537] +
      Complex<double> (0, 1) * amp[1538] - Complex<double> (0, 1) * amp[1661] +
      Complex<double> (0, 1) * amp[1659] - amp[1664] + amp[1662] + amp[1676] -
      amp[1682] - amp[1674] + amp[1680] + amp[1685] - amp[1683] - amp[1688] +
      amp[1686] + Complex<double> (0, 1) * amp[1691] - Complex<double> (0, 1) *
      amp[1689];
  jamp[119] = -amp[1] - amp[4] - amp[3] - amp[17] + amp[15] + amp[25] + amp[24]
      + amp[27] + amp[28] + amp[37] + amp[36] + amp[39] + amp[40] - amp[44] +
      amp[42] - Complex<double> (0, 1) * amp[82] - Complex<double> (0, 1) *
      amp[83] + amp[84] - Complex<double> (0, 1) * amp[89] + Complex<double>
      (0, 1) * amp[87] + amp[97] + Complex<double> (0, 1) * amp[98] +
      Complex<double> (0, 1) * amp[138] + amp[149] + Complex<double> (0, 1) *
      amp[150] + Complex<double> (0, 1) * amp[151] - Complex<double> (0, 1) *
      amp[158] + Complex<double> (0, 1) * amp[156] + Complex<double> (0, 1) *
      amp[336] - Complex<double> (0, 1) * amp[340] + Complex<double> (0, 1) *
      amp[338] - Complex<double> (0, 1) * amp[348] + amp[349] + amp[351] +
      Complex<double> (0, 1) * amp[352] - amp[409] - Complex<double> (0, 1) *
      amp[414] + amp[415] + amp[418] + Complex<double> (0, 1) * amp[419] +
      Complex<double> (0, 1) * amp[420] - Complex<double> (0, 1) * amp[423] +
      Complex<double> (0, 1) * amp[421] - Complex<double> (0, 1) * amp[435] +
      amp[436] + Complex<double> (0, 1) * amp[438] - Complex<double> (0, 1) *
      amp[441] + Complex<double> (0, 1) * amp[439] + Complex<double> (0, 1) *
      amp[444] - Complex<double> (0, 1) * amp[446] - amp[452] + amp[450] -
      amp[473] + amp[471] + Complex<double> (0, 1) * amp[474] - Complex<double>
      (0, 1) * amp[476] - amp[1075] - amp[1078] - amp[1077] + amp[1079] -
      amp[1083] + amp[1081] + amp[1084] - amp[1088] + amp[1086] + amp[1097] -
      amp[1095] + amp[1099] + amp[1098] + amp[1101] + amp[1102] -
      Complex<double> (0, 1) * amp[1116] + amp[1117] + Complex<double> (0, 1) *
      amp[1121] - Complex<double> (0, 1) * amp[1124] + Complex<double> (0, 1) *
      amp[1122] + Complex<double> (0, 1) * amp[1146] + amp[1308] + amp[1309] -
      amp[1312] + amp[1310] + amp[1319] - amp[1322] - amp[1321] - amp[1325] +
      amp[1323] + amp[1328] - amp[1326] + amp[1357] - amp[1360] + amp[1358] -
      amp[1370] - amp[1369] - amp[1386] + amp[1381] + amp[1380] - amp[1387] +
      amp[1389] - amp[1391] + Complex<double> (0, 1) * amp[1454] -
      Complex<double> (0, 1) * amp[1457] + Complex<double> (0, 1) * amp[1455] +
      amp[1486] + amp[1485] - amp[1492] - amp[1491] - amp[1496] + amp[1494] +
      amp[1500] - amp[1502] + amp[1512] - amp[1514] - Complex<double> (0, 1) *
      amp[1517] + Complex<double> (0, 1) * amp[1515] + Complex<double> (0, 1) *
      amp[1536] - Complex<double> (0, 1) * amp[1538] + amp[1810] + amp[1809] -
      amp[1816] - amp[1815] - amp[1820] + amp[1818] + amp[1821] - amp[1823] -
      Complex<double> (0, 1) * amp[1826] + Complex<double> (0, 1) * amp[1824];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[0][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}


}  // end namespace PY8MEs_namespace

