//==========================================================================
// This file has been automatically generated for Pythia 8 by
// MadGraph5_aMC@NLO v. 2.6.0, 2017-08-16
// By the MadGraph5_aMC@NLO Development Team
// Visit launchpad.net/madgraph5 and amcatnlo.web.cern.ch
//==========================================================================

#include "PY8MEs_R19_P55_sm_gq_gggq.h"
#include "HelAmps_sm.h"

using namespace Pythia8_sm; 

namespace PY8MEs_namespace 
{
//==========================================================================
// Class member functions for calculating the matrix elements for
// Process: g u > g g g u WEIGHTED<=4 @19
// Process: g c > g g g c WEIGHTED<=4 @19
// Process: g d > g g g d WEIGHTED<=4 @19
// Process: g s > g g g s WEIGHTED<=4 @19
// Process: g u~ > g g g u~ WEIGHTED<=4 @19
// Process: g c~ > g g g c~ WEIGHTED<=4 @19
// Process: g d~ > g g g d~ WEIGHTED<=4 @19
// Process: g s~ > g g g s~ WEIGHTED<=4 @19

// Exception class
class PY8MEs_R19_P55_sm_gq_gggqException : public exception
{
  virtual const char * what() const throw()
  {
    return "Exception in class 'PY8MEs_R19_P55_sm_gq_gggq'."; 
  }
}
PY8MEs_R19_P55_sm_gq_gggq_exception; 

std::set<int> PY8MEs_R19_P55_sm_gq_gggq::s_channel_proc = std::set<int>
    (createset<int> ());

int PY8MEs_R19_P55_sm_gq_gggq::helicities[ncomb][nexternal] = {{-1, -1, -1, -1,
    -1, -1}, {-1, -1, -1, -1, -1, 1}, {-1, -1, -1, -1, 1, -1}, {-1, -1, -1, -1,
    1, 1}, {-1, -1, -1, 1, -1, -1}, {-1, -1, -1, 1, -1, 1}, {-1, -1, -1, 1, 1,
    -1}, {-1, -1, -1, 1, 1, 1}, {-1, -1, 1, -1, -1, -1}, {-1, -1, 1, -1, -1,
    1}, {-1, -1, 1, -1, 1, -1}, {-1, -1, 1, -1, 1, 1}, {-1, -1, 1, 1, -1, -1},
    {-1, -1, 1, 1, -1, 1}, {-1, -1, 1, 1, 1, -1}, {-1, -1, 1, 1, 1, 1}, {-1, 1,
    -1, -1, -1, -1}, {-1, 1, -1, -1, -1, 1}, {-1, 1, -1, -1, 1, -1}, {-1, 1,
    -1, -1, 1, 1}, {-1, 1, -1, 1, -1, -1}, {-1, 1, -1, 1, -1, 1}, {-1, 1, -1,
    1, 1, -1}, {-1, 1, -1, 1, 1, 1}, {-1, 1, 1, -1, -1, -1}, {-1, 1, 1, -1, -1,
    1}, {-1, 1, 1, -1, 1, -1}, {-1, 1, 1, -1, 1, 1}, {-1, 1, 1, 1, -1, -1},
    {-1, 1, 1, 1, -1, 1}, {-1, 1, 1, 1, 1, -1}, {-1, 1, 1, 1, 1, 1}, {1, -1,
    -1, -1, -1, -1}, {1, -1, -1, -1, -1, 1}, {1, -1, -1, -1, 1, -1}, {1, -1,
    -1, -1, 1, 1}, {1, -1, -1, 1, -1, -1}, {1, -1, -1, 1, -1, 1}, {1, -1, -1,
    1, 1, -1}, {1, -1, -1, 1, 1, 1}, {1, -1, 1, -1, -1, -1}, {1, -1, 1, -1, -1,
    1}, {1, -1, 1, -1, 1, -1}, {1, -1, 1, -1, 1, 1}, {1, -1, 1, 1, -1, -1}, {1,
    -1, 1, 1, -1, 1}, {1, -1, 1, 1, 1, -1}, {1, -1, 1, 1, 1, 1}, {1, 1, -1, -1,
    -1, -1}, {1, 1, -1, -1, -1, 1}, {1, 1, -1, -1, 1, -1}, {1, 1, -1, -1, 1,
    1}, {1, 1, -1, 1, -1, -1}, {1, 1, -1, 1, -1, 1}, {1, 1, -1, 1, 1, -1}, {1,
    1, -1, 1, 1, 1}, {1, 1, 1, -1, -1, -1}, {1, 1, 1, -1, -1, 1}, {1, 1, 1, -1,
    1, -1}, {1, 1, 1, -1, 1, 1}, {1, 1, 1, 1, -1, -1}, {1, 1, 1, 1, -1, 1}, {1,
    1, 1, 1, 1, -1}, {1, 1, 1, 1, 1, 1}};

// Normalization factors the various processes
// Denominators: spins, colors and identical particles
int PY8MEs_R19_P55_sm_gq_gggq::denom_colors[nprocesses] = {24, 24}; 
int PY8MEs_R19_P55_sm_gq_gggq::denom_hels[nprocesses] = {4, 4}; 
int PY8MEs_R19_P55_sm_gq_gggq::denom_iden[nprocesses] = {6, 6}; 

//--------------------------------------------------------------------------
// Color config initialization
void PY8MEs_R19_P55_sm_gq_gggq::initColorConfigs() 
{
  color_configs = vector < vec_vec_int > (); 
  jamp_nc_relative_power = vector < vec_int > (); 

  // Color flows of process Process: g u > g g g u WEIGHTED<=4 @19
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(2)(5)(0)(3)(2)(4)(3)(5)(4)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #1
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(2)(4)(0)(3)(2)(4)(5)(5)(3)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #2
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(2)(5)(0)(3)(4)(4)(2)(5)(3)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #3
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(2)(3)(0)(3)(5)(4)(2)(5)(4)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #4
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(2)(4)(0)(3)(5)(4)(3)(5)(2)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #5
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(2)(3)(0)(3)(4)(4)(5)(5)(2)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #6
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(2)(5)(0)(3)(1)(4)(2)(5)(4)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #7
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(2)(4)(0)(3)(1)(4)(5)(5)(2)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #8
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(2)(5)(0)(3)(1)(4)(3)(5)(2)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #9
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(2)(2)(0)(3)(1)(4)(3)(5)(4)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #10
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(2)(4)(0)(3)(1)(4)(2)(5)(3)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #11
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(2)(2)(0)(3)(1)(4)(5)(5)(3)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #12
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(2)(5)(0)(3)(2)(4)(1)(5)(3)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #13
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(2)(3)(0)(3)(5)(4)(1)(5)(2)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #14
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(2)(5)(0)(3)(4)(4)(1)(5)(2)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #15
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(2)(2)(0)(3)(4)(4)(1)(5)(3)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #16
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(2)(3)(0)(3)(2)(4)(1)(5)(4)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #17
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(2)(2)(0)(3)(5)(4)(1)(5)(4)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #18
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(2)(4)(0)(3)(2)(4)(3)(5)(1)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #19
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(2)(3)(0)(3)(4)(4)(2)(5)(1)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #20
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(2)(4)(0)(3)(5)(4)(2)(5)(1)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #21
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(2)(2)(0)(3)(5)(4)(3)(5)(1)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #22
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(2)(3)(0)(3)(2)(4)(5)(5)(1)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #23
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(2)(2)(0)(3)(4)(4)(5)(5)(1)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 

  // Color flows of process Process: g u~ > g g g u~ WEIGHTED<=4 @19
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(2)(0)(1)(3)(2)(4)(3)(5)(4)(0)(5)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #1
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(2)(0)(1)(3)(2)(4)(5)(5)(3)(0)(4)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #2
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(2)(0)(1)(3)(4)(4)(2)(5)(3)(0)(5)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #3
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(2)(0)(1)(3)(5)(4)(2)(5)(4)(0)(3)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #4
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(2)(0)(1)(3)(5)(4)(3)(5)(2)(0)(4)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #5
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(2)(0)(1)(3)(4)(4)(5)(5)(2)(0)(3)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #6
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(2)(0)(1)(3)(1)(4)(2)(5)(4)(0)(5)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #7
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(2)(0)(1)(3)(1)(4)(5)(5)(2)(0)(4)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #8
  color_configs[1].push_back(vec_int(createvector<int>
      (4)(2)(0)(1)(3)(1)(4)(3)(5)(2)(0)(5)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #9
  color_configs[1].push_back(vec_int(createvector<int>
      (5)(2)(0)(1)(3)(1)(4)(3)(5)(4)(0)(2)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #10
  color_configs[1].push_back(vec_int(createvector<int>
      (5)(2)(0)(1)(3)(1)(4)(2)(5)(3)(0)(4)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #11
  color_configs[1].push_back(vec_int(createvector<int>
      (4)(2)(0)(1)(3)(1)(4)(5)(5)(3)(0)(2)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #12
  color_configs[1].push_back(vec_int(createvector<int>
      (4)(2)(0)(1)(3)(2)(4)(1)(5)(3)(0)(5)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #13
  color_configs[1].push_back(vec_int(createvector<int>
      (4)(2)(0)(1)(3)(5)(4)(1)(5)(2)(0)(3)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #14
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(2)(0)(1)(3)(4)(4)(1)(5)(2)(0)(5)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #15
  color_configs[1].push_back(vec_int(createvector<int>
      (5)(2)(0)(1)(3)(4)(4)(1)(5)(3)(0)(2)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #16
  color_configs[1].push_back(vec_int(createvector<int>
      (5)(2)(0)(1)(3)(2)(4)(1)(5)(4)(0)(3)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #17
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(2)(0)(1)(3)(5)(4)(1)(5)(4)(0)(2)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #18
  color_configs[1].push_back(vec_int(createvector<int>
      (5)(2)(0)(1)(3)(2)(4)(3)(5)(1)(0)(4)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #19
  color_configs[1].push_back(vec_int(createvector<int>
      (5)(2)(0)(1)(3)(4)(4)(2)(5)(1)(0)(3)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #20
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(2)(0)(1)(3)(5)(4)(2)(5)(1)(0)(4)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #21
  color_configs[1].push_back(vec_int(createvector<int>
      (4)(2)(0)(1)(3)(5)(4)(3)(5)(1)(0)(2)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #22
  color_configs[1].push_back(vec_int(createvector<int>
      (4)(2)(0)(1)(3)(2)(4)(5)(5)(1)(0)(3)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #23
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(2)(0)(1)(3)(4)(4)(5)(5)(1)(0)(2)));
  jamp_nc_relative_power[1].push_back(0); 
}

//--------------------------------------------------------------------------
// Destructor.
PY8MEs_R19_P55_sm_gq_gggq::~PY8MEs_R19_P55_sm_gq_gggq() 
{
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    delete[] p[i]; 
    p[i] = NULL; 
  }
}

//--------------------------------------------------------------------------
// Invert the permutation mapping
vector<int> PY8MEs_R19_P55_sm_gq_gggq::invert_mapping(vector<int> mapping) 
{
  vector<int> inverted_mapping; 
  for (unsigned int i = 0; i < mapping.size(); i++ )
  {
    for (unsigned int j = 0; j < mapping.size(); j++ )
    {
      if (mapping[j] == ((int)i))
      {
        inverted_mapping.push_back(j); 
        break; 
      }
    }
  }
  return inverted_mapping; 
}

//--------------------------------------------------------------------------
// Return the list of possible helicity configurations
vector < vec_int > PY8MEs_R19_P55_sm_gq_gggq::getHelicityConfigs(vector<int>
    permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(ncomb, vector<int> (nexternal, 0)); 
  for (unsigned int ihel = 0; ihel < ncomb; ihel++ )
  {
    for(unsigned int j = 0; j < nexternal; j++ )
    {
      res[ihel][chosenPerm[j]] = helicities[ihel][j]; 
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Return the list of possible color configurations
vector < vec_int > PY8MEs_R19_P55_sm_gq_gggq::getColorConfigs(int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(color_configs[chosenProcID].size(), vector<int>
      (nexternal * 2, 0));
  for (unsigned int icol = 0; icol < color_configs[chosenProcID].size(); icol++
      )
  {
    for(unsigned int j = 0; j < (2 * nexternal); j++ )
    {
      res[icol][chosenPerm[j/2] * 2 + j%2] =
          color_configs[chosenProcID][icol][j];
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Get JAMP relative N_c power
int PY8MEs_R19_P55_sm_gq_gggq::getColorFlowRelativeNCPower(int color_flow_ID,
    int specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return jamp_nc_relative_power[chosenProcID][color_flow_ID]; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity ID -> Helicity Config
vector<int> PY8MEs_R19_P55_sm_gq_gggq::getHelicityConfigForID(int hel_ID,
    vector<int> permutation)
{
  if (hel_ID < 0 || hel_ID >= ncomb)
  {
    cerr <<  "Error in function 'getHelicityConfigForID' of class" << 
    " 'PY8MEs_R19_P55_sm_gq_gggq': Specified helicity ID '" << 
    hel_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R19_P55_sm_gq_gggq_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(nexternal, 0); 
  for (unsigned int j = 0; j < nexternal; j++ )
  {
    res[chosenPerm[j]] = helicities[hel_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity Config -> Helicity ID
int PY8MEs_R19_P55_sm_gq_gggq::getHelicityIDForConfig(vector<int> hel_config,
    vector<int> permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  int user_ihel = -1; 
  if (hel_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < ncomb; i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < nexternal; j++ )
      {
        if (helicities[i][chosenPerm[j]] != hel_config[j])
        {
          found = false; 
          break; 
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_ihel = i; 
        break; 
      }
    }
    if (user_ihel == -1)
    {
      cerr <<  "Error in function 'getHelicityIDForConfig' of class" << 
      " 'PY8MEs_R19_P55_sm_gq_gggq': Specified helicity" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R19_P55_sm_gq_gggq_exception; 
    }
  }
  return user_ihel; 
}


//--------------------------------------------------------------------------
// Implements the map Color ID -> Color Config
vector<int> PY8MEs_R19_P55_sm_gq_gggq::getColorConfigForID(int color_ID, int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < 0 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getColorConfigForID' of class" << 
    " 'PY8MEs_R19_P55_sm_gq_gggq': Specified color ID '" << 
    color_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R19_P55_sm_gq_gggq_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(color_configs[chosenProcID][color_ID].size(), 0); 
  for (unsigned int j = 0; j < (2 * nexternal); j++ )
  {
    res[chosenPerm[j/2] * 2 + j%2] = color_configs[chosenProcID][color_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Color Config -> Color ID
int PY8MEs_R19_P55_sm_gq_gggq::getColorIDForConfig(vector<int> color_config,
    int specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = -1; 
  if (color_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < color_configs[chosenProcID].size(); i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < (nexternal * 2); j++ )
      {

        // If colorless then make sure it matches
        // The little arithmetics in the color index is just
        // the permutation applies on the particle list which is
        // twice smaller since each particle can have two color indices.
        if (color_config[j] == 0)
        {
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] != 0)
          {
            found = false; 
            break; 
          }
          // Otherwise check that the color linked position matches
        }
        else
        {
          int color_linked_pos = -1; 
          // Find the other end of the line in the user color config
          for (unsigned int k = 0; k < (nexternal * 2); k++ )
          {
            if (k == j)
              continue; 
            if (color_config[j] == color_config[k])
            {
              color_linked_pos = k; 
              break; 
            }
          }
          if (color_linked_pos == -1)
          {
            cerr <<  "Error in function 'getColorIDForConfig' of class" << 
            " 'PY8MEs_R19_P55_sm_gq_gggq': A color line could " << 
            " not be closed." << endl; 
            throw PY8MEs_R19_P55_sm_gq_gggq_exception; 
          }
          // Now check whether the color line matches
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] !=
              color_configs[chosenProcID][i][chosenPerm[color_linked_pos/2] * 2
              + color_linked_pos%2])
          {
            found = false; 
            break; 
          }
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_icol = i; 
        break; 
      }
    }

    if (user_icol == -1)
    {
      cerr <<  "Error in function 'getColorIDForConfig' of class" << 
      " 'PY8MEs_R19_P55_sm_gq_gggq': Specified color" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R19_P55_sm_gq_gggq_exception; 
    }
  }
  return user_icol; 
}

//--------------------------------------------------------------------------
// Returns all result previously computed in SigmaKin
vector < vec_double > PY8MEs_R19_P55_sm_gq_gggq::getAllResults(int
    specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return all_results[chosenProcID]; 
}

//--------------------------------------------------------------------------
// Returns a result previously computed in SigmaKin for a specific helicity
// and color ID. -1 means avg and summed over that characteristic.
double PY8MEs_R19_P55_sm_gq_gggq::getResult(int helicity_ID, int color_ID, int
    specify_proc_ID)
{
  if (helicity_ID < - 1 || helicity_ID >= ncomb)
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R19_P55_sm_gq_gggq': Specified helicity ID '" << 
    helicity_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R19_P55_sm_gq_gggq_exception; 
  }
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < - 1 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R19_P55_sm_gq_gggq': Specified color ID '" << 
    color_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R19_P55_sm_gq_gggq_exception; 
  }
  return all_results[chosenProcID][helicity_ID + 1][color_ID + 1]; 
}

//--------------------------------------------------------------------------
// Check for the availability of the requested process and if available,
// If available, this returns the corresponding permutation and Proc_ID to use.
// If not available, this returns a negative Proc_ID.
pair < vector<int> , int > PY8MEs_R19_P55_sm_gq_gggq::static_getPY8ME(vector<int> initial_pdgs, vector<int> final_pdgs, set<int> schannels) 
{

  // Not available return value
  pair < vector<int> , int > NA(vector<int> (), -1); 

  // Check if s-channel requirements match
  if (nreq_s_channels > 0)
  {
    if (schannels != s_channel_proc)
      return NA; 
  }
  else
  {
    if (schannels.size() != 0)
      return NA; 
  }

  // Check number of final state particles
  if (final_pdgs.size() != (nexternal - ninitial))
    return NA; 

  // Check number of initial state particles
  if (initial_pdgs.size() != ninitial)
    return NA; 

  // List of processes available in this class
  const int nprocs = 16; 
  const int proc_IDS[nprocs] = {0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1,
      1};
  const int in_pdgs[nprocs][ninitial] = {{21, 2}, {21, 4}, {21, 1}, {21, 3},
      {21, -2}, {21, -4}, {21, -1}, {21, -3}, {2, 21}, {4, 21}, {1, 21}, {3,
      21}, {-2, 21}, {-4, 21}, {-1, 21}, {-3, 21}};
  const int out_pdgs[nprocs][nexternal - ninitial] = {{21, 21, 21, 2}, {21, 21,
      21, 4}, {21, 21, 21, 1}, {21, 21, 21, 3}, {21, 21, 21, -2}, {21, 21, 21,
      -4}, {21, 21, 21, -1}, {21, 21, 21, -3}, {21, 21, 21, 2}, {21, 21, 21,
      4}, {21, 21, 21, 1}, {21, 21, 21, 3}, {21, 21, 21, -2}, {21, 21, 21, -4},
      {21, 21, 21, -1}, {21, 21, 21, -3}};

  bool in_pdgs_used[ninitial]; 
  bool out_pdgs_used[nexternal - ninitial]; 
  for(unsigned int i = 0; i < nprocs; i++ )
  {
    int permutations[nexternal]; 

    // Reinitialize initial state look-up variables
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      in_pdgs_used[j] = false; 
      permutations[j] = -1; 
    }
    // Look for initial state matches
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      for(unsigned int k = 0; k < ninitial; k++ )
      {
        // Make sure it has not been used already
        if (in_pdgs_used[k])
          continue; 
        if (initial_pdgs[k] == in_pdgs[i][j])
        {
          permutations[j] = k; 
          in_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[ninitial - 1] == -1)
      continue; 

    // Reinitialize final state look-up variables
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      out_pdgs_used[j] = false; 
      permutations[ninitial + j] = -1; 
    }
    // Look for final state matches
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      for(unsigned int k = 0; k < (nexternal - ninitial); k++ )
      {
        // Make sure it has not been used already
        if (out_pdgs_used[k])
          continue; 
        if (final_pdgs[k] == out_pdgs[i][j])
        {
          permutations[ninitial + j] = ninitial + k; 
          out_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[ninitial + j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[nexternal - 1] == -1)
      continue; 

    // Return process found
    return pair < vector<int> , int > (vector<int> (permutations, permutations
        + nexternal), proc_IDS[i]);
  }

  // No process found
  return NA; 
}

//--------------------------------------------------------------------------
// Set momenta
void PY8MEs_R19_P55_sm_gq_gggq::setMomenta(vector < vec_double > momenta_picked)
{
  if (momenta_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setMomenta' of class" << 
    " 'PY8MEs_R19_P55_sm_gq_gggq': Incorrect number of" << 
    " momenta specified." << endl; 
    throw PY8MEs_R19_P55_sm_gq_gggq_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    if (momenta_picked[i].size() != 4)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R19_P55_sm_gq_gggq': Incorrect number of" << 
      " momenta components specified." << endl; 
      throw PY8MEs_R19_P55_sm_gq_gggq_exception; 
    }
    if (momenta_picked[i][0] < 0.0)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R19_P55_sm_gq_gggq': A momentum was specified" << 
      " with negative energy. Check conventions." << endl; 
      throw PY8MEs_R19_P55_sm_gq_gggq_exception; 
    }
    for (unsigned int j = 0; j < 4; j++ )
    {
      p[i][j] = momenta_picked[i][j]; 
    }
  }
}

//--------------------------------------------------------------------------
// Set color configuration to use. An empty vector means sum over all.
void PY8MEs_R19_P55_sm_gq_gggq::setColors(vector<int> colors_picked)
{
  if (colors_picked.size() == 0)
  {
    user_colors = vector<int> (); 
    return; 
  }
  if (colors_picked.size() != (2 * nexternal))
  {
    cerr <<  "Error in function 'setColors' of class" << 
    " 'PY8MEs_R19_P55_sm_gq_gggq': Incorrect number" << 
    " of colors specified." << endl; 
    throw PY8MEs_R19_P55_sm_gq_gggq_exception; 
  }
  user_colors = vector<int> ((2 * nexternal), 0); 
  for(unsigned int i = 0; i < (2 * nexternal); i++ )
  {
    user_colors[i] = colors_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the helicity configuration to use. Am empty vector means sum over all.
void PY8MEs_R19_P55_sm_gq_gggq::setHelicities(vector<int> helicities_picked) 
{
  if (helicities_picked.size() != nexternal)
  {
    if (helicities_picked.size() == 0)
    {
      user_helicities = vector<int> (); 
      return; 
    }
    cerr <<  "Error in function 'setHelicities' of class" << 
    " 'PY8MEs_R19_P55_sm_gq_gggq': Incorrect number" << 
    " of helicities specified." << endl; 
    throw PY8MEs_R19_P55_sm_gq_gggq_exception; 
  }
  user_helicities = vector<int> (nexternal, 0); 
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    user_helicities[i] = helicities_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the permutation to use (will apply to momenta, colors and helicities)
void PY8MEs_R19_P55_sm_gq_gggq::setPermutation(vector<int> perm_picked) 
{
  if (perm_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setPermutations' of class" << 
    " 'PY8MEs_R19_P55_sm_gq_gggq': Incorrect number" << 
    " of permutations specified." << endl; 
    throw PY8MEs_R19_P55_sm_gq_gggq_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    perm[i] = perm_picked[i]; 
  }
}

// Set the proc_ID to use
void PY8MEs_R19_P55_sm_gq_gggq::setProcID(int procID_picked) 
{
  proc_ID = procID_picked; 
}

//--------------------------------------------------------------------------
// Initialize process.

void PY8MEs_R19_P55_sm_gq_gggq::initProc() 
{

  // Initialize flags
  include_symmetry_factors = true; 
  include_helicity_averaging_factors = true; 
  include_color_averaging_factors = true; 

  // Initialize vectors.
  perm = vector<int> (nexternal, 0); 
  user_colors = vector<int> (2 * nexternal, 0); 
  user_helicities = vector<int> (nexternal, 0); 
  p = vector < double * > (); 
  for (unsigned int i = 0; i < nexternal; i++ )
  {
    p.push_back(new double[4]); 
  }
  initColorConfigs(); 
  // Synchronize local variables dependent on the model with the active model.
  mME = vector<double> (nexternal, 0.); 
  syncProcModelParams(); 
  jamp2 = vector < vec_double > (2); 
  jamp2[0] = vector<double> (24, 0.); 
  jamp2[1] = vector<double> (24, 0.); 
  all_results = vector < vec_vec_double > (2); 
  // The first entry is always the color or helicity avg/summed matrix element.
  all_results[0] = vector < vec_double > (ncomb + 1, vector<double> (24 + 1,
      0.));
  all_results[1] = vector < vec_double > (ncomb + 1, vector<double> (24 + 1,
      0.));
}

// Synchronize local variables of the process that depend on the model
// parameters
void PY8MEs_R19_P55_sm_gq_gggq::syncProcModelParams() 
{

  // Instantiate the model class and set parameters that stay fixed during run
  mME[0] = pars->ZERO; 
  mME[1] = pars->ZERO; 
  mME[2] = pars->ZERO; 
  mME[3] = pars->ZERO; 
  mME[4] = pars->ZERO; 
  mME[5] = pars->ZERO; 
}

//--------------------------------------------------------------------------
// Setter allowing to force particular values for the external masses
void PY8MEs_R19_P55_sm_gq_gggq::setMasses(vec_double external_masses) 
{

  if (external_masses.size() != mME.size())
  {
    cerr <<  "Error in function 'setMasses' of class" << 
    " 'PY8MEs_R19_P55_sm_gq_gggq': Incorrect number of" << 
    " masses specified." << endl; 
    throw PY8MEs_R19_P55_sm_gq_gggq_exception; 
  }
  for (unsigned int j = 0; j < mME.size(); j++ )
  {
    mME[j] = external_masses[perm[j]]; 
  }
}

//--------------------------------------------------------------------------
// Getter accessing external masses with the correct ordering
vector<double> PY8MEs_R19_P55_sm_gq_gggq::getMasses() 
{

  vec_double external_masses; 
  vector<int> invertedPerm; 
  invertedPerm = invert_mapping(perm); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    external_masses.push_back(mME[invertedPerm[i]]); 
  }
  return external_masses; 

}


// Set all values of the external masses to float(-mode) where mode can be
// 0 : Mass taken from the model
// 1 : Mass taken from p_i^2 if not massless to begin with
// 2 : Mass always taken from p_i^2.
void PY8MEs_R19_P55_sm_gq_gggq::setExternalMassesMode(int mode) 
{
  if (mode != 0 && mode != 1 && mode != 2)
  {
    cerr <<  "Error in function 'setExternalMassesMode' of class" << 
    " 'PY8MEs_R19_P55_sm_gq_gggq': Incorrect mode selected :" << mode << 
    ". It must be either 0, 1 or 2" << endl; 
    throw PY8MEs_R19_P55_sm_gq_gggq_exception; 
  }
  if (mode == 0)
  {
    syncProcModelParams(); 
  }
  else if (mode == 1)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      if (mME[j] != pars->ZERO)
      {
        mME[j] = -1.0; 
      }
    }
  }
  else if (mode == 2)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      mME[j] = -1.0; 
    }
  }
}

//--------------------------------------------------------------------------
// Evaluate the squared matrix element.

double PY8MEs_R19_P55_sm_gq_gggq::sigmaKin() 
{
  // Set the parameters which change event by event
  pars->setDependentParameters(); 
  pars->setDependentCouplings(); 
  // Reset color flows
  for(int i = 0; i < 24; i++ )
    jamp2[0][i] = 0.; 
  for(int i = 0; i < 24; i++ )
    jamp2[1][i] = 0.; 

  // Save previous values of mME
  vector<double> saved_mME(mME.size(), 0.0); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (mME[i] < 0.0)
    {
      saved_mME[i] = mME[i]; 
      mME[i] = sqrt(abs(pow(p[perm[i]][0], 2) - 
      (pow(p[perm[i]][1], 2) + pow(p[perm[i]][2], 2) + pow(p[perm[i]][3],
          2))));
    }
  }

  // Local variables and constants
  const int max_tries = 10; 
  vector < vec_bool > goodhel(nprocesses, vec_bool(ncomb, false)); 
  vec_int ntry(nprocesses, 0); 
  double t = 0.; 
  double result = 0.; 

  if (ntry[proc_ID] <= max_tries)
    ntry[proc_ID] = ntry[proc_ID] + 1; 

  // Find which helicity configuration is asked for
  // -1 indicates one wants to sum over helicities
  int user_ihel = getHelicityIDForConfig(user_helicities); 

  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = getColorIDForConfig(user_colors); 

  // Reset the list of results that will be recomputed here
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      all_results[proc_ID][ihel + 1][icolor + 1] = 0.; 
    }
  }

  // Calculate the matrix element for all helicities
  // unless already detected as vanishing
  for(int ihel = 0; ihel < ncomb; ihel++ )
  {
    // Skip helicity if already detected as vanishing
    if ((ntry[proc_ID] >= max_tries) && !goodhel[proc_ID][ihel])
      continue; 

    // Also skip helicity if user asks for a specific one
    if ((ntry[proc_ID] >= max_tries) && user_ihel != -1 && user_ihel != ihel)
      continue; 

    calculate_wavefunctions(helicities[ihel]); 

    // Reset locally computed color flows
    for(int i = 0; i < 24; i++ )
      jamp2[0][i] = 0.; 
    for(int i = 0; i < 24; i++ )
      jamp2[1][i] = 0.; 

    if (proc_ID == 0)
      t = matrix_19_gu_gggu(); 
    if (proc_ID == 1)
      t = matrix_19_gux_gggux(); 

    // Store which helicities give non-zero result
    if ((ntry[proc_ID] < max_tries) && t != 0. && !goodhel[proc_ID][ihel])
      goodhel[proc_ID][ihel] = true; 

    // Aggregate results
    if (user_ihel == -1 || user_ihel == ihel)
    {
      if (user_icol == -1)
      {
        result = result + t; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][0] += t; 
          for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
          {
            all_results[proc_ID][0][i + 1] += jamp2[proc_ID][i]; 
          }
        }
        all_results[proc_ID][ihel + 1][0] += t; 
        for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
        {
          all_results[proc_ID][ihel + 1][i + 1] += jamp2[proc_ID][i]; 
        }
      }
      else
      {
        result = result + jamp2[proc_ID][user_icol]; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][user_icol + 1] += jamp2[proc_ID][user_icol]; 
        }
        all_results[proc_ID][ihel + 1][user_icol + 1] +=
            jamp2[proc_ID][user_icol];
      }
    }

  }

  // Normalize results with the identical particle factor
  if (include_symmetry_factors)
  {
    result = result/denom_iden[proc_ID]; 
  }
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      if (include_symmetry_factors)
      {
        all_results[proc_ID][ihel + 1][icolor + 1] /= denom_iden[proc_ID]; 
      }
    }
  }


  // Normalize when when summing+averaging over helicity configurations
  if (user_ihel == -1 && include_helicity_averaging_factors)
  {
    result /= denom_hels[proc_ID]; 
    if (user_icol == -1)
    {
      all_results[proc_ID][0][0] /= denom_hels[proc_ID]; 
      for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
      {
        all_results[proc_ID][0][i + 1] /= denom_hels[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][0][user_icol + 1] /= denom_hels[proc_ID]; 
    }
  }

  // Normalize when summing+averaging over color configurations
  if (user_icol == -1 && include_color_averaging_factors)
  {
    result /= denom_colors[proc_ID]; 
    if (user_ihel == -1)
    {
      all_results[proc_ID][0][0] /= denom_colors[proc_ID]; 
      for (unsigned int i = 0; i < ncomb; i++ )
      {
        all_results[proc_ID][i + 1][0] /= denom_colors[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][user_ihel + 1][0] /= denom_colors[proc_ID]; 
    }
  }

  // Reinstate previous values of mME
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (saved_mME[i] < 0.0)
    {
      mME[i] = saved_mME[i]; 
    }
  }

  // Finally return it
  return result; 
}

//==========================================================================
// Private class member functions

//--------------------------------------------------------------------------
// Evaluate |M|^2 for each subprocess

void PY8MEs_R19_P55_sm_gq_gggq::calculate_wavefunctions(const int hel[])
{
  // Calculate wavefunctions for all processes
  // Calculate all wavefunctions
  vxxxxx(p[perm[0]], mME[0], hel[0], -1, w[0]); 
  ixxxxx(p[perm[1]], mME[1], hel[1], +1, w[1]); 
  vxxxxx(p[perm[2]], mME[2], hel[2], +1, w[2]); 
  vxxxxx(p[perm[3]], mME[3], hel[3], +1, w[3]); 
  vxxxxx(p[perm[4]], mME[4], hel[4], +1, w[4]); 
  oxxxxx(p[perm[5]], mME[5], hel[5], +1, w[5]); 
  VVV1P0_1(w[0], w[2], pars->GC_10, pars->ZERO, pars->ZERO, w[6]); 
  VVV1P0_1(w[3], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[7]); 
  FFV1_1(w[5], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[8]); 
  FFV1_2(w[1], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[9]); 
  FFV1P0_3(w[1], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[10]); 
  FFV1_1(w[5], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[11]); 
  VVV1P0_1(w[6], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[12]); 
  FFV1_2(w[1], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[13]); 
  FFV1_2(w[1], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[14]); 
  FFV1_1(w[5], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[15]); 
  VVV1P0_1(w[6], w[3], pars->GC_10, pars->ZERO, pars->ZERO, w[16]); 
  VVV1P0_1(w[0], w[3], pars->GC_10, pars->ZERO, pars->ZERO, w[17]); 
  VVV1P0_1(w[2], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[18]); 
  FFV1_1(w[5], w[17], pars->GC_11, pars->ZERO, pars->ZERO, w[19]); 
  FFV1_2(w[1], w[17], pars->GC_11, pars->ZERO, pars->ZERO, w[20]); 
  FFV1_1(w[5], w[2], pars->GC_11, pars->ZERO, pars->ZERO, w[21]); 
  VVV1P0_1(w[17], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[22]); 
  FFV1_2(w[1], w[2], pars->GC_11, pars->ZERO, pars->ZERO, w[23]); 
  VVV1P0_1(w[17], w[2], pars->GC_10, pars->ZERO, pars->ZERO, w[24]); 
  VVV1P0_1(w[0], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[25]); 
  VVV1P0_1(w[2], w[3], pars->GC_10, pars->ZERO, pars->ZERO, w[26]); 
  FFV1_1(w[5], w[25], pars->GC_11, pars->ZERO, pars->ZERO, w[27]); 
  FFV1_2(w[1], w[25], pars->GC_11, pars->ZERO, pars->ZERO, w[28]); 
  VVV1P0_1(w[25], w[3], pars->GC_10, pars->ZERO, pars->ZERO, w[29]); 
  VVV1P0_1(w[25], w[2], pars->GC_10, pars->ZERO, pars->ZERO, w[30]); 
  FFV1_1(w[5], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[31]); 
  FFV1_1(w[31], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[32]); 
  FFV1P0_3(w[1], w[31], pars->GC_11, pars->ZERO, pars->ZERO, w[33]); 
  FFV1_1(w[31], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[34]); 
  FFV1_1(w[31], w[2], pars->GC_11, pars->ZERO, pars->ZERO, w[35]); 
  VVVV1P0_1(w[2], w[3], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[36]); 
  VVVV3P0_1(w[2], w[3], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[37]); 
  VVVV4P0_1(w[2], w[3], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[38]); 
  FFV1_2(w[1], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[39]); 
  FFV1_2(w[39], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[40]); 
  FFV1P0_3(w[39], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[41]); 
  FFV1_2(w[39], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[42]); 
  FFV1_2(w[39], w[2], pars->GC_11, pars->ZERO, pars->ZERO, w[43]); 
  VVV1P0_1(w[0], w[26], pars->GC_10, pars->ZERO, pars->ZERO, w[44]); 
  FFV1_1(w[15], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[45]); 
  FFV1_2(w[13], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[46]); 
  VVV1P0_1(w[0], w[10], pars->GC_10, pars->ZERO, pars->ZERO, w[47]); 
  VVV1P0_1(w[0], w[18], pars->GC_10, pars->ZERO, pars->ZERO, w[48]); 
  FFV1_1(w[11], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[49]); 
  FFV1_2(w[14], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[50]); 
  FFV1_1(w[21], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[51]); 
  VVV1P0_1(w[0], w[7], pars->GC_10, pars->ZERO, pars->ZERO, w[52]); 
  FFV1_2(w[23], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[53]); 
  VVVV1P0_1(w[0], w[2], w[3], pars->GC_12, pars->ZERO, pars->ZERO, w[54]); 
  VVVV3P0_1(w[0], w[2], w[3], pars->GC_12, pars->ZERO, pars->ZERO, w[55]); 
  VVVV4P0_1(w[0], w[2], w[3], pars->GC_12, pars->ZERO, pars->ZERO, w[56]); 
  VVVV1P0_1(w[0], w[2], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[57]); 
  VVVV3P0_1(w[0], w[2], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[58]); 
  VVVV4P0_1(w[0], w[2], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[59]); 
  VVVV1P0_1(w[0], w[3], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[60]); 
  VVVV3P0_1(w[0], w[3], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[61]); 
  VVVV4P0_1(w[0], w[3], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[62]); 
  oxxxxx(p[perm[1]], mME[1], hel[1], -1, w[63]); 
  ixxxxx(p[perm[5]], mME[5], hel[5], -1, w[64]); 
  FFV1_1(w[63], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[65]); 
  FFV1_2(w[64], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[66]); 
  FFV1P0_3(w[64], w[63], pars->GC_11, pars->ZERO, pars->ZERO, w[67]); 
  FFV1_1(w[63], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[68]); 
  FFV1_2(w[64], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[69]); 
  FFV1_2(w[64], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[70]); 
  FFV1_1(w[63], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[71]); 
  FFV1_1(w[63], w[17], pars->GC_11, pars->ZERO, pars->ZERO, w[72]); 
  FFV1_2(w[64], w[17], pars->GC_11, pars->ZERO, pars->ZERO, w[73]); 
  FFV1_1(w[63], w[2], pars->GC_11, pars->ZERO, pars->ZERO, w[74]); 
  FFV1_2(w[64], w[2], pars->GC_11, pars->ZERO, pars->ZERO, w[75]); 
  FFV1_1(w[63], w[25], pars->GC_11, pars->ZERO, pars->ZERO, w[76]); 
  FFV1_2(w[64], w[25], pars->GC_11, pars->ZERO, pars->ZERO, w[77]); 
  FFV1_1(w[63], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[78]); 
  FFV1_1(w[78], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[79]); 
  FFV1P0_3(w[64], w[78], pars->GC_11, pars->ZERO, pars->ZERO, w[80]); 
  FFV1_1(w[78], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[81]); 
  FFV1_1(w[78], w[2], pars->GC_11, pars->ZERO, pars->ZERO, w[82]); 
  FFV1_2(w[64], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[83]); 
  FFV1_2(w[83], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[84]); 
  FFV1P0_3(w[83], w[63], pars->GC_11, pars->ZERO, pars->ZERO, w[85]); 
  FFV1_2(w[83], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[86]); 
  FFV1_2(w[83], w[2], pars->GC_11, pars->ZERO, pars->ZERO, w[87]); 
  FFV1_1(w[71], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[88]); 
  FFV1_2(w[69], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[89]); 
  VVV1P0_1(w[0], w[67], pars->GC_10, pars->ZERO, pars->ZERO, w[90]); 
  FFV1_1(w[68], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[91]); 
  FFV1_2(w[70], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[92]); 
  FFV1_1(w[74], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[93]); 
  FFV1_2(w[75], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[94]); 

  // Calculate all amplitudes
  // Amplitude(s) for diagram number 0
  FFV1_0(w[1], w[8], w[7], pars->GC_11, amp[0]); 
  FFV1_0(w[9], w[5], w[7], pars->GC_11, amp[1]); 
  VVV1_0(w[6], w[7], w[10], pars->GC_10, amp[2]); 
  FFV1_0(w[1], w[11], w[12], pars->GC_11, amp[3]); 
  FFV1_0(w[9], w[11], w[4], pars->GC_11, amp[4]); 
  FFV1_0(w[13], w[11], w[6], pars->GC_11, amp[5]); 
  FFV1_0(w[14], w[5], w[12], pars->GC_11, amp[6]); 
  FFV1_0(w[14], w[8], w[4], pars->GC_11, amp[7]); 
  FFV1_0(w[14], w[15], w[6], pars->GC_11, amp[8]); 
  FFV1_0(w[1], w[15], w[16], pars->GC_11, amp[9]); 
  FFV1_0(w[9], w[15], w[3], pars->GC_11, amp[10]); 
  FFV1_0(w[13], w[5], w[16], pars->GC_11, amp[11]); 
  FFV1_0(w[13], w[8], w[3], pars->GC_11, amp[12]); 
  VVVV1_0(w[6], w[3], w[4], w[10], pars->GC_12, amp[13]); 
  VVVV3_0(w[6], w[3], w[4], w[10], pars->GC_12, amp[14]); 
  VVVV4_0(w[6], w[3], w[4], w[10], pars->GC_12, amp[15]); 
  VVV1_0(w[4], w[10], w[16], pars->GC_10, amp[16]); 
  VVV1_0(w[3], w[10], w[12], pars->GC_10, amp[17]); 
  FFV1_0(w[1], w[19], w[18], pars->GC_11, amp[18]); 
  FFV1_0(w[20], w[5], w[18], pars->GC_11, amp[19]); 
  VVV1_0(w[17], w[18], w[10], pars->GC_10, amp[20]); 
  FFV1_0(w[1], w[21], w[22], pars->GC_11, amp[21]); 
  FFV1_0(w[20], w[21], w[4], pars->GC_11, amp[22]); 
  FFV1_0(w[13], w[21], w[17], pars->GC_11, amp[23]); 
  FFV1_0(w[23], w[5], w[22], pars->GC_11, amp[24]); 
  FFV1_0(w[23], w[19], w[4], pars->GC_11, amp[25]); 
  FFV1_0(w[23], w[15], w[17], pars->GC_11, amp[26]); 
  FFV1_0(w[1], w[15], w[24], pars->GC_11, amp[27]); 
  FFV1_0(w[20], w[15], w[2], pars->GC_11, amp[28]); 
  FFV1_0(w[13], w[5], w[24], pars->GC_11, amp[29]); 
  FFV1_0(w[13], w[19], w[2], pars->GC_11, amp[30]); 
  VVVV1_0(w[17], w[2], w[4], w[10], pars->GC_12, amp[31]); 
  VVVV3_0(w[17], w[2], w[4], w[10], pars->GC_12, amp[32]); 
  VVVV4_0(w[17], w[2], w[4], w[10], pars->GC_12, amp[33]); 
  VVV1_0(w[4], w[10], w[24], pars->GC_10, amp[34]); 
  VVV1_0(w[2], w[10], w[22], pars->GC_10, amp[35]); 
  FFV1_0(w[1], w[27], w[26], pars->GC_11, amp[36]); 
  FFV1_0(w[28], w[5], w[26], pars->GC_11, amp[37]); 
  VVV1_0(w[25], w[26], w[10], pars->GC_10, amp[38]); 
  FFV1_0(w[1], w[21], w[29], pars->GC_11, amp[39]); 
  FFV1_0(w[28], w[21], w[3], pars->GC_11, amp[40]); 
  FFV1_0(w[14], w[21], w[25], pars->GC_11, amp[41]); 
  FFV1_0(w[23], w[5], w[29], pars->GC_11, amp[42]); 
  FFV1_0(w[23], w[27], w[3], pars->GC_11, amp[43]); 
  FFV1_0(w[23], w[11], w[25], pars->GC_11, amp[44]); 
  FFV1_0(w[1], w[11], w[30], pars->GC_11, amp[45]); 
  FFV1_0(w[28], w[11], w[2], pars->GC_11, amp[46]); 
  FFV1_0(w[14], w[5], w[30], pars->GC_11, amp[47]); 
  FFV1_0(w[14], w[27], w[2], pars->GC_11, amp[48]); 
  VVVV1_0(w[25], w[2], w[3], w[10], pars->GC_12, amp[49]); 
  VVVV3_0(w[25], w[2], w[3], w[10], pars->GC_12, amp[50]); 
  VVVV4_0(w[25], w[2], w[3], w[10], pars->GC_12, amp[51]); 
  VVV1_0(w[3], w[10], w[30], pars->GC_10, amp[52]); 
  VVV1_0(w[2], w[10], w[29], pars->GC_10, amp[53]); 
  FFV1_0(w[1], w[32], w[26], pars->GC_11, amp[54]); 
  VVV1_0(w[26], w[4], w[33], pars->GC_10, amp[55]); 
  FFV1_0(w[13], w[31], w[26], pars->GC_11, amp[56]); 
  FFV1_0(w[1], w[34], w[18], pars->GC_11, amp[57]); 
  VVV1_0(w[18], w[3], w[33], pars->GC_10, amp[58]); 
  FFV1_0(w[14], w[31], w[18], pars->GC_11, amp[59]); 
  FFV1_0(w[23], w[34], w[4], pars->GC_11, amp[60]); 
  FFV1_0(w[23], w[32], w[3], pars->GC_11, amp[61]); 
  FFV1_0(w[23], w[31], w[7], pars->GC_11, amp[62]); 
  FFV1_0(w[1], w[35], w[7], pars->GC_11, amp[63]); 
  VVV1_0(w[2], w[7], w[33], pars->GC_10, amp[64]); 
  FFV1_0(w[14], w[35], w[4], pars->GC_11, amp[65]); 
  FFV1_0(w[14], w[32], w[2], pars->GC_11, amp[66]); 
  FFV1_0(w[13], w[35], w[3], pars->GC_11, amp[67]); 
  FFV1_0(w[13], w[34], w[2], pars->GC_11, amp[68]); 
  FFV1_0(w[1], w[31], w[36], pars->GC_11, amp[69]); 
  FFV1_0(w[1], w[31], w[37], pars->GC_11, amp[70]); 
  FFV1_0(w[1], w[31], w[38], pars->GC_11, amp[71]); 
  FFV1_0(w[40], w[5], w[26], pars->GC_11, amp[72]); 
  VVV1_0(w[26], w[4], w[41], pars->GC_10, amp[73]); 
  FFV1_0(w[39], w[15], w[26], pars->GC_11, amp[74]); 
  FFV1_0(w[42], w[5], w[18], pars->GC_11, amp[75]); 
  VVV1_0(w[18], w[3], w[41], pars->GC_10, amp[76]); 
  FFV1_0(w[39], w[11], w[18], pars->GC_11, amp[77]); 
  FFV1_0(w[42], w[21], w[4], pars->GC_11, amp[78]); 
  FFV1_0(w[40], w[21], w[3], pars->GC_11, amp[79]); 
  FFV1_0(w[39], w[21], w[7], pars->GC_11, amp[80]); 
  FFV1_0(w[43], w[5], w[7], pars->GC_11, amp[81]); 
  VVV1_0(w[2], w[7], w[41], pars->GC_10, amp[82]); 
  FFV1_0(w[43], w[11], w[4], pars->GC_11, amp[83]); 
  FFV1_0(w[40], w[11], w[2], pars->GC_11, amp[84]); 
  FFV1_0(w[43], w[15], w[3], pars->GC_11, amp[85]); 
  FFV1_0(w[42], w[15], w[2], pars->GC_11, amp[86]); 
  FFV1_0(w[39], w[5], w[36], pars->GC_11, amp[87]); 
  FFV1_0(w[39], w[5], w[37], pars->GC_11, amp[88]); 
  FFV1_0(w[39], w[5], w[38], pars->GC_11, amp[89]); 
  FFV1_0(w[1], w[15], w[44], pars->GC_11, amp[90]); 
  FFV1_0(w[1], w[45], w[26], pars->GC_11, amp[91]); 
  FFV1_0(w[13], w[5], w[44], pars->GC_11, amp[92]); 
  FFV1_0(w[46], w[5], w[26], pars->GC_11, amp[93]); 
  VVVV1_0(w[0], w[26], w[4], w[10], pars->GC_12, amp[94]); 
  VVVV3_0(w[0], w[26], w[4], w[10], pars->GC_12, amp[95]); 
  VVVV4_0(w[0], w[26], w[4], w[10], pars->GC_12, amp[96]); 
  VVV1_0(w[4], w[10], w[44], pars->GC_10, amp[97]); 
  VVV1_0(w[26], w[4], w[47], pars->GC_10, amp[98]); 
  FFV1_0(w[1], w[11], w[48], pars->GC_11, amp[99]); 
  FFV1_0(w[1], w[49], w[18], pars->GC_11, amp[100]); 
  FFV1_0(w[14], w[5], w[48], pars->GC_11, amp[101]); 
  FFV1_0(w[50], w[5], w[18], pars->GC_11, amp[102]); 
  VVVV1_0(w[0], w[18], w[3], w[10], pars->GC_12, amp[103]); 
  VVVV3_0(w[0], w[18], w[3], w[10], pars->GC_12, amp[104]); 
  VVVV4_0(w[0], w[18], w[3], w[10], pars->GC_12, amp[105]); 
  VVV1_0(w[3], w[10], w[48], pars->GC_10, amp[106]); 
  VVV1_0(w[18], w[3], w[47], pars->GC_10, amp[107]); 
  FFV1_0(w[1], w[51], w[7], pars->GC_11, amp[108]); 
  FFV1_0(w[1], w[21], w[52], pars->GC_11, amp[109]); 
  FFV1_0(w[14], w[51], w[4], pars->GC_11, amp[110]); 
  FFV1_0(w[50], w[21], w[4], pars->GC_11, amp[111]); 
  FFV1_0(w[13], w[51], w[3], pars->GC_11, amp[112]); 
  FFV1_0(w[46], w[21], w[3], pars->GC_11, amp[113]); 
  FFV1_0(w[53], w[5], w[7], pars->GC_11, amp[114]); 
  FFV1_0(w[23], w[5], w[52], pars->GC_11, amp[115]); 
  FFV1_0(w[53], w[11], w[4], pars->GC_11, amp[116]); 
  FFV1_0(w[23], w[49], w[4], pars->GC_11, amp[117]); 
  FFV1_0(w[53], w[15], w[3], pars->GC_11, amp[118]); 
  FFV1_0(w[23], w[45], w[3], pars->GC_11, amp[119]); 
  VVVV1_0(w[0], w[2], w[7], w[10], pars->GC_12, amp[120]); 
  VVVV3_0(w[0], w[2], w[7], w[10], pars->GC_12, amp[121]); 
  VVVV4_0(w[0], w[2], w[7], w[10], pars->GC_12, amp[122]); 
  VVV1_0(w[2], w[10], w[52], pars->GC_10, amp[123]); 
  VVV1_0(w[2], w[7], w[47], pars->GC_10, amp[124]); 
  FFV1_0(w[13], w[49], w[2], pars->GC_11, amp[125]); 
  FFV1_0(w[46], w[11], w[2], pars->GC_11, amp[126]); 
  FFV1_0(w[50], w[15], w[2], pars->GC_11, amp[127]); 
  FFV1_0(w[14], w[45], w[2], pars->GC_11, amp[128]); 
  FFV1_0(w[1], w[15], w[54], pars->GC_11, amp[129]); 
  FFV1_0(w[1], w[15], w[55], pars->GC_11, amp[130]); 
  FFV1_0(w[1], w[15], w[56], pars->GC_11, amp[131]); 
  FFV1_0(w[13], w[5], w[54], pars->GC_11, amp[132]); 
  FFV1_0(w[13], w[5], w[55], pars->GC_11, amp[133]); 
  FFV1_0(w[13], w[5], w[56], pars->GC_11, amp[134]); 
  VVV1_0(w[54], w[4], w[10], pars->GC_10, amp[135]); 
  VVV1_0(w[55], w[4], w[10], pars->GC_10, amp[136]); 
  VVV1_0(w[56], w[4], w[10], pars->GC_10, amp[137]); 
  FFV1_0(w[1], w[11], w[57], pars->GC_11, amp[138]); 
  FFV1_0(w[1], w[11], w[58], pars->GC_11, amp[139]); 
  FFV1_0(w[1], w[11], w[59], pars->GC_11, amp[140]); 
  FFV1_0(w[14], w[5], w[57], pars->GC_11, amp[141]); 
  FFV1_0(w[14], w[5], w[58], pars->GC_11, amp[142]); 
  FFV1_0(w[14], w[5], w[59], pars->GC_11, amp[143]); 
  VVV1_0(w[57], w[3], w[10], pars->GC_10, amp[144]); 
  VVV1_0(w[58], w[3], w[10], pars->GC_10, amp[145]); 
  VVV1_0(w[59], w[3], w[10], pars->GC_10, amp[146]); 
  FFV1_0(w[1], w[21], w[60], pars->GC_11, amp[147]); 
  FFV1_0(w[1], w[21], w[61], pars->GC_11, amp[148]); 
  FFV1_0(w[1], w[21], w[62], pars->GC_11, amp[149]); 
  FFV1_0(w[23], w[5], w[60], pars->GC_11, amp[150]); 
  FFV1_0(w[23], w[5], w[61], pars->GC_11, amp[151]); 
  FFV1_0(w[23], w[5], w[62], pars->GC_11, amp[152]); 
  VVV1_0(w[60], w[2], w[10], pars->GC_10, amp[153]); 
  VVV1_0(w[61], w[2], w[10], pars->GC_10, amp[154]); 
  VVV1_0(w[62], w[2], w[10], pars->GC_10, amp[155]); 
  VVV1_0(w[0], w[36], w[10], pars->GC_10, amp[156]); 
  VVV1_0(w[0], w[37], w[10], pars->GC_10, amp[157]); 
  VVV1_0(w[0], w[38], w[10], pars->GC_10, amp[158]); 
  FFV1_0(w[64], w[65], w[7], pars->GC_11, amp[159]); 
  FFV1_0(w[66], w[63], w[7], pars->GC_11, amp[160]); 
  VVV1_0(w[6], w[7], w[67], pars->GC_10, amp[161]); 
  FFV1_0(w[64], w[68], w[12], pars->GC_11, amp[162]); 
  FFV1_0(w[66], w[68], w[4], pars->GC_11, amp[163]); 
  FFV1_0(w[69], w[68], w[6], pars->GC_11, amp[164]); 
  FFV1_0(w[70], w[63], w[12], pars->GC_11, amp[165]); 
  FFV1_0(w[70], w[65], w[4], pars->GC_11, amp[166]); 
  FFV1_0(w[70], w[71], w[6], pars->GC_11, amp[167]); 
  FFV1_0(w[64], w[71], w[16], pars->GC_11, amp[168]); 
  FFV1_0(w[66], w[71], w[3], pars->GC_11, amp[169]); 
  FFV1_0(w[69], w[63], w[16], pars->GC_11, amp[170]); 
  FFV1_0(w[69], w[65], w[3], pars->GC_11, amp[171]); 
  VVVV1_0(w[6], w[3], w[4], w[67], pars->GC_12, amp[172]); 
  VVVV3_0(w[6], w[3], w[4], w[67], pars->GC_12, amp[173]); 
  VVVV4_0(w[6], w[3], w[4], w[67], pars->GC_12, amp[174]); 
  VVV1_0(w[4], w[67], w[16], pars->GC_10, amp[175]); 
  VVV1_0(w[3], w[67], w[12], pars->GC_10, amp[176]); 
  FFV1_0(w[64], w[72], w[18], pars->GC_11, amp[177]); 
  FFV1_0(w[73], w[63], w[18], pars->GC_11, amp[178]); 
  VVV1_0(w[17], w[18], w[67], pars->GC_10, amp[179]); 
  FFV1_0(w[64], w[74], w[22], pars->GC_11, amp[180]); 
  FFV1_0(w[73], w[74], w[4], pars->GC_11, amp[181]); 
  FFV1_0(w[69], w[74], w[17], pars->GC_11, amp[182]); 
  FFV1_0(w[75], w[63], w[22], pars->GC_11, amp[183]); 
  FFV1_0(w[75], w[72], w[4], pars->GC_11, amp[184]); 
  FFV1_0(w[75], w[71], w[17], pars->GC_11, amp[185]); 
  FFV1_0(w[64], w[71], w[24], pars->GC_11, amp[186]); 
  FFV1_0(w[73], w[71], w[2], pars->GC_11, amp[187]); 
  FFV1_0(w[69], w[63], w[24], pars->GC_11, amp[188]); 
  FFV1_0(w[69], w[72], w[2], pars->GC_11, amp[189]); 
  VVVV1_0(w[17], w[2], w[4], w[67], pars->GC_12, amp[190]); 
  VVVV3_0(w[17], w[2], w[4], w[67], pars->GC_12, amp[191]); 
  VVVV4_0(w[17], w[2], w[4], w[67], pars->GC_12, amp[192]); 
  VVV1_0(w[4], w[67], w[24], pars->GC_10, amp[193]); 
  VVV1_0(w[2], w[67], w[22], pars->GC_10, amp[194]); 
  FFV1_0(w[64], w[76], w[26], pars->GC_11, amp[195]); 
  FFV1_0(w[77], w[63], w[26], pars->GC_11, amp[196]); 
  VVV1_0(w[25], w[26], w[67], pars->GC_10, amp[197]); 
  FFV1_0(w[64], w[74], w[29], pars->GC_11, amp[198]); 
  FFV1_0(w[77], w[74], w[3], pars->GC_11, amp[199]); 
  FFV1_0(w[70], w[74], w[25], pars->GC_11, amp[200]); 
  FFV1_0(w[75], w[63], w[29], pars->GC_11, amp[201]); 
  FFV1_0(w[75], w[76], w[3], pars->GC_11, amp[202]); 
  FFV1_0(w[75], w[68], w[25], pars->GC_11, amp[203]); 
  FFV1_0(w[64], w[68], w[30], pars->GC_11, amp[204]); 
  FFV1_0(w[77], w[68], w[2], pars->GC_11, amp[205]); 
  FFV1_0(w[70], w[63], w[30], pars->GC_11, amp[206]); 
  FFV1_0(w[70], w[76], w[2], pars->GC_11, amp[207]); 
  VVVV1_0(w[25], w[2], w[3], w[67], pars->GC_12, amp[208]); 
  VVVV3_0(w[25], w[2], w[3], w[67], pars->GC_12, amp[209]); 
  VVVV4_0(w[25], w[2], w[3], w[67], pars->GC_12, amp[210]); 
  VVV1_0(w[3], w[67], w[30], pars->GC_10, amp[211]); 
  VVV1_0(w[2], w[67], w[29], pars->GC_10, amp[212]); 
  FFV1_0(w[64], w[79], w[26], pars->GC_11, amp[213]); 
  VVV1_0(w[26], w[4], w[80], pars->GC_10, amp[214]); 
  FFV1_0(w[69], w[78], w[26], pars->GC_11, amp[215]); 
  FFV1_0(w[64], w[81], w[18], pars->GC_11, amp[216]); 
  VVV1_0(w[18], w[3], w[80], pars->GC_10, amp[217]); 
  FFV1_0(w[70], w[78], w[18], pars->GC_11, amp[218]); 
  FFV1_0(w[75], w[81], w[4], pars->GC_11, amp[219]); 
  FFV1_0(w[75], w[79], w[3], pars->GC_11, amp[220]); 
  FFV1_0(w[75], w[78], w[7], pars->GC_11, amp[221]); 
  FFV1_0(w[64], w[82], w[7], pars->GC_11, amp[222]); 
  VVV1_0(w[2], w[7], w[80], pars->GC_10, amp[223]); 
  FFV1_0(w[70], w[82], w[4], pars->GC_11, amp[224]); 
  FFV1_0(w[70], w[79], w[2], pars->GC_11, amp[225]); 
  FFV1_0(w[69], w[82], w[3], pars->GC_11, amp[226]); 
  FFV1_0(w[69], w[81], w[2], pars->GC_11, amp[227]); 
  FFV1_0(w[64], w[78], w[36], pars->GC_11, amp[228]); 
  FFV1_0(w[64], w[78], w[37], pars->GC_11, amp[229]); 
  FFV1_0(w[64], w[78], w[38], pars->GC_11, amp[230]); 
  FFV1_0(w[84], w[63], w[26], pars->GC_11, amp[231]); 
  VVV1_0(w[26], w[4], w[85], pars->GC_10, amp[232]); 
  FFV1_0(w[83], w[71], w[26], pars->GC_11, amp[233]); 
  FFV1_0(w[86], w[63], w[18], pars->GC_11, amp[234]); 
  VVV1_0(w[18], w[3], w[85], pars->GC_10, amp[235]); 
  FFV1_0(w[83], w[68], w[18], pars->GC_11, amp[236]); 
  FFV1_0(w[86], w[74], w[4], pars->GC_11, amp[237]); 
  FFV1_0(w[84], w[74], w[3], pars->GC_11, amp[238]); 
  FFV1_0(w[83], w[74], w[7], pars->GC_11, amp[239]); 
  FFV1_0(w[87], w[63], w[7], pars->GC_11, amp[240]); 
  VVV1_0(w[2], w[7], w[85], pars->GC_10, amp[241]); 
  FFV1_0(w[87], w[68], w[4], pars->GC_11, amp[242]); 
  FFV1_0(w[84], w[68], w[2], pars->GC_11, amp[243]); 
  FFV1_0(w[87], w[71], w[3], pars->GC_11, amp[244]); 
  FFV1_0(w[86], w[71], w[2], pars->GC_11, amp[245]); 
  FFV1_0(w[83], w[63], w[36], pars->GC_11, amp[246]); 
  FFV1_0(w[83], w[63], w[37], pars->GC_11, amp[247]); 
  FFV1_0(w[83], w[63], w[38], pars->GC_11, amp[248]); 
  FFV1_0(w[64], w[71], w[44], pars->GC_11, amp[249]); 
  FFV1_0(w[64], w[88], w[26], pars->GC_11, amp[250]); 
  FFV1_0(w[69], w[63], w[44], pars->GC_11, amp[251]); 
  FFV1_0(w[89], w[63], w[26], pars->GC_11, amp[252]); 
  VVVV1_0(w[0], w[26], w[4], w[67], pars->GC_12, amp[253]); 
  VVVV3_0(w[0], w[26], w[4], w[67], pars->GC_12, amp[254]); 
  VVVV4_0(w[0], w[26], w[4], w[67], pars->GC_12, amp[255]); 
  VVV1_0(w[4], w[67], w[44], pars->GC_10, amp[256]); 
  VVV1_0(w[26], w[4], w[90], pars->GC_10, amp[257]); 
  FFV1_0(w[64], w[68], w[48], pars->GC_11, amp[258]); 
  FFV1_0(w[64], w[91], w[18], pars->GC_11, amp[259]); 
  FFV1_0(w[70], w[63], w[48], pars->GC_11, amp[260]); 
  FFV1_0(w[92], w[63], w[18], pars->GC_11, amp[261]); 
  VVVV1_0(w[0], w[18], w[3], w[67], pars->GC_12, amp[262]); 
  VVVV3_0(w[0], w[18], w[3], w[67], pars->GC_12, amp[263]); 
  VVVV4_0(w[0], w[18], w[3], w[67], pars->GC_12, amp[264]); 
  VVV1_0(w[3], w[67], w[48], pars->GC_10, amp[265]); 
  VVV1_0(w[18], w[3], w[90], pars->GC_10, amp[266]); 
  FFV1_0(w[64], w[93], w[7], pars->GC_11, amp[267]); 
  FFV1_0(w[64], w[74], w[52], pars->GC_11, amp[268]); 
  FFV1_0(w[70], w[93], w[4], pars->GC_11, amp[269]); 
  FFV1_0(w[92], w[74], w[4], pars->GC_11, amp[270]); 
  FFV1_0(w[69], w[93], w[3], pars->GC_11, amp[271]); 
  FFV1_0(w[89], w[74], w[3], pars->GC_11, amp[272]); 
  FFV1_0(w[94], w[63], w[7], pars->GC_11, amp[273]); 
  FFV1_0(w[75], w[63], w[52], pars->GC_11, amp[274]); 
  FFV1_0(w[94], w[68], w[4], pars->GC_11, amp[275]); 
  FFV1_0(w[75], w[91], w[4], pars->GC_11, amp[276]); 
  FFV1_0(w[94], w[71], w[3], pars->GC_11, amp[277]); 
  FFV1_0(w[75], w[88], w[3], pars->GC_11, amp[278]); 
  VVVV1_0(w[0], w[2], w[7], w[67], pars->GC_12, amp[279]); 
  VVVV3_0(w[0], w[2], w[7], w[67], pars->GC_12, amp[280]); 
  VVVV4_0(w[0], w[2], w[7], w[67], pars->GC_12, amp[281]); 
  VVV1_0(w[2], w[67], w[52], pars->GC_10, amp[282]); 
  VVV1_0(w[2], w[7], w[90], pars->GC_10, amp[283]); 
  FFV1_0(w[69], w[91], w[2], pars->GC_11, amp[284]); 
  FFV1_0(w[89], w[68], w[2], pars->GC_11, amp[285]); 
  FFV1_0(w[92], w[71], w[2], pars->GC_11, amp[286]); 
  FFV1_0(w[70], w[88], w[2], pars->GC_11, amp[287]); 
  FFV1_0(w[64], w[71], w[54], pars->GC_11, amp[288]); 
  FFV1_0(w[64], w[71], w[55], pars->GC_11, amp[289]); 
  FFV1_0(w[64], w[71], w[56], pars->GC_11, amp[290]); 
  FFV1_0(w[69], w[63], w[54], pars->GC_11, amp[291]); 
  FFV1_0(w[69], w[63], w[55], pars->GC_11, amp[292]); 
  FFV1_0(w[69], w[63], w[56], pars->GC_11, amp[293]); 
  VVV1_0(w[54], w[4], w[67], pars->GC_10, amp[294]); 
  VVV1_0(w[55], w[4], w[67], pars->GC_10, amp[295]); 
  VVV1_0(w[56], w[4], w[67], pars->GC_10, amp[296]); 
  FFV1_0(w[64], w[68], w[57], pars->GC_11, amp[297]); 
  FFV1_0(w[64], w[68], w[58], pars->GC_11, amp[298]); 
  FFV1_0(w[64], w[68], w[59], pars->GC_11, amp[299]); 
  FFV1_0(w[70], w[63], w[57], pars->GC_11, amp[300]); 
  FFV1_0(w[70], w[63], w[58], pars->GC_11, amp[301]); 
  FFV1_0(w[70], w[63], w[59], pars->GC_11, amp[302]); 
  VVV1_0(w[57], w[3], w[67], pars->GC_10, amp[303]); 
  VVV1_0(w[58], w[3], w[67], pars->GC_10, amp[304]); 
  VVV1_0(w[59], w[3], w[67], pars->GC_10, amp[305]); 
  FFV1_0(w[64], w[74], w[60], pars->GC_11, amp[306]); 
  FFV1_0(w[64], w[74], w[61], pars->GC_11, amp[307]); 
  FFV1_0(w[64], w[74], w[62], pars->GC_11, amp[308]); 
  FFV1_0(w[75], w[63], w[60], pars->GC_11, amp[309]); 
  FFV1_0(w[75], w[63], w[61], pars->GC_11, amp[310]); 
  FFV1_0(w[75], w[63], w[62], pars->GC_11, amp[311]); 
  VVV1_0(w[60], w[2], w[67], pars->GC_10, amp[312]); 
  VVV1_0(w[61], w[2], w[67], pars->GC_10, amp[313]); 
  VVV1_0(w[62], w[2], w[67], pars->GC_10, amp[314]); 
  VVV1_0(w[0], w[36], w[67], pars->GC_10, amp[315]); 
  VVV1_0(w[0], w[37], w[67], pars->GC_10, amp[316]); 
  VVV1_0(w[0], w[38], w[67], pars->GC_10, amp[317]); 


}
double PY8MEs_R19_P55_sm_gq_gggq::matrix_19_gu_gggu() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 159;
  const int ncolor = 24; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {54, 54, 54, 54, 54, 54, 54, 54, 54, 54,
      54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54};
  static const double cf[ncolor][ncolor] = {{512, -64, -64, 8, 8, 80, -64, 8,
      8, -1, -1, -10, 8, -1, 80, -10, 71, 62, -1, -10, -10, 62, 62, -28}, {-64,
      512, 8, 80, -64, 8, 8, -64, -1, -10, 8, -1, -1, -10, -10, 62, 62, -28, 8,
      -1, 80, -10, 71, 62}, {-64, 8, 512, -64, 80, 8, 8, -1, 80, -10, 71, 62,
      -64, 8, 8, -1, -1, -10, -10, -1, 62, -28, -10, 62}, {8, 80, -64, 512, 8,
      -64, -1, -10, -10, 62, 62, -28, 8, -64, -1, -10, 8, -1, -1, 8, 71, 62,
      80, -10}, {8, -64, 80, 8, 512, -64, -1, 8, 71, 62, 80, -10, -10, -1, 62,
      -28, -10, 62, -64, 8, 8, -1, -1, -10}, {80, 8, 8, -64, -64, 512, -10, -1,
      62, -28, -10, 62, -1, 8, 71, 62, 80, -10, 8, -64, -1, -10, 8, -1}, {-64,
      8, 8, -1, -1, -10, 512, -64, -64, 8, 8, 80, 80, -10, 8, -1, 62, 71, -10,
      62, -1, -10, -28, 62}, {8, -64, -1, -10, 8, -1, -64, 512, 8, 80, -64, 8,
      -10, 62, -1, -10, -28, 62, 80, -10, 8, -1, 62, 71}, {8, -1, 80, -10, 71,
      62, -64, 8, 512, -64, 80, 8, 8, -1, -64, 8, -10, -1, 62, -28, -10, -1,
      62, -10}, {-1, -10, -10, 62, 62, -28, 8, 80, -64, 512, 8, -64, -1, -10,
      8, -64, -1, 8, 71, 62, -1, 8, -10, 80}, {-1, 8, 71, 62, 80, -10, 8, -64,
      80, 8, 512, -64, 62, -28, -10, -1, 62, -10, 8, -1, -64, 8, -10, -1},
      {-10, -1, 62, -28, -10, 62, 80, 8, 8, -64, -64, 512, 71, 62, -1, 8, -10,
      80, -1, -10, 8, -64, -1, 8}, {8, -1, -64, 8, -10, -1, 80, -10, 8, -1, 62,
      71, 512, -64, -64, 8, 8, 80, 62, -10, -28, 62, -1, -10}, {-1, -10, 8,
      -64, -1, 8, -10, 62, -1, -10, -28, 62, -64, 512, 8, 80, -64, 8, -10, 80,
      62, 71, 8, -1}, {80, -10, 8, -1, 62, 71, 8, -1, -64, 8, -10, -1, -64, 8,
      512, -64, 80, 8, -28, 62, 62, -10, -10, -1}, {-10, 62, -1, -10, -28, 62,
      -1, -10, 8, -64, -1, 8, 8, 80, -64, 512, 8, -64, 62, 71, -10, 80, -1, 8},
      {71, 62, -1, 8, -10, 80, 62, -28, -10, -1, 62, -10, 8, -64, 80, 8, 512,
      -64, -1, 8, -10, -1, -64, 8}, {62, -28, -10, -1, 62, -10, 71, 62, -1, 8,
      -10, 80, 80, 8, 8, -64, -64, 512, -10, -1, -1, 8, 8, -64}, {-1, 8, -10,
      -1, -64, 8, -10, 80, 62, 71, 8, -1, 62, -10, -28, 62, -1, -10, 512, -64,
      -64, 8, 8, 80}, {-10, -1, -1, 8, 8, -64, 62, -10, -28, 62, -1, -10, -10,
      80, 62, 71, 8, -1, -64, 512, 8, 80, -64, 8}, {-10, 80, 62, 71, 8, -1, -1,
      8, -10, -1, -64, 8, -28, 62, 62, -10, -10, -1, -64, 8, 512, -64, 80, 8},
      {62, -10, -28, 62, -1, -10, -10, -1, -1, 8, 8, -64, 62, 71, -10, 80, -1,
      8, 8, 80, -64, 512, 8, -64}, {62, 71, -10, 80, -1, 8, -28, 62, 62, -10,
      -10, -1, -1, 8, -10, -1, -64, 8, 8, -64, 80, 8, 512, -64}, {-28, 62, 62,
      -10, -10, -1, 62, 71, -10, 80, -1, 8, -10, -1, -1, 8, 8, -64, 80, 8, 8,
      -64, -64, 512}};

  // Calculate color flows
  jamp[0] = -amp[0] + Complex<double> (0, 1) * amp[2] - amp[11] -
      Complex<double> (0, 1) * amp[12] + Complex<double> (0, 1) * amp[13] -
      Complex<double> (0, 1) * amp[15] + Complex<double> (0, 1) * amp[16] -
      amp[55] - Complex<double> (0, 1) * amp[56] - Complex<double> (0, 1) *
      amp[63] - amp[64] + amp[67] + amp[71] - amp[69] - amp[92] +
      Complex<double> (0, 1) * amp[94] - Complex<double> (0, 1) * amp[96] +
      Complex<double> (0, 1) * amp[97] - Complex<double> (0, 1) * amp[98] +
      Complex<double> (0, 1) * amp[120] - Complex<double> (0, 1) * amp[122] -
      Complex<double> (0, 1) * amp[124] + amp[134] - amp[132] + Complex<double>
      (0, 1) * amp[135] - Complex<double> (0, 1) * amp[137] + Complex<double>
      (0, 1) * amp[156] - Complex<double> (0, 1) * amp[158];
  jamp[1] = +amp[0] - Complex<double> (0, 1) * amp[2] - amp[6] -
      Complex<double> (0, 1) * amp[7] + Complex<double> (0, 1) * amp[14] +
      Complex<double> (0, 1) * amp[15] + Complex<double> (0, 1) * amp[17] -
      amp[58] - Complex<double> (0, 1) * amp[59] + Complex<double> (0, 1) *
      amp[63] + amp[64] + amp[65] + amp[70] + amp[69] - amp[101] +
      Complex<double> (0, 1) * amp[103] - Complex<double> (0, 1) * amp[105] +
      Complex<double> (0, 1) * amp[106] - Complex<double> (0, 1) * amp[107] -
      Complex<double> (0, 1) * amp[120] + Complex<double> (0, 1) * amp[122] +
      Complex<double> (0, 1) * amp[124] + amp[143] - amp[141] + Complex<double>
      (0, 1) * amp[144] - Complex<double> (0, 1) * amp[146] - Complex<double>
      (0, 1) * amp[157] - Complex<double> (0, 1) * amp[156];
  jamp[2] = -amp[18] + Complex<double> (0, 1) * amp[20] - amp[29] -
      Complex<double> (0, 1) * amp[30] + Complex<double> (0, 1) * amp[31] -
      Complex<double> (0, 1) * amp[33] + Complex<double> (0, 1) * amp[34] +
      amp[55] + Complex<double> (0, 1) * amp[56] - Complex<double> (0, 1) *
      amp[57] + amp[58] + amp[68] - amp[71] - amp[70] + amp[92] -
      Complex<double> (0, 1) * amp[94] + Complex<double> (0, 1) * amp[96] -
      Complex<double> (0, 1) * amp[97] + Complex<double> (0, 1) * amp[98] +
      Complex<double> (0, 1) * amp[104] + Complex<double> (0, 1) * amp[105] +
      Complex<double> (0, 1) * amp[107] + amp[133] + amp[132] - Complex<double>
      (0, 1) * amp[136] - Complex<double> (0, 1) * amp[135] + Complex<double>
      (0, 1) * amp[157] + Complex<double> (0, 1) * amp[158];
  jamp[3] = +amp[18] - Complex<double> (0, 1) * amp[20] - amp[24] -
      Complex<double> (0, 1) * amp[25] + Complex<double> (0, 1) * amp[32] +
      Complex<double> (0, 1) * amp[33] + Complex<double> (0, 1) * amp[35] +
      Complex<double> (0, 1) * amp[57] - amp[58] + amp[60] - Complex<double>
      (0, 1) * amp[62] + amp[64] + amp[70] + amp[69] - Complex<double> (0, 1) *
      amp[104] - Complex<double> (0, 1) * amp[105] - Complex<double> (0, 1) *
      amp[107] - amp[115] + Complex<double> (0, 1) * amp[121] + Complex<double>
      (0, 1) * amp[122] + Complex<double> (0, 1) * amp[123] + Complex<double>
      (0, 1) * amp[124] + amp[152] - amp[150] + Complex<double> (0, 1) *
      amp[153] - Complex<double> (0, 1) * amp[155] - Complex<double> (0, 1) *
      amp[157] - Complex<double> (0, 1) * amp[156];
  jamp[4] = -amp[36] + Complex<double> (0, 1) * amp[38] - amp[47] -
      Complex<double> (0, 1) * amp[48] + Complex<double> (0, 1) * amp[49] -
      Complex<double> (0, 1) * amp[51] + Complex<double> (0, 1) * amp[52] -
      Complex<double> (0, 1) * amp[54] + amp[55] + amp[58] + Complex<double>
      (0, 1) * amp[59] + amp[66] - amp[71] - amp[70] + Complex<double> (0, 1) *
      amp[95] + Complex<double> (0, 1) * amp[96] + Complex<double> (0, 1) *
      amp[98] + amp[101] - Complex<double> (0, 1) * amp[103] + Complex<double>
      (0, 1) * amp[105] - Complex<double> (0, 1) * amp[106] + Complex<double>
      (0, 1) * amp[107] + amp[142] + amp[141] - Complex<double> (0, 1) *
      amp[145] - Complex<double> (0, 1) * amp[144] + Complex<double> (0, 1) *
      amp[157] + Complex<double> (0, 1) * amp[158];
  jamp[5] = +amp[36] - Complex<double> (0, 1) * amp[38] - amp[42] -
      Complex<double> (0, 1) * amp[43] + Complex<double> (0, 1) * amp[50] +
      Complex<double> (0, 1) * amp[51] + Complex<double> (0, 1) * amp[53] +
      Complex<double> (0, 1) * amp[54] - amp[55] + amp[61] + Complex<double>
      (0, 1) * amp[62] - amp[64] + amp[71] - amp[69] - Complex<double> (0, 1) *
      amp[95] - Complex<double> (0, 1) * amp[96] - Complex<double> (0, 1) *
      amp[98] + amp[115] - Complex<double> (0, 1) * amp[121] - Complex<double>
      (0, 1) * amp[122] - Complex<double> (0, 1) * amp[123] - Complex<double>
      (0, 1) * amp[124] + amp[151] + amp[150] - Complex<double> (0, 1) *
      amp[154] - Complex<double> (0, 1) * amp[153] + Complex<double> (0, 1) *
      amp[156] - Complex<double> (0, 1) * amp[158];
  jamp[6] = +amp[0] - Complex<double> (0, 1) * amp[2] + amp[11] +
      Complex<double> (0, 1) * amp[12] - Complex<double> (0, 1) * amp[13] +
      Complex<double> (0, 1) * amp[15] - Complex<double> (0, 1) * amp[16] -
      amp[21] - Complex<double> (0, 1) * amp[23] + amp[29] - Complex<double>
      (0, 1) * amp[31] - Complex<double> (0, 1) * amp[32] - Complex<double> (0,
      1) * amp[34] - Complex<double> (0, 1) * amp[35] - Complex<double> (0, 1)
      * amp[108] - amp[109] + amp[112] - Complex<double> (0, 1) * amp[120] -
      Complex<double> (0, 1) * amp[121] - Complex<double> (0, 1) * amp[123] -
      amp[134] - amp[133] + Complex<double> (0, 1) * amp[136] + Complex<double>
      (0, 1) * amp[137] + amp[149] - amp[147] - Complex<double> (0, 1) *
      amp[153] + Complex<double> (0, 1) * amp[155];
  jamp[7] = -amp[0] + Complex<double> (0, 1) * amp[2] + amp[6] +
      Complex<double> (0, 1) * amp[7] - Complex<double> (0, 1) * amp[14] -
      Complex<double> (0, 1) * amp[15] - Complex<double> (0, 1) * amp[17] -
      amp[39] - Complex<double> (0, 1) * amp[41] + amp[47] - Complex<double>
      (0, 1) * amp[49] - Complex<double> (0, 1) * amp[50] - Complex<double> (0,
      1) * amp[52] - Complex<double> (0, 1) * amp[53] + Complex<double> (0, 1)
      * amp[108] + amp[109] + amp[110] + Complex<double> (0, 1) * amp[120] +
      Complex<double> (0, 1) * amp[121] + Complex<double> (0, 1) * amp[123] -
      amp[143] - amp[142] + Complex<double> (0, 1) * amp[145] + Complex<double>
      (0, 1) * amp[146] + amp[148] + amp[147] + Complex<double> (0, 1) *
      amp[154] + Complex<double> (0, 1) * amp[153];
  jamp[8] = +amp[21] + Complex<double> (0, 1) * amp[23] - amp[29] +
      Complex<double> (0, 1) * amp[31] + Complex<double> (0, 1) * amp[32] +
      Complex<double> (0, 1) * amp[34] + Complex<double> (0, 1) * amp[35] -
      amp[37] - Complex<double> (0, 1) * amp[38] + amp[39] - Complex<double>
      (0, 1) * amp[40] + Complex<double> (0, 1) * amp[50] + Complex<double> (0,
      1) * amp[51] + Complex<double> (0, 1) * amp[53] + amp[92] -
      Complex<double> (0, 1) * amp[93] - Complex<double> (0, 1) * amp[94] -
      Complex<double> (0, 1) * amp[95] - Complex<double> (0, 1) * amp[97] +
      amp[113] + amp[133] + amp[132] - Complex<double> (0, 1) * amp[136] -
      Complex<double> (0, 1) * amp[135] - amp[149] - amp[148] - Complex<double>
      (0, 1) * amp[154] - Complex<double> (0, 1) * amp[155];
  jamp[9] = +amp[37] + Complex<double> (0, 1) * amp[38] - amp[39] +
      Complex<double> (0, 1) * amp[40] - Complex<double> (0, 1) * amp[50] -
      Complex<double> (0, 1) * amp[51] - Complex<double> (0, 1) * amp[53] -
      Complex<double> (0, 1) * amp[72] - amp[73] + amp[79] - Complex<double>
      (0, 1) * amp[80] - amp[82] + amp[89] - amp[87] + Complex<double> (0, 1) *
      amp[95] + Complex<double> (0, 1) * amp[96] + Complex<double> (0, 1) *
      amp[98] + amp[109] + Complex<double> (0, 1) * amp[121] + Complex<double>
      (0, 1) * amp[122] + Complex<double> (0, 1) * amp[123] + Complex<double>
      (0, 1) * amp[124] + amp[148] + amp[147] + Complex<double> (0, 1) *
      amp[154] + Complex<double> (0, 1) * amp[153] - Complex<double> (0, 1) *
      amp[156] + Complex<double> (0, 1) * amp[158];
  jamp[10] = -amp[19] - Complex<double> (0, 1) * amp[20] + amp[21] -
      Complex<double> (0, 1) * amp[22] + Complex<double> (0, 1) * amp[32] +
      Complex<double> (0, 1) * amp[33] + Complex<double> (0, 1) * amp[35] +
      amp[39] + Complex<double> (0, 1) * amp[41] - amp[47] + Complex<double>
      (0, 1) * amp[49] + Complex<double> (0, 1) * amp[50] + Complex<double> (0,
      1) * amp[52] + Complex<double> (0, 1) * amp[53] + amp[101] -
      Complex<double> (0, 1) * amp[102] - Complex<double> (0, 1) * amp[103] -
      Complex<double> (0, 1) * amp[104] - Complex<double> (0, 1) * amp[106] +
      amp[111] + amp[142] + amp[141] - Complex<double> (0, 1) * amp[145] -
      Complex<double> (0, 1) * amp[144] - amp[149] - amp[148] - Complex<double>
      (0, 1) * amp[154] - Complex<double> (0, 1) * amp[155];
  jamp[11] = +amp[19] + Complex<double> (0, 1) * amp[20] - amp[21] +
      Complex<double> (0, 1) * amp[22] - Complex<double> (0, 1) * amp[32] -
      Complex<double> (0, 1) * amp[33] - Complex<double> (0, 1) * amp[35] -
      Complex<double> (0, 1) * amp[75] - amp[76] + amp[78] + Complex<double>
      (0, 1) * amp[80] + amp[82] + amp[88] + amp[87] + Complex<double> (0, 1) *
      amp[104] + Complex<double> (0, 1) * amp[105] + Complex<double> (0, 1) *
      amp[107] - amp[109] - Complex<double> (0, 1) * amp[121] - Complex<double>
      (0, 1) * amp[122] - Complex<double> (0, 1) * amp[123] - Complex<double>
      (0, 1) * amp[124] + amp[149] - amp[147] - Complex<double> (0, 1) *
      amp[153] + Complex<double> (0, 1) * amp[155] + Complex<double> (0, 1) *
      amp[157] + Complex<double> (0, 1) * amp[156];
  jamp[12] = -amp[3] - Complex<double> (0, 1) * amp[5] + amp[11] -
      Complex<double> (0, 1) * amp[13] - Complex<double> (0, 1) * amp[14] -
      Complex<double> (0, 1) * amp[16] - Complex<double> (0, 1) * amp[17] +
      amp[18] - Complex<double> (0, 1) * amp[20] + amp[29] + Complex<double>
      (0, 1) * amp[30] - Complex<double> (0, 1) * amp[31] + Complex<double> (0,
      1) * amp[33] - Complex<double> (0, 1) * amp[34] - amp[99] -
      Complex<double> (0, 1) * amp[100] - Complex<double> (0, 1) * amp[103] -
      Complex<double> (0, 1) * amp[104] - Complex<double> (0, 1) * amp[106] +
      amp[125] - amp[134] - amp[133] + Complex<double> (0, 1) * amp[136] +
      Complex<double> (0, 1) * amp[137] + amp[140] - amp[138] - Complex<double>
      (0, 1) * amp[144] + Complex<double> (0, 1) * amp[146];
  jamp[13] = -amp[18] + Complex<double> (0, 1) * amp[20] + amp[24] +
      Complex<double> (0, 1) * amp[25] - Complex<double> (0, 1) * amp[32] -
      Complex<double> (0, 1) * amp[33] - Complex<double> (0, 1) * amp[35] +
      amp[42] - Complex<double> (0, 1) * amp[44] - amp[45] - Complex<double>
      (0, 1) * amp[49] - Complex<double> (0, 1) * amp[50] - Complex<double> (0,
      1) * amp[52] - Complex<double> (0, 1) * amp[53] + amp[99] +
      Complex<double> (0, 1) * amp[100] + Complex<double> (0, 1) * amp[103] +
      Complex<double> (0, 1) * amp[104] + Complex<double> (0, 1) * amp[106] +
      amp[117] + amp[139] + amp[138] + Complex<double> (0, 1) * amp[145] +
      Complex<double> (0, 1) * amp[144] - amp[152] - amp[151] + Complex<double>
      (0, 1) * amp[154] + Complex<double> (0, 1) * amp[155];
  jamp[14] = +amp[3] + Complex<double> (0, 1) * amp[5] - amp[11] +
      Complex<double> (0, 1) * amp[13] + Complex<double> (0, 1) * amp[14] +
      Complex<double> (0, 1) * amp[16] + Complex<double> (0, 1) * amp[17] +
      amp[37] + Complex<double> (0, 1) * amp[38] + amp[45] - Complex<double>
      (0, 1) * amp[46] + Complex<double> (0, 1) * amp[49] - Complex<double> (0,
      1) * amp[51] + Complex<double> (0, 1) * amp[52] - amp[92] +
      Complex<double> (0, 1) * amp[93] + Complex<double> (0, 1) * amp[94] +
      Complex<double> (0, 1) * amp[95] + Complex<double> (0, 1) * amp[97] +
      amp[126] + amp[134] - amp[132] + Complex<double> (0, 1) * amp[135] -
      Complex<double> (0, 1) * amp[137] - amp[140] - amp[139] - Complex<double>
      (0, 1) * amp[145] - Complex<double> (0, 1) * amp[146];
  jamp[15] = -amp[37] - Complex<double> (0, 1) * amp[38] - amp[45] +
      Complex<double> (0, 1) * amp[46] - Complex<double> (0, 1) * amp[49] +
      Complex<double> (0, 1) * amp[51] - Complex<double> (0, 1) * amp[52] +
      Complex<double> (0, 1) * amp[72] + amp[73] + amp[76] - Complex<double>
      (0, 1) * amp[77] + amp[84] - amp[89] - amp[88] - Complex<double> (0, 1) *
      amp[95] - Complex<double> (0, 1) * amp[96] - Complex<double> (0, 1) *
      amp[98] + amp[99] + Complex<double> (0, 1) * amp[103] - Complex<double>
      (0, 1) * amp[105] + Complex<double> (0, 1) * amp[106] - Complex<double>
      (0, 1) * amp[107] + amp[139] + amp[138] + Complex<double> (0, 1) *
      amp[145] + Complex<double> (0, 1) * amp[144] - Complex<double> (0, 1) *
      amp[157] - Complex<double> (0, 1) * amp[158];
  jamp[16] = -amp[1] - Complex<double> (0, 1) * amp[2] + amp[3] -
      Complex<double> (0, 1) * amp[4] + Complex<double> (0, 1) * amp[14] +
      Complex<double> (0, 1) * amp[15] + Complex<double> (0, 1) * amp[17] -
      amp[42] + Complex<double> (0, 1) * amp[44] + amp[45] + Complex<double>
      (0, 1) * amp[49] + Complex<double> (0, 1) * amp[50] + Complex<double> (0,
      1) * amp[52] + Complex<double> (0, 1) * amp[53] - Complex<double> (0, 1)
      * amp[114] + amp[115] + amp[116] - Complex<double> (0, 1) * amp[120] -
      Complex<double> (0, 1) * amp[121] - Complex<double> (0, 1) * amp[123] -
      amp[140] - amp[139] - Complex<double> (0, 1) * amp[145] - Complex<double>
      (0, 1) * amp[146] + amp[151] + amp[150] - Complex<double> (0, 1) *
      amp[154] - Complex<double> (0, 1) * amp[153];
  jamp[17] = +amp[1] + Complex<double> (0, 1) * amp[2] - amp[3] +
      Complex<double> (0, 1) * amp[4] - Complex<double> (0, 1) * amp[14] -
      Complex<double> (0, 1) * amp[15] - Complex<double> (0, 1) * amp[17] -
      amp[76] + Complex<double> (0, 1) * amp[77] - Complex<double> (0, 1) *
      amp[81] + amp[82] + amp[83] + amp[88] + amp[87] - amp[99] -
      Complex<double> (0, 1) * amp[103] + Complex<double> (0, 1) * amp[105] -
      Complex<double> (0, 1) * amp[106] + Complex<double> (0, 1) * amp[107] +
      Complex<double> (0, 1) * amp[120] - Complex<double> (0, 1) * amp[122] -
      Complex<double> (0, 1) * amp[124] + amp[140] - amp[138] - Complex<double>
      (0, 1) * amp[144] + Complex<double> (0, 1) * amp[146] + Complex<double>
      (0, 1) * amp[157] + Complex<double> (0, 1) * amp[156];
  jamp[18] = +amp[6] - Complex<double> (0, 1) * amp[8] - amp[9] -
      Complex<double> (0, 1) * amp[13] - Complex<double> (0, 1) * amp[14] -
      Complex<double> (0, 1) * amp[16] - Complex<double> (0, 1) * amp[17] +
      amp[36] - Complex<double> (0, 1) * amp[38] + amp[47] + Complex<double>
      (0, 1) * amp[48] - Complex<double> (0, 1) * amp[49] + Complex<double> (0,
      1) * amp[51] - Complex<double> (0, 1) * amp[52] - amp[90] -
      Complex<double> (0, 1) * amp[91] - Complex<double> (0, 1) * amp[94] -
      Complex<double> (0, 1) * amp[95] - Complex<double> (0, 1) * amp[97] +
      amp[128] + amp[131] - amp[129] - Complex<double> (0, 1) * amp[135] +
      Complex<double> (0, 1) * amp[137] - amp[143] - amp[142] + Complex<double>
      (0, 1) * amp[145] + Complex<double> (0, 1) * amp[146];
  jamp[19] = +amp[24] - Complex<double> (0, 1) * amp[26] - amp[27] -
      Complex<double> (0, 1) * amp[31] - Complex<double> (0, 1) * amp[32] -
      Complex<double> (0, 1) * amp[34] - Complex<double> (0, 1) * amp[35] -
      amp[36] + Complex<double> (0, 1) * amp[38] + amp[42] + Complex<double>
      (0, 1) * amp[43] - Complex<double> (0, 1) * amp[50] - Complex<double> (0,
      1) * amp[51] - Complex<double> (0, 1) * amp[53] + amp[90] +
      Complex<double> (0, 1) * amp[91] + Complex<double> (0, 1) * amp[94] +
      Complex<double> (0, 1) * amp[95] + Complex<double> (0, 1) * amp[97] +
      amp[119] + amp[130] + amp[129] + Complex<double> (0, 1) * amp[136] +
      Complex<double> (0, 1) * amp[135] - amp[152] - amp[151] + Complex<double>
      (0, 1) * amp[154] + Complex<double> (0, 1) * amp[155];
  jamp[20] = -amp[6] + Complex<double> (0, 1) * amp[8] + amp[9] +
      Complex<double> (0, 1) * amp[13] + Complex<double> (0, 1) * amp[14] +
      Complex<double> (0, 1) * amp[16] + Complex<double> (0, 1) * amp[17] +
      amp[19] + Complex<double> (0, 1) * amp[20] + amp[27] - Complex<double>
      (0, 1) * amp[28] + Complex<double> (0, 1) * amp[31] - Complex<double> (0,
      1) * amp[33] + Complex<double> (0, 1) * amp[34] - amp[101] +
      Complex<double> (0, 1) * amp[102] + Complex<double> (0, 1) * amp[103] +
      Complex<double> (0, 1) * amp[104] + Complex<double> (0, 1) * amp[106] +
      amp[127] - amp[131] - amp[130] - Complex<double> (0, 1) * amp[136] -
      Complex<double> (0, 1) * amp[137] + amp[143] - amp[141] + Complex<double>
      (0, 1) * amp[144] - Complex<double> (0, 1) * amp[146];
  jamp[21] = -amp[19] - Complex<double> (0, 1) * amp[20] - amp[27] +
      Complex<double> (0, 1) * amp[28] - Complex<double> (0, 1) * amp[31] +
      Complex<double> (0, 1) * amp[33] - Complex<double> (0, 1) * amp[34] +
      amp[73] - Complex<double> (0, 1) * amp[74] + Complex<double> (0, 1) *
      amp[75] + amp[76] + amp[86] - amp[89] - amp[88] + amp[90] +
      Complex<double> (0, 1) * amp[94] - Complex<double> (0, 1) * amp[96] +
      Complex<double> (0, 1) * amp[97] - Complex<double> (0, 1) * amp[98] -
      Complex<double> (0, 1) * amp[104] - Complex<double> (0, 1) * amp[105] -
      Complex<double> (0, 1) * amp[107] + amp[130] + amp[129] + Complex<double>
      (0, 1) * amp[136] + Complex<double> (0, 1) * amp[135] - Complex<double>
      (0, 1) * amp[157] - Complex<double> (0, 1) * amp[158];
  jamp[22] = +amp[1] + Complex<double> (0, 1) * amp[2] + amp[9] -
      Complex<double> (0, 1) * amp[10] + Complex<double> (0, 1) * amp[13] -
      Complex<double> (0, 1) * amp[15] + Complex<double> (0, 1) * amp[16] -
      amp[24] + Complex<double> (0, 1) * amp[26] + amp[27] + Complex<double>
      (0, 1) * amp[31] + Complex<double> (0, 1) * amp[32] + Complex<double> (0,
      1) * amp[34] + Complex<double> (0, 1) * amp[35] + Complex<double> (0, 1)
      * amp[114] - amp[115] + amp[118] + Complex<double> (0, 1) * amp[120] +
      Complex<double> (0, 1) * amp[121] + Complex<double> (0, 1) * amp[123] -
      amp[131] - amp[130] - Complex<double> (0, 1) * amp[136] - Complex<double>
      (0, 1) * amp[137] + amp[152] - amp[150] + Complex<double> (0, 1) *
      amp[153] - Complex<double> (0, 1) * amp[155];
  jamp[23] = -amp[1] - Complex<double> (0, 1) * amp[2] - amp[9] +
      Complex<double> (0, 1) * amp[10] - Complex<double> (0, 1) * amp[13] +
      Complex<double> (0, 1) * amp[15] - Complex<double> (0, 1) * amp[16] -
      amp[73] + Complex<double> (0, 1) * amp[74] + Complex<double> (0, 1) *
      amp[81] - amp[82] + amp[85] + amp[89] - amp[87] - amp[90] -
      Complex<double> (0, 1) * amp[94] + Complex<double> (0, 1) * amp[96] -
      Complex<double> (0, 1) * amp[97] + Complex<double> (0, 1) * amp[98] -
      Complex<double> (0, 1) * amp[120] + Complex<double> (0, 1) * amp[122] +
      Complex<double> (0, 1) * amp[124] + amp[131] - amp[129] - Complex<double>
      (0, 1) * amp[135] + Complex<double> (0, 1) * amp[137] - Complex<double>
      (0, 1) * amp[156] + Complex<double> (0, 1) * amp[158];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[0][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R19_P55_sm_gq_gggq::matrix_19_gux_gggux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 159;
  const int ncolor = 24; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {54, 54, 54, 54, 54, 54, 54, 54, 54, 54,
      54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54};
  static const double cf[ncolor][ncolor] = {{512, -64, -64, 8, 8, 80, -64, 8,
      8, -1, -1, -10, 8, -1, 80, -10, 71, 62, -1, -10, -10, 62, 62, -28}, {-64,
      512, 8, 80, -64, 8, 8, -64, -1, -10, 8, -1, -1, -10, -10, 62, 62, -28, 8,
      -1, 80, -10, 71, 62}, {-64, 8, 512, -64, 80, 8, 8, -1, 80, -10, 71, 62,
      -64, 8, 8, -1, -1, -10, -10, -1, 62, -28, -10, 62}, {8, 80, -64, 512, 8,
      -64, -1, -10, -10, 62, 62, -28, 8, -64, -1, -10, 8, -1, -1, 8, 71, 62,
      80, -10}, {8, -64, 80, 8, 512, -64, -1, 8, 71, 62, 80, -10, -10, -1, 62,
      -28, -10, 62, -64, 8, 8, -1, -1, -10}, {80, 8, 8, -64, -64, 512, -10, -1,
      62, -28, -10, 62, -1, 8, 71, 62, 80, -10, 8, -64, -1, -10, 8, -1}, {-64,
      8, 8, -1, -1, -10, 512, -64, -64, 8, 8, 80, 80, -10, 8, -1, 62, 71, -10,
      62, -1, -10, -28, 62}, {8, -64, -1, -10, 8, -1, -64, 512, 8, 80, -64, 8,
      -10, 62, -1, -10, -28, 62, 80, -10, 8, -1, 62, 71}, {8, -1, 80, -10, 71,
      62, -64, 8, 512, -64, 80, 8, 8, -1, -64, 8, -10, -1, 62, -28, -10, -1,
      62, -10}, {-1, -10, -10, 62, 62, -28, 8, 80, -64, 512, 8, -64, -1, -10,
      8, -64, -1, 8, 71, 62, -1, 8, -10, 80}, {-1, 8, 71, 62, 80, -10, 8, -64,
      80, 8, 512, -64, 62, -28, -10, -1, 62, -10, 8, -1, -64, 8, -10, -1},
      {-10, -1, 62, -28, -10, 62, 80, 8, 8, -64, -64, 512, 71, 62, -1, 8, -10,
      80, -1, -10, 8, -64, -1, 8}, {8, -1, -64, 8, -10, -1, 80, -10, 8, -1, 62,
      71, 512, -64, -64, 8, 8, 80, 62, -10, -28, 62, -1, -10}, {-1, -10, 8,
      -64, -1, 8, -10, 62, -1, -10, -28, 62, -64, 512, 8, 80, -64, 8, -10, 80,
      62, 71, 8, -1}, {80, -10, 8, -1, 62, 71, 8, -1, -64, 8, -10, -1, -64, 8,
      512, -64, 80, 8, -28, 62, 62, -10, -10, -1}, {-10, 62, -1, -10, -28, 62,
      -1, -10, 8, -64, -1, 8, 8, 80, -64, 512, 8, -64, 62, 71, -10, 80, -1, 8},
      {71, 62, -1, 8, -10, 80, 62, -28, -10, -1, 62, -10, 8, -64, 80, 8, 512,
      -64, -1, 8, -10, -1, -64, 8}, {62, -28, -10, -1, 62, -10, 71, 62, -1, 8,
      -10, 80, 80, 8, 8, -64, -64, 512, -10, -1, -1, 8, 8, -64}, {-1, 8, -10,
      -1, -64, 8, -10, 80, 62, 71, 8, -1, 62, -10, -28, 62, -1, -10, 512, -64,
      -64, 8, 8, 80}, {-10, -1, -1, 8, 8, -64, 62, -10, -28, 62, -1, -10, -10,
      80, 62, 71, 8, -1, -64, 512, 8, 80, -64, 8}, {-10, 80, 62, 71, 8, -1, -1,
      8, -10, -1, -64, 8, -28, 62, 62, -10, -10, -1, -64, 8, 512, -64, 80, 8},
      {62, -10, -28, 62, -1, -10, -10, -1, -1, 8, 8, -64, 62, 71, -10, 80, -1,
      8, 8, 80, -64, 512, 8, -64}, {62, 71, -10, 80, -1, 8, -28, 62, 62, -10,
      -10, -1, -1, 8, -10, -1, -64, 8, 8, -64, 80, 8, 512, -64}, {-28, 62, 62,
      -10, -10, -1, 62, 71, -10, 80, -1, 8, -10, -1, -1, 8, 8, -64, 80, 8, 8,
      -64, -64, 512}};

  // Calculate color flows
  jamp[0] = +amp[159] - Complex<double> (0, 1) * amp[161] + amp[170] +
      Complex<double> (0, 1) * amp[171] - Complex<double> (0, 1) * amp[172] +
      Complex<double> (0, 1) * amp[174] - Complex<double> (0, 1) * amp[175] +
      amp[214] + Complex<double> (0, 1) * amp[215] + Complex<double> (0, 1) *
      amp[222] + amp[223] - amp[226] - amp[230] + amp[228] + amp[251] -
      Complex<double> (0, 1) * amp[253] + Complex<double> (0, 1) * amp[255] -
      Complex<double> (0, 1) * amp[256] + Complex<double> (0, 1) * amp[257] -
      Complex<double> (0, 1) * amp[279] + Complex<double> (0, 1) * amp[281] +
      Complex<double> (0, 1) * amp[283] - amp[293] + amp[291] - Complex<double>
      (0, 1) * amp[294] + Complex<double> (0, 1) * amp[296] - Complex<double>
      (0, 1) * amp[315] + Complex<double> (0, 1) * amp[317];
  jamp[1] = -amp[159] + Complex<double> (0, 1) * amp[161] + amp[165] +
      Complex<double> (0, 1) * amp[166] - Complex<double> (0, 1) * amp[173] -
      Complex<double> (0, 1) * amp[174] - Complex<double> (0, 1) * amp[176] +
      amp[217] + Complex<double> (0, 1) * amp[218] - Complex<double> (0, 1) *
      amp[222] - amp[223] - amp[224] - amp[229] - amp[228] + amp[260] -
      Complex<double> (0, 1) * amp[262] + Complex<double> (0, 1) * amp[264] -
      Complex<double> (0, 1) * amp[265] + Complex<double> (0, 1) * amp[266] +
      Complex<double> (0, 1) * amp[279] - Complex<double> (0, 1) * amp[281] -
      Complex<double> (0, 1) * amp[283] - amp[302] + amp[300] - Complex<double>
      (0, 1) * amp[303] + Complex<double> (0, 1) * amp[305] + Complex<double>
      (0, 1) * amp[316] + Complex<double> (0, 1) * amp[315];
  jamp[2] = +amp[177] - Complex<double> (0, 1) * amp[179] + amp[188] +
      Complex<double> (0, 1) * amp[189] - Complex<double> (0, 1) * amp[190] +
      Complex<double> (0, 1) * amp[192] - Complex<double> (0, 1) * amp[193] -
      amp[214] - Complex<double> (0, 1) * amp[215] + Complex<double> (0, 1) *
      amp[216] - amp[217] - amp[227] + amp[230] + amp[229] - amp[251] +
      Complex<double> (0, 1) * amp[253] - Complex<double> (0, 1) * amp[255] +
      Complex<double> (0, 1) * amp[256] - Complex<double> (0, 1) * amp[257] -
      Complex<double> (0, 1) * amp[263] - Complex<double> (0, 1) * amp[264] -
      Complex<double> (0, 1) * amp[266] - amp[292] - amp[291] + Complex<double>
      (0, 1) * amp[295] + Complex<double> (0, 1) * amp[294] - Complex<double>
      (0, 1) * amp[316] - Complex<double> (0, 1) * amp[317];
  jamp[3] = -amp[177] + Complex<double> (0, 1) * amp[179] + amp[183] +
      Complex<double> (0, 1) * amp[184] - Complex<double> (0, 1) * amp[191] -
      Complex<double> (0, 1) * amp[192] - Complex<double> (0, 1) * amp[194] -
      Complex<double> (0, 1) * amp[216] + amp[217] - amp[219] + Complex<double>
      (0, 1) * amp[221] - amp[223] - amp[229] - amp[228] + Complex<double> (0,
      1) * amp[263] + Complex<double> (0, 1) * amp[264] + Complex<double> (0,
      1) * amp[266] + amp[274] - Complex<double> (0, 1) * amp[280] -
      Complex<double> (0, 1) * amp[281] - Complex<double> (0, 1) * amp[282] -
      Complex<double> (0, 1) * amp[283] - amp[311] + amp[309] - Complex<double>
      (0, 1) * amp[312] + Complex<double> (0, 1) * amp[314] + Complex<double>
      (0, 1) * amp[316] + Complex<double> (0, 1) * amp[315];
  jamp[4] = +amp[195] - Complex<double> (0, 1) * amp[197] + amp[206] +
      Complex<double> (0, 1) * amp[207] - Complex<double> (0, 1) * amp[208] +
      Complex<double> (0, 1) * amp[210] - Complex<double> (0, 1) * amp[211] +
      Complex<double> (0, 1) * amp[213] - amp[214] - amp[217] - Complex<double>
      (0, 1) * amp[218] - amp[225] + amp[230] + amp[229] - Complex<double> (0,
      1) * amp[254] - Complex<double> (0, 1) * amp[255] - Complex<double> (0,
      1) * amp[257] - amp[260] + Complex<double> (0, 1) * amp[262] -
      Complex<double> (0, 1) * amp[264] + Complex<double> (0, 1) * amp[265] -
      Complex<double> (0, 1) * amp[266] - amp[301] - amp[300] + Complex<double>
      (0, 1) * amp[304] + Complex<double> (0, 1) * amp[303] - Complex<double>
      (0, 1) * amp[316] - Complex<double> (0, 1) * amp[317];
  jamp[5] = -amp[195] + Complex<double> (0, 1) * amp[197] + amp[201] +
      Complex<double> (0, 1) * amp[202] - Complex<double> (0, 1) * amp[209] -
      Complex<double> (0, 1) * amp[210] - Complex<double> (0, 1) * amp[212] -
      Complex<double> (0, 1) * amp[213] + amp[214] - amp[220] - Complex<double>
      (0, 1) * amp[221] + amp[223] - amp[230] + amp[228] + Complex<double> (0,
      1) * amp[254] + Complex<double> (0, 1) * amp[255] + Complex<double> (0,
      1) * amp[257] - amp[274] + Complex<double> (0, 1) * amp[280] +
      Complex<double> (0, 1) * amp[281] + Complex<double> (0, 1) * amp[282] +
      Complex<double> (0, 1) * amp[283] - amp[310] - amp[309] + Complex<double>
      (0, 1) * amp[313] + Complex<double> (0, 1) * amp[312] - Complex<double>
      (0, 1) * amp[315] + Complex<double> (0, 1) * amp[317];
  jamp[6] = -amp[159] + Complex<double> (0, 1) * amp[161] - amp[170] -
      Complex<double> (0, 1) * amp[171] + Complex<double> (0, 1) * amp[172] -
      Complex<double> (0, 1) * amp[174] + Complex<double> (0, 1) * amp[175] +
      amp[180] + Complex<double> (0, 1) * amp[182] - amp[188] + Complex<double>
      (0, 1) * amp[190] + Complex<double> (0, 1) * amp[191] + Complex<double>
      (0, 1) * amp[193] + Complex<double> (0, 1) * amp[194] + Complex<double>
      (0, 1) * amp[267] + amp[268] - amp[271] + Complex<double> (0, 1) *
      amp[279] + Complex<double> (0, 1) * amp[280] + Complex<double> (0, 1) *
      amp[282] + amp[293] + amp[292] - Complex<double> (0, 1) * amp[295] -
      Complex<double> (0, 1) * amp[296] - amp[308] + amp[306] + Complex<double>
      (0, 1) * amp[312] - Complex<double> (0, 1) * amp[314];
  jamp[7] = +amp[159] - Complex<double> (0, 1) * amp[161] - amp[165] -
      Complex<double> (0, 1) * amp[166] + Complex<double> (0, 1) * amp[173] +
      Complex<double> (0, 1) * amp[174] + Complex<double> (0, 1) * amp[176] +
      amp[198] + Complex<double> (0, 1) * amp[200] - amp[206] + Complex<double>
      (0, 1) * amp[208] + Complex<double> (0, 1) * amp[209] + Complex<double>
      (0, 1) * amp[211] + Complex<double> (0, 1) * amp[212] - Complex<double>
      (0, 1) * amp[267] - amp[268] - amp[269] - Complex<double> (0, 1) *
      amp[279] - Complex<double> (0, 1) * amp[280] - Complex<double> (0, 1) *
      amp[282] + amp[302] + amp[301] - Complex<double> (0, 1) * amp[304] -
      Complex<double> (0, 1) * amp[305] - amp[307] - amp[306] - Complex<double>
      (0, 1) * amp[313] - Complex<double> (0, 1) * amp[312];
  jamp[8] = -amp[180] - Complex<double> (0, 1) * amp[182] + amp[188] -
      Complex<double> (0, 1) * amp[190] - Complex<double> (0, 1) * amp[191] -
      Complex<double> (0, 1) * amp[193] - Complex<double> (0, 1) * amp[194] +
      amp[196] + Complex<double> (0, 1) * amp[197] - amp[198] + Complex<double>
      (0, 1) * amp[199] - Complex<double> (0, 1) * amp[209] - Complex<double>
      (0, 1) * amp[210] - Complex<double> (0, 1) * amp[212] - amp[251] +
      Complex<double> (0, 1) * amp[252] + Complex<double> (0, 1) * amp[253] +
      Complex<double> (0, 1) * amp[254] + Complex<double> (0, 1) * amp[256] -
      amp[272] - amp[292] - amp[291] + Complex<double> (0, 1) * amp[295] +
      Complex<double> (0, 1) * amp[294] + amp[308] + amp[307] + Complex<double>
      (0, 1) * amp[313] + Complex<double> (0, 1) * amp[314];
  jamp[9] = -amp[196] - Complex<double> (0, 1) * amp[197] + amp[198] -
      Complex<double> (0, 1) * amp[199] + Complex<double> (0, 1) * amp[209] +
      Complex<double> (0, 1) * amp[210] + Complex<double> (0, 1) * amp[212] +
      Complex<double> (0, 1) * amp[231] + amp[232] - amp[238] + Complex<double>
      (0, 1) * amp[239] + amp[241] - amp[248] + amp[246] - Complex<double> (0,
      1) * amp[254] - Complex<double> (0, 1) * amp[255] - Complex<double> (0,
      1) * amp[257] - amp[268] - Complex<double> (0, 1) * amp[280] -
      Complex<double> (0, 1) * amp[281] - Complex<double> (0, 1) * amp[282] -
      Complex<double> (0, 1) * amp[283] - amp[307] - amp[306] - Complex<double>
      (0, 1) * amp[313] - Complex<double> (0, 1) * amp[312] + Complex<double>
      (0, 1) * amp[315] - Complex<double> (0, 1) * amp[317];
  jamp[10] = +amp[178] + Complex<double> (0, 1) * amp[179] - amp[180] +
      Complex<double> (0, 1) * amp[181] - Complex<double> (0, 1) * amp[191] -
      Complex<double> (0, 1) * amp[192] - Complex<double> (0, 1) * amp[194] -
      amp[198] - Complex<double> (0, 1) * amp[200] + amp[206] - Complex<double>
      (0, 1) * amp[208] - Complex<double> (0, 1) * amp[209] - Complex<double>
      (0, 1) * amp[211] - Complex<double> (0, 1) * amp[212] - amp[260] +
      Complex<double> (0, 1) * amp[261] + Complex<double> (0, 1) * amp[262] +
      Complex<double> (0, 1) * amp[263] + Complex<double> (0, 1) * amp[265] -
      amp[270] - amp[301] - amp[300] + Complex<double> (0, 1) * amp[304] +
      Complex<double> (0, 1) * amp[303] + amp[308] + amp[307] + Complex<double>
      (0, 1) * amp[313] + Complex<double> (0, 1) * amp[314];
  jamp[11] = -amp[178] - Complex<double> (0, 1) * amp[179] + amp[180] -
      Complex<double> (0, 1) * amp[181] + Complex<double> (0, 1) * amp[191] +
      Complex<double> (0, 1) * amp[192] + Complex<double> (0, 1) * amp[194] +
      Complex<double> (0, 1) * amp[234] + amp[235] - amp[237] - Complex<double>
      (0, 1) * amp[239] - amp[241] - amp[247] - amp[246] - Complex<double> (0,
      1) * amp[263] - Complex<double> (0, 1) * amp[264] - Complex<double> (0,
      1) * amp[266] + amp[268] + Complex<double> (0, 1) * amp[280] +
      Complex<double> (0, 1) * amp[281] + Complex<double> (0, 1) * amp[282] +
      Complex<double> (0, 1) * amp[283] - amp[308] + amp[306] + Complex<double>
      (0, 1) * amp[312] - Complex<double> (0, 1) * amp[314] - Complex<double>
      (0, 1) * amp[316] - Complex<double> (0, 1) * amp[315];
  jamp[12] = +amp[162] + Complex<double> (0, 1) * amp[164] - amp[170] +
      Complex<double> (0, 1) * amp[172] + Complex<double> (0, 1) * amp[173] +
      Complex<double> (0, 1) * amp[175] + Complex<double> (0, 1) * amp[176] -
      amp[177] + Complex<double> (0, 1) * amp[179] - amp[188] - Complex<double>
      (0, 1) * amp[189] + Complex<double> (0, 1) * amp[190] - Complex<double>
      (0, 1) * amp[192] + Complex<double> (0, 1) * amp[193] + amp[258] +
      Complex<double> (0, 1) * amp[259] + Complex<double> (0, 1) * amp[262] +
      Complex<double> (0, 1) * amp[263] + Complex<double> (0, 1) * amp[265] -
      amp[284] + amp[293] + amp[292] - Complex<double> (0, 1) * amp[295] -
      Complex<double> (0, 1) * amp[296] - amp[299] + amp[297] + Complex<double>
      (0, 1) * amp[303] - Complex<double> (0, 1) * amp[305];
  jamp[13] = +amp[177] - Complex<double> (0, 1) * amp[179] - amp[183] -
      Complex<double> (0, 1) * amp[184] + Complex<double> (0, 1) * amp[191] +
      Complex<double> (0, 1) * amp[192] + Complex<double> (0, 1) * amp[194] -
      amp[201] + Complex<double> (0, 1) * amp[203] + amp[204] + Complex<double>
      (0, 1) * amp[208] + Complex<double> (0, 1) * amp[209] + Complex<double>
      (0, 1) * amp[211] + Complex<double> (0, 1) * amp[212] - amp[258] -
      Complex<double> (0, 1) * amp[259] - Complex<double> (0, 1) * amp[262] -
      Complex<double> (0, 1) * amp[263] - Complex<double> (0, 1) * amp[265] -
      amp[276] - amp[298] - amp[297] - Complex<double> (0, 1) * amp[304] -
      Complex<double> (0, 1) * amp[303] + amp[311] + amp[310] - Complex<double>
      (0, 1) * amp[313] - Complex<double> (0, 1) * amp[314];
  jamp[14] = -amp[162] - Complex<double> (0, 1) * amp[164] + amp[170] -
      Complex<double> (0, 1) * amp[172] - Complex<double> (0, 1) * amp[173] -
      Complex<double> (0, 1) * amp[175] - Complex<double> (0, 1) * amp[176] -
      amp[196] - Complex<double> (0, 1) * amp[197] - amp[204] + Complex<double>
      (0, 1) * amp[205] - Complex<double> (0, 1) * amp[208] + Complex<double>
      (0, 1) * amp[210] - Complex<double> (0, 1) * amp[211] + amp[251] -
      Complex<double> (0, 1) * amp[252] - Complex<double> (0, 1) * amp[253] -
      Complex<double> (0, 1) * amp[254] - Complex<double> (0, 1) * amp[256] -
      amp[285] - amp[293] + amp[291] - Complex<double> (0, 1) * amp[294] +
      Complex<double> (0, 1) * amp[296] + amp[299] + amp[298] + Complex<double>
      (0, 1) * amp[304] + Complex<double> (0, 1) * amp[305];
  jamp[15] = +amp[196] + Complex<double> (0, 1) * amp[197] + amp[204] -
      Complex<double> (0, 1) * amp[205] + Complex<double> (0, 1) * amp[208] -
      Complex<double> (0, 1) * amp[210] + Complex<double> (0, 1) * amp[211] -
      Complex<double> (0, 1) * amp[231] - amp[232] - amp[235] + Complex<double>
      (0, 1) * amp[236] - amp[243] + amp[248] + amp[247] + Complex<double> (0,
      1) * amp[254] + Complex<double> (0, 1) * amp[255] + Complex<double> (0,
      1) * amp[257] - amp[258] - Complex<double> (0, 1) * amp[262] +
      Complex<double> (0, 1) * amp[264] - Complex<double> (0, 1) * amp[265] +
      Complex<double> (0, 1) * amp[266] - amp[298] - amp[297] - Complex<double>
      (0, 1) * amp[304] - Complex<double> (0, 1) * amp[303] + Complex<double>
      (0, 1) * amp[316] + Complex<double> (0, 1) * amp[317];
  jamp[16] = +amp[160] + Complex<double> (0, 1) * amp[161] - amp[162] +
      Complex<double> (0, 1) * amp[163] - Complex<double> (0, 1) * amp[173] -
      Complex<double> (0, 1) * amp[174] - Complex<double> (0, 1) * amp[176] +
      amp[201] - Complex<double> (0, 1) * amp[203] - amp[204] - Complex<double>
      (0, 1) * amp[208] - Complex<double> (0, 1) * amp[209] - Complex<double>
      (0, 1) * amp[211] - Complex<double> (0, 1) * amp[212] + Complex<double>
      (0, 1) * amp[273] - amp[274] - amp[275] + Complex<double> (0, 1) *
      amp[279] + Complex<double> (0, 1) * amp[280] + Complex<double> (0, 1) *
      amp[282] + amp[299] + amp[298] + Complex<double> (0, 1) * amp[304] +
      Complex<double> (0, 1) * amp[305] - amp[310] - amp[309] + Complex<double>
      (0, 1) * amp[313] + Complex<double> (0, 1) * amp[312];
  jamp[17] = -amp[160] - Complex<double> (0, 1) * amp[161] + amp[162] -
      Complex<double> (0, 1) * amp[163] + Complex<double> (0, 1) * amp[173] +
      Complex<double> (0, 1) * amp[174] + Complex<double> (0, 1) * amp[176] +
      amp[235] - Complex<double> (0, 1) * amp[236] + Complex<double> (0, 1) *
      amp[240] - amp[241] - amp[242] - amp[247] - amp[246] + amp[258] +
      Complex<double> (0, 1) * amp[262] - Complex<double> (0, 1) * amp[264] +
      Complex<double> (0, 1) * amp[265] - Complex<double> (0, 1) * amp[266] -
      Complex<double> (0, 1) * amp[279] + Complex<double> (0, 1) * amp[281] +
      Complex<double> (0, 1) * amp[283] - amp[299] + amp[297] + Complex<double>
      (0, 1) * amp[303] - Complex<double> (0, 1) * amp[305] - Complex<double>
      (0, 1) * amp[316] - Complex<double> (0, 1) * amp[315];
  jamp[18] = -amp[165] + Complex<double> (0, 1) * amp[167] + amp[168] +
      Complex<double> (0, 1) * amp[172] + Complex<double> (0, 1) * amp[173] +
      Complex<double> (0, 1) * amp[175] + Complex<double> (0, 1) * amp[176] -
      amp[195] + Complex<double> (0, 1) * amp[197] - amp[206] - Complex<double>
      (0, 1) * amp[207] + Complex<double> (0, 1) * amp[208] - Complex<double>
      (0, 1) * amp[210] + Complex<double> (0, 1) * amp[211] + amp[249] +
      Complex<double> (0, 1) * amp[250] + Complex<double> (0, 1) * amp[253] +
      Complex<double> (0, 1) * amp[254] + Complex<double> (0, 1) * amp[256] -
      amp[287] - amp[290] + amp[288] + Complex<double> (0, 1) * amp[294] -
      Complex<double> (0, 1) * amp[296] + amp[302] + amp[301] - Complex<double>
      (0, 1) * amp[304] - Complex<double> (0, 1) * amp[305];
  jamp[19] = -amp[183] + Complex<double> (0, 1) * amp[185] + amp[186] +
      Complex<double> (0, 1) * amp[190] + Complex<double> (0, 1) * amp[191] +
      Complex<double> (0, 1) * amp[193] + Complex<double> (0, 1) * amp[194] +
      amp[195] - Complex<double> (0, 1) * amp[197] - amp[201] - Complex<double>
      (0, 1) * amp[202] + Complex<double> (0, 1) * amp[209] + Complex<double>
      (0, 1) * amp[210] + Complex<double> (0, 1) * amp[212] - amp[249] -
      Complex<double> (0, 1) * amp[250] - Complex<double> (0, 1) * amp[253] -
      Complex<double> (0, 1) * amp[254] - Complex<double> (0, 1) * amp[256] -
      amp[278] - amp[289] - amp[288] - Complex<double> (0, 1) * amp[295] -
      Complex<double> (0, 1) * amp[294] + amp[311] + amp[310] - Complex<double>
      (0, 1) * amp[313] - Complex<double> (0, 1) * amp[314];
  jamp[20] = +amp[165] - Complex<double> (0, 1) * amp[167] - amp[168] -
      Complex<double> (0, 1) * amp[172] - Complex<double> (0, 1) * amp[173] -
      Complex<double> (0, 1) * amp[175] - Complex<double> (0, 1) * amp[176] -
      amp[178] - Complex<double> (0, 1) * amp[179] - amp[186] + Complex<double>
      (0, 1) * amp[187] - Complex<double> (0, 1) * amp[190] + Complex<double>
      (0, 1) * amp[192] - Complex<double> (0, 1) * amp[193] + amp[260] -
      Complex<double> (0, 1) * amp[261] - Complex<double> (0, 1) * amp[262] -
      Complex<double> (0, 1) * amp[263] - Complex<double> (0, 1) * amp[265] -
      amp[286] + amp[290] + amp[289] + Complex<double> (0, 1) * amp[295] +
      Complex<double> (0, 1) * amp[296] - amp[302] + amp[300] - Complex<double>
      (0, 1) * amp[303] + Complex<double> (0, 1) * amp[305];
  jamp[21] = +amp[178] + Complex<double> (0, 1) * amp[179] + amp[186] -
      Complex<double> (0, 1) * amp[187] + Complex<double> (0, 1) * amp[190] -
      Complex<double> (0, 1) * amp[192] + Complex<double> (0, 1) * amp[193] -
      amp[232] + Complex<double> (0, 1) * amp[233] - Complex<double> (0, 1) *
      amp[234] - amp[235] - amp[245] + amp[248] + amp[247] - amp[249] -
      Complex<double> (0, 1) * amp[253] + Complex<double> (0, 1) * amp[255] -
      Complex<double> (0, 1) * amp[256] + Complex<double> (0, 1) * amp[257] +
      Complex<double> (0, 1) * amp[263] + Complex<double> (0, 1) * amp[264] +
      Complex<double> (0, 1) * amp[266] - amp[289] - amp[288] - Complex<double>
      (0, 1) * amp[295] - Complex<double> (0, 1) * amp[294] + Complex<double>
      (0, 1) * amp[316] + Complex<double> (0, 1) * amp[317];
  jamp[22] = -amp[160] - Complex<double> (0, 1) * amp[161] - amp[168] +
      Complex<double> (0, 1) * amp[169] - Complex<double> (0, 1) * amp[172] +
      Complex<double> (0, 1) * amp[174] - Complex<double> (0, 1) * amp[175] +
      amp[183] - Complex<double> (0, 1) * amp[185] - amp[186] - Complex<double>
      (0, 1) * amp[190] - Complex<double> (0, 1) * amp[191] - Complex<double>
      (0, 1) * amp[193] - Complex<double> (0, 1) * amp[194] - Complex<double>
      (0, 1) * amp[273] + amp[274] - amp[277] - Complex<double> (0, 1) *
      amp[279] - Complex<double> (0, 1) * amp[280] - Complex<double> (0, 1) *
      amp[282] + amp[290] + amp[289] + Complex<double> (0, 1) * amp[295] +
      Complex<double> (0, 1) * amp[296] - amp[311] + amp[309] - Complex<double>
      (0, 1) * amp[312] + Complex<double> (0, 1) * amp[314];
  jamp[23] = +amp[160] + Complex<double> (0, 1) * amp[161] + amp[168] -
      Complex<double> (0, 1) * amp[169] + Complex<double> (0, 1) * amp[172] -
      Complex<double> (0, 1) * amp[174] + Complex<double> (0, 1) * amp[175] +
      amp[232] - Complex<double> (0, 1) * amp[233] - Complex<double> (0, 1) *
      amp[240] + amp[241] - amp[244] - amp[248] + amp[246] + amp[249] +
      Complex<double> (0, 1) * amp[253] - Complex<double> (0, 1) * amp[255] +
      Complex<double> (0, 1) * amp[256] - Complex<double> (0, 1) * amp[257] +
      Complex<double> (0, 1) * amp[279] - Complex<double> (0, 1) * amp[281] -
      Complex<double> (0, 1) * amp[283] - amp[290] + amp[288] + Complex<double>
      (0, 1) * amp[294] - Complex<double> (0, 1) * amp[296] + Complex<double>
      (0, 1) * amp[315] - Complex<double> (0, 1) * amp[317];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[1][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}


}  // end namespace PY8MEs_namespace

