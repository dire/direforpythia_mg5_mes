//==========================================================================
// This file has been automatically generated for Pythia 8 by
// MadGraph5_aMC@NLO v. 2.6.0, 2017-08-16
// By the MadGraph5_aMC@NLO Development Team
// Visit launchpad.net/madgraph5 and amcatnlo.web.cern.ch
//==========================================================================

#include "PY8MEs_R12_P27_sm_gq_zzggq.h"
#include "HelAmps_sm.h"

using namespace Pythia8_sm; 

namespace PY8MEs_namespace 
{
//==========================================================================
// Class member functions for calculating the matrix elements for
// Process: g u > z z g g u WEIGHTED<=7 @12
// Process: g c > z z g g c WEIGHTED<=7 @12
// Process: g d > z z g g d WEIGHTED<=7 @12
// Process: g s > z z g g s WEIGHTED<=7 @12
// Process: g u~ > z z g g u~ WEIGHTED<=7 @12
// Process: g c~ > z z g g c~ WEIGHTED<=7 @12
// Process: g d~ > z z g g d~ WEIGHTED<=7 @12
// Process: g s~ > z z g g s~ WEIGHTED<=7 @12

// Exception class
class PY8MEs_R12_P27_sm_gq_zzggqException : public exception
{
  virtual const char * what() const throw()
  {
    return "Exception in class 'PY8MEs_R12_P27_sm_gq_zzggq'."; 
  }
}
PY8MEs_R12_P27_sm_gq_zzggq_exception; 

std::set<int> PY8MEs_R12_P27_sm_gq_zzggq::s_channel_proc = std::set<int>
    (createset<int> ());

int PY8MEs_R12_P27_sm_gq_zzggq::helicities[ncomb][nexternal] = {{-1, -1, -1,
    -1, -1, -1, -1}, {-1, -1, -1, -1, -1, -1, 1}, {-1, -1, -1, -1, -1, 1, -1},
    {-1, -1, -1, -1, -1, 1, 1}, {-1, -1, -1, -1, 1, -1, -1}, {-1, -1, -1, -1,
    1, -1, 1}, {-1, -1, -1, -1, 1, 1, -1}, {-1, -1, -1, -1, 1, 1, 1}, {-1, -1,
    -1, 0, -1, -1, -1}, {-1, -1, -1, 0, -1, -1, 1}, {-1, -1, -1, 0, -1, 1, -1},
    {-1, -1, -1, 0, -1, 1, 1}, {-1, -1, -1, 0, 1, -1, -1}, {-1, -1, -1, 0, 1,
    -1, 1}, {-1, -1, -1, 0, 1, 1, -1}, {-1, -1, -1, 0, 1, 1, 1}, {-1, -1, -1,
    1, -1, -1, -1}, {-1, -1, -1, 1, -1, -1, 1}, {-1, -1, -1, 1, -1, 1, -1},
    {-1, -1, -1, 1, -1, 1, 1}, {-1, -1, -1, 1, 1, -1, -1}, {-1, -1, -1, 1, 1,
    -1, 1}, {-1, -1, -1, 1, 1, 1, -1}, {-1, -1, -1, 1, 1, 1, 1}, {-1, -1, 0,
    -1, -1, -1, -1}, {-1, -1, 0, -1, -1, -1, 1}, {-1, -1, 0, -1, -1, 1, -1},
    {-1, -1, 0, -1, -1, 1, 1}, {-1, -1, 0, -1, 1, -1, -1}, {-1, -1, 0, -1, 1,
    -1, 1}, {-1, -1, 0, -1, 1, 1, -1}, {-1, -1, 0, -1, 1, 1, 1}, {-1, -1, 0, 0,
    -1, -1, -1}, {-1, -1, 0, 0, -1, -1, 1}, {-1, -1, 0, 0, -1, 1, -1}, {-1, -1,
    0, 0, -1, 1, 1}, {-1, -1, 0, 0, 1, -1, -1}, {-1, -1, 0, 0, 1, -1, 1}, {-1,
    -1, 0, 0, 1, 1, -1}, {-1, -1, 0, 0, 1, 1, 1}, {-1, -1, 0, 1, -1, -1, -1},
    {-1, -1, 0, 1, -1, -1, 1}, {-1, -1, 0, 1, -1, 1, -1}, {-1, -1, 0, 1, -1, 1,
    1}, {-1, -1, 0, 1, 1, -1, -1}, {-1, -1, 0, 1, 1, -1, 1}, {-1, -1, 0, 1, 1,
    1, -1}, {-1, -1, 0, 1, 1, 1, 1}, {-1, -1, 1, -1, -1, -1, -1}, {-1, -1, 1,
    -1, -1, -1, 1}, {-1, -1, 1, -1, -1, 1, -1}, {-1, -1, 1, -1, -1, 1, 1}, {-1,
    -1, 1, -1, 1, -1, -1}, {-1, -1, 1, -1, 1, -1, 1}, {-1, -1, 1, -1, 1, 1,
    -1}, {-1, -1, 1, -1, 1, 1, 1}, {-1, -1, 1, 0, -1, -1, -1}, {-1, -1, 1, 0,
    -1, -1, 1}, {-1, -1, 1, 0, -1, 1, -1}, {-1, -1, 1, 0, -1, 1, 1}, {-1, -1,
    1, 0, 1, -1, -1}, {-1, -1, 1, 0, 1, -1, 1}, {-1, -1, 1, 0, 1, 1, -1}, {-1,
    -1, 1, 0, 1, 1, 1}, {-1, -1, 1, 1, -1, -1, -1}, {-1, -1, 1, 1, -1, -1, 1},
    {-1, -1, 1, 1, -1, 1, -1}, {-1, -1, 1, 1, -1, 1, 1}, {-1, -1, 1, 1, 1, -1,
    -1}, {-1, -1, 1, 1, 1, -1, 1}, {-1, -1, 1, 1, 1, 1, -1}, {-1, -1, 1, 1, 1,
    1, 1}, {-1, 1, -1, -1, -1, -1, -1}, {-1, 1, -1, -1, -1, -1, 1}, {-1, 1, -1,
    -1, -1, 1, -1}, {-1, 1, -1, -1, -1, 1, 1}, {-1, 1, -1, -1, 1, -1, -1}, {-1,
    1, -1, -1, 1, -1, 1}, {-1, 1, -1, -1, 1, 1, -1}, {-1, 1, -1, -1, 1, 1, 1},
    {-1, 1, -1, 0, -1, -1, -1}, {-1, 1, -1, 0, -1, -1, 1}, {-1, 1, -1, 0, -1,
    1, -1}, {-1, 1, -1, 0, -1, 1, 1}, {-1, 1, -1, 0, 1, -1, -1}, {-1, 1, -1, 0,
    1, -1, 1}, {-1, 1, -1, 0, 1, 1, -1}, {-1, 1, -1, 0, 1, 1, 1}, {-1, 1, -1,
    1, -1, -1, -1}, {-1, 1, -1, 1, -1, -1, 1}, {-1, 1, -1, 1, -1, 1, -1}, {-1,
    1, -1, 1, -1, 1, 1}, {-1, 1, -1, 1, 1, -1, -1}, {-1, 1, -1, 1, 1, -1, 1},
    {-1, 1, -1, 1, 1, 1, -1}, {-1, 1, -1, 1, 1, 1, 1}, {-1, 1, 0, -1, -1, -1,
    -1}, {-1, 1, 0, -1, -1, -1, 1}, {-1, 1, 0, -1, -1, 1, -1}, {-1, 1, 0, -1,
    -1, 1, 1}, {-1, 1, 0, -1, 1, -1, -1}, {-1, 1, 0, -1, 1, -1, 1}, {-1, 1, 0,
    -1, 1, 1, -1}, {-1, 1, 0, -1, 1, 1, 1}, {-1, 1, 0, 0, -1, -1, -1}, {-1, 1,
    0, 0, -1, -1, 1}, {-1, 1, 0, 0, -1, 1, -1}, {-1, 1, 0, 0, -1, 1, 1}, {-1,
    1, 0, 0, 1, -1, -1}, {-1, 1, 0, 0, 1, -1, 1}, {-1, 1, 0, 0, 1, 1, -1}, {-1,
    1, 0, 0, 1, 1, 1}, {-1, 1, 0, 1, -1, -1, -1}, {-1, 1, 0, 1, -1, -1, 1},
    {-1, 1, 0, 1, -1, 1, -1}, {-1, 1, 0, 1, -1, 1, 1}, {-1, 1, 0, 1, 1, -1,
    -1}, {-1, 1, 0, 1, 1, -1, 1}, {-1, 1, 0, 1, 1, 1, -1}, {-1, 1, 0, 1, 1, 1,
    1}, {-1, 1, 1, -1, -1, -1, -1}, {-1, 1, 1, -1, -1, -1, 1}, {-1, 1, 1, -1,
    -1, 1, -1}, {-1, 1, 1, -1, -1, 1, 1}, {-1, 1, 1, -1, 1, -1, -1}, {-1, 1, 1,
    -1, 1, -1, 1}, {-1, 1, 1, -1, 1, 1, -1}, {-1, 1, 1, -1, 1, 1, 1}, {-1, 1,
    1, 0, -1, -1, -1}, {-1, 1, 1, 0, -1, -1, 1}, {-1, 1, 1, 0, -1, 1, -1}, {-1,
    1, 1, 0, -1, 1, 1}, {-1, 1, 1, 0, 1, -1, -1}, {-1, 1, 1, 0, 1, -1, 1}, {-1,
    1, 1, 0, 1, 1, -1}, {-1, 1, 1, 0, 1, 1, 1}, {-1, 1, 1, 1, -1, -1, -1}, {-1,
    1, 1, 1, -1, -1, 1}, {-1, 1, 1, 1, -1, 1, -1}, {-1, 1, 1, 1, -1, 1, 1},
    {-1, 1, 1, 1, 1, -1, -1}, {-1, 1, 1, 1, 1, -1, 1}, {-1, 1, 1, 1, 1, 1, -1},
    {-1, 1, 1, 1, 1, 1, 1}, {1, -1, -1, -1, -1, -1, -1}, {1, -1, -1, -1, -1,
    -1, 1}, {1, -1, -1, -1, -1, 1, -1}, {1, -1, -1, -1, -1, 1, 1}, {1, -1, -1,
    -1, 1, -1, -1}, {1, -1, -1, -1, 1, -1, 1}, {1, -1, -1, -1, 1, 1, -1}, {1,
    -1, -1, -1, 1, 1, 1}, {1, -1, -1, 0, -1, -1, -1}, {1, -1, -1, 0, -1, -1,
    1}, {1, -1, -1, 0, -1, 1, -1}, {1, -1, -1, 0, -1, 1, 1}, {1, -1, -1, 0, 1,
    -1, -1}, {1, -1, -1, 0, 1, -1, 1}, {1, -1, -1, 0, 1, 1, -1}, {1, -1, -1, 0,
    1, 1, 1}, {1, -1, -1, 1, -1, -1, -1}, {1, -1, -1, 1, -1, -1, 1}, {1, -1,
    -1, 1, -1, 1, -1}, {1, -1, -1, 1, -1, 1, 1}, {1, -1, -1, 1, 1, -1, -1}, {1,
    -1, -1, 1, 1, -1, 1}, {1, -1, -1, 1, 1, 1, -1}, {1, -1, -1, 1, 1, 1, 1},
    {1, -1, 0, -1, -1, -1, -1}, {1, -1, 0, -1, -1, -1, 1}, {1, -1, 0, -1, -1,
    1, -1}, {1, -1, 0, -1, -1, 1, 1}, {1, -1, 0, -1, 1, -1, -1}, {1, -1, 0, -1,
    1, -1, 1}, {1, -1, 0, -1, 1, 1, -1}, {1, -1, 0, -1, 1, 1, 1}, {1, -1, 0, 0,
    -1, -1, -1}, {1, -1, 0, 0, -1, -1, 1}, {1, -1, 0, 0, -1, 1, -1}, {1, -1, 0,
    0, -1, 1, 1}, {1, -1, 0, 0, 1, -1, -1}, {1, -1, 0, 0, 1, -1, 1}, {1, -1, 0,
    0, 1, 1, -1}, {1, -1, 0, 0, 1, 1, 1}, {1, -1, 0, 1, -1, -1, -1}, {1, -1, 0,
    1, -1, -1, 1}, {1, -1, 0, 1, -1, 1, -1}, {1, -1, 0, 1, -1, 1, 1}, {1, -1,
    0, 1, 1, -1, -1}, {1, -1, 0, 1, 1, -1, 1}, {1, -1, 0, 1, 1, 1, -1}, {1, -1,
    0, 1, 1, 1, 1}, {1, -1, 1, -1, -1, -1, -1}, {1, -1, 1, -1, -1, -1, 1}, {1,
    -1, 1, -1, -1, 1, -1}, {1, -1, 1, -1, -1, 1, 1}, {1, -1, 1, -1, 1, -1, -1},
    {1, -1, 1, -1, 1, -1, 1}, {1, -1, 1, -1, 1, 1, -1}, {1, -1, 1, -1, 1, 1,
    1}, {1, -1, 1, 0, -1, -1, -1}, {1, -1, 1, 0, -1, -1, 1}, {1, -1, 1, 0, -1,
    1, -1}, {1, -1, 1, 0, -1, 1, 1}, {1, -1, 1, 0, 1, -1, -1}, {1, -1, 1, 0, 1,
    -1, 1}, {1, -1, 1, 0, 1, 1, -1}, {1, -1, 1, 0, 1, 1, 1}, {1, -1, 1, 1, -1,
    -1, -1}, {1, -1, 1, 1, -1, -1, 1}, {1, -1, 1, 1, -1, 1, -1}, {1, -1, 1, 1,
    -1, 1, 1}, {1, -1, 1, 1, 1, -1, -1}, {1, -1, 1, 1, 1, -1, 1}, {1, -1, 1, 1,
    1, 1, -1}, {1, -1, 1, 1, 1, 1, 1}, {1, 1, -1, -1, -1, -1, -1}, {1, 1, -1,
    -1, -1, -1, 1}, {1, 1, -1, -1, -1, 1, -1}, {1, 1, -1, -1, -1, 1, 1}, {1, 1,
    -1, -1, 1, -1, -1}, {1, 1, -1, -1, 1, -1, 1}, {1, 1, -1, -1, 1, 1, -1}, {1,
    1, -1, -1, 1, 1, 1}, {1, 1, -1, 0, -1, -1, -1}, {1, 1, -1, 0, -1, -1, 1},
    {1, 1, -1, 0, -1, 1, -1}, {1, 1, -1, 0, -1, 1, 1}, {1, 1, -1, 0, 1, -1,
    -1}, {1, 1, -1, 0, 1, -1, 1}, {1, 1, -1, 0, 1, 1, -1}, {1, 1, -1, 0, 1, 1,
    1}, {1, 1, -1, 1, -1, -1, -1}, {1, 1, -1, 1, -1, -1, 1}, {1, 1, -1, 1, -1,
    1, -1}, {1, 1, -1, 1, -1, 1, 1}, {1, 1, -1, 1, 1, -1, -1}, {1, 1, -1, 1, 1,
    -1, 1}, {1, 1, -1, 1, 1, 1, -1}, {1, 1, -1, 1, 1, 1, 1}, {1, 1, 0, -1, -1,
    -1, -1}, {1, 1, 0, -1, -1, -1, 1}, {1, 1, 0, -1, -1, 1, -1}, {1, 1, 0, -1,
    -1, 1, 1}, {1, 1, 0, -1, 1, -1, -1}, {1, 1, 0, -1, 1, -1, 1}, {1, 1, 0, -1,
    1, 1, -1}, {1, 1, 0, -1, 1, 1, 1}, {1, 1, 0, 0, -1, -1, -1}, {1, 1, 0, 0,
    -1, -1, 1}, {1, 1, 0, 0, -1, 1, -1}, {1, 1, 0, 0, -1, 1, 1}, {1, 1, 0, 0,
    1, -1, -1}, {1, 1, 0, 0, 1, -1, 1}, {1, 1, 0, 0, 1, 1, -1}, {1, 1, 0, 0, 1,
    1, 1}, {1, 1, 0, 1, -1, -1, -1}, {1, 1, 0, 1, -1, -1, 1}, {1, 1, 0, 1, -1,
    1, -1}, {1, 1, 0, 1, -1, 1, 1}, {1, 1, 0, 1, 1, -1, -1}, {1, 1, 0, 1, 1,
    -1, 1}, {1, 1, 0, 1, 1, 1, -1}, {1, 1, 0, 1, 1, 1, 1}, {1, 1, 1, -1, -1,
    -1, -1}, {1, 1, 1, -1, -1, -1, 1}, {1, 1, 1, -1, -1, 1, -1}, {1, 1, 1, -1,
    -1, 1, 1}, {1, 1, 1, -1, 1, -1, -1}, {1, 1, 1, -1, 1, -1, 1}, {1, 1, 1, -1,
    1, 1, -1}, {1, 1, 1, -1, 1, 1, 1}, {1, 1, 1, 0, -1, -1, -1}, {1, 1, 1, 0,
    -1, -1, 1}, {1, 1, 1, 0, -1, 1, -1}, {1, 1, 1, 0, -1, 1, 1}, {1, 1, 1, 0,
    1, -1, -1}, {1, 1, 1, 0, 1, -1, 1}, {1, 1, 1, 0, 1, 1, -1}, {1, 1, 1, 0, 1,
    1, 1}, {1, 1, 1, 1, -1, -1, -1}, {1, 1, 1, 1, -1, -1, 1}, {1, 1, 1, 1, -1,
    1, -1}, {1, 1, 1, 1, -1, 1, 1}, {1, 1, 1, 1, 1, -1, -1}, {1, 1, 1, 1, 1,
    -1, 1}, {1, 1, 1, 1, 1, 1, -1}, {1, 1, 1, 1, 1, 1, 1}};

// Normalization factors the various processes
// Denominators: spins, colors and identical particles
int PY8MEs_R12_P27_sm_gq_zzggq::denom_colors[nprocesses] = {24, 24, 24, 24}; 
int PY8MEs_R12_P27_sm_gq_zzggq::denom_hels[nprocesses] = {4, 4, 4, 4}; 
int PY8MEs_R12_P27_sm_gq_zzggq::denom_iden[nprocesses] = {4, 4, 4, 4}; 

//--------------------------------------------------------------------------
// Color config initialization
void PY8MEs_R12_P27_sm_gq_zzggq::initColorConfigs() 
{
  color_configs = vector < vec_vec_int > (); 
  jamp_nc_relative_power = vector < vec_int > (); 

  // Color flows of process Process: g u > z z g g u WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(2)(4)(0)(0)(0)(0)(0)(3)(2)(4)(3)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #1
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(2)(3)(0)(0)(0)(0)(0)(3)(4)(4)(2)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #2
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(2)(4)(0)(0)(0)(0)(0)(3)(1)(4)(2)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #3
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(2)(2)(0)(0)(0)(0)(0)(3)(1)(4)(3)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #4
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(2)(3)(0)(0)(0)(0)(0)(3)(2)(4)(1)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #5
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(2)(2)(0)(0)(0)(0)(0)(3)(4)(4)(1)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 

  // Color flows of process Process: g d > z z g g d WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(2)(4)(0)(0)(0)(0)(0)(3)(2)(4)(3)(1)(0)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #1
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(2)(3)(0)(0)(0)(0)(0)(3)(4)(4)(2)(1)(0)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #2
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(2)(4)(0)(0)(0)(0)(0)(3)(1)(4)(2)(1)(0)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #3
  color_configs[1].push_back(vec_int(createvector<int>
      (4)(2)(2)(0)(0)(0)(0)(0)(3)(1)(4)(3)(1)(0)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #4
  color_configs[1].push_back(vec_int(createvector<int>
      (4)(2)(3)(0)(0)(0)(0)(0)(3)(2)(4)(1)(1)(0)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #5
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(2)(2)(0)(0)(0)(0)(0)(3)(4)(4)(1)(1)(0)));
  jamp_nc_relative_power[1].push_back(0); 

  // Color flows of process Process: g u~ > z z g g u~ WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[2].push_back(vec_int(createvector<int>
      (1)(2)(0)(1)(0)(0)(0)(0)(3)(2)(4)(3)(0)(4)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #1
  color_configs[2].push_back(vec_int(createvector<int>
      (1)(2)(0)(1)(0)(0)(0)(0)(3)(4)(4)(2)(0)(3)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #2
  color_configs[2].push_back(vec_int(createvector<int>
      (3)(2)(0)(1)(0)(0)(0)(0)(3)(1)(4)(2)(0)(4)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #3
  color_configs[2].push_back(vec_int(createvector<int>
      (4)(2)(0)(1)(0)(0)(0)(0)(3)(1)(4)(3)(0)(2)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #4
  color_configs[2].push_back(vec_int(createvector<int>
      (4)(2)(0)(1)(0)(0)(0)(0)(3)(2)(4)(1)(0)(3)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #5
  color_configs[2].push_back(vec_int(createvector<int>
      (3)(2)(0)(1)(0)(0)(0)(0)(3)(4)(4)(1)(0)(2)));
  jamp_nc_relative_power[2].push_back(0); 

  // Color flows of process Process: g d~ > z z g g d~ WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[3].push_back(vec_int(createvector<int>
      (1)(2)(0)(1)(0)(0)(0)(0)(3)(2)(4)(3)(0)(4)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #1
  color_configs[3].push_back(vec_int(createvector<int>
      (1)(2)(0)(1)(0)(0)(0)(0)(3)(4)(4)(2)(0)(3)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #2
  color_configs[3].push_back(vec_int(createvector<int>
      (3)(2)(0)(1)(0)(0)(0)(0)(3)(1)(4)(2)(0)(4)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #3
  color_configs[3].push_back(vec_int(createvector<int>
      (4)(2)(0)(1)(0)(0)(0)(0)(3)(1)(4)(3)(0)(2)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #4
  color_configs[3].push_back(vec_int(createvector<int>
      (4)(2)(0)(1)(0)(0)(0)(0)(3)(2)(4)(1)(0)(3)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #5
  color_configs[3].push_back(vec_int(createvector<int>
      (3)(2)(0)(1)(0)(0)(0)(0)(3)(4)(4)(1)(0)(2)));
  jamp_nc_relative_power[3].push_back(0); 
}

//--------------------------------------------------------------------------
// Destructor.
PY8MEs_R12_P27_sm_gq_zzggq::~PY8MEs_R12_P27_sm_gq_zzggq() 
{
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    delete[] p[i]; 
    p[i] = NULL; 
  }
}

//--------------------------------------------------------------------------
// Invert the permutation mapping
vector<int> PY8MEs_R12_P27_sm_gq_zzggq::invert_mapping(vector<int> mapping) 
{
  vector<int> inverted_mapping; 
  for (unsigned int i = 0; i < mapping.size(); i++ )
  {
    for (unsigned int j = 0; j < mapping.size(); j++ )
    {
      if (mapping[j] == ((int)i))
      {
        inverted_mapping.push_back(j); 
        break; 
      }
    }
  }
  return inverted_mapping; 
}

//--------------------------------------------------------------------------
// Return the list of possible helicity configurations
vector < vec_int > PY8MEs_R12_P27_sm_gq_zzggq::getHelicityConfigs(vector<int>
    permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(ncomb, vector<int> (nexternal, 0)); 
  for (unsigned int ihel = 0; ihel < ncomb; ihel++ )
  {
    for(unsigned int j = 0; j < nexternal; j++ )
    {
      res[ihel][chosenPerm[j]] = helicities[ihel][j]; 
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Return the list of possible color configurations
vector < vec_int > PY8MEs_R12_P27_sm_gq_zzggq::getColorConfigs(int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(color_configs[chosenProcID].size(), vector<int>
      (nexternal * 2, 0));
  for (unsigned int icol = 0; icol < color_configs[chosenProcID].size(); icol++
      )
  {
    for(unsigned int j = 0; j < (2 * nexternal); j++ )
    {
      res[icol][chosenPerm[j/2] * 2 + j%2] =
          color_configs[chosenProcID][icol][j];
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Get JAMP relative N_c power
int PY8MEs_R12_P27_sm_gq_zzggq::getColorFlowRelativeNCPower(int color_flow_ID,
    int specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return jamp_nc_relative_power[chosenProcID][color_flow_ID]; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity ID -> Helicity Config
vector<int> PY8MEs_R12_P27_sm_gq_zzggq::getHelicityConfigForID(int hel_ID,
    vector<int> permutation)
{
  if (hel_ID < 0 || hel_ID >= ncomb)
  {
    cerr <<  "Error in function 'getHelicityConfigForID' of class" << 
    " 'PY8MEs_R12_P27_sm_gq_zzggq': Specified helicity ID '" << 
    hel_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R12_P27_sm_gq_zzggq_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(nexternal, 0); 
  for (unsigned int j = 0; j < nexternal; j++ )
  {
    res[chosenPerm[j]] = helicities[hel_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity Config -> Helicity ID
int PY8MEs_R12_P27_sm_gq_zzggq::getHelicityIDForConfig(vector<int> hel_config,
    vector<int> permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  int user_ihel = -1; 
  if (hel_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < ncomb; i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < nexternal; j++ )
      {
        if (helicities[i][chosenPerm[j]] != hel_config[j])
        {
          found = false; 
          break; 
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_ihel = i; 
        break; 
      }
    }
    if (user_ihel == -1)
    {
      cerr <<  "Error in function 'getHelicityIDForConfig' of class" << 
      " 'PY8MEs_R12_P27_sm_gq_zzggq': Specified helicity" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R12_P27_sm_gq_zzggq_exception; 
    }
  }
  return user_ihel; 
}


//--------------------------------------------------------------------------
// Implements the map Color ID -> Color Config
vector<int> PY8MEs_R12_P27_sm_gq_zzggq::getColorConfigForID(int color_ID, int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < 0 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getColorConfigForID' of class" << 
    " 'PY8MEs_R12_P27_sm_gq_zzggq': Specified color ID '" << 
    color_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R12_P27_sm_gq_zzggq_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(color_configs[chosenProcID][color_ID].size(), 0); 
  for (unsigned int j = 0; j < (2 * nexternal); j++ )
  {
    res[chosenPerm[j/2] * 2 + j%2] = color_configs[chosenProcID][color_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Color Config -> Color ID
int PY8MEs_R12_P27_sm_gq_zzggq::getColorIDForConfig(vector<int> color_config,
    int specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = -1; 
  if (color_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < color_configs[chosenProcID].size(); i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < (nexternal * 2); j++ )
      {

        // If colorless then make sure it matches
        // The little arithmetics in the color index is just
        // the permutation applies on the particle list which is
        // twice smaller since each particle can have two color indices.
        if (color_config[j] == 0)
        {
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] != 0)
          {
            found = false; 
            break; 
          }
          // Otherwise check that the color linked position matches
        }
        else
        {
          int color_linked_pos = -1; 
          // Find the other end of the line in the user color config
          for (unsigned int k = 0; k < (nexternal * 2); k++ )
          {
            if (k == j)
              continue; 
            if (color_config[j] == color_config[k])
            {
              color_linked_pos = k; 
              break; 
            }
          }
          if (color_linked_pos == -1)
          {
            cerr <<  "Error in function 'getColorIDForConfig' of class" << 
            " 'PY8MEs_R12_P27_sm_gq_zzggq': A color line could " << 
            " not be closed." << endl; 
            throw PY8MEs_R12_P27_sm_gq_zzggq_exception; 
          }
          // Now check whether the color line matches
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] !=
              color_configs[chosenProcID][i][chosenPerm[color_linked_pos/2] * 2
              + color_linked_pos%2])
          {
            found = false; 
            break; 
          }
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_icol = i; 
        break; 
      }
    }

    if (user_icol == -1)
    {
      cerr <<  "Error in function 'getColorIDForConfig' of class" << 
      " 'PY8MEs_R12_P27_sm_gq_zzggq': Specified color" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R12_P27_sm_gq_zzggq_exception; 
    }
  }
  return user_icol; 
}

//--------------------------------------------------------------------------
// Returns all result previously computed in SigmaKin
vector < vec_double > PY8MEs_R12_P27_sm_gq_zzggq::getAllResults(int
    specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return all_results[chosenProcID]; 
}

//--------------------------------------------------------------------------
// Returns a result previously computed in SigmaKin for a specific helicity
// and color ID. -1 means avg and summed over that characteristic.
double PY8MEs_R12_P27_sm_gq_zzggq::getResult(int helicity_ID, int color_ID, int
    specify_proc_ID)
{
  if (helicity_ID < - 1 || helicity_ID >= ncomb)
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R12_P27_sm_gq_zzggq': Specified helicity ID '" << 
    helicity_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R12_P27_sm_gq_zzggq_exception; 
  }
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < - 1 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R12_P27_sm_gq_zzggq': Specified color ID '" << 
    color_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R12_P27_sm_gq_zzggq_exception; 
  }
  return all_results[chosenProcID][helicity_ID + 1][color_ID + 1]; 
}

//--------------------------------------------------------------------------
// Check for the availability of the requested process and if available,
// If available, this returns the corresponding permutation and Proc_ID to use.
// If not available, this returns a negative Proc_ID.
pair < vector<int> , int > PY8MEs_R12_P27_sm_gq_zzggq::static_getPY8ME(vector<int> initial_pdgs, vector<int> final_pdgs, set<int> schannels) 
{

  // Not available return value
  pair < vector<int> , int > NA(vector<int> (), -1); 

  // Check if s-channel requirements match
  if (nreq_s_channels > 0)
  {
    if (schannels != s_channel_proc)
      return NA; 
  }
  else
  {
    if (schannels.size() != 0)
      return NA; 
  }

  // Check number of final state particles
  if (final_pdgs.size() != (nexternal - ninitial))
    return NA; 

  // Check number of initial state particles
  if (initial_pdgs.size() != ninitial)
    return NA; 

  // List of processes available in this class
  const int nprocs = 16; 
  const int proc_IDS[nprocs] = {0, 0, 1, 1, 2, 2, 3, 3, 0, 0, 1, 1, 2, 2, 3,
      3};
  const int in_pdgs[nprocs][ninitial] = {{21, 2}, {21, 4}, {21, 1}, {21, 3},
      {21, -2}, {21, -4}, {21, -1}, {21, -3}, {2, 21}, {4, 21}, {1, 21}, {3,
      21}, {-2, 21}, {-4, 21}, {-1, 21}, {-3, 21}};
  const int out_pdgs[nprocs][nexternal - ninitial] = {{23, 23, 21, 21, 2}, {23,
      23, 21, 21, 4}, {23, 23, 21, 21, 1}, {23, 23, 21, 21, 3}, {23, 23, 21,
      21, -2}, {23, 23, 21, 21, -4}, {23, 23, 21, 21, -1}, {23, 23, 21, 21,
      -3}, {23, 23, 21, 21, 2}, {23, 23, 21, 21, 4}, {23, 23, 21, 21, 1}, {23,
      23, 21, 21, 3}, {23, 23, 21, 21, -2}, {23, 23, 21, 21, -4}, {23, 23, 21,
      21, -1}, {23, 23, 21, 21, -3}};

  bool in_pdgs_used[ninitial]; 
  bool out_pdgs_used[nexternal - ninitial]; 
  for(unsigned int i = 0; i < nprocs; i++ )
  {
    int permutations[nexternal]; 

    // Reinitialize initial state look-up variables
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      in_pdgs_used[j] = false; 
      permutations[j] = -1; 
    }
    // Look for initial state matches
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      for(unsigned int k = 0; k < ninitial; k++ )
      {
        // Make sure it has not been used already
        if (in_pdgs_used[k])
          continue; 
        if (initial_pdgs[k] == in_pdgs[i][j])
        {
          permutations[j] = k; 
          in_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[ninitial - 1] == -1)
      continue; 

    // Reinitialize final state look-up variables
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      out_pdgs_used[j] = false; 
      permutations[ninitial + j] = -1; 
    }
    // Look for final state matches
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      for(unsigned int k = 0; k < (nexternal - ninitial); k++ )
      {
        // Make sure it has not been used already
        if (out_pdgs_used[k])
          continue; 
        if (final_pdgs[k] == out_pdgs[i][j])
        {
          permutations[ninitial + j] = ninitial + k; 
          out_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[ninitial + j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[nexternal - 1] == -1)
      continue; 

    // Return process found
    return pair < vector<int> , int > (vector<int> (permutations, permutations
        + nexternal), proc_IDS[i]);
  }

  // No process found
  return NA; 
}

//--------------------------------------------------------------------------
// Set momenta
void PY8MEs_R12_P27_sm_gq_zzggq::setMomenta(vector < vec_double >
    momenta_picked)
{
  if (momenta_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setMomenta' of class" << 
    " 'PY8MEs_R12_P27_sm_gq_zzggq': Incorrect number of" << 
    " momenta specified." << endl; 
    throw PY8MEs_R12_P27_sm_gq_zzggq_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    if (momenta_picked[i].size() != 4)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R12_P27_sm_gq_zzggq': Incorrect number of" << 
      " momenta components specified." << endl; 
      throw PY8MEs_R12_P27_sm_gq_zzggq_exception; 
    }
    if (momenta_picked[i][0] < 0.0)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R12_P27_sm_gq_zzggq': A momentum was specified" << 
      " with negative energy. Check conventions." << endl; 
      throw PY8MEs_R12_P27_sm_gq_zzggq_exception; 
    }
    for (unsigned int j = 0; j < 4; j++ )
    {
      p[i][j] = momenta_picked[i][j]; 
    }
  }
}

//--------------------------------------------------------------------------
// Set color configuration to use. An empty vector means sum over all.
void PY8MEs_R12_P27_sm_gq_zzggq::setColors(vector<int> colors_picked)
{
  if (colors_picked.size() == 0)
  {
    user_colors = vector<int> (); 
    return; 
  }
  if (colors_picked.size() != (2 * nexternal))
  {
    cerr <<  "Error in function 'setColors' of class" << 
    " 'PY8MEs_R12_P27_sm_gq_zzggq': Incorrect number" << 
    " of colors specified." << endl; 
    throw PY8MEs_R12_P27_sm_gq_zzggq_exception; 
  }
  user_colors = vector<int> ((2 * nexternal), 0); 
  for(unsigned int i = 0; i < (2 * nexternal); i++ )
  {
    user_colors[i] = colors_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the helicity configuration to use. Am empty vector means sum over all.
void PY8MEs_R12_P27_sm_gq_zzggq::setHelicities(vector<int> helicities_picked) 
{
  if (helicities_picked.size() != nexternal)
  {
    if (helicities_picked.size() == 0)
    {
      user_helicities = vector<int> (); 
      return; 
    }
    cerr <<  "Error in function 'setHelicities' of class" << 
    " 'PY8MEs_R12_P27_sm_gq_zzggq': Incorrect number" << 
    " of helicities specified." << endl; 
    throw PY8MEs_R12_P27_sm_gq_zzggq_exception; 
  }
  user_helicities = vector<int> (nexternal, 0); 
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    user_helicities[i] = helicities_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the permutation to use (will apply to momenta, colors and helicities)
void PY8MEs_R12_P27_sm_gq_zzggq::setPermutation(vector<int> perm_picked) 
{
  if (perm_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setPermutations' of class" << 
    " 'PY8MEs_R12_P27_sm_gq_zzggq': Incorrect number" << 
    " of permutations specified." << endl; 
    throw PY8MEs_R12_P27_sm_gq_zzggq_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    perm[i] = perm_picked[i]; 
  }
}

// Set the proc_ID to use
void PY8MEs_R12_P27_sm_gq_zzggq::setProcID(int procID_picked) 
{
  proc_ID = procID_picked; 
}

//--------------------------------------------------------------------------
// Initialize process.

void PY8MEs_R12_P27_sm_gq_zzggq::initProc() 
{

  // Initialize flags
  include_symmetry_factors = true; 
  include_helicity_averaging_factors = true; 
  include_color_averaging_factors = true; 

  // Initialize vectors.
  perm = vector<int> (nexternal, 0); 
  user_colors = vector<int> (2 * nexternal, 0); 
  user_helicities = vector<int> (nexternal, 0); 
  p = vector < double * > (); 
  for (unsigned int i = 0; i < nexternal; i++ )
  {
    p.push_back(new double[4]); 
  }
  initColorConfigs(); 
  // Synchronize local variables dependent on the model with the active model.
  mME = vector<double> (nexternal, 0.); 
  syncProcModelParams(); 
  jamp2 = vector < vec_double > (4); 
  jamp2[0] = vector<double> (6, 0.); 
  jamp2[1] = vector<double> (6, 0.); 
  jamp2[2] = vector<double> (6, 0.); 
  jamp2[3] = vector<double> (6, 0.); 
  all_results = vector < vec_vec_double > (4); 
  // The first entry is always the color or helicity avg/summed matrix element.
  all_results[0] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[1] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[2] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[3] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
}

// Synchronize local variables of the process that depend on the model
// parameters
void PY8MEs_R12_P27_sm_gq_zzggq::syncProcModelParams() 
{

  // Instantiate the model class and set parameters that stay fixed during run
  mME[0] = pars->ZERO; 
  mME[1] = pars->ZERO; 
  mME[2] = pars->mdl_MZ; 
  mME[3] = pars->mdl_MZ; 
  mME[4] = pars->ZERO; 
  mME[5] = pars->ZERO; 
  mME[6] = pars->ZERO; 
}

//--------------------------------------------------------------------------
// Setter allowing to force particular values for the external masses
void PY8MEs_R12_P27_sm_gq_zzggq::setMasses(vec_double external_masses) 
{

  if (external_masses.size() != mME.size())
  {
    cerr <<  "Error in function 'setMasses' of class" << 
    " 'PY8MEs_R12_P27_sm_gq_zzggq': Incorrect number of" << 
    " masses specified." << endl; 
    throw PY8MEs_R12_P27_sm_gq_zzggq_exception; 
  }
  for (unsigned int j = 0; j < mME.size(); j++ )
  {
    mME[j] = external_masses[perm[j]]; 
  }
}

//--------------------------------------------------------------------------
// Getter accessing external masses with the correct ordering
vector<double> PY8MEs_R12_P27_sm_gq_zzggq::getMasses() 
{

  vec_double external_masses; 
  vector<int> invertedPerm; 
  invertedPerm = invert_mapping(perm); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    external_masses.push_back(mME[invertedPerm[i]]); 
  }
  return external_masses; 

}


// Set all values of the external masses to float(-mode) where mode can be
// 0 : Mass taken from the model
// 1 : Mass taken from p_i^2 if not massless to begin with
// 2 : Mass always taken from p_i^2.
void PY8MEs_R12_P27_sm_gq_zzggq::setExternalMassesMode(int mode) 
{
  if (mode != 0 && mode != 1 && mode != 2)
  {
    cerr <<  "Error in function 'setExternalMassesMode' of class" << 
    " 'PY8MEs_R12_P27_sm_gq_zzggq': Incorrect mode selected :" << mode << 
    ". It must be either 0, 1 or 2" << endl; 
    throw PY8MEs_R12_P27_sm_gq_zzggq_exception; 
  }
  if (mode == 0)
  {
    syncProcModelParams(); 
  }
  else if (mode == 1)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      if (mME[j] != pars->ZERO)
      {
        mME[j] = -1.0; 
      }
    }
  }
  else if (mode == 2)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      mME[j] = -1.0; 
    }
  }
}

//--------------------------------------------------------------------------
// Evaluate the squared matrix element.

double PY8MEs_R12_P27_sm_gq_zzggq::sigmaKin() 
{
  // Set the parameters which change event by event
  pars->setDependentParameters(); 
  pars->setDependentCouplings(); 
  // Reset color flows
  for(int i = 0; i < 6; i++ )
    jamp2[0][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[1][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[2][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[3][i] = 0.; 

  // Save previous values of mME
  vector<double> saved_mME(mME.size(), 0.0); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (mME[i] < 0.0)
    {
      saved_mME[i] = mME[i]; 
      mME[i] = sqrt(abs(pow(p[perm[i]][0], 2) - 
      (pow(p[perm[i]][1], 2) + pow(p[perm[i]][2], 2) + pow(p[perm[i]][3],
          2))));
    }
  }

  // Local variables and constants
  const int max_tries = 10; 
  vector < vec_bool > goodhel(nprocesses, vec_bool(ncomb, false)); 
  vec_int ntry(nprocesses, 0); 
  double t = 0.; 
  double result = 0.; 

  if (ntry[proc_ID] <= max_tries)
    ntry[proc_ID] = ntry[proc_ID] + 1; 

  // Find which helicity configuration is asked for
  // -1 indicates one wants to sum over helicities
  int user_ihel = getHelicityIDForConfig(user_helicities); 

  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = getColorIDForConfig(user_colors); 

  // Reset the list of results that will be recomputed here
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      all_results[proc_ID][ihel + 1][icolor + 1] = 0.; 
    }
  }

  // Calculate the matrix element for all helicities
  // unless already detected as vanishing
  for(int ihel = 0; ihel < ncomb; ihel++ )
  {
    // Skip helicity if already detected as vanishing
    if ((ntry[proc_ID] >= max_tries) && !goodhel[proc_ID][ihel])
      continue; 

    // Also skip helicity if user asks for a specific one
    if ((ntry[proc_ID] >= max_tries) && user_ihel != -1 && user_ihel != ihel)
      continue; 

    calculate_wavefunctions(helicities[ihel]); 

    // Reset locally computed color flows
    for(int i = 0; i < 6; i++ )
      jamp2[0][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[1][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[2][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[3][i] = 0.; 

    if (proc_ID == 0)
      t = matrix_12_gu_zzggu(); 
    if (proc_ID == 1)
      t = matrix_12_gd_zzggd(); 
    if (proc_ID == 2)
      t = matrix_12_gux_zzggux(); 
    if (proc_ID == 3)
      t = matrix_12_gdx_zzggdx(); 

    // Store which helicities give non-zero result
    if ((ntry[proc_ID] < max_tries) && t != 0. && !goodhel[proc_ID][ihel])
      goodhel[proc_ID][ihel] = true; 

    // Aggregate results
    if (user_ihel == -1 || user_ihel == ihel)
    {
      if (user_icol == -1)
      {
        result = result + t; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][0] += t; 
          for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
          {
            all_results[proc_ID][0][i + 1] += jamp2[proc_ID][i]; 
          }
        }
        all_results[proc_ID][ihel + 1][0] += t; 
        for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
        {
          all_results[proc_ID][ihel + 1][i + 1] += jamp2[proc_ID][i]; 
        }
      }
      else
      {
        result = result + jamp2[proc_ID][user_icol]; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][user_icol + 1] += jamp2[proc_ID][user_icol]; 
        }
        all_results[proc_ID][ihel + 1][user_icol + 1] +=
            jamp2[proc_ID][user_icol];
      }
    }

  }

  // Normalize results with the identical particle factor
  if (include_symmetry_factors)
  {
    result = result/denom_iden[proc_ID]; 
  }
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      if (include_symmetry_factors)
      {
        all_results[proc_ID][ihel + 1][icolor + 1] /= denom_iden[proc_ID]; 
      }
    }
  }


  // Normalize when when summing+averaging over helicity configurations
  if (user_ihel == -1 && include_helicity_averaging_factors)
  {
    result /= denom_hels[proc_ID]; 
    if (user_icol == -1)
    {
      all_results[proc_ID][0][0] /= denom_hels[proc_ID]; 
      for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
      {
        all_results[proc_ID][0][i + 1] /= denom_hels[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][0][user_icol + 1] /= denom_hels[proc_ID]; 
    }
  }

  // Normalize when summing+averaging over color configurations
  if (user_icol == -1 && include_color_averaging_factors)
  {
    result /= denom_colors[proc_ID]; 
    if (user_ihel == -1)
    {
      all_results[proc_ID][0][0] /= denom_colors[proc_ID]; 
      for (unsigned int i = 0; i < ncomb; i++ )
      {
        all_results[proc_ID][i + 1][0] /= denom_colors[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][user_ihel + 1][0] /= denom_colors[proc_ID]; 
    }
  }

  // Reinstate previous values of mME
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (saved_mME[i] < 0.0)
    {
      mME[i] = saved_mME[i]; 
    }
  }

  // Finally return it
  return result; 
}

//==========================================================================
// Private class member functions

//--------------------------------------------------------------------------
// Evaluate |M|^2 for each subprocess

void PY8MEs_R12_P27_sm_gq_zzggq::calculate_wavefunctions(const int hel[])
{
  // Calculate wavefunctions for all processes
  // Calculate all wavefunctions
  vxxxxx(p[perm[0]], mME[0], hel[0], -1, w[0]); 
  ixxxxx(p[perm[1]], mME[1], hel[1], +1, w[1]); 
  vxxxxx(p[perm[2]], mME[2], hel[2], +1, w[2]); 
  vxxxxx(p[perm[3]], mME[3], hel[3], +1, w[3]); 
  vxxxxx(p[perm[4]], mME[4], hel[4], +1, w[4]); 
  vxxxxx(p[perm[5]], mME[5], hel[5], +1, w[5]); 
  oxxxxx(p[perm[6]], mME[6], hel[6], +1, w[6]); 
  VVV1P0_1(w[0], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[7]); 
  FFV2_5_1(w[6], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO, w[8]); 
  VVV1P0_1(w[7], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[9]); 
  FFV2_5_1(w[8], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[10]);
  FFV1_2(w[1], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[11]); 
  FFV1_1(w[8], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[12]); 
  FFV2_5_2(w[1], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[13]);
  FFV1_1(w[8], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[14]); 
  FFV1_2(w[13], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[15]); 
  FFV1_2(w[1], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[16]); 
  FFV1_2(w[16], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[17]); 
  FFV2_5_2(w[1], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[18]);
  FFV2_5_2(w[18], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[19]);
  FFV1_1(w[6], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[20]); 
  FFV1_2(w[18], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[21]); 
  FFV2_5_1(w[6], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[22]);
  FFV1_2(w[18], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[23]); 
  FFV1_1(w[22], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[24]); 
  FFV1_1(w[6], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[25]); 
  FFV1_1(w[25], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[26]); 
  FFV2_5_1(w[22], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[27]);
  FFV1_1(w[22], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[28]); 
  FFV2_5_2(w[13], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[29]);
  FFV1_2(w[13], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[30]); 
  FFV2_5_1(w[25], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[31]);
  FFV2_5_1(w[25], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[32]);
  FFV2_5_2(w[16], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[33]);
  FFV2_5_2(w[16], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[34]);
  VVV1P0_1(w[0], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[35]); 
  FFV1_1(w[6], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[36]); 
  FFV1_2(w[1], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[37]); 
  FFV2_5_1(w[36], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[38]);
  FFV2_5_1(w[36], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[39]);
  FFV1_1(w[36], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[40]); 
  FFV1_2(w[18], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[41]); 
  FFV1_2(w[13], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[42]); 
  FFV1_2(w[1], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[43]); 
  FFV1_1(w[6], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[44]); 
  FFV2_5_2(w[43], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[45]);
  FFV2_5_2(w[43], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[46]);
  FFV1_2(w[43], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[47]); 
  FFV1_1(w[8], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[48]); 
  FFV1_1(w[22], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[49]); 
  VVV1P0_1(w[35], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[50]); 
  FFV1_1(w[8], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[51]); 
  FFV1_2(w[18], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[52]); 
  FFV1_1(w[22], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[53]); 
  FFV1_2(w[13], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[54]); 
  FFV1_1(w[6], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[55]); 
  VVV1P0_1(w[4], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[56]); 
  FFV2_5_1(w[55], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[57]);
  FFV1_2(w[1], w[56], pars->GC_11, pars->ZERO, pars->ZERO, w[58]); 
  FFV2_5_1(w[55], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[59]);
  FFV1_1(w[55], w[56], pars->GC_11, pars->ZERO, pars->ZERO, w[60]); 
  FFV1_2(w[43], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[61]); 
  FFV1_1(w[55], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[62]); 
  FFV1_1(w[55], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[63]); 
  FFV1_2(w[16], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[64]); 
  FFV1_2(w[1], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[65]); 
  FFV2_5_2(w[65], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[66]);
  FFV1_1(w[6], w[56], pars->GC_11, pars->ZERO, pars->ZERO, w[67]); 
  FFV2_5_2(w[65], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[68]);
  FFV1_2(w[65], w[56], pars->GC_11, pars->ZERO, pars->ZERO, w[69]); 
  FFV1_1(w[36], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[70]); 
  FFV1_2(w[65], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[71]); 
  FFV1_2(w[65], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[72]); 
  FFV1_1(w[25], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[73]); 
  VVV1P0_1(w[0], w[56], pars->GC_10, pars->ZERO, pars->ZERO, w[74]); 
  FFV1_1(w[8], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[75]); 
  FFV1_2(w[13], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[76]); 
  FFV1_2(w[18], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[77]); 
  FFV1_1(w[22], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[78]); 
  FFV1_1(w[36], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[79]); 
  FFV1_2(w[16], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[80]); 
  FFV1_2(w[43], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[81]); 
  FFV1_1(w[25], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[82]); 
  VVVV1P0_1(w[0], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[83]); 
  VVVV3P0_1(w[0], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[84]); 
  VVVV4P0_1(w[0], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[85]); 
  FFV1_2(w[1], w[83], pars->GC_11, pars->ZERO, pars->ZERO, w[86]); 
  FFV1_2(w[1], w[84], pars->GC_11, pars->ZERO, pars->ZERO, w[87]); 
  FFV1_2(w[1], w[85], pars->GC_11, pars->ZERO, pars->ZERO, w[88]); 
  FFV1_1(w[6], w[83], pars->GC_11, pars->ZERO, pars->ZERO, w[89]); 
  FFV1_1(w[6], w[84], pars->GC_11, pars->ZERO, pars->ZERO, w[90]); 
  FFV1_1(w[6], w[85], pars->GC_11, pars->ZERO, pars->ZERO, w[91]); 
  FFV2_3_1(w[6], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[92]);
  FFV2_3_1(w[92], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[93]);
  FFV1_1(w[92], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[94]); 
  FFV2_3_2(w[1], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[95]);
  FFV1_1(w[92], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[96]); 
  FFV1_2(w[95], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[97]); 
  FFV2_3_2(w[1], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[98]);
  FFV2_3_2(w[98], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[99]);
  FFV1_2(w[98], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[100]); 
  FFV2_3_1(w[6], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[101]);
  FFV1_2(w[98], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[102]); 
  FFV1_1(w[101], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[103]); 
  FFV2_3_1(w[101], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[104]);
  FFV1_1(w[101], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[105]); 
  FFV2_3_2(w[95], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[106]);
  FFV1_2(w[95], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[107]); 
  FFV2_3_1(w[25], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[108]);
  FFV2_3_1(w[25], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[109]);
  FFV2_3_2(w[16], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[110]);
  FFV2_3_2(w[16], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[111]);
  FFV2_3_1(w[36], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[112]);
  FFV2_3_1(w[36], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[113]);
  FFV1_2(w[98], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[114]); 
  FFV1_2(w[95], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[115]); 
  FFV2_3_2(w[43], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[116]);
  FFV2_3_2(w[43], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[117]);
  FFV1_1(w[92], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[118]); 
  FFV1_1(w[101], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[119]); 
  FFV1_1(w[92], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[120]); 
  FFV1_2(w[98], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[121]); 
  FFV1_1(w[101], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[122]); 
  FFV1_2(w[95], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[123]); 
  FFV2_3_1(w[55], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[124]);
  FFV2_3_1(w[55], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[125]);
  FFV2_3_2(w[65], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[126]);
  FFV2_3_2(w[65], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[127]);
  FFV1_1(w[92], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[128]); 
  FFV1_2(w[95], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[129]); 
  FFV1_2(w[98], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[130]); 
  FFV1_1(w[101], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[131]); 
  oxxxxx(p[perm[1]], mME[1], hel[1], -1, w[132]); 
  ixxxxx(p[perm[6]], mME[6], hel[6], -1, w[133]); 
  FFV2_5_1(w[132], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[134]);
  FFV2_5_1(w[134], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[135]);
  FFV1_2(w[133], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[136]); 
  FFV1_1(w[134], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[137]); 
  FFV2_5_2(w[133], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[138]);
  FFV1_1(w[134], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[139]); 
  FFV1_2(w[138], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[140]); 
  FFV1_2(w[133], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[141]); 
  FFV1_2(w[141], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[142]); 
  FFV2_5_2(w[133], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[143]);
  FFV2_5_2(w[143], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[144]);
  FFV1_1(w[132], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[145]); 
  FFV1_2(w[143], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[146]); 
  FFV2_5_1(w[132], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[147]);
  FFV1_2(w[143], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[148]); 
  FFV1_1(w[147], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[149]); 
  FFV1_1(w[132], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[150]); 
  FFV1_1(w[150], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[151]); 
  FFV2_5_1(w[147], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[152]);
  FFV1_1(w[147], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[153]); 
  FFV2_5_2(w[138], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[154]);
  FFV1_2(w[138], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[155]); 
  FFV2_5_1(w[150], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[156]);
  FFV2_5_1(w[150], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[157]);
  FFV2_5_2(w[141], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[158]);
  FFV2_5_2(w[141], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[159]);
  FFV1_1(w[132], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[160]); 
  FFV1_2(w[133], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[161]); 
  FFV2_5_1(w[160], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[162]);
  FFV2_5_1(w[160], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[163]);
  FFV1_1(w[160], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[164]); 
  FFV1_2(w[143], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[165]); 
  FFV1_2(w[138], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[166]); 
  FFV1_2(w[133], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[167]); 
  FFV1_1(w[132], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[168]); 
  FFV2_5_2(w[167], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[169]);
  FFV2_5_2(w[167], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[170]);
  FFV1_2(w[167], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[171]); 
  FFV1_1(w[134], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[172]); 
  FFV1_1(w[147], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[173]); 
  FFV1_1(w[134], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[174]); 
  FFV1_2(w[143], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[175]); 
  FFV1_1(w[147], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[176]); 
  FFV1_2(w[138], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[177]); 
  FFV1_1(w[132], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[178]); 
  FFV2_5_1(w[178], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[179]);
  FFV1_2(w[133], w[56], pars->GC_11, pars->ZERO, pars->ZERO, w[180]); 
  FFV2_5_1(w[178], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[181]);
  FFV1_1(w[178], w[56], pars->GC_11, pars->ZERO, pars->ZERO, w[182]); 
  FFV1_2(w[167], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[183]); 
  FFV1_1(w[178], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[184]); 
  FFV1_1(w[178], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[185]); 
  FFV1_2(w[141], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[186]); 
  FFV1_2(w[133], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[187]); 
  FFV2_5_2(w[187], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[188]);
  FFV1_1(w[132], w[56], pars->GC_11, pars->ZERO, pars->ZERO, w[189]); 
  FFV2_5_2(w[187], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[190]);
  FFV1_2(w[187], w[56], pars->GC_11, pars->ZERO, pars->ZERO, w[191]); 
  FFV1_1(w[160], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[192]); 
  FFV1_2(w[187], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[193]); 
  FFV1_2(w[187], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[194]); 
  FFV1_1(w[150], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[195]); 
  FFV1_1(w[134], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[196]); 
  FFV1_2(w[138], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[197]); 
  FFV1_2(w[143], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[198]); 
  FFV1_1(w[147], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[199]); 
  FFV1_1(w[160], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[200]); 
  FFV1_2(w[141], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[201]); 
  FFV1_2(w[167], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[202]); 
  FFV1_1(w[150], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[203]); 
  FFV1_2(w[133], w[83], pars->GC_11, pars->ZERO, pars->ZERO, w[204]); 
  FFV1_2(w[133], w[84], pars->GC_11, pars->ZERO, pars->ZERO, w[205]); 
  FFV1_2(w[133], w[85], pars->GC_11, pars->ZERO, pars->ZERO, w[206]); 
  FFV1_1(w[132], w[83], pars->GC_11, pars->ZERO, pars->ZERO, w[207]); 
  FFV1_1(w[132], w[84], pars->GC_11, pars->ZERO, pars->ZERO, w[208]); 
  FFV1_1(w[132], w[85], pars->GC_11, pars->ZERO, pars->ZERO, w[209]); 
  FFV2_3_1(w[132], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[210]);
  FFV2_3_1(w[210], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[211]);
  FFV1_1(w[210], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[212]); 
  FFV2_3_2(w[133], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[213]);
  FFV1_1(w[210], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[214]); 
  FFV1_2(w[213], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[215]); 
  FFV2_3_2(w[133], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[216]);
  FFV2_3_2(w[216], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[217]);
  FFV1_2(w[216], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[218]); 
  FFV2_3_1(w[132], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[219]);
  FFV1_2(w[216], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[220]); 
  FFV1_1(w[219], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[221]); 
  FFV2_3_1(w[219], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[222]);
  FFV1_1(w[219], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[223]); 
  FFV2_3_2(w[213], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[224]);
  FFV1_2(w[213], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[225]); 
  FFV2_3_1(w[150], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[226]);
  FFV2_3_1(w[150], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[227]);
  FFV2_3_2(w[141], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[228]);
  FFV2_3_2(w[141], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[229]);
  FFV2_3_1(w[160], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[230]);
  FFV2_3_1(w[160], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[231]);
  FFV1_2(w[216], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[232]); 
  FFV1_2(w[213], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[233]); 
  FFV2_3_2(w[167], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[234]);
  FFV2_3_2(w[167], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[235]);
  FFV1_1(w[210], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[236]); 
  FFV1_1(w[219], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[237]); 
  FFV1_1(w[210], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[238]); 
  FFV1_2(w[216], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[239]); 
  FFV1_1(w[219], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[240]); 
  FFV1_2(w[213], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[241]); 
  FFV2_3_1(w[178], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[242]);
  FFV2_3_1(w[178], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[243]);
  FFV2_3_2(w[187], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[244]);
  FFV2_3_2(w[187], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[245]);
  FFV1_1(w[210], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[246]); 
  FFV1_2(w[213], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[247]); 
  FFV1_2(w[216], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[248]); 
  FFV1_1(w[219], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[249]); 

  // Calculate all amplitudes
  // Amplitude(s) for diagram number 0
  FFV1_0(w[1], w[10], w[9], pars->GC_11, amp[0]); 
  FFV1_0(w[11], w[10], w[5], pars->GC_11, amp[1]); 
  FFV2_5_0(w[11], w[12], w[3], pars->GC_51, pars->GC_58, amp[2]); 
  FFV1_0(w[13], w[14], w[5], pars->GC_11, amp[3]); 
  FFV1_0(w[15], w[8], w[5], pars->GC_11, amp[4]); 
  FFV1_0(w[13], w[8], w[9], pars->GC_11, amp[5]); 
  FFV2_5_0(w[16], w[14], w[3], pars->GC_51, pars->GC_58, amp[6]); 
  FFV2_5_0(w[17], w[8], w[3], pars->GC_51, pars->GC_58, amp[7]); 
  FFV1_0(w[19], w[6], w[9], pars->GC_11, amp[8]); 
  FFV1_0(w[19], w[20], w[5], pars->GC_11, amp[9]); 
  FFV2_5_0(w[21], w[20], w[3], pars->GC_51, pars->GC_58, amp[10]); 
  FFV1_0(w[23], w[22], w[5], pars->GC_11, amp[11]); 
  FFV1_0(w[18], w[24], w[5], pars->GC_11, amp[12]); 
  FFV1_0(w[18], w[22], w[9], pars->GC_11, amp[13]); 
  FFV2_5_0(w[23], w[25], w[3], pars->GC_51, pars->GC_58, amp[14]); 
  FFV2_5_0(w[18], w[26], w[3], pars->GC_51, pars->GC_58, amp[15]); 
  FFV1_0(w[1], w[27], w[9], pars->GC_11, amp[16]); 
  FFV1_0(w[11], w[27], w[5], pars->GC_11, amp[17]); 
  FFV2_5_0(w[11], w[28], w[2], pars->GC_51, pars->GC_58, amp[18]); 
  FFV2_5_0(w[16], w[24], w[2], pars->GC_51, pars->GC_58, amp[19]); 
  FFV2_5_0(w[17], w[22], w[2], pars->GC_51, pars->GC_58, amp[20]); 
  FFV1_0(w[29], w[6], w[9], pars->GC_11, amp[21]); 
  FFV1_0(w[29], w[20], w[5], pars->GC_11, amp[22]); 
  FFV2_5_0(w[30], w[20], w[2], pars->GC_51, pars->GC_58, amp[23]); 
  FFV2_5_0(w[15], w[25], w[2], pars->GC_51, pars->GC_58, amp[24]); 
  FFV2_5_0(w[13], w[26], w[2], pars->GC_51, pars->GC_58, amp[25]); 
  FFV2_5_0(w[11], w[31], w[3], pars->GC_51, pars->GC_58, amp[26]); 
  FFV2_5_0(w[11], w[32], w[2], pars->GC_51, pars->GC_58, amp[27]); 
  FFV2_5_0(w[33], w[20], w[3], pars->GC_51, pars->GC_58, amp[28]); 
  FFV2_5_0(w[34], w[20], w[2], pars->GC_51, pars->GC_58, amp[29]); 
  FFV2_5_0(w[37], w[38], w[3], pars->GC_51, pars->GC_58, amp[30]); 
  FFV2_5_0(w[37], w[39], w[2], pars->GC_51, pars->GC_58, amp[31]); 
  FFV2_5_0(w[18], w[40], w[3], pars->GC_51, pars->GC_58, amp[32]); 
  FFV2_5_0(w[41], w[36], w[3], pars->GC_51, pars->GC_58, amp[33]); 
  FFV2_5_0(w[13], w[40], w[2], pars->GC_51, pars->GC_58, amp[34]); 
  FFV2_5_0(w[42], w[36], w[2], pars->GC_51, pars->GC_58, amp[35]); 
  FFV2_5_0(w[45], w[44], w[3], pars->GC_51, pars->GC_58, amp[36]); 
  FFV2_5_0(w[46], w[44], w[2], pars->GC_51, pars->GC_58, amp[37]); 
  FFV2_5_0(w[47], w[8], w[3], pars->GC_51, pars->GC_58, amp[38]); 
  FFV2_5_0(w[43], w[48], w[3], pars->GC_51, pars->GC_58, amp[39]); 
  FFV2_5_0(w[47], w[22], w[2], pars->GC_51, pars->GC_58, amp[40]); 
  FFV2_5_0(w[43], w[49], w[2], pars->GC_51, pars->GC_58, amp[41]); 
  FFV1_0(w[1], w[10], w[50], pars->GC_11, amp[42]); 
  FFV2_5_0(w[37], w[51], w[3], pars->GC_51, pars->GC_58, amp[43]); 
  FFV1_0(w[37], w[10], w[4], pars->GC_11, amp[44]); 
  FFV1_0(w[13], w[8], w[50], pars->GC_11, amp[45]); 
  FFV1_0(w[13], w[48], w[4], pars->GC_11, amp[46]); 
  FFV1_0(w[42], w[8], w[4], pars->GC_11, amp[47]); 
  FFV1_0(w[19], w[6], w[50], pars->GC_11, amp[48]); 
  FFV2_5_0(w[52], w[44], w[3], pars->GC_51, pars->GC_58, amp[49]); 
  FFV1_0(w[19], w[44], w[4], pars->GC_11, amp[50]); 
  FFV1_0(w[18], w[22], w[50], pars->GC_11, amp[51]); 
  FFV1_0(w[41], w[22], w[4], pars->GC_11, amp[52]); 
  FFV1_0(w[18], w[49], w[4], pars->GC_11, amp[53]); 
  FFV1_0(w[1], w[27], w[50], pars->GC_11, amp[54]); 
  FFV2_5_0(w[37], w[53], w[2], pars->GC_51, pars->GC_58, amp[55]); 
  FFV1_0(w[37], w[27], w[4], pars->GC_11, amp[56]); 
  FFV1_0(w[29], w[6], w[50], pars->GC_11, amp[57]); 
  FFV2_5_0(w[54], w[44], w[2], pars->GC_51, pars->GC_58, amp[58]); 
  FFV1_0(w[29], w[44], w[4], pars->GC_11, amp[59]); 
  FFV2_5_0(w[58], w[57], w[3], pars->GC_51, pars->GC_58, amp[60]); 
  FFV2_5_0(w[58], w[59], w[2], pars->GC_51, pars->GC_58, amp[61]); 
  FFV2_5_0(w[18], w[60], w[3], pars->GC_51, pars->GC_58, amp[62]); 
  FFV1_0(w[18], w[59], w[56], pars->GC_11, amp[63]); 
  FFV2_5_0(w[13], w[60], w[2], pars->GC_51, pars->GC_58, amp[64]); 
  FFV1_0(w[13], w[57], w[56], pars->GC_11, amp[65]); 
  FFV1_0(w[46], w[57], w[5], pars->GC_11, amp[66]); 
  FFV2_5_0(w[61], w[57], w[3], pars->GC_51, pars->GC_58, amp[67]); 
  FFV1_0(w[45], w[59], w[5], pars->GC_11, amp[68]); 
  FFV2_5_0(w[61], w[59], w[2], pars->GC_51, pars->GC_58, amp[69]); 
  FFV2_5_0(w[45], w[62], w[3], pars->GC_51, pars->GC_58, amp[70]); 
  FFV2_5_0(w[46], w[62], w[2], pars->GC_51, pars->GC_58, amp[71]); 
  FFV1_0(w[19], w[63], w[5], pars->GC_11, amp[72]); 
  FFV2_5_0(w[21], w[63], w[3], pars->GC_51, pars->GC_58, amp[73]); 
  FFV1_0(w[52], w[59], w[5], pars->GC_11, amp[74]); 
  FFV1_0(w[21], w[59], w[4], pars->GC_11, amp[75]); 
  FFV2_5_0(w[52], w[62], w[3], pars->GC_51, pars->GC_58, amp[76]); 
  FFV1_0(w[19], w[62], w[4], pars->GC_11, amp[77]); 
  FFV1_0(w[29], w[63], w[5], pars->GC_11, amp[78]); 
  FFV2_5_0(w[30], w[63], w[2], pars->GC_51, pars->GC_58, amp[79]); 
  FFV1_0(w[54], w[57], w[5], pars->GC_11, amp[80]); 
  FFV1_0(w[30], w[57], w[4], pars->GC_11, amp[81]); 
  FFV2_5_0(w[54], w[62], w[2], pars->GC_51, pars->GC_58, amp[82]); 
  FFV1_0(w[29], w[62], w[4], pars->GC_11, amp[83]); 
  FFV2_5_0(w[33], w[63], w[3], pars->GC_51, pars->GC_58, amp[84]); 
  FFV2_5_0(w[34], w[63], w[2], pars->GC_51, pars->GC_58, amp[85]); 
  FFV2_5_0(w[64], w[57], w[3], pars->GC_51, pars->GC_58, amp[86]); 
  FFV1_0(w[34], w[57], w[4], pars->GC_11, amp[87]); 
  FFV2_5_0(w[64], w[59], w[2], pars->GC_51, pars->GC_58, amp[88]); 
  FFV1_0(w[33], w[59], w[4], pars->GC_11, amp[89]); 
  FFV2_5_0(w[66], w[67], w[3], pars->GC_51, pars->GC_58, amp[90]); 
  FFV2_5_0(w[68], w[67], w[2], pars->GC_51, pars->GC_58, amp[91]); 
  FFV2_5_0(w[69], w[8], w[3], pars->GC_51, pars->GC_58, amp[92]); 
  FFV1_0(w[68], w[8], w[56], pars->GC_11, amp[93]); 
  FFV2_5_0(w[69], w[22], w[2], pars->GC_51, pars->GC_58, amp[94]); 
  FFV1_0(w[66], w[22], w[56], pars->GC_11, amp[95]); 
  FFV1_0(w[66], w[39], w[5], pars->GC_11, amp[96]); 
  FFV2_5_0(w[66], w[70], w[3], pars->GC_51, pars->GC_58, amp[97]); 
  FFV1_0(w[68], w[38], w[5], pars->GC_11, amp[98]); 
  FFV2_5_0(w[68], w[70], w[2], pars->GC_51, pars->GC_58, amp[99]); 
  FFV2_5_0(w[71], w[38], w[3], pars->GC_51, pars->GC_58, amp[100]); 
  FFV2_5_0(w[71], w[39], w[2], pars->GC_51, pars->GC_58, amp[101]); 
  FFV1_0(w[72], w[10], w[5], pars->GC_11, amp[102]); 
  FFV2_5_0(w[72], w[12], w[3], pars->GC_51, pars->GC_58, amp[103]); 
  FFV1_0(w[68], w[51], w[5], pars->GC_11, amp[104]); 
  FFV1_0(w[68], w[12], w[4], pars->GC_11, amp[105]); 
  FFV2_5_0(w[71], w[51], w[3], pars->GC_51, pars->GC_58, amp[106]); 
  FFV1_0(w[71], w[10], w[4], pars->GC_11, amp[107]); 
  FFV1_0(w[72], w[27], w[5], pars->GC_11, amp[108]); 
  FFV2_5_0(w[72], w[28], w[2], pars->GC_51, pars->GC_58, amp[109]); 
  FFV1_0(w[66], w[53], w[5], pars->GC_11, amp[110]); 
  FFV1_0(w[66], w[28], w[4], pars->GC_11, amp[111]); 
  FFV2_5_0(w[71], w[53], w[2], pars->GC_51, pars->GC_58, amp[112]); 
  FFV1_0(w[71], w[27], w[4], pars->GC_11, amp[113]); 
  FFV2_5_0(w[72], w[31], w[3], pars->GC_51, pars->GC_58, amp[114]); 
  FFV2_5_0(w[72], w[32], w[2], pars->GC_51, pars->GC_58, amp[115]); 
  FFV2_5_0(w[66], w[73], w[3], pars->GC_51, pars->GC_58, amp[116]); 
  FFV1_0(w[66], w[32], w[4], pars->GC_11, amp[117]); 
  FFV2_5_0(w[68], w[73], w[2], pars->GC_51, pars->GC_58, amp[118]); 
  FFV1_0(w[68], w[31], w[4], pars->GC_11, amp[119]); 
  FFV1_0(w[1], w[10], w[74], pars->GC_11, amp[120]); 
  FFV2_5_0(w[58], w[75], w[3], pars->GC_51, pars->GC_58, amp[121]); 
  FFV1_0(w[58], w[10], w[0], pars->GC_11, amp[122]); 
  FFV1_0(w[13], w[8], w[74], pars->GC_11, amp[123]); 
  FFV1_0(w[13], w[75], w[56], pars->GC_11, amp[124]); 
  FFV1_0(w[76], w[8], w[56], pars->GC_11, amp[125]); 
  FFV1_0(w[19], w[6], w[74], pars->GC_11, amp[126]); 
  FFV2_5_0(w[77], w[67], w[3], pars->GC_51, pars->GC_58, amp[127]); 
  FFV1_0(w[19], w[67], w[0], pars->GC_11, amp[128]); 
  FFV1_0(w[18], w[22], w[74], pars->GC_11, amp[129]); 
  FFV1_0(w[77], w[22], w[56], pars->GC_11, amp[130]); 
  FFV1_0(w[18], w[78], w[56], pars->GC_11, amp[131]); 
  FFV1_0(w[1], w[27], w[74], pars->GC_11, amp[132]); 
  FFV2_5_0(w[58], w[78], w[2], pars->GC_51, pars->GC_58, amp[133]); 
  FFV1_0(w[58], w[27], w[0], pars->GC_11, amp[134]); 
  FFV1_0(w[29], w[6], w[74], pars->GC_11, amp[135]); 
  FFV2_5_0(w[76], w[67], w[2], pars->GC_51, pars->GC_58, amp[136]); 
  FFV1_0(w[29], w[67], w[0], pars->GC_11, amp[137]); 
  FFV1_0(w[19], w[79], w[5], pars->GC_11, amp[138]); 
  FFV2_5_0(w[21], w[79], w[3], pars->GC_51, pars->GC_58, amp[139]); 
  FFV1_0(w[77], w[39], w[5], pars->GC_11, amp[140]); 
  FFV2_5_0(w[77], w[70], w[3], pars->GC_51, pars->GC_58, amp[141]); 
  FFV1_0(w[21], w[39], w[0], pars->GC_11, amp[142]); 
  FFV1_0(w[19], w[70], w[0], pars->GC_11, amp[143]); 
  FFV1_0(w[29], w[79], w[5], pars->GC_11, amp[144]); 
  FFV2_5_0(w[30], w[79], w[2], pars->GC_51, pars->GC_58, amp[145]); 
  FFV1_0(w[76], w[38], w[5], pars->GC_11, amp[146]); 
  FFV2_5_0(w[76], w[70], w[2], pars->GC_51, pars->GC_58, amp[147]); 
  FFV1_0(w[30], w[38], w[0], pars->GC_11, amp[148]); 
  FFV1_0(w[29], w[70], w[0], pars->GC_11, amp[149]); 
  FFV2_5_0(w[33], w[79], w[3], pars->GC_51, pars->GC_58, amp[150]); 
  FFV2_5_0(w[34], w[79], w[2], pars->GC_51, pars->GC_58, amp[151]); 
  FFV2_5_0(w[80], w[38], w[3], pars->GC_51, pars->GC_58, amp[152]); 
  FFV2_5_0(w[80], w[39], w[2], pars->GC_51, pars->GC_58, amp[153]); 
  FFV1_0(w[34], w[38], w[0], pars->GC_11, amp[154]); 
  FFV1_0(w[33], w[39], w[0], pars->GC_11, amp[155]); 
  FFV1_0(w[81], w[10], w[5], pars->GC_11, amp[156]); 
  FFV2_5_0(w[81], w[12], w[3], pars->GC_51, pars->GC_58, amp[157]); 
  FFV1_0(w[46], w[75], w[5], pars->GC_11, amp[158]); 
  FFV2_5_0(w[61], w[75], w[3], pars->GC_51, pars->GC_58, amp[159]); 
  FFV1_0(w[46], w[12], w[0], pars->GC_11, amp[160]); 
  FFV1_0(w[61], w[10], w[0], pars->GC_11, amp[161]); 
  FFV1_0(w[81], w[27], w[5], pars->GC_11, amp[162]); 
  FFV2_5_0(w[81], w[28], w[2], pars->GC_51, pars->GC_58, amp[163]); 
  FFV1_0(w[45], w[78], w[5], pars->GC_11, amp[164]); 
  FFV2_5_0(w[61], w[78], w[2], pars->GC_51, pars->GC_58, amp[165]); 
  FFV1_0(w[45], w[28], w[0], pars->GC_11, amp[166]); 
  FFV1_0(w[61], w[27], w[0], pars->GC_11, amp[167]); 
  FFV2_5_0(w[81], w[31], w[3], pars->GC_51, pars->GC_58, amp[168]); 
  FFV2_5_0(w[81], w[32], w[2], pars->GC_51, pars->GC_58, amp[169]); 
  FFV2_5_0(w[45], w[82], w[3], pars->GC_51, pars->GC_58, amp[170]); 
  FFV2_5_0(w[46], w[82], w[2], pars->GC_51, pars->GC_58, amp[171]); 
  FFV1_0(w[45], w[32], w[0], pars->GC_11, amp[172]); 
  FFV1_0(w[46], w[31], w[0], pars->GC_11, amp[173]); 
  FFV1_0(w[54], w[75], w[5], pars->GC_11, amp[174]); 
  FFV1_0(w[30], w[75], w[4], pars->GC_11, amp[175]); 
  FFV1_0(w[76], w[51], w[5], pars->GC_11, amp[176]); 
  FFV1_0(w[76], w[12], w[4], pars->GC_11, amp[177]); 
  FFV1_0(w[30], w[51], w[0], pars->GC_11, amp[178]); 
  FFV1_0(w[54], w[12], w[0], pars->GC_11, amp[179]); 
  FFV2_5_0(w[64], w[75], w[3], pars->GC_51, pars->GC_58, amp[180]); 
  FFV1_0(w[34], w[75], w[4], pars->GC_11, amp[181]); 
  FFV2_5_0(w[80], w[51], w[3], pars->GC_51, pars->GC_58, amp[182]); 
  FFV1_0(w[80], w[10], w[4], pars->GC_11, amp[183]); 
  FFV1_0(w[34], w[51], w[0], pars->GC_11, amp[184]); 
  FFV1_0(w[64], w[10], w[0], pars->GC_11, amp[185]); 
  FFV1_0(w[77], w[53], w[5], pars->GC_11, amp[186]); 
  FFV1_0(w[77], w[28], w[4], pars->GC_11, amp[187]); 
  FFV1_0(w[52], w[78], w[5], pars->GC_11, amp[188]); 
  FFV1_0(w[21], w[78], w[4], pars->GC_11, amp[189]); 
  FFV1_0(w[52], w[28], w[0], pars->GC_11, amp[190]); 
  FFV1_0(w[21], w[53], w[0], pars->GC_11, amp[191]); 
  FFV2_5_0(w[77], w[73], w[3], pars->GC_51, pars->GC_58, amp[192]); 
  FFV1_0(w[77], w[32], w[4], pars->GC_11, amp[193]); 
  FFV2_5_0(w[52], w[82], w[3], pars->GC_51, pars->GC_58, amp[194]); 
  FFV1_0(w[19], w[82], w[4], pars->GC_11, amp[195]); 
  FFV1_0(w[52], w[32], w[0], pars->GC_11, amp[196]); 
  FFV1_0(w[19], w[73], w[0], pars->GC_11, amp[197]); 
  FFV2_5_0(w[64], w[78], w[2], pars->GC_51, pars->GC_58, amp[198]); 
  FFV1_0(w[33], w[78], w[4], pars->GC_11, amp[199]); 
  FFV2_5_0(w[80], w[53], w[2], pars->GC_51, pars->GC_58, amp[200]); 
  FFV1_0(w[80], w[27], w[4], pars->GC_11, amp[201]); 
  FFV1_0(w[33], w[53], w[0], pars->GC_11, amp[202]); 
  FFV1_0(w[64], w[27], w[0], pars->GC_11, amp[203]); 
  FFV2_5_0(w[76], w[73], w[2], pars->GC_51, pars->GC_58, amp[204]); 
  FFV1_0(w[76], w[31], w[4], pars->GC_11, amp[205]); 
  FFV2_5_0(w[54], w[82], w[2], pars->GC_51, pars->GC_58, amp[206]); 
  FFV1_0(w[29], w[82], w[4], pars->GC_11, amp[207]); 
  FFV1_0(w[54], w[31], w[0], pars->GC_11, amp[208]); 
  FFV1_0(w[29], w[73], w[0], pars->GC_11, amp[209]); 
  FFV2_5_0(w[86], w[8], w[3], pars->GC_51, pars->GC_58, amp[210]); 
  FFV2_5_0(w[87], w[8], w[3], pars->GC_51, pars->GC_58, amp[211]); 
  FFV2_5_0(w[88], w[8], w[3], pars->GC_51, pars->GC_58, amp[212]); 
  FFV1_0(w[13], w[8], w[83], pars->GC_11, amp[213]); 
  FFV1_0(w[13], w[8], w[84], pars->GC_11, amp[214]); 
  FFV1_0(w[13], w[8], w[85], pars->GC_11, amp[215]); 
  FFV2_5_0(w[18], w[89], w[3], pars->GC_51, pars->GC_58, amp[216]); 
  FFV2_5_0(w[18], w[90], w[3], pars->GC_51, pars->GC_58, amp[217]); 
  FFV2_5_0(w[18], w[91], w[3], pars->GC_51, pars->GC_58, amp[218]); 
  FFV1_0(w[18], w[22], w[83], pars->GC_11, amp[219]); 
  FFV1_0(w[18], w[22], w[84], pars->GC_11, amp[220]); 
  FFV1_0(w[18], w[22], w[85], pars->GC_11, amp[221]); 
  FFV2_5_0(w[86], w[22], w[2], pars->GC_51, pars->GC_58, amp[222]); 
  FFV2_5_0(w[87], w[22], w[2], pars->GC_51, pars->GC_58, amp[223]); 
  FFV2_5_0(w[88], w[22], w[2], pars->GC_51, pars->GC_58, amp[224]); 
  FFV2_5_0(w[13], w[89], w[2], pars->GC_51, pars->GC_58, amp[225]); 
  FFV2_5_0(w[13], w[90], w[2], pars->GC_51, pars->GC_58, amp[226]); 
  FFV2_5_0(w[13], w[91], w[2], pars->GC_51, pars->GC_58, amp[227]); 
  FFV1_0(w[1], w[93], w[9], pars->GC_11, amp[228]); 
  FFV1_0(w[11], w[93], w[5], pars->GC_11, amp[229]); 
  FFV2_3_0(w[11], w[94], w[3], pars->GC_50, pars->GC_58, amp[230]); 
  FFV1_0(w[95], w[96], w[5], pars->GC_11, amp[231]); 
  FFV1_0(w[97], w[92], w[5], pars->GC_11, amp[232]); 
  FFV1_0(w[95], w[92], w[9], pars->GC_11, amp[233]); 
  FFV2_3_0(w[16], w[96], w[3], pars->GC_50, pars->GC_58, amp[234]); 
  FFV2_3_0(w[17], w[92], w[3], pars->GC_50, pars->GC_58, amp[235]); 
  FFV1_0(w[99], w[6], w[9], pars->GC_11, amp[236]); 
  FFV1_0(w[99], w[20], w[5], pars->GC_11, amp[237]); 
  FFV2_3_0(w[100], w[20], w[3], pars->GC_50, pars->GC_58, amp[238]); 
  FFV1_0(w[102], w[101], w[5], pars->GC_11, amp[239]); 
  FFV1_0(w[98], w[103], w[5], pars->GC_11, amp[240]); 
  FFV1_0(w[98], w[101], w[9], pars->GC_11, amp[241]); 
  FFV2_3_0(w[102], w[25], w[3], pars->GC_50, pars->GC_58, amp[242]); 
  FFV2_3_0(w[98], w[26], w[3], pars->GC_50, pars->GC_58, amp[243]); 
  FFV1_0(w[1], w[104], w[9], pars->GC_11, amp[244]); 
  FFV1_0(w[11], w[104], w[5], pars->GC_11, amp[245]); 
  FFV2_3_0(w[11], w[105], w[2], pars->GC_50, pars->GC_58, amp[246]); 
  FFV2_3_0(w[16], w[103], w[2], pars->GC_50, pars->GC_58, amp[247]); 
  FFV2_3_0(w[17], w[101], w[2], pars->GC_50, pars->GC_58, amp[248]); 
  FFV1_0(w[106], w[6], w[9], pars->GC_11, amp[249]); 
  FFV1_0(w[106], w[20], w[5], pars->GC_11, amp[250]); 
  FFV2_3_0(w[107], w[20], w[2], pars->GC_50, pars->GC_58, amp[251]); 
  FFV2_3_0(w[97], w[25], w[2], pars->GC_50, pars->GC_58, amp[252]); 
  FFV2_3_0(w[95], w[26], w[2], pars->GC_50, pars->GC_58, amp[253]); 
  FFV2_3_0(w[11], w[108], w[3], pars->GC_50, pars->GC_58, amp[254]); 
  FFV2_3_0(w[11], w[109], w[2], pars->GC_50, pars->GC_58, amp[255]); 
  FFV2_3_0(w[110], w[20], w[3], pars->GC_50, pars->GC_58, amp[256]); 
  FFV2_3_0(w[111], w[20], w[2], pars->GC_50, pars->GC_58, amp[257]); 
  FFV2_3_0(w[37], w[112], w[3], pars->GC_50, pars->GC_58, amp[258]); 
  FFV2_3_0(w[37], w[113], w[2], pars->GC_50, pars->GC_58, amp[259]); 
  FFV2_3_0(w[98], w[40], w[3], pars->GC_50, pars->GC_58, amp[260]); 
  FFV2_3_0(w[114], w[36], w[3], pars->GC_50, pars->GC_58, amp[261]); 
  FFV2_3_0(w[95], w[40], w[2], pars->GC_50, pars->GC_58, amp[262]); 
  FFV2_3_0(w[115], w[36], w[2], pars->GC_50, pars->GC_58, amp[263]); 
  FFV2_3_0(w[116], w[44], w[3], pars->GC_50, pars->GC_58, amp[264]); 
  FFV2_3_0(w[117], w[44], w[2], pars->GC_50, pars->GC_58, amp[265]); 
  FFV2_3_0(w[47], w[92], w[3], pars->GC_50, pars->GC_58, amp[266]); 
  FFV2_3_0(w[43], w[118], w[3], pars->GC_50, pars->GC_58, amp[267]); 
  FFV2_3_0(w[47], w[101], w[2], pars->GC_50, pars->GC_58, amp[268]); 
  FFV2_3_0(w[43], w[119], w[2], pars->GC_50, pars->GC_58, amp[269]); 
  FFV1_0(w[1], w[93], w[50], pars->GC_11, amp[270]); 
  FFV2_3_0(w[37], w[120], w[3], pars->GC_50, pars->GC_58, amp[271]); 
  FFV1_0(w[37], w[93], w[4], pars->GC_11, amp[272]); 
  FFV1_0(w[95], w[92], w[50], pars->GC_11, amp[273]); 
  FFV1_0(w[95], w[118], w[4], pars->GC_11, amp[274]); 
  FFV1_0(w[115], w[92], w[4], pars->GC_11, amp[275]); 
  FFV1_0(w[99], w[6], w[50], pars->GC_11, amp[276]); 
  FFV2_3_0(w[121], w[44], w[3], pars->GC_50, pars->GC_58, amp[277]); 
  FFV1_0(w[99], w[44], w[4], pars->GC_11, amp[278]); 
  FFV1_0(w[98], w[101], w[50], pars->GC_11, amp[279]); 
  FFV1_0(w[114], w[101], w[4], pars->GC_11, amp[280]); 
  FFV1_0(w[98], w[119], w[4], pars->GC_11, amp[281]); 
  FFV1_0(w[1], w[104], w[50], pars->GC_11, amp[282]); 
  FFV2_3_0(w[37], w[122], w[2], pars->GC_50, pars->GC_58, amp[283]); 
  FFV1_0(w[37], w[104], w[4], pars->GC_11, amp[284]); 
  FFV1_0(w[106], w[6], w[50], pars->GC_11, amp[285]); 
  FFV2_3_0(w[123], w[44], w[2], pars->GC_50, pars->GC_58, amp[286]); 
  FFV1_0(w[106], w[44], w[4], pars->GC_11, amp[287]); 
  FFV2_3_0(w[58], w[124], w[3], pars->GC_50, pars->GC_58, amp[288]); 
  FFV2_3_0(w[58], w[125], w[2], pars->GC_50, pars->GC_58, amp[289]); 
  FFV2_3_0(w[98], w[60], w[3], pars->GC_50, pars->GC_58, amp[290]); 
  FFV1_0(w[98], w[125], w[56], pars->GC_11, amp[291]); 
  FFV2_3_0(w[95], w[60], w[2], pars->GC_50, pars->GC_58, amp[292]); 
  FFV1_0(w[95], w[124], w[56], pars->GC_11, amp[293]); 
  FFV1_0(w[117], w[124], w[5], pars->GC_11, amp[294]); 
  FFV2_3_0(w[61], w[124], w[3], pars->GC_50, pars->GC_58, amp[295]); 
  FFV1_0(w[116], w[125], w[5], pars->GC_11, amp[296]); 
  FFV2_3_0(w[61], w[125], w[2], pars->GC_50, pars->GC_58, amp[297]); 
  FFV2_3_0(w[116], w[62], w[3], pars->GC_50, pars->GC_58, amp[298]); 
  FFV2_3_0(w[117], w[62], w[2], pars->GC_50, pars->GC_58, amp[299]); 
  FFV1_0(w[99], w[63], w[5], pars->GC_11, amp[300]); 
  FFV2_3_0(w[100], w[63], w[3], pars->GC_50, pars->GC_58, amp[301]); 
  FFV1_0(w[121], w[125], w[5], pars->GC_11, amp[302]); 
  FFV1_0(w[100], w[125], w[4], pars->GC_11, amp[303]); 
  FFV2_3_0(w[121], w[62], w[3], pars->GC_50, pars->GC_58, amp[304]); 
  FFV1_0(w[99], w[62], w[4], pars->GC_11, amp[305]); 
  FFV1_0(w[106], w[63], w[5], pars->GC_11, amp[306]); 
  FFV2_3_0(w[107], w[63], w[2], pars->GC_50, pars->GC_58, amp[307]); 
  FFV1_0(w[123], w[124], w[5], pars->GC_11, amp[308]); 
  FFV1_0(w[107], w[124], w[4], pars->GC_11, amp[309]); 
  FFV2_3_0(w[123], w[62], w[2], pars->GC_50, pars->GC_58, amp[310]); 
  FFV1_0(w[106], w[62], w[4], pars->GC_11, amp[311]); 
  FFV2_3_0(w[110], w[63], w[3], pars->GC_50, pars->GC_58, amp[312]); 
  FFV2_3_0(w[111], w[63], w[2], pars->GC_50, pars->GC_58, amp[313]); 
  FFV2_3_0(w[64], w[124], w[3], pars->GC_50, pars->GC_58, amp[314]); 
  FFV1_0(w[111], w[124], w[4], pars->GC_11, amp[315]); 
  FFV2_3_0(w[64], w[125], w[2], pars->GC_50, pars->GC_58, amp[316]); 
  FFV1_0(w[110], w[125], w[4], pars->GC_11, amp[317]); 
  FFV2_3_0(w[126], w[67], w[3], pars->GC_50, pars->GC_58, amp[318]); 
  FFV2_3_0(w[127], w[67], w[2], pars->GC_50, pars->GC_58, amp[319]); 
  FFV2_3_0(w[69], w[92], w[3], pars->GC_50, pars->GC_58, amp[320]); 
  FFV1_0(w[127], w[92], w[56], pars->GC_11, amp[321]); 
  FFV2_3_0(w[69], w[101], w[2], pars->GC_50, pars->GC_58, amp[322]); 
  FFV1_0(w[126], w[101], w[56], pars->GC_11, amp[323]); 
  FFV1_0(w[126], w[113], w[5], pars->GC_11, amp[324]); 
  FFV2_3_0(w[126], w[70], w[3], pars->GC_50, pars->GC_58, amp[325]); 
  FFV1_0(w[127], w[112], w[5], pars->GC_11, amp[326]); 
  FFV2_3_0(w[127], w[70], w[2], pars->GC_50, pars->GC_58, amp[327]); 
  FFV2_3_0(w[71], w[112], w[3], pars->GC_50, pars->GC_58, amp[328]); 
  FFV2_3_0(w[71], w[113], w[2], pars->GC_50, pars->GC_58, amp[329]); 
  FFV1_0(w[72], w[93], w[5], pars->GC_11, amp[330]); 
  FFV2_3_0(w[72], w[94], w[3], pars->GC_50, pars->GC_58, amp[331]); 
  FFV1_0(w[127], w[120], w[5], pars->GC_11, amp[332]); 
  FFV1_0(w[127], w[94], w[4], pars->GC_11, amp[333]); 
  FFV2_3_0(w[71], w[120], w[3], pars->GC_50, pars->GC_58, amp[334]); 
  FFV1_0(w[71], w[93], w[4], pars->GC_11, amp[335]); 
  FFV1_0(w[72], w[104], w[5], pars->GC_11, amp[336]); 
  FFV2_3_0(w[72], w[105], w[2], pars->GC_50, pars->GC_58, amp[337]); 
  FFV1_0(w[126], w[122], w[5], pars->GC_11, amp[338]); 
  FFV1_0(w[126], w[105], w[4], pars->GC_11, amp[339]); 
  FFV2_3_0(w[71], w[122], w[2], pars->GC_50, pars->GC_58, amp[340]); 
  FFV1_0(w[71], w[104], w[4], pars->GC_11, amp[341]); 
  FFV2_3_0(w[72], w[108], w[3], pars->GC_50, pars->GC_58, amp[342]); 
  FFV2_3_0(w[72], w[109], w[2], pars->GC_50, pars->GC_58, amp[343]); 
  FFV2_3_0(w[126], w[73], w[3], pars->GC_50, pars->GC_58, amp[344]); 
  FFV1_0(w[126], w[109], w[4], pars->GC_11, amp[345]); 
  FFV2_3_0(w[127], w[73], w[2], pars->GC_50, pars->GC_58, amp[346]); 
  FFV1_0(w[127], w[108], w[4], pars->GC_11, amp[347]); 
  FFV1_0(w[1], w[93], w[74], pars->GC_11, amp[348]); 
  FFV2_3_0(w[58], w[128], w[3], pars->GC_50, pars->GC_58, amp[349]); 
  FFV1_0(w[58], w[93], w[0], pars->GC_11, amp[350]); 
  FFV1_0(w[95], w[92], w[74], pars->GC_11, amp[351]); 
  FFV1_0(w[95], w[128], w[56], pars->GC_11, amp[352]); 
  FFV1_0(w[129], w[92], w[56], pars->GC_11, amp[353]); 
  FFV1_0(w[99], w[6], w[74], pars->GC_11, amp[354]); 
  FFV2_3_0(w[130], w[67], w[3], pars->GC_50, pars->GC_58, amp[355]); 
  FFV1_0(w[99], w[67], w[0], pars->GC_11, amp[356]); 
  FFV1_0(w[98], w[101], w[74], pars->GC_11, amp[357]); 
  FFV1_0(w[130], w[101], w[56], pars->GC_11, amp[358]); 
  FFV1_0(w[98], w[131], w[56], pars->GC_11, amp[359]); 
  FFV1_0(w[1], w[104], w[74], pars->GC_11, amp[360]); 
  FFV2_3_0(w[58], w[131], w[2], pars->GC_50, pars->GC_58, amp[361]); 
  FFV1_0(w[58], w[104], w[0], pars->GC_11, amp[362]); 
  FFV1_0(w[106], w[6], w[74], pars->GC_11, amp[363]); 
  FFV2_3_0(w[129], w[67], w[2], pars->GC_50, pars->GC_58, amp[364]); 
  FFV1_0(w[106], w[67], w[0], pars->GC_11, amp[365]); 
  FFV1_0(w[99], w[79], w[5], pars->GC_11, amp[366]); 
  FFV2_3_0(w[100], w[79], w[3], pars->GC_50, pars->GC_58, amp[367]); 
  FFV1_0(w[130], w[113], w[5], pars->GC_11, amp[368]); 
  FFV2_3_0(w[130], w[70], w[3], pars->GC_50, pars->GC_58, amp[369]); 
  FFV1_0(w[100], w[113], w[0], pars->GC_11, amp[370]); 
  FFV1_0(w[99], w[70], w[0], pars->GC_11, amp[371]); 
  FFV1_0(w[106], w[79], w[5], pars->GC_11, amp[372]); 
  FFV2_3_0(w[107], w[79], w[2], pars->GC_50, pars->GC_58, amp[373]); 
  FFV1_0(w[129], w[112], w[5], pars->GC_11, amp[374]); 
  FFV2_3_0(w[129], w[70], w[2], pars->GC_50, pars->GC_58, amp[375]); 
  FFV1_0(w[107], w[112], w[0], pars->GC_11, amp[376]); 
  FFV1_0(w[106], w[70], w[0], pars->GC_11, amp[377]); 
  FFV2_3_0(w[110], w[79], w[3], pars->GC_50, pars->GC_58, amp[378]); 
  FFV2_3_0(w[111], w[79], w[2], pars->GC_50, pars->GC_58, amp[379]); 
  FFV2_3_0(w[80], w[112], w[3], pars->GC_50, pars->GC_58, amp[380]); 
  FFV2_3_0(w[80], w[113], w[2], pars->GC_50, pars->GC_58, amp[381]); 
  FFV1_0(w[111], w[112], w[0], pars->GC_11, amp[382]); 
  FFV1_0(w[110], w[113], w[0], pars->GC_11, amp[383]); 
  FFV1_0(w[81], w[93], w[5], pars->GC_11, amp[384]); 
  FFV2_3_0(w[81], w[94], w[3], pars->GC_50, pars->GC_58, amp[385]); 
  FFV1_0(w[117], w[128], w[5], pars->GC_11, amp[386]); 
  FFV2_3_0(w[61], w[128], w[3], pars->GC_50, pars->GC_58, amp[387]); 
  FFV1_0(w[117], w[94], w[0], pars->GC_11, amp[388]); 
  FFV1_0(w[61], w[93], w[0], pars->GC_11, amp[389]); 
  FFV1_0(w[81], w[104], w[5], pars->GC_11, amp[390]); 
  FFV2_3_0(w[81], w[105], w[2], pars->GC_50, pars->GC_58, amp[391]); 
  FFV1_0(w[116], w[131], w[5], pars->GC_11, amp[392]); 
  FFV2_3_0(w[61], w[131], w[2], pars->GC_50, pars->GC_58, amp[393]); 
  FFV1_0(w[116], w[105], w[0], pars->GC_11, amp[394]); 
  FFV1_0(w[61], w[104], w[0], pars->GC_11, amp[395]); 
  FFV2_3_0(w[81], w[108], w[3], pars->GC_50, pars->GC_58, amp[396]); 
  FFV2_3_0(w[81], w[109], w[2], pars->GC_50, pars->GC_58, amp[397]); 
  FFV2_3_0(w[116], w[82], w[3], pars->GC_50, pars->GC_58, amp[398]); 
  FFV2_3_0(w[117], w[82], w[2], pars->GC_50, pars->GC_58, amp[399]); 
  FFV1_0(w[116], w[109], w[0], pars->GC_11, amp[400]); 
  FFV1_0(w[117], w[108], w[0], pars->GC_11, amp[401]); 
  FFV1_0(w[123], w[128], w[5], pars->GC_11, amp[402]); 
  FFV1_0(w[107], w[128], w[4], pars->GC_11, amp[403]); 
  FFV1_0(w[129], w[120], w[5], pars->GC_11, amp[404]); 
  FFV1_0(w[129], w[94], w[4], pars->GC_11, amp[405]); 
  FFV1_0(w[107], w[120], w[0], pars->GC_11, amp[406]); 
  FFV1_0(w[123], w[94], w[0], pars->GC_11, amp[407]); 
  FFV2_3_0(w[64], w[128], w[3], pars->GC_50, pars->GC_58, amp[408]); 
  FFV1_0(w[111], w[128], w[4], pars->GC_11, amp[409]); 
  FFV2_3_0(w[80], w[120], w[3], pars->GC_50, pars->GC_58, amp[410]); 
  FFV1_0(w[80], w[93], w[4], pars->GC_11, amp[411]); 
  FFV1_0(w[111], w[120], w[0], pars->GC_11, amp[412]); 
  FFV1_0(w[64], w[93], w[0], pars->GC_11, amp[413]); 
  FFV1_0(w[130], w[122], w[5], pars->GC_11, amp[414]); 
  FFV1_0(w[130], w[105], w[4], pars->GC_11, amp[415]); 
  FFV1_0(w[121], w[131], w[5], pars->GC_11, amp[416]); 
  FFV1_0(w[100], w[131], w[4], pars->GC_11, amp[417]); 
  FFV1_0(w[121], w[105], w[0], pars->GC_11, amp[418]); 
  FFV1_0(w[100], w[122], w[0], pars->GC_11, amp[419]); 
  FFV2_3_0(w[130], w[73], w[3], pars->GC_50, pars->GC_58, amp[420]); 
  FFV1_0(w[130], w[109], w[4], pars->GC_11, amp[421]); 
  FFV2_3_0(w[121], w[82], w[3], pars->GC_50, pars->GC_58, amp[422]); 
  FFV1_0(w[99], w[82], w[4], pars->GC_11, amp[423]); 
  FFV1_0(w[121], w[109], w[0], pars->GC_11, amp[424]); 
  FFV1_0(w[99], w[73], w[0], pars->GC_11, amp[425]); 
  FFV2_3_0(w[64], w[131], w[2], pars->GC_50, pars->GC_58, amp[426]); 
  FFV1_0(w[110], w[131], w[4], pars->GC_11, amp[427]); 
  FFV2_3_0(w[80], w[122], w[2], pars->GC_50, pars->GC_58, amp[428]); 
  FFV1_0(w[80], w[104], w[4], pars->GC_11, amp[429]); 
  FFV1_0(w[110], w[122], w[0], pars->GC_11, amp[430]); 
  FFV1_0(w[64], w[104], w[0], pars->GC_11, amp[431]); 
  FFV2_3_0(w[129], w[73], w[2], pars->GC_50, pars->GC_58, amp[432]); 
  FFV1_0(w[129], w[108], w[4], pars->GC_11, amp[433]); 
  FFV2_3_0(w[123], w[82], w[2], pars->GC_50, pars->GC_58, amp[434]); 
  FFV1_0(w[106], w[82], w[4], pars->GC_11, amp[435]); 
  FFV1_0(w[123], w[108], w[0], pars->GC_11, amp[436]); 
  FFV1_0(w[106], w[73], w[0], pars->GC_11, amp[437]); 
  FFV2_3_0(w[86], w[92], w[3], pars->GC_50, pars->GC_58, amp[438]); 
  FFV2_3_0(w[87], w[92], w[3], pars->GC_50, pars->GC_58, amp[439]); 
  FFV2_3_0(w[88], w[92], w[3], pars->GC_50, pars->GC_58, amp[440]); 
  FFV1_0(w[95], w[92], w[83], pars->GC_11, amp[441]); 
  FFV1_0(w[95], w[92], w[84], pars->GC_11, amp[442]); 
  FFV1_0(w[95], w[92], w[85], pars->GC_11, amp[443]); 
  FFV2_3_0(w[98], w[89], w[3], pars->GC_50, pars->GC_58, amp[444]); 
  FFV2_3_0(w[98], w[90], w[3], pars->GC_50, pars->GC_58, amp[445]); 
  FFV2_3_0(w[98], w[91], w[3], pars->GC_50, pars->GC_58, amp[446]); 
  FFV1_0(w[98], w[101], w[83], pars->GC_11, amp[447]); 
  FFV1_0(w[98], w[101], w[84], pars->GC_11, amp[448]); 
  FFV1_0(w[98], w[101], w[85], pars->GC_11, amp[449]); 
  FFV2_3_0(w[86], w[101], w[2], pars->GC_50, pars->GC_58, amp[450]); 
  FFV2_3_0(w[87], w[101], w[2], pars->GC_50, pars->GC_58, amp[451]); 
  FFV2_3_0(w[88], w[101], w[2], pars->GC_50, pars->GC_58, amp[452]); 
  FFV2_3_0(w[95], w[89], w[2], pars->GC_50, pars->GC_58, amp[453]); 
  FFV2_3_0(w[95], w[90], w[2], pars->GC_50, pars->GC_58, amp[454]); 
  FFV2_3_0(w[95], w[91], w[2], pars->GC_50, pars->GC_58, amp[455]); 
  FFV1_0(w[133], w[135], w[9], pars->GC_11, amp[456]); 
  FFV1_0(w[136], w[135], w[5], pars->GC_11, amp[457]); 
  FFV2_5_0(w[136], w[137], w[3], pars->GC_51, pars->GC_58, amp[458]); 
  FFV1_0(w[138], w[139], w[5], pars->GC_11, amp[459]); 
  FFV1_0(w[140], w[134], w[5], pars->GC_11, amp[460]); 
  FFV1_0(w[138], w[134], w[9], pars->GC_11, amp[461]); 
  FFV2_5_0(w[141], w[139], w[3], pars->GC_51, pars->GC_58, amp[462]); 
  FFV2_5_0(w[142], w[134], w[3], pars->GC_51, pars->GC_58, amp[463]); 
  FFV1_0(w[144], w[132], w[9], pars->GC_11, amp[464]); 
  FFV1_0(w[144], w[145], w[5], pars->GC_11, amp[465]); 
  FFV2_5_0(w[146], w[145], w[3], pars->GC_51, pars->GC_58, amp[466]); 
  FFV1_0(w[148], w[147], w[5], pars->GC_11, amp[467]); 
  FFV1_0(w[143], w[149], w[5], pars->GC_11, amp[468]); 
  FFV1_0(w[143], w[147], w[9], pars->GC_11, amp[469]); 
  FFV2_5_0(w[148], w[150], w[3], pars->GC_51, pars->GC_58, amp[470]); 
  FFV2_5_0(w[143], w[151], w[3], pars->GC_51, pars->GC_58, amp[471]); 
  FFV1_0(w[133], w[152], w[9], pars->GC_11, amp[472]); 
  FFV1_0(w[136], w[152], w[5], pars->GC_11, amp[473]); 
  FFV2_5_0(w[136], w[153], w[2], pars->GC_51, pars->GC_58, amp[474]); 
  FFV2_5_0(w[141], w[149], w[2], pars->GC_51, pars->GC_58, amp[475]); 
  FFV2_5_0(w[142], w[147], w[2], pars->GC_51, pars->GC_58, amp[476]); 
  FFV1_0(w[154], w[132], w[9], pars->GC_11, amp[477]); 
  FFV1_0(w[154], w[145], w[5], pars->GC_11, amp[478]); 
  FFV2_5_0(w[155], w[145], w[2], pars->GC_51, pars->GC_58, amp[479]); 
  FFV2_5_0(w[140], w[150], w[2], pars->GC_51, pars->GC_58, amp[480]); 
  FFV2_5_0(w[138], w[151], w[2], pars->GC_51, pars->GC_58, amp[481]); 
  FFV2_5_0(w[136], w[156], w[3], pars->GC_51, pars->GC_58, amp[482]); 
  FFV2_5_0(w[136], w[157], w[2], pars->GC_51, pars->GC_58, amp[483]); 
  FFV2_5_0(w[158], w[145], w[3], pars->GC_51, pars->GC_58, amp[484]); 
  FFV2_5_0(w[159], w[145], w[2], pars->GC_51, pars->GC_58, amp[485]); 
  FFV2_5_0(w[161], w[162], w[3], pars->GC_51, pars->GC_58, amp[486]); 
  FFV2_5_0(w[161], w[163], w[2], pars->GC_51, pars->GC_58, amp[487]); 
  FFV2_5_0(w[143], w[164], w[3], pars->GC_51, pars->GC_58, amp[488]); 
  FFV2_5_0(w[165], w[160], w[3], pars->GC_51, pars->GC_58, amp[489]); 
  FFV2_5_0(w[138], w[164], w[2], pars->GC_51, pars->GC_58, amp[490]); 
  FFV2_5_0(w[166], w[160], w[2], pars->GC_51, pars->GC_58, amp[491]); 
  FFV2_5_0(w[169], w[168], w[3], pars->GC_51, pars->GC_58, amp[492]); 
  FFV2_5_0(w[170], w[168], w[2], pars->GC_51, pars->GC_58, amp[493]); 
  FFV2_5_0(w[171], w[134], w[3], pars->GC_51, pars->GC_58, amp[494]); 
  FFV2_5_0(w[167], w[172], w[3], pars->GC_51, pars->GC_58, amp[495]); 
  FFV2_5_0(w[171], w[147], w[2], pars->GC_51, pars->GC_58, amp[496]); 
  FFV2_5_0(w[167], w[173], w[2], pars->GC_51, pars->GC_58, amp[497]); 
  FFV1_0(w[133], w[135], w[50], pars->GC_11, amp[498]); 
  FFV2_5_0(w[161], w[174], w[3], pars->GC_51, pars->GC_58, amp[499]); 
  FFV1_0(w[161], w[135], w[4], pars->GC_11, amp[500]); 
  FFV1_0(w[138], w[134], w[50], pars->GC_11, amp[501]); 
  FFV1_0(w[138], w[172], w[4], pars->GC_11, amp[502]); 
  FFV1_0(w[166], w[134], w[4], pars->GC_11, amp[503]); 
  FFV1_0(w[144], w[132], w[50], pars->GC_11, amp[504]); 
  FFV2_5_0(w[175], w[168], w[3], pars->GC_51, pars->GC_58, amp[505]); 
  FFV1_0(w[144], w[168], w[4], pars->GC_11, amp[506]); 
  FFV1_0(w[143], w[147], w[50], pars->GC_11, amp[507]); 
  FFV1_0(w[165], w[147], w[4], pars->GC_11, amp[508]); 
  FFV1_0(w[143], w[173], w[4], pars->GC_11, amp[509]); 
  FFV1_0(w[133], w[152], w[50], pars->GC_11, amp[510]); 
  FFV2_5_0(w[161], w[176], w[2], pars->GC_51, pars->GC_58, amp[511]); 
  FFV1_0(w[161], w[152], w[4], pars->GC_11, amp[512]); 
  FFV1_0(w[154], w[132], w[50], pars->GC_11, amp[513]); 
  FFV2_5_0(w[177], w[168], w[2], pars->GC_51, pars->GC_58, amp[514]); 
  FFV1_0(w[154], w[168], w[4], pars->GC_11, amp[515]); 
  FFV2_5_0(w[180], w[179], w[3], pars->GC_51, pars->GC_58, amp[516]); 
  FFV2_5_0(w[180], w[181], w[2], pars->GC_51, pars->GC_58, amp[517]); 
  FFV2_5_0(w[143], w[182], w[3], pars->GC_51, pars->GC_58, amp[518]); 
  FFV1_0(w[143], w[181], w[56], pars->GC_11, amp[519]); 
  FFV2_5_0(w[138], w[182], w[2], pars->GC_51, pars->GC_58, amp[520]); 
  FFV1_0(w[138], w[179], w[56], pars->GC_11, amp[521]); 
  FFV1_0(w[170], w[179], w[5], pars->GC_11, amp[522]); 
  FFV2_5_0(w[183], w[179], w[3], pars->GC_51, pars->GC_58, amp[523]); 
  FFV1_0(w[169], w[181], w[5], pars->GC_11, amp[524]); 
  FFV2_5_0(w[183], w[181], w[2], pars->GC_51, pars->GC_58, amp[525]); 
  FFV2_5_0(w[169], w[184], w[3], pars->GC_51, pars->GC_58, amp[526]); 
  FFV2_5_0(w[170], w[184], w[2], pars->GC_51, pars->GC_58, amp[527]); 
  FFV1_0(w[144], w[185], w[5], pars->GC_11, amp[528]); 
  FFV2_5_0(w[146], w[185], w[3], pars->GC_51, pars->GC_58, amp[529]); 
  FFV1_0(w[175], w[181], w[5], pars->GC_11, amp[530]); 
  FFV1_0(w[146], w[181], w[4], pars->GC_11, amp[531]); 
  FFV2_5_0(w[175], w[184], w[3], pars->GC_51, pars->GC_58, amp[532]); 
  FFV1_0(w[144], w[184], w[4], pars->GC_11, amp[533]); 
  FFV1_0(w[154], w[185], w[5], pars->GC_11, amp[534]); 
  FFV2_5_0(w[155], w[185], w[2], pars->GC_51, pars->GC_58, amp[535]); 
  FFV1_0(w[177], w[179], w[5], pars->GC_11, amp[536]); 
  FFV1_0(w[155], w[179], w[4], pars->GC_11, amp[537]); 
  FFV2_5_0(w[177], w[184], w[2], pars->GC_51, pars->GC_58, amp[538]); 
  FFV1_0(w[154], w[184], w[4], pars->GC_11, amp[539]); 
  FFV2_5_0(w[158], w[185], w[3], pars->GC_51, pars->GC_58, amp[540]); 
  FFV2_5_0(w[159], w[185], w[2], pars->GC_51, pars->GC_58, amp[541]); 
  FFV2_5_0(w[186], w[179], w[3], pars->GC_51, pars->GC_58, amp[542]); 
  FFV1_0(w[159], w[179], w[4], pars->GC_11, amp[543]); 
  FFV2_5_0(w[186], w[181], w[2], pars->GC_51, pars->GC_58, amp[544]); 
  FFV1_0(w[158], w[181], w[4], pars->GC_11, amp[545]); 
  FFV2_5_0(w[188], w[189], w[3], pars->GC_51, pars->GC_58, amp[546]); 
  FFV2_5_0(w[190], w[189], w[2], pars->GC_51, pars->GC_58, amp[547]); 
  FFV2_5_0(w[191], w[134], w[3], pars->GC_51, pars->GC_58, amp[548]); 
  FFV1_0(w[190], w[134], w[56], pars->GC_11, amp[549]); 
  FFV2_5_0(w[191], w[147], w[2], pars->GC_51, pars->GC_58, amp[550]); 
  FFV1_0(w[188], w[147], w[56], pars->GC_11, amp[551]); 
  FFV1_0(w[188], w[163], w[5], pars->GC_11, amp[552]); 
  FFV2_5_0(w[188], w[192], w[3], pars->GC_51, pars->GC_58, amp[553]); 
  FFV1_0(w[190], w[162], w[5], pars->GC_11, amp[554]); 
  FFV2_5_0(w[190], w[192], w[2], pars->GC_51, pars->GC_58, amp[555]); 
  FFV2_5_0(w[193], w[162], w[3], pars->GC_51, pars->GC_58, amp[556]); 
  FFV2_5_0(w[193], w[163], w[2], pars->GC_51, pars->GC_58, amp[557]); 
  FFV1_0(w[194], w[135], w[5], pars->GC_11, amp[558]); 
  FFV2_5_0(w[194], w[137], w[3], pars->GC_51, pars->GC_58, amp[559]); 
  FFV1_0(w[190], w[174], w[5], pars->GC_11, amp[560]); 
  FFV1_0(w[190], w[137], w[4], pars->GC_11, amp[561]); 
  FFV2_5_0(w[193], w[174], w[3], pars->GC_51, pars->GC_58, amp[562]); 
  FFV1_0(w[193], w[135], w[4], pars->GC_11, amp[563]); 
  FFV1_0(w[194], w[152], w[5], pars->GC_11, amp[564]); 
  FFV2_5_0(w[194], w[153], w[2], pars->GC_51, pars->GC_58, amp[565]); 
  FFV1_0(w[188], w[176], w[5], pars->GC_11, amp[566]); 
  FFV1_0(w[188], w[153], w[4], pars->GC_11, amp[567]); 
  FFV2_5_0(w[193], w[176], w[2], pars->GC_51, pars->GC_58, amp[568]); 
  FFV1_0(w[193], w[152], w[4], pars->GC_11, amp[569]); 
  FFV2_5_0(w[194], w[156], w[3], pars->GC_51, pars->GC_58, amp[570]); 
  FFV2_5_0(w[194], w[157], w[2], pars->GC_51, pars->GC_58, amp[571]); 
  FFV2_5_0(w[188], w[195], w[3], pars->GC_51, pars->GC_58, amp[572]); 
  FFV1_0(w[188], w[157], w[4], pars->GC_11, amp[573]); 
  FFV2_5_0(w[190], w[195], w[2], pars->GC_51, pars->GC_58, amp[574]); 
  FFV1_0(w[190], w[156], w[4], pars->GC_11, amp[575]); 
  FFV1_0(w[133], w[135], w[74], pars->GC_11, amp[576]); 
  FFV2_5_0(w[180], w[196], w[3], pars->GC_51, pars->GC_58, amp[577]); 
  FFV1_0(w[180], w[135], w[0], pars->GC_11, amp[578]); 
  FFV1_0(w[138], w[134], w[74], pars->GC_11, amp[579]); 
  FFV1_0(w[138], w[196], w[56], pars->GC_11, amp[580]); 
  FFV1_0(w[197], w[134], w[56], pars->GC_11, amp[581]); 
  FFV1_0(w[144], w[132], w[74], pars->GC_11, amp[582]); 
  FFV2_5_0(w[198], w[189], w[3], pars->GC_51, pars->GC_58, amp[583]); 
  FFV1_0(w[144], w[189], w[0], pars->GC_11, amp[584]); 
  FFV1_0(w[143], w[147], w[74], pars->GC_11, amp[585]); 
  FFV1_0(w[198], w[147], w[56], pars->GC_11, amp[586]); 
  FFV1_0(w[143], w[199], w[56], pars->GC_11, amp[587]); 
  FFV1_0(w[133], w[152], w[74], pars->GC_11, amp[588]); 
  FFV2_5_0(w[180], w[199], w[2], pars->GC_51, pars->GC_58, amp[589]); 
  FFV1_0(w[180], w[152], w[0], pars->GC_11, amp[590]); 
  FFV1_0(w[154], w[132], w[74], pars->GC_11, amp[591]); 
  FFV2_5_0(w[197], w[189], w[2], pars->GC_51, pars->GC_58, amp[592]); 
  FFV1_0(w[154], w[189], w[0], pars->GC_11, amp[593]); 
  FFV1_0(w[144], w[200], w[5], pars->GC_11, amp[594]); 
  FFV2_5_0(w[146], w[200], w[3], pars->GC_51, pars->GC_58, amp[595]); 
  FFV1_0(w[198], w[163], w[5], pars->GC_11, amp[596]); 
  FFV2_5_0(w[198], w[192], w[3], pars->GC_51, pars->GC_58, amp[597]); 
  FFV1_0(w[146], w[163], w[0], pars->GC_11, amp[598]); 
  FFV1_0(w[144], w[192], w[0], pars->GC_11, amp[599]); 
  FFV1_0(w[154], w[200], w[5], pars->GC_11, amp[600]); 
  FFV2_5_0(w[155], w[200], w[2], pars->GC_51, pars->GC_58, amp[601]); 
  FFV1_0(w[197], w[162], w[5], pars->GC_11, amp[602]); 
  FFV2_5_0(w[197], w[192], w[2], pars->GC_51, pars->GC_58, amp[603]); 
  FFV1_0(w[155], w[162], w[0], pars->GC_11, amp[604]); 
  FFV1_0(w[154], w[192], w[0], pars->GC_11, amp[605]); 
  FFV2_5_0(w[158], w[200], w[3], pars->GC_51, pars->GC_58, amp[606]); 
  FFV2_5_0(w[159], w[200], w[2], pars->GC_51, pars->GC_58, amp[607]); 
  FFV2_5_0(w[201], w[162], w[3], pars->GC_51, pars->GC_58, amp[608]); 
  FFV2_5_0(w[201], w[163], w[2], pars->GC_51, pars->GC_58, amp[609]); 
  FFV1_0(w[159], w[162], w[0], pars->GC_11, amp[610]); 
  FFV1_0(w[158], w[163], w[0], pars->GC_11, amp[611]); 
  FFV1_0(w[202], w[135], w[5], pars->GC_11, amp[612]); 
  FFV2_5_0(w[202], w[137], w[3], pars->GC_51, pars->GC_58, amp[613]); 
  FFV1_0(w[170], w[196], w[5], pars->GC_11, amp[614]); 
  FFV2_5_0(w[183], w[196], w[3], pars->GC_51, pars->GC_58, amp[615]); 
  FFV1_0(w[170], w[137], w[0], pars->GC_11, amp[616]); 
  FFV1_0(w[183], w[135], w[0], pars->GC_11, amp[617]); 
  FFV1_0(w[202], w[152], w[5], pars->GC_11, amp[618]); 
  FFV2_5_0(w[202], w[153], w[2], pars->GC_51, pars->GC_58, amp[619]); 
  FFV1_0(w[169], w[199], w[5], pars->GC_11, amp[620]); 
  FFV2_5_0(w[183], w[199], w[2], pars->GC_51, pars->GC_58, amp[621]); 
  FFV1_0(w[169], w[153], w[0], pars->GC_11, amp[622]); 
  FFV1_0(w[183], w[152], w[0], pars->GC_11, amp[623]); 
  FFV2_5_0(w[202], w[156], w[3], pars->GC_51, pars->GC_58, amp[624]); 
  FFV2_5_0(w[202], w[157], w[2], pars->GC_51, pars->GC_58, amp[625]); 
  FFV2_5_0(w[169], w[203], w[3], pars->GC_51, pars->GC_58, amp[626]); 
  FFV2_5_0(w[170], w[203], w[2], pars->GC_51, pars->GC_58, amp[627]); 
  FFV1_0(w[169], w[157], w[0], pars->GC_11, amp[628]); 
  FFV1_0(w[170], w[156], w[0], pars->GC_11, amp[629]); 
  FFV1_0(w[177], w[196], w[5], pars->GC_11, amp[630]); 
  FFV1_0(w[155], w[196], w[4], pars->GC_11, amp[631]); 
  FFV1_0(w[197], w[174], w[5], pars->GC_11, amp[632]); 
  FFV1_0(w[197], w[137], w[4], pars->GC_11, amp[633]); 
  FFV1_0(w[155], w[174], w[0], pars->GC_11, amp[634]); 
  FFV1_0(w[177], w[137], w[0], pars->GC_11, amp[635]); 
  FFV2_5_0(w[186], w[196], w[3], pars->GC_51, pars->GC_58, amp[636]); 
  FFV1_0(w[159], w[196], w[4], pars->GC_11, amp[637]); 
  FFV2_5_0(w[201], w[174], w[3], pars->GC_51, pars->GC_58, amp[638]); 
  FFV1_0(w[201], w[135], w[4], pars->GC_11, amp[639]); 
  FFV1_0(w[159], w[174], w[0], pars->GC_11, amp[640]); 
  FFV1_0(w[186], w[135], w[0], pars->GC_11, amp[641]); 
  FFV1_0(w[198], w[176], w[5], pars->GC_11, amp[642]); 
  FFV1_0(w[198], w[153], w[4], pars->GC_11, amp[643]); 
  FFV1_0(w[175], w[199], w[5], pars->GC_11, amp[644]); 
  FFV1_0(w[146], w[199], w[4], pars->GC_11, amp[645]); 
  FFV1_0(w[175], w[153], w[0], pars->GC_11, amp[646]); 
  FFV1_0(w[146], w[176], w[0], pars->GC_11, amp[647]); 
  FFV2_5_0(w[198], w[195], w[3], pars->GC_51, pars->GC_58, amp[648]); 
  FFV1_0(w[198], w[157], w[4], pars->GC_11, amp[649]); 
  FFV2_5_0(w[175], w[203], w[3], pars->GC_51, pars->GC_58, amp[650]); 
  FFV1_0(w[144], w[203], w[4], pars->GC_11, amp[651]); 
  FFV1_0(w[175], w[157], w[0], pars->GC_11, amp[652]); 
  FFV1_0(w[144], w[195], w[0], pars->GC_11, amp[653]); 
  FFV2_5_0(w[186], w[199], w[2], pars->GC_51, pars->GC_58, amp[654]); 
  FFV1_0(w[158], w[199], w[4], pars->GC_11, amp[655]); 
  FFV2_5_0(w[201], w[176], w[2], pars->GC_51, pars->GC_58, amp[656]); 
  FFV1_0(w[201], w[152], w[4], pars->GC_11, amp[657]); 
  FFV1_0(w[158], w[176], w[0], pars->GC_11, amp[658]); 
  FFV1_0(w[186], w[152], w[0], pars->GC_11, amp[659]); 
  FFV2_5_0(w[197], w[195], w[2], pars->GC_51, pars->GC_58, amp[660]); 
  FFV1_0(w[197], w[156], w[4], pars->GC_11, amp[661]); 
  FFV2_5_0(w[177], w[203], w[2], pars->GC_51, pars->GC_58, amp[662]); 
  FFV1_0(w[154], w[203], w[4], pars->GC_11, amp[663]); 
  FFV1_0(w[177], w[156], w[0], pars->GC_11, amp[664]); 
  FFV1_0(w[154], w[195], w[0], pars->GC_11, amp[665]); 
  FFV2_5_0(w[204], w[134], w[3], pars->GC_51, pars->GC_58, amp[666]); 
  FFV2_5_0(w[205], w[134], w[3], pars->GC_51, pars->GC_58, amp[667]); 
  FFV2_5_0(w[206], w[134], w[3], pars->GC_51, pars->GC_58, amp[668]); 
  FFV1_0(w[138], w[134], w[83], pars->GC_11, amp[669]); 
  FFV1_0(w[138], w[134], w[84], pars->GC_11, amp[670]); 
  FFV1_0(w[138], w[134], w[85], pars->GC_11, amp[671]); 
  FFV2_5_0(w[143], w[207], w[3], pars->GC_51, pars->GC_58, amp[672]); 
  FFV2_5_0(w[143], w[208], w[3], pars->GC_51, pars->GC_58, amp[673]); 
  FFV2_5_0(w[143], w[209], w[3], pars->GC_51, pars->GC_58, amp[674]); 
  FFV1_0(w[143], w[147], w[83], pars->GC_11, amp[675]); 
  FFV1_0(w[143], w[147], w[84], pars->GC_11, amp[676]); 
  FFV1_0(w[143], w[147], w[85], pars->GC_11, amp[677]); 
  FFV2_5_0(w[204], w[147], w[2], pars->GC_51, pars->GC_58, amp[678]); 
  FFV2_5_0(w[205], w[147], w[2], pars->GC_51, pars->GC_58, amp[679]); 
  FFV2_5_0(w[206], w[147], w[2], pars->GC_51, pars->GC_58, amp[680]); 
  FFV2_5_0(w[138], w[207], w[2], pars->GC_51, pars->GC_58, amp[681]); 
  FFV2_5_0(w[138], w[208], w[2], pars->GC_51, pars->GC_58, amp[682]); 
  FFV2_5_0(w[138], w[209], w[2], pars->GC_51, pars->GC_58, amp[683]); 
  FFV1_0(w[133], w[211], w[9], pars->GC_11, amp[684]); 
  FFV1_0(w[136], w[211], w[5], pars->GC_11, amp[685]); 
  FFV2_3_0(w[136], w[212], w[3], pars->GC_50, pars->GC_58, amp[686]); 
  FFV1_0(w[213], w[214], w[5], pars->GC_11, amp[687]); 
  FFV1_0(w[215], w[210], w[5], pars->GC_11, amp[688]); 
  FFV1_0(w[213], w[210], w[9], pars->GC_11, amp[689]); 
  FFV2_3_0(w[141], w[214], w[3], pars->GC_50, pars->GC_58, amp[690]); 
  FFV2_3_0(w[142], w[210], w[3], pars->GC_50, pars->GC_58, amp[691]); 
  FFV1_0(w[217], w[132], w[9], pars->GC_11, amp[692]); 
  FFV1_0(w[217], w[145], w[5], pars->GC_11, amp[693]); 
  FFV2_3_0(w[218], w[145], w[3], pars->GC_50, pars->GC_58, amp[694]); 
  FFV1_0(w[220], w[219], w[5], pars->GC_11, amp[695]); 
  FFV1_0(w[216], w[221], w[5], pars->GC_11, amp[696]); 
  FFV1_0(w[216], w[219], w[9], pars->GC_11, amp[697]); 
  FFV2_3_0(w[220], w[150], w[3], pars->GC_50, pars->GC_58, amp[698]); 
  FFV2_3_0(w[216], w[151], w[3], pars->GC_50, pars->GC_58, amp[699]); 
  FFV1_0(w[133], w[222], w[9], pars->GC_11, amp[700]); 
  FFV1_0(w[136], w[222], w[5], pars->GC_11, amp[701]); 
  FFV2_3_0(w[136], w[223], w[2], pars->GC_50, pars->GC_58, amp[702]); 
  FFV2_3_0(w[141], w[221], w[2], pars->GC_50, pars->GC_58, amp[703]); 
  FFV2_3_0(w[142], w[219], w[2], pars->GC_50, pars->GC_58, amp[704]); 
  FFV1_0(w[224], w[132], w[9], pars->GC_11, amp[705]); 
  FFV1_0(w[224], w[145], w[5], pars->GC_11, amp[706]); 
  FFV2_3_0(w[225], w[145], w[2], pars->GC_50, pars->GC_58, amp[707]); 
  FFV2_3_0(w[215], w[150], w[2], pars->GC_50, pars->GC_58, amp[708]); 
  FFV2_3_0(w[213], w[151], w[2], pars->GC_50, pars->GC_58, amp[709]); 
  FFV2_3_0(w[136], w[226], w[3], pars->GC_50, pars->GC_58, amp[710]); 
  FFV2_3_0(w[136], w[227], w[2], pars->GC_50, pars->GC_58, amp[711]); 
  FFV2_3_0(w[228], w[145], w[3], pars->GC_50, pars->GC_58, amp[712]); 
  FFV2_3_0(w[229], w[145], w[2], pars->GC_50, pars->GC_58, amp[713]); 
  FFV2_3_0(w[161], w[230], w[3], pars->GC_50, pars->GC_58, amp[714]); 
  FFV2_3_0(w[161], w[231], w[2], pars->GC_50, pars->GC_58, amp[715]); 
  FFV2_3_0(w[216], w[164], w[3], pars->GC_50, pars->GC_58, amp[716]); 
  FFV2_3_0(w[232], w[160], w[3], pars->GC_50, pars->GC_58, amp[717]); 
  FFV2_3_0(w[213], w[164], w[2], pars->GC_50, pars->GC_58, amp[718]); 
  FFV2_3_0(w[233], w[160], w[2], pars->GC_50, pars->GC_58, amp[719]); 
  FFV2_3_0(w[234], w[168], w[3], pars->GC_50, pars->GC_58, amp[720]); 
  FFV2_3_0(w[235], w[168], w[2], pars->GC_50, pars->GC_58, amp[721]); 
  FFV2_3_0(w[171], w[210], w[3], pars->GC_50, pars->GC_58, amp[722]); 
  FFV2_3_0(w[167], w[236], w[3], pars->GC_50, pars->GC_58, amp[723]); 
  FFV2_3_0(w[171], w[219], w[2], pars->GC_50, pars->GC_58, amp[724]); 
  FFV2_3_0(w[167], w[237], w[2], pars->GC_50, pars->GC_58, amp[725]); 
  FFV1_0(w[133], w[211], w[50], pars->GC_11, amp[726]); 
  FFV2_3_0(w[161], w[238], w[3], pars->GC_50, pars->GC_58, amp[727]); 
  FFV1_0(w[161], w[211], w[4], pars->GC_11, amp[728]); 
  FFV1_0(w[213], w[210], w[50], pars->GC_11, amp[729]); 
  FFV1_0(w[213], w[236], w[4], pars->GC_11, amp[730]); 
  FFV1_0(w[233], w[210], w[4], pars->GC_11, amp[731]); 
  FFV1_0(w[217], w[132], w[50], pars->GC_11, amp[732]); 
  FFV2_3_0(w[239], w[168], w[3], pars->GC_50, pars->GC_58, amp[733]); 
  FFV1_0(w[217], w[168], w[4], pars->GC_11, amp[734]); 
  FFV1_0(w[216], w[219], w[50], pars->GC_11, amp[735]); 
  FFV1_0(w[232], w[219], w[4], pars->GC_11, amp[736]); 
  FFV1_0(w[216], w[237], w[4], pars->GC_11, amp[737]); 
  FFV1_0(w[133], w[222], w[50], pars->GC_11, amp[738]); 
  FFV2_3_0(w[161], w[240], w[2], pars->GC_50, pars->GC_58, amp[739]); 
  FFV1_0(w[161], w[222], w[4], pars->GC_11, amp[740]); 
  FFV1_0(w[224], w[132], w[50], pars->GC_11, amp[741]); 
  FFV2_3_0(w[241], w[168], w[2], pars->GC_50, pars->GC_58, amp[742]); 
  FFV1_0(w[224], w[168], w[4], pars->GC_11, amp[743]); 
  FFV2_3_0(w[180], w[242], w[3], pars->GC_50, pars->GC_58, amp[744]); 
  FFV2_3_0(w[180], w[243], w[2], pars->GC_50, pars->GC_58, amp[745]); 
  FFV2_3_0(w[216], w[182], w[3], pars->GC_50, pars->GC_58, amp[746]); 
  FFV1_0(w[216], w[243], w[56], pars->GC_11, amp[747]); 
  FFV2_3_0(w[213], w[182], w[2], pars->GC_50, pars->GC_58, amp[748]); 
  FFV1_0(w[213], w[242], w[56], pars->GC_11, amp[749]); 
  FFV1_0(w[235], w[242], w[5], pars->GC_11, amp[750]); 
  FFV2_3_0(w[183], w[242], w[3], pars->GC_50, pars->GC_58, amp[751]); 
  FFV1_0(w[234], w[243], w[5], pars->GC_11, amp[752]); 
  FFV2_3_0(w[183], w[243], w[2], pars->GC_50, pars->GC_58, amp[753]); 
  FFV2_3_0(w[234], w[184], w[3], pars->GC_50, pars->GC_58, amp[754]); 
  FFV2_3_0(w[235], w[184], w[2], pars->GC_50, pars->GC_58, amp[755]); 
  FFV1_0(w[217], w[185], w[5], pars->GC_11, amp[756]); 
  FFV2_3_0(w[218], w[185], w[3], pars->GC_50, pars->GC_58, amp[757]); 
  FFV1_0(w[239], w[243], w[5], pars->GC_11, amp[758]); 
  FFV1_0(w[218], w[243], w[4], pars->GC_11, amp[759]); 
  FFV2_3_0(w[239], w[184], w[3], pars->GC_50, pars->GC_58, amp[760]); 
  FFV1_0(w[217], w[184], w[4], pars->GC_11, amp[761]); 
  FFV1_0(w[224], w[185], w[5], pars->GC_11, amp[762]); 
  FFV2_3_0(w[225], w[185], w[2], pars->GC_50, pars->GC_58, amp[763]); 
  FFV1_0(w[241], w[242], w[5], pars->GC_11, amp[764]); 
  FFV1_0(w[225], w[242], w[4], pars->GC_11, amp[765]); 
  FFV2_3_0(w[241], w[184], w[2], pars->GC_50, pars->GC_58, amp[766]); 
  FFV1_0(w[224], w[184], w[4], pars->GC_11, amp[767]); 
  FFV2_3_0(w[228], w[185], w[3], pars->GC_50, pars->GC_58, amp[768]); 
  FFV2_3_0(w[229], w[185], w[2], pars->GC_50, pars->GC_58, amp[769]); 
  FFV2_3_0(w[186], w[242], w[3], pars->GC_50, pars->GC_58, amp[770]); 
  FFV1_0(w[229], w[242], w[4], pars->GC_11, amp[771]); 
  FFV2_3_0(w[186], w[243], w[2], pars->GC_50, pars->GC_58, amp[772]); 
  FFV1_0(w[228], w[243], w[4], pars->GC_11, amp[773]); 
  FFV2_3_0(w[244], w[189], w[3], pars->GC_50, pars->GC_58, amp[774]); 
  FFV2_3_0(w[245], w[189], w[2], pars->GC_50, pars->GC_58, amp[775]); 
  FFV2_3_0(w[191], w[210], w[3], pars->GC_50, pars->GC_58, amp[776]); 
  FFV1_0(w[245], w[210], w[56], pars->GC_11, amp[777]); 
  FFV2_3_0(w[191], w[219], w[2], pars->GC_50, pars->GC_58, amp[778]); 
  FFV1_0(w[244], w[219], w[56], pars->GC_11, amp[779]); 
  FFV1_0(w[244], w[231], w[5], pars->GC_11, amp[780]); 
  FFV2_3_0(w[244], w[192], w[3], pars->GC_50, pars->GC_58, amp[781]); 
  FFV1_0(w[245], w[230], w[5], pars->GC_11, amp[782]); 
  FFV2_3_0(w[245], w[192], w[2], pars->GC_50, pars->GC_58, amp[783]); 
  FFV2_3_0(w[193], w[230], w[3], pars->GC_50, pars->GC_58, amp[784]); 
  FFV2_3_0(w[193], w[231], w[2], pars->GC_50, pars->GC_58, amp[785]); 
  FFV1_0(w[194], w[211], w[5], pars->GC_11, amp[786]); 
  FFV2_3_0(w[194], w[212], w[3], pars->GC_50, pars->GC_58, amp[787]); 
  FFV1_0(w[245], w[238], w[5], pars->GC_11, amp[788]); 
  FFV1_0(w[245], w[212], w[4], pars->GC_11, amp[789]); 
  FFV2_3_0(w[193], w[238], w[3], pars->GC_50, pars->GC_58, amp[790]); 
  FFV1_0(w[193], w[211], w[4], pars->GC_11, amp[791]); 
  FFV1_0(w[194], w[222], w[5], pars->GC_11, amp[792]); 
  FFV2_3_0(w[194], w[223], w[2], pars->GC_50, pars->GC_58, amp[793]); 
  FFV1_0(w[244], w[240], w[5], pars->GC_11, amp[794]); 
  FFV1_0(w[244], w[223], w[4], pars->GC_11, amp[795]); 
  FFV2_3_0(w[193], w[240], w[2], pars->GC_50, pars->GC_58, amp[796]); 
  FFV1_0(w[193], w[222], w[4], pars->GC_11, amp[797]); 
  FFV2_3_0(w[194], w[226], w[3], pars->GC_50, pars->GC_58, amp[798]); 
  FFV2_3_0(w[194], w[227], w[2], pars->GC_50, pars->GC_58, amp[799]); 
  FFV2_3_0(w[244], w[195], w[3], pars->GC_50, pars->GC_58, amp[800]); 
  FFV1_0(w[244], w[227], w[4], pars->GC_11, amp[801]); 
  FFV2_3_0(w[245], w[195], w[2], pars->GC_50, pars->GC_58, amp[802]); 
  FFV1_0(w[245], w[226], w[4], pars->GC_11, amp[803]); 
  FFV1_0(w[133], w[211], w[74], pars->GC_11, amp[804]); 
  FFV2_3_0(w[180], w[246], w[3], pars->GC_50, pars->GC_58, amp[805]); 
  FFV1_0(w[180], w[211], w[0], pars->GC_11, amp[806]); 
  FFV1_0(w[213], w[210], w[74], pars->GC_11, amp[807]); 
  FFV1_0(w[213], w[246], w[56], pars->GC_11, amp[808]); 
  FFV1_0(w[247], w[210], w[56], pars->GC_11, amp[809]); 
  FFV1_0(w[217], w[132], w[74], pars->GC_11, amp[810]); 
  FFV2_3_0(w[248], w[189], w[3], pars->GC_50, pars->GC_58, amp[811]); 
  FFV1_0(w[217], w[189], w[0], pars->GC_11, amp[812]); 
  FFV1_0(w[216], w[219], w[74], pars->GC_11, amp[813]); 
  FFV1_0(w[248], w[219], w[56], pars->GC_11, amp[814]); 
  FFV1_0(w[216], w[249], w[56], pars->GC_11, amp[815]); 
  FFV1_0(w[133], w[222], w[74], pars->GC_11, amp[816]); 
  FFV2_3_0(w[180], w[249], w[2], pars->GC_50, pars->GC_58, amp[817]); 
  FFV1_0(w[180], w[222], w[0], pars->GC_11, amp[818]); 
  FFV1_0(w[224], w[132], w[74], pars->GC_11, amp[819]); 
  FFV2_3_0(w[247], w[189], w[2], pars->GC_50, pars->GC_58, amp[820]); 
  FFV1_0(w[224], w[189], w[0], pars->GC_11, amp[821]); 
  FFV1_0(w[217], w[200], w[5], pars->GC_11, amp[822]); 
  FFV2_3_0(w[218], w[200], w[3], pars->GC_50, pars->GC_58, amp[823]); 
  FFV1_0(w[248], w[231], w[5], pars->GC_11, amp[824]); 
  FFV2_3_0(w[248], w[192], w[3], pars->GC_50, pars->GC_58, amp[825]); 
  FFV1_0(w[218], w[231], w[0], pars->GC_11, amp[826]); 
  FFV1_0(w[217], w[192], w[0], pars->GC_11, amp[827]); 
  FFV1_0(w[224], w[200], w[5], pars->GC_11, amp[828]); 
  FFV2_3_0(w[225], w[200], w[2], pars->GC_50, pars->GC_58, amp[829]); 
  FFV1_0(w[247], w[230], w[5], pars->GC_11, amp[830]); 
  FFV2_3_0(w[247], w[192], w[2], pars->GC_50, pars->GC_58, amp[831]); 
  FFV1_0(w[225], w[230], w[0], pars->GC_11, amp[832]); 
  FFV1_0(w[224], w[192], w[0], pars->GC_11, amp[833]); 
  FFV2_3_0(w[228], w[200], w[3], pars->GC_50, pars->GC_58, amp[834]); 
  FFV2_3_0(w[229], w[200], w[2], pars->GC_50, pars->GC_58, amp[835]); 
  FFV2_3_0(w[201], w[230], w[3], pars->GC_50, pars->GC_58, amp[836]); 
  FFV2_3_0(w[201], w[231], w[2], pars->GC_50, pars->GC_58, amp[837]); 
  FFV1_0(w[229], w[230], w[0], pars->GC_11, amp[838]); 
  FFV1_0(w[228], w[231], w[0], pars->GC_11, amp[839]); 
  FFV1_0(w[202], w[211], w[5], pars->GC_11, amp[840]); 
  FFV2_3_0(w[202], w[212], w[3], pars->GC_50, pars->GC_58, amp[841]); 
  FFV1_0(w[235], w[246], w[5], pars->GC_11, amp[842]); 
  FFV2_3_0(w[183], w[246], w[3], pars->GC_50, pars->GC_58, amp[843]); 
  FFV1_0(w[235], w[212], w[0], pars->GC_11, amp[844]); 
  FFV1_0(w[183], w[211], w[0], pars->GC_11, amp[845]); 
  FFV1_0(w[202], w[222], w[5], pars->GC_11, amp[846]); 
  FFV2_3_0(w[202], w[223], w[2], pars->GC_50, pars->GC_58, amp[847]); 
  FFV1_0(w[234], w[249], w[5], pars->GC_11, amp[848]); 
  FFV2_3_0(w[183], w[249], w[2], pars->GC_50, pars->GC_58, amp[849]); 
  FFV1_0(w[234], w[223], w[0], pars->GC_11, amp[850]); 
  FFV1_0(w[183], w[222], w[0], pars->GC_11, amp[851]); 
  FFV2_3_0(w[202], w[226], w[3], pars->GC_50, pars->GC_58, amp[852]); 
  FFV2_3_0(w[202], w[227], w[2], pars->GC_50, pars->GC_58, amp[853]); 
  FFV2_3_0(w[234], w[203], w[3], pars->GC_50, pars->GC_58, amp[854]); 
  FFV2_3_0(w[235], w[203], w[2], pars->GC_50, pars->GC_58, amp[855]); 
  FFV1_0(w[234], w[227], w[0], pars->GC_11, amp[856]); 
  FFV1_0(w[235], w[226], w[0], pars->GC_11, amp[857]); 
  FFV1_0(w[241], w[246], w[5], pars->GC_11, amp[858]); 
  FFV1_0(w[225], w[246], w[4], pars->GC_11, amp[859]); 
  FFV1_0(w[247], w[238], w[5], pars->GC_11, amp[860]); 
  FFV1_0(w[247], w[212], w[4], pars->GC_11, amp[861]); 
  FFV1_0(w[225], w[238], w[0], pars->GC_11, amp[862]); 
  FFV1_0(w[241], w[212], w[0], pars->GC_11, amp[863]); 
  FFV2_3_0(w[186], w[246], w[3], pars->GC_50, pars->GC_58, amp[864]); 
  FFV1_0(w[229], w[246], w[4], pars->GC_11, amp[865]); 
  FFV2_3_0(w[201], w[238], w[3], pars->GC_50, pars->GC_58, amp[866]); 
  FFV1_0(w[201], w[211], w[4], pars->GC_11, amp[867]); 
  FFV1_0(w[229], w[238], w[0], pars->GC_11, amp[868]); 
  FFV1_0(w[186], w[211], w[0], pars->GC_11, amp[869]); 
  FFV1_0(w[248], w[240], w[5], pars->GC_11, amp[870]); 
  FFV1_0(w[248], w[223], w[4], pars->GC_11, amp[871]); 
  FFV1_0(w[239], w[249], w[5], pars->GC_11, amp[872]); 
  FFV1_0(w[218], w[249], w[4], pars->GC_11, amp[873]); 
  FFV1_0(w[239], w[223], w[0], pars->GC_11, amp[874]); 
  FFV1_0(w[218], w[240], w[0], pars->GC_11, amp[875]); 
  FFV2_3_0(w[248], w[195], w[3], pars->GC_50, pars->GC_58, amp[876]); 
  FFV1_0(w[248], w[227], w[4], pars->GC_11, amp[877]); 
  FFV2_3_0(w[239], w[203], w[3], pars->GC_50, pars->GC_58, amp[878]); 
  FFV1_0(w[217], w[203], w[4], pars->GC_11, amp[879]); 
  FFV1_0(w[239], w[227], w[0], pars->GC_11, amp[880]); 
  FFV1_0(w[217], w[195], w[0], pars->GC_11, amp[881]); 
  FFV2_3_0(w[186], w[249], w[2], pars->GC_50, pars->GC_58, amp[882]); 
  FFV1_0(w[228], w[249], w[4], pars->GC_11, amp[883]); 
  FFV2_3_0(w[201], w[240], w[2], pars->GC_50, pars->GC_58, amp[884]); 
  FFV1_0(w[201], w[222], w[4], pars->GC_11, amp[885]); 
  FFV1_0(w[228], w[240], w[0], pars->GC_11, amp[886]); 
  FFV1_0(w[186], w[222], w[0], pars->GC_11, amp[887]); 
  FFV2_3_0(w[247], w[195], w[2], pars->GC_50, pars->GC_58, amp[888]); 
  FFV1_0(w[247], w[226], w[4], pars->GC_11, amp[889]); 
  FFV2_3_0(w[241], w[203], w[2], pars->GC_50, pars->GC_58, amp[890]); 
  FFV1_0(w[224], w[203], w[4], pars->GC_11, amp[891]); 
  FFV1_0(w[241], w[226], w[0], pars->GC_11, amp[892]); 
  FFV1_0(w[224], w[195], w[0], pars->GC_11, amp[893]); 
  FFV2_3_0(w[204], w[210], w[3], pars->GC_50, pars->GC_58, amp[894]); 
  FFV2_3_0(w[205], w[210], w[3], pars->GC_50, pars->GC_58, amp[895]); 
  FFV2_3_0(w[206], w[210], w[3], pars->GC_50, pars->GC_58, amp[896]); 
  FFV1_0(w[213], w[210], w[83], pars->GC_11, amp[897]); 
  FFV1_0(w[213], w[210], w[84], pars->GC_11, amp[898]); 
  FFV1_0(w[213], w[210], w[85], pars->GC_11, amp[899]); 
  FFV2_3_0(w[216], w[207], w[3], pars->GC_50, pars->GC_58, amp[900]); 
  FFV2_3_0(w[216], w[208], w[3], pars->GC_50, pars->GC_58, amp[901]); 
  FFV2_3_0(w[216], w[209], w[3], pars->GC_50, pars->GC_58, amp[902]); 
  FFV1_0(w[216], w[219], w[83], pars->GC_11, amp[903]); 
  FFV1_0(w[216], w[219], w[84], pars->GC_11, amp[904]); 
  FFV1_0(w[216], w[219], w[85], pars->GC_11, amp[905]); 
  FFV2_3_0(w[204], w[219], w[2], pars->GC_50, pars->GC_58, amp[906]); 
  FFV2_3_0(w[205], w[219], w[2], pars->GC_50, pars->GC_58, amp[907]); 
  FFV2_3_0(w[206], w[219], w[2], pars->GC_50, pars->GC_58, amp[908]); 
  FFV2_3_0(w[213], w[207], w[2], pars->GC_50, pars->GC_58, amp[909]); 
  FFV2_3_0(w[213], w[208], w[2], pars->GC_50, pars->GC_58, amp[910]); 
  FFV2_3_0(w[213], w[209], w[2], pars->GC_50, pars->GC_58, amp[911]); 


}
double PY8MEs_R12_P27_sm_gq_zzggq::matrix_12_gu_zzggu() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 228;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = -amp[0] - Complex<double> (0, 1) * amp[3] - amp[5] -
      Complex<double> (0, 1) * amp[6] - Complex<double> (0, 1) * amp[7] -
      amp[8] - Complex<double> (0, 1) * amp[9] - Complex<double> (0, 1) *
      amp[10] - Complex<double> (0, 1) * amp[12] - amp[13] - amp[16] -
      Complex<double> (0, 1) * amp[19] - Complex<double> (0, 1) * amp[20] -
      amp[21] - Complex<double> (0, 1) * amp[22] - Complex<double> (0, 1) *
      amp[23] - Complex<double> (0, 1) * amp[28] - Complex<double> (0, 1) *
      amp[29] - Complex<double> (0, 1) * amp[60] - Complex<double> (0, 1) *
      amp[61] - Complex<double> (0, 1) * amp[62] - Complex<double> (0, 1) *
      amp[63] - Complex<double> (0, 1) * amp[64] - Complex<double> (0, 1) *
      amp[65] + amp[72] + amp[73] + amp[75] + amp[78] + amp[79] + amp[81] +
      amp[84] + amp[85] + amp[86] + amp[87] + amp[88] + amp[89] - amp[120] -
      Complex<double> (0, 1) * amp[121] - Complex<double> (0, 1) * amp[122] -
      amp[123] - Complex<double> (0, 1) * amp[124] - amp[126] - amp[129] -
      Complex<double> (0, 1) * amp[131] - amp[132] - Complex<double> (0, 1) *
      amp[133] - Complex<double> (0, 1) * amp[134] - amp[135] + amp[175] +
      amp[180] + amp[181] + amp[185] + amp[189] + amp[198] + amp[199] +
      amp[203] - amp[210] + amp[212] + amp[215] - amp[213] + amp[218] -
      amp[216] + amp[221] - amp[219] - amp[222] + amp[224] + amp[227] -
      amp[225];
  jamp[1] = -Complex<double> (0, 1) * amp[36] - Complex<double> (0, 1) *
      amp[37] - Complex<double> (0, 1) * amp[38] - Complex<double> (0, 1) *
      amp[39] - Complex<double> (0, 1) * amp[40] - Complex<double> (0, 1) *
      amp[41] - amp[42] - amp[45] - Complex<double> (0, 1) * amp[46] - amp[48]
      - Complex<double> (0, 1) * amp[49] - Complex<double> (0, 1) * amp[50] -
      amp[51] - Complex<double> (0, 1) * amp[53] - amp[54] - amp[57] -
      Complex<double> (0, 1) * amp[58] - Complex<double> (0, 1) * amp[59] +
      Complex<double> (0, 1) * amp[60] + Complex<double> (0, 1) * amp[61] +
      Complex<double> (0, 1) * amp[62] + Complex<double> (0, 1) * amp[63] +
      Complex<double> (0, 1) * amp[64] + Complex<double> (0, 1) * amp[65] +
      amp[66] + amp[67] + amp[68] + amp[69] + amp[70] + amp[71] + amp[74] +
      amp[76] + amp[77] + amp[80] + amp[82] + amp[83] + amp[120] +
      Complex<double> (0, 1) * amp[121] + Complex<double> (0, 1) * amp[122] +
      amp[123] + Complex<double> (0, 1) * amp[124] + amp[126] + amp[129] +
      Complex<double> (0, 1) * amp[131] + amp[132] + Complex<double> (0, 1) *
      amp[133] + Complex<double> (0, 1) * amp[134] + amp[135] + amp[158] +
      amp[159] + amp[161] + amp[164] + amp[165] + amp[167] + amp[174] +
      amp[188] + amp[210] + amp[211] + amp[213] + amp[214] + amp[216] +
      amp[217] + amp[219] + amp[220] + amp[222] + amp[223] + amp[225] +
      amp[226];
  jamp[2] = +amp[0] + Complex<double> (0, 1) * amp[3] + amp[5] +
      Complex<double> (0, 1) * amp[6] + Complex<double> (0, 1) * amp[7] +
      amp[8] + Complex<double> (0, 1) * amp[9] + Complex<double> (0, 1) *
      amp[10] + Complex<double> (0, 1) * amp[12] + amp[13] + amp[16] +
      Complex<double> (0, 1) * amp[19] + Complex<double> (0, 1) * amp[20] +
      amp[21] + Complex<double> (0, 1) * amp[22] + Complex<double> (0, 1) *
      amp[23] + Complex<double> (0, 1) * amp[28] + Complex<double> (0, 1) *
      amp[29] - Complex<double> (0, 1) * amp[30] - Complex<double> (0, 1) *
      amp[31] - Complex<double> (0, 1) * amp[32] - Complex<double> (0, 1) *
      amp[33] - Complex<double> (0, 1) * amp[34] - Complex<double> (0, 1) *
      amp[35] + amp[42] - Complex<double> (0, 1) * amp[43] - Complex<double>
      (0, 1) * amp[44] + amp[45] - Complex<double> (0, 1) * amp[47] + amp[48] +
      amp[51] - Complex<double> (0, 1) * amp[52] + amp[54] - Complex<double>
      (0, 1) * amp[55] - Complex<double> (0, 1) * amp[56] + amp[57] + amp[138]
      + amp[139] + amp[142] + amp[144] + amp[145] + amp[148] + amp[150] +
      amp[151] + amp[152] + amp[153] + amp[154] + amp[155] + amp[178] +
      amp[182] + amp[183] + amp[184] + amp[191] + amp[200] + amp[201] +
      amp[202] - amp[211] - amp[212] - amp[215] - amp[214] - amp[218] -
      amp[217] - amp[221] - amp[220] - amp[223] - amp[224] - amp[227] -
      amp[226];
  jamp[3] = +Complex<double> (0, 1) * amp[30] + Complex<double> (0, 1) *
      amp[31] + Complex<double> (0, 1) * amp[32] + Complex<double> (0, 1) *
      amp[33] + Complex<double> (0, 1) * amp[34] + Complex<double> (0, 1) *
      amp[35] - amp[42] + Complex<double> (0, 1) * amp[43] + Complex<double>
      (0, 1) * amp[44] - amp[45] + Complex<double> (0, 1) * amp[47] - amp[48] -
      amp[51] + Complex<double> (0, 1) * amp[52] - amp[54] + Complex<double>
      (0, 1) * amp[55] + Complex<double> (0, 1) * amp[56] - amp[57] -
      Complex<double> (0, 1) * amp[90] - Complex<double> (0, 1) * amp[91] -
      Complex<double> (0, 1) * amp[92] - Complex<double> (0, 1) * amp[93] -
      Complex<double> (0, 1) * amp[94] - Complex<double> (0, 1) * amp[95] +
      amp[96] + amp[97] + amp[98] + amp[99] + amp[100] + amp[101] + amp[104] +
      amp[106] + amp[107] + amp[110] + amp[112] + amp[113] + amp[120] +
      amp[123] - Complex<double> (0, 1) * amp[125] + amp[126] - Complex<double>
      (0, 1) * amp[127] - Complex<double> (0, 1) * amp[128] + amp[129] -
      Complex<double> (0, 1) * amp[130] + amp[132] + amp[135] - Complex<double>
      (0, 1) * amp[136] - Complex<double> (0, 1) * amp[137] + amp[140] +
      amp[141] + amp[143] + amp[146] + amp[147] + amp[149] + amp[176] +
      amp[186] + amp[210] + amp[211] + amp[213] + amp[214] + amp[216] +
      amp[217] + amp[219] + amp[220] + amp[222] + amp[223] + amp[225] +
      amp[226];
  jamp[4] = +amp[0] - Complex<double> (0, 1) * amp[1] - Complex<double> (0, 1)
      * amp[2] - Complex<double> (0, 1) * amp[4] + amp[5] + amp[8] -
      Complex<double> (0, 1) * amp[11] + amp[13] - Complex<double> (0, 1) *
      amp[14] - Complex<double> (0, 1) * amp[15] + amp[16] - Complex<double>
      (0, 1) * amp[17] - Complex<double> (0, 1) * amp[18] + amp[21] -
      Complex<double> (0, 1) * amp[24] - Complex<double> (0, 1) * amp[25] -
      Complex<double> (0, 1) * amp[26] - Complex<double> (0, 1) * amp[27] +
      Complex<double> (0, 1) * amp[36] + Complex<double> (0, 1) * amp[37] +
      Complex<double> (0, 1) * amp[38] + Complex<double> (0, 1) * amp[39] +
      Complex<double> (0, 1) * amp[40] + Complex<double> (0, 1) * amp[41] +
      amp[42] + amp[45] + Complex<double> (0, 1) * amp[46] + amp[48] +
      Complex<double> (0, 1) * amp[49] + Complex<double> (0, 1) * amp[50] +
      amp[51] + Complex<double> (0, 1) * amp[53] + amp[54] + amp[57] +
      Complex<double> (0, 1) * amp[58] + Complex<double> (0, 1) * amp[59] +
      amp[156] + amp[157] + amp[160] + amp[162] + amp[163] + amp[166] +
      amp[168] + amp[169] + amp[170] + amp[171] + amp[172] + amp[173] +
      amp[179] + amp[190] + amp[194] + amp[195] + amp[196] + amp[206] +
      amp[207] + amp[208] - amp[211] - amp[212] - amp[215] - amp[214] -
      amp[218] - amp[217] - amp[221] - amp[220] - amp[223] - amp[224] -
      amp[227] - amp[226];
  jamp[5] = -amp[0] + Complex<double> (0, 1) * amp[1] + Complex<double> (0, 1)
      * amp[2] + Complex<double> (0, 1) * amp[4] - amp[5] - amp[8] +
      Complex<double> (0, 1) * amp[11] - amp[13] + Complex<double> (0, 1) *
      amp[14] + Complex<double> (0, 1) * amp[15] - amp[16] + Complex<double>
      (0, 1) * amp[17] + Complex<double> (0, 1) * amp[18] - amp[21] +
      Complex<double> (0, 1) * amp[24] + Complex<double> (0, 1) * amp[25] +
      Complex<double> (0, 1) * amp[26] + Complex<double> (0, 1) * amp[27] +
      Complex<double> (0, 1) * amp[90] + Complex<double> (0, 1) * amp[91] +
      Complex<double> (0, 1) * amp[92] + Complex<double> (0, 1) * amp[93] +
      Complex<double> (0, 1) * amp[94] + Complex<double> (0, 1) * amp[95] +
      amp[102] + amp[103] + amp[105] + amp[108] + amp[109] + amp[111] +
      amp[114] + amp[115] + amp[116] + amp[117] + amp[118] + amp[119] -
      amp[120] - amp[123] + Complex<double> (0, 1) * amp[125] - amp[126] +
      Complex<double> (0, 1) * amp[127] + Complex<double> (0, 1) * amp[128] -
      amp[129] + Complex<double> (0, 1) * amp[130] - amp[132] - amp[135] +
      Complex<double> (0, 1) * amp[136] + Complex<double> (0, 1) * amp[137] +
      amp[177] + amp[187] + amp[192] + amp[193] + amp[197] + amp[204] +
      amp[205] + amp[209] - amp[210] + amp[212] + amp[215] - amp[213] +
      amp[218] - amp[216] + amp[221] - amp[219] - amp[222] + amp[224] +
      amp[227] - amp[225];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[0][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P27_sm_gq_zzggq::matrix_12_gd_zzggd() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 228;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = -amp[228] - Complex<double> (0, 1) * amp[231] - amp[233] -
      Complex<double> (0, 1) * amp[234] - Complex<double> (0, 1) * amp[235] -
      amp[236] - Complex<double> (0, 1) * amp[237] - Complex<double> (0, 1) *
      amp[238] - Complex<double> (0, 1) * amp[240] - amp[241] - amp[244] -
      Complex<double> (0, 1) * amp[247] - Complex<double> (0, 1) * amp[248] -
      amp[249] - Complex<double> (0, 1) * amp[250] - Complex<double> (0, 1) *
      amp[251] - Complex<double> (0, 1) * amp[256] - Complex<double> (0, 1) *
      amp[257] - Complex<double> (0, 1) * amp[288] - Complex<double> (0, 1) *
      amp[289] - Complex<double> (0, 1) * amp[290] - Complex<double> (0, 1) *
      amp[291] - Complex<double> (0, 1) * amp[292] - Complex<double> (0, 1) *
      amp[293] + amp[300] + amp[301] + amp[303] + amp[306] + amp[307] +
      amp[309] + amp[312] + amp[313] + amp[314] + amp[315] + amp[316] +
      amp[317] - amp[348] - Complex<double> (0, 1) * amp[349] - Complex<double>
      (0, 1) * amp[350] - amp[351] - Complex<double> (0, 1) * amp[352] -
      amp[354] - amp[357] - Complex<double> (0, 1) * amp[359] - amp[360] -
      Complex<double> (0, 1) * amp[361] - Complex<double> (0, 1) * amp[362] -
      amp[363] + amp[403] + amp[408] + amp[409] + amp[413] + amp[417] +
      amp[426] + amp[427] + amp[431] - amp[438] + amp[440] + amp[443] -
      amp[441] + amp[446] - amp[444] + amp[449] - amp[447] - amp[450] +
      amp[452] + amp[455] - amp[453];
  jamp[1] = -Complex<double> (0, 1) * amp[264] - Complex<double> (0, 1) *
      amp[265] - Complex<double> (0, 1) * amp[266] - Complex<double> (0, 1) *
      amp[267] - Complex<double> (0, 1) * amp[268] - Complex<double> (0, 1) *
      amp[269] - amp[270] - amp[273] - Complex<double> (0, 1) * amp[274] -
      amp[276] - Complex<double> (0, 1) * amp[277] - Complex<double> (0, 1) *
      amp[278] - amp[279] - Complex<double> (0, 1) * amp[281] - amp[282] -
      amp[285] - Complex<double> (0, 1) * amp[286] - Complex<double> (0, 1) *
      amp[287] + Complex<double> (0, 1) * amp[288] + Complex<double> (0, 1) *
      amp[289] + Complex<double> (0, 1) * amp[290] + Complex<double> (0, 1) *
      amp[291] + Complex<double> (0, 1) * amp[292] + Complex<double> (0, 1) *
      amp[293] + amp[294] + amp[295] + amp[296] + amp[297] + amp[298] +
      amp[299] + amp[302] + amp[304] + amp[305] + amp[308] + amp[310] +
      amp[311] + amp[348] + Complex<double> (0, 1) * amp[349] + Complex<double>
      (0, 1) * amp[350] + amp[351] + Complex<double> (0, 1) * amp[352] +
      amp[354] + amp[357] + Complex<double> (0, 1) * amp[359] + amp[360] +
      Complex<double> (0, 1) * amp[361] + Complex<double> (0, 1) * amp[362] +
      amp[363] + amp[386] + amp[387] + amp[389] + amp[392] + amp[393] +
      amp[395] + amp[402] + amp[416] + amp[438] + amp[439] + amp[441] +
      amp[442] + amp[444] + amp[445] + amp[447] + amp[448] + amp[450] +
      amp[451] + amp[453] + amp[454];
  jamp[2] = +amp[228] + Complex<double> (0, 1) * amp[231] + amp[233] +
      Complex<double> (0, 1) * amp[234] + Complex<double> (0, 1) * amp[235] +
      amp[236] + Complex<double> (0, 1) * amp[237] + Complex<double> (0, 1) *
      amp[238] + Complex<double> (0, 1) * amp[240] + amp[241] + amp[244] +
      Complex<double> (0, 1) * amp[247] + Complex<double> (0, 1) * amp[248] +
      amp[249] + Complex<double> (0, 1) * amp[250] + Complex<double> (0, 1) *
      amp[251] + Complex<double> (0, 1) * amp[256] + Complex<double> (0, 1) *
      amp[257] - Complex<double> (0, 1) * amp[258] - Complex<double> (0, 1) *
      amp[259] - Complex<double> (0, 1) * amp[260] - Complex<double> (0, 1) *
      amp[261] - Complex<double> (0, 1) * amp[262] - Complex<double> (0, 1) *
      amp[263] + amp[270] - Complex<double> (0, 1) * amp[271] - Complex<double>
      (0, 1) * amp[272] + amp[273] - Complex<double> (0, 1) * amp[275] +
      amp[276] + amp[279] - Complex<double> (0, 1) * amp[280] + amp[282] -
      Complex<double> (0, 1) * amp[283] - Complex<double> (0, 1) * amp[284] +
      amp[285] + amp[366] + amp[367] + amp[370] + amp[372] + amp[373] +
      amp[376] + amp[378] + amp[379] + amp[380] + amp[381] + amp[382] +
      amp[383] + amp[406] + amp[410] + amp[411] + amp[412] + amp[419] +
      amp[428] + amp[429] + amp[430] - amp[439] - amp[440] - amp[443] -
      amp[442] - amp[446] - amp[445] - amp[449] - amp[448] - amp[451] -
      amp[452] - amp[455] - amp[454];
  jamp[3] = +Complex<double> (0, 1) * amp[258] + Complex<double> (0, 1) *
      amp[259] + Complex<double> (0, 1) * amp[260] + Complex<double> (0, 1) *
      amp[261] + Complex<double> (0, 1) * amp[262] + Complex<double> (0, 1) *
      amp[263] - amp[270] + Complex<double> (0, 1) * amp[271] + Complex<double>
      (0, 1) * amp[272] - amp[273] + Complex<double> (0, 1) * amp[275] -
      amp[276] - amp[279] + Complex<double> (0, 1) * amp[280] - amp[282] +
      Complex<double> (0, 1) * amp[283] + Complex<double> (0, 1) * amp[284] -
      amp[285] - Complex<double> (0, 1) * amp[318] - Complex<double> (0, 1) *
      amp[319] - Complex<double> (0, 1) * amp[320] - Complex<double> (0, 1) *
      amp[321] - Complex<double> (0, 1) * amp[322] - Complex<double> (0, 1) *
      amp[323] + amp[324] + amp[325] + amp[326] + amp[327] + amp[328] +
      amp[329] + amp[332] + amp[334] + amp[335] + amp[338] + amp[340] +
      amp[341] + amp[348] + amp[351] - Complex<double> (0, 1) * amp[353] +
      amp[354] - Complex<double> (0, 1) * amp[355] - Complex<double> (0, 1) *
      amp[356] + amp[357] - Complex<double> (0, 1) * amp[358] + amp[360] +
      amp[363] - Complex<double> (0, 1) * amp[364] - Complex<double> (0, 1) *
      amp[365] + amp[368] + amp[369] + amp[371] + amp[374] + amp[375] +
      amp[377] + amp[404] + amp[414] + amp[438] + amp[439] + amp[441] +
      amp[442] + amp[444] + amp[445] + amp[447] + amp[448] + amp[450] +
      amp[451] + amp[453] + amp[454];
  jamp[4] = +amp[228] - Complex<double> (0, 1) * amp[229] - Complex<double> (0,
      1) * amp[230] - Complex<double> (0, 1) * amp[232] + amp[233] + amp[236] -
      Complex<double> (0, 1) * amp[239] + amp[241] - Complex<double> (0, 1) *
      amp[242] - Complex<double> (0, 1) * amp[243] + amp[244] - Complex<double>
      (0, 1) * amp[245] - Complex<double> (0, 1) * amp[246] + amp[249] -
      Complex<double> (0, 1) * amp[252] - Complex<double> (0, 1) * amp[253] -
      Complex<double> (0, 1) * amp[254] - Complex<double> (0, 1) * amp[255] +
      Complex<double> (0, 1) * amp[264] + Complex<double> (0, 1) * amp[265] +
      Complex<double> (0, 1) * amp[266] + Complex<double> (0, 1) * amp[267] +
      Complex<double> (0, 1) * amp[268] + Complex<double> (0, 1) * amp[269] +
      amp[270] + amp[273] + Complex<double> (0, 1) * amp[274] + amp[276] +
      Complex<double> (0, 1) * amp[277] + Complex<double> (0, 1) * amp[278] +
      amp[279] + Complex<double> (0, 1) * amp[281] + amp[282] + amp[285] +
      Complex<double> (0, 1) * amp[286] + Complex<double> (0, 1) * amp[287] +
      amp[384] + amp[385] + amp[388] + amp[390] + amp[391] + amp[394] +
      amp[396] + amp[397] + amp[398] + amp[399] + amp[400] + amp[401] +
      amp[407] + amp[418] + amp[422] + amp[423] + amp[424] + amp[434] +
      amp[435] + amp[436] - amp[439] - amp[440] - amp[443] - amp[442] -
      amp[446] - amp[445] - amp[449] - amp[448] - amp[451] - amp[452] -
      amp[455] - amp[454];
  jamp[5] = -amp[228] + Complex<double> (0, 1) * amp[229] + Complex<double> (0,
      1) * amp[230] + Complex<double> (0, 1) * amp[232] - amp[233] - amp[236] +
      Complex<double> (0, 1) * amp[239] - amp[241] + Complex<double> (0, 1) *
      amp[242] + Complex<double> (0, 1) * amp[243] - amp[244] + Complex<double>
      (0, 1) * amp[245] + Complex<double> (0, 1) * amp[246] - amp[249] +
      Complex<double> (0, 1) * amp[252] + Complex<double> (0, 1) * amp[253] +
      Complex<double> (0, 1) * amp[254] + Complex<double> (0, 1) * amp[255] +
      Complex<double> (0, 1) * amp[318] + Complex<double> (0, 1) * amp[319] +
      Complex<double> (0, 1) * amp[320] + Complex<double> (0, 1) * amp[321] +
      Complex<double> (0, 1) * amp[322] + Complex<double> (0, 1) * amp[323] +
      amp[330] + amp[331] + amp[333] + amp[336] + amp[337] + amp[339] +
      amp[342] + amp[343] + amp[344] + amp[345] + amp[346] + amp[347] -
      amp[348] - amp[351] + Complex<double> (0, 1) * amp[353] - amp[354] +
      Complex<double> (0, 1) * amp[355] + Complex<double> (0, 1) * amp[356] -
      amp[357] + Complex<double> (0, 1) * amp[358] - amp[360] - amp[363] +
      Complex<double> (0, 1) * amp[364] + Complex<double> (0, 1) * amp[365] +
      amp[405] + amp[415] + amp[420] + amp[421] + amp[425] + amp[432] +
      amp[433] + amp[437] - amp[438] + amp[440] + amp[443] - amp[441] +
      amp[446] - amp[444] + amp[449] - amp[447] - amp[450] + amp[452] +
      amp[455] - amp[453];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[1][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P27_sm_gq_zzggq::matrix_12_gux_zzggux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 228;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = +amp[456] + Complex<double> (0, 1) * amp[459] + amp[461] +
      Complex<double> (0, 1) * amp[462] + Complex<double> (0, 1) * amp[463] +
      amp[464] + Complex<double> (0, 1) * amp[465] + Complex<double> (0, 1) *
      amp[466] + Complex<double> (0, 1) * amp[468] + amp[469] + amp[472] +
      Complex<double> (0, 1) * amp[475] + Complex<double> (0, 1) * amp[476] +
      amp[477] + Complex<double> (0, 1) * amp[478] + Complex<double> (0, 1) *
      amp[479] + Complex<double> (0, 1) * amp[484] + Complex<double> (0, 1) *
      amp[485] + Complex<double> (0, 1) * amp[516] + Complex<double> (0, 1) *
      amp[517] + Complex<double> (0, 1) * amp[518] + Complex<double> (0, 1) *
      amp[519] + Complex<double> (0, 1) * amp[520] + Complex<double> (0, 1) *
      amp[521] - amp[528] - amp[529] - amp[531] - amp[534] - amp[535] -
      amp[537] - amp[540] - amp[541] - amp[542] - amp[543] - amp[544] -
      amp[545] + amp[576] + Complex<double> (0, 1) * amp[577] + Complex<double>
      (0, 1) * amp[578] + amp[579] + Complex<double> (0, 1) * amp[580] +
      amp[582] + amp[585] + Complex<double> (0, 1) * amp[587] + amp[588] +
      Complex<double> (0, 1) * amp[589] + Complex<double> (0, 1) * amp[590] +
      amp[591] - amp[631] - amp[636] - amp[637] - amp[641] - amp[645] -
      amp[654] - amp[655] - amp[659] + amp[666] - amp[668] - amp[671] +
      amp[669] - amp[674] + amp[672] - amp[677] + amp[675] + amp[678] -
      amp[680] - amp[683] + amp[681];
  jamp[1] = +Complex<double> (0, 1) * amp[492] + Complex<double> (0, 1) *
      amp[493] + Complex<double> (0, 1) * amp[494] + Complex<double> (0, 1) *
      amp[495] + Complex<double> (0, 1) * amp[496] + Complex<double> (0, 1) *
      amp[497] + amp[498] + amp[501] + Complex<double> (0, 1) * amp[502] +
      amp[504] + Complex<double> (0, 1) * amp[505] + Complex<double> (0, 1) *
      amp[506] + amp[507] + Complex<double> (0, 1) * amp[509] + amp[510] +
      amp[513] + Complex<double> (0, 1) * amp[514] + Complex<double> (0, 1) *
      amp[515] - Complex<double> (0, 1) * amp[516] - Complex<double> (0, 1) *
      amp[517] - Complex<double> (0, 1) * amp[518] - Complex<double> (0, 1) *
      amp[519] - Complex<double> (0, 1) * amp[520] - Complex<double> (0, 1) *
      amp[521] - amp[522] - amp[523] - amp[524] - amp[525] - amp[526] -
      amp[527] - amp[530] - amp[532] - amp[533] - amp[536] - amp[538] -
      amp[539] - amp[576] - Complex<double> (0, 1) * amp[577] - Complex<double>
      (0, 1) * amp[578] - amp[579] - Complex<double> (0, 1) * amp[580] -
      amp[582] - amp[585] - Complex<double> (0, 1) * amp[587] - amp[588] -
      Complex<double> (0, 1) * amp[589] - Complex<double> (0, 1) * amp[590] -
      amp[591] - amp[614] - amp[615] - amp[617] - amp[620] - amp[621] -
      amp[623] - amp[630] - amp[644] - amp[666] - amp[667] - amp[669] -
      amp[670] - amp[672] - amp[673] - amp[675] - amp[676] - amp[678] -
      amp[679] - amp[681] - amp[682];
  jamp[2] = -amp[456] - Complex<double> (0, 1) * amp[459] - amp[461] -
      Complex<double> (0, 1) * amp[462] - Complex<double> (0, 1) * amp[463] -
      amp[464] - Complex<double> (0, 1) * amp[465] - Complex<double> (0, 1) *
      amp[466] - Complex<double> (0, 1) * amp[468] - amp[469] - amp[472] -
      Complex<double> (0, 1) * amp[475] - Complex<double> (0, 1) * amp[476] -
      amp[477] - Complex<double> (0, 1) * amp[478] - Complex<double> (0, 1) *
      amp[479] - Complex<double> (0, 1) * amp[484] - Complex<double> (0, 1) *
      amp[485] + Complex<double> (0, 1) * amp[486] + Complex<double> (0, 1) *
      amp[487] + Complex<double> (0, 1) * amp[488] + Complex<double> (0, 1) *
      amp[489] + Complex<double> (0, 1) * amp[490] + Complex<double> (0, 1) *
      amp[491] - amp[498] + Complex<double> (0, 1) * amp[499] + Complex<double>
      (0, 1) * amp[500] - amp[501] + Complex<double> (0, 1) * amp[503] -
      amp[504] - amp[507] + Complex<double> (0, 1) * amp[508] - amp[510] +
      Complex<double> (0, 1) * amp[511] + Complex<double> (0, 1) * amp[512] -
      amp[513] - amp[594] - amp[595] - amp[598] - amp[600] - amp[601] -
      amp[604] - amp[606] - amp[607] - amp[608] - amp[609] - amp[610] -
      amp[611] - amp[634] - amp[638] - amp[639] - amp[640] - amp[647] -
      amp[656] - amp[657] - amp[658] + amp[667] + amp[668] + amp[671] +
      amp[670] + amp[674] + amp[673] + amp[677] + amp[676] + amp[679] +
      amp[680] + amp[683] + amp[682];
  jamp[3] = -Complex<double> (0, 1) * amp[486] - Complex<double> (0, 1) *
      amp[487] - Complex<double> (0, 1) * amp[488] - Complex<double> (0, 1) *
      amp[489] - Complex<double> (0, 1) * amp[490] - Complex<double> (0, 1) *
      amp[491] + amp[498] - Complex<double> (0, 1) * amp[499] - Complex<double>
      (0, 1) * amp[500] + amp[501] - Complex<double> (0, 1) * amp[503] +
      amp[504] + amp[507] - Complex<double> (0, 1) * amp[508] + amp[510] -
      Complex<double> (0, 1) * amp[511] - Complex<double> (0, 1) * amp[512] +
      amp[513] + Complex<double> (0, 1) * amp[546] + Complex<double> (0, 1) *
      amp[547] + Complex<double> (0, 1) * amp[548] + Complex<double> (0, 1) *
      amp[549] + Complex<double> (0, 1) * amp[550] + Complex<double> (0, 1) *
      amp[551] - amp[552] - amp[553] - amp[554] - amp[555] - amp[556] -
      amp[557] - amp[560] - amp[562] - amp[563] - amp[566] - amp[568] -
      amp[569] - amp[576] - amp[579] + Complex<double> (0, 1) * amp[581] -
      amp[582] + Complex<double> (0, 1) * amp[583] + Complex<double> (0, 1) *
      amp[584] - amp[585] + Complex<double> (0, 1) * amp[586] - amp[588] -
      amp[591] + Complex<double> (0, 1) * amp[592] + Complex<double> (0, 1) *
      amp[593] - amp[596] - amp[597] - amp[599] - amp[602] - amp[603] -
      amp[605] - amp[632] - amp[642] - amp[666] - amp[667] - amp[669] -
      amp[670] - amp[672] - amp[673] - amp[675] - amp[676] - amp[678] -
      amp[679] - amp[681] - amp[682];
  jamp[4] = -amp[456] + Complex<double> (0, 1) * amp[457] + Complex<double> (0,
      1) * amp[458] + Complex<double> (0, 1) * amp[460] - amp[461] - amp[464] +
      Complex<double> (0, 1) * amp[467] - amp[469] + Complex<double> (0, 1) *
      amp[470] + Complex<double> (0, 1) * amp[471] - amp[472] + Complex<double>
      (0, 1) * amp[473] + Complex<double> (0, 1) * amp[474] - amp[477] +
      Complex<double> (0, 1) * amp[480] + Complex<double> (0, 1) * amp[481] +
      Complex<double> (0, 1) * amp[482] + Complex<double> (0, 1) * amp[483] -
      Complex<double> (0, 1) * amp[492] - Complex<double> (0, 1) * amp[493] -
      Complex<double> (0, 1) * amp[494] - Complex<double> (0, 1) * amp[495] -
      Complex<double> (0, 1) * amp[496] - Complex<double> (0, 1) * amp[497] -
      amp[498] - amp[501] - Complex<double> (0, 1) * amp[502] - amp[504] -
      Complex<double> (0, 1) * amp[505] - Complex<double> (0, 1) * amp[506] -
      amp[507] - Complex<double> (0, 1) * amp[509] - amp[510] - amp[513] -
      Complex<double> (0, 1) * amp[514] - Complex<double> (0, 1) * amp[515] -
      amp[612] - amp[613] - amp[616] - amp[618] - amp[619] - amp[622] -
      amp[624] - amp[625] - amp[626] - amp[627] - amp[628] - amp[629] -
      amp[635] - amp[646] - amp[650] - amp[651] - amp[652] - amp[662] -
      amp[663] - amp[664] + amp[667] + amp[668] + amp[671] + amp[670] +
      amp[674] + amp[673] + amp[677] + amp[676] + amp[679] + amp[680] +
      amp[683] + amp[682];
  jamp[5] = +amp[456] - Complex<double> (0, 1) * amp[457] - Complex<double> (0,
      1) * amp[458] - Complex<double> (0, 1) * amp[460] + amp[461] + amp[464] -
      Complex<double> (0, 1) * amp[467] + amp[469] - Complex<double> (0, 1) *
      amp[470] - Complex<double> (0, 1) * amp[471] + amp[472] - Complex<double>
      (0, 1) * amp[473] - Complex<double> (0, 1) * amp[474] + amp[477] -
      Complex<double> (0, 1) * amp[480] - Complex<double> (0, 1) * amp[481] -
      Complex<double> (0, 1) * amp[482] - Complex<double> (0, 1) * amp[483] -
      Complex<double> (0, 1) * amp[546] - Complex<double> (0, 1) * amp[547] -
      Complex<double> (0, 1) * amp[548] - Complex<double> (0, 1) * amp[549] -
      Complex<double> (0, 1) * amp[550] - Complex<double> (0, 1) * amp[551] -
      amp[558] - amp[559] - amp[561] - amp[564] - amp[565] - amp[567] -
      amp[570] - amp[571] - amp[572] - amp[573] - amp[574] - amp[575] +
      amp[576] + amp[579] - Complex<double> (0, 1) * amp[581] + amp[582] -
      Complex<double> (0, 1) * amp[583] - Complex<double> (0, 1) * amp[584] +
      amp[585] - Complex<double> (0, 1) * amp[586] + amp[588] + amp[591] -
      Complex<double> (0, 1) * amp[592] - Complex<double> (0, 1) * amp[593] -
      amp[633] - amp[643] - amp[648] - amp[649] - amp[653] - amp[660] -
      amp[661] - amp[665] + amp[666] - amp[668] - amp[671] + amp[669] -
      amp[674] + amp[672] - amp[677] + amp[675] + amp[678] - amp[680] -
      amp[683] + amp[681];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[2][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P27_sm_gq_zzggq::matrix_12_gdx_zzggdx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 228;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = +amp[684] + Complex<double> (0, 1) * amp[687] + amp[689] +
      Complex<double> (0, 1) * amp[690] + Complex<double> (0, 1) * amp[691] +
      amp[692] + Complex<double> (0, 1) * amp[693] + Complex<double> (0, 1) *
      amp[694] + Complex<double> (0, 1) * amp[696] + amp[697] + amp[700] +
      Complex<double> (0, 1) * amp[703] + Complex<double> (0, 1) * amp[704] +
      amp[705] + Complex<double> (0, 1) * amp[706] + Complex<double> (0, 1) *
      amp[707] + Complex<double> (0, 1) * amp[712] + Complex<double> (0, 1) *
      amp[713] + Complex<double> (0, 1) * amp[744] + Complex<double> (0, 1) *
      amp[745] + Complex<double> (0, 1) * amp[746] + Complex<double> (0, 1) *
      amp[747] + Complex<double> (0, 1) * amp[748] + Complex<double> (0, 1) *
      amp[749] - amp[756] - amp[757] - amp[759] - amp[762] - amp[763] -
      amp[765] - amp[768] - amp[769] - amp[770] - amp[771] - amp[772] -
      amp[773] + amp[804] + Complex<double> (0, 1) * amp[805] + Complex<double>
      (0, 1) * amp[806] + amp[807] + Complex<double> (0, 1) * amp[808] +
      amp[810] + amp[813] + Complex<double> (0, 1) * amp[815] + amp[816] +
      Complex<double> (0, 1) * amp[817] + Complex<double> (0, 1) * amp[818] +
      amp[819] - amp[859] - amp[864] - amp[865] - amp[869] - amp[873] -
      amp[882] - amp[883] - amp[887] + amp[894] - amp[896] - amp[899] +
      amp[897] - amp[902] + amp[900] - amp[905] + amp[903] + amp[906] -
      amp[908] - amp[911] + amp[909];
  jamp[1] = +Complex<double> (0, 1) * amp[720] + Complex<double> (0, 1) *
      amp[721] + Complex<double> (0, 1) * amp[722] + Complex<double> (0, 1) *
      amp[723] + Complex<double> (0, 1) * amp[724] + Complex<double> (0, 1) *
      amp[725] + amp[726] + amp[729] + Complex<double> (0, 1) * amp[730] +
      amp[732] + Complex<double> (0, 1) * amp[733] + Complex<double> (0, 1) *
      amp[734] + amp[735] + Complex<double> (0, 1) * amp[737] + amp[738] +
      amp[741] + Complex<double> (0, 1) * amp[742] + Complex<double> (0, 1) *
      amp[743] - Complex<double> (0, 1) * amp[744] - Complex<double> (0, 1) *
      amp[745] - Complex<double> (0, 1) * amp[746] - Complex<double> (0, 1) *
      amp[747] - Complex<double> (0, 1) * amp[748] - Complex<double> (0, 1) *
      amp[749] - amp[750] - amp[751] - amp[752] - amp[753] - amp[754] -
      amp[755] - amp[758] - amp[760] - amp[761] - amp[764] - amp[766] -
      amp[767] - amp[804] - Complex<double> (0, 1) * amp[805] - Complex<double>
      (0, 1) * amp[806] - amp[807] - Complex<double> (0, 1) * amp[808] -
      amp[810] - amp[813] - Complex<double> (0, 1) * amp[815] - amp[816] -
      Complex<double> (0, 1) * amp[817] - Complex<double> (0, 1) * amp[818] -
      amp[819] - amp[842] - amp[843] - amp[845] - amp[848] - amp[849] -
      amp[851] - amp[858] - amp[872] - amp[894] - amp[895] - amp[897] -
      amp[898] - amp[900] - amp[901] - amp[903] - amp[904] - amp[906] -
      amp[907] - amp[909] - amp[910];
  jamp[2] = -amp[684] - Complex<double> (0, 1) * amp[687] - amp[689] -
      Complex<double> (0, 1) * amp[690] - Complex<double> (0, 1) * amp[691] -
      amp[692] - Complex<double> (0, 1) * amp[693] - Complex<double> (0, 1) *
      amp[694] - Complex<double> (0, 1) * amp[696] - amp[697] - amp[700] -
      Complex<double> (0, 1) * amp[703] - Complex<double> (0, 1) * amp[704] -
      amp[705] - Complex<double> (0, 1) * amp[706] - Complex<double> (0, 1) *
      amp[707] - Complex<double> (0, 1) * amp[712] - Complex<double> (0, 1) *
      amp[713] + Complex<double> (0, 1) * amp[714] + Complex<double> (0, 1) *
      amp[715] + Complex<double> (0, 1) * amp[716] + Complex<double> (0, 1) *
      amp[717] + Complex<double> (0, 1) * amp[718] + Complex<double> (0, 1) *
      amp[719] - amp[726] + Complex<double> (0, 1) * amp[727] + Complex<double>
      (0, 1) * amp[728] - amp[729] + Complex<double> (0, 1) * amp[731] -
      amp[732] - amp[735] + Complex<double> (0, 1) * amp[736] - amp[738] +
      Complex<double> (0, 1) * amp[739] + Complex<double> (0, 1) * amp[740] -
      amp[741] - amp[822] - amp[823] - amp[826] - amp[828] - amp[829] -
      amp[832] - amp[834] - amp[835] - amp[836] - amp[837] - amp[838] -
      amp[839] - amp[862] - amp[866] - amp[867] - amp[868] - amp[875] -
      amp[884] - amp[885] - amp[886] + amp[895] + amp[896] + amp[899] +
      amp[898] + amp[902] + amp[901] + amp[905] + amp[904] + amp[907] +
      amp[908] + amp[911] + amp[910];
  jamp[3] = -Complex<double> (0, 1) * amp[714] - Complex<double> (0, 1) *
      amp[715] - Complex<double> (0, 1) * amp[716] - Complex<double> (0, 1) *
      amp[717] - Complex<double> (0, 1) * amp[718] - Complex<double> (0, 1) *
      amp[719] + amp[726] - Complex<double> (0, 1) * amp[727] - Complex<double>
      (0, 1) * amp[728] + amp[729] - Complex<double> (0, 1) * amp[731] +
      amp[732] + amp[735] - Complex<double> (0, 1) * amp[736] + amp[738] -
      Complex<double> (0, 1) * amp[739] - Complex<double> (0, 1) * amp[740] +
      amp[741] + Complex<double> (0, 1) * amp[774] + Complex<double> (0, 1) *
      amp[775] + Complex<double> (0, 1) * amp[776] + Complex<double> (0, 1) *
      amp[777] + Complex<double> (0, 1) * amp[778] + Complex<double> (0, 1) *
      amp[779] - amp[780] - amp[781] - amp[782] - amp[783] - amp[784] -
      amp[785] - amp[788] - amp[790] - amp[791] - amp[794] - amp[796] -
      amp[797] - amp[804] - amp[807] + Complex<double> (0, 1) * amp[809] -
      amp[810] + Complex<double> (0, 1) * amp[811] + Complex<double> (0, 1) *
      amp[812] - amp[813] + Complex<double> (0, 1) * amp[814] - amp[816] -
      amp[819] + Complex<double> (0, 1) * amp[820] + Complex<double> (0, 1) *
      amp[821] - amp[824] - amp[825] - amp[827] - amp[830] - amp[831] -
      amp[833] - amp[860] - amp[870] - amp[894] - amp[895] - amp[897] -
      amp[898] - amp[900] - amp[901] - amp[903] - amp[904] - amp[906] -
      amp[907] - amp[909] - amp[910];
  jamp[4] = -amp[684] + Complex<double> (0, 1) * amp[685] + Complex<double> (0,
      1) * amp[686] + Complex<double> (0, 1) * amp[688] - amp[689] - amp[692] +
      Complex<double> (0, 1) * amp[695] - amp[697] + Complex<double> (0, 1) *
      amp[698] + Complex<double> (0, 1) * amp[699] - amp[700] + Complex<double>
      (0, 1) * amp[701] + Complex<double> (0, 1) * amp[702] - amp[705] +
      Complex<double> (0, 1) * amp[708] + Complex<double> (0, 1) * amp[709] +
      Complex<double> (0, 1) * amp[710] + Complex<double> (0, 1) * amp[711] -
      Complex<double> (0, 1) * amp[720] - Complex<double> (0, 1) * amp[721] -
      Complex<double> (0, 1) * amp[722] - Complex<double> (0, 1) * amp[723] -
      Complex<double> (0, 1) * amp[724] - Complex<double> (0, 1) * amp[725] -
      amp[726] - amp[729] - Complex<double> (0, 1) * amp[730] - amp[732] -
      Complex<double> (0, 1) * amp[733] - Complex<double> (0, 1) * amp[734] -
      amp[735] - Complex<double> (0, 1) * amp[737] - amp[738] - amp[741] -
      Complex<double> (0, 1) * amp[742] - Complex<double> (0, 1) * amp[743] -
      amp[840] - amp[841] - amp[844] - amp[846] - amp[847] - amp[850] -
      amp[852] - amp[853] - amp[854] - amp[855] - amp[856] - amp[857] -
      amp[863] - amp[874] - amp[878] - amp[879] - amp[880] - amp[890] -
      amp[891] - amp[892] + amp[895] + amp[896] + amp[899] + amp[898] +
      amp[902] + amp[901] + amp[905] + amp[904] + amp[907] + amp[908] +
      amp[911] + amp[910];
  jamp[5] = +amp[684] - Complex<double> (0, 1) * amp[685] - Complex<double> (0,
      1) * amp[686] - Complex<double> (0, 1) * amp[688] + amp[689] + amp[692] -
      Complex<double> (0, 1) * amp[695] + amp[697] - Complex<double> (0, 1) *
      amp[698] - Complex<double> (0, 1) * amp[699] + amp[700] - Complex<double>
      (0, 1) * amp[701] - Complex<double> (0, 1) * amp[702] + amp[705] -
      Complex<double> (0, 1) * amp[708] - Complex<double> (0, 1) * amp[709] -
      Complex<double> (0, 1) * amp[710] - Complex<double> (0, 1) * amp[711] -
      Complex<double> (0, 1) * amp[774] - Complex<double> (0, 1) * amp[775] -
      Complex<double> (0, 1) * amp[776] - Complex<double> (0, 1) * amp[777] -
      Complex<double> (0, 1) * amp[778] - Complex<double> (0, 1) * amp[779] -
      amp[786] - amp[787] - amp[789] - amp[792] - amp[793] - amp[795] -
      amp[798] - amp[799] - amp[800] - amp[801] - amp[802] - amp[803] +
      amp[804] + amp[807] - Complex<double> (0, 1) * amp[809] + amp[810] -
      Complex<double> (0, 1) * amp[811] - Complex<double> (0, 1) * amp[812] +
      amp[813] - Complex<double> (0, 1) * amp[814] + amp[816] + amp[819] -
      Complex<double> (0, 1) * amp[820] - Complex<double> (0, 1) * amp[821] -
      amp[861] - amp[871] - amp[876] - amp[877] - amp[881] - amp[888] -
      amp[889] - amp[893] + amp[894] - amp[896] - amp[899] + amp[897] -
      amp[902] + amp[900] - amp[905] + amp[903] + amp[906] - amp[908] -
      amp[911] + amp[909];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[3][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}


}  // end namespace PY8MEs_namespace

