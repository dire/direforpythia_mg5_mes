//==========================================================================
// This file has been automatically generated for Pythia 8 by
// MadGraph5_aMC@NLO v. 2.6.0, 2017-08-16
// By the MadGraph5_aMC@NLO Development Team
// Visit launchpad.net/madgraph5 and amcatnlo.web.cern.ch
//==========================================================================

#include "PY8MEs_R12_P34_sm_gq_wpzqqq.h"
#include "HelAmps_sm.h"

using namespace Pythia8_sm; 

namespace PY8MEs_namespace 
{
//==========================================================================
// Class member functions for calculating the matrix elements for
// Process: g u > w+ z u d u~ WEIGHTED<=7 @12
// Process: g c > w+ z c s c~ WEIGHTED<=7 @12
// Process: g u > w+ z d d d~ WEIGHTED<=7 @12
// Process: g c > w+ z s s s~ WEIGHTED<=7 @12
// Process: g d > w+ z d d u~ WEIGHTED<=7 @12
// Process: g s > w+ z s s c~ WEIGHTED<=7 @12
// Process: g u~ > w+ z d u~ u~ WEIGHTED<=7 @12
// Process: g c~ > w+ z s c~ c~ WEIGHTED<=7 @12
// Process: g d~ > w+ z u u~ u~ WEIGHTED<=7 @12
// Process: g s~ > w+ z c c~ c~ WEIGHTED<=7 @12
// Process: g d~ > w+ z d u~ d~ WEIGHTED<=7 @12
// Process: g s~ > w+ z s c~ s~ WEIGHTED<=7 @12
// Process: g u > w+ z u s c~ WEIGHTED<=7 @12
// Process: g c > w+ z c d u~ WEIGHTED<=7 @12
// Process: g u > w+ z c d c~ WEIGHTED<=7 @12
// Process: g c > w+ z u s u~ WEIGHTED<=7 @12
// Process: g u > w+ z d s s~ WEIGHTED<=7 @12
// Process: g c > w+ z s d d~ WEIGHTED<=7 @12
// Process: g d > w+ z d s c~ WEIGHTED<=7 @12
// Process: g s > w+ z s d u~ WEIGHTED<=7 @12
// Process: g u~ > w+ z s u~ c~ WEIGHTED<=7 @12
// Process: g c~ > w+ z d c~ u~ WEIGHTED<=7 @12
// Process: g d~ > w+ z c u~ c~ WEIGHTED<=7 @12
// Process: g s~ > w+ z u c~ u~ WEIGHTED<=7 @12
// Process: g d~ > w+ z s u~ s~ WEIGHTED<=7 @12
// Process: g s~ > w+ z d c~ d~ WEIGHTED<=7 @12
// Process: g d~ > w+ z s c~ d~ WEIGHTED<=7 @12
// Process: g s~ > w+ z d u~ s~ WEIGHTED<=7 @12

// Exception class
class PY8MEs_R12_P34_sm_gq_wpzqqqException : public exception
{
  virtual const char * what() const throw()
  {
    return "Exception in class 'PY8MEs_R12_P34_sm_gq_wpzqqq'."; 
  }
}
PY8MEs_R12_P34_sm_gq_wpzqqq_exception; 

std::set<int> PY8MEs_R12_P34_sm_gq_wpzqqq::s_channel_proc = std::set<int>
    (createset<int> ());

int PY8MEs_R12_P34_sm_gq_wpzqqq::helicities[ncomb][nexternal] = {{-1, -1, -1,
    -1, -1, -1, -1}, {-1, -1, -1, -1, -1, -1, 1}, {-1, -1, -1, -1, -1, 1, -1},
    {-1, -1, -1, -1, -1, 1, 1}, {-1, -1, -1, -1, 1, -1, -1}, {-1, -1, -1, -1,
    1, -1, 1}, {-1, -1, -1, -1, 1, 1, -1}, {-1, -1, -1, -1, 1, 1, 1}, {-1, -1,
    -1, 0, -1, -1, -1}, {-1, -1, -1, 0, -1, -1, 1}, {-1, -1, -1, 0, -1, 1, -1},
    {-1, -1, -1, 0, -1, 1, 1}, {-1, -1, -1, 0, 1, -1, -1}, {-1, -1, -1, 0, 1,
    -1, 1}, {-1, -1, -1, 0, 1, 1, -1}, {-1, -1, -1, 0, 1, 1, 1}, {-1, -1, -1,
    1, -1, -1, -1}, {-1, -1, -1, 1, -1, -1, 1}, {-1, -1, -1, 1, -1, 1, -1},
    {-1, -1, -1, 1, -1, 1, 1}, {-1, -1, -1, 1, 1, -1, -1}, {-1, -1, -1, 1, 1,
    -1, 1}, {-1, -1, -1, 1, 1, 1, -1}, {-1, -1, -1, 1, 1, 1, 1}, {-1, -1, 0,
    -1, -1, -1, -1}, {-1, -1, 0, -1, -1, -1, 1}, {-1, -1, 0, -1, -1, 1, -1},
    {-1, -1, 0, -1, -1, 1, 1}, {-1, -1, 0, -1, 1, -1, -1}, {-1, -1, 0, -1, 1,
    -1, 1}, {-1, -1, 0, -1, 1, 1, -1}, {-1, -1, 0, -1, 1, 1, 1}, {-1, -1, 0, 0,
    -1, -1, -1}, {-1, -1, 0, 0, -1, -1, 1}, {-1, -1, 0, 0, -1, 1, -1}, {-1, -1,
    0, 0, -1, 1, 1}, {-1, -1, 0, 0, 1, -1, -1}, {-1, -1, 0, 0, 1, -1, 1}, {-1,
    -1, 0, 0, 1, 1, -1}, {-1, -1, 0, 0, 1, 1, 1}, {-1, -1, 0, 1, -1, -1, -1},
    {-1, -1, 0, 1, -1, -1, 1}, {-1, -1, 0, 1, -1, 1, -1}, {-1, -1, 0, 1, -1, 1,
    1}, {-1, -1, 0, 1, 1, -1, -1}, {-1, -1, 0, 1, 1, -1, 1}, {-1, -1, 0, 1, 1,
    1, -1}, {-1, -1, 0, 1, 1, 1, 1}, {-1, -1, 1, -1, -1, -1, -1}, {-1, -1, 1,
    -1, -1, -1, 1}, {-1, -1, 1, -1, -1, 1, -1}, {-1, -1, 1, -1, -1, 1, 1}, {-1,
    -1, 1, -1, 1, -1, -1}, {-1, -1, 1, -1, 1, -1, 1}, {-1, -1, 1, -1, 1, 1,
    -1}, {-1, -1, 1, -1, 1, 1, 1}, {-1, -1, 1, 0, -1, -1, -1}, {-1, -1, 1, 0,
    -1, -1, 1}, {-1, -1, 1, 0, -1, 1, -1}, {-1, -1, 1, 0, -1, 1, 1}, {-1, -1,
    1, 0, 1, -1, -1}, {-1, -1, 1, 0, 1, -1, 1}, {-1, -1, 1, 0, 1, 1, -1}, {-1,
    -1, 1, 0, 1, 1, 1}, {-1, -1, 1, 1, -1, -1, -1}, {-1, -1, 1, 1, -1, -1, 1},
    {-1, -1, 1, 1, -1, 1, -1}, {-1, -1, 1, 1, -1, 1, 1}, {-1, -1, 1, 1, 1, -1,
    -1}, {-1, -1, 1, 1, 1, -1, 1}, {-1, -1, 1, 1, 1, 1, -1}, {-1, -1, 1, 1, 1,
    1, 1}, {-1, 1, -1, -1, -1, -1, -1}, {-1, 1, -1, -1, -1, -1, 1}, {-1, 1, -1,
    -1, -1, 1, -1}, {-1, 1, -1, -1, -1, 1, 1}, {-1, 1, -1, -1, 1, -1, -1}, {-1,
    1, -1, -1, 1, -1, 1}, {-1, 1, -1, -1, 1, 1, -1}, {-1, 1, -1, -1, 1, 1, 1},
    {-1, 1, -1, 0, -1, -1, -1}, {-1, 1, -1, 0, -1, -1, 1}, {-1, 1, -1, 0, -1,
    1, -1}, {-1, 1, -1, 0, -1, 1, 1}, {-1, 1, -1, 0, 1, -1, -1}, {-1, 1, -1, 0,
    1, -1, 1}, {-1, 1, -1, 0, 1, 1, -1}, {-1, 1, -1, 0, 1, 1, 1}, {-1, 1, -1,
    1, -1, -1, -1}, {-1, 1, -1, 1, -1, -1, 1}, {-1, 1, -1, 1, -1, 1, -1}, {-1,
    1, -1, 1, -1, 1, 1}, {-1, 1, -1, 1, 1, -1, -1}, {-1, 1, -1, 1, 1, -1, 1},
    {-1, 1, -1, 1, 1, 1, -1}, {-1, 1, -1, 1, 1, 1, 1}, {-1, 1, 0, -1, -1, -1,
    -1}, {-1, 1, 0, -1, -1, -1, 1}, {-1, 1, 0, -1, -1, 1, -1}, {-1, 1, 0, -1,
    -1, 1, 1}, {-1, 1, 0, -1, 1, -1, -1}, {-1, 1, 0, -1, 1, -1, 1}, {-1, 1, 0,
    -1, 1, 1, -1}, {-1, 1, 0, -1, 1, 1, 1}, {-1, 1, 0, 0, -1, -1, -1}, {-1, 1,
    0, 0, -1, -1, 1}, {-1, 1, 0, 0, -1, 1, -1}, {-1, 1, 0, 0, -1, 1, 1}, {-1,
    1, 0, 0, 1, -1, -1}, {-1, 1, 0, 0, 1, -1, 1}, {-1, 1, 0, 0, 1, 1, -1}, {-1,
    1, 0, 0, 1, 1, 1}, {-1, 1, 0, 1, -1, -1, -1}, {-1, 1, 0, 1, -1, -1, 1},
    {-1, 1, 0, 1, -1, 1, -1}, {-1, 1, 0, 1, -1, 1, 1}, {-1, 1, 0, 1, 1, -1,
    -1}, {-1, 1, 0, 1, 1, -1, 1}, {-1, 1, 0, 1, 1, 1, -1}, {-1, 1, 0, 1, 1, 1,
    1}, {-1, 1, 1, -1, -1, -1, -1}, {-1, 1, 1, -1, -1, -1, 1}, {-1, 1, 1, -1,
    -1, 1, -1}, {-1, 1, 1, -1, -1, 1, 1}, {-1, 1, 1, -1, 1, -1, -1}, {-1, 1, 1,
    -1, 1, -1, 1}, {-1, 1, 1, -1, 1, 1, -1}, {-1, 1, 1, -1, 1, 1, 1}, {-1, 1,
    1, 0, -1, -1, -1}, {-1, 1, 1, 0, -1, -1, 1}, {-1, 1, 1, 0, -1, 1, -1}, {-1,
    1, 1, 0, -1, 1, 1}, {-1, 1, 1, 0, 1, -1, -1}, {-1, 1, 1, 0, 1, -1, 1}, {-1,
    1, 1, 0, 1, 1, -1}, {-1, 1, 1, 0, 1, 1, 1}, {-1, 1, 1, 1, -1, -1, -1}, {-1,
    1, 1, 1, -1, -1, 1}, {-1, 1, 1, 1, -1, 1, -1}, {-1, 1, 1, 1, -1, 1, 1},
    {-1, 1, 1, 1, 1, -1, -1}, {-1, 1, 1, 1, 1, -1, 1}, {-1, 1, 1, 1, 1, 1, -1},
    {-1, 1, 1, 1, 1, 1, 1}, {1, -1, -1, -1, -1, -1, -1}, {1, -1, -1, -1, -1,
    -1, 1}, {1, -1, -1, -1, -1, 1, -1}, {1, -1, -1, -1, -1, 1, 1}, {1, -1, -1,
    -1, 1, -1, -1}, {1, -1, -1, -1, 1, -1, 1}, {1, -1, -1, -1, 1, 1, -1}, {1,
    -1, -1, -1, 1, 1, 1}, {1, -1, -1, 0, -1, -1, -1}, {1, -1, -1, 0, -1, -1,
    1}, {1, -1, -1, 0, -1, 1, -1}, {1, -1, -1, 0, -1, 1, 1}, {1, -1, -1, 0, 1,
    -1, -1}, {1, -1, -1, 0, 1, -1, 1}, {1, -1, -1, 0, 1, 1, -1}, {1, -1, -1, 0,
    1, 1, 1}, {1, -1, -1, 1, -1, -1, -1}, {1, -1, -1, 1, -1, -1, 1}, {1, -1,
    -1, 1, -1, 1, -1}, {1, -1, -1, 1, -1, 1, 1}, {1, -1, -1, 1, 1, -1, -1}, {1,
    -1, -1, 1, 1, -1, 1}, {1, -1, -1, 1, 1, 1, -1}, {1, -1, -1, 1, 1, 1, 1},
    {1, -1, 0, -1, -1, -1, -1}, {1, -1, 0, -1, -1, -1, 1}, {1, -1, 0, -1, -1,
    1, -1}, {1, -1, 0, -1, -1, 1, 1}, {1, -1, 0, -1, 1, -1, -1}, {1, -1, 0, -1,
    1, -1, 1}, {1, -1, 0, -1, 1, 1, -1}, {1, -1, 0, -1, 1, 1, 1}, {1, -1, 0, 0,
    -1, -1, -1}, {1, -1, 0, 0, -1, -1, 1}, {1, -1, 0, 0, -1, 1, -1}, {1, -1, 0,
    0, -1, 1, 1}, {1, -1, 0, 0, 1, -1, -1}, {1, -1, 0, 0, 1, -1, 1}, {1, -1, 0,
    0, 1, 1, -1}, {1, -1, 0, 0, 1, 1, 1}, {1, -1, 0, 1, -1, -1, -1}, {1, -1, 0,
    1, -1, -1, 1}, {1, -1, 0, 1, -1, 1, -1}, {1, -1, 0, 1, -1, 1, 1}, {1, -1,
    0, 1, 1, -1, -1}, {1, -1, 0, 1, 1, -1, 1}, {1, -1, 0, 1, 1, 1, -1}, {1, -1,
    0, 1, 1, 1, 1}, {1, -1, 1, -1, -1, -1, -1}, {1, -1, 1, -1, -1, -1, 1}, {1,
    -1, 1, -1, -1, 1, -1}, {1, -1, 1, -1, -1, 1, 1}, {1, -1, 1, -1, 1, -1, -1},
    {1, -1, 1, -1, 1, -1, 1}, {1, -1, 1, -1, 1, 1, -1}, {1, -1, 1, -1, 1, 1,
    1}, {1, -1, 1, 0, -1, -1, -1}, {1, -1, 1, 0, -1, -1, 1}, {1, -1, 1, 0, -1,
    1, -1}, {1, -1, 1, 0, -1, 1, 1}, {1, -1, 1, 0, 1, -1, -1}, {1, -1, 1, 0, 1,
    -1, 1}, {1, -1, 1, 0, 1, 1, -1}, {1, -1, 1, 0, 1, 1, 1}, {1, -1, 1, 1, -1,
    -1, -1}, {1, -1, 1, 1, -1, -1, 1}, {1, -1, 1, 1, -1, 1, -1}, {1, -1, 1, 1,
    -1, 1, 1}, {1, -1, 1, 1, 1, -1, -1}, {1, -1, 1, 1, 1, -1, 1}, {1, -1, 1, 1,
    1, 1, -1}, {1, -1, 1, 1, 1, 1, 1}, {1, 1, -1, -1, -1, -1, -1}, {1, 1, -1,
    -1, -1, -1, 1}, {1, 1, -1, -1, -1, 1, -1}, {1, 1, -1, -1, -1, 1, 1}, {1, 1,
    -1, -1, 1, -1, -1}, {1, 1, -1, -1, 1, -1, 1}, {1, 1, -1, -1, 1, 1, -1}, {1,
    1, -1, -1, 1, 1, 1}, {1, 1, -1, 0, -1, -1, -1}, {1, 1, -1, 0, -1, -1, 1},
    {1, 1, -1, 0, -1, 1, -1}, {1, 1, -1, 0, -1, 1, 1}, {1, 1, -1, 0, 1, -1,
    -1}, {1, 1, -1, 0, 1, -1, 1}, {1, 1, -1, 0, 1, 1, -1}, {1, 1, -1, 0, 1, 1,
    1}, {1, 1, -1, 1, -1, -1, -1}, {1, 1, -1, 1, -1, -1, 1}, {1, 1, -1, 1, -1,
    1, -1}, {1, 1, -1, 1, -1, 1, 1}, {1, 1, -1, 1, 1, -1, -1}, {1, 1, -1, 1, 1,
    -1, 1}, {1, 1, -1, 1, 1, 1, -1}, {1, 1, -1, 1, 1, 1, 1}, {1, 1, 0, -1, -1,
    -1, -1}, {1, 1, 0, -1, -1, -1, 1}, {1, 1, 0, -1, -1, 1, -1}, {1, 1, 0, -1,
    -1, 1, 1}, {1, 1, 0, -1, 1, -1, -1}, {1, 1, 0, -1, 1, -1, 1}, {1, 1, 0, -1,
    1, 1, -1}, {1, 1, 0, -1, 1, 1, 1}, {1, 1, 0, 0, -1, -1, -1}, {1, 1, 0, 0,
    -1, -1, 1}, {1, 1, 0, 0, -1, 1, -1}, {1, 1, 0, 0, -1, 1, 1}, {1, 1, 0, 0,
    1, -1, -1}, {1, 1, 0, 0, 1, -1, 1}, {1, 1, 0, 0, 1, 1, -1}, {1, 1, 0, 0, 1,
    1, 1}, {1, 1, 0, 1, -1, -1, -1}, {1, 1, 0, 1, -1, -1, 1}, {1, 1, 0, 1, -1,
    1, -1}, {1, 1, 0, 1, -1, 1, 1}, {1, 1, 0, 1, 1, -1, -1}, {1, 1, 0, 1, 1,
    -1, 1}, {1, 1, 0, 1, 1, 1, -1}, {1, 1, 0, 1, 1, 1, 1}, {1, 1, 1, -1, -1,
    -1, -1}, {1, 1, 1, -1, -1, -1, 1}, {1, 1, 1, -1, -1, 1, -1}, {1, 1, 1, -1,
    -1, 1, 1}, {1, 1, 1, -1, 1, -1, -1}, {1, 1, 1, -1, 1, -1, 1}, {1, 1, 1, -1,
    1, 1, -1}, {1, 1, 1, -1, 1, 1, 1}, {1, 1, 1, 0, -1, -1, -1}, {1, 1, 1, 0,
    -1, -1, 1}, {1, 1, 1, 0, -1, 1, -1}, {1, 1, 1, 0, -1, 1, 1}, {1, 1, 1, 0,
    1, -1, -1}, {1, 1, 1, 0, 1, -1, 1}, {1, 1, 1, 0, 1, 1, -1}, {1, 1, 1, 0, 1,
    1, 1}, {1, 1, 1, 1, -1, -1, -1}, {1, 1, 1, 1, -1, -1, 1}, {1, 1, 1, 1, -1,
    1, -1}, {1, 1, 1, 1, -1, 1, 1}, {1, 1, 1, 1, 1, -1, -1}, {1, 1, 1, 1, 1,
    -1, 1}, {1, 1, 1, 1, 1, 1, -1}, {1, 1, 1, 1, 1, 1, 1}};

// Normalization factors the various processes
// Denominators: spins, colors and identical particles
int PY8MEs_R12_P34_sm_gq_wpzqqq::denom_colors[nprocesses] = {24, 24, 24, 24,
    24, 24, 24, 24, 24, 24, 24, 24, 24, 24};
int PY8MEs_R12_P34_sm_gq_wpzqqq::denom_hels[nprocesses] = {4, 4, 4, 4, 4, 4, 4,
    4, 4, 4, 4, 4, 4, 4};
int PY8MEs_R12_P34_sm_gq_wpzqqq::denom_iden[nprocesses] = {1, 2, 2, 2, 2, 1, 1,
    1, 1, 1, 1, 1, 1, 1};

//--------------------------------------------------------------------------
// Color config initialization
void PY8MEs_R12_P34_sm_gq_wpzqqq::initColorConfigs() 
{
  color_configs = vector < vec_vec_int > (); 
  jamp_nc_relative_power = vector < vec_int > (); 

  // Color flows of process Process: g u > w+ z u d u~ WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(3)(3)(0)(0)(0)(0)(0)(1)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #1
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(3)(2)(0)(0)(0)(0)(0)(1)(0)(2)(0)(0)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #2
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(3)(3)(0)(0)(0)(0)(0)(1)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #3
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(3)(1)(0)(0)(0)(0)(0)(1)(0)(2)(0)(0)(3)));
  jamp_nc_relative_power[0].push_back(0); 

  // Color flows of process Process: g u > w+ z d d d~ WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(3)(3)(0)(0)(0)(0)(0)(1)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #1
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(3)(2)(0)(0)(0)(0)(0)(1)(0)(2)(0)(0)(3)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #2
  color_configs[1].push_back(vec_int(createvector<int>
      (2)(3)(3)(0)(0)(0)(0)(0)(1)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #3
  color_configs[1].push_back(vec_int(createvector<int>
      (2)(3)(1)(0)(0)(0)(0)(0)(1)(0)(2)(0)(0)(3)));
  jamp_nc_relative_power[1].push_back(0); 

  // Color flows of process Process: g d > w+ z d d u~ WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[2].push_back(vec_int(createvector<int>
      (1)(3)(3)(0)(0)(0)(0)(0)(1)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #1
  color_configs[2].push_back(vec_int(createvector<int>
      (1)(3)(2)(0)(0)(0)(0)(0)(1)(0)(2)(0)(0)(3)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #2
  color_configs[2].push_back(vec_int(createvector<int>
      (2)(3)(3)(0)(0)(0)(0)(0)(1)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #3
  color_configs[2].push_back(vec_int(createvector<int>
      (2)(3)(1)(0)(0)(0)(0)(0)(1)(0)(2)(0)(0)(3)));
  jamp_nc_relative_power[2].push_back(0); 

  // Color flows of process Process: g u~ > w+ z d u~ u~ WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[3].push_back(vec_int(createvector<int>
      (1)(3)(0)(1)(0)(0)(0)(0)(2)(0)(0)(3)(0)(2)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #1
  color_configs[3].push_back(vec_int(createvector<int>
      (1)(3)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)(0)(3)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #2
  color_configs[3].push_back(vec_int(createvector<int>
      (2)(3)(0)(1)(0)(0)(0)(0)(2)(0)(0)(3)(0)(1)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #3
  color_configs[3].push_back(vec_int(createvector<int>
      (2)(3)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)(0)(3)));
  jamp_nc_relative_power[3].push_back(0); 

  // Color flows of process Process: g d~ > w+ z u u~ u~ WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[4].push_back(vec_int(createvector<int>
      (1)(3)(0)(1)(0)(0)(0)(0)(2)(0)(0)(3)(0)(2)));
  jamp_nc_relative_power[4].push_back(0); 
  // JAMP #1
  color_configs[4].push_back(vec_int(createvector<int>
      (1)(3)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)(0)(3)));
  jamp_nc_relative_power[4].push_back(0); 
  // JAMP #2
  color_configs[4].push_back(vec_int(createvector<int>
      (2)(3)(0)(1)(0)(0)(0)(0)(2)(0)(0)(3)(0)(1)));
  jamp_nc_relative_power[4].push_back(0); 
  // JAMP #3
  color_configs[4].push_back(vec_int(createvector<int>
      (2)(3)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)(0)(3)));
  jamp_nc_relative_power[4].push_back(0); 

  // Color flows of process Process: g d~ > w+ z d u~ d~ WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[5].push_back(vec_int(createvector<int>
      (1)(3)(0)(1)(0)(0)(0)(0)(2)(0)(0)(3)(0)(2)));
  jamp_nc_relative_power[5].push_back(0); 
  // JAMP #1
  color_configs[5].push_back(vec_int(createvector<int>
      (1)(3)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)(0)(3)));
  jamp_nc_relative_power[5].push_back(0); 
  // JAMP #2
  color_configs[5].push_back(vec_int(createvector<int>
      (2)(3)(0)(1)(0)(0)(0)(0)(2)(0)(0)(3)(0)(1)));
  jamp_nc_relative_power[5].push_back(0); 
  // JAMP #3
  color_configs[5].push_back(vec_int(createvector<int>
      (2)(3)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)(0)(3)));
  jamp_nc_relative_power[5].push_back(0); 

  // Color flows of process Process: g u > w+ z u s c~ WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[6].push_back(vec_int(createvector<int>
      (1)(3)(3)(0)(0)(0)(0)(0)(1)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[6].push_back(-1); 
  // JAMP #1
  color_configs[6].push_back(vec_int(createvector<int>
      (1)(3)(2)(0)(0)(0)(0)(0)(1)(0)(2)(0)(0)(3)));
  jamp_nc_relative_power[6].push_back(0); 
  // JAMP #2
  color_configs[6].push_back(vec_int(createvector<int>
      (2)(3)(3)(0)(0)(0)(0)(0)(1)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[6].push_back(0); 
  // JAMP #3
  color_configs[6].push_back(vec_int(createvector<int>
      (2)(3)(1)(0)(0)(0)(0)(0)(1)(0)(2)(0)(0)(3)));
  jamp_nc_relative_power[6].push_back(-1); 

  // Color flows of process Process: g u > w+ z c d c~ WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[7].push_back(vec_int(createvector<int>
      (1)(3)(3)(0)(0)(0)(0)(0)(1)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[7].push_back(0); 
  // JAMP #1
  color_configs[7].push_back(vec_int(createvector<int>
      (1)(3)(2)(0)(0)(0)(0)(0)(1)(0)(2)(0)(0)(3)));
  jamp_nc_relative_power[7].push_back(-1); 
  // JAMP #2
  color_configs[7].push_back(vec_int(createvector<int>
      (2)(3)(3)(0)(0)(0)(0)(0)(1)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[7].push_back(-1); 
  // JAMP #3
  color_configs[7].push_back(vec_int(createvector<int>
      (2)(3)(1)(0)(0)(0)(0)(0)(1)(0)(2)(0)(0)(3)));
  jamp_nc_relative_power[7].push_back(0); 

  // Color flows of process Process: g u > w+ z d s s~ WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[8].push_back(vec_int(createvector<int>
      (1)(3)(3)(0)(0)(0)(0)(0)(1)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[8].push_back(-1); 
  // JAMP #1
  color_configs[8].push_back(vec_int(createvector<int>
      (1)(3)(2)(0)(0)(0)(0)(0)(1)(0)(2)(0)(0)(3)));
  jamp_nc_relative_power[8].push_back(0); 
  // JAMP #2
  color_configs[8].push_back(vec_int(createvector<int>
      (2)(3)(3)(0)(0)(0)(0)(0)(1)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[8].push_back(0); 
  // JAMP #3
  color_configs[8].push_back(vec_int(createvector<int>
      (2)(3)(1)(0)(0)(0)(0)(0)(1)(0)(2)(0)(0)(3)));
  jamp_nc_relative_power[8].push_back(-1); 

  // Color flows of process Process: g d > w+ z d s c~ WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[9].push_back(vec_int(createvector<int>
      (1)(3)(3)(0)(0)(0)(0)(0)(1)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[9].push_back(-1); 
  // JAMP #1
  color_configs[9].push_back(vec_int(createvector<int>
      (1)(3)(2)(0)(0)(0)(0)(0)(1)(0)(2)(0)(0)(3)));
  jamp_nc_relative_power[9].push_back(0); 
  // JAMP #2
  color_configs[9].push_back(vec_int(createvector<int>
      (2)(3)(3)(0)(0)(0)(0)(0)(1)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[9].push_back(0); 
  // JAMP #3
  color_configs[9].push_back(vec_int(createvector<int>
      (2)(3)(1)(0)(0)(0)(0)(0)(1)(0)(2)(0)(0)(3)));
  jamp_nc_relative_power[9].push_back(-1); 

  // Color flows of process Process: g u~ > w+ z s u~ c~ WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[10].push_back(vec_int(createvector<int>
      (1)(3)(0)(1)(0)(0)(0)(0)(2)(0)(0)(3)(0)(2)));
  jamp_nc_relative_power[10].push_back(-1); 
  // JAMP #1
  color_configs[10].push_back(vec_int(createvector<int>
      (1)(3)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)(0)(3)));
  jamp_nc_relative_power[10].push_back(0); 
  // JAMP #2
  color_configs[10].push_back(vec_int(createvector<int>
      (2)(3)(0)(1)(0)(0)(0)(0)(2)(0)(0)(3)(0)(1)));
  jamp_nc_relative_power[10].push_back(0); 
  // JAMP #3
  color_configs[10].push_back(vec_int(createvector<int>
      (2)(3)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)(0)(3)));
  jamp_nc_relative_power[10].push_back(-1); 

  // Color flows of process Process: g d~ > w+ z c u~ c~ WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[11].push_back(vec_int(createvector<int>
      (1)(3)(0)(1)(0)(0)(0)(0)(2)(0)(0)(3)(0)(2)));
  jamp_nc_relative_power[11].push_back(-1); 
  // JAMP #1
  color_configs[11].push_back(vec_int(createvector<int>
      (1)(3)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)(0)(3)));
  jamp_nc_relative_power[11].push_back(0); 
  // JAMP #2
  color_configs[11].push_back(vec_int(createvector<int>
      (2)(3)(0)(1)(0)(0)(0)(0)(2)(0)(0)(3)(0)(1)));
  jamp_nc_relative_power[11].push_back(0); 
  // JAMP #3
  color_configs[11].push_back(vec_int(createvector<int>
      (2)(3)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)(0)(3)));
  jamp_nc_relative_power[11].push_back(-1); 

  // Color flows of process Process: g d~ > w+ z s u~ s~ WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[12].push_back(vec_int(createvector<int>
      (1)(3)(0)(1)(0)(0)(0)(0)(2)(0)(0)(3)(0)(2)));
  jamp_nc_relative_power[12].push_back(-1); 
  // JAMP #1
  color_configs[12].push_back(vec_int(createvector<int>
      (1)(3)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)(0)(3)));
  jamp_nc_relative_power[12].push_back(0); 
  // JAMP #2
  color_configs[12].push_back(vec_int(createvector<int>
      (2)(3)(0)(1)(0)(0)(0)(0)(2)(0)(0)(3)(0)(1)));
  jamp_nc_relative_power[12].push_back(0); 
  // JAMP #3
  color_configs[12].push_back(vec_int(createvector<int>
      (2)(3)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)(0)(3)));
  jamp_nc_relative_power[12].push_back(-1); 

  // Color flows of process Process: g d~ > w+ z s c~ d~ WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[13].push_back(vec_int(createvector<int>
      (1)(3)(0)(1)(0)(0)(0)(0)(2)(0)(0)(3)(0)(2)));
  jamp_nc_relative_power[13].push_back(0); 
  // JAMP #1
  color_configs[13].push_back(vec_int(createvector<int>
      (1)(3)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)(0)(3)));
  jamp_nc_relative_power[13].push_back(-1); 
  // JAMP #2
  color_configs[13].push_back(vec_int(createvector<int>
      (2)(3)(0)(1)(0)(0)(0)(0)(2)(0)(0)(3)(0)(1)));
  jamp_nc_relative_power[13].push_back(-1); 
  // JAMP #3
  color_configs[13].push_back(vec_int(createvector<int>
      (2)(3)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)(0)(3)));
  jamp_nc_relative_power[13].push_back(0); 
}

//--------------------------------------------------------------------------
// Destructor.
PY8MEs_R12_P34_sm_gq_wpzqqq::~PY8MEs_R12_P34_sm_gq_wpzqqq() 
{
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    delete[] p[i]; 
    p[i] = NULL; 
  }
}

//--------------------------------------------------------------------------
// Invert the permutation mapping
vector<int> PY8MEs_R12_P34_sm_gq_wpzqqq::invert_mapping(vector<int> mapping) 
{
  vector<int> inverted_mapping; 
  for (unsigned int i = 0; i < mapping.size(); i++ )
  {
    for (unsigned int j = 0; j < mapping.size(); j++ )
    {
      if (mapping[j] == ((int)i))
      {
        inverted_mapping.push_back(j); 
        break; 
      }
    }
  }
  return inverted_mapping; 
}

//--------------------------------------------------------------------------
// Return the list of possible helicity configurations
vector < vec_int > PY8MEs_R12_P34_sm_gq_wpzqqq::getHelicityConfigs(vector<int>
    permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(ncomb, vector<int> (nexternal, 0)); 
  for (unsigned int ihel = 0; ihel < ncomb; ihel++ )
  {
    for(unsigned int j = 0; j < nexternal; j++ )
    {
      res[ihel][chosenPerm[j]] = helicities[ihel][j]; 
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Return the list of possible color configurations
vector < vec_int > PY8MEs_R12_P34_sm_gq_wpzqqq::getColorConfigs(int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(color_configs[chosenProcID].size(), vector<int>
      (nexternal * 2, 0));
  for (unsigned int icol = 0; icol < color_configs[chosenProcID].size(); icol++
      )
  {
    for(unsigned int j = 0; j < (2 * nexternal); j++ )
    {
      res[icol][chosenPerm[j/2] * 2 + j%2] =
          color_configs[chosenProcID][icol][j];
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Get JAMP relative N_c power
int PY8MEs_R12_P34_sm_gq_wpzqqq::getColorFlowRelativeNCPower(int color_flow_ID,
    int specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return jamp_nc_relative_power[chosenProcID][color_flow_ID]; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity ID -> Helicity Config
vector<int> PY8MEs_R12_P34_sm_gq_wpzqqq::getHelicityConfigForID(int hel_ID,
    vector<int> permutation)
{
  if (hel_ID < 0 || hel_ID >= ncomb)
  {
    cerr <<  "Error in function 'getHelicityConfigForID' of class" << 
    " 'PY8MEs_R12_P34_sm_gq_wpzqqq': Specified helicity ID '" << 
    hel_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R12_P34_sm_gq_wpzqqq_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(nexternal, 0); 
  for (unsigned int j = 0; j < nexternal; j++ )
  {
    res[chosenPerm[j]] = helicities[hel_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity Config -> Helicity ID
int PY8MEs_R12_P34_sm_gq_wpzqqq::getHelicityIDForConfig(vector<int> hel_config,
    vector<int> permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  int user_ihel = -1; 
  if (hel_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < ncomb; i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < nexternal; j++ )
      {
        if (helicities[i][chosenPerm[j]] != hel_config[j])
        {
          found = false; 
          break; 
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_ihel = i; 
        break; 
      }
    }
    if (user_ihel == -1)
    {
      cerr <<  "Error in function 'getHelicityIDForConfig' of class" << 
      " 'PY8MEs_R12_P34_sm_gq_wpzqqq': Specified helicity" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R12_P34_sm_gq_wpzqqq_exception; 
    }
  }
  return user_ihel; 
}


//--------------------------------------------------------------------------
// Implements the map Color ID -> Color Config
vector<int> PY8MEs_R12_P34_sm_gq_wpzqqq::getColorConfigForID(int color_ID, int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < 0 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getColorConfigForID' of class" << 
    " 'PY8MEs_R12_P34_sm_gq_wpzqqq': Specified color ID '" << 
    color_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R12_P34_sm_gq_wpzqqq_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(color_configs[chosenProcID][color_ID].size(), 0); 
  for (unsigned int j = 0; j < (2 * nexternal); j++ )
  {
    res[chosenPerm[j/2] * 2 + j%2] = color_configs[chosenProcID][color_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Color Config -> Color ID
int PY8MEs_R12_P34_sm_gq_wpzqqq::getColorIDForConfig(vector<int> color_config,
    int specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = -1; 
  if (color_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < color_configs[chosenProcID].size(); i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < (nexternal * 2); j++ )
      {

        // If colorless then make sure it matches
        // The little arithmetics in the color index is just
        // the permutation applies on the particle list which is
        // twice smaller since each particle can have two color indices.
        if (color_config[j] == 0)
        {
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] != 0)
          {
            found = false; 
            break; 
          }
          // Otherwise check that the color linked position matches
        }
        else
        {
          int color_linked_pos = -1; 
          // Find the other end of the line in the user color config
          for (unsigned int k = 0; k < (nexternal * 2); k++ )
          {
            if (k == j)
              continue; 
            if (color_config[j] == color_config[k])
            {
              color_linked_pos = k; 
              break; 
            }
          }
          if (color_linked_pos == -1)
          {
            cerr <<  "Error in function 'getColorIDForConfig' of class" << 
            " 'PY8MEs_R12_P34_sm_gq_wpzqqq': A color line could " << 
            " not be closed." << endl; 
            throw PY8MEs_R12_P34_sm_gq_wpzqqq_exception; 
          }
          // Now check whether the color line matches
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] !=
              color_configs[chosenProcID][i][chosenPerm[color_linked_pos/2] * 2
              + color_linked_pos%2])
          {
            found = false; 
            break; 
          }
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_icol = i; 
        break; 
      }
    }

    if (user_icol == -1)
    {
      cerr <<  "Error in function 'getColorIDForConfig' of class" << 
      " 'PY8MEs_R12_P34_sm_gq_wpzqqq': Specified color" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R12_P34_sm_gq_wpzqqq_exception; 
    }
  }
  return user_icol; 
}

//--------------------------------------------------------------------------
// Returns all result previously computed in SigmaKin
vector < vec_double > PY8MEs_R12_P34_sm_gq_wpzqqq::getAllResults(int
    specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return all_results[chosenProcID]; 
}

//--------------------------------------------------------------------------
// Returns a result previously computed in SigmaKin for a specific helicity
// and color ID. -1 means avg and summed over that characteristic.
double PY8MEs_R12_P34_sm_gq_wpzqqq::getResult(int helicity_ID, int color_ID,
    int specify_proc_ID)
{
  if (helicity_ID < - 1 || helicity_ID >= ncomb)
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R12_P34_sm_gq_wpzqqq': Specified helicity ID '" << 
    helicity_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R12_P34_sm_gq_wpzqqq_exception; 
  }
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < - 1 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R12_P34_sm_gq_wpzqqq': Specified color ID '" << 
    color_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R12_P34_sm_gq_wpzqqq_exception; 
  }
  return all_results[chosenProcID][helicity_ID + 1][color_ID + 1]; 
}

//--------------------------------------------------------------------------
// Check for the availability of the requested process and if available,
// If available, this returns the corresponding permutation and Proc_ID to use.
// If not available, this returns a negative Proc_ID.
pair < vector<int> , int > PY8MEs_R12_P34_sm_gq_wpzqqq::static_getPY8ME(vector<int> initial_pdgs, vector<int> final_pdgs, set<int> schannels) 
{

  // Not available return value
  pair < vector<int> , int > NA(vector<int> (), -1); 

  // Check if s-channel requirements match
  if (nreq_s_channels > 0)
  {
    if (schannels != s_channel_proc)
      return NA; 
  }
  else
  {
    if (schannels.size() != 0)
      return NA; 
  }

  // Check number of final state particles
  if (final_pdgs.size() != (nexternal - ninitial))
    return NA; 

  // Check number of initial state particles
  if (initial_pdgs.size() != ninitial)
    return NA; 

  // List of processes available in this class
  const int nprocs = 56; 
  const int proc_IDS[nprocs] = {0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7,
      8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4,
      5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13};
  const int in_pdgs[nprocs][ninitial] = {{21, 2}, {21, 4}, {21, 2}, {21, 4},
      {21, 1}, {21, 3}, {21, -2}, {21, -4}, {21, -1}, {21, -3}, {21, -1}, {21,
      -3}, {21, 2}, {21, 4}, {21, 2}, {21, 4}, {21, 2}, {21, 4}, {21, 1}, {21,
      3}, {21, -2}, {21, -4}, {21, -1}, {21, -3}, {21, -1}, {21, -3}, {21, -1},
      {21, -3}, {2, 21}, {4, 21}, {2, 21}, {4, 21}, {1, 21}, {3, 21}, {-2, 21},
      {-4, 21}, {-1, 21}, {-3, 21}, {-1, 21}, {-3, 21}, {2, 21}, {4, 21}, {2,
      21}, {4, 21}, {2, 21}, {4, 21}, {1, 21}, {3, 21}, {-2, 21}, {-4, 21},
      {-1, 21}, {-3, 21}, {-1, 21}, {-3, 21}, {-1, 21}, {-3, 21}};
  const int out_pdgs[nprocs][nexternal - ninitial] = {{24, 23, 2, 1, -2}, {24,
      23, 4, 3, -4}, {24, 23, 1, 1, -1}, {24, 23, 3, 3, -3}, {24, 23, 1, 1,
      -2}, {24, 23, 3, 3, -4}, {24, 23, 1, -2, -2}, {24, 23, 3, -4, -4}, {24,
      23, 2, -2, -2}, {24, 23, 4, -4, -4}, {24, 23, 1, -2, -1}, {24, 23, 3, -4,
      -3}, {24, 23, 2, 3, -4}, {24, 23, 4, 1, -2}, {24, 23, 4, 1, -4}, {24, 23,
      2, 3, -2}, {24, 23, 1, 3, -3}, {24, 23, 3, 1, -1}, {24, 23, 1, 3, -4},
      {24, 23, 3, 1, -2}, {24, 23, 3, -2, -4}, {24, 23, 1, -4, -2}, {24, 23, 4,
      -2, -4}, {24, 23, 2, -4, -2}, {24, 23, 3, -2, -3}, {24, 23, 1, -4, -1},
      {24, 23, 3, -4, -1}, {24, 23, 1, -2, -3}, {24, 23, 2, 1, -2}, {24, 23, 4,
      3, -4}, {24, 23, 1, 1, -1}, {24, 23, 3, 3, -3}, {24, 23, 1, 1, -2}, {24,
      23, 3, 3, -4}, {24, 23, 1, -2, -2}, {24, 23, 3, -4, -4}, {24, 23, 2, -2,
      -2}, {24, 23, 4, -4, -4}, {24, 23, 1, -2, -1}, {24, 23, 3, -4, -3}, {24,
      23, 2, 3, -4}, {24, 23, 4, 1, -2}, {24, 23, 4, 1, -4}, {24, 23, 2, 3,
      -2}, {24, 23, 1, 3, -3}, {24, 23, 3, 1, -1}, {24, 23, 1, 3, -4}, {24, 23,
      3, 1, -2}, {24, 23, 3, -2, -4}, {24, 23, 1, -4, -2}, {24, 23, 4, -2, -4},
      {24, 23, 2, -4, -2}, {24, 23, 3, -2, -3}, {24, 23, 1, -4, -1}, {24, 23,
      3, -4, -1}, {24, 23, 1, -2, -3}};

  bool in_pdgs_used[ninitial]; 
  bool out_pdgs_used[nexternal - ninitial]; 
  for(unsigned int i = 0; i < nprocs; i++ )
  {
    int permutations[nexternal]; 

    // Reinitialize initial state look-up variables
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      in_pdgs_used[j] = false; 
      permutations[j] = -1; 
    }
    // Look for initial state matches
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      for(unsigned int k = 0; k < ninitial; k++ )
      {
        // Make sure it has not been used already
        if (in_pdgs_used[k])
          continue; 
        if (initial_pdgs[k] == in_pdgs[i][j])
        {
          permutations[j] = k; 
          in_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[ninitial - 1] == -1)
      continue; 

    // Reinitialize final state look-up variables
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      out_pdgs_used[j] = false; 
      permutations[ninitial + j] = -1; 
    }
    // Look for final state matches
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      for(unsigned int k = 0; k < (nexternal - ninitial); k++ )
      {
        // Make sure it has not been used already
        if (out_pdgs_used[k])
          continue; 
        if (final_pdgs[k] == out_pdgs[i][j])
        {
          permutations[ninitial + j] = ninitial + k; 
          out_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[ninitial + j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[nexternal - 1] == -1)
      continue; 

    // Return process found
    return pair < vector<int> , int > (vector<int> (permutations, permutations
        + nexternal), proc_IDS[i]);
  }

  // No process found
  return NA; 
}

//--------------------------------------------------------------------------
// Set momenta
void PY8MEs_R12_P34_sm_gq_wpzqqq::setMomenta(vector < vec_double >
    momenta_picked)
{
  if (momenta_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setMomenta' of class" << 
    " 'PY8MEs_R12_P34_sm_gq_wpzqqq': Incorrect number of" << 
    " momenta specified." << endl; 
    throw PY8MEs_R12_P34_sm_gq_wpzqqq_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    if (momenta_picked[i].size() != 4)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R12_P34_sm_gq_wpzqqq': Incorrect number of" << 
      " momenta components specified." << endl; 
      throw PY8MEs_R12_P34_sm_gq_wpzqqq_exception; 
    }
    if (momenta_picked[i][0] < 0.0)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R12_P34_sm_gq_wpzqqq': A momentum was specified" << 
      " with negative energy. Check conventions." << endl; 
      throw PY8MEs_R12_P34_sm_gq_wpzqqq_exception; 
    }
    for (unsigned int j = 0; j < 4; j++ )
    {
      p[i][j] = momenta_picked[i][j]; 
    }
  }
}

//--------------------------------------------------------------------------
// Set color configuration to use. An empty vector means sum over all.
void PY8MEs_R12_P34_sm_gq_wpzqqq::setColors(vector<int> colors_picked)
{
  if (colors_picked.size() == 0)
  {
    user_colors = vector<int> (); 
    return; 
  }
  if (colors_picked.size() != (2 * nexternal))
  {
    cerr <<  "Error in function 'setColors' of class" << 
    " 'PY8MEs_R12_P34_sm_gq_wpzqqq': Incorrect number" << 
    " of colors specified." << endl; 
    throw PY8MEs_R12_P34_sm_gq_wpzqqq_exception; 
  }
  user_colors = vector<int> ((2 * nexternal), 0); 
  for(unsigned int i = 0; i < (2 * nexternal); i++ )
  {
    user_colors[i] = colors_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the helicity configuration to use. Am empty vector means sum over all.
void PY8MEs_R12_P34_sm_gq_wpzqqq::setHelicities(vector<int> helicities_picked) 
{
  if (helicities_picked.size() != nexternal)
  {
    if (helicities_picked.size() == 0)
    {
      user_helicities = vector<int> (); 
      return; 
    }
    cerr <<  "Error in function 'setHelicities' of class" << 
    " 'PY8MEs_R12_P34_sm_gq_wpzqqq': Incorrect number" << 
    " of helicities specified." << endl; 
    throw PY8MEs_R12_P34_sm_gq_wpzqqq_exception; 
  }
  user_helicities = vector<int> (nexternal, 0); 
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    user_helicities[i] = helicities_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the permutation to use (will apply to momenta, colors and helicities)
void PY8MEs_R12_P34_sm_gq_wpzqqq::setPermutation(vector<int> perm_picked) 
{
  if (perm_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setPermutations' of class" << 
    " 'PY8MEs_R12_P34_sm_gq_wpzqqq': Incorrect number" << 
    " of permutations specified." << endl; 
    throw PY8MEs_R12_P34_sm_gq_wpzqqq_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    perm[i] = perm_picked[i]; 
  }
}

// Set the proc_ID to use
void PY8MEs_R12_P34_sm_gq_wpzqqq::setProcID(int procID_picked) 
{
  proc_ID = procID_picked; 
}

//--------------------------------------------------------------------------
// Initialize process.

void PY8MEs_R12_P34_sm_gq_wpzqqq::initProc() 
{

  // Initialize flags
  include_symmetry_factors = true; 
  include_helicity_averaging_factors = true; 
  include_color_averaging_factors = true; 

  // Initialize vectors.
  perm = vector<int> (nexternal, 0); 
  user_colors = vector<int> (2 * nexternal, 0); 
  user_helicities = vector<int> (nexternal, 0); 
  p = vector < double * > (); 
  for (unsigned int i = 0; i < nexternal; i++ )
  {
    p.push_back(new double[4]); 
  }
  initColorConfigs(); 
  // Synchronize local variables dependent on the model with the active model.
  mME = vector<double> (nexternal, 0.); 
  syncProcModelParams(); 
  jamp2 = vector < vec_double > (14); 
  jamp2[0] = vector<double> (4, 0.); 
  jamp2[1] = vector<double> (4, 0.); 
  jamp2[2] = vector<double> (4, 0.); 
  jamp2[3] = vector<double> (4, 0.); 
  jamp2[4] = vector<double> (4, 0.); 
  jamp2[5] = vector<double> (4, 0.); 
  jamp2[6] = vector<double> (4, 0.); 
  jamp2[7] = vector<double> (4, 0.); 
  jamp2[8] = vector<double> (4, 0.); 
  jamp2[9] = vector<double> (4, 0.); 
  jamp2[10] = vector<double> (4, 0.); 
  jamp2[11] = vector<double> (4, 0.); 
  jamp2[12] = vector<double> (4, 0.); 
  jamp2[13] = vector<double> (4, 0.); 
  all_results = vector < vec_vec_double > (14); 
  // The first entry is always the color or helicity avg/summed matrix element.
  all_results[0] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[1] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[2] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[3] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[4] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[5] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[6] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[7] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[8] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[9] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[10] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[11] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[12] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[13] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
}

// Synchronize local variables of the process that depend on the model
// parameters
void PY8MEs_R12_P34_sm_gq_wpzqqq::syncProcModelParams() 
{

  // Instantiate the model class and set parameters that stay fixed during run
  mME[0] = pars->ZERO; 
  mME[1] = pars->ZERO; 
  mME[2] = pars->mdl_MW; 
  mME[3] = pars->mdl_MZ; 
  mME[4] = pars->ZERO; 
  mME[5] = pars->ZERO; 
  mME[6] = pars->ZERO; 
}

//--------------------------------------------------------------------------
// Setter allowing to force particular values for the external masses
void PY8MEs_R12_P34_sm_gq_wpzqqq::setMasses(vec_double external_masses) 
{

  if (external_masses.size() != mME.size())
  {
    cerr <<  "Error in function 'setMasses' of class" << 
    " 'PY8MEs_R12_P34_sm_gq_wpzqqq': Incorrect number of" << 
    " masses specified." << endl; 
    throw PY8MEs_R12_P34_sm_gq_wpzqqq_exception; 
  }
  for (unsigned int j = 0; j < mME.size(); j++ )
  {
    mME[j] = external_masses[perm[j]]; 
  }
}

//--------------------------------------------------------------------------
// Getter accessing external masses with the correct ordering
vector<double> PY8MEs_R12_P34_sm_gq_wpzqqq::getMasses() 
{

  vec_double external_masses; 
  vector<int> invertedPerm; 
  invertedPerm = invert_mapping(perm); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    external_masses.push_back(mME[invertedPerm[i]]); 
  }
  return external_masses; 

}


// Set all values of the external masses to float(-mode) where mode can be
// 0 : Mass taken from the model
// 1 : Mass taken from p_i^2 if not massless to begin with
// 2 : Mass always taken from p_i^2.
void PY8MEs_R12_P34_sm_gq_wpzqqq::setExternalMassesMode(int mode) 
{
  if (mode != 0 && mode != 1 && mode != 2)
  {
    cerr <<  "Error in function 'setExternalMassesMode' of class" << 
    " 'PY8MEs_R12_P34_sm_gq_wpzqqq': Incorrect mode selected :" << mode << 
    ". It must be either 0, 1 or 2" << endl; 
    throw PY8MEs_R12_P34_sm_gq_wpzqqq_exception; 
  }
  if (mode == 0)
  {
    syncProcModelParams(); 
  }
  else if (mode == 1)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      if (mME[j] != pars->ZERO)
      {
        mME[j] = -1.0; 
      }
    }
  }
  else if (mode == 2)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      mME[j] = -1.0; 
    }
  }
}

//--------------------------------------------------------------------------
// Evaluate the squared matrix element.

double PY8MEs_R12_P34_sm_gq_wpzqqq::sigmaKin() 
{
  // Set the parameters which change event by event
  pars->setDependentParameters(); 
  pars->setDependentCouplings(); 
  // Reset color flows
  for(int i = 0; i < 4; i++ )
    jamp2[0][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[1][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[2][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[3][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[4][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[5][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[6][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[7][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[8][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[9][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[10][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[11][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[12][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[13][i] = 0.; 

  // Save previous values of mME
  vector<double> saved_mME(mME.size(), 0.0); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (mME[i] < 0.0)
    {
      saved_mME[i] = mME[i]; 
      mME[i] = sqrt(abs(pow(p[perm[i]][0], 2) - 
      (pow(p[perm[i]][1], 2) + pow(p[perm[i]][2], 2) + pow(p[perm[i]][3],
          2))));
    }
  }

  // Local variables and constants
  const int max_tries = 10; 
  vector < vec_bool > goodhel(nprocesses, vec_bool(ncomb, false)); 
  vec_int ntry(nprocesses, 0); 
  double t = 0.; 
  double result = 0.; 

  if (ntry[proc_ID] <= max_tries)
    ntry[proc_ID] = ntry[proc_ID] + 1; 

  // Find which helicity configuration is asked for
  // -1 indicates one wants to sum over helicities
  int user_ihel = getHelicityIDForConfig(user_helicities); 

  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = getColorIDForConfig(user_colors); 

  // Reset the list of results that will be recomputed here
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      all_results[proc_ID][ihel + 1][icolor + 1] = 0.; 
    }
  }

  // Calculate the matrix element for all helicities
  // unless already detected as vanishing
  for(int ihel = 0; ihel < ncomb; ihel++ )
  {
    // Skip helicity if already detected as vanishing
    if ((ntry[proc_ID] >= max_tries) && !goodhel[proc_ID][ihel])
      continue; 

    // Also skip helicity if user asks for a specific one
    if ((ntry[proc_ID] >= max_tries) && user_ihel != -1 && user_ihel != ihel)
      continue; 

    calculate_wavefunctions(helicities[ihel]); 

    // Reset locally computed color flows
    for(int i = 0; i < 4; i++ )
      jamp2[0][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[1][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[2][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[3][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[4][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[5][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[6][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[7][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[8][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[9][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[10][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[11][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[12][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[13][i] = 0.; 

    if (proc_ID == 0)
      t = matrix_12_gu_wpzudux(); 
    if (proc_ID == 1)
      t = matrix_12_gu_wpzdddx(); 
    if (proc_ID == 2)
      t = matrix_12_gd_wpzddux(); 
    if (proc_ID == 3)
      t = matrix_12_gux_wpzduxux(); 
    if (proc_ID == 4)
      t = matrix_12_gdx_wpzuuxux(); 
    if (proc_ID == 5)
      t = matrix_12_gdx_wpzduxdx(); 
    if (proc_ID == 6)
      t = matrix_12_gu_wpzuscx(); 
    if (proc_ID == 7)
      t = matrix_12_gu_wpzcdcx(); 
    if (proc_ID == 8)
      t = matrix_12_gu_wpzdssx(); 
    if (proc_ID == 9)
      t = matrix_12_gd_wpzdscx(); 
    if (proc_ID == 10)
      t = matrix_12_gux_wpzsuxcx(); 
    if (proc_ID == 11)
      t = matrix_12_gdx_wpzcuxcx(); 
    if (proc_ID == 12)
      t = matrix_12_gdx_wpzsuxsx(); 
    if (proc_ID == 13)
      t = matrix_12_gdx_wpzscxdx(); 

    // Store which helicities give non-zero result
    if ((ntry[proc_ID] < max_tries) && t != 0. && !goodhel[proc_ID][ihel])
      goodhel[proc_ID][ihel] = true; 

    // Aggregate results
    if (user_ihel == -1 || user_ihel == ihel)
    {
      if (user_icol == -1)
      {
        result = result + t; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][0] += t; 
          for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
          {
            all_results[proc_ID][0][i + 1] += jamp2[proc_ID][i]; 
          }
        }
        all_results[proc_ID][ihel + 1][0] += t; 
        for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
        {
          all_results[proc_ID][ihel + 1][i + 1] += jamp2[proc_ID][i]; 
        }
      }
      else
      {
        result = result + jamp2[proc_ID][user_icol]; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][user_icol + 1] += jamp2[proc_ID][user_icol]; 
        }
        all_results[proc_ID][ihel + 1][user_icol + 1] +=
            jamp2[proc_ID][user_icol];
      }
    }

  }

  // Normalize results with the identical particle factor
  if (include_symmetry_factors)
  {
    result = result/denom_iden[proc_ID]; 
  }
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      if (include_symmetry_factors)
      {
        all_results[proc_ID][ihel + 1][icolor + 1] /= denom_iden[proc_ID]; 
      }
    }
  }


  // Normalize when when summing+averaging over helicity configurations
  if (user_ihel == -1 && include_helicity_averaging_factors)
  {
    result /= denom_hels[proc_ID]; 
    if (user_icol == -1)
    {
      all_results[proc_ID][0][0] /= denom_hels[proc_ID]; 
      for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
      {
        all_results[proc_ID][0][i + 1] /= denom_hels[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][0][user_icol + 1] /= denom_hels[proc_ID]; 
    }
  }

  // Normalize when summing+averaging over color configurations
  if (user_icol == -1 && include_color_averaging_factors)
  {
    result /= denom_colors[proc_ID]; 
    if (user_ihel == -1)
    {
      all_results[proc_ID][0][0] /= denom_colors[proc_ID]; 
      for (unsigned int i = 0; i < ncomb; i++ )
      {
        all_results[proc_ID][i + 1][0] /= denom_colors[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][user_ihel + 1][0] /= denom_colors[proc_ID]; 
    }
  }

  // Reinstate previous values of mME
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (saved_mME[i] < 0.0)
    {
      mME[i] = saved_mME[i]; 
    }
  }

  // Finally return it
  return result; 
}

//==========================================================================
// Private class member functions

//--------------------------------------------------------------------------
// Evaluate |M|^2 for each subprocess

void PY8MEs_R12_P34_sm_gq_wpzqqq::calculate_wavefunctions(const int hel[])
{
  // Calculate wavefunctions for all processes
  // Calculate all wavefunctions
  vxxxxx(p[perm[0]], mME[0], hel[0], -1, w[0]); 
  ixxxxx(p[perm[1]], mME[1], hel[1], +1, w[1]); 
  vxxxxx(p[perm[2]], mME[2], hel[2], +1, w[2]); 
  vxxxxx(p[perm[3]], mME[3], hel[3], +1, w[3]); 
  oxxxxx(p[perm[4]], mME[4], hel[4], +1, w[4]); 
  oxxxxx(p[perm[5]], mME[5], hel[5], +1, w[5]); 
  ixxxxx(p[perm[6]], mME[6], hel[6], -1, w[6]); 
  FFV1_2(w[1], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[7]); 
  VVV1_1(w[2], w[3], pars->GC_53, pars->mdl_MW, pars->mdl_WW, w[8]); 
  FFV1P0_3(w[7], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[9]); 
  FFV2_1(w[5], w[8], pars->GC_100, pars->ZERO, pars->ZERO, w[10]); 
  FFV2_2(w[6], w[8], pars->GC_100, pars->ZERO, pars->ZERO, w[11]); 
  FFV1P0_3(w[6], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[12]); 
  FFV2_2(w[7], w[8], pars->GC_100, pars->ZERO, pars->ZERO, w[13]); 
  FFV1_2(w[7], w[12], pars->GC_11, pars->ZERO, pars->ZERO, w[14]); 
  FFV2_1(w[5], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[15]); 
  FFV2_5_2(w[7], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[16]);
  FFV1P0_3(w[6], w[15], pars->GC_11, pars->ZERO, pars->ZERO, w[17]); 
  FFV2_5_1(w[15], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[18]);
  FFV2_5_1(w[4], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[19]);
  FFV1P0_3(w[7], w[15], pars->GC_11, pars->ZERO, pars->ZERO, w[20]); 
  FFV1P0_3(w[7], w[19], pars->GC_11, pars->ZERO, pars->ZERO, w[21]); 
  FFV2_5_2(w[6], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[22]);
  FFV2_2(w[6], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[23]); 
  FFV1P0_3(w[23], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[24]); 
  FFV2_3_2(w[23], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[25]);
  FFV2_3_1(w[5], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[26]);
  FFV2_2(w[7], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[27]); 
  FFV1P0_3(w[6], w[19], pars->GC_11, pars->ZERO, pars->ZERO, w[28]); 
  FFV2_1(w[26], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[29]); 
  FFV1P0_3(w[22], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[30]); 
  FFV2_2(w[22], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[31]); 
  FFV1_1(w[5], w[12], pars->GC_11, pars->ZERO, pars->ZERO, w[32]); 
  FFV1_1(w[4], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[33]); 
  FFV2_2(w[1], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[34]); 
  FFV2_5_1(w[33], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[35]);
  FFV1P0_3(w[34], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[36]); 
  FFV1P0_3(w[6], w[33], pars->GC_11, pars->ZERO, pars->ZERO, w[37]); 
  FFV2_3_2(w[34], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[38]);
  FFV1P0_3(w[22], w[33], pars->GC_11, pars->ZERO, pars->ZERO, w[39]); 
  FFV2_5_2(w[1], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[40]);
  FFV2_2(w[40], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[41]); 
  FFV1P0_3(w[40], w[33], pars->GC_11, pars->ZERO, pars->ZERO, w[42]); 
  FFV1P0_3(w[1], w[33], pars->GC_11, pars->ZERO, pars->ZERO, w[43]); 
  FFV2_2(w[1], w[8], pars->GC_100, pars->ZERO, pars->ZERO, w[44]); 
  FFV1P0_3(w[1], w[15], pars->GC_11, pars->ZERO, pars->ZERO, w[45]); 
  FFV1_1(w[5], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[46]); 
  FFV1P0_3(w[34], w[46], pars->GC_11, pars->ZERO, pars->ZERO, w[47]); 
  FFV2_3_1(w[46], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[48]);
  FFV1_1(w[46], w[12], pars->GC_11, pars->ZERO, pars->ZERO, w[49]); 
  FFV2_1(w[46], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[50]); 
  FFV1P0_3(w[40], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[51]); 
  FFV1P0_3(w[23], w[46], pars->GC_11, pars->ZERO, pars->ZERO, w[52]); 
  FFV1P0_3(w[1], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[53]); 
  FFV1_2(w[6], w[53], pars->GC_11, pars->ZERO, pars->ZERO, w[54]); 
  FFV1_1(w[46], w[53], pars->GC_11, pars->ZERO, pars->ZERO, w[55]); 
  FFV2_1(w[46], w[8], pars->GC_100, pars->ZERO, pars->ZERO, w[56]); 
  FFV1P0_3(w[1], w[19], pars->GC_11, pars->ZERO, pars->ZERO, w[57]); 
  FFV1_2(w[1], w[12], pars->GC_11, pars->ZERO, pars->ZERO, w[58]); 
  FFV1_2(w[6], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[59]); 
  FFV2_5_2(w[59], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[60]);
  FFV1P0_3(w[59], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[61]); 
  FFV1P0_3(w[59], w[19], pars->GC_11, pars->ZERO, pars->ZERO, w[62]); 
  FFV2_2(w[59], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[63]); 
  FFV1P0_3(w[59], w[15], pars->GC_11, pars->ZERO, pars->ZERO, w[64]); 
  FFV1_1(w[5], w[53], pars->GC_11, pars->ZERO, pars->ZERO, w[65]); 
  FFV1_2(w[59], w[53], pars->GC_11, pars->ZERO, pars->ZERO, w[66]); 
  FFV2_2(w[59], w[8], pars->GC_100, pars->ZERO, pars->ZERO, w[67]); 
  FFV1_2(w[34], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[68]); 
  FFV1_1(w[19], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[69]); 
  FFV1_1(w[26], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[70]); 
  VVV1P0_1(w[0], w[12], pars->GC_10, pars->ZERO, pars->ZERO, w[71]); 
  FFV1_2(w[22], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[72]); 
  FFV1_2(w[40], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[73]); 
  FFV1_1(w[15], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[74]); 
  FFV1_2(w[23], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[75]); 
  VVV1P0_1(w[0], w[53], pars->GC_10, pars->ZERO, pars->ZERO, w[76]); 
  FFV1P0_3(w[6], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[77]); 
  FFV1_2(w[7], w[77], pars->GC_11, pars->ZERO, pars->ZERO, w[78]); 
  FFV2_1(w[4], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[79]); 
  FFV1P0_3(w[7], w[79], pars->GC_11, pars->ZERO, pars->ZERO, w[80]); 
  FFV2_3_2(w[6], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[81]);
  FFV2_3_1(w[4], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[82]);
  FFV1P0_3(w[6], w[82], pars->GC_11, pars->ZERO, pars->ZERO, w[83]); 
  FFV1P0_3(w[6], w[26], pars->GC_11, pars->ZERO, pars->ZERO, w[84]); 
  FFV1P0_3(w[81], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[85]); 
  FFV1P0_3(w[81], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[86]); 
  FFV1_1(w[4], w[77], pars->GC_11, pars->ZERO, pars->ZERO, w[87]); 
  FFV2_3_1(w[33], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[88]);
  FFV1P0_3(w[34], w[33], pars->GC_11, pars->ZERO, pars->ZERO, w[89]); 
  FFV1P0_3(w[81], w[33], pars->GC_11, pars->ZERO, pars->ZERO, w[90]); 
  FFV1_1(w[33], w[77], pars->GC_11, pars->ZERO, pars->ZERO, w[91]); 
  FFV2_1(w[33], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[92]); 
  FFV2_1(w[33], w[8], pars->GC_100, pars->ZERO, pars->ZERO, w[93]); 
  FFV1_2(w[1], w[77], pars->GC_11, pars->ZERO, pars->ZERO, w[94]); 
  FFV1P0_3(w[34], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[95]); 
  FFV1P0_3(w[6], w[46], pars->GC_11, pars->ZERO, pars->ZERO, w[96]); 
  FFV1P0_3(w[81], w[46], pars->GC_11, pars->ZERO, pars->ZERO, w[97]); 
  FFV2_1(w[4], w[8], pars->GC_100, pars->ZERO, pars->ZERO, w[98]); 
  FFV1P0_3(w[1], w[79], pars->GC_11, pars->ZERO, pars->ZERO, w[99]); 
  FFV2_5_1(w[79], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[100]);
  FFV2_1(w[82], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[101]); 
  FFV2_3_2(w[59], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[102]);
  FFV1P0_3(w[59], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[103]); 
  FFV1P0_3(w[59], w[82], pars->GC_11, pars->ZERO, pars->ZERO, w[104]); 
  FFV1P0_3(w[59], w[26], pars->GC_11, pars->ZERO, pars->ZERO, w[105]); 
  FFV1_1(w[82], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[106]); 
  VVV1P0_1(w[0], w[77], pars->GC_10, pars->ZERO, pars->ZERO, w[107]); 
  FFV1_2(w[81], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[108]); 
  FFV1_1(w[79], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[109]); 
  FFV1P0_3(w[1], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[110]); 
  FFV1_2(w[59], w[110], pars->GC_11, pars->ZERO, pars->ZERO, w[111]); 
  FFV1P0_3(w[59], w[79], pars->GC_11, pars->ZERO, pars->ZERO, w[112]); 
  FFV2_3_2(w[1], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[113]);
  FFV1P0_3(w[1], w[82], pars->GC_11, pars->ZERO, pars->ZERO, w[114]); 
  FFV1P0_3(w[1], w[26], pars->GC_11, pars->ZERO, pars->ZERO, w[115]); 
  FFV1P0_3(w[113], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[116]); 
  FFV1P0_3(w[113], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[117]); 
  FFV1_1(w[4], w[110], pars->GC_11, pars->ZERO, pars->ZERO, w[118]); 
  FFV1P0_3(w[23], w[33], pars->GC_11, pars->ZERO, pars->ZERO, w[119]); 
  FFV1P0_3(w[113], w[33], pars->GC_11, pars->ZERO, pars->ZERO, w[120]); 
  FFV1_1(w[33], w[110], pars->GC_11, pars->ZERO, pars->ZERO, w[121]); 
  FFV1_2(w[6], w[110], pars->GC_11, pars->ZERO, pars->ZERO, w[122]); 
  FFV1P0_3(w[23], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[123]); 
  FFV1P0_3(w[1], w[46], pars->GC_11, pars->ZERO, pars->ZERO, w[124]); 
  FFV1P0_3(w[113], w[46], pars->GC_11, pars->ZERO, pars->ZERO, w[125]); 
  FFV1P0_3(w[6], w[79], pars->GC_11, pars->ZERO, pars->ZERO, w[126]); 
  FFV2_3_2(w[7], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[127]);
  FFV1P0_3(w[7], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[128]); 
  FFV1P0_3(w[7], w[82], pars->GC_11, pars->ZERO, pars->ZERO, w[129]); 
  FFV1P0_3(w[7], w[26], pars->GC_11, pars->ZERO, pars->ZERO, w[130]); 
  VVV1P0_1(w[0], w[110], pars->GC_10, pars->ZERO, pars->ZERO, w[131]); 
  FFV1_2(w[113], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[132]); 
  oxxxxx(p[perm[1]], mME[1], hel[1], -1, w[133]); 
  ixxxxx(p[perm[5]], mME[5], hel[5], -1, w[134]); 
  FFV1_2(w[134], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[135]); 
  FFV1P0_3(w[135], w[133], pars->GC_11, pars->ZERO, pars->ZERO, w[136]); 
  FFV1P0_3(w[6], w[133], pars->GC_11, pars->ZERO, pars->ZERO, w[137]); 
  FFV2_2(w[135], w[8], pars->GC_100, pars->ZERO, pars->ZERO, w[138]); 
  FFV1_2(w[135], w[137], pars->GC_11, pars->ZERO, pars->ZERO, w[139]); 
  FFV2_5_2(w[135], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[140]);
  FFV2_5_1(w[133], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[141]);
  FFV1P0_3(w[135], w[79], pars->GC_11, pars->ZERO, pars->ZERO, w[142]); 
  FFV1P0_3(w[135], w[141], pars->GC_11, pars->ZERO, pars->ZERO, w[143]); 
  FFV2_2(w[135], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[144]); 
  FFV1P0_3(w[6], w[141], pars->GC_11, pars->ZERO, pars->ZERO, w[145]); 
  FFV1P0_3(w[22], w[133], pars->GC_11, pars->ZERO, pars->ZERO, w[146]); 
  FFV1_1(w[4], w[137], pars->GC_11, pars->ZERO, pars->ZERO, w[147]); 
  FFV1_1(w[133], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[148]); 
  FFV2_2(w[134], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[149]); 
  FFV2_5_1(w[148], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[150]);
  FFV1P0_3(w[149], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[151]); 
  FFV1P0_3(w[6], w[148], pars->GC_11, pars->ZERO, pars->ZERO, w[152]); 
  FFV2_3_2(w[149], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[153]);
  FFV1P0_3(w[22], w[148], pars->GC_11, pars->ZERO, pars->ZERO, w[154]); 
  FFV2_5_2(w[134], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[155]);
  FFV2_2(w[155], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[156]); 
  FFV1P0_3(w[155], w[148], pars->GC_11, pars->ZERO, pars->ZERO, w[157]); 
  FFV1P0_3(w[134], w[148], pars->GC_11, pars->ZERO, pars->ZERO, w[158]); 
  FFV2_2(w[134], w[8], pars->GC_100, pars->ZERO, pars->ZERO, w[159]); 
  FFV1P0_3(w[134], w[79], pars->GC_11, pars->ZERO, pars->ZERO, w[160]); 
  FFV1P0_3(w[149], w[33], pars->GC_11, pars->ZERO, pars->ZERO, w[161]); 
  FFV1_1(w[33], w[137], pars->GC_11, pars->ZERO, pars->ZERO, w[162]); 
  FFV1P0_3(w[155], w[133], pars->GC_11, pars->ZERO, pars->ZERO, w[163]); 
  FFV1P0_3(w[134], w[133], pars->GC_11, pars->ZERO, pars->ZERO, w[164]); 
  FFV1_2(w[6], w[164], pars->GC_11, pars->ZERO, pars->ZERO, w[165]); 
  FFV1_1(w[33], w[164], pars->GC_11, pars->ZERO, pars->ZERO, w[166]); 
  FFV1P0_3(w[134], w[141], pars->GC_11, pars->ZERO, pars->ZERO, w[167]); 
  FFV1_2(w[134], w[137], pars->GC_11, pars->ZERO, pars->ZERO, w[168]); 
  FFV1P0_3(w[59], w[133], pars->GC_11, pars->ZERO, pars->ZERO, w[169]); 
  FFV1P0_3(w[59], w[141], pars->GC_11, pars->ZERO, pars->ZERO, w[170]); 
  FFV1_1(w[4], w[164], pars->GC_11, pars->ZERO, pars->ZERO, w[171]); 
  FFV1_2(w[59], w[164], pars->GC_11, pars->ZERO, pars->ZERO, w[172]); 
  FFV1_2(w[149], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[173]); 
  FFV1_1(w[141], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[174]); 
  VVV1P0_1(w[0], w[137], pars->GC_10, pars->ZERO, pars->ZERO, w[175]); 
  FFV1_2(w[155], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[176]); 
  VVV1P0_1(w[0], w[164], pars->GC_10, pars->ZERO, pars->ZERO, w[177]); 
  FFV1P0_3(w[135], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[178]); 
  FFV2_1(w[133], w[8], pars->GC_100, pars->ZERO, pars->ZERO, w[179]); 
  FFV1_2(w[135], w[12], pars->GC_11, pars->ZERO, pars->ZERO, w[180]); 
  FFV2_1(w[133], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[181]); 
  FFV1P0_3(w[6], w[181], pars->GC_11, pars->ZERO, pars->ZERO, w[182]); 
  FFV2_5_1(w[181], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[183]);
  FFV1P0_3(w[135], w[181], pars->GC_11, pars->ZERO, pars->ZERO, w[184]); 
  FFV1P0_3(w[135], w[19], pars->GC_11, pars->ZERO, pars->ZERO, w[185]); 
  FFV1P0_3(w[23], w[133], pars->GC_11, pars->ZERO, pars->ZERO, w[186]); 
  FFV2_3_1(w[133], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[187]);
  FFV2_1(w[187], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[188]); 
  FFV1_1(w[133], w[12], pars->GC_11, pars->ZERO, pars->ZERO, w[189]); 
  FFV1P0_3(w[149], w[133], pars->GC_11, pars->ZERO, pars->ZERO, w[190]); 
  FFV1P0_3(w[155], w[33], pars->GC_11, pars->ZERO, pars->ZERO, w[191]); 
  FFV1P0_3(w[134], w[33], pars->GC_11, pars->ZERO, pars->ZERO, w[192]); 
  FFV1P0_3(w[134], w[181], pars->GC_11, pars->ZERO, pars->ZERO, w[193]); 
  FFV1P0_3(w[149], w[148], pars->GC_11, pars->ZERO, pars->ZERO, w[194]); 
  FFV2_3_1(w[148], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[195]);
  FFV1_1(w[148], w[12], pars->GC_11, pars->ZERO, pars->ZERO, w[196]); 
  FFV2_1(w[148], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[197]); 
  FFV1P0_3(w[155], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[198]); 
  FFV1P0_3(w[23], w[148], pars->GC_11, pars->ZERO, pars->ZERO, w[199]); 
  FFV1P0_3(w[134], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[200]); 
  FFV1_2(w[6], w[200], pars->GC_11, pars->ZERO, pars->ZERO, w[201]); 
  FFV1_1(w[148], w[200], pars->GC_11, pars->ZERO, pars->ZERO, w[202]); 
  FFV2_1(w[148], w[8], pars->GC_100, pars->ZERO, pars->ZERO, w[203]); 
  FFV1P0_3(w[134], w[19], pars->GC_11, pars->ZERO, pars->ZERO, w[204]); 
  FFV1_2(w[134], w[12], pars->GC_11, pars->ZERO, pars->ZERO, w[205]); 
  FFV1P0_3(w[59], w[181], pars->GC_11, pars->ZERO, pars->ZERO, w[206]); 
  FFV1_1(w[133], w[200], pars->GC_11, pars->ZERO, pars->ZERO, w[207]); 
  FFV1_2(w[59], w[200], pars->GC_11, pars->ZERO, pars->ZERO, w[208]); 
  FFV1_1(w[187], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[209]); 
  FFV1_1(w[181], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[210]); 
  VVV1P0_1(w[0], w[200], pars->GC_10, pars->ZERO, pars->ZERO, w[211]); 
  FFV1P0_3(w[6], w[187], pars->GC_11, pars->ZERO, pars->ZERO, w[212]); 
  FFV1P0_3(w[81], w[133], pars->GC_11, pars->ZERO, pars->ZERO, w[213]); 
  FFV1P0_3(w[81], w[148], pars->GC_11, pars->ZERO, pars->ZERO, w[214]); 
  FFV1P0_3(w[59], w[187], pars->GC_11, pars->ZERO, pars->ZERO, w[215]); 

  // Calculate all amplitudes
  // Amplitude(s) for diagram number 0
  FFV1_0(w[6], w[10], w[9], pars->GC_11, amp[0]); 
  FFV1_0(w[11], w[5], w[9], pars->GC_11, amp[1]); 
  FFV1_0(w[13], w[5], w[12], pars->GC_11, amp[2]); 
  FFV2_0(w[14], w[5], w[8], pars->GC_100, amp[3]); 
  FFV1_0(w[16], w[4], w[17], pars->GC_11, amp[4]); 
  FFV1_0(w[6], w[18], w[9], pars->GC_11, amp[5]); 
  FFV1_0(w[6], w[19], w[20], pars->GC_11, amp[6]); 
  FFV1_0(w[6], w[15], w[21], pars->GC_11, amp[7]); 
  FFV1_0(w[22], w[4], w[20], pars->GC_11, amp[8]); 
  FFV1_0(w[22], w[15], w[9], pars->GC_11, amp[9]); 
  FFV1_0(w[16], w[15], w[12], pars->GC_11, amp[10]); 
  FFV2_5_0(w[14], w[15], w[3], pars->GC_51, pars->GC_58, amp[11]); 
  FFV1_0(w[16], w[4], w[24], pars->GC_11, amp[12]); 
  FFV1_0(w[25], w[5], w[9], pars->GC_11, amp[13]); 
  FFV1_0(w[23], w[5], w[21], pars->GC_11, amp[14]); 
  FFV1_0(w[23], w[26], w[9], pars->GC_11, amp[15]); 
  FFV1_0(w[27], w[5], w[28], pars->GC_11, amp[16]); 
  FFV1_0(w[6], w[29], w[9], pars->GC_11, amp[17]); 
  FFV1_0(w[27], w[26], w[12], pars->GC_11, amp[18]); 
  FFV2_0(w[14], w[26], w[2], pars->GC_100, amp[19]); 
  FFV1_0(w[27], w[5], w[30], pars->GC_11, amp[20]); 
  FFV1_0(w[31], w[5], w[9], pars->GC_11, amp[21]); 
  FFV2_3_0(w[27], w[32], w[3], pars->GC_50, pars->GC_58, amp[22]); 
  FFV2_0(w[16], w[32], w[2], pars->GC_100, amp[23]); 
  FFV1_0(w[6], w[35], w[36], pars->GC_11, amp[24]); 
  FFV1_0(w[38], w[5], w[37], pars->GC_11, amp[25]); 
  FFV1_0(w[34], w[26], w[37], pars->GC_11, amp[26]); 
  FFV1_0(w[34], w[5], w[39], pars->GC_11, amp[27]); 
  FFV1_0(w[41], w[5], w[37], pars->GC_11, amp[28]); 
  FFV1_0(w[6], w[15], w[42], pars->GC_11, amp[29]); 
  FFV1_0(w[40], w[15], w[37], pars->GC_11, amp[30]); 
  FFV1_0(w[23], w[5], w[42], pars->GC_11, amp[31]); 
  FFV1_0(w[6], w[10], w[43], pars->GC_11, amp[32]); 
  FFV1_0(w[11], w[5], w[43], pars->GC_11, amp[33]); 
  FFV1_0(w[44], w[5], w[37], pars->GC_11, amp[34]); 
  FFV1_0(w[1], w[10], w[37], pars->GC_11, amp[35]); 
  FFV1_0(w[6], w[18], w[43], pars->GC_11, amp[36]); 
  FFV1_0(w[6], w[35], w[45], pars->GC_11, amp[37]); 
  FFV1_0(w[1], w[35], w[17], pars->GC_11, amp[38]); 
  FFV1_0(w[1], w[18], w[37], pars->GC_11, amp[39]); 
  FFV1_0(w[22], w[15], w[43], pars->GC_11, amp[40]); 
  FFV1_0(w[1], w[15], w[39], pars->GC_11, amp[41]); 
  FFV1_0(w[25], w[5], w[43], pars->GC_11, amp[42]); 
  FFV1_0(w[1], w[35], w[24], pars->GC_11, amp[43]); 
  FFV1_0(w[23], w[26], w[43], pars->GC_11, amp[44]); 
  FFV1_0(w[6], w[29], w[43], pars->GC_11, amp[45]); 
  FFV1_0(w[1], w[29], w[37], pars->GC_11, amp[46]); 
  FFV1_0(w[31], w[5], w[43], pars->GC_11, amp[47]); 
  FFV1_0(w[6], w[19], w[47], pars->GC_11, amp[48]); 
  FFV1_0(w[22], w[4], w[47], pars->GC_11, amp[49]); 
  FFV1_0(w[34], w[48], w[12], pars->GC_11, amp[50]); 
  FFV2_3_0(w[34], w[49], w[3], pars->GC_50, pars->GC_58, amp[51]); 
  FFV1_0(w[6], w[50], w[51], pars->GC_11, amp[52]); 
  FFV1_0(w[40], w[4], w[52], pars->GC_11, amp[53]); 
  FFV1_0(w[40], w[50], w[12], pars->GC_11, amp[54]); 
  FFV2_0(w[40], w[49], w[2], pars->GC_100, amp[55]); 
  FFV2_5_0(w[54], w[50], w[3], pars->GC_51, pars->GC_58, amp[56]); 
  FFV2_0(w[54], w[48], w[2], pars->GC_100, amp[57]); 
  FFV2_0(w[6], w[55], w[8], pars->GC_100, amp[58]); 
  FFV1_0(w[6], w[56], w[53], pars->GC_11, amp[59]); 
  FFV2_3_0(w[23], w[55], w[3], pars->GC_50, pars->GC_58, amp[60]); 
  FFV1_0(w[23], w[48], w[53], pars->GC_11, amp[61]); 
  FFV2_0(w[22], w[55], w[2], pars->GC_100, amp[62]); 
  FFV1_0(w[22], w[50], w[53], pars->GC_11, amp[63]); 
  FFV1_0(w[1], w[56], w[12], pars->GC_11, amp[64]); 
  FFV2_0(w[1], w[49], w[8], pars->GC_100, amp[65]); 
  FFV1_0(w[1], w[19], w[52], pars->GC_11, amp[66]); 
  FFV1_0(w[6], w[50], w[57], pars->GC_11, amp[67]); 
  FFV1_0(w[1], w[50], w[28], pars->GC_11, amp[68]); 
  FFV1_0(w[1], w[50], w[30], pars->GC_11, amp[69]); 
  FFV2_5_0(w[58], w[50], w[3], pars->GC_51, pars->GC_58, amp[70]); 
  FFV2_0(w[58], w[48], w[2], pars->GC_100, amp[71]); 
  FFV1_0(w[60], w[4], w[36], pars->GC_11, amp[72]); 
  FFV1_0(w[38], w[5], w[61], pars->GC_11, amp[73]); 
  FFV1_0(w[34], w[5], w[62], pars->GC_11, amp[74]); 
  FFV1_0(w[34], w[26], w[61], pars->GC_11, amp[75]); 
  FFV1_0(w[63], w[5], w[51], pars->GC_11, amp[76]); 
  FFV1_0(w[41], w[5], w[61], pars->GC_11, amp[77]); 
  FFV1_0(w[40], w[4], w[64], pars->GC_11, amp[78]); 
  FFV1_0(w[40], w[15], w[61], pars->GC_11, amp[79]); 
  FFV2_3_0(w[63], w[65], w[3], pars->GC_50, pars->GC_58, amp[80]); 
  FFV2_0(w[60], w[65], w[2], pars->GC_100, amp[81]); 
  FFV2_0(w[66], w[5], w[8], pars->GC_100, amp[82]); 
  FFV1_0(w[67], w[5], w[53], pars->GC_11, amp[83]); 
  FFV2_5_0(w[66], w[15], w[3], pars->GC_51, pars->GC_58, amp[84]); 
  FFV1_0(w[60], w[15], w[53], pars->GC_11, amp[85]); 
  FFV2_0(w[66], w[26], w[2], pars->GC_100, amp[86]); 
  FFV1_0(w[63], w[26], w[53], pars->GC_11, amp[87]); 
  FFV1_0(w[44], w[5], w[61], pars->GC_11, amp[88]); 
  FFV1_0(w[1], w[10], w[61], pars->GC_11, amp[89]); 
  FFV1_0(w[60], w[4], w[45], pars->GC_11, amp[90]); 
  FFV1_0(w[1], w[18], w[61], pars->GC_11, amp[91]); 
  FFV1_0(w[1], w[19], w[64], pars->GC_11, amp[92]); 
  FFV1_0(w[1], w[15], w[62], pars->GC_11, amp[93]); 
  FFV1_0(w[63], w[5], w[57], pars->GC_11, amp[94]); 
  FFV1_0(w[1], w[29], w[61], pars->GC_11, amp[95]); 
  FFV1_0(w[68], w[5], w[28], pars->GC_11, amp[96]); 
  FFV1_0(w[6], w[69], w[36], pars->GC_11, amp[97]); 
  VVV1_0(w[0], w[36], w[28], pars->GC_10, amp[98]); 
  FFV1_0(w[68], w[26], w[12], pars->GC_11, amp[99]); 
  FFV1_0(w[34], w[70], w[12], pars->GC_11, amp[100]); 
  FFV1_0(w[34], w[26], w[71], pars->GC_11, amp[101]); 
  FFV1_0(w[68], w[5], w[30], pars->GC_11, amp[102]); 
  FFV1_0(w[72], w[4], w[36], pars->GC_11, amp[103]); 
  VVV1_0(w[0], w[36], w[30], pars->GC_10, amp[104]); 
  FFV2_3_0(w[68], w[32], w[3], pars->GC_50, pars->GC_58, amp[105]); 
  FFV1_0(w[38], w[5], w[71], pars->GC_11, amp[106]); 
  FFV1_0(w[38], w[32], w[0], pars->GC_11, amp[107]); 
  FFV1_0(w[73], w[4], w[17], pars->GC_11, amp[108]); 
  FFV1_0(w[6], w[74], w[51], pars->GC_11, amp[109]); 
  VVV1_0(w[0], w[51], w[17], pars->GC_10, amp[110]); 
  FFV1_0(w[73], w[15], w[12], pars->GC_11, amp[111]); 
  FFV1_0(w[40], w[74], w[12], pars->GC_11, amp[112]); 
  FFV1_0(w[40], w[15], w[71], pars->GC_11, amp[113]); 
  FFV1_0(w[73], w[4], w[24], pars->GC_11, amp[114]); 
  FFV1_0(w[75], w[5], w[51], pars->GC_11, amp[115]); 
  VVV1_0(w[0], w[51], w[24], pars->GC_10, amp[116]); 
  FFV2_0(w[73], w[32], w[2], pars->GC_100, amp[117]); 
  FFV1_0(w[41], w[5], w[71], pars->GC_11, amp[118]); 
  FFV1_0(w[41], w[32], w[0], pars->GC_11, amp[119]); 
  FFV1_0(w[6], w[10], w[76], pars->GC_11, amp[120]); 
  FFV1_0(w[11], w[5], w[76], pars->GC_11, amp[121]); 
  FFV1_0(w[11], w[65], w[0], pars->GC_11, amp[122]); 
  FFV1_0(w[54], w[10], w[0], pars->GC_11, amp[123]); 
  FFV1_0(w[6], w[18], w[76], pars->GC_11, amp[124]); 
  FFV2_5_0(w[54], w[74], w[3], pars->GC_51, pars->GC_58, amp[125]); 
  FFV1_0(w[54], w[18], w[0], pars->GC_11, amp[126]); 
  FFV1_0(w[22], w[15], w[76], pars->GC_11, amp[127]); 
  FFV1_0(w[22], w[74], w[53], pars->GC_11, amp[128]); 
  FFV1_0(w[72], w[15], w[53], pars->GC_11, amp[129]); 
  FFV1_0(w[25], w[5], w[76], pars->GC_11, amp[130]); 
  FFV2_3_0(w[75], w[65], w[3], pars->GC_50, pars->GC_58, amp[131]); 
  FFV1_0(w[25], w[65], w[0], pars->GC_11, amp[132]); 
  FFV1_0(w[23], w[26], w[76], pars->GC_11, amp[133]); 
  FFV1_0(w[75], w[26], w[53], pars->GC_11, amp[134]); 
  FFV1_0(w[23], w[70], w[53], pars->GC_11, amp[135]); 
  FFV1_0(w[6], w[29], w[76], pars->GC_11, amp[136]); 
  FFV2_0(w[54], w[70], w[2], pars->GC_100, amp[137]); 
  FFV1_0(w[54], w[29], w[0], pars->GC_11, amp[138]); 
  FFV1_0(w[31], w[5], w[76], pars->GC_11, amp[139]); 
  FFV2_0(w[72], w[65], w[2], pars->GC_100, amp[140]); 
  FFV1_0(w[31], w[65], w[0], pars->GC_11, amp[141]); 
  FFV1_0(w[44], w[5], w[71], pars->GC_11, amp[142]); 
  FFV1_0(w[1], w[10], w[71], pars->GC_11, amp[143]); 
  FFV1_0(w[44], w[32], w[0], pars->GC_11, amp[144]); 
  FFV1_0(w[58], w[10], w[0], pars->GC_11, amp[145]); 
  FFV1_0(w[6], w[74], w[57], pars->GC_11, amp[146]); 
  FFV1_0(w[1], w[74], w[28], pars->GC_11, amp[147]); 
  FFV1_0(w[6], w[69], w[45], pars->GC_11, amp[148]); 
  FFV1_0(w[1], w[69], w[17], pars->GC_11, amp[149]); 
  VVV1_0(w[0], w[45], w[28], pars->GC_10, amp[150]); 
  VVV1_0(w[0], w[57], w[17], pars->GC_10, amp[151]); 
  FFV1_0(w[1], w[74], w[30], pars->GC_11, amp[152]); 
  FFV1_0(w[72], w[4], w[45], pars->GC_11, amp[153]); 
  VVV1_0(w[0], w[45], w[30], pars->GC_10, amp[154]); 
  FFV2_5_0(w[58], w[74], w[3], pars->GC_51, pars->GC_58, amp[155]); 
  FFV1_0(w[1], w[18], w[71], pars->GC_11, amp[156]); 
  FFV1_0(w[58], w[18], w[0], pars->GC_11, amp[157]); 
  FFV1_0(w[75], w[5], w[57], pars->GC_11, amp[158]); 
  FFV1_0(w[1], w[69], w[24], pars->GC_11, amp[159]); 
  VVV1_0(w[0], w[57], w[24], pars->GC_10, amp[160]); 
  FFV2_0(w[58], w[70], w[2], pars->GC_100, amp[161]); 
  FFV1_0(w[1], w[29], w[71], pars->GC_11, amp[162]); 
  FFV1_0(w[58], w[29], w[0], pars->GC_11, amp[163]); 
  FFV1_0(w[13], w[5], w[12], pars->GC_11, amp[164]); 
  FFV2_0(w[14], w[5], w[8], pars->GC_100, amp[165]); 
  FFV1_0(w[13], w[4], w[77], pars->GC_11, amp[166]); 
  FFV2_0(w[78], w[4], w[8], pars->GC_100, amp[167]); 
  FFV1_0(w[6], w[26], w[80], pars->GC_11, amp[168]); 
  FFV1_0(w[81], w[5], w[80], pars->GC_11, amp[169]); 
  FFV1_0(w[16], w[79], w[77], pars->GC_11, amp[170]); 
  FFV2_5_0(w[78], w[79], w[3], pars->GC_51, pars->GC_58, amp[171]); 
  FFV1_0(w[6], w[82], w[20], pars->GC_11, amp[172]); 
  FFV1_0(w[81], w[4], w[20], pars->GC_11, amp[173]); 
  FFV1_0(w[27], w[5], w[83], pars->GC_11, amp[174]); 
  FFV1_0(w[27], w[82], w[77], pars->GC_11, amp[175]); 
  FFV2_0(w[78], w[82], w[2], pars->GC_100, amp[176]); 
  FFV1_0(w[27], w[4], w[84], pars->GC_11, amp[177]); 
  FFV1_0(w[27], w[26], w[12], pars->GC_11, amp[178]); 
  FFV2_0(w[14], w[26], w[2], pars->GC_100, amp[179]); 
  FFV1_0(w[27], w[5], w[85], pars->GC_11, amp[180]); 
  FFV1_0(w[27], w[4], w[86], pars->GC_11, amp[181]); 
  FFV2_3_0(w[27], w[32], w[3], pars->GC_50, pars->GC_58, amp[182]); 
  FFV2_0(w[16], w[32], w[2], pars->GC_100, amp[183]); 
  FFV2_3_0(w[27], w[87], w[3], pars->GC_50, pars->GC_58, amp[184]); 
  FFV2_0(w[16], w[87], w[2], pars->GC_100, amp[185]); 
  FFV1_0(w[6], w[88], w[36], pars->GC_11, amp[186]); 
  FFV1_0(w[6], w[26], w[89], pars->GC_11, amp[187]); 
  FFV1_0(w[34], w[26], w[37], pars->GC_11, amp[188]); 
  FFV1_0(w[81], w[5], w[89], pars->GC_11, amp[189]); 
  FFV1_0(w[34], w[5], w[90], pars->GC_11, amp[190]); 
  FFV1_0(w[34], w[88], w[77], pars->GC_11, amp[191]); 
  FFV2_3_0(w[34], w[91], w[3], pars->GC_50, pars->GC_58, amp[192]); 
  FFV1_0(w[41], w[5], w[37], pars->GC_11, amp[193]); 
  FFV1_0(w[40], w[15], w[37], pars->GC_11, amp[194]); 
  FFV1_0(w[40], w[92], w[77], pars->GC_11, amp[195]); 
  FFV2_0(w[40], w[91], w[2], pars->GC_100, amp[196]); 
  FFV1_0(w[44], w[5], w[37], pars->GC_11, amp[197]); 
  FFV1_0(w[1], w[10], w[37], pars->GC_11, amp[198]); 
  FFV1_0(w[1], w[93], w[77], pars->GC_11, amp[199]); 
  FFV2_0(w[1], w[91], w[8], pars->GC_100, amp[200]); 
  FFV1_0(w[6], w[88], w[45], pars->GC_11, amp[201]); 
  FFV1_0(w[1], w[18], w[37], pars->GC_11, amp[202]); 
  FFV1_0(w[1], w[15], w[90], pars->GC_11, amp[203]); 
  FFV1_0(w[1], w[92], w[84], pars->GC_11, amp[204]); 
  FFV1_0(w[1], w[29], w[37], pars->GC_11, amp[205]); 
  FFV1_0(w[1], w[92], w[86], pars->GC_11, amp[206]); 
  FFV2_5_0(w[94], w[92], w[3], pars->GC_51, pars->GC_58, amp[207]); 
  FFV2_0(w[94], w[88], w[2], pars->GC_100, amp[208]); 
  FFV1_0(w[6], w[48], w[95], pars->GC_11, amp[209]); 
  FFV1_0(w[38], w[4], w[96], pars->GC_11, amp[210]); 
  FFV1_0(w[6], w[82], w[47], pars->GC_11, amp[211]); 
  FFV1_0(w[34], w[82], w[96], pars->GC_11, amp[212]); 
  FFV1_0(w[81], w[4], w[47], pars->GC_11, amp[213]); 
  FFV1_0(w[34], w[4], w[97], pars->GC_11, amp[214]); 
  FFV1_0(w[34], w[48], w[12], pars->GC_11, amp[215]); 
  FFV2_3_0(w[34], w[49], w[3], pars->GC_50, pars->GC_58, amp[216]); 
  FFV1_0(w[41], w[4], w[96], pars->GC_11, amp[217]); 
  FFV1_0(w[40], w[79], w[96], pars->GC_11, amp[218]); 
  FFV1_0(w[40], w[50], w[12], pars->GC_11, amp[219]); 
  FFV2_0(w[40], w[49], w[2], pars->GC_100, amp[220]); 
  FFV1_0(w[44], w[4], w[96], pars->GC_11, amp[221]); 
  FFV1_0(w[1], w[98], w[96], pars->GC_11, amp[222]); 
  FFV1_0(w[1], w[56], w[12], pars->GC_11, amp[223]); 
  FFV2_0(w[1], w[49], w[8], pars->GC_100, amp[224]); 
  FFV1_0(w[6], w[48], w[99], pars->GC_11, amp[225]); 
  FFV1_0(w[1], w[100], w[96], pars->GC_11, amp[226]); 
  FFV1_0(w[1], w[79], w[97], pars->GC_11, amp[227]); 
  FFV1_0(w[1], w[50], w[83], pars->GC_11, amp[228]); 
  FFV1_0(w[1], w[101], w[96], pars->GC_11, amp[229]); 
  FFV1_0(w[1], w[50], w[85], pars->GC_11, amp[230]); 
  FFV1_0(w[102], w[5], w[95], pars->GC_11, amp[231]); 
  FFV1_0(w[102], w[4], w[36], pars->GC_11, amp[232]); 
  FFV1_0(w[38], w[5], w[61], pars->GC_11, amp[233]); 
  FFV1_0(w[38], w[4], w[103], pars->GC_11, amp[234]); 
  FFV1_0(w[34], w[5], w[104], pars->GC_11, amp[235]); 
  FFV1_0(w[34], w[82], w[103], pars->GC_11, amp[236]); 
  FFV1_0(w[34], w[4], w[105], pars->GC_11, amp[237]); 
  FFV1_0(w[34], w[26], w[61], pars->GC_11, amp[238]); 
  FFV1_0(w[41], w[5], w[61], pars->GC_11, amp[239]); 
  FFV1_0(w[41], w[4], w[103], pars->GC_11, amp[240]); 
  FFV1_0(w[40], w[79], w[103], pars->GC_11, amp[241]); 
  FFV1_0(w[40], w[15], w[61], pars->GC_11, amp[242]); 
  FFV1_0(w[44], w[5], w[61], pars->GC_11, amp[243]); 
  FFV1_0(w[1], w[10], w[61], pars->GC_11, amp[244]); 
  FFV1_0(w[44], w[4], w[103], pars->GC_11, amp[245]); 
  FFV1_0(w[1], w[98], w[103], pars->GC_11, amp[246]); 
  FFV1_0(w[102], w[5], w[99], pars->GC_11, amp[247]); 
  FFV1_0(w[1], w[100], w[103], pars->GC_11, amp[248]); 
  FFV1_0(w[1], w[79], w[105], pars->GC_11, amp[249]); 
  FFV1_0(w[102], w[4], w[45], pars->GC_11, amp[250]); 
  FFV1_0(w[1], w[18], w[61], pars->GC_11, amp[251]); 
  FFV1_0(w[1], w[15], w[104], pars->GC_11, amp[252]); 
  FFV1_0(w[1], w[101], w[103], pars->GC_11, amp[253]); 
  FFV1_0(w[68], w[5], w[83], pars->GC_11, amp[254]); 
  FFV1_0(w[6], w[106], w[36], pars->GC_11, amp[255]); 
  VVV1_0(w[0], w[36], w[83], pars->GC_10, amp[256]); 
  FFV1_0(w[68], w[82], w[77], pars->GC_11, amp[257]); 
  FFV1_0(w[34], w[106], w[77], pars->GC_11, amp[258]); 
  FFV1_0(w[34], w[82], w[107], pars->GC_11, amp[259]); 
  FFV1_0(w[68], w[4], w[84], pars->GC_11, amp[260]); 
  FFV1_0(w[6], w[70], w[95], pars->GC_11, amp[261]); 
  VVV1_0(w[0], w[95], w[84], pars->GC_10, amp[262]); 
  FFV1_0(w[68], w[26], w[12], pars->GC_11, amp[263]); 
  FFV1_0(w[34], w[70], w[12], pars->GC_11, amp[264]); 
  FFV1_0(w[34], w[26], w[71], pars->GC_11, amp[265]); 
  FFV1_0(w[68], w[5], w[85], pars->GC_11, amp[266]); 
  FFV1_0(w[68], w[4], w[86], pars->GC_11, amp[267]); 
  FFV1_0(w[108], w[5], w[95], pars->GC_11, amp[268]); 
  FFV1_0(w[108], w[4], w[36], pars->GC_11, amp[269]); 
  VVV1_0(w[0], w[95], w[86], pars->GC_10, amp[270]); 
  VVV1_0(w[0], w[36], w[85], pars->GC_10, amp[271]); 
  FFV2_3_0(w[68], w[32], w[3], pars->GC_50, pars->GC_58, amp[272]); 
  FFV1_0(w[38], w[5], w[71], pars->GC_11, amp[273]); 
  FFV1_0(w[38], w[32], w[0], pars->GC_11, amp[274]); 
  FFV2_3_0(w[68], w[87], w[3], pars->GC_50, pars->GC_58, amp[275]); 
  FFV1_0(w[38], w[4], w[107], pars->GC_11, amp[276]); 
  FFV1_0(w[38], w[87], w[0], pars->GC_11, amp[277]); 
  FFV1_0(w[73], w[79], w[77], pars->GC_11, amp[278]); 
  FFV1_0(w[40], w[109], w[77], pars->GC_11, amp[279]); 
  FFV1_0(w[40], w[79], w[107], pars->GC_11, amp[280]); 
  FFV1_0(w[73], w[15], w[12], pars->GC_11, amp[281]); 
  FFV1_0(w[40], w[74], w[12], pars->GC_11, amp[282]); 
  FFV1_0(w[40], w[15], w[71], pars->GC_11, amp[283]); 
  FFV2_0(w[73], w[32], w[2], pars->GC_100, amp[284]); 
  FFV1_0(w[41], w[5], w[71], pars->GC_11, amp[285]); 
  FFV1_0(w[41], w[32], w[0], pars->GC_11, amp[286]); 
  FFV2_0(w[73], w[87], w[2], pars->GC_100, amp[287]); 
  FFV1_0(w[41], w[4], w[107], pars->GC_11, amp[288]); 
  FFV1_0(w[41], w[87], w[0], pars->GC_11, amp[289]); 
  FFV1_0(w[44], w[5], w[71], pars->GC_11, amp[290]); 
  FFV1_0(w[1], w[10], w[71], pars->GC_11, amp[291]); 
  FFV1_0(w[44], w[32], w[0], pars->GC_11, amp[292]); 
  FFV1_0(w[58], w[10], w[0], pars->GC_11, amp[293]); 
  FFV1_0(w[44], w[4], w[107], pars->GC_11, amp[294]); 
  FFV1_0(w[1], w[98], w[107], pars->GC_11, amp[295]); 
  FFV1_0(w[44], w[87], w[0], pars->GC_11, amp[296]); 
  FFV1_0(w[94], w[98], w[0], pars->GC_11, amp[297]); 
  FFV1_0(w[1], w[109], w[84], pars->GC_11, amp[298]); 
  FFV1_0(w[6], w[70], w[99], pars->GC_11, amp[299]); 
  VVV1_0(w[0], w[99], w[84], pars->GC_10, amp[300]); 
  FFV1_0(w[1], w[109], w[86], pars->GC_11, amp[301]); 
  FFV1_0(w[108], w[5], w[99], pars->GC_11, amp[302]); 
  VVV1_0(w[0], w[99], w[86], pars->GC_10, amp[303]); 
  FFV2_5_0(w[94], w[109], w[3], pars->GC_51, pars->GC_58, amp[304]); 
  FFV1_0(w[1], w[100], w[107], pars->GC_11, amp[305]); 
  FFV1_0(w[94], w[100], w[0], pars->GC_11, amp[306]); 
  FFV1_0(w[1], w[74], w[83], pars->GC_11, amp[307]); 
  FFV1_0(w[6], w[106], w[45], pars->GC_11, amp[308]); 
  VVV1_0(w[0], w[45], w[83], pars->GC_10, amp[309]); 
  FFV1_0(w[1], w[74], w[85], pars->GC_11, amp[310]); 
  FFV1_0(w[108], w[4], w[45], pars->GC_11, amp[311]); 
  VVV1_0(w[0], w[45], w[85], pars->GC_10, amp[312]); 
  FFV2_0(w[94], w[106], w[2], pars->GC_100, amp[313]); 
  FFV1_0(w[1], w[101], w[107], pars->GC_11, amp[314]); 
  FFV1_0(w[94], w[101], w[0], pars->GC_11, amp[315]); 
  FFV1_0(w[67], w[5], w[53], pars->GC_11, amp[316]); 
  FFV2_0(w[66], w[5], w[8], pars->GC_100, amp[317]); 
  FFV1_0(w[67], w[4], w[110], pars->GC_11, amp[318]); 
  FFV2_0(w[111], w[4], w[8], pars->GC_100, amp[319]); 
  FFV1_0(w[1], w[26], w[112], pars->GC_11, amp[320]); 
  FFV1_0(w[113], w[5], w[112], pars->GC_11, amp[321]); 
  FFV1_0(w[60], w[79], w[110], pars->GC_11, amp[322]); 
  FFV2_5_0(w[111], w[79], w[3], pars->GC_51, pars->GC_58, amp[323]); 
  FFV1_0(w[1], w[82], w[64], pars->GC_11, amp[324]); 
  FFV1_0(w[113], w[4], w[64], pars->GC_11, amp[325]); 
  FFV1_0(w[60], w[15], w[53], pars->GC_11, amp[326]); 
  FFV2_5_0(w[66], w[15], w[3], pars->GC_51, pars->GC_58, amp[327]); 
  FFV1_0(w[63], w[5], w[114], pars->GC_11, amp[328]); 
  FFV1_0(w[63], w[82], w[110], pars->GC_11, amp[329]); 
  FFV2_0(w[111], w[82], w[2], pars->GC_100, amp[330]); 
  FFV1_0(w[63], w[4], w[115], pars->GC_11, amp[331]); 
  FFV1_0(w[63], w[26], w[53], pars->GC_11, amp[332]); 
  FFV2_0(w[66], w[26], w[2], pars->GC_100, amp[333]); 
  FFV1_0(w[63], w[5], w[116], pars->GC_11, amp[334]); 
  FFV1_0(w[63], w[4], w[117], pars->GC_11, amp[335]); 
  FFV2_3_0(w[63], w[65], w[3], pars->GC_50, pars->GC_58, amp[336]); 
  FFV2_0(w[60], w[65], w[2], pars->GC_100, amp[337]); 
  FFV2_3_0(w[63], w[118], w[3], pars->GC_50, pars->GC_58, amp[338]); 
  FFV2_0(w[60], w[118], w[2], pars->GC_100, amp[339]); 
  FFV1_0(w[1], w[88], w[24], pars->GC_11, amp[340]); 
  FFV1_0(w[25], w[5], w[43], pars->GC_11, amp[341]); 
  FFV1_0(w[1], w[26], w[119], pars->GC_11, amp[342]); 
  FFV1_0(w[23], w[26], w[43], pars->GC_11, amp[343]); 
  FFV1_0(w[113], w[5], w[119], pars->GC_11, amp[344]); 
  FFV1_0(w[23], w[5], w[120], pars->GC_11, amp[345]); 
  FFV1_0(w[23], w[88], w[110], pars->GC_11, amp[346]); 
  FFV2_3_0(w[23], w[121], w[3], pars->GC_50, pars->GC_58, amp[347]); 
  FFV1_0(w[31], w[5], w[43], pars->GC_11, amp[348]); 
  FFV1_0(w[22], w[15], w[43], pars->GC_11, amp[349]); 
  FFV1_0(w[22], w[92], w[110], pars->GC_11, amp[350]); 
  FFV2_0(w[22], w[121], w[2], pars->GC_100, amp[351]); 
  FFV1_0(w[11], w[5], w[43], pars->GC_11, amp[352]); 
  FFV1_0(w[6], w[10], w[43], pars->GC_11, amp[353]); 
  FFV1_0(w[6], w[93], w[110], pars->GC_11, amp[354]); 
  FFV2_0(w[6], w[121], w[8], pars->GC_100, amp[355]); 
  FFV1_0(w[1], w[88], w[17], pars->GC_11, amp[356]); 
  FFV1_0(w[6], w[18], w[43], pars->GC_11, amp[357]); 
  FFV1_0(w[6], w[15], w[120], pars->GC_11, amp[358]); 
  FFV1_0(w[6], w[92], w[115], pars->GC_11, amp[359]); 
  FFV1_0(w[6], w[29], w[43], pars->GC_11, amp[360]); 
  FFV1_0(w[6], w[92], w[117], pars->GC_11, amp[361]); 
  FFV2_5_0(w[122], w[92], w[3], pars->GC_51, pars->GC_58, amp[362]); 
  FFV2_0(w[122], w[88], w[2], pars->GC_100, amp[363]); 
  FFV1_0(w[1], w[48], w[123], pars->GC_11, amp[364]); 
  FFV1_0(w[25], w[4], w[124], pars->GC_11, amp[365]); 
  FFV1_0(w[1], w[82], w[52], pars->GC_11, amp[366]); 
  FFV1_0(w[23], w[82], w[124], pars->GC_11, amp[367]); 
  FFV1_0(w[113], w[4], w[52], pars->GC_11, amp[368]); 
  FFV1_0(w[23], w[4], w[125], pars->GC_11, amp[369]); 
  FFV1_0(w[23], w[48], w[53], pars->GC_11, amp[370]); 
  FFV2_3_0(w[23], w[55], w[3], pars->GC_50, pars->GC_58, amp[371]); 
  FFV1_0(w[31], w[4], w[124], pars->GC_11, amp[372]); 
  FFV1_0(w[22], w[79], w[124], pars->GC_11, amp[373]); 
  FFV1_0(w[22], w[50], w[53], pars->GC_11, amp[374]); 
  FFV2_0(w[22], w[55], w[2], pars->GC_100, amp[375]); 
  FFV1_0(w[11], w[4], w[124], pars->GC_11, amp[376]); 
  FFV1_0(w[6], w[98], w[124], pars->GC_11, amp[377]); 
  FFV1_0(w[6], w[56], w[53], pars->GC_11, amp[378]); 
  FFV2_0(w[6], w[55], w[8], pars->GC_100, amp[379]); 
  FFV1_0(w[1], w[48], w[126], pars->GC_11, amp[380]); 
  FFV1_0(w[6], w[100], w[124], pars->GC_11, amp[381]); 
  FFV1_0(w[6], w[79], w[125], pars->GC_11, amp[382]); 
  FFV1_0(w[6], w[50], w[114], pars->GC_11, amp[383]); 
  FFV1_0(w[6], w[101], w[124], pars->GC_11, amp[384]); 
  FFV1_0(w[6], w[50], w[116], pars->GC_11, amp[385]); 
  FFV2_5_0(w[54], w[50], w[3], pars->GC_51, pars->GC_58, amp[386]); 
  FFV2_0(w[54], w[48], w[2], pars->GC_100, amp[387]); 
  FFV1_0(w[127], w[5], w[123], pars->GC_11, amp[388]); 
  FFV1_0(w[127], w[4], w[24], pars->GC_11, amp[389]); 
  FFV1_0(w[25], w[5], w[9], pars->GC_11, amp[390]); 
  FFV1_0(w[25], w[4], w[128], pars->GC_11, amp[391]); 
  FFV1_0(w[23], w[5], w[129], pars->GC_11, amp[392]); 
  FFV1_0(w[23], w[82], w[128], pars->GC_11, amp[393]); 
  FFV1_0(w[23], w[4], w[130], pars->GC_11, amp[394]); 
  FFV1_0(w[23], w[26], w[9], pars->GC_11, amp[395]); 
  FFV1_0(w[31], w[5], w[9], pars->GC_11, amp[396]); 
  FFV1_0(w[31], w[4], w[128], pars->GC_11, amp[397]); 
  FFV1_0(w[22], w[79], w[128], pars->GC_11, amp[398]); 
  FFV1_0(w[22], w[15], w[9], pars->GC_11, amp[399]); 
  FFV1_0(w[11], w[5], w[9], pars->GC_11, amp[400]); 
  FFV1_0(w[6], w[10], w[9], pars->GC_11, amp[401]); 
  FFV1_0(w[11], w[4], w[128], pars->GC_11, amp[402]); 
  FFV1_0(w[6], w[98], w[128], pars->GC_11, amp[403]); 
  FFV1_0(w[127], w[5], w[126], pars->GC_11, amp[404]); 
  FFV1_0(w[6], w[100], w[128], pars->GC_11, amp[405]); 
  FFV1_0(w[6], w[79], w[130], pars->GC_11, amp[406]); 
  FFV1_0(w[127], w[4], w[17], pars->GC_11, amp[407]); 
  FFV1_0(w[6], w[18], w[9], pars->GC_11, amp[408]); 
  FFV1_0(w[6], w[15], w[129], pars->GC_11, amp[409]); 
  FFV1_0(w[6], w[101], w[128], pars->GC_11, amp[410]); 
  FFV1_0(w[6], w[29], w[9], pars->GC_11, amp[411]); 
  FFV1_0(w[75], w[5], w[114], pars->GC_11, amp[412]); 
  FFV1_0(w[1], w[106], w[24], pars->GC_11, amp[413]); 
  VVV1_0(w[0], w[24], w[114], pars->GC_10, amp[414]); 
  FFV1_0(w[75], w[82], w[110], pars->GC_11, amp[415]); 
  FFV1_0(w[23], w[106], w[110], pars->GC_11, amp[416]); 
  FFV1_0(w[23], w[82], w[131], pars->GC_11, amp[417]); 
  FFV1_0(w[75], w[4], w[115], pars->GC_11, amp[418]); 
  FFV1_0(w[1], w[70], w[123], pars->GC_11, amp[419]); 
  VVV1_0(w[0], w[123], w[115], pars->GC_10, amp[420]); 
  FFV1_0(w[75], w[26], w[53], pars->GC_11, amp[421]); 
  FFV1_0(w[23], w[70], w[53], pars->GC_11, amp[422]); 
  FFV1_0(w[23], w[26], w[76], pars->GC_11, amp[423]); 
  FFV1_0(w[75], w[5], w[116], pars->GC_11, amp[424]); 
  FFV1_0(w[75], w[4], w[117], pars->GC_11, amp[425]); 
  FFV1_0(w[132], w[5], w[123], pars->GC_11, amp[426]); 
  FFV1_0(w[132], w[4], w[24], pars->GC_11, amp[427]); 
  VVV1_0(w[0], w[123], w[117], pars->GC_10, amp[428]); 
  VVV1_0(w[0], w[24], w[116], pars->GC_10, amp[429]); 
  FFV2_3_0(w[75], w[65], w[3], pars->GC_50, pars->GC_58, amp[430]); 
  FFV1_0(w[25], w[5], w[76], pars->GC_11, amp[431]); 
  FFV1_0(w[25], w[65], w[0], pars->GC_11, amp[432]); 
  FFV2_3_0(w[75], w[118], w[3], pars->GC_50, pars->GC_58, amp[433]); 
  FFV1_0(w[25], w[4], w[131], pars->GC_11, amp[434]); 
  FFV1_0(w[25], w[118], w[0], pars->GC_11, amp[435]); 
  FFV1_0(w[72], w[79], w[110], pars->GC_11, amp[436]); 
  FFV1_0(w[22], w[109], w[110], pars->GC_11, amp[437]); 
  FFV1_0(w[22], w[79], w[131], pars->GC_11, amp[438]); 
  FFV1_0(w[72], w[15], w[53], pars->GC_11, amp[439]); 
  FFV1_0(w[22], w[74], w[53], pars->GC_11, amp[440]); 
  FFV1_0(w[22], w[15], w[76], pars->GC_11, amp[441]); 
  FFV2_0(w[72], w[65], w[2], pars->GC_100, amp[442]); 
  FFV1_0(w[31], w[5], w[76], pars->GC_11, amp[443]); 
  FFV1_0(w[31], w[65], w[0], pars->GC_11, amp[444]); 
  FFV2_0(w[72], w[118], w[2], pars->GC_100, amp[445]); 
  FFV1_0(w[31], w[4], w[131], pars->GC_11, amp[446]); 
  FFV1_0(w[31], w[118], w[0], pars->GC_11, amp[447]); 
  FFV1_0(w[11], w[5], w[76], pars->GC_11, amp[448]); 
  FFV1_0(w[6], w[10], w[76], pars->GC_11, amp[449]); 
  FFV1_0(w[11], w[65], w[0], pars->GC_11, amp[450]); 
  FFV1_0(w[54], w[10], w[0], pars->GC_11, amp[451]); 
  FFV1_0(w[11], w[4], w[131], pars->GC_11, amp[452]); 
  FFV1_0(w[6], w[98], w[131], pars->GC_11, amp[453]); 
  FFV1_0(w[11], w[118], w[0], pars->GC_11, amp[454]); 
  FFV1_0(w[122], w[98], w[0], pars->GC_11, amp[455]); 
  FFV1_0(w[6], w[109], w[115], pars->GC_11, amp[456]); 
  FFV1_0(w[1], w[70], w[126], pars->GC_11, amp[457]); 
  VVV1_0(w[0], w[126], w[115], pars->GC_10, amp[458]); 
  FFV1_0(w[6], w[109], w[117], pars->GC_11, amp[459]); 
  FFV1_0(w[132], w[5], w[126], pars->GC_11, amp[460]); 
  VVV1_0(w[0], w[126], w[117], pars->GC_10, amp[461]); 
  FFV2_5_0(w[122], w[109], w[3], pars->GC_51, pars->GC_58, amp[462]); 
  FFV1_0(w[6], w[100], w[131], pars->GC_11, amp[463]); 
  FFV1_0(w[122], w[100], w[0], pars->GC_11, amp[464]); 
  FFV1_0(w[6], w[74], w[114], pars->GC_11, amp[465]); 
  FFV1_0(w[1], w[106], w[17], pars->GC_11, amp[466]); 
  VVV1_0(w[0], w[17], w[114], pars->GC_10, amp[467]); 
  FFV1_0(w[6], w[74], w[116], pars->GC_11, amp[468]); 
  FFV1_0(w[132], w[4], w[17], pars->GC_11, amp[469]); 
  VVV1_0(w[0], w[17], w[116], pars->GC_10, amp[470]); 
  FFV2_5_0(w[54], w[74], w[3], pars->GC_51, pars->GC_58, amp[471]); 
  FFV1_0(w[6], w[18], w[76], pars->GC_11, amp[472]); 
  FFV1_0(w[54], w[18], w[0], pars->GC_11, amp[473]); 
  FFV2_0(w[122], w[106], w[2], pars->GC_100, amp[474]); 
  FFV1_0(w[6], w[101], w[131], pars->GC_11, amp[475]); 
  FFV1_0(w[122], w[101], w[0], pars->GC_11, amp[476]); 
  FFV2_0(w[54], w[70], w[2], pars->GC_100, amp[477]); 
  FFV1_0(w[6], w[29], w[76], pars->GC_11, amp[478]); 
  FFV1_0(w[54], w[29], w[0], pars->GC_11, amp[479]); 
  FFV1_0(w[6], w[98], w[136], pars->GC_11, amp[480]); 
  FFV1_0(w[11], w[4], w[136], pars->GC_11, amp[481]); 
  FFV1_0(w[138], w[4], w[137], pars->GC_11, amp[482]); 
  FFV2_0(w[139], w[4], w[8], pars->GC_100, amp[483]); 
  FFV1_0(w[140], w[133], w[126], pars->GC_11, amp[484]); 
  FFV1_0(w[6], w[100], w[136], pars->GC_11, amp[485]); 
  FFV1_0(w[6], w[141], w[142], pars->GC_11, amp[486]); 
  FFV1_0(w[6], w[79], w[143], pars->GC_11, amp[487]); 
  FFV1_0(w[22], w[133], w[142], pars->GC_11, amp[488]); 
  FFV1_0(w[22], w[79], w[136], pars->GC_11, amp[489]); 
  FFV1_0(w[140], w[79], w[137], pars->GC_11, amp[490]); 
  FFV2_5_0(w[139], w[79], w[3], pars->GC_51, pars->GC_58, amp[491]); 
  FFV1_0(w[140], w[133], w[123], pars->GC_11, amp[492]); 
  FFV1_0(w[25], w[4], w[136], pars->GC_11, amp[493]); 
  FFV1_0(w[23], w[4], w[143], pars->GC_11, amp[494]); 
  FFV1_0(w[23], w[82], w[136], pars->GC_11, amp[495]); 
  FFV1_0(w[144], w[4], w[145], pars->GC_11, amp[496]); 
  FFV1_0(w[6], w[101], w[136], pars->GC_11, amp[497]); 
  FFV1_0(w[144], w[82], w[137], pars->GC_11, amp[498]); 
  FFV2_0(w[139], w[82], w[2], pars->GC_100, amp[499]); 
  FFV1_0(w[144], w[4], w[146], pars->GC_11, amp[500]); 
  FFV1_0(w[31], w[4], w[136], pars->GC_11, amp[501]); 
  FFV2_3_0(w[144], w[147], w[3], pars->GC_50, pars->GC_58, amp[502]); 
  FFV2_0(w[140], w[147], w[2], pars->GC_100, amp[503]); 
  FFV1_0(w[6], w[150], w[151], pars->GC_11, amp[504]); 
  FFV1_0(w[153], w[4], w[152], pars->GC_11, amp[505]); 
  FFV1_0(w[149], w[82], w[152], pars->GC_11, amp[506]); 
  FFV1_0(w[149], w[4], w[154], pars->GC_11, amp[507]); 
  FFV1_0(w[156], w[4], w[152], pars->GC_11, amp[508]); 
  FFV1_0(w[6], w[79], w[157], pars->GC_11, amp[509]); 
  FFV1_0(w[155], w[79], w[152], pars->GC_11, amp[510]); 
  FFV1_0(w[23], w[4], w[157], pars->GC_11, amp[511]); 
  FFV1_0(w[6], w[98], w[158], pars->GC_11, amp[512]); 
  FFV1_0(w[11], w[4], w[158], pars->GC_11, amp[513]); 
  FFV1_0(w[159], w[4], w[152], pars->GC_11, amp[514]); 
  FFV1_0(w[134], w[98], w[152], pars->GC_11, amp[515]); 
  FFV1_0(w[6], w[100], w[158], pars->GC_11, amp[516]); 
  FFV1_0(w[6], w[150], w[160], pars->GC_11, amp[517]); 
  FFV1_0(w[134], w[150], w[126], pars->GC_11, amp[518]); 
  FFV1_0(w[134], w[100], w[152], pars->GC_11, amp[519]); 
  FFV1_0(w[22], w[79], w[158], pars->GC_11, amp[520]); 
  FFV1_0(w[134], w[79], w[154], pars->GC_11, amp[521]); 
  FFV1_0(w[25], w[4], w[158], pars->GC_11, amp[522]); 
  FFV1_0(w[134], w[150], w[123], pars->GC_11, amp[523]); 
  FFV1_0(w[23], w[82], w[158], pars->GC_11, amp[524]); 
  FFV1_0(w[6], w[101], w[158], pars->GC_11, amp[525]); 
  FFV1_0(w[134], w[101], w[152], pars->GC_11, amp[526]); 
  FFV1_0(w[31], w[4], w[158], pars->GC_11, amp[527]); 
  FFV1_0(w[6], w[141], w[161], pars->GC_11, amp[528]); 
  FFV1_0(w[22], w[133], w[161], pars->GC_11, amp[529]); 
  FFV1_0(w[149], w[88], w[137], pars->GC_11, amp[530]); 
  FFV2_3_0(w[149], w[162], w[3], pars->GC_50, pars->GC_58, amp[531]); 
  FFV1_0(w[6], w[92], w[163], pars->GC_11, amp[532]); 
  FFV1_0(w[155], w[133], w[119], pars->GC_11, amp[533]); 
  FFV1_0(w[155], w[92], w[137], pars->GC_11, amp[534]); 
  FFV2_0(w[155], w[162], w[2], pars->GC_100, amp[535]); 
  FFV2_5_0(w[165], w[92], w[3], pars->GC_51, pars->GC_58, amp[536]); 
  FFV2_0(w[165], w[88], w[2], pars->GC_100, amp[537]); 
  FFV2_0(w[6], w[166], w[8], pars->GC_100, amp[538]); 
  FFV1_0(w[6], w[93], w[164], pars->GC_11, amp[539]); 
  FFV2_3_0(w[23], w[166], w[3], pars->GC_50, pars->GC_58, amp[540]); 
  FFV1_0(w[23], w[88], w[164], pars->GC_11, amp[541]); 
  FFV2_0(w[22], w[166], w[2], pars->GC_100, amp[542]); 
  FFV1_0(w[22], w[92], w[164], pars->GC_11, amp[543]); 
  FFV1_0(w[134], w[93], w[137], pars->GC_11, amp[544]); 
  FFV2_0(w[134], w[162], w[8], pars->GC_100, amp[545]); 
  FFV1_0(w[134], w[141], w[119], pars->GC_11, amp[546]); 
  FFV1_0(w[6], w[92], w[167], pars->GC_11, amp[547]); 
  FFV1_0(w[134], w[92], w[145], pars->GC_11, amp[548]); 
  FFV1_0(w[134], w[92], w[146], pars->GC_11, amp[549]); 
  FFV2_5_0(w[168], w[92], w[3], pars->GC_51, pars->GC_58, amp[550]); 
  FFV2_0(w[168], w[88], w[2], pars->GC_100, amp[551]); 
  FFV1_0(w[60], w[133], w[151], pars->GC_11, amp[552]); 
  FFV1_0(w[153], w[4], w[169], pars->GC_11, amp[553]); 
  FFV1_0(w[149], w[4], w[170], pars->GC_11, amp[554]); 
  FFV1_0(w[149], w[82], w[169], pars->GC_11, amp[555]); 
  FFV1_0(w[63], w[4], w[163], pars->GC_11, amp[556]); 
  FFV1_0(w[156], w[4], w[169], pars->GC_11, amp[557]); 
  FFV1_0(w[155], w[133], w[112], pars->GC_11, amp[558]); 
  FFV1_0(w[155], w[79], w[169], pars->GC_11, amp[559]); 
  FFV2_3_0(w[63], w[171], w[3], pars->GC_50, pars->GC_58, amp[560]); 
  FFV2_0(w[60], w[171], w[2], pars->GC_100, amp[561]); 
  FFV2_0(w[172], w[4], w[8], pars->GC_100, amp[562]); 
  FFV1_0(w[67], w[4], w[164], pars->GC_11, amp[563]); 
  FFV2_5_0(w[172], w[79], w[3], pars->GC_51, pars->GC_58, amp[564]); 
  FFV1_0(w[60], w[79], w[164], pars->GC_11, amp[565]); 
  FFV2_0(w[172], w[82], w[2], pars->GC_100, amp[566]); 
  FFV1_0(w[63], w[82], w[164], pars->GC_11, amp[567]); 
  FFV1_0(w[159], w[4], w[169], pars->GC_11, amp[568]); 
  FFV1_0(w[134], w[98], w[169], pars->GC_11, amp[569]); 
  FFV1_0(w[60], w[133], w[160], pars->GC_11, amp[570]); 
  FFV1_0(w[134], w[100], w[169], pars->GC_11, amp[571]); 
  FFV1_0(w[134], w[141], w[112], pars->GC_11, amp[572]); 
  FFV1_0(w[134], w[79], w[170], pars->GC_11, amp[573]); 
  FFV1_0(w[63], w[4], w[167], pars->GC_11, amp[574]); 
  FFV1_0(w[134], w[101], w[169], pars->GC_11, amp[575]); 
  FFV1_0(w[173], w[4], w[145], pars->GC_11, amp[576]); 
  FFV1_0(w[6], w[174], w[151], pars->GC_11, amp[577]); 
  VVV1_0(w[0], w[151], w[145], pars->GC_10, amp[578]); 
  FFV1_0(w[173], w[82], w[137], pars->GC_11, amp[579]); 
  FFV1_0(w[149], w[106], w[137], pars->GC_11, amp[580]); 
  FFV1_0(w[149], w[82], w[175], pars->GC_11, amp[581]); 
  FFV1_0(w[173], w[4], w[146], pars->GC_11, amp[582]); 
  FFV1_0(w[72], w[133], w[151], pars->GC_11, amp[583]); 
  VVV1_0(w[0], w[151], w[146], pars->GC_10, amp[584]); 
  FFV2_3_0(w[173], w[147], w[3], pars->GC_50, pars->GC_58, amp[585]); 
  FFV1_0(w[153], w[4], w[175], pars->GC_11, amp[586]); 
  FFV1_0(w[153], w[147], w[0], pars->GC_11, amp[587]); 
  FFV1_0(w[176], w[133], w[126], pars->GC_11, amp[588]); 
  FFV1_0(w[6], w[109], w[163], pars->GC_11, amp[589]); 
  VVV1_0(w[0], w[163], w[126], pars->GC_10, amp[590]); 
  FFV1_0(w[176], w[79], w[137], pars->GC_11, amp[591]); 
  FFV1_0(w[155], w[109], w[137], pars->GC_11, amp[592]); 
  FFV1_0(w[155], w[79], w[175], pars->GC_11, amp[593]); 
  FFV1_0(w[176], w[133], w[123], pars->GC_11, amp[594]); 
  FFV1_0(w[75], w[4], w[163], pars->GC_11, amp[595]); 
  VVV1_0(w[0], w[163], w[123], pars->GC_10, amp[596]); 
  FFV2_0(w[176], w[147], w[2], pars->GC_100, amp[597]); 
  FFV1_0(w[156], w[4], w[175], pars->GC_11, amp[598]); 
  FFV1_0(w[156], w[147], w[0], pars->GC_11, amp[599]); 
  FFV1_0(w[6], w[98], w[177], pars->GC_11, amp[600]); 
  FFV1_0(w[11], w[4], w[177], pars->GC_11, amp[601]); 
  FFV1_0(w[11], w[171], w[0], pars->GC_11, amp[602]); 
  FFV1_0(w[165], w[98], w[0], pars->GC_11, amp[603]); 
  FFV1_0(w[6], w[100], w[177], pars->GC_11, amp[604]); 
  FFV2_5_0(w[165], w[109], w[3], pars->GC_51, pars->GC_58, amp[605]); 
  FFV1_0(w[165], w[100], w[0], pars->GC_11, amp[606]); 
  FFV1_0(w[22], w[79], w[177], pars->GC_11, amp[607]); 
  FFV1_0(w[22], w[109], w[164], pars->GC_11, amp[608]); 
  FFV1_0(w[72], w[79], w[164], pars->GC_11, amp[609]); 
  FFV1_0(w[25], w[4], w[177], pars->GC_11, amp[610]); 
  FFV2_3_0(w[75], w[171], w[3], pars->GC_50, pars->GC_58, amp[611]); 
  FFV1_0(w[25], w[171], w[0], pars->GC_11, amp[612]); 
  FFV1_0(w[23], w[82], w[177], pars->GC_11, amp[613]); 
  FFV1_0(w[75], w[82], w[164], pars->GC_11, amp[614]); 
  FFV1_0(w[23], w[106], w[164], pars->GC_11, amp[615]); 
  FFV1_0(w[6], w[101], w[177], pars->GC_11, amp[616]); 
  FFV2_0(w[165], w[106], w[2], pars->GC_100, amp[617]); 
  FFV1_0(w[165], w[101], w[0], pars->GC_11, amp[618]); 
  FFV1_0(w[31], w[4], w[177], pars->GC_11, amp[619]); 
  FFV2_0(w[72], w[171], w[2], pars->GC_100, amp[620]); 
  FFV1_0(w[31], w[171], w[0], pars->GC_11, amp[621]); 
  FFV1_0(w[159], w[4], w[175], pars->GC_11, amp[622]); 
  FFV1_0(w[134], w[98], w[175], pars->GC_11, amp[623]); 
  FFV1_0(w[159], w[147], w[0], pars->GC_11, amp[624]); 
  FFV1_0(w[168], w[98], w[0], pars->GC_11, amp[625]); 
  FFV1_0(w[6], w[109], w[167], pars->GC_11, amp[626]); 
  FFV1_0(w[134], w[109], w[145], pars->GC_11, amp[627]); 
  FFV1_0(w[6], w[174], w[160], pars->GC_11, amp[628]); 
  FFV1_0(w[134], w[174], w[126], pars->GC_11, amp[629]); 
  VVV1_0(w[0], w[160], w[145], pars->GC_10, amp[630]); 
  VVV1_0(w[0], w[167], w[126], pars->GC_10, amp[631]); 
  FFV1_0(w[134], w[109], w[146], pars->GC_11, amp[632]); 
  FFV1_0(w[72], w[133], w[160], pars->GC_11, amp[633]); 
  VVV1_0(w[0], w[160], w[146], pars->GC_10, amp[634]); 
  FFV2_5_0(w[168], w[109], w[3], pars->GC_51, pars->GC_58, amp[635]); 
  FFV1_0(w[134], w[100], w[175], pars->GC_11, amp[636]); 
  FFV1_0(w[168], w[100], w[0], pars->GC_11, amp[637]); 
  FFV1_0(w[75], w[4], w[167], pars->GC_11, amp[638]); 
  FFV1_0(w[134], w[174], w[123], pars->GC_11, amp[639]); 
  VVV1_0(w[0], w[167], w[123], pars->GC_10, amp[640]); 
  FFV2_0(w[168], w[106], w[2], pars->GC_100, amp[641]); 
  FFV1_0(w[134], w[101], w[175], pars->GC_11, amp[642]); 
  FFV1_0(w[168], w[101], w[0], pars->GC_11, amp[643]); 
  FFV1_0(w[6], w[179], w[178], pars->GC_11, amp[644]); 
  FFV1_0(w[11], w[133], w[178], pars->GC_11, amp[645]); 
  FFV1_0(w[138], w[133], w[12], pars->GC_11, amp[646]); 
  FFV2_0(w[180], w[133], w[8], pars->GC_100, amp[647]); 
  FFV1_0(w[140], w[4], w[182], pars->GC_11, amp[648]); 
  FFV1_0(w[6], w[183], w[178], pars->GC_11, amp[649]); 
  FFV1_0(w[6], w[19], w[184], pars->GC_11, amp[650]); 
  FFV1_0(w[6], w[181], w[185], pars->GC_11, amp[651]); 
  FFV1_0(w[22], w[4], w[184], pars->GC_11, amp[652]); 
  FFV1_0(w[22], w[181], w[178], pars->GC_11, amp[653]); 
  FFV1_0(w[140], w[181], w[12], pars->GC_11, amp[654]); 
  FFV2_5_0(w[180], w[181], w[3], pars->GC_51, pars->GC_58, amp[655]); 
  FFV1_0(w[140], w[4], w[186], pars->GC_11, amp[656]); 
  FFV1_0(w[25], w[133], w[178], pars->GC_11, amp[657]); 
  FFV1_0(w[23], w[133], w[185], pars->GC_11, amp[658]); 
  FFV1_0(w[23], w[187], w[178], pars->GC_11, amp[659]); 
  FFV1_0(w[144], w[133], w[28], pars->GC_11, amp[660]); 
  FFV1_0(w[6], w[188], w[178], pars->GC_11, amp[661]); 
  FFV1_0(w[144], w[187], w[12], pars->GC_11, amp[662]); 
  FFV2_0(w[180], w[187], w[2], pars->GC_100, amp[663]); 
  FFV1_0(w[144], w[133], w[30], pars->GC_11, amp[664]); 
  FFV1_0(w[31], w[133], w[178], pars->GC_11, amp[665]); 
  FFV2_3_0(w[144], w[189], w[3], pars->GC_50, pars->GC_58, amp[666]); 
  FFV2_0(w[140], w[189], w[2], pars->GC_100, amp[667]); 
  FFV1_0(w[6], w[35], w[190], pars->GC_11, amp[668]); 
  FFV1_0(w[153], w[133], w[37], pars->GC_11, amp[669]); 
  FFV1_0(w[149], w[187], w[37], pars->GC_11, amp[670]); 
  FFV1_0(w[149], w[133], w[39], pars->GC_11, amp[671]); 
  FFV1_0(w[156], w[133], w[37], pars->GC_11, amp[672]); 
  FFV1_0(w[6], w[181], w[191], pars->GC_11, amp[673]); 
  FFV1_0(w[155], w[181], w[37], pars->GC_11, amp[674]); 
  FFV1_0(w[23], w[133], w[191], pars->GC_11, amp[675]); 
  FFV1_0(w[6], w[179], w[192], pars->GC_11, amp[676]); 
  FFV1_0(w[11], w[133], w[192], pars->GC_11, amp[677]); 
  FFV1_0(w[159], w[133], w[37], pars->GC_11, amp[678]); 
  FFV1_0(w[134], w[179], w[37], pars->GC_11, amp[679]); 
  FFV1_0(w[6], w[183], w[192], pars->GC_11, amp[680]); 
  FFV1_0(w[6], w[35], w[193], pars->GC_11, amp[681]); 
  FFV1_0(w[134], w[35], w[182], pars->GC_11, amp[682]); 
  FFV1_0(w[134], w[183], w[37], pars->GC_11, amp[683]); 
  FFV1_0(w[22], w[181], w[192], pars->GC_11, amp[684]); 
  FFV1_0(w[134], w[181], w[39], pars->GC_11, amp[685]); 
  FFV1_0(w[25], w[133], w[192], pars->GC_11, amp[686]); 
  FFV1_0(w[134], w[35], w[186], pars->GC_11, amp[687]); 
  FFV1_0(w[23], w[187], w[192], pars->GC_11, amp[688]); 
  FFV1_0(w[6], w[188], w[192], pars->GC_11, amp[689]); 
  FFV1_0(w[134], w[188], w[37], pars->GC_11, amp[690]); 
  FFV1_0(w[31], w[133], w[192], pars->GC_11, amp[691]); 
  FFV1_0(w[6], w[19], w[194], pars->GC_11, amp[692]); 
  FFV1_0(w[22], w[4], w[194], pars->GC_11, amp[693]); 
  FFV1_0(w[149], w[195], w[12], pars->GC_11, amp[694]); 
  FFV2_3_0(w[149], w[196], w[3], pars->GC_50, pars->GC_58, amp[695]); 
  FFV1_0(w[6], w[197], w[198], pars->GC_11, amp[696]); 
  FFV1_0(w[155], w[4], w[199], pars->GC_11, amp[697]); 
  FFV1_0(w[155], w[197], w[12], pars->GC_11, amp[698]); 
  FFV2_0(w[155], w[196], w[2], pars->GC_100, amp[699]); 
  FFV2_5_0(w[201], w[197], w[3], pars->GC_51, pars->GC_58, amp[700]); 
  FFV2_0(w[201], w[195], w[2], pars->GC_100, amp[701]); 
  FFV2_0(w[6], w[202], w[8], pars->GC_100, amp[702]); 
  FFV1_0(w[6], w[203], w[200], pars->GC_11, amp[703]); 
  FFV2_3_0(w[23], w[202], w[3], pars->GC_50, pars->GC_58, amp[704]); 
  FFV1_0(w[23], w[195], w[200], pars->GC_11, amp[705]); 
  FFV2_0(w[22], w[202], w[2], pars->GC_100, amp[706]); 
  FFV1_0(w[22], w[197], w[200], pars->GC_11, amp[707]); 
  FFV1_0(w[134], w[203], w[12], pars->GC_11, amp[708]); 
  FFV2_0(w[134], w[196], w[8], pars->GC_100, amp[709]); 
  FFV1_0(w[134], w[19], w[199], pars->GC_11, amp[710]); 
  FFV1_0(w[6], w[197], w[204], pars->GC_11, amp[711]); 
  FFV1_0(w[134], w[197], w[28], pars->GC_11, amp[712]); 
  FFV1_0(w[134], w[197], w[30], pars->GC_11, amp[713]); 
  FFV2_5_0(w[205], w[197], w[3], pars->GC_51, pars->GC_58, amp[714]); 
  FFV2_0(w[205], w[195], w[2], pars->GC_100, amp[715]); 
  FFV1_0(w[60], w[4], w[190], pars->GC_11, amp[716]); 
  FFV1_0(w[153], w[133], w[61], pars->GC_11, amp[717]); 
  FFV1_0(w[149], w[133], w[62], pars->GC_11, amp[718]); 
  FFV1_0(w[149], w[187], w[61], pars->GC_11, amp[719]); 
  FFV1_0(w[63], w[133], w[198], pars->GC_11, amp[720]); 
  FFV1_0(w[156], w[133], w[61], pars->GC_11, amp[721]); 
  FFV1_0(w[155], w[4], w[206], pars->GC_11, amp[722]); 
  FFV1_0(w[155], w[181], w[61], pars->GC_11, amp[723]); 
  FFV2_3_0(w[63], w[207], w[3], pars->GC_50, pars->GC_58, amp[724]); 
  FFV2_0(w[60], w[207], w[2], pars->GC_100, amp[725]); 
  FFV2_0(w[208], w[133], w[8], pars->GC_100, amp[726]); 
  FFV1_0(w[67], w[133], w[200], pars->GC_11, amp[727]); 
  FFV2_5_0(w[208], w[181], w[3], pars->GC_51, pars->GC_58, amp[728]); 
  FFV1_0(w[60], w[181], w[200], pars->GC_11, amp[729]); 
  FFV2_0(w[208], w[187], w[2], pars->GC_100, amp[730]); 
  FFV1_0(w[63], w[187], w[200], pars->GC_11, amp[731]); 
  FFV1_0(w[159], w[133], w[61], pars->GC_11, amp[732]); 
  FFV1_0(w[134], w[179], w[61], pars->GC_11, amp[733]); 
  FFV1_0(w[60], w[4], w[193], pars->GC_11, amp[734]); 
  FFV1_0(w[134], w[183], w[61], pars->GC_11, amp[735]); 
  FFV1_0(w[134], w[19], w[206], pars->GC_11, amp[736]); 
  FFV1_0(w[134], w[181], w[62], pars->GC_11, amp[737]); 
  FFV1_0(w[63], w[133], w[204], pars->GC_11, amp[738]); 
  FFV1_0(w[134], w[188], w[61], pars->GC_11, amp[739]); 
  FFV1_0(w[173], w[133], w[28], pars->GC_11, amp[740]); 
  FFV1_0(w[6], w[69], w[190], pars->GC_11, amp[741]); 
  VVV1_0(w[0], w[190], w[28], pars->GC_10, amp[742]); 
  FFV1_0(w[173], w[187], w[12], pars->GC_11, amp[743]); 
  FFV1_0(w[149], w[209], w[12], pars->GC_11, amp[744]); 
  FFV1_0(w[149], w[187], w[71], pars->GC_11, amp[745]); 
  FFV1_0(w[173], w[133], w[30], pars->GC_11, amp[746]); 
  FFV1_0(w[72], w[4], w[190], pars->GC_11, amp[747]); 
  VVV1_0(w[0], w[190], w[30], pars->GC_10, amp[748]); 
  FFV2_3_0(w[173], w[189], w[3], pars->GC_50, pars->GC_58, amp[749]); 
  FFV1_0(w[153], w[133], w[71], pars->GC_11, amp[750]); 
  FFV1_0(w[153], w[189], w[0], pars->GC_11, amp[751]); 
  FFV1_0(w[176], w[4], w[182], pars->GC_11, amp[752]); 
  FFV1_0(w[6], w[210], w[198], pars->GC_11, amp[753]); 
  VVV1_0(w[0], w[198], w[182], pars->GC_10, amp[754]); 
  FFV1_0(w[176], w[181], w[12], pars->GC_11, amp[755]); 
  FFV1_0(w[155], w[210], w[12], pars->GC_11, amp[756]); 
  FFV1_0(w[155], w[181], w[71], pars->GC_11, amp[757]); 
  FFV1_0(w[176], w[4], w[186], pars->GC_11, amp[758]); 
  FFV1_0(w[75], w[133], w[198], pars->GC_11, amp[759]); 
  VVV1_0(w[0], w[198], w[186], pars->GC_10, amp[760]); 
  FFV2_0(w[176], w[189], w[2], pars->GC_100, amp[761]); 
  FFV1_0(w[156], w[133], w[71], pars->GC_11, amp[762]); 
  FFV1_0(w[156], w[189], w[0], pars->GC_11, amp[763]); 
  FFV1_0(w[6], w[179], w[211], pars->GC_11, amp[764]); 
  FFV1_0(w[11], w[133], w[211], pars->GC_11, amp[765]); 
  FFV1_0(w[11], w[207], w[0], pars->GC_11, amp[766]); 
  FFV1_0(w[201], w[179], w[0], pars->GC_11, amp[767]); 
  FFV1_0(w[6], w[183], w[211], pars->GC_11, amp[768]); 
  FFV2_5_0(w[201], w[210], w[3], pars->GC_51, pars->GC_58, amp[769]); 
  FFV1_0(w[201], w[183], w[0], pars->GC_11, amp[770]); 
  FFV1_0(w[22], w[181], w[211], pars->GC_11, amp[771]); 
  FFV1_0(w[22], w[210], w[200], pars->GC_11, amp[772]); 
  FFV1_0(w[72], w[181], w[200], pars->GC_11, amp[773]); 
  FFV1_0(w[25], w[133], w[211], pars->GC_11, amp[774]); 
  FFV2_3_0(w[75], w[207], w[3], pars->GC_50, pars->GC_58, amp[775]); 
  FFV1_0(w[25], w[207], w[0], pars->GC_11, amp[776]); 
  FFV1_0(w[23], w[187], w[211], pars->GC_11, amp[777]); 
  FFV1_0(w[75], w[187], w[200], pars->GC_11, amp[778]); 
  FFV1_0(w[23], w[209], w[200], pars->GC_11, amp[779]); 
  FFV1_0(w[6], w[188], w[211], pars->GC_11, amp[780]); 
  FFV2_0(w[201], w[209], w[2], pars->GC_100, amp[781]); 
  FFV1_0(w[201], w[188], w[0], pars->GC_11, amp[782]); 
  FFV1_0(w[31], w[133], w[211], pars->GC_11, amp[783]); 
  FFV2_0(w[72], w[207], w[2], pars->GC_100, amp[784]); 
  FFV1_0(w[31], w[207], w[0], pars->GC_11, amp[785]); 
  FFV1_0(w[159], w[133], w[71], pars->GC_11, amp[786]); 
  FFV1_0(w[134], w[179], w[71], pars->GC_11, amp[787]); 
  FFV1_0(w[159], w[189], w[0], pars->GC_11, amp[788]); 
  FFV1_0(w[205], w[179], w[0], pars->GC_11, amp[789]); 
  FFV1_0(w[6], w[210], w[204], pars->GC_11, amp[790]); 
  FFV1_0(w[134], w[210], w[28], pars->GC_11, amp[791]); 
  FFV1_0(w[6], w[69], w[193], pars->GC_11, amp[792]); 
  FFV1_0(w[134], w[69], w[182], pars->GC_11, amp[793]); 
  VVV1_0(w[0], w[193], w[28], pars->GC_10, amp[794]); 
  VVV1_0(w[0], w[204], w[182], pars->GC_10, amp[795]); 
  FFV1_0(w[134], w[210], w[30], pars->GC_11, amp[796]); 
  FFV1_0(w[72], w[4], w[193], pars->GC_11, amp[797]); 
  VVV1_0(w[0], w[193], w[30], pars->GC_10, amp[798]); 
  FFV2_5_0(w[205], w[210], w[3], pars->GC_51, pars->GC_58, amp[799]); 
  FFV1_0(w[134], w[183], w[71], pars->GC_11, amp[800]); 
  FFV1_0(w[205], w[183], w[0], pars->GC_11, amp[801]); 
  FFV1_0(w[75], w[133], w[204], pars->GC_11, amp[802]); 
  FFV1_0(w[134], w[69], w[186], pars->GC_11, amp[803]); 
  VVV1_0(w[0], w[204], w[186], pars->GC_10, amp[804]); 
  FFV2_0(w[205], w[209], w[2], pars->GC_100, amp[805]); 
  FFV1_0(w[134], w[188], w[71], pars->GC_11, amp[806]); 
  FFV1_0(w[205], w[188], w[0], pars->GC_11, amp[807]); 
  FFV1_0(w[138], w[4], w[137], pars->GC_11, amp[808]); 
  FFV2_0(w[139], w[4], w[8], pars->GC_100, amp[809]); 
  FFV1_0(w[138], w[133], w[12], pars->GC_11, amp[810]); 
  FFV2_0(w[180], w[133], w[8], pars->GC_100, amp[811]); 
  FFV1_0(w[6], w[82], w[184], pars->GC_11, amp[812]); 
  FFV1_0(w[81], w[4], w[184], pars->GC_11, amp[813]); 
  FFV1_0(w[140], w[181], w[12], pars->GC_11, amp[814]); 
  FFV2_5_0(w[180], w[181], w[3], pars->GC_51, pars->GC_58, amp[815]); 
  FFV1_0(w[6], w[187], w[142], pars->GC_11, amp[816]); 
  FFV1_0(w[81], w[133], w[142], pars->GC_11, amp[817]); 
  FFV1_0(w[140], w[79], w[137], pars->GC_11, amp[818]); 
  FFV2_5_0(w[139], w[79], w[3], pars->GC_51, pars->GC_58, amp[819]); 
  FFV1_0(w[144], w[4], w[212], pars->GC_11, amp[820]); 
  FFV1_0(w[144], w[187], w[12], pars->GC_11, amp[821]); 
  FFV2_0(w[180], w[187], w[2], pars->GC_100, amp[822]); 
  FFV1_0(w[144], w[133], w[83], pars->GC_11, amp[823]); 
  FFV1_0(w[144], w[82], w[137], pars->GC_11, amp[824]); 
  FFV2_0(w[139], w[82], w[2], pars->GC_100, amp[825]); 
  FFV1_0(w[144], w[4], w[213], pars->GC_11, amp[826]); 
  FFV1_0(w[144], w[133], w[85], pars->GC_11, amp[827]); 
  FFV2_3_0(w[144], w[147], w[3], pars->GC_50, pars->GC_58, amp[828]); 
  FFV2_0(w[140], w[147], w[2], pars->GC_100, amp[829]); 
  FFV2_3_0(w[144], w[189], w[3], pars->GC_50, pars->GC_58, amp[830]); 
  FFV2_0(w[140], w[189], w[2], pars->GC_100, amp[831]); 
  FFV1_0(w[6], w[195], w[151], pars->GC_11, amp[832]); 
  FFV1_0(w[153], w[4], w[152], pars->GC_11, amp[833]); 
  FFV1_0(w[6], w[82], w[194], pars->GC_11, amp[834]); 
  FFV1_0(w[149], w[82], w[152], pars->GC_11, amp[835]); 
  FFV1_0(w[81], w[4], w[194], pars->GC_11, amp[836]); 
  FFV1_0(w[149], w[4], w[214], pars->GC_11, amp[837]); 
  FFV1_0(w[149], w[195], w[12], pars->GC_11, amp[838]); 
  FFV2_3_0(w[149], w[196], w[3], pars->GC_50, pars->GC_58, amp[839]); 
  FFV1_0(w[156], w[4], w[152], pars->GC_11, amp[840]); 
  FFV1_0(w[155], w[79], w[152], pars->GC_11, amp[841]); 
  FFV1_0(w[155], w[197], w[12], pars->GC_11, amp[842]); 
  FFV2_0(w[155], w[196], w[2], pars->GC_100, amp[843]); 
  FFV1_0(w[159], w[4], w[152], pars->GC_11, amp[844]); 
  FFV1_0(w[134], w[98], w[152], pars->GC_11, amp[845]); 
  FFV1_0(w[134], w[203], w[12], pars->GC_11, amp[846]); 
  FFV2_0(w[134], w[196], w[8], pars->GC_100, amp[847]); 
  FFV1_0(w[6], w[195], w[160], pars->GC_11, amp[848]); 
  FFV1_0(w[134], w[100], w[152], pars->GC_11, amp[849]); 
  FFV1_0(w[134], w[79], w[214], pars->GC_11, amp[850]); 
  FFV1_0(w[134], w[197], w[83], pars->GC_11, amp[851]); 
  FFV1_0(w[134], w[101], w[152], pars->GC_11, amp[852]); 
  FFV1_0(w[134], w[197], w[85], pars->GC_11, amp[853]); 
  FFV2_5_0(w[205], w[197], w[3], pars->GC_51, pars->GC_58, amp[854]); 
  FFV2_0(w[205], w[195], w[2], pars->GC_100, amp[855]); 
  FFV1_0(w[6], w[88], w[190], pars->GC_11, amp[856]); 
  FFV1_0(w[153], w[133], w[37], pars->GC_11, amp[857]); 
  FFV1_0(w[6], w[187], w[161], pars->GC_11, amp[858]); 
  FFV1_0(w[149], w[187], w[37], pars->GC_11, amp[859]); 
  FFV1_0(w[81], w[133], w[161], pars->GC_11, amp[860]); 
  FFV1_0(w[149], w[133], w[90], pars->GC_11, amp[861]); 
  FFV1_0(w[149], w[88], w[137], pars->GC_11, amp[862]); 
  FFV2_3_0(w[149], w[162], w[3], pars->GC_50, pars->GC_58, amp[863]); 
  FFV1_0(w[156], w[133], w[37], pars->GC_11, amp[864]); 
  FFV1_0(w[155], w[181], w[37], pars->GC_11, amp[865]); 
  FFV1_0(w[155], w[92], w[137], pars->GC_11, amp[866]); 
  FFV2_0(w[155], w[162], w[2], pars->GC_100, amp[867]); 
  FFV1_0(w[159], w[133], w[37], pars->GC_11, amp[868]); 
  FFV1_0(w[134], w[179], w[37], pars->GC_11, amp[869]); 
  FFV1_0(w[134], w[93], w[137], pars->GC_11, amp[870]); 
  FFV2_0(w[134], w[162], w[8], pars->GC_100, amp[871]); 
  FFV1_0(w[6], w[88], w[193], pars->GC_11, amp[872]); 
  FFV1_0(w[134], w[183], w[37], pars->GC_11, amp[873]); 
  FFV1_0(w[134], w[181], w[90], pars->GC_11, amp[874]); 
  FFV1_0(w[134], w[92], w[212], pars->GC_11, amp[875]); 
  FFV1_0(w[134], w[188], w[37], pars->GC_11, amp[876]); 
  FFV1_0(w[134], w[92], w[213], pars->GC_11, amp[877]); 
  FFV2_5_0(w[168], w[92], w[3], pars->GC_51, pars->GC_58, amp[878]); 
  FFV2_0(w[168], w[88], w[2], pars->GC_100, amp[879]); 
  FFV1_0(w[102], w[4], w[190], pars->GC_11, amp[880]); 
  FFV1_0(w[102], w[133], w[151], pars->GC_11, amp[881]); 
  FFV1_0(w[153], w[4], w[169], pars->GC_11, amp[882]); 
  FFV1_0(w[153], w[133], w[61], pars->GC_11, amp[883]); 
  FFV1_0(w[149], w[4], w[215], pars->GC_11, amp[884]); 
  FFV1_0(w[149], w[187], w[61], pars->GC_11, amp[885]); 
  FFV1_0(w[149], w[133], w[104], pars->GC_11, amp[886]); 
  FFV1_0(w[149], w[82], w[169], pars->GC_11, amp[887]); 
  FFV1_0(w[156], w[4], w[169], pars->GC_11, amp[888]); 
  FFV1_0(w[156], w[133], w[61], pars->GC_11, amp[889]); 
  FFV1_0(w[155], w[181], w[61], pars->GC_11, amp[890]); 
  FFV1_0(w[155], w[79], w[169], pars->GC_11, amp[891]); 
  FFV1_0(w[159], w[4], w[169], pars->GC_11, amp[892]); 
  FFV1_0(w[134], w[98], w[169], pars->GC_11, amp[893]); 
  FFV1_0(w[159], w[133], w[61], pars->GC_11, amp[894]); 
  FFV1_0(w[134], w[179], w[61], pars->GC_11, amp[895]); 
  FFV1_0(w[102], w[4], w[193], pars->GC_11, amp[896]); 
  FFV1_0(w[134], w[183], w[61], pars->GC_11, amp[897]); 
  FFV1_0(w[134], w[181], w[104], pars->GC_11, amp[898]); 
  FFV1_0(w[102], w[133], w[160], pars->GC_11, amp[899]); 
  FFV1_0(w[134], w[100], w[169], pars->GC_11, amp[900]); 
  FFV1_0(w[134], w[79], w[215], pars->GC_11, amp[901]); 
  FFV1_0(w[134], w[188], w[61], pars->GC_11, amp[902]); 
  FFV1_0(w[134], w[101], w[169], pars->GC_11, amp[903]); 
  FFV1_0(w[173], w[4], w[212], pars->GC_11, amp[904]); 
  FFV1_0(w[6], w[209], w[151], pars->GC_11, amp[905]); 
  VVV1_0(w[0], w[151], w[212], pars->GC_10, amp[906]); 
  FFV1_0(w[173], w[187], w[12], pars->GC_11, amp[907]); 
  FFV1_0(w[149], w[209], w[12], pars->GC_11, amp[908]); 
  FFV1_0(w[149], w[187], w[71], pars->GC_11, amp[909]); 
  FFV1_0(w[173], w[133], w[83], pars->GC_11, amp[910]); 
  FFV1_0(w[6], w[106], w[190], pars->GC_11, amp[911]); 
  VVV1_0(w[0], w[190], w[83], pars->GC_10, amp[912]); 
  FFV1_0(w[173], w[82], w[137], pars->GC_11, amp[913]); 
  FFV1_0(w[149], w[106], w[137], pars->GC_11, amp[914]); 
  FFV1_0(w[149], w[82], w[175], pars->GC_11, amp[915]); 
  FFV1_0(w[173], w[4], w[213], pars->GC_11, amp[916]); 
  FFV1_0(w[173], w[133], w[85], pars->GC_11, amp[917]); 
  FFV1_0(w[108], w[4], w[190], pars->GC_11, amp[918]); 
  FFV1_0(w[108], w[133], w[151], pars->GC_11, amp[919]); 
  VVV1_0(w[0], w[190], w[85], pars->GC_10, amp[920]); 
  VVV1_0(w[0], w[151], w[213], pars->GC_10, amp[921]); 
  FFV2_3_0(w[173], w[147], w[3], pars->GC_50, pars->GC_58, amp[922]); 
  FFV1_0(w[153], w[4], w[175], pars->GC_11, amp[923]); 
  FFV1_0(w[153], w[147], w[0], pars->GC_11, amp[924]); 
  FFV2_3_0(w[173], w[189], w[3], pars->GC_50, pars->GC_58, amp[925]); 
  FFV1_0(w[153], w[133], w[71], pars->GC_11, amp[926]); 
  FFV1_0(w[153], w[189], w[0], pars->GC_11, amp[927]); 
  FFV1_0(w[176], w[181], w[12], pars->GC_11, amp[928]); 
  FFV1_0(w[155], w[210], w[12], pars->GC_11, amp[929]); 
  FFV1_0(w[155], w[181], w[71], pars->GC_11, amp[930]); 
  FFV1_0(w[176], w[79], w[137], pars->GC_11, amp[931]); 
  FFV1_0(w[155], w[109], w[137], pars->GC_11, amp[932]); 
  FFV1_0(w[155], w[79], w[175], pars->GC_11, amp[933]); 
  FFV2_0(w[176], w[147], w[2], pars->GC_100, amp[934]); 
  FFV1_0(w[156], w[4], w[175], pars->GC_11, amp[935]); 
  FFV1_0(w[156], w[147], w[0], pars->GC_11, amp[936]); 
  FFV2_0(w[176], w[189], w[2], pars->GC_100, amp[937]); 
  FFV1_0(w[156], w[133], w[71], pars->GC_11, amp[938]); 
  FFV1_0(w[156], w[189], w[0], pars->GC_11, amp[939]); 
  FFV1_0(w[159], w[4], w[175], pars->GC_11, amp[940]); 
  FFV1_0(w[134], w[98], w[175], pars->GC_11, amp[941]); 
  FFV1_0(w[159], w[147], w[0], pars->GC_11, amp[942]); 
  FFV1_0(w[168], w[98], w[0], pars->GC_11, amp[943]); 
  FFV1_0(w[159], w[133], w[71], pars->GC_11, amp[944]); 
  FFV1_0(w[134], w[179], w[71], pars->GC_11, amp[945]); 
  FFV1_0(w[159], w[189], w[0], pars->GC_11, amp[946]); 
  FFV1_0(w[205], w[179], w[0], pars->GC_11, amp[947]); 
  FFV1_0(w[134], w[210], w[83], pars->GC_11, amp[948]); 
  FFV1_0(w[6], w[106], w[193], pars->GC_11, amp[949]); 
  VVV1_0(w[0], w[193], w[83], pars->GC_10, amp[950]); 
  FFV1_0(w[134], w[210], w[85], pars->GC_11, amp[951]); 
  FFV1_0(w[108], w[4], w[193], pars->GC_11, amp[952]); 
  VVV1_0(w[0], w[193], w[85], pars->GC_10, amp[953]); 
  FFV2_5_0(w[205], w[210], w[3], pars->GC_51, pars->GC_58, amp[954]); 
  FFV1_0(w[134], w[183], w[71], pars->GC_11, amp[955]); 
  FFV1_0(w[205], w[183], w[0], pars->GC_11, amp[956]); 
  FFV1_0(w[134], w[109], w[212], pars->GC_11, amp[957]); 
  FFV1_0(w[6], w[209], w[160], pars->GC_11, amp[958]); 
  VVV1_0(w[0], w[160], w[212], pars->GC_10, amp[959]); 
  FFV1_0(w[134], w[109], w[213], pars->GC_11, amp[960]); 
  FFV1_0(w[108], w[133], w[160], pars->GC_11, amp[961]); 
  VVV1_0(w[0], w[160], w[213], pars->GC_10, amp[962]); 
  FFV2_5_0(w[168], w[109], w[3], pars->GC_51, pars->GC_58, amp[963]); 
  FFV1_0(w[134], w[100], w[175], pars->GC_11, amp[964]); 
  FFV1_0(w[168], w[100], w[0], pars->GC_11, amp[965]); 
  FFV2_0(w[205], w[209], w[2], pars->GC_100, amp[966]); 
  FFV1_0(w[134], w[188], w[71], pars->GC_11, amp[967]); 
  FFV1_0(w[205], w[188], w[0], pars->GC_11, amp[968]); 
  FFV2_0(w[168], w[106], w[2], pars->GC_100, amp[969]); 
  FFV1_0(w[134], w[101], w[175], pars->GC_11, amp[970]); 
  FFV1_0(w[168], w[101], w[0], pars->GC_11, amp[971]); 
  FFV1_0(w[16], w[4], w[17], pars->GC_11, amp[972]); 
  FFV1_0(w[6], w[18], w[9], pars->GC_11, amp[973]); 
  FFV1_0(w[6], w[15], w[21], pars->GC_11, amp[974]); 
  FFV1_0(w[22], w[15], w[9], pars->GC_11, amp[975]); 
  FFV1_0(w[16], w[4], w[24], pars->GC_11, amp[976]); 
  FFV1_0(w[25], w[5], w[9], pars->GC_11, amp[977]); 
  FFV1_0(w[23], w[5], w[21], pars->GC_11, amp[978]); 
  FFV1_0(w[23], w[26], w[9], pars->GC_11, amp[979]); 
  FFV1_0(w[6], w[29], w[9], pars->GC_11, amp[980]); 
  FFV1_0(w[31], w[5], w[9], pars->GC_11, amp[981]); 
  FFV1_0(w[6], w[15], w[42], pars->GC_11, amp[982]); 
  FFV1_0(w[23], w[5], w[42], pars->GC_11, amp[983]); 
  FFV1_0(w[6], w[10], w[43], pars->GC_11, amp[984]); 
  FFV1_0(w[11], w[5], w[43], pars->GC_11, amp[985]); 
  FFV1_0(w[6], w[18], w[43], pars->GC_11, amp[986]); 
  FFV1_0(w[1], w[35], w[17], pars->GC_11, amp[987]); 
  FFV1_0(w[22], w[15], w[43], pars->GC_11, amp[988]); 
  FFV1_0(w[25], w[5], w[43], pars->GC_11, amp[989]); 
  FFV1_0(w[1], w[35], w[24], pars->GC_11, amp[990]); 
  FFV1_0(w[23], w[26], w[43], pars->GC_11, amp[991]); 
  FFV1_0(w[6], w[29], w[43], pars->GC_11, amp[992]); 
  FFV1_0(w[31], w[5], w[43], pars->GC_11, amp[993]); 
  FFV1_0(w[6], w[50], w[51], pars->GC_11, amp[994]); 
  FFV1_0(w[40], w[4], w[52], pars->GC_11, amp[995]); 
  FFV2_5_0(w[54], w[50], w[3], pars->GC_51, pars->GC_58, amp[996]); 
  FFV2_0(w[54], w[48], w[2], pars->GC_100, amp[997]); 
  FFV2_0(w[6], w[55], w[8], pars->GC_100, amp[998]); 
  FFV1_0(w[6], w[56], w[53], pars->GC_11, amp[999]); 
  FFV2_3_0(w[23], w[55], w[3], pars->GC_50, pars->GC_58, amp[1000]); 
  FFV1_0(w[23], w[48], w[53], pars->GC_11, amp[1001]); 
  FFV2_0(w[22], w[55], w[2], pars->GC_100, amp[1002]); 
  FFV1_0(w[22], w[50], w[53], pars->GC_11, amp[1003]); 
  FFV1_0(w[1], w[19], w[52], pars->GC_11, amp[1004]); 
  FFV1_0(w[6], w[50], w[57], pars->GC_11, amp[1005]); 
  FFV1_0(w[63], w[5], w[51], pars->GC_11, amp[1006]); 
  FFV1_0(w[40], w[4], w[64], pars->GC_11, amp[1007]); 
  FFV2_3_0(w[63], w[65], w[3], pars->GC_50, pars->GC_58, amp[1008]); 
  FFV2_0(w[60], w[65], w[2], pars->GC_100, amp[1009]); 
  FFV2_0(w[66], w[5], w[8], pars->GC_100, amp[1010]); 
  FFV1_0(w[67], w[5], w[53], pars->GC_11, amp[1011]); 
  FFV2_5_0(w[66], w[15], w[3], pars->GC_51, pars->GC_58, amp[1012]); 
  FFV1_0(w[60], w[15], w[53], pars->GC_11, amp[1013]); 
  FFV2_0(w[66], w[26], w[2], pars->GC_100, amp[1014]); 
  FFV1_0(w[63], w[26], w[53], pars->GC_11, amp[1015]); 
  FFV1_0(w[1], w[19], w[64], pars->GC_11, amp[1016]); 
  FFV1_0(w[63], w[5], w[57], pars->GC_11, amp[1017]); 
  FFV1_0(w[73], w[4], w[17], pars->GC_11, amp[1018]); 
  FFV1_0(w[6], w[74], w[51], pars->GC_11, amp[1019]); 
  VVV1_0(w[0], w[51], w[17], pars->GC_10, amp[1020]); 
  FFV1_0(w[73], w[4], w[24], pars->GC_11, amp[1021]); 
  FFV1_0(w[75], w[5], w[51], pars->GC_11, amp[1022]); 
  VVV1_0(w[0], w[51], w[24], pars->GC_10, amp[1023]); 
  FFV1_0(w[6], w[10], w[76], pars->GC_11, amp[1024]); 
  FFV1_0(w[11], w[5], w[76], pars->GC_11, amp[1025]); 
  FFV1_0(w[11], w[65], w[0], pars->GC_11, amp[1026]); 
  FFV1_0(w[54], w[10], w[0], pars->GC_11, amp[1027]); 
  FFV1_0(w[6], w[18], w[76], pars->GC_11, amp[1028]); 
  FFV2_5_0(w[54], w[74], w[3], pars->GC_51, pars->GC_58, amp[1029]); 
  FFV1_0(w[54], w[18], w[0], pars->GC_11, amp[1030]); 
  FFV1_0(w[22], w[15], w[76], pars->GC_11, amp[1031]); 
  FFV1_0(w[22], w[74], w[53], pars->GC_11, amp[1032]); 
  FFV1_0(w[72], w[15], w[53], pars->GC_11, amp[1033]); 
  FFV1_0(w[25], w[5], w[76], pars->GC_11, amp[1034]); 
  FFV2_3_0(w[75], w[65], w[3], pars->GC_50, pars->GC_58, amp[1035]); 
  FFV1_0(w[25], w[65], w[0], pars->GC_11, amp[1036]); 
  FFV1_0(w[23], w[26], w[76], pars->GC_11, amp[1037]); 
  FFV1_0(w[75], w[26], w[53], pars->GC_11, amp[1038]); 
  FFV1_0(w[23], w[70], w[53], pars->GC_11, amp[1039]); 
  FFV1_0(w[6], w[29], w[76], pars->GC_11, amp[1040]); 
  FFV2_0(w[54], w[70], w[2], pars->GC_100, amp[1041]); 
  FFV1_0(w[54], w[29], w[0], pars->GC_11, amp[1042]); 
  FFV1_0(w[31], w[5], w[76], pars->GC_11, amp[1043]); 
  FFV2_0(w[72], w[65], w[2], pars->GC_100, amp[1044]); 
  FFV1_0(w[31], w[65], w[0], pars->GC_11, amp[1045]); 
  FFV1_0(w[6], w[74], w[57], pars->GC_11, amp[1046]); 
  FFV1_0(w[1], w[69], w[17], pars->GC_11, amp[1047]); 
  VVV1_0(w[0], w[57], w[17], pars->GC_10, amp[1048]); 
  FFV1_0(w[75], w[5], w[57], pars->GC_11, amp[1049]); 
  FFV1_0(w[1], w[69], w[24], pars->GC_11, amp[1050]); 
  VVV1_0(w[0], w[57], w[24], pars->GC_10, amp[1051]); 
  FFV1_0(w[13], w[5], w[12], pars->GC_11, amp[1052]); 
  FFV2_0(w[14], w[5], w[8], pars->GC_100, amp[1053]); 
  FFV1_0(w[6], w[19], w[20], pars->GC_11, amp[1054]); 
  FFV1_0(w[22], w[4], w[20], pars->GC_11, amp[1055]); 
  FFV1_0(w[16], w[15], w[12], pars->GC_11, amp[1056]); 
  FFV2_5_0(w[14], w[15], w[3], pars->GC_51, pars->GC_58, amp[1057]); 
  FFV1_0(w[27], w[5], w[28], pars->GC_11, amp[1058]); 
  FFV1_0(w[27], w[26], w[12], pars->GC_11, amp[1059]); 
  FFV2_0(w[14], w[26], w[2], pars->GC_100, amp[1060]); 
  FFV1_0(w[27], w[5], w[30], pars->GC_11, amp[1061]); 
  FFV2_3_0(w[27], w[32], w[3], pars->GC_50, pars->GC_58, amp[1062]); 
  FFV2_0(w[16], w[32], w[2], pars->GC_100, amp[1063]); 
  FFV1_0(w[6], w[35], w[36], pars->GC_11, amp[1064]); 
  FFV1_0(w[38], w[5], w[37], pars->GC_11, amp[1065]); 
  FFV1_0(w[34], w[26], w[37], pars->GC_11, amp[1066]); 
  FFV1_0(w[34], w[5], w[39], pars->GC_11, amp[1067]); 
  FFV1_0(w[41], w[5], w[37], pars->GC_11, amp[1068]); 
  FFV1_0(w[40], w[15], w[37], pars->GC_11, amp[1069]); 
  FFV1_0(w[44], w[5], w[37], pars->GC_11, amp[1070]); 
  FFV1_0(w[1], w[10], w[37], pars->GC_11, amp[1071]); 
  FFV1_0(w[6], w[35], w[45], pars->GC_11, amp[1072]); 
  FFV1_0(w[1], w[18], w[37], pars->GC_11, amp[1073]); 
  FFV1_0(w[1], w[15], w[39], pars->GC_11, amp[1074]); 
  FFV1_0(w[1], w[29], w[37], pars->GC_11, amp[1075]); 
  FFV1_0(w[6], w[19], w[47], pars->GC_11, amp[1076]); 
  FFV1_0(w[22], w[4], w[47], pars->GC_11, amp[1077]); 
  FFV1_0(w[34], w[48], w[12], pars->GC_11, amp[1078]); 
  FFV2_3_0(w[34], w[49], w[3], pars->GC_50, pars->GC_58, amp[1079]); 
  FFV1_0(w[40], w[50], w[12], pars->GC_11, amp[1080]); 
  FFV2_0(w[40], w[49], w[2], pars->GC_100, amp[1081]); 
  FFV1_0(w[1], w[56], w[12], pars->GC_11, amp[1082]); 
  FFV2_0(w[1], w[49], w[8], pars->GC_100, amp[1083]); 
  FFV1_0(w[1], w[50], w[28], pars->GC_11, amp[1084]); 
  FFV1_0(w[1], w[50], w[30], pars->GC_11, amp[1085]); 
  FFV2_5_0(w[58], w[50], w[3], pars->GC_51, pars->GC_58, amp[1086]); 
  FFV2_0(w[58], w[48], w[2], pars->GC_100, amp[1087]); 
  FFV1_0(w[60], w[4], w[36], pars->GC_11, amp[1088]); 
  FFV1_0(w[38], w[5], w[61], pars->GC_11, amp[1089]); 
  FFV1_0(w[34], w[5], w[62], pars->GC_11, amp[1090]); 
  FFV1_0(w[34], w[26], w[61], pars->GC_11, amp[1091]); 
  FFV1_0(w[41], w[5], w[61], pars->GC_11, amp[1092]); 
  FFV1_0(w[40], w[15], w[61], pars->GC_11, amp[1093]); 
  FFV1_0(w[44], w[5], w[61], pars->GC_11, amp[1094]); 
  FFV1_0(w[1], w[10], w[61], pars->GC_11, amp[1095]); 
  FFV1_0(w[60], w[4], w[45], pars->GC_11, amp[1096]); 
  FFV1_0(w[1], w[18], w[61], pars->GC_11, amp[1097]); 
  FFV1_0(w[1], w[15], w[62], pars->GC_11, amp[1098]); 
  FFV1_0(w[1], w[29], w[61], pars->GC_11, amp[1099]); 
  FFV1_0(w[68], w[5], w[28], pars->GC_11, amp[1100]); 
  FFV1_0(w[6], w[69], w[36], pars->GC_11, amp[1101]); 
  VVV1_0(w[0], w[36], w[28], pars->GC_10, amp[1102]); 
  FFV1_0(w[68], w[26], w[12], pars->GC_11, amp[1103]); 
  FFV1_0(w[34], w[70], w[12], pars->GC_11, amp[1104]); 
  FFV1_0(w[34], w[26], w[71], pars->GC_11, amp[1105]); 
  FFV1_0(w[68], w[5], w[30], pars->GC_11, amp[1106]); 
  FFV1_0(w[72], w[4], w[36], pars->GC_11, amp[1107]); 
  VVV1_0(w[0], w[36], w[30], pars->GC_10, amp[1108]); 
  FFV2_3_0(w[68], w[32], w[3], pars->GC_50, pars->GC_58, amp[1109]); 
  FFV1_0(w[38], w[5], w[71], pars->GC_11, amp[1110]); 
  FFV1_0(w[38], w[32], w[0], pars->GC_11, amp[1111]); 
  FFV1_0(w[73], w[15], w[12], pars->GC_11, amp[1112]); 
  FFV1_0(w[40], w[74], w[12], pars->GC_11, amp[1113]); 
  FFV1_0(w[40], w[15], w[71], pars->GC_11, amp[1114]); 
  FFV2_0(w[73], w[32], w[2], pars->GC_100, amp[1115]); 
  FFV1_0(w[41], w[5], w[71], pars->GC_11, amp[1116]); 
  FFV1_0(w[41], w[32], w[0], pars->GC_11, amp[1117]); 
  FFV1_0(w[44], w[5], w[71], pars->GC_11, amp[1118]); 
  FFV1_0(w[1], w[10], w[71], pars->GC_11, amp[1119]); 
  FFV1_0(w[44], w[32], w[0], pars->GC_11, amp[1120]); 
  FFV1_0(w[58], w[10], w[0], pars->GC_11, amp[1121]); 
  FFV1_0(w[1], w[74], w[28], pars->GC_11, amp[1122]); 
  FFV1_0(w[6], w[69], w[45], pars->GC_11, amp[1123]); 
  VVV1_0(w[0], w[45], w[28], pars->GC_10, amp[1124]); 
  FFV1_0(w[1], w[74], w[30], pars->GC_11, amp[1125]); 
  FFV1_0(w[72], w[4], w[45], pars->GC_11, amp[1126]); 
  VVV1_0(w[0], w[45], w[30], pars->GC_10, amp[1127]); 
  FFV2_5_0(w[58], w[74], w[3], pars->GC_51, pars->GC_58, amp[1128]); 
  FFV1_0(w[1], w[18], w[71], pars->GC_11, amp[1129]); 
  FFV1_0(w[58], w[18], w[0], pars->GC_11, amp[1130]); 
  FFV2_0(w[58], w[70], w[2], pars->GC_100, amp[1131]); 
  FFV1_0(w[1], w[29], w[71], pars->GC_11, amp[1132]); 
  FFV1_0(w[58], w[29], w[0], pars->GC_11, amp[1133]); 
  FFV1_0(w[13], w[4], w[77], pars->GC_11, amp[1134]); 
  FFV2_0(w[78], w[4], w[8], pars->GC_100, amp[1135]); 
  FFV1_0(w[6], w[26], w[80], pars->GC_11, amp[1136]); 
  FFV1_0(w[81], w[5], w[80], pars->GC_11, amp[1137]); 
  FFV1_0(w[16], w[79], w[77], pars->GC_11, amp[1138]); 
  FFV2_5_0(w[78], w[79], w[3], pars->GC_51, pars->GC_58, amp[1139]); 
  FFV1_0(w[27], w[82], w[77], pars->GC_11, amp[1140]); 
  FFV2_0(w[78], w[82], w[2], pars->GC_100, amp[1141]); 
  FFV1_0(w[27], w[4], w[84], pars->GC_11, amp[1142]); 
  FFV1_0(w[27], w[4], w[86], pars->GC_11, amp[1143]); 
  FFV2_3_0(w[27], w[87], w[3], pars->GC_50, pars->GC_58, amp[1144]); 
  FFV2_0(w[16], w[87], w[2], pars->GC_100, amp[1145]); 
  FFV1_0(w[6], w[26], w[89], pars->GC_11, amp[1146]); 
  FFV1_0(w[81], w[5], w[89], pars->GC_11, amp[1147]); 
  FFV1_0(w[34], w[88], w[77], pars->GC_11, amp[1148]); 
  FFV2_3_0(w[34], w[91], w[3], pars->GC_50, pars->GC_58, amp[1149]); 
  FFV1_0(w[40], w[92], w[77], pars->GC_11, amp[1150]); 
  FFV2_0(w[40], w[91], w[2], pars->GC_100, amp[1151]); 
  FFV1_0(w[1], w[93], w[77], pars->GC_11, amp[1152]); 
  FFV2_0(w[1], w[91], w[8], pars->GC_100, amp[1153]); 
  FFV1_0(w[1], w[92], w[84], pars->GC_11, amp[1154]); 
  FFV1_0(w[1], w[92], w[86], pars->GC_11, amp[1155]); 
  FFV2_5_0(w[94], w[92], w[3], pars->GC_51, pars->GC_58, amp[1156]); 
  FFV2_0(w[94], w[88], w[2], pars->GC_100, amp[1157]); 
  FFV1_0(w[6], w[48], w[95], pars->GC_11, amp[1158]); 
  FFV1_0(w[38], w[4], w[96], pars->GC_11, amp[1159]); 
  FFV1_0(w[34], w[82], w[96], pars->GC_11, amp[1160]); 
  FFV1_0(w[34], w[4], w[97], pars->GC_11, amp[1161]); 
  FFV1_0(w[41], w[4], w[96], pars->GC_11, amp[1162]); 
  FFV1_0(w[40], w[79], w[96], pars->GC_11, amp[1163]); 
  FFV1_0(w[44], w[4], w[96], pars->GC_11, amp[1164]); 
  FFV1_0(w[1], w[98], w[96], pars->GC_11, amp[1165]); 
  FFV1_0(w[6], w[48], w[99], pars->GC_11, amp[1166]); 
  FFV1_0(w[1], w[100], w[96], pars->GC_11, amp[1167]); 
  FFV1_0(w[1], w[79], w[97], pars->GC_11, amp[1168]); 
  FFV1_0(w[1], w[101], w[96], pars->GC_11, amp[1169]); 
  FFV1_0(w[102], w[5], w[95], pars->GC_11, amp[1170]); 
  FFV1_0(w[38], w[4], w[103], pars->GC_11, amp[1171]); 
  FFV1_0(w[34], w[82], w[103], pars->GC_11, amp[1172]); 
  FFV1_0(w[34], w[4], w[105], pars->GC_11, amp[1173]); 
  FFV1_0(w[41], w[4], w[103], pars->GC_11, amp[1174]); 
  FFV1_0(w[40], w[79], w[103], pars->GC_11, amp[1175]); 
  FFV1_0(w[44], w[4], w[103], pars->GC_11, amp[1176]); 
  FFV1_0(w[1], w[98], w[103], pars->GC_11, amp[1177]); 
  FFV1_0(w[102], w[5], w[99], pars->GC_11, amp[1178]); 
  FFV1_0(w[1], w[100], w[103], pars->GC_11, amp[1179]); 
  FFV1_0(w[1], w[79], w[105], pars->GC_11, amp[1180]); 
  FFV1_0(w[1], w[101], w[103], pars->GC_11, amp[1181]); 
  FFV1_0(w[68], w[82], w[77], pars->GC_11, amp[1182]); 
  FFV1_0(w[34], w[106], w[77], pars->GC_11, amp[1183]); 
  FFV1_0(w[34], w[82], w[107], pars->GC_11, amp[1184]); 
  FFV1_0(w[68], w[4], w[84], pars->GC_11, amp[1185]); 
  FFV1_0(w[6], w[70], w[95], pars->GC_11, amp[1186]); 
  VVV1_0(w[0], w[95], w[84], pars->GC_10, amp[1187]); 
  FFV1_0(w[68], w[4], w[86], pars->GC_11, amp[1188]); 
  FFV1_0(w[108], w[5], w[95], pars->GC_11, amp[1189]); 
  VVV1_0(w[0], w[95], w[86], pars->GC_10, amp[1190]); 
  FFV2_3_0(w[68], w[87], w[3], pars->GC_50, pars->GC_58, amp[1191]); 
  FFV1_0(w[38], w[4], w[107], pars->GC_11, amp[1192]); 
  FFV1_0(w[38], w[87], w[0], pars->GC_11, amp[1193]); 
  FFV1_0(w[73], w[79], w[77], pars->GC_11, amp[1194]); 
  FFV1_0(w[40], w[109], w[77], pars->GC_11, amp[1195]); 
  FFV1_0(w[40], w[79], w[107], pars->GC_11, amp[1196]); 
  FFV2_0(w[73], w[87], w[2], pars->GC_100, amp[1197]); 
  FFV1_0(w[41], w[4], w[107], pars->GC_11, amp[1198]); 
  FFV1_0(w[41], w[87], w[0], pars->GC_11, amp[1199]); 
  FFV1_0(w[44], w[4], w[107], pars->GC_11, amp[1200]); 
  FFV1_0(w[1], w[98], w[107], pars->GC_11, amp[1201]); 
  FFV1_0(w[44], w[87], w[0], pars->GC_11, amp[1202]); 
  FFV1_0(w[94], w[98], w[0], pars->GC_11, amp[1203]); 
  FFV1_0(w[1], w[109], w[84], pars->GC_11, amp[1204]); 
  FFV1_0(w[6], w[70], w[99], pars->GC_11, amp[1205]); 
  VVV1_0(w[0], w[99], w[84], pars->GC_10, amp[1206]); 
  FFV1_0(w[1], w[109], w[86], pars->GC_11, amp[1207]); 
  FFV1_0(w[108], w[5], w[99], pars->GC_11, amp[1208]); 
  VVV1_0(w[0], w[99], w[86], pars->GC_10, amp[1209]); 
  FFV2_5_0(w[94], w[109], w[3], pars->GC_51, pars->GC_58, amp[1210]); 
  FFV1_0(w[1], w[100], w[107], pars->GC_11, amp[1211]); 
  FFV1_0(w[94], w[100], w[0], pars->GC_11, amp[1212]); 
  FFV2_0(w[94], w[106], w[2], pars->GC_100, amp[1213]); 
  FFV1_0(w[1], w[101], w[107], pars->GC_11, amp[1214]); 
  FFV1_0(w[94], w[101], w[0], pars->GC_11, amp[1215]); 
  FFV1_0(w[67], w[5], w[53], pars->GC_11, amp[1216]); 
  FFV2_0(w[66], w[5], w[8], pars->GC_100, amp[1217]); 
  FFV1_0(w[1], w[82], w[64], pars->GC_11, amp[1218]); 
  FFV1_0(w[113], w[4], w[64], pars->GC_11, amp[1219]); 
  FFV1_0(w[60], w[15], w[53], pars->GC_11, amp[1220]); 
  FFV2_5_0(w[66], w[15], w[3], pars->GC_51, pars->GC_58, amp[1221]); 
  FFV1_0(w[63], w[5], w[114], pars->GC_11, amp[1222]); 
  FFV1_0(w[63], w[26], w[53], pars->GC_11, amp[1223]); 
  FFV2_0(w[66], w[26], w[2], pars->GC_100, amp[1224]); 
  FFV1_0(w[63], w[5], w[116], pars->GC_11, amp[1225]); 
  FFV2_3_0(w[63], w[65], w[3], pars->GC_50, pars->GC_58, amp[1226]); 
  FFV2_0(w[60], w[65], w[2], pars->GC_100, amp[1227]); 
  FFV1_0(w[1], w[88], w[24], pars->GC_11, amp[1228]); 
  FFV1_0(w[25], w[5], w[43], pars->GC_11, amp[1229]); 
  FFV1_0(w[23], w[26], w[43], pars->GC_11, amp[1230]); 
  FFV1_0(w[23], w[5], w[120], pars->GC_11, amp[1231]); 
  FFV1_0(w[31], w[5], w[43], pars->GC_11, amp[1232]); 
  FFV1_0(w[22], w[15], w[43], pars->GC_11, amp[1233]); 
  FFV1_0(w[11], w[5], w[43], pars->GC_11, amp[1234]); 
  FFV1_0(w[6], w[10], w[43], pars->GC_11, amp[1235]); 
  FFV1_0(w[1], w[88], w[17], pars->GC_11, amp[1236]); 
  FFV1_0(w[6], w[18], w[43], pars->GC_11, amp[1237]); 
  FFV1_0(w[6], w[15], w[120], pars->GC_11, amp[1238]); 
  FFV1_0(w[6], w[29], w[43], pars->GC_11, amp[1239]); 
  FFV1_0(w[1], w[82], w[52], pars->GC_11, amp[1240]); 
  FFV1_0(w[113], w[4], w[52], pars->GC_11, amp[1241]); 
  FFV1_0(w[23], w[48], w[53], pars->GC_11, amp[1242]); 
  FFV2_3_0(w[23], w[55], w[3], pars->GC_50, pars->GC_58, amp[1243]); 
  FFV1_0(w[22], w[50], w[53], pars->GC_11, amp[1244]); 
  FFV2_0(w[22], w[55], w[2], pars->GC_100, amp[1245]); 
  FFV1_0(w[6], w[56], w[53], pars->GC_11, amp[1246]); 
  FFV2_0(w[6], w[55], w[8], pars->GC_100, amp[1247]); 
  FFV1_0(w[6], w[50], w[114], pars->GC_11, amp[1248]); 
  FFV1_0(w[6], w[50], w[116], pars->GC_11, amp[1249]); 
  FFV2_5_0(w[54], w[50], w[3], pars->GC_51, pars->GC_58, amp[1250]); 
  FFV2_0(w[54], w[48], w[2], pars->GC_100, amp[1251]); 
  FFV1_0(w[127], w[4], w[24], pars->GC_11, amp[1252]); 
  FFV1_0(w[25], w[5], w[9], pars->GC_11, amp[1253]); 
  FFV1_0(w[23], w[5], w[129], pars->GC_11, amp[1254]); 
  FFV1_0(w[23], w[26], w[9], pars->GC_11, amp[1255]); 
  FFV1_0(w[31], w[5], w[9], pars->GC_11, amp[1256]); 
  FFV1_0(w[22], w[15], w[9], pars->GC_11, amp[1257]); 
  FFV1_0(w[11], w[5], w[9], pars->GC_11, amp[1258]); 
  FFV1_0(w[6], w[10], w[9], pars->GC_11, amp[1259]); 
  FFV1_0(w[127], w[4], w[17], pars->GC_11, amp[1260]); 
  FFV1_0(w[6], w[18], w[9], pars->GC_11, amp[1261]); 
  FFV1_0(w[6], w[15], w[129], pars->GC_11, amp[1262]); 
  FFV1_0(w[6], w[29], w[9], pars->GC_11, amp[1263]); 
  FFV1_0(w[75], w[5], w[114], pars->GC_11, amp[1264]); 
  FFV1_0(w[1], w[106], w[24], pars->GC_11, amp[1265]); 
  VVV1_0(w[0], w[24], w[114], pars->GC_10, amp[1266]); 
  FFV1_0(w[75], w[26], w[53], pars->GC_11, amp[1267]); 
  FFV1_0(w[23], w[70], w[53], pars->GC_11, amp[1268]); 
  FFV1_0(w[23], w[26], w[76], pars->GC_11, amp[1269]); 
  FFV1_0(w[75], w[5], w[116], pars->GC_11, amp[1270]); 
  FFV1_0(w[132], w[4], w[24], pars->GC_11, amp[1271]); 
  VVV1_0(w[0], w[24], w[116], pars->GC_10, amp[1272]); 
  FFV2_3_0(w[75], w[65], w[3], pars->GC_50, pars->GC_58, amp[1273]); 
  FFV1_0(w[25], w[5], w[76], pars->GC_11, amp[1274]); 
  FFV1_0(w[25], w[65], w[0], pars->GC_11, amp[1275]); 
  FFV1_0(w[72], w[15], w[53], pars->GC_11, amp[1276]); 
  FFV1_0(w[22], w[74], w[53], pars->GC_11, amp[1277]); 
  FFV1_0(w[22], w[15], w[76], pars->GC_11, amp[1278]); 
  FFV2_0(w[72], w[65], w[2], pars->GC_100, amp[1279]); 
  FFV1_0(w[31], w[5], w[76], pars->GC_11, amp[1280]); 
  FFV1_0(w[31], w[65], w[0], pars->GC_11, amp[1281]); 
  FFV1_0(w[11], w[5], w[76], pars->GC_11, amp[1282]); 
  FFV1_0(w[6], w[10], w[76], pars->GC_11, amp[1283]); 
  FFV1_0(w[11], w[65], w[0], pars->GC_11, amp[1284]); 
  FFV1_0(w[54], w[10], w[0], pars->GC_11, amp[1285]); 
  FFV1_0(w[6], w[74], w[114], pars->GC_11, amp[1286]); 
  FFV1_0(w[1], w[106], w[17], pars->GC_11, amp[1287]); 
  VVV1_0(w[0], w[17], w[114], pars->GC_10, amp[1288]); 
  FFV1_0(w[6], w[74], w[116], pars->GC_11, amp[1289]); 
  FFV1_0(w[132], w[4], w[17], pars->GC_11, amp[1290]); 
  VVV1_0(w[0], w[17], w[116], pars->GC_10, amp[1291]); 
  FFV2_5_0(w[54], w[74], w[3], pars->GC_51, pars->GC_58, amp[1292]); 
  FFV1_0(w[6], w[18], w[76], pars->GC_11, amp[1293]); 
  FFV1_0(w[54], w[18], w[0], pars->GC_11, amp[1294]); 
  FFV2_0(w[54], w[70], w[2], pars->GC_100, amp[1295]); 
  FFV1_0(w[6], w[29], w[76], pars->GC_11, amp[1296]); 
  FFV1_0(w[54], w[29], w[0], pars->GC_11, amp[1297]); 
  FFV1_0(w[6], w[98], w[136], pars->GC_11, amp[1298]); 
  FFV1_0(w[11], w[4], w[136], pars->GC_11, amp[1299]); 
  FFV1_0(w[140], w[133], w[126], pars->GC_11, amp[1300]); 
  FFV1_0(w[6], w[100], w[136], pars->GC_11, amp[1301]); 
  FFV1_0(w[6], w[79], w[143], pars->GC_11, amp[1302]); 
  FFV1_0(w[22], w[79], w[136], pars->GC_11, amp[1303]); 
  FFV1_0(w[140], w[133], w[123], pars->GC_11, amp[1304]); 
  FFV1_0(w[25], w[4], w[136], pars->GC_11, amp[1305]); 
  FFV1_0(w[23], w[4], w[143], pars->GC_11, amp[1306]); 
  FFV1_0(w[23], w[82], w[136], pars->GC_11, amp[1307]); 
  FFV1_0(w[6], w[101], w[136], pars->GC_11, amp[1308]); 
  FFV1_0(w[31], w[4], w[136], pars->GC_11, amp[1309]); 
  FFV1_0(w[6], w[79], w[157], pars->GC_11, amp[1310]); 
  FFV1_0(w[23], w[4], w[157], pars->GC_11, amp[1311]); 
  FFV1_0(w[6], w[98], w[158], pars->GC_11, amp[1312]); 
  FFV1_0(w[11], w[4], w[158], pars->GC_11, amp[1313]); 
  FFV1_0(w[6], w[100], w[158], pars->GC_11, amp[1314]); 
  FFV1_0(w[134], w[150], w[126], pars->GC_11, amp[1315]); 
  FFV1_0(w[22], w[79], w[158], pars->GC_11, amp[1316]); 
  FFV1_0(w[25], w[4], w[158], pars->GC_11, amp[1317]); 
  FFV1_0(w[134], w[150], w[123], pars->GC_11, amp[1318]); 
  FFV1_0(w[23], w[82], w[158], pars->GC_11, amp[1319]); 
  FFV1_0(w[6], w[101], w[158], pars->GC_11, amp[1320]); 
  FFV1_0(w[31], w[4], w[158], pars->GC_11, amp[1321]); 
  FFV1_0(w[6], w[92], w[163], pars->GC_11, amp[1322]); 
  FFV1_0(w[155], w[133], w[119], pars->GC_11, amp[1323]); 
  FFV2_5_0(w[165], w[92], w[3], pars->GC_51, pars->GC_58, amp[1324]); 
  FFV2_0(w[165], w[88], w[2], pars->GC_100, amp[1325]); 
  FFV2_0(w[6], w[166], w[8], pars->GC_100, amp[1326]); 
  FFV1_0(w[6], w[93], w[164], pars->GC_11, amp[1327]); 
  FFV2_3_0(w[23], w[166], w[3], pars->GC_50, pars->GC_58, amp[1328]); 
  FFV1_0(w[23], w[88], w[164], pars->GC_11, amp[1329]); 
  FFV2_0(w[22], w[166], w[2], pars->GC_100, amp[1330]); 
  FFV1_0(w[22], w[92], w[164], pars->GC_11, amp[1331]); 
  FFV1_0(w[134], w[141], w[119], pars->GC_11, amp[1332]); 
  FFV1_0(w[6], w[92], w[167], pars->GC_11, amp[1333]); 
  FFV1_0(w[63], w[4], w[163], pars->GC_11, amp[1334]); 
  FFV1_0(w[155], w[133], w[112], pars->GC_11, amp[1335]); 
  FFV2_3_0(w[63], w[171], w[3], pars->GC_50, pars->GC_58, amp[1336]); 
  FFV2_0(w[60], w[171], w[2], pars->GC_100, amp[1337]); 
  FFV2_0(w[172], w[4], w[8], pars->GC_100, amp[1338]); 
  FFV1_0(w[67], w[4], w[164], pars->GC_11, amp[1339]); 
  FFV2_5_0(w[172], w[79], w[3], pars->GC_51, pars->GC_58, amp[1340]); 
  FFV1_0(w[60], w[79], w[164], pars->GC_11, amp[1341]); 
  FFV2_0(w[172], w[82], w[2], pars->GC_100, amp[1342]); 
  FFV1_0(w[63], w[82], w[164], pars->GC_11, amp[1343]); 
  FFV1_0(w[134], w[141], w[112], pars->GC_11, amp[1344]); 
  FFV1_0(w[63], w[4], w[167], pars->GC_11, amp[1345]); 
  FFV1_0(w[176], w[133], w[126], pars->GC_11, amp[1346]); 
  FFV1_0(w[6], w[109], w[163], pars->GC_11, amp[1347]); 
  VVV1_0(w[0], w[163], w[126], pars->GC_10, amp[1348]); 
  FFV1_0(w[176], w[133], w[123], pars->GC_11, amp[1349]); 
  FFV1_0(w[75], w[4], w[163], pars->GC_11, amp[1350]); 
  VVV1_0(w[0], w[163], w[123], pars->GC_10, amp[1351]); 
  FFV1_0(w[6], w[98], w[177], pars->GC_11, amp[1352]); 
  FFV1_0(w[11], w[4], w[177], pars->GC_11, amp[1353]); 
  FFV1_0(w[11], w[171], w[0], pars->GC_11, amp[1354]); 
  FFV1_0(w[165], w[98], w[0], pars->GC_11, amp[1355]); 
  FFV1_0(w[6], w[100], w[177], pars->GC_11, amp[1356]); 
  FFV2_5_0(w[165], w[109], w[3], pars->GC_51, pars->GC_58, amp[1357]); 
  FFV1_0(w[165], w[100], w[0], pars->GC_11, amp[1358]); 
  FFV1_0(w[22], w[79], w[177], pars->GC_11, amp[1359]); 
  FFV1_0(w[22], w[109], w[164], pars->GC_11, amp[1360]); 
  FFV1_0(w[72], w[79], w[164], pars->GC_11, amp[1361]); 
  FFV1_0(w[25], w[4], w[177], pars->GC_11, amp[1362]); 
  FFV2_3_0(w[75], w[171], w[3], pars->GC_50, pars->GC_58, amp[1363]); 
  FFV1_0(w[25], w[171], w[0], pars->GC_11, amp[1364]); 
  FFV1_0(w[23], w[82], w[177], pars->GC_11, amp[1365]); 
  FFV1_0(w[75], w[82], w[164], pars->GC_11, amp[1366]); 
  FFV1_0(w[23], w[106], w[164], pars->GC_11, amp[1367]); 
  FFV1_0(w[6], w[101], w[177], pars->GC_11, amp[1368]); 
  FFV2_0(w[165], w[106], w[2], pars->GC_100, amp[1369]); 
  FFV1_0(w[165], w[101], w[0], pars->GC_11, amp[1370]); 
  FFV1_0(w[31], w[4], w[177], pars->GC_11, amp[1371]); 
  FFV2_0(w[72], w[171], w[2], pars->GC_100, amp[1372]); 
  FFV1_0(w[31], w[171], w[0], pars->GC_11, amp[1373]); 
  FFV1_0(w[6], w[109], w[167], pars->GC_11, amp[1374]); 
  FFV1_0(w[134], w[174], w[126], pars->GC_11, amp[1375]); 
  VVV1_0(w[0], w[167], w[126], pars->GC_10, amp[1376]); 
  FFV1_0(w[75], w[4], w[167], pars->GC_11, amp[1377]); 
  FFV1_0(w[134], w[174], w[123], pars->GC_11, amp[1378]); 
  VVV1_0(w[0], w[167], w[123], pars->GC_10, amp[1379]); 
  FFV1_0(w[138], w[133], w[12], pars->GC_11, amp[1380]); 
  FFV2_0(w[180], w[133], w[8], pars->GC_100, amp[1381]); 
  FFV1_0(w[6], w[19], w[184], pars->GC_11, amp[1382]); 
  FFV1_0(w[22], w[4], w[184], pars->GC_11, amp[1383]); 
  FFV1_0(w[140], w[181], w[12], pars->GC_11, amp[1384]); 
  FFV2_5_0(w[180], w[181], w[3], pars->GC_51, pars->GC_58, amp[1385]); 
  FFV1_0(w[144], w[133], w[28], pars->GC_11, amp[1386]); 
  FFV1_0(w[144], w[187], w[12], pars->GC_11, amp[1387]); 
  FFV2_0(w[180], w[187], w[2], pars->GC_100, amp[1388]); 
  FFV1_0(w[144], w[133], w[30], pars->GC_11, amp[1389]); 
  FFV2_3_0(w[144], w[189], w[3], pars->GC_50, pars->GC_58, amp[1390]); 
  FFV2_0(w[140], w[189], w[2], pars->GC_100, amp[1391]); 
  FFV1_0(w[6], w[35], w[190], pars->GC_11, amp[1392]); 
  FFV1_0(w[153], w[133], w[37], pars->GC_11, amp[1393]); 
  FFV1_0(w[149], w[187], w[37], pars->GC_11, amp[1394]); 
  FFV1_0(w[149], w[133], w[39], pars->GC_11, amp[1395]); 
  FFV1_0(w[156], w[133], w[37], pars->GC_11, amp[1396]); 
  FFV1_0(w[155], w[181], w[37], pars->GC_11, amp[1397]); 
  FFV1_0(w[159], w[133], w[37], pars->GC_11, amp[1398]); 
  FFV1_0(w[134], w[179], w[37], pars->GC_11, amp[1399]); 
  FFV1_0(w[6], w[35], w[193], pars->GC_11, amp[1400]); 
  FFV1_0(w[134], w[183], w[37], pars->GC_11, amp[1401]); 
  FFV1_0(w[134], w[181], w[39], pars->GC_11, amp[1402]); 
  FFV1_0(w[134], w[188], w[37], pars->GC_11, amp[1403]); 
  FFV1_0(w[6], w[19], w[194], pars->GC_11, amp[1404]); 
  FFV1_0(w[22], w[4], w[194], pars->GC_11, amp[1405]); 
  FFV1_0(w[149], w[195], w[12], pars->GC_11, amp[1406]); 
  FFV2_3_0(w[149], w[196], w[3], pars->GC_50, pars->GC_58, amp[1407]); 
  FFV1_0(w[155], w[197], w[12], pars->GC_11, amp[1408]); 
  FFV2_0(w[155], w[196], w[2], pars->GC_100, amp[1409]); 
  FFV1_0(w[134], w[203], w[12], pars->GC_11, amp[1410]); 
  FFV2_0(w[134], w[196], w[8], pars->GC_100, amp[1411]); 
  FFV1_0(w[134], w[197], w[28], pars->GC_11, amp[1412]); 
  FFV1_0(w[134], w[197], w[30], pars->GC_11, amp[1413]); 
  FFV2_5_0(w[205], w[197], w[3], pars->GC_51, pars->GC_58, amp[1414]); 
  FFV2_0(w[205], w[195], w[2], pars->GC_100, amp[1415]); 
  FFV1_0(w[60], w[4], w[190], pars->GC_11, amp[1416]); 
  FFV1_0(w[153], w[133], w[61], pars->GC_11, amp[1417]); 
  FFV1_0(w[149], w[133], w[62], pars->GC_11, amp[1418]); 
  FFV1_0(w[149], w[187], w[61], pars->GC_11, amp[1419]); 
  FFV1_0(w[156], w[133], w[61], pars->GC_11, amp[1420]); 
  FFV1_0(w[155], w[181], w[61], pars->GC_11, amp[1421]); 
  FFV1_0(w[159], w[133], w[61], pars->GC_11, amp[1422]); 
  FFV1_0(w[134], w[179], w[61], pars->GC_11, amp[1423]); 
  FFV1_0(w[60], w[4], w[193], pars->GC_11, amp[1424]); 
  FFV1_0(w[134], w[183], w[61], pars->GC_11, amp[1425]); 
  FFV1_0(w[134], w[181], w[62], pars->GC_11, amp[1426]); 
  FFV1_0(w[134], w[188], w[61], pars->GC_11, amp[1427]); 
  FFV1_0(w[173], w[133], w[28], pars->GC_11, amp[1428]); 
  FFV1_0(w[6], w[69], w[190], pars->GC_11, amp[1429]); 
  VVV1_0(w[0], w[190], w[28], pars->GC_10, amp[1430]); 
  FFV1_0(w[173], w[187], w[12], pars->GC_11, amp[1431]); 
  FFV1_0(w[149], w[209], w[12], pars->GC_11, amp[1432]); 
  FFV1_0(w[149], w[187], w[71], pars->GC_11, amp[1433]); 
  FFV1_0(w[173], w[133], w[30], pars->GC_11, amp[1434]); 
  FFV1_0(w[72], w[4], w[190], pars->GC_11, amp[1435]); 
  VVV1_0(w[0], w[190], w[30], pars->GC_10, amp[1436]); 
  FFV2_3_0(w[173], w[189], w[3], pars->GC_50, pars->GC_58, amp[1437]); 
  FFV1_0(w[153], w[133], w[71], pars->GC_11, amp[1438]); 
  FFV1_0(w[153], w[189], w[0], pars->GC_11, amp[1439]); 
  FFV1_0(w[176], w[181], w[12], pars->GC_11, amp[1440]); 
  FFV1_0(w[155], w[210], w[12], pars->GC_11, amp[1441]); 
  FFV1_0(w[155], w[181], w[71], pars->GC_11, amp[1442]); 
  FFV2_0(w[176], w[189], w[2], pars->GC_100, amp[1443]); 
  FFV1_0(w[156], w[133], w[71], pars->GC_11, amp[1444]); 
  FFV1_0(w[156], w[189], w[0], pars->GC_11, amp[1445]); 
  FFV1_0(w[159], w[133], w[71], pars->GC_11, amp[1446]); 
  FFV1_0(w[134], w[179], w[71], pars->GC_11, amp[1447]); 
  FFV1_0(w[159], w[189], w[0], pars->GC_11, amp[1448]); 
  FFV1_0(w[205], w[179], w[0], pars->GC_11, amp[1449]); 
  FFV1_0(w[134], w[210], w[28], pars->GC_11, amp[1450]); 
  FFV1_0(w[6], w[69], w[193], pars->GC_11, amp[1451]); 
  VVV1_0(w[0], w[193], w[28], pars->GC_10, amp[1452]); 
  FFV1_0(w[134], w[210], w[30], pars->GC_11, amp[1453]); 
  FFV1_0(w[72], w[4], w[193], pars->GC_11, amp[1454]); 
  VVV1_0(w[0], w[193], w[30], pars->GC_10, amp[1455]); 
  FFV2_5_0(w[205], w[210], w[3], pars->GC_51, pars->GC_58, amp[1456]); 
  FFV1_0(w[134], w[183], w[71], pars->GC_11, amp[1457]); 
  FFV1_0(w[205], w[183], w[0], pars->GC_11, amp[1458]); 
  FFV2_0(w[205], w[209], w[2], pars->GC_100, amp[1459]); 
  FFV1_0(w[134], w[188], w[71], pars->GC_11, amp[1460]); 
  FFV1_0(w[205], w[188], w[0], pars->GC_11, amp[1461]); 
  FFV1_0(w[138], w[133], w[12], pars->GC_11, amp[1462]); 
  FFV2_0(w[180], w[133], w[8], pars->GC_100, amp[1463]); 
  FFV1_0(w[6], w[82], w[184], pars->GC_11, amp[1464]); 
  FFV1_0(w[81], w[4], w[184], pars->GC_11, amp[1465]); 
  FFV1_0(w[140], w[181], w[12], pars->GC_11, amp[1466]); 
  FFV2_5_0(w[180], w[181], w[3], pars->GC_51, pars->GC_58, amp[1467]); 
  FFV1_0(w[144], w[187], w[12], pars->GC_11, amp[1468]); 
  FFV2_0(w[180], w[187], w[2], pars->GC_100, amp[1469]); 
  FFV1_0(w[144], w[133], w[83], pars->GC_11, amp[1470]); 
  FFV1_0(w[144], w[133], w[85], pars->GC_11, amp[1471]); 
  FFV2_3_0(w[144], w[189], w[3], pars->GC_50, pars->GC_58, amp[1472]); 
  FFV2_0(w[140], w[189], w[2], pars->GC_100, amp[1473]); 
  FFV1_0(w[6], w[82], w[194], pars->GC_11, amp[1474]); 
  FFV1_0(w[81], w[4], w[194], pars->GC_11, amp[1475]); 
  FFV1_0(w[149], w[195], w[12], pars->GC_11, amp[1476]); 
  FFV2_3_0(w[149], w[196], w[3], pars->GC_50, pars->GC_58, amp[1477]); 
  FFV1_0(w[155], w[197], w[12], pars->GC_11, amp[1478]); 
  FFV2_0(w[155], w[196], w[2], pars->GC_100, amp[1479]); 
  FFV1_0(w[134], w[203], w[12], pars->GC_11, amp[1480]); 
  FFV2_0(w[134], w[196], w[8], pars->GC_100, amp[1481]); 
  FFV1_0(w[134], w[197], w[83], pars->GC_11, amp[1482]); 
  FFV1_0(w[134], w[197], w[85], pars->GC_11, amp[1483]); 
  FFV2_5_0(w[205], w[197], w[3], pars->GC_51, pars->GC_58, amp[1484]); 
  FFV2_0(w[205], w[195], w[2], pars->GC_100, amp[1485]); 
  FFV1_0(w[6], w[88], w[190], pars->GC_11, amp[1486]); 
  FFV1_0(w[153], w[133], w[37], pars->GC_11, amp[1487]); 
  FFV1_0(w[149], w[187], w[37], pars->GC_11, amp[1488]); 
  FFV1_0(w[149], w[133], w[90], pars->GC_11, amp[1489]); 
  FFV1_0(w[156], w[133], w[37], pars->GC_11, amp[1490]); 
  FFV1_0(w[155], w[181], w[37], pars->GC_11, amp[1491]); 
  FFV1_0(w[159], w[133], w[37], pars->GC_11, amp[1492]); 
  FFV1_0(w[134], w[179], w[37], pars->GC_11, amp[1493]); 
  FFV1_0(w[6], w[88], w[193], pars->GC_11, amp[1494]); 
  FFV1_0(w[134], w[183], w[37], pars->GC_11, amp[1495]); 
  FFV1_0(w[134], w[181], w[90], pars->GC_11, amp[1496]); 
  FFV1_0(w[134], w[188], w[37], pars->GC_11, amp[1497]); 
  FFV1_0(w[102], w[4], w[190], pars->GC_11, amp[1498]); 
  FFV1_0(w[153], w[133], w[61], pars->GC_11, amp[1499]); 
  FFV1_0(w[149], w[187], w[61], pars->GC_11, amp[1500]); 
  FFV1_0(w[149], w[133], w[104], pars->GC_11, amp[1501]); 
  FFV1_0(w[156], w[133], w[61], pars->GC_11, amp[1502]); 
  FFV1_0(w[155], w[181], w[61], pars->GC_11, amp[1503]); 
  FFV1_0(w[159], w[133], w[61], pars->GC_11, amp[1504]); 
  FFV1_0(w[134], w[179], w[61], pars->GC_11, amp[1505]); 
  FFV1_0(w[102], w[4], w[193], pars->GC_11, amp[1506]); 
  FFV1_0(w[134], w[183], w[61], pars->GC_11, amp[1507]); 
  FFV1_0(w[134], w[181], w[104], pars->GC_11, amp[1508]); 
  FFV1_0(w[134], w[188], w[61], pars->GC_11, amp[1509]); 
  FFV1_0(w[173], w[187], w[12], pars->GC_11, amp[1510]); 
  FFV1_0(w[149], w[209], w[12], pars->GC_11, amp[1511]); 
  FFV1_0(w[149], w[187], w[71], pars->GC_11, amp[1512]); 
  FFV1_0(w[173], w[133], w[83], pars->GC_11, amp[1513]); 
  FFV1_0(w[6], w[106], w[190], pars->GC_11, amp[1514]); 
  VVV1_0(w[0], w[190], w[83], pars->GC_10, amp[1515]); 
  FFV1_0(w[173], w[133], w[85], pars->GC_11, amp[1516]); 
  FFV1_0(w[108], w[4], w[190], pars->GC_11, amp[1517]); 
  VVV1_0(w[0], w[190], w[85], pars->GC_10, amp[1518]); 
  FFV2_3_0(w[173], w[189], w[3], pars->GC_50, pars->GC_58, amp[1519]); 
  FFV1_0(w[153], w[133], w[71], pars->GC_11, amp[1520]); 
  FFV1_0(w[153], w[189], w[0], pars->GC_11, amp[1521]); 
  FFV1_0(w[176], w[181], w[12], pars->GC_11, amp[1522]); 
  FFV1_0(w[155], w[210], w[12], pars->GC_11, amp[1523]); 
  FFV1_0(w[155], w[181], w[71], pars->GC_11, amp[1524]); 
  FFV2_0(w[176], w[189], w[2], pars->GC_100, amp[1525]); 
  FFV1_0(w[156], w[133], w[71], pars->GC_11, amp[1526]); 
  FFV1_0(w[156], w[189], w[0], pars->GC_11, amp[1527]); 
  FFV1_0(w[159], w[133], w[71], pars->GC_11, amp[1528]); 
  FFV1_0(w[134], w[179], w[71], pars->GC_11, amp[1529]); 
  FFV1_0(w[159], w[189], w[0], pars->GC_11, amp[1530]); 
  FFV1_0(w[205], w[179], w[0], pars->GC_11, amp[1531]); 
  FFV1_0(w[134], w[210], w[83], pars->GC_11, amp[1532]); 
  FFV1_0(w[6], w[106], w[193], pars->GC_11, amp[1533]); 
  VVV1_0(w[0], w[193], w[83], pars->GC_10, amp[1534]); 
  FFV1_0(w[134], w[210], w[85], pars->GC_11, amp[1535]); 
  FFV1_0(w[108], w[4], w[193], pars->GC_11, amp[1536]); 
  VVV1_0(w[0], w[193], w[85], pars->GC_10, amp[1537]); 
  FFV2_5_0(w[205], w[210], w[3], pars->GC_51, pars->GC_58, amp[1538]); 
  FFV1_0(w[134], w[183], w[71], pars->GC_11, amp[1539]); 
  FFV1_0(w[205], w[183], w[0], pars->GC_11, amp[1540]); 
  FFV2_0(w[205], w[209], w[2], pars->GC_100, amp[1541]); 
  FFV1_0(w[134], w[188], w[71], pars->GC_11, amp[1542]); 
  FFV1_0(w[205], w[188], w[0], pars->GC_11, amp[1543]); 
  FFV1_0(w[138], w[4], w[137], pars->GC_11, amp[1544]); 
  FFV2_0(w[139], w[4], w[8], pars->GC_100, amp[1545]); 
  FFV1_0(w[6], w[187], w[142], pars->GC_11, amp[1546]); 
  FFV1_0(w[81], w[133], w[142], pars->GC_11, amp[1547]); 
  FFV1_0(w[140], w[79], w[137], pars->GC_11, amp[1548]); 
  FFV2_5_0(w[139], w[79], w[3], pars->GC_51, pars->GC_58, amp[1549]); 
  FFV1_0(w[144], w[4], w[212], pars->GC_11, amp[1550]); 
  FFV1_0(w[144], w[82], w[137], pars->GC_11, amp[1551]); 
  FFV2_0(w[139], w[82], w[2], pars->GC_100, amp[1552]); 
  FFV1_0(w[144], w[4], w[213], pars->GC_11, amp[1553]); 
  FFV2_3_0(w[144], w[147], w[3], pars->GC_50, pars->GC_58, amp[1554]); 
  FFV2_0(w[140], w[147], w[2], pars->GC_100, amp[1555]); 
  FFV1_0(w[6], w[195], w[151], pars->GC_11, amp[1556]); 
  FFV1_0(w[153], w[4], w[152], pars->GC_11, amp[1557]); 
  FFV1_0(w[149], w[82], w[152], pars->GC_11, amp[1558]); 
  FFV1_0(w[149], w[4], w[214], pars->GC_11, amp[1559]); 
  FFV1_0(w[156], w[4], w[152], pars->GC_11, amp[1560]); 
  FFV1_0(w[155], w[79], w[152], pars->GC_11, amp[1561]); 
  FFV1_0(w[159], w[4], w[152], pars->GC_11, amp[1562]); 
  FFV1_0(w[134], w[98], w[152], pars->GC_11, amp[1563]); 
  FFV1_0(w[6], w[195], w[160], pars->GC_11, amp[1564]); 
  FFV1_0(w[134], w[100], w[152], pars->GC_11, amp[1565]); 
  FFV1_0(w[134], w[79], w[214], pars->GC_11, amp[1566]); 
  FFV1_0(w[134], w[101], w[152], pars->GC_11, amp[1567]); 
  FFV1_0(w[6], w[187], w[161], pars->GC_11, amp[1568]); 
  FFV1_0(w[81], w[133], w[161], pars->GC_11, amp[1569]); 
  FFV1_0(w[149], w[88], w[137], pars->GC_11, amp[1570]); 
  FFV2_3_0(w[149], w[162], w[3], pars->GC_50, pars->GC_58, amp[1571]); 
  FFV1_0(w[155], w[92], w[137], pars->GC_11, amp[1572]); 
  FFV2_0(w[155], w[162], w[2], pars->GC_100, amp[1573]); 
  FFV1_0(w[134], w[93], w[137], pars->GC_11, amp[1574]); 
  FFV2_0(w[134], w[162], w[8], pars->GC_100, amp[1575]); 
  FFV1_0(w[134], w[92], w[212], pars->GC_11, amp[1576]); 
  FFV1_0(w[134], w[92], w[213], pars->GC_11, amp[1577]); 
  FFV2_5_0(w[168], w[92], w[3], pars->GC_51, pars->GC_58, amp[1578]); 
  FFV2_0(w[168], w[88], w[2], pars->GC_100, amp[1579]); 
  FFV1_0(w[102], w[133], w[151], pars->GC_11, amp[1580]); 
  FFV1_0(w[153], w[4], w[169], pars->GC_11, amp[1581]); 
  FFV1_0(w[149], w[4], w[215], pars->GC_11, amp[1582]); 
  FFV1_0(w[149], w[82], w[169], pars->GC_11, amp[1583]); 
  FFV1_0(w[156], w[4], w[169], pars->GC_11, amp[1584]); 
  FFV1_0(w[155], w[79], w[169], pars->GC_11, amp[1585]); 
  FFV1_0(w[159], w[4], w[169], pars->GC_11, amp[1586]); 
  FFV1_0(w[134], w[98], w[169], pars->GC_11, amp[1587]); 
  FFV1_0(w[102], w[133], w[160], pars->GC_11, amp[1588]); 
  FFV1_0(w[134], w[100], w[169], pars->GC_11, amp[1589]); 
  FFV1_0(w[134], w[79], w[215], pars->GC_11, amp[1590]); 
  FFV1_0(w[134], w[101], w[169], pars->GC_11, amp[1591]); 
  FFV1_0(w[173], w[4], w[212], pars->GC_11, amp[1592]); 
  FFV1_0(w[6], w[209], w[151], pars->GC_11, amp[1593]); 
  VVV1_0(w[0], w[151], w[212], pars->GC_10, amp[1594]); 
  FFV1_0(w[173], w[82], w[137], pars->GC_11, amp[1595]); 
  FFV1_0(w[149], w[106], w[137], pars->GC_11, amp[1596]); 
  FFV1_0(w[149], w[82], w[175], pars->GC_11, amp[1597]); 
  FFV1_0(w[173], w[4], w[213], pars->GC_11, amp[1598]); 
  FFV1_0(w[108], w[133], w[151], pars->GC_11, amp[1599]); 
  VVV1_0(w[0], w[151], w[213], pars->GC_10, amp[1600]); 
  FFV2_3_0(w[173], w[147], w[3], pars->GC_50, pars->GC_58, amp[1601]); 
  FFV1_0(w[153], w[4], w[175], pars->GC_11, amp[1602]); 
  FFV1_0(w[153], w[147], w[0], pars->GC_11, amp[1603]); 
  FFV1_0(w[176], w[79], w[137], pars->GC_11, amp[1604]); 
  FFV1_0(w[155], w[109], w[137], pars->GC_11, amp[1605]); 
  FFV1_0(w[155], w[79], w[175], pars->GC_11, amp[1606]); 
  FFV2_0(w[176], w[147], w[2], pars->GC_100, amp[1607]); 
  FFV1_0(w[156], w[4], w[175], pars->GC_11, amp[1608]); 
  FFV1_0(w[156], w[147], w[0], pars->GC_11, amp[1609]); 
  FFV1_0(w[159], w[4], w[175], pars->GC_11, amp[1610]); 
  FFV1_0(w[134], w[98], w[175], pars->GC_11, amp[1611]); 
  FFV1_0(w[159], w[147], w[0], pars->GC_11, amp[1612]); 
  FFV1_0(w[168], w[98], w[0], pars->GC_11, amp[1613]); 
  FFV1_0(w[134], w[109], w[212], pars->GC_11, amp[1614]); 
  FFV1_0(w[6], w[209], w[160], pars->GC_11, amp[1615]); 
  VVV1_0(w[0], w[160], w[212], pars->GC_10, amp[1616]); 
  FFV1_0(w[134], w[109], w[213], pars->GC_11, amp[1617]); 
  FFV1_0(w[108], w[133], w[160], pars->GC_11, amp[1618]); 
  VVV1_0(w[0], w[160], w[213], pars->GC_10, amp[1619]); 
  FFV2_5_0(w[168], w[109], w[3], pars->GC_51, pars->GC_58, amp[1620]); 
  FFV1_0(w[134], w[100], w[175], pars->GC_11, amp[1621]); 
  FFV1_0(w[168], w[100], w[0], pars->GC_11, amp[1622]); 
  FFV2_0(w[168], w[106], w[2], pars->GC_100, amp[1623]); 
  FFV1_0(w[134], w[101], w[175], pars->GC_11, amp[1624]); 
  FFV1_0(w[168], w[101], w[0], pars->GC_11, amp[1625]); 


}
double PY8MEs_R12_P34_sm_gq_wpzqqq::matrix_12_gu_wpzudux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 164;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[0] + 1./3. * amp[1] + amp[2] + amp[3] +
      1./3. * amp[4] + 1./3. * amp[5] + amp[6] + 1./3. * amp[7] + amp[8] +
      1./3. * amp[9] + amp[10] + amp[11] + 1./3. * amp[12] + 1./3. * amp[13] +
      1./3. * amp[14] + 1./3. * amp[15] + amp[16] + 1./3. * amp[17] + amp[18] +
      amp[19] + amp[20] + 1./3. * amp[21] + amp[22] + amp[23] + amp[24] +
      amp[25] + amp[26] + amp[27] + amp[28] + 1./3. * amp[29] + amp[30] + 1./3.
      * amp[31] + 1./3. * amp[32] + 1./3. * amp[33] + amp[34] + amp[35] + 1./3.
      * amp[36] + amp[37] + 1./3. * amp[38] + amp[39] + 1./3. * amp[40] +
      amp[41] + 1./3. * amp[42] + 1./3. * amp[43] + 1./3. * amp[44] + 1./3. *
      amp[45] + amp[46] + 1./3. * amp[47] + amp[96] + amp[97] - Complex<double>
      (0, 1) * amp[98] + amp[99] + Complex<double> (0, 1) * amp[101] + amp[102]
      - Complex<double> (0, 1) * amp[104] + amp[105] + Complex<double> (0, 1) *
      amp[106] + amp[107] + 1./3. * amp[108] + amp[111] + Complex<double> (0,
      1) * amp[113] + 1./3. * amp[114] + amp[117] + Complex<double> (0, 1) *
      amp[118] + amp[119] + Complex<double> (0, 1) * amp[142] + Complex<double>
      (0, 1) * amp[143] + amp[144] + amp[148] + 1./3. * amp[149] -
      Complex<double> (0, 1) * amp[150] - Complex<double> (0, 1) * amp[154] +
      Complex<double> (0, 1) * amp[156] + 1./3. * amp[159] + Complex<double>
      (0, 1) * amp[162]);
  jamp[1] = +1./2. * (-1./3. * amp[24] - 1./3. * amp[25] - 1./3. * amp[26] -
      1./3. * amp[27] - 1./3. * amp[28] - amp[29] - 1./3. * amp[30] - amp[31] -
      amp[32] - amp[33] - 1./3. * amp[34] - 1./3. * amp[35] - amp[36] - 1./3. *
      amp[37] - amp[38] - 1./3. * amp[39] - amp[40] - 1./3. * amp[41] - amp[42]
      - amp[43] - amp[44] - amp[45] - 1./3. * amp[46] - amp[47] - 1./3. *
      amp[72] - 1./3. * amp[73] - 1./3. * amp[74] - 1./3. * amp[75] - amp[76] -
      1./3. * amp[77] - amp[78] - 1./3. * amp[79] - amp[80] - amp[81] - amp[82]
      - amp[83] - amp[84] - amp[85] - amp[86] - amp[87] - 1./3. * amp[88] -
      1./3. * amp[89] - 1./3. * amp[90] - 1./3. * amp[91] - amp[92] - 1./3. *
      amp[93] - amp[94] - 1./3. * amp[95] - 1./3. * amp[97] - 1./3. * amp[103]
      - Complex<double> (0, 1) * amp[110] - amp[115] - Complex<double> (0, 1) *
      amp[116] - Complex<double> (0, 1) * amp[120] - Complex<double> (0, 1) *
      amp[121] - amp[122] - Complex<double> (0, 1) * amp[124] - Complex<double>
      (0, 1) * amp[127] - amp[129] - Complex<double> (0, 1) * amp[130] -
      amp[131] - amp[132] - Complex<double> (0, 1) * amp[133] - amp[134] -
      Complex<double> (0, 1) * amp[136] - Complex<double> (0, 1) * amp[139] -
      amp[140] - amp[141] - 1./3. * amp[148] - amp[149] - Complex<double> (0,
      1) * amp[151] - 1./3. * amp[153] - amp[158] - amp[159] - Complex<double>
      (0, 1) * amp[160]);
  jamp[2] = +1./2. * (-amp[0] - amp[1] - 1./3. * amp[2] - 1./3. * amp[3] -
      amp[4] - amp[5] - 1./3. * amp[6] - amp[7] - 1./3. * amp[8] - amp[9] -
      1./3. * amp[10] - 1./3. * amp[11] - amp[12] - amp[13] - amp[14] - amp[15]
      - 1./3. * amp[16] - amp[17] - 1./3. * amp[18] - 1./3. * amp[19] - 1./3. *
      amp[20] - amp[21] - 1./3. * amp[22] - 1./3. * amp[23] - 1./3. * amp[48] -
      1./3. * amp[49] - 1./3. * amp[50] - 1./3. * amp[51] - amp[52] - amp[53] -
      1./3. * amp[54] - 1./3. * amp[55] - amp[56] - amp[57] - amp[58] - amp[59]
      - amp[60] - amp[61] - amp[62] - amp[63] - 1./3. * amp[64] - 1./3. *
      amp[65] - amp[66] - amp[67] - 1./3. * amp[68] - 1./3. * amp[69] - 1./3. *
      amp[70] - 1./3. * amp[71] - 1./3. * amp[96] - 1./3. * amp[99] - 1./3. *
      amp[100] - 1./3. * amp[102] - 1./3. * amp[105] - 1./3. * amp[107] -
      amp[108] - amp[109] + Complex<double> (0, 1) * amp[110] - 1./3. *
      amp[111] - 1./3. * amp[112] - amp[114] + Complex<double> (0, 1) *
      amp[116] - 1./3. * amp[117] - 1./3. * amp[119] + Complex<double> (0, 1) *
      amp[120] + Complex<double> (0, 1) * amp[121] - amp[123] + Complex<double>
      (0, 1) * amp[124] - amp[125] - amp[126] + Complex<double> (0, 1) *
      amp[127] - amp[128] + Complex<double> (0, 1) * amp[130] + Complex<double>
      (0, 1) * amp[133] - amp[135] + Complex<double> (0, 1) * amp[136] -
      amp[137] - amp[138] + Complex<double> (0, 1) * amp[139] - 1./3. *
      amp[144] - 1./3. * amp[145] - amp[146] - 1./3. * amp[147] +
      Complex<double> (0, 1) * amp[151] - 1./3. * amp[152] - 1./3. * amp[155] -
      1./3. * amp[157] + Complex<double> (0, 1) * amp[160] - 1./3. * amp[161] -
      1./3. * amp[163]);
  jamp[3] = +1./2. * (+amp[48] + amp[49] + amp[50] + amp[51] + 1./3. * amp[52]
      + 1./3. * amp[53] + amp[54] + amp[55] + 1./3. * amp[56] + 1./3. * amp[57]
      + 1./3. * amp[58] + 1./3. * amp[59] + 1./3. * amp[60] + 1./3. * amp[61] +
      1./3. * amp[62] + 1./3. * amp[63] + amp[64] + amp[65] + 1./3. * amp[66] +
      1./3. * amp[67] + amp[68] + amp[69] + amp[70] + amp[71] + amp[72] +
      amp[73] + amp[74] + amp[75] + 1./3. * amp[76] + amp[77] + 1./3. * amp[78]
      + amp[79] + 1./3. * amp[80] + 1./3. * amp[81] + 1./3. * amp[82] + 1./3. *
      amp[83] + 1./3. * amp[84] + 1./3. * amp[85] + 1./3. * amp[86] + 1./3. *
      amp[87] + amp[88] + amp[89] + amp[90] + amp[91] + 1./3. * amp[92] +
      amp[93] + 1./3. * amp[94] + amp[95] + Complex<double> (0, 1) * amp[98] +
      amp[100] - Complex<double> (0, 1) * amp[101] + amp[103] + Complex<double>
      (0, 1) * amp[104] - Complex<double> (0, 1) * amp[106] + 1./3. * amp[109]
      + amp[112] - Complex<double> (0, 1) * amp[113] + 1./3. * amp[115] -
      Complex<double> (0, 1) * amp[118] + 1./3. * amp[122] + 1./3. * amp[123] +
      1./3. * amp[125] + 1./3. * amp[126] + 1./3. * amp[128] + 1./3. * amp[129]
      + 1./3. * amp[131] + 1./3. * amp[132] + 1./3. * amp[134] + 1./3. *
      amp[135] + 1./3. * amp[137] + 1./3. * amp[138] + 1./3. * amp[140] + 1./3.
      * amp[141] - Complex<double> (0, 1) * amp[142] - Complex<double> (0, 1) *
      amp[143] + amp[145] + 1./3. * amp[146] + amp[147] + Complex<double> (0,
      1) * amp[150] + amp[152] + amp[153] + Complex<double> (0, 1) * amp[154] +
      amp[155] - Complex<double> (0, 1) * amp[156] + amp[157] + 1./3. *
      amp[158] + amp[161] - Complex<double> (0, 1) * amp[162] + amp[163]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[0][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P34_sm_gq_wpzqqq::matrix_12_gu_wpzdddx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 164;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[164] + amp[165] + 1./3. * amp[166] + 1./3. *
      amp[167] + 1./3. * amp[168] + 1./3. * amp[169] + 1./3. * amp[170] + 1./3.
      * amp[171] + amp[172] + amp[173] + amp[10] + amp[11] + amp[174] + 1./3. *
      amp[175] + 1./3. * amp[176] + 1./3. * amp[177] + amp[178] + amp[179] +
      amp[180] + 1./3. * amp[181] + amp[182] + amp[183] + 1./3. * amp[184] +
      1./3. * amp[185] + amp[186] + amp[25] + 1./3. * amp[187] + amp[188] +
      1./3. * amp[189] + amp[190] + 1./3. * amp[191] + 1./3. * amp[192] +
      amp[193] + amp[194] + 1./3. * amp[195] + 1./3. * amp[196] + amp[197] +
      amp[198] + 1./3. * amp[199] + 1./3. * amp[200] + amp[201] + amp[202] +
      amp[203] + 1./3. * amp[204] + amp[205] + 1./3. * amp[206] + 1./3. *
      amp[207] + 1./3. * amp[208] + amp[254] + amp[255] - Complex<double> (0,
      1) * amp[256] + 1./3. * amp[257] + 1./3. * amp[258] + 1./3. * amp[260] +
      amp[263] + Complex<double> (0, 1) * amp[265] + amp[266] + 1./3. *
      amp[267] - Complex<double> (0, 1) * amp[271] + amp[272] + Complex<double>
      (0, 1) * amp[273] + amp[274] + 1./3. * amp[275] + 1./3. * amp[277] +
      1./3. * amp[278] + 1./3. * amp[279] + amp[281] + Complex<double> (0, 1) *
      amp[283] + amp[284] + Complex<double> (0, 1) * amp[285] + amp[286] +
      1./3. * amp[287] + 1./3. * amp[289] + Complex<double> (0, 1) * amp[290] +
      Complex<double> (0, 1) * amp[291] + amp[292] + 1./3. * amp[296] + 1./3. *
      amp[297] + 1./3. * amp[298] + 1./3. * amp[301] + 1./3. * amp[304] + 1./3.
      * amp[306] + amp[308] - Complex<double> (0, 1) * amp[309] -
      Complex<double> (0, 1) * amp[312] + Complex<double> (0, 1) * amp[156] +
      1./3. * amp[313] + 1./3. * amp[315] + Complex<double> (0, 1) * amp[162]);
  jamp[1] = +1./2. * (-1./3. * amp[186] - 1./3. * amp[25] - amp[187] - 1./3. *
      amp[188] - amp[189] - 1./3. * amp[190] - amp[191] - amp[192] - 1./3. *
      amp[193] - 1./3. * amp[194] - amp[195] - amp[196] - 1./3. * amp[197] -
      1./3. * amp[198] - amp[199] - amp[200] - 1./3. * amp[201] - 1./3. *
      amp[202] - 1./3. * amp[203] - amp[204] - 1./3. * amp[205] - amp[206] -
      amp[207] - amp[208] - amp[231] - 1./3. * amp[232] - 1./3. * amp[233] -
      amp[234] - 1./3. * amp[235] - amp[236] - amp[237] - 1./3. * amp[238] -
      1./3. * amp[239] - amp[240] - amp[241] - 1./3. * amp[242] - 1./3. *
      amp[243] - 1./3. * amp[244] - amp[245] - amp[246] - amp[247] - amp[248] -
      amp[249] - 1./3. * amp[250] - 1./3. * amp[251] - 1./3. * amp[252] -
      amp[253] - 1./3. * amp[95] - 1./3. * amp[255] - amp[258] +
      Complex<double> (0, 1) * amp[259] - Complex<double> (0, 1) * amp[262] -
      amp[268] - 1./3. * amp[269] - Complex<double> (0, 1) * amp[270] +
      Complex<double> (0, 1) * amp[276] - amp[279] + Complex<double> (0, 1) *
      amp[280] + Complex<double> (0, 1) * amp[288] + Complex<double> (0, 1) *
      amp[294] + Complex<double> (0, 1) * amp[295] - amp[297] - amp[298] -
      Complex<double> (0, 1) * amp[300] - amp[301] - amp[302] - Complex<double>
      (0, 1) * amp[303] - amp[304] + Complex<double> (0, 1) * amp[305] -
      amp[306] - 1./3. * amp[308] - 1./3. * amp[311] - amp[313] +
      Complex<double> (0, 1) * amp[314] - amp[315]);
  jamp[2] = +1./2. * (-1./3. * amp[164] - 1./3. * amp[165] - amp[166] -
      amp[167] - amp[168] - amp[169] - amp[170] - amp[171] - 1./3. * amp[172] -
      1./3. * amp[173] - 1./3. * amp[10] - 1./3. * amp[11] - 1./3. * amp[174] -
      amp[175] - amp[176] - amp[177] - 1./3. * amp[178] - 1./3. * amp[179] -
      1./3. * amp[180] - amp[181] - 1./3. * amp[182] - 1./3. * amp[183] -
      amp[184] - amp[185] - amp[209] - amp[210] - 1./3. * amp[211] - amp[212] -
      1./3. * amp[213] - amp[214] - 1./3. * amp[215] - 1./3. * amp[216] -
      amp[217] - amp[218] - 1./3. * amp[219] - 1./3. * amp[220] - amp[221] -
      amp[222] - 1./3. * amp[223] - 1./3. * amp[224] - amp[225] - amp[226] -
      amp[227] - 1./3. * amp[228] - amp[229] - 1./3. * amp[230] - 1./3. *
      amp[70] - 1./3. * amp[71] - 1./3. * amp[254] - amp[257] - Complex<double>
      (0, 1) * amp[259] - amp[260] - amp[261] + Complex<double> (0, 1) *
      amp[262] - 1./3. * amp[263] - 1./3. * amp[264] - 1./3. * amp[266] -
      amp[267] + Complex<double> (0, 1) * amp[270] - 1./3. * amp[272] - 1./3. *
      amp[274] - amp[275] - Complex<double> (0, 1) * amp[276] - amp[277] -
      amp[278] - Complex<double> (0, 1) * amp[280] - 1./3. * amp[281] - 1./3. *
      amp[282] - 1./3. * amp[284] - 1./3. * amp[286] - amp[287] -
      Complex<double> (0, 1) * amp[288] - amp[289] - 1./3. * amp[292] - 1./3. *
      amp[293] - Complex<double> (0, 1) * amp[294] - Complex<double> (0, 1) *
      amp[295] - amp[296] - amp[299] + Complex<double> (0, 1) * amp[300] +
      Complex<double> (0, 1) * amp[303] - Complex<double> (0, 1) * amp[305] -
      1./3. * amp[307] - 1./3. * amp[310] - 1./3. * amp[155] - 1./3. * amp[157]
      - Complex<double> (0, 1) * amp[314] - 1./3. * amp[161] - 1./3. *
      amp[163]);
  jamp[3] = +1./2. * (+1./3. * amp[209] + 1./3. * amp[210] + amp[211] + 1./3. *
      amp[212] + amp[213] + 1./3. * amp[214] + amp[215] + amp[216] + 1./3. *
      amp[217] + 1./3. * amp[218] + amp[219] + amp[220] + 1./3. * amp[221] +
      1./3. * amp[222] + amp[223] + amp[224] + 1./3. * amp[225] + 1./3. *
      amp[226] + 1./3. * amp[227] + amp[228] + 1./3. * amp[229] + amp[230] +
      amp[70] + amp[71] + 1./3. * amp[231] + amp[232] + amp[233] + 1./3. *
      amp[234] + amp[235] + 1./3. * amp[236] + 1./3. * amp[237] + amp[238] +
      amp[239] + 1./3. * amp[240] + 1./3. * amp[241] + amp[242] + amp[243] +
      amp[244] + 1./3. * amp[245] + 1./3. * amp[246] + 1./3. * amp[247] + 1./3.
      * amp[248] + 1./3. * amp[249] + amp[250] + amp[251] + amp[252] + 1./3. *
      amp[253] + amp[95] + Complex<double> (0, 1) * amp[256] + 1./3. * amp[261]
      + amp[264] - Complex<double> (0, 1) * amp[265] + 1./3. * amp[268] +
      amp[269] + Complex<double> (0, 1) * amp[271] - Complex<double> (0, 1) *
      amp[273] + amp[282] - Complex<double> (0, 1) * amp[283] - Complex<double>
      (0, 1) * amp[285] - Complex<double> (0, 1) * amp[290] - Complex<double>
      (0, 1) * amp[291] + amp[293] + 1./3. * amp[299] + 1./3. * amp[302] +
      amp[307] + Complex<double> (0, 1) * amp[309] + amp[310] + amp[311] +
      Complex<double> (0, 1) * amp[312] + amp[155] - Complex<double> (0, 1) *
      amp[156] + amp[157] + amp[161] - Complex<double> (0, 1) * amp[162] +
      amp[163]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[1][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P34_sm_gq_wpzqqq::matrix_12_gd_wpzddux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 164;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[340] + 1./3. * amp[341] + amp[342] + 1./3. *
      amp[343] + amp[344] + 1./3. * amp[345] + amp[346] + amp[347] + 1./3. *
      amp[348] + 1./3. * amp[349] + amp[350] + amp[351] + 1./3. * amp[352] +
      1./3. * amp[353] + amp[354] + amp[355] + 1./3. * amp[356] + 1./3. *
      amp[357] + 1./3. * amp[358] + amp[359] + 1./3. * amp[360] + amp[361] +
      amp[362] + amp[363] + amp[388] + 1./3. * amp[389] + 1./3. * amp[390] +
      amp[391] + 1./3. * amp[392] + amp[393] + amp[394] + 1./3. * amp[395] +
      1./3. * amp[396] + amp[397] + amp[398] + 1./3. * amp[399] + 1./3. *
      amp[400] + 1./3. * amp[401] + amp[402] + amp[403] + amp[404] + amp[405] +
      amp[406] + 1./3. * amp[407] + 1./3. * amp[408] + 1./3. * amp[409] +
      amp[410] + 1./3. * amp[411] + 1./3. * amp[413] + amp[416] -
      Complex<double> (0, 1) * amp[417] + Complex<double> (0, 1) * amp[420] +
      amp[426] + 1./3. * amp[427] + Complex<double> (0, 1) * amp[428] -
      Complex<double> (0, 1) * amp[434] + amp[437] - Complex<double> (0, 1) *
      amp[438] - Complex<double> (0, 1) * amp[446] - Complex<double> (0, 1) *
      amp[452] - Complex<double> (0, 1) * amp[453] + amp[455] + amp[456] +
      Complex<double> (0, 1) * amp[458] + amp[459] + amp[460] + Complex<double>
      (0, 1) * amp[461] + amp[462] - Complex<double> (0, 1) * amp[463] +
      amp[464] + 1./3. * amp[466] + 1./3. * amp[469] + amp[474] -
      Complex<double> (0, 1) * amp[475] + amp[476]);
  jamp[1] = +1./2. * (-amp[316] - amp[317] - 1./3. * amp[318] - 1./3. *
      amp[319] - 1./3. * amp[320] - 1./3. * amp[321] - 1./3. * amp[322] - 1./3.
      * amp[323] - amp[324] - amp[325] - amp[326] - amp[327] - amp[328] - 1./3.
      * amp[329] - 1./3. * amp[330] - 1./3. * amp[331] - amp[332] - amp[333] -
      amp[334] - 1./3. * amp[335] - amp[336] - amp[337] - 1./3. * amp[338] -
      1./3. * amp[339] - amp[340] - amp[341] - 1./3. * amp[342] - amp[343] -
      1./3. * amp[344] - amp[345] - 1./3. * amp[346] - 1./3. * amp[347] -
      amp[348] - amp[349] - 1./3. * amp[350] - 1./3. * amp[351] - amp[352] -
      amp[353] - 1./3. * amp[354] - 1./3. * amp[355] - amp[356] - amp[357] -
      amp[358] - 1./3. * amp[359] - amp[360] - 1./3. * amp[361] - 1./3. *
      amp[362] - 1./3. * amp[363] - amp[412] - amp[413] + Complex<double> (0,
      1) * amp[414] - 1./3. * amp[415] - 1./3. * amp[416] - 1./3. * amp[418] -
      amp[421] - Complex<double> (0, 1) * amp[423] - amp[424] - 1./3. *
      amp[425] + Complex<double> (0, 1) * amp[429] - amp[430] - Complex<double>
      (0, 1) * amp[431] - amp[432] - 1./3. * amp[433] - 1./3. * amp[435] -
      1./3. * amp[436] - 1./3. * amp[437] - amp[439] - Complex<double> (0, 1) *
      amp[441] - amp[442] - Complex<double> (0, 1) * amp[443] - amp[444] -
      1./3. * amp[445] - 1./3. * amp[447] - Complex<double> (0, 1) * amp[448] -
      Complex<double> (0, 1) * amp[449] - amp[450] - 1./3. * amp[454] - 1./3. *
      amp[455] - 1./3. * amp[456] - 1./3. * amp[459] - 1./3. * amp[462] - 1./3.
      * amp[464] - amp[466] + Complex<double> (0, 1) * amp[467] +
      Complex<double> (0, 1) * amp[470] - Complex<double> (0, 1) * amp[472] -
      1./3. * amp[474] - 1./3. * amp[476] - Complex<double> (0, 1) * amp[478]);
  jamp[2] = +1./2. * (-1./3. * amp[364] - 1./3. * amp[365] - amp[366] - 1./3. *
      amp[367] - amp[368] - 1./3. * amp[369] - amp[370] - amp[371] - 1./3. *
      amp[372] - 1./3. * amp[373] - amp[374] - amp[375] - 1./3. * amp[376] -
      1./3. * amp[377] - amp[378] - amp[379] - 1./3. * amp[380] - 1./3. *
      amp[381] - 1./3. * amp[382] - amp[383] - 1./3. * amp[384] - amp[385] -
      amp[386] - amp[387] - 1./3. * amp[388] - amp[389] - amp[390] - 1./3. *
      amp[391] - amp[392] - 1./3. * amp[393] - 1./3. * amp[394] - amp[395] -
      amp[396] - 1./3. * amp[397] - 1./3. * amp[398] - amp[399] - amp[400] -
      amp[401] - 1./3. * amp[402] - 1./3. * amp[403] - 1./3. * amp[404] - 1./3.
      * amp[405] - 1./3. * amp[406] - amp[407] - amp[408] - amp[409] - 1./3. *
      amp[410] - amp[411] - Complex<double> (0, 1) * amp[414] - 1./3. *
      amp[419] - amp[422] + Complex<double> (0, 1) * amp[423] - 1./3. *
      amp[426] - amp[427] - Complex<double> (0, 1) * amp[429] + Complex<double>
      (0, 1) * amp[431] - amp[440] + Complex<double> (0, 1) * amp[441] +
      Complex<double> (0, 1) * amp[443] + Complex<double> (0, 1) * amp[448] +
      Complex<double> (0, 1) * amp[449] - amp[451] - 1./3. * amp[457] - 1./3. *
      amp[460] - amp[465] - Complex<double> (0, 1) * amp[467] - amp[468] -
      amp[469] - Complex<double> (0, 1) * amp[470] - amp[471] + Complex<double>
      (0, 1) * amp[472] - amp[473] - amp[477] + Complex<double> (0, 1) *
      amp[478] - amp[479]);
  jamp[3] = +1./2. * (+1./3. * amp[316] + 1./3. * amp[317] + amp[318] +
      amp[319] + amp[320] + amp[321] + amp[322] + amp[323] + 1./3. * amp[324] +
      1./3. * amp[325] + 1./3. * amp[326] + 1./3. * amp[327] + 1./3. * amp[328]
      + amp[329] + amp[330] + amp[331] + 1./3. * amp[332] + 1./3. * amp[333] +
      1./3. * amp[334] + amp[335] + 1./3. * amp[336] + 1./3. * amp[337] +
      amp[338] + amp[339] + amp[364] + amp[365] + 1./3. * amp[366] + amp[367] +
      1./3. * amp[368] + amp[369] + 1./3. * amp[370] + 1./3. * amp[371] +
      amp[372] + amp[373] + 1./3. * amp[374] + 1./3. * amp[375] + amp[376] +
      amp[377] + 1./3. * amp[378] + 1./3. * amp[379] + amp[380] + amp[381] +
      amp[382] + 1./3. * amp[383] + amp[384] + 1./3. * amp[385] + 1./3. *
      amp[386] + 1./3. * amp[387] + 1./3. * amp[412] + amp[415] +
      Complex<double> (0, 1) * amp[417] + amp[418] + amp[419] - Complex<double>
      (0, 1) * amp[420] + 1./3. * amp[421] + 1./3. * amp[422] + 1./3. *
      amp[424] + amp[425] - Complex<double> (0, 1) * amp[428] + 1./3. *
      amp[430] + 1./3. * amp[432] + amp[433] + Complex<double> (0, 1) *
      amp[434] + amp[435] + amp[436] + Complex<double> (0, 1) * amp[438] +
      1./3. * amp[439] + 1./3. * amp[440] + 1./3. * amp[442] + 1./3. * amp[444]
      + amp[445] + Complex<double> (0, 1) * amp[446] + amp[447] + 1./3. *
      amp[450] + 1./3. * amp[451] + Complex<double> (0, 1) * amp[452] +
      Complex<double> (0, 1) * amp[453] + amp[454] + amp[457] - Complex<double>
      (0, 1) * amp[458] - Complex<double> (0, 1) * amp[461] + Complex<double>
      (0, 1) * amp[463] + 1./3. * amp[465] + 1./3. * amp[468] + 1./3. *
      amp[471] + 1./3. * amp[473] + Complex<double> (0, 1) * amp[475] + 1./3. *
      amp[477] + 1./3. * amp[479]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[2][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P34_sm_gq_wpzqqq::matrix_12_gux_wpzduxux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 164;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[480] + 1./3. * amp[481] + amp[482] +
      amp[483] + 1./3. * amp[484] + 1./3. * amp[485] + amp[486] + 1./3. *
      amp[487] + amp[488] + 1./3. * amp[489] + amp[490] + amp[491] + 1./3. *
      amp[492] + 1./3. * amp[493] + 1./3. * amp[494] + 1./3. * amp[495] +
      amp[496] + 1./3. * amp[497] + amp[498] + amp[499] + amp[500] + 1./3. *
      amp[501] + amp[502] + amp[503] + amp[504] + amp[505] + amp[506] +
      amp[507] + amp[508] + 1./3. * amp[509] + amp[510] + 1./3. * amp[511] +
      1./3. * amp[512] + 1./3. * amp[513] + amp[514] + amp[515] + 1./3. *
      amp[516] + amp[517] + 1./3. * amp[518] + amp[519] + 1./3. * amp[520] +
      amp[521] + 1./3. * amp[522] + 1./3. * amp[523] + 1./3. * amp[524] + 1./3.
      * amp[525] + amp[526] + 1./3. * amp[527] + amp[576] + amp[577] -
      Complex<double> (0, 1) * amp[578] + amp[579] + Complex<double> (0, 1) *
      amp[581] + amp[582] - Complex<double> (0, 1) * amp[584] + amp[585] +
      Complex<double> (0, 1) * amp[586] + amp[587] + 1./3. * amp[588] +
      amp[591] + Complex<double> (0, 1) * amp[593] + 1./3. * amp[594] +
      amp[597] + Complex<double> (0, 1) * amp[598] + amp[599] + Complex<double>
      (0, 1) * amp[622] + Complex<double> (0, 1) * amp[623] + amp[624] +
      amp[628] + 1./3. * amp[629] - Complex<double> (0, 1) * amp[630] -
      Complex<double> (0, 1) * amp[634] + Complex<double> (0, 1) * amp[636] +
      1./3. * amp[639] + Complex<double> (0, 1) * amp[642]);
  jamp[1] = +1./2. * (-1./3. * amp[504] - 1./3. * amp[505] - 1./3. * amp[506] -
      1./3. * amp[507] - 1./3. * amp[508] - amp[509] - 1./3. * amp[510] -
      amp[511] - amp[512] - amp[513] - 1./3. * amp[514] - 1./3. * amp[515] -
      amp[516] - 1./3. * amp[517] - amp[518] - 1./3. * amp[519] - amp[520] -
      1./3. * amp[521] - amp[522] - amp[523] - amp[524] - amp[525] - 1./3. *
      amp[526] - amp[527] - 1./3. * amp[552] - 1./3. * amp[553] - 1./3. *
      amp[554] - 1./3. * amp[555] - amp[556] - 1./3. * amp[557] - amp[558] -
      1./3. * amp[559] - amp[560] - amp[561] - amp[562] - amp[563] - amp[564] -
      amp[565] - amp[566] - amp[567] - 1./3. * amp[568] - 1./3. * amp[569] -
      1./3. * amp[570] - 1./3. * amp[571] - amp[572] - 1./3. * amp[573] -
      amp[574] - 1./3. * amp[575] - 1./3. * amp[577] - 1./3. * amp[583] -
      Complex<double> (0, 1) * amp[590] - amp[595] - Complex<double> (0, 1) *
      amp[596] - Complex<double> (0, 1) * amp[600] - Complex<double> (0, 1) *
      amp[601] - amp[602] - Complex<double> (0, 1) * amp[604] - Complex<double>
      (0, 1) * amp[607] - amp[609] - Complex<double> (0, 1) * amp[610] -
      amp[611] - amp[612] - Complex<double> (0, 1) * amp[613] - amp[614] -
      Complex<double> (0, 1) * amp[616] - Complex<double> (0, 1) * amp[619] -
      amp[620] - amp[621] - 1./3. * amp[628] - amp[629] - Complex<double> (0,
      1) * amp[631] - 1./3. * amp[633] - amp[638] - amp[639] - Complex<double>
      (0, 1) * amp[640]);
  jamp[2] = +1./2. * (-amp[480] - amp[481] - 1./3. * amp[482] - 1./3. *
      amp[483] - amp[484] - amp[485] - 1./3. * amp[486] - amp[487] - 1./3. *
      amp[488] - amp[489] - 1./3. * amp[490] - 1./3. * amp[491] - amp[492] -
      amp[493] - amp[494] - amp[495] - 1./3. * amp[496] - amp[497] - 1./3. *
      amp[498] - 1./3. * amp[499] - 1./3. * amp[500] - amp[501] - 1./3. *
      amp[502] - 1./3. * amp[503] - 1./3. * amp[528] - 1./3. * amp[529] - 1./3.
      * amp[530] - 1./3. * amp[531] - amp[532] - amp[533] - 1./3. * amp[534] -
      1./3. * amp[535] - amp[536] - amp[537] - amp[538] - amp[539] - amp[540] -
      amp[541] - amp[542] - amp[543] - 1./3. * amp[544] - 1./3. * amp[545] -
      amp[546] - amp[547] - 1./3. * amp[548] - 1./3. * amp[549] - 1./3. *
      amp[550] - 1./3. * amp[551] - 1./3. * amp[576] - 1./3. * amp[579] - 1./3.
      * amp[580] - 1./3. * amp[582] - 1./3. * amp[585] - 1./3. * amp[587] -
      amp[588] - amp[589] + Complex<double> (0, 1) * amp[590] - 1./3. *
      amp[591] - 1./3. * amp[592] - amp[594] + Complex<double> (0, 1) *
      amp[596] - 1./3. * amp[597] - 1./3. * amp[599] + Complex<double> (0, 1) *
      amp[600] + Complex<double> (0, 1) * amp[601] - amp[603] + Complex<double>
      (0, 1) * amp[604] - amp[605] - amp[606] + Complex<double> (0, 1) *
      amp[607] - amp[608] + Complex<double> (0, 1) * amp[610] + Complex<double>
      (0, 1) * amp[613] - amp[615] + Complex<double> (0, 1) * amp[616] -
      amp[617] - amp[618] + Complex<double> (0, 1) * amp[619] - 1./3. *
      amp[624] - 1./3. * amp[625] - amp[626] - 1./3. * amp[627] +
      Complex<double> (0, 1) * amp[631] - 1./3. * amp[632] - 1./3. * amp[635] -
      1./3. * amp[637] + Complex<double> (0, 1) * amp[640] - 1./3. * amp[641] -
      1./3. * amp[643]);
  jamp[3] = +1./2. * (+amp[528] + amp[529] + amp[530] + amp[531] + 1./3. *
      amp[532] + 1./3. * amp[533] + amp[534] + amp[535] + 1./3. * amp[536] +
      1./3. * amp[537] + 1./3. * amp[538] + 1./3. * amp[539] + 1./3. * amp[540]
      + 1./3. * amp[541] + 1./3. * amp[542] + 1./3. * amp[543] + amp[544] +
      amp[545] + 1./3. * amp[546] + 1./3. * amp[547] + amp[548] + amp[549] +
      amp[550] + amp[551] + amp[552] + amp[553] + amp[554] + amp[555] + 1./3. *
      amp[556] + amp[557] + 1./3. * amp[558] + amp[559] + 1./3. * amp[560] +
      1./3. * amp[561] + 1./3. * amp[562] + 1./3. * amp[563] + 1./3. * amp[564]
      + 1./3. * amp[565] + 1./3. * amp[566] + 1./3. * amp[567] + amp[568] +
      amp[569] + amp[570] + amp[571] + 1./3. * amp[572] + amp[573] + 1./3. *
      amp[574] + amp[575] + Complex<double> (0, 1) * amp[578] + amp[580] -
      Complex<double> (0, 1) * amp[581] + amp[583] + Complex<double> (0, 1) *
      amp[584] - Complex<double> (0, 1) * amp[586] + 1./3. * amp[589] +
      amp[592] - Complex<double> (0, 1) * amp[593] + 1./3. * amp[595] -
      Complex<double> (0, 1) * amp[598] + 1./3. * amp[602] + 1./3. * amp[603] +
      1./3. * amp[605] + 1./3. * amp[606] + 1./3. * amp[608] + 1./3. * amp[609]
      + 1./3. * amp[611] + 1./3. * amp[612] + 1./3. * amp[614] + 1./3. *
      amp[615] + 1./3. * amp[617] + 1./3. * amp[618] + 1./3. * amp[620] + 1./3.
      * amp[621] - Complex<double> (0, 1) * amp[622] - Complex<double> (0, 1) *
      amp[623] + amp[625] + 1./3. * amp[626] + amp[627] + Complex<double> (0,
      1) * amp[630] + amp[632] + amp[633] + Complex<double> (0, 1) * amp[634] +
      amp[635] - Complex<double> (0, 1) * amp[636] + amp[637] + 1./3. *
      amp[638] + amp[641] - Complex<double> (0, 1) * amp[642] + amp[643]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[3][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P34_sm_gq_wpzqqq::matrix_12_gdx_wpzuuxux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 164;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[644] + amp[645] + 1./3. * amp[646] + 1./3. *
      amp[647] + amp[648] + amp[649] + 1./3. * amp[650] + amp[651] + 1./3. *
      amp[652] + amp[653] + 1./3. * amp[654] + 1./3. * amp[655] + amp[656] +
      amp[657] + amp[658] + amp[659] + 1./3. * amp[660] + amp[661] + 1./3. *
      amp[662] + 1./3. * amp[663] + 1./3. * amp[664] + amp[665] + 1./3. *
      amp[666] + 1./3. * amp[667] + 1./3. * amp[692] + 1./3. * amp[693] + 1./3.
      * amp[694] + 1./3. * amp[695] + amp[696] + amp[697] + 1./3. * amp[698] +
      1./3. * amp[699] + amp[700] + amp[701] + amp[702] + amp[703] + amp[704] +
      amp[705] + amp[706] + amp[707] + 1./3. * amp[708] + 1./3. * amp[709] +
      amp[710] + amp[711] + 1./3. * amp[712] + 1./3. * amp[713] + 1./3. *
      amp[714] + 1./3. * amp[715] + 1./3. * amp[740] + 1./3. * amp[743] + 1./3.
      * amp[744] + 1./3. * amp[746] + 1./3. * amp[749] + 1./3. * amp[751] +
      amp[752] + amp[753] - Complex<double> (0, 1) * amp[754] + 1./3. *
      amp[755] + 1./3. * amp[756] + amp[758] - Complex<double> (0, 1) *
      amp[760] + 1./3. * amp[761] + 1./3. * amp[763] - Complex<double> (0, 1) *
      amp[764] - Complex<double> (0, 1) * amp[765] + amp[767] - Complex<double>
      (0, 1) * amp[768] + amp[769] + amp[770] - Complex<double> (0, 1) *
      amp[771] + amp[772] - Complex<double> (0, 1) * amp[774] - Complex<double>
      (0, 1) * amp[777] + amp[779] - Complex<double> (0, 1) * amp[780] +
      amp[781] + amp[782] - Complex<double> (0, 1) * amp[783] + 1./3. *
      amp[788] + 1./3. * amp[789] + amp[790] + 1./3. * amp[791] -
      Complex<double> (0, 1) * amp[795] + 1./3. * amp[796] + 1./3. * amp[799] +
      1./3. * amp[801] - Complex<double> (0, 1) * amp[804] + 1./3. * amp[805] +
      1./3. * amp[807]);
  jamp[1] = +1./2. * (-amp[692] - amp[693] - amp[694] - amp[695] - 1./3. *
      amp[696] - 1./3. * amp[697] - amp[698] - amp[699] - 1./3. * amp[700] -
      1./3. * amp[701] - 1./3. * amp[702] - 1./3. * amp[703] - 1./3. * amp[704]
      - 1./3. * amp[705] - 1./3. * amp[706] - 1./3. * amp[707] - amp[708] -
      amp[709] - 1./3. * amp[710] - 1./3. * amp[711] - amp[712] - amp[713] -
      amp[714] - amp[715] - amp[716] - amp[717] - amp[718] - amp[719] - 1./3. *
      amp[720] - amp[721] - 1./3. * amp[722] - amp[723] - 1./3. * amp[724] -
      1./3. * amp[725] - 1./3. * amp[726] - 1./3. * amp[727] - 1./3. * amp[728]
      - 1./3. * amp[729] - 1./3. * amp[730] - 1./3. * amp[731] - amp[732] -
      amp[733] - amp[734] - amp[735] - 1./3. * amp[736] - amp[737] - 1./3. *
      amp[738] - amp[739] - Complex<double> (0, 1) * amp[742] - amp[744] +
      Complex<double> (0, 1) * amp[745] - amp[747] - Complex<double> (0, 1) *
      amp[748] + Complex<double> (0, 1) * amp[750] - 1./3. * amp[753] -
      amp[756] + Complex<double> (0, 1) * amp[757] - 1./3. * amp[759] +
      Complex<double> (0, 1) * amp[762] - 1./3. * amp[766] - 1./3. * amp[767] -
      1./3. * amp[769] - 1./3. * amp[770] - 1./3. * amp[772] - 1./3. * amp[773]
      - 1./3. * amp[775] - 1./3. * amp[776] - 1./3. * amp[778] - 1./3. *
      amp[779] - 1./3. * amp[781] - 1./3. * amp[782] - 1./3. * amp[784] - 1./3.
      * amp[785] + Complex<double> (0, 1) * amp[786] + Complex<double> (0, 1) *
      amp[787] - amp[789] - 1./3. * amp[790] - amp[791] - Complex<double> (0,
      1) * amp[794] - amp[796] - amp[797] - Complex<double> (0, 1) * amp[798] -
      amp[799] + Complex<double> (0, 1) * amp[800] - amp[801] - 1./3. *
      amp[802] - amp[805] + Complex<double> (0, 1) * amp[806] - amp[807]);
  jamp[2] = +1./2. * (-1./3. * amp[644] - 1./3. * amp[645] - amp[646] -
      amp[647] - 1./3. * amp[648] - 1./3. * amp[649] - amp[650] - 1./3. *
      amp[651] - amp[652] - 1./3. * amp[653] - amp[654] - amp[655] - 1./3. *
      amp[656] - 1./3. * amp[657] - 1./3. * amp[658] - 1./3. * amp[659] -
      amp[660] - 1./3. * amp[661] - amp[662] - amp[663] - amp[664] - 1./3. *
      amp[665] - amp[666] - amp[667] - amp[668] - amp[669] - amp[670] -
      amp[671] - amp[672] - 1./3. * amp[673] - amp[674] - 1./3. * amp[675] -
      1./3. * amp[676] - 1./3. * amp[677] - amp[678] - amp[679] - 1./3. *
      amp[680] - amp[681] - 1./3. * amp[682] - amp[683] - 1./3. * amp[684] -
      amp[685] - 1./3. * amp[686] - 1./3. * amp[687] - 1./3. * amp[688] - 1./3.
      * amp[689] - amp[690] - 1./3. * amp[691] - amp[740] - amp[741] +
      Complex<double> (0, 1) * amp[742] - amp[743] - Complex<double> (0, 1) *
      amp[745] - amp[746] + Complex<double> (0, 1) * amp[748] - amp[749] -
      Complex<double> (0, 1) * amp[750] - amp[751] - 1./3. * amp[752] -
      amp[755] - Complex<double> (0, 1) * amp[757] - 1./3. * amp[758] -
      amp[761] - Complex<double> (0, 1) * amp[762] - amp[763] - Complex<double>
      (0, 1) * amp[786] - Complex<double> (0, 1) * amp[787] - amp[788] -
      amp[792] - 1./3. * amp[793] + Complex<double> (0, 1) * amp[794] +
      Complex<double> (0, 1) * amp[798] - Complex<double> (0, 1) * amp[800] -
      1./3. * amp[803] - Complex<double> (0, 1) * amp[806]);
  jamp[3] = +1./2. * (+1./3. * amp[668] + 1./3. * amp[669] + 1./3. * amp[670] +
      1./3. * amp[671] + 1./3. * amp[672] + amp[673] + 1./3. * amp[674] +
      amp[675] + amp[676] + amp[677] + 1./3. * amp[678] + 1./3. * amp[679] +
      amp[680] + 1./3. * amp[681] + amp[682] + 1./3. * amp[683] + amp[684] +
      1./3. * amp[685] + amp[686] + amp[687] + amp[688] + amp[689] + 1./3. *
      amp[690] + amp[691] + 1./3. * amp[716] + 1./3. * amp[717] + 1./3. *
      amp[718] + 1./3. * amp[719] + amp[720] + 1./3. * amp[721] + amp[722] +
      1./3. * amp[723] + amp[724] + amp[725] + amp[726] + amp[727] + amp[728] +
      amp[729] + amp[730] + amp[731] + 1./3. * amp[732] + 1./3. * amp[733] +
      1./3. * amp[734] + 1./3. * amp[735] + amp[736] + 1./3. * amp[737] +
      amp[738] + 1./3. * amp[739] + 1./3. * amp[741] + 1./3. * amp[747] +
      Complex<double> (0, 1) * amp[754] + amp[759] + Complex<double> (0, 1) *
      amp[760] + Complex<double> (0, 1) * amp[764] + Complex<double> (0, 1) *
      amp[765] + amp[766] + Complex<double> (0, 1) * amp[768] + Complex<double>
      (0, 1) * amp[771] + amp[773] + Complex<double> (0, 1) * amp[774] +
      amp[775] + amp[776] + Complex<double> (0, 1) * amp[777] + amp[778] +
      Complex<double> (0, 1) * amp[780] + Complex<double> (0, 1) * amp[783] +
      amp[784] + amp[785] + 1./3. * amp[792] + amp[793] + Complex<double> (0,
      1) * amp[795] + 1./3. * amp[797] + amp[802] + amp[803] + Complex<double>
      (0, 1) * amp[804]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[4][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P34_sm_gq_wpzqqq::matrix_12_gdx_wpzduxdx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 164;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[808] + amp[809] + 1./3. * amp[810] + 1./3. *
      amp[811] + 1./3. * amp[812] + 1./3. * amp[813] + 1./3. * amp[814] + 1./3.
      * amp[815] + amp[816] + amp[817] + amp[818] + amp[819] + amp[820] + 1./3.
      * amp[821] + 1./3. * amp[822] + 1./3. * amp[823] + amp[824] + amp[825] +
      amp[826] + 1./3. * amp[827] + amp[828] + amp[829] + 1./3. * amp[830] +
      1./3. * amp[831] + amp[832] + amp[833] + 1./3. * amp[834] + amp[835] +
      1./3. * amp[836] + amp[837] + 1./3. * amp[838] + 1./3. * amp[839] +
      amp[840] + amp[841] + 1./3. * amp[842] + 1./3. * amp[843] + amp[844] +
      amp[845] + 1./3. * amp[846] + 1./3. * amp[847] + amp[848] + amp[849] +
      amp[850] + 1./3. * amp[851] + amp[852] + 1./3. * amp[853] + 1./3. *
      amp[854] + 1./3. * amp[855] + amp[904] + amp[905] - Complex<double> (0,
      1) * amp[906] + 1./3. * amp[907] + 1./3. * amp[908] + 1./3. * amp[910] +
      amp[913] + Complex<double> (0, 1) * amp[915] + amp[916] + 1./3. *
      amp[917] - Complex<double> (0, 1) * amp[921] + amp[922] + Complex<double>
      (0, 1) * amp[923] + amp[924] + 1./3. * amp[925] + 1./3. * amp[927] +
      1./3. * amp[928] + 1./3. * amp[929] + amp[931] + Complex<double> (0, 1) *
      amp[933] + amp[934] + Complex<double> (0, 1) * amp[935] + amp[936] +
      1./3. * amp[937] + 1./3. * amp[939] + Complex<double> (0, 1) * amp[940] +
      Complex<double> (0, 1) * amp[941] + amp[942] + 1./3. * amp[946] + 1./3. *
      amp[947] + 1./3. * amp[948] + 1./3. * amp[951] + 1./3. * amp[954] + 1./3.
      * amp[956] + amp[958] - Complex<double> (0, 1) * amp[959] -
      Complex<double> (0, 1) * amp[962] + Complex<double> (0, 1) * amp[964] +
      1./3. * amp[966] + 1./3. * amp[968] + Complex<double> (0, 1) * amp[970]);
  jamp[1] = +1./2. * (-1./3. * amp[832] - 1./3. * amp[833] - amp[834] - 1./3. *
      amp[835] - amp[836] - 1./3. * amp[837] - amp[838] - amp[839] - 1./3. *
      amp[840] - 1./3. * amp[841] - amp[842] - amp[843] - 1./3. * amp[844] -
      1./3. * amp[845] - amp[846] - amp[847] - 1./3. * amp[848] - 1./3. *
      amp[849] - 1./3. * amp[850] - amp[851] - 1./3. * amp[852] - amp[853] -
      amp[854] - amp[855] - amp[880] - 1./3. * amp[881] - 1./3. * amp[882] -
      amp[883] - 1./3. * amp[884] - amp[885] - amp[886] - 1./3. * amp[887] -
      1./3. * amp[888] - amp[889] - amp[890] - 1./3. * amp[891] - 1./3. *
      amp[892] - 1./3. * amp[893] - amp[894] - amp[895] - amp[896] - amp[897] -
      amp[898] - 1./3. * amp[899] - 1./3. * amp[900] - 1./3. * amp[901] -
      amp[902] - 1./3. * amp[903] - 1./3. * amp[905] - amp[908] +
      Complex<double> (0, 1) * amp[909] - Complex<double> (0, 1) * amp[912] -
      amp[918] - 1./3. * amp[919] - Complex<double> (0, 1) * amp[920] +
      Complex<double> (0, 1) * amp[926] - amp[929] + Complex<double> (0, 1) *
      amp[930] + Complex<double> (0, 1) * amp[938] + Complex<double> (0, 1) *
      amp[944] + Complex<double> (0, 1) * amp[945] - amp[947] - amp[948] -
      Complex<double> (0, 1) * amp[950] - amp[951] - amp[952] - Complex<double>
      (0, 1) * amp[953] - amp[954] + Complex<double> (0, 1) * amp[955] -
      amp[956] - 1./3. * amp[958] - 1./3. * amp[961] - amp[966] +
      Complex<double> (0, 1) * amp[967] - amp[968]);
  jamp[2] = +1./2. * (-1./3. * amp[808] - 1./3. * amp[809] - amp[810] -
      amp[811] - amp[812] - amp[813] - amp[814] - amp[815] - 1./3. * amp[816] -
      1./3. * amp[817] - 1./3. * amp[818] - 1./3. * amp[819] - 1./3. * amp[820]
      - amp[821] - amp[822] - amp[823] - 1./3. * amp[824] - 1./3. * amp[825] -
      1./3. * amp[826] - amp[827] - 1./3. * amp[828] - 1./3. * amp[829] -
      amp[830] - amp[831] - amp[856] - amp[857] - 1./3. * amp[858] - amp[859] -
      1./3. * amp[860] - amp[861] - 1./3. * amp[862] - 1./3. * amp[863] -
      amp[864] - amp[865] - 1./3. * amp[866] - 1./3. * amp[867] - amp[868] -
      amp[869] - 1./3. * amp[870] - 1./3. * amp[871] - amp[872] - amp[873] -
      amp[874] - 1./3. * amp[875] - amp[876] - 1./3. * amp[877] - 1./3. *
      amp[878] - 1./3. * amp[879] - 1./3. * amp[904] - amp[907] -
      Complex<double> (0, 1) * amp[909] - amp[910] - amp[911] + Complex<double>
      (0, 1) * amp[912] - 1./3. * amp[913] - 1./3. * amp[914] - 1./3. *
      amp[916] - amp[917] + Complex<double> (0, 1) * amp[920] - 1./3. *
      amp[922] - 1./3. * amp[924] - amp[925] - Complex<double> (0, 1) *
      amp[926] - amp[927] - amp[928] - Complex<double> (0, 1) * amp[930] -
      1./3. * amp[931] - 1./3. * amp[932] - 1./3. * amp[934] - 1./3. * amp[936]
      - amp[937] - Complex<double> (0, 1) * amp[938] - amp[939] - 1./3. *
      amp[942] - 1./3. * amp[943] - Complex<double> (0, 1) * amp[944] -
      Complex<double> (0, 1) * amp[945] - amp[946] - amp[949] + Complex<double>
      (0, 1) * amp[950] + Complex<double> (0, 1) * amp[953] - Complex<double>
      (0, 1) * amp[955] - 1./3. * amp[957] - 1./3. * amp[960] - 1./3. *
      amp[963] - 1./3. * amp[965] - Complex<double> (0, 1) * amp[967] - 1./3. *
      amp[969] - 1./3. * amp[971]);
  jamp[3] = +1./2. * (+1./3. * amp[856] + 1./3. * amp[857] + amp[858] + 1./3. *
      amp[859] + amp[860] + 1./3. * amp[861] + amp[862] + amp[863] + 1./3. *
      amp[864] + 1./3. * amp[865] + amp[866] + amp[867] + 1./3. * amp[868] +
      1./3. * amp[869] + amp[870] + amp[871] + 1./3. * amp[872] + 1./3. *
      amp[873] + 1./3. * amp[874] + amp[875] + 1./3. * amp[876] + amp[877] +
      amp[878] + amp[879] + 1./3. * amp[880] + amp[881] + amp[882] + 1./3. *
      amp[883] + amp[884] + 1./3. * amp[885] + 1./3. * amp[886] + amp[887] +
      amp[888] + 1./3. * amp[889] + 1./3. * amp[890] + amp[891] + amp[892] +
      amp[893] + 1./3. * amp[894] + 1./3. * amp[895] + 1./3. * amp[896] + 1./3.
      * amp[897] + 1./3. * amp[898] + amp[899] + amp[900] + amp[901] + 1./3. *
      amp[902] + amp[903] + Complex<double> (0, 1) * amp[906] + 1./3. *
      amp[911] + amp[914] - Complex<double> (0, 1) * amp[915] + 1./3. *
      amp[918] + amp[919] + Complex<double> (0, 1) * amp[921] - Complex<double>
      (0, 1) * amp[923] + amp[932] - Complex<double> (0, 1) * amp[933] -
      Complex<double> (0, 1) * amp[935] - Complex<double> (0, 1) * amp[940] -
      Complex<double> (0, 1) * amp[941] + amp[943] + 1./3. * amp[949] + 1./3. *
      amp[952] + amp[957] + Complex<double> (0, 1) * amp[959] + amp[960] +
      amp[961] + Complex<double> (0, 1) * amp[962] + amp[963] - Complex<double>
      (0, 1) * amp[964] + amp[965] + amp[969] - Complex<double> (0, 1) *
      amp[970] + amp[971]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[5][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P34_sm_gq_wpzqqq::matrix_12_gu_wpzuscx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 82;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[0] + 1./3. * amp[1] + 1./3. * amp[972] +
      1./3. * amp[973] + 1./3. * amp[974] + 1./3. * amp[975] + 1./3. * amp[976]
      + 1./3. * amp[977] + 1./3. * amp[978] + 1./3. * amp[979] + 1./3. *
      amp[980] + 1./3. * amp[981] + 1./3. * amp[982] + 1./3. * amp[983] + 1./3.
      * amp[984] + 1./3. * amp[985] + 1./3. * amp[986] + 1./3. * amp[987] +
      1./3. * amp[988] + 1./3. * amp[989] + 1./3. * amp[990] + 1./3. * amp[991]
      + 1./3. * amp[992] + 1./3. * amp[993] + 1./3. * amp[1018] + 1./3. *
      amp[1021] + 1./3. * amp[1047] + 1./3. * amp[1050]);
  jamp[1] = +1./2. * (-amp[982] - amp[983] - amp[984] - amp[985] - amp[986] -
      amp[987] - amp[988] - amp[989] - amp[990] - amp[991] - amp[992] -
      amp[993] - amp[1006] - amp[1007] - amp[1008] - amp[1009] - amp[1010] -
      amp[1011] - amp[1012] - amp[1013] - amp[1014] - amp[1015] - amp[1016] -
      amp[1017] - Complex<double> (0, 1) * amp[1020] - amp[1022] -
      Complex<double> (0, 1) * amp[1023] - Complex<double> (0, 1) * amp[1024] -
      Complex<double> (0, 1) * amp[1025] - amp[1026] - Complex<double> (0, 1) *
      amp[1028] - Complex<double> (0, 1) * amp[1031] - amp[1033] -
      Complex<double> (0, 1) * amp[1034] - amp[1035] - amp[1036] -
      Complex<double> (0, 1) * amp[1037] - amp[1038] - Complex<double> (0, 1) *
      amp[1040] - Complex<double> (0, 1) * amp[1043] - amp[1044] - amp[1045] -
      amp[1047] - Complex<double> (0, 1) * amp[1048] - amp[1049] - amp[1050] -
      Complex<double> (0, 1) * amp[1051]);
  jamp[2] = +1./2. * (-amp[0] - amp[1] - amp[972] - amp[973] - amp[974] -
      amp[975] - amp[976] - amp[977] - amp[978] - amp[979] - amp[980] -
      amp[981] - amp[994] - amp[995] - amp[996] - amp[997] - amp[998] -
      amp[999] - amp[1000] - amp[1001] - amp[1002] - amp[1003] - amp[1004] -
      amp[1005] - amp[1018] - amp[1019] + Complex<double> (0, 1) * amp[1020] -
      amp[1021] + Complex<double> (0, 1) * amp[1023] + Complex<double> (0, 1) *
      amp[1024] + Complex<double> (0, 1) * amp[1025] - amp[1027] +
      Complex<double> (0, 1) * amp[1028] - amp[1029] - amp[1030] +
      Complex<double> (0, 1) * amp[1031] - amp[1032] + Complex<double> (0, 1) *
      amp[1034] + Complex<double> (0, 1) * amp[1037] - amp[1039] +
      Complex<double> (0, 1) * amp[1040] - amp[1041] - amp[1042] +
      Complex<double> (0, 1) * amp[1043] - amp[1046] + Complex<double> (0, 1) *
      amp[1048] + Complex<double> (0, 1) * amp[1051]);
  jamp[3] = +1./2. * (+1./3. * amp[994] + 1./3. * amp[995] + 1./3. * amp[996] +
      1./3. * amp[997] + 1./3. * amp[998] + 1./3. * amp[999] + 1./3. *
      amp[1000] + 1./3. * amp[1001] + 1./3. * amp[1002] + 1./3. * amp[1003] +
      1./3. * amp[1004] + 1./3. * amp[1005] + 1./3. * amp[1006] + 1./3. *
      amp[1007] + 1./3. * amp[1008] + 1./3. * amp[1009] + 1./3. * amp[1010] +
      1./3. * amp[1011] + 1./3. * amp[1012] + 1./3. * amp[1013] + 1./3. *
      amp[1014] + 1./3. * amp[1015] + 1./3. * amp[1016] + 1./3. * amp[1017] +
      1./3. * amp[1019] + 1./3. * amp[1022] + 1./3. * amp[1026] + 1./3. *
      amp[1027] + 1./3. * amp[1029] + 1./3. * amp[1030] + 1./3. * amp[1032] +
      1./3. * amp[1033] + 1./3. * amp[1035] + 1./3. * amp[1036] + 1./3. *
      amp[1038] + 1./3. * amp[1039] + 1./3. * amp[1041] + 1./3. * amp[1042] +
      1./3. * amp[1044] + 1./3. * amp[1045] + 1./3. * amp[1046] + 1./3. *
      amp[1049]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[6][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P34_sm_gq_wpzqqq::matrix_12_gu_wpzcdcx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 82;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[1052] + amp[1053] + amp[1054] + amp[1055] +
      amp[1056] + amp[1057] + amp[1058] + amp[1059] + amp[1060] + amp[1061] +
      amp[1062] + amp[1063] + amp[1064] + amp[1065] + amp[1066] + amp[1067] +
      amp[1068] + amp[1069] + amp[1070] + amp[1071] + amp[1072] + amp[1073] +
      amp[1074] + amp[1075] + amp[1100] + amp[1101] - Complex<double> (0, 1) *
      amp[1102] + amp[1103] + Complex<double> (0, 1) * amp[1105] + amp[1106] -
      Complex<double> (0, 1) * amp[1108] + amp[1109] + Complex<double> (0, 1) *
      amp[1110] + amp[1111] + amp[1112] + Complex<double> (0, 1) * amp[1114] +
      amp[1115] + Complex<double> (0, 1) * amp[1116] + amp[1117] +
      Complex<double> (0, 1) * amp[1118] + Complex<double> (0, 1) * amp[1119] +
      amp[1120] + amp[1123] - Complex<double> (0, 1) * amp[1124] -
      Complex<double> (0, 1) * amp[1127] + Complex<double> (0, 1) * amp[1129] +
      Complex<double> (0, 1) * amp[1132]);
  jamp[1] = +1./2. * (-1./3. * amp[1064] - 1./3. * amp[1065] - 1./3. *
      amp[1066] - 1./3. * amp[1067] - 1./3. * amp[1068] - 1./3. * amp[1069] -
      1./3. * amp[1070] - 1./3. * amp[1071] - 1./3. * amp[1072] - 1./3. *
      amp[1073] - 1./3. * amp[1074] - 1./3. * amp[1075] - 1./3. * amp[1088] -
      1./3. * amp[1089] - 1./3. * amp[1090] - 1./3. * amp[1091] - 1./3. *
      amp[1092] - 1./3. * amp[1093] - 1./3. * amp[1094] - 1./3. * amp[1095] -
      1./3. * amp[1096] - 1./3. * amp[1097] - 1./3. * amp[1098] - 1./3. *
      amp[1099] - 1./3. * amp[1101] - 1./3. * amp[1107] - 1./3. * amp[1123] -
      1./3. * amp[1126]);
  jamp[2] = +1./2. * (-1./3. * amp[1052] - 1./3. * amp[1053] - 1./3. *
      amp[1054] - 1./3. * amp[1055] - 1./3. * amp[1056] - 1./3. * amp[1057] -
      1./3. * amp[1058] - 1./3. * amp[1059] - 1./3. * amp[1060] - 1./3. *
      amp[1061] - 1./3. * amp[1062] - 1./3. * amp[1063] - 1./3. * amp[1076] -
      1./3. * amp[1077] - 1./3. * amp[1078] - 1./3. * amp[1079] - 1./3. *
      amp[1080] - 1./3. * amp[1081] - 1./3. * amp[1082] - 1./3. * amp[1083] -
      1./3. * amp[1084] - 1./3. * amp[1085] - 1./3. * amp[1086] - 1./3. *
      amp[1087] - 1./3. * amp[1100] - 1./3. * amp[1103] - 1./3. * amp[1104] -
      1./3. * amp[1106] - 1./3. * amp[1109] - 1./3. * amp[1111] - 1./3. *
      amp[1112] - 1./3. * amp[1113] - 1./3. * amp[1115] - 1./3. * amp[1117] -
      1./3. * amp[1120] - 1./3. * amp[1121] - 1./3. * amp[1122] - 1./3. *
      amp[1125] - 1./3. * amp[1128] - 1./3. * amp[1130] - 1./3. * amp[1131] -
      1./3. * amp[1133]);
  jamp[3] = +1./2. * (+amp[1076] + amp[1077] + amp[1078] + amp[1079] +
      amp[1080] + amp[1081] + amp[1082] + amp[1083] + amp[1084] + amp[1085] +
      amp[1086] + amp[1087] + amp[1088] + amp[1089] + amp[1090] + amp[1091] +
      amp[1092] + amp[1093] + amp[1094] + amp[1095] + amp[1096] + amp[1097] +
      amp[1098] + amp[1099] + Complex<double> (0, 1) * amp[1102] + amp[1104] -
      Complex<double> (0, 1) * amp[1105] + amp[1107] + Complex<double> (0, 1) *
      amp[1108] - Complex<double> (0, 1) * amp[1110] + amp[1113] -
      Complex<double> (0, 1) * amp[1114] - Complex<double> (0, 1) * amp[1116] -
      Complex<double> (0, 1) * amp[1118] - Complex<double> (0, 1) * amp[1119] +
      amp[1121] + amp[1122] + Complex<double> (0, 1) * amp[1124] + amp[1125] +
      amp[1126] + Complex<double> (0, 1) * amp[1127] + amp[1128] -
      Complex<double> (0, 1) * amp[1129] + amp[1130] + amp[1131] -
      Complex<double> (0, 1) * amp[1132] + amp[1133]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[7][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P34_sm_gq_wpzqqq::matrix_12_gu_wpzdssx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 82;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[1134] + 1./3. * amp[1135] + 1./3. *
      amp[1136] + 1./3. * amp[1137] + 1./3. * amp[1138] + 1./3. * amp[1139] +
      1./3. * amp[1140] + 1./3. * amp[1141] + 1./3. * amp[1142] + 1./3. *
      amp[1143] + 1./3. * amp[1144] + 1./3. * amp[1145] + 1./3. * amp[1146] +
      1./3. * amp[1147] + 1./3. * amp[1148] + 1./3. * amp[1149] + 1./3. *
      amp[1150] + 1./3. * amp[1151] + 1./3. * amp[1152] + 1./3. * amp[1153] +
      1./3. * amp[1154] + 1./3. * amp[1155] + 1./3. * amp[1156] + 1./3. *
      amp[1157] + 1./3. * amp[1182] + 1./3. * amp[1183] + 1./3. * amp[1185] +
      1./3. * amp[1188] + 1./3. * amp[1191] + 1./3. * amp[1193] + 1./3. *
      amp[1194] + 1./3. * amp[1195] + 1./3. * amp[1197] + 1./3. * amp[1199] +
      1./3. * amp[1202] + 1./3. * amp[1203] + 1./3. * amp[1204] + 1./3. *
      amp[1207] + 1./3. * amp[1210] + 1./3. * amp[1212] + 1./3. * amp[1213] +
      1./3. * amp[1215]);
  jamp[1] = +1./2. * (-amp[1146] - amp[1147] - amp[1148] - amp[1149] -
      amp[1150] - amp[1151] - amp[1152] - amp[1153] - amp[1154] - amp[1155] -
      amp[1156] - amp[1157] - amp[1170] - amp[1171] - amp[1172] - amp[1173] -
      amp[1174] - amp[1175] - amp[1176] - amp[1177] - amp[1178] - amp[1179] -
      amp[1180] - amp[1181] - amp[1183] + Complex<double> (0, 1) * amp[1184] -
      Complex<double> (0, 1) * amp[1187] - amp[1189] - Complex<double> (0, 1) *
      amp[1190] + Complex<double> (0, 1) * amp[1192] - amp[1195] +
      Complex<double> (0, 1) * amp[1196] + Complex<double> (0, 1) * amp[1198] +
      Complex<double> (0, 1) * amp[1200] + Complex<double> (0, 1) * amp[1201] -
      amp[1203] - amp[1204] - Complex<double> (0, 1) * amp[1206] - amp[1207] -
      amp[1208] - Complex<double> (0, 1) * amp[1209] - amp[1210] +
      Complex<double> (0, 1) * amp[1211] - amp[1212] - amp[1213] +
      Complex<double> (0, 1) * amp[1214] - amp[1215]);
  jamp[2] = +1./2. * (-amp[1134] - amp[1135] - amp[1136] - amp[1137] -
      amp[1138] - amp[1139] - amp[1140] - amp[1141] - amp[1142] - amp[1143] -
      amp[1144] - amp[1145] - amp[1158] - amp[1159] - amp[1160] - amp[1161] -
      amp[1162] - amp[1163] - amp[1164] - amp[1165] - amp[1166] - amp[1167] -
      amp[1168] - amp[1169] - amp[1182] - Complex<double> (0, 1) * amp[1184] -
      amp[1185] - amp[1186] + Complex<double> (0, 1) * amp[1187] - amp[1188] +
      Complex<double> (0, 1) * amp[1190] - amp[1191] - Complex<double> (0, 1) *
      amp[1192] - amp[1193] - amp[1194] - Complex<double> (0, 1) * amp[1196] -
      amp[1197] - Complex<double> (0, 1) * amp[1198] - amp[1199] -
      Complex<double> (0, 1) * amp[1200] - Complex<double> (0, 1) * amp[1201] -
      amp[1202] - amp[1205] + Complex<double> (0, 1) * amp[1206] +
      Complex<double> (0, 1) * amp[1209] - Complex<double> (0, 1) * amp[1211] -
      Complex<double> (0, 1) * amp[1214]);
  jamp[3] = +1./2. * (+1./3. * amp[1158] + 1./3. * amp[1159] + 1./3. *
      amp[1160] + 1./3. * amp[1161] + 1./3. * amp[1162] + 1./3. * amp[1163] +
      1./3. * amp[1164] + 1./3. * amp[1165] + 1./3. * amp[1166] + 1./3. *
      amp[1167] + 1./3. * amp[1168] + 1./3. * amp[1169] + 1./3. * amp[1170] +
      1./3. * amp[1171] + 1./3. * amp[1172] + 1./3. * amp[1173] + 1./3. *
      amp[1174] + 1./3. * amp[1175] + 1./3. * amp[1176] + 1./3. * amp[1177] +
      1./3. * amp[1178] + 1./3. * amp[1179] + 1./3. * amp[1180] + 1./3. *
      amp[1181] + 1./3. * amp[1186] + 1./3. * amp[1189] + 1./3. * amp[1205] +
      1./3. * amp[1208]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[8][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P34_sm_gq_wpzqqq::matrix_12_gd_wpzdscx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 82;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[1228] + 1./3. * amp[1229] + 1./3. *
      amp[1230] + 1./3. * amp[1231] + 1./3. * amp[1232] + 1./3. * amp[1233] +
      1./3. * amp[1234] + 1./3. * amp[1235] + 1./3. * amp[1236] + 1./3. *
      amp[1237] + 1./3. * amp[1238] + 1./3. * amp[1239] + 1./3. * amp[1252] +
      1./3. * amp[1253] + 1./3. * amp[1254] + 1./3. * amp[1255] + 1./3. *
      amp[1256] + 1./3. * amp[1257] + 1./3. * amp[1258] + 1./3. * amp[1259] +
      1./3. * amp[1260] + 1./3. * amp[1261] + 1./3. * amp[1262] + 1./3. *
      amp[1263] + 1./3. * amp[1265] + 1./3. * amp[1271] + 1./3. * amp[1287] +
      1./3. * amp[1290]);
  jamp[1] = +1./2. * (-amp[1216] - amp[1217] - amp[1218] - amp[1219] -
      amp[1220] - amp[1221] - amp[1222] - amp[1223] - amp[1224] - amp[1225] -
      amp[1226] - amp[1227] - amp[1228] - amp[1229] - amp[1230] - amp[1231] -
      amp[1232] - amp[1233] - amp[1234] - amp[1235] - amp[1236] - amp[1237] -
      amp[1238] - amp[1239] - amp[1264] - amp[1265] + Complex<double> (0, 1) *
      amp[1266] - amp[1267] - Complex<double> (0, 1) * amp[1269] - amp[1270] +
      Complex<double> (0, 1) * amp[1272] - amp[1273] - Complex<double> (0, 1) *
      amp[1274] - amp[1275] - amp[1276] - Complex<double> (0, 1) * amp[1278] -
      amp[1279] - Complex<double> (0, 1) * amp[1280] - amp[1281] -
      Complex<double> (0, 1) * amp[1282] - Complex<double> (0, 1) * amp[1283] -
      amp[1284] - amp[1287] + Complex<double> (0, 1) * amp[1288] +
      Complex<double> (0, 1) * amp[1291] - Complex<double> (0, 1) * amp[1293] -
      Complex<double> (0, 1) * amp[1296]);
  jamp[2] = +1./2. * (-amp[1240] - amp[1241] - amp[1242] - amp[1243] -
      amp[1244] - amp[1245] - amp[1246] - amp[1247] - amp[1248] - amp[1249] -
      amp[1250] - amp[1251] - amp[1252] - amp[1253] - amp[1254] - amp[1255] -
      amp[1256] - amp[1257] - amp[1258] - amp[1259] - amp[1260] - amp[1261] -
      amp[1262] - amp[1263] - Complex<double> (0, 1) * amp[1266] - amp[1268] +
      Complex<double> (0, 1) * amp[1269] - amp[1271] - Complex<double> (0, 1) *
      amp[1272] + Complex<double> (0, 1) * amp[1274] - amp[1277] +
      Complex<double> (0, 1) * amp[1278] + Complex<double> (0, 1) * amp[1280] +
      Complex<double> (0, 1) * amp[1282] + Complex<double> (0, 1) * amp[1283] -
      amp[1285] - amp[1286] - Complex<double> (0, 1) * amp[1288] - amp[1289] -
      amp[1290] - Complex<double> (0, 1) * amp[1291] - amp[1292] +
      Complex<double> (0, 1) * amp[1293] - amp[1294] - amp[1295] +
      Complex<double> (0, 1) * amp[1296] - amp[1297]);
  jamp[3] = +1./2. * (+1./3. * amp[1216] + 1./3. * amp[1217] + 1./3. *
      amp[1218] + 1./3. * amp[1219] + 1./3. * amp[1220] + 1./3. * amp[1221] +
      1./3. * amp[1222] + 1./3. * amp[1223] + 1./3. * amp[1224] + 1./3. *
      amp[1225] + 1./3. * amp[1226] + 1./3. * amp[1227] + 1./3. * amp[1240] +
      1./3. * amp[1241] + 1./3. * amp[1242] + 1./3. * amp[1243] + 1./3. *
      amp[1244] + 1./3. * amp[1245] + 1./3. * amp[1246] + 1./3. * amp[1247] +
      1./3. * amp[1248] + 1./3. * amp[1249] + 1./3. * amp[1250] + 1./3. *
      amp[1251] + 1./3. * amp[1264] + 1./3. * amp[1267] + 1./3. * amp[1268] +
      1./3. * amp[1270] + 1./3. * amp[1273] + 1./3. * amp[1275] + 1./3. *
      amp[1276] + 1./3. * amp[1277] + 1./3. * amp[1279] + 1./3. * amp[1281] +
      1./3. * amp[1284] + 1./3. * amp[1285] + 1./3. * amp[1286] + 1./3. *
      amp[1289] + 1./3. * amp[1292] + 1./3. * amp[1294] + 1./3. * amp[1295] +
      1./3. * amp[1297]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[9][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P34_sm_gq_wpzqqq::matrix_12_gux_wpzsuxcx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 82;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[1298] + 1./3. * amp[1299] + 1./3. *
      amp[1300] + 1./3. * amp[1301] + 1./3. * amp[1302] + 1./3. * amp[1303] +
      1./3. * amp[1304] + 1./3. * amp[1305] + 1./3. * amp[1306] + 1./3. *
      amp[1307] + 1./3. * amp[1308] + 1./3. * amp[1309] + 1./3. * amp[1310] +
      1./3. * amp[1311] + 1./3. * amp[1312] + 1./3. * amp[1313] + 1./3. *
      amp[1314] + 1./3. * amp[1315] + 1./3. * amp[1316] + 1./3. * amp[1317] +
      1./3. * amp[1318] + 1./3. * amp[1319] + 1./3. * amp[1320] + 1./3. *
      amp[1321] + 1./3. * amp[1346] + 1./3. * amp[1349] + 1./3. * amp[1375] +
      1./3. * amp[1378]);
  jamp[1] = +1./2. * (-amp[1310] - amp[1311] - amp[1312] - amp[1313] -
      amp[1314] - amp[1315] - amp[1316] - amp[1317] - amp[1318] - amp[1319] -
      amp[1320] - amp[1321] - amp[1334] - amp[1335] - amp[1336] - amp[1337] -
      amp[1338] - amp[1339] - amp[1340] - amp[1341] - amp[1342] - amp[1343] -
      amp[1344] - amp[1345] - Complex<double> (0, 1) * amp[1348] - amp[1350] -
      Complex<double> (0, 1) * amp[1351] - Complex<double> (0, 1) * amp[1352] -
      Complex<double> (0, 1) * amp[1353] - amp[1354] - Complex<double> (0, 1) *
      amp[1356] - Complex<double> (0, 1) * amp[1359] - amp[1361] -
      Complex<double> (0, 1) * amp[1362] - amp[1363] - amp[1364] -
      Complex<double> (0, 1) * amp[1365] - amp[1366] - Complex<double> (0, 1) *
      amp[1368] - Complex<double> (0, 1) * amp[1371] - amp[1372] - amp[1373] -
      amp[1375] - Complex<double> (0, 1) * amp[1376] - amp[1377] - amp[1378] -
      Complex<double> (0, 1) * amp[1379]);
  jamp[2] = +1./2. * (-amp[1298] - amp[1299] - amp[1300] - amp[1301] -
      amp[1302] - amp[1303] - amp[1304] - amp[1305] - amp[1306] - amp[1307] -
      amp[1308] - amp[1309] - amp[1322] - amp[1323] - amp[1324] - amp[1325] -
      amp[1326] - amp[1327] - amp[1328] - amp[1329] - amp[1330] - amp[1331] -
      amp[1332] - amp[1333] - amp[1346] - amp[1347] + Complex<double> (0, 1) *
      amp[1348] - amp[1349] + Complex<double> (0, 1) * amp[1351] +
      Complex<double> (0, 1) * amp[1352] + Complex<double> (0, 1) * amp[1353] -
      amp[1355] + Complex<double> (0, 1) * amp[1356] - amp[1357] - amp[1358] +
      Complex<double> (0, 1) * amp[1359] - amp[1360] + Complex<double> (0, 1) *
      amp[1362] + Complex<double> (0, 1) * amp[1365] - amp[1367] +
      Complex<double> (0, 1) * amp[1368] - amp[1369] - amp[1370] +
      Complex<double> (0, 1) * amp[1371] - amp[1374] + Complex<double> (0, 1) *
      amp[1376] + Complex<double> (0, 1) * amp[1379]);
  jamp[3] = +1./2. * (+1./3. * amp[1322] + 1./3. * amp[1323] + 1./3. *
      amp[1324] + 1./3. * amp[1325] + 1./3. * amp[1326] + 1./3. * amp[1327] +
      1./3. * amp[1328] + 1./3. * amp[1329] + 1./3. * amp[1330] + 1./3. *
      amp[1331] + 1./3. * amp[1332] + 1./3. * amp[1333] + 1./3. * amp[1334] +
      1./3. * amp[1335] + 1./3. * amp[1336] + 1./3. * amp[1337] + 1./3. *
      amp[1338] + 1./3. * amp[1339] + 1./3. * amp[1340] + 1./3. * amp[1341] +
      1./3. * amp[1342] + 1./3. * amp[1343] + 1./3. * amp[1344] + 1./3. *
      amp[1345] + 1./3. * amp[1347] + 1./3. * amp[1350] + 1./3. * amp[1354] +
      1./3. * amp[1355] + 1./3. * amp[1357] + 1./3. * amp[1358] + 1./3. *
      amp[1360] + 1./3. * amp[1361] + 1./3. * amp[1363] + 1./3. * amp[1364] +
      1./3. * amp[1366] + 1./3. * amp[1367] + 1./3. * amp[1369] + 1./3. *
      amp[1370] + 1./3. * amp[1372] + 1./3. * amp[1373] + 1./3. * amp[1374] +
      1./3. * amp[1377]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[10][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P34_sm_gq_wpzqqq::matrix_12_gdx_wpzcuxcx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 82;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[1380] + 1./3. * amp[1381] + 1./3. *
      amp[1382] + 1./3. * amp[1383] + 1./3. * amp[1384] + 1./3. * amp[1385] +
      1./3. * amp[1386] + 1./3. * amp[1387] + 1./3. * amp[1388] + 1./3. *
      amp[1389] + 1./3. * amp[1390] + 1./3. * amp[1391] + 1./3. * amp[1404] +
      1./3. * amp[1405] + 1./3. * amp[1406] + 1./3. * amp[1407] + 1./3. *
      amp[1408] + 1./3. * amp[1409] + 1./3. * amp[1410] + 1./3. * amp[1411] +
      1./3. * amp[1412] + 1./3. * amp[1413] + 1./3. * amp[1414] + 1./3. *
      amp[1415] + 1./3. * amp[1428] + 1./3. * amp[1431] + 1./3. * amp[1432] +
      1./3. * amp[1434] + 1./3. * amp[1437] + 1./3. * amp[1439] + 1./3. *
      amp[1440] + 1./3. * amp[1441] + 1./3. * amp[1443] + 1./3. * amp[1445] +
      1./3. * amp[1448] + 1./3. * amp[1449] + 1./3. * amp[1450] + 1./3. *
      amp[1453] + 1./3. * amp[1456] + 1./3. * amp[1458] + 1./3. * amp[1459] +
      1./3. * amp[1461]);
  jamp[1] = +1./2. * (-amp[1404] - amp[1405] - amp[1406] - amp[1407] -
      amp[1408] - amp[1409] - amp[1410] - amp[1411] - amp[1412] - amp[1413] -
      amp[1414] - amp[1415] - amp[1416] - amp[1417] - amp[1418] - amp[1419] -
      amp[1420] - amp[1421] - amp[1422] - amp[1423] - amp[1424] - amp[1425] -
      amp[1426] - amp[1427] - Complex<double> (0, 1) * amp[1430] - amp[1432] +
      Complex<double> (0, 1) * amp[1433] - amp[1435] - Complex<double> (0, 1) *
      amp[1436] + Complex<double> (0, 1) * amp[1438] - amp[1441] +
      Complex<double> (0, 1) * amp[1442] + Complex<double> (0, 1) * amp[1444] +
      Complex<double> (0, 1) * amp[1446] + Complex<double> (0, 1) * amp[1447] -
      amp[1449] - amp[1450] - Complex<double> (0, 1) * amp[1452] - amp[1453] -
      amp[1454] - Complex<double> (0, 1) * amp[1455] - amp[1456] +
      Complex<double> (0, 1) * amp[1457] - amp[1458] - amp[1459] +
      Complex<double> (0, 1) * amp[1460] - amp[1461]);
  jamp[2] = +1./2. * (-amp[1380] - amp[1381] - amp[1382] - amp[1383] -
      amp[1384] - amp[1385] - amp[1386] - amp[1387] - amp[1388] - amp[1389] -
      amp[1390] - amp[1391] - amp[1392] - amp[1393] - amp[1394] - amp[1395] -
      amp[1396] - amp[1397] - amp[1398] - amp[1399] - amp[1400] - amp[1401] -
      amp[1402] - amp[1403] - amp[1428] - amp[1429] + Complex<double> (0, 1) *
      amp[1430] - amp[1431] - Complex<double> (0, 1) * amp[1433] - amp[1434] +
      Complex<double> (0, 1) * amp[1436] - amp[1437] - Complex<double> (0, 1) *
      amp[1438] - amp[1439] - amp[1440] - Complex<double> (0, 1) * amp[1442] -
      amp[1443] - Complex<double> (0, 1) * amp[1444] - amp[1445] -
      Complex<double> (0, 1) * amp[1446] - Complex<double> (0, 1) * amp[1447] -
      amp[1448] - amp[1451] + Complex<double> (0, 1) * amp[1452] +
      Complex<double> (0, 1) * amp[1455] - Complex<double> (0, 1) * amp[1457] -
      Complex<double> (0, 1) * amp[1460]);
  jamp[3] = +1./2. * (+1./3. * amp[1392] + 1./3. * amp[1393] + 1./3. *
      amp[1394] + 1./3. * amp[1395] + 1./3. * amp[1396] + 1./3. * amp[1397] +
      1./3. * amp[1398] + 1./3. * amp[1399] + 1./3. * amp[1400] + 1./3. *
      amp[1401] + 1./3. * amp[1402] + 1./3. * amp[1403] + 1./3. * amp[1416] +
      1./3. * amp[1417] + 1./3. * amp[1418] + 1./3. * amp[1419] + 1./3. *
      amp[1420] + 1./3. * amp[1421] + 1./3. * amp[1422] + 1./3. * amp[1423] +
      1./3. * amp[1424] + 1./3. * amp[1425] + 1./3. * amp[1426] + 1./3. *
      amp[1427] + 1./3. * amp[1429] + 1./3. * amp[1435] + 1./3. * amp[1451] +
      1./3. * amp[1454]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[11][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P34_sm_gq_wpzqqq::matrix_12_gdx_wpzsuxsx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 82;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[1462] + 1./3. * amp[1463] + 1./3. *
      amp[1464] + 1./3. * amp[1465] + 1./3. * amp[1466] + 1./3. * amp[1467] +
      1./3. * amp[1468] + 1./3. * amp[1469] + 1./3. * amp[1470] + 1./3. *
      amp[1471] + 1./3. * amp[1472] + 1./3. * amp[1473] + 1./3. * amp[1474] +
      1./3. * amp[1475] + 1./3. * amp[1476] + 1./3. * amp[1477] + 1./3. *
      amp[1478] + 1./3. * amp[1479] + 1./3. * amp[1480] + 1./3. * amp[1481] +
      1./3. * amp[1482] + 1./3. * amp[1483] + 1./3. * amp[1484] + 1./3. *
      amp[1485] + 1./3. * amp[1510] + 1./3. * amp[1511] + 1./3. * amp[1513] +
      1./3. * amp[1516] + 1./3. * amp[1519] + 1./3. * amp[1521] + 1./3. *
      amp[1522] + 1./3. * amp[1523] + 1./3. * amp[1525] + 1./3. * amp[1527] +
      1./3. * amp[1530] + 1./3. * amp[1531] + 1./3. * amp[1532] + 1./3. *
      amp[1535] + 1./3. * amp[1538] + 1./3. * amp[1540] + 1./3. * amp[1541] +
      1./3. * amp[1543]);
  jamp[1] = +1./2. * (-amp[1474] - amp[1475] - amp[1476] - amp[1477] -
      amp[1478] - amp[1479] - amp[1480] - amp[1481] - amp[1482] - amp[1483] -
      amp[1484] - amp[1485] - amp[1498] - amp[1499] - amp[1500] - amp[1501] -
      amp[1502] - amp[1503] - amp[1504] - amp[1505] - amp[1506] - amp[1507] -
      amp[1508] - amp[1509] - amp[1511] + Complex<double> (0, 1) * amp[1512] -
      Complex<double> (0, 1) * amp[1515] - amp[1517] - Complex<double> (0, 1) *
      amp[1518] + Complex<double> (0, 1) * amp[1520] - amp[1523] +
      Complex<double> (0, 1) * amp[1524] + Complex<double> (0, 1) * amp[1526] +
      Complex<double> (0, 1) * amp[1528] + Complex<double> (0, 1) * amp[1529] -
      amp[1531] - amp[1532] - Complex<double> (0, 1) * amp[1534] - amp[1535] -
      amp[1536] - Complex<double> (0, 1) * amp[1537] - amp[1538] +
      Complex<double> (0, 1) * amp[1539] - amp[1540] - amp[1541] +
      Complex<double> (0, 1) * amp[1542] - amp[1543]);
  jamp[2] = +1./2. * (-amp[1462] - amp[1463] - amp[1464] - amp[1465] -
      amp[1466] - amp[1467] - amp[1468] - amp[1469] - amp[1470] - amp[1471] -
      amp[1472] - amp[1473] - amp[1486] - amp[1487] - amp[1488] - amp[1489] -
      amp[1490] - amp[1491] - amp[1492] - amp[1493] - amp[1494] - amp[1495] -
      amp[1496] - amp[1497] - amp[1510] - Complex<double> (0, 1) * amp[1512] -
      amp[1513] - amp[1514] + Complex<double> (0, 1) * amp[1515] - amp[1516] +
      Complex<double> (0, 1) * amp[1518] - amp[1519] - Complex<double> (0, 1) *
      amp[1520] - amp[1521] - amp[1522] - Complex<double> (0, 1) * amp[1524] -
      amp[1525] - Complex<double> (0, 1) * amp[1526] - amp[1527] -
      Complex<double> (0, 1) * amp[1528] - Complex<double> (0, 1) * amp[1529] -
      amp[1530] - amp[1533] + Complex<double> (0, 1) * amp[1534] +
      Complex<double> (0, 1) * amp[1537] - Complex<double> (0, 1) * amp[1539] -
      Complex<double> (0, 1) * amp[1542]);
  jamp[3] = +1./2. * (+1./3. * amp[1486] + 1./3. * amp[1487] + 1./3. *
      amp[1488] + 1./3. * amp[1489] + 1./3. * amp[1490] + 1./3. * amp[1491] +
      1./3. * amp[1492] + 1./3. * amp[1493] + 1./3. * amp[1494] + 1./3. *
      amp[1495] + 1./3. * amp[1496] + 1./3. * amp[1497] + 1./3. * amp[1498] +
      1./3. * amp[1499] + 1./3. * amp[1500] + 1./3. * amp[1501] + 1./3. *
      amp[1502] + 1./3. * amp[1503] + 1./3. * amp[1504] + 1./3. * amp[1505] +
      1./3. * amp[1506] + 1./3. * amp[1507] + 1./3. * amp[1508] + 1./3. *
      amp[1509] + 1./3. * amp[1514] + 1./3. * amp[1517] + 1./3. * amp[1533] +
      1./3. * amp[1536]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[12][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P34_sm_gq_wpzqqq::matrix_12_gdx_wpzscxdx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 82;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[1544] + amp[1545] + amp[1546] + amp[1547] +
      amp[1548] + amp[1549] + amp[1550] + amp[1551] + amp[1552] + amp[1553] +
      amp[1554] + amp[1555] + amp[1556] + amp[1557] + amp[1558] + amp[1559] +
      amp[1560] + amp[1561] + amp[1562] + amp[1563] + amp[1564] + amp[1565] +
      amp[1566] + amp[1567] + amp[1592] + amp[1593] - Complex<double> (0, 1) *
      amp[1594] + amp[1595] + Complex<double> (0, 1) * amp[1597] + amp[1598] -
      Complex<double> (0, 1) * amp[1600] + amp[1601] + Complex<double> (0, 1) *
      amp[1602] + amp[1603] + amp[1604] + Complex<double> (0, 1) * amp[1606] +
      amp[1607] + Complex<double> (0, 1) * amp[1608] + amp[1609] +
      Complex<double> (0, 1) * amp[1610] + Complex<double> (0, 1) * amp[1611] +
      amp[1612] + amp[1615] - Complex<double> (0, 1) * amp[1616] -
      Complex<double> (0, 1) * amp[1619] + Complex<double> (0, 1) * amp[1621] +
      Complex<double> (0, 1) * amp[1624]);
  jamp[1] = +1./2. * (-1./3. * amp[1556] - 1./3. * amp[1557] - 1./3. *
      amp[1558] - 1./3. * amp[1559] - 1./3. * amp[1560] - 1./3. * amp[1561] -
      1./3. * amp[1562] - 1./3. * amp[1563] - 1./3. * amp[1564] - 1./3. *
      amp[1565] - 1./3. * amp[1566] - 1./3. * amp[1567] - 1./3. * amp[1580] -
      1./3. * amp[1581] - 1./3. * amp[1582] - 1./3. * amp[1583] - 1./3. *
      amp[1584] - 1./3. * amp[1585] - 1./3. * amp[1586] - 1./3. * amp[1587] -
      1./3. * amp[1588] - 1./3. * amp[1589] - 1./3. * amp[1590] - 1./3. *
      amp[1591] - 1./3. * amp[1593] - 1./3. * amp[1599] - 1./3. * amp[1615] -
      1./3. * amp[1618]);
  jamp[2] = +1./2. * (-1./3. * amp[1544] - 1./3. * amp[1545] - 1./3. *
      amp[1546] - 1./3. * amp[1547] - 1./3. * amp[1548] - 1./3. * amp[1549] -
      1./3. * amp[1550] - 1./3. * amp[1551] - 1./3. * amp[1552] - 1./3. *
      amp[1553] - 1./3. * amp[1554] - 1./3. * amp[1555] - 1./3. * amp[1568] -
      1./3. * amp[1569] - 1./3. * amp[1570] - 1./3. * amp[1571] - 1./3. *
      amp[1572] - 1./3. * amp[1573] - 1./3. * amp[1574] - 1./3. * amp[1575] -
      1./3. * amp[1576] - 1./3. * amp[1577] - 1./3. * amp[1578] - 1./3. *
      amp[1579] - 1./3. * amp[1592] - 1./3. * amp[1595] - 1./3. * amp[1596] -
      1./3. * amp[1598] - 1./3. * amp[1601] - 1./3. * amp[1603] - 1./3. *
      amp[1604] - 1./3. * amp[1605] - 1./3. * amp[1607] - 1./3. * amp[1609] -
      1./3. * amp[1612] - 1./3. * amp[1613] - 1./3. * amp[1614] - 1./3. *
      amp[1617] - 1./3. * amp[1620] - 1./3. * amp[1622] - 1./3. * amp[1623] -
      1./3. * amp[1625]);
  jamp[3] = +1./2. * (+amp[1568] + amp[1569] + amp[1570] + amp[1571] +
      amp[1572] + amp[1573] + amp[1574] + amp[1575] + amp[1576] + amp[1577] +
      amp[1578] + amp[1579] + amp[1580] + amp[1581] + amp[1582] + amp[1583] +
      amp[1584] + amp[1585] + amp[1586] + amp[1587] + amp[1588] + amp[1589] +
      amp[1590] + amp[1591] + Complex<double> (0, 1) * amp[1594] + amp[1596] -
      Complex<double> (0, 1) * amp[1597] + amp[1599] + Complex<double> (0, 1) *
      amp[1600] - Complex<double> (0, 1) * amp[1602] + amp[1605] -
      Complex<double> (0, 1) * amp[1606] - Complex<double> (0, 1) * amp[1608] -
      Complex<double> (0, 1) * amp[1610] - Complex<double> (0, 1) * amp[1611] +
      amp[1613] + amp[1614] + Complex<double> (0, 1) * amp[1616] + amp[1617] +
      amp[1618] + Complex<double> (0, 1) * amp[1619] + amp[1620] -
      Complex<double> (0, 1) * amp[1621] + amp[1622] + amp[1623] -
      Complex<double> (0, 1) * amp[1624] + amp[1625]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[13][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}


}  // end namespace PY8MEs_namespace

