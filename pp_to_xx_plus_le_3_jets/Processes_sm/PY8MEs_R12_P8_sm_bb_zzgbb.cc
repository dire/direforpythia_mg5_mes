//==========================================================================
// This file has been automatically generated for Pythia 8 by
// MadGraph5_aMC@NLO v. 2.6.0, 2017-08-16
// By the MadGraph5_aMC@NLO Development Team
// Visit launchpad.net/madgraph5 and amcatnlo.web.cern.ch
//==========================================================================

#include "PY8MEs_R12_P8_sm_bb_zzgbb.h"
#include "HelAmps_sm.h"

using namespace Pythia8_sm; 

namespace PY8MEs_namespace 
{
//==========================================================================
// Class member functions for calculating the matrix elements for
// Process: b b > z z g b b WEIGHTED<=7 @12
// Process: b b~ > z z g b b~ WEIGHTED<=7 @12
// Process: b~ b~ > z z g b~ b~ WEIGHTED<=7 @12

// Exception class
class PY8MEs_R12_P8_sm_bb_zzgbbException : public exception
{
  virtual const char * what() const throw()
  {
    return "Exception in class 'PY8MEs_R12_P8_sm_bb_zzgbb'."; 
  }
}
PY8MEs_R12_P8_sm_bb_zzgbb_exception; 

std::set<int> PY8MEs_R12_P8_sm_bb_zzgbb::s_channel_proc = std::set<int>
    (createset<int> ());

int PY8MEs_R12_P8_sm_bb_zzgbb::helicities[ncomb][nexternal] = {{-1, -1, -1, -1,
    -1, -1, -1}, {-1, -1, -1, -1, -1, -1, 1}, {-1, -1, -1, -1, -1, 1, -1}, {-1,
    -1, -1, -1, -1, 1, 1}, {-1, -1, -1, -1, 1, -1, -1}, {-1, -1, -1, -1, 1, -1,
    1}, {-1, -1, -1, -1, 1, 1, -1}, {-1, -1, -1, -1, 1, 1, 1}, {-1, -1, -1, 0,
    -1, -1, -1}, {-1, -1, -1, 0, -1, -1, 1}, {-1, -1, -1, 0, -1, 1, -1}, {-1,
    -1, -1, 0, -1, 1, 1}, {-1, -1, -1, 0, 1, -1, -1}, {-1, -1, -1, 0, 1, -1,
    1}, {-1, -1, -1, 0, 1, 1, -1}, {-1, -1, -1, 0, 1, 1, 1}, {-1, -1, -1, 1,
    -1, -1, -1}, {-1, -1, -1, 1, -1, -1, 1}, {-1, -1, -1, 1, -1, 1, -1}, {-1,
    -1, -1, 1, -1, 1, 1}, {-1, -1, -1, 1, 1, -1, -1}, {-1, -1, -1, 1, 1, -1,
    1}, {-1, -1, -1, 1, 1, 1, -1}, {-1, -1, -1, 1, 1, 1, 1}, {-1, -1, 0, -1,
    -1, -1, -1}, {-1, -1, 0, -1, -1, -1, 1}, {-1, -1, 0, -1, -1, 1, -1}, {-1,
    -1, 0, -1, -1, 1, 1}, {-1, -1, 0, -1, 1, -1, -1}, {-1, -1, 0, -1, 1, -1,
    1}, {-1, -1, 0, -1, 1, 1, -1}, {-1, -1, 0, -1, 1, 1, 1}, {-1, -1, 0, 0, -1,
    -1, -1}, {-1, -1, 0, 0, -1, -1, 1}, {-1, -1, 0, 0, -1, 1, -1}, {-1, -1, 0,
    0, -1, 1, 1}, {-1, -1, 0, 0, 1, -1, -1}, {-1, -1, 0, 0, 1, -1, 1}, {-1, -1,
    0, 0, 1, 1, -1}, {-1, -1, 0, 0, 1, 1, 1}, {-1, -1, 0, 1, -1, -1, -1}, {-1,
    -1, 0, 1, -1, -1, 1}, {-1, -1, 0, 1, -1, 1, -1}, {-1, -1, 0, 1, -1, 1, 1},
    {-1, -1, 0, 1, 1, -1, -1}, {-1, -1, 0, 1, 1, -1, 1}, {-1, -1, 0, 1, 1, 1,
    -1}, {-1, -1, 0, 1, 1, 1, 1}, {-1, -1, 1, -1, -1, -1, -1}, {-1, -1, 1, -1,
    -1, -1, 1}, {-1, -1, 1, -1, -1, 1, -1}, {-1, -1, 1, -1, -1, 1, 1}, {-1, -1,
    1, -1, 1, -1, -1}, {-1, -1, 1, -1, 1, -1, 1}, {-1, -1, 1, -1, 1, 1, -1},
    {-1, -1, 1, -1, 1, 1, 1}, {-1, -1, 1, 0, -1, -1, -1}, {-1, -1, 1, 0, -1,
    -1, 1}, {-1, -1, 1, 0, -1, 1, -1}, {-1, -1, 1, 0, -1, 1, 1}, {-1, -1, 1, 0,
    1, -1, -1}, {-1, -1, 1, 0, 1, -1, 1}, {-1, -1, 1, 0, 1, 1, -1}, {-1, -1, 1,
    0, 1, 1, 1}, {-1, -1, 1, 1, -1, -1, -1}, {-1, -1, 1, 1, -1, -1, 1}, {-1,
    -1, 1, 1, -1, 1, -1}, {-1, -1, 1, 1, -1, 1, 1}, {-1, -1, 1, 1, 1, -1, -1},
    {-1, -1, 1, 1, 1, -1, 1}, {-1, -1, 1, 1, 1, 1, -1}, {-1, -1, 1, 1, 1, 1,
    1}, {-1, 1, -1, -1, -1, -1, -1}, {-1, 1, -1, -1, -1, -1, 1}, {-1, 1, -1,
    -1, -1, 1, -1}, {-1, 1, -1, -1, -1, 1, 1}, {-1, 1, -1, -1, 1, -1, -1}, {-1,
    1, -1, -1, 1, -1, 1}, {-1, 1, -1, -1, 1, 1, -1}, {-1, 1, -1, -1, 1, 1, 1},
    {-1, 1, -1, 0, -1, -1, -1}, {-1, 1, -1, 0, -1, -1, 1}, {-1, 1, -1, 0, -1,
    1, -1}, {-1, 1, -1, 0, -1, 1, 1}, {-1, 1, -1, 0, 1, -1, -1}, {-1, 1, -1, 0,
    1, -1, 1}, {-1, 1, -1, 0, 1, 1, -1}, {-1, 1, -1, 0, 1, 1, 1}, {-1, 1, -1,
    1, -1, -1, -1}, {-1, 1, -1, 1, -1, -1, 1}, {-1, 1, -1, 1, -1, 1, -1}, {-1,
    1, -1, 1, -1, 1, 1}, {-1, 1, -1, 1, 1, -1, -1}, {-1, 1, -1, 1, 1, -1, 1},
    {-1, 1, -1, 1, 1, 1, -1}, {-1, 1, -1, 1, 1, 1, 1}, {-1, 1, 0, -1, -1, -1,
    -1}, {-1, 1, 0, -1, -1, -1, 1}, {-1, 1, 0, -1, -1, 1, -1}, {-1, 1, 0, -1,
    -1, 1, 1}, {-1, 1, 0, -1, 1, -1, -1}, {-1, 1, 0, -1, 1, -1, 1}, {-1, 1, 0,
    -1, 1, 1, -1}, {-1, 1, 0, -1, 1, 1, 1}, {-1, 1, 0, 0, -1, -1, -1}, {-1, 1,
    0, 0, -1, -1, 1}, {-1, 1, 0, 0, -1, 1, -1}, {-1, 1, 0, 0, -1, 1, 1}, {-1,
    1, 0, 0, 1, -1, -1}, {-1, 1, 0, 0, 1, -1, 1}, {-1, 1, 0, 0, 1, 1, -1}, {-1,
    1, 0, 0, 1, 1, 1}, {-1, 1, 0, 1, -1, -1, -1}, {-1, 1, 0, 1, -1, -1, 1},
    {-1, 1, 0, 1, -1, 1, -1}, {-1, 1, 0, 1, -1, 1, 1}, {-1, 1, 0, 1, 1, -1,
    -1}, {-1, 1, 0, 1, 1, -1, 1}, {-1, 1, 0, 1, 1, 1, -1}, {-1, 1, 0, 1, 1, 1,
    1}, {-1, 1, 1, -1, -1, -1, -1}, {-1, 1, 1, -1, -1, -1, 1}, {-1, 1, 1, -1,
    -1, 1, -1}, {-1, 1, 1, -1, -1, 1, 1}, {-1, 1, 1, -1, 1, -1, -1}, {-1, 1, 1,
    -1, 1, -1, 1}, {-1, 1, 1, -1, 1, 1, -1}, {-1, 1, 1, -1, 1, 1, 1}, {-1, 1,
    1, 0, -1, -1, -1}, {-1, 1, 1, 0, -1, -1, 1}, {-1, 1, 1, 0, -1, 1, -1}, {-1,
    1, 1, 0, -1, 1, 1}, {-1, 1, 1, 0, 1, -1, -1}, {-1, 1, 1, 0, 1, -1, 1}, {-1,
    1, 1, 0, 1, 1, -1}, {-1, 1, 1, 0, 1, 1, 1}, {-1, 1, 1, 1, -1, -1, -1}, {-1,
    1, 1, 1, -1, -1, 1}, {-1, 1, 1, 1, -1, 1, -1}, {-1, 1, 1, 1, -1, 1, 1},
    {-1, 1, 1, 1, 1, -1, -1}, {-1, 1, 1, 1, 1, -1, 1}, {-1, 1, 1, 1, 1, 1, -1},
    {-1, 1, 1, 1, 1, 1, 1}, {1, -1, -1, -1, -1, -1, -1}, {1, -1, -1, -1, -1,
    -1, 1}, {1, -1, -1, -1, -1, 1, -1}, {1, -1, -1, -1, -1, 1, 1}, {1, -1, -1,
    -1, 1, -1, -1}, {1, -1, -1, -1, 1, -1, 1}, {1, -1, -1, -1, 1, 1, -1}, {1,
    -1, -1, -1, 1, 1, 1}, {1, -1, -1, 0, -1, -1, -1}, {1, -1, -1, 0, -1, -1,
    1}, {1, -1, -1, 0, -1, 1, -1}, {1, -1, -1, 0, -1, 1, 1}, {1, -1, -1, 0, 1,
    -1, -1}, {1, -1, -1, 0, 1, -1, 1}, {1, -1, -1, 0, 1, 1, -1}, {1, -1, -1, 0,
    1, 1, 1}, {1, -1, -1, 1, -1, -1, -1}, {1, -1, -1, 1, -1, -1, 1}, {1, -1,
    -1, 1, -1, 1, -1}, {1, -1, -1, 1, -1, 1, 1}, {1, -1, -1, 1, 1, -1, -1}, {1,
    -1, -1, 1, 1, -1, 1}, {1, -1, -1, 1, 1, 1, -1}, {1, -1, -1, 1, 1, 1, 1},
    {1, -1, 0, -1, -1, -1, -1}, {1, -1, 0, -1, -1, -1, 1}, {1, -1, 0, -1, -1,
    1, -1}, {1, -1, 0, -1, -1, 1, 1}, {1, -1, 0, -1, 1, -1, -1}, {1, -1, 0, -1,
    1, -1, 1}, {1, -1, 0, -1, 1, 1, -1}, {1, -1, 0, -1, 1, 1, 1}, {1, -1, 0, 0,
    -1, -1, -1}, {1, -1, 0, 0, -1, -1, 1}, {1, -1, 0, 0, -1, 1, -1}, {1, -1, 0,
    0, -1, 1, 1}, {1, -1, 0, 0, 1, -1, -1}, {1, -1, 0, 0, 1, -1, 1}, {1, -1, 0,
    0, 1, 1, -1}, {1, -1, 0, 0, 1, 1, 1}, {1, -1, 0, 1, -1, -1, -1}, {1, -1, 0,
    1, -1, -1, 1}, {1, -1, 0, 1, -1, 1, -1}, {1, -1, 0, 1, -1, 1, 1}, {1, -1,
    0, 1, 1, -1, -1}, {1, -1, 0, 1, 1, -1, 1}, {1, -1, 0, 1, 1, 1, -1}, {1, -1,
    0, 1, 1, 1, 1}, {1, -1, 1, -1, -1, -1, -1}, {1, -1, 1, -1, -1, -1, 1}, {1,
    -1, 1, -1, -1, 1, -1}, {1, -1, 1, -1, -1, 1, 1}, {1, -1, 1, -1, 1, -1, -1},
    {1, -1, 1, -1, 1, -1, 1}, {1, -1, 1, -1, 1, 1, -1}, {1, -1, 1, -1, 1, 1,
    1}, {1, -1, 1, 0, -1, -1, -1}, {1, -1, 1, 0, -1, -1, 1}, {1, -1, 1, 0, -1,
    1, -1}, {1, -1, 1, 0, -1, 1, 1}, {1, -1, 1, 0, 1, -1, -1}, {1, -1, 1, 0, 1,
    -1, 1}, {1, -1, 1, 0, 1, 1, -1}, {1, -1, 1, 0, 1, 1, 1}, {1, -1, 1, 1, -1,
    -1, -1}, {1, -1, 1, 1, -1, -1, 1}, {1, -1, 1, 1, -1, 1, -1}, {1, -1, 1, 1,
    -1, 1, 1}, {1, -1, 1, 1, 1, -1, -1}, {1, -1, 1, 1, 1, -1, 1}, {1, -1, 1, 1,
    1, 1, -1}, {1, -1, 1, 1, 1, 1, 1}, {1, 1, -1, -1, -1, -1, -1}, {1, 1, -1,
    -1, -1, -1, 1}, {1, 1, -1, -1, -1, 1, -1}, {1, 1, -1, -1, -1, 1, 1}, {1, 1,
    -1, -1, 1, -1, -1}, {1, 1, -1, -1, 1, -1, 1}, {1, 1, -1, -1, 1, 1, -1}, {1,
    1, -1, -1, 1, 1, 1}, {1, 1, -1, 0, -1, -1, -1}, {1, 1, -1, 0, -1, -1, 1},
    {1, 1, -1, 0, -1, 1, -1}, {1, 1, -1, 0, -1, 1, 1}, {1, 1, -1, 0, 1, -1,
    -1}, {1, 1, -1, 0, 1, -1, 1}, {1, 1, -1, 0, 1, 1, -1}, {1, 1, -1, 0, 1, 1,
    1}, {1, 1, -1, 1, -1, -1, -1}, {1, 1, -1, 1, -1, -1, 1}, {1, 1, -1, 1, -1,
    1, -1}, {1, 1, -1, 1, -1, 1, 1}, {1, 1, -1, 1, 1, -1, -1}, {1, 1, -1, 1, 1,
    -1, 1}, {1, 1, -1, 1, 1, 1, -1}, {1, 1, -1, 1, 1, 1, 1}, {1, 1, 0, -1, -1,
    -1, -1}, {1, 1, 0, -1, -1, -1, 1}, {1, 1, 0, -1, -1, 1, -1}, {1, 1, 0, -1,
    -1, 1, 1}, {1, 1, 0, -1, 1, -1, -1}, {1, 1, 0, -1, 1, -1, 1}, {1, 1, 0, -1,
    1, 1, -1}, {1, 1, 0, -1, 1, 1, 1}, {1, 1, 0, 0, -1, -1, -1}, {1, 1, 0, 0,
    -1, -1, 1}, {1, 1, 0, 0, -1, 1, -1}, {1, 1, 0, 0, -1, 1, 1}, {1, 1, 0, 0,
    1, -1, -1}, {1, 1, 0, 0, 1, -1, 1}, {1, 1, 0, 0, 1, 1, -1}, {1, 1, 0, 0, 1,
    1, 1}, {1, 1, 0, 1, -1, -1, -1}, {1, 1, 0, 1, -1, -1, 1}, {1, 1, 0, 1, -1,
    1, -1}, {1, 1, 0, 1, -1, 1, 1}, {1, 1, 0, 1, 1, -1, -1}, {1, 1, 0, 1, 1,
    -1, 1}, {1, 1, 0, 1, 1, 1, -1}, {1, 1, 0, 1, 1, 1, 1}, {1, 1, 1, -1, -1,
    -1, -1}, {1, 1, 1, -1, -1, -1, 1}, {1, 1, 1, -1, -1, 1, -1}, {1, 1, 1, -1,
    -1, 1, 1}, {1, 1, 1, -1, 1, -1, -1}, {1, 1, 1, -1, 1, -1, 1}, {1, 1, 1, -1,
    1, 1, -1}, {1, 1, 1, -1, 1, 1, 1}, {1, 1, 1, 0, -1, -1, -1}, {1, 1, 1, 0,
    -1, -1, 1}, {1, 1, 1, 0, -1, 1, -1}, {1, 1, 1, 0, -1, 1, 1}, {1, 1, 1, 0,
    1, -1, -1}, {1, 1, 1, 0, 1, -1, 1}, {1, 1, 1, 0, 1, 1, -1}, {1, 1, 1, 0, 1,
    1, 1}, {1, 1, 1, 1, -1, -1, -1}, {1, 1, 1, 1, -1, -1, 1}, {1, 1, 1, 1, -1,
    1, -1}, {1, 1, 1, 1, -1, 1, 1}, {1, 1, 1, 1, 1, -1, -1}, {1, 1, 1, 1, 1,
    -1, 1}, {1, 1, 1, 1, 1, 1, -1}, {1, 1, 1, 1, 1, 1, 1}};

// Normalization factors the various processes
// Denominators: spins, colors and identical particles
int PY8MEs_R12_P8_sm_bb_zzgbb::denom_colors[nprocesses] = {9, 9, 9}; 
int PY8MEs_R12_P8_sm_bb_zzgbb::denom_hels[nprocesses] = {4, 4, 4}; 
int PY8MEs_R12_P8_sm_bb_zzgbb::denom_iden[nprocesses] = {4, 2, 4}; 

//--------------------------------------------------------------------------
// Color config initialization
void PY8MEs_R12_P8_sm_bb_zzgbb::initColorConfigs() 
{
  color_configs = vector < vec_vec_int > (); 
  jamp_nc_relative_power = vector < vec_int > (); 

  // Color flows of process Process: b b > z z g b b WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #1
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #2
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #3
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 

  // Color flows of process Process: b b~ > z z g b b~ WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #1
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #2
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #3
  color_configs[1].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[1].push_back(0); 

  // Color flows of process Process: b~ b~ > z z g b~ b~ WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #1
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #2
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #3
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[2].push_back(0); 
}

//--------------------------------------------------------------------------
// Destructor.
PY8MEs_R12_P8_sm_bb_zzgbb::~PY8MEs_R12_P8_sm_bb_zzgbb() 
{
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    delete[] p[i]; 
    p[i] = NULL; 
  }
}

//--------------------------------------------------------------------------
// Invert the permutation mapping
vector<int> PY8MEs_R12_P8_sm_bb_zzgbb::invert_mapping(vector<int> mapping) 
{
  vector<int> inverted_mapping; 
  for (unsigned int i = 0; i < mapping.size(); i++ )
  {
    for (unsigned int j = 0; j < mapping.size(); j++ )
    {
      if (mapping[j] == ((int)i))
      {
        inverted_mapping.push_back(j); 
        break; 
      }
    }
  }
  return inverted_mapping; 
}

//--------------------------------------------------------------------------
// Return the list of possible helicity configurations
vector < vec_int > PY8MEs_R12_P8_sm_bb_zzgbb::getHelicityConfigs(vector<int>
    permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(ncomb, vector<int> (nexternal, 0)); 
  for (unsigned int ihel = 0; ihel < ncomb; ihel++ )
  {
    for(unsigned int j = 0; j < nexternal; j++ )
    {
      res[ihel][chosenPerm[j]] = helicities[ihel][j]; 
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Return the list of possible color configurations
vector < vec_int > PY8MEs_R12_P8_sm_bb_zzgbb::getColorConfigs(int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(color_configs[chosenProcID].size(), vector<int>
      (nexternal * 2, 0));
  for (unsigned int icol = 0; icol < color_configs[chosenProcID].size(); icol++
      )
  {
    for(unsigned int j = 0; j < (2 * nexternal); j++ )
    {
      res[icol][chosenPerm[j/2] * 2 + j%2] =
          color_configs[chosenProcID][icol][j];
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Get JAMP relative N_c power
int PY8MEs_R12_P8_sm_bb_zzgbb::getColorFlowRelativeNCPower(int color_flow_ID,
    int specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return jamp_nc_relative_power[chosenProcID][color_flow_ID]; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity ID -> Helicity Config
vector<int> PY8MEs_R12_P8_sm_bb_zzgbb::getHelicityConfigForID(int hel_ID,
    vector<int> permutation)
{
  if (hel_ID < 0 || hel_ID >= ncomb)
  {
    cerr <<  "Error in function 'getHelicityConfigForID' of class" << 
    " 'PY8MEs_R12_P8_sm_bb_zzgbb': Specified helicity ID '" << 
    hel_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R12_P8_sm_bb_zzgbb_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(nexternal, 0); 
  for (unsigned int j = 0; j < nexternal; j++ )
  {
    res[chosenPerm[j]] = helicities[hel_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity Config -> Helicity ID
int PY8MEs_R12_P8_sm_bb_zzgbb::getHelicityIDForConfig(vector<int> hel_config,
    vector<int> permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  int user_ihel = -1; 
  if (hel_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < ncomb; i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < nexternal; j++ )
      {
        if (helicities[i][chosenPerm[j]] != hel_config[j])
        {
          found = false; 
          break; 
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_ihel = i; 
        break; 
      }
    }
    if (user_ihel == -1)
    {
      cerr <<  "Error in function 'getHelicityIDForConfig' of class" << 
      " 'PY8MEs_R12_P8_sm_bb_zzgbb': Specified helicity" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R12_P8_sm_bb_zzgbb_exception; 
    }
  }
  return user_ihel; 
}


//--------------------------------------------------------------------------
// Implements the map Color ID -> Color Config
vector<int> PY8MEs_R12_P8_sm_bb_zzgbb::getColorConfigForID(int color_ID, int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < 0 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getColorConfigForID' of class" << 
    " 'PY8MEs_R12_P8_sm_bb_zzgbb': Specified color ID '" << 
    color_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R12_P8_sm_bb_zzgbb_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(color_configs[chosenProcID][color_ID].size(), 0); 
  for (unsigned int j = 0; j < (2 * nexternal); j++ )
  {
    res[chosenPerm[j/2] * 2 + j%2] = color_configs[chosenProcID][color_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Color Config -> Color ID
int PY8MEs_R12_P8_sm_bb_zzgbb::getColorIDForConfig(vector<int> color_config,
    int specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = -1; 
  if (color_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < color_configs[chosenProcID].size(); i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < (nexternal * 2); j++ )
      {

        // If colorless then make sure it matches
        // The little arithmetics in the color index is just
        // the permutation applies on the particle list which is
        // twice smaller since each particle can have two color indices.
        if (color_config[j] == 0)
        {
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] != 0)
          {
            found = false; 
            break; 
          }
          // Otherwise check that the color linked position matches
        }
        else
        {
          int color_linked_pos = -1; 
          // Find the other end of the line in the user color config
          for (unsigned int k = 0; k < (nexternal * 2); k++ )
          {
            if (k == j)
              continue; 
            if (color_config[j] == color_config[k])
            {
              color_linked_pos = k; 
              break; 
            }
          }
          if (color_linked_pos == -1)
          {
            cerr <<  "Error in function 'getColorIDForConfig' of class" << 
            " 'PY8MEs_R12_P8_sm_bb_zzgbb': A color line could " << 
            " not be closed." << endl; 
            throw PY8MEs_R12_P8_sm_bb_zzgbb_exception; 
          }
          // Now check whether the color line matches
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] !=
              color_configs[chosenProcID][i][chosenPerm[color_linked_pos/2] * 2
              + color_linked_pos%2])
          {
            found = false; 
            break; 
          }
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_icol = i; 
        break; 
      }
    }

    if (user_icol == -1)
    {
      cerr <<  "Error in function 'getColorIDForConfig' of class" << 
      " 'PY8MEs_R12_P8_sm_bb_zzgbb': Specified color" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R12_P8_sm_bb_zzgbb_exception; 
    }
  }
  return user_icol; 
}

//--------------------------------------------------------------------------
// Returns all result previously computed in SigmaKin
vector < vec_double > PY8MEs_R12_P8_sm_bb_zzgbb::getAllResults(int
    specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return all_results[chosenProcID]; 
}

//--------------------------------------------------------------------------
// Returns a result previously computed in SigmaKin for a specific helicity
// and color ID. -1 means avg and summed over that characteristic.
double PY8MEs_R12_P8_sm_bb_zzgbb::getResult(int helicity_ID, int color_ID, int
    specify_proc_ID)
{
  if (helicity_ID < - 1 || helicity_ID >= ncomb)
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R12_P8_sm_bb_zzgbb': Specified helicity ID '" << 
    helicity_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R12_P8_sm_bb_zzgbb_exception; 
  }
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < - 1 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R12_P8_sm_bb_zzgbb': Specified color ID '" << 
    color_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R12_P8_sm_bb_zzgbb_exception; 
  }
  return all_results[chosenProcID][helicity_ID + 1][color_ID + 1]; 
}

//--------------------------------------------------------------------------
// Check for the availability of the requested process and if available,
// If available, this returns the corresponding permutation and Proc_ID to use.
// If not available, this returns a negative Proc_ID.
pair < vector<int> , int > PY8MEs_R12_P8_sm_bb_zzgbb::static_getPY8ME(vector<int> initial_pdgs, vector<int> final_pdgs, set<int> schannels) 
{

  // Not available return value
  pair < vector<int> , int > NA(vector<int> (), -1); 

  // Check if s-channel requirements match
  if (nreq_s_channels > 0)
  {
    if (schannels != s_channel_proc)
      return NA; 
  }
  else
  {
    if (schannels.size() != 0)
      return NA; 
  }

  // Check number of final state particles
  if (final_pdgs.size() != (nexternal - ninitial))
    return NA; 

  // Check number of initial state particles
  if (initial_pdgs.size() != ninitial)
    return NA; 

  // List of processes available in this class
  const int nprocs = 4; 
  const int proc_IDS[nprocs] = {0, 1, 2, 1}; 
  const int in_pdgs[nprocs][ninitial] = {{5, 5}, {5, -5}, {-5, -5}, {-5, 5}}; 
  const int out_pdgs[nprocs][nexternal - ninitial] = {{23, 23, 21, 5, 5}, {23,
      23, 21, 5, -5}, {23, 23, 21, -5, -5}, {23, 23, 21, 5, -5}};

  bool in_pdgs_used[ninitial]; 
  bool out_pdgs_used[nexternal - ninitial]; 
  for(unsigned int i = 0; i < nprocs; i++ )
  {
    int permutations[nexternal]; 

    // Reinitialize initial state look-up variables
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      in_pdgs_used[j] = false; 
      permutations[j] = -1; 
    }
    // Look for initial state matches
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      for(unsigned int k = 0; k < ninitial; k++ )
      {
        // Make sure it has not been used already
        if (in_pdgs_used[k])
          continue; 
        if (initial_pdgs[k] == in_pdgs[i][j])
        {
          permutations[j] = k; 
          in_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[ninitial - 1] == -1)
      continue; 

    // Reinitialize final state look-up variables
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      out_pdgs_used[j] = false; 
      permutations[ninitial + j] = -1; 
    }
    // Look for final state matches
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      for(unsigned int k = 0; k < (nexternal - ninitial); k++ )
      {
        // Make sure it has not been used already
        if (out_pdgs_used[k])
          continue; 
        if (final_pdgs[k] == out_pdgs[i][j])
        {
          permutations[ninitial + j] = ninitial + k; 
          out_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[ninitial + j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[nexternal - 1] == -1)
      continue; 

    // Return process found
    return pair < vector<int> , int > (vector<int> (permutations, permutations
        + nexternal), proc_IDS[i]);
  }

  // No process found
  return NA; 
}

//--------------------------------------------------------------------------
// Set momenta
void PY8MEs_R12_P8_sm_bb_zzgbb::setMomenta(vector < vec_double > momenta_picked)
{
  if (momenta_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setMomenta' of class" << 
    " 'PY8MEs_R12_P8_sm_bb_zzgbb': Incorrect number of" << 
    " momenta specified." << endl; 
    throw PY8MEs_R12_P8_sm_bb_zzgbb_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    if (momenta_picked[i].size() != 4)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R12_P8_sm_bb_zzgbb': Incorrect number of" << 
      " momenta components specified." << endl; 
      throw PY8MEs_R12_P8_sm_bb_zzgbb_exception; 
    }
    if (momenta_picked[i][0] < 0.0)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R12_P8_sm_bb_zzgbb': A momentum was specified" << 
      " with negative energy. Check conventions." << endl; 
      throw PY8MEs_R12_P8_sm_bb_zzgbb_exception; 
    }
    for (unsigned int j = 0; j < 4; j++ )
    {
      p[i][j] = momenta_picked[i][j]; 
    }
  }
}

//--------------------------------------------------------------------------
// Set color configuration to use. An empty vector means sum over all.
void PY8MEs_R12_P8_sm_bb_zzgbb::setColors(vector<int> colors_picked)
{
  if (colors_picked.size() == 0)
  {
    user_colors = vector<int> (); 
    return; 
  }
  if (colors_picked.size() != (2 * nexternal))
  {
    cerr <<  "Error in function 'setColors' of class" << 
    " 'PY8MEs_R12_P8_sm_bb_zzgbb': Incorrect number" << 
    " of colors specified." << endl; 
    throw PY8MEs_R12_P8_sm_bb_zzgbb_exception; 
  }
  user_colors = vector<int> ((2 * nexternal), 0); 
  for(unsigned int i = 0; i < (2 * nexternal); i++ )
  {
    user_colors[i] = colors_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the helicity configuration to use. Am empty vector means sum over all.
void PY8MEs_R12_P8_sm_bb_zzgbb::setHelicities(vector<int> helicities_picked) 
{
  if (helicities_picked.size() != nexternal)
  {
    if (helicities_picked.size() == 0)
    {
      user_helicities = vector<int> (); 
      return; 
    }
    cerr <<  "Error in function 'setHelicities' of class" << 
    " 'PY8MEs_R12_P8_sm_bb_zzgbb': Incorrect number" << 
    " of helicities specified." << endl; 
    throw PY8MEs_R12_P8_sm_bb_zzgbb_exception; 
  }
  user_helicities = vector<int> (nexternal, 0); 
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    user_helicities[i] = helicities_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the permutation to use (will apply to momenta, colors and helicities)
void PY8MEs_R12_P8_sm_bb_zzgbb::setPermutation(vector<int> perm_picked) 
{
  if (perm_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setPermutations' of class" << 
    " 'PY8MEs_R12_P8_sm_bb_zzgbb': Incorrect number" << 
    " of permutations specified." << endl; 
    throw PY8MEs_R12_P8_sm_bb_zzgbb_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    perm[i] = perm_picked[i]; 
  }
}

// Set the proc_ID to use
void PY8MEs_R12_P8_sm_bb_zzgbb::setProcID(int procID_picked) 
{
  proc_ID = procID_picked; 
}

//--------------------------------------------------------------------------
// Initialize process.

void PY8MEs_R12_P8_sm_bb_zzgbb::initProc() 
{

  // Initialize flags
  include_symmetry_factors = true; 
  include_helicity_averaging_factors = true; 
  include_color_averaging_factors = true; 

  // Initialize vectors.
  perm = vector<int> (nexternal, 0); 
  user_colors = vector<int> (2 * nexternal, 0); 
  user_helicities = vector<int> (nexternal, 0); 
  p = vector < double * > (); 
  for (unsigned int i = 0; i < nexternal; i++ )
  {
    p.push_back(new double[4]); 
  }
  initColorConfigs(); 
  // Synchronize local variables dependent on the model with the active model.
  mME = vector<double> (nexternal, 0.); 
  syncProcModelParams(); 
  jamp2 = vector < vec_double > (3); 
  jamp2[0] = vector<double> (4, 0.); 
  jamp2[1] = vector<double> (4, 0.); 
  jamp2[2] = vector<double> (4, 0.); 
  all_results = vector < vec_vec_double > (3); 
  // The first entry is always the color or helicity avg/summed matrix element.
  all_results[0] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[1] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[2] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
}

// Synchronize local variables of the process that depend on the model
// parameters
void PY8MEs_R12_P8_sm_bb_zzgbb::syncProcModelParams() 
{

  // Instantiate the model class and set parameters that stay fixed during run
  mME[0] = pars->mdl_MB; 
  mME[1] = pars->mdl_MB; 
  mME[2] = pars->mdl_MZ; 
  mME[3] = pars->mdl_MZ; 
  mME[4] = pars->ZERO; 
  mME[5] = pars->mdl_MB; 
  mME[6] = pars->mdl_MB; 
}

//--------------------------------------------------------------------------
// Setter allowing to force particular values for the external masses
void PY8MEs_R12_P8_sm_bb_zzgbb::setMasses(vec_double external_masses) 
{

  if (external_masses.size() != mME.size())
  {
    cerr <<  "Error in function 'setMasses' of class" << 
    " 'PY8MEs_R12_P8_sm_bb_zzgbb': Incorrect number of" << 
    " masses specified." << endl; 
    throw PY8MEs_R12_P8_sm_bb_zzgbb_exception; 
  }
  for (unsigned int j = 0; j < mME.size(); j++ )
  {
    mME[j] = external_masses[perm[j]]; 
  }
}

//--------------------------------------------------------------------------
// Getter accessing external masses with the correct ordering
vector<double> PY8MEs_R12_P8_sm_bb_zzgbb::getMasses() 
{

  vec_double external_masses; 
  vector<int> invertedPerm; 
  invertedPerm = invert_mapping(perm); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    external_masses.push_back(mME[invertedPerm[i]]); 
  }
  return external_masses; 

}


// Set all values of the external masses to float(-mode) where mode can be
// 0 : Mass taken from the model
// 1 : Mass taken from p_i^2 if not massless to begin with
// 2 : Mass always taken from p_i^2.
void PY8MEs_R12_P8_sm_bb_zzgbb::setExternalMassesMode(int mode) 
{
  if (mode != 0 && mode != 1 && mode != 2)
  {
    cerr <<  "Error in function 'setExternalMassesMode' of class" << 
    " 'PY8MEs_R12_P8_sm_bb_zzgbb': Incorrect mode selected :" << mode << 
    ". It must be either 0, 1 or 2" << endl; 
    throw PY8MEs_R12_P8_sm_bb_zzgbb_exception; 
  }
  if (mode == 0)
  {
    syncProcModelParams(); 
  }
  else if (mode == 1)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      if (mME[j] != pars->ZERO)
      {
        mME[j] = -1.0; 
      }
    }
  }
  else if (mode == 2)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      mME[j] = -1.0; 
    }
  }
}

//--------------------------------------------------------------------------
// Evaluate the squared matrix element.

double PY8MEs_R12_P8_sm_bb_zzgbb::sigmaKin() 
{
  // Set the parameters which change event by event
  pars->setDependentParameters(); 
  pars->setDependentCouplings(); 
  // Reset color flows
  for(int i = 0; i < 4; i++ )
    jamp2[0][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[1][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[2][i] = 0.; 

  // Save previous values of mME
  vector<double> saved_mME(mME.size(), 0.0); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (mME[i] < 0.0)
    {
      saved_mME[i] = mME[i]; 
      mME[i] = sqrt(abs(pow(p[perm[i]][0], 2) - 
      (pow(p[perm[i]][1], 2) + pow(p[perm[i]][2], 2) + pow(p[perm[i]][3],
          2))));
    }
  }

  // Local variables and constants
  const int max_tries = 10; 
  vector < vec_bool > goodhel(nprocesses, vec_bool(ncomb, false)); 
  vec_int ntry(nprocesses, 0); 
  double t = 0.; 
  double result = 0.; 

  if (ntry[proc_ID] <= max_tries)
    ntry[proc_ID] = ntry[proc_ID] + 1; 

  // Find which helicity configuration is asked for
  // -1 indicates one wants to sum over helicities
  int user_ihel = getHelicityIDForConfig(user_helicities); 

  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = getColorIDForConfig(user_colors); 

  // Reset the list of results that will be recomputed here
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      all_results[proc_ID][ihel + 1][icolor + 1] = 0.; 
    }
  }

  // Calculate the matrix element for all helicities
  // unless already detected as vanishing
  for(int ihel = 0; ihel < ncomb; ihel++ )
  {
    // Skip helicity if already detected as vanishing
    if ((ntry[proc_ID] >= max_tries) && !goodhel[proc_ID][ihel])
      continue; 

    // Also skip helicity if user asks for a specific one
    if ((ntry[proc_ID] >= max_tries) && user_ihel != -1 && user_ihel != ihel)
      continue; 

    calculate_wavefunctions(helicities[ihel]); 

    // Reset locally computed color flows
    for(int i = 0; i < 4; i++ )
      jamp2[0][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[1][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[2][i] = 0.; 

    if (proc_ID == 0)
      t = matrix_12_bb_zzgbb(); 
    if (proc_ID == 1)
      t = matrix_12_bbx_zzgbbx(); 
    if (proc_ID == 2)
      t = matrix_12_bxbx_zzgbxbx(); 

    // Store which helicities give non-zero result
    if ((ntry[proc_ID] < max_tries) && t != 0. && !goodhel[proc_ID][ihel])
      goodhel[proc_ID][ihel] = true; 

    // Aggregate results
    if (user_ihel == -1 || user_ihel == ihel)
    {
      if (user_icol == -1)
      {
        result = result + t; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][0] += t; 
          for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
          {
            all_results[proc_ID][0][i + 1] += jamp2[proc_ID][i]; 
          }
        }
        all_results[proc_ID][ihel + 1][0] += t; 
        for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
        {
          all_results[proc_ID][ihel + 1][i + 1] += jamp2[proc_ID][i]; 
        }
      }
      else
      {
        result = result + jamp2[proc_ID][user_icol]; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][user_icol + 1] += jamp2[proc_ID][user_icol]; 
        }
        all_results[proc_ID][ihel + 1][user_icol + 1] +=
            jamp2[proc_ID][user_icol];
      }
    }

  }

  // Normalize results with the identical particle factor
  if (include_symmetry_factors)
  {
    result = result/denom_iden[proc_ID]; 
  }
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      if (include_symmetry_factors)
      {
        all_results[proc_ID][ihel + 1][icolor + 1] /= denom_iden[proc_ID]; 
      }
    }
  }


  // Normalize when when summing+averaging over helicity configurations
  if (user_ihel == -1 && include_helicity_averaging_factors)
  {
    result /= denom_hels[proc_ID]; 
    if (user_icol == -1)
    {
      all_results[proc_ID][0][0] /= denom_hels[proc_ID]; 
      for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
      {
        all_results[proc_ID][0][i + 1] /= denom_hels[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][0][user_icol + 1] /= denom_hels[proc_ID]; 
    }
  }

  // Normalize when summing+averaging over color configurations
  if (user_icol == -1 && include_color_averaging_factors)
  {
    result /= denom_colors[proc_ID]; 
    if (user_ihel == -1)
    {
      all_results[proc_ID][0][0] /= denom_colors[proc_ID]; 
      for (unsigned int i = 0; i < ncomb; i++ )
      {
        all_results[proc_ID][i + 1][0] /= denom_colors[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][user_ihel + 1][0] /= denom_colors[proc_ID]; 
    }
  }

  // Reinstate previous values of mME
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (saved_mME[i] < 0.0)
    {
      mME[i] = saved_mME[i]; 
    }
  }

  // Finally return it
  return result; 
}

//==========================================================================
// Private class member functions

//--------------------------------------------------------------------------
// Evaluate |M|^2 for each subprocess

void PY8MEs_R12_P8_sm_bb_zzgbb::calculate_wavefunctions(const int hel[])
{
  // Calculate wavefunctions for all processes
  // Calculate all wavefunctions
  ixxxxx(p[perm[0]], mME[0], hel[0], +1, w[0]); 
  ixxxxx(p[perm[1]], mME[1], hel[1], +1, w[1]); 
  vxxxxx(p[perm[2]], mME[2], hel[2], +1, w[2]); 
  vxxxxx(p[perm[3]], mME[3], hel[3], +1, w[3]); 
  vxxxxx(p[perm[4]], mME[4], hel[4], +1, w[4]); 
  oxxxxx(p[perm[5]], mME[5], hel[5], +1, w[5]); 
  oxxxxx(p[perm[6]], mME[6], hel[6], +1, w[6]); 
  FFV1_2(w[0], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[7]); 
  VVS1_3(w[2], w[3], pars->GC_81, pars->mdl_MH, pars->mdl_WH, w[8]); 
  FFV1P0_3(w[7], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[9]); 
  FFS4_1(w[6], w[8], pars->GC_83, pars->mdl_MB, pars->ZERO, w[10]); 
  FFS4_2(w[1], w[8], pars->GC_83, pars->mdl_MB, pars->ZERO, w[11]); 
  FFV1P0_3(w[7], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[12]); 
  FFS4_1(w[5], w[8], pars->GC_83, pars->mdl_MB, pars->ZERO, w[13]); 
  FFV1P0_3(w[1], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[14]); 
  FFS4_2(w[7], w[8], pars->GC_83, pars->mdl_MB, pars->ZERO, w[15]); 
  FFV1_2(w[7], w[14], pars->GC_11, pars->mdl_MB, pars->ZERO, w[16]); 
  FFV1P0_3(w[1], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[17]); 
  FFV1_2(w[7], w[17], pars->GC_11, pars->mdl_MB, pars->ZERO, w[18]); 
  FFV2_3_1(w[5], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[19]);
  FFV2_3_2(w[7], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[20]);
  FFV1P0_3(w[1], w[19], pars->GC_11, pars->ZERO, pars->ZERO, w[21]); 
  FFV2_3_1(w[19], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[22]);
  FFV2_3_1(w[6], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[23]);
  FFV1P0_3(w[7], w[19], pars->GC_11, pars->ZERO, pars->ZERO, w[24]); 
  FFV1P0_3(w[7], w[23], pars->GC_11, pars->ZERO, pars->ZERO, w[25]); 
  FFV2_3_2(w[1], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[26]);
  FFV2_3_1(w[6], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[27]);
  FFV1P0_3(w[1], w[27], pars->GC_11, pars->ZERO, pars->ZERO, w[28]); 
  FFV2_3_1(w[27], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[29]);
  FFV2_3_1(w[5], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[30]);
  FFV1P0_3(w[7], w[27], pars->GC_11, pars->ZERO, pars->ZERO, w[31]); 
  FFV1P0_3(w[7], w[30], pars->GC_11, pars->ZERO, pars->ZERO, w[32]); 
  FFV2_3_2(w[1], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[33]);
  FFV1P0_3(w[33], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[34]); 
  FFV1P0_3(w[33], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[35]); 
  FFV2_3_2(w[33], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[36]);
  FFV2_3_2(w[7], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[37]);
  FFV1P0_3(w[1], w[30], pars->GC_11, pars->ZERO, pars->ZERO, w[38]); 
  FFV2_3_1(w[30], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[39]);
  FFV1P0_3(w[1], w[23], pars->GC_11, pars->ZERO, pars->ZERO, w[40]); 
  FFV2_3_1(w[23], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[41]);
  FFV1P0_3(w[26], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[42]); 
  FFV1P0_3(w[26], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[43]); 
  FFV2_3_2(w[26], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[44]);
  FFV1_1(w[6], w[14], pars->GC_11, pars->mdl_MB, pars->ZERO, w[45]); 
  FFV1_1(w[5], w[17], pars->GC_11, pars->mdl_MB, pars->ZERO, w[46]); 
  FFV1_1(w[5], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[47]); 
  FFV2_3_2(w[0], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[48]);
  FFV2_3_1(w[47], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[49]);
  FFV1P0_3(w[48], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[50]); 
  FFV1P0_3(w[1], w[47], pars->GC_11, pars->ZERO, pars->ZERO, w[51]); 
  FFV2_3_2(w[48], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[52]);
  FFV1P0_3(w[48], w[47], pars->GC_11, pars->ZERO, pars->ZERO, w[53]); 
  FFV1P0_3(w[26], w[47], pars->GC_11, pars->ZERO, pars->ZERO, w[54]); 
  FFV1_1(w[47], w[17], pars->GC_11, pars->mdl_MB, pars->ZERO, w[55]); 
  FFV2_3_2(w[0], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[56]);
  FFV2_3_1(w[47], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[57]);
  FFV1P0_3(w[56], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[58]); 
  FFV2_3_2(w[56], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[59]);
  FFV1P0_3(w[56], w[47], pars->GC_11, pars->ZERO, pars->ZERO, w[60]); 
  FFV1P0_3(w[33], w[47], pars->GC_11, pars->ZERO, pars->ZERO, w[61]); 
  FFV1P0_3(w[0], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[62]); 
  FFV1_2(w[1], w[62], pars->GC_11, pars->mdl_MB, pars->ZERO, w[63]); 
  FFV1_1(w[47], w[62], pars->GC_11, pars->mdl_MB, pars->ZERO, w[64]); 
  FFS4_1(w[47], w[8], pars->GC_83, pars->mdl_MB, pars->ZERO, w[65]); 
  FFV1P0_3(w[0], w[47], pars->GC_11, pars->ZERO, pars->ZERO, w[66]); 
  FFS4_2(w[0], w[8], pars->GC_83, pars->mdl_MB, pars->ZERO, w[67]); 
  FFV1P0_3(w[0], w[27], pars->GC_11, pars->ZERO, pars->ZERO, w[68]); 
  FFV1P0_3(w[0], w[23], pars->GC_11, pars->ZERO, pars->ZERO, w[69]); 
  FFV1_2(w[0], w[17], pars->GC_11, pars->mdl_MB, pars->ZERO, w[70]); 
  FFV1_1(w[6], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[71]); 
  FFV2_3_1(w[71], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[72]);
  FFV1P0_3(w[48], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[73]); 
  FFV1P0_3(w[1], w[71], pars->GC_11, pars->ZERO, pars->ZERO, w[74]); 
  FFV1P0_3(w[48], w[71], pars->GC_11, pars->ZERO, pars->ZERO, w[75]); 
  FFV1P0_3(w[26], w[71], pars->GC_11, pars->ZERO, pars->ZERO, w[76]); 
  FFV1_1(w[71], w[14], pars->GC_11, pars->mdl_MB, pars->ZERO, w[77]); 
  FFV2_3_1(w[71], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[78]);
  FFV1P0_3(w[56], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[79]); 
  FFV1P0_3(w[56], w[71], pars->GC_11, pars->ZERO, pars->ZERO, w[80]); 
  FFV1P0_3(w[33], w[71], pars->GC_11, pars->ZERO, pars->ZERO, w[81]); 
  FFV1P0_3(w[0], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[82]); 
  FFV1_2(w[1], w[82], pars->GC_11, pars->mdl_MB, pars->ZERO, w[83]); 
  FFV1_1(w[71], w[82], pars->GC_11, pars->mdl_MB, pars->ZERO, w[84]); 
  FFS4_1(w[71], w[8], pars->GC_83, pars->mdl_MB, pars->ZERO, w[85]); 
  FFV1P0_3(w[0], w[71], pars->GC_11, pars->ZERO, pars->ZERO, w[86]); 
  FFV1P0_3(w[0], w[19], pars->GC_11, pars->ZERO, pars->ZERO, w[87]); 
  FFV1P0_3(w[0], w[30], pars->GC_11, pars->ZERO, pars->ZERO, w[88]); 
  FFV1_2(w[0], w[14], pars->GC_11, pars->mdl_MB, pars->ZERO, w[89]); 
  FFV1_2(w[1], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[90]); 
  FFV2_3_2(w[90], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[91]);
  FFV1P0_3(w[90], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[92]); 
  FFV1P0_3(w[90], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[93]); 
  FFV1P0_3(w[90], w[30], pars->GC_11, pars->ZERO, pars->ZERO, w[94]); 
  FFV1P0_3(w[90], w[23], pars->GC_11, pars->ZERO, pars->ZERO, w[95]); 
  FFV2_3_2(w[90], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[96]);
  FFV1P0_3(w[90], w[19], pars->GC_11, pars->ZERO, pars->ZERO, w[97]); 
  FFV1P0_3(w[90], w[27], pars->GC_11, pars->ZERO, pars->ZERO, w[98]); 
  FFV1_1(w[6], w[82], pars->GC_11, pars->mdl_MB, pars->ZERO, w[99]); 
  FFV1_2(w[90], w[82], pars->GC_11, pars->mdl_MB, pars->ZERO, w[100]); 
  FFS4_2(w[90], w[8], pars->GC_83, pars->mdl_MB, pars->ZERO, w[101]); 
  FFV1_1(w[5], w[62], pars->GC_11, pars->mdl_MB, pars->ZERO, w[102]); 
  FFV1_2(w[90], w[62], pars->GC_11, pars->mdl_MB, pars->ZERO, w[103]); 
  FFV1_2(w[48], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[104]); 
  FFV1_1(w[30], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[105]); 
  VVV1P0_1(w[4], w[17], pars->GC_10, pars->ZERO, pars->ZERO, w[106]); 
  FFV1_1(w[23], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[107]); 
  VVV1P0_1(w[4], w[14], pars->GC_10, pars->ZERO, pars->ZERO, w[108]); 
  FFV1_2(w[26], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[109]); 
  FFV1_2(w[56], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[110]); 
  FFV1_1(w[19], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[111]); 
  FFV1_1(w[27], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[112]); 
  FFV1_2(w[33], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[113]); 
  VVV1P0_1(w[4], w[82], pars->GC_10, pars->ZERO, pars->ZERO, w[114]); 
  VVV1P0_1(w[4], w[62], pars->GC_10, pars->ZERO, pars->ZERO, w[115]); 
  oxxxxx(p[perm[1]], mME[1], hel[1], -1, w[116]); 
  ixxxxx(p[perm[6]], mME[6], hel[6], -1, w[117]); 
  FFV1P0_3(w[7], w[116], pars->GC_11, pars->ZERO, pars->ZERO, w[118]); 
  FFS4_2(w[117], w[8], pars->GC_83, pars->mdl_MB, pars->ZERO, w[119]); 
  FFS4_1(w[116], w[8], pars->GC_83, pars->mdl_MB, pars->ZERO, w[120]); 
  FFV1P0_3(w[117], w[116], pars->GC_11, pars->ZERO, pars->ZERO, w[121]); 
  FFV1_2(w[7], w[121], pars->GC_11, pars->mdl_MB, pars->ZERO, w[122]); 
  FFV1P0_3(w[117], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[123]); 
  FFV1_2(w[7], w[123], pars->GC_11, pars->mdl_MB, pars->ZERO, w[124]); 
  FFV2_3_1(w[116], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[125]);
  FFV1P0_3(w[117], w[125], pars->GC_11, pars->ZERO, pars->ZERO, w[126]); 
  FFV2_3_1(w[125], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[127]);
  FFV1P0_3(w[7], w[125], pars->GC_11, pars->ZERO, pars->ZERO, w[128]); 
  FFV2_3_2(w[117], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[129]);
  FFV1P0_3(w[117], w[19], pars->GC_11, pars->ZERO, pars->ZERO, w[130]); 
  FFV2_3_1(w[116], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[131]);
  FFV1P0_3(w[7], w[131], pars->GC_11, pars->ZERO, pars->ZERO, w[132]); 
  FFV2_3_2(w[117], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[133]);
  FFV1P0_3(w[133], w[116], pars->GC_11, pars->ZERO, pars->ZERO, w[134]); 
  FFV1P0_3(w[133], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[135]); 
  FFV2_3_2(w[133], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[136]);
  FFV1P0_3(w[117], w[131], pars->GC_11, pars->ZERO, pars->ZERO, w[137]); 
  FFV2_3_1(w[131], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[138]);
  FFV1P0_3(w[117], w[30], pars->GC_11, pars->ZERO, pars->ZERO, w[139]); 
  FFV1P0_3(w[129], w[116], pars->GC_11, pars->ZERO, pars->ZERO, w[140]); 
  FFV1P0_3(w[129], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[141]); 
  FFV2_3_2(w[129], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[142]);
  FFV1_1(w[5], w[121], pars->GC_11, pars->mdl_MB, pars->ZERO, w[143]); 
  FFV1_1(w[116], w[123], pars->GC_11, pars->mdl_MB, pars->ZERO, w[144]); 
  FFV1_1(w[116], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[145]); 
  FFV2_3_1(w[145], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[146]);
  FFV1P0_3(w[117], w[145], pars->GC_11, pars->ZERO, pars->ZERO, w[147]); 
  FFV1P0_3(w[48], w[145], pars->GC_11, pars->ZERO, pars->ZERO, w[148]); 
  FFV1P0_3(w[129], w[145], pars->GC_11, pars->ZERO, pars->ZERO, w[149]); 
  FFV1_1(w[145], w[123], pars->GC_11, pars->mdl_MB, pars->ZERO, w[150]); 
  FFV2_3_1(w[145], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[151]);
  FFV1P0_3(w[56], w[145], pars->GC_11, pars->ZERO, pars->ZERO, w[152]); 
  FFV1P0_3(w[133], w[145], pars->GC_11, pars->ZERO, pars->ZERO, w[153]); 
  FFV1_2(w[117], w[82], pars->GC_11, pars->mdl_MB, pars->ZERO, w[154]); 
  FFV1_1(w[145], w[82], pars->GC_11, pars->mdl_MB, pars->ZERO, w[155]); 
  FFS4_1(w[145], w[8], pars->GC_83, pars->mdl_MB, pars->ZERO, w[156]); 
  FFV1P0_3(w[0], w[145], pars->GC_11, pars->ZERO, pars->ZERO, w[157]); 
  FFV1_2(w[0], w[123], pars->GC_11, pars->mdl_MB, pars->ZERO, w[158]); 
  FFV1P0_3(w[48], w[116], pars->GC_11, pars->ZERO, pars->ZERO, w[159]); 
  FFV1P0_3(w[117], w[47], pars->GC_11, pars->ZERO, pars->ZERO, w[160]); 
  FFV1P0_3(w[129], w[47], pars->GC_11, pars->ZERO, pars->ZERO, w[161]); 
  FFV1_1(w[47], w[121], pars->GC_11, pars->mdl_MB, pars->ZERO, w[162]); 
  FFV1P0_3(w[56], w[116], pars->GC_11, pars->ZERO, pars->ZERO, w[163]); 
  FFV1P0_3(w[133], w[47], pars->GC_11, pars->ZERO, pars->ZERO, w[164]); 
  FFV1P0_3(w[0], w[116], pars->GC_11, pars->ZERO, pars->ZERO, w[165]); 
  FFV1_2(w[117], w[165], pars->GC_11, pars->mdl_MB, pars->ZERO, w[166]); 
  FFV1_1(w[47], w[165], pars->GC_11, pars->mdl_MB, pars->ZERO, w[167]); 
  FFV1P0_3(w[0], w[125], pars->GC_11, pars->ZERO, pars->ZERO, w[168]); 
  FFV1P0_3(w[0], w[131], pars->GC_11, pars->ZERO, pars->ZERO, w[169]); 
  FFV1_2(w[0], w[121], pars->GC_11, pars->mdl_MB, pars->ZERO, w[170]); 
  FFV1_2(w[117], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[171]); 
  FFV2_3_2(w[171], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[172]);
  FFV1P0_3(w[171], w[116], pars->GC_11, pars->ZERO, pars->ZERO, w[173]); 
  FFV1P0_3(w[171], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[174]); 
  FFV1P0_3(w[171], w[131], pars->GC_11, pars->ZERO, pars->ZERO, w[175]); 
  FFV1P0_3(w[171], w[30], pars->GC_11, pars->ZERO, pars->ZERO, w[176]); 
  FFV2_3_2(w[171], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[177]);
  FFV1P0_3(w[171], w[125], pars->GC_11, pars->ZERO, pars->ZERO, w[178]); 
  FFV1P0_3(w[171], w[19], pars->GC_11, pars->ZERO, pars->ZERO, w[179]); 
  FFV1_1(w[5], w[165], pars->GC_11, pars->mdl_MB, pars->ZERO, w[180]); 
  FFV1_2(w[171], w[165], pars->GC_11, pars->mdl_MB, pars->ZERO, w[181]); 
  FFS4_2(w[171], w[8], pars->GC_83, pars->mdl_MB, pars->ZERO, w[182]); 
  FFV1_1(w[116], w[82], pars->GC_11, pars->mdl_MB, pars->ZERO, w[183]); 
  FFV1_2(w[171], w[82], pars->GC_11, pars->mdl_MB, pars->ZERO, w[184]); 
  FFV1_1(w[131], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[185]); 
  VVV1P0_1(w[4], w[123], pars->GC_10, pars->ZERO, pars->ZERO, w[186]); 
  VVV1P0_1(w[4], w[121], pars->GC_10, pars->ZERO, pars->ZERO, w[187]); 
  FFV1_2(w[129], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[188]); 
  FFV1_1(w[125], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[189]); 
  FFV1_2(w[133], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[190]); 
  VVV1P0_1(w[4], w[165], pars->GC_10, pars->ZERO, pars->ZERO, w[191]); 
  oxxxxx(p[perm[0]], mME[0], hel[0], -1, w[192]); 
  ixxxxx(p[perm[5]], mME[5], hel[5], -1, w[193]); 
  FFV1_2(w[193], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[194]); 
  FFV1P0_3(w[194], w[192], pars->GC_11, pars->ZERO, pars->ZERO, w[195]); 
  FFV1P0_3(w[194], w[116], pars->GC_11, pars->ZERO, pars->ZERO, w[196]); 
  FFS4_1(w[192], w[8], pars->GC_83, pars->mdl_MB, pars->ZERO, w[197]); 
  FFV1P0_3(w[117], w[192], pars->GC_11, pars->ZERO, pars->ZERO, w[198]); 
  FFS4_2(w[194], w[8], pars->GC_83, pars->mdl_MB, pars->ZERO, w[199]); 
  FFV1_2(w[194], w[198], pars->GC_11, pars->mdl_MB, pars->ZERO, w[200]); 
  FFV1_2(w[194], w[121], pars->GC_11, pars->mdl_MB, pars->ZERO, w[201]); 
  FFV2_3_1(w[192], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[202]);
  FFV2_3_2(w[194], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[203]);
  FFV1P0_3(w[117], w[202], pars->GC_11, pars->ZERO, pars->ZERO, w[204]); 
  FFV2_3_1(w[202], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[205]);
  FFV1P0_3(w[194], w[202], pars->GC_11, pars->ZERO, pars->ZERO, w[206]); 
  FFV1P0_3(w[194], w[131], pars->GC_11, pars->ZERO, pars->ZERO, w[207]); 
  FFV2_3_1(w[192], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[208]);
  FFV1P0_3(w[194], w[125], pars->GC_11, pars->ZERO, pars->ZERO, w[209]); 
  FFV1P0_3(w[194], w[208], pars->GC_11, pars->ZERO, pars->ZERO, w[210]); 
  FFV1P0_3(w[133], w[192], pars->GC_11, pars->ZERO, pars->ZERO, w[211]); 
  FFV2_3_2(w[194], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[212]);
  FFV1P0_3(w[117], w[208], pars->GC_11, pars->ZERO, pars->ZERO, w[213]); 
  FFV2_3_1(w[208], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[214]);
  FFV1P0_3(w[129], w[192], pars->GC_11, pars->ZERO, pars->ZERO, w[215]); 
  FFV1_1(w[116], w[198], pars->GC_11, pars->mdl_MB, pars->ZERO, w[216]); 
  FFV1_1(w[192], w[121], pars->GC_11, pars->mdl_MB, pars->ZERO, w[217]); 
  FFV1_1(w[192], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[218]); 
  FFV2_3_2(w[193], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[219]);
  FFV2_3_1(w[218], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[220]);
  FFV1P0_3(w[219], w[116], pars->GC_11, pars->ZERO, pars->ZERO, w[221]); 
  FFV1P0_3(w[117], w[218], pars->GC_11, pars->ZERO, pars->ZERO, w[222]); 
  FFV2_3_2(w[219], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[223]);
  FFV1P0_3(w[219], w[218], pars->GC_11, pars->ZERO, pars->ZERO, w[224]); 
  FFV1P0_3(w[129], w[218], pars->GC_11, pars->ZERO, pars->ZERO, w[225]); 
  FFV1_1(w[218], w[121], pars->GC_11, pars->mdl_MB, pars->ZERO, w[226]); 
  FFV2_3_2(w[193], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[227]);
  FFV2_3_1(w[218], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[228]);
  FFV1P0_3(w[227], w[116], pars->GC_11, pars->ZERO, pars->ZERO, w[229]); 
  FFV2_3_2(w[227], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[230]);
  FFV1P0_3(w[227], w[218], pars->GC_11, pars->ZERO, pars->ZERO, w[231]); 
  FFV1P0_3(w[133], w[218], pars->GC_11, pars->ZERO, pars->ZERO, w[232]); 
  FFV1P0_3(w[193], w[116], pars->GC_11, pars->ZERO, pars->ZERO, w[233]); 
  FFV1_2(w[117], w[233], pars->GC_11, pars->mdl_MB, pars->ZERO, w[234]); 
  FFV1_1(w[218], w[233], pars->GC_11, pars->mdl_MB, pars->ZERO, w[235]); 
  FFS4_1(w[218], w[8], pars->GC_83, pars->mdl_MB, pars->ZERO, w[236]); 
  FFV1P0_3(w[193], w[218], pars->GC_11, pars->ZERO, pars->ZERO, w[237]); 
  FFS4_2(w[193], w[8], pars->GC_83, pars->mdl_MB, pars->ZERO, w[238]); 
  FFV1P0_3(w[193], w[125], pars->GC_11, pars->ZERO, pars->ZERO, w[239]); 
  FFV1P0_3(w[193], w[131], pars->GC_11, pars->ZERO, pars->ZERO, w[240]); 
  FFV1_2(w[193], w[121], pars->GC_11, pars->mdl_MB, pars->ZERO, w[241]); 
  FFV1P0_3(w[219], w[192], pars->GC_11, pars->ZERO, pars->ZERO, w[242]); 
  FFV1P0_3(w[219], w[145], pars->GC_11, pars->ZERO, pars->ZERO, w[243]); 
  FFV1_1(w[145], w[198], pars->GC_11, pars->mdl_MB, pars->ZERO, w[244]); 
  FFV1P0_3(w[227], w[192], pars->GC_11, pars->ZERO, pars->ZERO, w[245]); 
  FFV1P0_3(w[227], w[145], pars->GC_11, pars->ZERO, pars->ZERO, w[246]); 
  FFV1P0_3(w[193], w[192], pars->GC_11, pars->ZERO, pars->ZERO, w[247]); 
  FFV1_2(w[117], w[247], pars->GC_11, pars->mdl_MB, pars->ZERO, w[248]); 
  FFV1_1(w[145], w[247], pars->GC_11, pars->mdl_MB, pars->ZERO, w[249]); 
  FFV1P0_3(w[193], w[145], pars->GC_11, pars->ZERO, pars->ZERO, w[250]); 
  FFV1P0_3(w[193], w[202], pars->GC_11, pars->ZERO, pars->ZERO, w[251]); 
  FFV1P0_3(w[193], w[208], pars->GC_11, pars->ZERO, pars->ZERO, w[252]); 
  FFV1_2(w[193], w[198], pars->GC_11, pars->mdl_MB, pars->ZERO, w[253]); 
  FFV1P0_3(w[171], w[192], pars->GC_11, pars->ZERO, pars->ZERO, w[254]); 
  FFV1P0_3(w[171], w[208], pars->GC_11, pars->ZERO, pars->ZERO, w[255]); 
  FFV1P0_3(w[171], w[202], pars->GC_11, pars->ZERO, pars->ZERO, w[256]); 
  FFV1_1(w[116], w[247], pars->GC_11, pars->mdl_MB, pars->ZERO, w[257]); 
  FFV1_2(w[171], w[247], pars->GC_11, pars->mdl_MB, pars->ZERO, w[258]); 
  FFV1_1(w[192], w[233], pars->GC_11, pars->mdl_MB, pars->ZERO, w[259]); 
  FFV1_2(w[171], w[233], pars->GC_11, pars->mdl_MB, pars->ZERO, w[260]); 
  FFV1_2(w[219], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[261]); 
  FFV1_1(w[208], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[262]); 
  VVV1P0_1(w[4], w[198], pars->GC_10, pars->ZERO, pars->ZERO, w[263]); 
  FFV1_2(w[227], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[264]); 
  FFV1_1(w[202], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[265]); 
  VVV1P0_1(w[4], w[247], pars->GC_10, pars->ZERO, pars->ZERO, w[266]); 
  VVV1P0_1(w[4], w[233], pars->GC_10, pars->ZERO, pars->ZERO, w[267]); 

  // Calculate all amplitudes
  // Amplitude(s) for diagram number 0
  FFV1_0(w[1], w[10], w[9], pars->GC_11, amp[0]); 
  FFV1_0(w[11], w[6], w[9], pars->GC_11, amp[1]); 
  FFV1_0(w[1], w[13], w[12], pars->GC_11, amp[2]); 
  FFV1_0(w[11], w[5], w[12], pars->GC_11, amp[3]); 
  FFV1_0(w[15], w[6], w[14], pars->GC_11, amp[4]); 
  FFS4_0(w[16], w[6], w[8], pars->GC_83, amp[5]); 
  FFV1_0(w[15], w[5], w[17], pars->GC_11, amp[6]); 
  FFS4_0(w[18], w[5], w[8], pars->GC_83, amp[7]); 
  FFV1_0(w[20], w[6], w[21], pars->GC_11, amp[8]); 
  FFV1_0(w[1], w[22], w[12], pars->GC_11, amp[9]); 
  FFV1_0(w[1], w[23], w[24], pars->GC_11, amp[10]); 
  FFV1_0(w[1], w[19], w[25], pars->GC_11, amp[11]); 
  FFV1_0(w[26], w[6], w[24], pars->GC_11, amp[12]); 
  FFV1_0(w[26], w[19], w[12], pars->GC_11, amp[13]); 
  FFV1_0(w[20], w[19], w[17], pars->GC_11, amp[14]); 
  FFV2_3_0(w[18], w[19], w[3], pars->GC_50, pars->GC_58, amp[15]); 
  FFV1_0(w[20], w[5], w[28], pars->GC_11, amp[16]); 
  FFV1_0(w[1], w[29], w[9], pars->GC_11, amp[17]); 
  FFV1_0(w[1], w[30], w[31], pars->GC_11, amp[18]); 
  FFV1_0(w[1], w[27], w[32], pars->GC_11, amp[19]); 
  FFV1_0(w[26], w[5], w[31], pars->GC_11, amp[20]); 
  FFV1_0(w[26], w[27], w[9], pars->GC_11, amp[21]); 
  FFV1_0(w[20], w[27], w[14], pars->GC_11, amp[22]); 
  FFV2_3_0(w[16], w[27], w[3], pars->GC_50, pars->GC_58, amp[23]); 
  FFV1_0(w[20], w[6], w[34], pars->GC_11, amp[24]); 
  FFV1_0(w[20], w[5], w[35], pars->GC_11, amp[25]); 
  FFV1_0(w[36], w[6], w[9], pars->GC_11, amp[26]); 
  FFV1_0(w[36], w[5], w[12], pars->GC_11, amp[27]); 
  FFV1_0(w[33], w[6], w[32], pars->GC_11, amp[28]); 
  FFV1_0(w[33], w[30], w[12], pars->GC_11, amp[29]); 
  FFV1_0(w[33], w[5], w[25], pars->GC_11, amp[30]); 
  FFV1_0(w[33], w[23], w[9], pars->GC_11, amp[31]); 
  FFV1_0(w[37], w[6], w[38], pars->GC_11, amp[32]); 
  FFV1_0(w[1], w[39], w[12], pars->GC_11, amp[33]); 
  FFV1_0(w[37], w[30], w[17], pars->GC_11, amp[34]); 
  FFV2_3_0(w[18], w[30], w[2], pars->GC_50, pars->GC_58, amp[35]); 
  FFV1_0(w[37], w[5], w[40], pars->GC_11, amp[36]); 
  FFV1_0(w[1], w[41], w[9], pars->GC_11, amp[37]); 
  FFV1_0(w[37], w[23], w[14], pars->GC_11, amp[38]); 
  FFV2_3_0(w[16], w[23], w[2], pars->GC_50, pars->GC_58, amp[39]); 
  FFV1_0(w[37], w[6], w[42], pars->GC_11, amp[40]); 
  FFV1_0(w[37], w[5], w[43], pars->GC_11, amp[41]); 
  FFV1_0(w[44], w[6], w[9], pars->GC_11, amp[42]); 
  FFV1_0(w[44], w[5], w[12], pars->GC_11, amp[43]); 
  FFV2_3_0(w[37], w[45], w[3], pars->GC_50, pars->GC_58, amp[44]); 
  FFV2_3_0(w[20], w[45], w[2], pars->GC_50, pars->GC_58, amp[45]); 
  FFV2_3_0(w[37], w[46], w[3], pars->GC_50, pars->GC_58, amp[46]); 
  FFV2_3_0(w[20], w[46], w[2], pars->GC_50, pars->GC_58, amp[47]); 
  FFV1_0(w[1], w[49], w[50], pars->GC_11, amp[48]); 
  FFV1_0(w[52], w[6], w[51], pars->GC_11, amp[49]); 
  FFV1_0(w[1], w[23], w[53], pars->GC_11, amp[50]); 
  FFV1_0(w[48], w[23], w[51], pars->GC_11, amp[51]); 
  FFV1_0(w[26], w[6], w[53], pars->GC_11, amp[52]); 
  FFV1_0(w[48], w[6], w[54], pars->GC_11, amp[53]); 
  FFV1_0(w[48], w[49], w[17], pars->GC_11, amp[54]); 
  FFV2_3_0(w[48], w[55], w[3], pars->GC_50, pars->GC_58, amp[55]); 
  FFV1_0(w[1], w[57], w[58], pars->GC_11, amp[56]); 
  FFV1_0(w[59], w[6], w[51], pars->GC_11, amp[57]); 
  FFV1_0(w[1], w[27], w[60], pars->GC_11, amp[58]); 
  FFV1_0(w[56], w[27], w[51], pars->GC_11, amp[59]); 
  FFV1_0(w[33], w[6], w[60], pars->GC_11, amp[60]); 
  FFV1_0(w[56], w[6], w[61], pars->GC_11, amp[61]); 
  FFV1_0(w[56], w[57], w[17], pars->GC_11, amp[62]); 
  FFV2_3_0(w[56], w[55], w[2], pars->GC_50, pars->GC_58, amp[63]); 
  FFV2_3_0(w[63], w[57], w[3], pars->GC_50, pars->GC_58, amp[64]); 
  FFV2_3_0(w[63], w[49], w[2], pars->GC_50, pars->GC_58, amp[65]); 
  FFS4_0(w[1], w[64], w[8], pars->GC_83, amp[66]); 
  FFV1_0(w[1], w[65], w[62], pars->GC_11, amp[67]); 
  FFV2_3_0(w[33], w[64], w[3], pars->GC_50, pars->GC_58, amp[68]); 
  FFV1_0(w[33], w[49], w[62], pars->GC_11, amp[69]); 
  FFV2_3_0(w[26], w[64], w[2], pars->GC_50, pars->GC_58, amp[70]); 
  FFV1_0(w[26], w[57], w[62], pars->GC_11, amp[71]); 
  FFV1_0(w[1], w[10], w[66], pars->GC_11, amp[72]); 
  FFV1_0(w[11], w[6], w[66], pars->GC_11, amp[73]); 
  FFV1_0(w[67], w[6], w[51], pars->GC_11, amp[74]); 
  FFV1_0(w[0], w[10], w[51], pars->GC_11, amp[75]); 
  FFV1_0(w[0], w[65], w[17], pars->GC_11, amp[76]); 
  FFS4_0(w[0], w[55], w[8], pars->GC_83, amp[77]); 
  FFV1_0(w[1], w[29], w[66], pars->GC_11, amp[78]); 
  FFV1_0(w[1], w[49], w[68], pars->GC_11, amp[79]); 
  FFV1_0(w[0], w[49], w[28], pars->GC_11, amp[80]); 
  FFV1_0(w[0], w[29], w[51], pars->GC_11, amp[81]); 
  FFV1_0(w[26], w[27], w[66], pars->GC_11, amp[82]); 
  FFV1_0(w[0], w[27], w[54], pars->GC_11, amp[83]); 
  FFV1_0(w[36], w[6], w[66], pars->GC_11, amp[84]); 
  FFV1_0(w[0], w[49], w[35], pars->GC_11, amp[85]); 
  FFV1_0(w[33], w[23], w[66], pars->GC_11, amp[86]); 
  FFV1_0(w[0], w[23], w[61], pars->GC_11, amp[87]); 
  FFV1_0(w[1], w[41], w[66], pars->GC_11, amp[88]); 
  FFV1_0(w[1], w[57], w[69], pars->GC_11, amp[89]); 
  FFV1_0(w[0], w[57], w[40], pars->GC_11, amp[90]); 
  FFV1_0(w[0], w[41], w[51], pars->GC_11, amp[91]); 
  FFV1_0(w[44], w[6], w[66], pars->GC_11, amp[92]); 
  FFV1_0(w[0], w[57], w[43], pars->GC_11, amp[93]); 
  FFV2_3_0(w[70], w[57], w[3], pars->GC_50, pars->GC_58, amp[94]); 
  FFV2_3_0(w[70], w[49], w[2], pars->GC_50, pars->GC_58, amp[95]); 
  FFV1_0(w[1], w[72], w[73], pars->GC_11, amp[96]); 
  FFV1_0(w[52], w[5], w[74], pars->GC_11, amp[97]); 
  FFV1_0(w[1], w[30], w[75], pars->GC_11, amp[98]); 
  FFV1_0(w[48], w[30], w[74], pars->GC_11, amp[99]); 
  FFV1_0(w[26], w[5], w[75], pars->GC_11, amp[100]); 
  FFV1_0(w[48], w[5], w[76], pars->GC_11, amp[101]); 
  FFV1_0(w[48], w[72], w[14], pars->GC_11, amp[102]); 
  FFV2_3_0(w[48], w[77], w[3], pars->GC_50, pars->GC_58, amp[103]); 
  FFV1_0(w[1], w[78], w[79], pars->GC_11, amp[104]); 
  FFV1_0(w[59], w[5], w[74], pars->GC_11, amp[105]); 
  FFV1_0(w[1], w[19], w[80], pars->GC_11, amp[106]); 
  FFV1_0(w[56], w[19], w[74], pars->GC_11, amp[107]); 
  FFV1_0(w[33], w[5], w[80], pars->GC_11, amp[108]); 
  FFV1_0(w[56], w[5], w[81], pars->GC_11, amp[109]); 
  FFV1_0(w[56], w[78], w[14], pars->GC_11, amp[110]); 
  FFV2_3_0(w[56], w[77], w[2], pars->GC_50, pars->GC_58, amp[111]); 
  FFV2_3_0(w[83], w[78], w[3], pars->GC_50, pars->GC_58, amp[112]); 
  FFV2_3_0(w[83], w[72], w[2], pars->GC_50, pars->GC_58, amp[113]); 
  FFS4_0(w[1], w[84], w[8], pars->GC_83, amp[114]); 
  FFV1_0(w[1], w[85], w[82], pars->GC_11, amp[115]); 
  FFV2_3_0(w[33], w[84], w[3], pars->GC_50, pars->GC_58, amp[116]); 
  FFV1_0(w[33], w[72], w[82], pars->GC_11, amp[117]); 
  FFV2_3_0(w[26], w[84], w[2], pars->GC_50, pars->GC_58, amp[118]); 
  FFV1_0(w[26], w[78], w[82], pars->GC_11, amp[119]); 
  FFV1_0(w[1], w[13], w[86], pars->GC_11, amp[120]); 
  FFV1_0(w[11], w[5], w[86], pars->GC_11, amp[121]); 
  FFV1_0(w[67], w[5], w[74], pars->GC_11, amp[122]); 
  FFV1_0(w[0], w[13], w[74], pars->GC_11, amp[123]); 
  FFV1_0(w[0], w[85], w[14], pars->GC_11, amp[124]); 
  FFS4_0(w[0], w[77], w[8], pars->GC_83, amp[125]); 
  FFV1_0(w[1], w[22], w[86], pars->GC_11, amp[126]); 
  FFV1_0(w[1], w[72], w[87], pars->GC_11, amp[127]); 
  FFV1_0(w[0], w[72], w[21], pars->GC_11, amp[128]); 
  FFV1_0(w[0], w[22], w[74], pars->GC_11, amp[129]); 
  FFV1_0(w[26], w[19], w[86], pars->GC_11, amp[130]); 
  FFV1_0(w[0], w[19], w[76], pars->GC_11, amp[131]); 
  FFV1_0(w[36], w[5], w[86], pars->GC_11, amp[132]); 
  FFV1_0(w[0], w[72], w[34], pars->GC_11, amp[133]); 
  FFV1_0(w[33], w[30], w[86], pars->GC_11, amp[134]); 
  FFV1_0(w[0], w[30], w[81], pars->GC_11, amp[135]); 
  FFV1_0(w[1], w[39], w[86], pars->GC_11, amp[136]); 
  FFV1_0(w[1], w[78], w[88], pars->GC_11, amp[137]); 
  FFV1_0(w[0], w[78], w[38], pars->GC_11, amp[138]); 
  FFV1_0(w[0], w[39], w[74], pars->GC_11, amp[139]); 
  FFV1_0(w[44], w[5], w[86], pars->GC_11, amp[140]); 
  FFV1_0(w[0], w[78], w[42], pars->GC_11, amp[141]); 
  FFV2_3_0(w[89], w[78], w[3], pars->GC_50, pars->GC_58, amp[142]); 
  FFV2_3_0(w[89], w[72], w[2], pars->GC_50, pars->GC_58, amp[143]); 
  FFV1_0(w[91], w[6], w[73], pars->GC_11, amp[144]); 
  FFV1_0(w[91], w[5], w[50], pars->GC_11, amp[145]); 
  FFV1_0(w[52], w[6], w[92], pars->GC_11, amp[146]); 
  FFV1_0(w[52], w[5], w[93], pars->GC_11, amp[147]); 
  FFV1_0(w[48], w[6], w[94], pars->GC_11, amp[148]); 
  FFV1_0(w[48], w[30], w[93], pars->GC_11, amp[149]); 
  FFV1_0(w[48], w[5], w[95], pars->GC_11, amp[150]); 
  FFV1_0(w[48], w[23], w[92], pars->GC_11, amp[151]); 
  FFV1_0(w[96], w[6], w[79], pars->GC_11, amp[152]); 
  FFV1_0(w[96], w[5], w[58], pars->GC_11, amp[153]); 
  FFV1_0(w[59], w[6], w[92], pars->GC_11, amp[154]); 
  FFV1_0(w[59], w[5], w[93], pars->GC_11, amp[155]); 
  FFV1_0(w[56], w[6], w[97], pars->GC_11, amp[156]); 
  FFV1_0(w[56], w[19], w[93], pars->GC_11, amp[157]); 
  FFV1_0(w[56], w[5], w[98], pars->GC_11, amp[158]); 
  FFV1_0(w[56], w[27], w[92], pars->GC_11, amp[159]); 
  FFV2_3_0(w[96], w[99], w[3], pars->GC_50, pars->GC_58, amp[160]); 
  FFV2_3_0(w[91], w[99], w[2], pars->GC_50, pars->GC_58, amp[161]); 
  FFS4_0(w[100], w[6], w[8], pars->GC_83, amp[162]); 
  FFV1_0(w[101], w[6], w[82], pars->GC_11, amp[163]); 
  FFV2_3_0(w[100], w[27], w[3], pars->GC_50, pars->GC_58, amp[164]); 
  FFV1_0(w[91], w[27], w[82], pars->GC_11, amp[165]); 
  FFV2_3_0(w[100], w[23], w[2], pars->GC_50, pars->GC_58, amp[166]); 
  FFV1_0(w[96], w[23], w[82], pars->GC_11, amp[167]); 
  FFV2_3_0(w[96], w[102], w[3], pars->GC_50, pars->GC_58, amp[168]); 
  FFV2_3_0(w[91], w[102], w[2], pars->GC_50, pars->GC_58, amp[169]); 
  FFS4_0(w[103], w[5], w[8], pars->GC_83, amp[170]); 
  FFV1_0(w[101], w[5], w[62], pars->GC_11, amp[171]); 
  FFV2_3_0(w[103], w[19], w[3], pars->GC_50, pars->GC_58, amp[172]); 
  FFV1_0(w[91], w[19], w[62], pars->GC_11, amp[173]); 
  FFV2_3_0(w[103], w[30], w[2], pars->GC_50, pars->GC_58, amp[174]); 
  FFV1_0(w[96], w[30], w[62], pars->GC_11, amp[175]); 
  FFV1_0(w[67], w[6], w[92], pars->GC_11, amp[176]); 
  FFV1_0(w[0], w[10], w[92], pars->GC_11, amp[177]); 
  FFV1_0(w[67], w[5], w[93], pars->GC_11, amp[178]); 
  FFV1_0(w[0], w[13], w[93], pars->GC_11, amp[179]); 
  FFV1_0(w[91], w[6], w[87], pars->GC_11, amp[180]); 
  FFV1_0(w[0], w[22], w[93], pars->GC_11, amp[181]); 
  FFV1_0(w[0], w[23], w[97], pars->GC_11, amp[182]); 
  FFV1_0(w[0], w[19], w[95], pars->GC_11, amp[183]); 
  FFV1_0(w[91], w[5], w[68], pars->GC_11, amp[184]); 
  FFV1_0(w[0], w[29], w[92], pars->GC_11, amp[185]); 
  FFV1_0(w[0], w[30], w[98], pars->GC_11, amp[186]); 
  FFV1_0(w[0], w[27], w[94], pars->GC_11, amp[187]); 
  FFV1_0(w[96], w[6], w[88], pars->GC_11, amp[188]); 
  FFV1_0(w[0], w[39], w[93], pars->GC_11, amp[189]); 
  FFV1_0(w[96], w[5], w[69], pars->GC_11, amp[190]); 
  FFV1_0(w[0], w[41], w[92], pars->GC_11, amp[191]); 
  FFV1_0(w[104], w[6], w[38], pars->GC_11, amp[192]); 
  FFV1_0(w[1], w[105], w[50], pars->GC_11, amp[193]); 
  VVV1_0(w[4], w[50], w[38], pars->GC_10, amp[194]); 
  FFV1_0(w[104], w[30], w[17], pars->GC_11, amp[195]); 
  FFV1_0(w[48], w[105], w[17], pars->GC_11, amp[196]); 
  FFV1_0(w[48], w[30], w[106], pars->GC_11, amp[197]); 
  FFV1_0(w[104], w[5], w[40], pars->GC_11, amp[198]); 
  FFV1_0(w[1], w[107], w[73], pars->GC_11, amp[199]); 
  VVV1_0(w[4], w[73], w[40], pars->GC_10, amp[200]); 
  FFV1_0(w[104], w[23], w[14], pars->GC_11, amp[201]); 
  FFV1_0(w[48], w[107], w[14], pars->GC_11, amp[202]); 
  FFV1_0(w[48], w[23], w[108], pars->GC_11, amp[203]); 
  FFV1_0(w[104], w[6], w[42], pars->GC_11, amp[204]); 
  FFV1_0(w[104], w[5], w[43], pars->GC_11, amp[205]); 
  FFV1_0(w[109], w[6], w[73], pars->GC_11, amp[206]); 
  FFV1_0(w[109], w[5], w[50], pars->GC_11, amp[207]); 
  VVV1_0(w[4], w[73], w[43], pars->GC_10, amp[208]); 
  VVV1_0(w[4], w[50], w[42], pars->GC_10, amp[209]); 
  FFV2_3_0(w[104], w[45], w[3], pars->GC_50, pars->GC_58, amp[210]); 
  FFV1_0(w[52], w[6], w[108], pars->GC_11, amp[211]); 
  FFV1_0(w[52], w[45], w[4], pars->GC_11, amp[212]); 
  FFV2_3_0(w[104], w[46], w[3], pars->GC_50, pars->GC_58, amp[213]); 
  FFV1_0(w[52], w[5], w[106], pars->GC_11, amp[214]); 
  FFV1_0(w[52], w[46], w[4], pars->GC_11, amp[215]); 
  FFV1_0(w[110], w[6], w[21], pars->GC_11, amp[216]); 
  FFV1_0(w[1], w[111], w[58], pars->GC_11, amp[217]); 
  VVV1_0(w[4], w[58], w[21], pars->GC_10, amp[218]); 
  FFV1_0(w[110], w[19], w[17], pars->GC_11, amp[219]); 
  FFV1_0(w[56], w[111], w[17], pars->GC_11, amp[220]); 
  FFV1_0(w[56], w[19], w[106], pars->GC_11, amp[221]); 
  FFV1_0(w[110], w[5], w[28], pars->GC_11, amp[222]); 
  FFV1_0(w[1], w[112], w[79], pars->GC_11, amp[223]); 
  VVV1_0(w[4], w[79], w[28], pars->GC_10, amp[224]); 
  FFV1_0(w[110], w[27], w[14], pars->GC_11, amp[225]); 
  FFV1_0(w[56], w[112], w[14], pars->GC_11, amp[226]); 
  FFV1_0(w[56], w[27], w[108], pars->GC_11, amp[227]); 
  FFV1_0(w[110], w[6], w[34], pars->GC_11, amp[228]); 
  FFV1_0(w[110], w[5], w[35], pars->GC_11, amp[229]); 
  FFV1_0(w[113], w[6], w[79], pars->GC_11, amp[230]); 
  FFV1_0(w[113], w[5], w[58], pars->GC_11, amp[231]); 
  VVV1_0(w[4], w[79], w[35], pars->GC_10, amp[232]); 
  VVV1_0(w[4], w[58], w[34], pars->GC_10, amp[233]); 
  FFV2_3_0(w[110], w[45], w[2], pars->GC_50, pars->GC_58, amp[234]); 
  FFV1_0(w[59], w[6], w[108], pars->GC_11, amp[235]); 
  FFV1_0(w[59], w[45], w[4], pars->GC_11, amp[236]); 
  FFV2_3_0(w[110], w[46], w[2], pars->GC_50, pars->GC_58, amp[237]); 
  FFV1_0(w[59], w[5], w[106], pars->GC_11, amp[238]); 
  FFV1_0(w[59], w[46], w[4], pars->GC_11, amp[239]); 
  FFV1_0(w[1], w[10], w[114], pars->GC_11, amp[240]); 
  FFV1_0(w[11], w[6], w[114], pars->GC_11, amp[241]); 
  FFV1_0(w[11], w[99], w[4], pars->GC_11, amp[242]); 
  FFV1_0(w[83], w[10], w[4], pars->GC_11, amp[243]); 
  FFV1_0(w[1], w[29], w[114], pars->GC_11, amp[244]); 
  FFV2_3_0(w[83], w[112], w[3], pars->GC_50, pars->GC_58, amp[245]); 
  FFV1_0(w[83], w[29], w[4], pars->GC_11, amp[246]); 
  FFV1_0(w[26], w[27], w[114], pars->GC_11, amp[247]); 
  FFV1_0(w[26], w[112], w[82], pars->GC_11, amp[248]); 
  FFV1_0(w[109], w[27], w[82], pars->GC_11, amp[249]); 
  FFV1_0(w[36], w[6], w[114], pars->GC_11, amp[250]); 
  FFV2_3_0(w[113], w[99], w[3], pars->GC_50, pars->GC_58, amp[251]); 
  FFV1_0(w[36], w[99], w[4], pars->GC_11, amp[252]); 
  FFV1_0(w[33], w[23], w[114], pars->GC_11, amp[253]); 
  FFV1_0(w[113], w[23], w[82], pars->GC_11, amp[254]); 
  FFV1_0(w[33], w[107], w[82], pars->GC_11, amp[255]); 
  FFV1_0(w[1], w[41], w[114], pars->GC_11, amp[256]); 
  FFV2_3_0(w[83], w[107], w[2], pars->GC_50, pars->GC_58, amp[257]); 
  FFV1_0(w[83], w[41], w[4], pars->GC_11, amp[258]); 
  FFV1_0(w[44], w[6], w[114], pars->GC_11, amp[259]); 
  FFV2_3_0(w[109], w[99], w[2], pars->GC_50, pars->GC_58, amp[260]); 
  FFV1_0(w[44], w[99], w[4], pars->GC_11, amp[261]); 
  FFV1_0(w[1], w[13], w[115], pars->GC_11, amp[262]); 
  FFV1_0(w[11], w[5], w[115], pars->GC_11, amp[263]); 
  FFV1_0(w[11], w[102], w[4], pars->GC_11, amp[264]); 
  FFV1_0(w[63], w[13], w[4], pars->GC_11, amp[265]); 
  FFV1_0(w[1], w[22], w[115], pars->GC_11, amp[266]); 
  FFV2_3_0(w[63], w[111], w[3], pars->GC_50, pars->GC_58, amp[267]); 
  FFV1_0(w[63], w[22], w[4], pars->GC_11, amp[268]); 
  FFV1_0(w[26], w[19], w[115], pars->GC_11, amp[269]); 
  FFV1_0(w[26], w[111], w[62], pars->GC_11, amp[270]); 
  FFV1_0(w[109], w[19], w[62], pars->GC_11, amp[271]); 
  FFV1_0(w[36], w[5], w[115], pars->GC_11, amp[272]); 
  FFV2_3_0(w[113], w[102], w[3], pars->GC_50, pars->GC_58, amp[273]); 
  FFV1_0(w[36], w[102], w[4], pars->GC_11, amp[274]); 
  FFV1_0(w[33], w[30], w[115], pars->GC_11, amp[275]); 
  FFV1_0(w[113], w[30], w[62], pars->GC_11, amp[276]); 
  FFV1_0(w[33], w[105], w[62], pars->GC_11, amp[277]); 
  FFV1_0(w[1], w[39], w[115], pars->GC_11, amp[278]); 
  FFV2_3_0(w[63], w[105], w[2], pars->GC_50, pars->GC_58, amp[279]); 
  FFV1_0(w[63], w[39], w[4], pars->GC_11, amp[280]); 
  FFV1_0(w[44], w[5], w[115], pars->GC_11, amp[281]); 
  FFV2_3_0(w[109], w[102], w[2], pars->GC_50, pars->GC_58, amp[282]); 
  FFV1_0(w[44], w[102], w[4], pars->GC_11, amp[283]); 
  FFV1_0(w[67], w[6], w[108], pars->GC_11, amp[284]); 
  FFV1_0(w[0], w[10], w[108], pars->GC_11, amp[285]); 
  FFV1_0(w[67], w[45], w[4], pars->GC_11, amp[286]); 
  FFV1_0(w[89], w[10], w[4], pars->GC_11, amp[287]); 
  FFV1_0(w[67], w[5], w[106], pars->GC_11, amp[288]); 
  FFV1_0(w[0], w[13], w[106], pars->GC_11, amp[289]); 
  FFV1_0(w[67], w[46], w[4], pars->GC_11, amp[290]); 
  FFV1_0(w[70], w[13], w[4], pars->GC_11, amp[291]); 
  FFV1_0(w[1], w[111], w[69], pars->GC_11, amp[292]); 
  FFV1_0(w[0], w[111], w[40], pars->GC_11, amp[293]); 
  FFV1_0(w[1], w[107], w[87], pars->GC_11, amp[294]); 
  FFV1_0(w[0], w[107], w[21], pars->GC_11, amp[295]); 
  VVV1_0(w[4], w[87], w[40], pars->GC_10, amp[296]); 
  VVV1_0(w[4], w[69], w[21], pars->GC_10, amp[297]); 
  FFV1_0(w[0], w[111], w[43], pars->GC_11, amp[298]); 
  FFV1_0(w[109], w[6], w[87], pars->GC_11, amp[299]); 
  VVV1_0(w[4], w[87], w[43], pars->GC_10, amp[300]); 
  FFV2_3_0(w[70], w[111], w[3], pars->GC_50, pars->GC_58, amp[301]); 
  FFV1_0(w[0], w[22], w[106], pars->GC_11, amp[302]); 
  FFV1_0(w[70], w[22], w[4], pars->GC_11, amp[303]); 
  FFV1_0(w[1], w[112], w[88], pars->GC_11, amp[304]); 
  FFV1_0(w[0], w[112], w[38], pars->GC_11, amp[305]); 
  FFV1_0(w[1], w[105], w[68], pars->GC_11, amp[306]); 
  FFV1_0(w[0], w[105], w[28], pars->GC_11, amp[307]); 
  VVV1_0(w[4], w[68], w[38], pars->GC_10, amp[308]); 
  VVV1_0(w[4], w[88], w[28], pars->GC_10, amp[309]); 
  FFV1_0(w[0], w[112], w[42], pars->GC_11, amp[310]); 
  FFV1_0(w[109], w[5], w[68], pars->GC_11, amp[311]); 
  VVV1_0(w[4], w[68], w[42], pars->GC_10, amp[312]); 
  FFV2_3_0(w[89], w[112], w[3], pars->GC_50, pars->GC_58, amp[313]); 
  FFV1_0(w[0], w[29], w[108], pars->GC_11, amp[314]); 
  FFV1_0(w[89], w[29], w[4], pars->GC_11, amp[315]); 
  FFV1_0(w[113], w[6], w[88], pars->GC_11, amp[316]); 
  FFV1_0(w[0], w[105], w[35], pars->GC_11, amp[317]); 
  VVV1_0(w[4], w[88], w[35], pars->GC_10, amp[318]); 
  FFV1_0(w[113], w[5], w[69], pars->GC_11, amp[319]); 
  FFV1_0(w[0], w[107], w[34], pars->GC_11, amp[320]); 
  VVV1_0(w[4], w[69], w[34], pars->GC_10, amp[321]); 
  FFV2_3_0(w[70], w[105], w[2], pars->GC_50, pars->GC_58, amp[322]); 
  FFV1_0(w[0], w[39], w[106], pars->GC_11, amp[323]); 
  FFV1_0(w[70], w[39], w[4], pars->GC_11, amp[324]); 
  FFV2_3_0(w[89], w[107], w[2], pars->GC_50, pars->GC_58, amp[325]); 
  FFV1_0(w[0], w[41], w[108], pars->GC_11, amp[326]); 
  FFV1_0(w[89], w[41], w[4], pars->GC_11, amp[327]); 
  FFV1_0(w[117], w[13], w[118], pars->GC_11, amp[328]); 
  FFV1_0(w[119], w[5], w[118], pars->GC_11, amp[329]); 
  FFV1_0(w[117], w[120], w[9], pars->GC_11, amp[330]); 
  FFV1_0(w[119], w[116], w[9], pars->GC_11, amp[331]); 
  FFV1_0(w[15], w[5], w[121], pars->GC_11, amp[332]); 
  FFS4_0(w[122], w[5], w[8], pars->GC_83, amp[333]); 
  FFV1_0(w[15], w[116], w[123], pars->GC_11, amp[334]); 
  FFS4_0(w[124], w[116], w[8], pars->GC_83, amp[335]); 
  FFV1_0(w[20], w[5], w[126], pars->GC_11, amp[336]); 
  FFV1_0(w[117], w[127], w[9], pars->GC_11, amp[337]); 
  FFV1_0(w[117], w[30], w[128], pars->GC_11, amp[338]); 
  FFV1_0(w[117], w[125], w[32], pars->GC_11, amp[339]); 
  FFV1_0(w[129], w[5], w[128], pars->GC_11, amp[340]); 
  FFV1_0(w[129], w[125], w[9], pars->GC_11, amp[341]); 
  FFV1_0(w[20], w[125], w[123], pars->GC_11, amp[342]); 
  FFV2_3_0(w[124], w[125], w[3], pars->GC_50, pars->GC_58, amp[343]); 
  FFV1_0(w[20], w[116], w[130], pars->GC_11, amp[344]); 
  FFV1_0(w[117], w[22], w[118], pars->GC_11, amp[345]); 
  FFV1_0(w[117], w[131], w[24], pars->GC_11, amp[346]); 
  FFV1_0(w[117], w[19], w[132], pars->GC_11, amp[347]); 
  FFV1_0(w[129], w[116], w[24], pars->GC_11, amp[348]); 
  FFV1_0(w[129], w[19], w[118], pars->GC_11, amp[349]); 
  FFV1_0(w[20], w[19], w[121], pars->GC_11, amp[350]); 
  FFV2_3_0(w[122], w[19], w[3], pars->GC_50, pars->GC_58, amp[351]); 
  FFV1_0(w[20], w[5], w[134], pars->GC_11, amp[352]); 
  FFV1_0(w[20], w[116], w[135], pars->GC_11, amp[353]); 
  FFV1_0(w[136], w[5], w[118], pars->GC_11, amp[354]); 
  FFV1_0(w[136], w[116], w[9], pars->GC_11, amp[355]); 
  FFV1_0(w[133], w[5], w[132], pars->GC_11, amp[356]); 
  FFV1_0(w[133], w[131], w[9], pars->GC_11, amp[357]); 
  FFV1_0(w[133], w[116], w[32], pars->GC_11, amp[358]); 
  FFV1_0(w[133], w[30], w[118], pars->GC_11, amp[359]); 
  FFV1_0(w[37], w[5], w[137], pars->GC_11, amp[360]); 
  FFV1_0(w[117], w[138], w[9], pars->GC_11, amp[361]); 
  FFV1_0(w[37], w[131], w[123], pars->GC_11, amp[362]); 
  FFV2_3_0(w[124], w[131], w[2], pars->GC_50, pars->GC_58, amp[363]); 
  FFV1_0(w[37], w[116], w[139], pars->GC_11, amp[364]); 
  FFV1_0(w[117], w[39], w[118], pars->GC_11, amp[365]); 
  FFV1_0(w[37], w[30], w[121], pars->GC_11, amp[366]); 
  FFV2_3_0(w[122], w[30], w[2], pars->GC_50, pars->GC_58, amp[367]); 
  FFV1_0(w[37], w[5], w[140], pars->GC_11, amp[368]); 
  FFV1_0(w[37], w[116], w[141], pars->GC_11, amp[369]); 
  FFV1_0(w[142], w[5], w[118], pars->GC_11, amp[370]); 
  FFV1_0(w[142], w[116], w[9], pars->GC_11, amp[371]); 
  FFV2_3_0(w[37], w[143], w[3], pars->GC_50, pars->GC_58, amp[372]); 
  FFV2_3_0(w[20], w[143], w[2], pars->GC_50, pars->GC_58, amp[373]); 
  FFV2_3_0(w[37], w[144], w[3], pars->GC_50, pars->GC_58, amp[374]); 
  FFV2_3_0(w[20], w[144], w[2], pars->GC_50, pars->GC_58, amp[375]); 
  FFV1_0(w[117], w[146], w[73], pars->GC_11, amp[376]); 
  FFV1_0(w[52], w[5], w[147], pars->GC_11, amp[377]); 
  FFV1_0(w[117], w[30], w[148], pars->GC_11, amp[378]); 
  FFV1_0(w[48], w[30], w[147], pars->GC_11, amp[379]); 
  FFV1_0(w[129], w[5], w[148], pars->GC_11, amp[380]); 
  FFV1_0(w[48], w[5], w[149], pars->GC_11, amp[381]); 
  FFV1_0(w[48], w[146], w[123], pars->GC_11, amp[382]); 
  FFV2_3_0(w[48], w[150], w[3], pars->GC_50, pars->GC_58, amp[383]); 
  FFV1_0(w[117], w[151], w[79], pars->GC_11, amp[384]); 
  FFV1_0(w[59], w[5], w[147], pars->GC_11, amp[385]); 
  FFV1_0(w[117], w[19], w[152], pars->GC_11, amp[386]); 
  FFV1_0(w[56], w[19], w[147], pars->GC_11, amp[387]); 
  FFV1_0(w[133], w[5], w[152], pars->GC_11, amp[388]); 
  FFV1_0(w[56], w[5], w[153], pars->GC_11, amp[389]); 
  FFV1_0(w[56], w[151], w[123], pars->GC_11, amp[390]); 
  FFV2_3_0(w[56], w[150], w[2], pars->GC_50, pars->GC_58, amp[391]); 
  FFV2_3_0(w[154], w[151], w[3], pars->GC_50, pars->GC_58, amp[392]); 
  FFV2_3_0(w[154], w[146], w[2], pars->GC_50, pars->GC_58, amp[393]); 
  FFS4_0(w[117], w[155], w[8], pars->GC_83, amp[394]); 
  FFV1_0(w[117], w[156], w[82], pars->GC_11, amp[395]); 
  FFV2_3_0(w[133], w[155], w[3], pars->GC_50, pars->GC_58, amp[396]); 
  FFV1_0(w[133], w[146], w[82], pars->GC_11, amp[397]); 
  FFV2_3_0(w[129], w[155], w[2], pars->GC_50, pars->GC_58, amp[398]); 
  FFV1_0(w[129], w[151], w[82], pars->GC_11, amp[399]); 
  FFV1_0(w[117], w[13], w[157], pars->GC_11, amp[400]); 
  FFV1_0(w[119], w[5], w[157], pars->GC_11, amp[401]); 
  FFV1_0(w[67], w[5], w[147], pars->GC_11, amp[402]); 
  FFV1_0(w[0], w[13], w[147], pars->GC_11, amp[403]); 
  FFV1_0(w[0], w[156], w[123], pars->GC_11, amp[404]); 
  FFS4_0(w[0], w[150], w[8], pars->GC_83, amp[405]); 
  FFV1_0(w[117], w[22], w[157], pars->GC_11, amp[406]); 
  FFV1_0(w[117], w[146], w[87], pars->GC_11, amp[407]); 
  FFV1_0(w[0], w[146], w[130], pars->GC_11, amp[408]); 
  FFV1_0(w[0], w[22], w[147], pars->GC_11, amp[409]); 
  FFV1_0(w[129], w[19], w[157], pars->GC_11, amp[410]); 
  FFV1_0(w[0], w[19], w[149], pars->GC_11, amp[411]); 
  FFV1_0(w[136], w[5], w[157], pars->GC_11, amp[412]); 
  FFV1_0(w[0], w[146], w[135], pars->GC_11, amp[413]); 
  FFV1_0(w[133], w[30], w[157], pars->GC_11, amp[414]); 
  FFV1_0(w[0], w[30], w[153], pars->GC_11, amp[415]); 
  FFV1_0(w[117], w[39], w[157], pars->GC_11, amp[416]); 
  FFV1_0(w[117], w[151], w[88], pars->GC_11, amp[417]); 
  FFV1_0(w[0], w[151], w[139], pars->GC_11, amp[418]); 
  FFV1_0(w[0], w[39], w[147], pars->GC_11, amp[419]); 
  FFV1_0(w[142], w[5], w[157], pars->GC_11, amp[420]); 
  FFV1_0(w[0], w[151], w[141], pars->GC_11, amp[421]); 
  FFV2_3_0(w[158], w[151], w[3], pars->GC_50, pars->GC_58, amp[422]); 
  FFV2_3_0(w[158], w[146], w[2], pars->GC_50, pars->GC_58, amp[423]); 
  FFV1_0(w[117], w[49], w[159], pars->GC_11, amp[424]); 
  FFV1_0(w[52], w[116], w[160], pars->GC_11, amp[425]); 
  FFV1_0(w[117], w[131], w[53], pars->GC_11, amp[426]); 
  FFV1_0(w[48], w[131], w[160], pars->GC_11, amp[427]); 
  FFV1_0(w[129], w[116], w[53], pars->GC_11, amp[428]); 
  FFV1_0(w[48], w[116], w[161], pars->GC_11, amp[429]); 
  FFV1_0(w[48], w[49], w[121], pars->GC_11, amp[430]); 
  FFV2_3_0(w[48], w[162], w[3], pars->GC_50, pars->GC_58, amp[431]); 
  FFV1_0(w[117], w[57], w[163], pars->GC_11, amp[432]); 
  FFV1_0(w[59], w[116], w[160], pars->GC_11, amp[433]); 
  FFV1_0(w[117], w[125], w[60], pars->GC_11, amp[434]); 
  FFV1_0(w[56], w[125], w[160], pars->GC_11, amp[435]); 
  FFV1_0(w[133], w[116], w[60], pars->GC_11, amp[436]); 
  FFV1_0(w[56], w[116], w[164], pars->GC_11, amp[437]); 
  FFV1_0(w[56], w[57], w[121], pars->GC_11, amp[438]); 
  FFV2_3_0(w[56], w[162], w[2], pars->GC_50, pars->GC_58, amp[439]); 
  FFV2_3_0(w[166], w[57], w[3], pars->GC_50, pars->GC_58, amp[440]); 
  FFV2_3_0(w[166], w[49], w[2], pars->GC_50, pars->GC_58, amp[441]); 
  FFS4_0(w[117], w[167], w[8], pars->GC_83, amp[442]); 
  FFV1_0(w[117], w[65], w[165], pars->GC_11, amp[443]); 
  FFV2_3_0(w[133], w[167], w[3], pars->GC_50, pars->GC_58, amp[444]); 
  FFV1_0(w[133], w[49], w[165], pars->GC_11, amp[445]); 
  FFV2_3_0(w[129], w[167], w[2], pars->GC_50, pars->GC_58, amp[446]); 
  FFV1_0(w[129], w[57], w[165], pars->GC_11, amp[447]); 
  FFV1_0(w[117], w[120], w[66], pars->GC_11, amp[448]); 
  FFV1_0(w[119], w[116], w[66], pars->GC_11, amp[449]); 
  FFV1_0(w[67], w[116], w[160], pars->GC_11, amp[450]); 
  FFV1_0(w[0], w[120], w[160], pars->GC_11, amp[451]); 
  FFV1_0(w[0], w[65], w[121], pars->GC_11, amp[452]); 
  FFS4_0(w[0], w[162], w[8], pars->GC_83, amp[453]); 
  FFV1_0(w[117], w[127], w[66], pars->GC_11, amp[454]); 
  FFV1_0(w[117], w[49], w[168], pars->GC_11, amp[455]); 
  FFV1_0(w[0], w[49], w[126], pars->GC_11, amp[456]); 
  FFV1_0(w[0], w[127], w[160], pars->GC_11, amp[457]); 
  FFV1_0(w[129], w[125], w[66], pars->GC_11, amp[458]); 
  FFV1_0(w[0], w[125], w[161], pars->GC_11, amp[459]); 
  FFV1_0(w[136], w[116], w[66], pars->GC_11, amp[460]); 
  FFV1_0(w[0], w[49], w[134], pars->GC_11, amp[461]); 
  FFV1_0(w[133], w[131], w[66], pars->GC_11, amp[462]); 
  FFV1_0(w[0], w[131], w[164], pars->GC_11, amp[463]); 
  FFV1_0(w[117], w[138], w[66], pars->GC_11, amp[464]); 
  FFV1_0(w[117], w[57], w[169], pars->GC_11, amp[465]); 
  FFV1_0(w[0], w[57], w[137], pars->GC_11, amp[466]); 
  FFV1_0(w[0], w[138], w[160], pars->GC_11, amp[467]); 
  FFV1_0(w[142], w[116], w[66], pars->GC_11, amp[468]); 
  FFV1_0(w[0], w[57], w[140], pars->GC_11, amp[469]); 
  FFV2_3_0(w[170], w[57], w[3], pars->GC_50, pars->GC_58, amp[470]); 
  FFV2_3_0(w[170], w[49], w[2], pars->GC_50, pars->GC_58, amp[471]); 
  FFV1_0(w[172], w[5], w[159], pars->GC_11, amp[472]); 
  FFV1_0(w[172], w[116], w[73], pars->GC_11, amp[473]); 
  FFV1_0(w[52], w[5], w[173], pars->GC_11, amp[474]); 
  FFV1_0(w[52], w[116], w[174], pars->GC_11, amp[475]); 
  FFV1_0(w[48], w[5], w[175], pars->GC_11, amp[476]); 
  FFV1_0(w[48], w[131], w[174], pars->GC_11, amp[477]); 
  FFV1_0(w[48], w[116], w[176], pars->GC_11, amp[478]); 
  FFV1_0(w[48], w[30], w[173], pars->GC_11, amp[479]); 
  FFV1_0(w[177], w[5], w[163], pars->GC_11, amp[480]); 
  FFV1_0(w[177], w[116], w[79], pars->GC_11, amp[481]); 
  FFV1_0(w[59], w[5], w[173], pars->GC_11, amp[482]); 
  FFV1_0(w[59], w[116], w[174], pars->GC_11, amp[483]); 
  FFV1_0(w[56], w[5], w[178], pars->GC_11, amp[484]); 
  FFV1_0(w[56], w[125], w[174], pars->GC_11, amp[485]); 
  FFV1_0(w[56], w[116], w[179], pars->GC_11, amp[486]); 
  FFV1_0(w[56], w[19], w[173], pars->GC_11, amp[487]); 
  FFV2_3_0(w[177], w[180], w[3], pars->GC_50, pars->GC_58, amp[488]); 
  FFV2_3_0(w[172], w[180], w[2], pars->GC_50, pars->GC_58, amp[489]); 
  FFS4_0(w[181], w[5], w[8], pars->GC_83, amp[490]); 
  FFV1_0(w[182], w[5], w[165], pars->GC_11, amp[491]); 
  FFV2_3_0(w[181], w[19], w[3], pars->GC_50, pars->GC_58, amp[492]); 
  FFV1_0(w[172], w[19], w[165], pars->GC_11, amp[493]); 
  FFV2_3_0(w[181], w[30], w[2], pars->GC_50, pars->GC_58, amp[494]); 
  FFV1_0(w[177], w[30], w[165], pars->GC_11, amp[495]); 
  FFV2_3_0(w[177], w[183], w[3], pars->GC_50, pars->GC_58, amp[496]); 
  FFV2_3_0(w[172], w[183], w[2], pars->GC_50, pars->GC_58, amp[497]); 
  FFS4_0(w[184], w[116], w[8], pars->GC_83, amp[498]); 
  FFV1_0(w[182], w[116], w[82], pars->GC_11, amp[499]); 
  FFV2_3_0(w[184], w[125], w[3], pars->GC_50, pars->GC_58, amp[500]); 
  FFV1_0(w[172], w[125], w[82], pars->GC_11, amp[501]); 
  FFV2_3_0(w[184], w[131], w[2], pars->GC_50, pars->GC_58, amp[502]); 
  FFV1_0(w[177], w[131], w[82], pars->GC_11, amp[503]); 
  FFV1_0(w[67], w[5], w[173], pars->GC_11, amp[504]); 
  FFV1_0(w[0], w[13], w[173], pars->GC_11, amp[505]); 
  FFV1_0(w[67], w[116], w[174], pars->GC_11, amp[506]); 
  FFV1_0(w[0], w[120], w[174], pars->GC_11, amp[507]); 
  FFV1_0(w[172], w[5], w[168], pars->GC_11, amp[508]); 
  FFV1_0(w[0], w[127], w[174], pars->GC_11, amp[509]); 
  FFV1_0(w[0], w[30], w[178], pars->GC_11, amp[510]); 
  FFV1_0(w[0], w[125], w[176], pars->GC_11, amp[511]); 
  FFV1_0(w[172], w[116], w[87], pars->GC_11, amp[512]); 
  FFV1_0(w[0], w[22], w[173], pars->GC_11, amp[513]); 
  FFV1_0(w[0], w[131], w[179], pars->GC_11, amp[514]); 
  FFV1_0(w[0], w[19], w[175], pars->GC_11, amp[515]); 
  FFV1_0(w[177], w[5], w[169], pars->GC_11, amp[516]); 
  FFV1_0(w[0], w[138], w[174], pars->GC_11, amp[517]); 
  FFV1_0(w[177], w[116], w[88], pars->GC_11, amp[518]); 
  FFV1_0(w[0], w[39], w[173], pars->GC_11, amp[519]); 
  FFV1_0(w[104], w[5], w[137], pars->GC_11, amp[520]); 
  FFV1_0(w[117], w[185], w[73], pars->GC_11, amp[521]); 
  VVV1_0(w[4], w[73], w[137], pars->GC_10, amp[522]); 
  FFV1_0(w[104], w[131], w[123], pars->GC_11, amp[523]); 
  FFV1_0(w[48], w[185], w[123], pars->GC_11, amp[524]); 
  FFV1_0(w[48], w[131], w[186], pars->GC_11, amp[525]); 
  FFV1_0(w[104], w[116], w[139], pars->GC_11, amp[526]); 
  FFV1_0(w[117], w[105], w[159], pars->GC_11, amp[527]); 
  VVV1_0(w[4], w[159], w[139], pars->GC_10, amp[528]); 
  FFV1_0(w[104], w[30], w[121], pars->GC_11, amp[529]); 
  FFV1_0(w[48], w[105], w[121], pars->GC_11, amp[530]); 
  FFV1_0(w[48], w[30], w[187], pars->GC_11, amp[531]); 
  FFV1_0(w[104], w[5], w[140], pars->GC_11, amp[532]); 
  FFV1_0(w[104], w[116], w[141], pars->GC_11, amp[533]); 
  FFV1_0(w[188], w[5], w[159], pars->GC_11, amp[534]); 
  FFV1_0(w[188], w[116], w[73], pars->GC_11, amp[535]); 
  VVV1_0(w[4], w[159], w[141], pars->GC_10, amp[536]); 
  VVV1_0(w[4], w[73], w[140], pars->GC_10, amp[537]); 
  FFV2_3_0(w[104], w[143], w[3], pars->GC_50, pars->GC_58, amp[538]); 
  FFV1_0(w[52], w[5], w[187], pars->GC_11, amp[539]); 
  FFV1_0(w[52], w[143], w[4], pars->GC_11, amp[540]); 
  FFV2_3_0(w[104], w[144], w[3], pars->GC_50, pars->GC_58, amp[541]); 
  FFV1_0(w[52], w[116], w[186], pars->GC_11, amp[542]); 
  FFV1_0(w[52], w[144], w[4], pars->GC_11, amp[543]); 
  FFV1_0(w[110], w[5], w[126], pars->GC_11, amp[544]); 
  FFV1_0(w[117], w[189], w[79], pars->GC_11, amp[545]); 
  VVV1_0(w[4], w[79], w[126], pars->GC_10, amp[546]); 
  FFV1_0(w[110], w[125], w[123], pars->GC_11, amp[547]); 
  FFV1_0(w[56], w[189], w[123], pars->GC_11, amp[548]); 
  FFV1_0(w[56], w[125], w[186], pars->GC_11, amp[549]); 
  FFV1_0(w[110], w[116], w[130], pars->GC_11, amp[550]); 
  FFV1_0(w[117], w[111], w[163], pars->GC_11, amp[551]); 
  VVV1_0(w[4], w[163], w[130], pars->GC_10, amp[552]); 
  FFV1_0(w[110], w[19], w[121], pars->GC_11, amp[553]); 
  FFV1_0(w[56], w[111], w[121], pars->GC_11, amp[554]); 
  FFV1_0(w[56], w[19], w[187], pars->GC_11, amp[555]); 
  FFV1_0(w[110], w[5], w[134], pars->GC_11, amp[556]); 
  FFV1_0(w[110], w[116], w[135], pars->GC_11, amp[557]); 
  FFV1_0(w[190], w[5], w[163], pars->GC_11, amp[558]); 
  FFV1_0(w[190], w[116], w[79], pars->GC_11, amp[559]); 
  VVV1_0(w[4], w[163], w[135], pars->GC_10, amp[560]); 
  VVV1_0(w[4], w[79], w[134], pars->GC_10, amp[561]); 
  FFV2_3_0(w[110], w[143], w[2], pars->GC_50, pars->GC_58, amp[562]); 
  FFV1_0(w[59], w[5], w[187], pars->GC_11, amp[563]); 
  FFV1_0(w[59], w[143], w[4], pars->GC_11, amp[564]); 
  FFV2_3_0(w[110], w[144], w[2], pars->GC_50, pars->GC_58, amp[565]); 
  FFV1_0(w[59], w[116], w[186], pars->GC_11, amp[566]); 
  FFV1_0(w[59], w[144], w[4], pars->GC_11, amp[567]); 
  FFV1_0(w[117], w[13], w[191], pars->GC_11, amp[568]); 
  FFV1_0(w[119], w[5], w[191], pars->GC_11, amp[569]); 
  FFV1_0(w[119], w[180], w[4], pars->GC_11, amp[570]); 
  FFV1_0(w[166], w[13], w[4], pars->GC_11, amp[571]); 
  FFV1_0(w[117], w[22], w[191], pars->GC_11, amp[572]); 
  FFV2_3_0(w[166], w[111], w[3], pars->GC_50, pars->GC_58, amp[573]); 
  FFV1_0(w[166], w[22], w[4], pars->GC_11, amp[574]); 
  FFV1_0(w[129], w[19], w[191], pars->GC_11, amp[575]); 
  FFV1_0(w[129], w[111], w[165], pars->GC_11, amp[576]); 
  FFV1_0(w[188], w[19], w[165], pars->GC_11, amp[577]); 
  FFV1_0(w[136], w[5], w[191], pars->GC_11, amp[578]); 
  FFV2_3_0(w[190], w[180], w[3], pars->GC_50, pars->GC_58, amp[579]); 
  FFV1_0(w[136], w[180], w[4], pars->GC_11, amp[580]); 
  FFV1_0(w[133], w[30], w[191], pars->GC_11, amp[581]); 
  FFV1_0(w[190], w[30], w[165], pars->GC_11, amp[582]); 
  FFV1_0(w[133], w[105], w[165], pars->GC_11, amp[583]); 
  FFV1_0(w[117], w[39], w[191], pars->GC_11, amp[584]); 
  FFV2_3_0(w[166], w[105], w[2], pars->GC_50, pars->GC_58, amp[585]); 
  FFV1_0(w[166], w[39], w[4], pars->GC_11, amp[586]); 
  FFV1_0(w[142], w[5], w[191], pars->GC_11, amp[587]); 
  FFV2_3_0(w[188], w[180], w[2], pars->GC_50, pars->GC_58, amp[588]); 
  FFV1_0(w[142], w[180], w[4], pars->GC_11, amp[589]); 
  FFV1_0(w[117], w[120], w[114], pars->GC_11, amp[590]); 
  FFV1_0(w[119], w[116], w[114], pars->GC_11, amp[591]); 
  FFV1_0(w[119], w[183], w[4], pars->GC_11, amp[592]); 
  FFV1_0(w[154], w[120], w[4], pars->GC_11, amp[593]); 
  FFV1_0(w[117], w[127], w[114], pars->GC_11, amp[594]); 
  FFV2_3_0(w[154], w[189], w[3], pars->GC_50, pars->GC_58, amp[595]); 
  FFV1_0(w[154], w[127], w[4], pars->GC_11, amp[596]); 
  FFV1_0(w[129], w[125], w[114], pars->GC_11, amp[597]); 
  FFV1_0(w[129], w[189], w[82], pars->GC_11, amp[598]); 
  FFV1_0(w[188], w[125], w[82], pars->GC_11, amp[599]); 
  FFV1_0(w[136], w[116], w[114], pars->GC_11, amp[600]); 
  FFV2_3_0(w[190], w[183], w[3], pars->GC_50, pars->GC_58, amp[601]); 
  FFV1_0(w[136], w[183], w[4], pars->GC_11, amp[602]); 
  FFV1_0(w[133], w[131], w[114], pars->GC_11, amp[603]); 
  FFV1_0(w[190], w[131], w[82], pars->GC_11, amp[604]); 
  FFV1_0(w[133], w[185], w[82], pars->GC_11, amp[605]); 
  FFV1_0(w[117], w[138], w[114], pars->GC_11, amp[606]); 
  FFV2_3_0(w[154], w[185], w[2], pars->GC_50, pars->GC_58, amp[607]); 
  FFV1_0(w[154], w[138], w[4], pars->GC_11, amp[608]); 
  FFV1_0(w[142], w[116], w[114], pars->GC_11, amp[609]); 
  FFV2_3_0(w[188], w[183], w[2], pars->GC_50, pars->GC_58, amp[610]); 
  FFV1_0(w[142], w[183], w[4], pars->GC_11, amp[611]); 
  FFV1_0(w[67], w[5], w[187], pars->GC_11, amp[612]); 
  FFV1_0(w[0], w[13], w[187], pars->GC_11, amp[613]); 
  FFV1_0(w[67], w[143], w[4], pars->GC_11, amp[614]); 
  FFV1_0(w[170], w[13], w[4], pars->GC_11, amp[615]); 
  FFV1_0(w[67], w[116], w[186], pars->GC_11, amp[616]); 
  FFV1_0(w[0], w[120], w[186], pars->GC_11, amp[617]); 
  FFV1_0(w[67], w[144], w[4], pars->GC_11, amp[618]); 
  FFV1_0(w[158], w[120], w[4], pars->GC_11, amp[619]); 
  FFV1_0(w[117], w[189], w[88], pars->GC_11, amp[620]); 
  FFV1_0(w[0], w[189], w[139], pars->GC_11, amp[621]); 
  FFV1_0(w[117], w[105], w[168], pars->GC_11, amp[622]); 
  FFV1_0(w[0], w[105], w[126], pars->GC_11, amp[623]); 
  VVV1_0(w[4], w[168], w[139], pars->GC_10, amp[624]); 
  VVV1_0(w[4], w[88], w[126], pars->GC_10, amp[625]); 
  FFV1_0(w[0], w[189], w[141], pars->GC_11, amp[626]); 
  FFV1_0(w[188], w[5], w[168], pars->GC_11, amp[627]); 
  VVV1_0(w[4], w[168], w[141], pars->GC_10, amp[628]); 
  FFV2_3_0(w[158], w[189], w[3], pars->GC_50, pars->GC_58, amp[629]); 
  FFV1_0(w[0], w[127], w[186], pars->GC_11, amp[630]); 
  FFV1_0(w[158], w[127], w[4], pars->GC_11, amp[631]); 
  FFV1_0(w[117], w[111], w[169], pars->GC_11, amp[632]); 
  FFV1_0(w[0], w[111], w[137], pars->GC_11, amp[633]); 
  FFV1_0(w[117], w[185], w[87], pars->GC_11, amp[634]); 
  FFV1_0(w[0], w[185], w[130], pars->GC_11, amp[635]); 
  VVV1_0(w[4], w[87], w[137], pars->GC_10, amp[636]); 
  VVV1_0(w[4], w[169], w[130], pars->GC_10, amp[637]); 
  FFV1_0(w[0], w[111], w[140], pars->GC_11, amp[638]); 
  FFV1_0(w[188], w[116], w[87], pars->GC_11, amp[639]); 
  VVV1_0(w[4], w[87], w[140], pars->GC_10, amp[640]); 
  FFV2_3_0(w[170], w[111], w[3], pars->GC_50, pars->GC_58, amp[641]); 
  FFV1_0(w[0], w[22], w[187], pars->GC_11, amp[642]); 
  FFV1_0(w[170], w[22], w[4], pars->GC_11, amp[643]); 
  FFV1_0(w[190], w[5], w[169], pars->GC_11, amp[644]); 
  FFV1_0(w[0], w[185], w[135], pars->GC_11, amp[645]); 
  VVV1_0(w[4], w[169], w[135], pars->GC_10, amp[646]); 
  FFV1_0(w[190], w[116], w[88], pars->GC_11, amp[647]); 
  FFV1_0(w[0], w[105], w[134], pars->GC_11, amp[648]); 
  VVV1_0(w[4], w[88], w[134], pars->GC_10, amp[649]); 
  FFV2_3_0(w[158], w[185], w[2], pars->GC_50, pars->GC_58, amp[650]); 
  FFV1_0(w[0], w[138], w[186], pars->GC_11, amp[651]); 
  FFV1_0(w[158], w[138], w[4], pars->GC_11, amp[652]); 
  FFV2_3_0(w[170], w[105], w[2], pars->GC_50, pars->GC_58, amp[653]); 
  FFV1_0(w[0], w[39], w[187], pars->GC_11, amp[654]); 
  FFV1_0(w[170], w[39], w[4], pars->GC_11, amp[655]); 
  FFV1_0(w[117], w[120], w[195], pars->GC_11, amp[656]); 
  FFV1_0(w[119], w[116], w[195], pars->GC_11, amp[657]); 
  FFV1_0(w[117], w[197], w[196], pars->GC_11, amp[658]); 
  FFV1_0(w[119], w[192], w[196], pars->GC_11, amp[659]); 
  FFV1_0(w[199], w[116], w[198], pars->GC_11, amp[660]); 
  FFS4_0(w[200], w[116], w[8], pars->GC_83, amp[661]); 
  FFV1_0(w[199], w[192], w[121], pars->GC_11, amp[662]); 
  FFS4_0(w[201], w[192], w[8], pars->GC_83, amp[663]); 
  FFV1_0(w[203], w[116], w[204], pars->GC_11, amp[664]); 
  FFV1_0(w[117], w[205], w[196], pars->GC_11, amp[665]); 
  FFV1_0(w[117], w[131], w[206], pars->GC_11, amp[666]); 
  FFV1_0(w[117], w[202], w[207], pars->GC_11, amp[667]); 
  FFV1_0(w[129], w[116], w[206], pars->GC_11, amp[668]); 
  FFV1_0(w[129], w[202], w[196], pars->GC_11, amp[669]); 
  FFV1_0(w[203], w[202], w[121], pars->GC_11, amp[670]); 
  FFV2_3_0(w[201], w[202], w[3], pars->GC_50, pars->GC_58, amp[671]); 
  FFV1_0(w[203], w[192], w[126], pars->GC_11, amp[672]); 
  FFV1_0(w[117], w[127], w[195], pars->GC_11, amp[673]); 
  FFV1_0(w[117], w[208], w[209], pars->GC_11, amp[674]); 
  FFV1_0(w[117], w[125], w[210], pars->GC_11, amp[675]); 
  FFV1_0(w[129], w[192], w[209], pars->GC_11, amp[676]); 
  FFV1_0(w[129], w[125], w[195], pars->GC_11, amp[677]); 
  FFV1_0(w[203], w[125], w[198], pars->GC_11, amp[678]); 
  FFV2_3_0(w[200], w[125], w[3], pars->GC_50, pars->GC_58, amp[679]); 
  FFV1_0(w[203], w[116], w[211], pars->GC_11, amp[680]); 
  FFV1_0(w[203], w[192], w[134], pars->GC_11, amp[681]); 
  FFV1_0(w[136], w[116], w[195], pars->GC_11, amp[682]); 
  FFV1_0(w[136], w[192], w[196], pars->GC_11, amp[683]); 
  FFV1_0(w[133], w[116], w[210], pars->GC_11, amp[684]); 
  FFV1_0(w[133], w[208], w[196], pars->GC_11, amp[685]); 
  FFV1_0(w[133], w[192], w[207], pars->GC_11, amp[686]); 
  FFV1_0(w[133], w[131], w[195], pars->GC_11, amp[687]); 
  FFV1_0(w[212], w[116], w[213], pars->GC_11, amp[688]); 
  FFV1_0(w[117], w[214], w[196], pars->GC_11, amp[689]); 
  FFV1_0(w[212], w[208], w[121], pars->GC_11, amp[690]); 
  FFV2_3_0(w[201], w[208], w[2], pars->GC_50, pars->GC_58, amp[691]); 
  FFV1_0(w[212], w[192], w[137], pars->GC_11, amp[692]); 
  FFV1_0(w[117], w[138], w[195], pars->GC_11, amp[693]); 
  FFV1_0(w[212], w[131], w[198], pars->GC_11, amp[694]); 
  FFV2_3_0(w[200], w[131], w[2], pars->GC_50, pars->GC_58, amp[695]); 
  FFV1_0(w[212], w[116], w[215], pars->GC_11, amp[696]); 
  FFV1_0(w[212], w[192], w[140], pars->GC_11, amp[697]); 
  FFV1_0(w[142], w[116], w[195], pars->GC_11, amp[698]); 
  FFV1_0(w[142], w[192], w[196], pars->GC_11, amp[699]); 
  FFV2_3_0(w[212], w[216], w[3], pars->GC_50, pars->GC_58, amp[700]); 
  FFV2_3_0(w[203], w[216], w[2], pars->GC_50, pars->GC_58, amp[701]); 
  FFV2_3_0(w[212], w[217], w[3], pars->GC_50, pars->GC_58, amp[702]); 
  FFV2_3_0(w[203], w[217], w[2], pars->GC_50, pars->GC_58, amp[703]); 
  FFV1_0(w[117], w[220], w[221], pars->GC_11, amp[704]); 
  FFV1_0(w[223], w[116], w[222], pars->GC_11, amp[705]); 
  FFV1_0(w[117], w[131], w[224], pars->GC_11, amp[706]); 
  FFV1_0(w[219], w[131], w[222], pars->GC_11, amp[707]); 
  FFV1_0(w[129], w[116], w[224], pars->GC_11, amp[708]); 
  FFV1_0(w[219], w[116], w[225], pars->GC_11, amp[709]); 
  FFV1_0(w[219], w[220], w[121], pars->GC_11, amp[710]); 
  FFV2_3_0(w[219], w[226], w[3], pars->GC_50, pars->GC_58, amp[711]); 
  FFV1_0(w[117], w[228], w[229], pars->GC_11, amp[712]); 
  FFV1_0(w[230], w[116], w[222], pars->GC_11, amp[713]); 
  FFV1_0(w[117], w[125], w[231], pars->GC_11, amp[714]); 
  FFV1_0(w[227], w[125], w[222], pars->GC_11, amp[715]); 
  FFV1_0(w[133], w[116], w[231], pars->GC_11, amp[716]); 
  FFV1_0(w[227], w[116], w[232], pars->GC_11, amp[717]); 
  FFV1_0(w[227], w[228], w[121], pars->GC_11, amp[718]); 
  FFV2_3_0(w[227], w[226], w[2], pars->GC_50, pars->GC_58, amp[719]); 
  FFV2_3_0(w[234], w[228], w[3], pars->GC_50, pars->GC_58, amp[720]); 
  FFV2_3_0(w[234], w[220], w[2], pars->GC_50, pars->GC_58, amp[721]); 
  FFS4_0(w[117], w[235], w[8], pars->GC_83, amp[722]); 
  FFV1_0(w[117], w[236], w[233], pars->GC_11, amp[723]); 
  FFV2_3_0(w[133], w[235], w[3], pars->GC_50, pars->GC_58, amp[724]); 
  FFV1_0(w[133], w[220], w[233], pars->GC_11, amp[725]); 
  FFV2_3_0(w[129], w[235], w[2], pars->GC_50, pars->GC_58, amp[726]); 
  FFV1_0(w[129], w[228], w[233], pars->GC_11, amp[727]); 
  FFV1_0(w[117], w[120], w[237], pars->GC_11, amp[728]); 
  FFV1_0(w[119], w[116], w[237], pars->GC_11, amp[729]); 
  FFV1_0(w[238], w[116], w[222], pars->GC_11, amp[730]); 
  FFV1_0(w[193], w[120], w[222], pars->GC_11, amp[731]); 
  FFV1_0(w[193], w[236], w[121], pars->GC_11, amp[732]); 
  FFS4_0(w[193], w[226], w[8], pars->GC_83, amp[733]); 
  FFV1_0(w[117], w[127], w[237], pars->GC_11, amp[734]); 
  FFV1_0(w[117], w[220], w[239], pars->GC_11, amp[735]); 
  FFV1_0(w[193], w[220], w[126], pars->GC_11, amp[736]); 
  FFV1_0(w[193], w[127], w[222], pars->GC_11, amp[737]); 
  FFV1_0(w[129], w[125], w[237], pars->GC_11, amp[738]); 
  FFV1_0(w[193], w[125], w[225], pars->GC_11, amp[739]); 
  FFV1_0(w[136], w[116], w[237], pars->GC_11, amp[740]); 
  FFV1_0(w[193], w[220], w[134], pars->GC_11, amp[741]); 
  FFV1_0(w[133], w[131], w[237], pars->GC_11, amp[742]); 
  FFV1_0(w[193], w[131], w[232], pars->GC_11, amp[743]); 
  FFV1_0(w[117], w[138], w[237], pars->GC_11, amp[744]); 
  FFV1_0(w[117], w[228], w[240], pars->GC_11, amp[745]); 
  FFV1_0(w[193], w[228], w[137], pars->GC_11, amp[746]); 
  FFV1_0(w[193], w[138], w[222], pars->GC_11, amp[747]); 
  FFV1_0(w[142], w[116], w[237], pars->GC_11, amp[748]); 
  FFV1_0(w[193], w[228], w[140], pars->GC_11, amp[749]); 
  FFV2_3_0(w[241], w[228], w[3], pars->GC_50, pars->GC_58, amp[750]); 
  FFV2_3_0(w[241], w[220], w[2], pars->GC_50, pars->GC_58, amp[751]); 
  FFV1_0(w[117], w[146], w[242], pars->GC_11, amp[752]); 
  FFV1_0(w[223], w[192], w[147], pars->GC_11, amp[753]); 
  FFV1_0(w[117], w[208], w[243], pars->GC_11, amp[754]); 
  FFV1_0(w[219], w[208], w[147], pars->GC_11, amp[755]); 
  FFV1_0(w[129], w[192], w[243], pars->GC_11, amp[756]); 
  FFV1_0(w[219], w[192], w[149], pars->GC_11, amp[757]); 
  FFV1_0(w[219], w[146], w[198], pars->GC_11, amp[758]); 
  FFV2_3_0(w[219], w[244], w[3], pars->GC_50, pars->GC_58, amp[759]); 
  FFV1_0(w[117], w[151], w[245], pars->GC_11, amp[760]); 
  FFV1_0(w[230], w[192], w[147], pars->GC_11, amp[761]); 
  FFV1_0(w[117], w[202], w[246], pars->GC_11, amp[762]); 
  FFV1_0(w[227], w[202], w[147], pars->GC_11, amp[763]); 
  FFV1_0(w[133], w[192], w[246], pars->GC_11, amp[764]); 
  FFV1_0(w[227], w[192], w[153], pars->GC_11, amp[765]); 
  FFV1_0(w[227], w[151], w[198], pars->GC_11, amp[766]); 
  FFV2_3_0(w[227], w[244], w[2], pars->GC_50, pars->GC_58, amp[767]); 
  FFV2_3_0(w[248], w[151], w[3], pars->GC_50, pars->GC_58, amp[768]); 
  FFV2_3_0(w[248], w[146], w[2], pars->GC_50, pars->GC_58, amp[769]); 
  FFS4_0(w[117], w[249], w[8], pars->GC_83, amp[770]); 
  FFV1_0(w[117], w[156], w[247], pars->GC_11, amp[771]); 
  FFV2_3_0(w[133], w[249], w[3], pars->GC_50, pars->GC_58, amp[772]); 
  FFV1_0(w[133], w[146], w[247], pars->GC_11, amp[773]); 
  FFV2_3_0(w[129], w[249], w[2], pars->GC_50, pars->GC_58, amp[774]); 
  FFV1_0(w[129], w[151], w[247], pars->GC_11, amp[775]); 
  FFV1_0(w[117], w[197], w[250], pars->GC_11, amp[776]); 
  FFV1_0(w[119], w[192], w[250], pars->GC_11, amp[777]); 
  FFV1_0(w[238], w[192], w[147], pars->GC_11, amp[778]); 
  FFV1_0(w[193], w[197], w[147], pars->GC_11, amp[779]); 
  FFV1_0(w[193], w[156], w[198], pars->GC_11, amp[780]); 
  FFS4_0(w[193], w[244], w[8], pars->GC_83, amp[781]); 
  FFV1_0(w[117], w[205], w[250], pars->GC_11, amp[782]); 
  FFV1_0(w[117], w[146], w[251], pars->GC_11, amp[783]); 
  FFV1_0(w[193], w[146], w[204], pars->GC_11, amp[784]); 
  FFV1_0(w[193], w[205], w[147], pars->GC_11, amp[785]); 
  FFV1_0(w[129], w[202], w[250], pars->GC_11, amp[786]); 
  FFV1_0(w[193], w[202], w[149], pars->GC_11, amp[787]); 
  FFV1_0(w[136], w[192], w[250], pars->GC_11, amp[788]); 
  FFV1_0(w[193], w[146], w[211], pars->GC_11, amp[789]); 
  FFV1_0(w[133], w[208], w[250], pars->GC_11, amp[790]); 
  FFV1_0(w[193], w[208], w[153], pars->GC_11, amp[791]); 
  FFV1_0(w[117], w[214], w[250], pars->GC_11, amp[792]); 
  FFV1_0(w[117], w[151], w[252], pars->GC_11, amp[793]); 
  FFV1_0(w[193], w[151], w[213], pars->GC_11, amp[794]); 
  FFV1_0(w[193], w[214], w[147], pars->GC_11, amp[795]); 
  FFV1_0(w[142], w[192], w[250], pars->GC_11, amp[796]); 
  FFV1_0(w[193], w[151], w[215], pars->GC_11, amp[797]); 
  FFV2_3_0(w[253], w[151], w[3], pars->GC_50, pars->GC_58, amp[798]); 
  FFV2_3_0(w[253], w[146], w[2], pars->GC_50, pars->GC_58, amp[799]); 
  FFV1_0(w[172], w[116], w[242], pars->GC_11, amp[800]); 
  FFV1_0(w[172], w[192], w[221], pars->GC_11, amp[801]); 
  FFV1_0(w[223], w[116], w[254], pars->GC_11, amp[802]); 
  FFV1_0(w[223], w[192], w[173], pars->GC_11, amp[803]); 
  FFV1_0(w[219], w[116], w[255], pars->GC_11, amp[804]); 
  FFV1_0(w[219], w[208], w[173], pars->GC_11, amp[805]); 
  FFV1_0(w[219], w[192], w[175], pars->GC_11, amp[806]); 
  FFV1_0(w[219], w[131], w[254], pars->GC_11, amp[807]); 
  FFV1_0(w[177], w[116], w[245], pars->GC_11, amp[808]); 
  FFV1_0(w[177], w[192], w[229], pars->GC_11, amp[809]); 
  FFV1_0(w[230], w[116], w[254], pars->GC_11, amp[810]); 
  FFV1_0(w[230], w[192], w[173], pars->GC_11, amp[811]); 
  FFV1_0(w[227], w[116], w[256], pars->GC_11, amp[812]); 
  FFV1_0(w[227], w[202], w[173], pars->GC_11, amp[813]); 
  FFV1_0(w[227], w[192], w[178], pars->GC_11, amp[814]); 
  FFV1_0(w[227], w[125], w[254], pars->GC_11, amp[815]); 
  FFV2_3_0(w[177], w[257], w[3], pars->GC_50, pars->GC_58, amp[816]); 
  FFV2_3_0(w[172], w[257], w[2], pars->GC_50, pars->GC_58, amp[817]); 
  FFS4_0(w[258], w[116], w[8], pars->GC_83, amp[818]); 
  FFV1_0(w[182], w[116], w[247], pars->GC_11, amp[819]); 
  FFV2_3_0(w[258], w[125], w[3], pars->GC_50, pars->GC_58, amp[820]); 
  FFV1_0(w[172], w[125], w[247], pars->GC_11, amp[821]); 
  FFV2_3_0(w[258], w[131], w[2], pars->GC_50, pars->GC_58, amp[822]); 
  FFV1_0(w[177], w[131], w[247], pars->GC_11, amp[823]); 
  FFV2_3_0(w[177], w[259], w[3], pars->GC_50, pars->GC_58, amp[824]); 
  FFV2_3_0(w[172], w[259], w[2], pars->GC_50, pars->GC_58, amp[825]); 
  FFS4_0(w[260], w[192], w[8], pars->GC_83, amp[826]); 
  FFV1_0(w[182], w[192], w[233], pars->GC_11, amp[827]); 
  FFV2_3_0(w[260], w[202], w[3], pars->GC_50, pars->GC_58, amp[828]); 
  FFV1_0(w[172], w[202], w[233], pars->GC_11, amp[829]); 
  FFV2_3_0(w[260], w[208], w[2], pars->GC_50, pars->GC_58, amp[830]); 
  FFV1_0(w[177], w[208], w[233], pars->GC_11, amp[831]); 
  FFV1_0(w[238], w[116], w[254], pars->GC_11, amp[832]); 
  FFV1_0(w[193], w[120], w[254], pars->GC_11, amp[833]); 
  FFV1_0(w[238], w[192], w[173], pars->GC_11, amp[834]); 
  FFV1_0(w[193], w[197], w[173], pars->GC_11, amp[835]); 
  FFV1_0(w[172], w[116], w[251], pars->GC_11, amp[836]); 
  FFV1_0(w[193], w[205], w[173], pars->GC_11, amp[837]); 
  FFV1_0(w[193], w[131], w[256], pars->GC_11, amp[838]); 
  FFV1_0(w[193], w[202], w[175], pars->GC_11, amp[839]); 
  FFV1_0(w[172], w[192], w[239], pars->GC_11, amp[840]); 
  FFV1_0(w[193], w[127], w[254], pars->GC_11, amp[841]); 
  FFV1_0(w[193], w[208], w[178], pars->GC_11, amp[842]); 
  FFV1_0(w[193], w[125], w[255], pars->GC_11, amp[843]); 
  FFV1_0(w[177], w[116], w[252], pars->GC_11, amp[844]); 
  FFV1_0(w[193], w[214], w[173], pars->GC_11, amp[845]); 
  FFV1_0(w[177], w[192], w[240], pars->GC_11, amp[846]); 
  FFV1_0(w[193], w[138], w[254], pars->GC_11, amp[847]); 
  FFV1_0(w[261], w[116], w[213], pars->GC_11, amp[848]); 
  FFV1_0(w[117], w[262], w[221], pars->GC_11, amp[849]); 
  VVV1_0(w[4], w[221], w[213], pars->GC_10, amp[850]); 
  FFV1_0(w[261], w[208], w[121], pars->GC_11, amp[851]); 
  FFV1_0(w[219], w[262], w[121], pars->GC_11, amp[852]); 
  FFV1_0(w[219], w[208], w[187], pars->GC_11, amp[853]); 
  FFV1_0(w[261], w[192], w[137], pars->GC_11, amp[854]); 
  FFV1_0(w[117], w[185], w[242], pars->GC_11, amp[855]); 
  VVV1_0(w[4], w[242], w[137], pars->GC_10, amp[856]); 
  FFV1_0(w[261], w[131], w[198], pars->GC_11, amp[857]); 
  FFV1_0(w[219], w[185], w[198], pars->GC_11, amp[858]); 
  FFV1_0(w[219], w[131], w[263], pars->GC_11, amp[859]); 
  FFV1_0(w[261], w[116], w[215], pars->GC_11, amp[860]); 
  FFV1_0(w[261], w[192], w[140], pars->GC_11, amp[861]); 
  FFV1_0(w[188], w[116], w[242], pars->GC_11, amp[862]); 
  FFV1_0(w[188], w[192], w[221], pars->GC_11, amp[863]); 
  VVV1_0(w[4], w[242], w[140], pars->GC_10, amp[864]); 
  VVV1_0(w[4], w[221], w[215], pars->GC_10, amp[865]); 
  FFV2_3_0(w[261], w[216], w[3], pars->GC_50, pars->GC_58, amp[866]); 
  FFV1_0(w[223], w[116], w[263], pars->GC_11, amp[867]); 
  FFV1_0(w[223], w[216], w[4], pars->GC_11, amp[868]); 
  FFV2_3_0(w[261], w[217], w[3], pars->GC_50, pars->GC_58, amp[869]); 
  FFV1_0(w[223], w[192], w[187], pars->GC_11, amp[870]); 
  FFV1_0(w[223], w[217], w[4], pars->GC_11, amp[871]); 
  FFV1_0(w[264], w[116], w[204], pars->GC_11, amp[872]); 
  FFV1_0(w[117], w[265], w[229], pars->GC_11, amp[873]); 
  VVV1_0(w[4], w[229], w[204], pars->GC_10, amp[874]); 
  FFV1_0(w[264], w[202], w[121], pars->GC_11, amp[875]); 
  FFV1_0(w[227], w[265], w[121], pars->GC_11, amp[876]); 
  FFV1_0(w[227], w[202], w[187], pars->GC_11, amp[877]); 
  FFV1_0(w[264], w[192], w[126], pars->GC_11, amp[878]); 
  FFV1_0(w[117], w[189], w[245], pars->GC_11, amp[879]); 
  VVV1_0(w[4], w[245], w[126], pars->GC_10, amp[880]); 
  FFV1_0(w[264], w[125], w[198], pars->GC_11, amp[881]); 
  FFV1_0(w[227], w[189], w[198], pars->GC_11, amp[882]); 
  FFV1_0(w[227], w[125], w[263], pars->GC_11, amp[883]); 
  FFV1_0(w[264], w[116], w[211], pars->GC_11, amp[884]); 
  FFV1_0(w[264], w[192], w[134], pars->GC_11, amp[885]); 
  FFV1_0(w[190], w[116], w[245], pars->GC_11, amp[886]); 
  FFV1_0(w[190], w[192], w[229], pars->GC_11, amp[887]); 
  VVV1_0(w[4], w[245], w[134], pars->GC_10, amp[888]); 
  VVV1_0(w[4], w[229], w[211], pars->GC_10, amp[889]); 
  FFV2_3_0(w[264], w[216], w[2], pars->GC_50, pars->GC_58, amp[890]); 
  FFV1_0(w[230], w[116], w[263], pars->GC_11, amp[891]); 
  FFV1_0(w[230], w[216], w[4], pars->GC_11, amp[892]); 
  FFV2_3_0(w[264], w[217], w[2], pars->GC_50, pars->GC_58, amp[893]); 
  FFV1_0(w[230], w[192], w[187], pars->GC_11, amp[894]); 
  FFV1_0(w[230], w[217], w[4], pars->GC_11, amp[895]); 
  FFV1_0(w[117], w[120], w[266], pars->GC_11, amp[896]); 
  FFV1_0(w[119], w[116], w[266], pars->GC_11, amp[897]); 
  FFV1_0(w[119], w[257], w[4], pars->GC_11, amp[898]); 
  FFV1_0(w[248], w[120], w[4], pars->GC_11, amp[899]); 
  FFV1_0(w[117], w[127], w[266], pars->GC_11, amp[900]); 
  FFV2_3_0(w[248], w[189], w[3], pars->GC_50, pars->GC_58, amp[901]); 
  FFV1_0(w[248], w[127], w[4], pars->GC_11, amp[902]); 
  FFV1_0(w[129], w[125], w[266], pars->GC_11, amp[903]); 
  FFV1_0(w[129], w[189], w[247], pars->GC_11, amp[904]); 
  FFV1_0(w[188], w[125], w[247], pars->GC_11, amp[905]); 
  FFV1_0(w[136], w[116], w[266], pars->GC_11, amp[906]); 
  FFV2_3_0(w[190], w[257], w[3], pars->GC_50, pars->GC_58, amp[907]); 
  FFV1_0(w[136], w[257], w[4], pars->GC_11, amp[908]); 
  FFV1_0(w[133], w[131], w[266], pars->GC_11, amp[909]); 
  FFV1_0(w[190], w[131], w[247], pars->GC_11, amp[910]); 
  FFV1_0(w[133], w[185], w[247], pars->GC_11, amp[911]); 
  FFV1_0(w[117], w[138], w[266], pars->GC_11, amp[912]); 
  FFV2_3_0(w[248], w[185], w[2], pars->GC_50, pars->GC_58, amp[913]); 
  FFV1_0(w[248], w[138], w[4], pars->GC_11, amp[914]); 
  FFV1_0(w[142], w[116], w[266], pars->GC_11, amp[915]); 
  FFV2_3_0(w[188], w[257], w[2], pars->GC_50, pars->GC_58, amp[916]); 
  FFV1_0(w[142], w[257], w[4], pars->GC_11, amp[917]); 
  FFV1_0(w[117], w[197], w[267], pars->GC_11, amp[918]); 
  FFV1_0(w[119], w[192], w[267], pars->GC_11, amp[919]); 
  FFV1_0(w[119], w[259], w[4], pars->GC_11, amp[920]); 
  FFV1_0(w[234], w[197], w[4], pars->GC_11, amp[921]); 
  FFV1_0(w[117], w[205], w[267], pars->GC_11, amp[922]); 
  FFV2_3_0(w[234], w[265], w[3], pars->GC_50, pars->GC_58, amp[923]); 
  FFV1_0(w[234], w[205], w[4], pars->GC_11, amp[924]); 
  FFV1_0(w[129], w[202], w[267], pars->GC_11, amp[925]); 
  FFV1_0(w[129], w[265], w[233], pars->GC_11, amp[926]); 
  FFV1_0(w[188], w[202], w[233], pars->GC_11, amp[927]); 
  FFV1_0(w[136], w[192], w[267], pars->GC_11, amp[928]); 
  FFV2_3_0(w[190], w[259], w[3], pars->GC_50, pars->GC_58, amp[929]); 
  FFV1_0(w[136], w[259], w[4], pars->GC_11, amp[930]); 
  FFV1_0(w[133], w[208], w[267], pars->GC_11, amp[931]); 
  FFV1_0(w[190], w[208], w[233], pars->GC_11, amp[932]); 
  FFV1_0(w[133], w[262], w[233], pars->GC_11, amp[933]); 
  FFV1_0(w[117], w[214], w[267], pars->GC_11, amp[934]); 
  FFV2_3_0(w[234], w[262], w[2], pars->GC_50, pars->GC_58, amp[935]); 
  FFV1_0(w[234], w[214], w[4], pars->GC_11, amp[936]); 
  FFV1_0(w[142], w[192], w[267], pars->GC_11, amp[937]); 
  FFV2_3_0(w[188], w[259], w[2], pars->GC_50, pars->GC_58, amp[938]); 
  FFV1_0(w[142], w[259], w[4], pars->GC_11, amp[939]); 
  FFV1_0(w[238], w[116], w[263], pars->GC_11, amp[940]); 
  FFV1_0(w[193], w[120], w[263], pars->GC_11, amp[941]); 
  FFV1_0(w[238], w[216], w[4], pars->GC_11, amp[942]); 
  FFV1_0(w[253], w[120], w[4], pars->GC_11, amp[943]); 
  FFV1_0(w[238], w[192], w[187], pars->GC_11, amp[944]); 
  FFV1_0(w[193], w[197], w[187], pars->GC_11, amp[945]); 
  FFV1_0(w[238], w[217], w[4], pars->GC_11, amp[946]); 
  FFV1_0(w[241], w[197], w[4], pars->GC_11, amp[947]); 
  FFV1_0(w[117], w[265], w[240], pars->GC_11, amp[948]); 
  FFV1_0(w[193], w[265], w[137], pars->GC_11, amp[949]); 
  FFV1_0(w[117], w[185], w[251], pars->GC_11, amp[950]); 
  FFV1_0(w[193], w[185], w[204], pars->GC_11, amp[951]); 
  VVV1_0(w[4], w[251], w[137], pars->GC_10, amp[952]); 
  VVV1_0(w[4], w[240], w[204], pars->GC_10, amp[953]); 
  FFV1_0(w[193], w[265], w[140], pars->GC_11, amp[954]); 
  FFV1_0(w[188], w[116], w[251], pars->GC_11, amp[955]); 
  VVV1_0(w[4], w[251], w[140], pars->GC_10, amp[956]); 
  FFV2_3_0(w[241], w[265], w[3], pars->GC_50, pars->GC_58, amp[957]); 
  FFV1_0(w[193], w[205], w[187], pars->GC_11, amp[958]); 
  FFV1_0(w[241], w[205], w[4], pars->GC_11, amp[959]); 
  FFV1_0(w[117], w[189], w[252], pars->GC_11, amp[960]); 
  FFV1_0(w[193], w[189], w[213], pars->GC_11, amp[961]); 
  FFV1_0(w[117], w[262], w[239], pars->GC_11, amp[962]); 
  FFV1_0(w[193], w[262], w[126], pars->GC_11, amp[963]); 
  VVV1_0(w[4], w[239], w[213], pars->GC_10, amp[964]); 
  VVV1_0(w[4], w[252], w[126], pars->GC_10, amp[965]); 
  FFV1_0(w[193], w[189], w[215], pars->GC_11, amp[966]); 
  FFV1_0(w[188], w[192], w[239], pars->GC_11, amp[967]); 
  VVV1_0(w[4], w[239], w[215], pars->GC_10, amp[968]); 
  FFV2_3_0(w[253], w[189], w[3], pars->GC_50, pars->GC_58, amp[969]); 
  FFV1_0(w[193], w[127], w[263], pars->GC_11, amp[970]); 
  FFV1_0(w[253], w[127], w[4], pars->GC_11, amp[971]); 
  FFV1_0(w[190], w[116], w[252], pars->GC_11, amp[972]); 
  FFV1_0(w[193], w[262], w[134], pars->GC_11, amp[973]); 
  VVV1_0(w[4], w[252], w[134], pars->GC_10, amp[974]); 
  FFV1_0(w[190], w[192], w[240], pars->GC_11, amp[975]); 
  FFV1_0(w[193], w[185], w[211], pars->GC_11, amp[976]); 
  VVV1_0(w[4], w[240], w[211], pars->GC_10, amp[977]); 
  FFV2_3_0(w[241], w[262], w[2], pars->GC_50, pars->GC_58, amp[978]); 
  FFV1_0(w[193], w[214], w[187], pars->GC_11, amp[979]); 
  FFV1_0(w[241], w[214], w[4], pars->GC_11, amp[980]); 
  FFV2_3_0(w[253], w[185], w[2], pars->GC_50, pars->GC_58, amp[981]); 
  FFV1_0(w[193], w[138], w[263], pars->GC_11, amp[982]); 
  FFV1_0(w[253], w[138], w[4], pars->GC_11, amp[983]); 


}
double PY8MEs_R12_P8_sm_bb_zzgbb::matrix_12_bb_zzgbb() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 328;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[0] + 1./3. * amp[1] + amp[2] + amp[3] +
      amp[4] + amp[5] + 1./3. * amp[6] + 1./3. * amp[7] + amp[8] + amp[9] +
      1./3. * amp[10] + amp[11] + 1./3. * amp[12] + amp[13] + 1./3. * amp[14] +
      1./3. * amp[15] + 1./3. * amp[16] + 1./3. * amp[17] + amp[18] + 1./3. *
      amp[19] + amp[20] + 1./3. * amp[21] + amp[22] + amp[23] + amp[24] + 1./3.
      * amp[25] + 1./3. * amp[26] + amp[27] + 1./3. * amp[28] + amp[29] +
      amp[30] + 1./3. * amp[31] + amp[32] + amp[33] + 1./3. * amp[34] + 1./3. *
      amp[35] + 1./3. * amp[36] + 1./3. * amp[37] + amp[38] + amp[39] + amp[40]
      + 1./3. * amp[41] + 1./3. * amp[42] + amp[43] + amp[44] + amp[45] + 1./3.
      * amp[46] + 1./3. * amp[47] + amp[48] + amp[49] + 1./3. * amp[50] +
      amp[51] + 1./3. * amp[52] + amp[53] + 1./3. * amp[54] + 1./3. * amp[55] +
      amp[56] + amp[57] + 1./3. * amp[58] + amp[59] + 1./3. * amp[60] + amp[61]
      + 1./3. * amp[62] + 1./3. * amp[63] + amp[64] + amp[65] + amp[66] +
      amp[67] + amp[68] + amp[69] + amp[70] + amp[71] + 1./3. * amp[72] + 1./3.
      * amp[73] + amp[74] + amp[75] + 1./3. * amp[76] + 1./3. * amp[77] + 1./3.
      * amp[78] + amp[79] + 1./3. * amp[80] + amp[81] + 1./3. * amp[82] +
      amp[83] + 1./3. * amp[84] + 1./3. * amp[85] + 1./3. * amp[86] + amp[87] +
      1./3. * amp[88] + amp[89] + 1./3. * amp[90] + amp[91] + 1./3. * amp[92] +
      1./3. * amp[93] + 1./3. * amp[94] + 1./3. * amp[95] + amp[192] + amp[193]
      - Complex<double> (0, 1) * amp[194] + 1./3. * amp[195] + 1./3. * amp[196]
      + 1./3. * amp[198] + amp[201] + Complex<double> (0, 1) * amp[203] +
      amp[204] + 1./3. * amp[205] - Complex<double> (0, 1) * amp[209] +
      amp[210] + Complex<double> (0, 1) * amp[211] + amp[212] + 1./3. *
      amp[213] + 1./3. * amp[215] + amp[216] + amp[217] - Complex<double> (0,
      1) * amp[218] + 1./3. * amp[219] + 1./3. * amp[220] + 1./3. * amp[222] +
      amp[225] + Complex<double> (0, 1) * amp[227] + amp[228] + 1./3. *
      amp[229] - Complex<double> (0, 1) * amp[233] + amp[234] + Complex<double>
      (0, 1) * amp[235] + amp[236] + 1./3. * amp[237] + 1./3. * amp[239] -
      Complex<double> (0, 1) * amp[262] - Complex<double> (0, 1) * amp[263] +
      amp[265] - Complex<double> (0, 1) * amp[266] + amp[267] + amp[268] -
      Complex<double> (0, 1) * amp[269] + amp[270] - Complex<double> (0, 1) *
      amp[272] - Complex<double> (0, 1) * amp[275] + amp[277] - Complex<double>
      (0, 1) * amp[278] + amp[279] + amp[280] - Complex<double> (0, 1) *
      amp[281] + Complex<double> (0, 1) * amp[284] + Complex<double> (0, 1) *
      amp[285] + amp[286] + 1./3. * amp[290] + 1./3. * amp[291] + amp[292] +
      1./3. * amp[293] - Complex<double> (0, 1) * amp[297] + 1./3. * amp[298] +
      1./3. * amp[301] + 1./3. * amp[303] + amp[306] + 1./3. * amp[307] -
      Complex<double> (0, 1) * amp[308] - Complex<double> (0, 1) * amp[312] +
      Complex<double> (0, 1) * amp[314] + 1./3. * amp[317] - Complex<double>
      (0, 1) * amp[321] + 1./3. * amp[322] + 1./3. * amp[324] + Complex<double>
      (0, 1) * amp[326]);
  jamp[1] = +1./2. * (-1./3. * amp[48] - 1./3. * amp[49] - amp[50] - 1./3. *
      amp[51] - amp[52] - 1./3. * amp[53] - amp[54] - amp[55] - 1./3. * amp[56]
      - 1./3. * amp[57] - amp[58] - 1./3. * amp[59] - amp[60] - 1./3. * amp[61]
      - amp[62] - amp[63] - 1./3. * amp[64] - 1./3. * amp[65] - 1./3. * amp[66]
      - 1./3. * amp[67] - 1./3. * amp[68] - 1./3. * amp[69] - 1./3. * amp[70] -
      1./3. * amp[71] - amp[72] - amp[73] - 1./3. * amp[74] - 1./3. * amp[75] -
      amp[76] - amp[77] - amp[78] - 1./3. * amp[79] - amp[80] - 1./3. * amp[81]
      - amp[82] - 1./3. * amp[83] - amp[84] - amp[85] - amp[86] - 1./3. *
      amp[87] - amp[88] - 1./3. * amp[89] - amp[90] - 1./3. * amp[91] - amp[92]
      - amp[93] - amp[94] - amp[95] - amp[144] - 1./3. * amp[145] - 1./3. *
      amp[146] - amp[147] - 1./3. * amp[148] - amp[149] - amp[150] - 1./3. *
      amp[151] - amp[152] - 1./3. * amp[153] - 1./3. * amp[154] - amp[155] -
      1./3. * amp[156] - amp[157] - amp[158] - 1./3. * amp[159] - amp[160] -
      amp[161] - amp[162] - amp[163] - amp[164] - amp[165] - amp[166] -
      amp[167] - 1./3. * amp[168] - 1./3. * amp[169] - 1./3. * amp[170] - 1./3.
      * amp[171] - 1./3. * amp[172] - 1./3. * amp[173] - 1./3. * amp[174] -
      1./3. * amp[175] - 1./3. * amp[176] - 1./3. * amp[177] - amp[178] -
      amp[179] - amp[180] - amp[181] - 1./3. * amp[182] - amp[183] - 1./3. *
      amp[184] - 1./3. * amp[185] - amp[186] - 1./3. * amp[187] - amp[188] -
      amp[189] - 1./3. * amp[190] - 1./3. * amp[191] - 1./3. * amp[193] -
      amp[196] + Complex<double> (0, 1) * amp[197] - Complex<double> (0, 1) *
      amp[200] - amp[206] - 1./3. * amp[207] - Complex<double> (0, 1) *
      amp[208] + Complex<double> (0, 1) * amp[214] - 1./3. * amp[217] -
      amp[220] + Complex<double> (0, 1) * amp[221] - Complex<double> (0, 1) *
      amp[224] - amp[230] - 1./3. * amp[231] - Complex<double> (0, 1) *
      amp[232] + Complex<double> (0, 1) * amp[238] - Complex<double> (0, 1) *
      amp[240] - Complex<double> (0, 1) * amp[241] - amp[242] - Complex<double>
      (0, 1) * amp[244] - Complex<double> (0, 1) * amp[247] - amp[249] -
      Complex<double> (0, 1) * amp[250] - amp[251] - amp[252] - Complex<double>
      (0, 1) * amp[253] - amp[254] - Complex<double> (0, 1) * amp[256] -
      Complex<double> (0, 1) * amp[259] - amp[260] - amp[261] - 1./3. *
      amp[264] - 1./3. * amp[265] - 1./3. * amp[267] - 1./3. * amp[268] - 1./3.
      * amp[270] - 1./3. * amp[271] - 1./3. * amp[273] - 1./3. * amp[274] -
      1./3. * amp[276] - 1./3. * amp[277] - 1./3. * amp[279] - 1./3. * amp[280]
      - 1./3. * amp[282] - 1./3. * amp[283] + Complex<double> (0, 1) * amp[288]
      + Complex<double> (0, 1) * amp[289] - amp[291] - 1./3. * amp[292] -
      amp[293] - Complex<double> (0, 1) * amp[296] - amp[298] - amp[299] -
      Complex<double> (0, 1) * amp[300] - amp[301] + Complex<double> (0, 1) *
      amp[302] - amp[303] - 1./3. * amp[306] - amp[307] - Complex<double> (0,
      1) * amp[309] - 1./3. * amp[311] - amp[316] - amp[317] - Complex<double>
      (0, 1) * amp[318] - 1./3. * amp[319] - amp[322] + Complex<double> (0, 1)
      * amp[323] - amp[324]);
  jamp[2] = +1./2. * (-amp[0] - amp[1] - 1./3. * amp[2] - 1./3. * amp[3] -
      1./3. * amp[4] - 1./3. * amp[5] - amp[6] - amp[7] - 1./3. * amp[8] -
      1./3. * amp[9] - amp[10] - 1./3. * amp[11] - amp[12] - 1./3. * amp[13] -
      amp[14] - amp[15] - amp[16] - amp[17] - 1./3. * amp[18] - amp[19] - 1./3.
      * amp[20] - amp[21] - 1./3. * amp[22] - 1./3. * amp[23] - 1./3. * amp[24]
      - amp[25] - amp[26] - 1./3. * amp[27] - amp[28] - 1./3. * amp[29] - 1./3.
      * amp[30] - amp[31] - 1./3. * amp[32] - 1./3. * amp[33] - amp[34] -
      amp[35] - amp[36] - amp[37] - 1./3. * amp[38] - 1./3. * amp[39] - 1./3. *
      amp[40] - amp[41] - amp[42] - 1./3. * amp[43] - 1./3. * amp[44] - 1./3. *
      amp[45] - amp[46] - amp[47] - amp[96] - amp[97] - 1./3. * amp[98] -
      amp[99] - 1./3. * amp[100] - amp[101] - 1./3. * amp[102] - 1./3. *
      amp[103] - amp[104] - amp[105] - 1./3. * amp[106] - amp[107] - 1./3. *
      amp[108] - amp[109] - 1./3. * amp[110] - 1./3. * amp[111] - amp[112] -
      amp[113] - amp[114] - amp[115] - amp[116] - amp[117] - amp[118] -
      amp[119] - 1./3. * amp[120] - 1./3. * amp[121] - amp[122] - amp[123] -
      1./3. * amp[124] - 1./3. * amp[125] - 1./3. * amp[126] - amp[127] - 1./3.
      * amp[128] - amp[129] - 1./3. * amp[130] - amp[131] - 1./3. * amp[132] -
      1./3. * amp[133] - 1./3. * amp[134] - amp[135] - 1./3. * amp[136] -
      amp[137] - 1./3. * amp[138] - amp[139] - 1./3. * amp[140] - 1./3. *
      amp[141] - 1./3. * amp[142] - 1./3. * amp[143] - 1./3. * amp[192] -
      amp[195] - Complex<double> (0, 1) * amp[197] - amp[198] - amp[199] +
      Complex<double> (0, 1) * amp[200] - 1./3. * amp[201] - 1./3. * amp[202] -
      1./3. * amp[204] - amp[205] + Complex<double> (0, 1) * amp[208] - 1./3. *
      amp[210] - 1./3. * amp[212] - amp[213] - Complex<double> (0, 1) *
      amp[214] - amp[215] - 1./3. * amp[216] - amp[219] - Complex<double> (0,
      1) * amp[221] - amp[222] - amp[223] + Complex<double> (0, 1) * amp[224] -
      1./3. * amp[225] - 1./3. * amp[226] - 1./3. * amp[228] - amp[229] +
      Complex<double> (0, 1) * amp[232] - 1./3. * amp[234] - 1./3. * amp[236] -
      amp[237] - Complex<double> (0, 1) * amp[238] - amp[239] + Complex<double>
      (0, 1) * amp[240] + Complex<double> (0, 1) * amp[241] - amp[243] +
      Complex<double> (0, 1) * amp[244] - amp[245] - amp[246] + Complex<double>
      (0, 1) * amp[247] - amp[248] + Complex<double> (0, 1) * amp[250] +
      Complex<double> (0, 1) * amp[253] - amp[255] + Complex<double> (0, 1) *
      amp[256] - amp[257] - amp[258] + Complex<double> (0, 1) * amp[259] -
      1./3. * amp[286] - 1./3. * amp[287] - Complex<double> (0, 1) * amp[288] -
      Complex<double> (0, 1) * amp[289] - amp[290] - amp[294] - 1./3. *
      amp[295] + Complex<double> (0, 1) * amp[296] + Complex<double> (0, 1) *
      amp[300] - Complex<double> (0, 1) * amp[302] - amp[304] - 1./3. *
      amp[305] + Complex<double> (0, 1) * amp[309] - 1./3. * amp[310] - 1./3. *
      amp[313] - 1./3. * amp[315] + Complex<double> (0, 1) * amp[318] - 1./3. *
      amp[320] - Complex<double> (0, 1) * amp[323] - 1./3. * amp[325] - 1./3. *
      amp[327]);
  jamp[3] = +1./2. * (+1./3. * amp[96] + 1./3. * amp[97] + amp[98] + 1./3. *
      amp[99] + amp[100] + 1./3. * amp[101] + amp[102] + amp[103] + 1./3. *
      amp[104] + 1./3. * amp[105] + amp[106] + 1./3. * amp[107] + amp[108] +
      1./3. * amp[109] + amp[110] + amp[111] + 1./3. * amp[112] + 1./3. *
      amp[113] + 1./3. * amp[114] + 1./3. * amp[115] + 1./3. * amp[116] + 1./3.
      * amp[117] + 1./3. * amp[118] + 1./3. * amp[119] + amp[120] + amp[121] +
      1./3. * amp[122] + 1./3. * amp[123] + amp[124] + amp[125] + amp[126] +
      1./3. * amp[127] + amp[128] + 1./3. * amp[129] + amp[130] + 1./3. *
      amp[131] + amp[132] + amp[133] + amp[134] + 1./3. * amp[135] + amp[136] +
      1./3. * amp[137] + amp[138] + 1./3. * amp[139] + amp[140] + amp[141] +
      amp[142] + amp[143] + 1./3. * amp[144] + amp[145] + amp[146] + 1./3. *
      amp[147] + amp[148] + 1./3. * amp[149] + 1./3. * amp[150] + amp[151] +
      1./3. * amp[152] + amp[153] + amp[154] + 1./3. * amp[155] + amp[156] +
      1./3. * amp[157] + 1./3. * amp[158] + amp[159] + 1./3. * amp[160] + 1./3.
      * amp[161] + 1./3. * amp[162] + 1./3. * amp[163] + 1./3. * amp[164] +
      1./3. * amp[165] + 1./3. * amp[166] + 1./3. * amp[167] + amp[168] +
      amp[169] + amp[170] + amp[171] + amp[172] + amp[173] + amp[174] +
      amp[175] + amp[176] + amp[177] + 1./3. * amp[178] + 1./3. * amp[179] +
      1./3. * amp[180] + 1./3. * amp[181] + amp[182] + 1./3. * amp[183] +
      amp[184] + amp[185] + 1./3. * amp[186] + amp[187] + 1./3. * amp[188] +
      1./3. * amp[189] + amp[190] + amp[191] + Complex<double> (0, 1) *
      amp[194] + 1./3. * amp[199] + amp[202] - Complex<double> (0, 1) *
      amp[203] + 1./3. * amp[206] + amp[207] + Complex<double> (0, 1) *
      amp[209] - Complex<double> (0, 1) * amp[211] + Complex<double> (0, 1) *
      amp[218] + 1./3. * amp[223] + amp[226] - Complex<double> (0, 1) *
      amp[227] + 1./3. * amp[230] + amp[231] + Complex<double> (0, 1) *
      amp[233] - Complex<double> (0, 1) * amp[235] + 1./3. * amp[242] + 1./3. *
      amp[243] + 1./3. * amp[245] + 1./3. * amp[246] + 1./3. * amp[248] + 1./3.
      * amp[249] + 1./3. * amp[251] + 1./3. * amp[252] + 1./3. * amp[254] +
      1./3. * amp[255] + 1./3. * amp[257] + 1./3. * amp[258] + 1./3. * amp[260]
      + 1./3. * amp[261] + Complex<double> (0, 1) * amp[262] + Complex<double>
      (0, 1) * amp[263] + amp[264] + Complex<double> (0, 1) * amp[266] +
      Complex<double> (0, 1) * amp[269] + amp[271] + Complex<double> (0, 1) *
      amp[272] + amp[273] + amp[274] + Complex<double> (0, 1) * amp[275] +
      amp[276] + Complex<double> (0, 1) * amp[278] + Complex<double> (0, 1) *
      amp[281] + amp[282] + amp[283] - Complex<double> (0, 1) * amp[284] -
      Complex<double> (0, 1) * amp[285] + amp[287] + 1./3. * amp[294] +
      amp[295] + Complex<double> (0, 1) * amp[297] + 1./3. * amp[299] + 1./3. *
      amp[304] + amp[305] + Complex<double> (0, 1) * amp[308] + amp[310] +
      amp[311] + Complex<double> (0, 1) * amp[312] + amp[313] - Complex<double>
      (0, 1) * amp[314] + amp[315] + 1./3. * amp[316] + amp[319] + amp[320] +
      Complex<double> (0, 1) * amp[321] + amp[325] - Complex<double> (0, 1) *
      amp[326] + amp[327]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[0][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P8_sm_bb_zzgbb::matrix_12_bbx_zzgbbx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 328;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[424] + 1./3. * amp[425] + amp[426] + 1./3. *
      amp[427] + amp[428] + 1./3. * amp[429] + amp[430] + amp[431] + 1./3. *
      amp[432] + 1./3. * amp[433] + amp[434] + 1./3. * amp[435] + amp[436] +
      1./3. * amp[437] + amp[438] + amp[439] + 1./3. * amp[440] + 1./3. *
      amp[441] + 1./3. * amp[442] + 1./3. * amp[443] + 1./3. * amp[444] + 1./3.
      * amp[445] + 1./3. * amp[446] + 1./3. * amp[447] + amp[448] + amp[449] +
      1./3. * amp[450] + 1./3. * amp[451] + amp[452] + amp[453] + amp[454] +
      1./3. * amp[455] + amp[456] + 1./3. * amp[457] + amp[458] + 1./3. *
      amp[459] + amp[460] + amp[461] + amp[462] + 1./3. * amp[463] + amp[464] +
      1./3. * amp[465] + amp[466] + 1./3. * amp[467] + amp[468] + amp[469] +
      amp[470] + amp[471] + 1./3. * amp[472] + amp[473] + amp[474] + 1./3. *
      amp[475] + amp[476] + 1./3. * amp[477] + 1./3. * amp[478] + amp[479] +
      1./3. * amp[480] + amp[481] + amp[482] + 1./3. * amp[483] + amp[484] +
      1./3. * amp[485] + 1./3. * amp[486] + amp[487] + 1./3. * amp[488] + 1./3.
      * amp[489] + 1./3. * amp[490] + 1./3. * amp[491] + 1./3. * amp[492] +
      1./3. * amp[493] + 1./3. * amp[494] + 1./3. * amp[495] + amp[496] +
      amp[497] + amp[498] + amp[499] + amp[500] + amp[501] + amp[502] +
      amp[503] + amp[504] + amp[505] + 1./3. * amp[506] + 1./3. * amp[507] +
      1./3. * amp[508] + 1./3. * amp[509] + amp[510] + 1./3. * amp[511] +
      amp[512] + amp[513] + 1./3. * amp[514] + amp[515] + 1./3. * amp[516] +
      1./3. * amp[517] + amp[518] + amp[519] + Complex<double> (0, 1) *
      amp[522] + 1./3. * amp[527] + amp[530] - Complex<double> (0, 1) *
      amp[531] + 1./3. * amp[534] + amp[535] + Complex<double> (0, 1) *
      amp[537] - Complex<double> (0, 1) * amp[539] + Complex<double> (0, 1) *
      amp[546] + 1./3. * amp[551] + amp[554] - Complex<double> (0, 1) *
      amp[555] + 1./3. * amp[558] + amp[559] + Complex<double> (0, 1) *
      amp[561] - Complex<double> (0, 1) * amp[563] + 1./3. * amp[570] + 1./3. *
      amp[571] + 1./3. * amp[573] + 1./3. * amp[574] + 1./3. * amp[576] + 1./3.
      * amp[577] + 1./3. * amp[579] + 1./3. * amp[580] + 1./3. * amp[582] +
      1./3. * amp[583] + 1./3. * amp[585] + 1./3. * amp[586] + 1./3. * amp[588]
      + 1./3. * amp[589] + Complex<double> (0, 1) * amp[590] + Complex<double>
      (0, 1) * amp[591] + amp[592] + Complex<double> (0, 1) * amp[594] +
      Complex<double> (0, 1) * amp[597] + amp[599] + Complex<double> (0, 1) *
      amp[600] + amp[601] + amp[602] + Complex<double> (0, 1) * amp[603] +
      amp[604] + Complex<double> (0, 1) * amp[606] + Complex<double> (0, 1) *
      amp[609] + amp[610] + amp[611] - Complex<double> (0, 1) * amp[612] -
      Complex<double> (0, 1) * amp[613] + amp[615] + 1./3. * amp[622] +
      amp[623] + Complex<double> (0, 1) * amp[625] + 1./3. * amp[627] + 1./3. *
      amp[632] + amp[633] + Complex<double> (0, 1) * amp[636] + amp[638] +
      amp[639] + Complex<double> (0, 1) * amp[640] + amp[641] - Complex<double>
      (0, 1) * amp[642] + amp[643] + 1./3. * amp[644] + amp[647] + amp[648] +
      Complex<double> (0, 1) * amp[649] + amp[653] - Complex<double> (0, 1) *
      amp[654] + amp[655]);
  jamp[1] = +1./2. * (-amp[328] - amp[329] - 1./3. * amp[330] - 1./3. *
      amp[331] - 1./3. * amp[332] - 1./3. * amp[333] - amp[334] - amp[335] -
      1./3. * amp[336] - 1./3. * amp[337] - amp[338] - 1./3. * amp[339] -
      amp[340] - 1./3. * amp[341] - amp[342] - amp[343] - amp[344] - amp[345] -
      1./3. * amp[346] - amp[347] - 1./3. * amp[348] - amp[349] - 1./3. *
      amp[350] - 1./3. * amp[351] - 1./3. * amp[352] - amp[353] - amp[354] -
      1./3. * amp[355] - amp[356] - 1./3. * amp[357] - 1./3. * amp[358] -
      amp[359] - 1./3. * amp[360] - 1./3. * amp[361] - amp[362] - amp[363] -
      amp[364] - amp[365] - 1./3. * amp[366] - 1./3. * amp[367] - 1./3. *
      amp[368] - amp[369] - amp[370] - 1./3. * amp[371] - 1./3. * amp[372] -
      1./3. * amp[373] - amp[374] - amp[375] - amp[424] - amp[425] - 1./3. *
      amp[426] - amp[427] - 1./3. * amp[428] - amp[429] - 1./3. * amp[430] -
      1./3. * amp[431] - amp[432] - amp[433] - 1./3. * amp[434] - amp[435] -
      1./3. * amp[436] - amp[437] - 1./3. * amp[438] - 1./3. * amp[439] -
      amp[440] - amp[441] - amp[442] - amp[443] - amp[444] - amp[445] -
      amp[446] - amp[447] - 1./3. * amp[448] - 1./3. * amp[449] - amp[450] -
      amp[451] - 1./3. * amp[452] - 1./3. * amp[453] - 1./3. * amp[454] -
      amp[455] - 1./3. * amp[456] - amp[457] - 1./3. * amp[458] - amp[459] -
      1./3. * amp[460] - 1./3. * amp[461] - 1./3. * amp[462] - amp[463] - 1./3.
      * amp[464] - amp[465] - 1./3. * amp[466] - amp[467] - 1./3. * amp[468] -
      1./3. * amp[469] - 1./3. * amp[470] - 1./3. * amp[471] - 1./3. * amp[520]
      - amp[523] - Complex<double> (0, 1) * amp[525] - amp[526] - amp[527] +
      Complex<double> (0, 1) * amp[528] - 1./3. * amp[529] - 1./3. * amp[530] -
      1./3. * amp[532] - amp[533] + Complex<double> (0, 1) * amp[536] - 1./3. *
      amp[538] - 1./3. * amp[540] - amp[541] - Complex<double> (0, 1) *
      amp[542] - amp[543] - 1./3. * amp[544] - amp[547] - Complex<double> (0,
      1) * amp[549] - amp[550] - amp[551] + Complex<double> (0, 1) * amp[552] -
      1./3. * amp[553] - 1./3. * amp[554] - 1./3. * amp[556] - amp[557] +
      Complex<double> (0, 1) * amp[560] - 1./3. * amp[562] - 1./3. * amp[564] -
      amp[565] - Complex<double> (0, 1) * amp[566] - amp[567] + Complex<double>
      (0, 1) * amp[568] + Complex<double> (0, 1) * amp[569] - amp[571] +
      Complex<double> (0, 1) * amp[572] - amp[573] - amp[574] + Complex<double>
      (0, 1) * amp[575] - amp[576] + Complex<double> (0, 1) * amp[578] +
      Complex<double> (0, 1) * amp[581] - amp[583] + Complex<double> (0, 1) *
      amp[584] - amp[585] - amp[586] + Complex<double> (0, 1) * amp[587] -
      1./3. * amp[614] - 1./3. * amp[615] - Complex<double> (0, 1) * amp[616] -
      Complex<double> (0, 1) * amp[617] - amp[618] - amp[622] - 1./3. *
      amp[623] + Complex<double> (0, 1) * amp[624] + Complex<double> (0, 1) *
      amp[628] - Complex<double> (0, 1) * amp[630] - amp[632] - 1./3. *
      amp[633] + Complex<double> (0, 1) * amp[637] - 1./3. * amp[638] - 1./3. *
      amp[641] - 1./3. * amp[643] + Complex<double> (0, 1) * amp[646] - 1./3. *
      amp[648] - Complex<double> (0, 1) * amp[651] - 1./3. * amp[653] - 1./3. *
      amp[655]);
  jamp[2] = +1./2. * (+1./3. * amp[328] + 1./3. * amp[329] + amp[330] +
      amp[331] + amp[332] + amp[333] + 1./3. * amp[334] + 1./3. * amp[335] +
      amp[336] + amp[337] + 1./3. * amp[338] + amp[339] + 1./3. * amp[340] +
      amp[341] + 1./3. * amp[342] + 1./3. * amp[343] + 1./3. * amp[344] + 1./3.
      * amp[345] + amp[346] + 1./3. * amp[347] + amp[348] + 1./3. * amp[349] +
      amp[350] + amp[351] + amp[352] + 1./3. * amp[353] + 1./3. * amp[354] +
      amp[355] + 1./3. * amp[356] + amp[357] + amp[358] + 1./3. * amp[359] +
      amp[360] + amp[361] + 1./3. * amp[362] + 1./3. * amp[363] + 1./3. *
      amp[364] + 1./3. * amp[365] + amp[366] + amp[367] + amp[368] + 1./3. *
      amp[369] + 1./3. * amp[370] + amp[371] + amp[372] + amp[373] + 1./3. *
      amp[374] + 1./3. * amp[375] + amp[376] + amp[377] + 1./3. * amp[378] +
      amp[379] + 1./3. * amp[380] + amp[381] + 1./3. * amp[382] + 1./3. *
      amp[383] + amp[384] + amp[385] + 1./3. * amp[386] + amp[387] + 1./3. *
      amp[388] + amp[389] + 1./3. * amp[390] + 1./3. * amp[391] + amp[392] +
      amp[393] + amp[394] + amp[395] + amp[396] + amp[397] + amp[398] +
      amp[399] + 1./3. * amp[400] + 1./3. * amp[401] + amp[402] + amp[403] +
      1./3. * amp[404] + 1./3. * amp[405] + 1./3. * amp[406] + amp[407] + 1./3.
      * amp[408] + amp[409] + 1./3. * amp[410] + amp[411] + 1./3. * amp[412] +
      1./3. * amp[413] + 1./3. * amp[414] + amp[415] + 1./3. * amp[416] +
      amp[417] + 1./3. * amp[418] + amp[419] + 1./3. * amp[420] + 1./3. *
      amp[421] + 1./3. * amp[422] + 1./3. * amp[423] + amp[520] + amp[521] -
      Complex<double> (0, 1) * amp[522] + 1./3. * amp[523] + 1./3. * amp[524] +
      1./3. * amp[526] + amp[529] + Complex<double> (0, 1) * amp[531] +
      amp[532] + 1./3. * amp[533] - Complex<double> (0, 1) * amp[537] +
      amp[538] + Complex<double> (0, 1) * amp[539] + amp[540] + 1./3. *
      amp[541] + 1./3. * amp[543] + amp[544] + amp[545] - Complex<double> (0,
      1) * amp[546] + 1./3. * amp[547] + 1./3. * amp[548] + 1./3. * amp[550] +
      amp[553] + Complex<double> (0, 1) * amp[555] + amp[556] + 1./3. *
      amp[557] - Complex<double> (0, 1) * amp[561] + amp[562] + Complex<double>
      (0, 1) * amp[563] + amp[564] + 1./3. * amp[565] + 1./3. * amp[567] -
      Complex<double> (0, 1) * amp[590] - Complex<double> (0, 1) * amp[591] +
      amp[593] - Complex<double> (0, 1) * amp[594] + amp[595] + amp[596] -
      Complex<double> (0, 1) * amp[597] + amp[598] - Complex<double> (0, 1) *
      amp[600] - Complex<double> (0, 1) * amp[603] + amp[605] - Complex<double>
      (0, 1) * amp[606] + amp[607] + amp[608] - Complex<double> (0, 1) *
      amp[609] + Complex<double> (0, 1) * amp[612] + Complex<double> (0, 1) *
      amp[613] + amp[614] + 1./3. * amp[618] + 1./3. * amp[619] + amp[620] +
      1./3. * amp[621] - Complex<double> (0, 1) * amp[625] + 1./3. * amp[626] +
      1./3. * amp[629] + 1./3. * amp[631] + amp[634] + 1./3. * amp[635] -
      Complex<double> (0, 1) * amp[636] - Complex<double> (0, 1) * amp[640] +
      Complex<double> (0, 1) * amp[642] + 1./3. * amp[645] - Complex<double>
      (0, 1) * amp[649] + 1./3. * amp[650] + 1./3. * amp[652] + Complex<double>
      (0, 1) * amp[654]);
  jamp[3] = +1./2. * (-1./3. * amp[376] - 1./3. * amp[377] - amp[378] - 1./3. *
      amp[379] - amp[380] - 1./3. * amp[381] - amp[382] - amp[383] - 1./3. *
      amp[384] - 1./3. * amp[385] - amp[386] - 1./3. * amp[387] - amp[388] -
      1./3. * amp[389] - amp[390] - amp[391] - 1./3. * amp[392] - 1./3. *
      amp[393] - 1./3. * amp[394] - 1./3. * amp[395] - 1./3. * amp[396] - 1./3.
      * amp[397] - 1./3. * amp[398] - 1./3. * amp[399] - amp[400] - amp[401] -
      1./3. * amp[402] - 1./3. * amp[403] - amp[404] - amp[405] - amp[406] -
      1./3. * amp[407] - amp[408] - 1./3. * amp[409] - amp[410] - 1./3. *
      amp[411] - amp[412] - amp[413] - amp[414] - 1./3. * amp[415] - amp[416] -
      1./3. * amp[417] - amp[418] - 1./3. * amp[419] - amp[420] - amp[421] -
      amp[422] - amp[423] - amp[472] - 1./3. * amp[473] - 1./3. * amp[474] -
      amp[475] - 1./3. * amp[476] - amp[477] - amp[478] - 1./3. * amp[479] -
      amp[480] - 1./3. * amp[481] - 1./3. * amp[482] - amp[483] - 1./3. *
      amp[484] - amp[485] - amp[486] - 1./3. * amp[487] - amp[488] - amp[489] -
      amp[490] - amp[491] - amp[492] - amp[493] - amp[494] - amp[495] - 1./3. *
      amp[496] - 1./3. * amp[497] - 1./3. * amp[498] - 1./3. * amp[499] - 1./3.
      * amp[500] - 1./3. * amp[501] - 1./3. * amp[502] - 1./3. * amp[503] -
      1./3. * amp[504] - 1./3. * amp[505] - amp[506] - amp[507] - amp[508] -
      amp[509] - 1./3. * amp[510] - amp[511] - 1./3. * amp[512] - 1./3. *
      amp[513] - amp[514] - 1./3. * amp[515] - amp[516] - amp[517] - 1./3. *
      amp[518] - 1./3. * amp[519] - 1./3. * amp[521] - amp[524] +
      Complex<double> (0, 1) * amp[525] - Complex<double> (0, 1) * amp[528] -
      amp[534] - 1./3. * amp[535] - Complex<double> (0, 1) * amp[536] +
      Complex<double> (0, 1) * amp[542] - 1./3. * amp[545] - amp[548] +
      Complex<double> (0, 1) * amp[549] - Complex<double> (0, 1) * amp[552] -
      amp[558] - 1./3. * amp[559] - Complex<double> (0, 1) * amp[560] +
      Complex<double> (0, 1) * amp[566] - Complex<double> (0, 1) * amp[568] -
      Complex<double> (0, 1) * amp[569] - amp[570] - Complex<double> (0, 1) *
      amp[572] - Complex<double> (0, 1) * amp[575] - amp[577] - Complex<double>
      (0, 1) * amp[578] - amp[579] - amp[580] - Complex<double> (0, 1) *
      amp[581] - amp[582] - Complex<double> (0, 1) * amp[584] - Complex<double>
      (0, 1) * amp[587] - amp[588] - amp[589] - 1./3. * amp[592] - 1./3. *
      amp[593] - 1./3. * amp[595] - 1./3. * amp[596] - 1./3. * amp[598] - 1./3.
      * amp[599] - 1./3. * amp[601] - 1./3. * amp[602] - 1./3. * amp[604] -
      1./3. * amp[605] - 1./3. * amp[607] - 1./3. * amp[608] - 1./3. * amp[610]
      - 1./3. * amp[611] + Complex<double> (0, 1) * amp[616] + Complex<double>
      (0, 1) * amp[617] - amp[619] - 1./3. * amp[620] - amp[621] -
      Complex<double> (0, 1) * amp[624] - amp[626] - amp[627] - Complex<double>
      (0, 1) * amp[628] - amp[629] + Complex<double> (0, 1) * amp[630] -
      amp[631] - 1./3. * amp[634] - amp[635] - Complex<double> (0, 1) *
      amp[637] - 1./3. * amp[639] - amp[644] - amp[645] - Complex<double> (0,
      1) * amp[646] - 1./3. * amp[647] - amp[650] + Complex<double> (0, 1) *
      amp[651] - amp[652]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[1][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P8_sm_bb_zzgbb::matrix_12_bxbx_zzgbxbx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 328;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[752] + 1./3. * amp[753] + amp[754] + 1./3. *
      amp[755] + amp[756] + 1./3. * amp[757] + amp[758] + amp[759] + 1./3. *
      amp[760] + 1./3. * amp[761] + amp[762] + 1./3. * amp[763] + amp[764] +
      1./3. * amp[765] + amp[766] + amp[767] + 1./3. * amp[768] + 1./3. *
      amp[769] + 1./3. * amp[770] + 1./3. * amp[771] + 1./3. * amp[772] + 1./3.
      * amp[773] + 1./3. * amp[774] + 1./3. * amp[775] + amp[776] + amp[777] +
      1./3. * amp[778] + 1./3. * amp[779] + amp[780] + amp[781] + amp[782] +
      1./3. * amp[783] + amp[784] + 1./3. * amp[785] + amp[786] + 1./3. *
      amp[787] + amp[788] + amp[789] + amp[790] + 1./3. * amp[791] + amp[792] +
      1./3. * amp[793] + amp[794] + 1./3. * amp[795] + amp[796] + amp[797] +
      amp[798] + amp[799] + 1./3. * amp[800] + amp[801] + amp[802] + 1./3. *
      amp[803] + amp[804] + 1./3. * amp[805] + 1./3. * amp[806] + amp[807] +
      1./3. * amp[808] + amp[809] + amp[810] + 1./3. * amp[811] + amp[812] +
      1./3. * amp[813] + 1./3. * amp[814] + amp[815] + 1./3. * amp[816] + 1./3.
      * amp[817] + 1./3. * amp[818] + 1./3. * amp[819] + 1./3. * amp[820] +
      1./3. * amp[821] + 1./3. * amp[822] + 1./3. * amp[823] + amp[824] +
      amp[825] + amp[826] + amp[827] + amp[828] + amp[829] + amp[830] +
      amp[831] + amp[832] + amp[833] + 1./3. * amp[834] + 1./3. * amp[835] +
      1./3. * amp[836] + 1./3. * amp[837] + amp[838] + 1./3. * amp[839] +
      amp[840] + amp[841] + 1./3. * amp[842] + amp[843] + 1./3. * amp[844] +
      1./3. * amp[845] + amp[846] + amp[847] + Complex<double> (0, 1) *
      amp[850] + 1./3. * amp[855] + amp[858] - Complex<double> (0, 1) *
      amp[859] + 1./3. * amp[862] + amp[863] + Complex<double> (0, 1) *
      amp[865] - Complex<double> (0, 1) * amp[867] + Complex<double> (0, 1) *
      amp[874] + 1./3. * amp[879] + amp[882] - Complex<double> (0, 1) *
      amp[883] + 1./3. * amp[886] + amp[887] + Complex<double> (0, 1) *
      amp[889] - Complex<double> (0, 1) * amp[891] + 1./3. * amp[898] + 1./3. *
      amp[899] + 1./3. * amp[901] + 1./3. * amp[902] + 1./3. * amp[904] + 1./3.
      * amp[905] + 1./3. * amp[907] + 1./3. * amp[908] + 1./3. * amp[910] +
      1./3. * amp[911] + 1./3. * amp[913] + 1./3. * amp[914] + 1./3. * amp[916]
      + 1./3. * amp[917] + Complex<double> (0, 1) * amp[918] + Complex<double>
      (0, 1) * amp[919] + amp[920] + Complex<double> (0, 1) * amp[922] +
      Complex<double> (0, 1) * amp[925] + amp[927] + Complex<double> (0, 1) *
      amp[928] + amp[929] + amp[930] + Complex<double> (0, 1) * amp[931] +
      amp[932] + Complex<double> (0, 1) * amp[934] + Complex<double> (0, 1) *
      amp[937] + amp[938] + amp[939] - Complex<double> (0, 1) * amp[940] -
      Complex<double> (0, 1) * amp[941] + amp[943] + 1./3. * amp[950] +
      amp[951] + Complex<double> (0, 1) * amp[953] + 1./3. * amp[955] + 1./3. *
      amp[960] + amp[961] + Complex<double> (0, 1) * amp[964] + amp[966] +
      amp[967] + Complex<double> (0, 1) * amp[968] + amp[969] - Complex<double>
      (0, 1) * amp[970] + amp[971] + 1./3. * amp[972] + amp[975] + amp[976] +
      Complex<double> (0, 1) * amp[977] + amp[981] - Complex<double> (0, 1) *
      amp[982] + amp[983]);
  jamp[1] = +1./2. * (-amp[656] - amp[657] - 1./3. * amp[658] - 1./3. *
      amp[659] - 1./3. * amp[660] - 1./3. * amp[661] - amp[662] - amp[663] -
      1./3. * amp[664] - 1./3. * amp[665] - amp[666] - 1./3. * amp[667] -
      amp[668] - 1./3. * amp[669] - amp[670] - amp[671] - amp[672] - amp[673] -
      1./3. * amp[674] - amp[675] - 1./3. * amp[676] - amp[677] - 1./3. *
      amp[678] - 1./3. * amp[679] - 1./3. * amp[680] - amp[681] - amp[682] -
      1./3. * amp[683] - amp[684] - 1./3. * amp[685] - 1./3. * amp[686] -
      amp[687] - 1./3. * amp[688] - 1./3. * amp[689] - amp[690] - amp[691] -
      amp[692] - amp[693] - 1./3. * amp[694] - 1./3. * amp[695] - 1./3. *
      amp[696] - amp[697] - amp[698] - 1./3. * amp[699] - 1./3. * amp[700] -
      1./3. * amp[701] - amp[702] - amp[703] - amp[752] - amp[753] - 1./3. *
      amp[754] - amp[755] - 1./3. * amp[756] - amp[757] - 1./3. * amp[758] -
      1./3. * amp[759] - amp[760] - amp[761] - 1./3. * amp[762] - amp[763] -
      1./3. * amp[764] - amp[765] - 1./3. * amp[766] - 1./3. * amp[767] -
      amp[768] - amp[769] - amp[770] - amp[771] - amp[772] - amp[773] -
      amp[774] - amp[775] - 1./3. * amp[776] - 1./3. * amp[777] - amp[778] -
      amp[779] - 1./3. * amp[780] - 1./3. * amp[781] - 1./3. * amp[782] -
      amp[783] - 1./3. * amp[784] - amp[785] - 1./3. * amp[786] - amp[787] -
      1./3. * amp[788] - 1./3. * amp[789] - 1./3. * amp[790] - amp[791] - 1./3.
      * amp[792] - amp[793] - 1./3. * amp[794] - amp[795] - 1./3. * amp[796] -
      1./3. * amp[797] - 1./3. * amp[798] - 1./3. * amp[799] - 1./3. * amp[848]
      - amp[851] - Complex<double> (0, 1) * amp[853] - amp[854] - amp[855] +
      Complex<double> (0, 1) * amp[856] - 1./3. * amp[857] - 1./3. * amp[858] -
      1./3. * amp[860] - amp[861] + Complex<double> (0, 1) * amp[864] - 1./3. *
      amp[866] - 1./3. * amp[868] - amp[869] - Complex<double> (0, 1) *
      amp[870] - amp[871] - 1./3. * amp[872] - amp[875] - Complex<double> (0,
      1) * amp[877] - amp[878] - amp[879] + Complex<double> (0, 1) * amp[880] -
      1./3. * amp[881] - 1./3. * amp[882] - 1./3. * amp[884] - amp[885] +
      Complex<double> (0, 1) * amp[888] - 1./3. * amp[890] - 1./3. * amp[892] -
      amp[893] - Complex<double> (0, 1) * amp[894] - amp[895] + Complex<double>
      (0, 1) * amp[896] + Complex<double> (0, 1) * amp[897] - amp[899] +
      Complex<double> (0, 1) * amp[900] - amp[901] - amp[902] + Complex<double>
      (0, 1) * amp[903] - amp[904] + Complex<double> (0, 1) * amp[906] +
      Complex<double> (0, 1) * amp[909] - amp[911] + Complex<double> (0, 1) *
      amp[912] - amp[913] - amp[914] + Complex<double> (0, 1) * amp[915] -
      1./3. * amp[942] - 1./3. * amp[943] - Complex<double> (0, 1) * amp[944] -
      Complex<double> (0, 1) * amp[945] - amp[946] - amp[950] - 1./3. *
      amp[951] + Complex<double> (0, 1) * amp[952] + Complex<double> (0, 1) *
      amp[956] - Complex<double> (0, 1) * amp[958] - amp[960] - 1./3. *
      amp[961] + Complex<double> (0, 1) * amp[965] - 1./3. * amp[966] - 1./3. *
      amp[969] - 1./3. * amp[971] + Complex<double> (0, 1) * amp[974] - 1./3. *
      amp[976] - Complex<double> (0, 1) * amp[979] - 1./3. * amp[981] - 1./3. *
      amp[983]);
  jamp[2] = +1./2. * (-1./3. * amp[704] - 1./3. * amp[705] - amp[706] - 1./3. *
      amp[707] - amp[708] - 1./3. * amp[709] - amp[710] - amp[711] - 1./3. *
      amp[712] - 1./3. * amp[713] - amp[714] - 1./3. * amp[715] - amp[716] -
      1./3. * amp[717] - amp[718] - amp[719] - 1./3. * amp[720] - 1./3. *
      amp[721] - 1./3. * amp[722] - 1./3. * amp[723] - 1./3. * amp[724] - 1./3.
      * amp[725] - 1./3. * amp[726] - 1./3. * amp[727] - amp[728] - amp[729] -
      1./3. * amp[730] - 1./3. * amp[731] - amp[732] - amp[733] - amp[734] -
      1./3. * amp[735] - amp[736] - 1./3. * amp[737] - amp[738] - 1./3. *
      amp[739] - amp[740] - amp[741] - amp[742] - 1./3. * amp[743] - amp[744] -
      1./3. * amp[745] - amp[746] - 1./3. * amp[747] - amp[748] - amp[749] -
      amp[750] - amp[751] - amp[800] - 1./3. * amp[801] - 1./3. * amp[802] -
      amp[803] - 1./3. * amp[804] - amp[805] - amp[806] - 1./3. * amp[807] -
      amp[808] - 1./3. * amp[809] - 1./3. * amp[810] - amp[811] - 1./3. *
      amp[812] - amp[813] - amp[814] - 1./3. * amp[815] - amp[816] - amp[817] -
      amp[818] - amp[819] - amp[820] - amp[821] - amp[822] - amp[823] - 1./3. *
      amp[824] - 1./3. * amp[825] - 1./3. * amp[826] - 1./3. * amp[827] - 1./3.
      * amp[828] - 1./3. * amp[829] - 1./3. * amp[830] - 1./3. * amp[831] -
      1./3. * amp[832] - 1./3. * amp[833] - amp[834] - amp[835] - amp[836] -
      amp[837] - 1./3. * amp[838] - amp[839] - 1./3. * amp[840] - 1./3. *
      amp[841] - amp[842] - 1./3. * amp[843] - amp[844] - amp[845] - 1./3. *
      amp[846] - 1./3. * amp[847] - 1./3. * amp[849] - amp[852] +
      Complex<double> (0, 1) * amp[853] - Complex<double> (0, 1) * amp[856] -
      amp[862] - 1./3. * amp[863] - Complex<double> (0, 1) * amp[864] +
      Complex<double> (0, 1) * amp[870] - 1./3. * amp[873] - amp[876] +
      Complex<double> (0, 1) * amp[877] - Complex<double> (0, 1) * amp[880] -
      amp[886] - 1./3. * amp[887] - Complex<double> (0, 1) * amp[888] +
      Complex<double> (0, 1) * amp[894] - Complex<double> (0, 1) * amp[896] -
      Complex<double> (0, 1) * amp[897] - amp[898] - Complex<double> (0, 1) *
      amp[900] - Complex<double> (0, 1) * amp[903] - amp[905] - Complex<double>
      (0, 1) * amp[906] - amp[907] - amp[908] - Complex<double> (0, 1) *
      amp[909] - amp[910] - Complex<double> (0, 1) * amp[912] - Complex<double>
      (0, 1) * amp[915] - amp[916] - amp[917] - 1./3. * amp[920] - 1./3. *
      amp[921] - 1./3. * amp[923] - 1./3. * amp[924] - 1./3. * amp[926] - 1./3.
      * amp[927] - 1./3. * amp[929] - 1./3. * amp[930] - 1./3. * amp[932] -
      1./3. * amp[933] - 1./3. * amp[935] - 1./3. * amp[936] - 1./3. * amp[938]
      - 1./3. * amp[939] + Complex<double> (0, 1) * amp[944] + Complex<double>
      (0, 1) * amp[945] - amp[947] - 1./3. * amp[948] - amp[949] -
      Complex<double> (0, 1) * amp[952] - amp[954] - amp[955] - Complex<double>
      (0, 1) * amp[956] - amp[957] + Complex<double> (0, 1) * amp[958] -
      amp[959] - 1./3. * amp[962] - amp[963] - Complex<double> (0, 1) *
      amp[965] - 1./3. * amp[967] - amp[972] - amp[973] - Complex<double> (0,
      1) * amp[974] - 1./3. * amp[975] - amp[978] + Complex<double> (0, 1) *
      amp[979] - amp[980]);
  jamp[3] = +1./2. * (+1./3. * amp[656] + 1./3. * amp[657] + amp[658] +
      amp[659] + amp[660] + amp[661] + 1./3. * amp[662] + 1./3. * amp[663] +
      amp[664] + amp[665] + 1./3. * amp[666] + amp[667] + 1./3. * amp[668] +
      amp[669] + 1./3. * amp[670] + 1./3. * amp[671] + 1./3. * amp[672] + 1./3.
      * amp[673] + amp[674] + 1./3. * amp[675] + amp[676] + 1./3. * amp[677] +
      amp[678] + amp[679] + amp[680] + 1./3. * amp[681] + 1./3. * amp[682] +
      amp[683] + 1./3. * amp[684] + amp[685] + amp[686] + 1./3. * amp[687] +
      amp[688] + amp[689] + 1./3. * amp[690] + 1./3. * amp[691] + 1./3. *
      amp[692] + 1./3. * amp[693] + amp[694] + amp[695] + amp[696] + 1./3. *
      amp[697] + 1./3. * amp[698] + amp[699] + amp[700] + amp[701] + 1./3. *
      amp[702] + 1./3. * amp[703] + amp[704] + amp[705] + 1./3. * amp[706] +
      amp[707] + 1./3. * amp[708] + amp[709] + 1./3. * amp[710] + 1./3. *
      amp[711] + amp[712] + amp[713] + 1./3. * amp[714] + amp[715] + 1./3. *
      amp[716] + amp[717] + 1./3. * amp[718] + 1./3. * amp[719] + amp[720] +
      amp[721] + amp[722] + amp[723] + amp[724] + amp[725] + amp[726] +
      amp[727] + 1./3. * amp[728] + 1./3. * amp[729] + amp[730] + amp[731] +
      1./3. * amp[732] + 1./3. * amp[733] + 1./3. * amp[734] + amp[735] + 1./3.
      * amp[736] + amp[737] + 1./3. * amp[738] + amp[739] + 1./3. * amp[740] +
      1./3. * amp[741] + 1./3. * amp[742] + amp[743] + 1./3. * amp[744] +
      amp[745] + 1./3. * amp[746] + amp[747] + 1./3. * amp[748] + 1./3. *
      amp[749] + 1./3. * amp[750] + 1./3. * amp[751] + amp[848] + amp[849] -
      Complex<double> (0, 1) * amp[850] + 1./3. * amp[851] + 1./3. * amp[852] +
      1./3. * amp[854] + amp[857] + Complex<double> (0, 1) * amp[859] +
      amp[860] + 1./3. * amp[861] - Complex<double> (0, 1) * amp[865] +
      amp[866] + Complex<double> (0, 1) * amp[867] + amp[868] + 1./3. *
      amp[869] + 1./3. * amp[871] + amp[872] + amp[873] - Complex<double> (0,
      1) * amp[874] + 1./3. * amp[875] + 1./3. * amp[876] + 1./3. * amp[878] +
      amp[881] + Complex<double> (0, 1) * amp[883] + amp[884] + 1./3. *
      amp[885] - Complex<double> (0, 1) * amp[889] + amp[890] + Complex<double>
      (0, 1) * amp[891] + amp[892] + 1./3. * amp[893] + 1./3. * amp[895] -
      Complex<double> (0, 1) * amp[918] - Complex<double> (0, 1) * amp[919] +
      amp[921] - Complex<double> (0, 1) * amp[922] + amp[923] + amp[924] -
      Complex<double> (0, 1) * amp[925] + amp[926] - Complex<double> (0, 1) *
      amp[928] - Complex<double> (0, 1) * amp[931] + amp[933] - Complex<double>
      (0, 1) * amp[934] + amp[935] + amp[936] - Complex<double> (0, 1) *
      amp[937] + Complex<double> (0, 1) * amp[940] + Complex<double> (0, 1) *
      amp[941] + amp[942] + 1./3. * amp[946] + 1./3. * amp[947] + amp[948] +
      1./3. * amp[949] - Complex<double> (0, 1) * amp[953] + 1./3. * amp[954] +
      1./3. * amp[957] + 1./3. * amp[959] + amp[962] + 1./3. * amp[963] -
      Complex<double> (0, 1) * amp[964] - Complex<double> (0, 1) * amp[968] +
      Complex<double> (0, 1) * amp[970] + 1./3. * amp[973] - Complex<double>
      (0, 1) * amp[977] + 1./3. * amp[978] + 1./3. * amp[980] + Complex<double>
      (0, 1) * amp[982]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[2][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}


}  // end namespace PY8MEs_namespace

