//==========================================================================
// This file has been automatically generated for Pythia 8 by
// MadGraph5_aMC@NLO v. 2.6.0, 2017-08-16
// By the MadGraph5_aMC@NLO Development Team
// Visit launchpad.net/madgraph5 and amcatnlo.web.cern.ch
//==========================================================================

#include "PY8MEs_R11_P164_sm_qq_wpwmqq.h"
#include "HelAmps_sm.h"

using namespace Pythia8_sm; 

namespace PY8MEs_namespace 
{
//==========================================================================
// Class member functions for calculating the matrix elements for
// Process: u u > w+ w- u u WEIGHTED<=6 @11
// Process: c c > w+ w- c c WEIGHTED<=6 @11
// Process: u u~ > w+ w- u u~ WEIGHTED<=6 @11
// Process: c c~ > w+ w- c c~ WEIGHTED<=6 @11
// Process: d d > w+ w- d d WEIGHTED<=6 @11
// Process: s s > w+ w- s s WEIGHTED<=6 @11
// Process: d d~ > w+ w- d d~ WEIGHTED<=6 @11
// Process: s s~ > w+ w- s s~ WEIGHTED<=6 @11
// Process: u~ u~ > w+ w- u~ u~ WEIGHTED<=6 @11
// Process: c~ c~ > w+ w- c~ c~ WEIGHTED<=6 @11
// Process: d~ d~ > w+ w- d~ d~ WEIGHTED<=6 @11
// Process: s~ s~ > w+ w- s~ s~ WEIGHTED<=6 @11
// Process: u d > w+ w- u d WEIGHTED<=6 @11
// Process: c s > w+ w- c s WEIGHTED<=6 @11
// Process: u u~ > w+ w- d d~ WEIGHTED<=6 @11
// Process: c c~ > w+ w- s s~ WEIGHTED<=6 @11
// Process: u d~ > w+ w- u d~ WEIGHTED<=6 @11
// Process: c s~ > w+ w- c s~ WEIGHTED<=6 @11
// Process: d u~ > w+ w- d u~ WEIGHTED<=6 @11
// Process: s c~ > w+ w- s c~ WEIGHTED<=6 @11
// Process: d d~ > w+ w- u u~ WEIGHTED<=6 @11
// Process: s s~ > w+ w- c c~ WEIGHTED<=6 @11
// Process: u~ d~ > w+ w- u~ d~ WEIGHTED<=6 @11
// Process: c~ s~ > w+ w- c~ s~ WEIGHTED<=6 @11
// Process: u u > w+ w+ d d WEIGHTED<=6 @11
// Process: c c > w+ w+ s s WEIGHTED<=6 @11
// Process: u d~ > w+ w+ d u~ WEIGHTED<=6 @11
// Process: c s~ > w+ w+ s c~ WEIGHTED<=6 @11
// Process: d d > w- w- u u WEIGHTED<=6 @11
// Process: s s > w- w- c c WEIGHTED<=6 @11
// Process: d u~ > w- w- u d~ WEIGHTED<=6 @11
// Process: s c~ > w- w- c s~ WEIGHTED<=6 @11
// Process: u~ u~ > w- w- d~ d~ WEIGHTED<=6 @11
// Process: c~ c~ > w- w- s~ s~ WEIGHTED<=6 @11
// Process: d~ d~ > w+ w+ u~ u~ WEIGHTED<=6 @11
// Process: s~ s~ > w+ w+ c~ c~ WEIGHTED<=6 @11
// Process: u c > w+ w- u c WEIGHTED<=6 @11
// Process: u s > w+ w- u s WEIGHTED<=6 @11
// Process: c d > w+ w- c d WEIGHTED<=6 @11
// Process: u u~ > w+ w- c c~ WEIGHTED<=6 @11
// Process: c c~ > w+ w- u u~ WEIGHTED<=6 @11
// Process: u u~ > w+ w- s s~ WEIGHTED<=6 @11
// Process: c c~ > w+ w- d d~ WEIGHTED<=6 @11
// Process: u c~ > w+ w- u c~ WEIGHTED<=6 @11
// Process: c u~ > w+ w- c u~ WEIGHTED<=6 @11
// Process: u s~ > w+ w- u s~ WEIGHTED<=6 @11
// Process: c d~ > w+ w- c d~ WEIGHTED<=6 @11
// Process: d s > w+ w- d s WEIGHTED<=6 @11
// Process: d c~ > w+ w- d c~ WEIGHTED<=6 @11
// Process: s u~ > w+ w- s u~ WEIGHTED<=6 @11
// Process: d d~ > w+ w- c c~ WEIGHTED<=6 @11
// Process: s s~ > w+ w- u u~ WEIGHTED<=6 @11
// Process: d d~ > w+ w- s s~ WEIGHTED<=6 @11
// Process: s s~ > w+ w- d d~ WEIGHTED<=6 @11
// Process: d s~ > w+ w- d s~ WEIGHTED<=6 @11
// Process: s d~ > w+ w- s d~ WEIGHTED<=6 @11
// Process: u~ c~ > w+ w- u~ c~ WEIGHTED<=6 @11
// Process: u~ s~ > w+ w- u~ s~ WEIGHTED<=6 @11
// Process: c~ d~ > w+ w- c~ d~ WEIGHTED<=6 @11
// Process: d~ s~ > w+ w- d~ s~ WEIGHTED<=6 @11
// Process: u c > w+ w+ d s WEIGHTED<=6 @11
// Process: u d~ > w+ w+ s c~ WEIGHTED<=6 @11
// Process: c s~ > w+ w+ d u~ WEIGHTED<=6 @11
// Process: u s~ > w+ w+ d c~ WEIGHTED<=6 @11
// Process: c d~ > w+ w+ s u~ WEIGHTED<=6 @11
// Process: d s > w- w- u c WEIGHTED<=6 @11
// Process: d u~ > w- w- c s~ WEIGHTED<=6 @11
// Process: s c~ > w- w- u d~ WEIGHTED<=6 @11
// Process: d c~ > w- w- u s~ WEIGHTED<=6 @11
// Process: s u~ > w- w- c d~ WEIGHTED<=6 @11
// Process: u~ c~ > w- w- d~ s~ WEIGHTED<=6 @11
// Process: d~ s~ > w+ w+ u~ c~ WEIGHTED<=6 @11
// Process: u s > w+ w- c d WEIGHTED<=6 @11
// Process: c d > w+ w- u s WEIGHTED<=6 @11
// Process: u c~ > w+ w- d s~ WEIGHTED<=6 @11
// Process: c u~ > w+ w- s d~ WEIGHTED<=6 @11
// Process: u d~ > w+ w- c s~ WEIGHTED<=6 @11
// Process: c s~ > w+ w- u d~ WEIGHTED<=6 @11
// Process: d u~ > w+ w- s c~ WEIGHTED<=6 @11
// Process: s c~ > w+ w- d u~ WEIGHTED<=6 @11
// Process: d s~ > w+ w- u c~ WEIGHTED<=6 @11
// Process: s d~ > w+ w- c u~ WEIGHTED<=6 @11
// Process: u~ s~ > w+ w- c~ d~ WEIGHTED<=6 @11
// Process: c~ d~ > w+ w- u~ s~ WEIGHTED<=6 @11

// Exception class
class PY8MEs_R11_P164_sm_qq_wpwmqqException : public exception
{
  virtual const char * what() const throw()
  {
    return "Exception in class 'PY8MEs_R11_P164_sm_qq_wpwmqq'."; 
  }
}
PY8MEs_R11_P164_sm_qq_wpwmqq_exception; 

std::set<int> PY8MEs_R11_P164_sm_qq_wpwmqq::s_channel_proc = std::set<int>
    (createset<int> ());

int PY8MEs_R11_P164_sm_qq_wpwmqq::helicities[ncomb][nexternal] = {{-1, -1, -1,
    -1, -1, -1}, {-1, -1, -1, -1, -1, 1}, {-1, -1, -1, -1, 1, -1}, {-1, -1, -1,
    -1, 1, 1}, {-1, -1, -1, 0, -1, -1}, {-1, -1, -1, 0, -1, 1}, {-1, -1, -1, 0,
    1, -1}, {-1, -1, -1, 0, 1, 1}, {-1, -1, -1, 1, -1, -1}, {-1, -1, -1, 1, -1,
    1}, {-1, -1, -1, 1, 1, -1}, {-1, -1, -1, 1, 1, 1}, {-1, -1, 0, -1, -1, -1},
    {-1, -1, 0, -1, -1, 1}, {-1, -1, 0, -1, 1, -1}, {-1, -1, 0, -1, 1, 1}, {-1,
    -1, 0, 0, -1, -1}, {-1, -1, 0, 0, -1, 1}, {-1, -1, 0, 0, 1, -1}, {-1, -1,
    0, 0, 1, 1}, {-1, -1, 0, 1, -1, -1}, {-1, -1, 0, 1, -1, 1}, {-1, -1, 0, 1,
    1, -1}, {-1, -1, 0, 1, 1, 1}, {-1, -1, 1, -1, -1, -1}, {-1, -1, 1, -1, -1,
    1}, {-1, -1, 1, -1, 1, -1}, {-1, -1, 1, -1, 1, 1}, {-1, -1, 1, 0, -1, -1},
    {-1, -1, 1, 0, -1, 1}, {-1, -1, 1, 0, 1, -1}, {-1, -1, 1, 0, 1, 1}, {-1,
    -1, 1, 1, -1, -1}, {-1, -1, 1, 1, -1, 1}, {-1, -1, 1, 1, 1, -1}, {-1, -1,
    1, 1, 1, 1}, {-1, 1, -1, -1, -1, -1}, {-1, 1, -1, -1, -1, 1}, {-1, 1, -1,
    -1, 1, -1}, {-1, 1, -1, -1, 1, 1}, {-1, 1, -1, 0, -1, -1}, {-1, 1, -1, 0,
    -1, 1}, {-1, 1, -1, 0, 1, -1}, {-1, 1, -1, 0, 1, 1}, {-1, 1, -1, 1, -1,
    -1}, {-1, 1, -1, 1, -1, 1}, {-1, 1, -1, 1, 1, -1}, {-1, 1, -1, 1, 1, 1},
    {-1, 1, 0, -1, -1, -1}, {-1, 1, 0, -1, -1, 1}, {-1, 1, 0, -1, 1, -1}, {-1,
    1, 0, -1, 1, 1}, {-1, 1, 0, 0, -1, -1}, {-1, 1, 0, 0, -1, 1}, {-1, 1, 0, 0,
    1, -1}, {-1, 1, 0, 0, 1, 1}, {-1, 1, 0, 1, -1, -1}, {-1, 1, 0, 1, -1, 1},
    {-1, 1, 0, 1, 1, -1}, {-1, 1, 0, 1, 1, 1}, {-1, 1, 1, -1, -1, -1}, {-1, 1,
    1, -1, -1, 1}, {-1, 1, 1, -1, 1, -1}, {-1, 1, 1, -1, 1, 1}, {-1, 1, 1, 0,
    -1, -1}, {-1, 1, 1, 0, -1, 1}, {-1, 1, 1, 0, 1, -1}, {-1, 1, 1, 0, 1, 1},
    {-1, 1, 1, 1, -1, -1}, {-1, 1, 1, 1, -1, 1}, {-1, 1, 1, 1, 1, -1}, {-1, 1,
    1, 1, 1, 1}, {1, -1, -1, -1, -1, -1}, {1, -1, -1, -1, -1, 1}, {1, -1, -1,
    -1, 1, -1}, {1, -1, -1, -1, 1, 1}, {1, -1, -1, 0, -1, -1}, {1, -1, -1, 0,
    -1, 1}, {1, -1, -1, 0, 1, -1}, {1, -1, -1, 0, 1, 1}, {1, -1, -1, 1, -1,
    -1}, {1, -1, -1, 1, -1, 1}, {1, -1, -1, 1, 1, -1}, {1, -1, -1, 1, 1, 1},
    {1, -1, 0, -1, -1, -1}, {1, -1, 0, -1, -1, 1}, {1, -1, 0, -1, 1, -1}, {1,
    -1, 0, -1, 1, 1}, {1, -1, 0, 0, -1, -1}, {1, -1, 0, 0, -1, 1}, {1, -1, 0,
    0, 1, -1}, {1, -1, 0, 0, 1, 1}, {1, -1, 0, 1, -1, -1}, {1, -1, 0, 1, -1,
    1}, {1, -1, 0, 1, 1, -1}, {1, -1, 0, 1, 1, 1}, {1, -1, 1, -1, -1, -1}, {1,
    -1, 1, -1, -1, 1}, {1, -1, 1, -1, 1, -1}, {1, -1, 1, -1, 1, 1}, {1, -1, 1,
    0, -1, -1}, {1, -1, 1, 0, -1, 1}, {1, -1, 1, 0, 1, -1}, {1, -1, 1, 0, 1,
    1}, {1, -1, 1, 1, -1, -1}, {1, -1, 1, 1, -1, 1}, {1, -1, 1, 1, 1, -1}, {1,
    -1, 1, 1, 1, 1}, {1, 1, -1, -1, -1, -1}, {1, 1, -1, -1, -1, 1}, {1, 1, -1,
    -1, 1, -1}, {1, 1, -1, -1, 1, 1}, {1, 1, -1, 0, -1, -1}, {1, 1, -1, 0, -1,
    1}, {1, 1, -1, 0, 1, -1}, {1, 1, -1, 0, 1, 1}, {1, 1, -1, 1, -1, -1}, {1,
    1, -1, 1, -1, 1}, {1, 1, -1, 1, 1, -1}, {1, 1, -1, 1, 1, 1}, {1, 1, 0, -1,
    -1, -1}, {1, 1, 0, -1, -1, 1}, {1, 1, 0, -1, 1, -1}, {1, 1, 0, -1, 1, 1},
    {1, 1, 0, 0, -1, -1}, {1, 1, 0, 0, -1, 1}, {1, 1, 0, 0, 1, -1}, {1, 1, 0,
    0, 1, 1}, {1, 1, 0, 1, -1, -1}, {1, 1, 0, 1, -1, 1}, {1, 1, 0, 1, 1, -1},
    {1, 1, 0, 1, 1, 1}, {1, 1, 1, -1, -1, -1}, {1, 1, 1, -1, -1, 1}, {1, 1, 1,
    -1, 1, -1}, {1, 1, 1, -1, 1, 1}, {1, 1, 1, 0, -1, -1}, {1, 1, 1, 0, -1, 1},
    {1, 1, 1, 0, 1, -1}, {1, 1, 1, 0, 1, 1}, {1, 1, 1, 1, -1, -1}, {1, 1, 1, 1,
    -1, 1}, {1, 1, 1, 1, 1, -1}, {1, 1, 1, 1, 1, 1}};

// Normalization factors the various processes
// Denominators: spins, colors and identical particles
int PY8MEs_R11_P164_sm_qq_wpwmqq::denom_colors[nprocesses] = {9, 9, 9, 9, 9, 9,
    9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
    9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9};
int PY8MEs_R11_P164_sm_qq_wpwmqq::denom_hels[nprocesses] = {4, 4, 4, 4, 4, 4,
    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4};
int PY8MEs_R11_P164_sm_qq_wpwmqq::denom_iden[nprocesses] = {2, 1, 2, 1, 2, 2,
    1, 1, 1, 1, 1, 1, 4, 2, 4, 2, 4, 4, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1};

//--------------------------------------------------------------------------
// Color config initialization
void PY8MEs_R11_P164_sm_qq_wpwmqq::initColorConfigs() 
{
  color_configs = vector < vec_vec_int > (); 
  jamp_nc_relative_power = vector < vec_int > (); 

  // Color flows of process Process: u u > w+ w- u u WEIGHTED<=6 @11
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #1
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(0)(1)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 

  // Color flows of process Process: u u~ > w+ w- u u~ WEIGHTED<=6 @11
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #1
  color_configs[1].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[1].push_back(0); 

  // Color flows of process Process: d d > w+ w- d d WEIGHTED<=6 @11
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[2].push_back(vec_int(createvector<int>
      (1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #1
  color_configs[2].push_back(vec_int(createvector<int>
      (2)(0)(1)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[2].push_back(0); 

  // Color flows of process Process: d d~ > w+ w- d d~ WEIGHTED<=6 @11
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[3].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #1
  color_configs[3].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[3].push_back(0); 

  // Color flows of process Process: u~ u~ > w+ w- u~ u~ WEIGHTED<=6 @11
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[4].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)));
  jamp_nc_relative_power[4].push_back(0); 
  // JAMP #1
  color_configs[4].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(2)(0)(1)));
  jamp_nc_relative_power[4].push_back(0); 

  // Color flows of process Process: d~ d~ > w+ w- d~ d~ WEIGHTED<=6 @11
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[5].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)));
  jamp_nc_relative_power[5].push_back(0); 
  // JAMP #1
  color_configs[5].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(2)(0)(1)));
  jamp_nc_relative_power[5].push_back(0); 

  // Color flows of process Process: u d > w+ w- u d WEIGHTED<=6 @11
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[6].push_back(vec_int(createvector<int>
      (1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[6].push_back(0); 
  // JAMP #1
  color_configs[6].push_back(vec_int(createvector<int>
      (2)(0)(1)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[6].push_back(0); 

  // Color flows of process Process: u u~ > w+ w- d d~ WEIGHTED<=6 @11
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[7].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[7].push_back(0); 
  // JAMP #1
  color_configs[7].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[7].push_back(0); 

  // Color flows of process Process: u d~ > w+ w- u d~ WEIGHTED<=6 @11
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[8].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[8].push_back(0); 
  // JAMP #1
  color_configs[8].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[8].push_back(0); 

  // Color flows of process Process: d u~ > w+ w- d u~ WEIGHTED<=6 @11
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[9].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[9].push_back(0); 
  // JAMP #1
  color_configs[9].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[9].push_back(0); 

  // Color flows of process Process: d d~ > w+ w- u u~ WEIGHTED<=6 @11
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[10].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[10].push_back(0); 
  // JAMP #1
  color_configs[10].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[10].push_back(0); 

  // Color flows of process Process: u~ d~ > w+ w- u~ d~ WEIGHTED<=6 @11
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[11].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)));
  jamp_nc_relative_power[11].push_back(0); 
  // JAMP #1
  color_configs[11].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(2)(0)(1)));
  jamp_nc_relative_power[11].push_back(0); 

  // Color flows of process Process: u u > w+ w+ d d WEIGHTED<=6 @11
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[12].push_back(vec_int(createvector<int>
      (1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[12].push_back(0); 
  // JAMP #1
  color_configs[12].push_back(vec_int(createvector<int>
      (2)(0)(1)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[12].push_back(0); 

  // Color flows of process Process: u d~ > w+ w+ d u~ WEIGHTED<=6 @11
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[13].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[13].push_back(0); 
  // JAMP #1
  color_configs[13].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[13].push_back(0); 

  // Color flows of process Process: d d > w- w- u u WEIGHTED<=6 @11
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[14].push_back(vec_int(createvector<int>
      (1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[14].push_back(0); 
  // JAMP #1
  color_configs[14].push_back(vec_int(createvector<int>
      (2)(0)(1)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[14].push_back(0); 

  // Color flows of process Process: d u~ > w- w- u d~ WEIGHTED<=6 @11
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[15].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[15].push_back(0); 
  // JAMP #1
  color_configs[15].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[15].push_back(0); 

  // Color flows of process Process: u~ u~ > w- w- d~ d~ WEIGHTED<=6 @11
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[16].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)));
  jamp_nc_relative_power[16].push_back(0); 
  // JAMP #1
  color_configs[16].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(2)(0)(1)));
  jamp_nc_relative_power[16].push_back(0); 

  // Color flows of process Process: d~ d~ > w+ w+ u~ u~ WEIGHTED<=6 @11
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[17].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)));
  jamp_nc_relative_power[17].push_back(0); 
  // JAMP #1
  color_configs[17].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(2)(0)(1)));
  jamp_nc_relative_power[17].push_back(0); 

  // Color flows of process Process: u c > w+ w- u c WEIGHTED<=6 @11
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[18].push_back(vec_int(createvector<int>
      (1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[18].push_back(-1); 
  // JAMP #1
  color_configs[18].push_back(vec_int(createvector<int>
      (2)(0)(1)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[18].push_back(0); 

  // Color flows of process Process: u s > w+ w- u s WEIGHTED<=6 @11
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[19].push_back(vec_int(createvector<int>
      (1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[19].push_back(-1); 
  // JAMP #1
  color_configs[19].push_back(vec_int(createvector<int>
      (2)(0)(1)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[19].push_back(0); 

  // Color flows of process Process: u u~ > w+ w- c c~ WEIGHTED<=6 @11
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[20].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[20].push_back(-1); 
  // JAMP #1
  color_configs[20].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[20].push_back(0); 

  // Color flows of process Process: u u~ > w+ w- s s~ WEIGHTED<=6 @11
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[21].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[21].push_back(-1); 
  // JAMP #1
  color_configs[21].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[21].push_back(0); 

  // Color flows of process Process: u c~ > w+ w- u c~ WEIGHTED<=6 @11
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[22].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[22].push_back(0); 
  // JAMP #1
  color_configs[22].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[22].push_back(-1); 

  // Color flows of process Process: u s~ > w+ w- u s~ WEIGHTED<=6 @11
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[23].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[23].push_back(0); 
  // JAMP #1
  color_configs[23].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[23].push_back(-1); 

  // Color flows of process Process: d s > w+ w- d s WEIGHTED<=6 @11
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[24].push_back(vec_int(createvector<int>
      (1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[24].push_back(-1); 
  // JAMP #1
  color_configs[24].push_back(vec_int(createvector<int>
      (2)(0)(1)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[24].push_back(0); 

  // Color flows of process Process: d c~ > w+ w- d c~ WEIGHTED<=6 @11
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[25].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[25].push_back(0); 
  // JAMP #1
  color_configs[25].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[25].push_back(-1); 

  // Color flows of process Process: d d~ > w+ w- c c~ WEIGHTED<=6 @11
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[26].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[26].push_back(-1); 
  // JAMP #1
  color_configs[26].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[26].push_back(0); 

  // Color flows of process Process: d d~ > w+ w- s s~ WEIGHTED<=6 @11
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[27].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[27].push_back(-1); 
  // JAMP #1
  color_configs[27].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[27].push_back(0); 

  // Color flows of process Process: d s~ > w+ w- d s~ WEIGHTED<=6 @11
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[28].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[28].push_back(0); 
  // JAMP #1
  color_configs[28].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[28].push_back(-1); 

  // Color flows of process Process: u~ c~ > w+ w- u~ c~ WEIGHTED<=6 @11
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[29].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)));
  jamp_nc_relative_power[29].push_back(-1); 
  // JAMP #1
  color_configs[29].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(2)(0)(1)));
  jamp_nc_relative_power[29].push_back(0); 

  // Color flows of process Process: u~ s~ > w+ w- u~ s~ WEIGHTED<=6 @11
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[30].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)));
  jamp_nc_relative_power[30].push_back(-1); 
  // JAMP #1
  color_configs[30].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(2)(0)(1)));
  jamp_nc_relative_power[30].push_back(0); 

  // Color flows of process Process: d~ s~ > w+ w- d~ s~ WEIGHTED<=6 @11
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[31].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)));
  jamp_nc_relative_power[31].push_back(-1); 
  // JAMP #1
  color_configs[31].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(2)(0)(1)));
  jamp_nc_relative_power[31].push_back(0); 

  // Color flows of process Process: u c > w+ w+ d s WEIGHTED<=6 @11
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[32].push_back(vec_int(createvector<int>
      (1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[32].push_back(-1); 
  // JAMP #1
  color_configs[32].push_back(vec_int(createvector<int>
      (2)(0)(1)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[32].push_back(0); 

  // Color flows of process Process: u d~ > w+ w+ s c~ WEIGHTED<=6 @11
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[33].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[33].push_back(-1); 
  // JAMP #1
  color_configs[33].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[33].push_back(0); 

  // Color flows of process Process: u s~ > w+ w+ d c~ WEIGHTED<=6 @11
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[34].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[34].push_back(0); 
  // JAMP #1
  color_configs[34].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[34].push_back(-1); 

  // Color flows of process Process: d s > w- w- u c WEIGHTED<=6 @11
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[35].push_back(vec_int(createvector<int>
      (1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[35].push_back(-1); 
  // JAMP #1
  color_configs[35].push_back(vec_int(createvector<int>
      (2)(0)(1)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[35].push_back(0); 

  // Color flows of process Process: d u~ > w- w- c s~ WEIGHTED<=6 @11
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[36].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[36].push_back(-1); 
  // JAMP #1
  color_configs[36].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[36].push_back(0); 

  // Color flows of process Process: d c~ > w- w- u s~ WEIGHTED<=6 @11
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[37].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[37].push_back(0); 
  // JAMP #1
  color_configs[37].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[37].push_back(-1); 

  // Color flows of process Process: u~ c~ > w- w- d~ s~ WEIGHTED<=6 @11
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[38].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)));
  jamp_nc_relative_power[38].push_back(-1); 
  // JAMP #1
  color_configs[38].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(2)(0)(1)));
  jamp_nc_relative_power[38].push_back(0); 

  // Color flows of process Process: d~ s~ > w+ w+ u~ c~ WEIGHTED<=6 @11
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[39].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)));
  jamp_nc_relative_power[39].push_back(-1); 
  // JAMP #1
  color_configs[39].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(2)(0)(1)));
  jamp_nc_relative_power[39].push_back(0); 

  // Color flows of process Process: u s > w+ w- c d WEIGHTED<=6 @11
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[40].push_back(vec_int(createvector<int>
      (1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[40].push_back(0); 
  // JAMP #1
  color_configs[40].push_back(vec_int(createvector<int>
      (2)(0)(1)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[40].push_back(-1); 

  // Color flows of process Process: u c~ > w+ w- d s~ WEIGHTED<=6 @11
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[41].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[41].push_back(0); 
  // JAMP #1
  color_configs[41].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[41].push_back(-1); 

  // Color flows of process Process: u d~ > w+ w- c s~ WEIGHTED<=6 @11
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[42].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[42].push_back(-1); 
  // JAMP #1
  color_configs[42].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[42].push_back(0); 

  // Color flows of process Process: d u~ > w+ w- s c~ WEIGHTED<=6 @11
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[43].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[43].push_back(-1); 
  // JAMP #1
  color_configs[43].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[43].push_back(0); 

  // Color flows of process Process: d s~ > w+ w- u c~ WEIGHTED<=6 @11
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[44].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[44].push_back(0); 
  // JAMP #1
  color_configs[44].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[44].push_back(-1); 

  // Color flows of process Process: u~ s~ > w+ w- c~ d~ WEIGHTED<=6 @11
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[45].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)));
  jamp_nc_relative_power[45].push_back(0); 
  // JAMP #1
  color_configs[45].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(2)(0)(1)));
  jamp_nc_relative_power[45].push_back(-1); 
}

//--------------------------------------------------------------------------
// Destructor.
PY8MEs_R11_P164_sm_qq_wpwmqq::~PY8MEs_R11_P164_sm_qq_wpwmqq() 
{
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    delete[] p[i]; 
    p[i] = NULL; 
  }
}

//--------------------------------------------------------------------------
// Invert the permutation mapping
vector<int> PY8MEs_R11_P164_sm_qq_wpwmqq::invert_mapping(vector<int> mapping) 
{
  vector<int> inverted_mapping; 
  for (unsigned int i = 0; i < mapping.size(); i++ )
  {
    for (unsigned int j = 0; j < mapping.size(); j++ )
    {
      if (mapping[j] == ((int)i))
      {
        inverted_mapping.push_back(j); 
        break; 
      }
    }
  }
  return inverted_mapping; 
}

//--------------------------------------------------------------------------
// Return the list of possible helicity configurations
vector < vec_int > PY8MEs_R11_P164_sm_qq_wpwmqq::getHelicityConfigs(vector<int>
    permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(ncomb, vector<int> (nexternal, 0)); 
  for (unsigned int ihel = 0; ihel < ncomb; ihel++ )
  {
    for(unsigned int j = 0; j < nexternal; j++ )
    {
      res[ihel][chosenPerm[j]] = helicities[ihel][j]; 
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Return the list of possible color configurations
vector < vec_int > PY8MEs_R11_P164_sm_qq_wpwmqq::getColorConfigs(int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(color_configs[chosenProcID].size(), vector<int>
      (nexternal * 2, 0));
  for (unsigned int icol = 0; icol < color_configs[chosenProcID].size(); icol++
      )
  {
    for(unsigned int j = 0; j < (2 * nexternal); j++ )
    {
      res[icol][chosenPerm[j/2] * 2 + j%2] =
          color_configs[chosenProcID][icol][j];
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Get JAMP relative N_c power
int PY8MEs_R11_P164_sm_qq_wpwmqq::getColorFlowRelativeNCPower(int
    color_flow_ID, int specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return jamp_nc_relative_power[chosenProcID][color_flow_ID]; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity ID -> Helicity Config
vector<int> PY8MEs_R11_P164_sm_qq_wpwmqq::getHelicityConfigForID(int hel_ID,
    vector<int> permutation)
{
  if (hel_ID < 0 || hel_ID >= ncomb)
  {
    cerr <<  "Error in function 'getHelicityConfigForID' of class" << 
    " 'PY8MEs_R11_P164_sm_qq_wpwmqq': Specified helicity ID '" << 
    hel_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R11_P164_sm_qq_wpwmqq_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(nexternal, 0); 
  for (unsigned int j = 0; j < nexternal; j++ )
  {
    res[chosenPerm[j]] = helicities[hel_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity Config -> Helicity ID
int PY8MEs_R11_P164_sm_qq_wpwmqq::getHelicityIDForConfig(vector<int>
    hel_config, vector<int> permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  int user_ihel = -1; 
  if (hel_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < ncomb; i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < nexternal; j++ )
      {
        if (helicities[i][chosenPerm[j]] != hel_config[j])
        {
          found = false; 
          break; 
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_ihel = i; 
        break; 
      }
    }
    if (user_ihel == -1)
    {
      cerr <<  "Error in function 'getHelicityIDForConfig' of class" << 
      " 'PY8MEs_R11_P164_sm_qq_wpwmqq': Specified helicity" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R11_P164_sm_qq_wpwmqq_exception; 
    }
  }
  return user_ihel; 
}


//--------------------------------------------------------------------------
// Implements the map Color ID -> Color Config
vector<int> PY8MEs_R11_P164_sm_qq_wpwmqq::getColorConfigForID(int color_ID, int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < 0 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getColorConfigForID' of class" << 
    " 'PY8MEs_R11_P164_sm_qq_wpwmqq': Specified color ID '" << 
    color_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R11_P164_sm_qq_wpwmqq_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(color_configs[chosenProcID][color_ID].size(), 0); 
  for (unsigned int j = 0; j < (2 * nexternal); j++ )
  {
    res[chosenPerm[j/2] * 2 + j%2] = color_configs[chosenProcID][color_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Color Config -> Color ID
int PY8MEs_R11_P164_sm_qq_wpwmqq::getColorIDForConfig(vector<int> color_config,
    int specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = -1; 
  if (color_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < color_configs[chosenProcID].size(); i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < (nexternal * 2); j++ )
      {

        // If colorless then make sure it matches
        // The little arithmetics in the color index is just
        // the permutation applies on the particle list which is
        // twice smaller since each particle can have two color indices.
        if (color_config[j] == 0)
        {
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] != 0)
          {
            found = false; 
            break; 
          }
          // Otherwise check that the color linked position matches
        }
        else
        {
          int color_linked_pos = -1; 
          // Find the other end of the line in the user color config
          for (unsigned int k = 0; k < (nexternal * 2); k++ )
          {
            if (k == j)
              continue; 
            if (color_config[j] == color_config[k])
            {
              color_linked_pos = k; 
              break; 
            }
          }
          if (color_linked_pos == -1)
          {
            cerr <<  "Error in function 'getColorIDForConfig' of class" << 
            " 'PY8MEs_R11_P164_sm_qq_wpwmqq': A color line could " << 
            " not be closed." << endl; 
            throw PY8MEs_R11_P164_sm_qq_wpwmqq_exception; 
          }
          // Now check whether the color line matches
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] !=
              color_configs[chosenProcID][i][chosenPerm[color_linked_pos/2] * 2
              + color_linked_pos%2])
          {
            found = false; 
            break; 
          }
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_icol = i; 
        break; 
      }
    }

    if (user_icol == -1)
    {
      cerr <<  "Error in function 'getColorIDForConfig' of class" << 
      " 'PY8MEs_R11_P164_sm_qq_wpwmqq': Specified color" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R11_P164_sm_qq_wpwmqq_exception; 
    }
  }
  return user_icol; 
}

//--------------------------------------------------------------------------
// Returns all result previously computed in SigmaKin
vector < vec_double > PY8MEs_R11_P164_sm_qq_wpwmqq::getAllResults(int
    specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return all_results[chosenProcID]; 
}

//--------------------------------------------------------------------------
// Returns a result previously computed in SigmaKin for a specific helicity
// and color ID. -1 means avg and summed over that characteristic.
double PY8MEs_R11_P164_sm_qq_wpwmqq::getResult(int helicity_ID, int color_ID,
    int specify_proc_ID)
{
  if (helicity_ID < - 1 || helicity_ID >= ncomb)
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R11_P164_sm_qq_wpwmqq': Specified helicity ID '" << 
    helicity_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R11_P164_sm_qq_wpwmqq_exception; 
  }
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < - 1 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R11_P164_sm_qq_wpwmqq': Specified color ID '" << 
    color_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R11_P164_sm_qq_wpwmqq_exception; 
  }
  return all_results[chosenProcID][helicity_ID + 1][color_ID + 1]; 
}

//--------------------------------------------------------------------------
// Check for the availability of the requested process and if available,
// If available, this returns the corresponding permutation and Proc_ID to use.
// If not available, this returns a negative Proc_ID.
pair < vector<int> , int > PY8MEs_R11_P164_sm_qq_wpwmqq::static_getPY8ME(vector<int> initial_pdgs, vector<int> final_pdgs, set<int> schannels) 
{

  // Not available return value
  pair < vector<int> , int > NA(vector<int> (), -1); 

  // Check if s-channel requirements match
  if (nreq_s_channels > 0)
  {
    if (schannels != s_channel_proc)
      return NA; 
  }
  else
  {
    if (schannels.size() != 0)
      return NA; 
  }

  // Check number of final state particles
  if (final_pdgs.size() != (nexternal - ninitial))
    return NA; 

  // Check number of initial state particles
  if (initial_pdgs.size() != ninitial)
    return NA; 

  // List of processes available in this class
  const int nprocs = 152; 
  const int proc_IDS[nprocs] = {0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7,
      8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13, 14, 14, 15, 15, 16, 16, 17,
      17, 18, 19, 19, 20, 20, 21, 21, 22, 22, 23, 23, 24, 25, 25, 26, 26, 27,
      27, 28, 28, 29, 30, 30, 31, 32, 33, 33, 34, 34, 35, 36, 36, 37, 37, 38,
      39, 40, 40, 41, 41, 42, 42, 43, 43, 44, 44, 45, 45, 1, 1, 3, 3, 6, 6, 7,
      7, 8, 8, 9, 9, 10, 10, 11, 11, 13, 13, 15, 15, 18, 19, 19, 20, 20, 21,
      21, 22, 22, 23, 23, 24, 25, 25, 26, 26, 27, 27, 28, 28, 29, 30, 30, 31,
      32, 33, 33, 34, 34, 35, 36, 36, 37, 37, 38, 39, 40, 40, 41, 41, 42, 42,
      43, 43, 44, 44, 45, 45};
  const int in_pdgs[nprocs][ninitial] = {{2, 2}, {4, 4}, {2, -2}, {4, -4}, {1,
      1}, {3, 3}, {1, -1}, {3, -3}, {-2, -2}, {-4, -4}, {-1, -1}, {-3, -3}, {2,
      1}, {4, 3}, {2, -2}, {4, -4}, {2, -1}, {4, -3}, {1, -2}, {3, -4}, {1,
      -1}, {3, -3}, {-2, -1}, {-4, -3}, {2, 2}, {4, 4}, {2, -1}, {4, -3}, {1,
      1}, {3, 3}, {1, -2}, {3, -4}, {-2, -2}, {-4, -4}, {-1, -1}, {-3, -3}, {2,
      4}, {2, 3}, {4, 1}, {2, -2}, {4, -4}, {2, -2}, {4, -4}, {2, -4}, {4, -2},
      {2, -3}, {4, -1}, {1, 3}, {1, -4}, {3, -2}, {1, -1}, {3, -3}, {1, -1},
      {3, -3}, {1, -3}, {3, -1}, {-2, -4}, {-2, -3}, {-4, -1}, {-1, -3}, {2,
      4}, {2, -1}, {4, -3}, {2, -3}, {4, -1}, {1, 3}, {1, -2}, {3, -4}, {1,
      -4}, {3, -2}, {-2, -4}, {-1, -3}, {2, 3}, {4, 1}, {2, -4}, {4, -2}, {2,
      -1}, {4, -3}, {1, -2}, {3, -4}, {1, -3}, {3, -1}, {-2, -3}, {-4, -1},
      {-2, 2}, {-4, 4}, {-1, 1}, {-3, 3}, {1, 2}, {3, 4}, {-2, 2}, {-4, 4},
      {-1, 2}, {-3, 4}, {-2, 1}, {-4, 3}, {-1, 1}, {-3, 3}, {-1, -2}, {-3, -4},
      {-1, 2}, {-3, 4}, {-2, 1}, {-4, 3}, {4, 2}, {3, 2}, {1, 4}, {-2, 2}, {-4,
      4}, {-2, 2}, {-4, 4}, {-4, 2}, {-2, 4}, {-3, 2}, {-1, 4}, {3, 1}, {-4,
      1}, {-2, 3}, {-1, 1}, {-3, 3}, {-1, 1}, {-3, 3}, {-3, 1}, {-1, 3}, {-4,
      -2}, {-3, -2}, {-1, -4}, {-3, -1}, {4, 2}, {-1, 2}, {-3, 4}, {-3, 2},
      {-1, 4}, {3, 1}, {-2, 1}, {-4, 3}, {-4, 1}, {-2, 3}, {-4, -2}, {-3, -1},
      {3, 2}, {1, 4}, {-4, 2}, {-2, 4}, {-1, 2}, {-3, 4}, {-2, 1}, {-4, 3},
      {-3, 1}, {-1, 3}, {-3, -2}, {-1, -4}};
  const int out_pdgs[nprocs][nexternal - ninitial] = {{24, -24, 2, 2}, {24,
      -24, 4, 4}, {24, -24, 2, -2}, {24, -24, 4, -4}, {24, -24, 1, 1}, {24,
      -24, 3, 3}, {24, -24, 1, -1}, {24, -24, 3, -3}, {24, -24, -2, -2}, {24,
      -24, -4, -4}, {24, -24, -1, -1}, {24, -24, -3, -3}, {24, -24, 2, 1}, {24,
      -24, 4, 3}, {24, -24, 1, -1}, {24, -24, 3, -3}, {24, -24, 2, -1}, {24,
      -24, 4, -3}, {24, -24, 1, -2}, {24, -24, 3, -4}, {24, -24, 2, -2}, {24,
      -24, 4, -4}, {24, -24, -2, -1}, {24, -24, -4, -3}, {24, 24, 1, 1}, {24,
      24, 3, 3}, {24, 24, 1, -2}, {24, 24, 3, -4}, {-24, -24, 2, 2}, {-24, -24,
      4, 4}, {-24, -24, 2, -1}, {-24, -24, 4, -3}, {-24, -24, -1, -1}, {-24,
      -24, -3, -3}, {24, 24, -2, -2}, {24, 24, -4, -4}, {24, -24, 2, 4}, {24,
      -24, 2, 3}, {24, -24, 4, 1}, {24, -24, 4, -4}, {24, -24, 2, -2}, {24,
      -24, 3, -3}, {24, -24, 1, -1}, {24, -24, 2, -4}, {24, -24, 4, -2}, {24,
      -24, 2, -3}, {24, -24, 4, -1}, {24, -24, 1, 3}, {24, -24, 1, -4}, {24,
      -24, 3, -2}, {24, -24, 4, -4}, {24, -24, 2, -2}, {24, -24, 3, -3}, {24,
      -24, 1, -1}, {24, -24, 1, -3}, {24, -24, 3, -1}, {24, -24, -2, -4}, {24,
      -24, -2, -3}, {24, -24, -4, -1}, {24, -24, -1, -3}, {24, 24, 1, 3}, {24,
      24, 3, -4}, {24, 24, 1, -2}, {24, 24, 1, -4}, {24, 24, 3, -2}, {-24, -24,
      2, 4}, {-24, -24, 4, -3}, {-24, -24, 2, -1}, {-24, -24, 2, -3}, {-24,
      -24, 4, -1}, {-24, -24, -1, -3}, {24, 24, -2, -4}, {24, -24, 4, 1}, {24,
      -24, 2, 3}, {24, -24, 1, -3}, {24, -24, 3, -1}, {24, -24, 4, -3}, {24,
      -24, 2, -1}, {24, -24, 3, -4}, {24, -24, 1, -2}, {24, -24, 2, -4}, {24,
      -24, 4, -2}, {24, -24, -4, -1}, {24, -24, -2, -3}, {24, -24, 2, -2}, {24,
      -24, 4, -4}, {24, -24, 1, -1}, {24, -24, 3, -3}, {24, -24, 2, 1}, {24,
      -24, 4, 3}, {24, -24, 1, -1}, {24, -24, 3, -3}, {24, -24, 2, -1}, {24,
      -24, 4, -3}, {24, -24, 1, -2}, {24, -24, 3, -4}, {24, -24, 2, -2}, {24,
      -24, 4, -4}, {24, -24, -2, -1}, {24, -24, -4, -3}, {24, 24, 1, -2}, {24,
      24, 3, -4}, {-24, -24, 2, -1}, {-24, -24, 4, -3}, {24, -24, 2, 4}, {24,
      -24, 2, 3}, {24, -24, 4, 1}, {24, -24, 4, -4}, {24, -24, 2, -2}, {24,
      -24, 3, -3}, {24, -24, 1, -1}, {24, -24, 2, -4}, {24, -24, 4, -2}, {24,
      -24, 2, -3}, {24, -24, 4, -1}, {24, -24, 1, 3}, {24, -24, 1, -4}, {24,
      -24, 3, -2}, {24, -24, 4, -4}, {24, -24, 2, -2}, {24, -24, 3, -3}, {24,
      -24, 1, -1}, {24, -24, 1, -3}, {24, -24, 3, -1}, {24, -24, -2, -4}, {24,
      -24, -2, -3}, {24, -24, -4, -1}, {24, -24, -1, -3}, {24, 24, 1, 3}, {24,
      24, 3, -4}, {24, 24, 1, -2}, {24, 24, 1, -4}, {24, 24, 3, -2}, {-24, -24,
      2, 4}, {-24, -24, 4, -3}, {-24, -24, 2, -1}, {-24, -24, 2, -3}, {-24,
      -24, 4, -1}, {-24, -24, -1, -3}, {24, 24, -2, -4}, {24, -24, 4, 1}, {24,
      -24, 2, 3}, {24, -24, 1, -3}, {24, -24, 3, -1}, {24, -24, 4, -3}, {24,
      -24, 2, -1}, {24, -24, 3, -4}, {24, -24, 1, -2}, {24, -24, 2, -4}, {24,
      -24, 4, -2}, {24, -24, -4, -1}, {24, -24, -2, -3}};

  bool in_pdgs_used[ninitial]; 
  bool out_pdgs_used[nexternal - ninitial]; 
  for(unsigned int i = 0; i < nprocs; i++ )
  {
    int permutations[nexternal]; 

    // Reinitialize initial state look-up variables
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      in_pdgs_used[j] = false; 
      permutations[j] = -1; 
    }
    // Look for initial state matches
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      for(unsigned int k = 0; k < ninitial; k++ )
      {
        // Make sure it has not been used already
        if (in_pdgs_used[k])
          continue; 
        if (initial_pdgs[k] == in_pdgs[i][j])
        {
          permutations[j] = k; 
          in_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[ninitial - 1] == -1)
      continue; 

    // Reinitialize final state look-up variables
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      out_pdgs_used[j] = false; 
      permutations[ninitial + j] = -1; 
    }
    // Look for final state matches
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      for(unsigned int k = 0; k < (nexternal - ninitial); k++ )
      {
        // Make sure it has not been used already
        if (out_pdgs_used[k])
          continue; 
        if (final_pdgs[k] == out_pdgs[i][j])
        {
          permutations[ninitial + j] = ninitial + k; 
          out_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[ninitial + j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[nexternal - 1] == -1)
      continue; 

    // Return process found
    return pair < vector<int> , int > (vector<int> (permutations, permutations
        + nexternal), proc_IDS[i]);
  }

  // No process found
  return NA; 
}

//--------------------------------------------------------------------------
// Set momenta
void PY8MEs_R11_P164_sm_qq_wpwmqq::setMomenta(vector < vec_double >
    momenta_picked)
{
  if (momenta_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setMomenta' of class" << 
    " 'PY8MEs_R11_P164_sm_qq_wpwmqq': Incorrect number of" << 
    " momenta specified." << endl; 
    throw PY8MEs_R11_P164_sm_qq_wpwmqq_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    if (momenta_picked[i].size() != 4)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R11_P164_sm_qq_wpwmqq': Incorrect number of" << 
      " momenta components specified." << endl; 
      throw PY8MEs_R11_P164_sm_qq_wpwmqq_exception; 
    }
    if (momenta_picked[i][0] < 0.0)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R11_P164_sm_qq_wpwmqq': A momentum was specified" << 
      " with negative energy. Check conventions." << endl; 
      throw PY8MEs_R11_P164_sm_qq_wpwmqq_exception; 
    }
    for (unsigned int j = 0; j < 4; j++ )
    {
      p[i][j] = momenta_picked[i][j]; 
    }
  }
}

//--------------------------------------------------------------------------
// Set color configuration to use. An empty vector means sum over all.
void PY8MEs_R11_P164_sm_qq_wpwmqq::setColors(vector<int> colors_picked)
{
  if (colors_picked.size() == 0)
  {
    user_colors = vector<int> (); 
    return; 
  }
  if (colors_picked.size() != (2 * nexternal))
  {
    cerr <<  "Error in function 'setColors' of class" << 
    " 'PY8MEs_R11_P164_sm_qq_wpwmqq': Incorrect number" << 
    " of colors specified." << endl; 
    throw PY8MEs_R11_P164_sm_qq_wpwmqq_exception; 
  }
  user_colors = vector<int> ((2 * nexternal), 0); 
  for(unsigned int i = 0; i < (2 * nexternal); i++ )
  {
    user_colors[i] = colors_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the helicity configuration to use. Am empty vector means sum over all.
void PY8MEs_R11_P164_sm_qq_wpwmqq::setHelicities(vector<int> helicities_picked) 
{
  if (helicities_picked.size() != nexternal)
  {
    if (helicities_picked.size() == 0)
    {
      user_helicities = vector<int> (); 
      return; 
    }
    cerr <<  "Error in function 'setHelicities' of class" << 
    " 'PY8MEs_R11_P164_sm_qq_wpwmqq': Incorrect number" << 
    " of helicities specified." << endl; 
    throw PY8MEs_R11_P164_sm_qq_wpwmqq_exception; 
  }
  user_helicities = vector<int> (nexternal, 0); 
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    user_helicities[i] = helicities_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the permutation to use (will apply to momenta, colors and helicities)
void PY8MEs_R11_P164_sm_qq_wpwmqq::setPermutation(vector<int> perm_picked) 
{
  if (perm_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setPermutations' of class" << 
    " 'PY8MEs_R11_P164_sm_qq_wpwmqq': Incorrect number" << 
    " of permutations specified." << endl; 
    throw PY8MEs_R11_P164_sm_qq_wpwmqq_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    perm[i] = perm_picked[i]; 
  }
}

// Set the proc_ID to use
void PY8MEs_R11_P164_sm_qq_wpwmqq::setProcID(int procID_picked) 
{
  proc_ID = procID_picked; 
}

//--------------------------------------------------------------------------
// Initialize process.

void PY8MEs_R11_P164_sm_qq_wpwmqq::initProc() 
{

  // Initialize flags
  include_symmetry_factors = true; 
  include_helicity_averaging_factors = true; 
  include_color_averaging_factors = true; 

  // Initialize vectors.
  perm = vector<int> (nexternal, 0); 
  user_colors = vector<int> (2 * nexternal, 0); 
  user_helicities = vector<int> (nexternal, 0); 
  p = vector < double * > (); 
  for (unsigned int i = 0; i < nexternal; i++ )
  {
    p.push_back(new double[4]); 
  }
  initColorConfigs(); 
  // Synchronize local variables dependent on the model with the active model.
  mME = vector<double> (nexternal, 0.); 
  syncProcModelParams(); 
  jamp2 = vector < vec_double > (46); 
  jamp2[0] = vector<double> (2, 0.); 
  jamp2[1] = vector<double> (2, 0.); 
  jamp2[2] = vector<double> (2, 0.); 
  jamp2[3] = vector<double> (2, 0.); 
  jamp2[4] = vector<double> (2, 0.); 
  jamp2[5] = vector<double> (2, 0.); 
  jamp2[6] = vector<double> (2, 0.); 
  jamp2[7] = vector<double> (2, 0.); 
  jamp2[8] = vector<double> (2, 0.); 
  jamp2[9] = vector<double> (2, 0.); 
  jamp2[10] = vector<double> (2, 0.); 
  jamp2[11] = vector<double> (2, 0.); 
  jamp2[12] = vector<double> (2, 0.); 
  jamp2[13] = vector<double> (2, 0.); 
  jamp2[14] = vector<double> (2, 0.); 
  jamp2[15] = vector<double> (2, 0.); 
  jamp2[16] = vector<double> (2, 0.); 
  jamp2[17] = vector<double> (2, 0.); 
  jamp2[18] = vector<double> (2, 0.); 
  jamp2[19] = vector<double> (2, 0.); 
  jamp2[20] = vector<double> (2, 0.); 
  jamp2[21] = vector<double> (2, 0.); 
  jamp2[22] = vector<double> (2, 0.); 
  jamp2[23] = vector<double> (2, 0.); 
  jamp2[24] = vector<double> (2, 0.); 
  jamp2[25] = vector<double> (2, 0.); 
  jamp2[26] = vector<double> (2, 0.); 
  jamp2[27] = vector<double> (2, 0.); 
  jamp2[28] = vector<double> (2, 0.); 
  jamp2[29] = vector<double> (2, 0.); 
  jamp2[30] = vector<double> (2, 0.); 
  jamp2[31] = vector<double> (2, 0.); 
  jamp2[32] = vector<double> (2, 0.); 
  jamp2[33] = vector<double> (2, 0.); 
  jamp2[34] = vector<double> (2, 0.); 
  jamp2[35] = vector<double> (2, 0.); 
  jamp2[36] = vector<double> (2, 0.); 
  jamp2[37] = vector<double> (2, 0.); 
  jamp2[38] = vector<double> (2, 0.); 
  jamp2[39] = vector<double> (2, 0.); 
  jamp2[40] = vector<double> (2, 0.); 
  jamp2[41] = vector<double> (2, 0.); 
  jamp2[42] = vector<double> (2, 0.); 
  jamp2[43] = vector<double> (2, 0.); 
  jamp2[44] = vector<double> (2, 0.); 
  jamp2[45] = vector<double> (2, 0.); 
  all_results = vector < vec_vec_double > (46); 
  // The first entry is always the color or helicity avg/summed matrix element.
  all_results[0] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[1] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[2] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[3] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[4] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[5] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[6] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[7] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[8] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[9] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[10] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[11] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[12] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[13] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[14] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[15] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[16] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[17] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[18] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[19] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[20] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[21] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[22] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[23] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[24] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[25] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[26] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[27] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[28] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[29] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[30] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[31] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[32] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[33] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[34] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[35] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[36] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[37] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[38] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[39] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[40] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[41] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[42] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[43] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[44] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[45] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
}

// Synchronize local variables of the process that depend on the model
// parameters
void PY8MEs_R11_P164_sm_qq_wpwmqq::syncProcModelParams() 
{

  // Instantiate the model class and set parameters that stay fixed during run
  mME[0] = pars->ZERO; 
  mME[1] = pars->ZERO; 
  mME[2] = pars->mdl_MW; 
  mME[3] = pars->mdl_MW; 
  mME[4] = pars->ZERO; 
  mME[5] = pars->ZERO; 
}

//--------------------------------------------------------------------------
// Setter allowing to force particular values for the external masses
void PY8MEs_R11_P164_sm_qq_wpwmqq::setMasses(vec_double external_masses) 
{

  if (external_masses.size() != mME.size())
  {
    cerr <<  "Error in function 'setMasses' of class" << 
    " 'PY8MEs_R11_P164_sm_qq_wpwmqq': Incorrect number of" << 
    " masses specified." << endl; 
    throw PY8MEs_R11_P164_sm_qq_wpwmqq_exception; 
  }
  for (unsigned int j = 0; j < mME.size(); j++ )
  {
    mME[j] = external_masses[perm[j]]; 
  }
}

//--------------------------------------------------------------------------
// Getter accessing external masses with the correct ordering
vector<double> PY8MEs_R11_P164_sm_qq_wpwmqq::getMasses() 
{

  vec_double external_masses; 
  vector<int> invertedPerm; 
  invertedPerm = invert_mapping(perm); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    external_masses.push_back(mME[invertedPerm[i]]); 
  }
  return external_masses; 

}


// Set all values of the external masses to float(-mode) where mode can be
// 0 : Mass taken from the model
// 1 : Mass taken from p_i^2 if not massless to begin with
// 2 : Mass always taken from p_i^2.
void PY8MEs_R11_P164_sm_qq_wpwmqq::setExternalMassesMode(int mode) 
{
  if (mode != 0 && mode != 1 && mode != 2)
  {
    cerr <<  "Error in function 'setExternalMassesMode' of class" << 
    " 'PY8MEs_R11_P164_sm_qq_wpwmqq': Incorrect mode selected :" << mode << 
    ". It must be either 0, 1 or 2" << endl; 
    throw PY8MEs_R11_P164_sm_qq_wpwmqq_exception; 
  }
  if (mode == 0)
  {
    syncProcModelParams(); 
  }
  else if (mode == 1)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      if (mME[j] != pars->ZERO)
      {
        mME[j] = -1.0; 
      }
    }
  }
  else if (mode == 2)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      mME[j] = -1.0; 
    }
  }
}

//--------------------------------------------------------------------------
// Evaluate the squared matrix element.

double PY8MEs_R11_P164_sm_qq_wpwmqq::sigmaKin() 
{
  // Set the parameters which change event by event
  pars->setDependentParameters(); 
  pars->setDependentCouplings(); 
  // Reset color flows
  for(int i = 0; i < 2; i++ )
    jamp2[0][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[1][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[2][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[3][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[4][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[5][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[6][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[7][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[8][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[9][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[10][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[11][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[12][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[13][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[14][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[15][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[16][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[17][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[18][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[19][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[20][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[21][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[22][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[23][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[24][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[25][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[26][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[27][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[28][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[29][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[30][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[31][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[32][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[33][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[34][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[35][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[36][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[37][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[38][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[39][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[40][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[41][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[42][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[43][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[44][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[45][i] = 0.; 

  // Save previous values of mME
  vector<double> saved_mME(mME.size(), 0.0); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (mME[i] < 0.0)
    {
      saved_mME[i] = mME[i]; 
      mME[i] = sqrt(abs(pow(p[perm[i]][0], 2) - 
      (pow(p[perm[i]][1], 2) + pow(p[perm[i]][2], 2) + pow(p[perm[i]][3],
          2))));
    }
  }

  // Local variables and constants
  const int max_tries = 10; 
  vector < vec_bool > goodhel(nprocesses, vec_bool(ncomb, false)); 
  vec_int ntry(nprocesses, 0); 
  double t = 0.; 
  double result = 0.; 

  if (ntry[proc_ID] <= max_tries)
    ntry[proc_ID] = ntry[proc_ID] + 1; 

  // Find which helicity configuration is asked for
  // -1 indicates one wants to sum over helicities
  int user_ihel = getHelicityIDForConfig(user_helicities); 

  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = getColorIDForConfig(user_colors); 

  // Reset the list of results that will be recomputed here
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      all_results[proc_ID][ihel + 1][icolor + 1] = 0.; 
    }
  }

  // Calculate the matrix element for all helicities
  // unless already detected as vanishing
  for(int ihel = 0; ihel < ncomb; ihel++ )
  {
    // Skip helicity if already detected as vanishing
    if ((ntry[proc_ID] >= max_tries) && !goodhel[proc_ID][ihel])
      continue; 

    // Also skip helicity if user asks for a specific one
    if ((ntry[proc_ID] >= max_tries) && user_ihel != -1 && user_ihel != ihel)
      continue; 

    calculate_wavefunctions(helicities[ihel]); 

    // Reset locally computed color flows
    for(int i = 0; i < 2; i++ )
      jamp2[0][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[1][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[2][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[3][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[4][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[5][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[6][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[7][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[8][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[9][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[10][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[11][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[12][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[13][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[14][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[15][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[16][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[17][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[18][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[19][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[20][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[21][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[22][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[23][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[24][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[25][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[26][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[27][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[28][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[29][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[30][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[31][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[32][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[33][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[34][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[35][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[36][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[37][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[38][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[39][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[40][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[41][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[42][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[43][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[44][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[45][i] = 0.; 

    if (proc_ID == 0)
      t = matrix_11_uu_wpwmuu(); 
    if (proc_ID == 1)
      t = matrix_11_uux_wpwmuux(); 
    if (proc_ID == 2)
      t = matrix_11_dd_wpwmdd(); 
    if (proc_ID == 3)
      t = matrix_11_ddx_wpwmddx(); 
    if (proc_ID == 4)
      t = matrix_11_uxux_wpwmuxux(); 
    if (proc_ID == 5)
      t = matrix_11_dxdx_wpwmdxdx(); 
    if (proc_ID == 6)
      t = matrix_11_ud_wpwmud(); 
    if (proc_ID == 7)
      t = matrix_11_uux_wpwmddx(); 
    if (proc_ID == 8)
      t = matrix_11_udx_wpwmudx(); 
    if (proc_ID == 9)
      t = matrix_11_dux_wpwmdux(); 
    if (proc_ID == 10)
      t = matrix_11_ddx_wpwmuux(); 
    if (proc_ID == 11)
      t = matrix_11_uxdx_wpwmuxdx(); 
    if (proc_ID == 12)
      t = matrix_11_uu_wpwpdd(); 
    if (proc_ID == 13)
      t = matrix_11_udx_wpwpdux(); 
    if (proc_ID == 14)
      t = matrix_11_dd_wmwmuu(); 
    if (proc_ID == 15)
      t = matrix_11_dux_wmwmudx(); 
    if (proc_ID == 16)
      t = matrix_11_uxux_wmwmdxdx(); 
    if (proc_ID == 17)
      t = matrix_11_dxdx_wpwpuxux(); 
    if (proc_ID == 18)
      t = matrix_11_uc_wpwmuc(); 
    if (proc_ID == 19)
      t = matrix_11_us_wpwmus(); 
    if (proc_ID == 20)
      t = matrix_11_uux_wpwmccx(); 
    if (proc_ID == 21)
      t = matrix_11_uux_wpwmssx(); 
    if (proc_ID == 22)
      t = matrix_11_ucx_wpwmucx(); 
    if (proc_ID == 23)
      t = matrix_11_usx_wpwmusx(); 
    if (proc_ID == 24)
      t = matrix_11_ds_wpwmds(); 
    if (proc_ID == 25)
      t = matrix_11_dcx_wpwmdcx(); 
    if (proc_ID == 26)
      t = matrix_11_ddx_wpwmccx(); 
    if (proc_ID == 27)
      t = matrix_11_ddx_wpwmssx(); 
    if (proc_ID == 28)
      t = matrix_11_dsx_wpwmdsx(); 
    if (proc_ID == 29)
      t = matrix_11_uxcx_wpwmuxcx(); 
    if (proc_ID == 30)
      t = matrix_11_uxsx_wpwmuxsx(); 
    if (proc_ID == 31)
      t = matrix_11_dxsx_wpwmdxsx(); 
    if (proc_ID == 32)
      t = matrix_11_uc_wpwpds(); 
    if (proc_ID == 33)
      t = matrix_11_udx_wpwpscx(); 
    if (proc_ID == 34)
      t = matrix_11_usx_wpwpdcx(); 
    if (proc_ID == 35)
      t = matrix_11_ds_wmwmuc(); 
    if (proc_ID == 36)
      t = matrix_11_dux_wmwmcsx(); 
    if (proc_ID == 37)
      t = matrix_11_dcx_wmwmusx(); 
    if (proc_ID == 38)
      t = matrix_11_uxcx_wmwmdxsx(); 
    if (proc_ID == 39)
      t = matrix_11_dxsx_wpwpuxcx(); 
    if (proc_ID == 40)
      t = matrix_11_us_wpwmcd(); 
    if (proc_ID == 41)
      t = matrix_11_ucx_wpwmdsx(); 
    if (proc_ID == 42)
      t = matrix_11_udx_wpwmcsx(); 
    if (proc_ID == 43)
      t = matrix_11_dux_wpwmscx(); 
    if (proc_ID == 44)
      t = matrix_11_dsx_wpwmucx(); 
    if (proc_ID == 45)
      t = matrix_11_uxsx_wpwmcxdx(); 

    // Store which helicities give non-zero result
    if ((ntry[proc_ID] < max_tries) && t != 0. && !goodhel[proc_ID][ihel])
      goodhel[proc_ID][ihel] = true; 

    // Aggregate results
    if (user_ihel == -1 || user_ihel == ihel)
    {
      if (user_icol == -1)
      {
        result = result + t; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][0] += t; 
          for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
          {
            all_results[proc_ID][0][i + 1] += jamp2[proc_ID][i]; 
          }
        }
        all_results[proc_ID][ihel + 1][0] += t; 
        for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
        {
          all_results[proc_ID][ihel + 1][i + 1] += jamp2[proc_ID][i]; 
        }
      }
      else
      {
        result = result + jamp2[proc_ID][user_icol]; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][user_icol + 1] += jamp2[proc_ID][user_icol]; 
        }
        all_results[proc_ID][ihel + 1][user_icol + 1] +=
            jamp2[proc_ID][user_icol];
      }
    }

  }

  // Normalize results with the identical particle factor
  if (include_symmetry_factors)
  {
    result = result/denom_iden[proc_ID]; 
  }
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      if (include_symmetry_factors)
      {
        all_results[proc_ID][ihel + 1][icolor + 1] /= denom_iden[proc_ID]; 
      }
    }
  }


  // Normalize when when summing+averaging over helicity configurations
  if (user_ihel == -1 && include_helicity_averaging_factors)
  {
    result /= denom_hels[proc_ID]; 
    if (user_icol == -1)
    {
      all_results[proc_ID][0][0] /= denom_hels[proc_ID]; 
      for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
      {
        all_results[proc_ID][0][i + 1] /= denom_hels[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][0][user_icol + 1] /= denom_hels[proc_ID]; 
    }
  }

  // Normalize when summing+averaging over color configurations
  if (user_icol == -1 && include_color_averaging_factors)
  {
    result /= denom_colors[proc_ID]; 
    if (user_ihel == -1)
    {
      all_results[proc_ID][0][0] /= denom_colors[proc_ID]; 
      for (unsigned int i = 0; i < ncomb; i++ )
      {
        all_results[proc_ID][i + 1][0] /= denom_colors[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][user_ihel + 1][0] /= denom_colors[proc_ID]; 
    }
  }

  // Reinstate previous values of mME
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (saved_mME[i] < 0.0)
    {
      mME[i] = saved_mME[i]; 
    }
  }

  // Finally return it
  return result; 
}

//==========================================================================
// Private class member functions

//--------------------------------------------------------------------------
// Evaluate |M|^2 for each subprocess

void PY8MEs_R11_P164_sm_qq_wpwmqq::calculate_wavefunctions(const int hel[])
{
  // Calculate wavefunctions for all processes
  // Calculate all wavefunctions
  ixxxxx(p[perm[0]], mME[0], hel[0], +1, w[0]); 
  ixxxxx(p[perm[1]], mME[1], hel[1], +1, w[1]); 
  vxxxxx(p[perm[2]], mME[2], hel[2], +1, w[2]); 
  vxxxxx(p[perm[3]], mME[3], hel[3], +1, w[3]); 
  oxxxxx(p[perm[4]], mME[4], hel[4], +1, w[4]); 
  oxxxxx(p[perm[5]], mME[5], hel[5], +1, w[5]); 
  FFV2_2(w[0], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[6]); 
  FFV1P0_3(w[1], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[7]); 
  FFV2_2(w[6], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[8]); 
  FFV2_1(w[5], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[9]); 
  FFV1P0_3(w[1], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[10]); 
  FFV2_1(w[4], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[11]); 
  FFV1P0_3(w[0], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[12]); 
  FFV2_2(w[1], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[13]); 
  FFV1_1(w[5], w[12], pars->GC_11, pars->ZERO, pars->ZERO, w[14]); 
  VVV1P0_1(w[3], w[2], pars->GC_4, pars->ZERO, pars->ZERO, w[15]); 
  FFV1_2(w[1], w[12], pars->GC_11, pars->ZERO, pars->ZERO, w[16]); 
  VVV1_3(w[3], w[2], pars->GC_53, pars->mdl_MZ, pars->mdl_WZ, w[17]); 
  FFV1P0_3(w[0], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[18]); 
  FFV1_1(w[4], w[18], pars->GC_11, pars->ZERO, pars->ZERO, w[19]); 
  FFV1_2(w[1], w[18], pars->GC_11, pars->ZERO, pars->ZERO, w[20]); 
  FFV1_2(w[0], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[21]); 
  FFV1_2(w[0], w[15], pars->GC_2, pars->ZERO, pars->ZERO, w[22]); 
  FFV2_5_2(w[0], w[17], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[23]);
  FFV1_2(w[0], w[10], pars->GC_11, pars->ZERO, pars->ZERO, w[24]); 
  oxxxxx(p[perm[1]], mME[1], hel[1], -1, w[25]); 
  ixxxxx(p[perm[5]], mME[5], hel[5], -1, w[26]); 
  FFV1P0_3(w[26], w[25], pars->GC_11, pars->ZERO, pars->ZERO, w[27]); 
  FFV1P0_3(w[26], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[28]); 
  FFV2_1(w[25], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[29]); 
  FFV1P0_3(w[0], w[25], pars->GC_11, pars->ZERO, pars->ZERO, w[30]); 
  FFV2_2(w[26], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[31]); 
  FFV1_1(w[4], w[30], pars->GC_11, pars->ZERO, pars->ZERO, w[32]); 
  FFV1_2(w[26], w[30], pars->GC_11, pars->ZERO, pars->ZERO, w[33]); 
  FFV1_1(w[25], w[12], pars->GC_11, pars->ZERO, pars->ZERO, w[34]); 
  FFV1_2(w[26], w[12], pars->GC_11, pars->ZERO, pars->ZERO, w[35]); 
  FFV1_2(w[0], w[27], pars->GC_11, pars->ZERO, pars->ZERO, w[36]); 
  FFV1_2(w[0], w[28], pars->GC_11, pars->ZERO, pars->ZERO, w[37]); 
  FFV2_2(w[0], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[38]); 
  FFV2_2(w[38], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[39]); 
  FFV2_1(w[5], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[40]); 
  FFV2_1(w[4], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[41]); 
  FFV2_2(w[1], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[42]); 
  FFV1_2(w[0], w[15], pars->GC_1, pars->ZERO, pars->ZERO, w[43]); 
  FFV2_3_2(w[0], w[17], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[44]);
  FFV2_1(w[25], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[45]); 
  FFV2_2(w[26], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[46]); 
  oxxxxx(p[perm[0]], mME[0], hel[0], -1, w[47]); 
  ixxxxx(p[perm[4]], mME[4], hel[4], -1, w[48]); 
  FFV2_2(w[48], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[49]); 
  FFV1P0_3(w[26], w[47], pars->GC_11, pars->ZERO, pars->ZERO, w[50]); 
  FFV2_2(w[49], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[51]); 
  FFV2_1(w[47], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[52]); 
  FFV1P0_3(w[48], w[47], pars->GC_11, pars->ZERO, pars->ZERO, w[53]); 
  FFV1_1(w[25], w[53], pars->GC_11, pars->ZERO, pars->ZERO, w[54]); 
  FFV1_2(w[26], w[53], pars->GC_11, pars->ZERO, pars->ZERO, w[55]); 
  FFV1P0_3(w[48], w[25], pars->GC_11, pars->ZERO, pars->ZERO, w[56]); 
  FFV1_1(w[47], w[56], pars->GC_11, pars->ZERO, pars->ZERO, w[57]); 
  FFV1_2(w[26], w[56], pars->GC_11, pars->ZERO, pars->ZERO, w[58]); 
  FFV1_2(w[48], w[50], pars->GC_11, pars->ZERO, pars->ZERO, w[59]); 
  FFV1_2(w[48], w[15], pars->GC_2, pars->ZERO, pars->ZERO, w[60]); 
  FFV2_5_2(w[48], w[17], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[61]);
  FFV1_2(w[48], w[27], pars->GC_11, pars->ZERO, pars->ZERO, w[62]); 
  FFV2_2(w[48], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[63]); 
  FFV2_2(w[63], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[64]); 
  FFV2_1(w[47], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[65]); 
  FFV1_2(w[48], w[15], pars->GC_1, pars->ZERO, pars->ZERO, w[66]); 
  FFV2_3_2(w[48], w[17], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[67]);
  FFV1P0_3(w[6], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[68]); 
  FFV1P0_3(w[0], w[40], pars->GC_11, pars->ZERO, pars->ZERO, w[69]); 
  FFV1P0_3(w[6], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[70]); 
  FFV1P0_3(w[0], w[41], pars->GC_11, pars->ZERO, pars->ZERO, w[71]); 
  FFV1P0_3(w[6], w[25], pars->GC_11, pars->ZERO, pars->ZERO, w[72]); 
  FFV1P0_3(w[0], w[45], pars->GC_11, pars->ZERO, pars->ZERO, w[73]); 
  FFV1P0_3(w[31], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[74]); 
  FFV2_2(w[31], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[75]); 
  FFV1_1(w[4], w[27], pars->GC_11, pars->ZERO, pars->ZERO, w[76]); 
  FFV1P0_3(w[26], w[41], pars->GC_11, pars->ZERO, pars->ZERO, w[77]); 
  FFV1_2(w[26], w[15], pars->GC_2, pars->ZERO, pars->ZERO, w[78]); 
  FFV2_5_2(w[26], w[17], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[79]);
  FFV1P0_3(w[31], w[25], pars->GC_11, pars->ZERO, pars->ZERO, w[80]); 
  FFV1_1(w[25], w[28], pars->GC_11, pars->ZERO, pars->ZERO, w[81]); 
  FFV1P0_3(w[26], w[45], pars->GC_11, pars->ZERO, pars->ZERO, w[82]); 
  FFV1P0_3(w[49], w[25], pars->GC_11, pars->ZERO, pars->ZERO, w[83]); 
  FFV1P0_3(w[48], w[45], pars->GC_11, pars->ZERO, pars->ZERO, w[84]); 
  FFV1P0_3(w[38], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[85]); 
  FFV1P0_3(w[38], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[86]); 
  FFV1P0_3(w[0], w[11], pars->GC_11, pars->ZERO, pars->ZERO, w[87]); 
  FFV1P0_3(w[0], w[9], pars->GC_11, pars->ZERO, pars->ZERO, w[88]); 
  FFV1P0_3(w[38], w[25], pars->GC_11, pars->ZERO, pars->ZERO, w[89]); 
  FFV1P0_3(w[0], w[29], pars->GC_11, pars->ZERO, pars->ZERO, w[90]); 
  FFV1P0_3(w[49], w[47], pars->GC_11, pars->ZERO, pars->ZERO, w[91]); 
  FFV1P0_3(w[63], w[47], pars->GC_11, pars->ZERO, pars->ZERO, w[92]); 
  FFV1P0_3(w[63], w[25], pars->GC_11, pars->ZERO, pars->ZERO, w[93]); 
  FFV1P0_3(w[48], w[52], pars->GC_11, pars->ZERO, pars->ZERO, w[94]); 
  FFV1P0_3(w[48], w[29], pars->GC_11, pars->ZERO, pars->ZERO, w[95]); 
  FFV1P0_3(w[48], w[65], pars->GC_11, pars->ZERO, pars->ZERO, w[96]); 

  // Calculate all amplitudes
  // Amplitude(s) for diagram number 0
  FFV1_0(w[8], w[5], w[7], pars->GC_11, amp[0]); 
  FFV1_0(w[6], w[9], w[7], pars->GC_11, amp[1]); 
  FFV1_0(w[8], w[4], w[10], pars->GC_11, amp[2]); 
  FFV1_0(w[6], w[11], w[10], pars->GC_11, amp[3]); 
  FFV2_0(w[13], w[14], w[3], pars->GC_100, amp[4]); 
  FFV1_0(w[13], w[9], w[12], pars->GC_11, amp[5]); 
  FFV1_0(w[16], w[5], w[15], pars->GC_2, amp[6]); 
  FFV1_0(w[1], w[14], w[15], pars->GC_2, amp[7]); 
  FFV2_5_0(w[16], w[5], w[17], pars->GC_51, pars->GC_58, amp[8]); 
  FFV2_5_0(w[1], w[14], w[17], pars->GC_51, pars->GC_58, amp[9]); 
  FFV2_0(w[16], w[9], w[2], pars->GC_100, amp[10]); 
  FFV2_0(w[13], w[19], w[3], pars->GC_100, amp[11]); 
  FFV1_0(w[13], w[11], w[18], pars->GC_11, amp[12]); 
  FFV1_0(w[20], w[4], w[15], pars->GC_2, amp[13]); 
  FFV1_0(w[1], w[19], w[15], pars->GC_2, amp[14]); 
  FFV2_5_0(w[20], w[4], w[17], pars->GC_51, pars->GC_58, amp[15]); 
  FFV2_5_0(w[1], w[19], w[17], pars->GC_51, pars->GC_58, amp[16]); 
  FFV2_0(w[20], w[11], w[2], pars->GC_100, amp[17]); 
  FFV1_0(w[21], w[5], w[15], pars->GC_2, amp[18]); 
  FFV1_0(w[22], w[5], w[7], pars->GC_11, amp[19]); 
  FFV2_5_0(w[21], w[5], w[17], pars->GC_51, pars->GC_58, amp[20]); 
  FFV1_0(w[23], w[5], w[7], pars->GC_11, amp[21]); 
  FFV2_0(w[21], w[9], w[2], pars->GC_100, amp[22]); 
  FFV1_0(w[24], w[4], w[15], pars->GC_2, amp[23]); 
  FFV1_0(w[22], w[4], w[10], pars->GC_11, amp[24]); 
  FFV2_5_0(w[24], w[4], w[17], pars->GC_51, pars->GC_58, amp[25]); 
  FFV1_0(w[23], w[4], w[10], pars->GC_11, amp[26]); 
  FFV2_0(w[24], w[11], w[2], pars->GC_100, amp[27]); 
  FFV1_0(w[8], w[4], w[27], pars->GC_11, amp[28]); 
  FFV1_0(w[6], w[11], w[27], pars->GC_11, amp[29]); 
  FFV1_0(w[8], w[25], w[28], pars->GC_11, amp[30]); 
  FFV1_0(w[6], w[29], w[28], pars->GC_11, amp[31]); 
  FFV2_0(w[31], w[32], w[3], pars->GC_100, amp[32]); 
  FFV1_0(w[31], w[11], w[30], pars->GC_11, amp[33]); 
  FFV1_0(w[33], w[4], w[15], pars->GC_2, amp[34]); 
  FFV1_0(w[26], w[32], w[15], pars->GC_2, amp[35]); 
  FFV2_5_0(w[33], w[4], w[17], pars->GC_51, pars->GC_58, amp[36]); 
  FFV2_5_0(w[26], w[32], w[17], pars->GC_51, pars->GC_58, amp[37]); 
  FFV2_0(w[33], w[11], w[2], pars->GC_100, amp[38]); 
  FFV2_0(w[31], w[34], w[3], pars->GC_100, amp[39]); 
  FFV1_0(w[31], w[29], w[12], pars->GC_11, amp[40]); 
  FFV1_0(w[35], w[25], w[15], pars->GC_2, amp[41]); 
  FFV1_0(w[26], w[34], w[15], pars->GC_2, amp[42]); 
  FFV2_5_0(w[35], w[25], w[17], pars->GC_51, pars->GC_58, amp[43]); 
  FFV2_5_0(w[26], w[34], w[17], pars->GC_51, pars->GC_58, amp[44]); 
  FFV2_0(w[35], w[29], w[2], pars->GC_100, amp[45]); 
  FFV1_0(w[36], w[4], w[15], pars->GC_2, amp[46]); 
  FFV1_0(w[22], w[4], w[27], pars->GC_11, amp[47]); 
  FFV2_5_0(w[36], w[4], w[17], pars->GC_51, pars->GC_58, amp[48]); 
  FFV1_0(w[23], w[4], w[27], pars->GC_11, amp[49]); 
  FFV2_0(w[36], w[11], w[2], pars->GC_100, amp[50]); 
  FFV1_0(w[37], w[25], w[15], pars->GC_2, amp[51]); 
  FFV1_0(w[22], w[25], w[28], pars->GC_11, amp[52]); 
  FFV2_5_0(w[37], w[25], w[17], pars->GC_51, pars->GC_58, amp[53]); 
  FFV1_0(w[23], w[25], w[28], pars->GC_11, amp[54]); 
  FFV2_0(w[37], w[29], w[2], pars->GC_100, amp[55]); 
  FFV1_0(w[39], w[5], w[7], pars->GC_11, amp[56]); 
  FFV1_0(w[38], w[40], w[7], pars->GC_11, amp[57]); 
  FFV1_0(w[39], w[4], w[10], pars->GC_11, amp[58]); 
  FFV1_0(w[38], w[41], w[10], pars->GC_11, amp[59]); 
  FFV2_0(w[42], w[14], w[2], pars->GC_100, amp[60]); 
  FFV1_0(w[42], w[40], w[12], pars->GC_11, amp[61]); 
  FFV1_0(w[16], w[5], w[15], pars->GC_1, amp[62]); 
  FFV1_0(w[1], w[14], w[15], pars->GC_1, amp[63]); 
  FFV2_3_0(w[16], w[5], w[17], pars->GC_50, pars->GC_58, amp[64]); 
  FFV2_3_0(w[1], w[14], w[17], pars->GC_50, pars->GC_58, amp[65]); 
  FFV2_0(w[16], w[40], w[3], pars->GC_100, amp[66]); 
  FFV2_0(w[42], w[19], w[2], pars->GC_100, amp[67]); 
  FFV1_0(w[42], w[41], w[18], pars->GC_11, amp[68]); 
  FFV1_0(w[20], w[4], w[15], pars->GC_1, amp[69]); 
  FFV1_0(w[1], w[19], w[15], pars->GC_1, amp[70]); 
  FFV2_3_0(w[20], w[4], w[17], pars->GC_50, pars->GC_58, amp[71]); 
  FFV2_3_0(w[1], w[19], w[17], pars->GC_50, pars->GC_58, amp[72]); 
  FFV2_0(w[20], w[41], w[3], pars->GC_100, amp[73]); 
  FFV1_0(w[21], w[5], w[15], pars->GC_1, amp[74]); 
  FFV1_0(w[43], w[5], w[7], pars->GC_11, amp[75]); 
  FFV2_3_0(w[21], w[5], w[17], pars->GC_50, pars->GC_58, amp[76]); 
  FFV1_0(w[44], w[5], w[7], pars->GC_11, amp[77]); 
  FFV2_0(w[21], w[40], w[3], pars->GC_100, amp[78]); 
  FFV1_0(w[24], w[4], w[15], pars->GC_1, amp[79]); 
  FFV1_0(w[43], w[4], w[10], pars->GC_11, amp[80]); 
  FFV2_3_0(w[24], w[4], w[17], pars->GC_50, pars->GC_58, amp[81]); 
  FFV1_0(w[44], w[4], w[10], pars->GC_11, amp[82]); 
  FFV2_0(w[24], w[41], w[3], pars->GC_100, amp[83]); 
  FFV1_0(w[39], w[4], w[27], pars->GC_11, amp[84]); 
  FFV1_0(w[38], w[41], w[27], pars->GC_11, amp[85]); 
  FFV1_0(w[39], w[25], w[28], pars->GC_11, amp[86]); 
  FFV1_0(w[38], w[45], w[28], pars->GC_11, amp[87]); 
  FFV2_0(w[46], w[32], w[2], pars->GC_100, amp[88]); 
  FFV1_0(w[46], w[41], w[30], pars->GC_11, amp[89]); 
  FFV1_0(w[33], w[4], w[15], pars->GC_1, amp[90]); 
  FFV1_0(w[26], w[32], w[15], pars->GC_1, amp[91]); 
  FFV2_3_0(w[33], w[4], w[17], pars->GC_50, pars->GC_58, amp[92]); 
  FFV2_3_0(w[26], w[32], w[17], pars->GC_50, pars->GC_58, amp[93]); 
  FFV2_0(w[33], w[41], w[3], pars->GC_100, amp[94]); 
  FFV2_0(w[46], w[34], w[2], pars->GC_100, amp[95]); 
  FFV1_0(w[46], w[45], w[12], pars->GC_11, amp[96]); 
  FFV1_0(w[35], w[25], w[15], pars->GC_1, amp[97]); 
  FFV1_0(w[26], w[34], w[15], pars->GC_1, amp[98]); 
  FFV2_3_0(w[35], w[25], w[17], pars->GC_50, pars->GC_58, amp[99]); 
  FFV2_3_0(w[26], w[34], w[17], pars->GC_50, pars->GC_58, amp[100]); 
  FFV2_0(w[35], w[45], w[3], pars->GC_100, amp[101]); 
  FFV1_0(w[36], w[4], w[15], pars->GC_1, amp[102]); 
  FFV1_0(w[43], w[4], w[27], pars->GC_11, amp[103]); 
  FFV2_3_0(w[36], w[4], w[17], pars->GC_50, pars->GC_58, amp[104]); 
  FFV1_0(w[44], w[4], w[27], pars->GC_11, amp[105]); 
  FFV2_0(w[36], w[41], w[3], pars->GC_100, amp[106]); 
  FFV1_0(w[37], w[25], w[15], pars->GC_1, amp[107]); 
  FFV1_0(w[43], w[25], w[28], pars->GC_11, amp[108]); 
  FFV2_3_0(w[37], w[25], w[17], pars->GC_50, pars->GC_58, amp[109]); 
  FFV1_0(w[44], w[25], w[28], pars->GC_11, amp[110]); 
  FFV2_0(w[37], w[45], w[3], pars->GC_100, amp[111]); 
  FFV1_0(w[51], w[25], w[50], pars->GC_11, amp[112]); 
  FFV1_0(w[49], w[29], w[50], pars->GC_11, amp[113]); 
  FFV1_0(w[51], w[47], w[27], pars->GC_11, amp[114]); 
  FFV1_0(w[49], w[52], w[27], pars->GC_11, amp[115]); 
  FFV2_0(w[31], w[54], w[3], pars->GC_100, amp[116]); 
  FFV1_0(w[31], w[29], w[53], pars->GC_11, amp[117]); 
  FFV1_0(w[55], w[25], w[15], pars->GC_2, amp[118]); 
  FFV1_0(w[26], w[54], w[15], pars->GC_2, amp[119]); 
  FFV2_5_0(w[55], w[25], w[17], pars->GC_51, pars->GC_58, amp[120]); 
  FFV2_5_0(w[26], w[54], w[17], pars->GC_51, pars->GC_58, amp[121]); 
  FFV2_0(w[55], w[29], w[2], pars->GC_100, amp[122]); 
  FFV2_0(w[31], w[57], w[3], pars->GC_100, amp[123]); 
  FFV1_0(w[31], w[52], w[56], pars->GC_11, amp[124]); 
  FFV1_0(w[58], w[47], w[15], pars->GC_2, amp[125]); 
  FFV1_0(w[26], w[57], w[15], pars->GC_2, amp[126]); 
  FFV2_5_0(w[58], w[47], w[17], pars->GC_51, pars->GC_58, amp[127]); 
  FFV2_5_0(w[26], w[57], w[17], pars->GC_51, pars->GC_58, amp[128]); 
  FFV2_0(w[58], w[52], w[2], pars->GC_100, amp[129]); 
  FFV1_0(w[59], w[25], w[15], pars->GC_2, amp[130]); 
  FFV1_0(w[60], w[25], w[50], pars->GC_11, amp[131]); 
  FFV2_5_0(w[59], w[25], w[17], pars->GC_51, pars->GC_58, amp[132]); 
  FFV1_0(w[61], w[25], w[50], pars->GC_11, amp[133]); 
  FFV2_0(w[59], w[29], w[2], pars->GC_100, amp[134]); 
  FFV1_0(w[62], w[47], w[15], pars->GC_2, amp[135]); 
  FFV1_0(w[60], w[47], w[27], pars->GC_11, amp[136]); 
  FFV2_5_0(w[62], w[47], w[17], pars->GC_51, pars->GC_58, amp[137]); 
  FFV1_0(w[61], w[47], w[27], pars->GC_11, amp[138]); 
  FFV2_0(w[62], w[52], w[2], pars->GC_100, amp[139]); 
  FFV1_0(w[64], w[25], w[50], pars->GC_11, amp[140]); 
  FFV1_0(w[63], w[45], w[50], pars->GC_11, amp[141]); 
  FFV1_0(w[64], w[47], w[27], pars->GC_11, amp[142]); 
  FFV1_0(w[63], w[65], w[27], pars->GC_11, amp[143]); 
  FFV2_0(w[46], w[54], w[2], pars->GC_100, amp[144]); 
  FFV1_0(w[46], w[45], w[53], pars->GC_11, amp[145]); 
  FFV1_0(w[55], w[25], w[15], pars->GC_1, amp[146]); 
  FFV1_0(w[26], w[54], w[15], pars->GC_1, amp[147]); 
  FFV2_3_0(w[55], w[25], w[17], pars->GC_50, pars->GC_58, amp[148]); 
  FFV2_3_0(w[26], w[54], w[17], pars->GC_50, pars->GC_58, amp[149]); 
  FFV2_0(w[55], w[45], w[3], pars->GC_100, amp[150]); 
  FFV2_0(w[46], w[57], w[2], pars->GC_100, amp[151]); 
  FFV1_0(w[46], w[65], w[56], pars->GC_11, amp[152]); 
  FFV1_0(w[58], w[47], w[15], pars->GC_1, amp[153]); 
  FFV1_0(w[26], w[57], w[15], pars->GC_1, amp[154]); 
  FFV2_3_0(w[58], w[47], w[17], pars->GC_50, pars->GC_58, amp[155]); 
  FFV2_3_0(w[26], w[57], w[17], pars->GC_50, pars->GC_58, amp[156]); 
  FFV2_0(w[58], w[65], w[3], pars->GC_100, amp[157]); 
  FFV1_0(w[59], w[25], w[15], pars->GC_1, amp[158]); 
  FFV1_0(w[66], w[25], w[50], pars->GC_11, amp[159]); 
  FFV2_3_0(w[59], w[25], w[17], pars->GC_50, pars->GC_58, amp[160]); 
  FFV1_0(w[67], w[25], w[50], pars->GC_11, amp[161]); 
  FFV2_0(w[59], w[45], w[3], pars->GC_100, amp[162]); 
  FFV1_0(w[62], w[47], w[15], pars->GC_1, amp[163]); 
  FFV1_0(w[66], w[47], w[27], pars->GC_11, amp[164]); 
  FFV2_3_0(w[62], w[47], w[17], pars->GC_50, pars->GC_58, amp[165]); 
  FFV1_0(w[67], w[47], w[27], pars->GC_11, amp[166]); 
  FFV2_0(w[62], w[65], w[3], pars->GC_100, amp[167]); 
  FFV1_0(w[42], w[4], w[68], pars->GC_11, amp[168]); 
  FFV1_0(w[8], w[4], w[10], pars->GC_11, amp[169]); 
  FFV1_0(w[6], w[11], w[10], pars->GC_11, amp[170]); 
  FFV1_0(w[1], w[11], w[68], pars->GC_11, amp[171]); 
  FFV2_0(w[42], w[14], w[2], pars->GC_100, amp[172]); 
  FFV1_0(w[42], w[40], w[12], pars->GC_11, amp[173]); 
  FFV1_0(w[16], w[5], w[15], pars->GC_1, amp[174]); 
  FFV1_0(w[1], w[14], w[15], pars->GC_1, amp[175]); 
  FFV2_3_0(w[16], w[5], w[17], pars->GC_50, pars->GC_58, amp[176]); 
  FFV2_3_0(w[1], w[14], w[17], pars->GC_50, pars->GC_58, amp[177]); 
  FFV2_0(w[16], w[40], w[3], pars->GC_100, amp[178]); 
  FFV1_0(w[42], w[4], w[69], pars->GC_11, amp[179]); 
  FFV1_0(w[24], w[4], w[15], pars->GC_2, amp[180]); 
  FFV1_0(w[22], w[4], w[10], pars->GC_11, amp[181]); 
  FFV2_5_0(w[24], w[4], w[17], pars->GC_51, pars->GC_58, amp[182]); 
  FFV1_0(w[23], w[4], w[10], pars->GC_11, amp[183]); 
  FFV2_0(w[24], w[11], w[2], pars->GC_100, amp[184]); 
  FFV1_0(w[1], w[11], w[69], pars->GC_11, amp[185]); 
  FFV1_0(w[46], w[25], w[70], pars->GC_11, amp[186]); 
  FFV1_0(w[8], w[25], w[28], pars->GC_11, amp[187]); 
  FFV1_0(w[6], w[29], w[28], pars->GC_11, amp[188]); 
  FFV1_0(w[26], w[29], w[70], pars->GC_11, amp[189]); 
  FFV2_0(w[46], w[32], w[2], pars->GC_100, amp[190]); 
  FFV1_0(w[46], w[41], w[30], pars->GC_11, amp[191]); 
  FFV1_0(w[33], w[4], w[15], pars->GC_1, amp[192]); 
  FFV1_0(w[26], w[32], w[15], pars->GC_1, amp[193]); 
  FFV2_3_0(w[33], w[4], w[17], pars->GC_50, pars->GC_58, amp[194]); 
  FFV2_3_0(w[26], w[32], w[17], pars->GC_50, pars->GC_58, amp[195]); 
  FFV2_0(w[33], w[41], w[3], pars->GC_100, amp[196]); 
  FFV1_0(w[46], w[25], w[71], pars->GC_11, amp[197]); 
  FFV1_0(w[37], w[25], w[15], pars->GC_2, amp[198]); 
  FFV1_0(w[22], w[25], w[28], pars->GC_11, amp[199]); 
  FFV2_5_0(w[37], w[25], w[17], pars->GC_51, pars->GC_58, amp[200]); 
  FFV1_0(w[23], w[25], w[28], pars->GC_11, amp[201]); 
  FFV2_0(w[37], w[29], w[2], pars->GC_100, amp[202]); 
  FFV1_0(w[26], w[29], w[71], pars->GC_11, amp[203]); 
  FFV1_0(w[46], w[4], w[72], pars->GC_11, amp[204]); 
  FFV1_0(w[8], w[4], w[27], pars->GC_11, amp[205]); 
  FFV1_0(w[6], w[11], w[27], pars->GC_11, amp[206]); 
  FFV1_0(w[26], w[11], w[72], pars->GC_11, amp[207]); 
  FFV2_0(w[46], w[34], w[2], pars->GC_100, amp[208]); 
  FFV1_0(w[46], w[45], w[12], pars->GC_11, amp[209]); 
  FFV1_0(w[35], w[25], w[15], pars->GC_1, amp[210]); 
  FFV1_0(w[26], w[34], w[15], pars->GC_1, amp[211]); 
  FFV2_3_0(w[35], w[25], w[17], pars->GC_50, pars->GC_58, amp[212]); 
  FFV2_3_0(w[26], w[34], w[17], pars->GC_50, pars->GC_58, amp[213]); 
  FFV2_0(w[35], w[45], w[3], pars->GC_100, amp[214]); 
  FFV1_0(w[46], w[4], w[73], pars->GC_11, amp[215]); 
  FFV1_0(w[36], w[4], w[15], pars->GC_2, amp[216]); 
  FFV1_0(w[22], w[4], w[27], pars->GC_11, amp[217]); 
  FFV2_5_0(w[36], w[4], w[17], pars->GC_51, pars->GC_58, amp[218]); 
  FFV1_0(w[23], w[4], w[27], pars->GC_11, amp[219]); 
  FFV2_0(w[36], w[11], w[2], pars->GC_100, amp[220]); 
  FFV1_0(w[26], w[11], w[73], pars->GC_11, amp[221]); 
  FFV1_0(w[38], w[25], w[74], pars->GC_11, amp[222]); 
  FFV1_0(w[75], w[25], w[12], pars->GC_11, amp[223]); 
  FFV1_0(w[31], w[29], w[12], pars->GC_11, amp[224]); 
  FFV1_0(w[0], w[29], w[74], pars->GC_11, amp[225]); 
  FFV2_0(w[38], w[76], w[2], pars->GC_100, amp[226]); 
  FFV1_0(w[38], w[41], w[27], pars->GC_11, amp[227]); 
  FFV1_0(w[36], w[4], w[15], pars->GC_1, amp[228]); 
  FFV1_0(w[0], w[76], w[15], pars->GC_1, amp[229]); 
  FFV2_3_0(w[36], w[4], w[17], pars->GC_50, pars->GC_58, amp[230]); 
  FFV2_3_0(w[0], w[76], w[17], pars->GC_50, pars->GC_58, amp[231]); 
  FFV2_0(w[36], w[41], w[3], pars->GC_100, amp[232]); 
  FFV1_0(w[38], w[25], w[77], pars->GC_11, amp[233]); 
  FFV1_0(w[35], w[25], w[15], pars->GC_2, amp[234]); 
  FFV1_0(w[78], w[25], w[12], pars->GC_11, amp[235]); 
  FFV2_5_0(w[35], w[25], w[17], pars->GC_51, pars->GC_58, amp[236]); 
  FFV1_0(w[79], w[25], w[12], pars->GC_11, amp[237]); 
  FFV2_0(w[35], w[29], w[2], pars->GC_100, amp[238]); 
  FFV1_0(w[0], w[29], w[77], pars->GC_11, amp[239]); 
  FFV1_0(w[38], w[4], w[80], pars->GC_11, amp[240]); 
  FFV1_0(w[75], w[4], w[30], pars->GC_11, amp[241]); 
  FFV1_0(w[31], w[11], w[30], pars->GC_11, amp[242]); 
  FFV1_0(w[0], w[11], w[80], pars->GC_11, amp[243]); 
  FFV2_0(w[38], w[81], w[2], pars->GC_100, amp[244]); 
  FFV1_0(w[38], w[45], w[28], pars->GC_11, amp[245]); 
  FFV1_0(w[37], w[25], w[15], pars->GC_1, amp[246]); 
  FFV1_0(w[0], w[81], w[15], pars->GC_1, amp[247]); 
  FFV2_3_0(w[37], w[25], w[17], pars->GC_50, pars->GC_58, amp[248]); 
  FFV2_3_0(w[0], w[81], w[17], pars->GC_50, pars->GC_58, amp[249]); 
  FFV2_0(w[37], w[45], w[3], pars->GC_100, amp[250]); 
  FFV1_0(w[38], w[4], w[82], pars->GC_11, amp[251]); 
  FFV1_0(w[33], w[4], w[15], pars->GC_2, amp[252]); 
  FFV1_0(w[78], w[4], w[30], pars->GC_11, amp[253]); 
  FFV2_5_0(w[33], w[4], w[17], pars->GC_51, pars->GC_58, amp[254]); 
  FFV1_0(w[79], w[4], w[30], pars->GC_11, amp[255]); 
  FFV2_0(w[33], w[11], w[2], pars->GC_100, amp[256]); 
  FFV1_0(w[0], w[11], w[82], pars->GC_11, amp[257]); 
  FFV1_0(w[46], w[47], w[83], pars->GC_11, amp[258]); 
  FFV1_0(w[51], w[47], w[27], pars->GC_11, amp[259]); 
  FFV1_0(w[49], w[52], w[27], pars->GC_11, amp[260]); 
  FFV1_0(w[26], w[52], w[83], pars->GC_11, amp[261]); 
  FFV2_0(w[46], w[54], w[2], pars->GC_100, amp[262]); 
  FFV1_0(w[46], w[45], w[53], pars->GC_11, amp[263]); 
  FFV1_0(w[55], w[25], w[15], pars->GC_1, amp[264]); 
  FFV1_0(w[26], w[54], w[15], pars->GC_1, amp[265]); 
  FFV2_3_0(w[55], w[25], w[17], pars->GC_50, pars->GC_58, amp[266]); 
  FFV2_3_0(w[26], w[54], w[17], pars->GC_50, pars->GC_58, amp[267]); 
  FFV2_0(w[55], w[45], w[3], pars->GC_100, amp[268]); 
  FFV1_0(w[46], w[47], w[84], pars->GC_11, amp[269]); 
  FFV1_0(w[62], w[47], w[15], pars->GC_2, amp[270]); 
  FFV1_0(w[60], w[47], w[27], pars->GC_11, amp[271]); 
  FFV2_5_0(w[62], w[47], w[17], pars->GC_51, pars->GC_58, amp[272]); 
  FFV1_0(w[61], w[47], w[27], pars->GC_11, amp[273]); 
  FFV2_0(w[62], w[52], w[2], pars->GC_100, amp[274]); 
  FFV1_0(w[26], w[52], w[84], pars->GC_11, amp[275]); 
  FFV1_0(w[42], w[5], w[70], pars->GC_11, amp[276]); 
  FFV1_0(w[42], w[4], w[68], pars->GC_11, amp[277]); 
  FFV1_0(w[1], w[11], w[68], pars->GC_11, amp[278]); 
  FFV1_0(w[1], w[9], w[70], pars->GC_11, amp[279]); 
  FFV1_0(w[13], w[5], w[85], pars->GC_11, amp[280]); 
  FFV1_0(w[13], w[4], w[86], pars->GC_11, amp[281]); 
  FFV1_0(w[1], w[41], w[86], pars->GC_11, amp[282]); 
  FFV1_0(w[1], w[40], w[85], pars->GC_11, amp[283]); 
  FFV1_0(w[13], w[5], w[87], pars->GC_11, amp[284]); 
  FFV1_0(w[13], w[4], w[88], pars->GC_11, amp[285]); 
  FFV1_0(w[42], w[5], w[71], pars->GC_11, amp[286]); 
  FFV1_0(w[42], w[4], w[69], pars->GC_11, amp[287]); 
  FFV1_0(w[1], w[9], w[71], pars->GC_11, amp[288]); 
  FFV1_0(w[1], w[41], w[88], pars->GC_11, amp[289]); 
  FFV1_0(w[1], w[11], w[69], pars->GC_11, amp[290]); 
  FFV1_0(w[1], w[40], w[87], pars->GC_11, amp[291]); 
  FFV1_0(w[46], w[4], w[72], pars->GC_11, amp[292]); 
  FFV1_0(w[46], w[25], w[70], pars->GC_11, amp[293]); 
  FFV1_0(w[26], w[29], w[70], pars->GC_11, amp[294]); 
  FFV1_0(w[26], w[11], w[72], pars->GC_11, amp[295]); 
  FFV1_0(w[31], w[4], w[89], pars->GC_11, amp[296]); 
  FFV1_0(w[31], w[25], w[85], pars->GC_11, amp[297]); 
  FFV1_0(w[26], w[45], w[85], pars->GC_11, amp[298]); 
  FFV1_0(w[26], w[41], w[89], pars->GC_11, amp[299]); 
  FFV1_0(w[31], w[4], w[90], pars->GC_11, amp[300]); 
  FFV1_0(w[31], w[25], w[87], pars->GC_11, amp[301]); 
  FFV1_0(w[46], w[4], w[73], pars->GC_11, amp[302]); 
  FFV1_0(w[46], w[25], w[71], pars->GC_11, amp[303]); 
  FFV1_0(w[26], w[11], w[73], pars->GC_11, amp[304]); 
  FFV1_0(w[26], w[45], w[87], pars->GC_11, amp[305]); 
  FFV1_0(w[26], w[29], w[71], pars->GC_11, amp[306]); 
  FFV1_0(w[26], w[41], w[90], pars->GC_11, amp[307]); 
  FFV1_0(w[42], w[5], w[70], pars->GC_11, amp[308]); 
  FFV1_0(w[42], w[4], w[68], pars->GC_11, amp[309]); 
  FFV1_0(w[1], w[11], w[68], pars->GC_11, amp[310]); 
  FFV1_0(w[1], w[9], w[70], pars->GC_11, amp[311]); 
  FFV1_0(w[13], w[5], w[85], pars->GC_11, amp[312]); 
  FFV1_0(w[13], w[4], w[86], pars->GC_11, amp[313]); 
  FFV1_0(w[1], w[41], w[86], pars->GC_11, amp[314]); 
  FFV1_0(w[1], w[40], w[85], pars->GC_11, amp[315]); 
  FFV1_0(w[13], w[5], w[87], pars->GC_11, amp[316]); 
  FFV1_0(w[13], w[4], w[88], pars->GC_11, amp[317]); 
  FFV1_0(w[42], w[5], w[71], pars->GC_11, amp[318]); 
  FFV1_0(w[42], w[4], w[69], pars->GC_11, amp[319]); 
  FFV1_0(w[1], w[9], w[71], pars->GC_11, amp[320]); 
  FFV1_0(w[1], w[41], w[88], pars->GC_11, amp[321]); 
  FFV1_0(w[1], w[11], w[69], pars->GC_11, amp[322]); 
  FFV1_0(w[1], w[40], w[87], pars->GC_11, amp[323]); 
  FFV1_0(w[46], w[4], w[72], pars->GC_11, amp[324]); 
  FFV1_0(w[46], w[25], w[70], pars->GC_11, amp[325]); 
  FFV1_0(w[26], w[29], w[70], pars->GC_11, amp[326]); 
  FFV1_0(w[26], w[11], w[72], pars->GC_11, amp[327]); 
  FFV1_0(w[31], w[4], w[89], pars->GC_11, amp[328]); 
  FFV1_0(w[31], w[25], w[85], pars->GC_11, amp[329]); 
  FFV1_0(w[26], w[45], w[85], pars->GC_11, amp[330]); 
  FFV1_0(w[26], w[41], w[89], pars->GC_11, amp[331]); 
  FFV1_0(w[31], w[4], w[90], pars->GC_11, amp[332]); 
  FFV1_0(w[31], w[25], w[87], pars->GC_11, amp[333]); 
  FFV1_0(w[46], w[4], w[73], pars->GC_11, amp[334]); 
  FFV1_0(w[46], w[25], w[71], pars->GC_11, amp[335]); 
  FFV1_0(w[26], w[11], w[73], pars->GC_11, amp[336]); 
  FFV1_0(w[26], w[45], w[87], pars->GC_11, amp[337]); 
  FFV1_0(w[26], w[29], w[71], pars->GC_11, amp[338]); 
  FFV1_0(w[26], w[41], w[90], pars->GC_11, amp[339]); 
  FFV1_0(w[46], w[25], w[91], pars->GC_11, amp[340]); 
  FFV1_0(w[46], w[47], w[83], pars->GC_11, amp[341]); 
  FFV1_0(w[26], w[52], w[83], pars->GC_11, amp[342]); 
  FFV1_0(w[26], w[29], w[91], pars->GC_11, amp[343]); 
  FFV1_0(w[31], w[25], w[92], pars->GC_11, amp[344]); 
  FFV1_0(w[31], w[47], w[93], pars->GC_11, amp[345]); 
  FFV1_0(w[26], w[65], w[93], pars->GC_11, amp[346]); 
  FFV1_0(w[26], w[45], w[92], pars->GC_11, amp[347]); 
  FFV1_0(w[31], w[25], w[94], pars->GC_11, amp[348]); 
  FFV1_0(w[31], w[47], w[95], pars->GC_11, amp[349]); 
  FFV1_0(w[46], w[25], w[96], pars->GC_11, amp[350]); 
  FFV1_0(w[46], w[47], w[84], pars->GC_11, amp[351]); 
  FFV1_0(w[26], w[29], w[96], pars->GC_11, amp[352]); 
  FFV1_0(w[26], w[65], w[95], pars->GC_11, amp[353]); 
  FFV1_0(w[26], w[52], w[84], pars->GC_11, amp[354]); 
  FFV1_0(w[26], w[45], w[94], pars->GC_11, amp[355]); 
  FFV1_0(w[46], w[25], w[91], pars->GC_11, amp[356]); 
  FFV1_0(w[46], w[47], w[83], pars->GC_11, amp[357]); 
  FFV1_0(w[26], w[52], w[83], pars->GC_11, amp[358]); 
  FFV1_0(w[26], w[29], w[91], pars->GC_11, amp[359]); 
  FFV1_0(w[31], w[25], w[92], pars->GC_11, amp[360]); 
  FFV1_0(w[31], w[47], w[93], pars->GC_11, amp[361]); 
  FFV1_0(w[26], w[65], w[93], pars->GC_11, amp[362]); 
  FFV1_0(w[26], w[45], w[92], pars->GC_11, amp[363]); 
  FFV1_0(w[31], w[25], w[94], pars->GC_11, amp[364]); 
  FFV1_0(w[31], w[47], w[95], pars->GC_11, amp[365]); 
  FFV1_0(w[46], w[25], w[96], pars->GC_11, amp[366]); 
  FFV1_0(w[46], w[47], w[84], pars->GC_11, amp[367]); 
  FFV1_0(w[26], w[29], w[96], pars->GC_11, amp[368]); 
  FFV1_0(w[26], w[65], w[95], pars->GC_11, amp[369]); 
  FFV1_0(w[26], w[52], w[84], pars->GC_11, amp[370]); 
  FFV1_0(w[26], w[45], w[94], pars->GC_11, amp[371]); 
  FFV1_0(w[8], w[4], w[10], pars->GC_11, amp[372]); 
  FFV1_0(w[6], w[11], w[10], pars->GC_11, amp[373]); 
  FFV2_0(w[13], w[14], w[3], pars->GC_100, amp[374]); 
  FFV1_0(w[13], w[9], w[12], pars->GC_11, amp[375]); 
  FFV1_0(w[16], w[5], w[15], pars->GC_2, amp[376]); 
  FFV1_0(w[1], w[14], w[15], pars->GC_2, amp[377]); 
  FFV2_5_0(w[16], w[5], w[17], pars->GC_51, pars->GC_58, amp[378]); 
  FFV2_5_0(w[1], w[14], w[17], pars->GC_51, pars->GC_58, amp[379]); 
  FFV2_0(w[16], w[9], w[2], pars->GC_100, amp[380]); 
  FFV1_0(w[24], w[4], w[15], pars->GC_2, amp[381]); 
  FFV1_0(w[22], w[4], w[10], pars->GC_11, amp[382]); 
  FFV2_5_0(w[24], w[4], w[17], pars->GC_51, pars->GC_58, amp[383]); 
  FFV1_0(w[23], w[4], w[10], pars->GC_11, amp[384]); 
  FFV2_0(w[24], w[11], w[2], pars->GC_100, amp[385]); 
  FFV1_0(w[8], w[4], w[10], pars->GC_11, amp[386]); 
  FFV1_0(w[6], w[11], w[10], pars->GC_11, amp[387]); 
  FFV2_0(w[42], w[14], w[2], pars->GC_100, amp[388]); 
  FFV1_0(w[42], w[40], w[12], pars->GC_11, amp[389]); 
  FFV1_0(w[16], w[5], w[15], pars->GC_1, amp[390]); 
  FFV1_0(w[1], w[14], w[15], pars->GC_1, amp[391]); 
  FFV2_3_0(w[16], w[5], w[17], pars->GC_50, pars->GC_58, amp[392]); 
  FFV2_3_0(w[1], w[14], w[17], pars->GC_50, pars->GC_58, amp[393]); 
  FFV2_0(w[16], w[40], w[3], pars->GC_100, amp[394]); 
  FFV1_0(w[24], w[4], w[15], pars->GC_2, amp[395]); 
  FFV1_0(w[22], w[4], w[10], pars->GC_11, amp[396]); 
  FFV2_5_0(w[24], w[4], w[17], pars->GC_51, pars->GC_58, amp[397]); 
  FFV1_0(w[23], w[4], w[10], pars->GC_11, amp[398]); 
  FFV2_0(w[24], w[11], w[2], pars->GC_100, amp[399]); 
  FFV1_0(w[8], w[25], w[28], pars->GC_11, amp[400]); 
  FFV1_0(w[6], w[29], w[28], pars->GC_11, amp[401]); 
  FFV2_0(w[31], w[32], w[3], pars->GC_100, amp[402]); 
  FFV1_0(w[31], w[11], w[30], pars->GC_11, amp[403]); 
  FFV1_0(w[33], w[4], w[15], pars->GC_2, amp[404]); 
  FFV1_0(w[26], w[32], w[15], pars->GC_2, amp[405]); 
  FFV2_5_0(w[33], w[4], w[17], pars->GC_51, pars->GC_58, amp[406]); 
  FFV2_5_0(w[26], w[32], w[17], pars->GC_51, pars->GC_58, amp[407]); 
  FFV2_0(w[33], w[11], w[2], pars->GC_100, amp[408]); 
  FFV1_0(w[37], w[25], w[15], pars->GC_2, amp[409]); 
  FFV1_0(w[22], w[25], w[28], pars->GC_11, amp[410]); 
  FFV2_5_0(w[37], w[25], w[17], pars->GC_51, pars->GC_58, amp[411]); 
  FFV1_0(w[23], w[25], w[28], pars->GC_11, amp[412]); 
  FFV2_0(w[37], w[29], w[2], pars->GC_100, amp[413]); 
  FFV1_0(w[8], w[25], w[28], pars->GC_11, amp[414]); 
  FFV1_0(w[6], w[29], w[28], pars->GC_11, amp[415]); 
  FFV2_0(w[46], w[32], w[2], pars->GC_100, amp[416]); 
  FFV1_0(w[46], w[41], w[30], pars->GC_11, amp[417]); 
  FFV1_0(w[33], w[4], w[15], pars->GC_1, amp[418]); 
  FFV1_0(w[26], w[32], w[15], pars->GC_1, amp[419]); 
  FFV2_3_0(w[33], w[4], w[17], pars->GC_50, pars->GC_58, amp[420]); 
  FFV2_3_0(w[26], w[32], w[17], pars->GC_50, pars->GC_58, amp[421]); 
  FFV2_0(w[33], w[41], w[3], pars->GC_100, amp[422]); 
  FFV1_0(w[37], w[25], w[15], pars->GC_2, amp[423]); 
  FFV1_0(w[22], w[25], w[28], pars->GC_11, amp[424]); 
  FFV2_5_0(w[37], w[25], w[17], pars->GC_51, pars->GC_58, amp[425]); 
  FFV1_0(w[23], w[25], w[28], pars->GC_11, amp[426]); 
  FFV2_0(w[37], w[29], w[2], pars->GC_100, amp[427]); 
  FFV1_0(w[8], w[4], w[27], pars->GC_11, amp[428]); 
  FFV1_0(w[6], w[11], w[27], pars->GC_11, amp[429]); 
  FFV2_0(w[31], w[34], w[3], pars->GC_100, amp[430]); 
  FFV1_0(w[31], w[29], w[12], pars->GC_11, amp[431]); 
  FFV1_0(w[35], w[25], w[15], pars->GC_2, amp[432]); 
  FFV1_0(w[26], w[34], w[15], pars->GC_2, amp[433]); 
  FFV2_5_0(w[35], w[25], w[17], pars->GC_51, pars->GC_58, amp[434]); 
  FFV2_5_0(w[26], w[34], w[17], pars->GC_51, pars->GC_58, amp[435]); 
  FFV2_0(w[35], w[29], w[2], pars->GC_100, amp[436]); 
  FFV1_0(w[36], w[4], w[15], pars->GC_2, amp[437]); 
  FFV1_0(w[22], w[4], w[27], pars->GC_11, amp[438]); 
  FFV2_5_0(w[36], w[4], w[17], pars->GC_51, pars->GC_58, amp[439]); 
  FFV1_0(w[23], w[4], w[27], pars->GC_11, amp[440]); 
  FFV2_0(w[36], w[11], w[2], pars->GC_100, amp[441]); 
  FFV1_0(w[8], w[4], w[27], pars->GC_11, amp[442]); 
  FFV1_0(w[6], w[11], w[27], pars->GC_11, amp[443]); 
  FFV2_0(w[46], w[34], w[2], pars->GC_100, amp[444]); 
  FFV1_0(w[46], w[45], w[12], pars->GC_11, amp[445]); 
  FFV1_0(w[35], w[25], w[15], pars->GC_1, amp[446]); 
  FFV1_0(w[26], w[34], w[15], pars->GC_1, amp[447]); 
  FFV2_3_0(w[35], w[25], w[17], pars->GC_50, pars->GC_58, amp[448]); 
  FFV2_3_0(w[26], w[34], w[17], pars->GC_50, pars->GC_58, amp[449]); 
  FFV2_0(w[35], w[45], w[3], pars->GC_100, amp[450]); 
  FFV1_0(w[36], w[4], w[15], pars->GC_2, amp[451]); 
  FFV1_0(w[22], w[4], w[27], pars->GC_11, amp[452]); 
  FFV2_5_0(w[36], w[4], w[17], pars->GC_51, pars->GC_58, amp[453]); 
  FFV1_0(w[23], w[4], w[27], pars->GC_11, amp[454]); 
  FFV2_0(w[36], w[11], w[2], pars->GC_100, amp[455]); 
  FFV1_0(w[39], w[4], w[10], pars->GC_11, amp[456]); 
  FFV1_0(w[38], w[41], w[10], pars->GC_11, amp[457]); 
  FFV2_0(w[42], w[14], w[2], pars->GC_100, amp[458]); 
  FFV1_0(w[42], w[40], w[12], pars->GC_11, amp[459]); 
  FFV1_0(w[16], w[5], w[15], pars->GC_1, amp[460]); 
  FFV1_0(w[1], w[14], w[15], pars->GC_1, amp[461]); 
  FFV2_3_0(w[16], w[5], w[17], pars->GC_50, pars->GC_58, amp[462]); 
  FFV2_3_0(w[1], w[14], w[17], pars->GC_50, pars->GC_58, amp[463]); 
  FFV2_0(w[16], w[40], w[3], pars->GC_100, amp[464]); 
  FFV1_0(w[24], w[4], w[15], pars->GC_1, amp[465]); 
  FFV1_0(w[43], w[4], w[10], pars->GC_11, amp[466]); 
  FFV2_3_0(w[24], w[4], w[17], pars->GC_50, pars->GC_58, amp[467]); 
  FFV1_0(w[44], w[4], w[10], pars->GC_11, amp[468]); 
  FFV2_0(w[24], w[41], w[3], pars->GC_100, amp[469]); 
  FFV1_0(w[75], w[25], w[12], pars->GC_11, amp[470]); 
  FFV1_0(w[31], w[29], w[12], pars->GC_11, amp[471]); 
  FFV2_0(w[38], w[76], w[2], pars->GC_100, amp[472]); 
  FFV1_0(w[38], w[41], w[27], pars->GC_11, amp[473]); 
  FFV1_0(w[36], w[4], w[15], pars->GC_1, amp[474]); 
  FFV1_0(w[0], w[76], w[15], pars->GC_1, amp[475]); 
  FFV2_3_0(w[36], w[4], w[17], pars->GC_50, pars->GC_58, amp[476]); 
  FFV2_3_0(w[0], w[76], w[17], pars->GC_50, pars->GC_58, amp[477]); 
  FFV2_0(w[36], w[41], w[3], pars->GC_100, amp[478]); 
  FFV1_0(w[35], w[25], w[15], pars->GC_2, amp[479]); 
  FFV1_0(w[78], w[25], w[12], pars->GC_11, amp[480]); 
  FFV2_5_0(w[35], w[25], w[17], pars->GC_51, pars->GC_58, amp[481]); 
  FFV1_0(w[79], w[25], w[12], pars->GC_11, amp[482]); 
  FFV2_0(w[35], w[29], w[2], pars->GC_100, amp[483]); 
  FFV1_0(w[75], w[4], w[30], pars->GC_11, amp[484]); 
  FFV1_0(w[31], w[11], w[30], pars->GC_11, amp[485]); 
  FFV2_0(w[38], w[81], w[2], pars->GC_100, amp[486]); 
  FFV1_0(w[38], w[45], w[28], pars->GC_11, amp[487]); 
  FFV1_0(w[37], w[25], w[15], pars->GC_1, amp[488]); 
  FFV1_0(w[0], w[81], w[15], pars->GC_1, amp[489]); 
  FFV2_3_0(w[37], w[25], w[17], pars->GC_50, pars->GC_58, amp[490]); 
  FFV2_3_0(w[0], w[81], w[17], pars->GC_50, pars->GC_58, amp[491]); 
  FFV2_0(w[37], w[45], w[3], pars->GC_100, amp[492]); 
  FFV1_0(w[33], w[4], w[15], pars->GC_2, amp[493]); 
  FFV1_0(w[78], w[4], w[30], pars->GC_11, amp[494]); 
  FFV2_5_0(w[33], w[4], w[17], pars->GC_51, pars->GC_58, amp[495]); 
  FFV1_0(w[79], w[4], w[30], pars->GC_11, amp[496]); 
  FFV2_0(w[33], w[11], w[2], pars->GC_100, amp[497]); 
  FFV1_0(w[39], w[25], w[28], pars->GC_11, amp[498]); 
  FFV1_0(w[38], w[45], w[28], pars->GC_11, amp[499]); 
  FFV2_0(w[46], w[32], w[2], pars->GC_100, amp[500]); 
  FFV1_0(w[46], w[41], w[30], pars->GC_11, amp[501]); 
  FFV1_0(w[33], w[4], w[15], pars->GC_1, amp[502]); 
  FFV1_0(w[26], w[32], w[15], pars->GC_1, amp[503]); 
  FFV2_3_0(w[33], w[4], w[17], pars->GC_50, pars->GC_58, amp[504]); 
  FFV2_3_0(w[26], w[32], w[17], pars->GC_50, pars->GC_58, amp[505]); 
  FFV2_0(w[33], w[41], w[3], pars->GC_100, amp[506]); 
  FFV1_0(w[37], w[25], w[15], pars->GC_1, amp[507]); 
  FFV1_0(w[43], w[25], w[28], pars->GC_11, amp[508]); 
  FFV2_3_0(w[37], w[25], w[17], pars->GC_50, pars->GC_58, amp[509]); 
  FFV1_0(w[44], w[25], w[28], pars->GC_11, amp[510]); 
  FFV2_0(w[37], w[45], w[3], pars->GC_100, amp[511]); 
  FFV1_0(w[39], w[4], w[27], pars->GC_11, amp[512]); 
  FFV1_0(w[38], w[41], w[27], pars->GC_11, amp[513]); 
  FFV2_0(w[46], w[34], w[2], pars->GC_100, amp[514]); 
  FFV1_0(w[46], w[45], w[12], pars->GC_11, amp[515]); 
  FFV1_0(w[35], w[25], w[15], pars->GC_1, amp[516]); 
  FFV1_0(w[26], w[34], w[15], pars->GC_1, amp[517]); 
  FFV2_3_0(w[35], w[25], w[17], pars->GC_50, pars->GC_58, amp[518]); 
  FFV2_3_0(w[26], w[34], w[17], pars->GC_50, pars->GC_58, amp[519]); 
  FFV2_0(w[35], w[45], w[3], pars->GC_100, amp[520]); 
  FFV1_0(w[36], w[4], w[15], pars->GC_1, amp[521]); 
  FFV1_0(w[43], w[4], w[27], pars->GC_11, amp[522]); 
  FFV2_3_0(w[36], w[4], w[17], pars->GC_50, pars->GC_58, amp[523]); 
  FFV1_0(w[44], w[4], w[27], pars->GC_11, amp[524]); 
  FFV2_0(w[36], w[41], w[3], pars->GC_100, amp[525]); 
  FFV1_0(w[51], w[47], w[27], pars->GC_11, amp[526]); 
  FFV1_0(w[49], w[52], w[27], pars->GC_11, amp[527]); 
  FFV2_0(w[31], w[54], w[3], pars->GC_100, amp[528]); 
  FFV1_0(w[31], w[29], w[53], pars->GC_11, amp[529]); 
  FFV1_0(w[55], w[25], w[15], pars->GC_2, amp[530]); 
  FFV1_0(w[26], w[54], w[15], pars->GC_2, amp[531]); 
  FFV2_5_0(w[55], w[25], w[17], pars->GC_51, pars->GC_58, amp[532]); 
  FFV2_5_0(w[26], w[54], w[17], pars->GC_51, pars->GC_58, amp[533]); 
  FFV2_0(w[55], w[29], w[2], pars->GC_100, amp[534]); 
  FFV1_0(w[62], w[47], w[15], pars->GC_2, amp[535]); 
  FFV1_0(w[60], w[47], w[27], pars->GC_11, amp[536]); 
  FFV2_5_0(w[62], w[47], w[17], pars->GC_51, pars->GC_58, amp[537]); 
  FFV1_0(w[61], w[47], w[27], pars->GC_11, amp[538]); 
  FFV2_0(w[62], w[52], w[2], pars->GC_100, amp[539]); 
  FFV1_0(w[51], w[47], w[27], pars->GC_11, amp[540]); 
  FFV1_0(w[49], w[52], w[27], pars->GC_11, amp[541]); 
  FFV2_0(w[46], w[54], w[2], pars->GC_100, amp[542]); 
  FFV1_0(w[46], w[45], w[53], pars->GC_11, amp[543]); 
  FFV1_0(w[55], w[25], w[15], pars->GC_1, amp[544]); 
  FFV1_0(w[26], w[54], w[15], pars->GC_1, amp[545]); 
  FFV2_3_0(w[55], w[25], w[17], pars->GC_50, pars->GC_58, amp[546]); 
  FFV2_3_0(w[26], w[54], w[17], pars->GC_50, pars->GC_58, amp[547]); 
  FFV2_0(w[55], w[45], w[3], pars->GC_100, amp[548]); 
  FFV1_0(w[62], w[47], w[15], pars->GC_2, amp[549]); 
  FFV1_0(w[60], w[47], w[27], pars->GC_11, amp[550]); 
  FFV2_5_0(w[62], w[47], w[17], pars->GC_51, pars->GC_58, amp[551]); 
  FFV1_0(w[61], w[47], w[27], pars->GC_11, amp[552]); 
  FFV2_0(w[62], w[52], w[2], pars->GC_100, amp[553]); 
  FFV1_0(w[64], w[47], w[27], pars->GC_11, amp[554]); 
  FFV1_0(w[63], w[65], w[27], pars->GC_11, amp[555]); 
  FFV2_0(w[46], w[54], w[2], pars->GC_100, amp[556]); 
  FFV1_0(w[46], w[45], w[53], pars->GC_11, amp[557]); 
  FFV1_0(w[55], w[25], w[15], pars->GC_1, amp[558]); 
  FFV1_0(w[26], w[54], w[15], pars->GC_1, amp[559]); 
  FFV2_3_0(w[55], w[25], w[17], pars->GC_50, pars->GC_58, amp[560]); 
  FFV2_3_0(w[26], w[54], w[17], pars->GC_50, pars->GC_58, amp[561]); 
  FFV2_0(w[55], w[45], w[3], pars->GC_100, amp[562]); 
  FFV1_0(w[62], w[47], w[15], pars->GC_1, amp[563]); 
  FFV1_0(w[66], w[47], w[27], pars->GC_11, amp[564]); 
  FFV2_3_0(w[62], w[47], w[17], pars->GC_50, pars->GC_58, amp[565]); 
  FFV1_0(w[67], w[47], w[27], pars->GC_11, amp[566]); 
  FFV2_0(w[62], w[65], w[3], pars->GC_100, amp[567]); 
  FFV1_0(w[42], w[5], w[70], pars->GC_11, amp[568]); 
  FFV1_0(w[1], w[9], w[70], pars->GC_11, amp[569]); 
  FFV1_0(w[13], w[5], w[85], pars->GC_11, amp[570]); 
  FFV1_0(w[1], w[40], w[85], pars->GC_11, amp[571]); 
  FFV1_0(w[13], w[5], w[87], pars->GC_11, amp[572]); 
  FFV1_0(w[42], w[5], w[71], pars->GC_11, amp[573]); 
  FFV1_0(w[1], w[9], w[71], pars->GC_11, amp[574]); 
  FFV1_0(w[1], w[40], w[87], pars->GC_11, amp[575]); 
  FFV1_0(w[46], w[4], w[72], pars->GC_11, amp[576]); 
  FFV1_0(w[26], w[11], w[72], pars->GC_11, amp[577]); 
  FFV1_0(w[31], w[4], w[89], pars->GC_11, amp[578]); 
  FFV1_0(w[26], w[41], w[89], pars->GC_11, amp[579]); 
  FFV1_0(w[31], w[4], w[90], pars->GC_11, amp[580]); 
  FFV1_0(w[46], w[4], w[73], pars->GC_11, amp[581]); 
  FFV1_0(w[26], w[11], w[73], pars->GC_11, amp[582]); 
  FFV1_0(w[26], w[41], w[90], pars->GC_11, amp[583]); 
  FFV1_0(w[46], w[25], w[70], pars->GC_11, amp[584]); 
  FFV1_0(w[26], w[29], w[70], pars->GC_11, amp[585]); 
  FFV1_0(w[31], w[25], w[85], pars->GC_11, amp[586]); 
  FFV1_0(w[26], w[45], w[85], pars->GC_11, amp[587]); 
  FFV1_0(w[31], w[25], w[87], pars->GC_11, amp[588]); 
  FFV1_0(w[46], w[25], w[71], pars->GC_11, amp[589]); 
  FFV1_0(w[26], w[29], w[71], pars->GC_11, amp[590]); 
  FFV1_0(w[26], w[45], w[87], pars->GC_11, amp[591]); 
  FFV1_0(w[42], w[5], w[70], pars->GC_11, amp[592]); 
  FFV1_0(w[1], w[9], w[70], pars->GC_11, amp[593]); 
  FFV1_0(w[13], w[5], w[85], pars->GC_11, amp[594]); 
  FFV1_0(w[1], w[40], w[85], pars->GC_11, amp[595]); 
  FFV1_0(w[13], w[5], w[87], pars->GC_11, amp[596]); 
  FFV1_0(w[42], w[5], w[71], pars->GC_11, amp[597]); 
  FFV1_0(w[1], w[9], w[71], pars->GC_11, amp[598]); 
  FFV1_0(w[1], w[40], w[87], pars->GC_11, amp[599]); 
  FFV1_0(w[46], w[4], w[72], pars->GC_11, amp[600]); 
  FFV1_0(w[26], w[11], w[72], pars->GC_11, amp[601]); 
  FFV1_0(w[31], w[4], w[89], pars->GC_11, amp[602]); 
  FFV1_0(w[26], w[41], w[89], pars->GC_11, amp[603]); 
  FFV1_0(w[31], w[4], w[90], pars->GC_11, amp[604]); 
  FFV1_0(w[46], w[4], w[73], pars->GC_11, amp[605]); 
  FFV1_0(w[26], w[11], w[73], pars->GC_11, amp[606]); 
  FFV1_0(w[26], w[41], w[90], pars->GC_11, amp[607]); 
  FFV1_0(w[46], w[25], w[70], pars->GC_11, amp[608]); 
  FFV1_0(w[26], w[29], w[70], pars->GC_11, amp[609]); 
  FFV1_0(w[31], w[25], w[85], pars->GC_11, amp[610]); 
  FFV1_0(w[26], w[45], w[85], pars->GC_11, amp[611]); 
  FFV1_0(w[31], w[25], w[87], pars->GC_11, amp[612]); 
  FFV1_0(w[46], w[25], w[71], pars->GC_11, amp[613]); 
  FFV1_0(w[26], w[29], w[71], pars->GC_11, amp[614]); 
  FFV1_0(w[26], w[45], w[87], pars->GC_11, amp[615]); 
  FFV1_0(w[46], w[25], w[91], pars->GC_11, amp[616]); 
  FFV1_0(w[26], w[29], w[91], pars->GC_11, amp[617]); 
  FFV1_0(w[31], w[25], w[92], pars->GC_11, amp[618]); 
  FFV1_0(w[26], w[45], w[92], pars->GC_11, amp[619]); 
  FFV1_0(w[31], w[25], w[94], pars->GC_11, amp[620]); 
  FFV1_0(w[46], w[25], w[96], pars->GC_11, amp[621]); 
  FFV1_0(w[26], w[29], w[96], pars->GC_11, amp[622]); 
  FFV1_0(w[26], w[45], w[94], pars->GC_11, amp[623]); 
  FFV1_0(w[46], w[25], w[91], pars->GC_11, amp[624]); 
  FFV1_0(w[26], w[29], w[91], pars->GC_11, amp[625]); 
  FFV1_0(w[31], w[25], w[92], pars->GC_11, amp[626]); 
  FFV1_0(w[26], w[45], w[92], pars->GC_11, amp[627]); 
  FFV1_0(w[31], w[25], w[94], pars->GC_11, amp[628]); 
  FFV1_0(w[46], w[25], w[96], pars->GC_11, amp[629]); 
  FFV1_0(w[26], w[29], w[96], pars->GC_11, amp[630]); 
  FFV1_0(w[26], w[45], w[94], pars->GC_11, amp[631]); 
  FFV1_0(w[42], w[4], w[68], pars->GC_11, amp[632]); 
  FFV1_0(w[1], w[11], w[68], pars->GC_11, amp[633]); 
  FFV1_0(w[42], w[4], w[69], pars->GC_11, amp[634]); 
  FFV1_0(w[1], w[11], w[69], pars->GC_11, amp[635]); 
  FFV1_0(w[46], w[25], w[70], pars->GC_11, amp[636]); 
  FFV1_0(w[26], w[29], w[70], pars->GC_11, amp[637]); 
  FFV1_0(w[46], w[25], w[71], pars->GC_11, amp[638]); 
  FFV1_0(w[26], w[29], w[71], pars->GC_11, amp[639]); 
  FFV1_0(w[46], w[4], w[72], pars->GC_11, amp[640]); 
  FFV1_0(w[26], w[11], w[72], pars->GC_11, amp[641]); 
  FFV1_0(w[46], w[4], w[73], pars->GC_11, amp[642]); 
  FFV1_0(w[26], w[11], w[73], pars->GC_11, amp[643]); 
  FFV1_0(w[38], w[25], w[74], pars->GC_11, amp[644]); 
  FFV1_0(w[0], w[29], w[74], pars->GC_11, amp[645]); 
  FFV1_0(w[38], w[25], w[77], pars->GC_11, amp[646]); 
  FFV1_0(w[0], w[29], w[77], pars->GC_11, amp[647]); 
  FFV1_0(w[38], w[4], w[80], pars->GC_11, amp[648]); 
  FFV1_0(w[0], w[11], w[80], pars->GC_11, amp[649]); 
  FFV1_0(w[38], w[4], w[82], pars->GC_11, amp[650]); 
  FFV1_0(w[0], w[11], w[82], pars->GC_11, amp[651]); 
  FFV1_0(w[46], w[47], w[83], pars->GC_11, amp[652]); 
  FFV1_0(w[26], w[52], w[83], pars->GC_11, amp[653]); 
  FFV1_0(w[46], w[47], w[84], pars->GC_11, amp[654]); 
  FFV1_0(w[26], w[52], w[84], pars->GC_11, amp[655]); 


}
double PY8MEs_R11_P164_sm_qq_wpwmqq::matrix_11_uu_wpwmuu() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 28;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[0] + amp[1] + 1./3. * amp[2] + 1./3. * amp[3] +
      1./3. * amp[4] + 1./3. * amp[5] + 1./3. * amp[6] + 1./3. * amp[7] + 1./3.
      * amp[8] + 1./3. * amp[9] + 1./3. * amp[10] + amp[11] + amp[12] + amp[13]
      + amp[14] + amp[15] + amp[16] + amp[17] + amp[18] + amp[19] + amp[20] +
      amp[21] + amp[22] + 1./3. * amp[23] + 1./3. * amp[24] + 1./3. * amp[25] +
      1./3. * amp[26] + 1./3. * amp[27]);
  jamp[1] = +1./2. * (-1./3. * amp[0] - 1./3. * amp[1] - amp[2] - amp[3] -
      amp[4] - amp[5] - amp[6] - amp[7] - amp[8] - amp[9] - amp[10] - 1./3. *
      amp[11] - 1./3. * amp[12] - 1./3. * amp[13] - 1./3. * amp[14] - 1./3. *
      amp[15] - 1./3. * amp[16] - 1./3. * amp[17] - 1./3. * amp[18] - 1./3. *
      amp[19] - 1./3. * amp[20] - 1./3. * amp[21] - 1./3. * amp[22] - amp[23] -
      amp[24] - amp[25] - amp[26] - amp[27]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[0][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R11_P164_sm_qq_wpwmqq::matrix_11_uux_wpwmuux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 28;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[28] + amp[29] + 1./3. * amp[30] + 1./3. * amp[31] +
      1./3. * amp[32] + 1./3. * amp[33] + 1./3. * amp[34] + 1./3. * amp[35] +
      1./3. * amp[36] + 1./3. * amp[37] + 1./3. * amp[38] + amp[39] + amp[40] +
      amp[41] + amp[42] + amp[43] + amp[44] + amp[45] + amp[46] + amp[47] +
      amp[48] + amp[49] + amp[50] + 1./3. * amp[51] + 1./3. * amp[52] + 1./3. *
      amp[53] + 1./3. * amp[54] + 1./3. * amp[55]);
  jamp[1] = +1./2. * (-1./3. * amp[28] - 1./3. * amp[29] - amp[30] - amp[31] -
      amp[32] - amp[33] - amp[34] - amp[35] - amp[36] - amp[37] - amp[38] -
      1./3. * amp[39] - 1./3. * amp[40] - 1./3. * amp[41] - 1./3. * amp[42] -
      1./3. * amp[43] - 1./3. * amp[44] - 1./3. * amp[45] - 1./3. * amp[46] -
      1./3. * amp[47] - 1./3. * amp[48] - 1./3. * amp[49] - 1./3. * amp[50] -
      amp[51] - amp[52] - amp[53] - amp[54] - amp[55]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[1][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R11_P164_sm_qq_wpwmqq::matrix_11_dd_wpwmdd() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 28;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[56] + amp[57] + 1./3. * amp[58] + 1./3. * amp[59] +
      1./3. * amp[60] + 1./3. * amp[61] + 1./3. * amp[62] + 1./3. * amp[63] +
      1./3. * amp[64] + 1./3. * amp[65] + 1./3. * amp[66] + amp[67] + amp[68] +
      amp[69] + amp[70] + amp[71] + amp[72] + amp[73] + amp[74] + amp[75] +
      amp[76] + amp[77] + amp[78] + 1./3. * amp[79] + 1./3. * amp[80] + 1./3. *
      amp[81] + 1./3. * amp[82] + 1./3. * amp[83]);
  jamp[1] = +1./2. * (-1./3. * amp[56] - 1./3. * amp[57] - amp[58] - amp[59] -
      amp[60] - amp[61] - amp[62] - amp[63] - amp[64] - amp[65] - amp[66] -
      1./3. * amp[67] - 1./3. * amp[68] - 1./3. * amp[69] - 1./3. * amp[70] -
      1./3. * amp[71] - 1./3. * amp[72] - 1./3. * amp[73] - 1./3. * amp[74] -
      1./3. * amp[75] - 1./3. * amp[76] - 1./3. * amp[77] - 1./3. * amp[78] -
      amp[79] - amp[80] - amp[81] - amp[82] - amp[83]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[2][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R11_P164_sm_qq_wpwmqq::matrix_11_ddx_wpwmddx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 28;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[84] + amp[85] + 1./3. * amp[86] + 1./3. * amp[87] +
      1./3. * amp[88] + 1./3. * amp[89] + 1./3. * amp[90] + 1./3. * amp[91] +
      1./3. * amp[92] + 1./3. * amp[93] + 1./3. * amp[94] + amp[95] + amp[96] +
      amp[97] + amp[98] + amp[99] + amp[100] + amp[101] + amp[102] + amp[103] +
      amp[104] + amp[105] + amp[106] + 1./3. * amp[107] + 1./3. * amp[108] +
      1./3. * amp[109] + 1./3. * amp[110] + 1./3. * amp[111]);
  jamp[1] = +1./2. * (-1./3. * amp[84] - 1./3. * amp[85] - amp[86] - amp[87] -
      amp[88] - amp[89] - amp[90] - amp[91] - amp[92] - amp[93] - amp[94] -
      1./3. * amp[95] - 1./3. * amp[96] - 1./3. * amp[97] - 1./3. * amp[98] -
      1./3. * amp[99] - 1./3. * amp[100] - 1./3. * amp[101] - 1./3. * amp[102]
      - 1./3. * amp[103] - 1./3. * amp[104] - 1./3. * amp[105] - 1./3. *
      amp[106] - amp[107] - amp[108] - amp[109] - amp[110] - amp[111]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[3][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R11_P164_sm_qq_wpwmqq::matrix_11_uxux_wpwmuxux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 28;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[112] + amp[113] + 1./3. * amp[114] + 1./3. *
      amp[115] + 1./3. * amp[116] + 1./3. * amp[117] + 1./3. * amp[118] + 1./3.
      * amp[119] + 1./3. * amp[120] + 1./3. * amp[121] + 1./3. * amp[122] +
      amp[123] + amp[124] + amp[125] + amp[126] + amp[127] + amp[128] +
      amp[129] + amp[130] + amp[131] + amp[132] + amp[133] + amp[134] + 1./3. *
      amp[135] + 1./3. * amp[136] + 1./3. * amp[137] + 1./3. * amp[138] + 1./3.
      * amp[139]);
  jamp[1] = +1./2. * (-1./3. * amp[112] - 1./3. * amp[113] - amp[114] -
      amp[115] - amp[116] - amp[117] - amp[118] - amp[119] - amp[120] -
      amp[121] - amp[122] - 1./3. * amp[123] - 1./3. * amp[124] - 1./3. *
      amp[125] - 1./3. * amp[126] - 1./3. * amp[127] - 1./3. * amp[128] - 1./3.
      * amp[129] - 1./3. * amp[130] - 1./3. * amp[131] - 1./3. * amp[132] -
      1./3. * amp[133] - 1./3. * amp[134] - amp[135] - amp[136] - amp[137] -
      amp[138] - amp[139]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[4][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R11_P164_sm_qq_wpwmqq::matrix_11_dxdx_wpwmdxdx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 28;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[140] + amp[141] + 1./3. * amp[142] + 1./3. *
      amp[143] + 1./3. * amp[144] + 1./3. * amp[145] + 1./3. * amp[146] + 1./3.
      * amp[147] + 1./3. * amp[148] + 1./3. * amp[149] + 1./3. * amp[150] +
      amp[151] + amp[152] + amp[153] + amp[154] + amp[155] + amp[156] +
      amp[157] + amp[158] + amp[159] + amp[160] + amp[161] + amp[162] + 1./3. *
      amp[163] + 1./3. * amp[164] + 1./3. * amp[165] + 1./3. * amp[166] + 1./3.
      * amp[167]);
  jamp[1] = +1./2. * (-1./3. * amp[140] - 1./3. * amp[141] - amp[142] -
      amp[143] - amp[144] - amp[145] - amp[146] - amp[147] - amp[148] -
      amp[149] - amp[150] - 1./3. * amp[151] - 1./3. * amp[152] - 1./3. *
      amp[153] - 1./3. * amp[154] - 1./3. * amp[155] - 1./3. * amp[156] - 1./3.
      * amp[157] - 1./3. * amp[158] - 1./3. * amp[159] - 1./3. * amp[160] -
      1./3. * amp[161] - 1./3. * amp[162] - amp[163] - amp[164] - amp[165] -
      amp[166] - amp[167]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[5][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R11_P164_sm_qq_wpwmqq::matrix_11_ud_wpwmud() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 18;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[168] + 1./3. * amp[169] + 1./3. * amp[170] +
      amp[171] + 1./3. * amp[172] + 1./3. * amp[173] + 1./3. * amp[174] + 1./3.
      * amp[175] + 1./3. * amp[176] + 1./3. * amp[177] + 1./3. * amp[178] +
      amp[179] + 1./3. * amp[180] + 1./3. * amp[181] + 1./3. * amp[182] + 1./3.
      * amp[183] + 1./3. * amp[184] + amp[185]);
  jamp[1] = +1./2. * (-1./3. * amp[168] - amp[169] - amp[170] - 1./3. *
      amp[171] - amp[172] - amp[173] - amp[174] - amp[175] - amp[176] -
      amp[177] - amp[178] - 1./3. * amp[179] - amp[180] - amp[181] - amp[182] -
      amp[183] - amp[184] - 1./3. * amp[185]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[6][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R11_P164_sm_qq_wpwmqq::matrix_11_uux_wpwmddx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 18;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[186] + 1./3. * amp[187] + 1./3. * amp[188] +
      amp[189] + 1./3. * amp[190] + 1./3. * amp[191] + 1./3. * amp[192] + 1./3.
      * amp[193] + 1./3. * amp[194] + 1./3. * amp[195] + 1./3. * amp[196] +
      amp[197] + 1./3. * amp[198] + 1./3. * amp[199] + 1./3. * amp[200] + 1./3.
      * amp[201] + 1./3. * amp[202] + amp[203]);
  jamp[1] = +1./2. * (-1./3. * amp[186] - amp[187] - amp[188] - 1./3. *
      amp[189] - amp[190] - amp[191] - amp[192] - amp[193] - amp[194] -
      amp[195] - amp[196] - 1./3. * amp[197] - amp[198] - amp[199] - amp[200] -
      amp[201] - amp[202] - 1./3. * amp[203]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[7][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R11_P164_sm_qq_wpwmqq::matrix_11_udx_wpwmudx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 18;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[204] + amp[205] + amp[206] + 1./3. *
      amp[207] + amp[208] + amp[209] + amp[210] + amp[211] + amp[212] +
      amp[213] + amp[214] + 1./3. * amp[215] + amp[216] + amp[217] + amp[218] +
      amp[219] + amp[220] + 1./3. * amp[221]);
  jamp[1] = +1./2. * (-amp[204] - 1./3. * amp[205] - 1./3. * amp[206] -
      amp[207] - 1./3. * amp[208] - 1./3. * amp[209] - 1./3. * amp[210] - 1./3.
      * amp[211] - 1./3. * amp[212] - 1./3. * amp[213] - 1./3. * amp[214] -
      amp[215] - 1./3. * amp[216] - 1./3. * amp[217] - 1./3. * amp[218] - 1./3.
      * amp[219] - 1./3. * amp[220] - amp[221]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[8][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R11_P164_sm_qq_wpwmqq::matrix_11_dux_wpwmdux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 18;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[222] + amp[223] + amp[224] + 1./3. *
      amp[225] + amp[226] + amp[227] + amp[228] + amp[229] + amp[230] +
      amp[231] + amp[232] + 1./3. * amp[233] + amp[234] + amp[235] + amp[236] +
      amp[237] + amp[238] + 1./3. * amp[239]);
  jamp[1] = +1./2. * (-amp[222] - 1./3. * amp[223] - 1./3. * amp[224] -
      amp[225] - 1./3. * amp[226] - 1./3. * amp[227] - 1./3. * amp[228] - 1./3.
      * amp[229] - 1./3. * amp[230] - 1./3. * amp[231] - 1./3. * amp[232] -
      amp[233] - 1./3. * amp[234] - 1./3. * amp[235] - 1./3. * amp[236] - 1./3.
      * amp[237] - 1./3. * amp[238] - amp[239]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[9][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R11_P164_sm_qq_wpwmqq::matrix_11_ddx_wpwmuux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 18;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[240] + 1./3. * amp[241] + 1./3. * amp[242] +
      amp[243] + 1./3. * amp[244] + 1./3. * amp[245] + 1./3. * amp[246] + 1./3.
      * amp[247] + 1./3. * amp[248] + 1./3. * amp[249] + 1./3. * amp[250] +
      amp[251] + 1./3. * amp[252] + 1./3. * amp[253] + 1./3. * amp[254] + 1./3.
      * amp[255] + 1./3. * amp[256] + amp[257]);
  jamp[1] = +1./2. * (-1./3. * amp[240] - amp[241] - amp[242] - 1./3. *
      amp[243] - amp[244] - amp[245] - amp[246] - amp[247] - amp[248] -
      amp[249] - amp[250] - 1./3. * amp[251] - amp[252] - amp[253] - amp[254] -
      amp[255] - amp[256] - 1./3. * amp[257]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[10][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R11_P164_sm_qq_wpwmqq::matrix_11_uxdx_wpwmuxdx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 18;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[258] + 1./3. * amp[259] + 1./3. * amp[260] +
      amp[261] + 1./3. * amp[262] + 1./3. * amp[263] + 1./3. * amp[264] + 1./3.
      * amp[265] + 1./3. * amp[266] + 1./3. * amp[267] + 1./3. * amp[268] +
      amp[269] + 1./3. * amp[270] + 1./3. * amp[271] + 1./3. * amp[272] + 1./3.
      * amp[273] + 1./3. * amp[274] + amp[275]);
  jamp[1] = +1./2. * (-1./3. * amp[258] - amp[259] - amp[260] - 1./3. *
      amp[261] - amp[262] - amp[263] - amp[264] - amp[265] - amp[266] -
      amp[267] - amp[268] - 1./3. * amp[269] - amp[270] - amp[271] - amp[272] -
      amp[273] - amp[274] - 1./3. * amp[275]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[11][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R11_P164_sm_qq_wpwmqq::matrix_11_uu_wpwpdd() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 16;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[276] + amp[277] + amp[278] + 1./3. *
      amp[279] + 1./3. * amp[280] + amp[281] + amp[282] + 1./3. * amp[283] +
      1./3. * amp[284] + amp[285] + 1./3. * amp[286] + amp[287] + 1./3. *
      amp[288] + amp[289] + amp[290] + 1./3. * amp[291]);
  jamp[1] = +1./2. * (-amp[276] - 1./3. * amp[277] - 1./3. * amp[278] -
      amp[279] - amp[280] - 1./3. * amp[281] - 1./3. * amp[282] - amp[283] -
      amp[284] - 1./3. * amp[285] - amp[286] - 1./3. * amp[287] - amp[288] -
      1./3. * amp[289] - 1./3. * amp[290] - amp[291]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[12][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R11_P164_sm_qq_wpwmqq::matrix_11_udx_wpwpdux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 16;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[292] + amp[293] + amp[294] + 1./3. *
      amp[295] + 1./3. * amp[296] + amp[297] + amp[298] + 1./3. * amp[299] +
      1./3. * amp[300] + amp[301] + 1./3. * amp[302] + amp[303] + 1./3. *
      amp[304] + amp[305] + amp[306] + 1./3. * amp[307]);
  jamp[1] = +1./2. * (-amp[292] - 1./3. * amp[293] - 1./3. * amp[294] -
      amp[295] - amp[296] - 1./3. * amp[297] - 1./3. * amp[298] - amp[299] -
      amp[300] - 1./3. * amp[301] - amp[302] - 1./3. * amp[303] - amp[304] -
      1./3. * amp[305] - 1./3. * amp[306] - amp[307]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[13][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R11_P164_sm_qq_wpwmqq::matrix_11_dd_wmwmuu() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 16;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[308] + amp[309] + amp[310] + 1./3. *
      amp[311] + 1./3. * amp[312] + amp[313] + amp[314] + 1./3. * amp[315] +
      1./3. * amp[316] + amp[317] + 1./3. * amp[318] + amp[319] + 1./3. *
      amp[320] + amp[321] + amp[322] + 1./3. * amp[323]);
  jamp[1] = +1./2. * (-amp[308] - 1./3. * amp[309] - 1./3. * amp[310] -
      amp[311] - amp[312] - 1./3. * amp[313] - 1./3. * amp[314] - amp[315] -
      amp[316] - 1./3. * amp[317] - amp[318] - 1./3. * amp[319] - amp[320] -
      1./3. * amp[321] - 1./3. * amp[322] - amp[323]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[14][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R11_P164_sm_qq_wpwmqq::matrix_11_dux_wmwmudx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 16;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[324] + amp[325] + amp[326] + 1./3. *
      amp[327] + 1./3. * amp[328] + amp[329] + amp[330] + 1./3. * amp[331] +
      1./3. * amp[332] + amp[333] + 1./3. * amp[334] + amp[335] + 1./3. *
      amp[336] + amp[337] + amp[338] + 1./3. * amp[339]);
  jamp[1] = +1./2. * (-amp[324] - 1./3. * amp[325] - 1./3. * amp[326] -
      amp[327] - amp[328] - 1./3. * amp[329] - 1./3. * amp[330] - amp[331] -
      amp[332] - 1./3. * amp[333] - amp[334] - 1./3. * amp[335] - amp[336] -
      1./3. * amp[337] - 1./3. * amp[338] - amp[339]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[15][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R11_P164_sm_qq_wpwmqq::matrix_11_uxux_wmwmdxdx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 16;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[340] + amp[341] + amp[342] + 1./3. *
      amp[343] + 1./3. * amp[344] + amp[345] + amp[346] + 1./3. * amp[347] +
      1./3. * amp[348] + amp[349] + 1./3. * amp[350] + amp[351] + 1./3. *
      amp[352] + amp[353] + amp[354] + 1./3. * amp[355]);
  jamp[1] = +1./2. * (-amp[340] - 1./3. * amp[341] - 1./3. * amp[342] -
      amp[343] - amp[344] - 1./3. * amp[345] - 1./3. * amp[346] - amp[347] -
      amp[348] - 1./3. * amp[349] - amp[350] - 1./3. * amp[351] - amp[352] -
      1./3. * amp[353] - 1./3. * amp[354] - amp[355]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[16][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R11_P164_sm_qq_wpwmqq::matrix_11_dxdx_wpwpuxux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 16;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[356] + amp[357] + amp[358] + 1./3. *
      amp[359] + 1./3. * amp[360] + amp[361] + amp[362] + 1./3. * amp[363] +
      1./3. * amp[364] + amp[365] + 1./3. * amp[366] + amp[367] + 1./3. *
      amp[368] + amp[369] + amp[370] + 1./3. * amp[371]);
  jamp[1] = +1./2. * (-amp[356] - 1./3. * amp[357] - 1./3. * amp[358] -
      amp[359] - amp[360] - 1./3. * amp[361] - 1./3. * amp[362] - amp[363] -
      amp[364] - 1./3. * amp[365] - amp[366] - 1./3. * amp[367] - amp[368] -
      1./3. * amp[369] - 1./3. * amp[370] - amp[371]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[17][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R11_P164_sm_qq_wpwmqq::matrix_11_uc_wpwmuc() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 14;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[372] + 1./3. * amp[373] + 1./3. * amp[374] +
      1./3. * amp[375] + 1./3. * amp[376] + 1./3. * amp[377] + 1./3. * amp[378]
      + 1./3. * amp[379] + 1./3. * amp[380] + 1./3. * amp[381] + 1./3. *
      amp[382] + 1./3. * amp[383] + 1./3. * amp[384] + 1./3. * amp[385]);
  jamp[1] = +1./2. * (-amp[372] - amp[373] - amp[374] - amp[375] - amp[376] -
      amp[377] - amp[378] - amp[379] - amp[380] - amp[381] - amp[382] -
      amp[383] - amp[384] - amp[385]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[18][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R11_P164_sm_qq_wpwmqq::matrix_11_us_wpwmus() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 14;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[386] + 1./3. * amp[387] + 1./3. * amp[388] +
      1./3. * amp[389] + 1./3. * amp[390] + 1./3. * amp[391] + 1./3. * amp[392]
      + 1./3. * amp[393] + 1./3. * amp[394] + 1./3. * amp[395] + 1./3. *
      amp[396] + 1./3. * amp[397] + 1./3. * amp[398] + 1./3. * amp[399]);
  jamp[1] = +1./2. * (-amp[386] - amp[387] - amp[388] - amp[389] - amp[390] -
      amp[391] - amp[392] - amp[393] - amp[394] - amp[395] - amp[396] -
      amp[397] - amp[398] - amp[399]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[19][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R11_P164_sm_qq_wpwmqq::matrix_11_uux_wpwmccx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 14;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[400] + 1./3. * amp[401] + 1./3. * amp[402] +
      1./3. * amp[403] + 1./3. * amp[404] + 1./3. * amp[405] + 1./3. * amp[406]
      + 1./3. * amp[407] + 1./3. * amp[408] + 1./3. * amp[409] + 1./3. *
      amp[410] + 1./3. * amp[411] + 1./3. * amp[412] + 1./3. * amp[413]);
  jamp[1] = +1./2. * (-amp[400] - amp[401] - amp[402] - amp[403] - amp[404] -
      amp[405] - amp[406] - amp[407] - amp[408] - amp[409] - amp[410] -
      amp[411] - amp[412] - amp[413]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[20][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R11_P164_sm_qq_wpwmqq::matrix_11_uux_wpwmssx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 14;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[414] + 1./3. * amp[415] + 1./3. * amp[416] +
      1./3. * amp[417] + 1./3. * amp[418] + 1./3. * amp[419] + 1./3. * amp[420]
      + 1./3. * amp[421] + 1./3. * amp[422] + 1./3. * amp[423] + 1./3. *
      amp[424] + 1./3. * amp[425] + 1./3. * amp[426] + 1./3. * amp[427]);
  jamp[1] = +1./2. * (-amp[414] - amp[415] - amp[416] - amp[417] - amp[418] -
      amp[419] - amp[420] - amp[421] - amp[422] - amp[423] - amp[424] -
      amp[425] - amp[426] - amp[427]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[21][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R11_P164_sm_qq_wpwmqq::matrix_11_ucx_wpwmucx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 14;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[428] + amp[429] + amp[430] + amp[431] + amp[432] +
      amp[433] + amp[434] + amp[435] + amp[436] + amp[437] + amp[438] +
      amp[439] + amp[440] + amp[441]);
  jamp[1] = +1./2. * (-1./3. * amp[428] - 1./3. * amp[429] - 1./3. * amp[430] -
      1./3. * amp[431] - 1./3. * amp[432] - 1./3. * amp[433] - 1./3. * amp[434]
      - 1./3. * amp[435] - 1./3. * amp[436] - 1./3. * amp[437] - 1./3. *
      amp[438] - 1./3. * amp[439] - 1./3. * amp[440] - 1./3. * amp[441]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[22][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R11_P164_sm_qq_wpwmqq::matrix_11_usx_wpwmusx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 14;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[442] + amp[443] + amp[444] + amp[445] + amp[446] +
      amp[447] + amp[448] + amp[449] + amp[450] + amp[451] + amp[452] +
      amp[453] + amp[454] + amp[455]);
  jamp[1] = +1./2. * (-1./3. * amp[442] - 1./3. * amp[443] - 1./3. * amp[444] -
      1./3. * amp[445] - 1./3. * amp[446] - 1./3. * amp[447] - 1./3. * amp[448]
      - 1./3. * amp[449] - 1./3. * amp[450] - 1./3. * amp[451] - 1./3. *
      amp[452] - 1./3. * amp[453] - 1./3. * amp[454] - 1./3. * amp[455]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[23][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R11_P164_sm_qq_wpwmqq::matrix_11_ds_wpwmds() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 14;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[456] + 1./3. * amp[457] + 1./3. * amp[458] +
      1./3. * amp[459] + 1./3. * amp[460] + 1./3. * amp[461] + 1./3. * amp[462]
      + 1./3. * amp[463] + 1./3. * amp[464] + 1./3. * amp[465] + 1./3. *
      amp[466] + 1./3. * amp[467] + 1./3. * amp[468] + 1./3. * amp[469]);
  jamp[1] = +1./2. * (-amp[456] - amp[457] - amp[458] - amp[459] - amp[460] -
      amp[461] - amp[462] - amp[463] - amp[464] - amp[465] - amp[466] -
      amp[467] - amp[468] - amp[469]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[24][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R11_P164_sm_qq_wpwmqq::matrix_11_dcx_wpwmdcx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 14;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[470] + amp[471] + amp[472] + amp[473] + amp[474] +
      amp[475] + amp[476] + amp[477] + amp[478] + amp[479] + amp[480] +
      amp[481] + amp[482] + amp[483]);
  jamp[1] = +1./2. * (-1./3. * amp[470] - 1./3. * amp[471] - 1./3. * amp[472] -
      1./3. * amp[473] - 1./3. * amp[474] - 1./3. * amp[475] - 1./3. * amp[476]
      - 1./3. * amp[477] - 1./3. * amp[478] - 1./3. * amp[479] - 1./3. *
      amp[480] - 1./3. * amp[481] - 1./3. * amp[482] - 1./3. * amp[483]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[25][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R11_P164_sm_qq_wpwmqq::matrix_11_ddx_wpwmccx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 14;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[484] + 1./3. * amp[485] + 1./3. * amp[486] +
      1./3. * amp[487] + 1./3. * amp[488] + 1./3. * amp[489] + 1./3. * amp[490]
      + 1./3. * amp[491] + 1./3. * amp[492] + 1./3. * amp[493] + 1./3. *
      amp[494] + 1./3. * amp[495] + 1./3. * amp[496] + 1./3. * amp[497]);
  jamp[1] = +1./2. * (-amp[484] - amp[485] - amp[486] - amp[487] - amp[488] -
      amp[489] - amp[490] - amp[491] - amp[492] - amp[493] - amp[494] -
      amp[495] - amp[496] - amp[497]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[26][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R11_P164_sm_qq_wpwmqq::matrix_11_ddx_wpwmssx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 14;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[498] + 1./3. * amp[499] + 1./3. * amp[500] +
      1./3. * amp[501] + 1./3. * amp[502] + 1./3. * amp[503] + 1./3. * amp[504]
      + 1./3. * amp[505] + 1./3. * amp[506] + 1./3. * amp[507] + 1./3. *
      amp[508] + 1./3. * amp[509] + 1./3. * amp[510] + 1./3. * amp[511]);
  jamp[1] = +1./2. * (-amp[498] - amp[499] - amp[500] - amp[501] - amp[502] -
      amp[503] - amp[504] - amp[505] - amp[506] - amp[507] - amp[508] -
      amp[509] - amp[510] - amp[511]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[27][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R11_P164_sm_qq_wpwmqq::matrix_11_dsx_wpwmdsx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 14;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[512] + amp[513] + amp[514] + amp[515] + amp[516] +
      amp[517] + amp[518] + amp[519] + amp[520] + amp[521] + amp[522] +
      amp[523] + amp[524] + amp[525]);
  jamp[1] = +1./2. * (-1./3. * amp[512] - 1./3. * amp[513] - 1./3. * amp[514] -
      1./3. * amp[515] - 1./3. * amp[516] - 1./3. * amp[517] - 1./3. * amp[518]
      - 1./3. * amp[519] - 1./3. * amp[520] - 1./3. * amp[521] - 1./3. *
      amp[522] - 1./3. * amp[523] - 1./3. * amp[524] - 1./3. * amp[525]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[28][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R11_P164_sm_qq_wpwmqq::matrix_11_uxcx_wpwmuxcx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 14;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[526] + 1./3. * amp[527] + 1./3. * amp[528] +
      1./3. * amp[529] + 1./3. * amp[530] + 1./3. * amp[531] + 1./3. * amp[532]
      + 1./3. * amp[533] + 1./3. * amp[534] + 1./3. * amp[535] + 1./3. *
      amp[536] + 1./3. * amp[537] + 1./3. * amp[538] + 1./3. * amp[539]);
  jamp[1] = +1./2. * (-amp[526] - amp[527] - amp[528] - amp[529] - amp[530] -
      amp[531] - amp[532] - amp[533] - amp[534] - amp[535] - amp[536] -
      amp[537] - amp[538] - amp[539]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[29][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R11_P164_sm_qq_wpwmqq::matrix_11_uxsx_wpwmuxsx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 14;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[540] + 1./3. * amp[541] + 1./3. * amp[542] +
      1./3. * amp[543] + 1./3. * amp[544] + 1./3. * amp[545] + 1./3. * amp[546]
      + 1./3. * amp[547] + 1./3. * amp[548] + 1./3. * amp[549] + 1./3. *
      amp[550] + 1./3. * amp[551] + 1./3. * amp[552] + 1./3. * amp[553]);
  jamp[1] = +1./2. * (-amp[540] - amp[541] - amp[542] - amp[543] - amp[544] -
      amp[545] - amp[546] - amp[547] - amp[548] - amp[549] - amp[550] -
      amp[551] - amp[552] - amp[553]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[30][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R11_P164_sm_qq_wpwmqq::matrix_11_dxsx_wpwmdxsx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 14;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[554] + 1./3. * amp[555] + 1./3. * amp[556] +
      1./3. * amp[557] + 1./3. * amp[558] + 1./3. * amp[559] + 1./3. * amp[560]
      + 1./3. * amp[561] + 1./3. * amp[562] + 1./3. * amp[563] + 1./3. *
      amp[564] + 1./3. * amp[565] + 1./3. * amp[566] + 1./3. * amp[567]);
  jamp[1] = +1./2. * (-amp[554] - amp[555] - amp[556] - amp[557] - amp[558] -
      amp[559] - amp[560] - amp[561] - amp[562] - amp[563] - amp[564] -
      amp[565] - amp[566] - amp[567]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[31][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R11_P164_sm_qq_wpwmqq::matrix_11_uc_wpwpds() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 8;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[568] + 1./3. * amp[569] + 1./3. * amp[570] +
      1./3. * amp[571] + 1./3. * amp[572] + 1./3. * amp[573] + 1./3. * amp[574]
      + 1./3. * amp[575]);
  jamp[1] = +1./2. * (-amp[568] - amp[569] - amp[570] - amp[571] - amp[572] -
      amp[573] - amp[574] - amp[575]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[32][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R11_P164_sm_qq_wpwmqq::matrix_11_udx_wpwpscx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 8;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[576] + 1./3. * amp[577] + 1./3. * amp[578] +
      1./3. * amp[579] + 1./3. * amp[580] + 1./3. * amp[581] + 1./3. * amp[582]
      + 1./3. * amp[583]);
  jamp[1] = +1./2. * (-amp[576] - amp[577] - amp[578] - amp[579] - amp[580] -
      amp[581] - amp[582] - amp[583]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[33][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R11_P164_sm_qq_wpwmqq::matrix_11_usx_wpwpdcx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 8;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[584] + amp[585] + amp[586] + amp[587] + amp[588] +
      amp[589] + amp[590] + amp[591]);
  jamp[1] = +1./2. * (-1./3. * amp[584] - 1./3. * amp[585] - 1./3. * amp[586] -
      1./3. * amp[587] - 1./3. * amp[588] - 1./3. * amp[589] - 1./3. * amp[590]
      - 1./3. * amp[591]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[34][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R11_P164_sm_qq_wpwmqq::matrix_11_ds_wmwmuc() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 8;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[592] + 1./3. * amp[593] + 1./3. * amp[594] +
      1./3. * amp[595] + 1./3. * amp[596] + 1./3. * amp[597] + 1./3. * amp[598]
      + 1./3. * amp[599]);
  jamp[1] = +1./2. * (-amp[592] - amp[593] - amp[594] - amp[595] - amp[596] -
      amp[597] - amp[598] - amp[599]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[35][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R11_P164_sm_qq_wpwmqq::matrix_11_dux_wmwmcsx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 8;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[600] + 1./3. * amp[601] + 1./3. * amp[602] +
      1./3. * amp[603] + 1./3. * amp[604] + 1./3. * amp[605] + 1./3. * amp[606]
      + 1./3. * amp[607]);
  jamp[1] = +1./2. * (-amp[600] - amp[601] - amp[602] - amp[603] - amp[604] -
      amp[605] - amp[606] - amp[607]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[36][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R11_P164_sm_qq_wpwmqq::matrix_11_dcx_wmwmusx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 8;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[608] + amp[609] + amp[610] + amp[611] + amp[612] +
      amp[613] + amp[614] + amp[615]);
  jamp[1] = +1./2. * (-1./3. * amp[608] - 1./3. * amp[609] - 1./3. * amp[610] -
      1./3. * amp[611] - 1./3. * amp[612] - 1./3. * amp[613] - 1./3. * amp[614]
      - 1./3. * amp[615]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[37][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R11_P164_sm_qq_wpwmqq::matrix_11_uxcx_wmwmdxsx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 8;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[616] + 1./3. * amp[617] + 1./3. * amp[618] +
      1./3. * amp[619] + 1./3. * amp[620] + 1./3. * amp[621] + 1./3. * amp[622]
      + 1./3. * amp[623]);
  jamp[1] = +1./2. * (-amp[616] - amp[617] - amp[618] - amp[619] - amp[620] -
      amp[621] - amp[622] - amp[623]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[38][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R11_P164_sm_qq_wpwmqq::matrix_11_dxsx_wpwpuxcx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 8;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[624] + 1./3. * amp[625] + 1./3. * amp[626] +
      1./3. * amp[627] + 1./3. * amp[628] + 1./3. * amp[629] + 1./3. * amp[630]
      + 1./3. * amp[631]);
  jamp[1] = +1./2. * (-amp[624] - amp[625] - amp[626] - amp[627] - amp[628] -
      amp[629] - amp[630] - amp[631]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[39][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R11_P164_sm_qq_wpwmqq::matrix_11_us_wpwmcd() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 4;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[632] + amp[633] + amp[634] + amp[635]); 
  jamp[1] = +1./2. * (-1./3. * amp[632] - 1./3. * amp[633] - 1./3. * amp[634] -
      1./3. * amp[635]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[40][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R11_P164_sm_qq_wpwmqq::matrix_11_ucx_wpwmdsx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 4;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[636] + amp[637] + amp[638] + amp[639]); 
  jamp[1] = +1./2. * (-1./3. * amp[636] - 1./3. * amp[637] - 1./3. * amp[638] -
      1./3. * amp[639]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[41][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R11_P164_sm_qq_wpwmqq::matrix_11_udx_wpwmcsx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 4;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[640] + 1./3. * amp[641] + 1./3. * amp[642] +
      1./3. * amp[643]);
  jamp[1] = +1./2. * (-amp[640] - amp[641] - amp[642] - amp[643]); 

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[42][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R11_P164_sm_qq_wpwmqq::matrix_11_dux_wpwmscx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 4;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[644] + 1./3. * amp[645] + 1./3. * amp[646] +
      1./3. * amp[647]);
  jamp[1] = +1./2. * (-amp[644] - amp[645] - amp[646] - amp[647]); 

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[43][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R11_P164_sm_qq_wpwmqq::matrix_11_dsx_wpwmucx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 4;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[648] + amp[649] + amp[650] + amp[651]); 
  jamp[1] = +1./2. * (-1./3. * amp[648] - 1./3. * amp[649] - 1./3. * amp[650] -
      1./3. * amp[651]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[44][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R11_P164_sm_qq_wpwmqq::matrix_11_uxsx_wpwmcxdx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 4;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[652] + amp[653] + amp[654] + amp[655]); 
  jamp[1] = +1./2. * (-1./3. * amp[652] - 1./3. * amp[653] - 1./3. * amp[654] -
      1./3. * amp[655]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[45][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}


}  // end namespace PY8MEs_namespace

