//==========================================================================
// This file has been automatically generated for Pythia 8 by
// MadGraph5_aMC@NLO v. 2.6.0, 2017-08-16
// By the MadGraph5_aMC@NLO Development Team
// Visit launchpad.net/madgraph5 and amcatnlo.web.cern.ch
//==========================================================================

#include "PY8MEs_R19_P130_sm_qq_qqqq.h"
#include "HelAmps_sm.h"

using namespace Pythia8_sm; 

namespace PY8MEs_namespace 
{
//==========================================================================
// Class member functions for calculating the matrix elements for
// Process: u u > u u u u~ WEIGHTED<=4 @19
// Process: c c > c c c c~ WEIGHTED<=4 @19
// Process: d d > d d d d~ WEIGHTED<=4 @19
// Process: s s > s s s s~ WEIGHTED<=4 @19
// Process: u u~ > u u u~ u~ WEIGHTED<=4 @19
// Process: c c~ > c c c~ c~ WEIGHTED<=4 @19
// Process: d d~ > d d d~ d~ WEIGHTED<=4 @19
// Process: s s~ > s s s~ s~ WEIGHTED<=4 @19
// Process: u~ u~ > u u~ u~ u~ WEIGHTED<=4 @19
// Process: c~ c~ > c c~ c~ c~ WEIGHTED<=4 @19
// Process: d~ d~ > d d~ d~ d~ WEIGHTED<=4 @19
// Process: s~ s~ > s s~ s~ s~ WEIGHTED<=4 @19
// Process: u u > u u c c~ WEIGHTED<=4 @19
// Process: u u > u u d d~ WEIGHTED<=4 @19
// Process: u u > u u s s~ WEIGHTED<=4 @19
// Process: c c > c u c u~ WEIGHTED<=4 @19
// Process: c c > c c d d~ WEIGHTED<=4 @19
// Process: c c > c c s s~ WEIGHTED<=4 @19
// Process: d d > d u d u~ WEIGHTED<=4 @19
// Process: d d > d c d c~ WEIGHTED<=4 @19
// Process: d d > d d s s~ WEIGHTED<=4 @19
// Process: s s > s u s u~ WEIGHTED<=4 @19
// Process: s s > s c s c~ WEIGHTED<=4 @19
// Process: s s > s d s d~ WEIGHTED<=4 @19
// Process: u c > u u c u~ WEIGHTED<=4 @19
// Process: u d > u u d u~ WEIGHTED<=4 @19
// Process: u s > u u s u~ WEIGHTED<=4 @19
// Process: c d > c c d c~ WEIGHTED<=4 @19
// Process: c s > c c s c~ WEIGHTED<=4 @19
// Process: d s > d d s d~ WEIGHTED<=4 @19
// Process: u c > u c c c~ WEIGHTED<=4 @19
// Process: u d > u d d d~ WEIGHTED<=4 @19
// Process: u s > u s s s~ WEIGHTED<=4 @19
// Process: c d > c d d d~ WEIGHTED<=4 @19
// Process: c s > c s s s~ WEIGHTED<=4 @19
// Process: d s > d s s s~ WEIGHTED<=4 @19
// Process: u u~ > u c u~ c~ WEIGHTED<=4 @19
// Process: u u~ > u d u~ d~ WEIGHTED<=4 @19
// Process: u u~ > u s u~ s~ WEIGHTED<=4 @19
// Process: c c~ > c d c~ d~ WEIGHTED<=4 @19
// Process: c c~ > c s c~ s~ WEIGHTED<=4 @19
// Process: d d~ > d s d~ s~ WEIGHTED<=4 @19
// Process: u u~ > c c c~ c~ WEIGHTED<=4 @19
// Process: u u~ > d d d~ d~ WEIGHTED<=4 @19
// Process: u u~ > s s s~ s~ WEIGHTED<=4 @19
// Process: c c~ > u u u~ u~ WEIGHTED<=4 @19
// Process: c c~ > d d d~ d~ WEIGHTED<=4 @19
// Process: c c~ > s s s~ s~ WEIGHTED<=4 @19
// Process: d d~ > u u u~ u~ WEIGHTED<=4 @19
// Process: d d~ > c c c~ c~ WEIGHTED<=4 @19
// Process: d d~ > s s s~ s~ WEIGHTED<=4 @19
// Process: s s~ > u u u~ u~ WEIGHTED<=4 @19
// Process: s s~ > c c c~ c~ WEIGHTED<=4 @19
// Process: s s~ > d d d~ d~ WEIGHTED<=4 @19
// Process: u c~ > u u u~ c~ WEIGHTED<=4 @19
// Process: u d~ > u u u~ d~ WEIGHTED<=4 @19
// Process: u s~ > u u u~ s~ WEIGHTED<=4 @19
// Process: c u~ > c c c~ u~ WEIGHTED<=4 @19
// Process: c d~ > c c c~ d~ WEIGHTED<=4 @19
// Process: c s~ > c c c~ s~ WEIGHTED<=4 @19
// Process: d u~ > d d d~ u~ WEIGHTED<=4 @19
// Process: d c~ > d d d~ c~ WEIGHTED<=4 @19
// Process: d s~ > d d d~ s~ WEIGHTED<=4 @19
// Process: s u~ > s s s~ u~ WEIGHTED<=4 @19
// Process: s c~ > s s s~ c~ WEIGHTED<=4 @19
// Process: s d~ > s s s~ d~ WEIGHTED<=4 @19
// Process: u c~ > u c c~ c~ WEIGHTED<=4 @19
// Process: u d~ > u d d~ d~ WEIGHTED<=4 @19
// Process: u s~ > u s s~ s~ WEIGHTED<=4 @19
// Process: c u~ > c u u~ u~ WEIGHTED<=4 @19
// Process: c d~ > c d d~ d~ WEIGHTED<=4 @19
// Process: c s~ > c s s~ s~ WEIGHTED<=4 @19
// Process: d u~ > d u u~ u~ WEIGHTED<=4 @19
// Process: d c~ > d c c~ c~ WEIGHTED<=4 @19
// Process: d s~ > d s s~ s~ WEIGHTED<=4 @19
// Process: s u~ > s u u~ u~ WEIGHTED<=4 @19
// Process: s c~ > s c c~ c~ WEIGHTED<=4 @19
// Process: s d~ > s d d~ d~ WEIGHTED<=4 @19
// Process: c c~ > u c u~ c~ WEIGHTED<=4 @19
// Process: d d~ > u d u~ d~ WEIGHTED<=4 @19
// Process: d d~ > c d c~ d~ WEIGHTED<=4 @19
// Process: s s~ > u s u~ s~ WEIGHTED<=4 @19
// Process: s s~ > c s c~ s~ WEIGHTED<=4 @19
// Process: s s~ > d s d~ s~ WEIGHTED<=4 @19
// Process: u~ u~ > c u~ u~ c~ WEIGHTED<=4 @19
// Process: u~ u~ > d u~ u~ d~ WEIGHTED<=4 @19
// Process: u~ u~ > s u~ u~ s~ WEIGHTED<=4 @19
// Process: c~ c~ > u c~ u~ c~ WEIGHTED<=4 @19
// Process: c~ c~ > d c~ c~ d~ WEIGHTED<=4 @19
// Process: c~ c~ > s c~ c~ s~ WEIGHTED<=4 @19
// Process: d~ d~ > u d~ u~ d~ WEIGHTED<=4 @19
// Process: d~ d~ > c d~ c~ d~ WEIGHTED<=4 @19
// Process: d~ d~ > s d~ d~ s~ WEIGHTED<=4 @19
// Process: s~ s~ > u s~ u~ s~ WEIGHTED<=4 @19
// Process: s~ s~ > c s~ c~ s~ WEIGHTED<=4 @19
// Process: s~ s~ > d s~ d~ s~ WEIGHTED<=4 @19
// Process: u~ c~ > u u~ u~ c~ WEIGHTED<=4 @19
// Process: u~ d~ > u u~ u~ d~ WEIGHTED<=4 @19
// Process: u~ s~ > u u~ u~ s~ WEIGHTED<=4 @19
// Process: c~ d~ > c c~ c~ d~ WEIGHTED<=4 @19
// Process: c~ s~ > c c~ c~ s~ WEIGHTED<=4 @19
// Process: d~ s~ > d d~ d~ s~ WEIGHTED<=4 @19
// Process: u~ c~ > c u~ c~ c~ WEIGHTED<=4 @19
// Process: u~ d~ > d u~ d~ d~ WEIGHTED<=4 @19
// Process: u~ s~ > s u~ s~ s~ WEIGHTED<=4 @19
// Process: c~ d~ > d c~ d~ d~ WEIGHTED<=4 @19
// Process: c~ s~ > s c~ s~ s~ WEIGHTED<=4 @19
// Process: d~ s~ > s d~ s~ s~ WEIGHTED<=4 @19
// Process: u c > u c d d~ WEIGHTED<=4 @19
// Process: u c > u c s s~ WEIGHTED<=4 @19
// Process: u d > u d c c~ WEIGHTED<=4 @19
// Process: u d > u d s s~ WEIGHTED<=4 @19
// Process: u s > u s c c~ WEIGHTED<=4 @19
// Process: u s > u s d d~ WEIGHTED<=4 @19
// Process: c d > d u c u~ WEIGHTED<=4 @19
// Process: c d > c d s s~ WEIGHTED<=4 @19
// Process: c s > s u c u~ WEIGHTED<=4 @19
// Process: c s > c s d d~ WEIGHTED<=4 @19
// Process: d s > s u d u~ WEIGHTED<=4 @19
// Process: d s > s c d c~ WEIGHTED<=4 @19
// Process: u u~ > c d c~ d~ WEIGHTED<=4 @19
// Process: u u~ > c s c~ s~ WEIGHTED<=4 @19
// Process: u u~ > d s d~ s~ WEIGHTED<=4 @19
// Process: c c~ > u d u~ d~ WEIGHTED<=4 @19
// Process: c c~ > u s u~ s~ WEIGHTED<=4 @19
// Process: c c~ > d s d~ s~ WEIGHTED<=4 @19
// Process: d d~ > u s u~ s~ WEIGHTED<=4 @19
// Process: d d~ > c s c~ s~ WEIGHTED<=4 @19
// Process: u c~ > u d c~ d~ WEIGHTED<=4 @19
// Process: u c~ > u s c~ s~ WEIGHTED<=4 @19
// Process: u d~ > u c d~ c~ WEIGHTED<=4 @19
// Process: u d~ > u s d~ s~ WEIGHTED<=4 @19
// Process: u s~ > u c s~ c~ WEIGHTED<=4 @19
// Process: u s~ > u d s~ d~ WEIGHTED<=4 @19
// Process: c u~ > c d u~ d~ WEIGHTED<=4 @19
// Process: c u~ > c s u~ s~ WEIGHTED<=4 @19
// Process: c d~ > c u d~ u~ WEIGHTED<=4 @19
// Process: c d~ > c s d~ s~ WEIGHTED<=4 @19
// Process: c s~ > c u s~ u~ WEIGHTED<=4 @19
// Process: c s~ > c d s~ d~ WEIGHTED<=4 @19
// Process: d u~ > d c u~ c~ WEIGHTED<=4 @19
// Process: d u~ > d s u~ s~ WEIGHTED<=4 @19
// Process: d c~ > d u c~ u~ WEIGHTED<=4 @19
// Process: d c~ > d s c~ s~ WEIGHTED<=4 @19
// Process: d s~ > d u s~ u~ WEIGHTED<=4 @19
// Process: d s~ > d c s~ c~ WEIGHTED<=4 @19
// Process: s u~ > s c u~ c~ WEIGHTED<=4 @19
// Process: s u~ > s d u~ d~ WEIGHTED<=4 @19
// Process: s c~ > s u c~ u~ WEIGHTED<=4 @19
// Process: s c~ > s d c~ d~ WEIGHTED<=4 @19
// Process: s d~ > s u d~ u~ WEIGHTED<=4 @19
// Process: s d~ > s c d~ c~ WEIGHTED<=4 @19
// Process: d d~ > u c u~ c~ WEIGHTED<=4 @19
// Process: s s~ > u c u~ c~ WEIGHTED<=4 @19
// Process: s s~ > u d u~ d~ WEIGHTED<=4 @19
// Process: s s~ > c d c~ d~ WEIGHTED<=4 @19
// Process: u~ c~ > d u~ c~ d~ WEIGHTED<=4 @19
// Process: u~ c~ > s u~ c~ s~ WEIGHTED<=4 @19
// Process: u~ d~ > c u~ d~ c~ WEIGHTED<=4 @19
// Process: u~ d~ > s u~ d~ s~ WEIGHTED<=4 @19
// Process: u~ s~ > c u~ s~ c~ WEIGHTED<=4 @19
// Process: u~ s~ > d u~ s~ d~ WEIGHTED<=4 @19
// Process: c~ d~ > u d~ u~ c~ WEIGHTED<=4 @19
// Process: c~ d~ > s c~ d~ s~ WEIGHTED<=4 @19
// Process: c~ s~ > u s~ u~ c~ WEIGHTED<=4 @19
// Process: c~ s~ > d c~ s~ d~ WEIGHTED<=4 @19
// Process: d~ s~ > u s~ u~ d~ WEIGHTED<=4 @19
// Process: d~ s~ > c s~ c~ d~ WEIGHTED<=4 @19

// Exception class
class PY8MEs_R19_P130_sm_qq_qqqqException : public exception
{
  virtual const char * what() const throw()
  {
    return "Exception in class 'PY8MEs_R19_P130_sm_qq_qqqq'."; 
  }
}
PY8MEs_R19_P130_sm_qq_qqqq_exception; 

std::set<int> PY8MEs_R19_P130_sm_qq_qqqq::s_channel_proc = std::set<int>
    (createset<int> ());

int PY8MEs_R19_P130_sm_qq_qqqq::helicities[ncomb][nexternal] = {{-1, -1, -1,
    -1, -1, -1}, {-1, -1, -1, -1, -1, 1}, {-1, -1, -1, -1, 1, -1}, {-1, -1, -1,
    -1, 1, 1}, {-1, -1, -1, 1, -1, -1}, {-1, -1, -1, 1, -1, 1}, {-1, -1, -1, 1,
    1, -1}, {-1, -1, -1, 1, 1, 1}, {-1, -1, 1, -1, -1, -1}, {-1, -1, 1, -1, -1,
    1}, {-1, -1, 1, -1, 1, -1}, {-1, -1, 1, -1, 1, 1}, {-1, -1, 1, 1, -1, -1},
    {-1, -1, 1, 1, -1, 1}, {-1, -1, 1, 1, 1, -1}, {-1, -1, 1, 1, 1, 1}, {-1, 1,
    -1, -1, -1, -1}, {-1, 1, -1, -1, -1, 1}, {-1, 1, -1, -1, 1, -1}, {-1, 1,
    -1, -1, 1, 1}, {-1, 1, -1, 1, -1, -1}, {-1, 1, -1, 1, -1, 1}, {-1, 1, -1,
    1, 1, -1}, {-1, 1, -1, 1, 1, 1}, {-1, 1, 1, -1, -1, -1}, {-1, 1, 1, -1, -1,
    1}, {-1, 1, 1, -1, 1, -1}, {-1, 1, 1, -1, 1, 1}, {-1, 1, 1, 1, -1, -1},
    {-1, 1, 1, 1, -1, 1}, {-1, 1, 1, 1, 1, -1}, {-1, 1, 1, 1, 1, 1}, {1, -1,
    -1, -1, -1, -1}, {1, -1, -1, -1, -1, 1}, {1, -1, -1, -1, 1, -1}, {1, -1,
    -1, -1, 1, 1}, {1, -1, -1, 1, -1, -1}, {1, -1, -1, 1, -1, 1}, {1, -1, -1,
    1, 1, -1}, {1, -1, -1, 1, 1, 1}, {1, -1, 1, -1, -1, -1}, {1, -1, 1, -1, -1,
    1}, {1, -1, 1, -1, 1, -1}, {1, -1, 1, -1, 1, 1}, {1, -1, 1, 1, -1, -1}, {1,
    -1, 1, 1, -1, 1}, {1, -1, 1, 1, 1, -1}, {1, -1, 1, 1, 1, 1}, {1, 1, -1, -1,
    -1, -1}, {1, 1, -1, -1, -1, 1}, {1, 1, -1, -1, 1, -1}, {1, 1, -1, -1, 1,
    1}, {1, 1, -1, 1, -1, -1}, {1, 1, -1, 1, -1, 1}, {1, 1, -1, 1, 1, -1}, {1,
    1, -1, 1, 1, 1}, {1, 1, 1, -1, -1, -1}, {1, 1, 1, -1, -1, 1}, {1, 1, 1, -1,
    1, -1}, {1, 1, 1, -1, 1, 1}, {1, 1, 1, 1, -1, -1}, {1, 1, 1, 1, -1, 1}, {1,
    1, 1, 1, 1, -1}, {1, 1, 1, 1, 1, 1}};

// Normalization factors the various processes
// Denominators: spins, colors and identical particles
int PY8MEs_R19_P130_sm_qq_qqqq::denom_colors[nprocesses] = {9, 9, 9, 9, 9, 9,
    9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9};
int PY8MEs_R19_P130_sm_qq_qqqq::denom_hels[nprocesses] = {4, 4, 4, 4, 4, 4, 4,
    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4};
int PY8MEs_R19_P130_sm_qq_qqqq::denom_iden[nprocesses] = {6, 4, 6, 2, 2, 2, 1,
    4, 2, 2, 1, 2, 2, 2, 1, 1, 1, 1, 1};

//--------------------------------------------------------------------------
// Color config initialization
void PY8MEs_R19_P130_sm_qq_qqqq::initColorConfigs() 
{
  color_configs = vector < vec_vec_int > (); 
  jamp_nc_relative_power = vector < vec_int > (); 

  // Color flows of process Process: u u > u u u u~ WEIGHTED<=4 @19
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(0)(2)(0)(1)(0)(2)(0)(3)(0)(0)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #1
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(1)(0)(2)(0)(3)(0)(0)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #2
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(0)(1)(0)(1)(0)(2)(0)(3)(0)(0)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #3
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(1)(0)(2)(0)(3)(0)(0)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #4
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(1)(0)(2)(0)(3)(0)(0)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #5
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(1)(0)(2)(0)(3)(0)(0)(1)));
  jamp_nc_relative_power[0].push_back(0); 

  // Color flows of process Process: u u~ > u u u~ u~ WEIGHTED<=4 @19
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(2)(0)(3)(0)(0)(2)(0)(3)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #1
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(2)(0)(3)(0)(0)(3)(0)(2)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #2
  color_configs[1].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(2)(0)(3)(0)(0)(1)(0)(3)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #3
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(2)(0)(3)(0)(0)(1)(0)(2)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #4
  color_configs[1].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(2)(0)(3)(0)(0)(3)(0)(1)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #5
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(2)(0)(3)(0)(0)(2)(0)(1)));
  jamp_nc_relative_power[1].push_back(0); 

  // Color flows of process Process: u~ u~ > u u~ u~ u~ WEIGHTED<=4 @19
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #1
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #2
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #3
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(3)(0)(1)(0)(2)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #4
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #5
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(3)(0)(2)(0)(1)));
  jamp_nc_relative_power[2].push_back(0); 

  // Color flows of process Process: u u > u u c c~ WEIGHTED<=4 @19
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[3].push_back(vec_int(createvector<int>
      (1)(0)(2)(0)(1)(0)(2)(0)(3)(0)(0)(3)));
  jamp_nc_relative_power[3].push_back(-1); 
  // JAMP #1
  color_configs[3].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(1)(0)(2)(0)(3)(0)(0)(2)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #2
  color_configs[3].push_back(vec_int(createvector<int>
      (2)(0)(1)(0)(1)(0)(2)(0)(3)(0)(0)(3)));
  jamp_nc_relative_power[3].push_back(-1); 
  // JAMP #3
  color_configs[3].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(1)(0)(2)(0)(3)(0)(0)(2)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #4
  color_configs[3].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(1)(0)(2)(0)(3)(0)(0)(1)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #5
  color_configs[3].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(1)(0)(2)(0)(3)(0)(0)(1)));
  jamp_nc_relative_power[3].push_back(0); 

  // Color flows of process Process: u c > u u c u~ WEIGHTED<=4 @19
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[4].push_back(vec_int(createvector<int>
      (1)(0)(2)(0)(1)(0)(2)(0)(3)(0)(0)(3)));
  jamp_nc_relative_power[4].push_back(0); 
  // JAMP #1
  color_configs[4].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(1)(0)(2)(0)(3)(0)(0)(2)));
  jamp_nc_relative_power[4].push_back(-1); 
  // JAMP #2
  color_configs[4].push_back(vec_int(createvector<int>
      (2)(0)(1)(0)(1)(0)(2)(0)(3)(0)(0)(3)));
  jamp_nc_relative_power[4].push_back(0); 
  // JAMP #3
  color_configs[4].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(1)(0)(2)(0)(3)(0)(0)(2)));
  jamp_nc_relative_power[4].push_back(0); 
  // JAMP #4
  color_configs[4].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(1)(0)(2)(0)(3)(0)(0)(1)));
  jamp_nc_relative_power[4].push_back(-1); 
  // JAMP #5
  color_configs[4].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(1)(0)(2)(0)(3)(0)(0)(1)));
  jamp_nc_relative_power[4].push_back(0); 

  // Color flows of process Process: u c > u c c c~ WEIGHTED<=4 @19
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[5].push_back(vec_int(createvector<int>
      (1)(0)(2)(0)(1)(0)(2)(0)(3)(0)(0)(3)));
  jamp_nc_relative_power[5].push_back(-1); 
  // JAMP #1
  color_configs[5].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(1)(0)(2)(0)(3)(0)(0)(2)));
  jamp_nc_relative_power[5].push_back(-1); 
  // JAMP #2
  color_configs[5].push_back(vec_int(createvector<int>
      (2)(0)(1)(0)(1)(0)(2)(0)(3)(0)(0)(3)));
  jamp_nc_relative_power[5].push_back(0); 
  // JAMP #3
  color_configs[5].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(1)(0)(2)(0)(3)(0)(0)(2)));
  jamp_nc_relative_power[5].push_back(0); 
  // JAMP #4
  color_configs[5].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(1)(0)(2)(0)(3)(0)(0)(1)));
  jamp_nc_relative_power[5].push_back(0); 
  // JAMP #5
  color_configs[5].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(1)(0)(2)(0)(3)(0)(0)(1)));
  jamp_nc_relative_power[5].push_back(0); 

  // Color flows of process Process: u u~ > u c u~ c~ WEIGHTED<=4 @19
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[6].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(2)(0)(3)(0)(0)(2)(0)(3)));
  jamp_nc_relative_power[6].push_back(-1); 
  // JAMP #1
  color_configs[6].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(2)(0)(3)(0)(0)(3)(0)(2)));
  jamp_nc_relative_power[6].push_back(0); 
  // JAMP #2
  color_configs[6].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(2)(0)(3)(0)(0)(1)(0)(3)));
  jamp_nc_relative_power[6].push_back(-1); 
  // JAMP #3
  color_configs[6].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(2)(0)(3)(0)(0)(1)(0)(2)));
  jamp_nc_relative_power[6].push_back(0); 
  // JAMP #4
  color_configs[6].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(2)(0)(3)(0)(0)(3)(0)(1)));
  jamp_nc_relative_power[6].push_back(0); 
  // JAMP #5
  color_configs[6].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(2)(0)(3)(0)(0)(2)(0)(1)));
  jamp_nc_relative_power[6].push_back(0); 

  // Color flows of process Process: u u~ > c c c~ c~ WEIGHTED<=4 @19
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[7].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(2)(0)(3)(0)(0)(2)(0)(3)));
  jamp_nc_relative_power[7].push_back(-1); 
  // JAMP #1
  color_configs[7].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(2)(0)(3)(0)(0)(3)(0)(2)));
  jamp_nc_relative_power[7].push_back(-1); 
  // JAMP #2
  color_configs[7].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(2)(0)(3)(0)(0)(1)(0)(3)));
  jamp_nc_relative_power[7].push_back(0); 
  // JAMP #3
  color_configs[7].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(2)(0)(3)(0)(0)(1)(0)(2)));
  jamp_nc_relative_power[7].push_back(0); 
  // JAMP #4
  color_configs[7].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(2)(0)(3)(0)(0)(3)(0)(1)));
  jamp_nc_relative_power[7].push_back(0); 
  // JAMP #5
  color_configs[7].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(2)(0)(3)(0)(0)(2)(0)(1)));
  jamp_nc_relative_power[7].push_back(0); 

  // Color flows of process Process: u c~ > u u u~ c~ WEIGHTED<=4 @19
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[8].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(2)(0)(3)(0)(0)(2)(0)(3)));
  jamp_nc_relative_power[8].push_back(0); 
  // JAMP #1
  color_configs[8].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(2)(0)(3)(0)(0)(3)(0)(2)));
  jamp_nc_relative_power[8].push_back(0); 
  // JAMP #2
  color_configs[8].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(2)(0)(3)(0)(0)(1)(0)(3)));
  jamp_nc_relative_power[8].push_back(0); 
  // JAMP #3
  color_configs[8].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(2)(0)(3)(0)(0)(1)(0)(2)));
  jamp_nc_relative_power[8].push_back(0); 
  // JAMP #4
  color_configs[8].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(2)(0)(3)(0)(0)(3)(0)(1)));
  jamp_nc_relative_power[8].push_back(-1); 
  // JAMP #5
  color_configs[8].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(2)(0)(3)(0)(0)(2)(0)(1)));
  jamp_nc_relative_power[8].push_back(-1); 

  // Color flows of process Process: u c~ > u c c~ c~ WEIGHTED<=4 @19
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[9].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(2)(0)(3)(0)(0)(2)(0)(3)));
  jamp_nc_relative_power[9].push_back(0); 
  // JAMP #1
  color_configs[9].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(2)(0)(3)(0)(0)(3)(0)(2)));
  jamp_nc_relative_power[9].push_back(0); 
  // JAMP #2
  color_configs[9].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(2)(0)(3)(0)(0)(1)(0)(3)));
  jamp_nc_relative_power[9].push_back(-1); 
  // JAMP #3
  color_configs[9].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(2)(0)(3)(0)(0)(1)(0)(2)));
  jamp_nc_relative_power[9].push_back(0); 
  // JAMP #4
  color_configs[9].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(2)(0)(3)(0)(0)(3)(0)(1)));
  jamp_nc_relative_power[9].push_back(-1); 
  // JAMP #5
  color_configs[9].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(2)(0)(3)(0)(0)(2)(0)(1)));
  jamp_nc_relative_power[9].push_back(0); 

  // Color flows of process Process: c c~ > u c u~ c~ WEIGHTED<=4 @19
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[10].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(2)(0)(3)(0)(0)(2)(0)(3)));
  jamp_nc_relative_power[10].push_back(-1); 
  // JAMP #1
  color_configs[10].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(2)(0)(3)(0)(0)(3)(0)(2)));
  jamp_nc_relative_power[10].push_back(0); 
  // JAMP #2
  color_configs[10].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(2)(0)(3)(0)(0)(1)(0)(3)));
  jamp_nc_relative_power[10].push_back(0); 
  // JAMP #3
  color_configs[10].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(2)(0)(3)(0)(0)(1)(0)(2)));
  jamp_nc_relative_power[10].push_back(0); 
  // JAMP #4
  color_configs[10].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(2)(0)(3)(0)(0)(3)(0)(1)));
  jamp_nc_relative_power[10].push_back(0); 
  // JAMP #5
  color_configs[10].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(2)(0)(3)(0)(0)(2)(0)(1)));
  jamp_nc_relative_power[10].push_back(-1); 

  // Color flows of process Process: u~ u~ > c u~ u~ c~ WEIGHTED<=4 @19
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[11].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[11].push_back(-1); 
  // JAMP #1
  color_configs[11].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[11].push_back(0); 
  // JAMP #2
  color_configs[11].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[11].push_back(-1); 
  // JAMP #3
  color_configs[11].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(3)(0)(1)(0)(2)));
  jamp_nc_relative_power[11].push_back(0); 
  // JAMP #4
  color_configs[11].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[11].push_back(0); 
  // JAMP #5
  color_configs[11].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(3)(0)(2)(0)(1)));
  jamp_nc_relative_power[11].push_back(0); 

  // Color flows of process Process: u~ c~ > u u~ u~ c~ WEIGHTED<=4 @19
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[12].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[12].push_back(0); 
  // JAMP #1
  color_configs[12].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[12].push_back(-1); 
  // JAMP #2
  color_configs[12].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[12].push_back(0); 
  // JAMP #3
  color_configs[12].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(3)(0)(1)(0)(2)));
  jamp_nc_relative_power[12].push_back(-1); 
  // JAMP #4
  color_configs[12].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[12].push_back(0); 
  // JAMP #5
  color_configs[12].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(3)(0)(2)(0)(1)));
  jamp_nc_relative_power[12].push_back(0); 

  // Color flows of process Process: u~ c~ > c u~ c~ c~ WEIGHTED<=4 @19
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[13].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[13].push_back(-1); 
  // JAMP #1
  color_configs[13].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[13].push_back(-1); 
  // JAMP #2
  color_configs[13].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[13].push_back(0); 
  // JAMP #3
  color_configs[13].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(3)(0)(1)(0)(2)));
  jamp_nc_relative_power[13].push_back(0); 
  // JAMP #4
  color_configs[13].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[13].push_back(0); 
  // JAMP #5
  color_configs[13].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(3)(0)(2)(0)(1)));
  jamp_nc_relative_power[13].push_back(0); 

  // Color flows of process Process: u c > u c d d~ WEIGHTED<=4 @19
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[14].push_back(vec_int(createvector<int>
      (1)(0)(2)(0)(1)(0)(2)(0)(3)(0)(0)(3)));
  jamp_nc_relative_power[14].push_back(-2); 
  // JAMP #1
  color_configs[14].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(1)(0)(2)(0)(3)(0)(0)(2)));
  jamp_nc_relative_power[14].push_back(-1); 
  // JAMP #2
  color_configs[14].push_back(vec_int(createvector<int>
      (2)(0)(1)(0)(1)(0)(2)(0)(3)(0)(0)(3)));
  jamp_nc_relative_power[14].push_back(-1); 
  // JAMP #3
  color_configs[14].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(1)(0)(2)(0)(3)(0)(0)(2)));
  jamp_nc_relative_power[14].push_back(0); 
  // JAMP #4
  color_configs[14].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(1)(0)(2)(0)(3)(0)(0)(1)));
  jamp_nc_relative_power[14].push_back(0); 
  // JAMP #5
  color_configs[14].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(1)(0)(2)(0)(3)(0)(0)(1)));
  jamp_nc_relative_power[14].push_back(-1); 

  // Color flows of process Process: u u~ > c d c~ d~ WEIGHTED<=4 @19
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[15].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(2)(0)(3)(0)(0)(2)(0)(3)));
  jamp_nc_relative_power[15].push_back(-2); 
  // JAMP #1
  color_configs[15].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(2)(0)(3)(0)(0)(3)(0)(2)));
  jamp_nc_relative_power[15].push_back(-1); 
  // JAMP #2
  color_configs[15].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(2)(0)(3)(0)(0)(1)(0)(3)));
  jamp_nc_relative_power[15].push_back(-1); 
  // JAMP #3
  color_configs[15].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(2)(0)(3)(0)(0)(1)(0)(2)));
  jamp_nc_relative_power[15].push_back(0); 
  // JAMP #4
  color_configs[15].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(2)(0)(3)(0)(0)(3)(0)(1)));
  jamp_nc_relative_power[15].push_back(0); 
  // JAMP #5
  color_configs[15].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(2)(0)(3)(0)(0)(2)(0)(1)));
  jamp_nc_relative_power[15].push_back(-1); 

  // Color flows of process Process: u c~ > u d c~ d~ WEIGHTED<=4 @19
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[16].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(2)(0)(3)(0)(0)(2)(0)(3)));
  jamp_nc_relative_power[16].push_back(-1); 
  // JAMP #1
  color_configs[16].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(2)(0)(3)(0)(0)(3)(0)(2)));
  jamp_nc_relative_power[16].push_back(0); 
  // JAMP #2
  color_configs[16].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(2)(0)(3)(0)(0)(1)(0)(3)));
  jamp_nc_relative_power[16].push_back(-2); 
  // JAMP #3
  color_configs[16].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(2)(0)(3)(0)(0)(1)(0)(2)));
  jamp_nc_relative_power[16].push_back(-1); 
  // JAMP #4
  color_configs[16].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(2)(0)(3)(0)(0)(3)(0)(1)));
  jamp_nc_relative_power[16].push_back(-1); 
  // JAMP #5
  color_configs[16].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(2)(0)(3)(0)(0)(2)(0)(1)));
  jamp_nc_relative_power[16].push_back(0); 

  // Color flows of process Process: d d~ > u c u~ c~ WEIGHTED<=4 @19
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[17].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(2)(0)(3)(0)(0)(2)(0)(3)));
  jamp_nc_relative_power[17].push_back(-2); 
  // JAMP #1
  color_configs[17].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(2)(0)(3)(0)(0)(3)(0)(2)));
  jamp_nc_relative_power[17].push_back(-1); 
  // JAMP #2
  color_configs[17].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(2)(0)(3)(0)(0)(1)(0)(3)));
  jamp_nc_relative_power[17].push_back(-1); 
  // JAMP #3
  color_configs[17].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(2)(0)(3)(0)(0)(1)(0)(2)));
  jamp_nc_relative_power[17].push_back(0); 
  // JAMP #4
  color_configs[17].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(2)(0)(3)(0)(0)(3)(0)(1)));
  jamp_nc_relative_power[17].push_back(0); 
  // JAMP #5
  color_configs[17].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(2)(0)(3)(0)(0)(2)(0)(1)));
  jamp_nc_relative_power[17].push_back(-1); 

  // Color flows of process Process: u~ c~ > d u~ c~ d~ WEIGHTED<=4 @19
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[18].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[18].push_back(-2); 
  // JAMP #1
  color_configs[18].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[18].push_back(-1); 
  // JAMP #2
  color_configs[18].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[18].push_back(-1); 
  // JAMP #3
  color_configs[18].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(3)(0)(1)(0)(2)));
  jamp_nc_relative_power[18].push_back(0); 
  // JAMP #4
  color_configs[18].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[18].push_back(0); 
  // JAMP #5
  color_configs[18].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(3)(0)(2)(0)(1)));
  jamp_nc_relative_power[18].push_back(-1); 
}

//--------------------------------------------------------------------------
// Destructor.
PY8MEs_R19_P130_sm_qq_qqqq::~PY8MEs_R19_P130_sm_qq_qqqq() 
{
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    delete[] p[i]; 
    p[i] = NULL; 
  }
}

//--------------------------------------------------------------------------
// Invert the permutation mapping
vector<int> PY8MEs_R19_P130_sm_qq_qqqq::invert_mapping(vector<int> mapping) 
{
  vector<int> inverted_mapping; 
  for (unsigned int i = 0; i < mapping.size(); i++ )
  {
    for (unsigned int j = 0; j < mapping.size(); j++ )
    {
      if (mapping[j] == ((int)i))
      {
        inverted_mapping.push_back(j); 
        break; 
      }
    }
  }
  return inverted_mapping; 
}

//--------------------------------------------------------------------------
// Return the list of possible helicity configurations
vector < vec_int > PY8MEs_R19_P130_sm_qq_qqqq::getHelicityConfigs(vector<int>
    permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(ncomb, vector<int> (nexternal, 0)); 
  for (unsigned int ihel = 0; ihel < ncomb; ihel++ )
  {
    for(unsigned int j = 0; j < nexternal; j++ )
    {
      res[ihel][chosenPerm[j]] = helicities[ihel][j]; 
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Return the list of possible color configurations
vector < vec_int > PY8MEs_R19_P130_sm_qq_qqqq::getColorConfigs(int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(color_configs[chosenProcID].size(), vector<int>
      (nexternal * 2, 0));
  for (unsigned int icol = 0; icol < color_configs[chosenProcID].size(); icol++
      )
  {
    for(unsigned int j = 0; j < (2 * nexternal); j++ )
    {
      res[icol][chosenPerm[j/2] * 2 + j%2] =
          color_configs[chosenProcID][icol][j];
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Get JAMP relative N_c power
int PY8MEs_R19_P130_sm_qq_qqqq::getColorFlowRelativeNCPower(int color_flow_ID,
    int specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return jamp_nc_relative_power[chosenProcID][color_flow_ID]; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity ID -> Helicity Config
vector<int> PY8MEs_R19_P130_sm_qq_qqqq::getHelicityConfigForID(int hel_ID,
    vector<int> permutation)
{
  if (hel_ID < 0 || hel_ID >= ncomb)
  {
    cerr <<  "Error in function 'getHelicityConfigForID' of class" << 
    " 'PY8MEs_R19_P130_sm_qq_qqqq': Specified helicity ID '" << 
    hel_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R19_P130_sm_qq_qqqq_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(nexternal, 0); 
  for (unsigned int j = 0; j < nexternal; j++ )
  {
    res[chosenPerm[j]] = helicities[hel_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity Config -> Helicity ID
int PY8MEs_R19_P130_sm_qq_qqqq::getHelicityIDForConfig(vector<int> hel_config,
    vector<int> permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  int user_ihel = -1; 
  if (hel_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < ncomb; i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < nexternal; j++ )
      {
        if (helicities[i][chosenPerm[j]] != hel_config[j])
        {
          found = false; 
          break; 
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_ihel = i; 
        break; 
      }
    }
    if (user_ihel == -1)
    {
      cerr <<  "Error in function 'getHelicityIDForConfig' of class" << 
      " 'PY8MEs_R19_P130_sm_qq_qqqq': Specified helicity" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R19_P130_sm_qq_qqqq_exception; 
    }
  }
  return user_ihel; 
}


//--------------------------------------------------------------------------
// Implements the map Color ID -> Color Config
vector<int> PY8MEs_R19_P130_sm_qq_qqqq::getColorConfigForID(int color_ID, int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < 0 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getColorConfigForID' of class" << 
    " 'PY8MEs_R19_P130_sm_qq_qqqq': Specified color ID '" << 
    color_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R19_P130_sm_qq_qqqq_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(color_configs[chosenProcID][color_ID].size(), 0); 
  for (unsigned int j = 0; j < (2 * nexternal); j++ )
  {
    res[chosenPerm[j/2] * 2 + j%2] = color_configs[chosenProcID][color_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Color Config -> Color ID
int PY8MEs_R19_P130_sm_qq_qqqq::getColorIDForConfig(vector<int> color_config,
    int specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = -1; 
  if (color_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < color_configs[chosenProcID].size(); i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < (nexternal * 2); j++ )
      {

        // If colorless then make sure it matches
        // The little arithmetics in the color index is just
        // the permutation applies on the particle list which is
        // twice smaller since each particle can have two color indices.
        if (color_config[j] == 0)
        {
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] != 0)
          {
            found = false; 
            break; 
          }
          // Otherwise check that the color linked position matches
        }
        else
        {
          int color_linked_pos = -1; 
          // Find the other end of the line in the user color config
          for (unsigned int k = 0; k < (nexternal * 2); k++ )
          {
            if (k == j)
              continue; 
            if (color_config[j] == color_config[k])
            {
              color_linked_pos = k; 
              break; 
            }
          }
          if (color_linked_pos == -1)
          {
            cerr <<  "Error in function 'getColorIDForConfig' of class" << 
            " 'PY8MEs_R19_P130_sm_qq_qqqq': A color line could " << 
            " not be closed." << endl; 
            throw PY8MEs_R19_P130_sm_qq_qqqq_exception; 
          }
          // Now check whether the color line matches
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] !=
              color_configs[chosenProcID][i][chosenPerm[color_linked_pos/2] * 2
              + color_linked_pos%2])
          {
            found = false; 
            break; 
          }
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_icol = i; 
        break; 
      }
    }

    if (user_icol == -1)
    {
      cerr <<  "Error in function 'getColorIDForConfig' of class" << 
      " 'PY8MEs_R19_P130_sm_qq_qqqq': Specified color" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R19_P130_sm_qq_qqqq_exception; 
    }
  }
  return user_icol; 
}

//--------------------------------------------------------------------------
// Returns all result previously computed in SigmaKin
vector < vec_double > PY8MEs_R19_P130_sm_qq_qqqq::getAllResults(int
    specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return all_results[chosenProcID]; 
}

//--------------------------------------------------------------------------
// Returns a result previously computed in SigmaKin for a specific helicity
// and color ID. -1 means avg and summed over that characteristic.
double PY8MEs_R19_P130_sm_qq_qqqq::getResult(int helicity_ID, int color_ID, int
    specify_proc_ID)
{
  if (helicity_ID < - 1 || helicity_ID >= ncomb)
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R19_P130_sm_qq_qqqq': Specified helicity ID '" << 
    helicity_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R19_P130_sm_qq_qqqq_exception; 
  }
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < - 1 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R19_P130_sm_qq_qqqq': Specified color ID '" << 
    color_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R19_P130_sm_qq_qqqq_exception; 
  }
  return all_results[chosenProcID][helicity_ID + 1][color_ID + 1]; 
}

//--------------------------------------------------------------------------
// Check for the availability of the requested process and if available,
// If available, this returns the corresponding permutation and Proc_ID to use.
// If not available, this returns a negative Proc_ID.
pair < vector<int> , int > PY8MEs_R19_P130_sm_qq_qqqq::static_getPY8ME(vector<int> initial_pdgs, vector<int> final_pdgs, set<int> schannels) 
{

  // Not available return value
  pair < vector<int> , int > NA(vector<int> (), -1); 

  // Check if s-channel requirements match
  if (nreq_s_channels > 0)
  {
    if (schannels != s_channel_proc)
      return NA; 
  }
  else
  {
    if (schannels.size() != 0)
      return NA; 
  }

  // Check number of final state particles
  if (final_pdgs.size() != (nexternal - ninitial))
    return NA; 

  // Check number of initial state particles
  if (initial_pdgs.size() != ninitial)
    return NA; 

  // List of processes available in this class
  const int nprocs = 304; 
  const int proc_IDS[nprocs] = {0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3,
      3, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 6, 6, 6, 6,
      6, 6, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
      8, 8, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 10, 10, 10, 10, 10, 10, 11, 11,
      11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 12, 12, 12, 12, 12, 12, 13, 13,
      13, 13, 13, 13, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 15, 15,
      15, 15, 15, 15, 15, 15, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16,
      16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 17, 17, 17, 17, 18, 18,
      18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 1, 1, 1, 1, 4, 4, 4, 4, 4, 4, 5,
      5, 5, 5, 5, 5, 6, 6, 6, 6, 6, 6, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 8,
      8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 10,
      10, 10, 10, 10, 10, 12, 12, 12, 12, 12, 12, 13, 13, 13, 13, 13, 13, 14,
      14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 15, 15, 15, 15, 15, 15, 15,
      15, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16,
      16, 16, 16, 16, 16, 16, 16, 17, 17, 17, 17, 18, 18, 18, 18, 18, 18, 18,
      18, 18, 18, 18, 18};
  const int in_pdgs[nprocs][ninitial] = {{2, 2}, {4, 4}, {1, 1}, {3, 3}, {2,
      -2}, {4, -4}, {1, -1}, {3, -3}, {-2, -2}, {-4, -4}, {-1, -1}, {-3, -3},
      {2, 2}, {2, 2}, {2, 2}, {4, 4}, {4, 4}, {4, 4}, {1, 1}, {1, 1}, {1, 1},
      {3, 3}, {3, 3}, {3, 3}, {2, 4}, {2, 1}, {2, 3}, {4, 1}, {4, 3}, {1, 3},
      {2, 4}, {2, 1}, {2, 3}, {4, 1}, {4, 3}, {1, 3}, {2, -2}, {2, -2}, {2,
      -2}, {4, -4}, {4, -4}, {1, -1}, {2, -2}, {2, -2}, {2, -2}, {4, -4}, {4,
      -4}, {4, -4}, {1, -1}, {1, -1}, {1, -1}, {3, -3}, {3, -3}, {3, -3}, {2,
      -4}, {2, -1}, {2, -3}, {4, -2}, {4, -1}, {4, -3}, {1, -2}, {1, -4}, {1,
      -3}, {3, -2}, {3, -4}, {3, -1}, {2, -4}, {2, -1}, {2, -3}, {4, -2}, {4,
      -1}, {4, -3}, {1, -2}, {1, -4}, {1, -3}, {3, -2}, {3, -4}, {3, -1}, {4,
      -4}, {1, -1}, {1, -1}, {3, -3}, {3, -3}, {3, -3}, {-2, -2}, {-2, -2},
      {-2, -2}, {-4, -4}, {-4, -4}, {-4, -4}, {-1, -1}, {-1, -1}, {-1, -1},
      {-3, -3}, {-3, -3}, {-3, -3}, {-2, -4}, {-2, -1}, {-2, -3}, {-4, -1},
      {-4, -3}, {-1, -3}, {-2, -4}, {-2, -1}, {-2, -3}, {-4, -1}, {-4, -3},
      {-1, -3}, {2, 4}, {2, 4}, {2, 1}, {2, 1}, {2, 3}, {2, 3}, {4, 1}, {4, 1},
      {4, 3}, {4, 3}, {1, 3}, {1, 3}, {2, -2}, {2, -2}, {2, -2}, {4, -4}, {4,
      -4}, {4, -4}, {1, -1}, {1, -1}, {2, -4}, {2, -4}, {2, -1}, {2, -1}, {2,
      -3}, {2, -3}, {4, -2}, {4, -2}, {4, -1}, {4, -1}, {4, -3}, {4, -3}, {1,
      -2}, {1, -2}, {1, -4}, {1, -4}, {1, -3}, {1, -3}, {3, -2}, {3, -2}, {3,
      -4}, {3, -4}, {3, -1}, {3, -1}, {1, -1}, {3, -3}, {3, -3}, {3, -3}, {-2,
      -4}, {-2, -4}, {-2, -1}, {-2, -1}, {-2, -3}, {-2, -3}, {-4, -1}, {-4,
      -1}, {-4, -3}, {-4, -3}, {-1, -3}, {-1, -3}, {-2, 2}, {-4, 4}, {-1, 1},
      {-3, 3}, {4, 2}, {1, 2}, {3, 2}, {1, 4}, {3, 4}, {3, 1}, {4, 2}, {1, 2},
      {3, 2}, {1, 4}, {3, 4}, {3, 1}, {-2, 2}, {-2, 2}, {-2, 2}, {-4, 4}, {-4,
      4}, {-1, 1}, {-2, 2}, {-2, 2}, {-2, 2}, {-4, 4}, {-4, 4}, {-4, 4}, {-1,
      1}, {-1, 1}, {-1, 1}, {-3, 3}, {-3, 3}, {-3, 3}, {-4, 2}, {-1, 2}, {-3,
      2}, {-2, 4}, {-1, 4}, {-3, 4}, {-2, 1}, {-4, 1}, {-3, 1}, {-2, 3}, {-4,
      3}, {-1, 3}, {-4, 2}, {-1, 2}, {-3, 2}, {-2, 4}, {-1, 4}, {-3, 4}, {-2,
      1}, {-4, 1}, {-3, 1}, {-2, 3}, {-4, 3}, {-1, 3}, {-4, 4}, {-1, 1}, {-1,
      1}, {-3, 3}, {-3, 3}, {-3, 3}, {-4, -2}, {-1, -2}, {-3, -2}, {-1, -4},
      {-3, -4}, {-3, -1}, {-4, -2}, {-1, -2}, {-3, -2}, {-1, -4}, {-3, -4},
      {-3, -1}, {4, 2}, {4, 2}, {1, 2}, {1, 2}, {3, 2}, {3, 2}, {1, 4}, {1, 4},
      {3, 4}, {3, 4}, {3, 1}, {3, 1}, {-2, 2}, {-2, 2}, {-2, 2}, {-4, 4}, {-4,
      4}, {-4, 4}, {-1, 1}, {-1, 1}, {-4, 2}, {-4, 2}, {-1, 2}, {-1, 2}, {-3,
      2}, {-3, 2}, {-2, 4}, {-2, 4}, {-1, 4}, {-1, 4}, {-3, 4}, {-3, 4}, {-2,
      1}, {-2, 1}, {-4, 1}, {-4, 1}, {-3, 1}, {-3, 1}, {-2, 3}, {-2, 3}, {-4,
      3}, {-4, 3}, {-1, 3}, {-1, 3}, {-1, 1}, {-3, 3}, {-3, 3}, {-3, 3}, {-4,
      -2}, {-4, -2}, {-1, -2}, {-1, -2}, {-3, -2}, {-3, -2}, {-1, -4}, {-1,
      -4}, {-3, -4}, {-3, -4}, {-3, -1}, {-3, -1}};
  const int out_pdgs[nprocs][nexternal - ninitial] = {{2, 2, 2, -2}, {4, 4, 4,
      -4}, {1, 1, 1, -1}, {3, 3, 3, -3}, {2, 2, -2, -2}, {4, 4, -4, -4}, {1, 1,
      -1, -1}, {3, 3, -3, -3}, {2, -2, -2, -2}, {4, -4, -4, -4}, {1, -1, -1,
      -1}, {3, -3, -3, -3}, {2, 2, 4, -4}, {2, 2, 1, -1}, {2, 2, 3, -3}, {4, 2,
      4, -2}, {4, 4, 1, -1}, {4, 4, 3, -3}, {1, 2, 1, -2}, {1, 4, 1, -4}, {1,
      1, 3, -3}, {3, 2, 3, -2}, {3, 4, 3, -4}, {3, 1, 3, -1}, {2, 2, 4, -2},
      {2, 2, 1, -2}, {2, 2, 3, -2}, {4, 4, 1, -4}, {4, 4, 3, -4}, {1, 1, 3,
      -1}, {2, 4, 4, -4}, {2, 1, 1, -1}, {2, 3, 3, -3}, {4, 1, 1, -1}, {4, 3,
      3, -3}, {1, 3, 3, -3}, {2, 4, -2, -4}, {2, 1, -2, -1}, {2, 3, -2, -3},
      {4, 1, -4, -1}, {4, 3, -4, -3}, {1, 3, -1, -3}, {4, 4, -4, -4}, {1, 1,
      -1, -1}, {3, 3, -3, -3}, {2, 2, -2, -2}, {1, 1, -1, -1}, {3, 3, -3, -3},
      {2, 2, -2, -2}, {4, 4, -4, -4}, {3, 3, -3, -3}, {2, 2, -2, -2}, {4, 4,
      -4, -4}, {1, 1, -1, -1}, {2, 2, -2, -4}, {2, 2, -2, -1}, {2, 2, -2, -3},
      {4, 4, -4, -2}, {4, 4, -4, -1}, {4, 4, -4, -3}, {1, 1, -1, -2}, {1, 1,
      -1, -4}, {1, 1, -1, -3}, {3, 3, -3, -2}, {3, 3, -3, -4}, {3, 3, -3, -1},
      {2, 4, -4, -4}, {2, 1, -1, -1}, {2, 3, -3, -3}, {4, 2, -2, -2}, {4, 1,
      -1, -1}, {4, 3, -3, -3}, {1, 2, -2, -2}, {1, 4, -4, -4}, {1, 3, -3, -3},
      {3, 2, -2, -2}, {3, 4, -4, -4}, {3, 1, -1, -1}, {2, 4, -2, -4}, {2, 1,
      -2, -1}, {4, 1, -4, -1}, {2, 3, -2, -3}, {4, 3, -4, -3}, {1, 3, -1, -3},
      {4, -2, -2, -4}, {1, -2, -2, -1}, {3, -2, -2, -3}, {2, -4, -2, -4}, {1,
      -4, -4, -1}, {3, -4, -4, -3}, {2, -1, -2, -1}, {4, -1, -4, -1}, {3, -1,
      -1, -3}, {2, -3, -2, -3}, {4, -3, -4, -3}, {1, -3, -1, -3}, {2, -2, -2,
      -4}, {2, -2, -2, -1}, {2, -2, -2, -3}, {4, -4, -4, -1}, {4, -4, -4, -3},
      {1, -1, -1, -3}, {4, -2, -4, -4}, {1, -2, -1, -1}, {3, -2, -3, -3}, {1,
      -4, -1, -1}, {3, -4, -3, -3}, {3, -1, -3, -3}, {2, 4, 1, -1}, {2, 4, 3,
      -3}, {2, 1, 4, -4}, {2, 1, 3, -3}, {2, 3, 4, -4}, {2, 3, 1, -1}, {1, 2,
      4, -2}, {4, 1, 3, -3}, {3, 2, 4, -2}, {4, 3, 1, -1}, {3, 2, 1, -2}, {3,
      4, 1, -4}, {4, 1, -4, -1}, {4, 3, -4, -3}, {1, 3, -1, -3}, {2, 1, -2,
      -1}, {2, 3, -2, -3}, {1, 3, -1, -3}, {2, 3, -2, -3}, {4, 3, -4, -3}, {2,
      1, -4, -1}, {2, 3, -4, -3}, {2, 4, -1, -4}, {2, 3, -1, -3}, {2, 4, -3,
      -4}, {2, 1, -3, -1}, {4, 1, -2, -1}, {4, 3, -2, -3}, {4, 2, -1, -2}, {4,
      3, -1, -3}, {4, 2, -3, -2}, {4, 1, -3, -1}, {1, 4, -2, -4}, {1, 3, -2,
      -3}, {1, 2, -4, -2}, {1, 3, -4, -3}, {1, 2, -3, -2}, {1, 4, -3, -4}, {3,
      4, -2, -4}, {3, 1, -2, -1}, {3, 2, -4, -2}, {3, 1, -4, -1}, {3, 2, -1,
      -2}, {3, 4, -1, -4}, {2, 4, -2, -4}, {2, 4, -2, -4}, {2, 1, -2, -1}, {4,
      1, -4, -1}, {1, -2, -4, -1}, {3, -2, -4, -3}, {4, -2, -1, -4}, {3, -2,
      -1, -3}, {4, -2, -3, -4}, {1, -2, -3, -1}, {2, -1, -2, -4}, {3, -4, -1,
      -3}, {2, -3, -2, -4}, {1, -4, -3, -1}, {2, -3, -2, -1}, {4, -3, -4, -1},
      {2, 2, -2, -2}, {4, 4, -4, -4}, {1, 1, -1, -1}, {3, 3, -3, -3}, {2, 2, 4,
      -2}, {2, 2, 1, -2}, {2, 2, 3, -2}, {4, 4, 1, -4}, {4, 4, 3, -4}, {1, 1,
      3, -1}, {2, 4, 4, -4}, {2, 1, 1, -1}, {2, 3, 3, -3}, {4, 1, 1, -1}, {4,
      3, 3, -3}, {1, 3, 3, -3}, {2, 4, -2, -4}, {2, 1, -2, -1}, {2, 3, -2, -3},
      {4, 1, -4, -1}, {4, 3, -4, -3}, {1, 3, -1, -3}, {4, 4, -4, -4}, {1, 1,
      -1, -1}, {3, 3, -3, -3}, {2, 2, -2, -2}, {1, 1, -1, -1}, {3, 3, -3, -3},
      {2, 2, -2, -2}, {4, 4, -4, -4}, {3, 3, -3, -3}, {2, 2, -2, -2}, {4, 4,
      -4, -4}, {1, 1, -1, -1}, {2, 2, -2, -4}, {2, 2, -2, -1}, {2, 2, -2, -3},
      {4, 4, -4, -2}, {4, 4, -4, -1}, {4, 4, -4, -3}, {1, 1, -1, -2}, {1, 1,
      -1, -4}, {1, 1, -1, -3}, {3, 3, -3, -2}, {3, 3, -3, -4}, {3, 3, -3, -1},
      {2, 4, -4, -4}, {2, 1, -1, -1}, {2, 3, -3, -3}, {4, 2, -2, -2}, {4, 1,
      -1, -1}, {4, 3, -3, -3}, {1, 2, -2, -2}, {1, 4, -4, -4}, {1, 3, -3, -3},
      {3, 2, -2, -2}, {3, 4, -4, -4}, {3, 1, -1, -1}, {2, 4, -2, -4}, {2, 1,
      -2, -1}, {4, 1, -4, -1}, {2, 3, -2, -3}, {4, 3, -4, -3}, {1, 3, -1, -3},
      {2, -2, -2, -4}, {2, -2, -2, -1}, {2, -2, -2, -3}, {4, -4, -4, -1}, {4,
      -4, -4, -3}, {1, -1, -1, -3}, {4, -2, -4, -4}, {1, -2, -1, -1}, {3, -2,
      -3, -3}, {1, -4, -1, -1}, {3, -4, -3, -3}, {3, -1, -3, -3}, {2, 4, 1,
      -1}, {2, 4, 3, -3}, {2, 1, 4, -4}, {2, 1, 3, -3}, {2, 3, 4, -4}, {2, 3,
      1, -1}, {1, 2, 4, -2}, {4, 1, 3, -3}, {3, 2, 4, -2}, {4, 3, 1, -1}, {3,
      2, 1, -2}, {3, 4, 1, -4}, {4, 1, -4, -1}, {4, 3, -4, -3}, {1, 3, -1, -3},
      {2, 1, -2, -1}, {2, 3, -2, -3}, {1, 3, -1, -3}, {2, 3, -2, -3}, {4, 3,
      -4, -3}, {2, 1, -4, -1}, {2, 3, -4, -3}, {2, 4, -1, -4}, {2, 3, -1, -3},
      {2, 4, -3, -4}, {2, 1, -3, -1}, {4, 1, -2, -1}, {4, 3, -2, -3}, {4, 2,
      -1, -2}, {4, 3, -1, -3}, {4, 2, -3, -2}, {4, 1, -3, -1}, {1, 4, -2, -4},
      {1, 3, -2, -3}, {1, 2, -4, -2}, {1, 3, -4, -3}, {1, 2, -3, -2}, {1, 4,
      -3, -4}, {3, 4, -2, -4}, {3, 1, -2, -1}, {3, 2, -4, -2}, {3, 1, -4, -1},
      {3, 2, -1, -2}, {3, 4, -1, -4}, {2, 4, -2, -4}, {2, 4, -2, -4}, {2, 1,
      -2, -1}, {4, 1, -4, -1}, {1, -2, -4, -1}, {3, -2, -4, -3}, {4, -2, -1,
      -4}, {3, -2, -1, -3}, {4, -2, -3, -4}, {1, -2, -3, -1}, {2, -1, -2, -4},
      {3, -4, -1, -3}, {2, -3, -2, -4}, {1, -4, -3, -1}, {2, -3, -2, -1}, {4,
      -3, -4, -1}};

  bool in_pdgs_used[ninitial]; 
  bool out_pdgs_used[nexternal - ninitial]; 
  for(unsigned int i = 0; i < nprocs; i++ )
  {
    int permutations[nexternal]; 

    // Reinitialize initial state look-up variables
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      in_pdgs_used[j] = false; 
      permutations[j] = -1; 
    }
    // Look for initial state matches
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      for(unsigned int k = 0; k < ninitial; k++ )
      {
        // Make sure it has not been used already
        if (in_pdgs_used[k])
          continue; 
        if (initial_pdgs[k] == in_pdgs[i][j])
        {
          permutations[j] = k; 
          in_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[ninitial - 1] == -1)
      continue; 

    // Reinitialize final state look-up variables
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      out_pdgs_used[j] = false; 
      permutations[ninitial + j] = -1; 
    }
    // Look for final state matches
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      for(unsigned int k = 0; k < (nexternal - ninitial); k++ )
      {
        // Make sure it has not been used already
        if (out_pdgs_used[k])
          continue; 
        if (final_pdgs[k] == out_pdgs[i][j])
        {
          permutations[ninitial + j] = ninitial + k; 
          out_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[ninitial + j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[nexternal - 1] == -1)
      continue; 

    // Return process found
    return pair < vector<int> , int > (vector<int> (permutations, permutations
        + nexternal), proc_IDS[i]);
  }

  // No process found
  return NA; 
}

//--------------------------------------------------------------------------
// Set momenta
void PY8MEs_R19_P130_sm_qq_qqqq::setMomenta(vector < vec_double >
    momenta_picked)
{
  if (momenta_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setMomenta' of class" << 
    " 'PY8MEs_R19_P130_sm_qq_qqqq': Incorrect number of" << 
    " momenta specified." << endl; 
    throw PY8MEs_R19_P130_sm_qq_qqqq_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    if (momenta_picked[i].size() != 4)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R19_P130_sm_qq_qqqq': Incorrect number of" << 
      " momenta components specified." << endl; 
      throw PY8MEs_R19_P130_sm_qq_qqqq_exception; 
    }
    if (momenta_picked[i][0] < 0.0)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R19_P130_sm_qq_qqqq': A momentum was specified" << 
      " with negative energy. Check conventions." << endl; 
      throw PY8MEs_R19_P130_sm_qq_qqqq_exception; 
    }
    for (unsigned int j = 0; j < 4; j++ )
    {
      p[i][j] = momenta_picked[i][j]; 
    }
  }
}

//--------------------------------------------------------------------------
// Set color configuration to use. An empty vector means sum over all.
void PY8MEs_R19_P130_sm_qq_qqqq::setColors(vector<int> colors_picked)
{
  if (colors_picked.size() == 0)
  {
    user_colors = vector<int> (); 
    return; 
  }
  if (colors_picked.size() != (2 * nexternal))
  {
    cerr <<  "Error in function 'setColors' of class" << 
    " 'PY8MEs_R19_P130_sm_qq_qqqq': Incorrect number" << 
    " of colors specified." << endl; 
    throw PY8MEs_R19_P130_sm_qq_qqqq_exception; 
  }
  user_colors = vector<int> ((2 * nexternal), 0); 
  for(unsigned int i = 0; i < (2 * nexternal); i++ )
  {
    user_colors[i] = colors_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the helicity configuration to use. Am empty vector means sum over all.
void PY8MEs_R19_P130_sm_qq_qqqq::setHelicities(vector<int> helicities_picked) 
{
  if (helicities_picked.size() != nexternal)
  {
    if (helicities_picked.size() == 0)
    {
      user_helicities = vector<int> (); 
      return; 
    }
    cerr <<  "Error in function 'setHelicities' of class" << 
    " 'PY8MEs_R19_P130_sm_qq_qqqq': Incorrect number" << 
    " of helicities specified." << endl; 
    throw PY8MEs_R19_P130_sm_qq_qqqq_exception; 
  }
  user_helicities = vector<int> (nexternal, 0); 
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    user_helicities[i] = helicities_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the permutation to use (will apply to momenta, colors and helicities)
void PY8MEs_R19_P130_sm_qq_qqqq::setPermutation(vector<int> perm_picked) 
{
  if (perm_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setPermutations' of class" << 
    " 'PY8MEs_R19_P130_sm_qq_qqqq': Incorrect number" << 
    " of permutations specified." << endl; 
    throw PY8MEs_R19_P130_sm_qq_qqqq_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    perm[i] = perm_picked[i]; 
  }
}

// Set the proc_ID to use
void PY8MEs_R19_P130_sm_qq_qqqq::setProcID(int procID_picked) 
{
  proc_ID = procID_picked; 
}

//--------------------------------------------------------------------------
// Initialize process.

void PY8MEs_R19_P130_sm_qq_qqqq::initProc() 
{

  // Initialize flags
  include_symmetry_factors = true; 
  include_helicity_averaging_factors = true; 
  include_color_averaging_factors = true; 

  // Initialize vectors.
  perm = vector<int> (nexternal, 0); 
  user_colors = vector<int> (2 * nexternal, 0); 
  user_helicities = vector<int> (nexternal, 0); 
  p = vector < double * > (); 
  for (unsigned int i = 0; i < nexternal; i++ )
  {
    p.push_back(new double[4]); 
  }
  initColorConfigs(); 
  // Synchronize local variables dependent on the model with the active model.
  mME = vector<double> (nexternal, 0.); 
  syncProcModelParams(); 
  jamp2 = vector < vec_double > (19); 
  jamp2[0] = vector<double> (6, 0.); 
  jamp2[1] = vector<double> (6, 0.); 
  jamp2[2] = vector<double> (6, 0.); 
  jamp2[3] = vector<double> (6, 0.); 
  jamp2[4] = vector<double> (6, 0.); 
  jamp2[5] = vector<double> (6, 0.); 
  jamp2[6] = vector<double> (6, 0.); 
  jamp2[7] = vector<double> (6, 0.); 
  jamp2[8] = vector<double> (6, 0.); 
  jamp2[9] = vector<double> (6, 0.); 
  jamp2[10] = vector<double> (6, 0.); 
  jamp2[11] = vector<double> (6, 0.); 
  jamp2[12] = vector<double> (6, 0.); 
  jamp2[13] = vector<double> (6, 0.); 
  jamp2[14] = vector<double> (6, 0.); 
  jamp2[15] = vector<double> (6, 0.); 
  jamp2[16] = vector<double> (6, 0.); 
  jamp2[17] = vector<double> (6, 0.); 
  jamp2[18] = vector<double> (6, 0.); 
  all_results = vector < vec_vec_double > (19); 
  // The first entry is always the color or helicity avg/summed matrix element.
  all_results[0] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[1] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[2] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[3] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[4] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[5] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[6] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[7] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[8] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[9] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[10] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[11] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[12] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[13] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[14] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[15] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[16] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[17] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[18] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
}

// Synchronize local variables of the process that depend on the model
// parameters
void PY8MEs_R19_P130_sm_qq_qqqq::syncProcModelParams() 
{

  // Instantiate the model class and set parameters that stay fixed during run
  mME[0] = pars->ZERO; 
  mME[1] = pars->ZERO; 
  mME[2] = pars->ZERO; 
  mME[3] = pars->ZERO; 
  mME[4] = pars->ZERO; 
  mME[5] = pars->ZERO; 
}

//--------------------------------------------------------------------------
// Setter allowing to force particular values for the external masses
void PY8MEs_R19_P130_sm_qq_qqqq::setMasses(vec_double external_masses) 
{

  if (external_masses.size() != mME.size())
  {
    cerr <<  "Error in function 'setMasses' of class" << 
    " 'PY8MEs_R19_P130_sm_qq_qqqq': Incorrect number of" << 
    " masses specified." << endl; 
    throw PY8MEs_R19_P130_sm_qq_qqqq_exception; 
  }
  for (unsigned int j = 0; j < mME.size(); j++ )
  {
    mME[j] = external_masses[perm[j]]; 
  }
}

//--------------------------------------------------------------------------
// Getter accessing external masses with the correct ordering
vector<double> PY8MEs_R19_P130_sm_qq_qqqq::getMasses() 
{

  vec_double external_masses; 
  vector<int> invertedPerm; 
  invertedPerm = invert_mapping(perm); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    external_masses.push_back(mME[invertedPerm[i]]); 
  }
  return external_masses; 

}


// Set all values of the external masses to float(-mode) where mode can be
// 0 : Mass taken from the model
// 1 : Mass taken from p_i^2 if not massless to begin with
// 2 : Mass always taken from p_i^2.
void PY8MEs_R19_P130_sm_qq_qqqq::setExternalMassesMode(int mode) 
{
  if (mode != 0 && mode != 1 && mode != 2)
  {
    cerr <<  "Error in function 'setExternalMassesMode' of class" << 
    " 'PY8MEs_R19_P130_sm_qq_qqqq': Incorrect mode selected :" << mode << 
    ". It must be either 0, 1 or 2" << endl; 
    throw PY8MEs_R19_P130_sm_qq_qqqq_exception; 
  }
  if (mode == 0)
  {
    syncProcModelParams(); 
  }
  else if (mode == 1)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      if (mME[j] != pars->ZERO)
      {
        mME[j] = -1.0; 
      }
    }
  }
  else if (mode == 2)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      mME[j] = -1.0; 
    }
  }
}

//--------------------------------------------------------------------------
// Evaluate the squared matrix element.

double PY8MEs_R19_P130_sm_qq_qqqq::sigmaKin() 
{
  // Set the parameters which change event by event
  pars->setDependentParameters(); 
  pars->setDependentCouplings(); 
  // Reset color flows
  for(int i = 0; i < 6; i++ )
    jamp2[0][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[1][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[2][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[3][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[4][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[5][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[6][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[7][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[8][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[9][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[10][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[11][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[12][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[13][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[14][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[15][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[16][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[17][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[18][i] = 0.; 

  // Save previous values of mME
  vector<double> saved_mME(mME.size(), 0.0); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (mME[i] < 0.0)
    {
      saved_mME[i] = mME[i]; 
      mME[i] = sqrt(abs(pow(p[perm[i]][0], 2) - 
      (pow(p[perm[i]][1], 2) + pow(p[perm[i]][2], 2) + pow(p[perm[i]][3],
          2))));
    }
  }

  // Local variables and constants
  const int max_tries = 10; 
  vector < vec_bool > goodhel(nprocesses, vec_bool(ncomb, false)); 
  vec_int ntry(nprocesses, 0); 
  double t = 0.; 
  double result = 0.; 

  if (ntry[proc_ID] <= max_tries)
    ntry[proc_ID] = ntry[proc_ID] + 1; 

  // Find which helicity configuration is asked for
  // -1 indicates one wants to sum over helicities
  int user_ihel = getHelicityIDForConfig(user_helicities); 

  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = getColorIDForConfig(user_colors); 

  // Reset the list of results that will be recomputed here
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      all_results[proc_ID][ihel + 1][icolor + 1] = 0.; 
    }
  }

  // Calculate the matrix element for all helicities
  // unless already detected as vanishing
  for(int ihel = 0; ihel < ncomb; ihel++ )
  {
    // Skip helicity if already detected as vanishing
    if ((ntry[proc_ID] >= max_tries) && !goodhel[proc_ID][ihel])
      continue; 

    // Also skip helicity if user asks for a specific one
    if ((ntry[proc_ID] >= max_tries) && user_ihel != -1 && user_ihel != ihel)
      continue; 

    calculate_wavefunctions(helicities[ihel]); 

    // Reset locally computed color flows
    for(int i = 0; i < 6; i++ )
      jamp2[0][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[1][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[2][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[3][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[4][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[5][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[6][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[7][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[8][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[9][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[10][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[11][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[12][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[13][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[14][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[15][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[16][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[17][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[18][i] = 0.; 

    if (proc_ID == 0)
      t = matrix_19_uu_uuuux(); 
    if (proc_ID == 1)
      t = matrix_19_uux_uuuxux(); 
    if (proc_ID == 2)
      t = matrix_19_uxux_uuxuxux(); 
    if (proc_ID == 3)
      t = matrix_19_uu_uuccx(); 
    if (proc_ID == 4)
      t = matrix_19_uc_uucux(); 
    if (proc_ID == 5)
      t = matrix_19_uc_ucccx(); 
    if (proc_ID == 6)
      t = matrix_19_uux_ucuxcx(); 
    if (proc_ID == 7)
      t = matrix_19_uux_cccxcx(); 
    if (proc_ID == 8)
      t = matrix_19_ucx_uuuxcx(); 
    if (proc_ID == 9)
      t = matrix_19_ucx_uccxcx(); 
    if (proc_ID == 10)
      t = matrix_19_ccx_ucuxcx(); 
    if (proc_ID == 11)
      t = matrix_19_uxux_cuxuxcx(); 
    if (proc_ID == 12)
      t = matrix_19_uxcx_uuxuxcx(); 
    if (proc_ID == 13)
      t = matrix_19_uxcx_cuxcxcx(); 
    if (proc_ID == 14)
      t = matrix_19_uc_ucddx(); 
    if (proc_ID == 15)
      t = matrix_19_uux_cdcxdx(); 
    if (proc_ID == 16)
      t = matrix_19_ucx_udcxdx(); 
    if (proc_ID == 17)
      t = matrix_19_ddx_ucuxcx(); 
    if (proc_ID == 18)
      t = matrix_19_uxcx_duxcxdx(); 

    // Store which helicities give non-zero result
    if ((ntry[proc_ID] < max_tries) && t != 0. && !goodhel[proc_ID][ihel])
      goodhel[proc_ID][ihel] = true; 

    // Aggregate results
    if (user_ihel == -1 || user_ihel == ihel)
    {
      if (user_icol == -1)
      {
        result = result + t; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][0] += t; 
          for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
          {
            all_results[proc_ID][0][i + 1] += jamp2[proc_ID][i]; 
          }
        }
        all_results[proc_ID][ihel + 1][0] += t; 
        for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
        {
          all_results[proc_ID][ihel + 1][i + 1] += jamp2[proc_ID][i]; 
        }
      }
      else
      {
        result = result + jamp2[proc_ID][user_icol]; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][user_icol + 1] += jamp2[proc_ID][user_icol]; 
        }
        all_results[proc_ID][ihel + 1][user_icol + 1] +=
            jamp2[proc_ID][user_icol];
      }
    }

  }

  // Normalize results with the identical particle factor
  if (include_symmetry_factors)
  {
    result = result/denom_iden[proc_ID]; 
  }
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      if (include_symmetry_factors)
      {
        all_results[proc_ID][ihel + 1][icolor + 1] /= denom_iden[proc_ID]; 
      }
    }
  }


  // Normalize when when summing+averaging over helicity configurations
  if (user_ihel == -1 && include_helicity_averaging_factors)
  {
    result /= denom_hels[proc_ID]; 
    if (user_icol == -1)
    {
      all_results[proc_ID][0][0] /= denom_hels[proc_ID]; 
      for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
      {
        all_results[proc_ID][0][i + 1] /= denom_hels[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][0][user_icol + 1] /= denom_hels[proc_ID]; 
    }
  }

  // Normalize when summing+averaging over color configurations
  if (user_icol == -1 && include_color_averaging_factors)
  {
    result /= denom_colors[proc_ID]; 
    if (user_ihel == -1)
    {
      all_results[proc_ID][0][0] /= denom_colors[proc_ID]; 
      for (unsigned int i = 0; i < ncomb; i++ )
      {
        all_results[proc_ID][i + 1][0] /= denom_colors[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][user_ihel + 1][0] /= denom_colors[proc_ID]; 
    }
  }

  // Reinstate previous values of mME
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (saved_mME[i] < 0.0)
    {
      mME[i] = saved_mME[i]; 
    }
  }

  // Finally return it
  return result; 
}

//==========================================================================
// Private class member functions

//--------------------------------------------------------------------------
// Evaluate |M|^2 for each subprocess

void PY8MEs_R19_P130_sm_qq_qqqq::calculate_wavefunctions(const int hel[])
{
  // Calculate wavefunctions for all processes
  // Calculate all wavefunctions
  ixxxxx(p[perm[0]], mME[0], hel[0], +1, w[0]); 
  ixxxxx(p[perm[1]], mME[1], hel[1], +1, w[1]); 
  oxxxxx(p[perm[2]], mME[2], hel[2], +1, w[2]); 
  oxxxxx(p[perm[3]], mME[3], hel[3], +1, w[3]); 
  oxxxxx(p[perm[4]], mME[4], hel[4], +1, w[4]); 
  ixxxxx(p[perm[5]], mME[5], hel[5], -1, w[5]); 
  FFV1P0_3(w[0], w[2], pars->GC_11, pars->ZERO, pars->ZERO, w[6]); 
  FFV1P0_3(w[1], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[7]); 
  FFV1_1(w[4], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[8]); 
  FFV1_2(w[5], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[9]); 
  FFV1P0_3(w[5], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[10]); 
  FFV1P0_3(w[1], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[11]); 
  FFV1_1(w[3], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[12]); 
  FFV1P0_3(w[5], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[13]); 
  FFV1_2(w[1], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[14]); 
  FFV1P0_3(w[0], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[15]); 
  FFV1P0_3(w[1], w[2], pars->GC_11, pars->ZERO, pars->ZERO, w[16]); 
  FFV1_1(w[4], w[15], pars->GC_11, pars->ZERO, pars->ZERO, w[17]); 
  FFV1_2(w[5], w[15], pars->GC_11, pars->ZERO, pars->ZERO, w[18]); 
  FFV1_1(w[2], w[15], pars->GC_11, pars->ZERO, pars->ZERO, w[19]); 
  FFV1P0_3(w[5], w[2], pars->GC_11, pars->ZERO, pars->ZERO, w[20]); 
  FFV1_2(w[1], w[15], pars->GC_11, pars->ZERO, pars->ZERO, w[21]); 
  FFV1P0_3(w[0], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[22]); 
  FFV1_1(w[3], w[22], pars->GC_11, pars->ZERO, pars->ZERO, w[23]); 
  FFV1_2(w[5], w[22], pars->GC_11, pars->ZERO, pars->ZERO, w[24]); 
  FFV1_1(w[2], w[22], pars->GC_11, pars->ZERO, pars->ZERO, w[25]); 
  FFV1_2(w[1], w[22], pars->GC_11, pars->ZERO, pars->ZERO, w[26]); 
  FFV1_2(w[0], w[16], pars->GC_11, pars->ZERO, pars->ZERO, w[27]); 
  FFV1_2(w[0], w[13], pars->GC_11, pars->ZERO, pars->ZERO, w[28]); 
  FFV1_2(w[0], w[10], pars->GC_11, pars->ZERO, pars->ZERO, w[29]); 
  FFV1_2(w[0], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[30]); 
  FFV1_2(w[0], w[20], pars->GC_11, pars->ZERO, pars->ZERO, w[31]); 
  FFV1_2(w[0], w[11], pars->GC_11, pars->ZERO, pars->ZERO, w[32]); 
  oxxxxx(p[perm[1]], mME[1], hel[1], -1, w[33]); 
  ixxxxx(p[perm[4]], mME[4], hel[4], -1, w[34]); 
  FFV1P0_3(w[0], w[33], pars->GC_11, pars->ZERO, pars->ZERO, w[35]); 
  FFV1P0_3(w[34], w[2], pars->GC_11, pars->ZERO, pars->ZERO, w[36]); 
  FFV1_1(w[3], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[37]); 
  FFV1_2(w[5], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[38]); 
  FFV1P0_3(w[34], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[39]); 
  FFV1_1(w[2], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[40]); 
  FFV1_2(w[34], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[41]); 
  FFV1P0_3(w[34], w[33], pars->GC_11, pars->ZERO, pars->ZERO, w[42]); 
  FFV1_1(w[33], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[43]); 
  FFV1P0_3(w[5], w[33], pars->GC_11, pars->ZERO, pars->ZERO, w[44]); 
  FFV1_2(w[34], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[45]); 
  FFV1_1(w[33], w[15], pars->GC_11, pars->ZERO, pars->ZERO, w[46]); 
  FFV1_2(w[34], w[15], pars->GC_11, pars->ZERO, pars->ZERO, w[47]); 
  FFV1_2(w[0], w[42], pars->GC_11, pars->ZERO, pars->ZERO, w[48]); 
  FFV1_2(w[0], w[36], pars->GC_11, pars->ZERO, pars->ZERO, w[49]); 
  FFV1_2(w[0], w[44], pars->GC_11, pars->ZERO, pars->ZERO, w[50]); 
  FFV1_2(w[0], w[39], pars->GC_11, pars->ZERO, pars->ZERO, w[51]); 
  oxxxxx(p[perm[0]], mME[0], hel[0], -1, w[52]); 
  ixxxxx(p[perm[3]], mME[3], hel[3], -1, w[53]); 
  FFV1P0_3(w[53], w[52], pars->GC_11, pars->ZERO, pars->ZERO, w[54]); 
  FFV1_1(w[2], w[54], pars->GC_11, pars->ZERO, pars->ZERO, w[55]); 
  FFV1_2(w[5], w[54], pars->GC_11, pars->ZERO, pars->ZERO, w[56]); 
  FFV1_1(w[33], w[54], pars->GC_11, pars->ZERO, pars->ZERO, w[57]); 
  FFV1_2(w[34], w[54], pars->GC_11, pars->ZERO, pars->ZERO, w[58]); 
  FFV1P0_3(w[53], w[33], pars->GC_11, pars->ZERO, pars->ZERO, w[59]); 
  FFV1P0_3(w[34], w[52], pars->GC_11, pars->ZERO, pars->ZERO, w[60]); 
  FFV1_1(w[2], w[59], pars->GC_11, pars->ZERO, pars->ZERO, w[61]); 
  FFV1_2(w[5], w[59], pars->GC_11, pars->ZERO, pars->ZERO, w[62]); 
  FFV1_1(w[52], w[59], pars->GC_11, pars->ZERO, pars->ZERO, w[63]); 
  FFV1P0_3(w[5], w[52], pars->GC_11, pars->ZERO, pars->ZERO, w[64]); 
  FFV1_2(w[34], w[59], pars->GC_11, pars->ZERO, pars->ZERO, w[65]); 
  FFV1P0_3(w[53], w[2], pars->GC_11, pars->ZERO, pars->ZERO, w[66]); 
  FFV1_1(w[33], w[66], pars->GC_11, pars->ZERO, pars->ZERO, w[67]); 
  FFV1_2(w[5], w[66], pars->GC_11, pars->ZERO, pars->ZERO, w[68]); 
  FFV1_1(w[52], w[66], pars->GC_11, pars->ZERO, pars->ZERO, w[69]); 
  FFV1_2(w[34], w[66], pars->GC_11, pars->ZERO, pars->ZERO, w[70]); 
  FFV1_2(w[53], w[60], pars->GC_11, pars->ZERO, pars->ZERO, w[71]); 
  FFV1_2(w[53], w[44], pars->GC_11, pars->ZERO, pars->ZERO, w[72]); 
  FFV1_2(w[53], w[20], pars->GC_11, pars->ZERO, pars->ZERO, w[73]); 
  FFV1_2(w[53], w[42], pars->GC_11, pars->ZERO, pars->ZERO, w[74]); 
  FFV1_2(w[53], w[64], pars->GC_11, pars->ZERO, pars->ZERO, w[75]); 
  FFV1_2(w[53], w[36], pars->GC_11, pars->ZERO, pars->ZERO, w[76]); 
  FFV1_1(w[3], w[36], pars->GC_11, pars->ZERO, pars->ZERO, w[77]); 
  FFV1_2(w[5], w[36], pars->GC_11, pars->ZERO, pars->ZERO, w[78]); 
  FFV1_1(w[33], w[36], pars->GC_11, pars->ZERO, pars->ZERO, w[79]); 
  FFV1_2(w[34], w[13], pars->GC_11, pars->ZERO, pars->ZERO, w[80]); 
  FFV1_2(w[34], w[44], pars->GC_11, pars->ZERO, pars->ZERO, w[81]); 

  // Calculate all amplitudes
  // Amplitude(s) for diagram number 0
  FFV1_0(w[5], w[8], w[7], pars->GC_11, amp[0]); 
  FFV1_0(w[9], w[4], w[7], pars->GC_11, amp[1]); 
  VVV1_0(w[6], w[7], w[10], pars->GC_10, amp[2]); 
  FFV1_0(w[5], w[12], w[11], pars->GC_11, amp[3]); 
  FFV1_0(w[9], w[3], w[11], pars->GC_11, amp[4]); 
  VVV1_0(w[6], w[11], w[13], pars->GC_10, amp[5]); 
  FFV1_0(w[14], w[4], w[13], pars->GC_11, amp[6]); 
  FFV1_0(w[1], w[8], w[13], pars->GC_11, amp[7]); 
  FFV1_0(w[14], w[3], w[10], pars->GC_11, amp[8]); 
  FFV1_0(w[1], w[12], w[10], pars->GC_11, amp[9]); 
  FFV1_0(w[5], w[17], w[16], pars->GC_11, amp[10]); 
  FFV1_0(w[18], w[4], w[16], pars->GC_11, amp[11]); 
  VVV1_0(w[15], w[16], w[10], pars->GC_10, amp[12]); 
  FFV1_0(w[5], w[19], w[11], pars->GC_11, amp[13]); 
  FFV1_0(w[18], w[2], w[11], pars->GC_11, amp[14]); 
  VVV1_0(w[15], w[11], w[20], pars->GC_10, amp[15]); 
  FFV1_0(w[21], w[4], w[20], pars->GC_11, amp[16]); 
  FFV1_0(w[1], w[17], w[20], pars->GC_11, amp[17]); 
  FFV1_0(w[21], w[2], w[10], pars->GC_11, amp[18]); 
  FFV1_0(w[1], w[19], w[10], pars->GC_11, amp[19]); 
  FFV1_0(w[5], w[23], w[16], pars->GC_11, amp[20]); 
  FFV1_0(w[24], w[3], w[16], pars->GC_11, amp[21]); 
  VVV1_0(w[22], w[16], w[13], pars->GC_10, amp[22]); 
  FFV1_0(w[5], w[25], w[7], pars->GC_11, amp[23]); 
  FFV1_0(w[24], w[2], w[7], pars->GC_11, amp[24]); 
  VVV1_0(w[22], w[7], w[20], pars->GC_10, amp[25]); 
  FFV1_0(w[26], w[3], w[20], pars->GC_11, amp[26]); 
  FFV1_0(w[1], w[23], w[20], pars->GC_11, amp[27]); 
  FFV1_0(w[26], w[2], w[13], pars->GC_11, amp[28]); 
  FFV1_0(w[1], w[25], w[13], pars->GC_11, amp[29]); 
  FFV1_0(w[27], w[4], w[13], pars->GC_11, amp[30]); 
  FFV1_0(w[28], w[4], w[16], pars->GC_11, amp[31]); 
  FFV1_0(w[27], w[3], w[10], pars->GC_11, amp[32]); 
  FFV1_0(w[29], w[3], w[16], pars->GC_11, amp[33]); 
  FFV1_0(w[30], w[4], w[20], pars->GC_11, amp[34]); 
  FFV1_0(w[31], w[4], w[7], pars->GC_11, amp[35]); 
  FFV1_0(w[30], w[2], w[10], pars->GC_11, amp[36]); 
  FFV1_0(w[29], w[2], w[7], pars->GC_11, amp[37]); 
  FFV1_0(w[32], w[3], w[20], pars->GC_11, amp[38]); 
  FFV1_0(w[31], w[3], w[11], pars->GC_11, amp[39]); 
  FFV1_0(w[32], w[2], w[13], pars->GC_11, amp[40]); 
  FFV1_0(w[28], w[2], w[11], pars->GC_11, amp[41]); 
  FFV1_0(w[5], w[37], w[36], pars->GC_11, amp[42]); 
  FFV1_0(w[38], w[3], w[36], pars->GC_11, amp[43]); 
  VVV1_0(w[35], w[36], w[13], pars->GC_10, amp[44]); 
  FFV1_0(w[5], w[40], w[39], pars->GC_11, amp[45]); 
  FFV1_0(w[38], w[2], w[39], pars->GC_11, amp[46]); 
  VVV1_0(w[35], w[39], w[20], pars->GC_10, amp[47]); 
  FFV1_0(w[41], w[3], w[20], pars->GC_11, amp[48]); 
  FFV1_0(w[34], w[37], w[20], pars->GC_11, amp[49]); 
  FFV1_0(w[41], w[2], w[13], pars->GC_11, amp[50]); 
  FFV1_0(w[34], w[40], w[13], pars->GC_11, amp[51]); 
  FFV1_0(w[5], w[12], w[42], pars->GC_11, amp[52]); 
  FFV1_0(w[9], w[3], w[42], pars->GC_11, amp[53]); 
  VVV1_0(w[6], w[42], w[13], pars->GC_10, amp[54]); 
  FFV1_0(w[5], w[43], w[39], pars->GC_11, amp[55]); 
  FFV1_0(w[9], w[33], w[39], pars->GC_11, amp[56]); 
  VVV1_0(w[6], w[39], w[44], pars->GC_10, amp[57]); 
  FFV1_0(w[45], w[3], w[44], pars->GC_11, amp[58]); 
  FFV1_0(w[34], w[12], w[44], pars->GC_11, amp[59]); 
  FFV1_0(w[45], w[33], w[13], pars->GC_11, amp[60]); 
  FFV1_0(w[34], w[43], w[13], pars->GC_11, amp[61]); 
  FFV1_0(w[5], w[19], w[42], pars->GC_11, amp[62]); 
  FFV1_0(w[18], w[2], w[42], pars->GC_11, amp[63]); 
  VVV1_0(w[15], w[42], w[20], pars->GC_10, amp[64]); 
  FFV1_0(w[5], w[46], w[36], pars->GC_11, amp[65]); 
  FFV1_0(w[18], w[33], w[36], pars->GC_11, amp[66]); 
  VVV1_0(w[15], w[36], w[44], pars->GC_10, amp[67]); 
  FFV1_0(w[47], w[2], w[44], pars->GC_11, amp[68]); 
  FFV1_0(w[34], w[19], w[44], pars->GC_11, amp[69]); 
  FFV1_0(w[47], w[33], w[20], pars->GC_11, amp[70]); 
  FFV1_0(w[34], w[46], w[20], pars->GC_11, amp[71]); 
  FFV1_0(w[48], w[3], w[20], pars->GC_11, amp[72]); 
  FFV1_0(w[31], w[3], w[42], pars->GC_11, amp[73]); 
  FFV1_0(w[48], w[2], w[13], pars->GC_11, amp[74]); 
  FFV1_0(w[28], w[2], w[42], pars->GC_11, amp[75]); 
  FFV1_0(w[49], w[3], w[44], pars->GC_11, amp[76]); 
  FFV1_0(w[50], w[3], w[36], pars->GC_11, amp[77]); 
  FFV1_0(w[49], w[33], w[13], pars->GC_11, amp[78]); 
  FFV1_0(w[28], w[33], w[36], pars->GC_11, amp[79]); 
  FFV1_0(w[51], w[2], w[44], pars->GC_11, amp[80]); 
  FFV1_0(w[50], w[2], w[39], pars->GC_11, amp[81]); 
  FFV1_0(w[51], w[33], w[20], pars->GC_11, amp[82]); 
  FFV1_0(w[31], w[33], w[39], pars->GC_11, amp[83]); 
  FFV1_0(w[5], w[55], w[42], pars->GC_11, amp[84]); 
  FFV1_0(w[56], w[2], w[42], pars->GC_11, amp[85]); 
  VVV1_0(w[54], w[42], w[20], pars->GC_10, amp[86]); 
  FFV1_0(w[5], w[57], w[36], pars->GC_11, amp[87]); 
  FFV1_0(w[56], w[33], w[36], pars->GC_11, amp[88]); 
  VVV1_0(w[54], w[36], w[44], pars->GC_10, amp[89]); 
  FFV1_0(w[58], w[2], w[44], pars->GC_11, amp[90]); 
  FFV1_0(w[34], w[55], w[44], pars->GC_11, amp[91]); 
  FFV1_0(w[58], w[33], w[20], pars->GC_11, amp[92]); 
  FFV1_0(w[34], w[57], w[20], pars->GC_11, amp[93]); 
  FFV1_0(w[5], w[61], w[60], pars->GC_11, amp[94]); 
  FFV1_0(w[62], w[2], w[60], pars->GC_11, amp[95]); 
  VVV1_0(w[59], w[60], w[20], pars->GC_10, amp[96]); 
  FFV1_0(w[5], w[63], w[36], pars->GC_11, amp[97]); 
  FFV1_0(w[62], w[52], w[36], pars->GC_11, amp[98]); 
  VVV1_0(w[59], w[36], w[64], pars->GC_10, amp[99]); 
  FFV1_0(w[65], w[2], w[64], pars->GC_11, amp[100]); 
  FFV1_0(w[34], w[61], w[64], pars->GC_11, amp[101]); 
  FFV1_0(w[65], w[52], w[20], pars->GC_11, amp[102]); 
  FFV1_0(w[34], w[63], w[20], pars->GC_11, amp[103]); 
  FFV1_0(w[5], w[67], w[60], pars->GC_11, amp[104]); 
  FFV1_0(w[68], w[33], w[60], pars->GC_11, amp[105]); 
  VVV1_0(w[66], w[60], w[44], pars->GC_10, amp[106]); 
  FFV1_0(w[5], w[69], w[42], pars->GC_11, amp[107]); 
  FFV1_0(w[68], w[52], w[42], pars->GC_11, amp[108]); 
  VVV1_0(w[66], w[42], w[64], pars->GC_10, amp[109]); 
  FFV1_0(w[70], w[33], w[64], pars->GC_11, amp[110]); 
  FFV1_0(w[34], w[67], w[64], pars->GC_11, amp[111]); 
  FFV1_0(w[70], w[52], w[44], pars->GC_11, amp[112]); 
  FFV1_0(w[34], w[69], w[44], pars->GC_11, amp[113]); 
  FFV1_0(w[71], w[2], w[44], pars->GC_11, amp[114]); 
  FFV1_0(w[72], w[2], w[60], pars->GC_11, amp[115]); 
  FFV1_0(w[71], w[33], w[20], pars->GC_11, amp[116]); 
  FFV1_0(w[73], w[33], w[60], pars->GC_11, amp[117]); 
  FFV1_0(w[74], w[2], w[64], pars->GC_11, amp[118]); 
  FFV1_0(w[75], w[2], w[42], pars->GC_11, amp[119]); 
  FFV1_0(w[74], w[52], w[20], pars->GC_11, amp[120]); 
  FFV1_0(w[73], w[52], w[42], pars->GC_11, amp[121]); 
  FFV1_0(w[76], w[33], w[64], pars->GC_11, amp[122]); 
  FFV1_0(w[75], w[33], w[36], pars->GC_11, amp[123]); 
  FFV1_0(w[76], w[52], w[44], pars->GC_11, amp[124]); 
  FFV1_0(w[72], w[52], w[36], pars->GC_11, amp[125]); 
  FFV1_0(w[14], w[3], w[10], pars->GC_11, amp[126]); 
  FFV1_0(w[1], w[12], w[10], pars->GC_11, amp[127]); 
  FFV1_0(w[5], w[17], w[16], pars->GC_11, amp[128]); 
  FFV1_0(w[18], w[4], w[16], pars->GC_11, amp[129]); 
  VVV1_0(w[15], w[16], w[10], pars->GC_10, amp[130]); 
  FFV1_0(w[21], w[2], w[10], pars->GC_11, amp[131]); 
  FFV1_0(w[1], w[19], w[10], pars->GC_11, amp[132]); 
  FFV1_0(w[27], w[3], w[10], pars->GC_11, amp[133]); 
  FFV1_0(w[29], w[3], w[16], pars->GC_11, amp[134]); 
  FFV1_0(w[30], w[2], w[10], pars->GC_11, amp[135]); 
  FFV1_0(w[29], w[2], w[7], pars->GC_11, amp[136]); 
  FFV1_0(w[1], w[8], w[13], pars->GC_11, amp[137]); 
  FFV1_0(w[14], w[4], w[13], pars->GC_11, amp[138]); 
  VVV1_0(w[6], w[13], w[11], pars->GC_10, amp[139]); 
  FFV1_0(w[9], w[3], w[11], pars->GC_11, amp[140]); 
  FFV1_0(w[5], w[12], w[11], pars->GC_11, amp[141]); 
  FFV1_0(w[1], w[17], w[20], pars->GC_11, amp[142]); 
  FFV1_0(w[21], w[4], w[20], pars->GC_11, amp[143]); 
  VVV1_0(w[15], w[20], w[11], pars->GC_10, amp[144]); 
  FFV1_0(w[18], w[2], w[11], pars->GC_11, amp[145]); 
  FFV1_0(w[5], w[19], w[11], pars->GC_11, amp[146]); 
  FFV1_0(w[31], w[3], w[11], pars->GC_11, amp[147]); 
  FFV1_0(w[32], w[3], w[20], pars->GC_11, amp[148]); 
  FFV1_0(w[28], w[2], w[11], pars->GC_11, amp[149]); 
  FFV1_0(w[32], w[2], w[13], pars->GC_11, amp[150]); 
  FFV1_0(w[30], w[2], w[10], pars->GC_11, amp[151]); 
  FFV1_0(w[29], w[2], w[7], pars->GC_11, amp[152]); 
  FFV1_0(w[32], w[2], w[13], pars->GC_11, amp[153]); 
  FFV1_0(w[28], w[2], w[11], pars->GC_11, amp[154]); 
  FFV1_0(w[5], w[37], w[36], pars->GC_11, amp[155]); 
  FFV1_0(w[38], w[3], w[36], pars->GC_11, amp[156]); 
  VVV1_0(w[35], w[36], w[13], pars->GC_10, amp[157]); 
  FFV1_0(w[41], w[2], w[13], pars->GC_11, amp[158]); 
  FFV1_0(w[34], w[40], w[13], pars->GC_11, amp[159]); 
  FFV1_0(w[5], w[12], w[42], pars->GC_11, amp[160]); 
  FFV1_0(w[9], w[3], w[42], pars->GC_11, amp[161]); 
  VVV1_0(w[6], w[42], w[13], pars->GC_10, amp[162]); 
  FFV1_0(w[45], w[33], w[13], pars->GC_11, amp[163]); 
  FFV1_0(w[34], w[43], w[13], pars->GC_11, amp[164]); 
  FFV1_0(w[48], w[2], w[13], pars->GC_11, amp[165]); 
  FFV1_0(w[28], w[2], w[42], pars->GC_11, amp[166]); 
  FFV1_0(w[49], w[33], w[13], pars->GC_11, amp[167]); 
  FFV1_0(w[28], w[33], w[36], pars->GC_11, amp[168]); 
  FFV1_0(w[5], w[37], w[36], pars->GC_11, amp[169]); 
  FFV1_0(w[38], w[3], w[36], pars->GC_11, amp[170]); 
  VVV1_0(w[35], w[36], w[13], pars->GC_10, amp[171]); 
  FFV1_0(w[5], w[40], w[39], pars->GC_11, amp[172]); 
  FFV1_0(w[38], w[2], w[39], pars->GC_11, amp[173]); 
  VVV1_0(w[35], w[39], w[20], pars->GC_10, amp[174]); 
  FFV1_0(w[41], w[3], w[20], pars->GC_11, amp[175]); 
  FFV1_0(w[34], w[37], w[20], pars->GC_11, amp[176]); 
  FFV1_0(w[41], w[2], w[13], pars->GC_11, amp[177]); 
  FFV1_0(w[34], w[40], w[13], pars->GC_11, amp[178]); 
  FFV1_0(w[49], w[33], w[13], pars->GC_11, amp[179]); 
  FFV1_0(w[28], w[33], w[36], pars->GC_11, amp[180]); 
  FFV1_0(w[51], w[33], w[20], pars->GC_11, amp[181]); 
  FFV1_0(w[31], w[33], w[39], pars->GC_11, amp[182]); 
  FFV1_0(w[5], w[43], w[39], pars->GC_11, amp[183]); 
  FFV1_0(w[9], w[33], w[39], pars->GC_11, amp[184]); 
  VVV1_0(w[6], w[39], w[44], pars->GC_10, amp[185]); 
  FFV1_0(w[45], w[3], w[44], pars->GC_11, amp[186]); 
  FFV1_0(w[34], w[12], w[44], pars->GC_11, amp[187]); 
  FFV1_0(w[5], w[46], w[36], pars->GC_11, amp[188]); 
  FFV1_0(w[18], w[33], w[36], pars->GC_11, amp[189]); 
  VVV1_0(w[15], w[36], w[44], pars->GC_10, amp[190]); 
  FFV1_0(w[47], w[2], w[44], pars->GC_11, amp[191]); 
  FFV1_0(w[34], w[19], w[44], pars->GC_11, amp[192]); 
  FFV1_0(w[49], w[3], w[44], pars->GC_11, amp[193]); 
  FFV1_0(w[50], w[3], w[36], pars->GC_11, amp[194]); 
  FFV1_0(w[51], w[2], w[44], pars->GC_11, amp[195]); 
  FFV1_0(w[50], w[2], w[39], pars->GC_11, amp[196]); 
  FFV1_0(w[5], w[12], w[42], pars->GC_11, amp[197]); 
  FFV1_0(w[9], w[3], w[42], pars->GC_11, amp[198]); 
  VVV1_0(w[6], w[42], w[13], pars->GC_10, amp[199]); 
  FFV1_0(w[5], w[43], w[39], pars->GC_11, amp[200]); 
  FFV1_0(w[9], w[33], w[39], pars->GC_11, amp[201]); 
  VVV1_0(w[6], w[39], w[44], pars->GC_10, amp[202]); 
  FFV1_0(w[45], w[3], w[44], pars->GC_11, amp[203]); 
  FFV1_0(w[34], w[12], w[44], pars->GC_11, amp[204]); 
  FFV1_0(w[45], w[33], w[13], pars->GC_11, amp[205]); 
  FFV1_0(w[34], w[43], w[13], pars->GC_11, amp[206]); 
  FFV1_0(w[48], w[2], w[13], pars->GC_11, amp[207]); 
  FFV1_0(w[28], w[2], w[42], pars->GC_11, amp[208]); 
  FFV1_0(w[51], w[2], w[44], pars->GC_11, amp[209]); 
  FFV1_0(w[50], w[2], w[39], pars->GC_11, amp[210]); 
  FFV1_0(w[5], w[77], w[35], pars->GC_11, amp[211]); 
  FFV1_0(w[78], w[3], w[35], pars->GC_11, amp[212]); 
  VVV1_0(w[36], w[35], w[13], pars->GC_10, amp[213]); 
  FFV1_0(w[5], w[79], w[15], pars->GC_11, amp[214]); 
  FFV1_0(w[78], w[33], w[15], pars->GC_11, amp[215]); 
  VVV1_0(w[36], w[15], w[44], pars->GC_10, amp[216]); 
  FFV1_0(w[49], w[3], w[44], pars->GC_11, amp[217]); 
  FFV1_0(w[0], w[77], w[44], pars->GC_11, amp[218]); 
  FFV1_0(w[49], w[33], w[13], pars->GC_11, amp[219]); 
  FFV1_0(w[0], w[79], w[13], pars->GC_11, amp[220]); 
  FFV1_0(w[41], w[2], w[13], pars->GC_11, amp[221]); 
  FFV1_0(w[80], w[2], w[35], pars->GC_11, amp[222]); 
  FFV1_0(w[47], w[2], w[44], pars->GC_11, amp[223]); 
  FFV1_0(w[81], w[2], w[15], pars->GC_11, amp[224]); 
  FFV1_0(w[5], w[55], w[42], pars->GC_11, amp[225]); 
  FFV1_0(w[56], w[2], w[42], pars->GC_11, amp[226]); 
  VVV1_0(w[54], w[42], w[20], pars->GC_10, amp[227]); 
  FFV1_0(w[58], w[33], w[20], pars->GC_11, amp[228]); 
  FFV1_0(w[34], w[57], w[20], pars->GC_11, amp[229]); 
  FFV1_0(w[5], w[61], w[60], pars->GC_11, amp[230]); 
  FFV1_0(w[62], w[2], w[60], pars->GC_11, amp[231]); 
  VVV1_0(w[59], w[60], w[20], pars->GC_10, amp[232]); 
  FFV1_0(w[65], w[52], w[20], pars->GC_11, amp[233]); 
  FFV1_0(w[34], w[63], w[20], pars->GC_11, amp[234]); 
  FFV1_0(w[71], w[33], w[20], pars->GC_11, amp[235]); 
  FFV1_0(w[73], w[33], w[60], pars->GC_11, amp[236]); 
  FFV1_0(w[74], w[52], w[20], pars->GC_11, amp[237]); 
  FFV1_0(w[73], w[52], w[42], pars->GC_11, amp[238]); 
  FFV1_0(w[5], w[57], w[36], pars->GC_11, amp[239]); 
  FFV1_0(w[56], w[33], w[36], pars->GC_11, amp[240]); 
  VVV1_0(w[54], w[36], w[44], pars->GC_10, amp[241]); 
  FFV1_0(w[58], w[2], w[44], pars->GC_11, amp[242]); 
  FFV1_0(w[34], w[55], w[44], pars->GC_11, amp[243]); 
  FFV1_0(w[5], w[67], w[60], pars->GC_11, amp[244]); 
  FFV1_0(w[68], w[33], w[60], pars->GC_11, amp[245]); 
  VVV1_0(w[66], w[60], w[44], pars->GC_10, amp[246]); 
  FFV1_0(w[70], w[52], w[44], pars->GC_11, amp[247]); 
  FFV1_0(w[34], w[69], w[44], pars->GC_11, amp[248]); 
  FFV1_0(w[71], w[2], w[44], pars->GC_11, amp[249]); 
  FFV1_0(w[72], w[2], w[60], pars->GC_11, amp[250]); 
  FFV1_0(w[76], w[52], w[44], pars->GC_11, amp[251]); 
  FFV1_0(w[72], w[52], w[36], pars->GC_11, amp[252]); 
  FFV1_0(w[5], w[55], w[42], pars->GC_11, amp[253]); 
  FFV1_0(w[56], w[2], w[42], pars->GC_11, amp[254]); 
  VVV1_0(w[54], w[42], w[20], pars->GC_10, amp[255]); 
  FFV1_0(w[5], w[57], w[36], pars->GC_11, amp[256]); 
  FFV1_0(w[56], w[33], w[36], pars->GC_11, amp[257]); 
  VVV1_0(w[54], w[36], w[44], pars->GC_10, amp[258]); 
  FFV1_0(w[58], w[2], w[44], pars->GC_11, amp[259]); 
  FFV1_0(w[34], w[55], w[44], pars->GC_11, amp[260]); 
  FFV1_0(w[58], w[33], w[20], pars->GC_11, amp[261]); 
  FFV1_0(w[34], w[57], w[20], pars->GC_11, amp[262]); 
  FFV1_0(w[74], w[52], w[20], pars->GC_11, amp[263]); 
  FFV1_0(w[73], w[52], w[42], pars->GC_11, amp[264]); 
  FFV1_0(w[76], w[52], w[44], pars->GC_11, amp[265]); 
  FFV1_0(w[72], w[52], w[36], pars->GC_11, amp[266]); 
  FFV1_0(w[14], w[3], w[10], pars->GC_11, amp[267]); 
  FFV1_0(w[1], w[12], w[10], pars->GC_11, amp[268]); 
  FFV1_0(w[30], w[2], w[10], pars->GC_11, amp[269]); 
  FFV1_0(w[29], w[2], w[7], pars->GC_11, amp[270]); 
  FFV1_0(w[5], w[37], w[36], pars->GC_11, amp[271]); 
  FFV1_0(w[38], w[3], w[36], pars->GC_11, amp[272]); 
  VVV1_0(w[35], w[36], w[13], pars->GC_10, amp[273]); 
  FFV1_0(w[41], w[2], w[13], pars->GC_11, amp[274]); 
  FFV1_0(w[34], w[40], w[13], pars->GC_11, amp[275]); 
  FFV1_0(w[49], w[33], w[13], pars->GC_11, amp[276]); 
  FFV1_0(w[28], w[33], w[36], pars->GC_11, amp[277]); 
  FFV1_0(w[5], w[12], w[42], pars->GC_11, amp[278]); 
  FFV1_0(w[9], w[3], w[42], pars->GC_11, amp[279]); 
  VVV1_0(w[6], w[42], w[13], pars->GC_10, amp[280]); 
  FFV1_0(w[45], w[33], w[13], pars->GC_11, amp[281]); 
  FFV1_0(w[34], w[43], w[13], pars->GC_11, amp[282]); 
  FFV1_0(w[48], w[2], w[13], pars->GC_11, amp[283]); 
  FFV1_0(w[28], w[2], w[42], pars->GC_11, amp[284]); 
  FFV1_0(w[0], w[79], w[13], pars->GC_11, amp[285]); 
  FFV1_0(w[49], w[33], w[13], pars->GC_11, amp[286]); 
  VVV1_0(w[36], w[13], w[35], pars->GC_10, amp[287]); 
  FFV1_0(w[78], w[3], w[35], pars->GC_11, amp[288]); 
  FFV1_0(w[5], w[77], w[35], pars->GC_11, amp[289]); 
  FFV1_0(w[80], w[2], w[35], pars->GC_11, amp[290]); 
  FFV1_0(w[41], w[2], w[13], pars->GC_11, amp[291]); 
  FFV1_0(w[5], w[55], w[42], pars->GC_11, amp[292]); 
  FFV1_0(w[56], w[2], w[42], pars->GC_11, amp[293]); 
  VVV1_0(w[54], w[42], w[20], pars->GC_10, amp[294]); 
  FFV1_0(w[58], w[33], w[20], pars->GC_11, amp[295]); 
  FFV1_0(w[34], w[57], w[20], pars->GC_11, amp[296]); 
  FFV1_0(w[74], w[52], w[20], pars->GC_11, amp[297]); 
  FFV1_0(w[73], w[52], w[42], pars->GC_11, amp[298]); 


}
double PY8MEs_R19_P130_sm_qq_qqqq::matrix_19_uu_uuuux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 42;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{27, 9, 9, 3, 3, 9}, {9, 27, 3, 9,
      9, 3}, {9, 3, 27, 9, 9, 3}, {3, 9, 9, 27, 3, 9}, {3, 9, 9, 3, 27, 9}, {9,
      3, 3, 9, 9, 27}};

  // Calculate color flows
  jamp[0] = +1./4. * (+1./9. * amp[0] + 1./9. * amp[1] + 1./3. * amp[3] + 1./3.
      * amp[4] + 1./3. * amp[6] + 1./3. * amp[7] + 1./9. * amp[8] + 1./9. *
      amp[9] + amp[13] - Complex<double> (0, 1) * amp[15] + amp[16] + 1./3. *
      amp[18] + 1./3. * amp[19] + amp[21] + Complex<double> (0, 1) * amp[22] +
      1./3. * amp[23] + 1./3. * amp[24] + amp[29] + amp[30] + 1./3. * amp[32] +
      1./3. * amp[33] + 1./3. * amp[34] + 1./3. * amp[35] + 1./9. * amp[36] +
      1./9. * amp[37] + amp[39]);
  jamp[1] = +1./4. * (-1./3. * amp[0] - 1./3. * amp[1] - 1./9. * amp[3] - 1./9.
      * amp[4] - 1./9. * amp[6] - 1./9. * amp[7] - 1./3. * amp[8] - 1./3. *
      amp[9] - amp[11] - Complex<double> (0, 1) * amp[12] - 1./3. * amp[13] -
      1./3. * amp[14] - amp[19] - amp[23] + Complex<double> (0, 1) * amp[25] -
      amp[26] - 1./3. * amp[28] - 1./3. * amp[29] - 1./3. * amp[30] - 1./3. *
      amp[31] - amp[32] - amp[35] - 1./3. * amp[38] - 1./3. * amp[39] - 1./9. *
      amp[40] - 1./9. * amp[41]);
  jamp[2] = +1./4. * (-amp[3] + Complex<double> (0, 1) * amp[5] - amp[6] -
      1./3. * amp[8] - 1./3. * amp[9] - 1./9. * amp[10] - 1./9. * amp[11] -
      1./3. * amp[13] - 1./3. * amp[14] - 1./3. * amp[16] - 1./3. * amp[17] -
      1./9. * amp[18] - 1./9. * amp[19] - 1./3. * amp[20] - 1./3. * amp[21] -
      amp[24] - Complex<double> (0, 1) * amp[25] - amp[27] - 1./3. * amp[30] -
      1./3. * amp[31] - 1./9. * amp[32] - 1./9. * amp[33] - amp[34] - 1./3. *
      amp[36] - 1./3. * amp[37] - amp[41]);
  jamp[3] = +1./4. * (+amp[0] - Complex<double> (0, 1) * amp[2] + 1./3. *
      amp[6] + 1./3. * amp[7] + amp[8] + 1./3. * amp[10] + 1./3. * amp[11] +
      amp[14] + Complex<double> (0, 1) * amp[15] + amp[17] + 1./9. * amp[20] +
      1./9. * amp[21] + 1./3. * amp[23] + 1./3. * amp[24] + 1./3. * amp[26] +
      1./3. * amp[27] + 1./9. * amp[28] + 1./9. * amp[29] + 1./9. * amp[30] +
      1./9. * amp[31] + 1./3. * amp[32] + 1./3. * amp[33] + amp[37] + amp[38] +
      1./3. * amp[40] + 1./3. * amp[41]);
  jamp[4] = +1./4. * (+amp[1] + Complex<double> (0, 1) * amp[2] + 1./3. *
      amp[3] + 1./3. * amp[4] + amp[9] + 1./3. * amp[10] + 1./3. * amp[11] +
      1./9. * amp[13] + 1./9. * amp[14] + 1./9. * amp[16] + 1./9. * amp[17] +
      1./3. * amp[18] + 1./3. * amp[19] + amp[20] - Complex<double> (0, 1) *
      amp[22] + 1./3. * amp[26] + 1./3. * amp[27] + amp[28] + amp[31] + 1./3. *
      amp[34] + 1./3. * amp[35] + amp[36] + 1./9. * amp[38] + 1./9. * amp[39] +
      1./3. * amp[40] + 1./3. * amp[41]);
  jamp[5] = +1./4. * (-1./3. * amp[0] - 1./3. * amp[1] - amp[4] -
      Complex<double> (0, 1) * amp[5] - amp[7] - amp[10] + Complex<double> (0,
      1) * amp[12] - 1./3. * amp[16] - 1./3. * amp[17] - amp[18] - 1./3. *
      amp[20] - 1./3. * amp[21] - 1./9. * amp[23] - 1./9. * amp[24] - 1./9. *
      amp[26] - 1./9. * amp[27] - 1./3. * amp[28] - 1./3. * amp[29] - amp[33] -
      1./9. * amp[34] - 1./9. * amp[35] - 1./3. * amp[36] - 1./3. * amp[37] -
      1./3. * amp[38] - 1./3. * amp[39] - amp[40]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[0][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R19_P130_sm_qq_qqqq::matrix_19_uux_uuuxux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 42;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{27, 9, 9, 3, 3, 9}, {9, 27, 3, 9,
      9, 3}, {9, 3, 27, 9, 9, 3}, {3, 9, 9, 27, 3, 9}, {3, 9, 9, 3, 27, 9}, {9,
      3, 3, 9, 9, 27}};

  // Calculate color flows
  jamp[0] = +1./4. * (-1./9. * amp[42] - 1./9. * amp[43] - 1./3. * amp[45] -
      1./3. * amp[46] - 1./3. * amp[48] - 1./3. * amp[49] - 1./9. * amp[50] -
      1./9. * amp[51] - amp[55] + Complex<double> (0, 1) * amp[57] - amp[58] -
      1./3. * amp[60] - 1./3. * amp[61] - amp[63] - Complex<double> (0, 1) *
      amp[64] - 1./3. * amp[65] - 1./3. * amp[66] - amp[71] - amp[72] - 1./3. *
      amp[74] - 1./3. * amp[75] - 1./3. * amp[76] - 1./3. * amp[77] - 1./9. *
      amp[78] - 1./9. * amp[79] - amp[81]);
  jamp[1] = +1./4. * (+1./3. * amp[42] + 1./3. * amp[43] + 1./9. * amp[45] +
      1./9. * amp[46] + 1./9. * amp[48] + 1./9. * amp[49] + 1./3. * amp[50] +
      1./3. * amp[51] + amp[53] + Complex<double> (0, 1) * amp[54] + 1./3. *
      amp[55] + 1./3. * amp[56] + amp[61] + amp[65] - Complex<double> (0, 1) *
      amp[67] + amp[68] + 1./3. * amp[70] + 1./3. * amp[71] + 1./3. * amp[72] +
      1./3. * amp[73] + amp[74] + amp[77] + 1./3. * amp[80] + 1./3. * amp[81] +
      1./9. * amp[82] + 1./9. * amp[83]);
  jamp[2] = +1./4. * (+amp[45] - Complex<double> (0, 1) * amp[47] + amp[48] +
      1./3. * amp[50] + 1./3. * amp[51] + 1./9. * amp[52] + 1./9. * amp[53] +
      1./3. * amp[55] + 1./3. * amp[56] + 1./3. * amp[58] + 1./3. * amp[59] +
      1./9. * amp[60] + 1./9. * amp[61] + 1./3. * amp[62] + 1./3. * amp[63] +
      amp[66] + Complex<double> (0, 1) * amp[67] + amp[69] + 1./3. * amp[72] +
      1./3. * amp[73] + 1./9. * amp[74] + 1./9. * amp[75] + amp[76] + 1./3. *
      amp[78] + 1./3. * amp[79] + amp[83]);
  jamp[3] = +1./4. * (-amp[42] + Complex<double> (0, 1) * amp[44] - 1./3. *
      amp[48] - 1./3. * amp[49] - amp[50] - 1./3. * amp[52] - 1./3. * amp[53] -
      amp[56] - Complex<double> (0, 1) * amp[57] - amp[59] - 1./9. * amp[62] -
      1./9. * amp[63] - 1./3. * amp[65] - 1./3. * amp[66] - 1./3. * amp[68] -
      1./3. * amp[69] - 1./9. * amp[70] - 1./9. * amp[71] - 1./9. * amp[72] -
      1./9. * amp[73] - 1./3. * amp[74] - 1./3. * amp[75] - amp[79] - amp[80] -
      1./3. * amp[82] - 1./3. * amp[83]);
  jamp[4] = +1./4. * (-amp[43] - Complex<double> (0, 1) * amp[44] - 1./3. *
      amp[45] - 1./3. * amp[46] - amp[51] - 1./3. * amp[52] - 1./3. * amp[53] -
      1./9. * amp[55] - 1./9. * amp[56] - 1./9. * amp[58] - 1./9. * amp[59] -
      1./3. * amp[60] - 1./3. * amp[61] - amp[62] + Complex<double> (0, 1) *
      amp[64] - 1./3. * amp[68] - 1./3. * amp[69] - amp[70] - amp[73] - 1./3. *
      amp[76] - 1./3. * amp[77] - amp[78] - 1./9. * amp[80] - 1./9. * amp[81] -
      1./3. * amp[82] - 1./3. * amp[83]);
  jamp[5] = +1./4. * (+1./3. * amp[42] + 1./3. * amp[43] + amp[46] +
      Complex<double> (0, 1) * amp[47] + amp[49] + amp[52] - Complex<double>
      (0, 1) * amp[54] + 1./3. * amp[58] + 1./3. * amp[59] + amp[60] + 1./3. *
      amp[62] + 1./3. * amp[63] + 1./9. * amp[65] + 1./9. * amp[66] + 1./9. *
      amp[68] + 1./9. * amp[69] + 1./3. * amp[70] + 1./3. * amp[71] + amp[75] +
      1./9. * amp[76] + 1./9. * amp[77] + 1./3. * amp[78] + 1./3. * amp[79] +
      1./3. * amp[80] + 1./3. * amp[81] + amp[82]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[1][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R19_P130_sm_qq_qqqq::matrix_19_uxux_uuxuxux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 42;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{27, 9, 9, 3, 3, 9}, {9, 27, 3, 9,
      9, 3}, {9, 3, 27, 9, 9, 3}, {3, 9, 9, 27, 3, 9}, {3, 9, 9, 3, 27, 9}, {9,
      3, 3, 9, 9, 27}};

  // Calculate color flows
  jamp[0] = +1./4. * (+1./9. * amp[84] + 1./9. * amp[85] + 1./3. * amp[87] +
      1./3. * amp[88] + 1./3. * amp[90] + 1./3. * amp[91] + 1./9. * amp[92] +
      1./9. * amp[93] + amp[97] - Complex<double> (0, 1) * amp[99] + amp[100] +
      1./3. * amp[102] + 1./3. * amp[103] + amp[105] + Complex<double> (0, 1) *
      amp[106] + 1./3. * amp[107] + 1./3. * amp[108] + amp[113] + amp[114] +
      1./3. * amp[116] + 1./3. * amp[117] + 1./3. * amp[118] + 1./3. * amp[119]
      + 1./9. * amp[120] + 1./9. * amp[121] + amp[123]);
  jamp[1] = +1./4. * (-1./3. * amp[84] - 1./3. * amp[85] - 1./9. * amp[87] -
      1./9. * amp[88] - 1./9. * amp[90] - 1./9. * amp[91] - 1./3. * amp[92] -
      1./3. * amp[93] - amp[95] - Complex<double> (0, 1) * amp[96] - 1./3. *
      amp[97] - 1./3. * amp[98] - amp[103] - amp[107] + Complex<double> (0, 1)
      * amp[109] - amp[110] - 1./3. * amp[112] - 1./3. * amp[113] - 1./3. *
      amp[114] - 1./3. * amp[115] - amp[116] - amp[119] - 1./3. * amp[122] -
      1./3. * amp[123] - 1./9. * amp[124] - 1./9. * amp[125]);
  jamp[2] = +1./4. * (-amp[87] + Complex<double> (0, 1) * amp[89] - amp[90] -
      1./3. * amp[92] - 1./3. * amp[93] - 1./9. * amp[94] - 1./9. * amp[95] -
      1./3. * amp[97] - 1./3. * amp[98] - 1./3. * amp[100] - 1./3. * amp[101] -
      1./9. * amp[102] - 1./9. * amp[103] - 1./3. * amp[104] - 1./3. * amp[105]
      - amp[108] - Complex<double> (0, 1) * amp[109] - amp[111] - 1./3. *
      amp[114] - 1./3. * amp[115] - 1./9. * amp[116] - 1./9. * amp[117] -
      amp[118] - 1./3. * amp[120] - 1./3. * amp[121] - amp[125]);
  jamp[3] = +1./4. * (+amp[84] - Complex<double> (0, 1) * amp[86] + 1./3. *
      amp[90] + 1./3. * amp[91] + amp[92] + 1./3. * amp[94] + 1./3. * amp[95] +
      amp[98] + Complex<double> (0, 1) * amp[99] + amp[101] + 1./9. * amp[104]
      + 1./9. * amp[105] + 1./3. * amp[107] + 1./3. * amp[108] + 1./3. *
      amp[110] + 1./3. * amp[111] + 1./9. * amp[112] + 1./9. * amp[113] + 1./9.
      * amp[114] + 1./9. * amp[115] + 1./3. * amp[116] + 1./3. * amp[117] +
      amp[121] + amp[122] + 1./3. * amp[124] + 1./3. * amp[125]);
  jamp[4] = +1./4. * (+amp[85] + Complex<double> (0, 1) * amp[86] + 1./3. *
      amp[87] + 1./3. * amp[88] + amp[93] + 1./3. * amp[94] + 1./3. * amp[95] +
      1./9. * amp[97] + 1./9. * amp[98] + 1./9. * amp[100] + 1./9. * amp[101] +
      1./3. * amp[102] + 1./3. * amp[103] + amp[104] - Complex<double> (0, 1) *
      amp[106] + 1./3. * amp[110] + 1./3. * amp[111] + amp[112] + amp[115] +
      1./3. * amp[118] + 1./3. * amp[119] + amp[120] + 1./9. * amp[122] + 1./9.
      * amp[123] + 1./3. * amp[124] + 1./3. * amp[125]);
  jamp[5] = +1./4. * (-1./3. * amp[84] - 1./3. * amp[85] - amp[88] -
      Complex<double> (0, 1) * amp[89] - amp[91] - amp[94] + Complex<double>
      (0, 1) * amp[96] - 1./3. * amp[100] - 1./3. * amp[101] - amp[102] - 1./3.
      * amp[104] - 1./3. * amp[105] - 1./9. * amp[107] - 1./9. * amp[108] -
      1./9. * amp[110] - 1./9. * amp[111] - 1./3. * amp[112] - 1./3. * amp[113]
      - amp[117] - 1./9. * amp[118] - 1./9. * amp[119] - 1./3. * amp[120] -
      1./3. * amp[121] - 1./3. * amp[122] - 1./3. * amp[123] - amp[124]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[2][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R19_P130_sm_qq_qqqq::matrix_19_uu_uuccx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 14;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{27, 9, 9, 3, 3, 9}, {9, 27, 3, 9,
      9, 3}, {9, 3, 27, 9, 9, 3}, {3, 9, 9, 27, 3, 9}, {3, 9, 9, 3, 27, 9}, {9,
      3, 3, 9, 9, 27}};

  // Calculate color flows
  jamp[0] = +1./4. * (+1./9. * amp[0] + 1./9. * amp[1] + 1./9. * amp[126] +
      1./9. * amp[127] + 1./3. * amp[131] + 1./3. * amp[132] + 1./3. * amp[133]
      + 1./3. * amp[134] + 1./9. * amp[135] + 1./9. * amp[136]);
  jamp[1] = +1./4. * (-1./3. * amp[0] - 1./3. * amp[1] - 1./3. * amp[126] -
      1./3. * amp[127] - amp[129] - Complex<double> (0, 1) * amp[130] -
      amp[132] - amp[133]);
  jamp[2] = +1./4. * (-1./3. * amp[126] - 1./3. * amp[127] - 1./9. * amp[128] -
      1./9. * amp[129] - 1./9. * amp[131] - 1./9. * amp[132] - 1./9. * amp[133]
      - 1./9. * amp[134] - 1./3. * amp[135] - 1./3. * amp[136]);
  jamp[3] = +1./4. * (+amp[0] - Complex<double> (0, 1) * amp[2] + amp[126] +
      1./3. * amp[128] + 1./3. * amp[129] + 1./3. * amp[133] + 1./3. * amp[134]
      + amp[136]);
  jamp[4] = +1./4. * (+amp[1] + Complex<double> (0, 1) * amp[2] + amp[127] +
      1./3. * amp[128] + 1./3. * amp[129] + 1./3. * amp[131] + 1./3. * amp[132]
      + amp[135]);
  jamp[5] = +1./4. * (-1./3. * amp[0] - 1./3. * amp[1] - amp[128] +
      Complex<double> (0, 1) * amp[130] - amp[131] - amp[134] - 1./3. *
      amp[135] - 1./3. * amp[136]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[3][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R19_P130_sm_qq_qqqq::matrix_19_uc_uucux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 14;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{27, 9, 9, 3, 3, 9}, {9, 27, 3, 9,
      9, 3}, {9, 3, 27, 9, 9, 3}, {3, 9, 9, 27, 3, 9}, {3, 9, 9, 3, 27, 9}, {9,
      3, 3, 9, 9, 27}};

  // Calculate color flows
  jamp[0] = +1./4. * (+1./3. * amp[137] + 1./3. * amp[138] + 1./3. * amp[140] +
      1./3. * amp[141] + amp[143] + Complex<double> (0, 1) * amp[144] +
      amp[146] + amp[147]);
  jamp[1] = +1./4. * (-1./9. * amp[137] - 1./9. * amp[138] - 1./9. * amp[140] -
      1./9. * amp[141] - 1./3. * amp[145] - 1./3. * amp[146] - 1./3. * amp[147]
      - 1./3. * amp[148] - 1./9. * amp[149] - 1./9. * amp[150]);
  jamp[2] = +1./4. * (-amp[138] - Complex<double> (0, 1) * amp[139] - amp[141]
      - 1./3. * amp[142] - 1./3. * amp[143] - 1./3. * amp[145] - 1./3. *
      amp[146] - amp[149]);
  jamp[3] = +1./4. * (+1./3. * amp[137] + 1./3. * amp[138] + amp[142] -
      Complex<double> (0, 1) * amp[144] + amp[145] + amp[148] + 1./3. *
      amp[149] + 1./3. * amp[150]);
  jamp[4] = +1./4. * (+1./3. * amp[140] + 1./3. * amp[141] + 1./9. * amp[142] +
      1./9. * amp[143] + 1./9. * amp[145] + 1./9. * amp[146] + 1./9. * amp[147]
      + 1./9. * amp[148] + 1./3. * amp[149] + 1./3. * amp[150]);
  jamp[5] = +1./4. * (-amp[137] + Complex<double> (0, 1) * amp[139] - amp[140]
      - 1./3. * amp[142] - 1./3. * amp[143] - 1./3. * amp[147] - 1./3. *
      amp[148] - amp[150]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[4][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R19_P130_sm_qq_qqqq::matrix_19_uc_ucccx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 14;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{27, 9, 9, 3, 3, 9}, {9, 27, 3, 9,
      9, 3}, {9, 3, 27, 9, 9, 3}, {3, 9, 9, 27, 3, 9}, {3, 9, 9, 3, 27, 9}, {9,
      3, 3, 9, 9, 27}};

  // Calculate color flows
  jamp[0] = +1./4. * (+1./9. * amp[0] + 1./9. * amp[1] + 1./3. * amp[3] + 1./3.
      * amp[4] + 1./3. * amp[6] + 1./3. * amp[7] + 1./9. * amp[8] + 1./9. *
      amp[9] + 1./9. * amp[151] + 1./9. * amp[152]);
  jamp[1] = +1./4. * (-1./3. * amp[0] - 1./3. * amp[1] - 1./9. * amp[3] - 1./9.
      * amp[4] - 1./9. * amp[6] - 1./9. * amp[7] - 1./3. * amp[8] - 1./3. *
      amp[9] - 1./9. * amp[153] - 1./9. * amp[154]);
  jamp[2] = +1./4. * (-amp[3] + Complex<double> (0, 1) * amp[5] - amp[6] -
      1./3. * amp[8] - 1./3. * amp[9] - 1./3. * amp[151] - 1./3. * amp[152] -
      amp[154]);
  jamp[3] = +1./4. * (+amp[0] - Complex<double> (0, 1) * amp[2] + 1./3. *
      amp[6] + 1./3. * amp[7] + amp[8] + amp[152] + 1./3. * amp[153] + 1./3. *
      amp[154]);
  jamp[4] = +1./4. * (+amp[1] + Complex<double> (0, 1) * amp[2] + 1./3. *
      amp[3] + 1./3. * amp[4] + amp[9] + amp[151] + 1./3. * amp[153] + 1./3. *
      amp[154]);
  jamp[5] = +1./4. * (-1./3. * amp[0] - 1./3. * amp[1] - amp[4] -
      Complex<double> (0, 1) * amp[5] - amp[7] - 1./3. * amp[151] - 1./3. *
      amp[152] - amp[153]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[5][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R19_P130_sm_qq_qqqq::matrix_19_uux_ucuxcx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 14;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{27, 9, 9, 3, 3, 9}, {9, 27, 3, 9,
      9, 3}, {9, 3, 27, 9, 9, 3}, {3, 9, 9, 27, 3, 9}, {3, 9, 9, 3, 27, 9}, {9,
      3, 3, 9, 9, 27}};

  // Calculate color flows
  jamp[0] = +1./4. * (-1./9. * amp[155] - 1./9. * amp[156] - 1./9. * amp[158] -
      1./9. * amp[159] - 1./3. * amp[163] - 1./3. * amp[164] - 1./3. * amp[165]
      - 1./3. * amp[166] - 1./9. * amp[167] - 1./9. * amp[168]);
  jamp[1] = +1./4. * (+1./3. * amp[155] + 1./3. * amp[156] + 1./3. * amp[158] +
      1./3. * amp[159] + amp[161] + Complex<double> (0, 1) * amp[162] +
      amp[164] + amp[165]);
  jamp[2] = +1./4. * (+1./3. * amp[158] + 1./3. * amp[159] + 1./9. * amp[160] +
      1./9. * amp[161] + 1./9. * amp[163] + 1./9. * amp[164] + 1./9. * amp[165]
      + 1./9. * amp[166] + 1./3. * amp[167] + 1./3. * amp[168]);
  jamp[3] = +1./4. * (-amp[155] + Complex<double> (0, 1) * amp[157] - amp[158]
      - 1./3. * amp[160] - 1./3. * amp[161] - 1./3. * amp[165] - 1./3. *
      amp[166] - amp[168]);
  jamp[4] = +1./4. * (-amp[156] - Complex<double> (0, 1) * amp[157] - amp[159]
      - 1./3. * amp[160] - 1./3. * amp[161] - 1./3. * amp[163] - 1./3. *
      amp[164] - amp[167]);
  jamp[5] = +1./4. * (+1./3. * amp[155] + 1./3. * amp[156] + amp[160] -
      Complex<double> (0, 1) * amp[162] + amp[163] + amp[166] + 1./3. *
      amp[167] + 1./3. * amp[168]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[6][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R19_P130_sm_qq_qqqq::matrix_19_uux_cccxcx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 14;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{27, 9, 9, 3, 3, 9}, {9, 27, 3, 9,
      9, 3}, {9, 3, 27, 9, 9, 3}, {3, 9, 9, 27, 3, 9}, {3, 9, 9, 3, 27, 9}, {9,
      3, 3, 9, 9, 27}};

  // Calculate color flows
  jamp[0] = +1./4. * (-1./9. * amp[169] - 1./9. * amp[170] - 1./3. * amp[172] -
      1./3. * amp[173] - 1./3. * amp[175] - 1./3. * amp[176] - 1./9. * amp[177]
      - 1./9. * amp[178] - 1./9. * amp[179] - 1./9. * amp[180]);
  jamp[1] = +1./4. * (+1./3. * amp[169] + 1./3. * amp[170] + 1./9. * amp[172] +
      1./9. * amp[173] + 1./9. * amp[175] + 1./9. * amp[176] + 1./3. * amp[177]
      + 1./3. * amp[178] + 1./9. * amp[181] + 1./9. * amp[182]);
  jamp[2] = +1./4. * (+amp[172] - Complex<double> (0, 1) * amp[174] + amp[175]
      + 1./3. * amp[177] + 1./3. * amp[178] + 1./3. * amp[179] + 1./3. *
      amp[180] + amp[182]);
  jamp[3] = +1./4. * (-amp[169] + Complex<double> (0, 1) * amp[171] - 1./3. *
      amp[175] - 1./3. * amp[176] - amp[177] - amp[180] - 1./3. * amp[181] -
      1./3. * amp[182]);
  jamp[4] = +1./4. * (-amp[170] - Complex<double> (0, 1) * amp[171] - 1./3. *
      amp[172] - 1./3. * amp[173] - amp[178] - amp[179] - 1./3. * amp[181] -
      1./3. * amp[182]);
  jamp[5] = +1./4. * (+1./3. * amp[169] + 1./3. * amp[170] + amp[173] +
      Complex<double> (0, 1) * amp[174] + amp[176] + 1./3. * amp[179] + 1./3. *
      amp[180] + amp[181]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[7][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R19_P130_sm_qq_qqqq::matrix_19_ucx_uuuxcx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 14;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{27, 9, 9, 3, 3, 9}, {9, 27, 3, 9,
      9, 3}, {9, 3, 27, 9, 9, 3}, {3, 9, 9, 27, 3, 9}, {3, 9, 9, 3, 27, 9}, {9,
      3, 3, 9, 9, 27}};

  // Calculate color flows
  jamp[0] = +1./4. * (-amp[183] + Complex<double> (0, 1) * amp[185] - amp[186]
      - 1./3. * amp[188] - 1./3. * amp[189] - 1./3. * amp[193] - 1./3. *
      amp[194] - amp[196]);
  jamp[1] = +1./4. * (+1./3. * amp[183] + 1./3. * amp[184] + amp[188] -
      Complex<double> (0, 1) * amp[190] + amp[191] + amp[194] + 1./3. *
      amp[195] + 1./3. * amp[196]);
  jamp[2] = +1./4. * (+1./3. * amp[183] + 1./3. * amp[184] + 1./3. * amp[186] +
      1./3. * amp[187] + amp[189] + Complex<double> (0, 1) * amp[190] +
      amp[192] + amp[193]);
  jamp[3] = +1./4. * (-amp[184] - Complex<double> (0, 1) * amp[185] - amp[187]
      - 1./3. * amp[188] - 1./3. * amp[189] - 1./3. * amp[191] - 1./3. *
      amp[192] - amp[195]);
  jamp[4] = +1./4. * (-1./9. * amp[183] - 1./9. * amp[184] - 1./9. * amp[186] -
      1./9. * amp[187] - 1./3. * amp[191] - 1./3. * amp[192] - 1./3. * amp[193]
      - 1./3. * amp[194] - 1./9. * amp[195] - 1./9. * amp[196]);
  jamp[5] = +1./4. * (+1./3. * amp[186] + 1./3. * amp[187] + 1./9. * amp[188] +
      1./9. * amp[189] + 1./9. * amp[191] + 1./9. * amp[192] + 1./9. * amp[193]
      + 1./9. * amp[194] + 1./3. * amp[195] + 1./3. * amp[196]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[8][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R19_P130_sm_qq_qqqq::matrix_19_ucx_uccxcx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 14;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{27, 9, 9, 3, 3, 9}, {9, 27, 3, 9,
      9, 3}, {9, 3, 27, 9, 9, 3}, {3, 9, 9, 27, 3, 9}, {3, 9, 9, 3, 27, 9}, {9,
      3, 3, 9, 9, 27}};

  // Calculate color flows
  jamp[0] = +1./4. * (-amp[200] + Complex<double> (0, 1) * amp[202] - amp[203]
      - 1./3. * amp[205] - 1./3. * amp[206] - 1./3. * amp[207] - 1./3. *
      amp[208] - amp[210]);
  jamp[1] = +1./4. * (+amp[198] + Complex<double> (0, 1) * amp[199] + 1./3. *
      amp[200] + 1./3. * amp[201] + amp[206] + amp[207] + 1./3. * amp[209] +
      1./3. * amp[210]);
  jamp[2] = +1./4. * (+1./9. * amp[197] + 1./9. * amp[198] + 1./3. * amp[200] +
      1./3. * amp[201] + 1./3. * amp[203] + 1./3. * amp[204] + 1./9. * amp[205]
      + 1./9. * amp[206] + 1./9. * amp[207] + 1./9. * amp[208]);
  jamp[3] = +1./4. * (-1./3. * amp[197] - 1./3. * amp[198] - amp[201] -
      Complex<double> (0, 1) * amp[202] - amp[204] - 1./3. * amp[207] - 1./3. *
      amp[208] - amp[209]);
  jamp[4] = +1./4. * (-1./3. * amp[197] - 1./3. * amp[198] - 1./9. * amp[200] -
      1./9. * amp[201] - 1./9. * amp[203] - 1./9. * amp[204] - 1./3. * amp[205]
      - 1./3. * amp[206] - 1./9. * amp[209] - 1./9. * amp[210]);
  jamp[5] = +1./4. * (+amp[197] - Complex<double> (0, 1) * amp[199] + 1./3. *
      amp[203] + 1./3. * amp[204] + amp[205] + amp[208] + 1./3. * amp[209] +
      1./3. * amp[210]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[9][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R19_P130_sm_qq_qqqq::matrix_19_ccx_ucuxcx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 14;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{27, 9, 9, 3, 3, 9}, {9, 27, 3, 9,
      9, 3}, {9, 3, 27, 9, 9, 3}, {3, 9, 9, 27, 3, 9}, {3, 9, 9, 3, 27, 9}, {9,
      3, 3, 9, 9, 27}};

  // Calculate color flows
  jamp[0] = +1./4. * (-1./9. * amp[211] - 1./9. * amp[212] - 1./3. * amp[214] -
      1./3. * amp[215] - 1./3. * amp[217] - 1./3. * amp[218] - 1./9. * amp[219]
      - 1./9. * amp[220] - 1./9. * amp[221] - 1./9. * amp[222]);
  jamp[1] = +1./4. * (+1./3. * amp[211] + 1./3. * amp[212] + amp[215] +
      Complex<double> (0, 1) * amp[216] + amp[218] + 1./3. * amp[221] + 1./3. *
      amp[222] + amp[223]);
  jamp[2] = +1./4. * (+amp[214] - Complex<double> (0, 1) * amp[216] + amp[217]
      + 1./3. * amp[219] + 1./3. * amp[220] + 1./3. * amp[221] + 1./3. *
      amp[222] + amp[224]);
  jamp[3] = +1./4. * (-amp[212] - Complex<double> (0, 1) * amp[213] - 1./3. *
      amp[214] - 1./3. * amp[215] - amp[220] - amp[221] - 1./3. * amp[223] -
      1./3. * amp[224]);
  jamp[4] = +1./4. * (-amp[211] + Complex<double> (0, 1) * amp[213] - 1./3. *
      amp[217] - 1./3. * amp[218] - amp[219] - amp[222] - 1./3. * amp[223] -
      1./3. * amp[224]);
  jamp[5] = +1./4. * (+1./3. * amp[211] + 1./3. * amp[212] + 1./9. * amp[214] +
      1./9. * amp[215] + 1./9. * amp[217] + 1./9. * amp[218] + 1./3. * amp[219]
      + 1./3. * amp[220] + 1./9. * amp[223] + 1./9. * amp[224]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[10][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R19_P130_sm_qq_qqqq::matrix_19_uxux_cuxuxcx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 14;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{27, 9, 9, 3, 3, 9}, {9, 27, 3, 9,
      9, 3}, {9, 3, 27, 9, 9, 3}, {3, 9, 9, 27, 3, 9}, {3, 9, 9, 3, 27, 9}, {9,
      3, 3, 9, 9, 27}};

  // Calculate color flows
  jamp[0] = +1./4. * (+1./9. * amp[225] + 1./9. * amp[226] + 1./9. * amp[228] +
      1./9. * amp[229] + 1./3. * amp[233] + 1./3. * amp[234] + 1./3. * amp[235]
      + 1./3. * amp[236] + 1./9. * amp[237] + 1./9. * amp[238]);
  jamp[1] = +1./4. * (-1./3. * amp[225] - 1./3. * amp[226] - 1./3. * amp[228] -
      1./3. * amp[229] - amp[231] - Complex<double> (0, 1) * amp[232] -
      amp[234] - amp[235]);
  jamp[2] = +1./4. * (-1./3. * amp[228] - 1./3. * amp[229] - 1./9. * amp[230] -
      1./9. * amp[231] - 1./9. * amp[233] - 1./9. * amp[234] - 1./9. * amp[235]
      - 1./9. * amp[236] - 1./3. * amp[237] - 1./3. * amp[238]);
  jamp[3] = +1./4. * (+amp[225] - Complex<double> (0, 1) * amp[227] + amp[228]
      + 1./3. * amp[230] + 1./3. * amp[231] + 1./3. * amp[235] + 1./3. *
      amp[236] + amp[238]);
  jamp[4] = +1./4. * (+amp[226] + Complex<double> (0, 1) * amp[227] + amp[229]
      + 1./3. * amp[230] + 1./3. * amp[231] + 1./3. * amp[233] + 1./3. *
      amp[234] + amp[237]);
  jamp[5] = +1./4. * (-1./3. * amp[225] - 1./3. * amp[226] - amp[230] +
      Complex<double> (0, 1) * amp[232] - amp[233] - amp[236] - 1./3. *
      amp[237] - 1./3. * amp[238]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[11][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R19_P130_sm_qq_qqqq::matrix_19_uxcx_uuxuxcx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 14;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{27, 9, 9, 3, 3, 9}, {9, 27, 3, 9,
      9, 3}, {9, 3, 27, 9, 9, 3}, {3, 9, 9, 27, 3, 9}, {3, 9, 9, 3, 27, 9}, {9,
      3, 3, 9, 9, 27}};

  // Calculate color flows
  jamp[0] = +1./4. * (+1./3. * amp[239] + 1./3. * amp[240] + 1./3. * amp[242] +
      1./3. * amp[243] + amp[245] + Complex<double> (0, 1) * amp[246] +
      amp[248] + amp[249]);
  jamp[1] = +1./4. * (-1./9. * amp[239] - 1./9. * amp[240] - 1./9. * amp[242] -
      1./9. * amp[243] - 1./3. * amp[247] - 1./3. * amp[248] - 1./3. * amp[249]
      - 1./3. * amp[250] - 1./9. * amp[251] - 1./9. * amp[252]);
  jamp[2] = +1./4. * (-amp[239] + Complex<double> (0, 1) * amp[241] - amp[242]
      - 1./3. * amp[244] - 1./3. * amp[245] - 1./3. * amp[249] - 1./3. *
      amp[250] - amp[252]);
  jamp[3] = +1./4. * (+1./3. * amp[242] + 1./3. * amp[243] + 1./9. * amp[244] +
      1./9. * amp[245] + 1./9. * amp[247] + 1./9. * amp[248] + 1./9. * amp[249]
      + 1./9. * amp[250] + 1./3. * amp[251] + 1./3. * amp[252]);
  jamp[4] = +1./4. * (+1./3. * amp[239] + 1./3. * amp[240] + amp[244] -
      Complex<double> (0, 1) * amp[246] + amp[247] + amp[250] + 1./3. *
      amp[251] + 1./3. * amp[252]);
  jamp[5] = +1./4. * (-amp[240] - Complex<double> (0, 1) * amp[241] - amp[243]
      - 1./3. * amp[244] - 1./3. * amp[245] - 1./3. * amp[247] - 1./3. *
      amp[248] - amp[251]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[12][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R19_P130_sm_qq_qqqq::matrix_19_uxcx_cuxcxcx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 14;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{27, 9, 9, 3, 3, 9}, {9, 27, 3, 9,
      9, 3}, {9, 3, 27, 9, 9, 3}, {3, 9, 9, 27, 3, 9}, {3, 9, 9, 3, 27, 9}, {9,
      3, 3, 9, 9, 27}};

  // Calculate color flows
  jamp[0] = +1./4. * (+1./9. * amp[253] + 1./9. * amp[254] + 1./3. * amp[256] +
      1./3. * amp[257] + 1./3. * amp[259] + 1./3. * amp[260] + 1./9. * amp[261]
      + 1./9. * amp[262] + 1./9. * amp[263] + 1./9. * amp[264]);
  jamp[1] = +1./4. * (-1./3. * amp[253] - 1./3. * amp[254] - 1./9. * amp[256] -
      1./9. * amp[257] - 1./9. * amp[259] - 1./9. * amp[260] - 1./3. * amp[261]
      - 1./3. * amp[262] - 1./9. * amp[265] - 1./9. * amp[266]);
  jamp[2] = +1./4. * (-amp[256] + Complex<double> (0, 1) * amp[258] - amp[259]
      - 1./3. * amp[261] - 1./3. * amp[262] - 1./3. * amp[263] - 1./3. *
      amp[264] - amp[266]);
  jamp[3] = +1./4. * (+amp[253] - Complex<double> (0, 1) * amp[255] + 1./3. *
      amp[259] + 1./3. * amp[260] + amp[261] + amp[264] + 1./3. * amp[265] +
      1./3. * amp[266]);
  jamp[4] = +1./4. * (+amp[254] + Complex<double> (0, 1) * amp[255] + 1./3. *
      amp[256] + 1./3. * amp[257] + amp[262] + amp[263] + 1./3. * amp[265] +
      1./3. * amp[266]);
  jamp[5] = +1./4. * (-1./3. * amp[253] - 1./3. * amp[254] - amp[257] -
      Complex<double> (0, 1) * amp[258] - amp[260] - 1./3. * amp[263] - 1./3. *
      amp[264] - amp[265]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[13][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R19_P130_sm_qq_qqqq::matrix_19_uc_ucddx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 7;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{27, 9, 9, 3, 3, 9}, {9, 27, 3, 9,
      9, 3}, {9, 3, 27, 9, 9, 3}, {3, 9, 9, 27, 3, 9}, {3, 9, 9, 3, 27, 9}, {9,
      3, 3, 9, 9, 27}};

  // Calculate color flows
  jamp[0] = +1./4. * (+1./9. * amp[0] + 1./9. * amp[1] + 1./9. * amp[267] +
      1./9. * amp[268] + 1./9. * amp[269] + 1./9. * amp[270]);
  jamp[1] = +1./4. * (-1./3. * amp[0] - 1./3. * amp[1] - 1./3. * amp[267] -
      1./3. * amp[268]);
  jamp[2] = +1./4. * (-1./3. * amp[267] - 1./3. * amp[268] - 1./3. * amp[269] -
      1./3. * amp[270]);
  jamp[3] = +1./4. * (+amp[0] - Complex<double> (0, 1) * amp[2] + amp[267] +
      amp[270]);
  jamp[4] = +1./4. * (+amp[1] + Complex<double> (0, 1) * amp[2] + amp[268] +
      amp[269]);
  jamp[5] = +1./4. * (-1./3. * amp[0] - 1./3. * amp[1] - 1./3. * amp[269] -
      1./3. * amp[270]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[14][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R19_P130_sm_qq_qqqq::matrix_19_uux_cdcxdx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 7;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{27, 9, 9, 3, 3, 9}, {9, 27, 3, 9,
      9, 3}, {9, 3, 27, 9, 9, 3}, {3, 9, 9, 27, 3, 9}, {3, 9, 9, 3, 27, 9}, {9,
      3, 3, 9, 9, 27}};

  // Calculate color flows
  jamp[0] = +1./4. * (-1./9. * amp[271] - 1./9. * amp[272] - 1./9. * amp[274] -
      1./9. * amp[275] - 1./9. * amp[276] - 1./9. * amp[277]);
  jamp[1] = +1./4. * (+1./3. * amp[271] + 1./3. * amp[272] + 1./3. * amp[274] +
      1./3. * amp[275]);
  jamp[2] = +1./4. * (+1./3. * amp[274] + 1./3. * amp[275] + 1./3. * amp[276] +
      1./3. * amp[277]);
  jamp[3] = +1./4. * (-amp[271] + Complex<double> (0, 1) * amp[273] - amp[274]
      - amp[277]);
  jamp[4] = +1./4. * (-amp[272] - Complex<double> (0, 1) * amp[273] - amp[275]
      - amp[276]);
  jamp[5] = +1./4. * (+1./3. * amp[271] + 1./3. * amp[272] + 1./3. * amp[276] +
      1./3. * amp[277]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[15][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R19_P130_sm_qq_qqqq::matrix_19_ucx_udcxdx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 7;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{27, 9, 9, 3, 3, 9}, {9, 27, 3, 9,
      9, 3}, {9, 3, 27, 9, 9, 3}, {3, 9, 9, 27, 3, 9}, {3, 9, 9, 3, 27, 9}, {9,
      3, 3, 9, 9, 27}};

  // Calculate color flows
  jamp[0] = +1./4. * (-1./3. * amp[281] - 1./3. * amp[282] - 1./3. * amp[283] -
      1./3. * amp[284]);
  jamp[1] = +1./4. * (+amp[279] + Complex<double> (0, 1) * amp[280] + amp[282]
      + amp[283]);
  jamp[2] = +1./4. * (+1./9. * amp[278] + 1./9. * amp[279] + 1./9. * amp[281] +
      1./9. * amp[282] + 1./9. * amp[283] + 1./9. * amp[284]);
  jamp[3] = +1./4. * (-1./3. * amp[278] - 1./3. * amp[279] - 1./3. * amp[283] -
      1./3. * amp[284]);
  jamp[4] = +1./4. * (-1./3. * amp[278] - 1./3. * amp[279] - 1./3. * amp[281] -
      1./3. * amp[282]);
  jamp[5] = +1./4. * (+amp[278] - Complex<double> (0, 1) * amp[280] + amp[281]
      + amp[284]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[16][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R19_P130_sm_qq_qqqq::matrix_19_ddx_ucuxcx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 7;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{27, 9, 9, 3, 3, 9}, {9, 27, 3, 9,
      9, 3}, {9, 3, 27, 9, 9, 3}, {3, 9, 9, 27, 3, 9}, {3, 9, 9, 3, 27, 9}, {9,
      3, 3, 9, 9, 27}};

  // Calculate color flows
  jamp[0] = +1./4. * (-1./9. * amp[285] - 1./9. * amp[286] - 1./9. * amp[288] -
      1./9. * amp[289] - 1./9. * amp[290] - 1./9. * amp[291]);
  jamp[1] = +1./4. * (+1./3. * amp[288] + 1./3. * amp[289] + 1./3. * amp[290] +
      1./3. * amp[291]);
  jamp[2] = +1./4. * (+1./3. * amp[285] + 1./3. * amp[286] + 1./3. * amp[290] +
      1./3. * amp[291]);
  jamp[3] = +1./4. * (-amp[285] + Complex<double> (0, 1) * amp[287] - amp[288]
      - amp[291]);
  jamp[4] = +1./4. * (-amp[286] - Complex<double> (0, 1) * amp[287] - amp[289]
      - amp[290]);
  jamp[5] = +1./4. * (+1./3. * amp[285] + 1./3. * amp[286] + 1./3. * amp[288] +
      1./3. * amp[289]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[17][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R19_P130_sm_qq_qqqq::matrix_19_uxcx_duxcxdx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 7;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{27, 9, 9, 3, 3, 9}, {9, 27, 3, 9,
      9, 3}, {9, 3, 27, 9, 9, 3}, {3, 9, 9, 27, 3, 9}, {3, 9, 9, 3, 27, 9}, {9,
      3, 3, 9, 9, 27}};

  // Calculate color flows
  jamp[0] = +1./4. * (+1./9. * amp[292] + 1./9. * amp[293] + 1./9. * amp[295] +
      1./9. * amp[296] + 1./9. * amp[297] + 1./9. * amp[298]);
  jamp[1] = +1./4. * (-1./3. * amp[292] - 1./3. * amp[293] - 1./3. * amp[295] -
      1./3. * amp[296]);
  jamp[2] = +1./4. * (-1./3. * amp[295] - 1./3. * amp[296] - 1./3. * amp[297] -
      1./3. * amp[298]);
  jamp[3] = +1./4. * (+amp[292] - Complex<double> (0, 1) * amp[294] + amp[295]
      + amp[298]);
  jamp[4] = +1./4. * (+amp[293] + Complex<double> (0, 1) * amp[294] + amp[296]
      + amp[297]);
  jamp[5] = +1./4. * (-1./3. * amp[292] - 1./3. * amp[293] - 1./3. * amp[297] -
      1./3. * amp[298]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[18][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}


}  // end namespace PY8MEs_namespace

