//==========================================================================
// This file has been automatically generated for Pythia 8 by
// MadGraph5_aMC@NLO v. 2.6.0, 2017-08-16
// By the MadGraph5_aMC@NLO Development Team
// Visit launchpad.net/madgraph5 and amcatnlo.web.cern.ch
//==========================================================================

#include "PY8MEs_R12_P16_sm_gb_zzggb.h"
#include "HelAmps_sm.h"

using namespace Pythia8_sm; 

namespace PY8MEs_namespace 
{
//==========================================================================
// Class member functions for calculating the matrix elements for
// Process: g b > z z g g b WEIGHTED<=7 @12
// Process: g b~ > z z g g b~ WEIGHTED<=7 @12

// Exception class
class PY8MEs_R12_P16_sm_gb_zzggbException : public exception
{
  virtual const char * what() const throw()
  {
    return "Exception in class 'PY8MEs_R12_P16_sm_gb_zzggb'."; 
  }
}
PY8MEs_R12_P16_sm_gb_zzggb_exception; 

std::set<int> PY8MEs_R12_P16_sm_gb_zzggb::s_channel_proc = std::set<int>
    (createset<int> ());

int PY8MEs_R12_P16_sm_gb_zzggb::helicities[ncomb][nexternal] = {{-1, -1, -1,
    -1, -1, -1, -1}, {-1, -1, -1, -1, -1, -1, 1}, {-1, -1, -1, -1, -1, 1, -1},
    {-1, -1, -1, -1, -1, 1, 1}, {-1, -1, -1, -1, 1, -1, -1}, {-1, -1, -1, -1,
    1, -1, 1}, {-1, -1, -1, -1, 1, 1, -1}, {-1, -1, -1, -1, 1, 1, 1}, {-1, -1,
    -1, 0, -1, -1, -1}, {-1, -1, -1, 0, -1, -1, 1}, {-1, -1, -1, 0, -1, 1, -1},
    {-1, -1, -1, 0, -1, 1, 1}, {-1, -1, -1, 0, 1, -1, -1}, {-1, -1, -1, 0, 1,
    -1, 1}, {-1, -1, -1, 0, 1, 1, -1}, {-1, -1, -1, 0, 1, 1, 1}, {-1, -1, -1,
    1, -1, -1, -1}, {-1, -1, -1, 1, -1, -1, 1}, {-1, -1, -1, 1, -1, 1, -1},
    {-1, -1, -1, 1, -1, 1, 1}, {-1, -1, -1, 1, 1, -1, -1}, {-1, -1, -1, 1, 1,
    -1, 1}, {-1, -1, -1, 1, 1, 1, -1}, {-1, -1, -1, 1, 1, 1, 1}, {-1, -1, 0,
    -1, -1, -1, -1}, {-1, -1, 0, -1, -1, -1, 1}, {-1, -1, 0, -1, -1, 1, -1},
    {-1, -1, 0, -1, -1, 1, 1}, {-1, -1, 0, -1, 1, -1, -1}, {-1, -1, 0, -1, 1,
    -1, 1}, {-1, -1, 0, -1, 1, 1, -1}, {-1, -1, 0, -1, 1, 1, 1}, {-1, -1, 0, 0,
    -1, -1, -1}, {-1, -1, 0, 0, -1, -1, 1}, {-1, -1, 0, 0, -1, 1, -1}, {-1, -1,
    0, 0, -1, 1, 1}, {-1, -1, 0, 0, 1, -1, -1}, {-1, -1, 0, 0, 1, -1, 1}, {-1,
    -1, 0, 0, 1, 1, -1}, {-1, -1, 0, 0, 1, 1, 1}, {-1, -1, 0, 1, -1, -1, -1},
    {-1, -1, 0, 1, -1, -1, 1}, {-1, -1, 0, 1, -1, 1, -1}, {-1, -1, 0, 1, -1, 1,
    1}, {-1, -1, 0, 1, 1, -1, -1}, {-1, -1, 0, 1, 1, -1, 1}, {-1, -1, 0, 1, 1,
    1, -1}, {-1, -1, 0, 1, 1, 1, 1}, {-1, -1, 1, -1, -1, -1, -1}, {-1, -1, 1,
    -1, -1, -1, 1}, {-1, -1, 1, -1, -1, 1, -1}, {-1, -1, 1, -1, -1, 1, 1}, {-1,
    -1, 1, -1, 1, -1, -1}, {-1, -1, 1, -1, 1, -1, 1}, {-1, -1, 1, -1, 1, 1,
    -1}, {-1, -1, 1, -1, 1, 1, 1}, {-1, -1, 1, 0, -1, -1, -1}, {-1, -1, 1, 0,
    -1, -1, 1}, {-1, -1, 1, 0, -1, 1, -1}, {-1, -1, 1, 0, -1, 1, 1}, {-1, -1,
    1, 0, 1, -1, -1}, {-1, -1, 1, 0, 1, -1, 1}, {-1, -1, 1, 0, 1, 1, -1}, {-1,
    -1, 1, 0, 1, 1, 1}, {-1, -1, 1, 1, -1, -1, -1}, {-1, -1, 1, 1, -1, -1, 1},
    {-1, -1, 1, 1, -1, 1, -1}, {-1, -1, 1, 1, -1, 1, 1}, {-1, -1, 1, 1, 1, -1,
    -1}, {-1, -1, 1, 1, 1, -1, 1}, {-1, -1, 1, 1, 1, 1, -1}, {-1, -1, 1, 1, 1,
    1, 1}, {-1, 1, -1, -1, -1, -1, -1}, {-1, 1, -1, -1, -1, -1, 1}, {-1, 1, -1,
    -1, -1, 1, -1}, {-1, 1, -1, -1, -1, 1, 1}, {-1, 1, -1, -1, 1, -1, -1}, {-1,
    1, -1, -1, 1, -1, 1}, {-1, 1, -1, -1, 1, 1, -1}, {-1, 1, -1, -1, 1, 1, 1},
    {-1, 1, -1, 0, -1, -1, -1}, {-1, 1, -1, 0, -1, -1, 1}, {-1, 1, -1, 0, -1,
    1, -1}, {-1, 1, -1, 0, -1, 1, 1}, {-1, 1, -1, 0, 1, -1, -1}, {-1, 1, -1, 0,
    1, -1, 1}, {-1, 1, -1, 0, 1, 1, -1}, {-1, 1, -1, 0, 1, 1, 1}, {-1, 1, -1,
    1, -1, -1, -1}, {-1, 1, -1, 1, -1, -1, 1}, {-1, 1, -1, 1, -1, 1, -1}, {-1,
    1, -1, 1, -1, 1, 1}, {-1, 1, -1, 1, 1, -1, -1}, {-1, 1, -1, 1, 1, -1, 1},
    {-1, 1, -1, 1, 1, 1, -1}, {-1, 1, -1, 1, 1, 1, 1}, {-1, 1, 0, -1, -1, -1,
    -1}, {-1, 1, 0, -1, -1, -1, 1}, {-1, 1, 0, -1, -1, 1, -1}, {-1, 1, 0, -1,
    -1, 1, 1}, {-1, 1, 0, -1, 1, -1, -1}, {-1, 1, 0, -1, 1, -1, 1}, {-1, 1, 0,
    -1, 1, 1, -1}, {-1, 1, 0, -1, 1, 1, 1}, {-1, 1, 0, 0, -1, -1, -1}, {-1, 1,
    0, 0, -1, -1, 1}, {-1, 1, 0, 0, -1, 1, -1}, {-1, 1, 0, 0, -1, 1, 1}, {-1,
    1, 0, 0, 1, -1, -1}, {-1, 1, 0, 0, 1, -1, 1}, {-1, 1, 0, 0, 1, 1, -1}, {-1,
    1, 0, 0, 1, 1, 1}, {-1, 1, 0, 1, -1, -1, -1}, {-1, 1, 0, 1, -1, -1, 1},
    {-1, 1, 0, 1, -1, 1, -1}, {-1, 1, 0, 1, -1, 1, 1}, {-1, 1, 0, 1, 1, -1,
    -1}, {-1, 1, 0, 1, 1, -1, 1}, {-1, 1, 0, 1, 1, 1, -1}, {-1, 1, 0, 1, 1, 1,
    1}, {-1, 1, 1, -1, -1, -1, -1}, {-1, 1, 1, -1, -1, -1, 1}, {-1, 1, 1, -1,
    -1, 1, -1}, {-1, 1, 1, -1, -1, 1, 1}, {-1, 1, 1, -1, 1, -1, -1}, {-1, 1, 1,
    -1, 1, -1, 1}, {-1, 1, 1, -1, 1, 1, -1}, {-1, 1, 1, -1, 1, 1, 1}, {-1, 1,
    1, 0, -1, -1, -1}, {-1, 1, 1, 0, -1, -1, 1}, {-1, 1, 1, 0, -1, 1, -1}, {-1,
    1, 1, 0, -1, 1, 1}, {-1, 1, 1, 0, 1, -1, -1}, {-1, 1, 1, 0, 1, -1, 1}, {-1,
    1, 1, 0, 1, 1, -1}, {-1, 1, 1, 0, 1, 1, 1}, {-1, 1, 1, 1, -1, -1, -1}, {-1,
    1, 1, 1, -1, -1, 1}, {-1, 1, 1, 1, -1, 1, -1}, {-1, 1, 1, 1, -1, 1, 1},
    {-1, 1, 1, 1, 1, -1, -1}, {-1, 1, 1, 1, 1, -1, 1}, {-1, 1, 1, 1, 1, 1, -1},
    {-1, 1, 1, 1, 1, 1, 1}, {1, -1, -1, -1, -1, -1, -1}, {1, -1, -1, -1, -1,
    -1, 1}, {1, -1, -1, -1, -1, 1, -1}, {1, -1, -1, -1, -1, 1, 1}, {1, -1, -1,
    -1, 1, -1, -1}, {1, -1, -1, -1, 1, -1, 1}, {1, -1, -1, -1, 1, 1, -1}, {1,
    -1, -1, -1, 1, 1, 1}, {1, -1, -1, 0, -1, -1, -1}, {1, -1, -1, 0, -1, -1,
    1}, {1, -1, -1, 0, -1, 1, -1}, {1, -1, -1, 0, -1, 1, 1}, {1, -1, -1, 0, 1,
    -1, -1}, {1, -1, -1, 0, 1, -1, 1}, {1, -1, -1, 0, 1, 1, -1}, {1, -1, -1, 0,
    1, 1, 1}, {1, -1, -1, 1, -1, -1, -1}, {1, -1, -1, 1, -1, -1, 1}, {1, -1,
    -1, 1, -1, 1, -1}, {1, -1, -1, 1, -1, 1, 1}, {1, -1, -1, 1, 1, -1, -1}, {1,
    -1, -1, 1, 1, -1, 1}, {1, -1, -1, 1, 1, 1, -1}, {1, -1, -1, 1, 1, 1, 1},
    {1, -1, 0, -1, -1, -1, -1}, {1, -1, 0, -1, -1, -1, 1}, {1, -1, 0, -1, -1,
    1, -1}, {1, -1, 0, -1, -1, 1, 1}, {1, -1, 0, -1, 1, -1, -1}, {1, -1, 0, -1,
    1, -1, 1}, {1, -1, 0, -1, 1, 1, -1}, {1, -1, 0, -1, 1, 1, 1}, {1, -1, 0, 0,
    -1, -1, -1}, {1, -1, 0, 0, -1, -1, 1}, {1, -1, 0, 0, -1, 1, -1}, {1, -1, 0,
    0, -1, 1, 1}, {1, -1, 0, 0, 1, -1, -1}, {1, -1, 0, 0, 1, -1, 1}, {1, -1, 0,
    0, 1, 1, -1}, {1, -1, 0, 0, 1, 1, 1}, {1, -1, 0, 1, -1, -1, -1}, {1, -1, 0,
    1, -1, -1, 1}, {1, -1, 0, 1, -1, 1, -1}, {1, -1, 0, 1, -1, 1, 1}, {1, -1,
    0, 1, 1, -1, -1}, {1, -1, 0, 1, 1, -1, 1}, {1, -1, 0, 1, 1, 1, -1}, {1, -1,
    0, 1, 1, 1, 1}, {1, -1, 1, -1, -1, -1, -1}, {1, -1, 1, -1, -1, -1, 1}, {1,
    -1, 1, -1, -1, 1, -1}, {1, -1, 1, -1, -1, 1, 1}, {1, -1, 1, -1, 1, -1, -1},
    {1, -1, 1, -1, 1, -1, 1}, {1, -1, 1, -1, 1, 1, -1}, {1, -1, 1, -1, 1, 1,
    1}, {1, -1, 1, 0, -1, -1, -1}, {1, -1, 1, 0, -1, -1, 1}, {1, -1, 1, 0, -1,
    1, -1}, {1, -1, 1, 0, -1, 1, 1}, {1, -1, 1, 0, 1, -1, -1}, {1, -1, 1, 0, 1,
    -1, 1}, {1, -1, 1, 0, 1, 1, -1}, {1, -1, 1, 0, 1, 1, 1}, {1, -1, 1, 1, -1,
    -1, -1}, {1, -1, 1, 1, -1, -1, 1}, {1, -1, 1, 1, -1, 1, -1}, {1, -1, 1, 1,
    -1, 1, 1}, {1, -1, 1, 1, 1, -1, -1}, {1, -1, 1, 1, 1, -1, 1}, {1, -1, 1, 1,
    1, 1, -1}, {1, -1, 1, 1, 1, 1, 1}, {1, 1, -1, -1, -1, -1, -1}, {1, 1, -1,
    -1, -1, -1, 1}, {1, 1, -1, -1, -1, 1, -1}, {1, 1, -1, -1, -1, 1, 1}, {1, 1,
    -1, -1, 1, -1, -1}, {1, 1, -1, -1, 1, -1, 1}, {1, 1, -1, -1, 1, 1, -1}, {1,
    1, -1, -1, 1, 1, 1}, {1, 1, -1, 0, -1, -1, -1}, {1, 1, -1, 0, -1, -1, 1},
    {1, 1, -1, 0, -1, 1, -1}, {1, 1, -1, 0, -1, 1, 1}, {1, 1, -1, 0, 1, -1,
    -1}, {1, 1, -1, 0, 1, -1, 1}, {1, 1, -1, 0, 1, 1, -1}, {1, 1, -1, 0, 1, 1,
    1}, {1, 1, -1, 1, -1, -1, -1}, {1, 1, -1, 1, -1, -1, 1}, {1, 1, -1, 1, -1,
    1, -1}, {1, 1, -1, 1, -1, 1, 1}, {1, 1, -1, 1, 1, -1, -1}, {1, 1, -1, 1, 1,
    -1, 1}, {1, 1, -1, 1, 1, 1, -1}, {1, 1, -1, 1, 1, 1, 1}, {1, 1, 0, -1, -1,
    -1, -1}, {1, 1, 0, -1, -1, -1, 1}, {1, 1, 0, -1, -1, 1, -1}, {1, 1, 0, -1,
    -1, 1, 1}, {1, 1, 0, -1, 1, -1, -1}, {1, 1, 0, -1, 1, -1, 1}, {1, 1, 0, -1,
    1, 1, -1}, {1, 1, 0, -1, 1, 1, 1}, {1, 1, 0, 0, -1, -1, -1}, {1, 1, 0, 0,
    -1, -1, 1}, {1, 1, 0, 0, -1, 1, -1}, {1, 1, 0, 0, -1, 1, 1}, {1, 1, 0, 0,
    1, -1, -1}, {1, 1, 0, 0, 1, -1, 1}, {1, 1, 0, 0, 1, 1, -1}, {1, 1, 0, 0, 1,
    1, 1}, {1, 1, 0, 1, -1, -1, -1}, {1, 1, 0, 1, -1, -1, 1}, {1, 1, 0, 1, -1,
    1, -1}, {1, 1, 0, 1, -1, 1, 1}, {1, 1, 0, 1, 1, -1, -1}, {1, 1, 0, 1, 1,
    -1, 1}, {1, 1, 0, 1, 1, 1, -1}, {1, 1, 0, 1, 1, 1, 1}, {1, 1, 1, -1, -1,
    -1, -1}, {1, 1, 1, -1, -1, -1, 1}, {1, 1, 1, -1, -1, 1, -1}, {1, 1, 1, -1,
    -1, 1, 1}, {1, 1, 1, -1, 1, -1, -1}, {1, 1, 1, -1, 1, -1, 1}, {1, 1, 1, -1,
    1, 1, -1}, {1, 1, 1, -1, 1, 1, 1}, {1, 1, 1, 0, -1, -1, -1}, {1, 1, 1, 0,
    -1, -1, 1}, {1, 1, 1, 0, -1, 1, -1}, {1, 1, 1, 0, -1, 1, 1}, {1, 1, 1, 0,
    1, -1, -1}, {1, 1, 1, 0, 1, -1, 1}, {1, 1, 1, 0, 1, 1, -1}, {1, 1, 1, 0, 1,
    1, 1}, {1, 1, 1, 1, -1, -1, -1}, {1, 1, 1, 1, -1, -1, 1}, {1, 1, 1, 1, -1,
    1, -1}, {1, 1, 1, 1, -1, 1, 1}, {1, 1, 1, 1, 1, -1, -1}, {1, 1, 1, 1, 1,
    -1, 1}, {1, 1, 1, 1, 1, 1, -1}, {1, 1, 1, 1, 1, 1, 1}};

// Normalization factors the various processes
// Denominators: spins, colors and identical particles
int PY8MEs_R12_P16_sm_gb_zzggb::denom_colors[nprocesses] = {24, 24}; 
int PY8MEs_R12_P16_sm_gb_zzggb::denom_hels[nprocesses] = {4, 4}; 
int PY8MEs_R12_P16_sm_gb_zzggb::denom_iden[nprocesses] = {4, 4}; 

//--------------------------------------------------------------------------
// Color config initialization
void PY8MEs_R12_P16_sm_gb_zzggb::initColorConfigs() 
{
  color_configs = vector < vec_vec_int > (); 
  jamp_nc_relative_power = vector < vec_int > (); 

  // Color flows of process Process: g b > z z g g b WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(2)(4)(0)(0)(0)(0)(0)(3)(2)(4)(3)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #1
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(2)(3)(0)(0)(0)(0)(0)(3)(4)(4)(2)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #2
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(2)(4)(0)(0)(0)(0)(0)(3)(1)(4)(2)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #3
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(2)(2)(0)(0)(0)(0)(0)(3)(1)(4)(3)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #4
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(2)(3)(0)(0)(0)(0)(0)(3)(2)(4)(1)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #5
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(2)(2)(0)(0)(0)(0)(0)(3)(4)(4)(1)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 

  // Color flows of process Process: g b~ > z z g g b~ WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(2)(0)(1)(0)(0)(0)(0)(3)(2)(4)(3)(0)(4)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #1
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(2)(0)(1)(0)(0)(0)(0)(3)(4)(4)(2)(0)(3)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #2
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(2)(0)(1)(0)(0)(0)(0)(3)(1)(4)(2)(0)(4)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #3
  color_configs[1].push_back(vec_int(createvector<int>
      (4)(2)(0)(1)(0)(0)(0)(0)(3)(1)(4)(3)(0)(2)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #4
  color_configs[1].push_back(vec_int(createvector<int>
      (4)(2)(0)(1)(0)(0)(0)(0)(3)(2)(4)(1)(0)(3)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #5
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(2)(0)(1)(0)(0)(0)(0)(3)(4)(4)(1)(0)(2)));
  jamp_nc_relative_power[1].push_back(0); 
}

//--------------------------------------------------------------------------
// Destructor.
PY8MEs_R12_P16_sm_gb_zzggb::~PY8MEs_R12_P16_sm_gb_zzggb() 
{
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    delete[] p[i]; 
    p[i] = NULL; 
  }
}

//--------------------------------------------------------------------------
// Invert the permutation mapping
vector<int> PY8MEs_R12_P16_sm_gb_zzggb::invert_mapping(vector<int> mapping) 
{
  vector<int> inverted_mapping; 
  for (unsigned int i = 0; i < mapping.size(); i++ )
  {
    for (unsigned int j = 0; j < mapping.size(); j++ )
    {
      if (mapping[j] == ((int)i))
      {
        inverted_mapping.push_back(j); 
        break; 
      }
    }
  }
  return inverted_mapping; 
}

//--------------------------------------------------------------------------
// Return the list of possible helicity configurations
vector < vec_int > PY8MEs_R12_P16_sm_gb_zzggb::getHelicityConfigs(vector<int>
    permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(ncomb, vector<int> (nexternal, 0)); 
  for (unsigned int ihel = 0; ihel < ncomb; ihel++ )
  {
    for(unsigned int j = 0; j < nexternal; j++ )
    {
      res[ihel][chosenPerm[j]] = helicities[ihel][j]; 
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Return the list of possible color configurations
vector < vec_int > PY8MEs_R12_P16_sm_gb_zzggb::getColorConfigs(int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(color_configs[chosenProcID].size(), vector<int>
      (nexternal * 2, 0));
  for (unsigned int icol = 0; icol < color_configs[chosenProcID].size(); icol++
      )
  {
    for(unsigned int j = 0; j < (2 * nexternal); j++ )
    {
      res[icol][chosenPerm[j/2] * 2 + j%2] =
          color_configs[chosenProcID][icol][j];
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Get JAMP relative N_c power
int PY8MEs_R12_P16_sm_gb_zzggb::getColorFlowRelativeNCPower(int color_flow_ID,
    int specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return jamp_nc_relative_power[chosenProcID][color_flow_ID]; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity ID -> Helicity Config
vector<int> PY8MEs_R12_P16_sm_gb_zzggb::getHelicityConfigForID(int hel_ID,
    vector<int> permutation)
{
  if (hel_ID < 0 || hel_ID >= ncomb)
  {
    cerr <<  "Error in function 'getHelicityConfigForID' of class" << 
    " 'PY8MEs_R12_P16_sm_gb_zzggb': Specified helicity ID '" << 
    hel_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R12_P16_sm_gb_zzggb_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(nexternal, 0); 
  for (unsigned int j = 0; j < nexternal; j++ )
  {
    res[chosenPerm[j]] = helicities[hel_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity Config -> Helicity ID
int PY8MEs_R12_P16_sm_gb_zzggb::getHelicityIDForConfig(vector<int> hel_config,
    vector<int> permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  int user_ihel = -1; 
  if (hel_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < ncomb; i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < nexternal; j++ )
      {
        if (helicities[i][chosenPerm[j]] != hel_config[j])
        {
          found = false; 
          break; 
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_ihel = i; 
        break; 
      }
    }
    if (user_ihel == -1)
    {
      cerr <<  "Error in function 'getHelicityIDForConfig' of class" << 
      " 'PY8MEs_R12_P16_sm_gb_zzggb': Specified helicity" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R12_P16_sm_gb_zzggb_exception; 
    }
  }
  return user_ihel; 
}


//--------------------------------------------------------------------------
// Implements the map Color ID -> Color Config
vector<int> PY8MEs_R12_P16_sm_gb_zzggb::getColorConfigForID(int color_ID, int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < 0 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getColorConfigForID' of class" << 
    " 'PY8MEs_R12_P16_sm_gb_zzggb': Specified color ID '" << 
    color_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R12_P16_sm_gb_zzggb_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(color_configs[chosenProcID][color_ID].size(), 0); 
  for (unsigned int j = 0; j < (2 * nexternal); j++ )
  {
    res[chosenPerm[j/2] * 2 + j%2] = color_configs[chosenProcID][color_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Color Config -> Color ID
int PY8MEs_R12_P16_sm_gb_zzggb::getColorIDForConfig(vector<int> color_config,
    int specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = -1; 
  if (color_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < color_configs[chosenProcID].size(); i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < (nexternal * 2); j++ )
      {

        // If colorless then make sure it matches
        // The little arithmetics in the color index is just
        // the permutation applies on the particle list which is
        // twice smaller since each particle can have two color indices.
        if (color_config[j] == 0)
        {
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] != 0)
          {
            found = false; 
            break; 
          }
          // Otherwise check that the color linked position matches
        }
        else
        {
          int color_linked_pos = -1; 
          // Find the other end of the line in the user color config
          for (unsigned int k = 0; k < (nexternal * 2); k++ )
          {
            if (k == j)
              continue; 
            if (color_config[j] == color_config[k])
            {
              color_linked_pos = k; 
              break; 
            }
          }
          if (color_linked_pos == -1)
          {
            cerr <<  "Error in function 'getColorIDForConfig' of class" << 
            " 'PY8MEs_R12_P16_sm_gb_zzggb': A color line could " << 
            " not be closed." << endl; 
            throw PY8MEs_R12_P16_sm_gb_zzggb_exception; 
          }
          // Now check whether the color line matches
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] !=
              color_configs[chosenProcID][i][chosenPerm[color_linked_pos/2] * 2
              + color_linked_pos%2])
          {
            found = false; 
            break; 
          }
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_icol = i; 
        break; 
      }
    }

    if (user_icol == -1)
    {
      cerr <<  "Error in function 'getColorIDForConfig' of class" << 
      " 'PY8MEs_R12_P16_sm_gb_zzggb': Specified color" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R12_P16_sm_gb_zzggb_exception; 
    }
  }
  return user_icol; 
}

//--------------------------------------------------------------------------
// Returns all result previously computed in SigmaKin
vector < vec_double > PY8MEs_R12_P16_sm_gb_zzggb::getAllResults(int
    specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return all_results[chosenProcID]; 
}

//--------------------------------------------------------------------------
// Returns a result previously computed in SigmaKin for a specific helicity
// and color ID. -1 means avg and summed over that characteristic.
double PY8MEs_R12_P16_sm_gb_zzggb::getResult(int helicity_ID, int color_ID, int
    specify_proc_ID)
{
  if (helicity_ID < - 1 || helicity_ID >= ncomb)
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R12_P16_sm_gb_zzggb': Specified helicity ID '" << 
    helicity_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R12_P16_sm_gb_zzggb_exception; 
  }
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < - 1 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R12_P16_sm_gb_zzggb': Specified color ID '" << 
    color_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R12_P16_sm_gb_zzggb_exception; 
  }
  return all_results[chosenProcID][helicity_ID + 1][color_ID + 1]; 
}

//--------------------------------------------------------------------------
// Check for the availability of the requested process and if available,
// If available, this returns the corresponding permutation and Proc_ID to use.
// If not available, this returns a negative Proc_ID.
pair < vector<int> , int > PY8MEs_R12_P16_sm_gb_zzggb::static_getPY8ME(vector<int> initial_pdgs, vector<int> final_pdgs, set<int> schannels) 
{

  // Not available return value
  pair < vector<int> , int > NA(vector<int> (), -1); 

  // Check if s-channel requirements match
  if (nreq_s_channels > 0)
  {
    if (schannels != s_channel_proc)
      return NA; 
  }
  else
  {
    if (schannels.size() != 0)
      return NA; 
  }

  // Check number of final state particles
  if (final_pdgs.size() != (nexternal - ninitial))
    return NA; 

  // Check number of initial state particles
  if (initial_pdgs.size() != ninitial)
    return NA; 

  // List of processes available in this class
  const int nprocs = 4; 
  const int proc_IDS[nprocs] = {0, 1, 0, 1}; 
  const int in_pdgs[nprocs][ninitial] = {{21, 5}, {21, -5}, {5, 21}, {-5, 21}}; 
  const int out_pdgs[nprocs][nexternal - ninitial] = {{23, 23, 21, 21, 5}, {23,
      23, 21, 21, -5}, {23, 23, 21, 21, 5}, {23, 23, 21, 21, -5}};

  bool in_pdgs_used[ninitial]; 
  bool out_pdgs_used[nexternal - ninitial]; 
  for(unsigned int i = 0; i < nprocs; i++ )
  {
    int permutations[nexternal]; 

    // Reinitialize initial state look-up variables
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      in_pdgs_used[j] = false; 
      permutations[j] = -1; 
    }
    // Look for initial state matches
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      for(unsigned int k = 0; k < ninitial; k++ )
      {
        // Make sure it has not been used already
        if (in_pdgs_used[k])
          continue; 
        if (initial_pdgs[k] == in_pdgs[i][j])
        {
          permutations[j] = k; 
          in_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[ninitial - 1] == -1)
      continue; 

    // Reinitialize final state look-up variables
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      out_pdgs_used[j] = false; 
      permutations[ninitial + j] = -1; 
    }
    // Look for final state matches
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      for(unsigned int k = 0; k < (nexternal - ninitial); k++ )
      {
        // Make sure it has not been used already
        if (out_pdgs_used[k])
          continue; 
        if (final_pdgs[k] == out_pdgs[i][j])
        {
          permutations[ninitial + j] = ninitial + k; 
          out_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[ninitial + j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[nexternal - 1] == -1)
      continue; 

    // Return process found
    return pair < vector<int> , int > (vector<int> (permutations, permutations
        + nexternal), proc_IDS[i]);
  }

  // No process found
  return NA; 
}

//--------------------------------------------------------------------------
// Set momenta
void PY8MEs_R12_P16_sm_gb_zzggb::setMomenta(vector < vec_double >
    momenta_picked)
{
  if (momenta_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setMomenta' of class" << 
    " 'PY8MEs_R12_P16_sm_gb_zzggb': Incorrect number of" << 
    " momenta specified." << endl; 
    throw PY8MEs_R12_P16_sm_gb_zzggb_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    if (momenta_picked[i].size() != 4)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R12_P16_sm_gb_zzggb': Incorrect number of" << 
      " momenta components specified." << endl; 
      throw PY8MEs_R12_P16_sm_gb_zzggb_exception; 
    }
    if (momenta_picked[i][0] < 0.0)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R12_P16_sm_gb_zzggb': A momentum was specified" << 
      " with negative energy. Check conventions." << endl; 
      throw PY8MEs_R12_P16_sm_gb_zzggb_exception; 
    }
    for (unsigned int j = 0; j < 4; j++ )
    {
      p[i][j] = momenta_picked[i][j]; 
    }
  }
}

//--------------------------------------------------------------------------
// Set color configuration to use. An empty vector means sum over all.
void PY8MEs_R12_P16_sm_gb_zzggb::setColors(vector<int> colors_picked)
{
  if (colors_picked.size() == 0)
  {
    user_colors = vector<int> (); 
    return; 
  }
  if (colors_picked.size() != (2 * nexternal))
  {
    cerr <<  "Error in function 'setColors' of class" << 
    " 'PY8MEs_R12_P16_sm_gb_zzggb': Incorrect number" << 
    " of colors specified." << endl; 
    throw PY8MEs_R12_P16_sm_gb_zzggb_exception; 
  }
  user_colors = vector<int> ((2 * nexternal), 0); 
  for(unsigned int i = 0; i < (2 * nexternal); i++ )
  {
    user_colors[i] = colors_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the helicity configuration to use. Am empty vector means sum over all.
void PY8MEs_R12_P16_sm_gb_zzggb::setHelicities(vector<int> helicities_picked) 
{
  if (helicities_picked.size() != nexternal)
  {
    if (helicities_picked.size() == 0)
    {
      user_helicities = vector<int> (); 
      return; 
    }
    cerr <<  "Error in function 'setHelicities' of class" << 
    " 'PY8MEs_R12_P16_sm_gb_zzggb': Incorrect number" << 
    " of helicities specified." << endl; 
    throw PY8MEs_R12_P16_sm_gb_zzggb_exception; 
  }
  user_helicities = vector<int> (nexternal, 0); 
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    user_helicities[i] = helicities_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the permutation to use (will apply to momenta, colors and helicities)
void PY8MEs_R12_P16_sm_gb_zzggb::setPermutation(vector<int> perm_picked) 
{
  if (perm_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setPermutations' of class" << 
    " 'PY8MEs_R12_P16_sm_gb_zzggb': Incorrect number" << 
    " of permutations specified." << endl; 
    throw PY8MEs_R12_P16_sm_gb_zzggb_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    perm[i] = perm_picked[i]; 
  }
}

// Set the proc_ID to use
void PY8MEs_R12_P16_sm_gb_zzggb::setProcID(int procID_picked) 
{
  proc_ID = procID_picked; 
}

//--------------------------------------------------------------------------
// Initialize process.

void PY8MEs_R12_P16_sm_gb_zzggb::initProc() 
{

  // Initialize flags
  include_symmetry_factors = true; 
  include_helicity_averaging_factors = true; 
  include_color_averaging_factors = true; 

  // Initialize vectors.
  perm = vector<int> (nexternal, 0); 
  user_colors = vector<int> (2 * nexternal, 0); 
  user_helicities = vector<int> (nexternal, 0); 
  p = vector < double * > (); 
  for (unsigned int i = 0; i < nexternal; i++ )
  {
    p.push_back(new double[4]); 
  }
  initColorConfigs(); 
  // Synchronize local variables dependent on the model with the active model.
  mME = vector<double> (nexternal, 0.); 
  syncProcModelParams(); 
  jamp2 = vector < vec_double > (2); 
  jamp2[0] = vector<double> (6, 0.); 
  jamp2[1] = vector<double> (6, 0.); 
  all_results = vector < vec_vec_double > (2); 
  // The first entry is always the color or helicity avg/summed matrix element.
  all_results[0] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[1] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
}

// Synchronize local variables of the process that depend on the model
// parameters
void PY8MEs_R12_P16_sm_gb_zzggb::syncProcModelParams() 
{

  // Instantiate the model class and set parameters that stay fixed during run
  mME[0] = pars->ZERO; 
  mME[1] = pars->mdl_MB; 
  mME[2] = pars->mdl_MZ; 
  mME[3] = pars->mdl_MZ; 
  mME[4] = pars->ZERO; 
  mME[5] = pars->ZERO; 
  mME[6] = pars->mdl_MB; 
}

//--------------------------------------------------------------------------
// Setter allowing to force particular values for the external masses
void PY8MEs_R12_P16_sm_gb_zzggb::setMasses(vec_double external_masses) 
{

  if (external_masses.size() != mME.size())
  {
    cerr <<  "Error in function 'setMasses' of class" << 
    " 'PY8MEs_R12_P16_sm_gb_zzggb': Incorrect number of" << 
    " masses specified." << endl; 
    throw PY8MEs_R12_P16_sm_gb_zzggb_exception; 
  }
  for (unsigned int j = 0; j < mME.size(); j++ )
  {
    mME[j] = external_masses[perm[j]]; 
  }
}

//--------------------------------------------------------------------------
// Getter accessing external masses with the correct ordering
vector<double> PY8MEs_R12_P16_sm_gb_zzggb::getMasses() 
{

  vec_double external_masses; 
  vector<int> invertedPerm; 
  invertedPerm = invert_mapping(perm); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    external_masses.push_back(mME[invertedPerm[i]]); 
  }
  return external_masses; 

}


// Set all values of the external masses to float(-mode) where mode can be
// 0 : Mass taken from the model
// 1 : Mass taken from p_i^2 if not massless to begin with
// 2 : Mass always taken from p_i^2.
void PY8MEs_R12_P16_sm_gb_zzggb::setExternalMassesMode(int mode) 
{
  if (mode != 0 && mode != 1 && mode != 2)
  {
    cerr <<  "Error in function 'setExternalMassesMode' of class" << 
    " 'PY8MEs_R12_P16_sm_gb_zzggb': Incorrect mode selected :" << mode << 
    ". It must be either 0, 1 or 2" << endl; 
    throw PY8MEs_R12_P16_sm_gb_zzggb_exception; 
  }
  if (mode == 0)
  {
    syncProcModelParams(); 
  }
  else if (mode == 1)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      if (mME[j] != pars->ZERO)
      {
        mME[j] = -1.0; 
      }
    }
  }
  else if (mode == 2)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      mME[j] = -1.0; 
    }
  }
}

//--------------------------------------------------------------------------
// Evaluate the squared matrix element.

double PY8MEs_R12_P16_sm_gb_zzggb::sigmaKin() 
{
  // Set the parameters which change event by event
  pars->setDependentParameters(); 
  pars->setDependentCouplings(); 
  // Reset color flows
  for(int i = 0; i < 6; i++ )
    jamp2[0][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[1][i] = 0.; 

  // Save previous values of mME
  vector<double> saved_mME(mME.size(), 0.0); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (mME[i] < 0.0)
    {
      saved_mME[i] = mME[i]; 
      mME[i] = sqrt(abs(pow(p[perm[i]][0], 2) - 
      (pow(p[perm[i]][1], 2) + pow(p[perm[i]][2], 2) + pow(p[perm[i]][3],
          2))));
    }
  }

  // Local variables and constants
  const int max_tries = 10; 
  vector < vec_bool > goodhel(nprocesses, vec_bool(ncomb, false)); 
  vec_int ntry(nprocesses, 0); 
  double t = 0.; 
  double result = 0.; 

  if (ntry[proc_ID] <= max_tries)
    ntry[proc_ID] = ntry[proc_ID] + 1; 

  // Find which helicity configuration is asked for
  // -1 indicates one wants to sum over helicities
  int user_ihel = getHelicityIDForConfig(user_helicities); 

  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = getColorIDForConfig(user_colors); 

  // Reset the list of results that will be recomputed here
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      all_results[proc_ID][ihel + 1][icolor + 1] = 0.; 
    }
  }

  // Calculate the matrix element for all helicities
  // unless already detected as vanishing
  for(int ihel = 0; ihel < ncomb; ihel++ )
  {
    // Skip helicity if already detected as vanishing
    if ((ntry[proc_ID] >= max_tries) && !goodhel[proc_ID][ihel])
      continue; 

    // Also skip helicity if user asks for a specific one
    if ((ntry[proc_ID] >= max_tries) && user_ihel != -1 && user_ihel != ihel)
      continue; 

    calculate_wavefunctions(helicities[ihel]); 

    // Reset locally computed color flows
    for(int i = 0; i < 6; i++ )
      jamp2[0][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[1][i] = 0.; 

    if (proc_ID == 0)
      t = matrix_12_gb_zzggb(); 
    if (proc_ID == 1)
      t = matrix_12_gbx_zzggbx(); 

    // Store which helicities give non-zero result
    if ((ntry[proc_ID] < max_tries) && t != 0. && !goodhel[proc_ID][ihel])
      goodhel[proc_ID][ihel] = true; 

    // Aggregate results
    if (user_ihel == -1 || user_ihel == ihel)
    {
      if (user_icol == -1)
      {
        result = result + t; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][0] += t; 
          for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
          {
            all_results[proc_ID][0][i + 1] += jamp2[proc_ID][i]; 
          }
        }
        all_results[proc_ID][ihel + 1][0] += t; 
        for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
        {
          all_results[proc_ID][ihel + 1][i + 1] += jamp2[proc_ID][i]; 
        }
      }
      else
      {
        result = result + jamp2[proc_ID][user_icol]; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][user_icol + 1] += jamp2[proc_ID][user_icol]; 
        }
        all_results[proc_ID][ihel + 1][user_icol + 1] +=
            jamp2[proc_ID][user_icol];
      }
    }

  }

  // Normalize results with the identical particle factor
  if (include_symmetry_factors)
  {
    result = result/denom_iden[proc_ID]; 
  }
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      if (include_symmetry_factors)
      {
        all_results[proc_ID][ihel + 1][icolor + 1] /= denom_iden[proc_ID]; 
      }
    }
  }


  // Normalize when when summing+averaging over helicity configurations
  if (user_ihel == -1 && include_helicity_averaging_factors)
  {
    result /= denom_hels[proc_ID]; 
    if (user_icol == -1)
    {
      all_results[proc_ID][0][0] /= denom_hels[proc_ID]; 
      for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
      {
        all_results[proc_ID][0][i + 1] /= denom_hels[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][0][user_icol + 1] /= denom_hels[proc_ID]; 
    }
  }

  // Normalize when summing+averaging over color configurations
  if (user_icol == -1 && include_color_averaging_factors)
  {
    result /= denom_colors[proc_ID]; 
    if (user_ihel == -1)
    {
      all_results[proc_ID][0][0] /= denom_colors[proc_ID]; 
      for (unsigned int i = 0; i < ncomb; i++ )
      {
        all_results[proc_ID][i + 1][0] /= denom_colors[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][user_ihel + 1][0] /= denom_colors[proc_ID]; 
    }
  }

  // Reinstate previous values of mME
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (saved_mME[i] < 0.0)
    {
      mME[i] = saved_mME[i]; 
    }
  }

  // Finally return it
  return result; 
}

//==========================================================================
// Private class member functions

//--------------------------------------------------------------------------
// Evaluate |M|^2 for each subprocess

void PY8MEs_R12_P16_sm_gb_zzggb::calculate_wavefunctions(const int hel[])
{
  // Calculate wavefunctions for all processes
  // Calculate all wavefunctions
  vxxxxx(p[perm[0]], mME[0], hel[0], -1, w[0]); 
  ixxxxx(p[perm[1]], mME[1], hel[1], +1, w[1]); 
  vxxxxx(p[perm[2]], mME[2], hel[2], +1, w[2]); 
  vxxxxx(p[perm[3]], mME[3], hel[3], +1, w[3]); 
  vxxxxx(p[perm[4]], mME[4], hel[4], +1, w[4]); 
  vxxxxx(p[perm[5]], mME[5], hel[5], +1, w[5]); 
  oxxxxx(p[perm[6]], mME[6], hel[6], +1, w[6]); 
  VVV1P0_1(w[0], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[7]); 
  VVS1_3(w[2], w[3], pars->GC_81, pars->mdl_MH, pars->mdl_WH, w[8]); 
  VVV1P0_1(w[7], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[9]); 
  FFS4_1(w[6], w[8], pars->GC_83, pars->mdl_MB, pars->ZERO, w[10]); 
  FFS4_2(w[1], w[8], pars->GC_83, pars->mdl_MB, pars->ZERO, w[11]); 
  FFV1_1(w[6], w[7], pars->GC_11, pars->mdl_MB, pars->ZERO, w[12]); 
  FFV1_2(w[1], w[7], pars->GC_11, pars->mdl_MB, pars->ZERO, w[13]); 
  FFV1_1(w[6], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[14]); 
  FFV1_1(w[14], w[7], pars->GC_11, pars->mdl_MB, pars->ZERO, w[15]); 
  FFV1_2(w[1], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[16]); 
  FFV1_2(w[16], w[7], pars->GC_11, pars->mdl_MB, pars->ZERO, w[17]); 
  FFV2_3_1(w[6], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[18]);
  FFV2_3_1(w[18], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[19]);
  FFV1_1(w[18], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[20]); 
  FFV2_3_2(w[1], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[21]);
  FFV1_1(w[18], w[7], pars->GC_11, pars->mdl_MB, pars->ZERO, w[22]); 
  FFV1_2(w[21], w[7], pars->GC_11, pars->mdl_MB, pars->ZERO, w[23]); 
  FFV2_3_2(w[1], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[24]);
  FFV2_3_2(w[24], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[25]);
  FFV1_2(w[24], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[26]); 
  FFV2_3_1(w[6], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[27]);
  FFV1_2(w[24], w[7], pars->GC_11, pars->mdl_MB, pars->ZERO, w[28]); 
  FFV1_1(w[27], w[7], pars->GC_11, pars->mdl_MB, pars->ZERO, w[29]); 
  FFV2_3_1(w[27], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[30]);
  FFV1_1(w[27], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[31]); 
  FFV2_3_2(w[21], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[32]);
  FFV1_2(w[21], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[33]); 
  FFV2_3_1(w[14], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[34]);
  FFV2_3_1(w[14], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[35]);
  FFV2_3_2(w[16], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[36]);
  FFV2_3_2(w[16], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[37]);
  VVV1P0_1(w[0], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[38]); 
  FFV1_1(w[6], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[39]); 
  FFV1_2(w[1], w[38], pars->GC_11, pars->mdl_MB, pars->ZERO, w[40]); 
  FFV2_3_1(w[39], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[41]);
  FFV2_3_1(w[39], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[42]);
  FFV1_1(w[39], w[38], pars->GC_11, pars->mdl_MB, pars->ZERO, w[43]); 
  FFV1_2(w[24], w[38], pars->GC_11, pars->mdl_MB, pars->ZERO, w[44]); 
  FFV1_2(w[21], w[38], pars->GC_11, pars->mdl_MB, pars->ZERO, w[45]); 
  FFV1_2(w[1], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[46]); 
  FFV1_1(w[6], w[38], pars->GC_11, pars->mdl_MB, pars->ZERO, w[47]); 
  FFV2_3_2(w[46], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[48]);
  FFV2_3_2(w[46], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[49]);
  FFV1_2(w[46], w[38], pars->GC_11, pars->mdl_MB, pars->ZERO, w[50]); 
  FFV1_1(w[18], w[38], pars->GC_11, pars->mdl_MB, pars->ZERO, w[51]); 
  FFV1_1(w[27], w[38], pars->GC_11, pars->mdl_MB, pars->ZERO, w[52]); 
  VVV1P0_1(w[38], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[53]); 
  FFV1_1(w[18], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[54]); 
  FFV1_2(w[24], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[55]); 
  FFV1_1(w[27], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[56]); 
  FFV1_2(w[21], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[57]); 
  FFV1_1(w[6], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[58]); 
  VVV1P0_1(w[4], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[59]); 
  FFV2_3_1(w[58], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[60]);
  FFV1_2(w[1], w[59], pars->GC_11, pars->mdl_MB, pars->ZERO, w[61]); 
  FFV2_3_1(w[58], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[62]);
  FFV1_1(w[58], w[59], pars->GC_11, pars->mdl_MB, pars->ZERO, w[63]); 
  FFS4_1(w[58], w[8], pars->GC_83, pars->mdl_MB, pars->ZERO, w[64]); 
  FFV1_2(w[46], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[65]); 
  FFV1_1(w[58], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[66]); 
  FFV1_1(w[58], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[67]); 
  FFV1_2(w[16], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[68]); 
  FFV1_2(w[1], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[69]); 
  FFV2_3_2(w[69], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[70]);
  FFV1_1(w[6], w[59], pars->GC_11, pars->mdl_MB, pars->ZERO, w[71]); 
  FFV2_3_2(w[69], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[72]);
  FFV1_2(w[69], w[59], pars->GC_11, pars->mdl_MB, pars->ZERO, w[73]); 
  FFS4_2(w[69], w[8], pars->GC_83, pars->mdl_MB, pars->ZERO, w[74]); 
  FFV1_1(w[39], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[75]); 
  FFV1_2(w[69], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[76]); 
  FFV1_2(w[69], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[77]); 
  FFV1_1(w[14], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[78]); 
  VVV1P0_1(w[0], w[59], pars->GC_10, pars->ZERO, pars->ZERO, w[79]); 
  FFV1_1(w[18], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[80]); 
  FFV1_2(w[21], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[81]); 
  FFV1_2(w[24], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[82]); 
  FFV1_1(w[27], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[83]); 
  FFV1_1(w[39], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[84]); 
  FFV1_2(w[16], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[85]); 
  FFV1_2(w[46], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[86]); 
  FFV1_1(w[14], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[87]); 
  VVVV1P0_1(w[0], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[88]); 
  VVVV3P0_1(w[0], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[89]); 
  VVVV4P0_1(w[0], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[90]); 
  FFV1_1(w[6], w[88], pars->GC_11, pars->mdl_MB, pars->ZERO, w[91]); 
  FFV1_1(w[6], w[89], pars->GC_11, pars->mdl_MB, pars->ZERO, w[92]); 
  FFV1_1(w[6], w[90], pars->GC_11, pars->mdl_MB, pars->ZERO, w[93]); 
  FFV1_2(w[1], w[88], pars->GC_11, pars->mdl_MB, pars->ZERO, w[94]); 
  FFV1_2(w[1], w[89], pars->GC_11, pars->mdl_MB, pars->ZERO, w[95]); 
  FFV1_2(w[1], w[90], pars->GC_11, pars->mdl_MB, pars->ZERO, w[96]); 
  oxxxxx(p[perm[1]], mME[1], hel[1], -1, w[97]); 
  ixxxxx(p[perm[6]], mME[6], hel[6], -1, w[98]); 
  FFS4_1(w[97], w[8], pars->GC_83, pars->mdl_MB, pars->ZERO, w[99]); 
  FFS4_2(w[98], w[8], pars->GC_83, pars->mdl_MB, pars->ZERO, w[100]); 
  FFV1_1(w[97], w[7], pars->GC_11, pars->mdl_MB, pars->ZERO, w[101]); 
  FFV1_2(w[98], w[7], pars->GC_11, pars->mdl_MB, pars->ZERO, w[102]); 
  FFV1_1(w[97], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[103]); 
  FFV1_1(w[103], w[7], pars->GC_11, pars->mdl_MB, pars->ZERO, w[104]); 
  FFV1_2(w[98], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[105]); 
  FFV1_2(w[105], w[7], pars->GC_11, pars->mdl_MB, pars->ZERO, w[106]); 
  FFV2_3_1(w[97], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[107]);
  FFV2_3_1(w[107], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[108]);
  FFV1_1(w[107], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[109]); 
  FFV2_3_2(w[98], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[110]);
  FFV1_1(w[107], w[7], pars->GC_11, pars->mdl_MB, pars->ZERO, w[111]); 
  FFV1_2(w[110], w[7], pars->GC_11, pars->mdl_MB, pars->ZERO, w[112]); 
  FFV2_3_2(w[98], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[113]);
  FFV2_3_2(w[113], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[114]);
  FFV1_2(w[113], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[115]); 
  FFV2_3_1(w[97], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[116]);
  FFV1_2(w[113], w[7], pars->GC_11, pars->mdl_MB, pars->ZERO, w[117]); 
  FFV1_1(w[116], w[7], pars->GC_11, pars->mdl_MB, pars->ZERO, w[118]); 
  FFV2_3_1(w[116], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[119]);
  FFV1_1(w[116], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[120]); 
  FFV2_3_2(w[110], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[121]);
  FFV1_2(w[110], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[122]); 
  FFV2_3_1(w[103], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[123]);
  FFV2_3_1(w[103], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[124]);
  FFV2_3_2(w[105], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[125]);
  FFV2_3_2(w[105], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[126]);
  FFV1_1(w[97], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[127]); 
  FFV1_2(w[98], w[38], pars->GC_11, pars->mdl_MB, pars->ZERO, w[128]); 
  FFV2_3_1(w[127], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[129]);
  FFV2_3_1(w[127], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[130]);
  FFV1_1(w[127], w[38], pars->GC_11, pars->mdl_MB, pars->ZERO, w[131]); 
  FFV1_2(w[113], w[38], pars->GC_11, pars->mdl_MB, pars->ZERO, w[132]); 
  FFV1_2(w[110], w[38], pars->GC_11, pars->mdl_MB, pars->ZERO, w[133]); 
  FFV1_2(w[98], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[134]); 
  FFV1_1(w[97], w[38], pars->GC_11, pars->mdl_MB, pars->ZERO, w[135]); 
  FFV2_3_2(w[134], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[136]);
  FFV2_3_2(w[134], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[137]);
  FFV1_2(w[134], w[38], pars->GC_11, pars->mdl_MB, pars->ZERO, w[138]); 
  FFV1_1(w[107], w[38], pars->GC_11, pars->mdl_MB, pars->ZERO, w[139]); 
  FFV1_1(w[116], w[38], pars->GC_11, pars->mdl_MB, pars->ZERO, w[140]); 
  FFV1_1(w[107], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[141]); 
  FFV1_2(w[113], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[142]); 
  FFV1_1(w[116], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[143]); 
  FFV1_2(w[110], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[144]); 
  FFV1_1(w[97], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[145]); 
  FFV2_3_1(w[145], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[146]);
  FFV1_2(w[98], w[59], pars->GC_11, pars->mdl_MB, pars->ZERO, w[147]); 
  FFV2_3_1(w[145], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[148]);
  FFV1_1(w[145], w[59], pars->GC_11, pars->mdl_MB, pars->ZERO, w[149]); 
  FFS4_1(w[145], w[8], pars->GC_83, pars->mdl_MB, pars->ZERO, w[150]); 
  FFV1_2(w[134], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[151]); 
  FFV1_1(w[145], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[152]); 
  FFV1_1(w[145], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[153]); 
  FFV1_2(w[105], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[154]); 
  FFV1_2(w[98], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[155]); 
  FFV2_3_2(w[155], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[156]);
  FFV1_1(w[97], w[59], pars->GC_11, pars->mdl_MB, pars->ZERO, w[157]); 
  FFV2_3_2(w[155], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[158]);
  FFV1_2(w[155], w[59], pars->GC_11, pars->mdl_MB, pars->ZERO, w[159]); 
  FFS4_2(w[155], w[8], pars->GC_83, pars->mdl_MB, pars->ZERO, w[160]); 
  FFV1_1(w[127], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[161]); 
  FFV1_2(w[155], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[162]); 
  FFV1_2(w[155], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[163]); 
  FFV1_1(w[103], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[164]); 
  FFV1_1(w[107], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[165]); 
  FFV1_2(w[110], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[166]); 
  FFV1_2(w[113], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[167]); 
  FFV1_1(w[116], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[168]); 
  FFV1_1(w[127], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[169]); 
  FFV1_2(w[105], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[170]); 
  FFV1_2(w[134], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[171]); 
  FFV1_1(w[103], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[172]); 
  FFV1_1(w[97], w[88], pars->GC_11, pars->mdl_MB, pars->ZERO, w[173]); 
  FFV1_1(w[97], w[89], pars->GC_11, pars->mdl_MB, pars->ZERO, w[174]); 
  FFV1_1(w[97], w[90], pars->GC_11, pars->mdl_MB, pars->ZERO, w[175]); 
  FFV1_2(w[98], w[88], pars->GC_11, pars->mdl_MB, pars->ZERO, w[176]); 
  FFV1_2(w[98], w[89], pars->GC_11, pars->mdl_MB, pars->ZERO, w[177]); 
  FFV1_2(w[98], w[90], pars->GC_11, pars->mdl_MB, pars->ZERO, w[178]); 

  // Calculate all amplitudes
  // Amplitude(s) for diagram number 0
  FFV1_0(w[1], w[10], w[9], pars->GC_11, amp[0]); 
  FFV1_0(w[11], w[6], w[9], pars->GC_11, amp[1]); 
  FFV1_0(w[11], w[12], w[5], pars->GC_11, amp[2]); 
  FFV1_0(w[13], w[10], w[5], pars->GC_11, amp[3]); 
  FFS4_0(w[1], w[15], w[8], pars->GC_83, amp[4]); 
  FFS4_0(w[13], w[14], w[8], pars->GC_83, amp[5]); 
  FFS4_0(w[17], w[6], w[8], pars->GC_83, amp[6]); 
  FFS4_0(w[16], w[12], w[8], pars->GC_83, amp[7]); 
  FFV1_0(w[1], w[19], w[9], pars->GC_11, amp[8]); 
  FFV1_0(w[13], w[19], w[5], pars->GC_11, amp[9]); 
  FFV2_3_0(w[13], w[20], w[3], pars->GC_50, pars->GC_58, amp[10]); 
  FFV1_0(w[21], w[22], w[5], pars->GC_11, amp[11]); 
  FFV1_0(w[23], w[18], w[5], pars->GC_11, amp[12]); 
  FFV1_0(w[21], w[18], w[9], pars->GC_11, amp[13]); 
  FFV2_3_0(w[16], w[22], w[3], pars->GC_50, pars->GC_58, amp[14]); 
  FFV2_3_0(w[17], w[18], w[3], pars->GC_50, pars->GC_58, amp[15]); 
  FFV1_0(w[25], w[6], w[9], pars->GC_11, amp[16]); 
  FFV1_0(w[25], w[12], w[5], pars->GC_11, amp[17]); 
  FFV2_3_0(w[26], w[12], w[3], pars->GC_50, pars->GC_58, amp[18]); 
  FFV1_0(w[28], w[27], w[5], pars->GC_11, amp[19]); 
  FFV1_0(w[24], w[29], w[5], pars->GC_11, amp[20]); 
  FFV1_0(w[24], w[27], w[9], pars->GC_11, amp[21]); 
  FFV2_3_0(w[28], w[14], w[3], pars->GC_50, pars->GC_58, amp[22]); 
  FFV2_3_0(w[24], w[15], w[3], pars->GC_50, pars->GC_58, amp[23]); 
  FFV1_0(w[1], w[30], w[9], pars->GC_11, amp[24]); 
  FFV1_0(w[13], w[30], w[5], pars->GC_11, amp[25]); 
  FFV2_3_0(w[13], w[31], w[2], pars->GC_50, pars->GC_58, amp[26]); 
  FFV2_3_0(w[16], w[29], w[2], pars->GC_50, pars->GC_58, amp[27]); 
  FFV2_3_0(w[17], w[27], w[2], pars->GC_50, pars->GC_58, amp[28]); 
  FFV1_0(w[32], w[6], w[9], pars->GC_11, amp[29]); 
  FFV1_0(w[32], w[12], w[5], pars->GC_11, amp[30]); 
  FFV2_3_0(w[33], w[12], w[2], pars->GC_50, pars->GC_58, amp[31]); 
  FFV2_3_0(w[23], w[14], w[2], pars->GC_50, pars->GC_58, amp[32]); 
  FFV2_3_0(w[21], w[15], w[2], pars->GC_50, pars->GC_58, amp[33]); 
  FFV2_3_0(w[13], w[34], w[3], pars->GC_50, pars->GC_58, amp[34]); 
  FFV2_3_0(w[13], w[35], w[2], pars->GC_50, pars->GC_58, amp[35]); 
  FFV2_3_0(w[36], w[12], w[3], pars->GC_50, pars->GC_58, amp[36]); 
  FFV2_3_0(w[37], w[12], w[2], pars->GC_50, pars->GC_58, amp[37]); 
  FFV2_3_0(w[40], w[41], w[3], pars->GC_50, pars->GC_58, amp[38]); 
  FFV2_3_0(w[40], w[42], w[2], pars->GC_50, pars->GC_58, amp[39]); 
  FFS4_0(w[1], w[43], w[8], pars->GC_83, amp[40]); 
  FFS4_0(w[40], w[39], w[8], pars->GC_83, amp[41]); 
  FFV2_3_0(w[24], w[43], w[3], pars->GC_50, pars->GC_58, amp[42]); 
  FFV2_3_0(w[44], w[39], w[3], pars->GC_50, pars->GC_58, amp[43]); 
  FFV2_3_0(w[21], w[43], w[2], pars->GC_50, pars->GC_58, amp[44]); 
  FFV2_3_0(w[45], w[39], w[2], pars->GC_50, pars->GC_58, amp[45]); 
  FFV2_3_0(w[48], w[47], w[3], pars->GC_50, pars->GC_58, amp[46]); 
  FFV2_3_0(w[49], w[47], w[2], pars->GC_50, pars->GC_58, amp[47]); 
  FFS4_0(w[50], w[6], w[8], pars->GC_83, amp[48]); 
  FFS4_0(w[46], w[47], w[8], pars->GC_83, amp[49]); 
  FFV2_3_0(w[50], w[18], w[3], pars->GC_50, pars->GC_58, amp[50]); 
  FFV2_3_0(w[46], w[51], w[3], pars->GC_50, pars->GC_58, amp[51]); 
  FFV2_3_0(w[50], w[27], w[2], pars->GC_50, pars->GC_58, amp[52]); 
  FFV2_3_0(w[46], w[52], w[2], pars->GC_50, pars->GC_58, amp[53]); 
  FFV1_0(w[1], w[10], w[53], pars->GC_11, amp[54]); 
  FFV1_0(w[11], w[6], w[53], pars->GC_11, amp[55]); 
  FFV1_0(w[11], w[47], w[4], pars->GC_11, amp[56]); 
  FFV1_0(w[40], w[10], w[4], pars->GC_11, amp[57]); 
  FFV1_0(w[1], w[19], w[53], pars->GC_11, amp[58]); 
  FFV2_3_0(w[40], w[54], w[3], pars->GC_50, pars->GC_58, amp[59]); 
  FFV1_0(w[40], w[19], w[4], pars->GC_11, amp[60]); 
  FFV1_0(w[21], w[18], w[53], pars->GC_11, amp[61]); 
  FFV1_0(w[21], w[51], w[4], pars->GC_11, amp[62]); 
  FFV1_0(w[45], w[18], w[4], pars->GC_11, amp[63]); 
  FFV1_0(w[25], w[6], w[53], pars->GC_11, amp[64]); 
  FFV2_3_0(w[55], w[47], w[3], pars->GC_50, pars->GC_58, amp[65]); 
  FFV1_0(w[25], w[47], w[4], pars->GC_11, amp[66]); 
  FFV1_0(w[24], w[27], w[53], pars->GC_11, amp[67]); 
  FFV1_0(w[44], w[27], w[4], pars->GC_11, amp[68]); 
  FFV1_0(w[24], w[52], w[4], pars->GC_11, amp[69]); 
  FFV1_0(w[1], w[30], w[53], pars->GC_11, amp[70]); 
  FFV2_3_0(w[40], w[56], w[2], pars->GC_50, pars->GC_58, amp[71]); 
  FFV1_0(w[40], w[30], w[4], pars->GC_11, amp[72]); 
  FFV1_0(w[32], w[6], w[53], pars->GC_11, amp[73]); 
  FFV2_3_0(w[57], w[47], w[2], pars->GC_50, pars->GC_58, amp[74]); 
  FFV1_0(w[32], w[47], w[4], pars->GC_11, amp[75]); 
  FFV2_3_0(w[61], w[60], w[3], pars->GC_50, pars->GC_58, amp[76]); 
  FFV2_3_0(w[61], w[62], w[2], pars->GC_50, pars->GC_58, amp[77]); 
  FFS4_0(w[1], w[63], w[8], pars->GC_83, amp[78]); 
  FFV1_0(w[1], w[64], w[59], pars->GC_11, amp[79]); 
  FFV2_3_0(w[24], w[63], w[3], pars->GC_50, pars->GC_58, amp[80]); 
  FFV1_0(w[24], w[62], w[59], pars->GC_11, amp[81]); 
  FFV2_3_0(w[21], w[63], w[2], pars->GC_50, pars->GC_58, amp[82]); 
  FFV1_0(w[21], w[60], w[59], pars->GC_11, amp[83]); 
  FFV1_0(w[49], w[60], w[5], pars->GC_11, amp[84]); 
  FFV2_3_0(w[65], w[60], w[3], pars->GC_50, pars->GC_58, amp[85]); 
  FFV1_0(w[48], w[62], w[5], pars->GC_11, amp[86]); 
  FFV2_3_0(w[65], w[62], w[2], pars->GC_50, pars->GC_58, amp[87]); 
  FFV2_3_0(w[48], w[66], w[3], pars->GC_50, pars->GC_58, amp[88]); 
  FFV2_3_0(w[49], w[66], w[2], pars->GC_50, pars->GC_58, amp[89]); 
  FFV1_0(w[46], w[64], w[5], pars->GC_11, amp[90]); 
  FFS4_0(w[46], w[66], w[8], pars->GC_83, amp[91]); 
  FFV1_0(w[11], w[67], w[5], pars->GC_11, amp[92]); 
  FFV1_0(w[11], w[66], w[4], pars->GC_11, amp[93]); 
  FFS4_0(w[16], w[67], w[8], pars->GC_83, amp[94]); 
  FFV1_0(w[16], w[64], w[4], pars->GC_11, amp[95]); 
  FFV1_0(w[25], w[67], w[5], pars->GC_11, amp[96]); 
  FFV2_3_0(w[26], w[67], w[3], pars->GC_50, pars->GC_58, amp[97]); 
  FFV1_0(w[55], w[62], w[5], pars->GC_11, amp[98]); 
  FFV1_0(w[26], w[62], w[4], pars->GC_11, amp[99]); 
  FFV2_3_0(w[55], w[66], w[3], pars->GC_50, pars->GC_58, amp[100]); 
  FFV1_0(w[25], w[66], w[4], pars->GC_11, amp[101]); 
  FFV1_0(w[32], w[67], w[5], pars->GC_11, amp[102]); 
  FFV2_3_0(w[33], w[67], w[2], pars->GC_50, pars->GC_58, amp[103]); 
  FFV1_0(w[57], w[60], w[5], pars->GC_11, amp[104]); 
  FFV1_0(w[33], w[60], w[4], pars->GC_11, amp[105]); 
  FFV2_3_0(w[57], w[66], w[2], pars->GC_50, pars->GC_58, amp[106]); 
  FFV1_0(w[32], w[66], w[4], pars->GC_11, amp[107]); 
  FFV2_3_0(w[36], w[67], w[3], pars->GC_50, pars->GC_58, amp[108]); 
  FFV2_3_0(w[37], w[67], w[2], pars->GC_50, pars->GC_58, amp[109]); 
  FFV2_3_0(w[68], w[60], w[3], pars->GC_50, pars->GC_58, amp[110]); 
  FFV1_0(w[37], w[60], w[4], pars->GC_11, amp[111]); 
  FFV2_3_0(w[68], w[62], w[2], pars->GC_50, pars->GC_58, amp[112]); 
  FFV1_0(w[36], w[62], w[4], pars->GC_11, amp[113]); 
  FFV2_3_0(w[70], w[71], w[3], pars->GC_50, pars->GC_58, amp[114]); 
  FFV2_3_0(w[72], w[71], w[2], pars->GC_50, pars->GC_58, amp[115]); 
  FFS4_0(w[73], w[6], w[8], pars->GC_83, amp[116]); 
  FFV1_0(w[74], w[6], w[59], pars->GC_11, amp[117]); 
  FFV2_3_0(w[73], w[18], w[3], pars->GC_50, pars->GC_58, amp[118]); 
  FFV1_0(w[72], w[18], w[59], pars->GC_11, amp[119]); 
  FFV2_3_0(w[73], w[27], w[2], pars->GC_50, pars->GC_58, amp[120]); 
  FFV1_0(w[70], w[27], w[59], pars->GC_11, amp[121]); 
  FFV1_0(w[70], w[42], w[5], pars->GC_11, amp[122]); 
  FFV2_3_0(w[70], w[75], w[3], pars->GC_50, pars->GC_58, amp[123]); 
  FFV1_0(w[72], w[41], w[5], pars->GC_11, amp[124]); 
  FFV2_3_0(w[72], w[75], w[2], pars->GC_50, pars->GC_58, amp[125]); 
  FFV2_3_0(w[76], w[41], w[3], pars->GC_50, pars->GC_58, amp[126]); 
  FFV2_3_0(w[76], w[42], w[2], pars->GC_50, pars->GC_58, amp[127]); 
  FFV1_0(w[74], w[39], w[5], pars->GC_11, amp[128]); 
  FFS4_0(w[76], w[39], w[8], pars->GC_83, amp[129]); 
  FFV1_0(w[77], w[10], w[5], pars->GC_11, amp[130]); 
  FFV1_0(w[76], w[10], w[4], pars->GC_11, amp[131]); 
  FFS4_0(w[77], w[14], w[8], pars->GC_83, amp[132]); 
  FFV1_0(w[74], w[14], w[4], pars->GC_11, amp[133]); 
  FFV1_0(w[77], w[19], w[5], pars->GC_11, amp[134]); 
  FFV2_3_0(w[77], w[20], w[3], pars->GC_50, pars->GC_58, amp[135]); 
  FFV1_0(w[72], w[54], w[5], pars->GC_11, amp[136]); 
  FFV1_0(w[72], w[20], w[4], pars->GC_11, amp[137]); 
  FFV2_3_0(w[76], w[54], w[3], pars->GC_50, pars->GC_58, amp[138]); 
  FFV1_0(w[76], w[19], w[4], pars->GC_11, amp[139]); 
  FFV1_0(w[77], w[30], w[5], pars->GC_11, amp[140]); 
  FFV2_3_0(w[77], w[31], w[2], pars->GC_50, pars->GC_58, amp[141]); 
  FFV1_0(w[70], w[56], w[5], pars->GC_11, amp[142]); 
  FFV1_0(w[70], w[31], w[4], pars->GC_11, amp[143]); 
  FFV2_3_0(w[76], w[56], w[2], pars->GC_50, pars->GC_58, amp[144]); 
  FFV1_0(w[76], w[30], w[4], pars->GC_11, amp[145]); 
  FFV2_3_0(w[77], w[34], w[3], pars->GC_50, pars->GC_58, amp[146]); 
  FFV2_3_0(w[77], w[35], w[2], pars->GC_50, pars->GC_58, amp[147]); 
  FFV2_3_0(w[70], w[78], w[3], pars->GC_50, pars->GC_58, amp[148]); 
  FFV1_0(w[70], w[35], w[4], pars->GC_11, amp[149]); 
  FFV2_3_0(w[72], w[78], w[2], pars->GC_50, pars->GC_58, amp[150]); 
  FFV1_0(w[72], w[34], w[4], pars->GC_11, amp[151]); 
  FFV1_0(w[1], w[10], w[79], pars->GC_11, amp[152]); 
  FFV1_0(w[11], w[6], w[79], pars->GC_11, amp[153]); 
  FFV1_0(w[11], w[71], w[0], pars->GC_11, amp[154]); 
  FFV1_0(w[61], w[10], w[0], pars->GC_11, amp[155]); 
  FFV1_0(w[1], w[19], w[79], pars->GC_11, amp[156]); 
  FFV2_3_0(w[61], w[80], w[3], pars->GC_50, pars->GC_58, amp[157]); 
  FFV1_0(w[61], w[19], w[0], pars->GC_11, amp[158]); 
  FFV1_0(w[21], w[18], w[79], pars->GC_11, amp[159]); 
  FFV1_0(w[21], w[80], w[59], pars->GC_11, amp[160]); 
  FFV1_0(w[81], w[18], w[59], pars->GC_11, amp[161]); 
  FFV1_0(w[25], w[6], w[79], pars->GC_11, amp[162]); 
  FFV2_3_0(w[82], w[71], w[3], pars->GC_50, pars->GC_58, amp[163]); 
  FFV1_0(w[25], w[71], w[0], pars->GC_11, amp[164]); 
  FFV1_0(w[24], w[27], w[79], pars->GC_11, amp[165]); 
  FFV1_0(w[82], w[27], w[59], pars->GC_11, amp[166]); 
  FFV1_0(w[24], w[83], w[59], pars->GC_11, amp[167]); 
  FFV1_0(w[1], w[30], w[79], pars->GC_11, amp[168]); 
  FFV2_3_0(w[61], w[83], w[2], pars->GC_50, pars->GC_58, amp[169]); 
  FFV1_0(w[61], w[30], w[0], pars->GC_11, amp[170]); 
  FFV1_0(w[32], w[6], w[79], pars->GC_11, amp[171]); 
  FFV2_3_0(w[81], w[71], w[2], pars->GC_50, pars->GC_58, amp[172]); 
  FFV1_0(w[32], w[71], w[0], pars->GC_11, amp[173]); 
  FFV1_0(w[11], w[84], w[5], pars->GC_11, amp[174]); 
  FFV1_0(w[11], w[75], w[0], pars->GC_11, amp[175]); 
  FFS4_0(w[16], w[84], w[8], pars->GC_83, amp[176]); 
  FFS4_0(w[85], w[39], w[8], pars->GC_83, amp[177]); 
  FFV1_0(w[25], w[84], w[5], pars->GC_11, amp[178]); 
  FFV2_3_0(w[26], w[84], w[3], pars->GC_50, pars->GC_58, amp[179]); 
  FFV1_0(w[82], w[42], w[5], pars->GC_11, amp[180]); 
  FFV2_3_0(w[82], w[75], w[3], pars->GC_50, pars->GC_58, amp[181]); 
  FFV1_0(w[26], w[42], w[0], pars->GC_11, amp[182]); 
  FFV1_0(w[25], w[75], w[0], pars->GC_11, amp[183]); 
  FFV1_0(w[32], w[84], w[5], pars->GC_11, amp[184]); 
  FFV2_3_0(w[33], w[84], w[2], pars->GC_50, pars->GC_58, amp[185]); 
  FFV1_0(w[81], w[41], w[5], pars->GC_11, amp[186]); 
  FFV2_3_0(w[81], w[75], w[2], pars->GC_50, pars->GC_58, amp[187]); 
  FFV1_0(w[33], w[41], w[0], pars->GC_11, amp[188]); 
  FFV1_0(w[32], w[75], w[0], pars->GC_11, amp[189]); 
  FFV2_3_0(w[36], w[84], w[3], pars->GC_50, pars->GC_58, amp[190]); 
  FFV2_3_0(w[37], w[84], w[2], pars->GC_50, pars->GC_58, amp[191]); 
  FFV2_3_0(w[85], w[41], w[3], pars->GC_50, pars->GC_58, amp[192]); 
  FFV2_3_0(w[85], w[42], w[2], pars->GC_50, pars->GC_58, amp[193]); 
  FFV1_0(w[37], w[41], w[0], pars->GC_11, amp[194]); 
  FFV1_0(w[36], w[42], w[0], pars->GC_11, amp[195]); 
  FFV1_0(w[86], w[10], w[5], pars->GC_11, amp[196]); 
  FFV1_0(w[65], w[10], w[0], pars->GC_11, amp[197]); 
  FFS4_0(w[86], w[14], w[8], pars->GC_83, amp[198]); 
  FFS4_0(w[46], w[87], w[8], pars->GC_83, amp[199]); 
  FFV1_0(w[86], w[19], w[5], pars->GC_11, amp[200]); 
  FFV2_3_0(w[86], w[20], w[3], pars->GC_50, pars->GC_58, amp[201]); 
  FFV1_0(w[49], w[80], w[5], pars->GC_11, amp[202]); 
  FFV2_3_0(w[65], w[80], w[3], pars->GC_50, pars->GC_58, amp[203]); 
  FFV1_0(w[49], w[20], w[0], pars->GC_11, amp[204]); 
  FFV1_0(w[65], w[19], w[0], pars->GC_11, amp[205]); 
  FFV1_0(w[86], w[30], w[5], pars->GC_11, amp[206]); 
  FFV2_3_0(w[86], w[31], w[2], pars->GC_50, pars->GC_58, amp[207]); 
  FFV1_0(w[48], w[83], w[5], pars->GC_11, amp[208]); 
  FFV2_3_0(w[65], w[83], w[2], pars->GC_50, pars->GC_58, amp[209]); 
  FFV1_0(w[48], w[31], w[0], pars->GC_11, amp[210]); 
  FFV1_0(w[65], w[30], w[0], pars->GC_11, amp[211]); 
  FFV2_3_0(w[86], w[34], w[3], pars->GC_50, pars->GC_58, amp[212]); 
  FFV2_3_0(w[86], w[35], w[2], pars->GC_50, pars->GC_58, amp[213]); 
  FFV2_3_0(w[48], w[87], w[3], pars->GC_50, pars->GC_58, amp[214]); 
  FFV2_3_0(w[49], w[87], w[2], pars->GC_50, pars->GC_58, amp[215]); 
  FFV1_0(w[48], w[35], w[0], pars->GC_11, amp[216]); 
  FFV1_0(w[49], w[34], w[0], pars->GC_11, amp[217]); 
  FFV1_0(w[11], w[87], w[4], pars->GC_11, amp[218]); 
  FFV1_0(w[11], w[78], w[0], pars->GC_11, amp[219]); 
  FFV1_0(w[85], w[10], w[4], pars->GC_11, amp[220]); 
  FFV1_0(w[68], w[10], w[0], pars->GC_11, amp[221]); 
  FFV1_0(w[57], w[80], w[5], pars->GC_11, amp[222]); 
  FFV1_0(w[33], w[80], w[4], pars->GC_11, amp[223]); 
  FFV1_0(w[81], w[54], w[5], pars->GC_11, amp[224]); 
  FFV1_0(w[81], w[20], w[4], pars->GC_11, amp[225]); 
  FFV1_0(w[33], w[54], w[0], pars->GC_11, amp[226]); 
  FFV1_0(w[57], w[20], w[0], pars->GC_11, amp[227]); 
  FFV2_3_0(w[68], w[80], w[3], pars->GC_50, pars->GC_58, amp[228]); 
  FFV1_0(w[37], w[80], w[4], pars->GC_11, amp[229]); 
  FFV2_3_0(w[85], w[54], w[3], pars->GC_50, pars->GC_58, amp[230]); 
  FFV1_0(w[85], w[19], w[4], pars->GC_11, amp[231]); 
  FFV1_0(w[37], w[54], w[0], pars->GC_11, amp[232]); 
  FFV1_0(w[68], w[19], w[0], pars->GC_11, amp[233]); 
  FFV1_0(w[82], w[56], w[5], pars->GC_11, amp[234]); 
  FFV1_0(w[82], w[31], w[4], pars->GC_11, amp[235]); 
  FFV1_0(w[55], w[83], w[5], pars->GC_11, amp[236]); 
  FFV1_0(w[26], w[83], w[4], pars->GC_11, amp[237]); 
  FFV1_0(w[55], w[31], w[0], pars->GC_11, amp[238]); 
  FFV1_0(w[26], w[56], w[0], pars->GC_11, amp[239]); 
  FFV2_3_0(w[82], w[78], w[3], pars->GC_50, pars->GC_58, amp[240]); 
  FFV1_0(w[82], w[35], w[4], pars->GC_11, amp[241]); 
  FFV2_3_0(w[55], w[87], w[3], pars->GC_50, pars->GC_58, amp[242]); 
  FFV1_0(w[25], w[87], w[4], pars->GC_11, amp[243]); 
  FFV1_0(w[55], w[35], w[0], pars->GC_11, amp[244]); 
  FFV1_0(w[25], w[78], w[0], pars->GC_11, amp[245]); 
  FFV2_3_0(w[68], w[83], w[2], pars->GC_50, pars->GC_58, amp[246]); 
  FFV1_0(w[36], w[83], w[4], pars->GC_11, amp[247]); 
  FFV2_3_0(w[85], w[56], w[2], pars->GC_50, pars->GC_58, amp[248]); 
  FFV1_0(w[85], w[30], w[4], pars->GC_11, amp[249]); 
  FFV1_0(w[36], w[56], w[0], pars->GC_11, amp[250]); 
  FFV1_0(w[68], w[30], w[0], pars->GC_11, amp[251]); 
  FFV2_3_0(w[81], w[78], w[2], pars->GC_50, pars->GC_58, amp[252]); 
  FFV1_0(w[81], w[34], w[4], pars->GC_11, amp[253]); 
  FFV2_3_0(w[57], w[87], w[2], pars->GC_50, pars->GC_58, amp[254]); 
  FFV1_0(w[32], w[87], w[4], pars->GC_11, amp[255]); 
  FFV1_0(w[57], w[34], w[0], pars->GC_11, amp[256]); 
  FFV1_0(w[32], w[78], w[0], pars->GC_11, amp[257]); 
  FFS4_0(w[1], w[91], w[8], pars->GC_83, amp[258]); 
  FFS4_0(w[1], w[92], w[8], pars->GC_83, amp[259]); 
  FFS4_0(w[1], w[93], w[8], pars->GC_83, amp[260]); 
  FFS4_0(w[94], w[6], w[8], pars->GC_83, amp[261]); 
  FFS4_0(w[95], w[6], w[8], pars->GC_83, amp[262]); 
  FFS4_0(w[96], w[6], w[8], pars->GC_83, amp[263]); 
  FFV2_3_0(w[94], w[18], w[3], pars->GC_50, pars->GC_58, amp[264]); 
  FFV2_3_0(w[95], w[18], w[3], pars->GC_50, pars->GC_58, amp[265]); 
  FFV2_3_0(w[96], w[18], w[3], pars->GC_50, pars->GC_58, amp[266]); 
  FFV1_0(w[21], w[18], w[88], pars->GC_11, amp[267]); 
  FFV1_0(w[21], w[18], w[89], pars->GC_11, amp[268]); 
  FFV1_0(w[21], w[18], w[90], pars->GC_11, amp[269]); 
  FFV2_3_0(w[24], w[91], w[3], pars->GC_50, pars->GC_58, amp[270]); 
  FFV2_3_0(w[24], w[92], w[3], pars->GC_50, pars->GC_58, amp[271]); 
  FFV2_3_0(w[24], w[93], w[3], pars->GC_50, pars->GC_58, amp[272]); 
  FFV1_0(w[24], w[27], w[88], pars->GC_11, amp[273]); 
  FFV1_0(w[24], w[27], w[89], pars->GC_11, amp[274]); 
  FFV1_0(w[24], w[27], w[90], pars->GC_11, amp[275]); 
  FFV2_3_0(w[94], w[27], w[2], pars->GC_50, pars->GC_58, amp[276]); 
  FFV2_3_0(w[95], w[27], w[2], pars->GC_50, pars->GC_58, amp[277]); 
  FFV2_3_0(w[96], w[27], w[2], pars->GC_50, pars->GC_58, amp[278]); 
  FFV2_3_0(w[21], w[91], w[2], pars->GC_50, pars->GC_58, amp[279]); 
  FFV2_3_0(w[21], w[92], w[2], pars->GC_50, pars->GC_58, amp[280]); 
  FFV2_3_0(w[21], w[93], w[2], pars->GC_50, pars->GC_58, amp[281]); 
  FFV1_0(w[98], w[99], w[9], pars->GC_11, amp[282]); 
  FFV1_0(w[100], w[97], w[9], pars->GC_11, amp[283]); 
  FFV1_0(w[100], w[101], w[5], pars->GC_11, amp[284]); 
  FFV1_0(w[102], w[99], w[5], pars->GC_11, amp[285]); 
  FFS4_0(w[98], w[104], w[8], pars->GC_83, amp[286]); 
  FFS4_0(w[102], w[103], w[8], pars->GC_83, amp[287]); 
  FFS4_0(w[106], w[97], w[8], pars->GC_83, amp[288]); 
  FFS4_0(w[105], w[101], w[8], pars->GC_83, amp[289]); 
  FFV1_0(w[98], w[108], w[9], pars->GC_11, amp[290]); 
  FFV1_0(w[102], w[108], w[5], pars->GC_11, amp[291]); 
  FFV2_3_0(w[102], w[109], w[3], pars->GC_50, pars->GC_58, amp[292]); 
  FFV1_0(w[110], w[111], w[5], pars->GC_11, amp[293]); 
  FFV1_0(w[112], w[107], w[5], pars->GC_11, amp[294]); 
  FFV1_0(w[110], w[107], w[9], pars->GC_11, amp[295]); 
  FFV2_3_0(w[105], w[111], w[3], pars->GC_50, pars->GC_58, amp[296]); 
  FFV2_3_0(w[106], w[107], w[3], pars->GC_50, pars->GC_58, amp[297]); 
  FFV1_0(w[114], w[97], w[9], pars->GC_11, amp[298]); 
  FFV1_0(w[114], w[101], w[5], pars->GC_11, amp[299]); 
  FFV2_3_0(w[115], w[101], w[3], pars->GC_50, pars->GC_58, amp[300]); 
  FFV1_0(w[117], w[116], w[5], pars->GC_11, amp[301]); 
  FFV1_0(w[113], w[118], w[5], pars->GC_11, amp[302]); 
  FFV1_0(w[113], w[116], w[9], pars->GC_11, amp[303]); 
  FFV2_3_0(w[117], w[103], w[3], pars->GC_50, pars->GC_58, amp[304]); 
  FFV2_3_0(w[113], w[104], w[3], pars->GC_50, pars->GC_58, amp[305]); 
  FFV1_0(w[98], w[119], w[9], pars->GC_11, amp[306]); 
  FFV1_0(w[102], w[119], w[5], pars->GC_11, amp[307]); 
  FFV2_3_0(w[102], w[120], w[2], pars->GC_50, pars->GC_58, amp[308]); 
  FFV2_3_0(w[105], w[118], w[2], pars->GC_50, pars->GC_58, amp[309]); 
  FFV2_3_0(w[106], w[116], w[2], pars->GC_50, pars->GC_58, amp[310]); 
  FFV1_0(w[121], w[97], w[9], pars->GC_11, amp[311]); 
  FFV1_0(w[121], w[101], w[5], pars->GC_11, amp[312]); 
  FFV2_3_0(w[122], w[101], w[2], pars->GC_50, pars->GC_58, amp[313]); 
  FFV2_3_0(w[112], w[103], w[2], pars->GC_50, pars->GC_58, amp[314]); 
  FFV2_3_0(w[110], w[104], w[2], pars->GC_50, pars->GC_58, amp[315]); 
  FFV2_3_0(w[102], w[123], w[3], pars->GC_50, pars->GC_58, amp[316]); 
  FFV2_3_0(w[102], w[124], w[2], pars->GC_50, pars->GC_58, amp[317]); 
  FFV2_3_0(w[125], w[101], w[3], pars->GC_50, pars->GC_58, amp[318]); 
  FFV2_3_0(w[126], w[101], w[2], pars->GC_50, pars->GC_58, amp[319]); 
  FFV2_3_0(w[128], w[129], w[3], pars->GC_50, pars->GC_58, amp[320]); 
  FFV2_3_0(w[128], w[130], w[2], pars->GC_50, pars->GC_58, amp[321]); 
  FFS4_0(w[98], w[131], w[8], pars->GC_83, amp[322]); 
  FFS4_0(w[128], w[127], w[8], pars->GC_83, amp[323]); 
  FFV2_3_0(w[113], w[131], w[3], pars->GC_50, pars->GC_58, amp[324]); 
  FFV2_3_0(w[132], w[127], w[3], pars->GC_50, pars->GC_58, amp[325]); 
  FFV2_3_0(w[110], w[131], w[2], pars->GC_50, pars->GC_58, amp[326]); 
  FFV2_3_0(w[133], w[127], w[2], pars->GC_50, pars->GC_58, amp[327]); 
  FFV2_3_0(w[136], w[135], w[3], pars->GC_50, pars->GC_58, amp[328]); 
  FFV2_3_0(w[137], w[135], w[2], pars->GC_50, pars->GC_58, amp[329]); 
  FFS4_0(w[138], w[97], w[8], pars->GC_83, amp[330]); 
  FFS4_0(w[134], w[135], w[8], pars->GC_83, amp[331]); 
  FFV2_3_0(w[138], w[107], w[3], pars->GC_50, pars->GC_58, amp[332]); 
  FFV2_3_0(w[134], w[139], w[3], pars->GC_50, pars->GC_58, amp[333]); 
  FFV2_3_0(w[138], w[116], w[2], pars->GC_50, pars->GC_58, amp[334]); 
  FFV2_3_0(w[134], w[140], w[2], pars->GC_50, pars->GC_58, amp[335]); 
  FFV1_0(w[98], w[99], w[53], pars->GC_11, amp[336]); 
  FFV1_0(w[100], w[97], w[53], pars->GC_11, amp[337]); 
  FFV1_0(w[100], w[135], w[4], pars->GC_11, amp[338]); 
  FFV1_0(w[128], w[99], w[4], pars->GC_11, amp[339]); 
  FFV1_0(w[98], w[108], w[53], pars->GC_11, amp[340]); 
  FFV2_3_0(w[128], w[141], w[3], pars->GC_50, pars->GC_58, amp[341]); 
  FFV1_0(w[128], w[108], w[4], pars->GC_11, amp[342]); 
  FFV1_0(w[110], w[107], w[53], pars->GC_11, amp[343]); 
  FFV1_0(w[110], w[139], w[4], pars->GC_11, amp[344]); 
  FFV1_0(w[133], w[107], w[4], pars->GC_11, amp[345]); 
  FFV1_0(w[114], w[97], w[53], pars->GC_11, amp[346]); 
  FFV2_3_0(w[142], w[135], w[3], pars->GC_50, pars->GC_58, amp[347]); 
  FFV1_0(w[114], w[135], w[4], pars->GC_11, amp[348]); 
  FFV1_0(w[113], w[116], w[53], pars->GC_11, amp[349]); 
  FFV1_0(w[132], w[116], w[4], pars->GC_11, amp[350]); 
  FFV1_0(w[113], w[140], w[4], pars->GC_11, amp[351]); 
  FFV1_0(w[98], w[119], w[53], pars->GC_11, amp[352]); 
  FFV2_3_0(w[128], w[143], w[2], pars->GC_50, pars->GC_58, amp[353]); 
  FFV1_0(w[128], w[119], w[4], pars->GC_11, amp[354]); 
  FFV1_0(w[121], w[97], w[53], pars->GC_11, amp[355]); 
  FFV2_3_0(w[144], w[135], w[2], pars->GC_50, pars->GC_58, amp[356]); 
  FFV1_0(w[121], w[135], w[4], pars->GC_11, amp[357]); 
  FFV2_3_0(w[147], w[146], w[3], pars->GC_50, pars->GC_58, amp[358]); 
  FFV2_3_0(w[147], w[148], w[2], pars->GC_50, pars->GC_58, amp[359]); 
  FFS4_0(w[98], w[149], w[8], pars->GC_83, amp[360]); 
  FFV1_0(w[98], w[150], w[59], pars->GC_11, amp[361]); 
  FFV2_3_0(w[113], w[149], w[3], pars->GC_50, pars->GC_58, amp[362]); 
  FFV1_0(w[113], w[148], w[59], pars->GC_11, amp[363]); 
  FFV2_3_0(w[110], w[149], w[2], pars->GC_50, pars->GC_58, amp[364]); 
  FFV1_0(w[110], w[146], w[59], pars->GC_11, amp[365]); 
  FFV1_0(w[137], w[146], w[5], pars->GC_11, amp[366]); 
  FFV2_3_0(w[151], w[146], w[3], pars->GC_50, pars->GC_58, amp[367]); 
  FFV1_0(w[136], w[148], w[5], pars->GC_11, amp[368]); 
  FFV2_3_0(w[151], w[148], w[2], pars->GC_50, pars->GC_58, amp[369]); 
  FFV2_3_0(w[136], w[152], w[3], pars->GC_50, pars->GC_58, amp[370]); 
  FFV2_3_0(w[137], w[152], w[2], pars->GC_50, pars->GC_58, amp[371]); 
  FFV1_0(w[134], w[150], w[5], pars->GC_11, amp[372]); 
  FFS4_0(w[134], w[152], w[8], pars->GC_83, amp[373]); 
  FFV1_0(w[100], w[153], w[5], pars->GC_11, amp[374]); 
  FFV1_0(w[100], w[152], w[4], pars->GC_11, amp[375]); 
  FFS4_0(w[105], w[153], w[8], pars->GC_83, amp[376]); 
  FFV1_0(w[105], w[150], w[4], pars->GC_11, amp[377]); 
  FFV1_0(w[114], w[153], w[5], pars->GC_11, amp[378]); 
  FFV2_3_0(w[115], w[153], w[3], pars->GC_50, pars->GC_58, amp[379]); 
  FFV1_0(w[142], w[148], w[5], pars->GC_11, amp[380]); 
  FFV1_0(w[115], w[148], w[4], pars->GC_11, amp[381]); 
  FFV2_3_0(w[142], w[152], w[3], pars->GC_50, pars->GC_58, amp[382]); 
  FFV1_0(w[114], w[152], w[4], pars->GC_11, amp[383]); 
  FFV1_0(w[121], w[153], w[5], pars->GC_11, amp[384]); 
  FFV2_3_0(w[122], w[153], w[2], pars->GC_50, pars->GC_58, amp[385]); 
  FFV1_0(w[144], w[146], w[5], pars->GC_11, amp[386]); 
  FFV1_0(w[122], w[146], w[4], pars->GC_11, amp[387]); 
  FFV2_3_0(w[144], w[152], w[2], pars->GC_50, pars->GC_58, amp[388]); 
  FFV1_0(w[121], w[152], w[4], pars->GC_11, amp[389]); 
  FFV2_3_0(w[125], w[153], w[3], pars->GC_50, pars->GC_58, amp[390]); 
  FFV2_3_0(w[126], w[153], w[2], pars->GC_50, pars->GC_58, amp[391]); 
  FFV2_3_0(w[154], w[146], w[3], pars->GC_50, pars->GC_58, amp[392]); 
  FFV1_0(w[126], w[146], w[4], pars->GC_11, amp[393]); 
  FFV2_3_0(w[154], w[148], w[2], pars->GC_50, pars->GC_58, amp[394]); 
  FFV1_0(w[125], w[148], w[4], pars->GC_11, amp[395]); 
  FFV2_3_0(w[156], w[157], w[3], pars->GC_50, pars->GC_58, amp[396]); 
  FFV2_3_0(w[158], w[157], w[2], pars->GC_50, pars->GC_58, amp[397]); 
  FFS4_0(w[159], w[97], w[8], pars->GC_83, amp[398]); 
  FFV1_0(w[160], w[97], w[59], pars->GC_11, amp[399]); 
  FFV2_3_0(w[159], w[107], w[3], pars->GC_50, pars->GC_58, amp[400]); 
  FFV1_0(w[158], w[107], w[59], pars->GC_11, amp[401]); 
  FFV2_3_0(w[159], w[116], w[2], pars->GC_50, pars->GC_58, amp[402]); 
  FFV1_0(w[156], w[116], w[59], pars->GC_11, amp[403]); 
  FFV1_0(w[156], w[130], w[5], pars->GC_11, amp[404]); 
  FFV2_3_0(w[156], w[161], w[3], pars->GC_50, pars->GC_58, amp[405]); 
  FFV1_0(w[158], w[129], w[5], pars->GC_11, amp[406]); 
  FFV2_3_0(w[158], w[161], w[2], pars->GC_50, pars->GC_58, amp[407]); 
  FFV2_3_0(w[162], w[129], w[3], pars->GC_50, pars->GC_58, amp[408]); 
  FFV2_3_0(w[162], w[130], w[2], pars->GC_50, pars->GC_58, amp[409]); 
  FFV1_0(w[160], w[127], w[5], pars->GC_11, amp[410]); 
  FFS4_0(w[162], w[127], w[8], pars->GC_83, amp[411]); 
  FFV1_0(w[163], w[99], w[5], pars->GC_11, amp[412]); 
  FFV1_0(w[162], w[99], w[4], pars->GC_11, amp[413]); 
  FFS4_0(w[163], w[103], w[8], pars->GC_83, amp[414]); 
  FFV1_0(w[160], w[103], w[4], pars->GC_11, amp[415]); 
  FFV1_0(w[163], w[108], w[5], pars->GC_11, amp[416]); 
  FFV2_3_0(w[163], w[109], w[3], pars->GC_50, pars->GC_58, amp[417]); 
  FFV1_0(w[158], w[141], w[5], pars->GC_11, amp[418]); 
  FFV1_0(w[158], w[109], w[4], pars->GC_11, amp[419]); 
  FFV2_3_0(w[162], w[141], w[3], pars->GC_50, pars->GC_58, amp[420]); 
  FFV1_0(w[162], w[108], w[4], pars->GC_11, amp[421]); 
  FFV1_0(w[163], w[119], w[5], pars->GC_11, amp[422]); 
  FFV2_3_0(w[163], w[120], w[2], pars->GC_50, pars->GC_58, amp[423]); 
  FFV1_0(w[156], w[143], w[5], pars->GC_11, amp[424]); 
  FFV1_0(w[156], w[120], w[4], pars->GC_11, amp[425]); 
  FFV2_3_0(w[162], w[143], w[2], pars->GC_50, pars->GC_58, amp[426]); 
  FFV1_0(w[162], w[119], w[4], pars->GC_11, amp[427]); 
  FFV2_3_0(w[163], w[123], w[3], pars->GC_50, pars->GC_58, amp[428]); 
  FFV2_3_0(w[163], w[124], w[2], pars->GC_50, pars->GC_58, amp[429]); 
  FFV2_3_0(w[156], w[164], w[3], pars->GC_50, pars->GC_58, amp[430]); 
  FFV1_0(w[156], w[124], w[4], pars->GC_11, amp[431]); 
  FFV2_3_0(w[158], w[164], w[2], pars->GC_50, pars->GC_58, amp[432]); 
  FFV1_0(w[158], w[123], w[4], pars->GC_11, amp[433]); 
  FFV1_0(w[98], w[99], w[79], pars->GC_11, amp[434]); 
  FFV1_0(w[100], w[97], w[79], pars->GC_11, amp[435]); 
  FFV1_0(w[100], w[157], w[0], pars->GC_11, amp[436]); 
  FFV1_0(w[147], w[99], w[0], pars->GC_11, amp[437]); 
  FFV1_0(w[98], w[108], w[79], pars->GC_11, amp[438]); 
  FFV2_3_0(w[147], w[165], w[3], pars->GC_50, pars->GC_58, amp[439]); 
  FFV1_0(w[147], w[108], w[0], pars->GC_11, amp[440]); 
  FFV1_0(w[110], w[107], w[79], pars->GC_11, amp[441]); 
  FFV1_0(w[110], w[165], w[59], pars->GC_11, amp[442]); 
  FFV1_0(w[166], w[107], w[59], pars->GC_11, amp[443]); 
  FFV1_0(w[114], w[97], w[79], pars->GC_11, amp[444]); 
  FFV2_3_0(w[167], w[157], w[3], pars->GC_50, pars->GC_58, amp[445]); 
  FFV1_0(w[114], w[157], w[0], pars->GC_11, amp[446]); 
  FFV1_0(w[113], w[116], w[79], pars->GC_11, amp[447]); 
  FFV1_0(w[167], w[116], w[59], pars->GC_11, amp[448]); 
  FFV1_0(w[113], w[168], w[59], pars->GC_11, amp[449]); 
  FFV1_0(w[98], w[119], w[79], pars->GC_11, amp[450]); 
  FFV2_3_0(w[147], w[168], w[2], pars->GC_50, pars->GC_58, amp[451]); 
  FFV1_0(w[147], w[119], w[0], pars->GC_11, amp[452]); 
  FFV1_0(w[121], w[97], w[79], pars->GC_11, amp[453]); 
  FFV2_3_0(w[166], w[157], w[2], pars->GC_50, pars->GC_58, amp[454]); 
  FFV1_0(w[121], w[157], w[0], pars->GC_11, amp[455]); 
  FFV1_0(w[100], w[169], w[5], pars->GC_11, amp[456]); 
  FFV1_0(w[100], w[161], w[0], pars->GC_11, amp[457]); 
  FFS4_0(w[105], w[169], w[8], pars->GC_83, amp[458]); 
  FFS4_0(w[170], w[127], w[8], pars->GC_83, amp[459]); 
  FFV1_0(w[114], w[169], w[5], pars->GC_11, amp[460]); 
  FFV2_3_0(w[115], w[169], w[3], pars->GC_50, pars->GC_58, amp[461]); 
  FFV1_0(w[167], w[130], w[5], pars->GC_11, amp[462]); 
  FFV2_3_0(w[167], w[161], w[3], pars->GC_50, pars->GC_58, amp[463]); 
  FFV1_0(w[115], w[130], w[0], pars->GC_11, amp[464]); 
  FFV1_0(w[114], w[161], w[0], pars->GC_11, amp[465]); 
  FFV1_0(w[121], w[169], w[5], pars->GC_11, amp[466]); 
  FFV2_3_0(w[122], w[169], w[2], pars->GC_50, pars->GC_58, amp[467]); 
  FFV1_0(w[166], w[129], w[5], pars->GC_11, amp[468]); 
  FFV2_3_0(w[166], w[161], w[2], pars->GC_50, pars->GC_58, amp[469]); 
  FFV1_0(w[122], w[129], w[0], pars->GC_11, amp[470]); 
  FFV1_0(w[121], w[161], w[0], pars->GC_11, amp[471]); 
  FFV2_3_0(w[125], w[169], w[3], pars->GC_50, pars->GC_58, amp[472]); 
  FFV2_3_0(w[126], w[169], w[2], pars->GC_50, pars->GC_58, amp[473]); 
  FFV2_3_0(w[170], w[129], w[3], pars->GC_50, pars->GC_58, amp[474]); 
  FFV2_3_0(w[170], w[130], w[2], pars->GC_50, pars->GC_58, amp[475]); 
  FFV1_0(w[126], w[129], w[0], pars->GC_11, amp[476]); 
  FFV1_0(w[125], w[130], w[0], pars->GC_11, amp[477]); 
  FFV1_0(w[171], w[99], w[5], pars->GC_11, amp[478]); 
  FFV1_0(w[151], w[99], w[0], pars->GC_11, amp[479]); 
  FFS4_0(w[171], w[103], w[8], pars->GC_83, amp[480]); 
  FFS4_0(w[134], w[172], w[8], pars->GC_83, amp[481]); 
  FFV1_0(w[171], w[108], w[5], pars->GC_11, amp[482]); 
  FFV2_3_0(w[171], w[109], w[3], pars->GC_50, pars->GC_58, amp[483]); 
  FFV1_0(w[137], w[165], w[5], pars->GC_11, amp[484]); 
  FFV2_3_0(w[151], w[165], w[3], pars->GC_50, pars->GC_58, amp[485]); 
  FFV1_0(w[137], w[109], w[0], pars->GC_11, amp[486]); 
  FFV1_0(w[151], w[108], w[0], pars->GC_11, amp[487]); 
  FFV1_0(w[171], w[119], w[5], pars->GC_11, amp[488]); 
  FFV2_3_0(w[171], w[120], w[2], pars->GC_50, pars->GC_58, amp[489]); 
  FFV1_0(w[136], w[168], w[5], pars->GC_11, amp[490]); 
  FFV2_3_0(w[151], w[168], w[2], pars->GC_50, pars->GC_58, amp[491]); 
  FFV1_0(w[136], w[120], w[0], pars->GC_11, amp[492]); 
  FFV1_0(w[151], w[119], w[0], pars->GC_11, amp[493]); 
  FFV2_3_0(w[171], w[123], w[3], pars->GC_50, pars->GC_58, amp[494]); 
  FFV2_3_0(w[171], w[124], w[2], pars->GC_50, pars->GC_58, amp[495]); 
  FFV2_3_0(w[136], w[172], w[3], pars->GC_50, pars->GC_58, amp[496]); 
  FFV2_3_0(w[137], w[172], w[2], pars->GC_50, pars->GC_58, amp[497]); 
  FFV1_0(w[136], w[124], w[0], pars->GC_11, amp[498]); 
  FFV1_0(w[137], w[123], w[0], pars->GC_11, amp[499]); 
  FFV1_0(w[100], w[172], w[4], pars->GC_11, amp[500]); 
  FFV1_0(w[100], w[164], w[0], pars->GC_11, amp[501]); 
  FFV1_0(w[170], w[99], w[4], pars->GC_11, amp[502]); 
  FFV1_0(w[154], w[99], w[0], pars->GC_11, amp[503]); 
  FFV1_0(w[144], w[165], w[5], pars->GC_11, amp[504]); 
  FFV1_0(w[122], w[165], w[4], pars->GC_11, amp[505]); 
  FFV1_0(w[166], w[141], w[5], pars->GC_11, amp[506]); 
  FFV1_0(w[166], w[109], w[4], pars->GC_11, amp[507]); 
  FFV1_0(w[122], w[141], w[0], pars->GC_11, amp[508]); 
  FFV1_0(w[144], w[109], w[0], pars->GC_11, amp[509]); 
  FFV2_3_0(w[154], w[165], w[3], pars->GC_50, pars->GC_58, amp[510]); 
  FFV1_0(w[126], w[165], w[4], pars->GC_11, amp[511]); 
  FFV2_3_0(w[170], w[141], w[3], pars->GC_50, pars->GC_58, amp[512]); 
  FFV1_0(w[170], w[108], w[4], pars->GC_11, amp[513]); 
  FFV1_0(w[126], w[141], w[0], pars->GC_11, amp[514]); 
  FFV1_0(w[154], w[108], w[0], pars->GC_11, amp[515]); 
  FFV1_0(w[167], w[143], w[5], pars->GC_11, amp[516]); 
  FFV1_0(w[167], w[120], w[4], pars->GC_11, amp[517]); 
  FFV1_0(w[142], w[168], w[5], pars->GC_11, amp[518]); 
  FFV1_0(w[115], w[168], w[4], pars->GC_11, amp[519]); 
  FFV1_0(w[142], w[120], w[0], pars->GC_11, amp[520]); 
  FFV1_0(w[115], w[143], w[0], pars->GC_11, amp[521]); 
  FFV2_3_0(w[167], w[164], w[3], pars->GC_50, pars->GC_58, amp[522]); 
  FFV1_0(w[167], w[124], w[4], pars->GC_11, amp[523]); 
  FFV2_3_0(w[142], w[172], w[3], pars->GC_50, pars->GC_58, amp[524]); 
  FFV1_0(w[114], w[172], w[4], pars->GC_11, amp[525]); 
  FFV1_0(w[142], w[124], w[0], pars->GC_11, amp[526]); 
  FFV1_0(w[114], w[164], w[0], pars->GC_11, amp[527]); 
  FFV2_3_0(w[154], w[168], w[2], pars->GC_50, pars->GC_58, amp[528]); 
  FFV1_0(w[125], w[168], w[4], pars->GC_11, amp[529]); 
  FFV2_3_0(w[170], w[143], w[2], pars->GC_50, pars->GC_58, amp[530]); 
  FFV1_0(w[170], w[119], w[4], pars->GC_11, amp[531]); 
  FFV1_0(w[125], w[143], w[0], pars->GC_11, amp[532]); 
  FFV1_0(w[154], w[119], w[0], pars->GC_11, amp[533]); 
  FFV2_3_0(w[166], w[164], w[2], pars->GC_50, pars->GC_58, amp[534]); 
  FFV1_0(w[166], w[123], w[4], pars->GC_11, amp[535]); 
  FFV2_3_0(w[144], w[172], w[2], pars->GC_50, pars->GC_58, amp[536]); 
  FFV1_0(w[121], w[172], w[4], pars->GC_11, amp[537]); 
  FFV1_0(w[144], w[123], w[0], pars->GC_11, amp[538]); 
  FFV1_0(w[121], w[164], w[0], pars->GC_11, amp[539]); 
  FFS4_0(w[98], w[173], w[8], pars->GC_83, amp[540]); 
  FFS4_0(w[98], w[174], w[8], pars->GC_83, amp[541]); 
  FFS4_0(w[98], w[175], w[8], pars->GC_83, amp[542]); 
  FFS4_0(w[176], w[97], w[8], pars->GC_83, amp[543]); 
  FFS4_0(w[177], w[97], w[8], pars->GC_83, amp[544]); 
  FFS4_0(w[178], w[97], w[8], pars->GC_83, amp[545]); 
  FFV2_3_0(w[176], w[107], w[3], pars->GC_50, pars->GC_58, amp[546]); 
  FFV2_3_0(w[177], w[107], w[3], pars->GC_50, pars->GC_58, amp[547]); 
  FFV2_3_0(w[178], w[107], w[3], pars->GC_50, pars->GC_58, amp[548]); 
  FFV1_0(w[110], w[107], w[88], pars->GC_11, amp[549]); 
  FFV1_0(w[110], w[107], w[89], pars->GC_11, amp[550]); 
  FFV1_0(w[110], w[107], w[90], pars->GC_11, amp[551]); 
  FFV2_3_0(w[113], w[173], w[3], pars->GC_50, pars->GC_58, amp[552]); 
  FFV2_3_0(w[113], w[174], w[3], pars->GC_50, pars->GC_58, amp[553]); 
  FFV2_3_0(w[113], w[175], w[3], pars->GC_50, pars->GC_58, amp[554]); 
  FFV1_0(w[113], w[116], w[88], pars->GC_11, amp[555]); 
  FFV1_0(w[113], w[116], w[89], pars->GC_11, amp[556]); 
  FFV1_0(w[113], w[116], w[90], pars->GC_11, amp[557]); 
  FFV2_3_0(w[176], w[116], w[2], pars->GC_50, pars->GC_58, amp[558]); 
  FFV2_3_0(w[177], w[116], w[2], pars->GC_50, pars->GC_58, amp[559]); 
  FFV2_3_0(w[178], w[116], w[2], pars->GC_50, pars->GC_58, amp[560]); 
  FFV2_3_0(w[110], w[173], w[2], pars->GC_50, pars->GC_58, amp[561]); 
  FFV2_3_0(w[110], w[174], w[2], pars->GC_50, pars->GC_58, amp[562]); 
  FFV2_3_0(w[110], w[175], w[2], pars->GC_50, pars->GC_58, amp[563]); 


}
double PY8MEs_R12_P16_sm_gb_zzggb::matrix_12_gb_zzggb() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 282;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = -amp[0] - amp[1] - Complex<double> (0, 1) * amp[2] -
      Complex<double> (0, 1) * amp[6] - Complex<double> (0, 1) * amp[7] -
      amp[8] - Complex<double> (0, 1) * amp[11] - amp[13] - Complex<double> (0,
      1) * amp[14] - Complex<double> (0, 1) * amp[15] - amp[16] -
      Complex<double> (0, 1) * amp[17] - Complex<double> (0, 1) * amp[18] -
      Complex<double> (0, 1) * amp[20] - amp[21] - amp[24] - Complex<double>
      (0, 1) * amp[27] - Complex<double> (0, 1) * amp[28] - amp[29] -
      Complex<double> (0, 1) * amp[30] - Complex<double> (0, 1) * amp[31] -
      Complex<double> (0, 1) * amp[36] - Complex<double> (0, 1) * amp[37] -
      Complex<double> (0, 1) * amp[76] - Complex<double> (0, 1) * amp[77] -
      Complex<double> (0, 1) * amp[78] - Complex<double> (0, 1) * amp[79] -
      Complex<double> (0, 1) * amp[80] - Complex<double> (0, 1) * amp[81] -
      Complex<double> (0, 1) * amp[82] - Complex<double> (0, 1) * amp[83] +
      amp[92] + amp[94] + amp[95] + amp[96] + amp[97] + amp[99] + amp[102] +
      amp[103] + amp[105] + amp[108] + amp[109] + amp[110] + amp[111] +
      amp[112] + amp[113] - amp[152] - amp[153] - Complex<double> (0, 1) *
      amp[155] - amp[156] - Complex<double> (0, 1) * amp[157] - Complex<double>
      (0, 1) * amp[158] - amp[159] - Complex<double> (0, 1) * amp[160] -
      amp[162] - amp[165] - Complex<double> (0, 1) * amp[167] - amp[168] -
      Complex<double> (0, 1) * amp[169] - Complex<double> (0, 1) * amp[170] -
      amp[171] + amp[221] + amp[223] + amp[228] + amp[229] + amp[233] +
      amp[237] + amp[246] + amp[247] + amp[251] - amp[258] + amp[260] -
      amp[261] + amp[263] - amp[264] + amp[266] + amp[269] - amp[267] +
      amp[272] - amp[270] + amp[275] - amp[273] - amp[276] + amp[278] +
      amp[281] - amp[279];
  jamp[1] = -Complex<double> (0, 1) * amp[46] - Complex<double> (0, 1) *
      amp[47] - Complex<double> (0, 1) * amp[48] - Complex<double> (0, 1) *
      amp[49] - Complex<double> (0, 1) * amp[50] - Complex<double> (0, 1) *
      amp[51] - Complex<double> (0, 1) * amp[52] - Complex<double> (0, 1) *
      amp[53] - amp[54] - amp[55] - Complex<double> (0, 1) * amp[56] - amp[58]
      - amp[61] - Complex<double> (0, 1) * amp[62] - amp[64] - Complex<double>
      (0, 1) * amp[65] - Complex<double> (0, 1) * amp[66] - amp[67] -
      Complex<double> (0, 1) * amp[69] - amp[70] - amp[73] - Complex<double>
      (0, 1) * amp[74] - Complex<double> (0, 1) * amp[75] + Complex<double> (0,
      1) * amp[76] + Complex<double> (0, 1) * amp[77] + Complex<double> (0, 1)
      * amp[78] + Complex<double> (0, 1) * amp[79] + Complex<double> (0, 1) *
      amp[80] + Complex<double> (0, 1) * amp[81] + Complex<double> (0, 1) *
      amp[82] + Complex<double> (0, 1) * amp[83] + amp[84] + amp[85] + amp[86]
      + amp[87] + amp[88] + amp[89] + amp[90] + amp[91] + amp[93] + amp[98] +
      amp[100] + amp[101] + amp[104] + amp[106] + amp[107] + amp[152] +
      amp[153] + Complex<double> (0, 1) * amp[155] + amp[156] + Complex<double>
      (0, 1) * amp[157] + Complex<double> (0, 1) * amp[158] + amp[159] +
      Complex<double> (0, 1) * amp[160] + amp[162] + amp[165] + Complex<double>
      (0, 1) * amp[167] + amp[168] + Complex<double> (0, 1) * amp[169] +
      Complex<double> (0, 1) * amp[170] + amp[171] + amp[197] + amp[202] +
      amp[203] + amp[205] + amp[208] + amp[209] + amp[211] + amp[222] +
      amp[236] + amp[258] + amp[259] + amp[261] + amp[262] + amp[264] +
      amp[265] + amp[267] + amp[268] + amp[270] + amp[271] + amp[273] +
      amp[274] + amp[276] + amp[277] + amp[279] + amp[280];
  jamp[2] = +amp[0] + amp[1] + Complex<double> (0, 1) * amp[2] +
      Complex<double> (0, 1) * amp[6] + Complex<double> (0, 1) * amp[7] +
      amp[8] + Complex<double> (0, 1) * amp[11] + amp[13] + Complex<double> (0,
      1) * amp[14] + Complex<double> (0, 1) * amp[15] + amp[16] +
      Complex<double> (0, 1) * amp[17] + Complex<double> (0, 1) * amp[18] +
      Complex<double> (0, 1) * amp[20] + amp[21] + amp[24] + Complex<double>
      (0, 1) * amp[27] + Complex<double> (0, 1) * amp[28] + amp[29] +
      Complex<double> (0, 1) * amp[30] + Complex<double> (0, 1) * amp[31] +
      Complex<double> (0, 1) * amp[36] + Complex<double> (0, 1) * amp[37] -
      Complex<double> (0, 1) * amp[38] - Complex<double> (0, 1) * amp[39] -
      Complex<double> (0, 1) * amp[40] - Complex<double> (0, 1) * amp[41] -
      Complex<double> (0, 1) * amp[42] - Complex<double> (0, 1) * amp[43] -
      Complex<double> (0, 1) * amp[44] - Complex<double> (0, 1) * amp[45] +
      amp[54] + amp[55] - Complex<double> (0, 1) * amp[57] + amp[58] -
      Complex<double> (0, 1) * amp[59] - Complex<double> (0, 1) * amp[60] +
      amp[61] - Complex<double> (0, 1) * amp[63] + amp[64] + amp[67] -
      Complex<double> (0, 1) * amp[68] + amp[70] - Complex<double> (0, 1) *
      amp[71] - Complex<double> (0, 1) * amp[72] + amp[73] + amp[174] +
      amp[176] + amp[177] + amp[178] + amp[179] + amp[182] + amp[184] +
      amp[185] + amp[188] + amp[190] + amp[191] + amp[192] + amp[193] +
      amp[194] + amp[195] + amp[220] + amp[226] + amp[230] + amp[231] +
      amp[232] + amp[239] + amp[248] + amp[249] + amp[250] - amp[259] -
      amp[260] - amp[262] - amp[263] - amp[265] - amp[266] - amp[269] -
      amp[268] - amp[272] - amp[271] - amp[275] - amp[274] - amp[277] -
      amp[278] - amp[281] - amp[280];
  jamp[3] = +Complex<double> (0, 1) * amp[38] + Complex<double> (0, 1) *
      amp[39] + Complex<double> (0, 1) * amp[40] + Complex<double> (0, 1) *
      amp[41] + Complex<double> (0, 1) * amp[42] + Complex<double> (0, 1) *
      amp[43] + Complex<double> (0, 1) * amp[44] + Complex<double> (0, 1) *
      amp[45] - amp[54] - amp[55] + Complex<double> (0, 1) * amp[57] - amp[58]
      + Complex<double> (0, 1) * amp[59] + Complex<double> (0, 1) * amp[60] -
      amp[61] + Complex<double> (0, 1) * amp[63] - amp[64] - amp[67] +
      Complex<double> (0, 1) * amp[68] - amp[70] + Complex<double> (0, 1) *
      amp[71] + Complex<double> (0, 1) * amp[72] - amp[73] - Complex<double>
      (0, 1) * amp[114] - Complex<double> (0, 1) * amp[115] - Complex<double>
      (0, 1) * amp[116] - Complex<double> (0, 1) * amp[117] - Complex<double>
      (0, 1) * amp[118] - Complex<double> (0, 1) * amp[119] - Complex<double>
      (0, 1) * amp[120] - Complex<double> (0, 1) * amp[121] + amp[122] +
      amp[123] + amp[124] + amp[125] + amp[126] + amp[127] + amp[128] +
      amp[129] + amp[131] + amp[136] + amp[138] + amp[139] + amp[142] +
      amp[144] + amp[145] + amp[152] + amp[153] - Complex<double> (0, 1) *
      amp[154] + amp[156] + amp[159] - Complex<double> (0, 1) * amp[161] +
      amp[162] - Complex<double> (0, 1) * amp[163] - Complex<double> (0, 1) *
      amp[164] + amp[165] - Complex<double> (0, 1) * amp[166] + amp[168] +
      amp[171] - Complex<double> (0, 1) * amp[172] - Complex<double> (0, 1) *
      amp[173] + amp[175] + amp[180] + amp[181] + amp[183] + amp[186] +
      amp[187] + amp[189] + amp[224] + amp[234] + amp[258] + amp[259] +
      amp[261] + amp[262] + amp[264] + amp[265] + amp[267] + amp[268] +
      amp[270] + amp[271] + amp[273] + amp[274] + amp[276] + amp[277] +
      amp[279] + amp[280];
  jamp[4] = +amp[0] + amp[1] - Complex<double> (0, 1) * amp[3] -
      Complex<double> (0, 1) * amp[4] - Complex<double> (0, 1) * amp[5] +
      amp[8] - Complex<double> (0, 1) * amp[9] - Complex<double> (0, 1) *
      amp[10] - Complex<double> (0, 1) * amp[12] + amp[13] + amp[16] -
      Complex<double> (0, 1) * amp[19] + amp[21] - Complex<double> (0, 1) *
      amp[22] - Complex<double> (0, 1) * amp[23] + amp[24] - Complex<double>
      (0, 1) * amp[25] - Complex<double> (0, 1) * amp[26] + amp[29] -
      Complex<double> (0, 1) * amp[32] - Complex<double> (0, 1) * amp[33] -
      Complex<double> (0, 1) * amp[34] - Complex<double> (0, 1) * amp[35] +
      Complex<double> (0, 1) * amp[46] + Complex<double> (0, 1) * amp[47] +
      Complex<double> (0, 1) * amp[48] + Complex<double> (0, 1) * amp[49] +
      Complex<double> (0, 1) * amp[50] + Complex<double> (0, 1) * amp[51] +
      Complex<double> (0, 1) * amp[52] + Complex<double> (0, 1) * amp[53] +
      amp[54] + amp[55] + Complex<double> (0, 1) * amp[56] + amp[58] + amp[61]
      + Complex<double> (0, 1) * amp[62] + amp[64] + Complex<double> (0, 1) *
      amp[65] + Complex<double> (0, 1) * amp[66] + amp[67] + Complex<double>
      (0, 1) * amp[69] + amp[70] + amp[73] + Complex<double> (0, 1) * amp[74] +
      Complex<double> (0, 1) * amp[75] + amp[196] + amp[198] + amp[199] +
      amp[200] + amp[201] + amp[204] + amp[206] + amp[207] + amp[210] +
      amp[212] + amp[213] + amp[214] + amp[215] + amp[216] + amp[217] +
      amp[218] + amp[227] + amp[238] + amp[242] + amp[243] + amp[244] +
      amp[254] + amp[255] + amp[256] - amp[259] - amp[260] - amp[262] -
      amp[263] - amp[265] - amp[266] - amp[269] - amp[268] - amp[272] -
      amp[271] - amp[275] - amp[274] - amp[277] - amp[278] - amp[281] -
      amp[280];
  jamp[5] = -amp[0] - amp[1] + Complex<double> (0, 1) * amp[3] +
      Complex<double> (0, 1) * amp[4] + Complex<double> (0, 1) * amp[5] -
      amp[8] + Complex<double> (0, 1) * amp[9] + Complex<double> (0, 1) *
      amp[10] + Complex<double> (0, 1) * amp[12] - amp[13] - amp[16] +
      Complex<double> (0, 1) * amp[19] - amp[21] + Complex<double> (0, 1) *
      amp[22] + Complex<double> (0, 1) * amp[23] - amp[24] + Complex<double>
      (0, 1) * amp[25] + Complex<double> (0, 1) * amp[26] - amp[29] +
      Complex<double> (0, 1) * amp[32] + Complex<double> (0, 1) * amp[33] +
      Complex<double> (0, 1) * amp[34] + Complex<double> (0, 1) * amp[35] +
      Complex<double> (0, 1) * amp[114] + Complex<double> (0, 1) * amp[115] +
      Complex<double> (0, 1) * amp[116] + Complex<double> (0, 1) * amp[117] +
      Complex<double> (0, 1) * amp[118] + Complex<double> (0, 1) * amp[119] +
      Complex<double> (0, 1) * amp[120] + Complex<double> (0, 1) * amp[121] +
      amp[130] + amp[132] + amp[133] + amp[134] + amp[135] + amp[137] +
      amp[140] + amp[141] + amp[143] + amp[146] + amp[147] + amp[148] +
      amp[149] + amp[150] + amp[151] - amp[152] - amp[153] + Complex<double>
      (0, 1) * amp[154] - amp[156] - amp[159] + Complex<double> (0, 1) *
      amp[161] - amp[162] + Complex<double> (0, 1) * amp[163] + Complex<double>
      (0, 1) * amp[164] - amp[165] + Complex<double> (0, 1) * amp[166] -
      amp[168] - amp[171] + Complex<double> (0, 1) * amp[172] + Complex<double>
      (0, 1) * amp[173] + amp[219] + amp[225] + amp[235] + amp[240] + amp[241]
      + amp[245] + amp[252] + amp[253] + amp[257] - amp[258] + amp[260] -
      amp[261] + amp[263] - amp[264] + amp[266] + amp[269] - amp[267] +
      amp[272] - amp[270] + amp[275] - amp[273] - amp[276] + amp[278] +
      amp[281] - amp[279];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[0][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P16_sm_gb_zzggb::matrix_12_gbx_zzggbx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 282;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = +amp[282] + amp[283] + Complex<double> (0, 1) * amp[284] +
      Complex<double> (0, 1) * amp[288] + Complex<double> (0, 1) * amp[289] +
      amp[290] + Complex<double> (0, 1) * amp[293] + amp[295] + Complex<double>
      (0, 1) * amp[296] + Complex<double> (0, 1) * amp[297] + amp[298] +
      Complex<double> (0, 1) * amp[299] + Complex<double> (0, 1) * amp[300] +
      Complex<double> (0, 1) * amp[302] + amp[303] + amp[306] + Complex<double>
      (0, 1) * amp[309] + Complex<double> (0, 1) * amp[310] + amp[311] +
      Complex<double> (0, 1) * amp[312] + Complex<double> (0, 1) * amp[313] +
      Complex<double> (0, 1) * amp[318] + Complex<double> (0, 1) * amp[319] +
      Complex<double> (0, 1) * amp[358] + Complex<double> (0, 1) * amp[359] +
      Complex<double> (0, 1) * amp[360] + Complex<double> (0, 1) * amp[361] +
      Complex<double> (0, 1) * amp[362] + Complex<double> (0, 1) * amp[363] +
      Complex<double> (0, 1) * amp[364] + Complex<double> (0, 1) * amp[365] -
      amp[374] - amp[376] - amp[377] - amp[378] - amp[379] - amp[381] -
      amp[384] - amp[385] - amp[387] - amp[390] - amp[391] - amp[392] -
      amp[393] - amp[394] - amp[395] + amp[434] + amp[435] + Complex<double>
      (0, 1) * amp[437] + amp[438] + Complex<double> (0, 1) * amp[439] +
      Complex<double> (0, 1) * amp[440] + amp[441] + Complex<double> (0, 1) *
      amp[442] + amp[444] + amp[447] + Complex<double> (0, 1) * amp[449] +
      amp[450] + Complex<double> (0, 1) * amp[451] + Complex<double> (0, 1) *
      amp[452] + amp[453] - amp[503] - amp[505] - amp[510] - amp[511] -
      amp[515] - amp[519] - amp[528] - amp[529] - amp[533] + amp[540] -
      amp[542] + amp[543] - amp[545] + amp[546] - amp[548] - amp[551] +
      amp[549] - amp[554] + amp[552] - amp[557] + amp[555] + amp[558] -
      amp[560] - amp[563] + amp[561];
  jamp[1] = +Complex<double> (0, 1) * amp[328] + Complex<double> (0, 1) *
      amp[329] + Complex<double> (0, 1) * amp[330] + Complex<double> (0, 1) *
      amp[331] + Complex<double> (0, 1) * amp[332] + Complex<double> (0, 1) *
      amp[333] + Complex<double> (0, 1) * amp[334] + Complex<double> (0, 1) *
      amp[335] + amp[336] + amp[337] + Complex<double> (0, 1) * amp[338] +
      amp[340] + amp[343] + Complex<double> (0, 1) * amp[344] + amp[346] +
      Complex<double> (0, 1) * amp[347] + Complex<double> (0, 1) * amp[348] +
      amp[349] + Complex<double> (0, 1) * amp[351] + amp[352] + amp[355] +
      Complex<double> (0, 1) * amp[356] + Complex<double> (0, 1) * amp[357] -
      Complex<double> (0, 1) * amp[358] - Complex<double> (0, 1) * amp[359] -
      Complex<double> (0, 1) * amp[360] - Complex<double> (0, 1) * amp[361] -
      Complex<double> (0, 1) * amp[362] - Complex<double> (0, 1) * amp[363] -
      Complex<double> (0, 1) * amp[364] - Complex<double> (0, 1) * amp[365] -
      amp[366] - amp[367] - amp[368] - amp[369] - amp[370] - amp[371] -
      amp[372] - amp[373] - amp[375] - amp[380] - amp[382] - amp[383] -
      amp[386] - amp[388] - amp[389] - amp[434] - amp[435] - Complex<double>
      (0, 1) * amp[437] - amp[438] - Complex<double> (0, 1) * amp[439] -
      Complex<double> (0, 1) * amp[440] - amp[441] - Complex<double> (0, 1) *
      amp[442] - amp[444] - amp[447] - Complex<double> (0, 1) * amp[449] -
      amp[450] - Complex<double> (0, 1) * amp[451] - Complex<double> (0, 1) *
      amp[452] - amp[453] - amp[479] - amp[484] - amp[485] - amp[487] -
      amp[490] - amp[491] - amp[493] - amp[504] - amp[518] - amp[540] -
      amp[541] - amp[543] - amp[544] - amp[546] - amp[547] - amp[549] -
      amp[550] - amp[552] - amp[553] - amp[555] - amp[556] - amp[558] -
      amp[559] - amp[561] - amp[562];
  jamp[2] = -amp[282] - amp[283] - Complex<double> (0, 1) * amp[284] -
      Complex<double> (0, 1) * amp[288] - Complex<double> (0, 1) * amp[289] -
      amp[290] - Complex<double> (0, 1) * amp[293] - amp[295] - Complex<double>
      (0, 1) * amp[296] - Complex<double> (0, 1) * amp[297] - amp[298] -
      Complex<double> (0, 1) * amp[299] - Complex<double> (0, 1) * amp[300] -
      Complex<double> (0, 1) * amp[302] - amp[303] - amp[306] - Complex<double>
      (0, 1) * amp[309] - Complex<double> (0, 1) * amp[310] - amp[311] -
      Complex<double> (0, 1) * amp[312] - Complex<double> (0, 1) * amp[313] -
      Complex<double> (0, 1) * amp[318] - Complex<double> (0, 1) * amp[319] +
      Complex<double> (0, 1) * amp[320] + Complex<double> (0, 1) * amp[321] +
      Complex<double> (0, 1) * amp[322] + Complex<double> (0, 1) * amp[323] +
      Complex<double> (0, 1) * amp[324] + Complex<double> (0, 1) * amp[325] +
      Complex<double> (0, 1) * amp[326] + Complex<double> (0, 1) * amp[327] -
      amp[336] - amp[337] + Complex<double> (0, 1) * amp[339] - amp[340] +
      Complex<double> (0, 1) * amp[341] + Complex<double> (0, 1) * amp[342] -
      amp[343] + Complex<double> (0, 1) * amp[345] - amp[346] - amp[349] +
      Complex<double> (0, 1) * amp[350] - amp[352] + Complex<double> (0, 1) *
      amp[353] + Complex<double> (0, 1) * amp[354] - amp[355] - amp[456] -
      amp[458] - amp[459] - amp[460] - amp[461] - amp[464] - amp[466] -
      amp[467] - amp[470] - amp[472] - amp[473] - amp[474] - amp[475] -
      amp[476] - amp[477] - amp[502] - amp[508] - amp[512] - amp[513] -
      amp[514] - amp[521] - amp[530] - amp[531] - amp[532] + amp[541] +
      amp[542] + amp[544] + amp[545] + amp[547] + amp[548] + amp[551] +
      amp[550] + amp[554] + amp[553] + amp[557] + amp[556] + amp[559] +
      amp[560] + amp[563] + amp[562];
  jamp[3] = -Complex<double> (0, 1) * amp[320] - Complex<double> (0, 1) *
      amp[321] - Complex<double> (0, 1) * amp[322] - Complex<double> (0, 1) *
      amp[323] - Complex<double> (0, 1) * amp[324] - Complex<double> (0, 1) *
      amp[325] - Complex<double> (0, 1) * amp[326] - Complex<double> (0, 1) *
      amp[327] + amp[336] + amp[337] - Complex<double> (0, 1) * amp[339] +
      amp[340] - Complex<double> (0, 1) * amp[341] - Complex<double> (0, 1) *
      amp[342] + amp[343] - Complex<double> (0, 1) * amp[345] + amp[346] +
      amp[349] - Complex<double> (0, 1) * amp[350] + amp[352] - Complex<double>
      (0, 1) * amp[353] - Complex<double> (0, 1) * amp[354] + amp[355] +
      Complex<double> (0, 1) * amp[396] + Complex<double> (0, 1) * amp[397] +
      Complex<double> (0, 1) * amp[398] + Complex<double> (0, 1) * amp[399] +
      Complex<double> (0, 1) * amp[400] + Complex<double> (0, 1) * amp[401] +
      Complex<double> (0, 1) * amp[402] + Complex<double> (0, 1) * amp[403] -
      amp[404] - amp[405] - amp[406] - amp[407] - amp[408] - amp[409] -
      amp[410] - amp[411] - amp[413] - amp[418] - amp[420] - amp[421] -
      amp[424] - amp[426] - amp[427] - amp[434] - amp[435] + Complex<double>
      (0, 1) * amp[436] - amp[438] - amp[441] + Complex<double> (0, 1) *
      amp[443] - amp[444] + Complex<double> (0, 1) * amp[445] + Complex<double>
      (0, 1) * amp[446] - amp[447] + Complex<double> (0, 1) * amp[448] -
      amp[450] - amp[453] + Complex<double> (0, 1) * amp[454] + Complex<double>
      (0, 1) * amp[455] - amp[457] - amp[462] - amp[463] - amp[465] - amp[468]
      - amp[469] - amp[471] - amp[506] - amp[516] - amp[540] - amp[541] -
      amp[543] - amp[544] - amp[546] - amp[547] - amp[549] - amp[550] -
      amp[552] - amp[553] - amp[555] - amp[556] - amp[558] - amp[559] -
      amp[561] - amp[562];
  jamp[4] = -amp[282] - amp[283] + Complex<double> (0, 1) * amp[285] +
      Complex<double> (0, 1) * amp[286] + Complex<double> (0, 1) * amp[287] -
      amp[290] + Complex<double> (0, 1) * amp[291] + Complex<double> (0, 1) *
      amp[292] + Complex<double> (0, 1) * amp[294] - amp[295] - amp[298] +
      Complex<double> (0, 1) * amp[301] - amp[303] + Complex<double> (0, 1) *
      amp[304] + Complex<double> (0, 1) * amp[305] - amp[306] + Complex<double>
      (0, 1) * amp[307] + Complex<double> (0, 1) * amp[308] - amp[311] +
      Complex<double> (0, 1) * amp[314] + Complex<double> (0, 1) * amp[315] +
      Complex<double> (0, 1) * amp[316] + Complex<double> (0, 1) * amp[317] -
      Complex<double> (0, 1) * amp[328] - Complex<double> (0, 1) * amp[329] -
      Complex<double> (0, 1) * amp[330] - Complex<double> (0, 1) * amp[331] -
      Complex<double> (0, 1) * amp[332] - Complex<double> (0, 1) * amp[333] -
      Complex<double> (0, 1) * amp[334] - Complex<double> (0, 1) * amp[335] -
      amp[336] - amp[337] - Complex<double> (0, 1) * amp[338] - amp[340] -
      amp[343] - Complex<double> (0, 1) * amp[344] - amp[346] - Complex<double>
      (0, 1) * amp[347] - Complex<double> (0, 1) * amp[348] - amp[349] -
      Complex<double> (0, 1) * amp[351] - amp[352] - amp[355] - Complex<double>
      (0, 1) * amp[356] - Complex<double> (0, 1) * amp[357] - amp[478] -
      amp[480] - amp[481] - amp[482] - amp[483] - amp[486] - amp[488] -
      amp[489] - amp[492] - amp[494] - amp[495] - amp[496] - amp[497] -
      amp[498] - amp[499] - amp[500] - amp[509] - amp[520] - amp[524] -
      amp[525] - amp[526] - amp[536] - amp[537] - amp[538] + amp[541] +
      amp[542] + amp[544] + amp[545] + amp[547] + amp[548] + amp[551] +
      amp[550] + amp[554] + amp[553] + amp[557] + amp[556] + amp[559] +
      amp[560] + amp[563] + amp[562];
  jamp[5] = +amp[282] + amp[283] - Complex<double> (0, 1) * amp[285] -
      Complex<double> (0, 1) * amp[286] - Complex<double> (0, 1) * amp[287] +
      amp[290] - Complex<double> (0, 1) * amp[291] - Complex<double> (0, 1) *
      amp[292] - Complex<double> (0, 1) * amp[294] + amp[295] + amp[298] -
      Complex<double> (0, 1) * amp[301] + amp[303] - Complex<double> (0, 1) *
      amp[304] - Complex<double> (0, 1) * amp[305] + amp[306] - Complex<double>
      (0, 1) * amp[307] - Complex<double> (0, 1) * amp[308] + amp[311] -
      Complex<double> (0, 1) * amp[314] - Complex<double> (0, 1) * amp[315] -
      Complex<double> (0, 1) * amp[316] - Complex<double> (0, 1) * amp[317] -
      Complex<double> (0, 1) * amp[396] - Complex<double> (0, 1) * amp[397] -
      Complex<double> (0, 1) * amp[398] - Complex<double> (0, 1) * amp[399] -
      Complex<double> (0, 1) * amp[400] - Complex<double> (0, 1) * amp[401] -
      Complex<double> (0, 1) * amp[402] - Complex<double> (0, 1) * amp[403] -
      amp[412] - amp[414] - amp[415] - amp[416] - amp[417] - amp[419] -
      amp[422] - amp[423] - amp[425] - amp[428] - amp[429] - amp[430] -
      amp[431] - amp[432] - amp[433] + amp[434] + amp[435] - Complex<double>
      (0, 1) * amp[436] + amp[438] + amp[441] - Complex<double> (0, 1) *
      amp[443] + amp[444] - Complex<double> (0, 1) * amp[445] - Complex<double>
      (0, 1) * amp[446] + amp[447] - Complex<double> (0, 1) * amp[448] +
      amp[450] + amp[453] - Complex<double> (0, 1) * amp[454] - Complex<double>
      (0, 1) * amp[455] - amp[501] - amp[507] - amp[517] - amp[522] - amp[523]
      - amp[527] - amp[534] - amp[535] - amp[539] + amp[540] - amp[542] +
      amp[543] - amp[545] + amp[546] - amp[548] - amp[551] + amp[549] -
      amp[554] + amp[552] - amp[557] + amp[555] + amp[558] - amp[560] -
      amp[563] + amp[561];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[1][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}


}  // end namespace PY8MEs_namespace

