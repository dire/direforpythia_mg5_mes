//==========================================================================
// This file has been automatically generated for Pythia 8 by
// MadGraph5_aMC@NLO v. 2.6.0, 2017-08-16
// By the MadGraph5_aMC@NLO Development Team
// Visit launchpad.net/madgraph5 and amcatnlo.web.cern.ch
//==========================================================================

#include "PY8MEs_R16_P50_sm_qq_ttxgqq.h"
#include "HelAmps_sm.h"

using namespace Pythia8_sm; 

namespace PY8MEs_namespace 
{
//==========================================================================
// Class member functions for calculating the matrix elements for
// Process: u u > t t~ g u u WEIGHTED<=5 @16
// Process: c c > t t~ g c c WEIGHTED<=5 @16
// Process: d d > t t~ g d d WEIGHTED<=5 @16
// Process: s s > t t~ g s s WEIGHTED<=5 @16
// Process: u u~ > t t~ g u u~ WEIGHTED<=5 @16
// Process: c c~ > t t~ g c c~ WEIGHTED<=5 @16
// Process: d d~ > t t~ g d d~ WEIGHTED<=5 @16
// Process: s s~ > t t~ g s s~ WEIGHTED<=5 @16
// Process: u~ u~ > t t~ g u~ u~ WEIGHTED<=5 @16
// Process: c~ c~ > t t~ g c~ c~ WEIGHTED<=5 @16
// Process: d~ d~ > t t~ g d~ d~ WEIGHTED<=5 @16
// Process: s~ s~ > t t~ g s~ s~ WEIGHTED<=5 @16
// Process: u c > t t~ g u c WEIGHTED<=5 @16
// Process: u d > t t~ g u d WEIGHTED<=5 @16
// Process: u s > t t~ g u s WEIGHTED<=5 @16
// Process: c d > t t~ g c d WEIGHTED<=5 @16
// Process: c s > t t~ g c s WEIGHTED<=5 @16
// Process: d s > t t~ g d s WEIGHTED<=5 @16
// Process: u u~ > t t~ g c c~ WEIGHTED<=5 @16
// Process: u u~ > t t~ g d d~ WEIGHTED<=5 @16
// Process: u u~ > t t~ g s s~ WEIGHTED<=5 @16
// Process: c c~ > t t~ g u u~ WEIGHTED<=5 @16
// Process: c c~ > t t~ g d d~ WEIGHTED<=5 @16
// Process: c c~ > t t~ g s s~ WEIGHTED<=5 @16
// Process: d d~ > t t~ g u u~ WEIGHTED<=5 @16
// Process: d d~ > t t~ g c c~ WEIGHTED<=5 @16
// Process: d d~ > t t~ g s s~ WEIGHTED<=5 @16
// Process: s s~ > t t~ g u u~ WEIGHTED<=5 @16
// Process: s s~ > t t~ g c c~ WEIGHTED<=5 @16
// Process: s s~ > t t~ g d d~ WEIGHTED<=5 @16
// Process: u c~ > t t~ g u c~ WEIGHTED<=5 @16
// Process: u d~ > t t~ g u d~ WEIGHTED<=5 @16
// Process: u s~ > t t~ g u s~ WEIGHTED<=5 @16
// Process: c u~ > t t~ g c u~ WEIGHTED<=5 @16
// Process: c d~ > t t~ g c d~ WEIGHTED<=5 @16
// Process: c s~ > t t~ g c s~ WEIGHTED<=5 @16
// Process: d u~ > t t~ g d u~ WEIGHTED<=5 @16
// Process: d c~ > t t~ g d c~ WEIGHTED<=5 @16
// Process: d s~ > t t~ g d s~ WEIGHTED<=5 @16
// Process: s u~ > t t~ g s u~ WEIGHTED<=5 @16
// Process: s c~ > t t~ g s c~ WEIGHTED<=5 @16
// Process: s d~ > t t~ g s d~ WEIGHTED<=5 @16
// Process: u~ c~ > t t~ g u~ c~ WEIGHTED<=5 @16
// Process: u~ d~ > t t~ g u~ d~ WEIGHTED<=5 @16
// Process: u~ s~ > t t~ g u~ s~ WEIGHTED<=5 @16
// Process: c~ d~ > t t~ g c~ d~ WEIGHTED<=5 @16
// Process: c~ s~ > t t~ g c~ s~ WEIGHTED<=5 @16
// Process: d~ s~ > t t~ g d~ s~ WEIGHTED<=5 @16

// Exception class
class PY8MEs_R16_P50_sm_qq_ttxgqqException : public exception
{
  virtual const char * what() const throw()
  {
    return "Exception in class 'PY8MEs_R16_P50_sm_qq_ttxgqq'."; 
  }
}
PY8MEs_R16_P50_sm_qq_ttxgqq_exception; 

std::set<int> PY8MEs_R16_P50_sm_qq_ttxgqq::s_channel_proc = std::set<int>
    (createset<int> ());

int PY8MEs_R16_P50_sm_qq_ttxgqq::helicities[ncomb][nexternal] = {{-1, -1, -1,
    -1, -1, -1, -1}, {-1, -1, -1, -1, -1, -1, 1}, {-1, -1, -1, -1, -1, 1, -1},
    {-1, -1, -1, -1, -1, 1, 1}, {-1, -1, -1, -1, 1, -1, -1}, {-1, -1, -1, -1,
    1, -1, 1}, {-1, -1, -1, -1, 1, 1, -1}, {-1, -1, -1, -1, 1, 1, 1}, {-1, -1,
    -1, 1, -1, -1, -1}, {-1, -1, -1, 1, -1, -1, 1}, {-1, -1, -1, 1, -1, 1, -1},
    {-1, -1, -1, 1, -1, 1, 1}, {-1, -1, -1, 1, 1, -1, -1}, {-1, -1, -1, 1, 1,
    -1, 1}, {-1, -1, -1, 1, 1, 1, -1}, {-1, -1, -1, 1, 1, 1, 1}, {-1, -1, 1,
    -1, -1, -1, -1}, {-1, -1, 1, -1, -1, -1, 1}, {-1, -1, 1, -1, -1, 1, -1},
    {-1, -1, 1, -1, -1, 1, 1}, {-1, -1, 1, -1, 1, -1, -1}, {-1, -1, 1, -1, 1,
    -1, 1}, {-1, -1, 1, -1, 1, 1, -1}, {-1, -1, 1, -1, 1, 1, 1}, {-1, -1, 1, 1,
    -1, -1, -1}, {-1, -1, 1, 1, -1, -1, 1}, {-1, -1, 1, 1, -1, 1, -1}, {-1, -1,
    1, 1, -1, 1, 1}, {-1, -1, 1, 1, 1, -1, -1}, {-1, -1, 1, 1, 1, -1, 1}, {-1,
    -1, 1, 1, 1, 1, -1}, {-1, -1, 1, 1, 1, 1, 1}, {-1, 1, -1, -1, -1, -1, -1},
    {-1, 1, -1, -1, -1, -1, 1}, {-1, 1, -1, -1, -1, 1, -1}, {-1, 1, -1, -1, -1,
    1, 1}, {-1, 1, -1, -1, 1, -1, -1}, {-1, 1, -1, -1, 1, -1, 1}, {-1, 1, -1,
    -1, 1, 1, -1}, {-1, 1, -1, -1, 1, 1, 1}, {-1, 1, -1, 1, -1, -1, -1}, {-1,
    1, -1, 1, -1, -1, 1}, {-1, 1, -1, 1, -1, 1, -1}, {-1, 1, -1, 1, -1, 1, 1},
    {-1, 1, -1, 1, 1, -1, -1}, {-1, 1, -1, 1, 1, -1, 1}, {-1, 1, -1, 1, 1, 1,
    -1}, {-1, 1, -1, 1, 1, 1, 1}, {-1, 1, 1, -1, -1, -1, -1}, {-1, 1, 1, -1,
    -1, -1, 1}, {-1, 1, 1, -1, -1, 1, -1}, {-1, 1, 1, -1, -1, 1, 1}, {-1, 1, 1,
    -1, 1, -1, -1}, {-1, 1, 1, -1, 1, -1, 1}, {-1, 1, 1, -1, 1, 1, -1}, {-1, 1,
    1, -1, 1, 1, 1}, {-1, 1, 1, 1, -1, -1, -1}, {-1, 1, 1, 1, -1, -1, 1}, {-1,
    1, 1, 1, -1, 1, -1}, {-1, 1, 1, 1, -1, 1, 1}, {-1, 1, 1, 1, 1, -1, -1},
    {-1, 1, 1, 1, 1, -1, 1}, {-1, 1, 1, 1, 1, 1, -1}, {-1, 1, 1, 1, 1, 1, 1},
    {1, -1, -1, -1, -1, -1, -1}, {1, -1, -1, -1, -1, -1, 1}, {1, -1, -1, -1,
    -1, 1, -1}, {1, -1, -1, -1, -1, 1, 1}, {1, -1, -1, -1, 1, -1, -1}, {1, -1,
    -1, -1, 1, -1, 1}, {1, -1, -1, -1, 1, 1, -1}, {1, -1, -1, -1, 1, 1, 1}, {1,
    -1, -1, 1, -1, -1, -1}, {1, -1, -1, 1, -1, -1, 1}, {1, -1, -1, 1, -1, 1,
    -1}, {1, -1, -1, 1, -1, 1, 1}, {1, -1, -1, 1, 1, -1, -1}, {1, -1, -1, 1, 1,
    -1, 1}, {1, -1, -1, 1, 1, 1, -1}, {1, -1, -1, 1, 1, 1, 1}, {1, -1, 1, -1,
    -1, -1, -1}, {1, -1, 1, -1, -1, -1, 1}, {1, -1, 1, -1, -1, 1, -1}, {1, -1,
    1, -1, -1, 1, 1}, {1, -1, 1, -1, 1, -1, -1}, {1, -1, 1, -1, 1, -1, 1}, {1,
    -1, 1, -1, 1, 1, -1}, {1, -1, 1, -1, 1, 1, 1}, {1, -1, 1, 1, -1, -1, -1},
    {1, -1, 1, 1, -1, -1, 1}, {1, -1, 1, 1, -1, 1, -1}, {1, -1, 1, 1, -1, 1,
    1}, {1, -1, 1, 1, 1, -1, -1}, {1, -1, 1, 1, 1, -1, 1}, {1, -1, 1, 1, 1, 1,
    -1}, {1, -1, 1, 1, 1, 1, 1}, {1, 1, -1, -1, -1, -1, -1}, {1, 1, -1, -1, -1,
    -1, 1}, {1, 1, -1, -1, -1, 1, -1}, {1, 1, -1, -1, -1, 1, 1}, {1, 1, -1, -1,
    1, -1, -1}, {1, 1, -1, -1, 1, -1, 1}, {1, 1, -1, -1, 1, 1, -1}, {1, 1, -1,
    -1, 1, 1, 1}, {1, 1, -1, 1, -1, -1, -1}, {1, 1, -1, 1, -1, -1, 1}, {1, 1,
    -1, 1, -1, 1, -1}, {1, 1, -1, 1, -1, 1, 1}, {1, 1, -1, 1, 1, -1, -1}, {1,
    1, -1, 1, 1, -1, 1}, {1, 1, -1, 1, 1, 1, -1}, {1, 1, -1, 1, 1, 1, 1}, {1,
    1, 1, -1, -1, -1, -1}, {1, 1, 1, -1, -1, -1, 1}, {1, 1, 1, -1, -1, 1, -1},
    {1, 1, 1, -1, -1, 1, 1}, {1, 1, 1, -1, 1, -1, -1}, {1, 1, 1, -1, 1, -1, 1},
    {1, 1, 1, -1, 1, 1, -1}, {1, 1, 1, -1, 1, 1, 1}, {1, 1, 1, 1, -1, -1, -1},
    {1, 1, 1, 1, -1, -1, 1}, {1, 1, 1, 1, -1, 1, -1}, {1, 1, 1, 1, -1, 1, 1},
    {1, 1, 1, 1, 1, -1, -1}, {1, 1, 1, 1, 1, -1, 1}, {1, 1, 1, 1, 1, 1, -1},
    {1, 1, 1, 1, 1, 1, 1}};

// Normalization factors the various processes
// Denominators: spins, colors and identical particles
int PY8MEs_R16_P50_sm_qq_ttxgqq::denom_colors[nprocesses] = {9, 9, 9, 9, 9, 9,
    9};
int PY8MEs_R16_P50_sm_qq_ttxgqq::denom_hels[nprocesses] = {4, 4, 4, 4, 4, 4,
    4};
int PY8MEs_R16_P50_sm_qq_ttxgqq::denom_iden[nprocesses] = {2, 1, 2, 1, 1, 1,
    1};

//--------------------------------------------------------------------------
// Color config initialization
void PY8MEs_R16_P50_sm_qq_ttxgqq::initColorConfigs() 
{
  color_configs = vector < vec_vec_int > (); 
  jamp_nc_relative_power = vector < vec_int > (); 

  // Color flows of process Process: u u > t t~ g u u WEIGHTED<=5 @16
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(0)(4)(0)(1)(0)(0)(3)(4)(2)(2)(0)(3)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #1
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(1)(0)(0)(4)(4)(2)(2)(0)(3)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #2
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(0)(4)(0)(1)(0)(0)(2)(4)(3)(2)(0)(3)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #3
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(0)(2)(0)(1)(0)(0)(4)(4)(3)(2)(0)(3)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #4
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(0)(1)(0)(1)(0)(0)(3)(4)(2)(2)(0)(3)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #5
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(1)(0)(0)(4)(4)(2)(2)(0)(3)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #6
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(0)(1)(0)(1)(0)(0)(2)(4)(3)(2)(0)(3)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #7
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(0)(1)(0)(1)(0)(0)(4)(4)(3)(2)(0)(3)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #8
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(0)(3)(0)(1)(0)(0)(1)(4)(2)(2)(0)(3)(0)));
  jamp_nc_relative_power[0].push_back(-1); 
  // JAMP #9
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(0)(4)(0)(1)(0)(0)(1)(4)(2)(2)(0)(3)(0)));
  jamp_nc_relative_power[0].push_back(-1); 
  // JAMP #10
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(0)(2)(0)(1)(0)(0)(1)(4)(3)(2)(0)(3)(0)));
  jamp_nc_relative_power[0].push_back(-1); 
  // JAMP #11
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(0)(4)(0)(1)(0)(0)(1)(4)(3)(2)(0)(3)(0)));
  jamp_nc_relative_power[0].push_back(-1); 
  // JAMP #12
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(0)(2)(0)(1)(0)(0)(3)(4)(1)(2)(0)(3)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #13
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(0)(3)(0)(1)(0)(0)(2)(4)(1)(2)(0)(3)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #14
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(0)(4)(0)(1)(0)(0)(3)(4)(1)(2)(0)(3)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #15
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(0)(4)(0)(1)(0)(0)(2)(4)(1)(2)(0)(3)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #16
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(1)(0)(0)(4)(4)(1)(2)(0)(3)(0)));
  jamp_nc_relative_power[0].push_back(-1); 
  // JAMP #17
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(1)(0)(0)(4)(4)(1)(2)(0)(3)(0)));
  jamp_nc_relative_power[0].push_back(-1); 

  // Color flows of process Process: u u~ > t t~ g u u~ WEIGHTED<=5 @16
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(2)(0)(0)(2)(4)(3)(3)(0)(0)(4)));
  jamp_nc_relative_power[1].push_back(-1); 
  // JAMP #1
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(2)(0)(0)(4)(4)(3)(3)(0)(0)(2)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #2
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(2)(0)(0)(4)(4)(2)(3)(0)(0)(3)));
  jamp_nc_relative_power[1].push_back(-1); 
  // JAMP #3
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(2)(0)(0)(3)(4)(2)(3)(0)(0)(4)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #4
  color_configs[1].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(2)(0)(0)(1)(4)(3)(3)(0)(0)(4)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #5
  color_configs[1].push_back(vec_int(createvector<int>
      (4)(0)(0)(1)(2)(0)(0)(1)(4)(3)(3)(0)(0)(2)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #6
  color_configs[1].push_back(vec_int(createvector<int>
      (4)(0)(0)(1)(2)(0)(0)(1)(4)(2)(3)(0)(0)(3)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #7
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(2)(0)(0)(1)(4)(2)(3)(0)(0)(4)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #8
  color_configs[1].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(2)(0)(0)(4)(4)(3)(3)(0)(0)(1)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #9
  color_configs[1].push_back(vec_int(createvector<int>
      (4)(0)(0)(1)(2)(0)(0)(2)(4)(3)(3)(0)(0)(1)));
  jamp_nc_relative_power[1].push_back(-1); 
  // JAMP #10
  color_configs[1].push_back(vec_int(createvector<int>
      (4)(0)(0)(1)(2)(0)(0)(3)(4)(2)(3)(0)(0)(1)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #11
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(2)(0)(0)(4)(4)(2)(3)(0)(0)(1)));
  jamp_nc_relative_power[1].push_back(-1); 
  // JAMP #12
  color_configs[1].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(2)(0)(0)(4)(4)(1)(3)(0)(0)(3)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #13
  color_configs[1].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(2)(0)(0)(3)(4)(1)(3)(0)(0)(4)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #14
  color_configs[1].push_back(vec_int(createvector<int>
      (4)(0)(0)(1)(2)(0)(0)(2)(4)(1)(3)(0)(0)(3)));
  jamp_nc_relative_power[1].push_back(-1); 
  // JAMP #15
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(2)(0)(0)(2)(4)(1)(3)(0)(0)(4)));
  jamp_nc_relative_power[1].push_back(-1); 
  // JAMP #16
  color_configs[1].push_back(vec_int(createvector<int>
      (4)(0)(0)(1)(2)(0)(0)(3)(4)(1)(3)(0)(0)(2)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #17
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(2)(0)(0)(4)(4)(1)(3)(0)(0)(2)));
  jamp_nc_relative_power[1].push_back(0); 

  // Color flows of process Process: u~ u~ > t t~ g u~ u~ WEIGHTED<=5 @16
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(1)(4)(3)(0)(2)(0)(4)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #1
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(1)(4)(3)(0)(4)(0)(2)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #2
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(1)(4)(2)(0)(3)(0)(4)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #3
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(1)(4)(2)(0)(4)(0)(3)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #4
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(2)(4)(3)(0)(1)(0)(4)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #5
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(4)(4)(3)(0)(1)(0)(2)));
  jamp_nc_relative_power[2].push_back(-1); 
  // JAMP #6
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(3)(4)(2)(0)(1)(0)(4)));
  jamp_nc_relative_power[2].push_back(-1); 
  // JAMP #7
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(4)(4)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #8
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(2)(4)(3)(0)(4)(0)(1)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #9
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(4)(4)(3)(0)(2)(0)(1)));
  jamp_nc_relative_power[2].push_back(-1); 
  // JAMP #10
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(3)(4)(2)(0)(4)(0)(1)));
  jamp_nc_relative_power[2].push_back(-1); 
  // JAMP #11
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(4)(4)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #12
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(2)(4)(1)(0)(3)(0)(4)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #13
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(2)(4)(1)(0)(4)(0)(3)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #14
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(3)(4)(1)(0)(2)(0)(4)));
  jamp_nc_relative_power[2].push_back(-1); 
  // JAMP #15
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(4)(4)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #16
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(3)(4)(1)(0)(4)(0)(2)));
  jamp_nc_relative_power[2].push_back(-1); 
  // JAMP #17
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(4)(4)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[2].push_back(0); 

  // Color flows of process Process: u c > t t~ g u c WEIGHTED<=5 @16
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[3].push_back(vec_int(createvector<int>
      (1)(0)(4)(0)(1)(0)(0)(3)(4)(2)(2)(0)(3)(0)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #1
  color_configs[3].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(1)(0)(0)(4)(4)(2)(2)(0)(3)(0)));
  jamp_nc_relative_power[3].push_back(-1); 
  // JAMP #2
  color_configs[3].push_back(vec_int(createvector<int>
      (1)(0)(4)(0)(1)(0)(0)(2)(4)(3)(2)(0)(3)(0)));
  jamp_nc_relative_power[3].push_back(-1); 
  // JAMP #3
  color_configs[3].push_back(vec_int(createvector<int>
      (1)(0)(2)(0)(1)(0)(0)(4)(4)(3)(2)(0)(3)(0)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #4
  color_configs[3].push_back(vec_int(createvector<int>
      (4)(0)(1)(0)(1)(0)(0)(3)(4)(2)(2)(0)(3)(0)));
  jamp_nc_relative_power[3].push_back(-1); 
  // JAMP #5
  color_configs[3].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(1)(0)(0)(4)(4)(2)(2)(0)(3)(0)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #6
  color_configs[3].push_back(vec_int(createvector<int>
      (4)(0)(1)(0)(1)(0)(0)(2)(4)(3)(2)(0)(3)(0)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #7
  color_configs[3].push_back(vec_int(createvector<int>
      (2)(0)(1)(0)(1)(0)(0)(4)(4)(3)(2)(0)(3)(0)));
  jamp_nc_relative_power[3].push_back(-1); 
  // JAMP #8
  color_configs[3].push_back(vec_int(createvector<int>
      (4)(0)(3)(0)(1)(0)(0)(1)(4)(2)(2)(0)(3)(0)));
  jamp_nc_relative_power[3].push_back(-2); 
  // JAMP #9
  color_configs[3].push_back(vec_int(createvector<int>
      (3)(0)(4)(0)(1)(0)(0)(1)(4)(2)(2)(0)(3)(0)));
  jamp_nc_relative_power[3].push_back(-1); 
  // JAMP #10
  color_configs[3].push_back(vec_int(createvector<int>
      (4)(0)(2)(0)(1)(0)(0)(1)(4)(3)(2)(0)(3)(0)));
  jamp_nc_relative_power[3].push_back(-1); 
  // JAMP #11
  color_configs[3].push_back(vec_int(createvector<int>
      (2)(0)(4)(0)(1)(0)(0)(1)(4)(3)(2)(0)(3)(0)));
  jamp_nc_relative_power[3].push_back(-2); 
  // JAMP #12
  color_configs[3].push_back(vec_int(createvector<int>
      (4)(0)(2)(0)(1)(0)(0)(3)(4)(1)(2)(0)(3)(0)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #13
  color_configs[3].push_back(vec_int(createvector<int>
      (4)(0)(3)(0)(1)(0)(0)(2)(4)(1)(2)(0)(3)(0)));
  jamp_nc_relative_power[3].push_back(-1); 
  // JAMP #14
  color_configs[3].push_back(vec_int(createvector<int>
      (2)(0)(4)(0)(1)(0)(0)(3)(4)(1)(2)(0)(3)(0)));
  jamp_nc_relative_power[3].push_back(-1); 
  // JAMP #15
  color_configs[3].push_back(vec_int(createvector<int>
      (3)(0)(4)(0)(1)(0)(0)(2)(4)(1)(2)(0)(3)(0)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #16
  color_configs[3].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(1)(0)(0)(4)(4)(1)(2)(0)(3)(0)));
  jamp_nc_relative_power[3].push_back(-2); 
  // JAMP #17
  color_configs[3].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(1)(0)(0)(4)(4)(1)(2)(0)(3)(0)));
  jamp_nc_relative_power[3].push_back(-1); 

  // Color flows of process Process: u u~ > t t~ g c c~ WEIGHTED<=5 @16
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[4].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(2)(0)(0)(2)(4)(3)(3)(0)(0)(4)));
  jamp_nc_relative_power[4].push_back(-2); 
  // JAMP #1
  color_configs[4].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(2)(0)(0)(4)(4)(3)(3)(0)(0)(2)));
  jamp_nc_relative_power[4].push_back(-1); 
  // JAMP #2
  color_configs[4].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(2)(0)(0)(4)(4)(2)(3)(0)(0)(3)));
  jamp_nc_relative_power[4].push_back(-2); 
  // JAMP #3
  color_configs[4].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(2)(0)(0)(3)(4)(2)(3)(0)(0)(4)));
  jamp_nc_relative_power[4].push_back(-1); 
  // JAMP #4
  color_configs[4].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(2)(0)(0)(1)(4)(3)(3)(0)(0)(4)));
  jamp_nc_relative_power[4].push_back(-1); 
  // JAMP #5
  color_configs[4].push_back(vec_int(createvector<int>
      (4)(0)(0)(1)(2)(0)(0)(1)(4)(3)(3)(0)(0)(2)));
  jamp_nc_relative_power[4].push_back(0); 
  // JAMP #6
  color_configs[4].push_back(vec_int(createvector<int>
      (4)(0)(0)(1)(2)(0)(0)(1)(4)(2)(3)(0)(0)(3)));
  jamp_nc_relative_power[4].push_back(-1); 
  // JAMP #7
  color_configs[4].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(2)(0)(0)(1)(4)(2)(3)(0)(0)(4)));
  jamp_nc_relative_power[4].push_back(0); 
  // JAMP #8
  color_configs[4].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(2)(0)(0)(4)(4)(3)(3)(0)(0)(1)));
  jamp_nc_relative_power[4].push_back(0); 
  // JAMP #9
  color_configs[4].push_back(vec_int(createvector<int>
      (4)(0)(0)(1)(2)(0)(0)(2)(4)(3)(3)(0)(0)(1)));
  jamp_nc_relative_power[4].push_back(-1); 
  // JAMP #10
  color_configs[4].push_back(vec_int(createvector<int>
      (4)(0)(0)(1)(2)(0)(0)(3)(4)(2)(3)(0)(0)(1)));
  jamp_nc_relative_power[4].push_back(0); 
  // JAMP #11
  color_configs[4].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(2)(0)(0)(4)(4)(2)(3)(0)(0)(1)));
  jamp_nc_relative_power[4].push_back(-1); 
  // JAMP #12
  color_configs[4].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(2)(0)(0)(4)(4)(1)(3)(0)(0)(3)));
  jamp_nc_relative_power[4].push_back(-1); 
  // JAMP #13
  color_configs[4].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(2)(0)(0)(3)(4)(1)(3)(0)(0)(4)));
  jamp_nc_relative_power[4].push_back(0); 
  // JAMP #14
  color_configs[4].push_back(vec_int(createvector<int>
      (4)(0)(0)(1)(2)(0)(0)(2)(4)(1)(3)(0)(0)(3)));
  jamp_nc_relative_power[4].push_back(-2); 
  // JAMP #15
  color_configs[4].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(2)(0)(0)(2)(4)(1)(3)(0)(0)(4)));
  jamp_nc_relative_power[4].push_back(-1); 
  // JAMP #16
  color_configs[4].push_back(vec_int(createvector<int>
      (4)(0)(0)(1)(2)(0)(0)(3)(4)(1)(3)(0)(0)(2)));
  jamp_nc_relative_power[4].push_back(-1); 
  // JAMP #17
  color_configs[4].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(2)(0)(0)(4)(4)(1)(3)(0)(0)(2)));
  jamp_nc_relative_power[4].push_back(0); 

  // Color flows of process Process: u c~ > t t~ g u c~ WEIGHTED<=5 @16
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[5].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(2)(0)(0)(2)(4)(3)(3)(0)(0)(4)));
  jamp_nc_relative_power[5].push_back(-1); 
  // JAMP #1
  color_configs[5].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(2)(0)(0)(4)(4)(3)(3)(0)(0)(2)));
  jamp_nc_relative_power[5].push_back(0); 
  // JAMP #2
  color_configs[5].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(2)(0)(0)(4)(4)(2)(3)(0)(0)(3)));
  jamp_nc_relative_power[5].push_back(-1); 
  // JAMP #3
  color_configs[5].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(2)(0)(0)(3)(4)(2)(3)(0)(0)(4)));
  jamp_nc_relative_power[5].push_back(0); 
  // JAMP #4
  color_configs[5].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(2)(0)(0)(1)(4)(3)(3)(0)(0)(4)));
  jamp_nc_relative_power[5].push_back(0); 
  // JAMP #5
  color_configs[5].push_back(vec_int(createvector<int>
      (4)(0)(0)(1)(2)(0)(0)(1)(4)(3)(3)(0)(0)(2)));
  jamp_nc_relative_power[5].push_back(-1); 
  // JAMP #6
  color_configs[5].push_back(vec_int(createvector<int>
      (4)(0)(0)(1)(2)(0)(0)(1)(4)(2)(3)(0)(0)(3)));
  jamp_nc_relative_power[5].push_back(0); 
  // JAMP #7
  color_configs[5].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(2)(0)(0)(1)(4)(2)(3)(0)(0)(4)));
  jamp_nc_relative_power[5].push_back(-1); 
  // JAMP #8
  color_configs[5].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(2)(0)(0)(4)(4)(3)(3)(0)(0)(1)));
  jamp_nc_relative_power[5].push_back(-1); 
  // JAMP #9
  color_configs[5].push_back(vec_int(createvector<int>
      (4)(0)(0)(1)(2)(0)(0)(2)(4)(3)(3)(0)(0)(1)));
  jamp_nc_relative_power[5].push_back(-2); 
  // JAMP #10
  color_configs[5].push_back(vec_int(createvector<int>
      (4)(0)(0)(1)(2)(0)(0)(3)(4)(2)(3)(0)(0)(1)));
  jamp_nc_relative_power[5].push_back(-1); 
  // JAMP #11
  color_configs[5].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(2)(0)(0)(4)(4)(2)(3)(0)(0)(1)));
  jamp_nc_relative_power[5].push_back(-2); 
  // JAMP #12
  color_configs[5].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(2)(0)(0)(4)(4)(1)(3)(0)(0)(3)));
  jamp_nc_relative_power[5].push_back(0); 
  // JAMP #13
  color_configs[5].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(2)(0)(0)(3)(4)(1)(3)(0)(0)(4)));
  jamp_nc_relative_power[5].push_back(-1); 
  // JAMP #14
  color_configs[5].push_back(vec_int(createvector<int>
      (4)(0)(0)(1)(2)(0)(0)(2)(4)(1)(3)(0)(0)(3)));
  jamp_nc_relative_power[5].push_back(-1); 
  // JAMP #15
  color_configs[5].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(2)(0)(0)(2)(4)(1)(3)(0)(0)(4)));
  jamp_nc_relative_power[5].push_back(-2); 
  // JAMP #16
  color_configs[5].push_back(vec_int(createvector<int>
      (4)(0)(0)(1)(2)(0)(0)(3)(4)(1)(3)(0)(0)(2)));
  jamp_nc_relative_power[5].push_back(0); 
  // JAMP #17
  color_configs[5].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(2)(0)(0)(4)(4)(1)(3)(0)(0)(2)));
  jamp_nc_relative_power[5].push_back(-1); 

  // Color flows of process Process: u~ c~ > t t~ g u~ c~ WEIGHTED<=5 @16
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[6].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(1)(4)(3)(0)(2)(0)(4)));
  jamp_nc_relative_power[6].push_back(0); 
  // JAMP #1
  color_configs[6].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(1)(4)(3)(0)(4)(0)(2)));
  jamp_nc_relative_power[6].push_back(-1); 
  // JAMP #2
  color_configs[6].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(1)(4)(2)(0)(3)(0)(4)));
  jamp_nc_relative_power[6].push_back(-1); 
  // JAMP #3
  color_configs[6].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(1)(4)(2)(0)(4)(0)(3)));
  jamp_nc_relative_power[6].push_back(0); 
  // JAMP #4
  color_configs[6].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(2)(4)(3)(0)(1)(0)(4)));
  jamp_nc_relative_power[6].push_back(-1); 
  // JAMP #5
  color_configs[6].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(4)(4)(3)(0)(1)(0)(2)));
  jamp_nc_relative_power[6].push_back(-2); 
  // JAMP #6
  color_configs[6].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(3)(4)(2)(0)(1)(0)(4)));
  jamp_nc_relative_power[6].push_back(-2); 
  // JAMP #7
  color_configs[6].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(4)(4)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[6].push_back(-1); 
  // JAMP #8
  color_configs[6].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(2)(4)(3)(0)(4)(0)(1)));
  jamp_nc_relative_power[6].push_back(0); 
  // JAMP #9
  color_configs[6].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(4)(4)(3)(0)(2)(0)(1)));
  jamp_nc_relative_power[6].push_back(-1); 
  // JAMP #10
  color_configs[6].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(3)(4)(2)(0)(4)(0)(1)));
  jamp_nc_relative_power[6].push_back(-1); 
  // JAMP #11
  color_configs[6].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(4)(4)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[6].push_back(0); 
  // JAMP #12
  color_configs[6].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(2)(4)(1)(0)(3)(0)(4)));
  jamp_nc_relative_power[6].push_back(0); 
  // JAMP #13
  color_configs[6].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(2)(4)(1)(0)(4)(0)(3)));
  jamp_nc_relative_power[6].push_back(-1); 
  // JAMP #14
  color_configs[6].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(3)(4)(1)(0)(2)(0)(4)));
  jamp_nc_relative_power[6].push_back(-1); 
  // JAMP #15
  color_configs[6].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(4)(4)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[6].push_back(0); 
  // JAMP #16
  color_configs[6].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(3)(4)(1)(0)(4)(0)(2)));
  jamp_nc_relative_power[6].push_back(-2); 
  // JAMP #17
  color_configs[6].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(3)(0)(0)(4)(4)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[6].push_back(-1); 
}

//--------------------------------------------------------------------------
// Destructor.
PY8MEs_R16_P50_sm_qq_ttxgqq::~PY8MEs_R16_P50_sm_qq_ttxgqq() 
{
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    delete[] p[i]; 
    p[i] = NULL; 
  }
}

//--------------------------------------------------------------------------
// Invert the permutation mapping
vector<int> PY8MEs_R16_P50_sm_qq_ttxgqq::invert_mapping(vector<int> mapping) 
{
  vector<int> inverted_mapping; 
  for (unsigned int i = 0; i < mapping.size(); i++ )
  {
    for (unsigned int j = 0; j < mapping.size(); j++ )
    {
      if (mapping[j] == ((int)i))
      {
        inverted_mapping.push_back(j); 
        break; 
      }
    }
  }
  return inverted_mapping; 
}

//--------------------------------------------------------------------------
// Return the list of possible helicity configurations
vector < vec_int > PY8MEs_R16_P50_sm_qq_ttxgqq::getHelicityConfigs(vector<int>
    permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(ncomb, vector<int> (nexternal, 0)); 
  for (unsigned int ihel = 0; ihel < ncomb; ihel++ )
  {
    for(unsigned int j = 0; j < nexternal; j++ )
    {
      res[ihel][chosenPerm[j]] = helicities[ihel][j]; 
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Return the list of possible color configurations
vector < vec_int > PY8MEs_R16_P50_sm_qq_ttxgqq::getColorConfigs(int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(color_configs[chosenProcID].size(), vector<int>
      (nexternal * 2, 0));
  for (unsigned int icol = 0; icol < color_configs[chosenProcID].size(); icol++
      )
  {
    for(unsigned int j = 0; j < (2 * nexternal); j++ )
    {
      res[icol][chosenPerm[j/2] * 2 + j%2] =
          color_configs[chosenProcID][icol][j];
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Get JAMP relative N_c power
int PY8MEs_R16_P50_sm_qq_ttxgqq::getColorFlowRelativeNCPower(int color_flow_ID,
    int specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return jamp_nc_relative_power[chosenProcID][color_flow_ID]; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity ID -> Helicity Config
vector<int> PY8MEs_R16_P50_sm_qq_ttxgqq::getHelicityConfigForID(int hel_ID,
    vector<int> permutation)
{
  if (hel_ID < 0 || hel_ID >= ncomb)
  {
    cerr <<  "Error in function 'getHelicityConfigForID' of class" << 
    " 'PY8MEs_R16_P50_sm_qq_ttxgqq': Specified helicity ID '" << 
    hel_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R16_P50_sm_qq_ttxgqq_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(nexternal, 0); 
  for (unsigned int j = 0; j < nexternal; j++ )
  {
    res[chosenPerm[j]] = helicities[hel_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity Config -> Helicity ID
int PY8MEs_R16_P50_sm_qq_ttxgqq::getHelicityIDForConfig(vector<int> hel_config,
    vector<int> permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  int user_ihel = -1; 
  if (hel_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < ncomb; i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < nexternal; j++ )
      {
        if (helicities[i][chosenPerm[j]] != hel_config[j])
        {
          found = false; 
          break; 
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_ihel = i; 
        break; 
      }
    }
    if (user_ihel == -1)
    {
      cerr <<  "Error in function 'getHelicityIDForConfig' of class" << 
      " 'PY8MEs_R16_P50_sm_qq_ttxgqq': Specified helicity" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R16_P50_sm_qq_ttxgqq_exception; 
    }
  }
  return user_ihel; 
}


//--------------------------------------------------------------------------
// Implements the map Color ID -> Color Config
vector<int> PY8MEs_R16_P50_sm_qq_ttxgqq::getColorConfigForID(int color_ID, int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < 0 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getColorConfigForID' of class" << 
    " 'PY8MEs_R16_P50_sm_qq_ttxgqq': Specified color ID '" << 
    color_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R16_P50_sm_qq_ttxgqq_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(color_configs[chosenProcID][color_ID].size(), 0); 
  for (unsigned int j = 0; j < (2 * nexternal); j++ )
  {
    res[chosenPerm[j/2] * 2 + j%2] = color_configs[chosenProcID][color_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Color Config -> Color ID
int PY8MEs_R16_P50_sm_qq_ttxgqq::getColorIDForConfig(vector<int> color_config,
    int specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = -1; 
  if (color_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < color_configs[chosenProcID].size(); i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < (nexternal * 2); j++ )
      {

        // If colorless then make sure it matches
        // The little arithmetics in the color index is just
        // the permutation applies on the particle list which is
        // twice smaller since each particle can have two color indices.
        if (color_config[j] == 0)
        {
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] != 0)
          {
            found = false; 
            break; 
          }
          // Otherwise check that the color linked position matches
        }
        else
        {
          int color_linked_pos = -1; 
          // Find the other end of the line in the user color config
          for (unsigned int k = 0; k < (nexternal * 2); k++ )
          {
            if (k == j)
              continue; 
            if (color_config[j] == color_config[k])
            {
              color_linked_pos = k; 
              break; 
            }
          }
          if (color_linked_pos == -1)
          {
            cerr <<  "Error in function 'getColorIDForConfig' of class" << 
            " 'PY8MEs_R16_P50_sm_qq_ttxgqq': A color line could " << 
            " not be closed." << endl; 
            throw PY8MEs_R16_P50_sm_qq_ttxgqq_exception; 
          }
          // Now check whether the color line matches
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] !=
              color_configs[chosenProcID][i][chosenPerm[color_linked_pos/2] * 2
              + color_linked_pos%2])
          {
            found = false; 
            break; 
          }
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_icol = i; 
        break; 
      }
    }

    if (user_icol == -1)
    {
      cerr <<  "Error in function 'getColorIDForConfig' of class" << 
      " 'PY8MEs_R16_P50_sm_qq_ttxgqq': Specified color" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R16_P50_sm_qq_ttxgqq_exception; 
    }
  }
  return user_icol; 
}

//--------------------------------------------------------------------------
// Returns all result previously computed in SigmaKin
vector < vec_double > PY8MEs_R16_P50_sm_qq_ttxgqq::getAllResults(int
    specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return all_results[chosenProcID]; 
}

//--------------------------------------------------------------------------
// Returns a result previously computed in SigmaKin for a specific helicity
// and color ID. -1 means avg and summed over that characteristic.
double PY8MEs_R16_P50_sm_qq_ttxgqq::getResult(int helicity_ID, int color_ID,
    int specify_proc_ID)
{
  if (helicity_ID < - 1 || helicity_ID >= ncomb)
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R16_P50_sm_qq_ttxgqq': Specified helicity ID '" << 
    helicity_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R16_P50_sm_qq_ttxgqq_exception; 
  }
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < - 1 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R16_P50_sm_qq_ttxgqq': Specified color ID '" << 
    color_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R16_P50_sm_qq_ttxgqq_exception; 
  }
  return all_results[chosenProcID][helicity_ID + 1][color_ID + 1]; 
}

//--------------------------------------------------------------------------
// Check for the availability of the requested process and if available,
// If available, this returns the corresponding permutation and Proc_ID to use.
// If not available, this returns a negative Proc_ID.
pair < vector<int> , int > PY8MEs_R16_P50_sm_qq_ttxgqq::static_getPY8ME(vector<int> initial_pdgs, vector<int> final_pdgs, set<int> schannels) 
{

  // Not available return value
  pair < vector<int> , int > NA(vector<int> (), -1); 

  // Check if s-channel requirements match
  if (nreq_s_channels > 0)
  {
    if (schannels != s_channel_proc)
      return NA; 
  }
  else
  {
    if (schannels.size() != 0)
      return NA; 
  }

  // Check number of final state particles
  if (final_pdgs.size() != (nexternal - ninitial))
    return NA; 

  // Check number of initial state particles
  if (initial_pdgs.size() != ninitial)
    return NA; 

  // List of processes available in this class
  const int nprocs = 88; 
  const int proc_IDS[nprocs] = {0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3,
      3, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5,
      5, 5, 6, 6, 6, 6, 6, 6, 1, 1, 1, 1, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4,
      4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 6, 6, 6, 6, 6, 6};
  const int in_pdgs[nprocs][ninitial] = {{2, 2}, {4, 4}, {1, 1}, {3, 3}, {2,
      -2}, {4, -4}, {1, -1}, {3, -3}, {-2, -2}, {-4, -4}, {-1, -1}, {-3, -3},
      {2, 4}, {2, 1}, {2, 3}, {4, 1}, {4, 3}, {1, 3}, {2, -2}, {2, -2}, {2,
      -2}, {4, -4}, {4, -4}, {4, -4}, {1, -1}, {1, -1}, {1, -1}, {3, -3}, {3,
      -3}, {3, -3}, {2, -4}, {2, -1}, {2, -3}, {4, -2}, {4, -1}, {4, -3}, {1,
      -2}, {1, -4}, {1, -3}, {3, -2}, {3, -4}, {3, -1}, {-2, -4}, {-2, -1},
      {-2, -3}, {-4, -1}, {-4, -3}, {-1, -3}, {-2, 2}, {-4, 4}, {-1, 1}, {-3,
      3}, {4, 2}, {1, 2}, {3, 2}, {1, 4}, {3, 4}, {3, 1}, {-2, 2}, {-2, 2},
      {-2, 2}, {-4, 4}, {-4, 4}, {-4, 4}, {-1, 1}, {-1, 1}, {-1, 1}, {-3, 3},
      {-3, 3}, {-3, 3}, {-4, 2}, {-1, 2}, {-3, 2}, {-2, 4}, {-1, 4}, {-3, 4},
      {-2, 1}, {-4, 1}, {-3, 1}, {-2, 3}, {-4, 3}, {-1, 3}, {-4, -2}, {-1, -2},
      {-3, -2}, {-1, -4}, {-3, -4}, {-3, -1}};
  const int out_pdgs[nprocs][nexternal - ninitial] = {{6, -6, 21, 2, 2}, {6,
      -6, 21, 4, 4}, {6, -6, 21, 1, 1}, {6, -6, 21, 3, 3}, {6, -6, 21, 2, -2},
      {6, -6, 21, 4, -4}, {6, -6, 21, 1, -1}, {6, -6, 21, 3, -3}, {6, -6, 21,
      -2, -2}, {6, -6, 21, -4, -4}, {6, -6, 21, -1, -1}, {6, -6, 21, -3, -3},
      {6, -6, 21, 2, 4}, {6, -6, 21, 2, 1}, {6, -6, 21, 2, 3}, {6, -6, 21, 4,
      1}, {6, -6, 21, 4, 3}, {6, -6, 21, 1, 3}, {6, -6, 21, 4, -4}, {6, -6, 21,
      1, -1}, {6, -6, 21, 3, -3}, {6, -6, 21, 2, -2}, {6, -6, 21, 1, -1}, {6,
      -6, 21, 3, -3}, {6, -6, 21, 2, -2}, {6, -6, 21, 4, -4}, {6, -6, 21, 3,
      -3}, {6, -6, 21, 2, -2}, {6, -6, 21, 4, -4}, {6, -6, 21, 1, -1}, {6, -6,
      21, 2, -4}, {6, -6, 21, 2, -1}, {6, -6, 21, 2, -3}, {6, -6, 21, 4, -2},
      {6, -6, 21, 4, -1}, {6, -6, 21, 4, -3}, {6, -6, 21, 1, -2}, {6, -6, 21,
      1, -4}, {6, -6, 21, 1, -3}, {6, -6, 21, 3, -2}, {6, -6, 21, 3, -4}, {6,
      -6, 21, 3, -1}, {6, -6, 21, -2, -4}, {6, -6, 21, -2, -1}, {6, -6, 21, -2,
      -3}, {6, -6, 21, -4, -1}, {6, -6, 21, -4, -3}, {6, -6, 21, -1, -3}, {6,
      -6, 21, 2, -2}, {6, -6, 21, 4, -4}, {6, -6, 21, 1, -1}, {6, -6, 21, 3,
      -3}, {6, -6, 21, 2, 4}, {6, -6, 21, 2, 1}, {6, -6, 21, 2, 3}, {6, -6, 21,
      4, 1}, {6, -6, 21, 4, 3}, {6, -6, 21, 1, 3}, {6, -6, 21, 4, -4}, {6, -6,
      21, 1, -1}, {6, -6, 21, 3, -3}, {6, -6, 21, 2, -2}, {6, -6, 21, 1, -1},
      {6, -6, 21, 3, -3}, {6, -6, 21, 2, -2}, {6, -6, 21, 4, -4}, {6, -6, 21,
      3, -3}, {6, -6, 21, 2, -2}, {6, -6, 21, 4, -4}, {6, -6, 21, 1, -1}, {6,
      -6, 21, 2, -4}, {6, -6, 21, 2, -1}, {6, -6, 21, 2, -3}, {6, -6, 21, 4,
      -2}, {6, -6, 21, 4, -1}, {6, -6, 21, 4, -3}, {6, -6, 21, 1, -2}, {6, -6,
      21, 1, -4}, {6, -6, 21, 1, -3}, {6, -6, 21, 3, -2}, {6, -6, 21, 3, -4},
      {6, -6, 21, 3, -1}, {6, -6, 21, -2, -4}, {6, -6, 21, -2, -1}, {6, -6, 21,
      -2, -3}, {6, -6, 21, -4, -1}, {6, -6, 21, -4, -3}, {6, -6, 21, -1, -3}};

  bool in_pdgs_used[ninitial]; 
  bool out_pdgs_used[nexternal - ninitial]; 
  for(unsigned int i = 0; i < nprocs; i++ )
  {
    int permutations[nexternal]; 

    // Reinitialize initial state look-up variables
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      in_pdgs_used[j] = false; 
      permutations[j] = -1; 
    }
    // Look for initial state matches
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      for(unsigned int k = 0; k < ninitial; k++ )
      {
        // Make sure it has not been used already
        if (in_pdgs_used[k])
          continue; 
        if (initial_pdgs[k] == in_pdgs[i][j])
        {
          permutations[j] = k; 
          in_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[ninitial - 1] == -1)
      continue; 

    // Reinitialize final state look-up variables
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      out_pdgs_used[j] = false; 
      permutations[ninitial + j] = -1; 
    }
    // Look for final state matches
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      for(unsigned int k = 0; k < (nexternal - ninitial); k++ )
      {
        // Make sure it has not been used already
        if (out_pdgs_used[k])
          continue; 
        if (final_pdgs[k] == out_pdgs[i][j])
        {
          permutations[ninitial + j] = ninitial + k; 
          out_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[ninitial + j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[nexternal - 1] == -1)
      continue; 

    // Return process found
    return pair < vector<int> , int > (vector<int> (permutations, permutations
        + nexternal), proc_IDS[i]);
  }

  // No process found
  return NA; 
}

//--------------------------------------------------------------------------
// Set momenta
void PY8MEs_R16_P50_sm_qq_ttxgqq::setMomenta(vector < vec_double >
    momenta_picked)
{
  if (momenta_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setMomenta' of class" << 
    " 'PY8MEs_R16_P50_sm_qq_ttxgqq': Incorrect number of" << 
    " momenta specified." << endl; 
    throw PY8MEs_R16_P50_sm_qq_ttxgqq_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    if (momenta_picked[i].size() != 4)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R16_P50_sm_qq_ttxgqq': Incorrect number of" << 
      " momenta components specified." << endl; 
      throw PY8MEs_R16_P50_sm_qq_ttxgqq_exception; 
    }
    if (momenta_picked[i][0] < 0.0)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R16_P50_sm_qq_ttxgqq': A momentum was specified" << 
      " with negative energy. Check conventions." << endl; 
      throw PY8MEs_R16_P50_sm_qq_ttxgqq_exception; 
    }
    for (unsigned int j = 0; j < 4; j++ )
    {
      p[i][j] = momenta_picked[i][j]; 
    }
  }
}

//--------------------------------------------------------------------------
// Set color configuration to use. An empty vector means sum over all.
void PY8MEs_R16_P50_sm_qq_ttxgqq::setColors(vector<int> colors_picked)
{
  if (colors_picked.size() == 0)
  {
    user_colors = vector<int> (); 
    return; 
  }
  if (colors_picked.size() != (2 * nexternal))
  {
    cerr <<  "Error in function 'setColors' of class" << 
    " 'PY8MEs_R16_P50_sm_qq_ttxgqq': Incorrect number" << 
    " of colors specified." << endl; 
    throw PY8MEs_R16_P50_sm_qq_ttxgqq_exception; 
  }
  user_colors = vector<int> ((2 * nexternal), 0); 
  for(unsigned int i = 0; i < (2 * nexternal); i++ )
  {
    user_colors[i] = colors_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the helicity configuration to use. Am empty vector means sum over all.
void PY8MEs_R16_P50_sm_qq_ttxgqq::setHelicities(vector<int> helicities_picked) 
{
  if (helicities_picked.size() != nexternal)
  {
    if (helicities_picked.size() == 0)
    {
      user_helicities = vector<int> (); 
      return; 
    }
    cerr <<  "Error in function 'setHelicities' of class" << 
    " 'PY8MEs_R16_P50_sm_qq_ttxgqq': Incorrect number" << 
    " of helicities specified." << endl; 
    throw PY8MEs_R16_P50_sm_qq_ttxgqq_exception; 
  }
  user_helicities = vector<int> (nexternal, 0); 
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    user_helicities[i] = helicities_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the permutation to use (will apply to momenta, colors and helicities)
void PY8MEs_R16_P50_sm_qq_ttxgqq::setPermutation(vector<int> perm_picked) 
{
  if (perm_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setPermutations' of class" << 
    " 'PY8MEs_R16_P50_sm_qq_ttxgqq': Incorrect number" << 
    " of permutations specified." << endl; 
    throw PY8MEs_R16_P50_sm_qq_ttxgqq_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    perm[i] = perm_picked[i]; 
  }
}

// Set the proc_ID to use
void PY8MEs_R16_P50_sm_qq_ttxgqq::setProcID(int procID_picked) 
{
  proc_ID = procID_picked; 
}

//--------------------------------------------------------------------------
// Initialize process.

void PY8MEs_R16_P50_sm_qq_ttxgqq::initProc() 
{

  // Initialize flags
  include_symmetry_factors = true; 
  include_helicity_averaging_factors = true; 
  include_color_averaging_factors = true; 

  // Initialize vectors.
  perm = vector<int> (nexternal, 0); 
  user_colors = vector<int> (2 * nexternal, 0); 
  user_helicities = vector<int> (nexternal, 0); 
  p = vector < double * > (); 
  for (unsigned int i = 0; i < nexternal; i++ )
  {
    p.push_back(new double[4]); 
  }
  initColorConfigs(); 
  // Synchronize local variables dependent on the model with the active model.
  mME = vector<double> (nexternal, 0.); 
  syncProcModelParams(); 
  jamp2 = vector < vec_double > (7); 
  jamp2[0] = vector<double> (18, 0.); 
  jamp2[1] = vector<double> (18, 0.); 
  jamp2[2] = vector<double> (18, 0.); 
  jamp2[3] = vector<double> (18, 0.); 
  jamp2[4] = vector<double> (18, 0.); 
  jamp2[5] = vector<double> (18, 0.); 
  jamp2[6] = vector<double> (18, 0.); 
  all_results = vector < vec_vec_double > (7); 
  // The first entry is always the color or helicity avg/summed matrix element.
  all_results[0] = vector < vec_double > (ncomb + 1, vector<double> (18 + 1,
      0.));
  all_results[1] = vector < vec_double > (ncomb + 1, vector<double> (18 + 1,
      0.));
  all_results[2] = vector < vec_double > (ncomb + 1, vector<double> (18 + 1,
      0.));
  all_results[3] = vector < vec_double > (ncomb + 1, vector<double> (18 + 1,
      0.));
  all_results[4] = vector < vec_double > (ncomb + 1, vector<double> (18 + 1,
      0.));
  all_results[5] = vector < vec_double > (ncomb + 1, vector<double> (18 + 1,
      0.));
  all_results[6] = vector < vec_double > (ncomb + 1, vector<double> (18 + 1,
      0.));
}

// Synchronize local variables of the process that depend on the model
// parameters
void PY8MEs_R16_P50_sm_qq_ttxgqq::syncProcModelParams() 
{

  // Instantiate the model class and set parameters that stay fixed during run
  mME[0] = pars->ZERO; 
  mME[1] = pars->ZERO; 
  mME[2] = pars->mdl_MT; 
  mME[3] = pars->mdl_MT; 
  mME[4] = pars->ZERO; 
  mME[5] = pars->ZERO; 
  mME[6] = pars->ZERO; 
}

//--------------------------------------------------------------------------
// Setter allowing to force particular values for the external masses
void PY8MEs_R16_P50_sm_qq_ttxgqq::setMasses(vec_double external_masses) 
{

  if (external_masses.size() != mME.size())
  {
    cerr <<  "Error in function 'setMasses' of class" << 
    " 'PY8MEs_R16_P50_sm_qq_ttxgqq': Incorrect number of" << 
    " masses specified." << endl; 
    throw PY8MEs_R16_P50_sm_qq_ttxgqq_exception; 
  }
  for (unsigned int j = 0; j < mME.size(); j++ )
  {
    mME[j] = external_masses[perm[j]]; 
  }
}

//--------------------------------------------------------------------------
// Getter accessing external masses with the correct ordering
vector<double> PY8MEs_R16_P50_sm_qq_ttxgqq::getMasses() 
{

  vec_double external_masses; 
  vector<int> invertedPerm; 
  invertedPerm = invert_mapping(perm); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    external_masses.push_back(mME[invertedPerm[i]]); 
  }
  return external_masses; 

}


// Set all values of the external masses to float(-mode) where mode can be
// 0 : Mass taken from the model
// 1 : Mass taken from p_i^2 if not massless to begin with
// 2 : Mass always taken from p_i^2.
void PY8MEs_R16_P50_sm_qq_ttxgqq::setExternalMassesMode(int mode) 
{
  if (mode != 0 && mode != 1 && mode != 2)
  {
    cerr <<  "Error in function 'setExternalMassesMode' of class" << 
    " 'PY8MEs_R16_P50_sm_qq_ttxgqq': Incorrect mode selected :" << mode << 
    ". It must be either 0, 1 or 2" << endl; 
    throw PY8MEs_R16_P50_sm_qq_ttxgqq_exception; 
  }
  if (mode == 0)
  {
    syncProcModelParams(); 
  }
  else if (mode == 1)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      if (mME[j] != pars->ZERO)
      {
        mME[j] = -1.0; 
      }
    }
  }
  else if (mode == 2)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      mME[j] = -1.0; 
    }
  }
}

//--------------------------------------------------------------------------
// Evaluate the squared matrix element.

double PY8MEs_R16_P50_sm_qq_ttxgqq::sigmaKin() 
{
  // Set the parameters which change event by event
  pars->setDependentParameters(); 
  pars->setDependentCouplings(); 
  // Reset color flows
  for(int i = 0; i < 18; i++ )
    jamp2[0][i] = 0.; 
  for(int i = 0; i < 18; i++ )
    jamp2[1][i] = 0.; 
  for(int i = 0; i < 18; i++ )
    jamp2[2][i] = 0.; 
  for(int i = 0; i < 18; i++ )
    jamp2[3][i] = 0.; 
  for(int i = 0; i < 18; i++ )
    jamp2[4][i] = 0.; 
  for(int i = 0; i < 18; i++ )
    jamp2[5][i] = 0.; 
  for(int i = 0; i < 18; i++ )
    jamp2[6][i] = 0.; 

  // Save previous values of mME
  vector<double> saved_mME(mME.size(), 0.0); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (mME[i] < 0.0)
    {
      saved_mME[i] = mME[i]; 
      mME[i] = sqrt(abs(pow(p[perm[i]][0], 2) - 
      (pow(p[perm[i]][1], 2) + pow(p[perm[i]][2], 2) + pow(p[perm[i]][3],
          2))));
    }
  }

  // Local variables and constants
  const int max_tries = 10; 
  vector < vec_bool > goodhel(nprocesses, vec_bool(ncomb, false)); 
  vec_int ntry(nprocesses, 0); 
  double t = 0.; 
  double result = 0.; 

  if (ntry[proc_ID] <= max_tries)
    ntry[proc_ID] = ntry[proc_ID] + 1; 

  // Find which helicity configuration is asked for
  // -1 indicates one wants to sum over helicities
  int user_ihel = getHelicityIDForConfig(user_helicities); 

  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = getColorIDForConfig(user_colors); 

  // Reset the list of results that will be recomputed here
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      all_results[proc_ID][ihel + 1][icolor + 1] = 0.; 
    }
  }

  // Calculate the matrix element for all helicities
  // unless already detected as vanishing
  for(int ihel = 0; ihel < ncomb; ihel++ )
  {
    // Skip helicity if already detected as vanishing
    if ((ntry[proc_ID] >= max_tries) && !goodhel[proc_ID][ihel])
      continue; 

    // Also skip helicity if user asks for a specific one
    if ((ntry[proc_ID] >= max_tries) && user_ihel != -1 && user_ihel != ihel)
      continue; 

    calculate_wavefunctions(helicities[ihel]); 

    // Reset locally computed color flows
    for(int i = 0; i < 18; i++ )
      jamp2[0][i] = 0.; 
    for(int i = 0; i < 18; i++ )
      jamp2[1][i] = 0.; 
    for(int i = 0; i < 18; i++ )
      jamp2[2][i] = 0.; 
    for(int i = 0; i < 18; i++ )
      jamp2[3][i] = 0.; 
    for(int i = 0; i < 18; i++ )
      jamp2[4][i] = 0.; 
    for(int i = 0; i < 18; i++ )
      jamp2[5][i] = 0.; 
    for(int i = 0; i < 18; i++ )
      jamp2[6][i] = 0.; 

    if (proc_ID == 0)
      t = matrix_16_uu_ttxguu(); 
    if (proc_ID == 1)
      t = matrix_16_uux_ttxguux(); 
    if (proc_ID == 2)
      t = matrix_16_uxux_ttxguxux(); 
    if (proc_ID == 3)
      t = matrix_16_uc_ttxguc(); 
    if (proc_ID == 4)
      t = matrix_16_uux_ttxgccx(); 
    if (proc_ID == 5)
      t = matrix_16_ucx_ttxgucx(); 
    if (proc_ID == 6)
      t = matrix_16_uxcx_ttxguxcx(); 

    // Store which helicities give non-zero result
    if ((ntry[proc_ID] < max_tries) && t != 0. && !goodhel[proc_ID][ihel])
      goodhel[proc_ID][ihel] = true; 

    // Aggregate results
    if (user_ihel == -1 || user_ihel == ihel)
    {
      if (user_icol == -1)
      {
        result = result + t; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][0] += t; 
          for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
          {
            all_results[proc_ID][0][i + 1] += jamp2[proc_ID][i]; 
          }
        }
        all_results[proc_ID][ihel + 1][0] += t; 
        for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
        {
          all_results[proc_ID][ihel + 1][i + 1] += jamp2[proc_ID][i]; 
        }
      }
      else
      {
        result = result + jamp2[proc_ID][user_icol]; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][user_icol + 1] += jamp2[proc_ID][user_icol]; 
        }
        all_results[proc_ID][ihel + 1][user_icol + 1] +=
            jamp2[proc_ID][user_icol];
      }
    }

  }

  // Normalize results with the identical particle factor
  if (include_symmetry_factors)
  {
    result = result/denom_iden[proc_ID]; 
  }
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      if (include_symmetry_factors)
      {
        all_results[proc_ID][ihel + 1][icolor + 1] /= denom_iden[proc_ID]; 
      }
    }
  }


  // Normalize when when summing+averaging over helicity configurations
  if (user_ihel == -1 && include_helicity_averaging_factors)
  {
    result /= denom_hels[proc_ID]; 
    if (user_icol == -1)
    {
      all_results[proc_ID][0][0] /= denom_hels[proc_ID]; 
      for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
      {
        all_results[proc_ID][0][i + 1] /= denom_hels[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][0][user_icol + 1] /= denom_hels[proc_ID]; 
    }
  }

  // Normalize when summing+averaging over color configurations
  if (user_icol == -1 && include_color_averaging_factors)
  {
    result /= denom_colors[proc_ID]; 
    if (user_ihel == -1)
    {
      all_results[proc_ID][0][0] /= denom_colors[proc_ID]; 
      for (unsigned int i = 0; i < ncomb; i++ )
      {
        all_results[proc_ID][i + 1][0] /= denom_colors[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][user_ihel + 1][0] /= denom_colors[proc_ID]; 
    }
  }

  // Reinstate previous values of mME
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (saved_mME[i] < 0.0)
    {
      mME[i] = saved_mME[i]; 
    }
  }

  // Finally return it
  return result; 
}

//==========================================================================
// Private class member functions

//--------------------------------------------------------------------------
// Evaluate |M|^2 for each subprocess

void PY8MEs_R16_P50_sm_qq_ttxgqq::calculate_wavefunctions(const int hel[])
{
  // Calculate wavefunctions for all processes
  // Calculate all wavefunctions
  ixxxxx(p[perm[0]], mME[0], hel[0], +1, w[0]); 
  ixxxxx(p[perm[1]], mME[1], hel[1], +1, w[1]); 
  oxxxxx(p[perm[2]], mME[2], hel[2], +1, w[2]); 
  ixxxxx(p[perm[3]], mME[3], hel[3], -1, w[3]); 
  vxxxxx(p[perm[4]], mME[4], hel[4], +1, w[4]); 
  oxxxxx(p[perm[5]], mME[5], hel[5], +1, w[5]); 
  oxxxxx(p[perm[6]], mME[6], hel[6], +1, w[6]); 
  FFV1_2(w[0], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[7]); 
  FFV1P0_3(w[3], w[2], pars->GC_11, pars->ZERO, pars->ZERO, w[8]); 
  FFV1P0_3(w[7], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[9]); 
  FFV1_1(w[6], w[8], pars->GC_11, pars->ZERO, pars->ZERO, w[10]); 
  FFV1_2(w[1], w[8], pars->GC_11, pars->ZERO, pars->ZERO, w[11]); 
  FFV1P0_3(w[7], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[12]); 
  FFV1_1(w[5], w[8], pars->GC_11, pars->ZERO, pars->ZERO, w[13]); 
  FFV1P0_3(w[1], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[14]); 
  FFV1_2(w[7], w[8], pars->GC_11, pars->ZERO, pars->ZERO, w[15]); 
  FFV1_2(w[7], w[14], pars->GC_11, pars->ZERO, pars->ZERO, w[16]); 
  FFV1P0_3(w[1], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[17]); 
  FFV1_2(w[7], w[17], pars->GC_11, pars->ZERO, pars->ZERO, w[18]); 
  FFV1_1(w[2], w[14], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[19]); 
  FFV1_2(w[3], w[14], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[20]); 
  FFV1_1(w[2], w[17], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[21]); 
  FFV1_2(w[3], w[17], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[22]); 
  FFV1_1(w[2], w[4], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[23]); 
  FFV1P0_3(w[0], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[24]); 
  FFV1P0_3(w[3], w[23], pars->GC_11, pars->ZERO, pars->ZERO, w[25]); 
  FFV1_1(w[6], w[24], pars->GC_11, pars->ZERO, pars->ZERO, w[26]); 
  FFV1_2(w[1], w[24], pars->GC_11, pars->ZERO, pars->ZERO, w[27]); 
  FFV1_1(w[23], w[24], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[28]); 
  FFV1_1(w[23], w[17], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[29]); 
  FFV1P0_3(w[0], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[30]); 
  FFV1_1(w[5], w[30], pars->GC_11, pars->ZERO, pars->ZERO, w[31]); 
  FFV1_2(w[1], w[30], pars->GC_11, pars->ZERO, pars->ZERO, w[32]); 
  FFV1_1(w[23], w[30], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[33]); 
  FFV1_1(w[23], w[14], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[34]); 
  FFV1_2(w[0], w[14], pars->GC_11, pars->ZERO, pars->ZERO, w[35]); 
  FFV1_1(w[6], w[14], pars->GC_11, pars->ZERO, pars->ZERO, w[36]); 
  FFV1_2(w[0], w[17], pars->GC_11, pars->ZERO, pars->ZERO, w[37]); 
  FFV1_1(w[5], w[17], pars->GC_11, pars->ZERO, pars->ZERO, w[38]); 
  FFV1_2(w[3], w[4], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[39]); 
  FFV1P0_3(w[39], w[2], pars->GC_11, pars->ZERO, pars->ZERO, w[40]); 
  FFV1_2(w[39], w[24], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[41]); 
  FFV1_2(w[39], w[17], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[42]); 
  FFV1_2(w[39], w[30], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[43]); 
  FFV1_2(w[39], w[14], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[44]); 
  FFV1_1(w[5], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[45]); 
  FFV1P0_3(w[1], w[45], pars->GC_11, pars->ZERO, pars->ZERO, w[46]); 
  FFV1_1(w[2], w[30], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[47]); 
  FFV1_2(w[3], w[30], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[48]); 
  FFV1_1(w[45], w[30], pars->GC_11, pars->ZERO, pars->ZERO, w[49]); 
  FFV1_1(w[45], w[8], pars->GC_11, pars->ZERO, pars->ZERO, w[50]); 
  FFV1P0_3(w[0], w[45], pars->GC_11, pars->ZERO, pars->ZERO, w[51]); 
  FFV1_2(w[0], w[8], pars->GC_11, pars->ZERO, pars->ZERO, w[52]); 
  FFV1_1(w[45], w[17], pars->GC_11, pars->ZERO, pars->ZERO, w[53]); 
  FFV1_1(w[6], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[54]); 
  FFV1P0_3(w[1], w[54], pars->GC_11, pars->ZERO, pars->ZERO, w[55]); 
  FFV1_1(w[2], w[24], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[56]); 
  FFV1_2(w[3], w[24], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[57]); 
  FFV1_1(w[54], w[24], pars->GC_11, pars->ZERO, pars->ZERO, w[58]); 
  FFV1_1(w[54], w[8], pars->GC_11, pars->ZERO, pars->ZERO, w[59]); 
  FFV1P0_3(w[0], w[54], pars->GC_11, pars->ZERO, pars->ZERO, w[60]); 
  FFV1_1(w[54], w[14], pars->GC_11, pars->ZERO, pars->ZERO, w[61]); 
  FFV1_2(w[1], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[62]); 
  FFV1P0_3(w[62], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[63]); 
  FFV1_2(w[62], w[24], pars->GC_11, pars->ZERO, pars->ZERO, w[64]); 
  FFV1_2(w[62], w[8], pars->GC_11, pars->ZERO, pars->ZERO, w[65]); 
  FFV1P0_3(w[62], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[66]); 
  FFV1_2(w[62], w[30], pars->GC_11, pars->ZERO, pars->ZERO, w[67]); 
  VVV1P0_1(w[4], w[24], pars->GC_10, pars->ZERO, pars->ZERO, w[68]); 
  VVV1P0_1(w[4], w[8], pars->GC_10, pars->ZERO, pars->ZERO, w[69]); 
  VVV1P0_1(w[4], w[17], pars->GC_10, pars->ZERO, pars->ZERO, w[70]); 
  VVV1P0_1(w[4], w[30], pars->GC_10, pars->ZERO, pars->ZERO, w[71]); 
  VVV1P0_1(w[4], w[14], pars->GC_10, pars->ZERO, pars->ZERO, w[72]); 
  oxxxxx(p[perm[1]], mME[1], hel[1], -1, w[73]); 
  ixxxxx(p[perm[6]], mME[6], hel[6], -1, w[74]); 
  FFV1P0_3(w[7], w[73], pars->GC_11, pars->ZERO, pars->ZERO, w[75]); 
  FFV1_2(w[74], w[8], pars->GC_11, pars->ZERO, pars->ZERO, w[76]); 
  FFV1_1(w[73], w[8], pars->GC_11, pars->ZERO, pars->ZERO, w[77]); 
  FFV1P0_3(w[74], w[73], pars->GC_11, pars->ZERO, pars->ZERO, w[78]); 
  FFV1_2(w[7], w[78], pars->GC_11, pars->ZERO, pars->ZERO, w[79]); 
  FFV1P0_3(w[74], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[80]); 
  FFV1_2(w[7], w[80], pars->GC_11, pars->ZERO, pars->ZERO, w[81]); 
  FFV1_1(w[2], w[78], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[82]); 
  FFV1_2(w[3], w[78], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[83]); 
  FFV1_1(w[2], w[80], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[84]); 
  FFV1_2(w[3], w[80], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[85]); 
  FFV1P0_3(w[0], w[73], pars->GC_11, pars->ZERO, pars->ZERO, w[86]); 
  FFV1_1(w[5], w[86], pars->GC_11, pars->ZERO, pars->ZERO, w[87]); 
  FFV1_2(w[74], w[86], pars->GC_11, pars->ZERO, pars->ZERO, w[88]); 
  FFV1_1(w[23], w[86], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[89]); 
  FFV1_1(w[23], w[80], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[90]); 
  FFV1_1(w[73], w[24], pars->GC_11, pars->ZERO, pars->ZERO, w[91]); 
  FFV1_2(w[74], w[24], pars->GC_11, pars->ZERO, pars->ZERO, w[92]); 
  FFV1_1(w[23], w[78], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[93]); 
  FFV1_2(w[0], w[78], pars->GC_11, pars->ZERO, pars->ZERO, w[94]); 
  FFV1_1(w[5], w[78], pars->GC_11, pars->ZERO, pars->ZERO, w[95]); 
  FFV1_2(w[0], w[80], pars->GC_11, pars->ZERO, pars->ZERO, w[96]); 
  FFV1_1(w[73], w[80], pars->GC_11, pars->ZERO, pars->ZERO, w[97]); 
  FFV1_2(w[39], w[86], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[98]); 
  FFV1_2(w[39], w[80], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[99]); 
  FFV1_2(w[39], w[78], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[100]); 
  FFV1_1(w[73], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[101]); 
  FFV1P0_3(w[74], w[101], pars->GC_11, pars->ZERO, pars->ZERO, w[102]); 
  FFV1_1(w[101], w[24], pars->GC_11, pars->ZERO, pars->ZERO, w[103]); 
  FFV1_1(w[101], w[8], pars->GC_11, pars->ZERO, pars->ZERO, w[104]); 
  FFV1P0_3(w[0], w[101], pars->GC_11, pars->ZERO, pars->ZERO, w[105]); 
  FFV1_1(w[101], w[80], pars->GC_11, pars->ZERO, pars->ZERO, w[106]); 
  FFV1P0_3(w[74], w[45], pars->GC_11, pars->ZERO, pars->ZERO, w[107]); 
  FFV1_1(w[2], w[86], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[108]); 
  FFV1_2(w[3], w[86], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[109]); 
  FFV1_1(w[45], w[86], pars->GC_11, pars->ZERO, pars->ZERO, w[110]); 
  FFV1_1(w[45], w[78], pars->GC_11, pars->ZERO, pars->ZERO, w[111]); 
  FFV1_2(w[74], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[112]); 
  FFV1P0_3(w[112], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[113]); 
  FFV1_2(w[112], w[86], pars->GC_11, pars->ZERO, pars->ZERO, w[114]); 
  FFV1_2(w[112], w[8], pars->GC_11, pars->ZERO, pars->ZERO, w[115]); 
  FFV1P0_3(w[112], w[73], pars->GC_11, pars->ZERO, pars->ZERO, w[116]); 
  FFV1_2(w[112], w[24], pars->GC_11, pars->ZERO, pars->ZERO, w[117]); 
  VVV1P0_1(w[4], w[86], pars->GC_10, pars->ZERO, pars->ZERO, w[118]); 
  VVV1P0_1(w[4], w[80], pars->GC_10, pars->ZERO, pars->ZERO, w[119]); 
  VVV1P0_1(w[4], w[78], pars->GC_10, pars->ZERO, pars->ZERO, w[120]); 
  oxxxxx(p[perm[0]], mME[0], hel[0], -1, w[121]); 
  ixxxxx(p[perm[5]], mME[5], hel[5], -1, w[122]); 
  FFV1_2(w[122], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[123]); 
  FFV1P0_3(w[123], w[121], pars->GC_11, pars->ZERO, pars->ZERO, w[124]); 
  FFV1P0_3(w[123], w[73], pars->GC_11, pars->ZERO, pars->ZERO, w[125]); 
  FFV1_1(w[121], w[8], pars->GC_11, pars->ZERO, pars->ZERO, w[126]); 
  FFV1P0_3(w[74], w[121], pars->GC_11, pars->ZERO, pars->ZERO, w[127]); 
  FFV1_2(w[123], w[8], pars->GC_11, pars->ZERO, pars->ZERO, w[128]); 
  FFV1_2(w[123], w[127], pars->GC_11, pars->ZERO, pars->ZERO, w[129]); 
  FFV1_2(w[123], w[78], pars->GC_11, pars->ZERO, pars->ZERO, w[130]); 
  FFV1_1(w[2], w[127], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[131]); 
  FFV1_2(w[3], w[127], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[132]); 
  FFV1P0_3(w[122], w[121], pars->GC_11, pars->ZERO, pars->ZERO, w[133]); 
  FFV1_1(w[73], w[133], pars->GC_11, pars->ZERO, pars->ZERO, w[134]); 
  FFV1_2(w[74], w[133], pars->GC_11, pars->ZERO, pars->ZERO, w[135]); 
  FFV1_1(w[23], w[133], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[136]); 
  FFV1P0_3(w[122], w[73], pars->GC_11, pars->ZERO, pars->ZERO, w[137]); 
  FFV1_1(w[121], w[137], pars->GC_11, pars->ZERO, pars->ZERO, w[138]); 
  FFV1_2(w[74], w[137], pars->GC_11, pars->ZERO, pars->ZERO, w[139]); 
  FFV1_1(w[23], w[137], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[140]); 
  FFV1_1(w[23], w[127], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[141]); 
  FFV1_2(w[122], w[127], pars->GC_11, pars->ZERO, pars->ZERO, w[142]); 
  FFV1_1(w[73], w[127], pars->GC_11, pars->ZERO, pars->ZERO, w[143]); 
  FFV1_2(w[122], w[78], pars->GC_11, pars->ZERO, pars->ZERO, w[144]); 
  FFV1_1(w[121], w[78], pars->GC_11, pars->ZERO, pars->ZERO, w[145]); 
  FFV1_2(w[39], w[133], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[146]); 
  FFV1_2(w[39], w[137], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[147]); 
  FFV1_2(w[39], w[127], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[148]); 
  FFV1_1(w[121], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[149]); 
  FFV1P0_3(w[74], w[149], pars->GC_11, pars->ZERO, pars->ZERO, w[150]); 
  FFV1_1(w[2], w[137], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[151]); 
  FFV1_2(w[3], w[137], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[152]); 
  FFV1_1(w[149], w[137], pars->GC_11, pars->ZERO, pars->ZERO, w[153]); 
  FFV1_1(w[149], w[8], pars->GC_11, pars->ZERO, pars->ZERO, w[154]); 
  FFV1P0_3(w[122], w[149], pars->GC_11, pars->ZERO, pars->ZERO, w[155]); 
  FFV1_2(w[122], w[8], pars->GC_11, pars->ZERO, pars->ZERO, w[156]); 
  FFV1_1(w[149], w[78], pars->GC_11, pars->ZERO, pars->ZERO, w[157]); 
  FFV1_1(w[2], w[133], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[158]); 
  FFV1_2(w[3], w[133], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[159]); 
  FFV1_1(w[101], w[133], pars->GC_11, pars->ZERO, pars->ZERO, w[160]); 
  FFV1P0_3(w[122], w[101], pars->GC_11, pars->ZERO, pars->ZERO, w[161]); 
  FFV1_1(w[101], w[127], pars->GC_11, pars->ZERO, pars->ZERO, w[162]); 
  FFV1_2(w[112], w[133], pars->GC_11, pars->ZERO, pars->ZERO, w[163]); 
  FFV1P0_3(w[112], w[121], pars->GC_11, pars->ZERO, pars->ZERO, w[164]); 
  FFV1_2(w[112], w[137], pars->GC_11, pars->ZERO, pars->ZERO, w[165]); 
  VVV1P0_1(w[4], w[133], pars->GC_10, pars->ZERO, pars->ZERO, w[166]); 
  VVV1P0_1(w[4], w[137], pars->GC_10, pars->ZERO, pars->ZERO, w[167]); 
  VVV1P0_1(w[4], w[127], pars->GC_10, pars->ZERO, pars->ZERO, w[168]); 

  // Calculate all amplitudes
  // Amplitude(s) for diagram number 0
  FFV1_0(w[1], w[10], w[9], pars->GC_11, amp[0]); 
  FFV1_0(w[11], w[6], w[9], pars->GC_11, amp[1]); 
  FFV1_0(w[1], w[13], w[12], pars->GC_11, amp[2]); 
  FFV1_0(w[11], w[5], w[12], pars->GC_11, amp[3]); 
  FFV1_0(w[15], w[6], w[14], pars->GC_11, amp[4]); 
  FFV1_0(w[16], w[6], w[8], pars->GC_11, amp[5]); 
  VVV1_0(w[8], w[14], w[12], pars->GC_10, amp[6]); 
  FFV1_0(w[15], w[5], w[17], pars->GC_11, amp[7]); 
  VVV1_0(w[8], w[17], w[9], pars->GC_10, amp[8]); 
  FFV1_0(w[18], w[5], w[8], pars->GC_11, amp[9]); 
  FFV1_0(w[3], w[19], w[12], pars->GC_11, amp[10]); 
  FFV1_0(w[20], w[2], w[12], pars->GC_11, amp[11]); 
  FFV1_0(w[3], w[21], w[9], pars->GC_11, amp[12]); 
  FFV1_0(w[22], w[2], w[9], pars->GC_11, amp[13]); 
  FFV1_0(w[1], w[26], w[25], pars->GC_11, amp[14]); 
  FFV1_0(w[27], w[6], w[25], pars->GC_11, amp[15]); 
  FFV1_0(w[3], w[28], w[17], pars->GC_11, amp[16]); 
  VVV1_0(w[24], w[17], w[25], pars->GC_10, amp[17]); 
  FFV1_0(w[3], w[29], w[24], pars->GC_11, amp[18]); 
  FFV1_0(w[1], w[31], w[25], pars->GC_11, amp[19]); 
  FFV1_0(w[32], w[5], w[25], pars->GC_11, amp[20]); 
  FFV1_0(w[3], w[33], w[14], pars->GC_11, amp[21]); 
  VVV1_0(w[30], w[14], w[25], pars->GC_10, amp[22]); 
  FFV1_0(w[3], w[34], w[30], pars->GC_11, amp[23]); 
  FFV1_0(w[35], w[6], w[25], pars->GC_11, amp[24]); 
  FFV1_0(w[0], w[36], w[25], pars->GC_11, amp[25]); 
  FFV1_0(w[37], w[5], w[25], pars->GC_11, amp[26]); 
  FFV1_0(w[0], w[38], w[25], pars->GC_11, amp[27]); 
  FFV1_0(w[1], w[26], w[40], pars->GC_11, amp[28]); 
  FFV1_0(w[27], w[6], w[40], pars->GC_11, amp[29]); 
  FFV1_0(w[41], w[2], w[17], pars->GC_11, amp[30]); 
  VVV1_0(w[24], w[17], w[40], pars->GC_10, amp[31]); 
  FFV1_0(w[42], w[2], w[24], pars->GC_11, amp[32]); 
  FFV1_0(w[1], w[31], w[40], pars->GC_11, amp[33]); 
  FFV1_0(w[32], w[5], w[40], pars->GC_11, amp[34]); 
  FFV1_0(w[43], w[2], w[14], pars->GC_11, amp[35]); 
  VVV1_0(w[30], w[14], w[40], pars->GC_10, amp[36]); 
  FFV1_0(w[44], w[2], w[30], pars->GC_11, amp[37]); 
  FFV1_0(w[35], w[6], w[40], pars->GC_11, amp[38]); 
  FFV1_0(w[0], w[36], w[40], pars->GC_11, amp[39]); 
  FFV1_0(w[37], w[5], w[40], pars->GC_11, amp[40]); 
  FFV1_0(w[0], w[38], w[40], pars->GC_11, amp[41]); 
  FFV1_0(w[3], w[47], w[46], pars->GC_11, amp[42]); 
  FFV1_0(w[48], w[2], w[46], pars->GC_11, amp[43]); 
  FFV1_0(w[1], w[49], w[8], pars->GC_11, amp[44]); 
  FFV1_0(w[1], w[50], w[30], pars->GC_11, amp[45]); 
  VVV1_0(w[30], w[8], w[46], pars->GC_10, amp[46]); 
  FFV1_0(w[1], w[10], w[51], pars->GC_11, amp[47]); 
  FFV1_0(w[11], w[6], w[51], pars->GC_11, amp[48]); 
  FFV1_0(w[52], w[6], w[46], pars->GC_11, amp[49]); 
  FFV1_0(w[0], w[10], w[46], pars->GC_11, amp[50]); 
  VVV1_0(w[8], w[17], w[51], pars->GC_10, amp[51]); 
  FFV1_0(w[0], w[50], w[17], pars->GC_11, amp[52]); 
  FFV1_0(w[0], w[53], w[8], pars->GC_11, amp[53]); 
  FFV1_0(w[3], w[21], w[51], pars->GC_11, amp[54]); 
  FFV1_0(w[22], w[2], w[51], pars->GC_11, amp[55]); 
  FFV1_0(w[3], w[56], w[55], pars->GC_11, amp[56]); 
  FFV1_0(w[57], w[2], w[55], pars->GC_11, amp[57]); 
  FFV1_0(w[1], w[58], w[8], pars->GC_11, amp[58]); 
  FFV1_0(w[1], w[59], w[24], pars->GC_11, amp[59]); 
  VVV1_0(w[24], w[8], w[55], pars->GC_10, amp[60]); 
  FFV1_0(w[1], w[13], w[60], pars->GC_11, amp[61]); 
  FFV1_0(w[11], w[5], w[60], pars->GC_11, amp[62]); 
  FFV1_0(w[52], w[5], w[55], pars->GC_11, amp[63]); 
  FFV1_0(w[0], w[13], w[55], pars->GC_11, amp[64]); 
  VVV1_0(w[8], w[14], w[60], pars->GC_10, amp[65]); 
  FFV1_0(w[0], w[59], w[14], pars->GC_11, amp[66]); 
  FFV1_0(w[0], w[61], w[8], pars->GC_11, amp[67]); 
  FFV1_0(w[3], w[19], w[60], pars->GC_11, amp[68]); 
  FFV1_0(w[20], w[2], w[60], pars->GC_11, amp[69]); 
  FFV1_0(w[3], w[56], w[63], pars->GC_11, amp[70]); 
  FFV1_0(w[57], w[2], w[63], pars->GC_11, amp[71]); 
  FFV1_0(w[64], w[6], w[8], pars->GC_11, amp[72]); 
  FFV1_0(w[65], w[6], w[24], pars->GC_11, amp[73]); 
  VVV1_0(w[24], w[8], w[63], pars->GC_10, amp[74]); 
  FFV1_0(w[3], w[47], w[66], pars->GC_11, amp[75]); 
  FFV1_0(w[48], w[2], w[66], pars->GC_11, amp[76]); 
  FFV1_0(w[67], w[5], w[8], pars->GC_11, amp[77]); 
  FFV1_0(w[65], w[5], w[30], pars->GC_11, amp[78]); 
  VVV1_0(w[30], w[8], w[66], pars->GC_10, amp[79]); 
  FFV1_0(w[52], w[6], w[66], pars->GC_11, amp[80]); 
  FFV1_0(w[0], w[10], w[66], pars->GC_11, amp[81]); 
  FFV1_0(w[52], w[5], w[63], pars->GC_11, amp[82]); 
  FFV1_0(w[0], w[13], w[63], pars->GC_11, amp[83]); 
  FFV1_0(w[1], w[10], w[68], pars->GC_11, amp[84]); 
  FFV1_0(w[11], w[6], w[68], pars->GC_11, amp[85]); 
  FFV1_0(w[1], w[26], w[69], pars->GC_11, amp[86]); 
  FFV1_0(w[27], w[6], w[69], pars->GC_11, amp[87]); 
  FFV1_0(w[11], w[26], w[4], pars->GC_11, amp[88]); 
  FFV1_0(w[27], w[10], w[4], pars->GC_11, amp[89]); 
  VVVV1_0(w[4], w[24], w[8], w[17], pars->GC_12, amp[90]); 
  VVVV3_0(w[4], w[24], w[8], w[17], pars->GC_12, amp[91]); 
  VVVV4_0(w[4], w[24], w[8], w[17], pars->GC_12, amp[92]); 
  VVV1_0(w[8], w[17], w[68], pars->GC_10, amp[93]); 
  VVV1_0(w[24], w[17], w[69], pars->GC_10, amp[94]); 
  VVV1_0(w[24], w[8], w[70], pars->GC_10, amp[95]); 
  FFV1_0(w[3], w[21], w[68], pars->GC_11, amp[96]); 
  FFV1_0(w[22], w[2], w[68], pars->GC_11, amp[97]); 
  FFV1_0(w[3], w[56], w[70], pars->GC_11, amp[98]); 
  FFV1_0(w[57], w[2], w[70], pars->GC_11, amp[99]); 
  FFV1_0(w[22], w[56], w[4], pars->GC_11, amp[100]); 
  FFV1_0(w[57], w[21], w[4], pars->GC_11, amp[101]); 
  FFV1_0(w[1], w[13], w[71], pars->GC_11, amp[102]); 
  FFV1_0(w[11], w[5], w[71], pars->GC_11, amp[103]); 
  FFV1_0(w[1], w[31], w[69], pars->GC_11, amp[104]); 
  FFV1_0(w[32], w[5], w[69], pars->GC_11, amp[105]); 
  FFV1_0(w[11], w[31], w[4], pars->GC_11, amp[106]); 
  FFV1_0(w[32], w[13], w[4], pars->GC_11, amp[107]); 
  VVVV1_0(w[4], w[30], w[8], w[14], pars->GC_12, amp[108]); 
  VVVV3_0(w[4], w[30], w[8], w[14], pars->GC_12, amp[109]); 
  VVVV4_0(w[4], w[30], w[8], w[14], pars->GC_12, amp[110]); 
  VVV1_0(w[8], w[14], w[71], pars->GC_10, amp[111]); 
  VVV1_0(w[30], w[14], w[69], pars->GC_10, amp[112]); 
  VVV1_0(w[30], w[8], w[72], pars->GC_10, amp[113]); 
  FFV1_0(w[3], w[19], w[71], pars->GC_11, amp[114]); 
  FFV1_0(w[20], w[2], w[71], pars->GC_11, amp[115]); 
  FFV1_0(w[3], w[47], w[72], pars->GC_11, amp[116]); 
  FFV1_0(w[48], w[2], w[72], pars->GC_11, amp[117]); 
  FFV1_0(w[20], w[47], w[4], pars->GC_11, amp[118]); 
  FFV1_0(w[48], w[19], w[4], pars->GC_11, amp[119]); 
  FFV1_0(w[35], w[6], w[69], pars->GC_11, amp[120]); 
  FFV1_0(w[0], w[36], w[69], pars->GC_11, amp[121]); 
  FFV1_0(w[52], w[6], w[72], pars->GC_11, amp[122]); 
  FFV1_0(w[0], w[10], w[72], pars->GC_11, amp[123]); 
  FFV1_0(w[52], w[36], w[4], pars->GC_11, amp[124]); 
  FFV1_0(w[35], w[10], w[4], pars->GC_11, amp[125]); 
  FFV1_0(w[37], w[5], w[69], pars->GC_11, amp[126]); 
  FFV1_0(w[0], w[38], w[69], pars->GC_11, amp[127]); 
  FFV1_0(w[52], w[5], w[70], pars->GC_11, amp[128]); 
  FFV1_0(w[0], w[13], w[70], pars->GC_11, amp[129]); 
  FFV1_0(w[52], w[38], w[4], pars->GC_11, amp[130]); 
  FFV1_0(w[37], w[13], w[4], pars->GC_11, amp[131]); 
  FFV1_0(w[74], w[13], w[75], pars->GC_11, amp[132]); 
  FFV1_0(w[76], w[5], w[75], pars->GC_11, amp[133]); 
  FFV1_0(w[74], w[77], w[9], pars->GC_11, amp[134]); 
  FFV1_0(w[76], w[73], w[9], pars->GC_11, amp[135]); 
  FFV1_0(w[15], w[5], w[78], pars->GC_11, amp[136]); 
  FFV1_0(w[79], w[5], w[8], pars->GC_11, amp[137]); 
  VVV1_0(w[8], w[78], w[9], pars->GC_10, amp[138]); 
  FFV1_0(w[15], w[73], w[80], pars->GC_11, amp[139]); 
  VVV1_0(w[8], w[80], w[75], pars->GC_10, amp[140]); 
  FFV1_0(w[81], w[73], w[8], pars->GC_11, amp[141]); 
  FFV1_0(w[3], w[82], w[9], pars->GC_11, amp[142]); 
  FFV1_0(w[83], w[2], w[9], pars->GC_11, amp[143]); 
  FFV1_0(w[3], w[84], w[75], pars->GC_11, amp[144]); 
  FFV1_0(w[85], w[2], w[75], pars->GC_11, amp[145]); 
  FFV1_0(w[74], w[87], w[25], pars->GC_11, amp[146]); 
  FFV1_0(w[88], w[5], w[25], pars->GC_11, amp[147]); 
  FFV1_0(w[3], w[89], w[80], pars->GC_11, amp[148]); 
  VVV1_0(w[86], w[80], w[25], pars->GC_10, amp[149]); 
  FFV1_0(w[3], w[90], w[86], pars->GC_11, amp[150]); 
  FFV1_0(w[74], w[91], w[25], pars->GC_11, amp[151]); 
  FFV1_0(w[92], w[73], w[25], pars->GC_11, amp[152]); 
  FFV1_0(w[3], w[28], w[78], pars->GC_11, amp[153]); 
  VVV1_0(w[24], w[78], w[25], pars->GC_10, amp[154]); 
  FFV1_0(w[3], w[93], w[24], pars->GC_11, amp[155]); 
  FFV1_0(w[94], w[5], w[25], pars->GC_11, amp[156]); 
  FFV1_0(w[0], w[95], w[25], pars->GC_11, amp[157]); 
  FFV1_0(w[96], w[73], w[25], pars->GC_11, amp[158]); 
  FFV1_0(w[0], w[97], w[25], pars->GC_11, amp[159]); 
  FFV1_0(w[74], w[87], w[40], pars->GC_11, amp[160]); 
  FFV1_0(w[88], w[5], w[40], pars->GC_11, amp[161]); 
  FFV1_0(w[98], w[2], w[80], pars->GC_11, amp[162]); 
  VVV1_0(w[86], w[80], w[40], pars->GC_10, amp[163]); 
  FFV1_0(w[99], w[2], w[86], pars->GC_11, amp[164]); 
  FFV1_0(w[74], w[91], w[40], pars->GC_11, amp[165]); 
  FFV1_0(w[92], w[73], w[40], pars->GC_11, amp[166]); 
  FFV1_0(w[41], w[2], w[78], pars->GC_11, amp[167]); 
  VVV1_0(w[24], w[78], w[40], pars->GC_10, amp[168]); 
  FFV1_0(w[100], w[2], w[24], pars->GC_11, amp[169]); 
  FFV1_0(w[94], w[5], w[40], pars->GC_11, amp[170]); 
  FFV1_0(w[0], w[95], w[40], pars->GC_11, amp[171]); 
  FFV1_0(w[96], w[73], w[40], pars->GC_11, amp[172]); 
  FFV1_0(w[0], w[97], w[40], pars->GC_11, amp[173]); 
  FFV1_0(w[3], w[56], w[102], pars->GC_11, amp[174]); 
  FFV1_0(w[57], w[2], w[102], pars->GC_11, amp[175]); 
  FFV1_0(w[74], w[103], w[8], pars->GC_11, amp[176]); 
  FFV1_0(w[74], w[104], w[24], pars->GC_11, amp[177]); 
  VVV1_0(w[24], w[8], w[102], pars->GC_10, amp[178]); 
  FFV1_0(w[74], w[13], w[105], pars->GC_11, amp[179]); 
  FFV1_0(w[76], w[5], w[105], pars->GC_11, amp[180]); 
  FFV1_0(w[52], w[5], w[102], pars->GC_11, amp[181]); 
  FFV1_0(w[0], w[13], w[102], pars->GC_11, amp[182]); 
  VVV1_0(w[8], w[80], w[105], pars->GC_10, amp[183]); 
  FFV1_0(w[0], w[104], w[80], pars->GC_11, amp[184]); 
  FFV1_0(w[0], w[106], w[8], pars->GC_11, amp[185]); 
  FFV1_0(w[3], w[84], w[105], pars->GC_11, amp[186]); 
  FFV1_0(w[85], w[2], w[105], pars->GC_11, amp[187]); 
  FFV1_0(w[3], w[108], w[107], pars->GC_11, amp[188]); 
  FFV1_0(w[109], w[2], w[107], pars->GC_11, amp[189]); 
  FFV1_0(w[74], w[110], w[8], pars->GC_11, amp[190]); 
  FFV1_0(w[74], w[50], w[86], pars->GC_11, amp[191]); 
  VVV1_0(w[86], w[8], w[107], pars->GC_10, amp[192]); 
  FFV1_0(w[74], w[77], w[51], pars->GC_11, amp[193]); 
  FFV1_0(w[76], w[73], w[51], pars->GC_11, amp[194]); 
  FFV1_0(w[52], w[73], w[107], pars->GC_11, amp[195]); 
  FFV1_0(w[0], w[77], w[107], pars->GC_11, amp[196]); 
  VVV1_0(w[8], w[78], w[51], pars->GC_10, amp[197]); 
  FFV1_0(w[0], w[50], w[78], pars->GC_11, amp[198]); 
  FFV1_0(w[0], w[111], w[8], pars->GC_11, amp[199]); 
  FFV1_0(w[3], w[82], w[51], pars->GC_11, amp[200]); 
  FFV1_0(w[83], w[2], w[51], pars->GC_11, amp[201]); 
  FFV1_0(w[3], w[108], w[113], pars->GC_11, amp[202]); 
  FFV1_0(w[109], w[2], w[113], pars->GC_11, amp[203]); 
  FFV1_0(w[114], w[5], w[8], pars->GC_11, amp[204]); 
  FFV1_0(w[115], w[5], w[86], pars->GC_11, amp[205]); 
  VVV1_0(w[86], w[8], w[113], pars->GC_10, amp[206]); 
  FFV1_0(w[3], w[56], w[116], pars->GC_11, amp[207]); 
  FFV1_0(w[57], w[2], w[116], pars->GC_11, amp[208]); 
  FFV1_0(w[117], w[73], w[8], pars->GC_11, amp[209]); 
  FFV1_0(w[115], w[73], w[24], pars->GC_11, amp[210]); 
  VVV1_0(w[24], w[8], w[116], pars->GC_10, amp[211]); 
  FFV1_0(w[52], w[5], w[116], pars->GC_11, amp[212]); 
  FFV1_0(w[0], w[13], w[116], pars->GC_11, amp[213]); 
  FFV1_0(w[52], w[73], w[113], pars->GC_11, amp[214]); 
  FFV1_0(w[0], w[77], w[113], pars->GC_11, amp[215]); 
  FFV1_0(w[74], w[13], w[118], pars->GC_11, amp[216]); 
  FFV1_0(w[76], w[5], w[118], pars->GC_11, amp[217]); 
  FFV1_0(w[74], w[87], w[69], pars->GC_11, amp[218]); 
  FFV1_0(w[88], w[5], w[69], pars->GC_11, amp[219]); 
  FFV1_0(w[76], w[87], w[4], pars->GC_11, amp[220]); 
  FFV1_0(w[88], w[13], w[4], pars->GC_11, amp[221]); 
  VVVV1_0(w[4], w[86], w[8], w[80], pars->GC_12, amp[222]); 
  VVVV3_0(w[4], w[86], w[8], w[80], pars->GC_12, amp[223]); 
  VVVV4_0(w[4], w[86], w[8], w[80], pars->GC_12, amp[224]); 
  VVV1_0(w[8], w[80], w[118], pars->GC_10, amp[225]); 
  VVV1_0(w[86], w[80], w[69], pars->GC_10, amp[226]); 
  VVV1_0(w[86], w[8], w[119], pars->GC_10, amp[227]); 
  FFV1_0(w[3], w[84], w[118], pars->GC_11, amp[228]); 
  FFV1_0(w[85], w[2], w[118], pars->GC_11, amp[229]); 
  FFV1_0(w[3], w[108], w[119], pars->GC_11, amp[230]); 
  FFV1_0(w[109], w[2], w[119], pars->GC_11, amp[231]); 
  FFV1_0(w[85], w[108], w[4], pars->GC_11, amp[232]); 
  FFV1_0(w[109], w[84], w[4], pars->GC_11, amp[233]); 
  FFV1_0(w[74], w[77], w[68], pars->GC_11, amp[234]); 
  FFV1_0(w[76], w[73], w[68], pars->GC_11, amp[235]); 
  FFV1_0(w[74], w[91], w[69], pars->GC_11, amp[236]); 
  FFV1_0(w[92], w[73], w[69], pars->GC_11, amp[237]); 
  FFV1_0(w[76], w[91], w[4], pars->GC_11, amp[238]); 
  FFV1_0(w[92], w[77], w[4], pars->GC_11, amp[239]); 
  VVVV1_0(w[4], w[24], w[8], w[78], pars->GC_12, amp[240]); 
  VVVV3_0(w[4], w[24], w[8], w[78], pars->GC_12, amp[241]); 
  VVVV4_0(w[4], w[24], w[8], w[78], pars->GC_12, amp[242]); 
  VVV1_0(w[8], w[78], w[68], pars->GC_10, amp[243]); 
  VVV1_0(w[24], w[78], w[69], pars->GC_10, amp[244]); 
  VVV1_0(w[24], w[8], w[120], pars->GC_10, amp[245]); 
  FFV1_0(w[3], w[82], w[68], pars->GC_11, amp[246]); 
  FFV1_0(w[83], w[2], w[68], pars->GC_11, amp[247]); 
  FFV1_0(w[3], w[56], w[120], pars->GC_11, amp[248]); 
  FFV1_0(w[57], w[2], w[120], pars->GC_11, amp[249]); 
  FFV1_0(w[83], w[56], w[4], pars->GC_11, amp[250]); 
  FFV1_0(w[57], w[82], w[4], pars->GC_11, amp[251]); 
  FFV1_0(w[94], w[5], w[69], pars->GC_11, amp[252]); 
  FFV1_0(w[0], w[95], w[69], pars->GC_11, amp[253]); 
  FFV1_0(w[52], w[5], w[120], pars->GC_11, amp[254]); 
  FFV1_0(w[0], w[13], w[120], pars->GC_11, amp[255]); 
  FFV1_0(w[52], w[95], w[4], pars->GC_11, amp[256]); 
  FFV1_0(w[94], w[13], w[4], pars->GC_11, amp[257]); 
  FFV1_0(w[96], w[73], w[69], pars->GC_11, amp[258]); 
  FFV1_0(w[0], w[97], w[69], pars->GC_11, amp[259]); 
  FFV1_0(w[52], w[73], w[119], pars->GC_11, amp[260]); 
  FFV1_0(w[0], w[77], w[119], pars->GC_11, amp[261]); 
  FFV1_0(w[52], w[97], w[4], pars->GC_11, amp[262]); 
  FFV1_0(w[96], w[77], w[4], pars->GC_11, amp[263]); 
  FFV1_0(w[74], w[77], w[124], pars->GC_11, amp[264]); 
  FFV1_0(w[76], w[73], w[124], pars->GC_11, amp[265]); 
  FFV1_0(w[74], w[126], w[125], pars->GC_11, amp[266]); 
  FFV1_0(w[76], w[121], w[125], pars->GC_11, amp[267]); 
  FFV1_0(w[128], w[73], w[127], pars->GC_11, amp[268]); 
  FFV1_0(w[129], w[73], w[8], pars->GC_11, amp[269]); 
  VVV1_0(w[8], w[127], w[125], pars->GC_10, amp[270]); 
  FFV1_0(w[128], w[121], w[78], pars->GC_11, amp[271]); 
  VVV1_0(w[8], w[78], w[124], pars->GC_10, amp[272]); 
  FFV1_0(w[130], w[121], w[8], pars->GC_11, amp[273]); 
  FFV1_0(w[3], w[131], w[125], pars->GC_11, amp[274]); 
  FFV1_0(w[132], w[2], w[125], pars->GC_11, amp[275]); 
  FFV1_0(w[3], w[82], w[124], pars->GC_11, amp[276]); 
  FFV1_0(w[83], w[2], w[124], pars->GC_11, amp[277]); 
  FFV1_0(w[74], w[134], w[25], pars->GC_11, amp[278]); 
  FFV1_0(w[135], w[73], w[25], pars->GC_11, amp[279]); 
  FFV1_0(w[3], w[136], w[78], pars->GC_11, amp[280]); 
  VVV1_0(w[133], w[78], w[25], pars->GC_10, amp[281]); 
  FFV1_0(w[3], w[93], w[133], pars->GC_11, amp[282]); 
  FFV1_0(w[74], w[138], w[25], pars->GC_11, amp[283]); 
  FFV1_0(w[139], w[121], w[25], pars->GC_11, amp[284]); 
  FFV1_0(w[3], w[140], w[127], pars->GC_11, amp[285]); 
  VVV1_0(w[137], w[127], w[25], pars->GC_10, amp[286]); 
  FFV1_0(w[3], w[141], w[137], pars->GC_11, amp[287]); 
  FFV1_0(w[142], w[73], w[25], pars->GC_11, amp[288]); 
  FFV1_0(w[122], w[143], w[25], pars->GC_11, amp[289]); 
  FFV1_0(w[144], w[121], w[25], pars->GC_11, amp[290]); 
  FFV1_0(w[122], w[145], w[25], pars->GC_11, amp[291]); 
  FFV1_0(w[74], w[134], w[40], pars->GC_11, amp[292]); 
  FFV1_0(w[135], w[73], w[40], pars->GC_11, amp[293]); 
  FFV1_0(w[146], w[2], w[78], pars->GC_11, amp[294]); 
  VVV1_0(w[133], w[78], w[40], pars->GC_10, amp[295]); 
  FFV1_0(w[100], w[2], w[133], pars->GC_11, amp[296]); 
  FFV1_0(w[74], w[138], w[40], pars->GC_11, amp[297]); 
  FFV1_0(w[139], w[121], w[40], pars->GC_11, amp[298]); 
  FFV1_0(w[147], w[2], w[127], pars->GC_11, amp[299]); 
  VVV1_0(w[137], w[127], w[40], pars->GC_10, amp[300]); 
  FFV1_0(w[148], w[2], w[137], pars->GC_11, amp[301]); 
  FFV1_0(w[142], w[73], w[40], pars->GC_11, amp[302]); 
  FFV1_0(w[122], w[143], w[40], pars->GC_11, amp[303]); 
  FFV1_0(w[144], w[121], w[40], pars->GC_11, amp[304]); 
  FFV1_0(w[122], w[145], w[40], pars->GC_11, amp[305]); 
  FFV1_0(w[3], w[151], w[150], pars->GC_11, amp[306]); 
  FFV1_0(w[152], w[2], w[150], pars->GC_11, amp[307]); 
  FFV1_0(w[74], w[153], w[8], pars->GC_11, amp[308]); 
  FFV1_0(w[74], w[154], w[137], pars->GC_11, amp[309]); 
  VVV1_0(w[137], w[8], w[150], pars->GC_10, amp[310]); 
  FFV1_0(w[74], w[77], w[155], pars->GC_11, amp[311]); 
  FFV1_0(w[76], w[73], w[155], pars->GC_11, amp[312]); 
  FFV1_0(w[156], w[73], w[150], pars->GC_11, amp[313]); 
  FFV1_0(w[122], w[77], w[150], pars->GC_11, amp[314]); 
  VVV1_0(w[8], w[78], w[155], pars->GC_10, amp[315]); 
  FFV1_0(w[122], w[154], w[78], pars->GC_11, amp[316]); 
  FFV1_0(w[122], w[157], w[8], pars->GC_11, amp[317]); 
  FFV1_0(w[3], w[82], w[155], pars->GC_11, amp[318]); 
  FFV1_0(w[83], w[2], w[155], pars->GC_11, amp[319]); 
  FFV1_0(w[3], w[158], w[102], pars->GC_11, amp[320]); 
  FFV1_0(w[159], w[2], w[102], pars->GC_11, amp[321]); 
  FFV1_0(w[74], w[160], w[8], pars->GC_11, amp[322]); 
  FFV1_0(w[74], w[104], w[133], pars->GC_11, amp[323]); 
  VVV1_0(w[133], w[8], w[102], pars->GC_10, amp[324]); 
  FFV1_0(w[74], w[126], w[161], pars->GC_11, amp[325]); 
  FFV1_0(w[76], w[121], w[161], pars->GC_11, amp[326]); 
  FFV1_0(w[156], w[121], w[102], pars->GC_11, amp[327]); 
  FFV1_0(w[122], w[126], w[102], pars->GC_11, amp[328]); 
  VVV1_0(w[8], w[127], w[161], pars->GC_10, amp[329]); 
  FFV1_0(w[122], w[104], w[127], pars->GC_11, amp[330]); 
  FFV1_0(w[122], w[162], w[8], pars->GC_11, amp[331]); 
  FFV1_0(w[3], w[131], w[161], pars->GC_11, amp[332]); 
  FFV1_0(w[132], w[2], w[161], pars->GC_11, amp[333]); 
  FFV1_0(w[3], w[158], w[116], pars->GC_11, amp[334]); 
  FFV1_0(w[159], w[2], w[116], pars->GC_11, amp[335]); 
  FFV1_0(w[163], w[73], w[8], pars->GC_11, amp[336]); 
  FFV1_0(w[115], w[73], w[133], pars->GC_11, amp[337]); 
  VVV1_0(w[133], w[8], w[116], pars->GC_10, amp[338]); 
  FFV1_0(w[3], w[151], w[164], pars->GC_11, amp[339]); 
  FFV1_0(w[152], w[2], w[164], pars->GC_11, amp[340]); 
  FFV1_0(w[165], w[121], w[8], pars->GC_11, amp[341]); 
  FFV1_0(w[115], w[121], w[137], pars->GC_11, amp[342]); 
  VVV1_0(w[137], w[8], w[164], pars->GC_10, amp[343]); 
  FFV1_0(w[156], w[73], w[164], pars->GC_11, amp[344]); 
  FFV1_0(w[122], w[77], w[164], pars->GC_11, amp[345]); 
  FFV1_0(w[156], w[121], w[116], pars->GC_11, amp[346]); 
  FFV1_0(w[122], w[126], w[116], pars->GC_11, amp[347]); 
  FFV1_0(w[74], w[77], w[166], pars->GC_11, amp[348]); 
  FFV1_0(w[76], w[73], w[166], pars->GC_11, amp[349]); 
  FFV1_0(w[74], w[134], w[69], pars->GC_11, amp[350]); 
  FFV1_0(w[135], w[73], w[69], pars->GC_11, amp[351]); 
  FFV1_0(w[76], w[134], w[4], pars->GC_11, amp[352]); 
  FFV1_0(w[135], w[77], w[4], pars->GC_11, amp[353]); 
  VVVV1_0(w[4], w[133], w[8], w[78], pars->GC_12, amp[354]); 
  VVVV3_0(w[4], w[133], w[8], w[78], pars->GC_12, amp[355]); 
  VVVV4_0(w[4], w[133], w[8], w[78], pars->GC_12, amp[356]); 
  VVV1_0(w[8], w[78], w[166], pars->GC_10, amp[357]); 
  VVV1_0(w[133], w[78], w[69], pars->GC_10, amp[358]); 
  VVV1_0(w[133], w[8], w[120], pars->GC_10, amp[359]); 
  FFV1_0(w[3], w[82], w[166], pars->GC_11, amp[360]); 
  FFV1_0(w[83], w[2], w[166], pars->GC_11, amp[361]); 
  FFV1_0(w[3], w[158], w[120], pars->GC_11, amp[362]); 
  FFV1_0(w[159], w[2], w[120], pars->GC_11, amp[363]); 
  FFV1_0(w[83], w[158], w[4], pars->GC_11, amp[364]); 
  FFV1_0(w[159], w[82], w[4], pars->GC_11, amp[365]); 
  FFV1_0(w[74], w[126], w[167], pars->GC_11, amp[366]); 
  FFV1_0(w[76], w[121], w[167], pars->GC_11, amp[367]); 
  FFV1_0(w[74], w[138], w[69], pars->GC_11, amp[368]); 
  FFV1_0(w[139], w[121], w[69], pars->GC_11, amp[369]); 
  FFV1_0(w[76], w[138], w[4], pars->GC_11, amp[370]); 
  FFV1_0(w[139], w[126], w[4], pars->GC_11, amp[371]); 
  VVVV1_0(w[4], w[137], w[8], w[127], pars->GC_12, amp[372]); 
  VVVV3_0(w[4], w[137], w[8], w[127], pars->GC_12, amp[373]); 
  VVVV4_0(w[4], w[137], w[8], w[127], pars->GC_12, amp[374]); 
  VVV1_0(w[8], w[127], w[167], pars->GC_10, amp[375]); 
  VVV1_0(w[137], w[127], w[69], pars->GC_10, amp[376]); 
  VVV1_0(w[137], w[8], w[168], pars->GC_10, amp[377]); 
  FFV1_0(w[3], w[131], w[167], pars->GC_11, amp[378]); 
  FFV1_0(w[132], w[2], w[167], pars->GC_11, amp[379]); 
  FFV1_0(w[3], w[151], w[168], pars->GC_11, amp[380]); 
  FFV1_0(w[152], w[2], w[168], pars->GC_11, amp[381]); 
  FFV1_0(w[132], w[151], w[4], pars->GC_11, amp[382]); 
  FFV1_0(w[152], w[131], w[4], pars->GC_11, amp[383]); 
  FFV1_0(w[142], w[73], w[69], pars->GC_11, amp[384]); 
  FFV1_0(w[122], w[143], w[69], pars->GC_11, amp[385]); 
  FFV1_0(w[156], w[73], w[168], pars->GC_11, amp[386]); 
  FFV1_0(w[122], w[77], w[168], pars->GC_11, amp[387]); 
  FFV1_0(w[156], w[143], w[4], pars->GC_11, amp[388]); 
  FFV1_0(w[142], w[77], w[4], pars->GC_11, amp[389]); 
  FFV1_0(w[144], w[121], w[69], pars->GC_11, amp[390]); 
  FFV1_0(w[122], w[145], w[69], pars->GC_11, amp[391]); 
  FFV1_0(w[156], w[121], w[120], pars->GC_11, amp[392]); 
  FFV1_0(w[122], w[126], w[120], pars->GC_11, amp[393]); 
  FFV1_0(w[156], w[145], w[4], pars->GC_11, amp[394]); 
  FFV1_0(w[144], w[126], w[4], pars->GC_11, amp[395]); 
  FFV1_0(w[15], w[5], w[17], pars->GC_11, amp[396]); 
  VVV1_0(w[8], w[17], w[9], pars->GC_10, amp[397]); 
  FFV1_0(w[18], w[5], w[8], pars->GC_11, amp[398]); 
  FFV1_0(w[3], w[21], w[9], pars->GC_11, amp[399]); 
  FFV1_0(w[22], w[2], w[9], pars->GC_11, amp[400]); 
  FFV1_0(w[1], w[26], w[25], pars->GC_11, amp[401]); 
  FFV1_0(w[27], w[6], w[25], pars->GC_11, amp[402]); 
  FFV1_0(w[3], w[28], w[17], pars->GC_11, amp[403]); 
  VVV1_0(w[24], w[17], w[25], pars->GC_10, amp[404]); 
  FFV1_0(w[3], w[29], w[24], pars->GC_11, amp[405]); 
  FFV1_0(w[37], w[5], w[25], pars->GC_11, amp[406]); 
  FFV1_0(w[0], w[38], w[25], pars->GC_11, amp[407]); 
  FFV1_0(w[1], w[26], w[40], pars->GC_11, amp[408]); 
  FFV1_0(w[27], w[6], w[40], pars->GC_11, amp[409]); 
  FFV1_0(w[41], w[2], w[17], pars->GC_11, amp[410]); 
  VVV1_0(w[24], w[17], w[40], pars->GC_10, amp[411]); 
  FFV1_0(w[42], w[2], w[24], pars->GC_11, amp[412]); 
  FFV1_0(w[37], w[5], w[40], pars->GC_11, amp[413]); 
  FFV1_0(w[0], w[38], w[40], pars->GC_11, amp[414]); 
  FFV1_0(w[1], w[10], w[51], pars->GC_11, amp[415]); 
  FFV1_0(w[11], w[6], w[51], pars->GC_11, amp[416]); 
  VVV1_0(w[8], w[17], w[51], pars->GC_10, amp[417]); 
  FFV1_0(w[0], w[50], w[17], pars->GC_11, amp[418]); 
  FFV1_0(w[0], w[53], w[8], pars->GC_11, amp[419]); 
  FFV1_0(w[3], w[21], w[51], pars->GC_11, amp[420]); 
  FFV1_0(w[22], w[2], w[51], pars->GC_11, amp[421]); 
  FFV1_0(w[3], w[56], w[55], pars->GC_11, amp[422]); 
  FFV1_0(w[57], w[2], w[55], pars->GC_11, amp[423]); 
  FFV1_0(w[1], w[58], w[8], pars->GC_11, amp[424]); 
  FFV1_0(w[1], w[59], w[24], pars->GC_11, amp[425]); 
  VVV1_0(w[24], w[8], w[55], pars->GC_10, amp[426]); 
  FFV1_0(w[52], w[5], w[55], pars->GC_11, amp[427]); 
  FFV1_0(w[0], w[13], w[55], pars->GC_11, amp[428]); 
  FFV1_0(w[3], w[56], w[63], pars->GC_11, amp[429]); 
  FFV1_0(w[57], w[2], w[63], pars->GC_11, amp[430]); 
  FFV1_0(w[64], w[6], w[8], pars->GC_11, amp[431]); 
  FFV1_0(w[65], w[6], w[24], pars->GC_11, amp[432]); 
  VVV1_0(w[24], w[8], w[63], pars->GC_10, amp[433]); 
  FFV1_0(w[52], w[5], w[63], pars->GC_11, amp[434]); 
  FFV1_0(w[0], w[13], w[63], pars->GC_11, amp[435]); 
  FFV1_0(w[1], w[10], w[68], pars->GC_11, amp[436]); 
  FFV1_0(w[11], w[6], w[68], pars->GC_11, amp[437]); 
  FFV1_0(w[1], w[26], w[69], pars->GC_11, amp[438]); 
  FFV1_0(w[27], w[6], w[69], pars->GC_11, amp[439]); 
  FFV1_0(w[11], w[26], w[4], pars->GC_11, amp[440]); 
  FFV1_0(w[27], w[10], w[4], pars->GC_11, amp[441]); 
  VVVV1_0(w[4], w[24], w[8], w[17], pars->GC_12, amp[442]); 
  VVVV3_0(w[4], w[24], w[8], w[17], pars->GC_12, amp[443]); 
  VVVV4_0(w[4], w[24], w[8], w[17], pars->GC_12, amp[444]); 
  VVV1_0(w[8], w[17], w[68], pars->GC_10, amp[445]); 
  VVV1_0(w[24], w[17], w[69], pars->GC_10, amp[446]); 
  VVV1_0(w[24], w[8], w[70], pars->GC_10, amp[447]); 
  FFV1_0(w[3], w[21], w[68], pars->GC_11, amp[448]); 
  FFV1_0(w[22], w[2], w[68], pars->GC_11, amp[449]); 
  FFV1_0(w[3], w[56], w[70], pars->GC_11, amp[450]); 
  FFV1_0(w[57], w[2], w[70], pars->GC_11, amp[451]); 
  FFV1_0(w[22], w[56], w[4], pars->GC_11, amp[452]); 
  FFV1_0(w[57], w[21], w[4], pars->GC_11, amp[453]); 
  FFV1_0(w[37], w[5], w[69], pars->GC_11, amp[454]); 
  FFV1_0(w[0], w[38], w[69], pars->GC_11, amp[455]); 
  FFV1_0(w[52], w[5], w[70], pars->GC_11, amp[456]); 
  FFV1_0(w[0], w[13], w[70], pars->GC_11, amp[457]); 
  FFV1_0(w[52], w[38], w[4], pars->GC_11, amp[458]); 
  FFV1_0(w[37], w[13], w[4], pars->GC_11, amp[459]); 
  FFV1_0(w[74], w[13], w[75], pars->GC_11, amp[460]); 
  FFV1_0(w[76], w[5], w[75], pars->GC_11, amp[461]); 
  FFV1_0(w[15], w[73], w[80], pars->GC_11, amp[462]); 
  VVV1_0(w[8], w[80], w[75], pars->GC_10, amp[463]); 
  FFV1_0(w[81], w[73], w[8], pars->GC_11, amp[464]); 
  FFV1_0(w[3], w[84], w[75], pars->GC_11, amp[465]); 
  FFV1_0(w[85], w[2], w[75], pars->GC_11, amp[466]); 
  FFV1_0(w[74], w[87], w[25], pars->GC_11, amp[467]); 
  FFV1_0(w[88], w[5], w[25], pars->GC_11, amp[468]); 
  FFV1_0(w[3], w[89], w[80], pars->GC_11, amp[469]); 
  VVV1_0(w[86], w[80], w[25], pars->GC_10, amp[470]); 
  FFV1_0(w[3], w[90], w[86], pars->GC_11, amp[471]); 
  FFV1_0(w[96], w[73], w[25], pars->GC_11, amp[472]); 
  FFV1_0(w[0], w[97], w[25], pars->GC_11, amp[473]); 
  FFV1_0(w[74], w[87], w[40], pars->GC_11, amp[474]); 
  FFV1_0(w[88], w[5], w[40], pars->GC_11, amp[475]); 
  FFV1_0(w[98], w[2], w[80], pars->GC_11, amp[476]); 
  VVV1_0(w[86], w[80], w[40], pars->GC_10, amp[477]); 
  FFV1_0(w[99], w[2], w[86], pars->GC_11, amp[478]); 
  FFV1_0(w[96], w[73], w[40], pars->GC_11, amp[479]); 
  FFV1_0(w[0], w[97], w[40], pars->GC_11, amp[480]); 
  FFV1_0(w[74], w[13], w[105], pars->GC_11, amp[481]); 
  FFV1_0(w[76], w[5], w[105], pars->GC_11, amp[482]); 
  VVV1_0(w[8], w[80], w[105], pars->GC_10, amp[483]); 
  FFV1_0(w[0], w[104], w[80], pars->GC_11, amp[484]); 
  FFV1_0(w[0], w[106], w[8], pars->GC_11, amp[485]); 
  FFV1_0(w[3], w[84], w[105], pars->GC_11, amp[486]); 
  FFV1_0(w[85], w[2], w[105], pars->GC_11, amp[487]); 
  FFV1_0(w[3], w[108], w[107], pars->GC_11, amp[488]); 
  FFV1_0(w[109], w[2], w[107], pars->GC_11, amp[489]); 
  FFV1_0(w[74], w[110], w[8], pars->GC_11, amp[490]); 
  FFV1_0(w[74], w[50], w[86], pars->GC_11, amp[491]); 
  VVV1_0(w[86], w[8], w[107], pars->GC_10, amp[492]); 
  FFV1_0(w[52], w[73], w[107], pars->GC_11, amp[493]); 
  FFV1_0(w[0], w[77], w[107], pars->GC_11, amp[494]); 
  FFV1_0(w[3], w[108], w[113], pars->GC_11, amp[495]); 
  FFV1_0(w[109], w[2], w[113], pars->GC_11, amp[496]); 
  FFV1_0(w[114], w[5], w[8], pars->GC_11, amp[497]); 
  FFV1_0(w[115], w[5], w[86], pars->GC_11, amp[498]); 
  VVV1_0(w[86], w[8], w[113], pars->GC_10, amp[499]); 
  FFV1_0(w[52], w[73], w[113], pars->GC_11, amp[500]); 
  FFV1_0(w[0], w[77], w[113], pars->GC_11, amp[501]); 
  FFV1_0(w[74], w[13], w[118], pars->GC_11, amp[502]); 
  FFV1_0(w[76], w[5], w[118], pars->GC_11, amp[503]); 
  FFV1_0(w[74], w[87], w[69], pars->GC_11, amp[504]); 
  FFV1_0(w[88], w[5], w[69], pars->GC_11, amp[505]); 
  FFV1_0(w[76], w[87], w[4], pars->GC_11, amp[506]); 
  FFV1_0(w[88], w[13], w[4], pars->GC_11, amp[507]); 
  VVVV1_0(w[4], w[86], w[8], w[80], pars->GC_12, amp[508]); 
  VVVV3_0(w[4], w[86], w[8], w[80], pars->GC_12, amp[509]); 
  VVVV4_0(w[4], w[86], w[8], w[80], pars->GC_12, amp[510]); 
  VVV1_0(w[8], w[80], w[118], pars->GC_10, amp[511]); 
  VVV1_0(w[86], w[80], w[69], pars->GC_10, amp[512]); 
  VVV1_0(w[86], w[8], w[119], pars->GC_10, amp[513]); 
  FFV1_0(w[3], w[84], w[118], pars->GC_11, amp[514]); 
  FFV1_0(w[85], w[2], w[118], pars->GC_11, amp[515]); 
  FFV1_0(w[3], w[108], w[119], pars->GC_11, amp[516]); 
  FFV1_0(w[109], w[2], w[119], pars->GC_11, amp[517]); 
  FFV1_0(w[85], w[108], w[4], pars->GC_11, amp[518]); 
  FFV1_0(w[109], w[84], w[4], pars->GC_11, amp[519]); 
  FFV1_0(w[96], w[73], w[69], pars->GC_11, amp[520]); 
  FFV1_0(w[0], w[97], w[69], pars->GC_11, amp[521]); 
  FFV1_0(w[52], w[73], w[119], pars->GC_11, amp[522]); 
  FFV1_0(w[0], w[77], w[119], pars->GC_11, amp[523]); 
  FFV1_0(w[52], w[97], w[4], pars->GC_11, amp[524]); 
  FFV1_0(w[96], w[77], w[4], pars->GC_11, amp[525]); 
  FFV1_0(w[74], w[77], w[9], pars->GC_11, amp[526]); 
  FFV1_0(w[76], w[73], w[9], pars->GC_11, amp[527]); 
  FFV1_0(w[15], w[5], w[78], pars->GC_11, amp[528]); 
  VVV1_0(w[8], w[78], w[9], pars->GC_10, amp[529]); 
  FFV1_0(w[79], w[5], w[8], pars->GC_11, amp[530]); 
  FFV1_0(w[3], w[82], w[9], pars->GC_11, amp[531]); 
  FFV1_0(w[83], w[2], w[9], pars->GC_11, amp[532]); 
  FFV1_0(w[74], w[91], w[25], pars->GC_11, amp[533]); 
  FFV1_0(w[92], w[73], w[25], pars->GC_11, amp[534]); 
  FFV1_0(w[3], w[28], w[78], pars->GC_11, amp[535]); 
  VVV1_0(w[24], w[78], w[25], pars->GC_10, amp[536]); 
  FFV1_0(w[3], w[93], w[24], pars->GC_11, amp[537]); 
  FFV1_0(w[94], w[5], w[25], pars->GC_11, amp[538]); 
  FFV1_0(w[0], w[95], w[25], pars->GC_11, amp[539]); 
  FFV1_0(w[74], w[91], w[40], pars->GC_11, amp[540]); 
  FFV1_0(w[92], w[73], w[40], pars->GC_11, amp[541]); 
  FFV1_0(w[41], w[2], w[78], pars->GC_11, amp[542]); 
  VVV1_0(w[24], w[78], w[40], pars->GC_10, amp[543]); 
  FFV1_0(w[100], w[2], w[24], pars->GC_11, amp[544]); 
  FFV1_0(w[94], w[5], w[40], pars->GC_11, amp[545]); 
  FFV1_0(w[0], w[95], w[40], pars->GC_11, amp[546]); 
  FFV1_0(w[74], w[77], w[51], pars->GC_11, amp[547]); 
  FFV1_0(w[76], w[73], w[51], pars->GC_11, amp[548]); 
  VVV1_0(w[8], w[78], w[51], pars->GC_10, amp[549]); 
  FFV1_0(w[0], w[50], w[78], pars->GC_11, amp[550]); 
  FFV1_0(w[0], w[111], w[8], pars->GC_11, amp[551]); 
  FFV1_0(w[3], w[82], w[51], pars->GC_11, amp[552]); 
  FFV1_0(w[83], w[2], w[51], pars->GC_11, amp[553]); 
  FFV1_0(w[3], w[56], w[102], pars->GC_11, amp[554]); 
  FFV1_0(w[57], w[2], w[102], pars->GC_11, amp[555]); 
  FFV1_0(w[74], w[103], w[8], pars->GC_11, amp[556]); 
  FFV1_0(w[74], w[104], w[24], pars->GC_11, amp[557]); 
  VVV1_0(w[24], w[8], w[102], pars->GC_10, amp[558]); 
  FFV1_0(w[52], w[5], w[102], pars->GC_11, amp[559]); 
  FFV1_0(w[0], w[13], w[102], pars->GC_11, amp[560]); 
  FFV1_0(w[3], w[56], w[116], pars->GC_11, amp[561]); 
  FFV1_0(w[57], w[2], w[116], pars->GC_11, amp[562]); 
  FFV1_0(w[117], w[73], w[8], pars->GC_11, amp[563]); 
  FFV1_0(w[115], w[73], w[24], pars->GC_11, amp[564]); 
  VVV1_0(w[24], w[8], w[116], pars->GC_10, amp[565]); 
  FFV1_0(w[52], w[5], w[116], pars->GC_11, amp[566]); 
  FFV1_0(w[0], w[13], w[116], pars->GC_11, amp[567]); 
  FFV1_0(w[74], w[77], w[68], pars->GC_11, amp[568]); 
  FFV1_0(w[76], w[73], w[68], pars->GC_11, amp[569]); 
  FFV1_0(w[74], w[91], w[69], pars->GC_11, amp[570]); 
  FFV1_0(w[92], w[73], w[69], pars->GC_11, amp[571]); 
  FFV1_0(w[76], w[91], w[4], pars->GC_11, amp[572]); 
  FFV1_0(w[92], w[77], w[4], pars->GC_11, amp[573]); 
  VVVV1_0(w[4], w[24], w[8], w[78], pars->GC_12, amp[574]); 
  VVVV3_0(w[4], w[24], w[8], w[78], pars->GC_12, amp[575]); 
  VVVV4_0(w[4], w[24], w[8], w[78], pars->GC_12, amp[576]); 
  VVV1_0(w[8], w[78], w[68], pars->GC_10, amp[577]); 
  VVV1_0(w[24], w[78], w[69], pars->GC_10, amp[578]); 
  VVV1_0(w[24], w[8], w[120], pars->GC_10, amp[579]); 
  FFV1_0(w[3], w[82], w[68], pars->GC_11, amp[580]); 
  FFV1_0(w[83], w[2], w[68], pars->GC_11, amp[581]); 
  FFV1_0(w[3], w[56], w[120], pars->GC_11, amp[582]); 
  FFV1_0(w[57], w[2], w[120], pars->GC_11, amp[583]); 
  FFV1_0(w[83], w[56], w[4], pars->GC_11, amp[584]); 
  FFV1_0(w[57], w[82], w[4], pars->GC_11, amp[585]); 
  FFV1_0(w[94], w[5], w[69], pars->GC_11, amp[586]); 
  FFV1_0(w[0], w[95], w[69], pars->GC_11, amp[587]); 
  FFV1_0(w[52], w[5], w[120], pars->GC_11, amp[588]); 
  FFV1_0(w[0], w[13], w[120], pars->GC_11, amp[589]); 
  FFV1_0(w[52], w[95], w[4], pars->GC_11, amp[590]); 
  FFV1_0(w[94], w[13], w[4], pars->GC_11, amp[591]); 
  FFV1_0(w[74], w[77], w[124], pars->GC_11, amp[592]); 
  FFV1_0(w[76], w[73], w[124], pars->GC_11, amp[593]); 
  FFV1_0(w[128], w[121], w[78], pars->GC_11, amp[594]); 
  VVV1_0(w[8], w[78], w[124], pars->GC_10, amp[595]); 
  FFV1_0(w[130], w[121], w[8], pars->GC_11, amp[596]); 
  FFV1_0(w[3], w[82], w[124], pars->GC_11, amp[597]); 
  FFV1_0(w[83], w[2], w[124], pars->GC_11, amp[598]); 
  FFV1_0(w[74], w[134], w[25], pars->GC_11, amp[599]); 
  FFV1_0(w[135], w[73], w[25], pars->GC_11, amp[600]); 
  FFV1_0(w[3], w[136], w[78], pars->GC_11, amp[601]); 
  VVV1_0(w[133], w[78], w[25], pars->GC_10, amp[602]); 
  FFV1_0(w[3], w[93], w[133], pars->GC_11, amp[603]); 
  FFV1_0(w[144], w[121], w[25], pars->GC_11, amp[604]); 
  FFV1_0(w[122], w[145], w[25], pars->GC_11, amp[605]); 
  FFV1_0(w[74], w[134], w[40], pars->GC_11, amp[606]); 
  FFV1_0(w[135], w[73], w[40], pars->GC_11, amp[607]); 
  FFV1_0(w[146], w[2], w[78], pars->GC_11, amp[608]); 
  VVV1_0(w[133], w[78], w[40], pars->GC_10, amp[609]); 
  FFV1_0(w[100], w[2], w[133], pars->GC_11, amp[610]); 
  FFV1_0(w[144], w[121], w[40], pars->GC_11, amp[611]); 
  FFV1_0(w[122], w[145], w[40], pars->GC_11, amp[612]); 
  FFV1_0(w[74], w[77], w[155], pars->GC_11, amp[613]); 
  FFV1_0(w[76], w[73], w[155], pars->GC_11, amp[614]); 
  VVV1_0(w[8], w[78], w[155], pars->GC_10, amp[615]); 
  FFV1_0(w[122], w[154], w[78], pars->GC_11, amp[616]); 
  FFV1_0(w[122], w[157], w[8], pars->GC_11, amp[617]); 
  FFV1_0(w[3], w[82], w[155], pars->GC_11, amp[618]); 
  FFV1_0(w[83], w[2], w[155], pars->GC_11, amp[619]); 
  FFV1_0(w[3], w[158], w[102], pars->GC_11, amp[620]); 
  FFV1_0(w[159], w[2], w[102], pars->GC_11, amp[621]); 
  FFV1_0(w[74], w[160], w[8], pars->GC_11, amp[622]); 
  FFV1_0(w[74], w[104], w[133], pars->GC_11, amp[623]); 
  VVV1_0(w[133], w[8], w[102], pars->GC_10, amp[624]); 
  FFV1_0(w[156], w[121], w[102], pars->GC_11, amp[625]); 
  FFV1_0(w[122], w[126], w[102], pars->GC_11, amp[626]); 
  FFV1_0(w[3], w[158], w[116], pars->GC_11, amp[627]); 
  FFV1_0(w[159], w[2], w[116], pars->GC_11, amp[628]); 
  FFV1_0(w[163], w[73], w[8], pars->GC_11, amp[629]); 
  FFV1_0(w[115], w[73], w[133], pars->GC_11, amp[630]); 
  VVV1_0(w[133], w[8], w[116], pars->GC_10, amp[631]); 
  FFV1_0(w[156], w[121], w[116], pars->GC_11, amp[632]); 
  FFV1_0(w[122], w[126], w[116], pars->GC_11, amp[633]); 
  FFV1_0(w[74], w[77], w[166], pars->GC_11, amp[634]); 
  FFV1_0(w[76], w[73], w[166], pars->GC_11, amp[635]); 
  FFV1_0(w[74], w[134], w[69], pars->GC_11, amp[636]); 
  FFV1_0(w[135], w[73], w[69], pars->GC_11, amp[637]); 
  FFV1_0(w[76], w[134], w[4], pars->GC_11, amp[638]); 
  FFV1_0(w[135], w[77], w[4], pars->GC_11, amp[639]); 
  VVVV1_0(w[4], w[133], w[8], w[78], pars->GC_12, amp[640]); 
  VVVV3_0(w[4], w[133], w[8], w[78], pars->GC_12, amp[641]); 
  VVVV4_0(w[4], w[133], w[8], w[78], pars->GC_12, amp[642]); 
  VVV1_0(w[8], w[78], w[166], pars->GC_10, amp[643]); 
  VVV1_0(w[133], w[78], w[69], pars->GC_10, amp[644]); 
  VVV1_0(w[133], w[8], w[120], pars->GC_10, amp[645]); 
  FFV1_0(w[3], w[82], w[166], pars->GC_11, amp[646]); 
  FFV1_0(w[83], w[2], w[166], pars->GC_11, amp[647]); 
  FFV1_0(w[3], w[158], w[120], pars->GC_11, amp[648]); 
  FFV1_0(w[159], w[2], w[120], pars->GC_11, amp[649]); 
  FFV1_0(w[83], w[158], w[4], pars->GC_11, amp[650]); 
  FFV1_0(w[159], w[82], w[4], pars->GC_11, amp[651]); 
  FFV1_0(w[144], w[121], w[69], pars->GC_11, amp[652]); 
  FFV1_0(w[122], w[145], w[69], pars->GC_11, amp[653]); 
  FFV1_0(w[156], w[121], w[120], pars->GC_11, amp[654]); 
  FFV1_0(w[122], w[126], w[120], pars->GC_11, amp[655]); 
  FFV1_0(w[156], w[145], w[4], pars->GC_11, amp[656]); 
  FFV1_0(w[144], w[126], w[4], pars->GC_11, amp[657]); 


}
double PY8MEs_R16_P50_sm_qq_ttxgqq::matrix_16_uu_ttxguu() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 132;
  const int ncolor = 18; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
      1, 1, 1, 1, 1};
  static const double cf[ncolor][ncolor] = {{36, 12, 12, 0, 12, 4, 4, 0, 4, 12,
      0, 4, 0, 0, 12, 4, 4, 0}, {12, 36, 0, 12, 4, 12, 0, 4, 12, 4, 4, 0, 0, 0,
      4, 0, 12, 4}, {12, 0, 36, 12, 4, 0, 12, 4, 0, 4, 4, 12, 0, 0, 4, 12, 0,
      4}, {0, 12, 12, 36, 0, 4, 4, 12, 4, 0, 12, 4, 0, 0, 0, 4, 4, 12}, {12, 4,
      4, 0, 36, 12, 12, 0, 12, 4, 4, 0, 12, 4, 0, 0, 0, 4}, {4, 12, 0, 4, 12,
      36, 0, 12, 4, 12, 0, 4, 4, 0, 0, 0, 4, 12}, {4, 0, 12, 4, 12, 0, 36, 12,
      4, 0, 12, 4, 4, 12, 0, 0, 4, 0}, {0, 4, 4, 12, 0, 12, 12, 36, 0, 4, 4,
      12, 0, 4, 0, 0, 12, 4}, {4, 12, 0, 4, 12, 4, 4, 0, 36, 12, 12, 0, 4, 12,
      0, 4, 0, 0}, {12, 4, 4, 0, 4, 12, 0, 4, 12, 36, 0, 12, 0, 4, 4, 12, 0,
      0}, {0, 4, 4, 12, 4, 0, 12, 4, 12, 0, 36, 12, 12, 4, 4, 0, 0, 0}, {4, 0,
      12, 4, 0, 4, 4, 12, 0, 12, 12, 36, 4, 0, 12, 4, 0, 0}, {0, 0, 0, 0, 12,
      4, 4, 0, 4, 0, 12, 4, 36, 12, 12, 4, 4, 12}, {0, 0, 0, 0, 4, 0, 12, 4,
      12, 4, 4, 0, 12, 36, 4, 12, 12, 4}, {12, 4, 4, 0, 0, 0, 0, 0, 0, 4, 4,
      12, 12, 4, 36, 12, 12, 4}, {4, 0, 12, 4, 0, 0, 0, 0, 4, 12, 0, 4, 4, 12,
      12, 36, 4, 12}, {4, 12, 0, 4, 0, 4, 4, 12, 0, 0, 0, 0, 4, 12, 12, 4, 36,
      12}, {0, 4, 4, 12, 4, 12, 0, 4, 0, 0, 0, 0, 12, 4, 4, 12, 12, 36}};

  // Calculate color flows
  jamp[0] = +1./4. * (+1./3. * amp[42] + 1./3. * amp[43] + amp[47] + 1./3. *
      amp[49] + 1./3. * amp[50] + Complex<double> (0, 1) * amp[51] + amp[53] +
      amp[55] + amp[70] + amp[72] + Complex<double> (0, 1) * amp[74] + 1./3. *
      amp[75] + 1./3. * amp[76] + 1./3. * amp[80] + 1./3. * amp[81] + amp[82] +
      Complex<double> (0, 1) * amp[84] + amp[92] - amp[90] - amp[93] + amp[95]
      + Complex<double> (0, 1) * amp[97] - Complex<double> (0, 1) * amp[98] +
      amp[100] - Complex<double> (0, 1) * amp[128]);
  jamp[1] = +1./4. * (-1./3. * amp[30] - 1./3. * amp[32] - amp[34] +
      Complex<double> (0, 1) * amp[36] - amp[37] - amp[39] - 1./3. * amp[40] -
      1./3. * amp[41] - amp[42] - amp[45] - Complex<double> (0, 1) * amp[46] -
      amp[49] - 1./3. * amp[52] - 1./3. * amp[53] - 1./3. * amp[54] - 1./3. *
      amp[55] - 1./3. * Complex<double> (0, 1) * amp[96] - 1./3. *
      Complex<double> (0, 1) * amp[97] - 1./3. * amp[100] + Complex<double> (0,
      1) * amp[105] + amp[110] + amp[109] + amp[112] + amp[113] -
      Complex<double> (0, 1) * amp[116] + Complex<double> (0, 1) * amp[121] -
      Complex<double> (0, 1) * amp[122] - amp[124] + 1./3. * Complex<double>
      (0, 1) * amp[126] + 1./3. * Complex<double> (0, 1) * amp[127] - 1./3. *
      amp[130]);
  jamp[2] = +1./4. * (-1./3. * amp[56] - 1./3. * amp[57] - amp[61] - 1./3. *
      amp[63] - 1./3. * amp[64] - Complex<double> (0, 1) * amp[65] - amp[67] -
      amp[69] - 1./3. * amp[70] - 1./3. * amp[71] - amp[75] - amp[77] -
      Complex<double> (0, 1) * amp[79] - amp[80] - 1./3. * amp[82] - 1./3. *
      amp[83] - Complex<double> (0, 1) * amp[102] - amp[110] + amp[108] +
      amp[111] - amp[113] - Complex<double> (0, 1) * amp[115] + Complex<double>
      (0, 1) * amp[116] - amp[118] + Complex<double> (0, 1) * amp[122]);
  jamp[3] = +1./4. * (+amp[29] - Complex<double> (0, 1) * amp[31] + amp[32] +
      1./3. * amp[35] + 1./3. * amp[37] + 1./3. * amp[38] + 1./3. * amp[39] +
      amp[41] + amp[56] + amp[59] + Complex<double> (0, 1) * amp[60] + amp[63]
      + 1./3. * amp[66] + 1./3. * amp[67] + 1./3. * amp[68] + 1./3. * amp[69] -
      Complex<double> (0, 1) * amp[87] - amp[92] - amp[91] - amp[94] - amp[95]
      + Complex<double> (0, 1) * amp[98] + 1./3. * Complex<double> (0, 1) *
      amp[114] + 1./3. * Complex<double> (0, 1) * amp[115] + 1./3. * amp[118] -
      1./3. * Complex<double> (0, 1) * amp[120] - 1./3. * Complex<double> (0,
      1) * amp[121] + 1./3. * amp[124] - Complex<double> (0, 1) * amp[127] +
      Complex<double> (0, 1) * amp[128] + amp[130]);
  jamp[4] = +1./4. * (-1./3. * amp[0] - 1./3. * amp[1] - amp[3] - amp[5] +
      Complex<double> (0, 1) * amp[6] - amp[10] - 1./3. * amp[12] - 1./3. *
      amp[13] - amp[43] - amp[44] + Complex<double> (0, 1) * amp[46] - 1./3. *
      amp[47] - 1./3. * amp[48] - amp[50] - 1./3. * amp[54] - 1./3. * amp[55] +
      Complex<double> (0, 1) * amp[103] - amp[110] + amp[108] + amp[111] -
      amp[113] + Complex<double> (0, 1) * amp[114] - Complex<double> (0, 1) *
      amp[117] - amp[119] - Complex<double> (0, 1) * amp[123]);
  jamp[5] = +1./4. * (+amp[28] + amp[30] + Complex<double> (0, 1) * amp[31] +
      1./3. * amp[33] + 1./3. * amp[34] + 1./3. * amp[35] + 1./3. * amp[37] +
      amp[40] + 1./3. * amp[42] + 1./3. * amp[43] + 1./3. * amp[44] + 1./3. *
      amp[45] + amp[48] - Complex<double> (0, 1) * amp[51] + amp[52] + amp[54]
      + Complex<double> (0, 1) * amp[85] - Complex<double> (0, 1) * amp[86] +
      amp[88] + amp[91] + amp[90] + amp[93] + amp[94] + Complex<double> (0, 1)
      * amp[96] - 1./3. * Complex<double> (0, 1) * amp[104] - 1./3. *
      Complex<double> (0, 1) * amp[105] + 1./3. * amp[106] + 1./3. *
      Complex<double> (0, 1) * amp[116] + 1./3. * Complex<double> (0, 1) *
      amp[117] + 1./3. * amp[119] - Complex<double> (0, 1) * amp[126]);
  jamp[6] = +1./4. * (+amp[1] + 1./3. * amp[2] + 1./3. * amp[3] -
      Complex<double> (0, 1) * amp[8] + amp[9] + 1./3. * amp[10] + 1./3. *
      amp[11] + amp[12] + amp[57] + amp[58] - Complex<double> (0, 1) * amp[60]
      + 1./3. * amp[61] + 1./3. * amp[62] + amp[64] + 1./3. * amp[68] + 1./3. *
      amp[69] - Complex<double> (0, 1) * amp[85] + amp[92] - amp[90] - amp[93]
      + amp[95] - Complex<double> (0, 1) * amp[96] + Complex<double> (0, 1) *
      amp[99] + amp[101] + Complex<double> (0, 1) * amp[129]);
  jamp[7] = +1./4. * (-1./3. * amp[28] - 1./3. * amp[29] - 1./3. * amp[30] -
      1./3. * amp[32] - amp[33] - amp[35] - Complex<double> (0, 1) * amp[36] -
      amp[38] - 1./3. * amp[56] - 1./3. * amp[57] - 1./3. * amp[58] - 1./3. *
      amp[59] - amp[62] + Complex<double> (0, 1) * amp[65] - amp[66] - amp[68]
      + 1./3. * Complex<double> (0, 1) * amp[86] + 1./3. * Complex<double> (0,
      1) * amp[87] - 1./3. * amp[88] - 1./3. * Complex<double> (0, 1) * amp[98]
      - 1./3. * Complex<double> (0, 1) * amp[99] - 1./3. * amp[101] -
      Complex<double> (0, 1) * amp[103] + Complex<double> (0, 1) * amp[104] -
      amp[106] - amp[109] - amp[108] - amp[111] - amp[112] - Complex<double>
      (0, 1) * amp[114] + Complex<double> (0, 1) * amp[120]);
  jamp[8] = +1./4. * (+1./9. * amp[0] + 1./9. * amp[1] + 1./3. * amp[2] + 1./3.
      * amp[3] + 1./3. * amp[4] + 1./3. * amp[5] + 1./9. * amp[7] + 1./9. *
      amp[9] + 1./9. * amp[12] + 1./9. * amp[13] + 1./3. * amp[44] + 1./3. *
      amp[45] + 1./9. * amp[47] + 1./9. * amp[48] + 1./3. * amp[49] + 1./3. *
      amp[50] + 1./9. * amp[52] + 1./9. * amp[53] + 1./9. * amp[54] + 1./9. *
      amp[55] - 1./3. * Complex<double> (0, 1) * amp[102] - 1./3. *
      Complex<double> (0, 1) * amp[103] + 1./3. * amp[107] + 1./3. *
      Complex<double> (0, 1) * amp[122] + 1./3. * Complex<double> (0, 1) *
      amp[123] + 1./3. * amp[124] + 1./9. * amp[130] + 1./9. * amp[131]);
  jamp[9] = +1./4. * (-1./9. * amp[42] - 1./9. * amp[43] - 1./9. * amp[44] -
      1./9. * amp[45] - 1./3. * amp[47] - 1./3. * amp[48] - 1./9. * amp[49] -
      1./9. * amp[50] - 1./3. * amp[52] - 1./3. * amp[53] - 1./3. * amp[72] -
      1./3. * amp[73] - 1./9. * amp[75] - 1./9. * amp[76] - 1./9. * amp[77] -
      1./9. * amp[78] - 1./9. * amp[80] - 1./9. * amp[81] - 1./3. * amp[82] -
      1./3. * amp[83] - 1./3. * Complex<double> (0, 1) * amp[84] - 1./3. *
      Complex<double> (0, 1) * amp[85] - 1./3. * amp[88] - 1./9. * amp[106] -
      1./9. * amp[107] + 1./3. * Complex<double> (0, 1) * amp[128] + 1./3. *
      Complex<double> (0, 1) * amp[129] - 1./3. * amp[131]);
  jamp[10] = +1./4. * (-1./3. * amp[0] - 1./3. * amp[1] - 1./9. * amp[2] -
      1./9. * amp[3] - 1./9. * amp[4] - 1./9. * amp[5] - 1./3. * amp[7] - 1./3.
      * amp[9] - 1./9. * amp[10] - 1./9. * amp[11] - 1./3. * amp[58] - 1./3. *
      amp[59] - 1./9. * amp[61] - 1./9. * amp[62] - 1./3. * amp[63] - 1./3. *
      amp[64] - 1./9. * amp[66] - 1./9. * amp[67] - 1./9. * amp[68] - 1./9. *
      amp[69] + 1./3. * Complex<double> (0, 1) * amp[84] + 1./3. *
      Complex<double> (0, 1) * amp[85] - 1./3. * amp[89] - 1./9. * amp[124] -
      1./9. * amp[125] - 1./3. * Complex<double> (0, 1) * amp[128] - 1./3. *
      Complex<double> (0, 1) * amp[129] - 1./3. * amp[130]);
  jamp[11] = +1./4. * (+1./9. * amp[56] + 1./9. * amp[57] + 1./9. * amp[58] +
      1./9. * amp[59] + 1./3. * amp[61] + 1./3. * amp[62] + 1./9. * amp[63] +
      1./9. * amp[64] + 1./3. * amp[66] + 1./3. * amp[67] + 1./9. * amp[70] +
      1./9. * amp[71] + 1./9. * amp[72] + 1./9. * amp[73] + 1./3. * amp[77] +
      1./3. * amp[78] + 1./3. * amp[80] + 1./3. * amp[81] + 1./9. * amp[82] +
      1./9. * amp[83] + 1./9. * amp[88] + 1./9. * amp[89] + 1./3. *
      Complex<double> (0, 1) * amp[102] + 1./3. * Complex<double> (0, 1) *
      amp[103] + 1./3. * amp[106] - 1./3. * Complex<double> (0, 1) * amp[122] -
      1./3. * Complex<double> (0, 1) * amp[123] + 1./3. * amp[125]);
  jamp[12] = +1./4. * (+amp[0] + 1./3. * amp[4] + 1./3. * amp[5] + amp[7] +
      Complex<double> (0, 1) * amp[8] + 1./3. * amp[10] + 1./3. * amp[11] +
      amp[13] + amp[15] + amp[16] - Complex<double> (0, 1) * amp[17] + 1./3. *
      amp[21] + 1./3. * amp[23] + 1./3. * amp[24] + 1./3. * amp[25] + amp[27] -
      Complex<double> (0, 1) * amp[84] + Complex<double> (0, 1) * amp[87] +
      amp[89] + amp[91] + amp[90] + amp[93] + amp[94] - Complex<double> (0, 1)
      * amp[97] - 1./3. * Complex<double> (0, 1) * amp[114] - 1./3. *
      Complex<double> (0, 1) * amp[115] + 1./3. * amp[119] + 1./3. *
      Complex<double> (0, 1) * amp[120] + 1./3. * Complex<double> (0, 1) *
      amp[121] + 1./3. * amp[125] + Complex<double> (0, 1) * amp[127]);
  jamp[13] = +1./4. * (-amp[2] - amp[4] - Complex<double> (0, 1) * amp[6] -
      1./3. * amp[7] - 1./3. * amp[9] - amp[11] - 1./3. * amp[12] - 1./3. *
      amp[13] - 1./3. * amp[16] - 1./3. * amp[18] - amp[20] - amp[21] +
      Complex<double> (0, 1) * amp[22] - amp[25] - 1./3. * amp[26] - 1./3. *
      amp[27] + 1./3. * Complex<double> (0, 1) * amp[96] + 1./3. *
      Complex<double> (0, 1) * amp[97] - 1./3. * amp[101] + Complex<double> (0,
      1) * amp[102] - Complex<double> (0, 1) * amp[105] - amp[107] - amp[109] -
      amp[108] - amp[111] - amp[112] + Complex<double> (0, 1) * amp[115] -
      Complex<double> (0, 1) * amp[121] - 1./3. * Complex<double> (0, 1) *
      amp[126] - 1./3. * Complex<double> (0, 1) * amp[127] - 1./3. * amp[131]);
  jamp[14] = +1./4. * (-1./3. * amp[14] - 1./3. * amp[15] - 1./3. * amp[16] -
      1./3. * amp[18] - amp[19] - Complex<double> (0, 1) * amp[22] - amp[23] -
      amp[24] - 1./3. * amp[70] - 1./3. * amp[71] - 1./3. * amp[72] - 1./3. *
      amp[73] - amp[76] - amp[78] + Complex<double> (0, 1) * amp[79] - amp[81]
      - 1./3. * Complex<double> (0, 1) * amp[86] - 1./3. * Complex<double> (0,
      1) * amp[87] - 1./3. * amp[89] + 1./3. * Complex<double> (0, 1) * amp[98]
      + 1./3. * Complex<double> (0, 1) * amp[99] - 1./3. * amp[100] -
      Complex<double> (0, 1) * amp[104] + amp[110] + amp[109] + amp[112] +
      amp[113] + Complex<double> (0, 1) * amp[117] - Complex<double> (0, 1) *
      amp[120] + Complex<double> (0, 1) * amp[123] - amp[125]);
  jamp[15] = +1./4. * (+amp[14] + Complex<double> (0, 1) * amp[17] + amp[18] +
      1./3. * amp[19] + 1./3. * amp[20] + 1./3. * amp[21] + 1./3. * amp[23] +
      amp[26] + amp[71] + amp[73] - Complex<double> (0, 1) * amp[74] + 1./3. *
      amp[75] + 1./3. * amp[76] + 1./3. * amp[77] + 1./3. * amp[78] + amp[83] +
      Complex<double> (0, 1) * amp[86] - amp[92] - amp[91] - amp[94] - amp[95]
      - Complex<double> (0, 1) * amp[99] + 1./3. * Complex<double> (0, 1) *
      amp[104] + 1./3. * Complex<double> (0, 1) * amp[105] + 1./3. * amp[107] -
      1./3. * Complex<double> (0, 1) * amp[116] - 1./3. * Complex<double> (0,
      1) * amp[117] + 1./3. * amp[118] + Complex<double> (0, 1) * amp[126] -
      Complex<double> (0, 1) * amp[129] + amp[131]);
  jamp[16] = +1./4. * (+1./9. * amp[14] + 1./9. * amp[15] + 1./9. * amp[16] +
      1./9. * amp[18] + 1./3. * amp[19] + 1./3. * amp[20] + 1./3. * amp[24] +
      1./3. * amp[25] + 1./9. * amp[26] + 1./9. * amp[27] + 1./9. * amp[28] +
      1./9. * amp[29] + 1./9. * amp[30] + 1./9. * amp[32] + 1./3. * amp[33] +
      1./3. * amp[34] + 1./3. * amp[38] + 1./3. * amp[39] + 1./9. * amp[40] +
      1./9. * amp[41] + 1./9. * amp[100] + 1./9. * amp[101]);
  jamp[17] = +1./4. * (-1./3. * amp[14] - 1./3. * amp[15] - 1./9. * amp[19] -
      1./9. * amp[20] - 1./9. * amp[21] - 1./9. * amp[23] - 1./9. * amp[24] -
      1./9. * amp[25] - 1./3. * amp[26] - 1./3. * amp[27] - 1./3. * amp[28] -
      1./3. * amp[29] - 1./9. * amp[33] - 1./9. * amp[34] - 1./9. * amp[35] -
      1./9. * amp[37] - 1./9. * amp[38] - 1./9. * amp[39] - 1./3. * amp[40] -
      1./3. * amp[41] - 1./9. * amp[118] - 1./9. * amp[119]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[0][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R16_P50_sm_qq_ttxgqq::matrix_16_uux_ttxguux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 132;
  const int ncolor = 18; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
      1, 1, 1, 1, 1};
  static const double cf[ncolor][ncolor] = {{36, 12, 0, 12, 12, 4, 0, 4, 4, 12,
      4, 0, 0, 4, 0, 12, 0, 4}, {12, 36, 12, 0, 4, 12, 4, 0, 12, 4, 0, 4, 4, 0,
      0, 4, 0, 12}, {0, 12, 36, 12, 0, 4, 12, 4, 4, 0, 4, 12, 12, 4, 0, 0, 0,
      4}, {12, 0, 12, 36, 4, 0, 4, 12, 0, 4, 12, 4, 4, 12, 0, 4, 0, 0}, {12, 4,
      0, 4, 36, 12, 0, 12, 12, 4, 0, 4, 0, 12, 0, 4, 4, 0}, {4, 12, 4, 0, 12,
      36, 12, 0, 4, 12, 4, 0, 0, 4, 4, 0, 12, 0}, {0, 4, 12, 4, 0, 12, 36, 12,
      0, 4, 12, 4, 0, 0, 12, 4, 4, 0}, {4, 0, 4, 12, 12, 0, 12, 36, 4, 0, 4,
      12, 0, 4, 4, 12, 0, 0}, {4, 12, 4, 0, 12, 4, 0, 4, 36, 12, 0, 12, 12, 0,
      4, 0, 0, 4}, {12, 4, 0, 4, 4, 12, 4, 0, 12, 36, 12, 0, 4, 0, 12, 0, 4,
      0}, {4, 0, 4, 12, 0, 4, 12, 4, 0, 12, 36, 12, 0, 0, 4, 0, 12, 4}, {0, 4,
      12, 4, 4, 0, 4, 12, 12, 0, 12, 36, 4, 0, 0, 0, 4, 12}, {0, 4, 12, 4, 0,
      0, 0, 0, 12, 4, 0, 4, 36, 12, 12, 4, 4, 12}, {4, 0, 4, 12, 12, 4, 0, 4,
      0, 0, 0, 0, 12, 36, 4, 12, 12, 4}, {0, 0, 0, 0, 0, 4, 12, 4, 4, 12, 4, 0,
      12, 4, 36, 12, 12, 4}, {12, 4, 0, 4, 4, 0, 4, 12, 0, 0, 0, 0, 4, 12, 12,
      36, 4, 12}, {0, 0, 0, 0, 4, 12, 4, 0, 0, 4, 12, 4, 4, 12, 12, 4, 36, 12},
      {4, 12, 4, 0, 0, 0, 0, 0, 4, 0, 4, 12, 12, 4, 4, 12, 12, 36}};

  // Calculate color flows
  jamp[0] = +1./4. * (+1./9. * amp[188] + 1./9. * amp[189] + 1./9. * amp[190] +
      1./9. * amp[191] + 1./3. * amp[193] + 1./3. * amp[194] + 1./9. * amp[195]
      + 1./9. * amp[196] + 1./3. * amp[198] + 1./3. * amp[199] + 1./9. *
      amp[202] + 1./9. * amp[203] + 1./9. * amp[204] + 1./9. * amp[205] + 1./3.
      * amp[209] + 1./3. * amp[210] + 1./3. * amp[212] + 1./3. * amp[213] +
      1./9. * amp[214] + 1./9. * amp[215] + 1./9. * amp[220] + 1./9. * amp[221]
      + 1./3. * Complex<double> (0, 1) * amp[234] + 1./3. * Complex<double> (0,
      1) * amp[235] + 1./3. * amp[238] - 1./3. * Complex<double> (0, 1) *
      amp[254] - 1./3. * Complex<double> (0, 1) * amp[255] + 1./3. * amp[257]);
  jamp[1] = +1./4. * (-1./3. * amp[160] - 1./3. * amp[161] - 1./3. * amp[162] -
      1./3. * amp[164] - amp[165] - amp[167] - Complex<double> (0, 1) *
      amp[168] - amp[170] - 1./3. * amp[188] - 1./3. * amp[189] - 1./3. *
      amp[190] - 1./3. * amp[191] - amp[194] + Complex<double> (0, 1) *
      amp[197] - amp[198] - amp[200] + 1./3. * Complex<double> (0, 1) *
      amp[218] + 1./3. * Complex<double> (0, 1) * amp[219] - 1./3. * amp[220] -
      1./3. * Complex<double> (0, 1) * amp[230] - 1./3. * Complex<double> (0,
      1) * amp[231] - 1./3. * amp[233] - Complex<double> (0, 1) * amp[235] +
      Complex<double> (0, 1) * amp[236] - amp[238] - amp[241] - amp[240] -
      amp[243] - amp[244] - Complex<double> (0, 1) * amp[246] + Complex<double>
      (0, 1) * amp[252]);
  jamp[2] = +1./4. * (+1./9. * amp[146] + 1./9. * amp[147] + 1./9. * amp[148] +
      1./9. * amp[150] + 1./3. * amp[151] + 1./3. * amp[152] + 1./3. * amp[156]
      + 1./3. * amp[157] + 1./9. * amp[158] + 1./9. * amp[159] + 1./9. *
      amp[160] + 1./9. * amp[161] + 1./9. * amp[162] + 1./9. * amp[164] + 1./3.
      * amp[165] + 1./3. * amp[166] + 1./3. * amp[170] + 1./3. * amp[171] +
      1./9. * amp[172] + 1./9. * amp[173] + 1./9. * amp[232] + 1./9. *
      amp[233]);
  jamp[3] = +1./4. * (-1./3. * amp[146] - 1./3. * amp[147] - 1./3. * amp[148] -
      1./3. * amp[150] - amp[151] - Complex<double> (0, 1) * amp[154] -
      amp[155] - amp[156] - 1./3. * amp[202] - 1./3. * amp[203] - 1./3. *
      amp[204] - 1./3. * amp[205] - amp[208] - amp[210] + Complex<double> (0,
      1) * amp[211] - amp[213] - 1./3. * Complex<double> (0, 1) * amp[218] -
      1./3. * Complex<double> (0, 1) * amp[219] - 1./3. * amp[221] + 1./3. *
      Complex<double> (0, 1) * amp[230] + 1./3. * Complex<double> (0, 1) *
      amp[231] - 1./3. * amp[232] - Complex<double> (0, 1) * amp[236] +
      amp[242] + amp[241] + amp[244] + amp[245] + Complex<double> (0, 1) *
      amp[249] - Complex<double> (0, 1) * amp[252] + Complex<double> (0, 1) *
      amp[255] - amp[257]);
  jamp[4] = +1./4. * (-1./3. * amp[188] - 1./3. * amp[189] - amp[193] - 1./3. *
      amp[195] - 1./3. * amp[196] - Complex<double> (0, 1) * amp[197] -
      amp[199] - amp[201] - 1./3. * amp[202] - 1./3. * amp[203] - amp[207] -
      amp[209] - Complex<double> (0, 1) * amp[211] - amp[212] - 1./3. *
      amp[214] - 1./3. * amp[215] - Complex<double> (0, 1) * amp[234] -
      amp[242] + amp[240] + amp[243] - amp[245] - Complex<double> (0, 1) *
      amp[247] + Complex<double> (0, 1) * amp[248] - amp[250] + Complex<double>
      (0, 1) * amp[254]);
  jamp[5] = +1./4. * (+amp[133] + 1./3. * amp[134] + 1./3. * amp[135] -
      Complex<double> (0, 1) * amp[140] + amp[141] + 1./3. * amp[142] + 1./3. *
      amp[143] + amp[144] + amp[189] + amp[190] - Complex<double> (0, 1) *
      amp[192] + 1./3. * amp[193] + 1./3. * amp[194] + amp[196] + 1./3. *
      amp[200] + 1./3. * amp[201] - Complex<double> (0, 1) * amp[217] +
      amp[224] - amp[222] - amp[225] + amp[227] - Complex<double> (0, 1) *
      amp[228] + Complex<double> (0, 1) * amp[231] + amp[233] + Complex<double>
      (0, 1) * amp[261]);
  jamp[6] = +1./4. * (-amp[134] - amp[136] - Complex<double> (0, 1) * amp[138]
      - 1./3. * amp[139] - 1./3. * amp[141] - amp[143] - 1./3. * amp[144] -
      1./3. * amp[145] - 1./3. * amp[148] - 1./3. * amp[150] - amp[152] -
      amp[153] + Complex<double> (0, 1) * amp[154] - amp[157] - 1./3. *
      amp[158] - 1./3. * amp[159] + 1./3. * Complex<double> (0, 1) * amp[228] +
      1./3. * Complex<double> (0, 1) * amp[229] - 1./3. * amp[233] +
      Complex<double> (0, 1) * amp[234] - Complex<double> (0, 1) * amp[237] -
      amp[239] - amp[241] - amp[240] - amp[243] - amp[244] + Complex<double>
      (0, 1) * amp[247] - Complex<double> (0, 1) * amp[253] - 1./3. *
      Complex<double> (0, 1) * amp[258] - 1./3. * Complex<double> (0, 1) *
      amp[259] - 1./3. * amp[263]);
  jamp[7] = +1./4. * (+amp[146] + Complex<double> (0, 1) * amp[149] + amp[150]
      + 1./3. * amp[151] + 1./3. * amp[152] + 1./3. * amp[153] + 1./3. *
      amp[155] + amp[158] + amp[203] + amp[205] - Complex<double> (0, 1) *
      amp[206] + 1./3. * amp[207] + 1./3. * amp[208] + 1./3. * amp[209] + 1./3.
      * amp[210] + amp[215] + Complex<double> (0, 1) * amp[218] - amp[224] -
      amp[223] - amp[226] - amp[227] - Complex<double> (0, 1) * amp[231] +
      1./3. * Complex<double> (0, 1) * amp[236] + 1./3. * Complex<double> (0,
      1) * amp[237] + 1./3. * amp[239] - 1./3. * Complex<double> (0, 1) *
      amp[248] - 1./3. * Complex<double> (0, 1) * amp[249] + 1./3. * amp[250] +
      Complex<double> (0, 1) * amp[258] - Complex<double> (0, 1) * amp[261] +
      amp[263]);
  jamp[8] = +1./4. * (+amp[161] - Complex<double> (0, 1) * amp[163] + amp[164]
      + 1./3. * amp[167] + 1./3. * amp[169] + 1./3. * amp[170] + 1./3. *
      amp[171] + amp[173] + amp[188] + amp[191] + Complex<double> (0, 1) *
      amp[192] + amp[195] + 1./3. * amp[198] + 1./3. * amp[199] + 1./3. *
      amp[200] + 1./3. * amp[201] - Complex<double> (0, 1) * amp[219] -
      amp[224] - amp[223] - amp[226] - amp[227] + Complex<double> (0, 1) *
      amp[230] + 1./3. * Complex<double> (0, 1) * amp[246] + 1./3. *
      Complex<double> (0, 1) * amp[247] + 1./3. * amp[250] - 1./3. *
      Complex<double> (0, 1) * amp[252] - 1./3. * Complex<double> (0, 1) *
      amp[253] + 1./3. * amp[256] - Complex<double> (0, 1) * amp[259] +
      Complex<double> (0, 1) * amp[260] + amp[262]);
  jamp[9] = +1./4. * (-1./3. * amp[132] - 1./3. * amp[133] - 1./9. * amp[134] -
      1./9. * amp[135] - 1./9. * amp[136] - 1./9. * amp[137] - 1./3. * amp[139]
      - 1./3. * amp[141] - 1./9. * amp[142] - 1./9. * amp[143] - 1./3. *
      amp[190] - 1./3. * amp[191] - 1./9. * amp[193] - 1./9. * amp[194] - 1./3.
      * amp[195] - 1./3. * amp[196] - 1./9. * amp[198] - 1./9. * amp[199] -
      1./9. * amp[200] - 1./9. * amp[201] + 1./3. * Complex<double> (0, 1) *
      amp[216] + 1./3. * Complex<double> (0, 1) * amp[217] - 1./3. * amp[221] -
      1./9. * amp[256] - 1./9. * amp[257] - 1./3. * Complex<double> (0, 1) *
      amp[260] - 1./3. * Complex<double> (0, 1) * amp[261] - 1./3. * amp[262]);
  jamp[10] = +1./4. * (+amp[132] + 1./3. * amp[136] + 1./3. * amp[137] +
      amp[139] + Complex<double> (0, 1) * amp[140] + 1./3. * amp[142] + 1./3. *
      amp[143] + amp[145] + amp[147] + amp[148] - Complex<double> (0, 1) *
      amp[149] + 1./3. * amp[153] + 1./3. * amp[155] + 1./3. * amp[156] + 1./3.
      * amp[157] + amp[159] - Complex<double> (0, 1) * amp[216] +
      Complex<double> (0, 1) * amp[219] + amp[221] + amp[223] + amp[222] +
      amp[225] + amp[226] - Complex<double> (0, 1) * amp[229] - 1./3. *
      Complex<double> (0, 1) * amp[246] - 1./3. * Complex<double> (0, 1) *
      amp[247] + 1./3. * amp[251] + 1./3. * Complex<double> (0, 1) * amp[252] +
      1./3. * Complex<double> (0, 1) * amp[253] + 1./3. * amp[257] +
      Complex<double> (0, 1) * amp[259]);
  jamp[11] = +1./4. * (-1./3. * amp[146] - 1./3. * amp[147] - 1./9. * amp[151]
      - 1./9. * amp[152] - 1./9. * amp[153] - 1./9. * amp[155] - 1./9. *
      amp[156] - 1./9. * amp[157] - 1./3. * amp[158] - 1./3. * amp[159] - 1./3.
      * amp[160] - 1./3. * amp[161] - 1./9. * amp[165] - 1./9. * amp[166] -
      1./9. * amp[167] - 1./9. * amp[169] - 1./9. * amp[170] - 1./9. * amp[171]
      - 1./3. * amp[172] - 1./3. * amp[173] - 1./9. * amp[250] - 1./9. *
      amp[251]);
  jamp[12] = +1./4. * (-1./3. * amp[162] - 1./3. * amp[164] - amp[166] +
      Complex<double> (0, 1) * amp[168] - amp[169] - amp[171] - 1./3. *
      amp[172] - 1./3. * amp[173] - amp[174] - amp[177] - Complex<double> (0,
      1) * amp[178] - amp[181] - 1./3. * amp[184] - 1./3. * amp[185] - 1./3. *
      amp[186] - 1./3. * amp[187] - 1./3. * Complex<double> (0, 1) * amp[228] -
      1./3. * Complex<double> (0, 1) * amp[229] - 1./3. * amp[232] +
      Complex<double> (0, 1) * amp[237] + amp[242] + amp[241] + amp[244] +
      amp[245] - Complex<double> (0, 1) * amp[248] + Complex<double> (0, 1) *
      amp[253] - Complex<double> (0, 1) * amp[254] - amp[256] + 1./3. *
      Complex<double> (0, 1) * amp[258] + 1./3. * Complex<double> (0, 1) *
      amp[259] - 1./3. * amp[262]);
  jamp[13] = +1./4. * (+1./3. * amp[174] + 1./3. * amp[175] + amp[179] + 1./3.
      * amp[181] + 1./3. * amp[182] + Complex<double> (0, 1) * amp[183] +
      amp[185] + amp[187] + amp[202] + amp[204] + Complex<double> (0, 1) *
      amp[206] + 1./3. * amp[207] + 1./3. * amp[208] + 1./3. * amp[212] + 1./3.
      * amp[213] + amp[214] + Complex<double> (0, 1) * amp[216] + amp[224] -
      amp[222] - amp[225] + amp[227] + Complex<double> (0, 1) * amp[229] -
      Complex<double> (0, 1) * amp[230] + amp[232] - Complex<double> (0, 1) *
      amp[260]);
  jamp[14] = +1./4. * (+1./9. * amp[132] + 1./9. * amp[133] + 1./3. * amp[134]
      + 1./3. * amp[135] + 1./3. * amp[136] + 1./3. * amp[137] + 1./9. *
      amp[139] + 1./9. * amp[141] + 1./9. * amp[144] + 1./9. * amp[145] + 1./3.
      * amp[176] + 1./3. * amp[177] + 1./9. * amp[179] + 1./9. * amp[180] +
      1./3. * amp[181] + 1./3. * amp[182] + 1./9. * amp[184] + 1./9. * amp[185]
      + 1./9. * amp[186] + 1./9. * amp[187] - 1./3. * Complex<double> (0, 1) *
      amp[234] - 1./3. * Complex<double> (0, 1) * amp[235] + 1./3. * amp[239] +
      1./3. * Complex<double> (0, 1) * amp[254] + 1./3. * Complex<double> (0,
      1) * amp[255] + 1./3. * amp[256] + 1./9. * amp[262] + 1./9. * amp[263]);
  jamp[15] = +1./4. * (-1./9. * amp[174] - 1./9. * amp[175] - 1./9. * amp[176]
      - 1./9. * amp[177] - 1./3. * amp[179] - 1./3. * amp[180] - 1./9. *
      amp[181] - 1./9. * amp[182] - 1./3. * amp[184] - 1./3. * amp[185] - 1./3.
      * amp[204] - 1./3. * amp[205] - 1./9. * amp[207] - 1./9. * amp[208] -
      1./9. * amp[209] - 1./9. * amp[210] - 1./9. * amp[212] - 1./9. * amp[213]
      - 1./3. * amp[214] - 1./3. * amp[215] - 1./3. * Complex<double> (0, 1) *
      amp[216] - 1./3. * Complex<double> (0, 1) * amp[217] - 1./3. * amp[220] -
      1./9. * amp[238] - 1./9. * amp[239] + 1./3. * Complex<double> (0, 1) *
      amp[260] + 1./3. * Complex<double> (0, 1) * amp[261] - 1./3. * amp[263]);
  jamp[16] = +1./4. * (-1./3. * amp[132] - 1./3. * amp[133] - amp[135] -
      amp[137] + Complex<double> (0, 1) * amp[138] - amp[142] - 1./3. *
      amp[144] - 1./3. * amp[145] - amp[175] - amp[176] + Complex<double> (0,
      1) * amp[178] - 1./3. * amp[179] - 1./3. * amp[180] - amp[182] - 1./3. *
      amp[186] - 1./3. * amp[187] + Complex<double> (0, 1) * amp[235] -
      amp[242] + amp[240] + amp[243] - amp[245] + Complex<double> (0, 1) *
      amp[246] - Complex<double> (0, 1) * amp[249] - amp[251] - Complex<double>
      (0, 1) * amp[255]);
  jamp[17] = +1./4. * (+amp[160] + amp[162] + Complex<double> (0, 1) * amp[163]
      + 1./3. * amp[165] + 1./3. * amp[166] + 1./3. * amp[167] + 1./3. *
      amp[169] + amp[172] + 1./3. * amp[174] + 1./3. * amp[175] + 1./3. *
      amp[176] + 1./3. * amp[177] + amp[180] - Complex<double> (0, 1) *
      amp[183] + amp[184] + amp[186] + Complex<double> (0, 1) * amp[217] -
      Complex<double> (0, 1) * amp[218] + amp[220] + amp[223] + amp[222] +
      amp[225] + amp[226] + Complex<double> (0, 1) * amp[228] - 1./3. *
      Complex<double> (0, 1) * amp[236] - 1./3. * Complex<double> (0, 1) *
      amp[237] + 1./3. * amp[238] + 1./3. * Complex<double> (0, 1) * amp[248] +
      1./3. * Complex<double> (0, 1) * amp[249] + 1./3. * amp[251] -
      Complex<double> (0, 1) * amp[258]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[1][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R16_P50_sm_qq_ttxgqq::matrix_16_uxux_ttxguxux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 132;
  const int ncolor = 18; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
      1, 1, 1, 1, 1};
  static const double cf[ncolor][ncolor] = {{36, 12, 12, 0, 12, 4, 4, 0, 4, 12,
      0, 4, 4, 0, 12, 0, 4, 0}, {12, 36, 0, 12, 4, 12, 0, 4, 12, 4, 4, 0, 0, 4,
      4, 0, 12, 0}, {12, 0, 36, 12, 4, 0, 12, 4, 0, 4, 4, 12, 12, 4, 4, 0, 0,
      0}, {0, 12, 12, 36, 0, 4, 4, 12, 4, 0, 12, 4, 4, 12, 0, 0, 4, 0}, {12, 4,
      4, 0, 36, 12, 12, 0, 12, 4, 4, 0, 12, 0, 4, 0, 0, 4}, {4, 12, 0, 4, 12,
      36, 0, 12, 4, 12, 0, 4, 4, 0, 0, 4, 0, 12}, {4, 0, 12, 4, 12, 0, 36, 12,
      4, 0, 12, 4, 4, 0, 12, 4, 0, 0}, {0, 4, 4, 12, 0, 12, 12, 36, 0, 4, 4,
      12, 0, 0, 4, 12, 0, 4}, {4, 12, 0, 4, 12, 4, 4, 0, 36, 12, 12, 0, 0, 12,
      0, 4, 4, 0}, {12, 4, 4, 0, 4, 12, 0, 4, 12, 36, 0, 12, 0, 4, 0, 12, 0,
      4}, {0, 4, 4, 12, 4, 0, 12, 4, 12, 0, 36, 12, 0, 4, 0, 0, 12, 4}, {4, 0,
      12, 4, 0, 4, 4, 12, 0, 12, 12, 36, 0, 0, 0, 4, 4, 12}, {4, 0, 12, 4, 12,
      4, 4, 0, 0, 0, 0, 0, 36, 12, 12, 4, 4, 12}, {0, 4, 4, 12, 0, 0, 0, 0, 12,
      4, 4, 0, 12, 36, 4, 12, 12, 4}, {12, 4, 4, 0, 4, 0, 12, 4, 0, 0, 0, 0,
      12, 4, 36, 12, 12, 4}, {0, 0, 0, 0, 0, 4, 4, 12, 4, 12, 0, 4, 4, 12, 12,
      36, 4, 12}, {4, 12, 0, 4, 0, 0, 0, 0, 4, 0, 12, 4, 4, 12, 12, 4, 36, 12},
      {0, 0, 0, 0, 4, 12, 0, 4, 0, 4, 4, 12, 12, 4, 4, 12, 12, 36}};

  // Calculate color flows
  jamp[0] = +1./4. * (+amp[278] + Complex<double> (0, 1) * amp[281] + amp[282]
      + 1./3. * amp[283] + 1./3. * amp[284] + 1./3. * amp[285] + 1./3. *
      amp[287] + amp[290] + amp[335] + amp[337] - Complex<double> (0, 1) *
      amp[338] + 1./3. * amp[339] + 1./3. * amp[340] + 1./3. * amp[341] + 1./3.
      * amp[342] + amp[347] + Complex<double> (0, 1) * amp[350] - amp[356] -
      amp[355] - amp[358] - amp[359] - Complex<double> (0, 1) * amp[363] +
      1./3. * Complex<double> (0, 1) * amp[368] + 1./3. * Complex<double> (0,
      1) * amp[369] + 1./3. * amp[371] - 1./3. * Complex<double> (0, 1) *
      amp[380] - 1./3. * Complex<double> (0, 1) * amp[381] + 1./3. * amp[382] +
      Complex<double> (0, 1) * amp[390] - Complex<double> (0, 1) * amp[393] +
      amp[395]);
  jamp[1] = +1./4. * (-amp[266] - amp[268] - Complex<double> (0, 1) * amp[270]
      - 1./3. * amp[271] - 1./3. * amp[273] - amp[275] - 1./3. * amp[276] -
      1./3. * amp[277] - 1./3. * amp[280] - 1./3. * amp[282] - amp[284] -
      amp[285] + Complex<double> (0, 1) * amp[286] - amp[289] - 1./3. *
      amp[290] - 1./3. * amp[291] + 1./3. * Complex<double> (0, 1) * amp[360] +
      1./3. * Complex<double> (0, 1) * amp[361] - 1./3. * amp[365] +
      Complex<double> (0, 1) * amp[366] - Complex<double> (0, 1) * amp[369] -
      amp[371] - amp[373] - amp[372] - amp[375] - amp[376] + Complex<double>
      (0, 1) * amp[379] - Complex<double> (0, 1) * amp[385] - 1./3. *
      Complex<double> (0, 1) * amp[390] - 1./3. * Complex<double> (0, 1) *
      amp[391] - 1./3. * amp[395]);
  jamp[2] = +1./4. * (-1./3. * amp[320] - 1./3. * amp[321] - amp[325] - 1./3. *
      amp[327] - 1./3. * amp[328] - Complex<double> (0, 1) * amp[329] -
      amp[331] - amp[333] - 1./3. * amp[334] - 1./3. * amp[335] - amp[339] -
      amp[341] - Complex<double> (0, 1) * amp[343] - amp[344] - 1./3. *
      amp[346] - 1./3. * amp[347] - Complex<double> (0, 1) * amp[366] -
      amp[374] + amp[372] + amp[375] - amp[377] - Complex<double> (0, 1) *
      amp[379] + Complex<double> (0, 1) * amp[380] - amp[382] + Complex<double>
      (0, 1) * amp[386]);
  jamp[3] = +1./4. * (+amp[265] + 1./3. * amp[266] + 1./3. * amp[267] -
      Complex<double> (0, 1) * amp[272] + amp[273] + 1./3. * amp[274] + 1./3. *
      amp[275] + amp[276] + amp[321] + amp[322] - Complex<double> (0, 1) *
      amp[324] + 1./3. * amp[325] + 1./3. * amp[326] + amp[328] + 1./3. *
      amp[332] + 1./3. * amp[333] - Complex<double> (0, 1) * amp[349] +
      amp[356] - amp[354] - amp[357] + amp[359] - Complex<double> (0, 1) *
      amp[360] + Complex<double> (0, 1) * amp[363] + amp[365] + Complex<double>
      (0, 1) * amp[393]);
  jamp[4] = +1./4. * (-1./3. * amp[278] - 1./3. * amp[279] - 1./3. * amp[280] -
      1./3. * amp[282] - amp[283] - Complex<double> (0, 1) * amp[286] -
      amp[287] - amp[288] - 1./3. * amp[334] - 1./3. * amp[335] - 1./3. *
      amp[336] - 1./3. * amp[337] - amp[340] - amp[342] + Complex<double> (0,
      1) * amp[343] - amp[345] - 1./3. * Complex<double> (0, 1) * amp[350] -
      1./3. * Complex<double> (0, 1) * amp[351] - 1./3. * amp[353] + 1./3. *
      Complex<double> (0, 1) * amp[362] + 1./3. * Complex<double> (0, 1) *
      amp[363] - 1./3. * amp[364] - Complex<double> (0, 1) * amp[368] +
      amp[374] + amp[373] + amp[376] + amp[377] + Complex<double> (0, 1) *
      amp[381] - Complex<double> (0, 1) * amp[384] + Complex<double> (0, 1) *
      amp[387] - amp[389]);
  jamp[5] = +1./4. * (+1./9. * amp[278] + 1./9. * amp[279] + 1./9. * amp[280] +
      1./9. * amp[282] + 1./3. * amp[283] + 1./3. * amp[284] + 1./3. * amp[288]
      + 1./3. * amp[289] + 1./9. * amp[290] + 1./9. * amp[291] + 1./9. *
      amp[292] + 1./9. * amp[293] + 1./9. * amp[294] + 1./9. * amp[296] + 1./3.
      * amp[297] + 1./3. * amp[298] + 1./3. * amp[302] + 1./3. * amp[303] +
      1./9. * amp[304] + 1./9. * amp[305] + 1./9. * amp[364] + 1./9. *
      amp[365]);
  jamp[6] = +1./4. * (+1./9. * amp[320] + 1./9. * amp[321] + 1./9. * amp[322] +
      1./9. * amp[323] + 1./3. * amp[325] + 1./3. * amp[326] + 1./9. * amp[327]
      + 1./9. * amp[328] + 1./3. * amp[330] + 1./3. * amp[331] + 1./9. *
      amp[334] + 1./9. * amp[335] + 1./9. * amp[336] + 1./9. * amp[337] + 1./3.
      * amp[341] + 1./3. * amp[342] + 1./3. * amp[344] + 1./3. * amp[345] +
      1./9. * amp[346] + 1./9. * amp[347] + 1./9. * amp[352] + 1./9. * amp[353]
      + 1./3. * Complex<double> (0, 1) * amp[366] + 1./3. * Complex<double> (0,
      1) * amp[367] + 1./3. * amp[370] - 1./3. * Complex<double> (0, 1) *
      amp[386] - 1./3. * Complex<double> (0, 1) * amp[387] + 1./3. * amp[389]);
  jamp[7] = +1./4. * (-1./3. * amp[292] - 1./3. * amp[293] - 1./3. * amp[294] -
      1./3. * amp[296] - amp[297] - amp[299] - Complex<double> (0, 1) *
      amp[300] - amp[302] - 1./3. * amp[320] - 1./3. * amp[321] - 1./3. *
      amp[322] - 1./3. * amp[323] - amp[326] + Complex<double> (0, 1) *
      amp[329] - amp[330] - amp[332] + 1./3. * Complex<double> (0, 1) *
      amp[350] + 1./3. * Complex<double> (0, 1) * amp[351] - 1./3. * amp[352] -
      1./3. * Complex<double> (0, 1) * amp[362] - 1./3. * Complex<double> (0,
      1) * amp[363] - 1./3. * amp[365] - Complex<double> (0, 1) * amp[367] +
      Complex<double> (0, 1) * amp[368] - amp[370] - amp[373] - amp[372] -
      amp[375] - amp[376] - Complex<double> (0, 1) * amp[378] + Complex<double>
      (0, 1) * amp[384]);
  jamp[8] = +1./4. * (+amp[264] + 1./3. * amp[268] + 1./3. * amp[269] +
      amp[271] + Complex<double> (0, 1) * amp[272] + 1./3. * amp[274] + 1./3. *
      amp[275] + amp[277] + amp[279] + amp[280] - Complex<double> (0, 1) *
      amp[281] + 1./3. * amp[285] + 1./3. * amp[287] + 1./3. * amp[288] + 1./3.
      * amp[289] + amp[291] - Complex<double> (0, 1) * amp[348] +
      Complex<double> (0, 1) * amp[351] + amp[353] + amp[355] + amp[354] +
      amp[357] + amp[358] - Complex<double> (0, 1) * amp[361] - 1./3. *
      Complex<double> (0, 1) * amp[378] - 1./3. * Complex<double> (0, 1) *
      amp[379] + 1./3. * amp[383] + 1./3. * Complex<double> (0, 1) * amp[384] +
      1./3. * Complex<double> (0, 1) * amp[385] + 1./3. * amp[389] +
      Complex<double> (0, 1) * amp[391]);
  jamp[9] = +1./4. * (-1./3. * amp[278] - 1./3. * amp[279] - 1./9. * amp[283] -
      1./9. * amp[284] - 1./9. * amp[285] - 1./9. * amp[287] - 1./9. * amp[288]
      - 1./9. * amp[289] - 1./3. * amp[290] - 1./3. * amp[291] - 1./3. *
      amp[292] - 1./3. * amp[293] - 1./9. * amp[297] - 1./9. * amp[298] - 1./9.
      * amp[299] - 1./9. * amp[301] - 1./9. * amp[302] - 1./9. * amp[303] -
      1./3. * amp[304] - 1./3. * amp[305] - 1./9. * amp[382] - 1./9. *
      amp[383]);
  jamp[10] = +1./4. * (-1./3. * amp[264] - 1./3. * amp[265] - 1./9. * amp[266]
      - 1./9. * amp[267] - 1./9. * amp[268] - 1./9. * amp[269] - 1./3. *
      amp[271] - 1./3. * amp[273] - 1./9. * amp[274] - 1./9. * amp[275] - 1./3.
      * amp[322] - 1./3. * amp[323] - 1./9. * amp[325] - 1./9. * amp[326] -
      1./3. * amp[327] - 1./3. * amp[328] - 1./9. * amp[330] - 1./9. * amp[331]
      - 1./9. * amp[332] - 1./9. * amp[333] + 1./3. * Complex<double> (0, 1) *
      amp[348] + 1./3. * Complex<double> (0, 1) * amp[349] - 1./3. * amp[353] -
      1./9. * amp[388] - 1./9. * amp[389] - 1./3. * Complex<double> (0, 1) *
      amp[392] - 1./3. * Complex<double> (0, 1) * amp[393] - 1./3. * amp[394]);
  jamp[11] = +1./4. * (+amp[293] - Complex<double> (0, 1) * amp[295] + amp[296]
      + 1./3. * amp[299] + 1./3. * amp[301] + 1./3. * amp[302] + 1./3. *
      amp[303] + amp[305] + amp[320] + amp[323] + Complex<double> (0, 1) *
      amp[324] + amp[327] + 1./3. * amp[330] + 1./3. * amp[331] + 1./3. *
      amp[332] + 1./3. * amp[333] - Complex<double> (0, 1) * amp[351] -
      amp[356] - amp[355] - amp[358] - amp[359] + Complex<double> (0, 1) *
      amp[362] + 1./3. * Complex<double> (0, 1) * amp[378] + 1./3. *
      Complex<double> (0, 1) * amp[379] + 1./3. * amp[382] - 1./3. *
      Complex<double> (0, 1) * amp[384] - 1./3. * Complex<double> (0, 1) *
      amp[385] + 1./3. * amp[388] - Complex<double> (0, 1) * amp[391] +
      Complex<double> (0, 1) * amp[392] + amp[394]);
  jamp[12] = +1./4. * (+1./3. * amp[306] + 1./3. * amp[307] + amp[311] + 1./3.
      * amp[313] + 1./3. * amp[314] + Complex<double> (0, 1) * amp[315] +
      amp[317] + amp[319] + amp[334] + amp[336] + Complex<double> (0, 1) *
      amp[338] + 1./3. * amp[339] + 1./3. * amp[340] + 1./3. * amp[344] + 1./3.
      * amp[345] + amp[346] + Complex<double> (0, 1) * amp[348] + amp[356] -
      amp[354] - amp[357] + amp[359] + Complex<double> (0, 1) * amp[361] -
      Complex<double> (0, 1) * amp[362] + amp[364] - Complex<double> (0, 1) *
      amp[392]);
  jamp[13] = +1./4. * (-1./3. * amp[264] - 1./3. * amp[265] - amp[267] -
      amp[269] + Complex<double> (0, 1) * amp[270] - amp[274] - 1./3. *
      amp[276] - 1./3. * amp[277] - amp[307] - amp[308] + Complex<double> (0,
      1) * amp[310] - 1./3. * amp[311] - 1./3. * amp[312] - amp[314] - 1./3. *
      amp[318] - 1./3. * amp[319] + Complex<double> (0, 1) * amp[367] -
      amp[374] + amp[372] + amp[375] - amp[377] + Complex<double> (0, 1) *
      amp[378] - Complex<double> (0, 1) * amp[381] - amp[383] - Complex<double>
      (0, 1) * amp[387]);
  jamp[14] = +1./4. * (-1./9. * amp[306] - 1./9. * amp[307] - 1./9. * amp[308]
      - 1./9. * amp[309] - 1./3. * amp[311] - 1./3. * amp[312] - 1./9. *
      amp[313] - 1./9. * amp[314] - 1./3. * amp[316] - 1./3. * amp[317] - 1./3.
      * amp[336] - 1./3. * amp[337] - 1./9. * amp[339] - 1./9. * amp[340] -
      1./9. * amp[341] - 1./9. * amp[342] - 1./9. * amp[344] - 1./9. * amp[345]
      - 1./3. * amp[346] - 1./3. * amp[347] - 1./3. * Complex<double> (0, 1) *
      amp[348] - 1./3. * Complex<double> (0, 1) * amp[349] - 1./3. * amp[352] -
      1./9. * amp[370] - 1./9. * amp[371] + 1./3. * Complex<double> (0, 1) *
      amp[392] + 1./3. * Complex<double> (0, 1) * amp[393] - 1./3. * amp[395]);
  jamp[15] = +1./4. * (+amp[292] + amp[294] + Complex<double> (0, 1) * amp[295]
      + 1./3. * amp[297] + 1./3. * amp[298] + 1./3. * amp[299] + 1./3. *
      amp[301] + amp[304] + 1./3. * amp[306] + 1./3. * amp[307] + 1./3. *
      amp[308] + 1./3. * amp[309] + amp[312] - Complex<double> (0, 1) *
      amp[315] + amp[316] + amp[318] + Complex<double> (0, 1) * amp[349] -
      Complex<double> (0, 1) * amp[350] + amp[352] + amp[355] + amp[354] +
      amp[357] + amp[358] + Complex<double> (0, 1) * amp[360] - 1./3. *
      Complex<double> (0, 1) * amp[368] - 1./3. * Complex<double> (0, 1) *
      amp[369] + 1./3. * amp[370] + 1./3. * Complex<double> (0, 1) * amp[380] +
      1./3. * Complex<double> (0, 1) * amp[381] + 1./3. * amp[383] -
      Complex<double> (0, 1) * amp[390]);
  jamp[16] = +1./4. * (+1./9. * amp[264] + 1./9. * amp[265] + 1./3. * amp[266]
      + 1./3. * amp[267] + 1./3. * amp[268] + 1./3. * amp[269] + 1./9. *
      amp[271] + 1./9. * amp[273] + 1./9. * amp[276] + 1./9. * amp[277] + 1./3.
      * amp[308] + 1./3. * amp[309] + 1./9. * amp[311] + 1./9. * amp[312] +
      1./3. * amp[313] + 1./3. * amp[314] + 1./9. * amp[316] + 1./9. * amp[317]
      + 1./9. * amp[318] + 1./9. * amp[319] - 1./3. * Complex<double> (0, 1) *
      amp[366] - 1./3. * Complex<double> (0, 1) * amp[367] + 1./3. * amp[371] +
      1./3. * Complex<double> (0, 1) * amp[386] + 1./3. * Complex<double> (0,
      1) * amp[387] + 1./3. * amp[388] + 1./9. * amp[394] + 1./9. * amp[395]);
  jamp[17] = +1./4. * (-1./3. * amp[294] - 1./3. * amp[296] - amp[298] +
      Complex<double> (0, 1) * amp[300] - amp[301] - amp[303] - 1./3. *
      amp[304] - 1./3. * amp[305] - amp[306] - amp[309] - Complex<double> (0,
      1) * amp[310] - amp[313] - 1./3. * amp[316] - 1./3. * amp[317] - 1./3. *
      amp[318] - 1./3. * amp[319] - 1./3. * Complex<double> (0, 1) * amp[360] -
      1./3. * Complex<double> (0, 1) * amp[361] - 1./3. * amp[364] +
      Complex<double> (0, 1) * amp[369] + amp[374] + amp[373] + amp[376] +
      amp[377] - Complex<double> (0, 1) * amp[380] + Complex<double> (0, 1) *
      amp[385] - Complex<double> (0, 1) * amp[386] - amp[388] + 1./3. *
      Complex<double> (0, 1) * amp[390] + 1./3. * Complex<double> (0, 1) *
      amp[391] - 1./3. * amp[394]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[2][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R16_P50_sm_qq_ttxgqq::matrix_16_uc_ttxguc() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 66;
  const int ncolor = 18; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
      1, 1, 1, 1, 1};
  static const double cf[ncolor][ncolor] = {{36, 12, 12, 0, 12, 4, 4, 0, 4, 12,
      0, 4, 0, 0, 12, 4, 4, 0}, {12, 36, 0, 12, 4, 12, 0, 4, 12, 4, 4, 0, 0, 0,
      4, 0, 12, 4}, {12, 0, 36, 12, 4, 0, 12, 4, 0, 4, 4, 12, 0, 0, 4, 12, 0,
      4}, {0, 12, 12, 36, 0, 4, 4, 12, 4, 0, 12, 4, 0, 0, 0, 4, 4, 12}, {12, 4,
      4, 0, 36, 12, 12, 0, 12, 4, 4, 0, 12, 4, 0, 0, 0, 4}, {4, 12, 0, 4, 12,
      36, 0, 12, 4, 12, 0, 4, 4, 0, 0, 0, 4, 12}, {4, 0, 12, 4, 12, 0, 36, 12,
      4, 0, 12, 4, 4, 12, 0, 0, 4, 0}, {0, 4, 4, 12, 0, 12, 12, 36, 0, 4, 4,
      12, 0, 4, 0, 0, 12, 4}, {4, 12, 0, 4, 12, 4, 4, 0, 36, 12, 12, 0, 4, 12,
      0, 4, 0, 0}, {12, 4, 4, 0, 4, 12, 0, 4, 12, 36, 0, 12, 0, 4, 4, 12, 0,
      0}, {0, 4, 4, 12, 4, 0, 12, 4, 12, 0, 36, 12, 12, 4, 4, 0, 0, 0}, {4, 0,
      12, 4, 0, 4, 4, 12, 0, 12, 12, 36, 4, 0, 12, 4, 0, 0}, {0, 0, 0, 0, 12,
      4, 4, 0, 4, 0, 12, 4, 36, 12, 12, 4, 4, 12}, {0, 0, 0, 0, 4, 0, 12, 4,
      12, 4, 4, 0, 12, 36, 4, 12, 12, 4}, {12, 4, 4, 0, 0, 0, 0, 0, 0, 4, 4,
      12, 12, 4, 36, 12, 12, 4}, {4, 0, 12, 4, 0, 0, 0, 0, 4, 12, 0, 4, 4, 12,
      12, 36, 4, 12}, {4, 12, 0, 4, 0, 4, 4, 12, 0, 0, 0, 0, 4, 12, 12, 4, 36,
      12}, {0, 4, 4, 12, 4, 12, 0, 4, 0, 0, 0, 0, 12, 4, 4, 12, 12, 36}};

  // Calculate color flows
  jamp[0] = +1./4. * (+amp[415] + Complex<double> (0, 1) * amp[417] + amp[419]
      + amp[421] + amp[429] + amp[431] + Complex<double> (0, 1) * amp[433] +
      amp[434] + Complex<double> (0, 1) * amp[436] + amp[444] - amp[442] -
      amp[445] + amp[447] + Complex<double> (0, 1) * amp[449] - Complex<double>
      (0, 1) * amp[450] + amp[452] - Complex<double> (0, 1) * amp[456]);
  jamp[1] = +1./4. * (-1./3. * amp[410] - 1./3. * amp[412] - 1./3. * amp[413] -
      1./3. * amp[414] - 1./3. * amp[418] - 1./3. * amp[419] - 1./3. * amp[420]
      - 1./3. * amp[421] - 1./3. * Complex<double> (0, 1) * amp[448] - 1./3. *
      Complex<double> (0, 1) * amp[449] - 1./3. * amp[452] + 1./3. *
      Complex<double> (0, 1) * amp[454] + 1./3. * Complex<double> (0, 1) *
      amp[455] - 1./3. * amp[458]);
  jamp[2] = +1./4. * (-1./3. * amp[422] - 1./3. * amp[423] - 1./3. * amp[427] -
      1./3. * amp[428] - 1./3. * amp[429] - 1./3. * amp[430] - 1./3. * amp[434]
      - 1./3. * amp[435]);
  jamp[3] = +1./4. * (+amp[409] - Complex<double> (0, 1) * amp[411] + amp[412]
      + amp[414] + amp[422] + amp[425] + Complex<double> (0, 1) * amp[426] +
      amp[427] - Complex<double> (0, 1) * amp[439] - amp[444] - amp[443] -
      amp[446] - amp[447] + Complex<double> (0, 1) * amp[450] - Complex<double>
      (0, 1) * amp[455] + Complex<double> (0, 1) * amp[456] + amp[458]);
  jamp[4] = +1./4. * (-1./3. * amp[0] - 1./3. * amp[1] - 1./3. * amp[399] -
      1./3. * amp[400] - 1./3. * amp[415] - 1./3. * amp[416] - 1./3. * amp[420]
      - 1./3. * amp[421]);
  jamp[5] = +1./4. * (+amp[408] + amp[410] + Complex<double> (0, 1) * amp[411]
      + amp[413] + amp[416] - Complex<double> (0, 1) * amp[417] + amp[418] +
      amp[420] + Complex<double> (0, 1) * amp[437] - Complex<double> (0, 1) *
      amp[438] + amp[440] + amp[443] + amp[442] + amp[445] + amp[446] +
      Complex<double> (0, 1) * amp[448] - Complex<double> (0, 1) * amp[454]);
  jamp[6] = +1./4. * (+amp[1] - Complex<double> (0, 1) * amp[397] + amp[398] +
      amp[399] + amp[423] + amp[424] - Complex<double> (0, 1) * amp[426] +
      amp[428] - Complex<double> (0, 1) * amp[437] + amp[444] - amp[442] -
      amp[445] + amp[447] - Complex<double> (0, 1) * amp[448] + Complex<double>
      (0, 1) * amp[451] + amp[453] + Complex<double> (0, 1) * amp[457]);
  jamp[7] = +1./4. * (-1./3. * amp[408] - 1./3. * amp[409] - 1./3. * amp[410] -
      1./3. * amp[412] - 1./3. * amp[422] - 1./3. * amp[423] - 1./3. * amp[424]
      - 1./3. * amp[425] + 1./3. * Complex<double> (0, 1) * amp[438] + 1./3. *
      Complex<double> (0, 1) * amp[439] - 1./3. * amp[440] - 1./3. *
      Complex<double> (0, 1) * amp[450] - 1./3. * Complex<double> (0, 1) *
      amp[451] - 1./3. * amp[453]);
  jamp[8] = +1./4. * (+1./9. * amp[0] + 1./9. * amp[1] + 1./9. * amp[396] +
      1./9. * amp[398] + 1./9. * amp[399] + 1./9. * amp[400] + 1./9. * amp[415]
      + 1./9. * amp[416] + 1./9. * amp[418] + 1./9. * amp[419] + 1./9. *
      amp[420] + 1./9. * amp[421] + 1./9. * amp[458] + 1./9. * amp[459]);
  jamp[9] = +1./4. * (-1./3. * amp[415] - 1./3. * amp[416] - 1./3. * amp[418] -
      1./3. * amp[419] - 1./3. * amp[431] - 1./3. * amp[432] - 1./3. * amp[434]
      - 1./3. * amp[435] - 1./3. * Complex<double> (0, 1) * amp[436] - 1./3. *
      Complex<double> (0, 1) * amp[437] - 1./3. * amp[440] + 1./3. *
      Complex<double> (0, 1) * amp[456] + 1./3. * Complex<double> (0, 1) *
      amp[457] - 1./3. * amp[459]);
  jamp[10] = +1./4. * (-1./3. * amp[0] - 1./3. * amp[1] - 1./3. * amp[396] -
      1./3. * amp[398] - 1./3. * amp[424] - 1./3. * amp[425] - 1./3. * amp[427]
      - 1./3. * amp[428] + 1./3. * Complex<double> (0, 1) * amp[436] + 1./3. *
      Complex<double> (0, 1) * amp[437] - 1./3. * amp[441] - 1./3. *
      Complex<double> (0, 1) * amp[456] - 1./3. * Complex<double> (0, 1) *
      amp[457] - 1./3. * amp[458]);
  jamp[11] = +1./4. * (+1./9. * amp[422] + 1./9. * amp[423] + 1./9. * amp[424]
      + 1./9. * amp[425] + 1./9. * amp[427] + 1./9. * amp[428] + 1./9. *
      amp[429] + 1./9. * amp[430] + 1./9. * amp[431] + 1./9. * amp[432] + 1./9.
      * amp[434] + 1./9. * amp[435] + 1./9. * amp[440] + 1./9. * amp[441]);
  jamp[12] = +1./4. * (+amp[0] + amp[396] + Complex<double> (0, 1) * amp[397] +
      amp[400] + amp[402] + amp[403] - Complex<double> (0, 1) * amp[404] +
      amp[407] - Complex<double> (0, 1) * amp[436] + Complex<double> (0, 1) *
      amp[439] + amp[441] + amp[443] + amp[442] + amp[445] + amp[446] -
      Complex<double> (0, 1) * amp[449] + Complex<double> (0, 1) * amp[455]);
  jamp[13] = +1./4. * (-1./3. * amp[396] - 1./3. * amp[398] - 1./3. * amp[399]
      - 1./3. * amp[400] - 1./3. * amp[403] - 1./3. * amp[405] - 1./3. *
      amp[406] - 1./3. * amp[407] + 1./3. * Complex<double> (0, 1) * amp[448] +
      1./3. * Complex<double> (0, 1) * amp[449] - 1./3. * amp[453] - 1./3. *
      Complex<double> (0, 1) * amp[454] - 1./3. * Complex<double> (0, 1) *
      amp[455] - 1./3. * amp[459]);
  jamp[14] = +1./4. * (-1./3. * amp[401] - 1./3. * amp[402] - 1./3. * amp[403]
      - 1./3. * amp[405] - 1./3. * amp[429] - 1./3. * amp[430] - 1./3. *
      amp[431] - 1./3. * amp[432] - 1./3. * Complex<double> (0, 1) * amp[438] -
      1./3. * Complex<double> (0, 1) * amp[439] - 1./3. * amp[441] + 1./3. *
      Complex<double> (0, 1) * amp[450] + 1./3. * Complex<double> (0, 1) *
      amp[451] - 1./3. * amp[452]);
  jamp[15] = +1./4. * (+amp[401] + Complex<double> (0, 1) * amp[404] + amp[405]
      + amp[406] + amp[430] + amp[432] - Complex<double> (0, 1) * amp[433] +
      amp[435] + Complex<double> (0, 1) * amp[438] - amp[444] - amp[443] -
      amp[446] - amp[447] - Complex<double> (0, 1) * amp[451] + Complex<double>
      (0, 1) * amp[454] - Complex<double> (0, 1) * amp[457] + amp[459]);
  jamp[16] = +1./4. * (+1./9. * amp[401] + 1./9. * amp[402] + 1./9. * amp[403]
      + 1./9. * amp[405] + 1./9. * amp[406] + 1./9. * amp[407] + 1./9. *
      amp[408] + 1./9. * amp[409] + 1./9. * amp[410] + 1./9. * amp[412] + 1./9.
      * amp[413] + 1./9. * amp[414] + 1./9. * amp[452] + 1./9. * amp[453]);
  jamp[17] = +1./4. * (-1./3. * amp[401] - 1./3. * amp[402] - 1./3. * amp[406]
      - 1./3. * amp[407] - 1./3. * amp[408] - 1./3. * amp[409] - 1./3. *
      amp[413] - 1./3. * amp[414]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[3][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R16_P50_sm_qq_ttxgqq::matrix_16_uux_ttxgccx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 66;
  const int ncolor = 18; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
      1, 1, 1, 1, 1};
  static const double cf[ncolor][ncolor] = {{36, 12, 0, 12, 12, 4, 0, 4, 4, 12,
      4, 0, 0, 4, 0, 12, 0, 4}, {12, 36, 12, 0, 4, 12, 4, 0, 12, 4, 0, 4, 4, 0,
      0, 4, 0, 12}, {0, 12, 36, 12, 0, 4, 12, 4, 4, 0, 4, 12, 12, 4, 0, 0, 0,
      4}, {12, 0, 12, 36, 4, 0, 4, 12, 0, 4, 12, 4, 4, 12, 0, 4, 0, 0}, {12, 4,
      0, 4, 36, 12, 0, 12, 12, 4, 0, 4, 0, 12, 0, 4, 4, 0}, {4, 12, 4, 0, 12,
      36, 12, 0, 4, 12, 4, 0, 0, 4, 4, 0, 12, 0}, {0, 4, 12, 4, 0, 12, 36, 12,
      0, 4, 12, 4, 0, 0, 12, 4, 4, 0}, {4, 0, 4, 12, 12, 0, 12, 36, 4, 0, 4,
      12, 0, 4, 4, 12, 0, 0}, {4, 12, 4, 0, 12, 4, 0, 4, 36, 12, 0, 12, 12, 0,
      4, 0, 0, 4}, {12, 4, 0, 4, 4, 12, 4, 0, 12, 36, 12, 0, 4, 0, 12, 0, 4,
      0}, {4, 0, 4, 12, 0, 4, 12, 4, 0, 12, 36, 12, 0, 0, 4, 0, 12, 4}, {0, 4,
      12, 4, 4, 0, 4, 12, 12, 0, 12, 36, 4, 0, 0, 0, 4, 12}, {0, 4, 12, 4, 0,
      0, 0, 0, 12, 4, 0, 4, 36, 12, 12, 4, 4, 12}, {4, 0, 4, 12, 12, 4, 0, 4,
      0, 0, 0, 0, 12, 36, 4, 12, 12, 4}, {0, 0, 0, 0, 0, 4, 12, 4, 4, 12, 4, 0,
      12, 4, 36, 12, 12, 4}, {12, 4, 0, 4, 4, 0, 4, 12, 0, 0, 0, 0, 4, 12, 12,
      36, 4, 12}, {0, 0, 0, 0, 4, 12, 4, 0, 0, 4, 12, 4, 4, 12, 12, 4, 36, 12},
      {4, 12, 4, 0, 0, 0, 0, 0, 4, 0, 4, 12, 12, 4, 4, 12, 12, 36}};

  // Calculate color flows
  jamp[0] = +1./4. * (+1./9. * amp[488] + 1./9. * amp[489] + 1./9. * amp[490] +
      1./9. * amp[491] + 1./9. * amp[493] + 1./9. * amp[494] + 1./9. * amp[495]
      + 1./9. * amp[496] + 1./9. * amp[497] + 1./9. * amp[498] + 1./9. *
      amp[500] + 1./9. * amp[501] + 1./9. * amp[506] + 1./9. * amp[507]);
  jamp[1] = +1./4. * (-1./3. * amp[474] - 1./3. * amp[475] - 1./3. * amp[476] -
      1./3. * amp[478] - 1./3. * amp[488] - 1./3. * amp[489] - 1./3. * amp[490]
      - 1./3. * amp[491] + 1./3. * Complex<double> (0, 1) * amp[504] + 1./3. *
      Complex<double> (0, 1) * amp[505] - 1./3. * amp[506] - 1./3. *
      Complex<double> (0, 1) * amp[516] - 1./3. * Complex<double> (0, 1) *
      amp[517] - 1./3. * amp[519]);
  jamp[2] = +1./4. * (+1./9. * amp[467] + 1./9. * amp[468] + 1./9. * amp[469] +
      1./9. * amp[471] + 1./9. * amp[472] + 1./9. * amp[473] + 1./9. * amp[474]
      + 1./9. * amp[475] + 1./9. * amp[476] + 1./9. * amp[478] + 1./9. *
      amp[479] + 1./9. * amp[480] + 1./9. * amp[518] + 1./9. * amp[519]);
  jamp[3] = +1./4. * (-1./3. * amp[467] - 1./3. * amp[468] - 1./3. * amp[469] -
      1./3. * amp[471] - 1./3. * amp[495] - 1./3. * amp[496] - 1./3. * amp[497]
      - 1./3. * amp[498] - 1./3. * Complex<double> (0, 1) * amp[504] - 1./3. *
      Complex<double> (0, 1) * amp[505] - 1./3. * amp[507] + 1./3. *
      Complex<double> (0, 1) * amp[516] + 1./3. * Complex<double> (0, 1) *
      amp[517] - 1./3. * amp[518]);
  jamp[4] = +1./4. * (-1./3. * amp[488] - 1./3. * amp[489] - 1./3. * amp[493] -
      1./3. * amp[494] - 1./3. * amp[495] - 1./3. * amp[496] - 1./3. * amp[500]
      - 1./3. * amp[501]);
  jamp[5] = +1./4. * (+amp[461] - Complex<double> (0, 1) * amp[463] + amp[464]
      + amp[465] + amp[489] + amp[490] - Complex<double> (0, 1) * amp[492] +
      amp[494] - Complex<double> (0, 1) * amp[503] + amp[510] - amp[508] -
      amp[511] + amp[513] - Complex<double> (0, 1) * amp[514] + Complex<double>
      (0, 1) * amp[517] + amp[519] + Complex<double> (0, 1) * amp[523]);
  jamp[6] = +1./4. * (-1./3. * amp[462] - 1./3. * amp[464] - 1./3. * amp[465] -
      1./3. * amp[466] - 1./3. * amp[469] - 1./3. * amp[471] - 1./3. * amp[472]
      - 1./3. * amp[473] + 1./3. * Complex<double> (0, 1) * amp[514] + 1./3. *
      Complex<double> (0, 1) * amp[515] - 1./3. * amp[519] - 1./3. *
      Complex<double> (0, 1) * amp[520] - 1./3. * Complex<double> (0, 1) *
      amp[521] - 1./3. * amp[525]);
  jamp[7] = +1./4. * (+amp[467] + Complex<double> (0, 1) * amp[470] + amp[471]
      + amp[472] + amp[496] + amp[498] - Complex<double> (0, 1) * amp[499] +
      amp[501] + Complex<double> (0, 1) * amp[504] - amp[510] - amp[509] -
      amp[512] - amp[513] - Complex<double> (0, 1) * amp[517] + Complex<double>
      (0, 1) * amp[520] - Complex<double> (0, 1) * amp[523] + amp[525]);
  jamp[8] = +1./4. * (+amp[475] - Complex<double> (0, 1) * amp[477] + amp[478]
      + amp[480] + amp[488] + amp[491] + Complex<double> (0, 1) * amp[492] +
      amp[493] - Complex<double> (0, 1) * amp[505] - amp[510] - amp[509] -
      amp[512] - amp[513] + Complex<double> (0, 1) * amp[516] - Complex<double>
      (0, 1) * amp[521] + Complex<double> (0, 1) * amp[522] + amp[524]);
  jamp[9] = +1./4. * (-1./3. * amp[460] - 1./3. * amp[461] - 1./3. * amp[462] -
      1./3. * amp[464] - 1./3. * amp[490] - 1./3. * amp[491] - 1./3. * amp[493]
      - 1./3. * amp[494] + 1./3. * Complex<double> (0, 1) * amp[502] + 1./3. *
      Complex<double> (0, 1) * amp[503] - 1./3. * amp[507] - 1./3. *
      Complex<double> (0, 1) * amp[522] - 1./3. * Complex<double> (0, 1) *
      amp[523] - 1./3. * amp[524]);
  jamp[10] = +1./4. * (+amp[460] + amp[462] + Complex<double> (0, 1) * amp[463]
      + amp[466] + amp[468] + amp[469] - Complex<double> (0, 1) * amp[470] +
      amp[473] - Complex<double> (0, 1) * amp[502] + Complex<double> (0, 1) *
      amp[505] + amp[507] + amp[509] + amp[508] + amp[511] + amp[512] -
      Complex<double> (0, 1) * amp[515] + Complex<double> (0, 1) * amp[521]);
  jamp[11] = +1./4. * (-1./3. * amp[467] - 1./3. * amp[468] - 1./3. * amp[472]
      - 1./3. * amp[473] - 1./3. * amp[474] - 1./3. * amp[475] - 1./3. *
      amp[479] - 1./3. * amp[480]);
  jamp[12] = +1./4. * (-1./3. * amp[476] - 1./3. * amp[478] - 1./3. * amp[479]
      - 1./3. * amp[480] - 1./3. * amp[484] - 1./3. * amp[485] - 1./3. *
      amp[486] - 1./3. * amp[487] - 1./3. * Complex<double> (0, 1) * amp[514] -
      1./3. * Complex<double> (0, 1) * amp[515] - 1./3. * amp[518] + 1./3. *
      Complex<double> (0, 1) * amp[520] + 1./3. * Complex<double> (0, 1) *
      amp[521] - 1./3. * amp[524]);
  jamp[13] = +1./4. * (+amp[481] + Complex<double> (0, 1) * amp[483] + amp[485]
      + amp[487] + amp[495] + amp[497] + Complex<double> (0, 1) * amp[499] +
      amp[500] + Complex<double> (0, 1) * amp[502] + amp[510] - amp[508] -
      amp[511] + amp[513] + Complex<double> (0, 1) * amp[515] - Complex<double>
      (0, 1) * amp[516] + amp[518] - Complex<double> (0, 1) * amp[522]);
  jamp[14] = +1./4. * (+1./9. * amp[460] + 1./9. * amp[461] + 1./9. * amp[462]
      + 1./9. * amp[464] + 1./9. * amp[465] + 1./9. * amp[466] + 1./9. *
      amp[481] + 1./9. * amp[482] + 1./9. * amp[484] + 1./9. * amp[485] + 1./9.
      * amp[486] + 1./9. * amp[487] + 1./9. * amp[524] + 1./9. * amp[525]);
  jamp[15] = +1./4. * (-1./3. * amp[481] - 1./3. * amp[482] - 1./3. * amp[484]
      - 1./3. * amp[485] - 1./3. * amp[497] - 1./3. * amp[498] - 1./3. *
      amp[500] - 1./3. * amp[501] - 1./3. * Complex<double> (0, 1) * amp[502] -
      1./3. * Complex<double> (0, 1) * amp[503] - 1./3. * amp[506] + 1./3. *
      Complex<double> (0, 1) * amp[522] + 1./3. * Complex<double> (0, 1) *
      amp[523] - 1./3. * amp[525]);
  jamp[16] = +1./4. * (-1./3. * amp[460] - 1./3. * amp[461] - 1./3. * amp[465]
      - 1./3. * amp[466] - 1./3. * amp[481] - 1./3. * amp[482] - 1./3. *
      amp[486] - 1./3. * amp[487]);
  jamp[17] = +1./4. * (+amp[474] + amp[476] + Complex<double> (0, 1) * amp[477]
      + amp[479] + amp[482] - Complex<double> (0, 1) * amp[483] + amp[484] +
      amp[486] + Complex<double> (0, 1) * amp[503] - Complex<double> (0, 1) *
      amp[504] + amp[506] + amp[509] + amp[508] + amp[511] + amp[512] +
      Complex<double> (0, 1) * amp[514] - Complex<double> (0, 1) * amp[520]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[4][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R16_P50_sm_qq_ttxgqq::matrix_16_ucx_ttxgucx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 66;
  const int ncolor = 18; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
      1, 1, 1, 1, 1};
  static const double cf[ncolor][ncolor] = {{36, 12, 0, 12, 12, 4, 0, 4, 4, 12,
      4, 0, 0, 4, 0, 12, 0, 4}, {12, 36, 12, 0, 4, 12, 4, 0, 12, 4, 0, 4, 4, 0,
      0, 4, 0, 12}, {0, 12, 36, 12, 0, 4, 12, 4, 4, 0, 4, 12, 12, 4, 0, 0, 0,
      4}, {12, 0, 12, 36, 4, 0, 4, 12, 0, 4, 12, 4, 4, 12, 0, 4, 0, 0}, {12, 4,
      0, 4, 36, 12, 0, 12, 12, 4, 0, 4, 0, 12, 0, 4, 4, 0}, {4, 12, 4, 0, 12,
      36, 12, 0, 4, 12, 4, 0, 0, 4, 4, 0, 12, 0}, {0, 4, 12, 4, 0, 12, 36, 12,
      0, 4, 12, 4, 0, 0, 12, 4, 4, 0}, {4, 0, 4, 12, 12, 0, 12, 36, 4, 0, 4,
      12, 0, 4, 4, 12, 0, 0}, {4, 12, 4, 0, 12, 4, 0, 4, 36, 12, 0, 12, 12, 0,
      4, 0, 0, 4}, {12, 4, 0, 4, 4, 12, 4, 0, 12, 36, 12, 0, 4, 0, 12, 0, 4,
      0}, {4, 0, 4, 12, 0, 4, 12, 4, 0, 12, 36, 12, 0, 0, 4, 0, 12, 4}, {0, 4,
      12, 4, 4, 0, 4, 12, 12, 0, 12, 36, 4, 0, 0, 0, 4, 12}, {0, 4, 12, 4, 0,
      0, 0, 0, 12, 4, 0, 4, 36, 12, 12, 4, 4, 12}, {4, 0, 4, 12, 12, 4, 0, 4,
      0, 0, 0, 0, 12, 36, 4, 12, 12, 4}, {0, 0, 0, 0, 0, 4, 12, 4, 4, 12, 4, 0,
      12, 4, 36, 12, 12, 4}, {12, 4, 0, 4, 4, 0, 4, 12, 0, 0, 0, 0, 4, 12, 12,
      36, 4, 12}, {0, 0, 0, 0, 4, 12, 4, 0, 0, 4, 12, 4, 4, 12, 12, 4, 36, 12},
      {4, 12, 4, 0, 0, 0, 0, 0, 4, 0, 4, 12, 12, 4, 4, 12, 12, 36}};

  // Calculate color flows
  jamp[0] = +1./4. * (+1./3. * amp[547] + 1./3. * amp[548] + 1./3. * amp[550] +
      1./3. * amp[551] + 1./3. * amp[563] + 1./3. * amp[564] + 1./3. * amp[566]
      + 1./3. * amp[567] + 1./3. * Complex<double> (0, 1) * amp[568] + 1./3. *
      Complex<double> (0, 1) * amp[569] + 1./3. * amp[572] - 1./3. *
      Complex<double> (0, 1) * amp[588] - 1./3. * Complex<double> (0, 1) *
      amp[589] + 1./3. * amp[591]);
  jamp[1] = +1./4. * (-amp[540] - amp[542] - Complex<double> (0, 1) * amp[543]
      - amp[545] - amp[548] + Complex<double> (0, 1) * amp[549] - amp[550] -
      amp[552] - Complex<double> (0, 1) * amp[569] + Complex<double> (0, 1) *
      amp[570] - amp[572] - amp[575] - amp[574] - amp[577] - amp[578] -
      Complex<double> (0, 1) * amp[580] + Complex<double> (0, 1) * amp[586]);
  jamp[2] = +1./4. * (+1./3. * amp[533] + 1./3. * amp[534] + 1./3. * amp[538] +
      1./3. * amp[539] + 1./3. * amp[540] + 1./3. * amp[541] + 1./3. * amp[545]
      + 1./3. * amp[546]);
  jamp[3] = +1./4. * (-amp[533] - Complex<double> (0, 1) * amp[536] - amp[537]
      - amp[538] - amp[562] - amp[564] + Complex<double> (0, 1) * amp[565] -
      amp[567] - Complex<double> (0, 1) * amp[570] + amp[576] + amp[575] +
      amp[578] + amp[579] + Complex<double> (0, 1) * amp[583] - Complex<double>
      (0, 1) * amp[586] + Complex<double> (0, 1) * amp[589] - amp[591]);
  jamp[4] = +1./4. * (-amp[547] - Complex<double> (0, 1) * amp[549] - amp[551]
      - amp[553] - amp[561] - amp[563] - Complex<double> (0, 1) * amp[565] -
      amp[566] - Complex<double> (0, 1) * amp[568] - amp[576] + amp[574] +
      amp[577] - amp[579] - Complex<double> (0, 1) * amp[581] + Complex<double>
      (0, 1) * amp[582] - amp[584] + Complex<double> (0, 1) * amp[588]);
  jamp[5] = +1./4. * (+1./3. * amp[526] + 1./3. * amp[527] + 1./3. * amp[531] +
      1./3. * amp[532] + 1./3. * amp[547] + 1./3. * amp[548] + 1./3. * amp[552]
      + 1./3. * amp[553]);
  jamp[6] = +1./4. * (-amp[526] - amp[528] - Complex<double> (0, 1) * amp[529]
      - amp[532] - amp[534] - amp[535] + Complex<double> (0, 1) * amp[536] -
      amp[539] + Complex<double> (0, 1) * amp[568] - Complex<double> (0, 1) *
      amp[571] - amp[573] - amp[575] - amp[574] - amp[577] - amp[578] +
      Complex<double> (0, 1) * amp[581] - Complex<double> (0, 1) * amp[587]);
  jamp[7] = +1./4. * (+1./3. * amp[533] + 1./3. * amp[534] + 1./3. * amp[535] +
      1./3. * amp[537] + 1./3. * amp[561] + 1./3. * amp[562] + 1./3. * amp[563]
      + 1./3. * amp[564] + 1./3. * Complex<double> (0, 1) * amp[570] + 1./3. *
      Complex<double> (0, 1) * amp[571] + 1./3. * amp[573] - 1./3. *
      Complex<double> (0, 1) * amp[582] - 1./3. * Complex<double> (0, 1) *
      amp[583] + 1./3. * amp[584]);
  jamp[8] = +1./4. * (+1./3. * amp[542] + 1./3. * amp[544] + 1./3. * amp[545] +
      1./3. * amp[546] + 1./3. * amp[550] + 1./3. * amp[551] + 1./3. * amp[552]
      + 1./3. * amp[553] + 1./3. * Complex<double> (0, 1) * amp[580] + 1./3. *
      Complex<double> (0, 1) * amp[581] + 1./3. * amp[584] - 1./3. *
      Complex<double> (0, 1) * amp[586] - 1./3. * Complex<double> (0, 1) *
      amp[587] + 1./3. * amp[590]);
  jamp[9] = +1./4. * (-1./9. * amp[526] - 1./9. * amp[527] - 1./9. * amp[528] -
      1./9. * amp[530] - 1./9. * amp[531] - 1./9. * amp[532] - 1./9. * amp[547]
      - 1./9. * amp[548] - 1./9. * amp[550] - 1./9. * amp[551] - 1./9. *
      amp[552] - 1./9. * amp[553] - 1./9. * amp[590] - 1./9. * amp[591]);
  jamp[10] = +1./4. * (+1./3. * amp[528] + 1./3. * amp[530] + 1./3. * amp[531]
      + 1./3. * amp[532] + 1./3. * amp[535] + 1./3. * amp[537] + 1./3. *
      amp[538] + 1./3. * amp[539] - 1./3. * Complex<double> (0, 1) * amp[580] -
      1./3. * Complex<double> (0, 1) * amp[581] + 1./3. * amp[585] + 1./3. *
      Complex<double> (0, 1) * amp[586] + 1./3. * Complex<double> (0, 1) *
      amp[587] + 1./3. * amp[591]);
  jamp[11] = +1./4. * (-1./9. * amp[533] - 1./9. * amp[534] - 1./9. * amp[535]
      - 1./9. * amp[537] - 1./9. * amp[538] - 1./9. * amp[539] - 1./9. *
      amp[540] - 1./9. * amp[541] - 1./9. * amp[542] - 1./9. * amp[544] - 1./9.
      * amp[545] - 1./9. * amp[546] - 1./9. * amp[584] - 1./9. * amp[585]);
  jamp[12] = +1./4. * (-amp[541] + Complex<double> (0, 1) * amp[543] - amp[544]
      - amp[546] - amp[554] - amp[557] - Complex<double> (0, 1) * amp[558] -
      amp[559] + Complex<double> (0, 1) * amp[571] + amp[576] + amp[575] +
      amp[578] + amp[579] - Complex<double> (0, 1) * amp[582] + Complex<double>
      (0, 1) * amp[587] - Complex<double> (0, 1) * amp[588] - amp[590]);
  jamp[13] = +1./4. * (+1./3. * amp[554] + 1./3. * amp[555] + 1./3. * amp[559]
      + 1./3. * amp[560] + 1./3. * amp[561] + 1./3. * amp[562] + 1./3. *
      amp[566] + 1./3. * amp[567]);
  jamp[14] = +1./4. * (+1./3. * amp[526] + 1./3. * amp[527] + 1./3. * amp[528]
      + 1./3. * amp[530] + 1./3. * amp[556] + 1./3. * amp[557] + 1./3. *
      amp[559] + 1./3. * amp[560] - 1./3. * Complex<double> (0, 1) * amp[568] -
      1./3. * Complex<double> (0, 1) * amp[569] + 1./3. * amp[573] + 1./3. *
      Complex<double> (0, 1) * amp[588] + 1./3. * Complex<double> (0, 1) *
      amp[589] + 1./3. * amp[590]);
  jamp[15] = +1./4. * (-1./9. * amp[554] - 1./9. * amp[555] - 1./9. * amp[556]
      - 1./9. * amp[557] - 1./9. * amp[559] - 1./9. * amp[560] - 1./9. *
      amp[561] - 1./9. * amp[562] - 1./9. * amp[563] - 1./9. * amp[564] - 1./9.
      * amp[566] - 1./9. * amp[567] - 1./9. * amp[572] - 1./9. * amp[573]);
  jamp[16] = +1./4. * (-amp[527] + Complex<double> (0, 1) * amp[529] - amp[530]
      - amp[531] - amp[555] - amp[556] + Complex<double> (0, 1) * amp[558] -
      amp[560] + Complex<double> (0, 1) * amp[569] - amp[576] + amp[574] +
      amp[577] - amp[579] + Complex<double> (0, 1) * amp[580] - Complex<double>
      (0, 1) * amp[583] - amp[585] - Complex<double> (0, 1) * amp[589]);
  jamp[17] = +1./4. * (+1./3. * amp[540] + 1./3. * amp[541] + 1./3. * amp[542]
      + 1./3. * amp[544] + 1./3. * amp[554] + 1./3. * amp[555] + 1./3. *
      amp[556] + 1./3. * amp[557] - 1./3. * Complex<double> (0, 1) * amp[570] -
      1./3. * Complex<double> (0, 1) * amp[571] + 1./3. * amp[572] + 1./3. *
      Complex<double> (0, 1) * amp[582] + 1./3. * Complex<double> (0, 1) *
      amp[583] + 1./3. * amp[585]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[5][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R16_P50_sm_qq_ttxgqq::matrix_16_uxcx_ttxguxcx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 66;
  const int ncolor = 18; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
      1, 1, 1, 1, 1};
  static const double cf[ncolor][ncolor] = {{36, 12, 12, 0, 12, 4, 4, 0, 4, 12,
      0, 4, 4, 0, 12, 0, 4, 0}, {12, 36, 0, 12, 4, 12, 0, 4, 12, 4, 4, 0, 0, 4,
      4, 0, 12, 0}, {12, 0, 36, 12, 4, 0, 12, 4, 0, 4, 4, 12, 12, 4, 4, 0, 0,
      0}, {0, 12, 12, 36, 0, 4, 4, 12, 4, 0, 12, 4, 4, 12, 0, 0, 4, 0}, {12, 4,
      4, 0, 36, 12, 12, 0, 12, 4, 4, 0, 12, 0, 4, 0, 0, 4}, {4, 12, 0, 4, 12,
      36, 0, 12, 4, 12, 0, 4, 4, 0, 0, 4, 0, 12}, {4, 0, 12, 4, 12, 0, 36, 12,
      4, 0, 12, 4, 4, 0, 12, 4, 0, 0}, {0, 4, 4, 12, 0, 12, 12, 36, 0, 4, 4,
      12, 0, 0, 4, 12, 0, 4}, {4, 12, 0, 4, 12, 4, 4, 0, 36, 12, 12, 0, 0, 12,
      0, 4, 4, 0}, {12, 4, 4, 0, 4, 12, 0, 4, 12, 36, 0, 12, 0, 4, 0, 12, 0,
      4}, {0, 4, 4, 12, 4, 0, 12, 4, 12, 0, 36, 12, 0, 4, 0, 0, 12, 4}, {4, 0,
      12, 4, 0, 4, 4, 12, 0, 12, 12, 36, 0, 0, 0, 4, 4, 12}, {4, 0, 12, 4, 12,
      4, 4, 0, 0, 0, 0, 0, 36, 12, 12, 4, 4, 12}, {0, 4, 4, 12, 0, 0, 0, 0, 12,
      4, 4, 0, 12, 36, 4, 12, 12, 4}, {12, 4, 4, 0, 4, 0, 12, 4, 0, 0, 0, 0,
      12, 4, 36, 12, 12, 4}, {0, 0, 0, 0, 0, 4, 4, 12, 4, 12, 0, 4, 4, 12, 12,
      36, 4, 12}, {4, 12, 0, 4, 0, 0, 0, 0, 4, 0, 12, 4, 4, 12, 12, 4, 36, 12},
      {0, 0, 0, 0, 4, 12, 0, 4, 0, 4, 4, 12, 12, 4, 4, 12, 12, 36}};

  // Calculate color flows
  jamp[0] = +1./4. * (+amp[599] + Complex<double> (0, 1) * amp[602] + amp[603]
      + amp[604] + amp[628] + amp[630] - Complex<double> (0, 1) * amp[631] +
      amp[633] + Complex<double> (0, 1) * amp[636] - amp[642] - amp[641] -
      amp[644] - amp[645] - Complex<double> (0, 1) * amp[649] + Complex<double>
      (0, 1) * amp[652] - Complex<double> (0, 1) * amp[655] + amp[657]);
  jamp[1] = +1./4. * (-1./3. * amp[594] - 1./3. * amp[596] - 1./3. * amp[597] -
      1./3. * amp[598] - 1./3. * amp[601] - 1./3. * amp[603] - 1./3. * amp[604]
      - 1./3. * amp[605] + 1./3. * Complex<double> (0, 1) * amp[646] + 1./3. *
      Complex<double> (0, 1) * amp[647] - 1./3. * amp[651] - 1./3. *
      Complex<double> (0, 1) * amp[652] - 1./3. * Complex<double> (0, 1) *
      amp[653] - 1./3. * amp[657]);
  jamp[2] = +1./4. * (-1./3. * amp[620] - 1./3. * amp[621] - 1./3. * amp[625] -
      1./3. * amp[626] - 1./3. * amp[627] - 1./3. * amp[628] - 1./3. * amp[632]
      - 1./3. * amp[633]);
  jamp[3] = +1./4. * (+amp[593] - Complex<double> (0, 1) * amp[595] + amp[596]
      + amp[597] + amp[621] + amp[622] - Complex<double> (0, 1) * amp[624] +
      amp[626] - Complex<double> (0, 1) * amp[635] + amp[642] - amp[640] -
      amp[643] + amp[645] - Complex<double> (0, 1) * amp[646] + Complex<double>
      (0, 1) * amp[649] + amp[651] + Complex<double> (0, 1) * amp[655]);
  jamp[4] = +1./4. * (-1./3. * amp[599] - 1./3. * amp[600] - 1./3. * amp[601] -
      1./3. * amp[603] - 1./3. * amp[627] - 1./3. * amp[628] - 1./3. * amp[629]
      - 1./3. * amp[630] - 1./3. * Complex<double> (0, 1) * amp[636] - 1./3. *
      Complex<double> (0, 1) * amp[637] - 1./3. * amp[639] + 1./3. *
      Complex<double> (0, 1) * amp[648] + 1./3. * Complex<double> (0, 1) *
      amp[649] - 1./3. * amp[650]);
  jamp[5] = +1./4. * (+1./9. * amp[599] + 1./9. * amp[600] + 1./9. * amp[601] +
      1./9. * amp[603] + 1./9. * amp[604] + 1./9. * amp[605] + 1./9. * amp[606]
      + 1./9. * amp[607] + 1./9. * amp[608] + 1./9. * amp[610] + 1./9. *
      amp[611] + 1./9. * amp[612] + 1./9. * amp[650] + 1./9. * amp[651]);
  jamp[6] = +1./4. * (+1./9. * amp[620] + 1./9. * amp[621] + 1./9. * amp[622] +
      1./9. * amp[623] + 1./9. * amp[625] + 1./9. * amp[626] + 1./9. * amp[627]
      + 1./9. * amp[628] + 1./9. * amp[629] + 1./9. * amp[630] + 1./9. *
      amp[632] + 1./9. * amp[633] + 1./9. * amp[638] + 1./9. * amp[639]);
  jamp[7] = +1./4. * (-1./3. * amp[606] - 1./3. * amp[607] - 1./3. * amp[608] -
      1./3. * amp[610] - 1./3. * amp[620] - 1./3. * amp[621] - 1./3. * amp[622]
      - 1./3. * amp[623] + 1./3. * Complex<double> (0, 1) * amp[636] + 1./3. *
      Complex<double> (0, 1) * amp[637] - 1./3. * amp[638] - 1./3. *
      Complex<double> (0, 1) * amp[648] - 1./3. * Complex<double> (0, 1) *
      amp[649] - 1./3. * amp[651]);
  jamp[8] = +1./4. * (+amp[592] + amp[594] + Complex<double> (0, 1) * amp[595]
      + amp[598] + amp[600] + amp[601] - Complex<double> (0, 1) * amp[602] +
      amp[605] - Complex<double> (0, 1) * amp[634] + Complex<double> (0, 1) *
      amp[637] + amp[639] + amp[641] + amp[640] + amp[643] + amp[644] -
      Complex<double> (0, 1) * amp[647] + Complex<double> (0, 1) * amp[653]);
  jamp[9] = +1./4. * (-1./3. * amp[599] - 1./3. * amp[600] - 1./3. * amp[604] -
      1./3. * amp[605] - 1./3. * amp[606] - 1./3. * amp[607] - 1./3. * amp[611]
      - 1./3. * amp[612]);
  jamp[10] = +1./4. * (-1./3. * amp[592] - 1./3. * amp[593] - 1./3. * amp[594]
      - 1./3. * amp[596] - 1./3. * amp[622] - 1./3. * amp[623] - 1./3. *
      amp[625] - 1./3. * amp[626] + 1./3. * Complex<double> (0, 1) * amp[634] +
      1./3. * Complex<double> (0, 1) * amp[635] - 1./3. * amp[639] - 1./3. *
      Complex<double> (0, 1) * amp[654] - 1./3. * Complex<double> (0, 1) *
      amp[655] - 1./3. * amp[656]);
  jamp[11] = +1./4. * (+amp[607] - Complex<double> (0, 1) * amp[609] + amp[610]
      + amp[612] + amp[620] + amp[623] + Complex<double> (0, 1) * amp[624] +
      amp[625] - Complex<double> (0, 1) * amp[637] - amp[642] - amp[641] -
      amp[644] - amp[645] + Complex<double> (0, 1) * amp[648] - Complex<double>
      (0, 1) * amp[653] + Complex<double> (0, 1) * amp[654] + amp[656]);
  jamp[12] = +1./4. * (+amp[613] + Complex<double> (0, 1) * amp[615] + amp[617]
      + amp[619] + amp[627] + amp[629] + Complex<double> (0, 1) * amp[631] +
      amp[632] + Complex<double> (0, 1) * amp[634] + amp[642] - amp[640] -
      amp[643] + amp[645] + Complex<double> (0, 1) * amp[647] - Complex<double>
      (0, 1) * amp[648] + amp[650] - Complex<double> (0, 1) * amp[654]);
  jamp[13] = +1./4. * (-1./3. * amp[592] - 1./3. * amp[593] - 1./3. * amp[597]
      - 1./3. * amp[598] - 1./3. * amp[613] - 1./3. * amp[614] - 1./3. *
      amp[618] - 1./3. * amp[619]);
  jamp[14] = +1./4. * (-1./3. * amp[613] - 1./3. * amp[614] - 1./3. * amp[616]
      - 1./3. * amp[617] - 1./3. * amp[629] - 1./3. * amp[630] - 1./3. *
      amp[632] - 1./3. * amp[633] - 1./3. * Complex<double> (0, 1) * amp[634] -
      1./3. * Complex<double> (0, 1) * amp[635] - 1./3. * amp[638] + 1./3. *
      Complex<double> (0, 1) * amp[654] + 1./3. * Complex<double> (0, 1) *
      amp[655] - 1./3. * amp[657]);
  jamp[15] = +1./4. * (+amp[606] + amp[608] + Complex<double> (0, 1) * amp[609]
      + amp[611] + amp[614] - Complex<double> (0, 1) * amp[615] + amp[616] +
      amp[618] + Complex<double> (0, 1) * amp[635] - Complex<double> (0, 1) *
      amp[636] + amp[638] + amp[641] + amp[640] + amp[643] + amp[644] +
      Complex<double> (0, 1) * amp[646] - Complex<double> (0, 1) * amp[652]);
  jamp[16] = +1./4. * (+1./9. * amp[592] + 1./9. * amp[593] + 1./9. * amp[594]
      + 1./9. * amp[596] + 1./9. * amp[597] + 1./9. * amp[598] + 1./9. *
      amp[613] + 1./9. * amp[614] + 1./9. * amp[616] + 1./9. * amp[617] + 1./9.
      * amp[618] + 1./9. * amp[619] + 1./9. * amp[656] + 1./9. * amp[657]);
  jamp[17] = +1./4. * (-1./3. * amp[608] - 1./3. * amp[610] - 1./3. * amp[611]
      - 1./3. * amp[612] - 1./3. * amp[616] - 1./3. * amp[617] - 1./3. *
      amp[618] - 1./3. * amp[619] - 1./3. * Complex<double> (0, 1) * amp[646] -
      1./3. * Complex<double> (0, 1) * amp[647] - 1./3. * amp[650] + 1./3. *
      Complex<double> (0, 1) * amp[652] + 1./3. * Complex<double> (0, 1) *
      amp[653] - 1./3. * amp[656]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[6][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}


}  // end namespace PY8MEs_namespace

