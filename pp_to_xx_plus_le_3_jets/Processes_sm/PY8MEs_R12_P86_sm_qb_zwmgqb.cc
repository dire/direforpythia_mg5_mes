//==========================================================================
// This file has been automatically generated for Pythia 8 by
// MadGraph5_aMC@NLO v. 2.6.0, 2017-08-16
// By the MadGraph5_aMC@NLO Development Team
// Visit launchpad.net/madgraph5 and amcatnlo.web.cern.ch
//==========================================================================

#include "PY8MEs_R12_P86_sm_qb_zwmgqb.h"
#include "HelAmps_sm.h"

using namespace Pythia8_sm; 

namespace PY8MEs_namespace 
{
//==========================================================================
// Class member functions for calculating the matrix elements for
// Process: d b > z w- g u b WEIGHTED<=7 @12
// Process: s b > z w- g c b WEIGHTED<=7 @12
// Process: d b~ > z w- g u b~ WEIGHTED<=7 @12
// Process: s b~ > z w- g c b~ WEIGHTED<=7 @12
// Process: u~ b > z w- g d~ b WEIGHTED<=7 @12
// Process: c~ b > z w- g s~ b WEIGHTED<=7 @12
// Process: u~ b~ > z w- g d~ b~ WEIGHTED<=7 @12
// Process: c~ b~ > z w- g s~ b~ WEIGHTED<=7 @12

// Exception class
class PY8MEs_R12_P86_sm_qb_zwmgqbException : public exception
{
  virtual const char * what() const throw()
  {
    return "Exception in class 'PY8MEs_R12_P86_sm_qb_zwmgqb'."; 
  }
}
PY8MEs_R12_P86_sm_qb_zwmgqb_exception; 

std::set<int> PY8MEs_R12_P86_sm_qb_zwmgqb::s_channel_proc = std::set<int>
    (createset<int> ());

int PY8MEs_R12_P86_sm_qb_zwmgqb::helicities[ncomb][nexternal] = {{-1, -1, -1,
    -1, -1, -1, -1}, {-1, -1, -1, -1, -1, -1, 1}, {-1, -1, -1, -1, -1, 1, -1},
    {-1, -1, -1, -1, -1, 1, 1}, {-1, -1, -1, -1, 1, -1, -1}, {-1, -1, -1, -1,
    1, -1, 1}, {-1, -1, -1, -1, 1, 1, -1}, {-1, -1, -1, -1, 1, 1, 1}, {-1, -1,
    -1, 0, -1, -1, -1}, {-1, -1, -1, 0, -1, -1, 1}, {-1, -1, -1, 0, -1, 1, -1},
    {-1, -1, -1, 0, -1, 1, 1}, {-1, -1, -1, 0, 1, -1, -1}, {-1, -1, -1, 0, 1,
    -1, 1}, {-1, -1, -1, 0, 1, 1, -1}, {-1, -1, -1, 0, 1, 1, 1}, {-1, -1, -1,
    1, -1, -1, -1}, {-1, -1, -1, 1, -1, -1, 1}, {-1, -1, -1, 1, -1, 1, -1},
    {-1, -1, -1, 1, -1, 1, 1}, {-1, -1, -1, 1, 1, -1, -1}, {-1, -1, -1, 1, 1,
    -1, 1}, {-1, -1, -1, 1, 1, 1, -1}, {-1, -1, -1, 1, 1, 1, 1}, {-1, -1, 0,
    -1, -1, -1, -1}, {-1, -1, 0, -1, -1, -1, 1}, {-1, -1, 0, -1, -1, 1, -1},
    {-1, -1, 0, -1, -1, 1, 1}, {-1, -1, 0, -1, 1, -1, -1}, {-1, -1, 0, -1, 1,
    -1, 1}, {-1, -1, 0, -1, 1, 1, -1}, {-1, -1, 0, -1, 1, 1, 1}, {-1, -1, 0, 0,
    -1, -1, -1}, {-1, -1, 0, 0, -1, -1, 1}, {-1, -1, 0, 0, -1, 1, -1}, {-1, -1,
    0, 0, -1, 1, 1}, {-1, -1, 0, 0, 1, -1, -1}, {-1, -1, 0, 0, 1, -1, 1}, {-1,
    -1, 0, 0, 1, 1, -1}, {-1, -1, 0, 0, 1, 1, 1}, {-1, -1, 0, 1, -1, -1, -1},
    {-1, -1, 0, 1, -1, -1, 1}, {-1, -1, 0, 1, -1, 1, -1}, {-1, -1, 0, 1, -1, 1,
    1}, {-1, -1, 0, 1, 1, -1, -1}, {-1, -1, 0, 1, 1, -1, 1}, {-1, -1, 0, 1, 1,
    1, -1}, {-1, -1, 0, 1, 1, 1, 1}, {-1, -1, 1, -1, -1, -1, -1}, {-1, -1, 1,
    -1, -1, -1, 1}, {-1, -1, 1, -1, -1, 1, -1}, {-1, -1, 1, -1, -1, 1, 1}, {-1,
    -1, 1, -1, 1, -1, -1}, {-1, -1, 1, -1, 1, -1, 1}, {-1, -1, 1, -1, 1, 1,
    -1}, {-1, -1, 1, -1, 1, 1, 1}, {-1, -1, 1, 0, -1, -1, -1}, {-1, -1, 1, 0,
    -1, -1, 1}, {-1, -1, 1, 0, -1, 1, -1}, {-1, -1, 1, 0, -1, 1, 1}, {-1, -1,
    1, 0, 1, -1, -1}, {-1, -1, 1, 0, 1, -1, 1}, {-1, -1, 1, 0, 1, 1, -1}, {-1,
    -1, 1, 0, 1, 1, 1}, {-1, -1, 1, 1, -1, -1, -1}, {-1, -1, 1, 1, -1, -1, 1},
    {-1, -1, 1, 1, -1, 1, -1}, {-1, -1, 1, 1, -1, 1, 1}, {-1, -1, 1, 1, 1, -1,
    -1}, {-1, -1, 1, 1, 1, -1, 1}, {-1, -1, 1, 1, 1, 1, -1}, {-1, -1, 1, 1, 1,
    1, 1}, {-1, 1, -1, -1, -1, -1, -1}, {-1, 1, -1, -1, -1, -1, 1}, {-1, 1, -1,
    -1, -1, 1, -1}, {-1, 1, -1, -1, -1, 1, 1}, {-1, 1, -1, -1, 1, -1, -1}, {-1,
    1, -1, -1, 1, -1, 1}, {-1, 1, -1, -1, 1, 1, -1}, {-1, 1, -1, -1, 1, 1, 1},
    {-1, 1, -1, 0, -1, -1, -1}, {-1, 1, -1, 0, -1, -1, 1}, {-1, 1, -1, 0, -1,
    1, -1}, {-1, 1, -1, 0, -1, 1, 1}, {-1, 1, -1, 0, 1, -1, -1}, {-1, 1, -1, 0,
    1, -1, 1}, {-1, 1, -1, 0, 1, 1, -1}, {-1, 1, -1, 0, 1, 1, 1}, {-1, 1, -1,
    1, -1, -1, -1}, {-1, 1, -1, 1, -1, -1, 1}, {-1, 1, -1, 1, -1, 1, -1}, {-1,
    1, -1, 1, -1, 1, 1}, {-1, 1, -1, 1, 1, -1, -1}, {-1, 1, -1, 1, 1, -1, 1},
    {-1, 1, -1, 1, 1, 1, -1}, {-1, 1, -1, 1, 1, 1, 1}, {-1, 1, 0, -1, -1, -1,
    -1}, {-1, 1, 0, -1, -1, -1, 1}, {-1, 1, 0, -1, -1, 1, -1}, {-1, 1, 0, -1,
    -1, 1, 1}, {-1, 1, 0, -1, 1, -1, -1}, {-1, 1, 0, -1, 1, -1, 1}, {-1, 1, 0,
    -1, 1, 1, -1}, {-1, 1, 0, -1, 1, 1, 1}, {-1, 1, 0, 0, -1, -1, -1}, {-1, 1,
    0, 0, -1, -1, 1}, {-1, 1, 0, 0, -1, 1, -1}, {-1, 1, 0, 0, -1, 1, 1}, {-1,
    1, 0, 0, 1, -1, -1}, {-1, 1, 0, 0, 1, -1, 1}, {-1, 1, 0, 0, 1, 1, -1}, {-1,
    1, 0, 0, 1, 1, 1}, {-1, 1, 0, 1, -1, -1, -1}, {-1, 1, 0, 1, -1, -1, 1},
    {-1, 1, 0, 1, -1, 1, -1}, {-1, 1, 0, 1, -1, 1, 1}, {-1, 1, 0, 1, 1, -1,
    -1}, {-1, 1, 0, 1, 1, -1, 1}, {-1, 1, 0, 1, 1, 1, -1}, {-1, 1, 0, 1, 1, 1,
    1}, {-1, 1, 1, -1, -1, -1, -1}, {-1, 1, 1, -1, -1, -1, 1}, {-1, 1, 1, -1,
    -1, 1, -1}, {-1, 1, 1, -1, -1, 1, 1}, {-1, 1, 1, -1, 1, -1, -1}, {-1, 1, 1,
    -1, 1, -1, 1}, {-1, 1, 1, -1, 1, 1, -1}, {-1, 1, 1, -1, 1, 1, 1}, {-1, 1,
    1, 0, -1, -1, -1}, {-1, 1, 1, 0, -1, -1, 1}, {-1, 1, 1, 0, -1, 1, -1}, {-1,
    1, 1, 0, -1, 1, 1}, {-1, 1, 1, 0, 1, -1, -1}, {-1, 1, 1, 0, 1, -1, 1}, {-1,
    1, 1, 0, 1, 1, -1}, {-1, 1, 1, 0, 1, 1, 1}, {-1, 1, 1, 1, -1, -1, -1}, {-1,
    1, 1, 1, -1, -1, 1}, {-1, 1, 1, 1, -1, 1, -1}, {-1, 1, 1, 1, -1, 1, 1},
    {-1, 1, 1, 1, 1, -1, -1}, {-1, 1, 1, 1, 1, -1, 1}, {-1, 1, 1, 1, 1, 1, -1},
    {-1, 1, 1, 1, 1, 1, 1}, {1, -1, -1, -1, -1, -1, -1}, {1, -1, -1, -1, -1,
    -1, 1}, {1, -1, -1, -1, -1, 1, -1}, {1, -1, -1, -1, -1, 1, 1}, {1, -1, -1,
    -1, 1, -1, -1}, {1, -1, -1, -1, 1, -1, 1}, {1, -1, -1, -1, 1, 1, -1}, {1,
    -1, -1, -1, 1, 1, 1}, {1, -1, -1, 0, -1, -1, -1}, {1, -1, -1, 0, -1, -1,
    1}, {1, -1, -1, 0, -1, 1, -1}, {1, -1, -1, 0, -1, 1, 1}, {1, -1, -1, 0, 1,
    -1, -1}, {1, -1, -1, 0, 1, -1, 1}, {1, -1, -1, 0, 1, 1, -1}, {1, -1, -1, 0,
    1, 1, 1}, {1, -1, -1, 1, -1, -1, -1}, {1, -1, -1, 1, -1, -1, 1}, {1, -1,
    -1, 1, -1, 1, -1}, {1, -1, -1, 1, -1, 1, 1}, {1, -1, -1, 1, 1, -1, -1}, {1,
    -1, -1, 1, 1, -1, 1}, {1, -1, -1, 1, 1, 1, -1}, {1, -1, -1, 1, 1, 1, 1},
    {1, -1, 0, -1, -1, -1, -1}, {1, -1, 0, -1, -1, -1, 1}, {1, -1, 0, -1, -1,
    1, -1}, {1, -1, 0, -1, -1, 1, 1}, {1, -1, 0, -1, 1, -1, -1}, {1, -1, 0, -1,
    1, -1, 1}, {1, -1, 0, -1, 1, 1, -1}, {1, -1, 0, -1, 1, 1, 1}, {1, -1, 0, 0,
    -1, -1, -1}, {1, -1, 0, 0, -1, -1, 1}, {1, -1, 0, 0, -1, 1, -1}, {1, -1, 0,
    0, -1, 1, 1}, {1, -1, 0, 0, 1, -1, -1}, {1, -1, 0, 0, 1, -1, 1}, {1, -1, 0,
    0, 1, 1, -1}, {1, -1, 0, 0, 1, 1, 1}, {1, -1, 0, 1, -1, -1, -1}, {1, -1, 0,
    1, -1, -1, 1}, {1, -1, 0, 1, -1, 1, -1}, {1, -1, 0, 1, -1, 1, 1}, {1, -1,
    0, 1, 1, -1, -1}, {1, -1, 0, 1, 1, -1, 1}, {1, -1, 0, 1, 1, 1, -1}, {1, -1,
    0, 1, 1, 1, 1}, {1, -1, 1, -1, -1, -1, -1}, {1, -1, 1, -1, -1, -1, 1}, {1,
    -1, 1, -1, -1, 1, -1}, {1, -1, 1, -1, -1, 1, 1}, {1, -1, 1, -1, 1, -1, -1},
    {1, -1, 1, -1, 1, -1, 1}, {1, -1, 1, -1, 1, 1, -1}, {1, -1, 1, -1, 1, 1,
    1}, {1, -1, 1, 0, -1, -1, -1}, {1, -1, 1, 0, -1, -1, 1}, {1, -1, 1, 0, -1,
    1, -1}, {1, -1, 1, 0, -1, 1, 1}, {1, -1, 1, 0, 1, -1, -1}, {1, -1, 1, 0, 1,
    -1, 1}, {1, -1, 1, 0, 1, 1, -1}, {1, -1, 1, 0, 1, 1, 1}, {1, -1, 1, 1, -1,
    -1, -1}, {1, -1, 1, 1, -1, -1, 1}, {1, -1, 1, 1, -1, 1, -1}, {1, -1, 1, 1,
    -1, 1, 1}, {1, -1, 1, 1, 1, -1, -1}, {1, -1, 1, 1, 1, -1, 1}, {1, -1, 1, 1,
    1, 1, -1}, {1, -1, 1, 1, 1, 1, 1}, {1, 1, -1, -1, -1, -1, -1}, {1, 1, -1,
    -1, -1, -1, 1}, {1, 1, -1, -1, -1, 1, -1}, {1, 1, -1, -1, -1, 1, 1}, {1, 1,
    -1, -1, 1, -1, -1}, {1, 1, -1, -1, 1, -1, 1}, {1, 1, -1, -1, 1, 1, -1}, {1,
    1, -1, -1, 1, 1, 1}, {1, 1, -1, 0, -1, -1, -1}, {1, 1, -1, 0, -1, -1, 1},
    {1, 1, -1, 0, -1, 1, -1}, {1, 1, -1, 0, -1, 1, 1}, {1, 1, -1, 0, 1, -1,
    -1}, {1, 1, -1, 0, 1, -1, 1}, {1, 1, -1, 0, 1, 1, -1}, {1, 1, -1, 0, 1, 1,
    1}, {1, 1, -1, 1, -1, -1, -1}, {1, 1, -1, 1, -1, -1, 1}, {1, 1, -1, 1, -1,
    1, -1}, {1, 1, -1, 1, -1, 1, 1}, {1, 1, -1, 1, 1, -1, -1}, {1, 1, -1, 1, 1,
    -1, 1}, {1, 1, -1, 1, 1, 1, -1}, {1, 1, -1, 1, 1, 1, 1}, {1, 1, 0, -1, -1,
    -1, -1}, {1, 1, 0, -1, -1, -1, 1}, {1, 1, 0, -1, -1, 1, -1}, {1, 1, 0, -1,
    -1, 1, 1}, {1, 1, 0, -1, 1, -1, -1}, {1, 1, 0, -1, 1, -1, 1}, {1, 1, 0, -1,
    1, 1, -1}, {1, 1, 0, -1, 1, 1, 1}, {1, 1, 0, 0, -1, -1, -1}, {1, 1, 0, 0,
    -1, -1, 1}, {1, 1, 0, 0, -1, 1, -1}, {1, 1, 0, 0, -1, 1, 1}, {1, 1, 0, 0,
    1, -1, -1}, {1, 1, 0, 0, 1, -1, 1}, {1, 1, 0, 0, 1, 1, -1}, {1, 1, 0, 0, 1,
    1, 1}, {1, 1, 0, 1, -1, -1, -1}, {1, 1, 0, 1, -1, -1, 1}, {1, 1, 0, 1, -1,
    1, -1}, {1, 1, 0, 1, -1, 1, 1}, {1, 1, 0, 1, 1, -1, -1}, {1, 1, 0, 1, 1,
    -1, 1}, {1, 1, 0, 1, 1, 1, -1}, {1, 1, 0, 1, 1, 1, 1}, {1, 1, 1, -1, -1,
    -1, -1}, {1, 1, 1, -1, -1, -1, 1}, {1, 1, 1, -1, -1, 1, -1}, {1, 1, 1, -1,
    -1, 1, 1}, {1, 1, 1, -1, 1, -1, -1}, {1, 1, 1, -1, 1, -1, 1}, {1, 1, 1, -1,
    1, 1, -1}, {1, 1, 1, -1, 1, 1, 1}, {1, 1, 1, 0, -1, -1, -1}, {1, 1, 1, 0,
    -1, -1, 1}, {1, 1, 1, 0, -1, 1, -1}, {1, 1, 1, 0, -1, 1, 1}, {1, 1, 1, 0,
    1, -1, -1}, {1, 1, 1, 0, 1, -1, 1}, {1, 1, 1, 0, 1, 1, -1}, {1, 1, 1, 0, 1,
    1, 1}, {1, 1, 1, 1, -1, -1, -1}, {1, 1, 1, 1, -1, -1, 1}, {1, 1, 1, 1, -1,
    1, -1}, {1, 1, 1, 1, -1, 1, 1}, {1, 1, 1, 1, 1, -1, -1}, {1, 1, 1, 1, 1,
    -1, 1}, {1, 1, 1, 1, 1, 1, -1}, {1, 1, 1, 1, 1, 1, 1}};

// Normalization factors the various processes
// Denominators: spins, colors and identical particles
int PY8MEs_R12_P86_sm_qb_zwmgqb::denom_colors[nprocesses] = {9, 9, 9, 9}; 
int PY8MEs_R12_P86_sm_qb_zwmgqb::denom_hels[nprocesses] = {4, 4, 4, 4}; 
int PY8MEs_R12_P86_sm_qb_zwmgqb::denom_iden[nprocesses] = {1, 1, 1, 1}; 

//--------------------------------------------------------------------------
// Color config initialization
void PY8MEs_R12_P86_sm_qb_zwmgqb::initColorConfigs() 
{
  color_configs = vector < vec_vec_int > (); 
  jamp_nc_relative_power = vector < vec_int > (); 

  // Color flows of process Process: d b > z w- g u b WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[0].push_back(-1); 
  // JAMP #1
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #2
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #3
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[0].push_back(-1); 

  // Color flows of process Process: d b~ > z w- g u b~ WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #1
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[1].push_back(-1); 
  // JAMP #2
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #3
  color_configs[1].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[1].push_back(-1); 

  // Color flows of process Process: u~ b > z w- g d~ b WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(1)(1)(0)(0)(0)(0)(0)(3)(2)(0)(3)(2)(0)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #1
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(1)(3)(0)(0)(0)(0)(0)(3)(2)(0)(1)(2)(0)));
  jamp_nc_relative_power[2].push_back(-1); 
  // JAMP #2
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(1)(3)(0)(0)(0)(0)(0)(3)(1)(0)(2)(2)(0)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #3
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(1)(2)(0)(0)(0)(0)(0)(3)(1)(0)(3)(2)(0)));
  jamp_nc_relative_power[2].push_back(-1); 

  // Color flows of process Process: u~ b~ > z w- g d~ b~ WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[3].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[3].push_back(-1); 
  // JAMP #1
  color_configs[3].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #2
  color_configs[3].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #3
  color_configs[3].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[3].push_back(-1); 
}

//--------------------------------------------------------------------------
// Destructor.
PY8MEs_R12_P86_sm_qb_zwmgqb::~PY8MEs_R12_P86_sm_qb_zwmgqb() 
{
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    delete[] p[i]; 
    p[i] = NULL; 
  }
}

//--------------------------------------------------------------------------
// Invert the permutation mapping
vector<int> PY8MEs_R12_P86_sm_qb_zwmgqb::invert_mapping(vector<int> mapping) 
{
  vector<int> inverted_mapping; 
  for (unsigned int i = 0; i < mapping.size(); i++ )
  {
    for (unsigned int j = 0; j < mapping.size(); j++ )
    {
      if (mapping[j] == ((int)i))
      {
        inverted_mapping.push_back(j); 
        break; 
      }
    }
  }
  return inverted_mapping; 
}

//--------------------------------------------------------------------------
// Return the list of possible helicity configurations
vector < vec_int > PY8MEs_R12_P86_sm_qb_zwmgqb::getHelicityConfigs(vector<int>
    permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(ncomb, vector<int> (nexternal, 0)); 
  for (unsigned int ihel = 0; ihel < ncomb; ihel++ )
  {
    for(unsigned int j = 0; j < nexternal; j++ )
    {
      res[ihel][chosenPerm[j]] = helicities[ihel][j]; 
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Return the list of possible color configurations
vector < vec_int > PY8MEs_R12_P86_sm_qb_zwmgqb::getColorConfigs(int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(color_configs[chosenProcID].size(), vector<int>
      (nexternal * 2, 0));
  for (unsigned int icol = 0; icol < color_configs[chosenProcID].size(); icol++
      )
  {
    for(unsigned int j = 0; j < (2 * nexternal); j++ )
    {
      res[icol][chosenPerm[j/2] * 2 + j%2] =
          color_configs[chosenProcID][icol][j];
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Get JAMP relative N_c power
int PY8MEs_R12_P86_sm_qb_zwmgqb::getColorFlowRelativeNCPower(int color_flow_ID,
    int specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return jamp_nc_relative_power[chosenProcID][color_flow_ID]; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity ID -> Helicity Config
vector<int> PY8MEs_R12_P86_sm_qb_zwmgqb::getHelicityConfigForID(int hel_ID,
    vector<int> permutation)
{
  if (hel_ID < 0 || hel_ID >= ncomb)
  {
    cerr <<  "Error in function 'getHelicityConfigForID' of class" << 
    " 'PY8MEs_R12_P86_sm_qb_zwmgqb': Specified helicity ID '" << 
    hel_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R12_P86_sm_qb_zwmgqb_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(nexternal, 0); 
  for (unsigned int j = 0; j < nexternal; j++ )
  {
    res[chosenPerm[j]] = helicities[hel_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity Config -> Helicity ID
int PY8MEs_R12_P86_sm_qb_zwmgqb::getHelicityIDForConfig(vector<int> hel_config,
    vector<int> permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  int user_ihel = -1; 
  if (hel_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < ncomb; i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < nexternal; j++ )
      {
        if (helicities[i][chosenPerm[j]] != hel_config[j])
        {
          found = false; 
          break; 
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_ihel = i; 
        break; 
      }
    }
    if (user_ihel == -1)
    {
      cerr <<  "Error in function 'getHelicityIDForConfig' of class" << 
      " 'PY8MEs_R12_P86_sm_qb_zwmgqb': Specified helicity" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R12_P86_sm_qb_zwmgqb_exception; 
    }
  }
  return user_ihel; 
}


//--------------------------------------------------------------------------
// Implements the map Color ID -> Color Config
vector<int> PY8MEs_R12_P86_sm_qb_zwmgqb::getColorConfigForID(int color_ID, int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < 0 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getColorConfigForID' of class" << 
    " 'PY8MEs_R12_P86_sm_qb_zwmgqb': Specified color ID '" << 
    color_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R12_P86_sm_qb_zwmgqb_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(color_configs[chosenProcID][color_ID].size(), 0); 
  for (unsigned int j = 0; j < (2 * nexternal); j++ )
  {
    res[chosenPerm[j/2] * 2 + j%2] = color_configs[chosenProcID][color_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Color Config -> Color ID
int PY8MEs_R12_P86_sm_qb_zwmgqb::getColorIDForConfig(vector<int> color_config,
    int specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = -1; 
  if (color_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < color_configs[chosenProcID].size(); i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < (nexternal * 2); j++ )
      {

        // If colorless then make sure it matches
        // The little arithmetics in the color index is just
        // the permutation applies on the particle list which is
        // twice smaller since each particle can have two color indices.
        if (color_config[j] == 0)
        {
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] != 0)
          {
            found = false; 
            break; 
          }
          // Otherwise check that the color linked position matches
        }
        else
        {
          int color_linked_pos = -1; 
          // Find the other end of the line in the user color config
          for (unsigned int k = 0; k < (nexternal * 2); k++ )
          {
            if (k == j)
              continue; 
            if (color_config[j] == color_config[k])
            {
              color_linked_pos = k; 
              break; 
            }
          }
          if (color_linked_pos == -1)
          {
            cerr <<  "Error in function 'getColorIDForConfig' of class" << 
            " 'PY8MEs_R12_P86_sm_qb_zwmgqb': A color line could " << 
            " not be closed." << endl; 
            throw PY8MEs_R12_P86_sm_qb_zwmgqb_exception; 
          }
          // Now check whether the color line matches
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] !=
              color_configs[chosenProcID][i][chosenPerm[color_linked_pos/2] * 2
              + color_linked_pos%2])
          {
            found = false; 
            break; 
          }
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_icol = i; 
        break; 
      }
    }

    if (user_icol == -1)
    {
      cerr <<  "Error in function 'getColorIDForConfig' of class" << 
      " 'PY8MEs_R12_P86_sm_qb_zwmgqb': Specified color" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R12_P86_sm_qb_zwmgqb_exception; 
    }
  }
  return user_icol; 
}

//--------------------------------------------------------------------------
// Returns all result previously computed in SigmaKin
vector < vec_double > PY8MEs_R12_P86_sm_qb_zwmgqb::getAllResults(int
    specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return all_results[chosenProcID]; 
}

//--------------------------------------------------------------------------
// Returns a result previously computed in SigmaKin for a specific helicity
// and color ID. -1 means avg and summed over that characteristic.
double PY8MEs_R12_P86_sm_qb_zwmgqb::getResult(int helicity_ID, int color_ID,
    int specify_proc_ID)
{
  if (helicity_ID < - 1 || helicity_ID >= ncomb)
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R12_P86_sm_qb_zwmgqb': Specified helicity ID '" << 
    helicity_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R12_P86_sm_qb_zwmgqb_exception; 
  }
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < - 1 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R12_P86_sm_qb_zwmgqb': Specified color ID '" << 
    color_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R12_P86_sm_qb_zwmgqb_exception; 
  }
  return all_results[chosenProcID][helicity_ID + 1][color_ID + 1]; 
}

//--------------------------------------------------------------------------
// Check for the availability of the requested process and if available,
// If available, this returns the corresponding permutation and Proc_ID to use.
// If not available, this returns a negative Proc_ID.
pair < vector<int> , int > PY8MEs_R12_P86_sm_qb_zwmgqb::static_getPY8ME(vector<int> initial_pdgs, vector<int> final_pdgs, set<int> schannels) 
{

  // Not available return value
  pair < vector<int> , int > NA(vector<int> (), -1); 

  // Check if s-channel requirements match
  if (nreq_s_channels > 0)
  {
    if (schannels != s_channel_proc)
      return NA; 
  }
  else
  {
    if (schannels.size() != 0)
      return NA; 
  }

  // Check number of final state particles
  if (final_pdgs.size() != (nexternal - ninitial))
    return NA; 

  // Check number of initial state particles
  if (initial_pdgs.size() != ninitial)
    return NA; 

  // List of processes available in this class
  const int nprocs = 16; 
  const int proc_IDS[nprocs] = {0, 0, 1, 1, 2, 2, 3, 3, 0, 0, 1, 1, 2, 2, 3,
      3};
  const int in_pdgs[nprocs][ninitial] = {{1, 5}, {3, 5}, {1, -5}, {3, -5}, {-2,
      5}, {-4, 5}, {-2, -5}, {-4, -5}, {5, 1}, {5, 3}, {-5, 1}, {-5, 3}, {5,
      -2}, {5, -4}, {-5, -2}, {-5, -4}};
  const int out_pdgs[nprocs][nexternal - ninitial] = {{23, -24, 21, 2, 5}, {23,
      -24, 21, 4, 5}, {23, -24, 21, 2, -5}, {23, -24, 21, 4, -5}, {23, -24, 21,
      -1, 5}, {23, -24, 21, -3, 5}, {23, -24, 21, -1, -5}, {23, -24, 21, -3,
      -5}, {23, -24, 21, 2, 5}, {23, -24, 21, 4, 5}, {23, -24, 21, 2, -5}, {23,
      -24, 21, 4, -5}, {23, -24, 21, -1, 5}, {23, -24, 21, -3, 5}, {23, -24,
      21, -1, -5}, {23, -24, 21, -3, -5}};

  bool in_pdgs_used[ninitial]; 
  bool out_pdgs_used[nexternal - ninitial]; 
  for(unsigned int i = 0; i < nprocs; i++ )
  {
    int permutations[nexternal]; 

    // Reinitialize initial state look-up variables
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      in_pdgs_used[j] = false; 
      permutations[j] = -1; 
    }
    // Look for initial state matches
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      for(unsigned int k = 0; k < ninitial; k++ )
      {
        // Make sure it has not been used already
        if (in_pdgs_used[k])
          continue; 
        if (initial_pdgs[k] == in_pdgs[i][j])
        {
          permutations[j] = k; 
          in_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[ninitial - 1] == -1)
      continue; 

    // Reinitialize final state look-up variables
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      out_pdgs_used[j] = false; 
      permutations[ninitial + j] = -1; 
    }
    // Look for final state matches
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      for(unsigned int k = 0; k < (nexternal - ninitial); k++ )
      {
        // Make sure it has not been used already
        if (out_pdgs_used[k])
          continue; 
        if (final_pdgs[k] == out_pdgs[i][j])
        {
          permutations[ninitial + j] = ninitial + k; 
          out_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[ninitial + j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[nexternal - 1] == -1)
      continue; 

    // Return process found
    return pair < vector<int> , int > (vector<int> (permutations, permutations
        + nexternal), proc_IDS[i]);
  }

  // No process found
  return NA; 
}

//--------------------------------------------------------------------------
// Set momenta
void PY8MEs_R12_P86_sm_qb_zwmgqb::setMomenta(vector < vec_double >
    momenta_picked)
{
  if (momenta_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setMomenta' of class" << 
    " 'PY8MEs_R12_P86_sm_qb_zwmgqb': Incorrect number of" << 
    " momenta specified." << endl; 
    throw PY8MEs_R12_P86_sm_qb_zwmgqb_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    if (momenta_picked[i].size() != 4)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R12_P86_sm_qb_zwmgqb': Incorrect number of" << 
      " momenta components specified." << endl; 
      throw PY8MEs_R12_P86_sm_qb_zwmgqb_exception; 
    }
    if (momenta_picked[i][0] < 0.0)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R12_P86_sm_qb_zwmgqb': A momentum was specified" << 
      " with negative energy. Check conventions." << endl; 
      throw PY8MEs_R12_P86_sm_qb_zwmgqb_exception; 
    }
    for (unsigned int j = 0; j < 4; j++ )
    {
      p[i][j] = momenta_picked[i][j]; 
    }
  }
}

//--------------------------------------------------------------------------
// Set color configuration to use. An empty vector means sum over all.
void PY8MEs_R12_P86_sm_qb_zwmgqb::setColors(vector<int> colors_picked)
{
  if (colors_picked.size() == 0)
  {
    user_colors = vector<int> (); 
    return; 
  }
  if (colors_picked.size() != (2 * nexternal))
  {
    cerr <<  "Error in function 'setColors' of class" << 
    " 'PY8MEs_R12_P86_sm_qb_zwmgqb': Incorrect number" << 
    " of colors specified." << endl; 
    throw PY8MEs_R12_P86_sm_qb_zwmgqb_exception; 
  }
  user_colors = vector<int> ((2 * nexternal), 0); 
  for(unsigned int i = 0; i < (2 * nexternal); i++ )
  {
    user_colors[i] = colors_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the helicity configuration to use. Am empty vector means sum over all.
void PY8MEs_R12_P86_sm_qb_zwmgqb::setHelicities(vector<int> helicities_picked) 
{
  if (helicities_picked.size() != nexternal)
  {
    if (helicities_picked.size() == 0)
    {
      user_helicities = vector<int> (); 
      return; 
    }
    cerr <<  "Error in function 'setHelicities' of class" << 
    " 'PY8MEs_R12_P86_sm_qb_zwmgqb': Incorrect number" << 
    " of helicities specified." << endl; 
    throw PY8MEs_R12_P86_sm_qb_zwmgqb_exception; 
  }
  user_helicities = vector<int> (nexternal, 0); 
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    user_helicities[i] = helicities_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the permutation to use (will apply to momenta, colors and helicities)
void PY8MEs_R12_P86_sm_qb_zwmgqb::setPermutation(vector<int> perm_picked) 
{
  if (perm_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setPermutations' of class" << 
    " 'PY8MEs_R12_P86_sm_qb_zwmgqb': Incorrect number" << 
    " of permutations specified." << endl; 
    throw PY8MEs_R12_P86_sm_qb_zwmgqb_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    perm[i] = perm_picked[i]; 
  }
}

// Set the proc_ID to use
void PY8MEs_R12_P86_sm_qb_zwmgqb::setProcID(int procID_picked) 
{
  proc_ID = procID_picked; 
}

//--------------------------------------------------------------------------
// Initialize process.

void PY8MEs_R12_P86_sm_qb_zwmgqb::initProc() 
{

  // Initialize flags
  include_symmetry_factors = true; 
  include_helicity_averaging_factors = true; 
  include_color_averaging_factors = true; 

  // Initialize vectors.
  perm = vector<int> (nexternal, 0); 
  user_colors = vector<int> (2 * nexternal, 0); 
  user_helicities = vector<int> (nexternal, 0); 
  p = vector < double * > (); 
  for (unsigned int i = 0; i < nexternal; i++ )
  {
    p.push_back(new double[4]); 
  }
  initColorConfigs(); 
  // Synchronize local variables dependent on the model with the active model.
  mME = vector<double> (nexternal, 0.); 
  syncProcModelParams(); 
  jamp2 = vector < vec_double > (4); 
  jamp2[0] = vector<double> (4, 0.); 
  jamp2[1] = vector<double> (4, 0.); 
  jamp2[2] = vector<double> (4, 0.); 
  jamp2[3] = vector<double> (4, 0.); 
  all_results = vector < vec_vec_double > (4); 
  // The first entry is always the color or helicity avg/summed matrix element.
  all_results[0] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[1] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[2] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[3] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
}

// Synchronize local variables of the process that depend on the model
// parameters
void PY8MEs_R12_P86_sm_qb_zwmgqb::syncProcModelParams() 
{

  // Instantiate the model class and set parameters that stay fixed during run
  mME[0] = pars->ZERO; 
  mME[1] = pars->mdl_MB; 
  mME[2] = pars->mdl_MZ; 
  mME[3] = pars->mdl_MW; 
  mME[4] = pars->ZERO; 
  mME[5] = pars->ZERO; 
  mME[6] = pars->mdl_MB; 
}

//--------------------------------------------------------------------------
// Setter allowing to force particular values for the external masses
void PY8MEs_R12_P86_sm_qb_zwmgqb::setMasses(vec_double external_masses) 
{

  if (external_masses.size() != mME.size())
  {
    cerr <<  "Error in function 'setMasses' of class" << 
    " 'PY8MEs_R12_P86_sm_qb_zwmgqb': Incorrect number of" << 
    " masses specified." << endl; 
    throw PY8MEs_R12_P86_sm_qb_zwmgqb_exception; 
  }
  for (unsigned int j = 0; j < mME.size(); j++ )
  {
    mME[j] = external_masses[perm[j]]; 
  }
}

//--------------------------------------------------------------------------
// Getter accessing external masses with the correct ordering
vector<double> PY8MEs_R12_P86_sm_qb_zwmgqb::getMasses() 
{

  vec_double external_masses; 
  vector<int> invertedPerm; 
  invertedPerm = invert_mapping(perm); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    external_masses.push_back(mME[invertedPerm[i]]); 
  }
  return external_masses; 

}


// Set all values of the external masses to float(-mode) where mode can be
// 0 : Mass taken from the model
// 1 : Mass taken from p_i^2 if not massless to begin with
// 2 : Mass always taken from p_i^2.
void PY8MEs_R12_P86_sm_qb_zwmgqb::setExternalMassesMode(int mode) 
{
  if (mode != 0 && mode != 1 && mode != 2)
  {
    cerr <<  "Error in function 'setExternalMassesMode' of class" << 
    " 'PY8MEs_R12_P86_sm_qb_zwmgqb': Incorrect mode selected :" << mode << 
    ". It must be either 0, 1 or 2" << endl; 
    throw PY8MEs_R12_P86_sm_qb_zwmgqb_exception; 
  }
  if (mode == 0)
  {
    syncProcModelParams(); 
  }
  else if (mode == 1)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      if (mME[j] != pars->ZERO)
      {
        mME[j] = -1.0; 
      }
    }
  }
  else if (mode == 2)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      mME[j] = -1.0; 
    }
  }
}

//--------------------------------------------------------------------------
// Evaluate the squared matrix element.

double PY8MEs_R12_P86_sm_qb_zwmgqb::sigmaKin() 
{
  // Set the parameters which change event by event
  pars->setDependentParameters(); 
  pars->setDependentCouplings(); 
  // Reset color flows
  for(int i = 0; i < 4; i++ )
    jamp2[0][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[1][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[2][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[3][i] = 0.; 

  // Save previous values of mME
  vector<double> saved_mME(mME.size(), 0.0); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (mME[i] < 0.0)
    {
      saved_mME[i] = mME[i]; 
      mME[i] = sqrt(abs(pow(p[perm[i]][0], 2) - 
      (pow(p[perm[i]][1], 2) + pow(p[perm[i]][2], 2) + pow(p[perm[i]][3],
          2))));
    }
  }

  // Local variables and constants
  const int max_tries = 10; 
  vector < vec_bool > goodhel(nprocesses, vec_bool(ncomb, false)); 
  vec_int ntry(nprocesses, 0); 
  double t = 0.; 
  double result = 0.; 

  if (ntry[proc_ID] <= max_tries)
    ntry[proc_ID] = ntry[proc_ID] + 1; 

  // Find which helicity configuration is asked for
  // -1 indicates one wants to sum over helicities
  int user_ihel = getHelicityIDForConfig(user_helicities); 

  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = getColorIDForConfig(user_colors); 

  // Reset the list of results that will be recomputed here
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      all_results[proc_ID][ihel + 1][icolor + 1] = 0.; 
    }
  }

  // Calculate the matrix element for all helicities
  // unless already detected as vanishing
  for(int ihel = 0; ihel < ncomb; ihel++ )
  {
    // Skip helicity if already detected as vanishing
    if ((ntry[proc_ID] >= max_tries) && !goodhel[proc_ID][ihel])
      continue; 

    // Also skip helicity if user asks for a specific one
    if ((ntry[proc_ID] >= max_tries) && user_ihel != -1 && user_ihel != ihel)
      continue; 

    calculate_wavefunctions(helicities[ihel]); 

    // Reset locally computed color flows
    for(int i = 0; i < 4; i++ )
      jamp2[0][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[1][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[2][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[3][i] = 0.; 

    if (proc_ID == 0)
      t = matrix_12_db_zwmgub(); 
    if (proc_ID == 1)
      t = matrix_12_dbx_zwmgubx(); 
    if (proc_ID == 2)
      t = matrix_12_uxb_zwmgdxb(); 
    if (proc_ID == 3)
      t = matrix_12_uxbx_zwmgdxbx(); 

    // Store which helicities give non-zero result
    if ((ntry[proc_ID] < max_tries) && t != 0. && !goodhel[proc_ID][ihel])
      goodhel[proc_ID][ihel] = true; 

    // Aggregate results
    if (user_ihel == -1 || user_ihel == ihel)
    {
      if (user_icol == -1)
      {
        result = result + t; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][0] += t; 
          for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
          {
            all_results[proc_ID][0][i + 1] += jamp2[proc_ID][i]; 
          }
        }
        all_results[proc_ID][ihel + 1][0] += t; 
        for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
        {
          all_results[proc_ID][ihel + 1][i + 1] += jamp2[proc_ID][i]; 
        }
      }
      else
      {
        result = result + jamp2[proc_ID][user_icol]; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][user_icol + 1] += jamp2[proc_ID][user_icol]; 
        }
        all_results[proc_ID][ihel + 1][user_icol + 1] +=
            jamp2[proc_ID][user_icol];
      }
    }

  }

  // Normalize results with the identical particle factor
  if (include_symmetry_factors)
  {
    result = result/denom_iden[proc_ID]; 
  }
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      if (include_symmetry_factors)
      {
        all_results[proc_ID][ihel + 1][icolor + 1] /= denom_iden[proc_ID]; 
      }
    }
  }


  // Normalize when when summing+averaging over helicity configurations
  if (user_ihel == -1 && include_helicity_averaging_factors)
  {
    result /= denom_hels[proc_ID]; 
    if (user_icol == -1)
    {
      all_results[proc_ID][0][0] /= denom_hels[proc_ID]; 
      for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
      {
        all_results[proc_ID][0][i + 1] /= denom_hels[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][0][user_icol + 1] /= denom_hels[proc_ID]; 
    }
  }

  // Normalize when summing+averaging over color configurations
  if (user_icol == -1 && include_color_averaging_factors)
  {
    result /= denom_colors[proc_ID]; 
    if (user_ihel == -1)
    {
      all_results[proc_ID][0][0] /= denom_colors[proc_ID]; 
      for (unsigned int i = 0; i < ncomb; i++ )
      {
        all_results[proc_ID][i + 1][0] /= denom_colors[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][user_ihel + 1][0] /= denom_colors[proc_ID]; 
    }
  }

  // Reinstate previous values of mME
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (saved_mME[i] < 0.0)
    {
      mME[i] = saved_mME[i]; 
    }
  }

  // Finally return it
  return result; 
}

//==========================================================================
// Private class member functions

//--------------------------------------------------------------------------
// Evaluate |M|^2 for each subprocess

void PY8MEs_R12_P86_sm_qb_zwmgqb::calculate_wavefunctions(const int hel[])
{
  // Calculate wavefunctions for all processes
  // Calculate all wavefunctions
  ixxxxx(p[perm[0]], mME[0], hel[0], +1, w[0]); 
  ixxxxx(p[perm[1]], mME[1], hel[1], +1, w[1]); 
  vxxxxx(p[perm[2]], mME[2], hel[2], +1, w[2]); 
  vxxxxx(p[perm[3]], mME[3], hel[3], +1, w[3]); 
  vxxxxx(p[perm[4]], mME[4], hel[4], +1, w[4]); 
  oxxxxx(p[perm[5]], mME[5], hel[5], +1, w[5]); 
  oxxxxx(p[perm[6]], mME[6], hel[6], +1, w[6]); 
  FFV1_2(w[0], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[7]); 
  VVV1_2(w[3], w[2], pars->GC_53, pars->mdl_MW, pars->mdl_WW, w[8]); 
  FFV1P0_3(w[1], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[9]); 
  FFV2_2(w[7], w[8], pars->GC_100, pars->ZERO, pars->ZERO, w[10]); 
  FFV1_2(w[7], w[9], pars->GC_11, pars->ZERO, pars->ZERO, w[11]); 
  FFV2_5_1(w[5], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[12]);
  FFV2_2(w[7], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[13]); 
  FFV2_3_1(w[6], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[14]);
  FFV1P0_3(w[1], w[14], pars->GC_11, pars->ZERO, pars->ZERO, w[15]); 
  FFV2_1(w[5], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[16]); 
  FFV1P0_3(w[7], w[16], pars->GC_11, pars->ZERO, pars->ZERO, w[17]); 
  FFV2_3_2(w[1], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[18]);
  FFV1P0_3(w[18], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[19]); 
  FFV2_3_2(w[7], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[20]);
  FFV1_1(w[5], w[9], pars->GC_11, pars->ZERO, pars->ZERO, w[21]); 
  FFV1_1(w[5], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[22]); 
  FFV2_3_2(w[0], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[23]);
  FFV2_1(w[22], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[24]); 
  FFV1_1(w[22], w[9], pars->GC_11, pars->ZERO, pars->ZERO, w[25]); 
  FFV2_2(w[0], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[26]); 
  FFV1P0_3(w[26], w[22], pars->GC_11, pars->ZERO, pars->ZERO, w[27]); 
  FFV2_5_1(w[22], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[28]);
  FFV2_1(w[22], w[8], pars->GC_100, pars->ZERO, pars->ZERO, w[29]); 
  FFV1_2(w[0], w[9], pars->GC_11, pars->ZERO, pars->ZERO, w[30]); 
  FFV1_1(w[6], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[31]); 
  FFV1P0_3(w[1], w[31], pars->GC_11, pars->ZERO, pars->ZERO, w[32]); 
  FFV2_2(w[23], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[33]); 
  FFV2_3_1(w[31], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[34]);
  FFV1P0_3(w[26], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[35]); 
  FFV2_5_2(w[26], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[36]);
  FFV1P0_3(w[18], w[31], pars->GC_11, pars->ZERO, pars->ZERO, w[37]); 
  FFV2_2(w[0], w[8], pars->GC_100, pars->ZERO, pars->ZERO, w[38]); 
  FFV2_1(w[5], w[8], pars->GC_100, pars->ZERO, pars->ZERO, w[39]); 
  FFV2_1(w[12], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[40]); 
  FFV1P0_3(w[0], w[16], pars->GC_11, pars->ZERO, pars->ZERO, w[41]); 
  FFV2_3_1(w[16], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[42]);
  FFV1_2(w[1], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[43]); 
  FFV1P0_3(w[43], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[44]); 
  FFV2_3_2(w[43], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[45]);
  FFV1P0_3(w[43], w[14], pars->GC_11, pars->ZERO, pars->ZERO, w[46]); 
  FFV1_2(w[23], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[47]); 
  FFV1_1(w[16], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[48]); 
  VVV1P0_1(w[4], w[9], pars->GC_10, pars->ZERO, pars->ZERO, w[49]); 
  FFV1_2(w[26], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[50]); 
  FFV1_1(w[12], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[51]); 
  FFV1_1(w[14], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[52]); 
  FFV1_2(w[18], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[53]); 
  oxxxxx(p[perm[1]], mME[1], hel[1], -1, w[54]); 
  ixxxxx(p[perm[6]], mME[6], hel[6], -1, w[55]); 
  FFV1P0_3(w[55], w[54], pars->GC_11, pars->ZERO, pars->ZERO, w[56]); 
  FFV1_2(w[7], w[56], pars->GC_11, pars->ZERO, pars->ZERO, w[57]); 
  FFV2_3_1(w[54], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[58]);
  FFV1P0_3(w[55], w[58], pars->GC_11, pars->ZERO, pars->ZERO, w[59]); 
  FFV2_3_2(w[55], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[60]);
  FFV1P0_3(w[60], w[54], pars->GC_11, pars->ZERO, pars->ZERO, w[61]); 
  FFV1_1(w[5], w[56], pars->GC_11, pars->ZERO, pars->ZERO, w[62]); 
  FFV1_1(w[22], w[56], pars->GC_11, pars->ZERO, pars->ZERO, w[63]); 
  FFV1_2(w[0], w[56], pars->GC_11, pars->ZERO, pars->ZERO, w[64]); 
  FFV1_1(w[54], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[65]); 
  FFV1P0_3(w[55], w[65], pars->GC_11, pars->ZERO, pars->ZERO, w[66]); 
  FFV2_3_1(w[65], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[67]);
  FFV1P0_3(w[60], w[65], pars->GC_11, pars->ZERO, pars->ZERO, w[68]); 
  FFV1_2(w[55], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[69]); 
  FFV1P0_3(w[69], w[54], pars->GC_11, pars->ZERO, pars->ZERO, w[70]); 
  FFV2_3_2(w[69], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[71]);
  FFV1P0_3(w[69], w[58], pars->GC_11, pars->ZERO, pars->ZERO, w[72]); 
  VVV1P0_1(w[4], w[56], pars->GC_10, pars->ZERO, pars->ZERO, w[73]); 
  FFV1_1(w[58], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[74]); 
  FFV1_2(w[60], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[75]); 
  oxxxxx(p[perm[0]], mME[0], hel[0], -1, w[76]); 
  ixxxxx(p[perm[5]], mME[5], hel[5], -1, w[77]); 
  FFV1_2(w[77], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[78]); 
  FFV2_2(w[78], w[8], pars->GC_100, pars->ZERO, pars->ZERO, w[79]); 
  FFV1_2(w[78], w[9], pars->GC_11, pars->ZERO, pars->ZERO, w[80]); 
  FFV2_5_1(w[76], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[81]);
  FFV2_2(w[78], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[82]); 
  FFV2_1(w[76], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[83]); 
  FFV1P0_3(w[78], w[83], pars->GC_11, pars->ZERO, pars->ZERO, w[84]); 
  FFV2_3_2(w[78], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[85]);
  FFV1_1(w[76], w[9], pars->GC_11, pars->ZERO, pars->ZERO, w[86]); 
  FFV1_1(w[76], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[87]); 
  FFV2_3_2(w[77], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[88]);
  FFV2_1(w[87], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[89]); 
  FFV1_1(w[87], w[9], pars->GC_11, pars->ZERO, pars->ZERO, w[90]); 
  FFV2_2(w[77], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[91]); 
  FFV1P0_3(w[91], w[87], pars->GC_11, pars->ZERO, pars->ZERO, w[92]); 
  FFV2_5_1(w[87], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[93]);
  FFV2_1(w[87], w[8], pars->GC_100, pars->ZERO, pars->ZERO, w[94]); 
  FFV1_2(w[77], w[9], pars->GC_11, pars->ZERO, pars->ZERO, w[95]); 
  FFV2_2(w[88], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[96]); 
  FFV1P0_3(w[91], w[76], pars->GC_11, pars->ZERO, pars->ZERO, w[97]); 
  FFV2_5_2(w[91], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[98]);
  FFV2_2(w[77], w[8], pars->GC_100, pars->ZERO, pars->ZERO, w[99]); 
  FFV2_1(w[76], w[8], pars->GC_100, pars->ZERO, pars->ZERO, w[100]); 
  FFV2_1(w[81], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[101]); 
  FFV1P0_3(w[77], w[83], pars->GC_11, pars->ZERO, pars->ZERO, w[102]); 
  FFV2_3_1(w[83], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[103]);
  FFV1_2(w[88], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[104]); 
  FFV1_1(w[83], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[105]); 
  FFV1_2(w[91], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[106]); 
  FFV1_1(w[81], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[107]); 
  FFV1_2(w[78], w[56], pars->GC_11, pars->ZERO, pars->ZERO, w[108]); 
  FFV1_1(w[76], w[56], pars->GC_11, pars->ZERO, pars->ZERO, w[109]); 
  FFV1_1(w[87], w[56], pars->GC_11, pars->ZERO, pars->ZERO, w[110]); 
  FFV1_2(w[77], w[56], pars->GC_11, pars->ZERO, pars->ZERO, w[111]); 

  // Calculate all amplitudes
  // Amplitude(s) for diagram number 0
  FFV1_0(w[10], w[5], w[9], pars->GC_11, amp[0]); 
  FFV2_0(w[11], w[5], w[8], pars->GC_100, amp[1]); 
  FFV1_0(w[13], w[12], w[9], pars->GC_11, amp[2]); 
  FFV2_0(w[11], w[12], w[3], pars->GC_100, amp[3]); 
  FFV1_0(w[13], w[5], w[15], pars->GC_11, amp[4]); 
  FFV1_0(w[1], w[14], w[17], pars->GC_11, amp[5]); 
  FFV1_0(w[13], w[5], w[19], pars->GC_11, amp[6]); 
  FFV1_0(w[18], w[6], w[17], pars->GC_11, amp[7]); 
  FFV1_0(w[20], w[16], w[9], pars->GC_11, amp[8]); 
  FFV2_3_0(w[11], w[16], w[2], pars->GC_50, pars->GC_58, amp[9]); 
  FFV2_0(w[20], w[21], w[3], pars->GC_100, amp[10]); 
  FFV2_5_0(w[13], w[21], w[2], pars->GC_51, pars->GC_58, amp[11]); 
  FFV1_0(w[23], w[24], w[9], pars->GC_11, amp[12]); 
  FFV2_0(w[23], w[25], w[3], pars->GC_100, amp[13]); 
  FFV1_0(w[1], w[14], w[27], pars->GC_11, amp[14]); 
  FFV1_0(w[18], w[6], w[27], pars->GC_11, amp[15]); 
  FFV1_0(w[26], w[28], w[9], pars->GC_11, amp[16]); 
  FFV2_5_0(w[26], w[25], w[2], pars->GC_51, pars->GC_58, amp[17]); 
  FFV1_0(w[0], w[29], w[9], pars->GC_11, amp[18]); 
  FFV2_0(w[0], w[25], w[8], pars->GC_100, amp[19]); 
  FFV1_0(w[0], w[24], w[15], pars->GC_11, amp[20]); 
  FFV1_0(w[0], w[24], w[19], pars->GC_11, amp[21]); 
  FFV2_0(w[30], w[28], w[3], pars->GC_100, amp[22]); 
  FFV2_3_0(w[30], w[24], w[2], pars->GC_50, pars->GC_58, amp[23]); 
  FFV1_0(w[33], w[5], w[32], pars->GC_11, amp[24]); 
  FFV1_0(w[23], w[16], w[32], pars->GC_11, amp[25]); 
  FFV1_0(w[1], w[34], w[35], pars->GC_11, amp[26]); 
  FFV1_0(w[36], w[5], w[32], pars->GC_11, amp[27]); 
  FFV1_0(w[26], w[12], w[32], pars->GC_11, amp[28]); 
  FFV1_0(w[26], w[5], w[37], pars->GC_11, amp[29]); 
  FFV1_0(w[38], w[5], w[32], pars->GC_11, amp[30]); 
  FFV1_0(w[0], w[39], w[32], pars->GC_11, amp[31]); 
  FFV1_0(w[0], w[40], w[32], pars->GC_11, amp[32]); 
  FFV1_0(w[0], w[16], w[37], pars->GC_11, amp[33]); 
  FFV1_0(w[1], w[34], w[41], pars->GC_11, amp[34]); 
  FFV1_0(w[0], w[42], w[32], pars->GC_11, amp[35]); 
  FFV1_0(w[33], w[5], w[44], pars->GC_11, amp[36]); 
  FFV1_0(w[23], w[16], w[44], pars->GC_11, amp[37]); 
  FFV1_0(w[45], w[6], w[35], pars->GC_11, amp[38]); 
  FFV1_0(w[36], w[5], w[44], pars->GC_11, amp[39]); 
  FFV1_0(w[26], w[12], w[44], pars->GC_11, amp[40]); 
  FFV1_0(w[26], w[5], w[46], pars->GC_11, amp[41]); 
  FFV1_0(w[38], w[5], w[44], pars->GC_11, amp[42]); 
  FFV1_0(w[0], w[39], w[44], pars->GC_11, amp[43]); 
  FFV1_0(w[0], w[40], w[44], pars->GC_11, amp[44]); 
  FFV1_0(w[0], w[16], w[46], pars->GC_11, amp[45]); 
  FFV1_0(w[45], w[6], w[41], pars->GC_11, amp[46]); 
  FFV1_0(w[0], w[42], w[44], pars->GC_11, amp[47]); 
  FFV1_0(w[47], w[16], w[9], pars->GC_11, amp[48]); 
  FFV1_0(w[23], w[48], w[9], pars->GC_11, amp[49]); 
  FFV1_0(w[23], w[16], w[49], pars->GC_11, amp[50]); 
  FFV2_0(w[47], w[21], w[3], pars->GC_100, amp[51]); 
  FFV1_0(w[33], w[5], w[49], pars->GC_11, amp[52]); 
  FFV1_0(w[33], w[21], w[4], pars->GC_11, amp[53]); 
  FFV1_0(w[50], w[12], w[9], pars->GC_11, amp[54]); 
  FFV1_0(w[26], w[51], w[9], pars->GC_11, amp[55]); 
  FFV1_0(w[26], w[12], w[49], pars->GC_11, amp[56]); 
  FFV1_0(w[50], w[5], w[15], pars->GC_11, amp[57]); 
  FFV1_0(w[1], w[52], w[35], pars->GC_11, amp[58]); 
  VVV1_0(w[4], w[35], w[15], pars->GC_10, amp[59]); 
  FFV1_0(w[50], w[5], w[19], pars->GC_11, amp[60]); 
  FFV1_0(w[53], w[6], w[35], pars->GC_11, amp[61]); 
  VVV1_0(w[4], w[35], w[19], pars->GC_10, amp[62]); 
  FFV2_5_0(w[50], w[21], w[2], pars->GC_51, pars->GC_58, amp[63]); 
  FFV1_0(w[36], w[5], w[49], pars->GC_11, amp[64]); 
  FFV1_0(w[36], w[21], w[4], pars->GC_11, amp[65]); 
  FFV1_0(w[38], w[5], w[49], pars->GC_11, amp[66]); 
  FFV1_0(w[0], w[39], w[49], pars->GC_11, amp[67]); 
  FFV1_0(w[38], w[21], w[4], pars->GC_11, amp[68]); 
  FFV1_0(w[30], w[39], w[4], pars->GC_11, amp[69]); 
  FFV2_0(w[30], w[51], w[3], pars->GC_100, amp[70]); 
  FFV1_0(w[0], w[40], w[49], pars->GC_11, amp[71]); 
  FFV1_0(w[30], w[40], w[4], pars->GC_11, amp[72]); 
  FFV1_0(w[1], w[52], w[41], pars->GC_11, amp[73]); 
  FFV1_0(w[0], w[48], w[15], pars->GC_11, amp[74]); 
  VVV1_0(w[4], w[41], w[15], pars->GC_10, amp[75]); 
  FFV1_0(w[53], w[6], w[41], pars->GC_11, amp[76]); 
  FFV1_0(w[0], w[48], w[19], pars->GC_11, amp[77]); 
  VVV1_0(w[4], w[41], w[19], pars->GC_10, amp[78]); 
  FFV2_3_0(w[30], w[48], w[2], pars->GC_50, pars->GC_58, amp[79]); 
  FFV1_0(w[0], w[42], w[49], pars->GC_11, amp[80]); 
  FFV1_0(w[30], w[42], w[4], pars->GC_11, amp[81]); 
  FFV1_0(w[10], w[5], w[56], pars->GC_11, amp[82]); 
  FFV2_0(w[57], w[5], w[8], pars->GC_100, amp[83]); 
  FFV1_0(w[13], w[12], w[56], pars->GC_11, amp[84]); 
  FFV2_0(w[57], w[12], w[3], pars->GC_100, amp[85]); 
  FFV1_0(w[13], w[5], w[59], pars->GC_11, amp[86]); 
  FFV1_0(w[55], w[58], w[17], pars->GC_11, amp[87]); 
  FFV1_0(w[13], w[5], w[61], pars->GC_11, amp[88]); 
  FFV1_0(w[60], w[54], w[17], pars->GC_11, amp[89]); 
  FFV1_0(w[20], w[16], w[56], pars->GC_11, amp[90]); 
  FFV2_3_0(w[57], w[16], w[2], pars->GC_50, pars->GC_58, amp[91]); 
  FFV2_0(w[20], w[62], w[3], pars->GC_100, amp[92]); 
  FFV2_5_0(w[13], w[62], w[2], pars->GC_51, pars->GC_58, amp[93]); 
  FFV1_0(w[23], w[24], w[56], pars->GC_11, amp[94]); 
  FFV2_0(w[23], w[63], w[3], pars->GC_100, amp[95]); 
  FFV1_0(w[55], w[58], w[27], pars->GC_11, amp[96]); 
  FFV1_0(w[60], w[54], w[27], pars->GC_11, amp[97]); 
  FFV1_0(w[26], w[28], w[56], pars->GC_11, amp[98]); 
  FFV2_5_0(w[26], w[63], w[2], pars->GC_51, pars->GC_58, amp[99]); 
  FFV1_0(w[0], w[29], w[56], pars->GC_11, amp[100]); 
  FFV2_0(w[0], w[63], w[8], pars->GC_100, amp[101]); 
  FFV1_0(w[0], w[24], w[59], pars->GC_11, amp[102]); 
  FFV1_0(w[0], w[24], w[61], pars->GC_11, amp[103]); 
  FFV2_0(w[64], w[28], w[3], pars->GC_100, amp[104]); 
  FFV2_3_0(w[64], w[24], w[2], pars->GC_50, pars->GC_58, amp[105]); 
  FFV1_0(w[33], w[5], w[66], pars->GC_11, amp[106]); 
  FFV1_0(w[23], w[16], w[66], pars->GC_11, amp[107]); 
  FFV1_0(w[55], w[67], w[35], pars->GC_11, amp[108]); 
  FFV1_0(w[36], w[5], w[66], pars->GC_11, amp[109]); 
  FFV1_0(w[26], w[12], w[66], pars->GC_11, amp[110]); 
  FFV1_0(w[26], w[5], w[68], pars->GC_11, amp[111]); 
  FFV1_0(w[38], w[5], w[66], pars->GC_11, amp[112]); 
  FFV1_0(w[0], w[39], w[66], pars->GC_11, amp[113]); 
  FFV1_0(w[0], w[40], w[66], pars->GC_11, amp[114]); 
  FFV1_0(w[0], w[16], w[68], pars->GC_11, amp[115]); 
  FFV1_0(w[55], w[67], w[41], pars->GC_11, amp[116]); 
  FFV1_0(w[0], w[42], w[66], pars->GC_11, amp[117]); 
  FFV1_0(w[33], w[5], w[70], pars->GC_11, amp[118]); 
  FFV1_0(w[23], w[16], w[70], pars->GC_11, amp[119]); 
  FFV1_0(w[71], w[54], w[35], pars->GC_11, amp[120]); 
  FFV1_0(w[36], w[5], w[70], pars->GC_11, amp[121]); 
  FFV1_0(w[26], w[12], w[70], pars->GC_11, amp[122]); 
  FFV1_0(w[26], w[5], w[72], pars->GC_11, amp[123]); 
  FFV1_0(w[38], w[5], w[70], pars->GC_11, amp[124]); 
  FFV1_0(w[0], w[39], w[70], pars->GC_11, amp[125]); 
  FFV1_0(w[0], w[40], w[70], pars->GC_11, amp[126]); 
  FFV1_0(w[0], w[16], w[72], pars->GC_11, amp[127]); 
  FFV1_0(w[71], w[54], w[41], pars->GC_11, amp[128]); 
  FFV1_0(w[0], w[42], w[70], pars->GC_11, amp[129]); 
  FFV1_0(w[47], w[16], w[56], pars->GC_11, amp[130]); 
  FFV1_0(w[23], w[48], w[56], pars->GC_11, amp[131]); 
  FFV1_0(w[23], w[16], w[73], pars->GC_11, amp[132]); 
  FFV2_0(w[47], w[62], w[3], pars->GC_100, amp[133]); 
  FFV1_0(w[33], w[5], w[73], pars->GC_11, amp[134]); 
  FFV1_0(w[33], w[62], w[4], pars->GC_11, amp[135]); 
  FFV1_0(w[50], w[12], w[56], pars->GC_11, amp[136]); 
  FFV1_0(w[26], w[51], w[56], pars->GC_11, amp[137]); 
  FFV1_0(w[26], w[12], w[73], pars->GC_11, amp[138]); 
  FFV1_0(w[50], w[5], w[59], pars->GC_11, amp[139]); 
  FFV1_0(w[55], w[74], w[35], pars->GC_11, amp[140]); 
  VVV1_0(w[4], w[35], w[59], pars->GC_10, amp[141]); 
  FFV1_0(w[50], w[5], w[61], pars->GC_11, amp[142]); 
  FFV1_0(w[75], w[54], w[35], pars->GC_11, amp[143]); 
  VVV1_0(w[4], w[35], w[61], pars->GC_10, amp[144]); 
  FFV2_5_0(w[50], w[62], w[2], pars->GC_51, pars->GC_58, amp[145]); 
  FFV1_0(w[36], w[5], w[73], pars->GC_11, amp[146]); 
  FFV1_0(w[36], w[62], w[4], pars->GC_11, amp[147]); 
  FFV1_0(w[38], w[5], w[73], pars->GC_11, amp[148]); 
  FFV1_0(w[0], w[39], w[73], pars->GC_11, amp[149]); 
  FFV1_0(w[38], w[62], w[4], pars->GC_11, amp[150]); 
  FFV1_0(w[64], w[39], w[4], pars->GC_11, amp[151]); 
  FFV2_0(w[64], w[51], w[3], pars->GC_100, amp[152]); 
  FFV1_0(w[0], w[40], w[73], pars->GC_11, amp[153]); 
  FFV1_0(w[64], w[40], w[4], pars->GC_11, amp[154]); 
  FFV1_0(w[55], w[74], w[41], pars->GC_11, amp[155]); 
  FFV1_0(w[0], w[48], w[59], pars->GC_11, amp[156]); 
  VVV1_0(w[4], w[41], w[59], pars->GC_10, amp[157]); 
  FFV1_0(w[75], w[54], w[41], pars->GC_11, amp[158]); 
  FFV1_0(w[0], w[48], w[61], pars->GC_11, amp[159]); 
  VVV1_0(w[4], w[41], w[61], pars->GC_10, amp[160]); 
  FFV2_3_0(w[64], w[48], w[2], pars->GC_50, pars->GC_58, amp[161]); 
  FFV1_0(w[0], w[42], w[73], pars->GC_11, amp[162]); 
  FFV1_0(w[64], w[42], w[4], pars->GC_11, amp[163]); 
  FFV1_0(w[79], w[76], w[9], pars->GC_11, amp[164]); 
  FFV2_0(w[80], w[76], w[8], pars->GC_100, amp[165]); 
  FFV1_0(w[82], w[81], w[9], pars->GC_11, amp[166]); 
  FFV2_0(w[80], w[81], w[3], pars->GC_100, amp[167]); 
  FFV1_0(w[82], w[76], w[15], pars->GC_11, amp[168]); 
  FFV1_0(w[1], w[14], w[84], pars->GC_11, amp[169]); 
  FFV1_0(w[82], w[76], w[19], pars->GC_11, amp[170]); 
  FFV1_0(w[18], w[6], w[84], pars->GC_11, amp[171]); 
  FFV1_0(w[85], w[83], w[9], pars->GC_11, amp[172]); 
  FFV2_3_0(w[80], w[83], w[2], pars->GC_50, pars->GC_58, amp[173]); 
  FFV2_0(w[85], w[86], w[3], pars->GC_100, amp[174]); 
  FFV2_5_0(w[82], w[86], w[2], pars->GC_51, pars->GC_58, amp[175]); 
  FFV1_0(w[88], w[89], w[9], pars->GC_11, amp[176]); 
  FFV2_0(w[88], w[90], w[3], pars->GC_100, amp[177]); 
  FFV1_0(w[1], w[14], w[92], pars->GC_11, amp[178]); 
  FFV1_0(w[18], w[6], w[92], pars->GC_11, amp[179]); 
  FFV1_0(w[91], w[93], w[9], pars->GC_11, amp[180]); 
  FFV2_5_0(w[91], w[90], w[2], pars->GC_51, pars->GC_58, amp[181]); 
  FFV1_0(w[77], w[94], w[9], pars->GC_11, amp[182]); 
  FFV2_0(w[77], w[90], w[8], pars->GC_100, amp[183]); 
  FFV1_0(w[77], w[89], w[15], pars->GC_11, amp[184]); 
  FFV1_0(w[77], w[89], w[19], pars->GC_11, amp[185]); 
  FFV2_0(w[95], w[93], w[3], pars->GC_100, amp[186]); 
  FFV2_3_0(w[95], w[89], w[2], pars->GC_50, pars->GC_58, amp[187]); 
  FFV1_0(w[96], w[76], w[32], pars->GC_11, amp[188]); 
  FFV1_0(w[88], w[83], w[32], pars->GC_11, amp[189]); 
  FFV1_0(w[1], w[34], w[97], pars->GC_11, amp[190]); 
  FFV1_0(w[98], w[76], w[32], pars->GC_11, amp[191]); 
  FFV1_0(w[91], w[81], w[32], pars->GC_11, amp[192]); 
  FFV1_0(w[91], w[76], w[37], pars->GC_11, amp[193]); 
  FFV1_0(w[99], w[76], w[32], pars->GC_11, amp[194]); 
  FFV1_0(w[77], w[100], w[32], pars->GC_11, amp[195]); 
  FFV1_0(w[77], w[101], w[32], pars->GC_11, amp[196]); 
  FFV1_0(w[77], w[83], w[37], pars->GC_11, amp[197]); 
  FFV1_0(w[1], w[34], w[102], pars->GC_11, amp[198]); 
  FFV1_0(w[77], w[103], w[32], pars->GC_11, amp[199]); 
  FFV1_0(w[96], w[76], w[44], pars->GC_11, amp[200]); 
  FFV1_0(w[88], w[83], w[44], pars->GC_11, amp[201]); 
  FFV1_0(w[45], w[6], w[97], pars->GC_11, amp[202]); 
  FFV1_0(w[98], w[76], w[44], pars->GC_11, amp[203]); 
  FFV1_0(w[91], w[81], w[44], pars->GC_11, amp[204]); 
  FFV1_0(w[91], w[76], w[46], pars->GC_11, amp[205]); 
  FFV1_0(w[99], w[76], w[44], pars->GC_11, amp[206]); 
  FFV1_0(w[77], w[100], w[44], pars->GC_11, amp[207]); 
  FFV1_0(w[77], w[101], w[44], pars->GC_11, amp[208]); 
  FFV1_0(w[77], w[83], w[46], pars->GC_11, amp[209]); 
  FFV1_0(w[45], w[6], w[102], pars->GC_11, amp[210]); 
  FFV1_0(w[77], w[103], w[44], pars->GC_11, amp[211]); 
  FFV1_0(w[104], w[83], w[9], pars->GC_11, amp[212]); 
  FFV1_0(w[88], w[105], w[9], pars->GC_11, amp[213]); 
  FFV1_0(w[88], w[83], w[49], pars->GC_11, amp[214]); 
  FFV2_0(w[104], w[86], w[3], pars->GC_100, amp[215]); 
  FFV1_0(w[96], w[76], w[49], pars->GC_11, amp[216]); 
  FFV1_0(w[96], w[86], w[4], pars->GC_11, amp[217]); 
  FFV1_0(w[106], w[81], w[9], pars->GC_11, amp[218]); 
  FFV1_0(w[91], w[107], w[9], pars->GC_11, amp[219]); 
  FFV1_0(w[91], w[81], w[49], pars->GC_11, amp[220]); 
  FFV1_0(w[106], w[76], w[15], pars->GC_11, amp[221]); 
  FFV1_0(w[1], w[52], w[97], pars->GC_11, amp[222]); 
  VVV1_0(w[4], w[97], w[15], pars->GC_10, amp[223]); 
  FFV1_0(w[106], w[76], w[19], pars->GC_11, amp[224]); 
  FFV1_0(w[53], w[6], w[97], pars->GC_11, amp[225]); 
  VVV1_0(w[4], w[97], w[19], pars->GC_10, amp[226]); 
  FFV2_5_0(w[106], w[86], w[2], pars->GC_51, pars->GC_58, amp[227]); 
  FFV1_0(w[98], w[76], w[49], pars->GC_11, amp[228]); 
  FFV1_0(w[98], w[86], w[4], pars->GC_11, amp[229]); 
  FFV1_0(w[99], w[76], w[49], pars->GC_11, amp[230]); 
  FFV1_0(w[77], w[100], w[49], pars->GC_11, amp[231]); 
  FFV1_0(w[99], w[86], w[4], pars->GC_11, amp[232]); 
  FFV1_0(w[95], w[100], w[4], pars->GC_11, amp[233]); 
  FFV2_0(w[95], w[107], w[3], pars->GC_100, amp[234]); 
  FFV1_0(w[77], w[101], w[49], pars->GC_11, amp[235]); 
  FFV1_0(w[95], w[101], w[4], pars->GC_11, amp[236]); 
  FFV1_0(w[1], w[52], w[102], pars->GC_11, amp[237]); 
  FFV1_0(w[77], w[105], w[15], pars->GC_11, amp[238]); 
  VVV1_0(w[4], w[102], w[15], pars->GC_10, amp[239]); 
  FFV1_0(w[53], w[6], w[102], pars->GC_11, amp[240]); 
  FFV1_0(w[77], w[105], w[19], pars->GC_11, amp[241]); 
  VVV1_0(w[4], w[102], w[19], pars->GC_10, amp[242]); 
  FFV2_3_0(w[95], w[105], w[2], pars->GC_50, pars->GC_58, amp[243]); 
  FFV1_0(w[77], w[103], w[49], pars->GC_11, amp[244]); 
  FFV1_0(w[95], w[103], w[4], pars->GC_11, amp[245]); 
  FFV1_0(w[79], w[76], w[56], pars->GC_11, amp[246]); 
  FFV2_0(w[108], w[76], w[8], pars->GC_100, amp[247]); 
  FFV1_0(w[82], w[81], w[56], pars->GC_11, amp[248]); 
  FFV2_0(w[108], w[81], w[3], pars->GC_100, amp[249]); 
  FFV1_0(w[82], w[76], w[59], pars->GC_11, amp[250]); 
  FFV1_0(w[55], w[58], w[84], pars->GC_11, amp[251]); 
  FFV1_0(w[82], w[76], w[61], pars->GC_11, amp[252]); 
  FFV1_0(w[60], w[54], w[84], pars->GC_11, amp[253]); 
  FFV1_0(w[85], w[83], w[56], pars->GC_11, amp[254]); 
  FFV2_3_0(w[108], w[83], w[2], pars->GC_50, pars->GC_58, amp[255]); 
  FFV2_0(w[85], w[109], w[3], pars->GC_100, amp[256]); 
  FFV2_5_0(w[82], w[109], w[2], pars->GC_51, pars->GC_58, amp[257]); 
  FFV1_0(w[88], w[89], w[56], pars->GC_11, amp[258]); 
  FFV2_0(w[88], w[110], w[3], pars->GC_100, amp[259]); 
  FFV1_0(w[55], w[58], w[92], pars->GC_11, amp[260]); 
  FFV1_0(w[60], w[54], w[92], pars->GC_11, amp[261]); 
  FFV1_0(w[91], w[93], w[56], pars->GC_11, amp[262]); 
  FFV2_5_0(w[91], w[110], w[2], pars->GC_51, pars->GC_58, amp[263]); 
  FFV1_0(w[77], w[94], w[56], pars->GC_11, amp[264]); 
  FFV2_0(w[77], w[110], w[8], pars->GC_100, amp[265]); 
  FFV1_0(w[77], w[89], w[59], pars->GC_11, amp[266]); 
  FFV1_0(w[77], w[89], w[61], pars->GC_11, amp[267]); 
  FFV2_0(w[111], w[93], w[3], pars->GC_100, amp[268]); 
  FFV2_3_0(w[111], w[89], w[2], pars->GC_50, pars->GC_58, amp[269]); 
  FFV1_0(w[96], w[76], w[66], pars->GC_11, amp[270]); 
  FFV1_0(w[88], w[83], w[66], pars->GC_11, amp[271]); 
  FFV1_0(w[55], w[67], w[97], pars->GC_11, amp[272]); 
  FFV1_0(w[98], w[76], w[66], pars->GC_11, amp[273]); 
  FFV1_0(w[91], w[81], w[66], pars->GC_11, amp[274]); 
  FFV1_0(w[91], w[76], w[68], pars->GC_11, amp[275]); 
  FFV1_0(w[99], w[76], w[66], pars->GC_11, amp[276]); 
  FFV1_0(w[77], w[100], w[66], pars->GC_11, amp[277]); 
  FFV1_0(w[77], w[101], w[66], pars->GC_11, amp[278]); 
  FFV1_0(w[77], w[83], w[68], pars->GC_11, amp[279]); 
  FFV1_0(w[55], w[67], w[102], pars->GC_11, amp[280]); 
  FFV1_0(w[77], w[103], w[66], pars->GC_11, amp[281]); 
  FFV1_0(w[96], w[76], w[70], pars->GC_11, amp[282]); 
  FFV1_0(w[88], w[83], w[70], pars->GC_11, amp[283]); 
  FFV1_0(w[71], w[54], w[97], pars->GC_11, amp[284]); 
  FFV1_0(w[98], w[76], w[70], pars->GC_11, amp[285]); 
  FFV1_0(w[91], w[81], w[70], pars->GC_11, amp[286]); 
  FFV1_0(w[91], w[76], w[72], pars->GC_11, amp[287]); 
  FFV1_0(w[99], w[76], w[70], pars->GC_11, amp[288]); 
  FFV1_0(w[77], w[100], w[70], pars->GC_11, amp[289]); 
  FFV1_0(w[77], w[101], w[70], pars->GC_11, amp[290]); 
  FFV1_0(w[77], w[83], w[72], pars->GC_11, amp[291]); 
  FFV1_0(w[71], w[54], w[102], pars->GC_11, amp[292]); 
  FFV1_0(w[77], w[103], w[70], pars->GC_11, amp[293]); 
  FFV1_0(w[104], w[83], w[56], pars->GC_11, amp[294]); 
  FFV1_0(w[88], w[105], w[56], pars->GC_11, amp[295]); 
  FFV1_0(w[88], w[83], w[73], pars->GC_11, amp[296]); 
  FFV2_0(w[104], w[109], w[3], pars->GC_100, amp[297]); 
  FFV1_0(w[96], w[76], w[73], pars->GC_11, amp[298]); 
  FFV1_0(w[96], w[109], w[4], pars->GC_11, amp[299]); 
  FFV1_0(w[106], w[81], w[56], pars->GC_11, amp[300]); 
  FFV1_0(w[91], w[107], w[56], pars->GC_11, amp[301]); 
  FFV1_0(w[91], w[81], w[73], pars->GC_11, amp[302]); 
  FFV1_0(w[106], w[76], w[59], pars->GC_11, amp[303]); 
  FFV1_0(w[55], w[74], w[97], pars->GC_11, amp[304]); 
  VVV1_0(w[4], w[97], w[59], pars->GC_10, amp[305]); 
  FFV1_0(w[106], w[76], w[61], pars->GC_11, amp[306]); 
  FFV1_0(w[75], w[54], w[97], pars->GC_11, amp[307]); 
  VVV1_0(w[4], w[97], w[61], pars->GC_10, amp[308]); 
  FFV2_5_0(w[106], w[109], w[2], pars->GC_51, pars->GC_58, amp[309]); 
  FFV1_0(w[98], w[76], w[73], pars->GC_11, amp[310]); 
  FFV1_0(w[98], w[109], w[4], pars->GC_11, amp[311]); 
  FFV1_0(w[99], w[76], w[73], pars->GC_11, amp[312]); 
  FFV1_0(w[77], w[100], w[73], pars->GC_11, amp[313]); 
  FFV1_0(w[99], w[109], w[4], pars->GC_11, amp[314]); 
  FFV1_0(w[111], w[100], w[4], pars->GC_11, amp[315]); 
  FFV2_0(w[111], w[107], w[3], pars->GC_100, amp[316]); 
  FFV1_0(w[77], w[101], w[73], pars->GC_11, amp[317]); 
  FFV1_0(w[111], w[101], w[4], pars->GC_11, amp[318]); 
  FFV1_0(w[55], w[74], w[102], pars->GC_11, amp[319]); 
  FFV1_0(w[77], w[105], w[59], pars->GC_11, amp[320]); 
  VVV1_0(w[4], w[102], w[59], pars->GC_10, amp[321]); 
  FFV1_0(w[75], w[54], w[102], pars->GC_11, amp[322]); 
  FFV1_0(w[77], w[105], w[61], pars->GC_11, amp[323]); 
  VVV1_0(w[4], w[102], w[61], pars->GC_10, amp[324]); 
  FFV2_3_0(w[111], w[105], w[2], pars->GC_50, pars->GC_58, amp[325]); 
  FFV1_0(w[77], w[103], w[73], pars->GC_11, amp[326]); 
  FFV1_0(w[111], w[103], w[4], pars->GC_11, amp[327]); 


}
double PY8MEs_R12_P86_sm_qb_zwmgqb::matrix_12_db_zwmgub() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 82;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[0] + 1./3. * amp[1] + 1./3. * amp[2] + 1./3.
      * amp[3] + 1./3. * amp[4] + 1./3. * amp[5] + 1./3. * amp[6] + 1./3. *
      amp[7] + 1./3. * amp[8] + 1./3. * amp[9] + 1./3. * amp[10] + 1./3. *
      amp[11] + 1./3. * amp[12] + 1./3. * amp[13] + 1./3. * amp[14] + 1./3. *
      amp[15] + 1./3. * amp[16] + 1./3. * amp[17] + 1./3. * amp[18] + 1./3. *
      amp[19] + 1./3. * amp[20] + 1./3. * amp[21] + 1./3. * amp[22] + 1./3. *
      amp[23] + 1./3. * amp[48] + 1./3. * amp[49] + 1./3. * amp[51] + 1./3. *
      amp[53] + 1./3. * amp[54] + 1./3. * amp[55] + 1./3. * amp[57] + 1./3. *
      amp[60] + 1./3. * amp[63] + 1./3. * amp[65] + 1./3. * amp[68] + 1./3. *
      amp[69] + 1./3. * amp[70] + 1./3. * amp[72] + 1./3. * amp[74] + 1./3. *
      amp[77] + 1./3. * amp[79] + 1./3. * amp[81]);
  jamp[1] = +1./2. * (-amp[12] - amp[13] - amp[14] - amp[15] - amp[16] -
      amp[17] - amp[18] - amp[19] - amp[20] - amp[21] - amp[22] - amp[23] -
      amp[36] - amp[37] - amp[38] - amp[39] - amp[40] - amp[41] - amp[42] -
      amp[43] - amp[44] - amp[45] - amp[46] - amp[47] - amp[49] +
      Complex<double> (0, 1) * amp[50] + Complex<double> (0, 1) * amp[52] -
      amp[55] + Complex<double> (0, 1) * amp[56] - Complex<double> (0, 1) *
      amp[59] - amp[61] - Complex<double> (0, 1) * amp[62] + Complex<double>
      (0, 1) * amp[64] + Complex<double> (0, 1) * amp[66] + Complex<double> (0,
      1) * amp[67] - amp[69] - amp[70] + Complex<double> (0, 1) * amp[71] -
      amp[72] - amp[74] - Complex<double> (0, 1) * amp[75] - amp[76] - amp[77]
      - Complex<double> (0, 1) * amp[78] - amp[79] + Complex<double> (0, 1) *
      amp[80] - amp[81]);
  jamp[2] = +1./2. * (-amp[0] - amp[1] - amp[2] - amp[3] - amp[4] - amp[5] -
      amp[6] - amp[7] - amp[8] - amp[9] - amp[10] - amp[11] - amp[24] - amp[25]
      - amp[26] - amp[27] - amp[28] - amp[29] - amp[30] - amp[31] - amp[32] -
      amp[33] - amp[34] - amp[35] - amp[48] - Complex<double> (0, 1) * amp[50]
      - amp[51] - Complex<double> (0, 1) * amp[52] - amp[53] - amp[54] -
      Complex<double> (0, 1) * amp[56] - amp[57] - amp[58] + Complex<double>
      (0, 1) * amp[59] - amp[60] + Complex<double> (0, 1) * amp[62] - amp[63] -
      Complex<double> (0, 1) * amp[64] - amp[65] - Complex<double> (0, 1) *
      amp[66] - Complex<double> (0, 1) * amp[67] - amp[68] - Complex<double>
      (0, 1) * amp[71] - amp[73] + Complex<double> (0, 1) * amp[75] +
      Complex<double> (0, 1) * amp[78] - Complex<double> (0, 1) * amp[80]);
  jamp[3] = +1./2. * (+1./3. * amp[24] + 1./3. * amp[25] + 1./3. * amp[26] +
      1./3. * amp[27] + 1./3. * amp[28] + 1./3. * amp[29] + 1./3. * amp[30] +
      1./3. * amp[31] + 1./3. * amp[32] + 1./3. * amp[33] + 1./3. * amp[34] +
      1./3. * amp[35] + 1./3. * amp[36] + 1./3. * amp[37] + 1./3. * amp[38] +
      1./3. * amp[39] + 1./3. * amp[40] + 1./3. * amp[41] + 1./3. * amp[42] +
      1./3. * amp[43] + 1./3. * amp[44] + 1./3. * amp[45] + 1./3. * amp[46] +
      1./3. * amp[47] + 1./3. * amp[58] + 1./3. * amp[61] + 1./3. * amp[73] +
      1./3. * amp[76]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[0][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P86_sm_qb_zwmgqb::matrix_12_dbx_zwmgubx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 82;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[94] + amp[95] + amp[96] + amp[97] + amp[98] +
      amp[99] + amp[100] + amp[101] + amp[102] + amp[103] + amp[104] + amp[105]
      + amp[118] + amp[119] + amp[120] + amp[121] + amp[122] + amp[123] +
      amp[124] + amp[125] + amp[126] + amp[127] + amp[128] + amp[129] +
      amp[131] - Complex<double> (0, 1) * amp[132] - Complex<double> (0, 1) *
      amp[134] + amp[137] - Complex<double> (0, 1) * amp[138] + Complex<double>
      (0, 1) * amp[141] + amp[143] + Complex<double> (0, 1) * amp[144] -
      Complex<double> (0, 1) * amp[146] - Complex<double> (0, 1) * amp[148] -
      Complex<double> (0, 1) * amp[149] + amp[151] + amp[152] - Complex<double>
      (0, 1) * amp[153] + amp[154] + amp[156] + Complex<double> (0, 1) *
      amp[157] + amp[158] + amp[159] + Complex<double> (0, 1) * amp[160] +
      amp[161] - Complex<double> (0, 1) * amp[162] + amp[163]);
  jamp[1] = +1./2. * (-1./3. * amp[82] - 1./3. * amp[83] - 1./3. * amp[84] -
      1./3. * amp[85] - 1./3. * amp[86] - 1./3. * amp[87] - 1./3. * amp[88] -
      1./3. * amp[89] - 1./3. * amp[90] - 1./3. * amp[91] - 1./3. * amp[92] -
      1./3. * amp[93] - 1./3. * amp[94] - 1./3. * amp[95] - 1./3. * amp[96] -
      1./3. * amp[97] - 1./3. * amp[98] - 1./3. * amp[99] - 1./3. * amp[100] -
      1./3. * amp[101] - 1./3. * amp[102] - 1./3. * amp[103] - 1./3. * amp[104]
      - 1./3. * amp[105] - 1./3. * amp[130] - 1./3. * amp[131] - 1./3. *
      amp[133] - 1./3. * amp[135] - 1./3. * amp[136] - 1./3. * amp[137] - 1./3.
      * amp[139] - 1./3. * amp[142] - 1./3. * amp[145] - 1./3. * amp[147] -
      1./3. * amp[150] - 1./3. * amp[151] - 1./3. * amp[152] - 1./3. * amp[154]
      - 1./3. * amp[156] - 1./3. * amp[159] - 1./3. * amp[161] - 1./3. *
      amp[163]);
  jamp[2] = +1./2. * (+amp[82] + amp[83] + amp[84] + amp[85] + amp[86] +
      amp[87] + amp[88] + amp[89] + amp[90] + amp[91] + amp[92] + amp[93] +
      amp[106] + amp[107] + amp[108] + amp[109] + amp[110] + amp[111] +
      amp[112] + amp[113] + amp[114] + amp[115] + amp[116] + amp[117] +
      amp[130] + Complex<double> (0, 1) * amp[132] + amp[133] + Complex<double>
      (0, 1) * amp[134] + amp[135] + amp[136] + Complex<double> (0, 1) *
      amp[138] + amp[139] + amp[140] - Complex<double> (0, 1) * amp[141] +
      amp[142] - Complex<double> (0, 1) * amp[144] + amp[145] + Complex<double>
      (0, 1) * amp[146] + amp[147] + Complex<double> (0, 1) * amp[148] +
      Complex<double> (0, 1) * amp[149] + amp[150] + Complex<double> (0, 1) *
      amp[153] + amp[155] - Complex<double> (0, 1) * amp[157] - Complex<double>
      (0, 1) * amp[160] + Complex<double> (0, 1) * amp[162]);
  jamp[3] = +1./2. * (-1./3. * amp[106] - 1./3. * amp[107] - 1./3. * amp[108] -
      1./3. * amp[109] - 1./3. * amp[110] - 1./3. * amp[111] - 1./3. * amp[112]
      - 1./3. * amp[113] - 1./3. * amp[114] - 1./3. * amp[115] - 1./3. *
      amp[116] - 1./3. * amp[117] - 1./3. * amp[118] - 1./3. * amp[119] - 1./3.
      * amp[120] - 1./3. * amp[121] - 1./3. * amp[122] - 1./3. * amp[123] -
      1./3. * amp[124] - 1./3. * amp[125] - 1./3. * amp[126] - 1./3. * amp[127]
      - 1./3. * amp[128] - 1./3. * amp[129] - 1./3. * amp[140] - 1./3. *
      amp[143] - 1./3. * amp[155] - 1./3. * amp[158]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[1][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P86_sm_qb_zwmgqb::matrix_12_uxb_zwmgdxb() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 82;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[164] + amp[165] + amp[166] + amp[167] + amp[168] +
      amp[169] + amp[170] + amp[171] + amp[172] + amp[173] + amp[174] +
      amp[175] + amp[188] + amp[189] + amp[190] + amp[191] + amp[192] +
      amp[193] + amp[194] + amp[195] + amp[196] + amp[197] + amp[198] +
      amp[199] + amp[212] + Complex<double> (0, 1) * amp[214] + amp[215] +
      Complex<double> (0, 1) * amp[216] + amp[217] + amp[218] + Complex<double>
      (0, 1) * amp[220] + amp[221] + amp[222] - Complex<double> (0, 1) *
      amp[223] + amp[224] - Complex<double> (0, 1) * amp[226] + amp[227] +
      Complex<double> (0, 1) * amp[228] + amp[229] + Complex<double> (0, 1) *
      amp[230] + Complex<double> (0, 1) * amp[231] + amp[232] + Complex<double>
      (0, 1) * amp[235] + amp[237] - Complex<double> (0, 1) * amp[239] -
      Complex<double> (0, 1) * amp[242] + Complex<double> (0, 1) * amp[244]);
  jamp[1] = +1./2. * (-1./3. * amp[188] - 1./3. * amp[189] - 1./3. * amp[190] -
      1./3. * amp[191] - 1./3. * amp[192] - 1./3. * amp[193] - 1./3. * amp[194]
      - 1./3. * amp[195] - 1./3. * amp[196] - 1./3. * amp[197] - 1./3. *
      amp[198] - 1./3. * amp[199] - 1./3. * amp[200] - 1./3. * amp[201] - 1./3.
      * amp[202] - 1./3. * amp[203] - 1./3. * amp[204] - 1./3. * amp[205] -
      1./3. * amp[206] - 1./3. * amp[207] - 1./3. * amp[208] - 1./3. * amp[209]
      - 1./3. * amp[210] - 1./3. * amp[211] - 1./3. * amp[222] - 1./3. *
      amp[225] - 1./3. * amp[237] - 1./3. * amp[240]);
  jamp[2] = +1./2. * (+amp[176] + amp[177] + amp[178] + amp[179] + amp[180] +
      amp[181] + amp[182] + amp[183] + amp[184] + amp[185] + amp[186] +
      amp[187] + amp[200] + amp[201] + amp[202] + amp[203] + amp[204] +
      amp[205] + amp[206] + amp[207] + amp[208] + amp[209] + amp[210] +
      amp[211] + amp[213] - Complex<double> (0, 1) * amp[214] - Complex<double>
      (0, 1) * amp[216] + amp[219] - Complex<double> (0, 1) * amp[220] +
      Complex<double> (0, 1) * amp[223] + amp[225] + Complex<double> (0, 1) *
      amp[226] - Complex<double> (0, 1) * amp[228] - Complex<double> (0, 1) *
      amp[230] - Complex<double> (0, 1) * amp[231] + amp[233] + amp[234] -
      Complex<double> (0, 1) * amp[235] + amp[236] + amp[238] + Complex<double>
      (0, 1) * amp[239] + amp[240] + amp[241] + Complex<double> (0, 1) *
      amp[242] + amp[243] - Complex<double> (0, 1) * amp[244] + amp[245]);
  jamp[3] = +1./2. * (-1./3. * amp[164] - 1./3. * amp[165] - 1./3. * amp[166] -
      1./3. * amp[167] - 1./3. * amp[168] - 1./3. * amp[169] - 1./3. * amp[170]
      - 1./3. * amp[171] - 1./3. * amp[172] - 1./3. * amp[173] - 1./3. *
      amp[174] - 1./3. * amp[175] - 1./3. * amp[176] - 1./3. * amp[177] - 1./3.
      * amp[178] - 1./3. * amp[179] - 1./3. * amp[180] - 1./3. * amp[181] -
      1./3. * amp[182] - 1./3. * amp[183] - 1./3. * amp[184] - 1./3. * amp[185]
      - 1./3. * amp[186] - 1./3. * amp[187] - 1./3. * amp[212] - 1./3. *
      amp[213] - 1./3. * amp[215] - 1./3. * amp[217] - 1./3. * amp[218] - 1./3.
      * amp[219] - 1./3. * amp[221] - 1./3. * amp[224] - 1./3. * amp[227] -
      1./3. * amp[229] - 1./3. * amp[232] - 1./3. * amp[233] - 1./3. * amp[234]
      - 1./3. * amp[236] - 1./3. * amp[238] - 1./3. * amp[241] - 1./3. *
      amp[243] - 1./3. * amp[245]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[2][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P86_sm_qb_zwmgqb::matrix_12_uxbx_zwmgdxbx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 82;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[270] + 1./3. * amp[271] + 1./3. * amp[272] +
      1./3. * amp[273] + 1./3. * amp[274] + 1./3. * amp[275] + 1./3. * amp[276]
      + 1./3. * amp[277] + 1./3. * amp[278] + 1./3. * amp[279] + 1./3. *
      amp[280] + 1./3. * amp[281] + 1./3. * amp[282] + 1./3. * amp[283] + 1./3.
      * amp[284] + 1./3. * amp[285] + 1./3. * amp[286] + 1./3. * amp[287] +
      1./3. * amp[288] + 1./3. * amp[289] + 1./3. * amp[290] + 1./3. * amp[291]
      + 1./3. * amp[292] + 1./3. * amp[293] + 1./3. * amp[304] + 1./3. *
      amp[307] + 1./3. * amp[319] + 1./3. * amp[322]);
  jamp[1] = +1./2. * (-amp[246] - amp[247] - amp[248] - amp[249] - amp[250] -
      amp[251] - amp[252] - amp[253] - amp[254] - amp[255] - amp[256] -
      amp[257] - amp[270] - amp[271] - amp[272] - amp[273] - amp[274] -
      amp[275] - amp[276] - amp[277] - amp[278] - amp[279] - amp[280] -
      amp[281] - amp[294] - Complex<double> (0, 1) * amp[296] - amp[297] -
      Complex<double> (0, 1) * amp[298] - amp[299] - amp[300] - Complex<double>
      (0, 1) * amp[302] - amp[303] - amp[304] + Complex<double> (0, 1) *
      amp[305] - amp[306] + Complex<double> (0, 1) * amp[308] - amp[309] -
      Complex<double> (0, 1) * amp[310] - amp[311] - Complex<double> (0, 1) *
      amp[312] - Complex<double> (0, 1) * amp[313] - amp[314] - Complex<double>
      (0, 1) * amp[317] - amp[319] + Complex<double> (0, 1) * amp[321] +
      Complex<double> (0, 1) * amp[324] - Complex<double> (0, 1) * amp[326]);
  jamp[2] = +1./2. * (-amp[258] - amp[259] - amp[260] - amp[261] - amp[262] -
      amp[263] - amp[264] - amp[265] - amp[266] - amp[267] - amp[268] -
      amp[269] - amp[282] - amp[283] - amp[284] - amp[285] - amp[286] -
      amp[287] - amp[288] - amp[289] - amp[290] - amp[291] - amp[292] -
      amp[293] - amp[295] + Complex<double> (0, 1) * amp[296] + Complex<double>
      (0, 1) * amp[298] - amp[301] + Complex<double> (0, 1) * amp[302] -
      Complex<double> (0, 1) * amp[305] - amp[307] - Complex<double> (0, 1) *
      amp[308] + Complex<double> (0, 1) * amp[310] + Complex<double> (0, 1) *
      amp[312] + Complex<double> (0, 1) * amp[313] - amp[315] - amp[316] +
      Complex<double> (0, 1) * amp[317] - amp[318] - amp[320] - Complex<double>
      (0, 1) * amp[321] - amp[322] - amp[323] - Complex<double> (0, 1) *
      amp[324] - amp[325] + Complex<double> (0, 1) * amp[326] - amp[327]);
  jamp[3] = +1./2. * (+1./3. * amp[246] + 1./3. * amp[247] + 1./3. * amp[248] +
      1./3. * amp[249] + 1./3. * amp[250] + 1./3. * amp[251] + 1./3. * amp[252]
      + 1./3. * amp[253] + 1./3. * amp[254] + 1./3. * amp[255] + 1./3. *
      amp[256] + 1./3. * amp[257] + 1./3. * amp[258] + 1./3. * amp[259] + 1./3.
      * amp[260] + 1./3. * amp[261] + 1./3. * amp[262] + 1./3. * amp[263] +
      1./3. * amp[264] + 1./3. * amp[265] + 1./3. * amp[266] + 1./3. * amp[267]
      + 1./3. * amp[268] + 1./3. * amp[269] + 1./3. * amp[294] + 1./3. *
      amp[295] + 1./3. * amp[297] + 1./3. * amp[299] + 1./3. * amp[300] + 1./3.
      * amp[301] + 1./3. * amp[303] + 1./3. * amp[306] + 1./3. * amp[309] +
      1./3. * amp[311] + 1./3. * amp[314] + 1./3. * amp[315] + 1./3. * amp[316]
      + 1./3. * amp[318] + 1./3. * amp[320] + 1./3. * amp[323] + 1./3. *
      amp[325] + 1./3. * amp[327]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[3][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}


}  // end namespace PY8MEs_namespace

