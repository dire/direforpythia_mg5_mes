//==========================================================================
// This file has been automatically generated for Pythia 8 by
// MadGraph5_aMC@NLO v. 2.6.0, 2017-08-16
// By the MadGraph5_aMC@NLO Development Team
// Visit launchpad.net/madgraph5 and amcatnlo.web.cern.ch
//==========================================================================

#include "PY8MEs_R16_P4_sm_gb_ttxggb.h"
#include "HelAmps_sm.h"

using namespace Pythia8_sm; 

namespace PY8MEs_namespace 
{
//==========================================================================
// Class member functions for calculating the matrix elements for
// Process: g b > t t~ g g b WEIGHTED<=5 @16
// Process: g b~ > t t~ g g b~ WEIGHTED<=5 @16

// Exception class
class PY8MEs_R16_P4_sm_gb_ttxggbException : public exception
{
  virtual const char * what() const throw()
  {
    return "Exception in class 'PY8MEs_R16_P4_sm_gb_ttxggb'."; 
  }
}
PY8MEs_R16_P4_sm_gb_ttxggb_exception; 

std::set<int> PY8MEs_R16_P4_sm_gb_ttxggb::s_channel_proc = std::set<int>
    (createset<int> ());

int PY8MEs_R16_P4_sm_gb_ttxggb::helicities[ncomb][nexternal] = {{-1, -1, -1,
    -1, -1, -1, -1}, {-1, -1, -1, -1, -1, -1, 1}, {-1, -1, -1, -1, -1, 1, -1},
    {-1, -1, -1, -1, -1, 1, 1}, {-1, -1, -1, -1, 1, -1, -1}, {-1, -1, -1, -1,
    1, -1, 1}, {-1, -1, -1, -1, 1, 1, -1}, {-1, -1, -1, -1, 1, 1, 1}, {-1, -1,
    -1, 1, -1, -1, -1}, {-1, -1, -1, 1, -1, -1, 1}, {-1, -1, -1, 1, -1, 1, -1},
    {-1, -1, -1, 1, -1, 1, 1}, {-1, -1, -1, 1, 1, -1, -1}, {-1, -1, -1, 1, 1,
    -1, 1}, {-1, -1, -1, 1, 1, 1, -1}, {-1, -1, -1, 1, 1, 1, 1}, {-1, -1, 1,
    -1, -1, -1, -1}, {-1, -1, 1, -1, -1, -1, 1}, {-1, -1, 1, -1, -1, 1, -1},
    {-1, -1, 1, -1, -1, 1, 1}, {-1, -1, 1, -1, 1, -1, -1}, {-1, -1, 1, -1, 1,
    -1, 1}, {-1, -1, 1, -1, 1, 1, -1}, {-1, -1, 1, -1, 1, 1, 1}, {-1, -1, 1, 1,
    -1, -1, -1}, {-1, -1, 1, 1, -1, -1, 1}, {-1, -1, 1, 1, -1, 1, -1}, {-1, -1,
    1, 1, -1, 1, 1}, {-1, -1, 1, 1, 1, -1, -1}, {-1, -1, 1, 1, 1, -1, 1}, {-1,
    -1, 1, 1, 1, 1, -1}, {-1, -1, 1, 1, 1, 1, 1}, {-1, 1, -1, -1, -1, -1, -1},
    {-1, 1, -1, -1, -1, -1, 1}, {-1, 1, -1, -1, -1, 1, -1}, {-1, 1, -1, -1, -1,
    1, 1}, {-1, 1, -1, -1, 1, -1, -1}, {-1, 1, -1, -1, 1, -1, 1}, {-1, 1, -1,
    -1, 1, 1, -1}, {-1, 1, -1, -1, 1, 1, 1}, {-1, 1, -1, 1, -1, -1, -1}, {-1,
    1, -1, 1, -1, -1, 1}, {-1, 1, -1, 1, -1, 1, -1}, {-1, 1, -1, 1, -1, 1, 1},
    {-1, 1, -1, 1, 1, -1, -1}, {-1, 1, -1, 1, 1, -1, 1}, {-1, 1, -1, 1, 1, 1,
    -1}, {-1, 1, -1, 1, 1, 1, 1}, {-1, 1, 1, -1, -1, -1, -1}, {-1, 1, 1, -1,
    -1, -1, 1}, {-1, 1, 1, -1, -1, 1, -1}, {-1, 1, 1, -1, -1, 1, 1}, {-1, 1, 1,
    -1, 1, -1, -1}, {-1, 1, 1, -1, 1, -1, 1}, {-1, 1, 1, -1, 1, 1, -1}, {-1, 1,
    1, -1, 1, 1, 1}, {-1, 1, 1, 1, -1, -1, -1}, {-1, 1, 1, 1, -1, -1, 1}, {-1,
    1, 1, 1, -1, 1, -1}, {-1, 1, 1, 1, -1, 1, 1}, {-1, 1, 1, 1, 1, -1, -1},
    {-1, 1, 1, 1, 1, -1, 1}, {-1, 1, 1, 1, 1, 1, -1}, {-1, 1, 1, 1, 1, 1, 1},
    {1, -1, -1, -1, -1, -1, -1}, {1, -1, -1, -1, -1, -1, 1}, {1, -1, -1, -1,
    -1, 1, -1}, {1, -1, -1, -1, -1, 1, 1}, {1, -1, -1, -1, 1, -1, -1}, {1, -1,
    -1, -1, 1, -1, 1}, {1, -1, -1, -1, 1, 1, -1}, {1, -1, -1, -1, 1, 1, 1}, {1,
    -1, -1, 1, -1, -1, -1}, {1, -1, -1, 1, -1, -1, 1}, {1, -1, -1, 1, -1, 1,
    -1}, {1, -1, -1, 1, -1, 1, 1}, {1, -1, -1, 1, 1, -1, -1}, {1, -1, -1, 1, 1,
    -1, 1}, {1, -1, -1, 1, 1, 1, -1}, {1, -1, -1, 1, 1, 1, 1}, {1, -1, 1, -1,
    -1, -1, -1}, {1, -1, 1, -1, -1, -1, 1}, {1, -1, 1, -1, -1, 1, -1}, {1, -1,
    1, -1, -1, 1, 1}, {1, -1, 1, -1, 1, -1, -1}, {1, -1, 1, -1, 1, -1, 1}, {1,
    -1, 1, -1, 1, 1, -1}, {1, -1, 1, -1, 1, 1, 1}, {1, -1, 1, 1, -1, -1, -1},
    {1, -1, 1, 1, -1, -1, 1}, {1, -1, 1, 1, -1, 1, -1}, {1, -1, 1, 1, -1, 1,
    1}, {1, -1, 1, 1, 1, -1, -1}, {1, -1, 1, 1, 1, -1, 1}, {1, -1, 1, 1, 1, 1,
    -1}, {1, -1, 1, 1, 1, 1, 1}, {1, 1, -1, -1, -1, -1, -1}, {1, 1, -1, -1, -1,
    -1, 1}, {1, 1, -1, -1, -1, 1, -1}, {1, 1, -1, -1, -1, 1, 1}, {1, 1, -1, -1,
    1, -1, -1}, {1, 1, -1, -1, 1, -1, 1}, {1, 1, -1, -1, 1, 1, -1}, {1, 1, -1,
    -1, 1, 1, 1}, {1, 1, -1, 1, -1, -1, -1}, {1, 1, -1, 1, -1, -1, 1}, {1, 1,
    -1, 1, -1, 1, -1}, {1, 1, -1, 1, -1, 1, 1}, {1, 1, -1, 1, 1, -1, -1}, {1,
    1, -1, 1, 1, -1, 1}, {1, 1, -1, 1, 1, 1, -1}, {1, 1, -1, 1, 1, 1, 1}, {1,
    1, 1, -1, -1, -1, -1}, {1, 1, 1, -1, -1, -1, 1}, {1, 1, 1, -1, -1, 1, -1},
    {1, 1, 1, -1, -1, 1, 1}, {1, 1, 1, -1, 1, -1, -1}, {1, 1, 1, -1, 1, -1, 1},
    {1, 1, 1, -1, 1, 1, -1}, {1, 1, 1, -1, 1, 1, 1}, {1, 1, 1, 1, -1, -1, -1},
    {1, 1, 1, 1, -1, -1, 1}, {1, 1, 1, 1, -1, 1, -1}, {1, 1, 1, 1, -1, 1, 1},
    {1, 1, 1, 1, 1, -1, -1}, {1, 1, 1, 1, 1, -1, 1}, {1, 1, 1, 1, 1, 1, -1},
    {1, 1, 1, 1, 1, 1, 1}};

// Normalization factors the various processes
// Denominators: spins, colors and identical particles
int PY8MEs_R16_P4_sm_gb_ttxggb::denom_colors[nprocesses] = {24, 24}; 
int PY8MEs_R16_P4_sm_gb_ttxggb::denom_hels[nprocesses] = {4, 4}; 
int PY8MEs_R16_P4_sm_gb_ttxggb::denom_iden[nprocesses] = {2, 2}; 

//--------------------------------------------------------------------------
// Color config initialization
void PY8MEs_R16_P4_sm_gb_ttxggb::initColorConfigs() 
{
  color_configs = vector < vec_vec_int > (); 
  jamp_nc_relative_power = vector < vec_int > (); 

  // Color flows of process Process: g b > t t~ g g b WEIGHTED<=5 @16
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(3)(3)(0)(1)(0)(0)(5)(4)(2)(5)(4)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #1
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(3)(3)(0)(1)(0)(0)(4)(4)(5)(5)(2)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #2
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(3)(5)(0)(1)(0)(0)(3)(4)(2)(5)(4)(2)(0)));
  jamp_nc_relative_power[0].push_back(-1); 
  // JAMP #3
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(3)(4)(0)(1)(0)(0)(3)(4)(5)(5)(2)(2)(0)));
  jamp_nc_relative_power[0].push_back(-1); 
  // JAMP #4
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(3)(4)(0)(1)(0)(0)(5)(4)(3)(5)(2)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #5
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(3)(5)(0)(1)(0)(0)(4)(4)(3)(5)(2)(2)(0)));
  jamp_nc_relative_power[0].push_back(-1); 
  // JAMP #6
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(3)(5)(0)(1)(0)(0)(2)(4)(3)(5)(4)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #7
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(3)(2)(0)(1)(0)(0)(5)(4)(3)(5)(4)(2)(0)));
  jamp_nc_relative_power[0].push_back(-1); 
  // JAMP #8
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(3)(5)(0)(1)(0)(0)(1)(4)(3)(5)(4)(2)(0)));
  jamp_nc_relative_power[0].push_back(-1); 
  // JAMP #9
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(3)(1)(0)(1)(0)(0)(5)(4)(3)(5)(4)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #10
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(3)(4)(0)(1)(0)(0)(5)(4)(3)(5)(1)(2)(0)));
  jamp_nc_relative_power[0].push_back(-1); 
  // JAMP #11
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(3)(5)(0)(1)(0)(0)(4)(4)(3)(5)(1)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #12
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(3)(5)(0)(1)(0)(0)(4)(4)(2)(5)(3)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #13
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(3)(4)(0)(1)(0)(0)(5)(4)(2)(5)(3)(2)(0)));
  jamp_nc_relative_power[0].push_back(-1); 
  // JAMP #14
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(3)(4)(0)(1)(0)(0)(2)(4)(5)(5)(3)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #15
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(3)(2)(0)(1)(0)(0)(4)(4)(5)(5)(3)(2)(0)));
  jamp_nc_relative_power[0].push_back(-1); 
  // JAMP #16
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(3)(4)(0)(1)(0)(0)(1)(4)(5)(5)(3)(2)(0)));
  jamp_nc_relative_power[0].push_back(-1); 
  // JAMP #17
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(3)(1)(0)(1)(0)(0)(4)(4)(5)(5)(3)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #18
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(3)(5)(0)(1)(0)(0)(4)(4)(1)(5)(3)(2)(0)));
  jamp_nc_relative_power[0].push_back(-1); 
  // JAMP #19
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(3)(4)(0)(1)(0)(0)(5)(4)(1)(5)(3)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #20
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(3)(3)(0)(1)(0)(0)(5)(4)(1)(5)(4)(2)(0)));
  jamp_nc_relative_power[0].push_back(-1); 
  // JAMP #21
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(3)(3)(0)(1)(0)(0)(4)(4)(5)(5)(1)(2)(0)));
  jamp_nc_relative_power[0].push_back(-1); 
  // JAMP #22
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(3)(5)(0)(1)(0)(0)(3)(4)(1)(5)(4)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #23
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(3)(4)(0)(1)(0)(0)(3)(4)(5)(5)(1)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #24
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(3)(1)(0)(1)(0)(0)(5)(4)(2)(5)(3)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #25
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(3)(1)(0)(1)(0)(0)(3)(4)(2)(5)(4)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #26
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(3)(1)(0)(1)(0)(0)(4)(4)(3)(5)(2)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #27
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(3)(1)(0)(1)(0)(0)(3)(4)(5)(5)(2)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #28
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(3)(5)(0)(1)(0)(0)(1)(4)(2)(5)(3)(2)(0)));
  jamp_nc_relative_power[0].push_back(-1); 
  // JAMP #29
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(3)(3)(0)(1)(0)(0)(1)(4)(2)(5)(4)(2)(0)));
  jamp_nc_relative_power[0].push_back(-1); 
  // JAMP #30
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(3)(4)(0)(1)(0)(0)(1)(4)(3)(5)(2)(2)(0)));
  jamp_nc_relative_power[0].push_back(-1); 
  // JAMP #31
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(3)(3)(0)(1)(0)(0)(1)(4)(5)(5)(2)(2)(0)));
  jamp_nc_relative_power[0].push_back(-1); 
  // JAMP #32
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(3)(3)(0)(1)(0)(0)(5)(4)(1)(5)(2)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #33
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(3)(5)(0)(1)(0)(0)(3)(4)(1)(5)(2)(2)(0)));
  jamp_nc_relative_power[0].push_back(-1); 
  // JAMP #34
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(3)(5)(0)(1)(0)(0)(2)(4)(1)(5)(3)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #35
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(3)(2)(0)(1)(0)(0)(5)(4)(1)(5)(3)(2)(0)));
  jamp_nc_relative_power[0].push_back(-1); 
  // JAMP #36
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(3)(3)(0)(1)(0)(0)(5)(4)(2)(5)(1)(2)(0)));
  jamp_nc_relative_power[0].push_back(-1); 
  // JAMP #37
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(3)(5)(0)(1)(0)(0)(3)(4)(2)(5)(1)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #38
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(3)(4)(0)(1)(0)(0)(3)(4)(1)(5)(2)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #39
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(3)(3)(0)(1)(0)(0)(4)(4)(1)(5)(2)(2)(0)));
  jamp_nc_relative_power[0].push_back(-1); 
  // JAMP #40
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(3)(3)(0)(1)(0)(0)(2)(4)(1)(5)(4)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #41
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(3)(2)(0)(1)(0)(0)(3)(4)(1)(5)(4)(2)(0)));
  jamp_nc_relative_power[0].push_back(-1); 
  // JAMP #42
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(3)(4)(0)(1)(0)(0)(3)(4)(2)(5)(1)(2)(0)));
  jamp_nc_relative_power[0].push_back(-1); 
  // JAMP #43
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(3)(3)(0)(1)(0)(0)(4)(4)(2)(5)(1)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #44
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(3)(4)(0)(1)(0)(0)(2)(4)(3)(5)(1)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #45
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(3)(2)(0)(1)(0)(0)(4)(4)(3)(5)(1)(2)(0)));
  jamp_nc_relative_power[0].push_back(-1); 
  // JAMP #46
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(3)(3)(0)(1)(0)(0)(2)(4)(5)(5)(1)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #47
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(3)(2)(0)(1)(0)(0)(3)(4)(5)(5)(1)(2)(0)));
  jamp_nc_relative_power[0].push_back(-1); 

  // Color flows of process Process: g b~ > t t~ g g b~ WEIGHTED<=5 @16
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(3)(0)(1)(2)(0)(0)(3)(4)(2)(5)(4)(0)(5)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #1
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(3)(0)(1)(2)(0)(0)(3)(4)(5)(5)(2)(0)(4)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #2
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(3)(0)(1)(2)(0)(0)(5)(4)(2)(5)(4)(0)(3)));
  jamp_nc_relative_power[1].push_back(-1); 
  // JAMP #3
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(3)(0)(1)(2)(0)(0)(4)(4)(5)(5)(2)(0)(3)));
  jamp_nc_relative_power[1].push_back(-1); 
  // JAMP #4
  color_configs[1].push_back(vec_int(createvector<int>
      (2)(3)(0)(1)(2)(0)(0)(3)(4)(1)(5)(4)(0)(5)));
  jamp_nc_relative_power[1].push_back(-1); 
  // JAMP #5
  color_configs[1].push_back(vec_int(createvector<int>
      (2)(3)(0)(1)(2)(0)(0)(3)(4)(5)(5)(1)(0)(4)));
  jamp_nc_relative_power[1].push_back(-1); 
  // JAMP #6
  color_configs[1].push_back(vec_int(createvector<int>
      (2)(3)(0)(1)(2)(0)(0)(5)(4)(1)(5)(4)(0)(3)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #7
  color_configs[1].push_back(vec_int(createvector<int>
      (2)(3)(0)(1)(2)(0)(0)(4)(4)(5)(5)(1)(0)(3)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #8
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(3)(0)(1)(2)(0)(0)(4)(4)(3)(5)(2)(0)(5)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #9
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(3)(0)(1)(2)(0)(0)(5)(4)(3)(5)(2)(0)(4)));
  jamp_nc_relative_power[1].push_back(-1); 
  // JAMP #10
  color_configs[1].push_back(vec_int(createvector<int>
      (2)(3)(0)(1)(2)(0)(0)(4)(4)(3)(5)(1)(0)(5)));
  jamp_nc_relative_power[1].push_back(-1); 
  // JAMP #11
  color_configs[1].push_back(vec_int(createvector<int>
      (2)(3)(0)(1)(2)(0)(0)(5)(4)(3)(5)(1)(0)(4)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #12
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(3)(0)(1)(2)(0)(0)(5)(4)(3)(5)(4)(0)(2)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #13
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(3)(0)(1)(2)(0)(0)(2)(4)(3)(5)(4)(0)(5)));
  jamp_nc_relative_power[1].push_back(-1); 
  // JAMP #14
  color_configs[1].push_back(vec_int(createvector<int>
      (2)(3)(0)(1)(2)(0)(0)(5)(4)(3)(5)(4)(0)(1)));
  jamp_nc_relative_power[1].push_back(-1); 
  // JAMP #15
  color_configs[1].push_back(vec_int(createvector<int>
      (2)(3)(0)(1)(2)(0)(0)(1)(4)(3)(5)(4)(0)(5)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #16
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(3)(0)(1)(2)(0)(0)(5)(4)(2)(5)(3)(0)(4)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #17
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(3)(0)(1)(2)(0)(0)(4)(4)(2)(5)(3)(0)(5)));
  jamp_nc_relative_power[1].push_back(-1); 
  // JAMP #18
  color_configs[1].push_back(vec_int(createvector<int>
      (2)(3)(0)(1)(2)(0)(0)(5)(4)(1)(5)(3)(0)(4)));
  jamp_nc_relative_power[1].push_back(-1); 
  // JAMP #19
  color_configs[1].push_back(vec_int(createvector<int>
      (2)(3)(0)(1)(2)(0)(0)(4)(4)(1)(5)(3)(0)(5)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #20
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(3)(0)(1)(2)(0)(0)(4)(4)(5)(5)(3)(0)(2)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #21
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(3)(0)(1)(2)(0)(0)(2)(4)(5)(5)(3)(0)(4)));
  jamp_nc_relative_power[1].push_back(-1); 
  // JAMP #22
  color_configs[1].push_back(vec_int(createvector<int>
      (2)(3)(0)(1)(2)(0)(0)(4)(4)(5)(5)(3)(0)(1)));
  jamp_nc_relative_power[1].push_back(-1); 
  // JAMP #23
  color_configs[1].push_back(vec_int(createvector<int>
      (2)(3)(0)(1)(2)(0)(0)(1)(4)(5)(5)(3)(0)(4)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #24
  color_configs[1].push_back(vec_int(createvector<int>
      (4)(3)(0)(1)(2)(0)(0)(1)(4)(2)(5)(3)(0)(5)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #25
  color_configs[1].push_back(vec_int(createvector<int>
      (5)(3)(0)(1)(2)(0)(0)(1)(4)(2)(5)(4)(0)(3)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #26
  color_configs[1].push_back(vec_int(createvector<int>
      (5)(3)(0)(1)(2)(0)(0)(1)(4)(3)(5)(2)(0)(4)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #27
  color_configs[1].push_back(vec_int(createvector<int>
      (4)(3)(0)(1)(2)(0)(0)(1)(4)(5)(5)(2)(0)(3)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #28
  color_configs[1].push_back(vec_int(createvector<int>
      (4)(3)(0)(1)(2)(0)(0)(5)(4)(2)(5)(3)(0)(1)));
  jamp_nc_relative_power[1].push_back(-1); 
  // JAMP #29
  color_configs[1].push_back(vec_int(createvector<int>
      (5)(3)(0)(1)(2)(0)(0)(3)(4)(2)(5)(4)(0)(1)));
  jamp_nc_relative_power[1].push_back(-1); 
  // JAMP #30
  color_configs[1].push_back(vec_int(createvector<int>
      (5)(3)(0)(1)(2)(0)(0)(4)(4)(3)(5)(2)(0)(1)));
  jamp_nc_relative_power[1].push_back(-1); 
  // JAMP #31
  color_configs[1].push_back(vec_int(createvector<int>
      (4)(3)(0)(1)(2)(0)(0)(3)(4)(5)(5)(2)(0)(1)));
  jamp_nc_relative_power[1].push_back(-1); 
  // JAMP #32
  color_configs[1].push_back(vec_int(createvector<int>
      (4)(3)(0)(1)(2)(0)(0)(2)(4)(1)(5)(3)(0)(5)));
  jamp_nc_relative_power[1].push_back(-1); 
  // JAMP #33
  color_configs[1].push_back(vec_int(createvector<int>
      (5)(3)(0)(1)(2)(0)(0)(2)(4)(1)(5)(4)(0)(3)));
  jamp_nc_relative_power[1].push_back(-1); 
  // JAMP #34
  color_configs[1].push_back(vec_int(createvector<int>
      (5)(3)(0)(1)(2)(0)(0)(2)(4)(3)(5)(1)(0)(4)));
  jamp_nc_relative_power[1].push_back(-1); 
  // JAMP #35
  color_configs[1].push_back(vec_int(createvector<int>
      (4)(3)(0)(1)(2)(0)(0)(2)(4)(5)(5)(1)(0)(3)));
  jamp_nc_relative_power[1].push_back(-1); 
  // JAMP #36
  color_configs[1].push_back(vec_int(createvector<int>
      (4)(3)(0)(1)(2)(0)(0)(5)(4)(1)(5)(3)(0)(2)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #37
  color_configs[1].push_back(vec_int(createvector<int>
      (5)(3)(0)(1)(2)(0)(0)(3)(4)(1)(5)(4)(0)(2)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #38
  color_configs[1].push_back(vec_int(createvector<int>
      (5)(3)(0)(1)(2)(0)(0)(4)(4)(3)(5)(1)(0)(2)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #39
  color_configs[1].push_back(vec_int(createvector<int>
      (4)(3)(0)(1)(2)(0)(0)(3)(4)(5)(5)(1)(0)(2)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #40
  color_configs[1].push_back(vec_int(createvector<int>
      (4)(3)(0)(1)(2)(0)(0)(3)(4)(1)(5)(2)(0)(5)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #41
  color_configs[1].push_back(vec_int(createvector<int>
      (4)(3)(0)(1)(2)(0)(0)(5)(4)(1)(5)(2)(0)(3)));
  jamp_nc_relative_power[1].push_back(-1); 
  // JAMP #42
  color_configs[1].push_back(vec_int(createvector<int>
      (4)(3)(0)(1)(2)(0)(0)(3)(4)(2)(5)(1)(0)(5)));
  jamp_nc_relative_power[1].push_back(-1); 
  // JAMP #43
  color_configs[1].push_back(vec_int(createvector<int>
      (4)(3)(0)(1)(2)(0)(0)(5)(4)(2)(5)(1)(0)(3)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #44
  color_configs[1].push_back(vec_int(createvector<int>
      (5)(3)(0)(1)(2)(0)(0)(4)(4)(1)(5)(2)(0)(3)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #45
  color_configs[1].push_back(vec_int(createvector<int>
      (5)(3)(0)(1)(2)(0)(0)(3)(4)(1)(5)(2)(0)(4)));
  jamp_nc_relative_power[1].push_back(-1); 
  // JAMP #46
  color_configs[1].push_back(vec_int(createvector<int>
      (5)(3)(0)(1)(2)(0)(0)(4)(4)(2)(5)(1)(0)(3)));
  jamp_nc_relative_power[1].push_back(-1); 
  // JAMP #47
  color_configs[1].push_back(vec_int(createvector<int>
      (5)(3)(0)(1)(2)(0)(0)(3)(4)(2)(5)(1)(0)(4)));
  jamp_nc_relative_power[1].push_back(0); 
}

//--------------------------------------------------------------------------
// Destructor.
PY8MEs_R16_P4_sm_gb_ttxggb::~PY8MEs_R16_P4_sm_gb_ttxggb() 
{
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    delete[] p[i]; 
    p[i] = NULL; 
  }
}

//--------------------------------------------------------------------------
// Invert the permutation mapping
vector<int> PY8MEs_R16_P4_sm_gb_ttxggb::invert_mapping(vector<int> mapping) 
{
  vector<int> inverted_mapping; 
  for (unsigned int i = 0; i < mapping.size(); i++ )
  {
    for (unsigned int j = 0; j < mapping.size(); j++ )
    {
      if (mapping[j] == ((int)i))
      {
        inverted_mapping.push_back(j); 
        break; 
      }
    }
  }
  return inverted_mapping; 
}

//--------------------------------------------------------------------------
// Return the list of possible helicity configurations
vector < vec_int > PY8MEs_R16_P4_sm_gb_ttxggb::getHelicityConfigs(vector<int>
    permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(ncomb, vector<int> (nexternal, 0)); 
  for (unsigned int ihel = 0; ihel < ncomb; ihel++ )
  {
    for(unsigned int j = 0; j < nexternal; j++ )
    {
      res[ihel][chosenPerm[j]] = helicities[ihel][j]; 
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Return the list of possible color configurations
vector < vec_int > PY8MEs_R16_P4_sm_gb_ttxggb::getColorConfigs(int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(color_configs[chosenProcID].size(), vector<int>
      (nexternal * 2, 0));
  for (unsigned int icol = 0; icol < color_configs[chosenProcID].size(); icol++
      )
  {
    for(unsigned int j = 0; j < (2 * nexternal); j++ )
    {
      res[icol][chosenPerm[j/2] * 2 + j%2] =
          color_configs[chosenProcID][icol][j];
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Get JAMP relative N_c power
int PY8MEs_R16_P4_sm_gb_ttxggb::getColorFlowRelativeNCPower(int color_flow_ID,
    int specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return jamp_nc_relative_power[chosenProcID][color_flow_ID]; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity ID -> Helicity Config
vector<int> PY8MEs_R16_P4_sm_gb_ttxggb::getHelicityConfigForID(int hel_ID,
    vector<int> permutation)
{
  if (hel_ID < 0 || hel_ID >= ncomb)
  {
    cerr <<  "Error in function 'getHelicityConfigForID' of class" << 
    " 'PY8MEs_R16_P4_sm_gb_ttxggb': Specified helicity ID '" << 
    hel_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R16_P4_sm_gb_ttxggb_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(nexternal, 0); 
  for (unsigned int j = 0; j < nexternal; j++ )
  {
    res[chosenPerm[j]] = helicities[hel_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity Config -> Helicity ID
int PY8MEs_R16_P4_sm_gb_ttxggb::getHelicityIDForConfig(vector<int> hel_config,
    vector<int> permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  int user_ihel = -1; 
  if (hel_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < ncomb; i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < nexternal; j++ )
      {
        if (helicities[i][chosenPerm[j]] != hel_config[j])
        {
          found = false; 
          break; 
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_ihel = i; 
        break; 
      }
    }
    if (user_ihel == -1)
    {
      cerr <<  "Error in function 'getHelicityIDForConfig' of class" << 
      " 'PY8MEs_R16_P4_sm_gb_ttxggb': Specified helicity" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R16_P4_sm_gb_ttxggb_exception; 
    }
  }
  return user_ihel; 
}


//--------------------------------------------------------------------------
// Implements the map Color ID -> Color Config
vector<int> PY8MEs_R16_P4_sm_gb_ttxggb::getColorConfigForID(int color_ID, int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < 0 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getColorConfigForID' of class" << 
    " 'PY8MEs_R16_P4_sm_gb_ttxggb': Specified color ID '" << 
    color_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R16_P4_sm_gb_ttxggb_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(color_configs[chosenProcID][color_ID].size(), 0); 
  for (unsigned int j = 0; j < (2 * nexternal); j++ )
  {
    res[chosenPerm[j/2] * 2 + j%2] = color_configs[chosenProcID][color_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Color Config -> Color ID
int PY8MEs_R16_P4_sm_gb_ttxggb::getColorIDForConfig(vector<int> color_config,
    int specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = -1; 
  if (color_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < color_configs[chosenProcID].size(); i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < (nexternal * 2); j++ )
      {

        // If colorless then make sure it matches
        // The little arithmetics in the color index is just
        // the permutation applies on the particle list which is
        // twice smaller since each particle can have two color indices.
        if (color_config[j] == 0)
        {
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] != 0)
          {
            found = false; 
            break; 
          }
          // Otherwise check that the color linked position matches
        }
        else
        {
          int color_linked_pos = -1; 
          // Find the other end of the line in the user color config
          for (unsigned int k = 0; k < (nexternal * 2); k++ )
          {
            if (k == j)
              continue; 
            if (color_config[j] == color_config[k])
            {
              color_linked_pos = k; 
              break; 
            }
          }
          if (color_linked_pos == -1)
          {
            cerr <<  "Error in function 'getColorIDForConfig' of class" << 
            " 'PY8MEs_R16_P4_sm_gb_ttxggb': A color line could " << 
            " not be closed." << endl; 
            throw PY8MEs_R16_P4_sm_gb_ttxggb_exception; 
          }
          // Now check whether the color line matches
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] !=
              color_configs[chosenProcID][i][chosenPerm[color_linked_pos/2] * 2
              + color_linked_pos%2])
          {
            found = false; 
            break; 
          }
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_icol = i; 
        break; 
      }
    }

    if (user_icol == -1)
    {
      cerr <<  "Error in function 'getColorIDForConfig' of class" << 
      " 'PY8MEs_R16_P4_sm_gb_ttxggb': Specified color" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R16_P4_sm_gb_ttxggb_exception; 
    }
  }
  return user_icol; 
}

//--------------------------------------------------------------------------
// Returns all result previously computed in SigmaKin
vector < vec_double > PY8MEs_R16_P4_sm_gb_ttxggb::getAllResults(int
    specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return all_results[chosenProcID]; 
}

//--------------------------------------------------------------------------
// Returns a result previously computed in SigmaKin for a specific helicity
// and color ID. -1 means avg and summed over that characteristic.
double PY8MEs_R16_P4_sm_gb_ttxggb::getResult(int helicity_ID, int color_ID, int
    specify_proc_ID)
{
  if (helicity_ID < - 1 || helicity_ID >= ncomb)
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R16_P4_sm_gb_ttxggb': Specified helicity ID '" << 
    helicity_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R16_P4_sm_gb_ttxggb_exception; 
  }
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < - 1 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R16_P4_sm_gb_ttxggb': Specified color ID '" << 
    color_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R16_P4_sm_gb_ttxggb_exception; 
  }
  return all_results[chosenProcID][helicity_ID + 1][color_ID + 1]; 
}

//--------------------------------------------------------------------------
// Check for the availability of the requested process and if available,
// If available, this returns the corresponding permutation and Proc_ID to use.
// If not available, this returns a negative Proc_ID.
pair < vector<int> , int > PY8MEs_R16_P4_sm_gb_ttxggb::static_getPY8ME(vector<int> initial_pdgs, vector<int> final_pdgs, set<int> schannels) 
{

  // Not available return value
  pair < vector<int> , int > NA(vector<int> (), -1); 

  // Check if s-channel requirements match
  if (nreq_s_channels > 0)
  {
    if (schannels != s_channel_proc)
      return NA; 
  }
  else
  {
    if (schannels.size() != 0)
      return NA; 
  }

  // Check number of final state particles
  if (final_pdgs.size() != (nexternal - ninitial))
    return NA; 

  // Check number of initial state particles
  if (initial_pdgs.size() != ninitial)
    return NA; 

  // List of processes available in this class
  const int nprocs = 4; 
  const int proc_IDS[nprocs] = {0, 1, 0, 1}; 
  const int in_pdgs[nprocs][ninitial] = {{21, 5}, {21, -5}, {5, 21}, {-5, 21}}; 
  const int out_pdgs[nprocs][nexternal - ninitial] = {{6, -6, 21, 21, 5}, {6,
      -6, 21, 21, -5}, {6, -6, 21, 21, 5}, {6, -6, 21, 21, -5}};

  bool in_pdgs_used[ninitial]; 
  bool out_pdgs_used[nexternal - ninitial]; 
  for(unsigned int i = 0; i < nprocs; i++ )
  {
    int permutations[nexternal]; 

    // Reinitialize initial state look-up variables
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      in_pdgs_used[j] = false; 
      permutations[j] = -1; 
    }
    // Look for initial state matches
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      for(unsigned int k = 0; k < ninitial; k++ )
      {
        // Make sure it has not been used already
        if (in_pdgs_used[k])
          continue; 
        if (initial_pdgs[k] == in_pdgs[i][j])
        {
          permutations[j] = k; 
          in_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[ninitial - 1] == -1)
      continue; 

    // Reinitialize final state look-up variables
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      out_pdgs_used[j] = false; 
      permutations[ninitial + j] = -1; 
    }
    // Look for final state matches
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      for(unsigned int k = 0; k < (nexternal - ninitial); k++ )
      {
        // Make sure it has not been used already
        if (out_pdgs_used[k])
          continue; 
        if (final_pdgs[k] == out_pdgs[i][j])
        {
          permutations[ninitial + j] = ninitial + k; 
          out_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[ninitial + j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[nexternal - 1] == -1)
      continue; 

    // Return process found
    return pair < vector<int> , int > (vector<int> (permutations, permutations
        + nexternal), proc_IDS[i]);
  }

  // No process found
  return NA; 
}

//--------------------------------------------------------------------------
// Set momenta
void PY8MEs_R16_P4_sm_gb_ttxggb::setMomenta(vector < vec_double >
    momenta_picked)
{
  if (momenta_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setMomenta' of class" << 
    " 'PY8MEs_R16_P4_sm_gb_ttxggb': Incorrect number of" << 
    " momenta specified." << endl; 
    throw PY8MEs_R16_P4_sm_gb_ttxggb_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    if (momenta_picked[i].size() != 4)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R16_P4_sm_gb_ttxggb': Incorrect number of" << 
      " momenta components specified." << endl; 
      throw PY8MEs_R16_P4_sm_gb_ttxggb_exception; 
    }
    if (momenta_picked[i][0] < 0.0)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R16_P4_sm_gb_ttxggb': A momentum was specified" << 
      " with negative energy. Check conventions." << endl; 
      throw PY8MEs_R16_P4_sm_gb_ttxggb_exception; 
    }
    for (unsigned int j = 0; j < 4; j++ )
    {
      p[i][j] = momenta_picked[i][j]; 
    }
  }
}

//--------------------------------------------------------------------------
// Set color configuration to use. An empty vector means sum over all.
void PY8MEs_R16_P4_sm_gb_ttxggb::setColors(vector<int> colors_picked)
{
  if (colors_picked.size() == 0)
  {
    user_colors = vector<int> (); 
    return; 
  }
  if (colors_picked.size() != (2 * nexternal))
  {
    cerr <<  "Error in function 'setColors' of class" << 
    " 'PY8MEs_R16_P4_sm_gb_ttxggb': Incorrect number" << 
    " of colors specified." << endl; 
    throw PY8MEs_R16_P4_sm_gb_ttxggb_exception; 
  }
  user_colors = vector<int> ((2 * nexternal), 0); 
  for(unsigned int i = 0; i < (2 * nexternal); i++ )
  {
    user_colors[i] = colors_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the helicity configuration to use. Am empty vector means sum over all.
void PY8MEs_R16_P4_sm_gb_ttxggb::setHelicities(vector<int> helicities_picked) 
{
  if (helicities_picked.size() != nexternal)
  {
    if (helicities_picked.size() == 0)
    {
      user_helicities = vector<int> (); 
      return; 
    }
    cerr <<  "Error in function 'setHelicities' of class" << 
    " 'PY8MEs_R16_P4_sm_gb_ttxggb': Incorrect number" << 
    " of helicities specified." << endl; 
    throw PY8MEs_R16_P4_sm_gb_ttxggb_exception; 
  }
  user_helicities = vector<int> (nexternal, 0); 
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    user_helicities[i] = helicities_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the permutation to use (will apply to momenta, colors and helicities)
void PY8MEs_R16_P4_sm_gb_ttxggb::setPermutation(vector<int> perm_picked) 
{
  if (perm_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setPermutations' of class" << 
    " 'PY8MEs_R16_P4_sm_gb_ttxggb': Incorrect number" << 
    " of permutations specified." << endl; 
    throw PY8MEs_R16_P4_sm_gb_ttxggb_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    perm[i] = perm_picked[i]; 
  }
}

// Set the proc_ID to use
void PY8MEs_R16_P4_sm_gb_ttxggb::setProcID(int procID_picked) 
{
  proc_ID = procID_picked; 
}

//--------------------------------------------------------------------------
// Initialize process.

void PY8MEs_R16_P4_sm_gb_ttxggb::initProc() 
{

  // Initialize flags
  include_symmetry_factors = true; 
  include_helicity_averaging_factors = true; 
  include_color_averaging_factors = true; 

  // Initialize vectors.
  perm = vector<int> (nexternal, 0); 
  user_colors = vector<int> (2 * nexternal, 0); 
  user_helicities = vector<int> (nexternal, 0); 
  p = vector < double * > (); 
  for (unsigned int i = 0; i < nexternal; i++ )
  {
    p.push_back(new double[4]); 
  }
  initColorConfigs(); 
  // Synchronize local variables dependent on the model with the active model.
  mME = vector<double> (nexternal, 0.); 
  syncProcModelParams(); 
  jamp2 = vector < vec_double > (2); 
  jamp2[0] = vector<double> (48, 0.); 
  jamp2[1] = vector<double> (48, 0.); 
  all_results = vector < vec_vec_double > (2); 
  // The first entry is always the color or helicity avg/summed matrix element.
  all_results[0] = vector < vec_double > (ncomb + 1, vector<double> (48 + 1,
      0.));
  all_results[1] = vector < vec_double > (ncomb + 1, vector<double> (48 + 1,
      0.));
}

// Synchronize local variables of the process that depend on the model
// parameters
void PY8MEs_R16_P4_sm_gb_ttxggb::syncProcModelParams() 
{

  // Instantiate the model class and set parameters that stay fixed during run
  mME[0] = pars->ZERO; 
  mME[1] = pars->mdl_MB; 
  mME[2] = pars->mdl_MT; 
  mME[3] = pars->mdl_MT; 
  mME[4] = pars->ZERO; 
  mME[5] = pars->ZERO; 
  mME[6] = pars->mdl_MB; 
}

//--------------------------------------------------------------------------
// Setter allowing to force particular values for the external masses
void PY8MEs_R16_P4_sm_gb_ttxggb::setMasses(vec_double external_masses) 
{

  if (external_masses.size() != mME.size())
  {
    cerr <<  "Error in function 'setMasses' of class" << 
    " 'PY8MEs_R16_P4_sm_gb_ttxggb': Incorrect number of" << 
    " masses specified." << endl; 
    throw PY8MEs_R16_P4_sm_gb_ttxggb_exception; 
  }
  for (unsigned int j = 0; j < mME.size(); j++ )
  {
    mME[j] = external_masses[perm[j]]; 
  }
}

//--------------------------------------------------------------------------
// Getter accessing external masses with the correct ordering
vector<double> PY8MEs_R16_P4_sm_gb_ttxggb::getMasses() 
{

  vec_double external_masses; 
  vector<int> invertedPerm; 
  invertedPerm = invert_mapping(perm); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    external_masses.push_back(mME[invertedPerm[i]]); 
  }
  return external_masses; 

}


// Set all values of the external masses to float(-mode) where mode can be
// 0 : Mass taken from the model
// 1 : Mass taken from p_i^2 if not massless to begin with
// 2 : Mass always taken from p_i^2.
void PY8MEs_R16_P4_sm_gb_ttxggb::setExternalMassesMode(int mode) 
{
  if (mode != 0 && mode != 1 && mode != 2)
  {
    cerr <<  "Error in function 'setExternalMassesMode' of class" << 
    " 'PY8MEs_R16_P4_sm_gb_ttxggb': Incorrect mode selected :" << mode << 
    ". It must be either 0, 1 or 2" << endl; 
    throw PY8MEs_R16_P4_sm_gb_ttxggb_exception; 
  }
  if (mode == 0)
  {
    syncProcModelParams(); 
  }
  else if (mode == 1)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      if (mME[j] != pars->ZERO)
      {
        mME[j] = -1.0; 
      }
    }
  }
  else if (mode == 2)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      mME[j] = -1.0; 
    }
  }
}

//--------------------------------------------------------------------------
// Evaluate the squared matrix element.

double PY8MEs_R16_P4_sm_gb_ttxggb::sigmaKin() 
{
  // Set the parameters which change event by event
  pars->setDependentParameters(); 
  pars->setDependentCouplings(); 
  // Reset color flows
  for(int i = 0; i < 48; i++ )
    jamp2[0][i] = 0.; 
  for(int i = 0; i < 48; i++ )
    jamp2[1][i] = 0.; 

  // Save previous values of mME
  vector<double> saved_mME(mME.size(), 0.0); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (mME[i] < 0.0)
    {
      saved_mME[i] = mME[i]; 
      mME[i] = sqrt(abs(pow(p[perm[i]][0], 2) - 
      (pow(p[perm[i]][1], 2) + pow(p[perm[i]][2], 2) + pow(p[perm[i]][3],
          2))));
    }
  }

  // Local variables and constants
  const int max_tries = 10; 
  vector < vec_bool > goodhel(nprocesses, vec_bool(ncomb, false)); 
  vec_int ntry(nprocesses, 0); 
  double t = 0.; 
  double result = 0.; 

  if (ntry[proc_ID] <= max_tries)
    ntry[proc_ID] = ntry[proc_ID] + 1; 

  // Find which helicity configuration is asked for
  // -1 indicates one wants to sum over helicities
  int user_ihel = getHelicityIDForConfig(user_helicities); 

  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = getColorIDForConfig(user_colors); 

  // Reset the list of results that will be recomputed here
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      all_results[proc_ID][ihel + 1][icolor + 1] = 0.; 
    }
  }

  // Calculate the matrix element for all helicities
  // unless already detected as vanishing
  for(int ihel = 0; ihel < ncomb; ihel++ )
  {
    // Skip helicity if already detected as vanishing
    if ((ntry[proc_ID] >= max_tries) && !goodhel[proc_ID][ihel])
      continue; 

    // Also skip helicity if user asks for a specific one
    if ((ntry[proc_ID] >= max_tries) && user_ihel != -1 && user_ihel != ihel)
      continue; 

    calculate_wavefunctions(helicities[ihel]); 

    // Reset locally computed color flows
    for(int i = 0; i < 48; i++ )
      jamp2[0][i] = 0.; 
    for(int i = 0; i < 48; i++ )
      jamp2[1][i] = 0.; 

    if (proc_ID == 0)
      t = matrix_16_gb_ttxggb(); 
    if (proc_ID == 1)
      t = matrix_16_gbx_ttxggbx(); 

    // Store which helicities give non-zero result
    if ((ntry[proc_ID] < max_tries) && t != 0. && !goodhel[proc_ID][ihel])
      goodhel[proc_ID][ihel] = true; 

    // Aggregate results
    if (user_ihel == -1 || user_ihel == ihel)
    {
      if (user_icol == -1)
      {
        result = result + t; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][0] += t; 
          for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
          {
            all_results[proc_ID][0][i + 1] += jamp2[proc_ID][i]; 
          }
        }
        all_results[proc_ID][ihel + 1][0] += t; 
        for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
        {
          all_results[proc_ID][ihel + 1][i + 1] += jamp2[proc_ID][i]; 
        }
      }
      else
      {
        result = result + jamp2[proc_ID][user_icol]; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][user_icol + 1] += jamp2[proc_ID][user_icol]; 
        }
        all_results[proc_ID][ihel + 1][user_icol + 1] +=
            jamp2[proc_ID][user_icol];
      }
    }

  }

  // Normalize results with the identical particle factor
  if (include_symmetry_factors)
  {
    result = result/denom_iden[proc_ID]; 
  }
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      if (include_symmetry_factors)
      {
        all_results[proc_ID][ihel + 1][icolor + 1] /= denom_iden[proc_ID]; 
      }
    }
  }


  // Normalize when when summing+averaging over helicity configurations
  if (user_ihel == -1 && include_helicity_averaging_factors)
  {
    result /= denom_hels[proc_ID]; 
    if (user_icol == -1)
    {
      all_results[proc_ID][0][0] /= denom_hels[proc_ID]; 
      for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
      {
        all_results[proc_ID][0][i + 1] /= denom_hels[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][0][user_icol + 1] /= denom_hels[proc_ID]; 
    }
  }

  // Normalize when summing+averaging over color configurations
  if (user_icol == -1 && include_color_averaging_factors)
  {
    result /= denom_colors[proc_ID]; 
    if (user_ihel == -1)
    {
      all_results[proc_ID][0][0] /= denom_colors[proc_ID]; 
      for (unsigned int i = 0; i < ncomb; i++ )
      {
        all_results[proc_ID][i + 1][0] /= denom_colors[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][user_ihel + 1][0] /= denom_colors[proc_ID]; 
    }
  }

  // Reinstate previous values of mME
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (saved_mME[i] < 0.0)
    {
      mME[i] = saved_mME[i]; 
    }
  }

  // Finally return it
  return result; 
}

//==========================================================================
// Private class member functions

//--------------------------------------------------------------------------
// Evaluate |M|^2 for each subprocess

void PY8MEs_R16_P4_sm_gb_ttxggb::calculate_wavefunctions(const int hel[])
{
  // Calculate wavefunctions for all processes
  // Calculate all wavefunctions
  vxxxxx(p[perm[0]], mME[0], hel[0], -1, w[0]); 
  ixxxxx(p[perm[1]], mME[1], hel[1], +1, w[1]); 
  oxxxxx(p[perm[2]], mME[2], hel[2], +1, w[2]); 
  ixxxxx(p[perm[3]], mME[3], hel[3], -1, w[3]); 
  vxxxxx(p[perm[4]], mME[4], hel[4], +1, w[4]); 
  vxxxxx(p[perm[5]], mME[5], hel[5], +1, w[5]); 
  oxxxxx(p[perm[6]], mME[6], hel[6], +1, w[6]); 
  VVV1P0_1(w[0], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[7]); 
  FFV1P0_3(w[3], w[2], pars->GC_11, pars->ZERO, pars->ZERO, w[8]); 
  VVV1P0_1(w[7], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[9]); 
  FFV1_1(w[6], w[8], pars->GC_11, pars->mdl_MB, pars->ZERO, w[10]); 
  FFV1_2(w[1], w[8], pars->GC_11, pars->mdl_MB, pars->ZERO, w[11]); 
  FFV1_1(w[6], w[7], pars->GC_11, pars->mdl_MB, pars->ZERO, w[12]); 
  VVV1P0_1(w[8], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[13]); 
  FFV1_2(w[1], w[7], pars->GC_11, pars->mdl_MB, pars->ZERO, w[14]); 
  FFV1_1(w[6], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[15]); 
  VVV1P0_1(w[7], w[8], pars->GC_10, pars->ZERO, pars->ZERO, w[16]); 
  FFV1_1(w[15], w[7], pars->GC_11, pars->mdl_MB, pars->ZERO, w[17]); 
  FFV1_2(w[1], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[18]); 
  FFV1_2(w[18], w[7], pars->GC_11, pars->mdl_MB, pars->ZERO, w[19]); 
  FFV1P0_3(w[1], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[20]); 
  VVV1P0_1(w[7], w[20], pars->GC_10, pars->ZERO, pars->ZERO, w[21]); 
  FFV1_1(w[2], w[5], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[22]); 
  FFV1P0_3(w[3], w[22], pars->GC_11, pars->ZERO, pars->ZERO, w[23]); 
  FFV1_1(w[22], w[7], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[24]); 
  FFV1_2(w[3], w[7], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[25]); 
  FFV1_2(w[3], w[5], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[26]); 
  FFV1P0_3(w[26], w[2], pars->GC_11, pars->ZERO, pars->ZERO, w[27]); 
  FFV1_1(w[2], w[7], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[28]); 
  FFV1_2(w[26], w[7], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[29]); 
  FFV1P0_3(w[1], w[15], pars->GC_11, pars->ZERO, pars->ZERO, w[30]); 
  FFV1P0_3(w[18], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[31]); 
  FFV1_2(w[3], w[20], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[32]); 
  VVV1P0_1(w[5], w[20], pars->GC_10, pars->ZERO, pars->ZERO, w[33]); 
  FFV1_1(w[2], w[20], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[34]); 
  FFV1_1(w[2], w[0], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[35]); 
  FFV1_2(w[3], w[4], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[36]); 
  FFV1P0_3(w[36], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[37]); 
  FFV1_1(w[35], w[5], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[38]); 
  FFV1_1(w[35], w[20], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[39]); 
  VVV1P0_1(w[4], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[40]); 
  FFV1P0_3(w[3], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[41]); 
  FFV1_1(w[6], w[40], pars->GC_11, pars->mdl_MB, pars->ZERO, w[42]); 
  FFV1_2(w[1], w[40], pars->GC_11, pars->mdl_MB, pars->ZERO, w[43]); 
  FFV1_1(w[35], w[40], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[44]); 
  FFV1_1(w[6], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[45]); 
  FFV1_1(w[45], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[46]); 
  FFV1P0_3(w[1], w[45], pars->GC_11, pars->ZERO, pars->ZERO, w[47]); 
  FFV1P0_3(w[26], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[48]); 
  FFV1_2(w[1], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[49]); 
  FFV1_2(w[49], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[50]); 
  FFV1P0_3(w[49], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[51]); 
  FFV1_1(w[35], w[4], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[52]); 
  FFV1_1(w[15], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[53]); 
  FFV1_2(w[18], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[54]); 
  VVV1P0_1(w[4], w[20], pars->GC_10, pars->ZERO, pars->ZERO, w[55]); 
  FFV1_2(w[3], w[0], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[56]); 
  FFV1_1(w[2], w[4], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[57]); 
  FFV1P0_3(w[56], w[57], pars->GC_11, pars->ZERO, pars->ZERO, w[58]); 
  FFV1_2(w[56], w[5], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[59]); 
  FFV1_2(w[56], w[20], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[60]); 
  FFV1P0_3(w[56], w[2], pars->GC_11, pars->ZERO, pars->ZERO, w[61]); 
  FFV1_2(w[56], w[40], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[62]); 
  FFV1P0_3(w[56], w[22], pars->GC_11, pars->ZERO, pars->ZERO, w[63]); 
  FFV1_2(w[56], w[4], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[64]); 
  VVV1P0_1(w[0], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[65]); 
  FFV1_1(w[6], w[65], pars->GC_11, pars->mdl_MB, pars->ZERO, w[66]); 
  FFV1P0_3(w[3], w[57], pars->GC_11, pars->ZERO, pars->ZERO, w[67]); 
  FFV1_2(w[1], w[65], pars->GC_11, pars->mdl_MB, pars->ZERO, w[68]); 
  FFV1_1(w[57], w[65], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[69]); 
  FFV1_2(w[3], w[65], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[70]); 
  VVV1P0_1(w[65], w[20], pars->GC_10, pars->ZERO, pars->ZERO, w[71]); 
  FFV1P0_3(w[36], w[2], pars->GC_11, pars->ZERO, pars->ZERO, w[72]); 
  FFV1_2(w[36], w[65], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[73]); 
  FFV1_1(w[2], w[65], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[74]); 
  FFV1_1(w[45], w[65], pars->GC_11, pars->mdl_MB, pars->ZERO, w[75]); 
  VVV1P0_1(w[65], w[8], pars->GC_10, pars->ZERO, pars->ZERO, w[76]); 
  FFV1_2(w[49], w[65], pars->GC_11, pars->mdl_MB, pars->ZERO, w[77]); 
  VVV1P0_1(w[65], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[78]); 
  VVV1P0_1(w[4], w[8], pars->GC_10, pars->ZERO, pars->ZERO, w[79]); 
  FFV1_1(w[6], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[80]); 
  FFV1_1(w[80], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[81]); 
  FFV1P0_3(w[1], w[80], pars->GC_11, pars->ZERO, pars->ZERO, w[82]); 
  FFV1_1(w[57], w[5], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[83]); 
  FFV1P0_3(w[18], w[80], pars->GC_11, pars->ZERO, pars->ZERO, w[84]); 
  FFV1_2(w[36], w[5], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[85]); 
  FFV1_1(w[2], w[40], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[86]); 
  FFV1_2(w[3], w[40], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[87]); 
  FFV1_1(w[80], w[40], pars->GC_11, pars->mdl_MB, pars->ZERO, w[88]); 
  FFV1_1(w[80], w[8], pars->GC_11, pars->mdl_MB, pars->ZERO, w[89]); 
  FFV1P0_3(w[49], w[80], pars->GC_11, pars->ZERO, pars->ZERO, w[90]); 
  FFV1_1(w[80], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[91]); 
  FFV1_1(w[22], w[4], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[92]); 
  FFV1_2(w[26], w[4], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[93]); 
  FFV1_2(w[1], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[94]); 
  FFV1_2(w[94], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[95]); 
  FFV1P0_3(w[94], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[96]); 
  FFV1P0_3(w[94], w[15], pars->GC_11, pars->ZERO, pars->ZERO, w[97]); 
  FFV1_2(w[94], w[40], pars->GC_11, pars->mdl_MB, pars->ZERO, w[98]); 
  FFV1_2(w[94], w[8], pars->GC_11, pars->mdl_MB, pars->ZERO, w[99]); 
  FFV1P0_3(w[94], w[45], pars->GC_11, pars->ZERO, pars->ZERO, w[100]); 
  FFV1_2(w[94], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[101]); 
  FFV1_1(w[57], w[0], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[102]); 
  FFV1_2(w[26], w[0], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[103]); 
  VVV1P0_1(w[0], w[20], pars->GC_10, pars->ZERO, pars->ZERO, w[104]); 
  FFV1_1(w[15], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[105]); 
  FFV1_2(w[18], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[106]); 
  VVVV1P0_1(w[0], w[5], w[20], pars->GC_12, pars->ZERO, pars->ZERO, w[107]); 
  VVVV3P0_1(w[0], w[5], w[20], pars->GC_12, pars->ZERO, pars->ZERO, w[108]); 
  VVVV4P0_1(w[0], w[5], w[20], pars->GC_12, pars->ZERO, pars->ZERO, w[109]); 
  FFV1_2(w[36], w[0], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[110]); 
  FFV1_1(w[22], w[0], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[111]); 
  VVV1P0_1(w[0], w[40], pars->GC_10, pars->ZERO, pars->ZERO, w[112]); 
  VVV1P0_1(w[0], w[8], pars->GC_10, pars->ZERO, pars->ZERO, w[113]); 
  FFV1_1(w[45], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[114]); 
  VVVV1P0_1(w[0], w[8], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[115]); 
  VVVV3P0_1(w[0], w[8], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[116]); 
  VVVV4P0_1(w[0], w[8], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[117]); 
  FFV1_2(w[49], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[118]); 
  VVVV1P0_1(w[0], w[4], w[8], pars->GC_12, pars->ZERO, pars->ZERO, w[119]); 
  VVVV3P0_1(w[0], w[4], w[8], pars->GC_12, pars->ZERO, pars->ZERO, w[120]); 
  VVVV4P0_1(w[0], w[4], w[8], pars->GC_12, pars->ZERO, pars->ZERO, w[121]); 
  VVVV1P0_1(w[0], w[4], w[20], pars->GC_12, pars->ZERO, pars->ZERO, w[122]); 
  VVVV3P0_1(w[0], w[4], w[20], pars->GC_12, pars->ZERO, pars->ZERO, w[123]); 
  VVVV4P0_1(w[0], w[4], w[20], pars->GC_12, pars->ZERO, pars->ZERO, w[124]); 
  VVVV1P0_1(w[0], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[125]); 
  VVVV3P0_1(w[0], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[126]); 
  VVVV4P0_1(w[0], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[127]); 
  FFV1_1(w[6], w[125], pars->GC_11, pars->mdl_MB, pars->ZERO, w[128]); 
  FFV1_1(w[6], w[126], pars->GC_11, pars->mdl_MB, pars->ZERO, w[129]); 
  FFV1_1(w[6], w[127], pars->GC_11, pars->mdl_MB, pars->ZERO, w[130]); 
  FFV1_2(w[1], w[125], pars->GC_11, pars->mdl_MB, pars->ZERO, w[131]); 
  FFV1_2(w[1], w[126], pars->GC_11, pars->mdl_MB, pars->ZERO, w[132]); 
  FFV1_2(w[1], w[127], pars->GC_11, pars->mdl_MB, pars->ZERO, w[133]); 
  FFV1_1(w[2], w[125], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[134]); 
  FFV1_1(w[2], w[126], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[135]); 
  FFV1_1(w[2], w[127], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[136]); 
  FFV1_2(w[3], w[125], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[137]); 
  FFV1_2(w[3], w[126], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[138]); 
  FFV1_2(w[3], w[127], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[139]); 
  oxxxxx(p[perm[1]], mME[1], hel[1], -1, w[140]); 
  ixxxxx(p[perm[6]], mME[6], hel[6], -1, w[141]); 
  FFV1_1(w[140], w[8], pars->GC_11, pars->mdl_MB, pars->ZERO, w[142]); 
  FFV1_2(w[141], w[8], pars->GC_11, pars->mdl_MB, pars->ZERO, w[143]); 
  FFV1_1(w[140], w[7], pars->GC_11, pars->mdl_MB, pars->ZERO, w[144]); 
  FFV1_2(w[141], w[7], pars->GC_11, pars->mdl_MB, pars->ZERO, w[145]); 
  FFV1_1(w[140], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[146]); 
  FFV1_1(w[146], w[7], pars->GC_11, pars->mdl_MB, pars->ZERO, w[147]); 
  FFV1_2(w[141], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[148]); 
  FFV1_2(w[148], w[7], pars->GC_11, pars->mdl_MB, pars->ZERO, w[149]); 
  FFV1P0_3(w[141], w[140], pars->GC_11, pars->ZERO, pars->ZERO, w[150]); 
  VVV1P0_1(w[7], w[150], pars->GC_10, pars->ZERO, pars->ZERO, w[151]); 
  FFV1P0_3(w[141], w[146], pars->GC_11, pars->ZERO, pars->ZERO, w[152]); 
  FFV1P0_3(w[148], w[140], pars->GC_11, pars->ZERO, pars->ZERO, w[153]); 
  FFV1_2(w[3], w[150], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[154]); 
  VVV1P0_1(w[5], w[150], pars->GC_10, pars->ZERO, pars->ZERO, w[155]); 
  FFV1_1(w[2], w[150], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[156]); 
  FFV1_1(w[35], w[150], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[157]); 
  FFV1_1(w[140], w[40], pars->GC_11, pars->mdl_MB, pars->ZERO, w[158]); 
  FFV1_2(w[141], w[40], pars->GC_11, pars->mdl_MB, pars->ZERO, w[159]); 
  FFV1_1(w[140], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[160]); 
  FFV1_1(w[160], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[161]); 
  FFV1P0_3(w[141], w[160], pars->GC_11, pars->ZERO, pars->ZERO, w[162]); 
  FFV1_2(w[141], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[163]); 
  FFV1_2(w[163], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[164]); 
  FFV1P0_3(w[163], w[140], pars->GC_11, pars->ZERO, pars->ZERO, w[165]); 
  FFV1_1(w[146], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[166]); 
  FFV1_2(w[148], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[167]); 
  VVV1P0_1(w[4], w[150], pars->GC_10, pars->ZERO, pars->ZERO, w[168]); 
  FFV1_2(w[56], w[150], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[169]); 
  FFV1_1(w[140], w[65], pars->GC_11, pars->mdl_MB, pars->ZERO, w[170]); 
  FFV1_2(w[141], w[65], pars->GC_11, pars->mdl_MB, pars->ZERO, w[171]); 
  VVV1P0_1(w[65], w[150], pars->GC_10, pars->ZERO, pars->ZERO, w[172]); 
  FFV1_1(w[160], w[65], pars->GC_11, pars->mdl_MB, pars->ZERO, w[173]); 
  FFV1_2(w[163], w[65], pars->GC_11, pars->mdl_MB, pars->ZERO, w[174]); 
  FFV1_1(w[140], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[175]); 
  FFV1_1(w[175], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[176]); 
  FFV1P0_3(w[141], w[175], pars->GC_11, pars->ZERO, pars->ZERO, w[177]); 
  FFV1P0_3(w[148], w[175], pars->GC_11, pars->ZERO, pars->ZERO, w[178]); 
  FFV1_1(w[175], w[40], pars->GC_11, pars->mdl_MB, pars->ZERO, w[179]); 
  FFV1_1(w[175], w[8], pars->GC_11, pars->mdl_MB, pars->ZERO, w[180]); 
  FFV1P0_3(w[163], w[175], pars->GC_11, pars->ZERO, pars->ZERO, w[181]); 
  FFV1_1(w[175], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[182]); 
  FFV1_2(w[141], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[183]); 
  FFV1_2(w[183], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[184]); 
  FFV1P0_3(w[183], w[140], pars->GC_11, pars->ZERO, pars->ZERO, w[185]); 
  FFV1P0_3(w[183], w[146], pars->GC_11, pars->ZERO, pars->ZERO, w[186]); 
  FFV1_2(w[183], w[40], pars->GC_11, pars->mdl_MB, pars->ZERO, w[187]); 
  FFV1_2(w[183], w[8], pars->GC_11, pars->mdl_MB, pars->ZERO, w[188]); 
  FFV1P0_3(w[183], w[160], pars->GC_11, pars->ZERO, pars->ZERO, w[189]); 
  FFV1_2(w[183], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[190]); 
  VVV1P0_1(w[0], w[150], pars->GC_10, pars->ZERO, pars->ZERO, w[191]); 
  FFV1_1(w[146], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[192]); 
  FFV1_2(w[148], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[193]); 
  VVVV1P0_1(w[0], w[5], w[150], pars->GC_12, pars->ZERO, pars->ZERO, w[194]); 
  VVVV3P0_1(w[0], w[5], w[150], pars->GC_12, pars->ZERO, pars->ZERO, w[195]); 
  VVVV4P0_1(w[0], w[5], w[150], pars->GC_12, pars->ZERO, pars->ZERO, w[196]); 
  FFV1_1(w[160], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[197]); 
  FFV1_2(w[163], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[198]); 
  VVVV1P0_1(w[0], w[4], w[150], pars->GC_12, pars->ZERO, pars->ZERO, w[199]); 
  VVVV3P0_1(w[0], w[4], w[150], pars->GC_12, pars->ZERO, pars->ZERO, w[200]); 
  VVVV4P0_1(w[0], w[4], w[150], pars->GC_12, pars->ZERO, pars->ZERO, w[201]); 
  FFV1_1(w[140], w[125], pars->GC_11, pars->mdl_MB, pars->ZERO, w[202]); 
  FFV1_1(w[140], w[126], pars->GC_11, pars->mdl_MB, pars->ZERO, w[203]); 
  FFV1_1(w[140], w[127], pars->GC_11, pars->mdl_MB, pars->ZERO, w[204]); 
  FFV1_2(w[141], w[125], pars->GC_11, pars->mdl_MB, pars->ZERO, w[205]); 
  FFV1_2(w[141], w[126], pars->GC_11, pars->mdl_MB, pars->ZERO, w[206]); 
  FFV1_2(w[141], w[127], pars->GC_11, pars->mdl_MB, pars->ZERO, w[207]); 

  // Calculate all amplitudes
  // Amplitude(s) for diagram number 0
  FFV1_0(w[1], w[10], w[9], pars->GC_11, amp[0]); 
  FFV1_0(w[11], w[6], w[9], pars->GC_11, amp[1]); 
  FFV1_0(w[1], w[12], w[13], pars->GC_11, amp[2]); 
  FFV1_0(w[11], w[12], w[5], pars->GC_11, amp[3]); 
  FFV1_0(w[14], w[6], w[13], pars->GC_11, amp[4]); 
  FFV1_0(w[14], w[10], w[5], pars->GC_11, amp[5]); 
  FFV1_0(w[1], w[15], w[16], pars->GC_11, amp[6]); 
  FFV1_0(w[1], w[17], w[8], pars->GC_11, amp[7]); 
  FFV1_0(w[14], w[15], w[8], pars->GC_11, amp[8]); 
  FFV1_0(w[18], w[6], w[16], pars->GC_11, amp[9]); 
  FFV1_0(w[19], w[6], w[8], pars->GC_11, amp[10]); 
  FFV1_0(w[18], w[12], w[8], pars->GC_11, amp[11]); 
  VVVV1_0(w[7], w[8], w[5], w[20], pars->GC_12, amp[12]); 
  VVVV3_0(w[7], w[8], w[5], w[20], pars->GC_12, amp[13]); 
  VVVV4_0(w[7], w[8], w[5], w[20], pars->GC_12, amp[14]); 
  VVV1_0(w[5], w[20], w[16], pars->GC_10, amp[15]); 
  VVV1_0(w[8], w[20], w[9], pars->GC_10, amp[16]); 
  VVV1_0(w[8], w[5], w[21], pars->GC_10, amp[17]); 
  FFV1_0(w[1], w[12], w[23], pars->GC_11, amp[18]); 
  FFV1_0(w[14], w[6], w[23], pars->GC_11, amp[19]); 
  FFV1_0(w[3], w[24], w[20], pars->GC_11, amp[20]); 
  FFV1_0(w[25], w[22], w[20], pars->GC_11, amp[21]); 
  FFV1_0(w[3], w[22], w[21], pars->GC_11, amp[22]); 
  FFV1_0(w[1], w[12], w[27], pars->GC_11, amp[23]); 
  FFV1_0(w[14], w[6], w[27], pars->GC_11, amp[24]); 
  FFV1_0(w[26], w[28], w[20], pars->GC_11, amp[25]); 
  FFV1_0(w[29], w[2], w[20], pars->GC_11, amp[26]); 
  FFV1_0(w[26], w[2], w[21], pars->GC_11, amp[27]); 
  FFV1_0(w[3], w[28], w[30], pars->GC_11, amp[28]); 
  FFV1_0(w[25], w[2], w[30], pars->GC_11, amp[29]); 
  FFV1_0(w[3], w[28], w[31], pars->GC_11, amp[30]); 
  FFV1_0(w[25], w[2], w[31], pars->GC_11, amp[31]); 
  FFV1_0(w[32], w[28], w[5], pars->GC_11, amp[32]); 
  FFV1_0(w[3], w[28], w[33], pars->GC_11, amp[33]); 
  FFV1_0(w[25], w[34], w[5], pars->GC_11, amp[34]); 
  FFV1_0(w[25], w[2], w[33], pars->GC_11, amp[35]); 
  FFV1_0(w[3], w[34], w[9], pars->GC_11, amp[36]); 
  FFV1_0(w[32], w[2], w[9], pars->GC_11, amp[37]); 
  FFV1_0(w[1], w[15], w[37], pars->GC_11, amp[38]); 
  FFV1_0(w[18], w[6], w[37], pars->GC_11, amp[39]); 
  VVV1_0(w[5], w[20], w[37], pars->GC_10, amp[40]); 
  FFV1_0(w[36], w[38], w[20], pars->GC_11, amp[41]); 
  FFV1_0(w[36], w[39], w[5], pars->GC_11, amp[42]); 
  FFV1_0(w[1], w[42], w[41], pars->GC_11, amp[43]); 
  FFV1_0(w[43], w[6], w[41], pars->GC_11, amp[44]); 
  FFV1_0(w[3], w[44], w[20], pars->GC_11, amp[45]); 
  VVV1_0(w[40], w[20], w[41], pars->GC_10, amp[46]); 
  FFV1_0(w[3], w[39], w[40], pars->GC_11, amp[47]); 
  FFV1_0(w[1], w[46], w[41], pars->GC_11, amp[48]); 
  VVV1_0(w[41], w[47], w[5], pars->GC_10, amp[49]); 
  FFV1_0(w[3], w[38], w[47], pars->GC_11, amp[50]); 
  FFV1_0(w[1], w[45], w[48], pars->GC_11, amp[51]); 
  FFV1_0(w[18], w[45], w[41], pars->GC_11, amp[52]); 
  FFV1_0(w[50], w[6], w[41], pars->GC_11, amp[53]); 
  VVV1_0(w[41], w[51], w[5], pars->GC_10, amp[54]); 
  FFV1_0(w[3], w[38], w[51], pars->GC_11, amp[55]); 
  FFV1_0(w[49], w[6], w[48], pars->GC_11, amp[56]); 
  FFV1_0(w[49], w[15], w[41], pars->GC_11, amp[57]); 
  FFV1_0(w[26], w[52], w[20], pars->GC_11, amp[58]); 
  VVV1_0(w[4], w[20], w[48], pars->GC_10, amp[59]); 
  FFV1_0(w[26], w[39], w[4], pars->GC_11, amp[60]); 
  FFV1_0(w[3], w[52], w[30], pars->GC_11, amp[61]); 
  FFV1_0(w[1], w[53], w[41], pars->GC_11, amp[62]); 
  VVV1_0(w[41], w[4], w[30], pars->GC_10, amp[63]); 
  FFV1_0(w[3], w[52], w[31], pars->GC_11, amp[64]); 
  FFV1_0(w[54], w[6], w[41], pars->GC_11, amp[65]); 
  VVV1_0(w[41], w[4], w[31], pars->GC_10, amp[66]); 
  FFV1_0(w[32], w[52], w[5], pars->GC_11, amp[67]); 
  FFV1_0(w[3], w[52], w[33], pars->GC_11, amp[68]); 
  VVV1_0(w[41], w[55], w[5], pars->GC_10, amp[69]); 
  VVV1_0(w[41], w[4], w[33], pars->GC_10, amp[70]); 
  VVVV1_0(w[4], w[5], w[20], w[41], pars->GC_12, amp[71]); 
  VVVV3_0(w[4], w[5], w[20], w[41], pars->GC_12, amp[72]); 
  VVVV4_0(w[4], w[5], w[20], w[41], pars->GC_12, amp[73]); 
  FFV1_0(w[3], w[38], w[55], pars->GC_11, amp[74]); 
  FFV1_0(w[32], w[38], w[4], pars->GC_11, amp[75]); 
  FFV1_0(w[1], w[15], w[58], pars->GC_11, amp[76]); 
  FFV1_0(w[18], w[6], w[58], pars->GC_11, amp[77]); 
  VVV1_0(w[5], w[20], w[58], pars->GC_10, amp[78]); 
  FFV1_0(w[59], w[57], w[20], pars->GC_11, amp[79]); 
  FFV1_0(w[60], w[57], w[5], pars->GC_11, amp[80]); 
  FFV1_0(w[1], w[42], w[61], pars->GC_11, amp[81]); 
  FFV1_0(w[43], w[6], w[61], pars->GC_11, amp[82]); 
  FFV1_0(w[62], w[2], w[20], pars->GC_11, amp[83]); 
  VVV1_0(w[40], w[20], w[61], pars->GC_10, amp[84]); 
  FFV1_0(w[60], w[2], w[40], pars->GC_11, amp[85]); 
  FFV1_0(w[1], w[46], w[61], pars->GC_11, amp[86]); 
  VVV1_0(w[61], w[47], w[5], pars->GC_10, amp[87]); 
  FFV1_0(w[59], w[2], w[47], pars->GC_11, amp[88]); 
  FFV1_0(w[1], w[45], w[63], pars->GC_11, amp[89]); 
  FFV1_0(w[18], w[45], w[61], pars->GC_11, amp[90]); 
  FFV1_0(w[50], w[6], w[61], pars->GC_11, amp[91]); 
  VVV1_0(w[61], w[51], w[5], pars->GC_10, amp[92]); 
  FFV1_0(w[59], w[2], w[51], pars->GC_11, amp[93]); 
  FFV1_0(w[49], w[6], w[63], pars->GC_11, amp[94]); 
  FFV1_0(w[49], w[15], w[61], pars->GC_11, amp[95]); 
  FFV1_0(w[64], w[22], w[20], pars->GC_11, amp[96]); 
  VVV1_0(w[4], w[20], w[63], pars->GC_10, amp[97]); 
  FFV1_0(w[60], w[22], w[4], pars->GC_11, amp[98]); 
  FFV1_0(w[64], w[2], w[30], pars->GC_11, amp[99]); 
  FFV1_0(w[1], w[53], w[61], pars->GC_11, amp[100]); 
  VVV1_0(w[61], w[4], w[30], pars->GC_10, amp[101]); 
  FFV1_0(w[64], w[2], w[31], pars->GC_11, amp[102]); 
  FFV1_0(w[54], w[6], w[61], pars->GC_11, amp[103]); 
  VVV1_0(w[61], w[4], w[31], pars->GC_10, amp[104]); 
  FFV1_0(w[64], w[34], w[5], pars->GC_11, amp[105]); 
  FFV1_0(w[64], w[2], w[33], pars->GC_11, amp[106]); 
  VVV1_0(w[61], w[55], w[5], pars->GC_10, amp[107]); 
  VVV1_0(w[61], w[4], w[33], pars->GC_10, amp[108]); 
  VVVV1_0(w[4], w[5], w[20], w[61], pars->GC_12, amp[109]); 
  VVVV3_0(w[4], w[5], w[20], w[61], pars->GC_12, amp[110]); 
  VVVV4_0(w[4], w[5], w[20], w[61], pars->GC_12, amp[111]); 
  FFV1_0(w[59], w[2], w[55], pars->GC_11, amp[112]); 
  FFV1_0(w[59], w[34], w[4], pars->GC_11, amp[113]); 
  FFV1_0(w[1], w[66], w[67], pars->GC_11, amp[114]); 
  FFV1_0(w[68], w[6], w[67], pars->GC_11, amp[115]); 
  FFV1_0(w[3], w[69], w[20], pars->GC_11, amp[116]); 
  FFV1_0(w[70], w[57], w[20], pars->GC_11, amp[117]); 
  FFV1_0(w[3], w[57], w[71], pars->GC_11, amp[118]); 
  FFV1_0(w[1], w[66], w[72], pars->GC_11, amp[119]); 
  FFV1_0(w[68], w[6], w[72], pars->GC_11, amp[120]); 
  FFV1_0(w[73], w[2], w[20], pars->GC_11, amp[121]); 
  FFV1_0(w[36], w[74], w[20], pars->GC_11, amp[122]); 
  FFV1_0(w[36], w[2], w[71], pars->GC_11, amp[123]); 
  FFV1_0(w[3], w[74], w[47], pars->GC_11, amp[124]); 
  FFV1_0(w[70], w[2], w[47], pars->GC_11, amp[125]); 
  FFV1_0(w[1], w[75], w[8], pars->GC_11, amp[126]); 
  FFV1_0(w[1], w[45], w[76], pars->GC_11, amp[127]); 
  FFV1_0(w[68], w[45], w[8], pars->GC_11, amp[128]); 
  FFV1_0(w[3], w[74], w[51], pars->GC_11, amp[129]); 
  FFV1_0(w[70], w[2], w[51], pars->GC_11, amp[130]); 
  FFV1_0(w[77], w[6], w[8], pars->GC_11, amp[131]); 
  FFV1_0(w[49], w[6], w[76], pars->GC_11, amp[132]); 
  FFV1_0(w[49], w[66], w[8], pars->GC_11, amp[133]); 
  FFV1_0(w[1], w[10], w[78], pars->GC_11, amp[134]); 
  FFV1_0(w[11], w[6], w[78], pars->GC_11, amp[135]); 
  FFV1_0(w[1], w[66], w[79], pars->GC_11, amp[136]); 
  FFV1_0(w[11], w[66], w[4], pars->GC_11, amp[137]); 
  FFV1_0(w[68], w[6], w[79], pars->GC_11, amp[138]); 
  FFV1_0(w[68], w[10], w[4], pars->GC_11, amp[139]); 
  VVVV1_0(w[65], w[4], w[8], w[20], pars->GC_12, amp[140]); 
  VVVV3_0(w[65], w[4], w[8], w[20], pars->GC_12, amp[141]); 
  VVVV4_0(w[65], w[4], w[8], w[20], pars->GC_12, amp[142]); 
  VVV1_0(w[8], w[20], w[78], pars->GC_10, amp[143]); 
  VVV1_0(w[4], w[20], w[76], pars->GC_10, amp[144]); 
  VVV1_0(w[4], w[8], w[71], pars->GC_10, amp[145]); 
  FFV1_0(w[3], w[34], w[78], pars->GC_11, amp[146]); 
  FFV1_0(w[32], w[2], w[78], pars->GC_11, amp[147]); 
  FFV1_0(w[3], w[74], w[55], pars->GC_11, amp[148]); 
  FFV1_0(w[32], w[74], w[4], pars->GC_11, amp[149]); 
  FFV1_0(w[70], w[2], w[55], pars->GC_11, amp[150]); 
  FFV1_0(w[70], w[34], w[4], pars->GC_11, amp[151]); 
  FFV1_0(w[1], w[81], w[67], pars->GC_11, amp[152]); 
  VVV1_0(w[82], w[67], w[5], pars->GC_10, amp[153]); 
  FFV1_0(w[3], w[83], w[82], pars->GC_11, amp[154]); 
  FFV1_0(w[26], w[57], w[82], pars->GC_11, amp[155]); 
  FFV1_0(w[3], w[57], w[84], pars->GC_11, amp[156]); 
  FFV1_0(w[1], w[81], w[72], pars->GC_11, amp[157]); 
  VVV1_0(w[82], w[72], w[5], pars->GC_10, amp[158]); 
  FFV1_0(w[85], w[2], w[82], pars->GC_11, amp[159]); 
  FFV1_0(w[36], w[22], w[82], pars->GC_11, amp[160]); 
  FFV1_0(w[36], w[2], w[84], pars->GC_11, amp[161]); 
  FFV1_0(w[3], w[86], w[82], pars->GC_11, amp[162]); 
  FFV1_0(w[87], w[2], w[82], pars->GC_11, amp[163]); 
  FFV1_0(w[1], w[88], w[8], pars->GC_11, amp[164]); 
  FFV1_0(w[1], w[89], w[40], pars->GC_11, amp[165]); 
  VVV1_0(w[40], w[8], w[82], pars->GC_10, amp[166]); 
  VVV1_0(w[8], w[5], w[90], pars->GC_10, amp[167]); 
  FFV1_0(w[49], w[89], w[5], pars->GC_11, amp[168]); 
  FFV1_0(w[49], w[81], w[8], pars->GC_11, amp[169]); 
  FFV1_0(w[3], w[22], w[90], pars->GC_11, amp[170]); 
  FFV1_0(w[26], w[2], w[90], pars->GC_11, amp[171]); 
  FFV1_0(w[1], w[91], w[13], pars->GC_11, amp[172]); 
  FFV1_0(w[11], w[91], w[5], pars->GC_11, amp[173]); 
  FFV1_0(w[1], w[81], w[79], pars->GC_11, amp[174]); 
  FFV1_0(w[11], w[81], w[4], pars->GC_11, amp[175]); 
  VVV1_0(w[82], w[79], w[5], pars->GC_10, amp[176]); 
  VVV1_0(w[82], w[4], w[13], pars->GC_10, amp[177]); 
  VVVV1_0(w[4], w[8], w[5], w[82], pars->GC_12, amp[178]); 
  VVVV3_0(w[4], w[8], w[5], w[82], pars->GC_12, amp[179]); 
  VVVV4_0(w[4], w[8], w[5], w[82], pars->GC_12, amp[180]); 
  FFV1_0(w[18], w[91], w[8], pars->GC_11, amp[181]); 
  FFV1_0(w[18], w[89], w[4], pars->GC_11, amp[182]); 
  VVV1_0(w[4], w[8], w[84], pars->GC_10, amp[183]); 
  FFV1_0(w[1], w[91], w[23], pars->GC_11, amp[184]); 
  FFV1_0(w[3], w[92], w[82], pars->GC_11, amp[185]); 
  VVV1_0(w[82], w[4], w[23], pars->GC_10, amp[186]); 
  FFV1_0(w[1], w[91], w[27], pars->GC_11, amp[187]); 
  FFV1_0(w[93], w[2], w[82], pars->GC_11, amp[188]); 
  VVV1_0(w[82], w[4], w[27], pars->GC_10, amp[189]); 
  FFV1_0(w[95], w[6], w[67], pars->GC_11, amp[190]); 
  VVV1_0(w[96], w[67], w[5], pars->GC_10, amp[191]); 
  FFV1_0(w[3], w[83], w[96], pars->GC_11, amp[192]); 
  FFV1_0(w[26], w[57], w[96], pars->GC_11, amp[193]); 
  FFV1_0(w[3], w[57], w[97], pars->GC_11, amp[194]); 
  FFV1_0(w[95], w[6], w[72], pars->GC_11, amp[195]); 
  VVV1_0(w[96], w[72], w[5], pars->GC_10, amp[196]); 
  FFV1_0(w[85], w[2], w[96], pars->GC_11, amp[197]); 
  FFV1_0(w[36], w[22], w[96], pars->GC_11, amp[198]); 
  FFV1_0(w[36], w[2], w[97], pars->GC_11, amp[199]); 
  FFV1_0(w[3], w[86], w[96], pars->GC_11, amp[200]); 
  FFV1_0(w[87], w[2], w[96], pars->GC_11, amp[201]); 
  FFV1_0(w[98], w[6], w[8], pars->GC_11, amp[202]); 
  FFV1_0(w[99], w[6], w[40], pars->GC_11, amp[203]); 
  VVV1_0(w[40], w[8], w[96], pars->GC_10, amp[204]); 
  VVV1_0(w[8], w[5], w[100], pars->GC_10, amp[205]); 
  FFV1_0(w[99], w[45], w[5], pars->GC_11, amp[206]); 
  FFV1_0(w[95], w[45], w[8], pars->GC_11, amp[207]); 
  FFV1_0(w[3], w[22], w[100], pars->GC_11, amp[208]); 
  FFV1_0(w[26], w[2], w[100], pars->GC_11, amp[209]); 
  FFV1_0(w[101], w[6], w[13], pars->GC_11, amp[210]); 
  FFV1_0(w[101], w[10], w[5], pars->GC_11, amp[211]); 
  FFV1_0(w[95], w[6], w[79], pars->GC_11, amp[212]); 
  FFV1_0(w[95], w[10], w[4], pars->GC_11, amp[213]); 
  VVV1_0(w[96], w[79], w[5], pars->GC_10, amp[214]); 
  VVV1_0(w[96], w[4], w[13], pars->GC_10, amp[215]); 
  VVVV1_0(w[4], w[8], w[5], w[96], pars->GC_12, amp[216]); 
  VVVV3_0(w[4], w[8], w[5], w[96], pars->GC_12, amp[217]); 
  VVVV4_0(w[4], w[8], w[5], w[96], pars->GC_12, amp[218]); 
  FFV1_0(w[101], w[15], w[8], pars->GC_11, amp[219]); 
  FFV1_0(w[99], w[15], w[4], pars->GC_11, amp[220]); 
  VVV1_0(w[4], w[8], w[97], pars->GC_10, amp[221]); 
  FFV1_0(w[101], w[6], w[23], pars->GC_11, amp[222]); 
  FFV1_0(w[3], w[92], w[96], pars->GC_11, amp[223]); 
  VVV1_0(w[96], w[4], w[23], pars->GC_10, amp[224]); 
  FFV1_0(w[101], w[6], w[27], pars->GC_11, amp[225]); 
  FFV1_0(w[93], w[2], w[96], pars->GC_11, amp[226]); 
  VVV1_0(w[96], w[4], w[27], pars->GC_10, amp[227]); 
  FFV1_0(w[26], w[102], w[20], pars->GC_11, amp[228]); 
  FFV1_0(w[103], w[57], w[20], pars->GC_11, amp[229]); 
  FFV1_0(w[26], w[57], w[104], pars->GC_11, amp[230]); 
  FFV1_0(w[3], w[102], w[30], pars->GC_11, amp[231]); 
  FFV1_0(w[1], w[105], w[67], pars->GC_11, amp[232]); 
  VVV1_0(w[0], w[67], w[30], pars->GC_10, amp[233]); 
  FFV1_0(w[3], w[102], w[31], pars->GC_11, amp[234]); 
  FFV1_0(w[106], w[6], w[67], pars->GC_11, amp[235]); 
  VVV1_0(w[0], w[67], w[31], pars->GC_10, amp[236]); 
  FFV1_0(w[32], w[102], w[5], pars->GC_11, amp[237]); 
  FFV1_0(w[3], w[102], w[33], pars->GC_11, amp[238]); 
  VVV1_0(w[104], w[67], w[5], pars->GC_10, amp[239]); 
  FFV1_0(w[3], w[83], w[104], pars->GC_11, amp[240]); 
  VVV1_0(w[0], w[67], w[33], pars->GC_10, amp[241]); 
  FFV1_0(w[32], w[83], w[0], pars->GC_11, amp[242]); 
  FFV1_0(w[3], w[57], w[107], pars->GC_11, amp[243]); 
  FFV1_0(w[3], w[57], w[108], pars->GC_11, amp[244]); 
  FFV1_0(w[3], w[57], w[109], pars->GC_11, amp[245]); 
  FFV1_0(w[110], w[22], w[20], pars->GC_11, amp[246]); 
  FFV1_0(w[36], w[111], w[20], pars->GC_11, amp[247]); 
  FFV1_0(w[36], w[22], w[104], pars->GC_11, amp[248]); 
  FFV1_0(w[110], w[2], w[30], pars->GC_11, amp[249]); 
  FFV1_0(w[1], w[105], w[72], pars->GC_11, amp[250]); 
  VVV1_0(w[0], w[72], w[30], pars->GC_10, amp[251]); 
  FFV1_0(w[110], w[2], w[31], pars->GC_11, amp[252]); 
  FFV1_0(w[106], w[6], w[72], pars->GC_11, amp[253]); 
  VVV1_0(w[0], w[72], w[31], pars->GC_10, amp[254]); 
  FFV1_0(w[110], w[34], w[5], pars->GC_11, amp[255]); 
  FFV1_0(w[110], w[2], w[33], pars->GC_11, amp[256]); 
  VVV1_0(w[104], w[72], w[5], pars->GC_10, amp[257]); 
  FFV1_0(w[85], w[2], w[104], pars->GC_11, amp[258]); 
  VVV1_0(w[0], w[72], w[33], pars->GC_10, amp[259]); 
  FFV1_0(w[85], w[34], w[0], pars->GC_11, amp[260]); 
  FFV1_0(w[36], w[2], w[107], pars->GC_11, amp[261]); 
  FFV1_0(w[36], w[2], w[108], pars->GC_11, amp[262]); 
  FFV1_0(w[36], w[2], w[109], pars->GC_11, amp[263]); 
  FFV1_0(w[1], w[10], w[112], pars->GC_11, amp[264]); 
  FFV1_0(w[11], w[6], w[112], pars->GC_11, amp[265]); 
  FFV1_0(w[1], w[42], w[113], pars->GC_11, amp[266]); 
  FFV1_0(w[43], w[6], w[113], pars->GC_11, amp[267]); 
  FFV1_0(w[11], w[42], w[0], pars->GC_11, amp[268]); 
  FFV1_0(w[43], w[10], w[0], pars->GC_11, amp[269]); 
  VVVV1_0(w[0], w[40], w[8], w[20], pars->GC_12, amp[270]); 
  VVVV3_0(w[0], w[40], w[8], w[20], pars->GC_12, amp[271]); 
  VVVV4_0(w[0], w[40], w[8], w[20], pars->GC_12, amp[272]); 
  VVV1_0(w[8], w[20], w[112], pars->GC_10, amp[273]); 
  VVV1_0(w[40], w[20], w[113], pars->GC_10, amp[274]); 
  VVV1_0(w[40], w[8], w[104], pars->GC_10, amp[275]); 
  FFV1_0(w[3], w[34], w[112], pars->GC_11, amp[276]); 
  FFV1_0(w[32], w[2], w[112], pars->GC_11, amp[277]); 
  FFV1_0(w[3], w[86], w[104], pars->GC_11, amp[278]); 
  FFV1_0(w[87], w[2], w[104], pars->GC_11, amp[279]); 
  FFV1_0(w[32], w[86], w[0], pars->GC_11, amp[280]); 
  FFV1_0(w[87], w[34], w[0], pars->GC_11, amp[281]); 
  FFV1_0(w[1], w[114], w[13], pars->GC_11, amp[282]); 
  FFV1_0(w[11], w[114], w[5], pars->GC_11, amp[283]); 
  FFV1_0(w[1], w[46], w[113], pars->GC_11, amp[284]); 
  VVV1_0(w[113], w[47], w[5], pars->GC_10, amp[285]); 
  FFV1_0(w[11], w[46], w[0], pars->GC_11, amp[286]); 
  VVV1_0(w[0], w[47], w[13], pars->GC_10, amp[287]); 
  FFV1_0(w[1], w[45], w[115], pars->GC_11, amp[288]); 
  FFV1_0(w[1], w[45], w[116], pars->GC_11, amp[289]); 
  FFV1_0(w[1], w[45], w[117], pars->GC_11, amp[290]); 
  FFV1_0(w[18], w[114], w[8], pars->GC_11, amp[291]); 
  FFV1_0(w[18], w[45], w[113], pars->GC_11, amp[292]); 
  FFV1_0(w[106], w[45], w[8], pars->GC_11, amp[293]); 
  FFV1_0(w[1], w[114], w[23], pars->GC_11, amp[294]); 
  FFV1_0(w[3], w[111], w[47], pars->GC_11, amp[295]); 
  VVV1_0(w[0], w[47], w[23], pars->GC_10, amp[296]); 
  FFV1_0(w[1], w[114], w[27], pars->GC_11, amp[297]); 
  FFV1_0(w[103], w[2], w[47], pars->GC_11, amp[298]); 
  VVV1_0(w[0], w[47], w[27], pars->GC_10, amp[299]); 
  FFV1_0(w[118], w[6], w[13], pars->GC_11, amp[300]); 
  FFV1_0(w[118], w[10], w[5], pars->GC_11, amp[301]); 
  FFV1_0(w[50], w[6], w[113], pars->GC_11, amp[302]); 
  VVV1_0(w[113], w[51], w[5], pars->GC_10, amp[303]); 
  FFV1_0(w[50], w[10], w[0], pars->GC_11, amp[304]); 
  VVV1_0(w[0], w[51], w[13], pars->GC_10, amp[305]); 
  FFV1_0(w[49], w[6], w[115], pars->GC_11, amp[306]); 
  FFV1_0(w[49], w[6], w[116], pars->GC_11, amp[307]); 
  FFV1_0(w[49], w[6], w[117], pars->GC_11, amp[308]); 
  FFV1_0(w[118], w[15], w[8], pars->GC_11, amp[309]); 
  FFV1_0(w[49], w[15], w[113], pars->GC_11, amp[310]); 
  FFV1_0(w[49], w[105], w[8], pars->GC_11, amp[311]); 
  FFV1_0(w[118], w[6], w[23], pars->GC_11, amp[312]); 
  FFV1_0(w[3], w[111], w[51], pars->GC_11, amp[313]); 
  VVV1_0(w[0], w[51], w[23], pars->GC_10, amp[314]); 
  FFV1_0(w[118], w[6], w[27], pars->GC_11, amp[315]); 
  FFV1_0(w[103], w[2], w[51], pars->GC_11, amp[316]); 
  VVV1_0(w[0], w[51], w[27], pars->GC_10, amp[317]); 
  FFV1_0(w[1], w[53], w[113], pars->GC_11, amp[318]); 
  VVV1_0(w[113], w[4], w[30], pars->GC_10, amp[319]); 
  FFV1_0(w[1], w[105], w[79], pars->GC_11, amp[320]); 
  FFV1_0(w[11], w[105], w[4], pars->GC_11, amp[321]); 
  VVV1_0(w[0], w[79], w[30], pars->GC_10, amp[322]); 
  FFV1_0(w[11], w[53], w[0], pars->GC_11, amp[323]); 
  FFV1_0(w[1], w[15], w[119], pars->GC_11, amp[324]); 
  FFV1_0(w[1], w[15], w[120], pars->GC_11, amp[325]); 
  FFV1_0(w[1], w[15], w[121], pars->GC_11, amp[326]); 
  FFV1_0(w[54], w[6], w[113], pars->GC_11, amp[327]); 
  VVV1_0(w[113], w[4], w[31], pars->GC_10, amp[328]); 
  FFV1_0(w[106], w[6], w[79], pars->GC_11, amp[329]); 
  FFV1_0(w[106], w[10], w[4], pars->GC_11, amp[330]); 
  VVV1_0(w[0], w[79], w[31], pars->GC_10, amp[331]); 
  FFV1_0(w[54], w[10], w[0], pars->GC_11, amp[332]); 
  FFV1_0(w[18], w[6], w[119], pars->GC_11, amp[333]); 
  FFV1_0(w[18], w[6], w[120], pars->GC_11, amp[334]); 
  FFV1_0(w[18], w[6], w[121], pars->GC_11, amp[335]); 
  VVV1_0(w[113], w[55], w[5], pars->GC_10, amp[336]); 
  VVV1_0(w[113], w[4], w[33], pars->GC_10, amp[337]); 
  VVVV1_0(w[4], w[5], w[20], w[113], pars->GC_12, amp[338]); 
  VVVV3_0(w[4], w[5], w[20], w[113], pars->GC_12, amp[339]); 
  VVVV4_0(w[4], w[5], w[20], w[113], pars->GC_12, amp[340]); 
  VVV1_0(w[104], w[79], w[5], pars->GC_10, amp[341]); 
  VVV1_0(w[104], w[4], w[13], pars->GC_10, amp[342]); 
  VVVV1_0(w[4], w[8], w[5], w[104], pars->GC_12, amp[343]); 
  VVVV3_0(w[4], w[8], w[5], w[104], pars->GC_12, amp[344]); 
  VVVV4_0(w[4], w[8], w[5], w[104], pars->GC_12, amp[345]); 
  VVV1_0(w[0], w[79], w[33], pars->GC_10, amp[346]); 
  VVV1_0(w[0], w[55], w[13], pars->GC_10, amp[347]); 
  VVV1_0(w[5], w[20], w[119], pars->GC_10, amp[348]); 
  VVV1_0(w[5], w[20], w[120], pars->GC_10, amp[349]); 
  VVV1_0(w[5], w[20], w[121], pars->GC_10, amp[350]); 
  VVV1_0(w[8], w[5], w[122], pars->GC_10, amp[351]); 
  VVV1_0(w[8], w[5], w[123], pars->GC_10, amp[352]); 
  VVV1_0(w[8], w[5], w[124], pars->GC_10, amp[353]); 
  VVV1_0(w[4], w[20], w[115], pars->GC_10, amp[354]); 
  VVV1_0(w[4], w[20], w[116], pars->GC_10, amp[355]); 
  VVV1_0(w[4], w[20], w[117], pars->GC_10, amp[356]); 
  VVV1_0(w[4], w[8], w[107], pars->GC_10, amp[357]); 
  VVV1_0(w[4], w[8], w[108], pars->GC_10, amp[358]); 
  VVV1_0(w[4], w[8], w[109], pars->GC_10, amp[359]); 
  FFV1_0(w[3], w[111], w[55], pars->GC_11, amp[360]); 
  FFV1_0(w[32], w[111], w[4], pars->GC_11, amp[361]); 
  FFV1_0(w[3], w[92], w[104], pars->GC_11, amp[362]); 
  VVV1_0(w[104], w[4], w[23], pars->GC_10, amp[363]); 
  FFV1_0(w[32], w[92], w[0], pars->GC_11, amp[364]); 
  VVV1_0(w[0], w[55], w[23], pars->GC_10, amp[365]); 
  FFV1_0(w[3], w[22], w[122], pars->GC_11, amp[366]); 
  FFV1_0(w[3], w[22], w[123], pars->GC_11, amp[367]); 
  FFV1_0(w[3], w[22], w[124], pars->GC_11, amp[368]); 
  FFV1_0(w[103], w[2], w[55], pars->GC_11, amp[369]); 
  FFV1_0(w[103], w[34], w[4], pars->GC_11, amp[370]); 
  FFV1_0(w[93], w[2], w[104], pars->GC_11, amp[371]); 
  VVV1_0(w[104], w[4], w[27], pars->GC_10, amp[372]); 
  FFV1_0(w[93], w[34], w[0], pars->GC_11, amp[373]); 
  VVV1_0(w[0], w[55], w[27], pars->GC_10, amp[374]); 
  FFV1_0(w[26], w[2], w[122], pars->GC_11, amp[375]); 
  FFV1_0(w[26], w[2], w[123], pars->GC_11, amp[376]); 
  FFV1_0(w[26], w[2], w[124], pars->GC_11, amp[377]); 
  FFV1_0(w[1], w[128], w[8], pars->GC_11, amp[378]); 
  FFV1_0(w[1], w[129], w[8], pars->GC_11, amp[379]); 
  FFV1_0(w[1], w[130], w[8], pars->GC_11, amp[380]); 
  FFV1_0(w[131], w[6], w[8], pars->GC_11, amp[381]); 
  FFV1_0(w[132], w[6], w[8], pars->GC_11, amp[382]); 
  FFV1_0(w[133], w[6], w[8], pars->GC_11, amp[383]); 
  VVV1_0(w[125], w[8], w[20], pars->GC_10, amp[384]); 
  VVV1_0(w[126], w[8], w[20], pars->GC_10, amp[385]); 
  VVV1_0(w[127], w[8], w[20], pars->GC_10, amp[386]); 
  FFV1_0(w[3], w[134], w[20], pars->GC_11, amp[387]); 
  FFV1_0(w[3], w[135], w[20], pars->GC_11, amp[388]); 
  FFV1_0(w[3], w[136], w[20], pars->GC_11, amp[389]); 
  FFV1_0(w[137], w[2], w[20], pars->GC_11, amp[390]); 
  FFV1_0(w[138], w[2], w[20], pars->GC_11, amp[391]); 
  FFV1_0(w[139], w[2], w[20], pars->GC_11, amp[392]); 
  FFV1_0(w[141], w[142], w[9], pars->GC_11, amp[393]); 
  FFV1_0(w[143], w[140], w[9], pars->GC_11, amp[394]); 
  FFV1_0(w[141], w[144], w[13], pars->GC_11, amp[395]); 
  FFV1_0(w[143], w[144], w[5], pars->GC_11, amp[396]); 
  FFV1_0(w[145], w[140], w[13], pars->GC_11, amp[397]); 
  FFV1_0(w[145], w[142], w[5], pars->GC_11, amp[398]); 
  FFV1_0(w[141], w[146], w[16], pars->GC_11, amp[399]); 
  FFV1_0(w[141], w[147], w[8], pars->GC_11, amp[400]); 
  FFV1_0(w[145], w[146], w[8], pars->GC_11, amp[401]); 
  FFV1_0(w[148], w[140], w[16], pars->GC_11, amp[402]); 
  FFV1_0(w[149], w[140], w[8], pars->GC_11, amp[403]); 
  FFV1_0(w[148], w[144], w[8], pars->GC_11, amp[404]); 
  VVVV1_0(w[7], w[8], w[5], w[150], pars->GC_12, amp[405]); 
  VVVV3_0(w[7], w[8], w[5], w[150], pars->GC_12, amp[406]); 
  VVVV4_0(w[7], w[8], w[5], w[150], pars->GC_12, amp[407]); 
  VVV1_0(w[5], w[150], w[16], pars->GC_10, amp[408]); 
  VVV1_0(w[8], w[150], w[9], pars->GC_10, amp[409]); 
  VVV1_0(w[8], w[5], w[151], pars->GC_10, amp[410]); 
  FFV1_0(w[141], w[144], w[23], pars->GC_11, amp[411]); 
  FFV1_0(w[145], w[140], w[23], pars->GC_11, amp[412]); 
  FFV1_0(w[3], w[24], w[150], pars->GC_11, amp[413]); 
  FFV1_0(w[25], w[22], w[150], pars->GC_11, amp[414]); 
  FFV1_0(w[3], w[22], w[151], pars->GC_11, amp[415]); 
  FFV1_0(w[141], w[144], w[27], pars->GC_11, amp[416]); 
  FFV1_0(w[145], w[140], w[27], pars->GC_11, amp[417]); 
  FFV1_0(w[26], w[28], w[150], pars->GC_11, amp[418]); 
  FFV1_0(w[29], w[2], w[150], pars->GC_11, amp[419]); 
  FFV1_0(w[26], w[2], w[151], pars->GC_11, amp[420]); 
  FFV1_0(w[3], w[28], w[152], pars->GC_11, amp[421]); 
  FFV1_0(w[25], w[2], w[152], pars->GC_11, amp[422]); 
  FFV1_0(w[3], w[28], w[153], pars->GC_11, amp[423]); 
  FFV1_0(w[25], w[2], w[153], pars->GC_11, amp[424]); 
  FFV1_0(w[154], w[28], w[5], pars->GC_11, amp[425]); 
  FFV1_0(w[3], w[28], w[155], pars->GC_11, amp[426]); 
  FFV1_0(w[25], w[156], w[5], pars->GC_11, amp[427]); 
  FFV1_0(w[25], w[2], w[155], pars->GC_11, amp[428]); 
  FFV1_0(w[3], w[156], w[9], pars->GC_11, amp[429]); 
  FFV1_0(w[154], w[2], w[9], pars->GC_11, amp[430]); 
  FFV1_0(w[141], w[146], w[37], pars->GC_11, amp[431]); 
  FFV1_0(w[148], w[140], w[37], pars->GC_11, amp[432]); 
  VVV1_0(w[5], w[150], w[37], pars->GC_10, amp[433]); 
  FFV1_0(w[36], w[38], w[150], pars->GC_11, amp[434]); 
  FFV1_0(w[36], w[157], w[5], pars->GC_11, amp[435]); 
  FFV1_0(w[141], w[158], w[41], pars->GC_11, amp[436]); 
  FFV1_0(w[159], w[140], w[41], pars->GC_11, amp[437]); 
  FFV1_0(w[3], w[44], w[150], pars->GC_11, amp[438]); 
  VVV1_0(w[40], w[150], w[41], pars->GC_10, amp[439]); 
  FFV1_0(w[3], w[157], w[40], pars->GC_11, amp[440]); 
  FFV1_0(w[141], w[161], w[41], pars->GC_11, amp[441]); 
  VVV1_0(w[41], w[162], w[5], pars->GC_10, amp[442]); 
  FFV1_0(w[3], w[38], w[162], pars->GC_11, amp[443]); 
  FFV1_0(w[141], w[160], w[48], pars->GC_11, amp[444]); 
  FFV1_0(w[148], w[160], w[41], pars->GC_11, amp[445]); 
  FFV1_0(w[164], w[140], w[41], pars->GC_11, amp[446]); 
  VVV1_0(w[41], w[165], w[5], pars->GC_10, amp[447]); 
  FFV1_0(w[3], w[38], w[165], pars->GC_11, amp[448]); 
  FFV1_0(w[163], w[140], w[48], pars->GC_11, amp[449]); 
  FFV1_0(w[163], w[146], w[41], pars->GC_11, amp[450]); 
  FFV1_0(w[26], w[52], w[150], pars->GC_11, amp[451]); 
  VVV1_0(w[4], w[150], w[48], pars->GC_10, amp[452]); 
  FFV1_0(w[26], w[157], w[4], pars->GC_11, amp[453]); 
  FFV1_0(w[3], w[52], w[152], pars->GC_11, amp[454]); 
  FFV1_0(w[141], w[166], w[41], pars->GC_11, amp[455]); 
  VVV1_0(w[41], w[4], w[152], pars->GC_10, amp[456]); 
  FFV1_0(w[3], w[52], w[153], pars->GC_11, amp[457]); 
  FFV1_0(w[167], w[140], w[41], pars->GC_11, amp[458]); 
  VVV1_0(w[41], w[4], w[153], pars->GC_10, amp[459]); 
  FFV1_0(w[154], w[52], w[5], pars->GC_11, amp[460]); 
  FFV1_0(w[3], w[52], w[155], pars->GC_11, amp[461]); 
  VVV1_0(w[41], w[168], w[5], pars->GC_10, amp[462]); 
  VVV1_0(w[41], w[4], w[155], pars->GC_10, amp[463]); 
  VVVV1_0(w[4], w[5], w[150], w[41], pars->GC_12, amp[464]); 
  VVVV3_0(w[4], w[5], w[150], w[41], pars->GC_12, amp[465]); 
  VVVV4_0(w[4], w[5], w[150], w[41], pars->GC_12, amp[466]); 
  FFV1_0(w[3], w[38], w[168], pars->GC_11, amp[467]); 
  FFV1_0(w[154], w[38], w[4], pars->GC_11, amp[468]); 
  FFV1_0(w[141], w[146], w[58], pars->GC_11, amp[469]); 
  FFV1_0(w[148], w[140], w[58], pars->GC_11, amp[470]); 
  VVV1_0(w[5], w[150], w[58], pars->GC_10, amp[471]); 
  FFV1_0(w[59], w[57], w[150], pars->GC_11, amp[472]); 
  FFV1_0(w[169], w[57], w[5], pars->GC_11, amp[473]); 
  FFV1_0(w[141], w[158], w[61], pars->GC_11, amp[474]); 
  FFV1_0(w[159], w[140], w[61], pars->GC_11, amp[475]); 
  FFV1_0(w[62], w[2], w[150], pars->GC_11, amp[476]); 
  VVV1_0(w[40], w[150], w[61], pars->GC_10, amp[477]); 
  FFV1_0(w[169], w[2], w[40], pars->GC_11, amp[478]); 
  FFV1_0(w[141], w[161], w[61], pars->GC_11, amp[479]); 
  VVV1_0(w[61], w[162], w[5], pars->GC_10, amp[480]); 
  FFV1_0(w[59], w[2], w[162], pars->GC_11, amp[481]); 
  FFV1_0(w[141], w[160], w[63], pars->GC_11, amp[482]); 
  FFV1_0(w[148], w[160], w[61], pars->GC_11, amp[483]); 
  FFV1_0(w[164], w[140], w[61], pars->GC_11, amp[484]); 
  VVV1_0(w[61], w[165], w[5], pars->GC_10, amp[485]); 
  FFV1_0(w[59], w[2], w[165], pars->GC_11, amp[486]); 
  FFV1_0(w[163], w[140], w[63], pars->GC_11, amp[487]); 
  FFV1_0(w[163], w[146], w[61], pars->GC_11, amp[488]); 
  FFV1_0(w[64], w[22], w[150], pars->GC_11, amp[489]); 
  VVV1_0(w[4], w[150], w[63], pars->GC_10, amp[490]); 
  FFV1_0(w[169], w[22], w[4], pars->GC_11, amp[491]); 
  FFV1_0(w[64], w[2], w[152], pars->GC_11, amp[492]); 
  FFV1_0(w[141], w[166], w[61], pars->GC_11, amp[493]); 
  VVV1_0(w[61], w[4], w[152], pars->GC_10, amp[494]); 
  FFV1_0(w[64], w[2], w[153], pars->GC_11, amp[495]); 
  FFV1_0(w[167], w[140], w[61], pars->GC_11, amp[496]); 
  VVV1_0(w[61], w[4], w[153], pars->GC_10, amp[497]); 
  FFV1_0(w[64], w[156], w[5], pars->GC_11, amp[498]); 
  FFV1_0(w[64], w[2], w[155], pars->GC_11, amp[499]); 
  VVV1_0(w[61], w[168], w[5], pars->GC_10, amp[500]); 
  VVV1_0(w[61], w[4], w[155], pars->GC_10, amp[501]); 
  VVVV1_0(w[4], w[5], w[150], w[61], pars->GC_12, amp[502]); 
  VVVV3_0(w[4], w[5], w[150], w[61], pars->GC_12, amp[503]); 
  VVVV4_0(w[4], w[5], w[150], w[61], pars->GC_12, amp[504]); 
  FFV1_0(w[59], w[2], w[168], pars->GC_11, amp[505]); 
  FFV1_0(w[59], w[156], w[4], pars->GC_11, amp[506]); 
  FFV1_0(w[141], w[170], w[67], pars->GC_11, amp[507]); 
  FFV1_0(w[171], w[140], w[67], pars->GC_11, amp[508]); 
  FFV1_0(w[3], w[69], w[150], pars->GC_11, amp[509]); 
  FFV1_0(w[70], w[57], w[150], pars->GC_11, amp[510]); 
  FFV1_0(w[3], w[57], w[172], pars->GC_11, amp[511]); 
  FFV1_0(w[141], w[170], w[72], pars->GC_11, amp[512]); 
  FFV1_0(w[171], w[140], w[72], pars->GC_11, amp[513]); 
  FFV1_0(w[73], w[2], w[150], pars->GC_11, amp[514]); 
  FFV1_0(w[36], w[74], w[150], pars->GC_11, amp[515]); 
  FFV1_0(w[36], w[2], w[172], pars->GC_11, amp[516]); 
  FFV1_0(w[3], w[74], w[162], pars->GC_11, amp[517]); 
  FFV1_0(w[70], w[2], w[162], pars->GC_11, amp[518]); 
  FFV1_0(w[141], w[173], w[8], pars->GC_11, amp[519]); 
  FFV1_0(w[141], w[160], w[76], pars->GC_11, amp[520]); 
  FFV1_0(w[171], w[160], w[8], pars->GC_11, amp[521]); 
  FFV1_0(w[3], w[74], w[165], pars->GC_11, amp[522]); 
  FFV1_0(w[70], w[2], w[165], pars->GC_11, amp[523]); 
  FFV1_0(w[174], w[140], w[8], pars->GC_11, amp[524]); 
  FFV1_0(w[163], w[140], w[76], pars->GC_11, amp[525]); 
  FFV1_0(w[163], w[170], w[8], pars->GC_11, amp[526]); 
  FFV1_0(w[141], w[142], w[78], pars->GC_11, amp[527]); 
  FFV1_0(w[143], w[140], w[78], pars->GC_11, amp[528]); 
  FFV1_0(w[141], w[170], w[79], pars->GC_11, amp[529]); 
  FFV1_0(w[143], w[170], w[4], pars->GC_11, amp[530]); 
  FFV1_0(w[171], w[140], w[79], pars->GC_11, amp[531]); 
  FFV1_0(w[171], w[142], w[4], pars->GC_11, amp[532]); 
  VVVV1_0(w[65], w[4], w[8], w[150], pars->GC_12, amp[533]); 
  VVVV3_0(w[65], w[4], w[8], w[150], pars->GC_12, amp[534]); 
  VVVV4_0(w[65], w[4], w[8], w[150], pars->GC_12, amp[535]); 
  VVV1_0(w[8], w[150], w[78], pars->GC_10, amp[536]); 
  VVV1_0(w[4], w[150], w[76], pars->GC_10, amp[537]); 
  VVV1_0(w[4], w[8], w[172], pars->GC_10, amp[538]); 
  FFV1_0(w[3], w[156], w[78], pars->GC_11, amp[539]); 
  FFV1_0(w[154], w[2], w[78], pars->GC_11, amp[540]); 
  FFV1_0(w[3], w[74], w[168], pars->GC_11, amp[541]); 
  FFV1_0(w[154], w[74], w[4], pars->GC_11, amp[542]); 
  FFV1_0(w[70], w[2], w[168], pars->GC_11, amp[543]); 
  FFV1_0(w[70], w[156], w[4], pars->GC_11, amp[544]); 
  FFV1_0(w[141], w[176], w[67], pars->GC_11, amp[545]); 
  VVV1_0(w[177], w[67], w[5], pars->GC_10, amp[546]); 
  FFV1_0(w[3], w[83], w[177], pars->GC_11, amp[547]); 
  FFV1_0(w[26], w[57], w[177], pars->GC_11, amp[548]); 
  FFV1_0(w[3], w[57], w[178], pars->GC_11, amp[549]); 
  FFV1_0(w[141], w[176], w[72], pars->GC_11, amp[550]); 
  VVV1_0(w[177], w[72], w[5], pars->GC_10, amp[551]); 
  FFV1_0(w[85], w[2], w[177], pars->GC_11, amp[552]); 
  FFV1_0(w[36], w[22], w[177], pars->GC_11, amp[553]); 
  FFV1_0(w[36], w[2], w[178], pars->GC_11, amp[554]); 
  FFV1_0(w[3], w[86], w[177], pars->GC_11, amp[555]); 
  FFV1_0(w[87], w[2], w[177], pars->GC_11, amp[556]); 
  FFV1_0(w[141], w[179], w[8], pars->GC_11, amp[557]); 
  FFV1_0(w[141], w[180], w[40], pars->GC_11, amp[558]); 
  VVV1_0(w[40], w[8], w[177], pars->GC_10, amp[559]); 
  VVV1_0(w[8], w[5], w[181], pars->GC_10, amp[560]); 
  FFV1_0(w[163], w[180], w[5], pars->GC_11, amp[561]); 
  FFV1_0(w[163], w[176], w[8], pars->GC_11, amp[562]); 
  FFV1_0(w[3], w[22], w[181], pars->GC_11, amp[563]); 
  FFV1_0(w[26], w[2], w[181], pars->GC_11, amp[564]); 
  FFV1_0(w[141], w[182], w[13], pars->GC_11, amp[565]); 
  FFV1_0(w[143], w[182], w[5], pars->GC_11, amp[566]); 
  FFV1_0(w[141], w[176], w[79], pars->GC_11, amp[567]); 
  FFV1_0(w[143], w[176], w[4], pars->GC_11, amp[568]); 
  VVV1_0(w[177], w[79], w[5], pars->GC_10, amp[569]); 
  VVV1_0(w[177], w[4], w[13], pars->GC_10, amp[570]); 
  VVVV1_0(w[4], w[8], w[5], w[177], pars->GC_12, amp[571]); 
  VVVV3_0(w[4], w[8], w[5], w[177], pars->GC_12, amp[572]); 
  VVVV4_0(w[4], w[8], w[5], w[177], pars->GC_12, amp[573]); 
  FFV1_0(w[148], w[182], w[8], pars->GC_11, amp[574]); 
  FFV1_0(w[148], w[180], w[4], pars->GC_11, amp[575]); 
  VVV1_0(w[4], w[8], w[178], pars->GC_10, amp[576]); 
  FFV1_0(w[141], w[182], w[23], pars->GC_11, amp[577]); 
  FFV1_0(w[3], w[92], w[177], pars->GC_11, amp[578]); 
  VVV1_0(w[177], w[4], w[23], pars->GC_10, amp[579]); 
  FFV1_0(w[141], w[182], w[27], pars->GC_11, amp[580]); 
  FFV1_0(w[93], w[2], w[177], pars->GC_11, amp[581]); 
  VVV1_0(w[177], w[4], w[27], pars->GC_10, amp[582]); 
  FFV1_0(w[184], w[140], w[67], pars->GC_11, amp[583]); 
  VVV1_0(w[185], w[67], w[5], pars->GC_10, amp[584]); 
  FFV1_0(w[3], w[83], w[185], pars->GC_11, amp[585]); 
  FFV1_0(w[26], w[57], w[185], pars->GC_11, amp[586]); 
  FFV1_0(w[3], w[57], w[186], pars->GC_11, amp[587]); 
  FFV1_0(w[184], w[140], w[72], pars->GC_11, amp[588]); 
  VVV1_0(w[185], w[72], w[5], pars->GC_10, amp[589]); 
  FFV1_0(w[85], w[2], w[185], pars->GC_11, amp[590]); 
  FFV1_0(w[36], w[22], w[185], pars->GC_11, amp[591]); 
  FFV1_0(w[36], w[2], w[186], pars->GC_11, amp[592]); 
  FFV1_0(w[3], w[86], w[185], pars->GC_11, amp[593]); 
  FFV1_0(w[87], w[2], w[185], pars->GC_11, amp[594]); 
  FFV1_0(w[187], w[140], w[8], pars->GC_11, amp[595]); 
  FFV1_0(w[188], w[140], w[40], pars->GC_11, amp[596]); 
  VVV1_0(w[40], w[8], w[185], pars->GC_10, amp[597]); 
  VVV1_0(w[8], w[5], w[189], pars->GC_10, amp[598]); 
  FFV1_0(w[188], w[160], w[5], pars->GC_11, amp[599]); 
  FFV1_0(w[184], w[160], w[8], pars->GC_11, amp[600]); 
  FFV1_0(w[3], w[22], w[189], pars->GC_11, amp[601]); 
  FFV1_0(w[26], w[2], w[189], pars->GC_11, amp[602]); 
  FFV1_0(w[190], w[140], w[13], pars->GC_11, amp[603]); 
  FFV1_0(w[190], w[142], w[5], pars->GC_11, amp[604]); 
  FFV1_0(w[184], w[140], w[79], pars->GC_11, amp[605]); 
  FFV1_0(w[184], w[142], w[4], pars->GC_11, amp[606]); 
  VVV1_0(w[185], w[79], w[5], pars->GC_10, amp[607]); 
  VVV1_0(w[185], w[4], w[13], pars->GC_10, amp[608]); 
  VVVV1_0(w[4], w[8], w[5], w[185], pars->GC_12, amp[609]); 
  VVVV3_0(w[4], w[8], w[5], w[185], pars->GC_12, amp[610]); 
  VVVV4_0(w[4], w[8], w[5], w[185], pars->GC_12, amp[611]); 
  FFV1_0(w[190], w[146], w[8], pars->GC_11, amp[612]); 
  FFV1_0(w[188], w[146], w[4], pars->GC_11, amp[613]); 
  VVV1_0(w[4], w[8], w[186], pars->GC_10, amp[614]); 
  FFV1_0(w[190], w[140], w[23], pars->GC_11, amp[615]); 
  FFV1_0(w[3], w[92], w[185], pars->GC_11, amp[616]); 
  VVV1_0(w[185], w[4], w[23], pars->GC_10, amp[617]); 
  FFV1_0(w[190], w[140], w[27], pars->GC_11, amp[618]); 
  FFV1_0(w[93], w[2], w[185], pars->GC_11, amp[619]); 
  VVV1_0(w[185], w[4], w[27], pars->GC_10, amp[620]); 
  FFV1_0(w[26], w[102], w[150], pars->GC_11, amp[621]); 
  FFV1_0(w[103], w[57], w[150], pars->GC_11, amp[622]); 
  FFV1_0(w[26], w[57], w[191], pars->GC_11, amp[623]); 
  FFV1_0(w[3], w[102], w[152], pars->GC_11, amp[624]); 
  FFV1_0(w[141], w[192], w[67], pars->GC_11, amp[625]); 
  VVV1_0(w[0], w[67], w[152], pars->GC_10, amp[626]); 
  FFV1_0(w[3], w[102], w[153], pars->GC_11, amp[627]); 
  FFV1_0(w[193], w[140], w[67], pars->GC_11, amp[628]); 
  VVV1_0(w[0], w[67], w[153], pars->GC_10, amp[629]); 
  FFV1_0(w[154], w[102], w[5], pars->GC_11, amp[630]); 
  FFV1_0(w[3], w[102], w[155], pars->GC_11, amp[631]); 
  VVV1_0(w[191], w[67], w[5], pars->GC_10, amp[632]); 
  FFV1_0(w[3], w[83], w[191], pars->GC_11, amp[633]); 
  VVV1_0(w[0], w[67], w[155], pars->GC_10, amp[634]); 
  FFV1_0(w[154], w[83], w[0], pars->GC_11, amp[635]); 
  FFV1_0(w[3], w[57], w[194], pars->GC_11, amp[636]); 
  FFV1_0(w[3], w[57], w[195], pars->GC_11, amp[637]); 
  FFV1_0(w[3], w[57], w[196], pars->GC_11, amp[638]); 
  FFV1_0(w[110], w[22], w[150], pars->GC_11, amp[639]); 
  FFV1_0(w[36], w[111], w[150], pars->GC_11, amp[640]); 
  FFV1_0(w[36], w[22], w[191], pars->GC_11, amp[641]); 
  FFV1_0(w[110], w[2], w[152], pars->GC_11, amp[642]); 
  FFV1_0(w[141], w[192], w[72], pars->GC_11, amp[643]); 
  VVV1_0(w[0], w[72], w[152], pars->GC_10, amp[644]); 
  FFV1_0(w[110], w[2], w[153], pars->GC_11, amp[645]); 
  FFV1_0(w[193], w[140], w[72], pars->GC_11, amp[646]); 
  VVV1_0(w[0], w[72], w[153], pars->GC_10, amp[647]); 
  FFV1_0(w[110], w[156], w[5], pars->GC_11, amp[648]); 
  FFV1_0(w[110], w[2], w[155], pars->GC_11, amp[649]); 
  VVV1_0(w[191], w[72], w[5], pars->GC_10, amp[650]); 
  FFV1_0(w[85], w[2], w[191], pars->GC_11, amp[651]); 
  VVV1_0(w[0], w[72], w[155], pars->GC_10, amp[652]); 
  FFV1_0(w[85], w[156], w[0], pars->GC_11, amp[653]); 
  FFV1_0(w[36], w[2], w[194], pars->GC_11, amp[654]); 
  FFV1_0(w[36], w[2], w[195], pars->GC_11, amp[655]); 
  FFV1_0(w[36], w[2], w[196], pars->GC_11, amp[656]); 
  FFV1_0(w[141], w[142], w[112], pars->GC_11, amp[657]); 
  FFV1_0(w[143], w[140], w[112], pars->GC_11, amp[658]); 
  FFV1_0(w[141], w[158], w[113], pars->GC_11, amp[659]); 
  FFV1_0(w[159], w[140], w[113], pars->GC_11, amp[660]); 
  FFV1_0(w[143], w[158], w[0], pars->GC_11, amp[661]); 
  FFV1_0(w[159], w[142], w[0], pars->GC_11, amp[662]); 
  VVVV1_0(w[0], w[40], w[8], w[150], pars->GC_12, amp[663]); 
  VVVV3_0(w[0], w[40], w[8], w[150], pars->GC_12, amp[664]); 
  VVVV4_0(w[0], w[40], w[8], w[150], pars->GC_12, amp[665]); 
  VVV1_0(w[8], w[150], w[112], pars->GC_10, amp[666]); 
  VVV1_0(w[40], w[150], w[113], pars->GC_10, amp[667]); 
  VVV1_0(w[40], w[8], w[191], pars->GC_10, amp[668]); 
  FFV1_0(w[3], w[156], w[112], pars->GC_11, amp[669]); 
  FFV1_0(w[154], w[2], w[112], pars->GC_11, amp[670]); 
  FFV1_0(w[3], w[86], w[191], pars->GC_11, amp[671]); 
  FFV1_0(w[87], w[2], w[191], pars->GC_11, amp[672]); 
  FFV1_0(w[154], w[86], w[0], pars->GC_11, amp[673]); 
  FFV1_0(w[87], w[156], w[0], pars->GC_11, amp[674]); 
  FFV1_0(w[141], w[197], w[13], pars->GC_11, amp[675]); 
  FFV1_0(w[143], w[197], w[5], pars->GC_11, amp[676]); 
  FFV1_0(w[141], w[161], w[113], pars->GC_11, amp[677]); 
  VVV1_0(w[113], w[162], w[5], pars->GC_10, amp[678]); 
  FFV1_0(w[143], w[161], w[0], pars->GC_11, amp[679]); 
  VVV1_0(w[0], w[162], w[13], pars->GC_10, amp[680]); 
  FFV1_0(w[141], w[160], w[115], pars->GC_11, amp[681]); 
  FFV1_0(w[141], w[160], w[116], pars->GC_11, amp[682]); 
  FFV1_0(w[141], w[160], w[117], pars->GC_11, amp[683]); 
  FFV1_0(w[148], w[197], w[8], pars->GC_11, amp[684]); 
  FFV1_0(w[148], w[160], w[113], pars->GC_11, amp[685]); 
  FFV1_0(w[193], w[160], w[8], pars->GC_11, amp[686]); 
  FFV1_0(w[141], w[197], w[23], pars->GC_11, amp[687]); 
  FFV1_0(w[3], w[111], w[162], pars->GC_11, amp[688]); 
  VVV1_0(w[0], w[162], w[23], pars->GC_10, amp[689]); 
  FFV1_0(w[141], w[197], w[27], pars->GC_11, amp[690]); 
  FFV1_0(w[103], w[2], w[162], pars->GC_11, amp[691]); 
  VVV1_0(w[0], w[162], w[27], pars->GC_10, amp[692]); 
  FFV1_0(w[198], w[140], w[13], pars->GC_11, amp[693]); 
  FFV1_0(w[198], w[142], w[5], pars->GC_11, amp[694]); 
  FFV1_0(w[164], w[140], w[113], pars->GC_11, amp[695]); 
  VVV1_0(w[113], w[165], w[5], pars->GC_10, amp[696]); 
  FFV1_0(w[164], w[142], w[0], pars->GC_11, amp[697]); 
  VVV1_0(w[0], w[165], w[13], pars->GC_10, amp[698]); 
  FFV1_0(w[163], w[140], w[115], pars->GC_11, amp[699]); 
  FFV1_0(w[163], w[140], w[116], pars->GC_11, amp[700]); 
  FFV1_0(w[163], w[140], w[117], pars->GC_11, amp[701]); 
  FFV1_0(w[198], w[146], w[8], pars->GC_11, amp[702]); 
  FFV1_0(w[163], w[146], w[113], pars->GC_11, amp[703]); 
  FFV1_0(w[163], w[192], w[8], pars->GC_11, amp[704]); 
  FFV1_0(w[198], w[140], w[23], pars->GC_11, amp[705]); 
  FFV1_0(w[3], w[111], w[165], pars->GC_11, amp[706]); 
  VVV1_0(w[0], w[165], w[23], pars->GC_10, amp[707]); 
  FFV1_0(w[198], w[140], w[27], pars->GC_11, amp[708]); 
  FFV1_0(w[103], w[2], w[165], pars->GC_11, amp[709]); 
  VVV1_0(w[0], w[165], w[27], pars->GC_10, amp[710]); 
  FFV1_0(w[141], w[166], w[113], pars->GC_11, amp[711]); 
  VVV1_0(w[113], w[4], w[152], pars->GC_10, amp[712]); 
  FFV1_0(w[141], w[192], w[79], pars->GC_11, amp[713]); 
  FFV1_0(w[143], w[192], w[4], pars->GC_11, amp[714]); 
  VVV1_0(w[0], w[79], w[152], pars->GC_10, amp[715]); 
  FFV1_0(w[143], w[166], w[0], pars->GC_11, amp[716]); 
  FFV1_0(w[141], w[146], w[119], pars->GC_11, amp[717]); 
  FFV1_0(w[141], w[146], w[120], pars->GC_11, amp[718]); 
  FFV1_0(w[141], w[146], w[121], pars->GC_11, amp[719]); 
  FFV1_0(w[167], w[140], w[113], pars->GC_11, amp[720]); 
  VVV1_0(w[113], w[4], w[153], pars->GC_10, amp[721]); 
  FFV1_0(w[193], w[140], w[79], pars->GC_11, amp[722]); 
  FFV1_0(w[193], w[142], w[4], pars->GC_11, amp[723]); 
  VVV1_0(w[0], w[79], w[153], pars->GC_10, amp[724]); 
  FFV1_0(w[167], w[142], w[0], pars->GC_11, amp[725]); 
  FFV1_0(w[148], w[140], w[119], pars->GC_11, amp[726]); 
  FFV1_0(w[148], w[140], w[120], pars->GC_11, amp[727]); 
  FFV1_0(w[148], w[140], w[121], pars->GC_11, amp[728]); 
  VVV1_0(w[113], w[168], w[5], pars->GC_10, amp[729]); 
  VVV1_0(w[113], w[4], w[155], pars->GC_10, amp[730]); 
  VVVV1_0(w[4], w[5], w[150], w[113], pars->GC_12, amp[731]); 
  VVVV3_0(w[4], w[5], w[150], w[113], pars->GC_12, amp[732]); 
  VVVV4_0(w[4], w[5], w[150], w[113], pars->GC_12, amp[733]); 
  VVV1_0(w[191], w[79], w[5], pars->GC_10, amp[734]); 
  VVV1_0(w[191], w[4], w[13], pars->GC_10, amp[735]); 
  VVVV1_0(w[4], w[8], w[5], w[191], pars->GC_12, amp[736]); 
  VVVV3_0(w[4], w[8], w[5], w[191], pars->GC_12, amp[737]); 
  VVVV4_0(w[4], w[8], w[5], w[191], pars->GC_12, amp[738]); 
  VVV1_0(w[0], w[79], w[155], pars->GC_10, amp[739]); 
  VVV1_0(w[0], w[168], w[13], pars->GC_10, amp[740]); 
  VVV1_0(w[5], w[150], w[119], pars->GC_10, amp[741]); 
  VVV1_0(w[5], w[150], w[120], pars->GC_10, amp[742]); 
  VVV1_0(w[5], w[150], w[121], pars->GC_10, amp[743]); 
  VVV1_0(w[8], w[5], w[199], pars->GC_10, amp[744]); 
  VVV1_0(w[8], w[5], w[200], pars->GC_10, amp[745]); 
  VVV1_0(w[8], w[5], w[201], pars->GC_10, amp[746]); 
  VVV1_0(w[4], w[150], w[115], pars->GC_10, amp[747]); 
  VVV1_0(w[4], w[150], w[116], pars->GC_10, amp[748]); 
  VVV1_0(w[4], w[150], w[117], pars->GC_10, amp[749]); 
  VVV1_0(w[4], w[8], w[194], pars->GC_10, amp[750]); 
  VVV1_0(w[4], w[8], w[195], pars->GC_10, amp[751]); 
  VVV1_0(w[4], w[8], w[196], pars->GC_10, amp[752]); 
  FFV1_0(w[3], w[111], w[168], pars->GC_11, amp[753]); 
  FFV1_0(w[154], w[111], w[4], pars->GC_11, amp[754]); 
  FFV1_0(w[3], w[92], w[191], pars->GC_11, amp[755]); 
  VVV1_0(w[191], w[4], w[23], pars->GC_10, amp[756]); 
  FFV1_0(w[154], w[92], w[0], pars->GC_11, amp[757]); 
  VVV1_0(w[0], w[168], w[23], pars->GC_10, amp[758]); 
  FFV1_0(w[3], w[22], w[199], pars->GC_11, amp[759]); 
  FFV1_0(w[3], w[22], w[200], pars->GC_11, amp[760]); 
  FFV1_0(w[3], w[22], w[201], pars->GC_11, amp[761]); 
  FFV1_0(w[103], w[2], w[168], pars->GC_11, amp[762]); 
  FFV1_0(w[103], w[156], w[4], pars->GC_11, amp[763]); 
  FFV1_0(w[93], w[2], w[191], pars->GC_11, amp[764]); 
  VVV1_0(w[191], w[4], w[27], pars->GC_10, amp[765]); 
  FFV1_0(w[93], w[156], w[0], pars->GC_11, amp[766]); 
  VVV1_0(w[0], w[168], w[27], pars->GC_10, amp[767]); 
  FFV1_0(w[26], w[2], w[199], pars->GC_11, amp[768]); 
  FFV1_0(w[26], w[2], w[200], pars->GC_11, amp[769]); 
  FFV1_0(w[26], w[2], w[201], pars->GC_11, amp[770]); 
  FFV1_0(w[141], w[202], w[8], pars->GC_11, amp[771]); 
  FFV1_0(w[141], w[203], w[8], pars->GC_11, amp[772]); 
  FFV1_0(w[141], w[204], w[8], pars->GC_11, amp[773]); 
  FFV1_0(w[205], w[140], w[8], pars->GC_11, amp[774]); 
  FFV1_0(w[206], w[140], w[8], pars->GC_11, amp[775]); 
  FFV1_0(w[207], w[140], w[8], pars->GC_11, amp[776]); 
  VVV1_0(w[125], w[8], w[150], pars->GC_10, amp[777]); 
  VVV1_0(w[126], w[8], w[150], pars->GC_10, amp[778]); 
  VVV1_0(w[127], w[8], w[150], pars->GC_10, amp[779]); 
  FFV1_0(w[3], w[134], w[150], pars->GC_11, amp[780]); 
  FFV1_0(w[3], w[135], w[150], pars->GC_11, amp[781]); 
  FFV1_0(w[3], w[136], w[150], pars->GC_11, amp[782]); 
  FFV1_0(w[137], w[2], w[150], pars->GC_11, amp[783]); 
  FFV1_0(w[138], w[2], w[150], pars->GC_11, amp[784]); 
  FFV1_0(w[139], w[2], w[150], pars->GC_11, amp[785]); 


}
double PY8MEs_R16_P4_sm_gb_ttxggb::matrix_16_gb_ttxggb() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 393;
  const int ncolor = 48; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
      9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
      9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9};
  static const double cf[ncolor][ncolor] = {{192, -24, 64, -8, 0, -8, 24, 64,
      1, 0, -8, -3, 0, 64, 24, -8, 10, 0, 1, 24, 64, -8, 21, -6, 0, 0, 0, 0,
      -8, 64, 1, -8, 0, 1, -3, -8, 64, 24, -3, -8, 24, 1, -8, 0, -3, 1, 24,
      10}, {-24, 192, -8, 64, 0, 64, 24, -8, 10, 0, 1, 24, 0, -8, 24, 64, 1, 0,
      -8, -3, -8, 64, -6, 21, 0, 0, 0, 0, 1, -8, -8, 64, 0, -8, -3, 1, -8, -3,
      24, 64, 24, 10, 1, 0, -3, -8, 24, 1}, {64, -8, 192, -24, -8, 0, 64, 24,
      0, 1, -3, -8, 64, 0, -8, 24, 0, 10, 24, 1, 21, -6, 64, -8, -8, 64, 1, -8,
      0, 0, 0, 0, 1, 0, -8, -3, 24, 64, -8, -3, 1, 24, 0, -8, 1, -3, 10, 24},
      {-8, 64, -24, 192, 64, 0, -8, 24, 0, 10, 24, 1, -8, 0, 64, 24, 0, 1, -3,
      -8, -6, 21, -8, 64, 1, -8, -8, 64, 0, 0, 0, 0, -8, 0, 1, -3, -3, -8, 64,
      24, 10, 24, 0, 1, -8, -3, 1, 24}, {0, 0, -8, 64, 192, 64, 0, 64, 1, 24,
      64, 21, 24, 64, 0, -8, -8, -3, 1, 0, -8, 1, -3, 24, 24, -3, 24, 24, 10,
      1, 64, -8, -24, -8, 0, -8, -8, -6, 0, -8, 0, 1, -8, -3, 0, 1, 0, 10},
      {-8, 64, 0, 0, 64, 192, 64, 0, 24, 1, 21, 64, 64, 24, -8, 0, -3, -8, 0,
      1, -3, 24, -8, 1, 10, 1, 64, -8, 24, -3, 24, 24, -8, -24, -8, 0, -6, -8,
      -8, 0, 1, 0, -3, -8, 1, 0, 10, 0}, {24, 24, 64, -8, 0, 64, 192, 64, 64,
      21, 1, 24, 0, -8, -24, -8, -8, -6, -8, -3, 1, 10, 0, 0, -6, 21, 21, -6,
      -8, 1, 1, 10, 0, -8, -24, -8, 10, 24, -3, 1, 3, 1, 1, 0, 3, 1, 30, 10},
      {64, -8, 24, 24, 64, 0, 64, 192, 21, 64, 24, 1, -8, 0, -8, -24, -6, -8,
      -3, -8, 0, 0, 1, 10, -8, 1, 1, 10, -6, 21, 21, -6, -8, 0, -8, -24, 24,
      10, 1, -3, 1, 3, 0, 1, 1, 3, 10, 30}, {1, 10, 0, 0, 1, 24, 64, 21, 192,
      64, 0, 64, -8, -3, -8, -6, -24, -8, 0, -8, 24, 24, 64, -8, -8, 1, 1, 10,
      -24, 3, 3, 30, 10, 24, -8, -6, 0, -8, 1, 0, 1, 21, -3, 1, 1, 21, 10, -6},
      {0, 0, 1, 10, 24, 1, 21, 64, 64, 192, 64, 0, -3, -8, -6, -8, -8, -24, -8,
      0, 64, -8, 24, 24, -24, 3, 3, 30, -8, 1, 1, 10, 24, 10, -6, -8, -8, 0, 0,
      1, 21, 1, 1, -3, 21, 1, -6, 10}, {-8, 1, -3, 24, 64, 21, 1, 24, 0, 64,
      192, 64, 1, 0, -8, -3, 0, -8, 24, 64, 0, 0, -8, 64, -8, 1, 1, 10, 0, 0,
      0, 0, -8, -6, 10, 24, -24, -8, -8, -3, 1, -3, 0, -8, 64, 24, -8, 24},
      {-3, 24, -8, 1, 21, 64, 24, 1, 64, 0, 64, 192, 0, 1, -3, -8, -8, 0, 64,
      24, -8, 64, 0, 0, 0, 0, 0, 0, -8, 1, 1, 10, -6, -8, 24, 10, -8, -24, -3,
      -8, -3, 1, -8, 0, 24, 64, 24, -8}, {0, 0, 64, -8, 24, 64, 0, -8, -8, -3,
      1, 0, 192, 64, 0, 64, 1, 24, 64, 21, 1, -8, 24, -3, 24, 24, 24, -3, 64,
      -8, 10, 1, -3, -8, 0, 1, -8, 0, -6, -8, 0, 10, -8, -24, 0, -8, 0, 1},
      {64, -8, 0, 0, 64, 24, -8, 0, -3, -8, 0, 1, 64, 192, 64, 0, 24, 1, 21,
      64, 24, -3, 1, -8, 64, -8, 10, 1, 24, 24, 24, -3, -8, -3, 1, 0, 0, -8,
      -8, -6, 10, 0, -24, -8, -8, 0, 1, 0}, {24, 24, -8, 64, 0, -8, -24, -8,
      -8, -6, -8, -3, 0, 64, 192, 64, 64, 21, 1, 24, 10, 1, 0, 0, 21, -6, -6,
      21, 1, 10, -8, 1, 0, 1, 3, 1, 1, -3, 24, 10, 30, 10, -8, 0, -24, -8, 3,
      1}, {-8, 64, 24, 24, -8, 0, -8, -24, -6, -8, -3, -8, 64, 0, 64, 192, 21,
      64, 24, 1, 0, 0, 10, 1, 1, 10, -8, 1, 21, -6, -6, 21, 1, 0, 1, 3, -3, 1,
      10, 24, 10, 30, 0, -8, -8, -24, 1, 3}, {10, 1, 0, 0, -8, -3, -8, -6, -24,
      -8, 0, -8, 1, 24, 64, 21, 192, 64, 0, 64, 24, 24, -8, 64, 1, 10, -8, 1,
      3, 30, -24, 3, 1, -3, 1, 21, 0, 1, -8, 0, 10, -6, 24, 10, -8, -6, 1, 21},
      {0, 0, 10, 1, -3, -8, -6, -8, -8, -24, -8, 0, 24, 1, 21, 64, 64, 192, 64,
      0, -8, 64, 24, 24, 3, 30, -24, 3, 1, 10, -8, 1, -3, 1, 21, 1, 1, 0, 0,
      -8, -6, 10, 10, 24, -6, -8, 21, 1}, {1, -8, 24, -3, 1, 0, -8, -3, 0, -8,
      24, 64, 64, 21, 1, 24, 0, 64, 192, 64, 0, 0, 64, -8, 1, 10, -8, 1, 0, 0,
      0, 0, -8, 0, 64, 24, -3, -8, -8, -24, -8, 24, -6, -8, 10, 24, 1, -3},
      {24, -3, 1, -8, 0, 1, -3, -8, -8, 0, 64, 24, 21, 64, 24, 1, 64, 0, 64,
      192, 64, -8, 0, 0, 0, 0, 0, 0, 1, 10, -8, 1, 0, -8, 24, 64, -8, -3, -24,
      -8, 24, -8, -8, -6, 24, 10, -3, 1}, {64, -8, 21, -6, -8, -3, 1, 0, 24,
      64, 0, -8, 1, 24, 10, 0, 24, -8, 0, 64, 192, -24, 64, -8, -8, 1, 1, 10,
      -3, 24, -3, 24, 64, 24, -8, 0, 0, 1, -8, 0, 64, 0, -3, -8, 1, 0, -8, 0},
      {-8, 64, -6, 21, 1, 24, 10, 0, 24, -8, 0, 64, -8, -3, 1, 0, 24, 64, 0,
      -8, -24, 192, -8, 64, 1, 10, -8, 1, -3, 24, -3, 24, -8, -3, 1, 0, 0, -8,
      1, 0, -8, 0, 24, 64, -8, 0, 64, 0}, {21, -6, 64, -8, -3, -8, 0, 1, 64,
      24, -8, 0, 24, 1, 0, 10, -8, 24, 64, 0, 64, -8, 192, -24, -3, 24, -3, 24,
      -8, 1, 1, 10, 24, 64, 0, -8, 1, 0, 0, -8, 0, 64, -8, -3, 0, 1, 0, -8},
      {-6, 21, -8, 64, 24, 1, 0, 10, -8, 24, 64, 0, -3, -8, 0, 1, 64, 24, -8,
      0, -8, 64, -24, 192, -3, 24, -3, 24, 1, 10, -8, 1, -3, -8, 0, 1, -8, 0,
      0, 1, 0, -8, 64, 24, 0, -8, 0, 64}, {0, 0, -8, 1, 24, 10, -6, -8, -8,
      -24, -8, 0, 24, 64, 21, 1, 1, 3, 1, 0, -8, 1, -3, -3, 192, -24, 30, 3,
      64, -8, 10, 1, 24, 1, 21, 64, 64, 0, 0, 10, -6, -8, -8, 24, -6, 10, 21,
      1}, {0, 0, 64, -8, -3, 1, 21, 1, 1, 3, 1, 0, 24, -8, -6, 10, 10, 30, 10,
      0, 1, 10, 24, 24, -24, 192, 3, -24, -8, 64, 1, -8, -3, -8, -6, -8, -8, 0,
      0, 1, 21, 64, 64, 24, 21, 1, -6, -8}, {0, 0, 1, -8, 24, 64, 21, 1, 1, 3,
      1, 0, 24, 10, -6, -8, -8, -24, -8, 0, 1, -8, -3, -3, 30, 3, 192, -24, 10,
      1, 64, -8, 24, -8, -6, 10, 10, 0, 0, 64, 21, 1, 1, 24, 21, 64, -6, -8},
      {0, 0, -8, 64, 24, -8, -6, 10, 10, 30, 10, 0, -3, 1, 21, 1, 1, 3, 1, 0,
      10, 1, 24, 24, 3, -24, -24, 192, 1, -8, -8, 64, 24, 64, 21, 1, 1, 0, 0,
      -8, -6, -8, -8, -3, -6, -8, 21, 64}, {-8, 1, 0, 0, 10, 24, -8, -6, -24,
      -8, 0, -8, 64, 24, 1, 21, 3, 1, 0, 1, -3, -3, -8, 1, 64, -8, 10, 1, 192,
      -24, 30, 3, 1, 24, 64, 21, 0, 64, 10, 0, -8, -6, 24, -8, 10, -6, 1, 21},
      {64, -8, 0, 0, 1, -3, 1, 21, 3, 1, 0, 1, -8, 24, 10, -6, 30, 10, 0, 10,
      24, 24, 1, 10, -8, 64, 1, -8, -24, 192, 3, -24, -8, -3, -8, -6, 0, -8, 1,
      0, 64, 21, 24, 64, 1, 21, -8, -6}, {1, -8, 0, 0, 64, 24, 1, 21, 3, 1, 0,
      1, 10, 24, -8, -6, -24, -8, 0, -8, -3, -3, 1, -8, 10, 1, 64, -8, 30, 3,
      192, -24, -8, 24, 10, -6, 0, 10, 64, 0, 1, 21, 24, 1, 64, 21, -8, -6},
      {-8, 64, 0, 0, -8, 24, 10, -6, 30, 10, 0, 10, 1, -3, 1, 21, 3, 1, 0, 1,
      24, 24, 10, 1, 1, -8, -8, 64, 3, -24, -24, 192, 64, 24, 1, 21, 0, 1, -8,
      0, -8, -6, -3, -8, -8, -6, 64, 21}, {0, 0, 1, -8, -24, -8, 0, -8, 10, 24,
      -8, -6, -3, -8, 0, 1, 1, -3, -8, 0, 64, -8, 24, -3, 24, -3, 24, 24, 1,
      -8, -8, 64, 192, 64, 0, 64, 64, 21, 0, 64, 0, -8, 1, 24, 0, 10, 0, 1},
      {1, -8, 0, 0, -8, -24, -8, 0, 24, 10, -6, -8, -8, -3, 1, 0, -3, 1, 0, -8,
      24, -3, 64, -8, 1, -8, -8, 64, 24, -3, 24, 24, 64, 192, 64, 0, 21, 64,
      64, 0, -8, 0, 24, 1, 10, 0, 1, 0}, {-3, -3, -8, 1, 0, -8, -24, -8, -8,
      -6, 10, 24, 0, 1, 3, 1, 1, 21, 64, 24, -8, 1, 0, 0, 21, -6, -6, 21, 64,
      -8, 10, 1, 0, 64, 192, 64, 1, 24, 24, -8, -24, -8, 10, 0, 30, 10, 3, 1},
      {-8, 1, -3, -3, -8, 0, -8, -24, -6, -8, 24, 10, 1, 0, 1, 3, 21, 1, 24,
      64, 0, 0, -8, 1, 64, -8, 10, 1, 21, -6, -6, 21, 64, 0, 64, 192, 24, 1,
      -8, 24, -8, -24, 0, 10, 10, 30, 1, 3}, {64, -8, 24, -3, -8, -6, 10, 24,
      0, -8, -24, -8, -8, 0, 1, -3, 0, 1, -3, -8, 0, 0, 1, -8, 64, -8, 10, 1,
      0, 0, 0, 0, 64, 21, 1, 24, 192, 64, 1, 24, -8, -3, 0, 64, -8, 24, 64,
      24}, {24, -3, 64, -8, -6, -8, 24, 10, -8, 0, -8, -24, 0, -8, -3, 1, 1, 0,
      -8, -3, 1, -8, 0, 0, 0, 0, 0, 0, 64, -8, 10, 1, 21, 64, 24, 1, 64, 192,
      24, 1, -3, -8, 64, 0, 24, -8, 24, 64}, {-3, 24, -8, 64, 0, -8, -3, 1, 1,
      0, -8, -3, -6, -8, 24, 10, -8, 0, -8, -24, -8, 1, 0, 0, 0, 0, 0, 0, 10,
      1, 64, -8, 0, 64, 24, -8, 1, 24, 192, 64, 24, 64, 64, 21, 24, 1, -3, -8},
      {-8, 64, -3, 24, -8, 0, 1, -3, 0, 1, -3, -8, -8, -6, 10, 24, 0, -8, -24,
      -8, 0, 0, -8, 1, 10, 1, 64, -8, 0, 0, 0, 0, 64, 0, -8, 24, 24, 1, 64,
      192, 64, 24, 21, 64, 1, 24, -8, -3}, {24, 24, 1, 10, 0, 1, 3, 1, 1, 21,
      1, -3, 0, 10, 30, 10, 10, -6, -8, 24, 64, -8, 0, 0, -6, 21, 21, -6, -8,
      64, 1, -8, 0, -8, -24, -8, -8, -3, 24, 64, 192, 64, 1, 0, 3, 1, -24, -8},
      {1, 10, 24, 24, 1, 0, 1, 3, 21, 1, -3, 1, 10, 0, 10, 30, -6, 10, 24, -8,
      0, 0, 64, -8, -8, 64, 1, -8, -6, 21, 21, -6, -8, 0, -8, -24, -3, -8, 64,
      24, 64, 192, 0, 1, 1, 3, -8, -24}, {-8, 1, 0, 0, -8, -3, 1, 0, -3, 1, 0,
      -8, -8, -24, -8, 0, 24, 10, -6, -8, -3, 24, -8, 64, -8, 64, 1, -8, 24,
      24, 24, -3, 1, 24, 10, 0, 0, 64, 64, 21, 1, 0, 192, 64, 64, 0, -8, 0},
      {0, 0, -8, 1, -3, -8, 0, 1, 1, -3, -8, 0, -24, -8, 0, -8, 10, 24, -8, -6,
      -8, 64, -3, 24, 24, 24, 24, -3, -8, 64, 1, -8, 24, 1, 0, 10, 64, 0, 21,
      64, 0, 1, 64, 192, 0, 64, 0, -8}, {-3, -3, 1, -8, 0, 1, 3, 1, 1, 21, 64,
      24, 0, -8, -24, -8, -8, -6, 10, 24, 1, -8, 0, 0, -6, 21, 21, -6, 10, 1,
      64, -8, 0, 10, 30, 10, -8, 24, 24, 1, 3, 1, 64, 0, 192, 64, -24, -8}, {1,
      -8, -3, -3, 1, 0, 1, 3, 21, 1, 24, 64, -8, 0, -8, -24, -6, -8, 24, 10, 0,
      0, 1, -8, 10, 1, 64, -8, -6, 21, 21, -6, 10, 0, 10, 30, 24, -8, 1, 24, 1,
      3, 0, 64, 64, 192, -8, -24}, {24, 24, 10, 1, 0, 10, 30, 10, 10, -6, -8,
      24, 0, 1, 3, 1, 1, 21, 1, -3, -8, 64, 0, 0, 21, -6, -6, 21, 1, -8, -8,
      64, 0, 1, 3, 1, 64, 24, -3, -8, -24, -8, -8, 0, -24, -8, 192, 64}, {10,
      1, 24, 24, 10, 0, 10, 30, -6, 10, 24, -8, 1, 0, 1, 3, 21, 1, -3, 1, 0, 0,
      -8, 64, 1, -8, -8, 64, 21, -6, -6, 21, 1, 0, 1, 3, 24, 64, -8, -3, -8,
      -24, 0, -8, -8, -24, 64, 192}};

  // Calculate color flows
  jamp[0] = +1./2. * (+Complex<double> (0, 1) * amp[43] - amp[46] +
      Complex<double> (0, 1) * amp[47] - amp[48] + Complex<double> (0, 1) *
      amp[49] - amp[51] - Complex<double> (0, 1) * amp[59] - amp[60] - amp[69]
      - amp[72] - amp[71] + Complex<double> (0, 1) * amp[201] + Complex<double>
      (0, 1) * amp[203] + amp[204] - Complex<double> (0, 1) * amp[205] -
      amp[206] - amp[209] - amp[215] + amp[218] + amp[217] - amp[226] +
      Complex<double> (0, 1) * amp[227] - amp[266] - Complex<double> (0, 1) *
      amp[272] - Complex<double> (0, 1) * amp[271] - Complex<double> (0, 1) *
      amp[274] - Complex<double> (0, 1) * amp[275] + amp[279] - Complex<double>
      (0, 1) * amp[284] - amp[285] - amp[287] - amp[290] + amp[288] +
      Complex<double> (0, 1) * amp[299] - Complex<double> (0, 1) * amp[336] -
      Complex<double> (0, 1) * amp[339] - Complex<double> (0, 1) * amp[338] +
      Complex<double> (0, 1) * amp[342] - Complex<double> (0, 1) * amp[345] -
      Complex<double> (0, 1) * amp[344] - Complex<double> (0, 1) * amp[347] -
      Complex<double> (0, 1) * amp[351] - Complex<double> (0, 1) * amp[352] -
      Complex<double> (0, 1) * amp[356] + Complex<double> (0, 1) * amp[354] +
      Complex<double> (0, 1) * amp[371] + amp[372] - amp[374] - amp[375] -
      amp[376]);
  jamp[1] = +1./2. * (-amp[38] - Complex<double> (0, 1) * amp[40] - amp[42] -
      Complex<double> (0, 1) * amp[43] + amp[46] - Complex<double> (0, 1) *
      amp[47] - amp[62] - Complex<double> (0, 1) * amp[63] + amp[70] - amp[73]
      + amp[71] - Complex<double> (0, 1) * amp[196] - amp[197] - amp[199] -
      Complex<double> (0, 1) * amp[201] - Complex<double> (0, 1) * amp[203] -
      amp[204] - amp[214] - amp[217] - amp[216] - amp[220] + Complex<double>
      (0, 1) * amp[221] - Complex<double> (0, 1) * amp[251] - amp[257] +
      Complex<double> (0, 1) * amp[258] + amp[259] - amp[261] - amp[262] +
      amp[266] + Complex<double> (0, 1) * amp[272] + Complex<double> (0, 1) *
      amp[271] + Complex<double> (0, 1) * amp[274] + Complex<double> (0, 1) *
      amp[275] - amp[279] - Complex<double> (0, 1) * amp[318] + amp[319] -
      amp[322] - amp[324] - amp[325] + Complex<double> (0, 1) * amp[337] -
      Complex<double> (0, 1) * amp[340] + Complex<double> (0, 1) * amp[338] +
      Complex<double> (0, 1) * amp[341] + Complex<double> (0, 1) * amp[344] +
      Complex<double> (0, 1) * amp[343] - Complex<double> (0, 1) * amp[346] -
      Complex<double> (0, 1) * amp[348] - Complex<double> (0, 1) * amp[349] +
      Complex<double> (0, 1) * amp[357] + Complex<double> (0, 1) * amp[358]);
  jamp[2] = +1./2. * (-1./3. * Complex<double> (0, 1) * amp[43] - 1./3. *
      Complex<double> (0, 1) * amp[44] + 1./3. * amp[48] + 1./3. * amp[52] +
      1./3. * amp[65] - 1./3. * Complex<double> (0, 1) * amp[81] - 1./3. *
      Complex<double> (0, 1) * amp[82] + 1./3. * amp[86] + 1./3. * amp[90] +
      1./3. * amp[103]);
  jamp[3] = +1./2. * (+1./3. * Complex<double> (0, 1) * amp[43] + 1./3. *
      Complex<double> (0, 1) * amp[44] + 1./3. * amp[53] + 1./3. * amp[57] +
      1./3. * amp[62] + 1./3. * Complex<double> (0, 1) * amp[81] + 1./3. *
      Complex<double> (0, 1) * amp[82] + 1./3. * amp[91] + 1./3. * amp[95] +
      1./3. * amp[100]);
  jamp[4] = +1./2. * (-amp[4] - amp[6] + Complex<double> (0, 1) * amp[8] +
      Complex<double> (0, 1) * amp[14] - Complex<double> (0, 1) * amp[12] -
      Complex<double> (0, 1) * amp[15] + Complex<double> (0, 1) * amp[17] +
      Complex<double> (0, 1) * amp[24] + Complex<double> (0, 1) * amp[25] +
      amp[27] + Complex<double> (0, 1) * amp[28] - amp[33] + Complex<double>
      (0, 1) * amp[54] - amp[56] - amp[57] - amp[58] + Complex<double> (0, 1) *
      amp[59] - amp[61] + Complex<double> (0, 1) * amp[63] - Complex<double>
      (0, 1) * amp[68] + amp[69] - amp[70] + amp[73] + amp[72] -
      Complex<double> (0, 1) * amp[300] - amp[303] - amp[305] - amp[308] +
      amp[306] - amp[309] - Complex<double> (0, 1) * amp[310] - amp[315] +
      Complex<double> (0, 1) * amp[317] - amp[319] + amp[326] + amp[325] +
      Complex<double> (0, 1) * amp[336] - Complex<double> (0, 1) * amp[337] +
      Complex<double> (0, 1) * amp[340] + Complex<double> (0, 1) * amp[339] +
      Complex<double> (0, 1) * amp[347] + Complex<double> (0, 1) * amp[350] +
      Complex<double> (0, 1) * amp[349] - Complex<double> (0, 1) * amp[353] +
      Complex<double> (0, 1) * amp[351] + Complex<double> (0, 1) * amp[356] -
      Complex<double> (0, 1) * amp[354] + amp[374] - amp[377] + amp[375]);
  jamp[5] = +1./2. * (-1./3. * Complex<double> (0, 1) * amp[28] - 1./3. *
      Complex<double> (0, 1) * amp[29] - 1./3. * Complex<double> (0, 1) *
      amp[30] - 1./3. * Complex<double> (0, 1) * amp[31] + 1./3. * amp[38] +
      1./3. * amp[39] + 1./3. * amp[61] + 1./3. * amp[64] + 1./3. * amp[249] +
      1./3. * amp[252]);
  jamp[6] = +1./2. * (+amp[0] - amp[9] + Complex<double> (0, 1) * amp[10] +
      Complex<double> (0, 1) * amp[13] + Complex<double> (0, 1) * amp[12] +
      Complex<double> (0, 1) * amp[15] + Complex<double> (0, 1) * amp[16] +
      Complex<double> (0, 1) * amp[30] + Complex<double> (0, 1) * amp[32] +
      amp[33] + amp[37] + Complex<double> (0, 1) * amp[44] + Complex<double>
      (0, 1) * amp[45] + amp[46] - amp[64] - amp[65] + Complex<double> (0, 1) *
      amp[66] - amp[67] + Complex<double> (0, 1) * amp[68] + amp[70] - amp[73]
      + amp[71] + amp[264] - amp[267] + Complex<double> (0, 1) * amp[269] +
      Complex<double> (0, 1) * amp[271] + Complex<double> (0, 1) * amp[270] +
      Complex<double> (0, 1) * amp[273] + Complex<double> (0, 1) * amp[274] +
      amp[277] - Complex<double> (0, 1) * amp[327] - amp[328] - amp[332] +
      amp[335] + amp[334] + Complex<double> (0, 1) * amp[337] - Complex<double>
      (0, 1) * amp[340] + Complex<double> (0, 1) * amp[338] - Complex<double>
      (0, 1) * amp[350] - Complex<double> (0, 1) * amp[349] + amp[381] -
      amp[383] + Complex<double> (0, 1) * amp[384] - Complex<double> (0, 1) *
      amp[386] + amp[387] - amp[389]);
  jamp[7] = +1./2. * (-1./3. * Complex<double> (0, 1) * amp[25] - 1./3. *
      Complex<double> (0, 1) * amp[26] - 1./3. * Complex<double> (0, 1) *
      amp[32] - 1./3. * amp[36] - 1./3. * amp[37] - 1./3. * Complex<double> (0,
      1) * amp[45] - 1./3. * Complex<double> (0, 1) * amp[47] + 1./3. * amp[58]
      + 1./3. * amp[60] + 1./3. * amp[67] - 1./3. * amp[276] - 1./3. * amp[277]
      - 1./3. * Complex<double> (0, 1) * amp[281] + 1./3. * amp[373] - 1./3. *
      amp[387] + 1./3. * amp[389] - 1./3. * amp[390] + 1./3. * amp[392]);
  jamp[8] = +1./2. * (-1./3. * amp[0] - 1./3. * amp[1] - 1./3. *
      Complex<double> (0, 1) * amp[3] - 1./3. * Complex<double> (0, 1) *
      amp[10] - 1./3. * Complex<double> (0, 1) * amp[11] - 1./3. *
      Complex<double> (0, 1) * amp[164] - 1./3. * Complex<double> (0, 1) *
      amp[165] + 1./3. * amp[173] + 1./3. * amp[181] + 1./3. * amp[182] - 1./3.
      * amp[264] - 1./3. * amp[265] - 1./3. * Complex<double> (0, 1) * amp[269]
      + 1./3. * amp[332] - 1./3. * amp[378] + 1./3. * amp[380] - 1./3. *
      amp[381] + 1./3. * amp[383]);
  jamp[9] = +1./2. * (+amp[1] - amp[2] + Complex<double> (0, 1) * amp[3] -
      Complex<double> (0, 1) * amp[14] - Complex<double> (0, 1) * amp[13] -
      Complex<double> (0, 1) * amp[16] - Complex<double> (0, 1) * amp[17] +
      Complex<double> (0, 1) * amp[23] + Complex<double> (0, 1) * amp[26] -
      amp[27] + amp[36] + Complex<double> (0, 1) * amp[163] + Complex<double>
      (0, 1) * amp[164] + amp[166] - Complex<double> (0, 1) * amp[172] -
      amp[173] - amp[177] + amp[180] + amp[179] - amp[187] - amp[188] +
      Complex<double> (0, 1) * amp[189] + amp[265] + Complex<double> (0, 1) *
      amp[272] - Complex<double> (0, 1) * amp[270] - Complex<double> (0, 1) *
      amp[273] + Complex<double> (0, 1) * amp[275] + amp[276] - amp[279] +
      Complex<double> (0, 1) * amp[281] - Complex<double> (0, 1) * amp[342] +
      Complex<double> (0, 1) * amp[345] + Complex<double> (0, 1) * amp[344] +
      Complex<double> (0, 1) * amp[353] + Complex<double> (0, 1) * amp[352] -
      Complex<double> (0, 1) * amp[371] - amp[372] - amp[373] + amp[377] +
      amp[376] + amp[378] - amp[380] - Complex<double> (0, 1) * amp[384] +
      Complex<double> (0, 1) * amp[386] + amp[390] - amp[392]);
  jamp[10] = +1./2. * (-1./3. * Complex<double> (0, 1) * amp[18] - 1./3. *
      Complex<double> (0, 1) * amp[19] - 1./3. * Complex<double> (0, 1) *
      amp[23] - 1./3. * Complex<double> (0, 1) * amp[24] + 1./3. * amp[170] +
      1./3. * amp[171] + 1./3. * amp[184] + 1./3. * amp[187] + 1./3. * amp[312]
      + 1./3. * amp[315]);
  jamp[11] = +1./2. * (+amp[2] + amp[9] + Complex<double> (0, 1) * amp[11] +
      Complex<double> (0, 1) * amp[14] - Complex<double> (0, 1) * amp[12] -
      Complex<double> (0, 1) * amp[15] + Complex<double> (0, 1) * amp[17] +
      Complex<double> (0, 1) * amp[18] + Complex<double> (0, 1) * amp[21] -
      amp[22] + Complex<double> (0, 1) * amp[31] + amp[35] + Complex<double>
      (0, 1) * amp[158] - amp[160] - amp[161] + Complex<double> (0, 1) *
      amp[172] + amp[176] + amp[177] - amp[180] + amp[178] - amp[181] +
      Complex<double> (0, 1) * amp[183] - amp[184] + Complex<double> (0, 1) *
      amp[186] - amp[246] - Complex<double> (0, 1) * amp[248] - amp[252] +
      Complex<double> (0, 1) * amp[254] + Complex<double> (0, 1) * amp[256] -
      amp[257] + amp[259] - amp[261] - amp[262] + amp[331] - amp[335] +
      amp[333] + Complex<double> (0, 1) * amp[341] + Complex<double> (0, 1) *
      amp[342] - Complex<double> (0, 1) * amp[345] + Complex<double> (0, 1) *
      amp[343] - Complex<double> (0, 1) * amp[346] + Complex<double> (0, 1) *
      amp[350] - Complex<double> (0, 1) * amp[348] - Complex<double> (0, 1) *
      amp[353] - Complex<double> (0, 1) * amp[352] + Complex<double> (0, 1) *
      amp[357] + Complex<double> (0, 1) * amp[358] - amp[363] + amp[368] +
      amp[367]);
  jamp[12] = +1./2. * (-amp[39] + Complex<double> (0, 1) * amp[40] - amp[41] -
      Complex<double> (0, 1) * amp[49] - amp[50] - amp[52] - Complex<double>
      (0, 1) * amp[66] + amp[69] - amp[70] + amp[73] + amp[72] -
      Complex<double> (0, 1) * amp[74] + Complex<double> (0, 1) * amp[120] +
      Complex<double> (0, 1) * amp[122] + amp[123] + Complex<double> (0, 1) *
      amp[124] - amp[127] + Complex<double> (0, 1) * amp[128] + amp[138] -
      Complex<double> (0, 1) * amp[142] - Complex<double> (0, 1) * amp[141] -
      Complex<double> (0, 1) * amp[144] - Complex<double> (0, 1) * amp[145] -
      amp[148] - amp[253] - Complex<double> (0, 1) * amp[254] - amp[259] -
      amp[263] + amp[261] + amp[285] + amp[290] + amp[289] - Complex<double>
      (0, 1) * amp[292] - amp[293] + amp[328] + Complex<double> (0, 1) *
      amp[329] - amp[331] - amp[333] - amp[334] + Complex<double> (0, 1) *
      amp[336] - Complex<double> (0, 1) * amp[337] + Complex<double> (0, 1) *
      amp[340] + Complex<double> (0, 1) * amp[339] + Complex<double> (0, 1) *
      amp[346] + Complex<double> (0, 1) * amp[348] + Complex<double> (0, 1) *
      amp[349] + Complex<double> (0, 1) * amp[356] + Complex<double> (0, 1) *
      amp[355] + Complex<double> (0, 1) * amp[359] - Complex<double> (0, 1) *
      amp[357]);
  jamp[13] = +1./2. * (+1./3. * amp[50] + 1./3. * amp[51] + 1./3. * amp[55] +
      1./3. * amp[56] - 1./3. * Complex<double> (0, 1) * amp[124] - 1./3. *
      Complex<double> (0, 1) * amp[125] - 1./3. * Complex<double> (0, 1) *
      amp[129] - 1./3. * Complex<double> (0, 1) * amp[130] + 1./3. * amp[298] +
      1./3. * amp[316]);
  jamp[14] = +1./2. * (-Complex<double> (0, 1) * amp[44] - Complex<double> (0,
      1) * amp[45] - amp[46] - amp[53] - Complex<double> (0, 1) * amp[54] -
      amp[55] - amp[69] - amp[72] - amp[71] + Complex<double> (0, 1) * amp[74]
      - amp[75] + Complex<double> (0, 1) * amp[129] + Complex<double> (0, 1) *
      amp[131] - amp[132] + amp[134] + Complex<double> (0, 1) * amp[141] +
      Complex<double> (0, 1) * amp[140] + Complex<double> (0, 1) * amp[143] +
      Complex<double> (0, 1) * amp[144] + amp[147] + amp[148] + Complex<double>
      (0, 1) * amp[149] - amp[264] + amp[267] - Complex<double> (0, 1) *
      amp[269] - Complex<double> (0, 1) * amp[271] - Complex<double> (0, 1) *
      amp[270] - Complex<double> (0, 1) * amp[273] - Complex<double> (0, 1) *
      amp[274] - amp[277] - Complex<double> (0, 1) * amp[302] + amp[303] -
      amp[304] + amp[308] + amp[307] - Complex<double> (0, 1) * amp[336] -
      Complex<double> (0, 1) * amp[339] - Complex<double> (0, 1) * amp[338] -
      Complex<double> (0, 1) * amp[356] - Complex<double> (0, 1) * amp[355] -
      amp[381] - amp[382] - Complex<double> (0, 1) * amp[384] - Complex<double>
      (0, 1) * amp[385] - amp[387] - amp[388]);
  jamp[15] = +1./2. * (+1./3. * amp[41] + 1./3. * amp[42] + 1./3. *
      Complex<double> (0, 1) * amp[45] + 1./3. * Complex<double> (0, 1) *
      amp[47] + 1./3. * amp[75] - 1./3. * Complex<double> (0, 1) * amp[121] -
      1./3. * Complex<double> (0, 1) * amp[122] - 1./3. * amp[146] - 1./3. *
      amp[147] - 1./3. * Complex<double> (0, 1) * amp[149] + 1./3. * amp[260] +
      1./3. * amp[276] + 1./3. * amp[277] + 1./3. * Complex<double> (0, 1) *
      amp[281] + 1./3. * amp[387] + 1./3. * amp[388] + 1./3. * amp[390] + 1./3.
      * amp[391]);
  jamp[16] = +1./2. * (-1./3. * Complex<double> (0, 1) * amp[131] - 1./3. *
      Complex<double> (0, 1) * amp[133] - 1./3. * amp[134] - 1./3. * amp[135] -
      1./3. * Complex<double> (0, 1) * amp[137] + 1./3. * Complex<double> (0,
      1) * amp[164] + 1./3. * Complex<double> (0, 1) * amp[165] + 1./3. *
      amp[168] + 1./3. * amp[169] + 1./3. * amp[175] + 1./3. * amp[264] + 1./3.
      * amp[265] + 1./3. * Complex<double> (0, 1) * amp[269] + 1./3. * amp[304]
      + 1./3. * amp[378] + 1./3. * amp[379] + 1./3. * amp[381] + 1./3. *
      amp[382]);
  jamp[17] = +1./2. * (+Complex<double> (0, 1) * amp[119] + Complex<double> (0,
      1) * amp[121] - amp[123] + amp[135] + amp[136] + Complex<double> (0, 1) *
      amp[137] + Complex<double> (0, 1) * amp[142] - Complex<double> (0, 1) *
      amp[140] - Complex<double> (0, 1) * amp[143] + Complex<double> (0, 1) *
      amp[145] + amp[146] - amp[157] - Complex<double> (0, 1) * amp[158] -
      amp[159] - Complex<double> (0, 1) * amp[163] - Complex<double> (0, 1) *
      amp[164] - amp[166] + Complex<double> (0, 1) * amp[174] - amp[175] -
      amp[176] - amp[179] - amp[178] + amp[257] - Complex<double> (0, 1) *
      amp[258] - amp[260] + amp[263] + amp[262] - amp[265] - Complex<double>
      (0, 1) * amp[272] + Complex<double> (0, 1) * amp[270] + Complex<double>
      (0, 1) * amp[273] - Complex<double> (0, 1) * amp[275] - amp[276] +
      amp[279] - Complex<double> (0, 1) * amp[281] - Complex<double> (0, 1) *
      amp[341] - Complex<double> (0, 1) * amp[344] - Complex<double> (0, 1) *
      amp[343] - Complex<double> (0, 1) * amp[359] - Complex<double> (0, 1) *
      amp[358] - amp[378] - amp[379] + Complex<double> (0, 1) * amp[384] +
      Complex<double> (0, 1) * amp[385] - amp[390] - amp[391]);
  jamp[18] = +1./2. * (-1./3. * Complex<double> (0, 1) * amp[114] - 1./3. *
      Complex<double> (0, 1) * amp[115] - 1./3. * Complex<double> (0, 1) *
      amp[119] - 1./3. * Complex<double> (0, 1) * amp[120] + 1./3. * amp[152] +
      1./3. * amp[156] + 1./3. * amp[157] + 1./3. * amp[161] + 1./3. * amp[235]
      + 1./3. * amp[253]);
  jamp[19] = +1./2. * (+Complex<double> (0, 1) * amp[114] + Complex<double> (0,
      1) * amp[117] - amp[118] + Complex<double> (0, 1) * amp[130] + amp[132] +
      Complex<double> (0, 1) * amp[133] - amp[136] - Complex<double> (0, 1) *
      amp[142] - Complex<double> (0, 1) * amp[141] - Complex<double> (0, 1) *
      amp[144] - Complex<double> (0, 1) * amp[145] + amp[150] - amp[152] -
      Complex<double> (0, 1) * amp[153] - amp[155] - Complex<double> (0, 1) *
      amp[167] - amp[169] - amp[171] - Complex<double> (0, 1) * amp[174] +
      amp[176] + amp[177] - amp[180] + amp[178] - Complex<double> (0, 1) *
      amp[189] - amp[229] - Complex<double> (0, 1) * amp[230] + amp[239] +
      amp[245] + amp[244] + amp[305] - amp[306] - amp[307] - amp[316] -
      Complex<double> (0, 1) * amp[317] + Complex<double> (0, 1) * amp[341] +
      Complex<double> (0, 1) * amp[342] - Complex<double> (0, 1) * amp[345] +
      Complex<double> (0, 1) * amp[343] - Complex<double> (0, 1) * amp[347] -
      Complex<double> (0, 1) * amp[351] - Complex<double> (0, 1) * amp[352] +
      Complex<double> (0, 1) * amp[354] + Complex<double> (0, 1) * amp[355] +
      Complex<double> (0, 1) * amp[359] + Complex<double> (0, 1) * amp[358] +
      Complex<double> (0, 1) * amp[369] + amp[372] - amp[374] - amp[375] -
      amp[376]);
  jamp[20] = +1./2. * (+1./3. * amp[154] + 1./3. * amp[155] - 1./3. *
      Complex<double> (0, 1) * amp[162] - 1./3. * Complex<double> (0, 1) *
      amp[163] + 1./3. * amp[188] + 1./3. * amp[192] + 1./3. * amp[193] - 1./3.
      * Complex<double> (0, 1) * amp[200] - 1./3. * Complex<double> (0, 1) *
      amp[201] + 1./3. * amp[226]);
  jamp[21] = +1./2. * (+1./3. * amp[159] + 1./3. * amp[160] + 1./3. *
      Complex<double> (0, 1) * amp[162] + 1./3. * Complex<double> (0, 1) *
      amp[163] + 1./3. * amp[185] + 1./3. * amp[197] + 1./3. * amp[198] + 1./3.
      * Complex<double> (0, 1) * amp[200] + 1./3. * Complex<double> (0, 1) *
      amp[201] + 1./3. * amp[223]);
  jamp[22] = +1./2. * (-amp[77] + Complex<double> (0, 1) * amp[78] - amp[80] +
      Complex<double> (0, 1) * amp[82] + amp[84] + Complex<double> (0, 1) *
      amp[85] - amp[103] + Complex<double> (0, 1) * amp[104] + amp[108] -
      amp[111] + amp[109] + Complex<double> (0, 1) * amp[153] - amp[154] -
      amp[156] + Complex<double> (0, 1) * amp[162] + Complex<double> (0, 1) *
      amp[165] - amp[166] - amp[176] - amp[179] - amp[178] - amp[182] -
      Complex<double> (0, 1) * amp[183] + Complex<double> (0, 1) * amp[236] -
      amp[239] - Complex<double> (0, 1) * amp[240] + amp[241] - amp[243] -
      amp[244] + amp[267] - Complex<double> (0, 1) * amp[272] - Complex<double>
      (0, 1) * amp[271] - Complex<double> (0, 1) * amp[274] - Complex<double>
      (0, 1) * amp[275] - amp[278] + Complex<double> (0, 1) * amp[327] +
      amp[328] - amp[331] - amp[333] - amp[334] - Complex<double> (0, 1) *
      amp[337] + Complex<double> (0, 1) * amp[340] - Complex<double> (0, 1) *
      amp[338] - Complex<double> (0, 1) * amp[341] - Complex<double> (0, 1) *
      amp[344] - Complex<double> (0, 1) * amp[343] + Complex<double> (0, 1) *
      amp[346] + Complex<double> (0, 1) * amp[348] + Complex<double> (0, 1) *
      amp[349] - Complex<double> (0, 1) * amp[357] - Complex<double> (0, 1) *
      amp[358]);
  jamp[23] = +1./2. * (-Complex<double> (0, 1) * amp[82] - amp[84] -
      Complex<double> (0, 1) * amp[85] - amp[91] - Complex<double> (0, 1) *
      amp[92] - amp[94] + Complex<double> (0, 1) * amp[97] - amp[98] - amp[107]
      - amp[110] - amp[109] - Complex<double> (0, 1) * amp[162] -
      Complex<double> (0, 1) * amp[165] + amp[166] + Complex<double> (0, 1) *
      amp[167] - amp[168] - amp[170] - amp[177] + amp[180] + amp[179] -
      amp[185] - Complex<double> (0, 1) * amp[186] - amp[267] + Complex<double>
      (0, 1) * amp[272] + Complex<double> (0, 1) * amp[271] + Complex<double>
      (0, 1) * amp[274] + Complex<double> (0, 1) * amp[275] + amp[278] +
      Complex<double> (0, 1) * amp[302] - amp[303] - amp[305] - amp[308] +
      amp[306] - Complex<double> (0, 1) * amp[314] + Complex<double> (0, 1) *
      amp[336] + Complex<double> (0, 1) * amp[339] + Complex<double> (0, 1) *
      amp[338] - Complex<double> (0, 1) * amp[342] + Complex<double> (0, 1) *
      amp[345] + Complex<double> (0, 1) * amp[344] + Complex<double> (0, 1) *
      amp[347] + Complex<double> (0, 1) * amp[351] + Complex<double> (0, 1) *
      amp[352] + Complex<double> (0, 1) * amp[356] - Complex<double> (0, 1) *
      amp[354] - Complex<double> (0, 1) * amp[362] + amp[363] - amp[365] -
      amp[366] - amp[367]);
  jamp[24] = +1./2. * (-amp[1] + amp[2] - Complex<double> (0, 1) * amp[3] +
      Complex<double> (0, 1) * amp[14] + Complex<double> (0, 1) * amp[13] +
      Complex<double> (0, 1) * amp[16] + Complex<double> (0, 1) * amp[17] -
      Complex<double> (0, 1) * amp[23] - Complex<double> (0, 1) * amp[26] +
      amp[27] - amp[36] + Complex<double> (0, 1) * amp[125] + Complex<double>
      (0, 1) * amp[126] + amp[127] - amp[135] + Complex<double> (0, 1) *
      amp[141] + Complex<double> (0, 1) * amp[140] + Complex<double> (0, 1) *
      amp[143] + Complex<double> (0, 1) * amp[144] - amp[146] - amp[150] +
      Complex<double> (0, 1) * amp[151] - Complex<double> (0, 1) * amp[282] -
      amp[283] + amp[287] - amp[288] - amp[289] - amp[297] - amp[298] -
      Complex<double> (0, 1) * amp[299] + Complex<double> (0, 1) * amp[347] -
      Complex<double> (0, 1) * amp[353] + Complex<double> (0, 1) * amp[351] -
      Complex<double> (0, 1) * amp[354] - Complex<double> (0, 1) * amp[355] -
      Complex<double> (0, 1) * amp[369] - amp[370] + amp[374] - amp[377] +
      amp[375] + amp[379] + amp[380] - Complex<double> (0, 1) * amp[385] -
      Complex<double> (0, 1) * amp[386] + amp[391] + amp[392]);
  jamp[25] = +1./2. * (+Complex<double> (0, 1) * amp[81] + Complex<double> (0,
      1) * amp[83] - amp[84] - amp[86] + Complex<double> (0, 1) * amp[87] -
      amp[88] - amp[107] - amp[110] - amp[109] - Complex<double> (0, 1) *
      amp[112] - amp[113] - Complex<double> (0, 1) * amp[125] - Complex<double>
      (0, 1) * amp[126] - amp[127] + amp[135] - Complex<double> (0, 1) *
      amp[141] - Complex<double> (0, 1) * amp[140] - Complex<double> (0, 1) *
      amp[143] - Complex<double> (0, 1) * amp[144] + amp[146] + amp[150] -
      Complex<double> (0, 1) * amp[151] - amp[265] + amp[266] + Complex<double>
      (0, 1) * amp[268] + Complex<double> (0, 1) * amp[271] + Complex<double>
      (0, 1) * amp[270] + Complex<double> (0, 1) * amp[273] + Complex<double>
      (0, 1) * amp[274] - amp[276] + Complex<double> (0, 1) * amp[284] +
      amp[285] - amp[286] + amp[290] + amp[289] + Complex<double> (0, 1) *
      amp[336] + Complex<double> (0, 1) * amp[339] + Complex<double> (0, 1) *
      amp[338] + Complex<double> (0, 1) * amp[356] + Complex<double> (0, 1) *
      amp[355] - amp[378] - amp[379] + Complex<double> (0, 1) * amp[384] +
      Complex<double> (0, 1) * amp[385] - amp[390] - amp[391]);
  jamp[26] = +1./2. * (-amp[1] + amp[6] + Complex<double> (0, 1) * amp[7] +
      Complex<double> (0, 1) * amp[13] + Complex<double> (0, 1) * amp[12] +
      Complex<double> (0, 1) * amp[15] + Complex<double> (0, 1) * amp[16] +
      Complex<double> (0, 1) * amp[29] + Complex<double> (0, 1) * amp[34] -
      amp[35] - amp[36] - Complex<double> (0, 1) * amp[119] - Complex<double>
      (0, 1) * amp[121] + amp[123] - amp[135] - amp[136] - Complex<double> (0,
      1) * amp[137] - Complex<double> (0, 1) * amp[142] + Complex<double> (0,
      1) * amp[140] + Complex<double> (0, 1) * amp[143] - Complex<double> (0,
      1) * amp[145] - amp[146] - amp[249] - amp[250] + Complex<double> (0, 1) *
      amp[251] - amp[255] - Complex<double> (0, 1) * amp[256] - amp[259] -
      amp[263] + amp[261] + Complex<double> (0, 1) * amp[320] - amp[321] +
      amp[322] - amp[326] + amp[324] + Complex<double> (0, 1) * amp[346] -
      Complex<double> (0, 1) * amp[350] + Complex<double> (0, 1) * amp[348] +
      Complex<double> (0, 1) * amp[359] - Complex<double> (0, 1) * amp[357] +
      amp[379] + amp[380] - Complex<double> (0, 1) * amp[385] - Complex<double>
      (0, 1) * amp[386] + amp[391] + amp[392]);
  jamp[27] = +1./2. * (+amp[1] - amp[6] - Complex<double> (0, 1) * amp[7] -
      Complex<double> (0, 1) * amp[13] - Complex<double> (0, 1) * amp[12] -
      Complex<double> (0, 1) * amp[15] - Complex<double> (0, 1) * amp[16] -
      Complex<double> (0, 1) * amp[29] - Complex<double> (0, 1) * amp[34] +
      amp[35] + amp[36] - Complex<double> (0, 1) * amp[81] - Complex<double>
      (0, 1) * amp[83] + amp[84] - amp[99] - amp[100] - Complex<double> (0, 1)
      * amp[101] - amp[105] - Complex<double> (0, 1) * amp[106] + amp[108] -
      amp[111] + amp[109] + amp[265] - amp[266] - Complex<double> (0, 1) *
      amp[268] - Complex<double> (0, 1) * amp[271] - Complex<double> (0, 1) *
      amp[270] - Complex<double> (0, 1) * amp[273] - Complex<double> (0, 1) *
      amp[274] + amp[276] + Complex<double> (0, 1) * amp[318] - amp[319] -
      amp[323] + amp[326] + amp[325] - Complex<double> (0, 1) * amp[337] +
      Complex<double> (0, 1) * amp[340] - Complex<double> (0, 1) * amp[338] +
      Complex<double> (0, 1) * amp[350] + Complex<double> (0, 1) * amp[349] +
      amp[378] - amp[380] - Complex<double> (0, 1) * amp[384] + Complex<double>
      (0, 1) * amp[386] + amp[390] - amp[392]);
  jamp[28] = +1./2. * (+1./3. * amp[0] + 1./3. * amp[1] + 1./3. *
      Complex<double> (0, 1) * amp[3] + 1./3. * Complex<double> (0, 1) *
      amp[10] + 1./3. * Complex<double> (0, 1) * amp[11] - 1./3. *
      Complex<double> (0, 1) * amp[126] - 1./3. * Complex<double> (0, 1) *
      amp[128] + 1./3. * amp[134] + 1./3. * amp[135] - 1./3. * Complex<double>
      (0, 1) * amp[139] + 1./3. * amp[283] + 1./3. * amp[291] + 1./3. *
      amp[293] + 1./3. * amp[330] - 1./3. * amp[379] - 1./3. * amp[380] - 1./3.
      * amp[382] - 1./3. * amp[383]);
  jamp[29] = +1./2. * (+1./3. * Complex<double> (0, 1) * amp[126] + 1./3. *
      Complex<double> (0, 1) * amp[128] - 1./3. * amp[134] - 1./3. * amp[135] +
      1./3. * Complex<double> (0, 1) * amp[139] - 1./3. * Complex<double> (0,
      1) * amp[202] - 1./3. * Complex<double> (0, 1) * amp[203] + 1./3. *
      amp[206] + 1./3. * amp[207] + 1./3. * amp[213] + 1./3. * amp[264] + 1./3.
      * amp[265] - 1./3. * Complex<double> (0, 1) * amp[268] + 1./3. * amp[286]
      + 1./3. * amp[378] + 1./3. * amp[379] + 1./3. * amp[381] + 1./3. *
      amp[382]);
  jamp[30] = +1./2. * (+1./3. * amp[0] + 1./3. * amp[1] - 1./3. *
      Complex<double> (0, 1) * amp[5] - 1./3. * Complex<double> (0, 1) * amp[7]
      - 1./3. * Complex<double> (0, 1) * amp[8] + 1./3. * Complex<double> (0,
      1) * amp[131] + 1./3. * Complex<double> (0, 1) * amp[133] + 1./3. *
      amp[134] + 1./3. * amp[135] + 1./3. * Complex<double> (0, 1) * amp[137] +
      1./3. * amp[301] + 1./3. * amp[309] + 1./3. * amp[311] + 1./3. * amp[321]
      - 1./3. * amp[379] - 1./3. * amp[380] - 1./3. * amp[382] - 1./3. *
      amp[383]);
  jamp[31] = +1./2. * (-1./3. * amp[0] - 1./3. * amp[1] + 1./3. *
      Complex<double> (0, 1) * amp[5] + 1./3. * Complex<double> (0, 1) * amp[7]
      + 1./3. * Complex<double> (0, 1) * amp[8] + 1./3. * Complex<double> (0,
      1) * amp[202] + 1./3. * Complex<double> (0, 1) * amp[203] + 1./3. *
      amp[211] + 1./3. * amp[219] + 1./3. * amp[220] - 1./3. * amp[264] - 1./3.
      * amp[265] + 1./3. * Complex<double> (0, 1) * amp[268] + 1./3. * amp[323]
      - 1./3. * amp[378] + 1./3. * amp[380] - 1./3. * amp[381] + 1./3. *
      amp[383]);
  jamp[32] = +1./2. * (+amp[4] + amp[6] - Complex<double> (0, 1) * amp[8] -
      Complex<double> (0, 1) * amp[14] + Complex<double> (0, 1) * amp[12] +
      Complex<double> (0, 1) * amp[15] - Complex<double> (0, 1) * amp[17] -
      Complex<double> (0, 1) * amp[24] - Complex<double> (0, 1) * amp[25] -
      amp[27] - Complex<double> (0, 1) * amp[28] + amp[33] - Complex<double>
      (0, 1) * amp[191] - amp[193] - amp[194] - Complex<double> (0, 1) *
      amp[210] + amp[214] + amp[215] - amp[218] + amp[216] - amp[219] -
      Complex<double> (0, 1) * amp[221] - amp[225] - Complex<double> (0, 1) *
      amp[227] - amp[228] + Complex<double> (0, 1) * amp[230] - amp[231] -
      Complex<double> (0, 1) * amp[233] - Complex<double> (0, 1) * amp[238] -
      amp[239] + amp[241] - amp[243] - amp[244] + amp[322] - amp[326] +
      amp[324] - Complex<double> (0, 1) * amp[341] - Complex<double> (0, 1) *
      amp[342] + Complex<double> (0, 1) * amp[345] - Complex<double> (0, 1) *
      amp[343] + Complex<double> (0, 1) * amp[346] - Complex<double> (0, 1) *
      amp[350] + Complex<double> (0, 1) * amp[348] + Complex<double> (0, 1) *
      amp[353] + Complex<double> (0, 1) * amp[352] - Complex<double> (0, 1) *
      amp[357] - Complex<double> (0, 1) * amp[358] - amp[372] + amp[377] +
      amp[376]);
  jamp[33] = +1./2. * (+1./3. * Complex<double> (0, 1) * amp[28] + 1./3. *
      Complex<double> (0, 1) * amp[29] + 1./3. * Complex<double> (0, 1) *
      amp[30] + 1./3. * Complex<double> (0, 1) * amp[31] + 1./3. * amp[76] +
      1./3. * amp[77] + 1./3. * amp[99] + 1./3. * amp[102] + 1./3. * amp[231] +
      1./3. * amp[234]);
  jamp[34] = +1./2. * (-amp[0] + amp[9] - Complex<double> (0, 1) * amp[10] -
      Complex<double> (0, 1) * amp[13] - Complex<double> (0, 1) * amp[12] -
      Complex<double> (0, 1) * amp[15] - Complex<double> (0, 1) * amp[16] -
      Complex<double> (0, 1) * amp[30] - Complex<double> (0, 1) * amp[32] -
      amp[33] - amp[37] + Complex<double> (0, 1) * amp[115] + Complex<double>
      (0, 1) * amp[116] + amp[118] - amp[134] - amp[138] + Complex<double> (0,
      1) * amp[139] + Complex<double> (0, 1) * amp[142] - Complex<double> (0,
      1) * amp[140] - Complex<double> (0, 1) * amp[143] + Complex<double> (0,
      1) * amp[145] - amp[147] - amp[234] - amp[235] - Complex<double> (0, 1) *
      amp[236] - amp[237] + Complex<double> (0, 1) * amp[238] - amp[241] -
      amp[245] + amp[243] - Complex<double> (0, 1) * amp[329] - amp[330] +
      amp[331] - amp[335] + amp[333] - Complex<double> (0, 1) * amp[346] +
      Complex<double> (0, 1) * amp[350] - Complex<double> (0, 1) * amp[348] -
      Complex<double> (0, 1) * amp[359] + Complex<double> (0, 1) * amp[357] +
      amp[382] + amp[383] + Complex<double> (0, 1) * amp[385] + Complex<double>
      (0, 1) * amp[386] + amp[388] + amp[389]);
  jamp[35] = +1./2. * (+1./3. * Complex<double> (0, 1) * amp[25] + 1./3. *
      Complex<double> (0, 1) * amp[26] + 1./3. * Complex<double> (0, 1) *
      amp[32] + 1./3. * amp[36] + 1./3. * amp[37] - 1./3. * Complex<double> (0,
      1) * amp[116] - 1./3. * Complex<double> (0, 1) * amp[117] + 1./3. *
      amp[146] + 1./3. * amp[147] - 1./3. * Complex<double> (0, 1) * amp[151] +
      1./3. * amp[228] + 1./3. * amp[229] + 1./3. * amp[237] + 1./3. * amp[370]
      - 1./3. * amp[388] - 1./3. * amp[389] - 1./3. * amp[391] - 1./3. *
      amp[392]);
  jamp[36] = +1./2. * (+1./3. * Complex<double> (0, 1) * amp[18] + 1./3. *
      Complex<double> (0, 1) * amp[19] + 1./3. * Complex<double> (0, 1) *
      amp[23] + 1./3. * Complex<double> (0, 1) * amp[24] + 1./3. * amp[208] +
      1./3. * amp[209] + 1./3. * amp[222] + 1./3. * amp[225] + 1./3. * amp[294]
      + 1./3. * amp[297]);
  jamp[37] = +1./2. * (-amp[2] - amp[9] - Complex<double> (0, 1) * amp[11] -
      Complex<double> (0, 1) * amp[14] + Complex<double> (0, 1) * amp[12] +
      Complex<double> (0, 1) * amp[15] - Complex<double> (0, 1) * amp[17] -
      Complex<double> (0, 1) * amp[18] - Complex<double> (0, 1) * amp[21] +
      amp[22] - Complex<double> (0, 1) * amp[31] - amp[35] - Complex<double>
      (0, 1) * amp[87] - amp[89] - amp[90] - amp[96] - Complex<double> (0, 1) *
      amp[97] - amp[102] - Complex<double> (0, 1) * amp[104] + Complex<double>
      (0, 1) * amp[106] + amp[107] - amp[108] + amp[111] + amp[110] +
      Complex<double> (0, 1) * amp[282] - amp[285] - amp[287] - amp[290] +
      amp[288] - amp[291] + Complex<double> (0, 1) * amp[292] - amp[294] -
      Complex<double> (0, 1) * amp[296] - amp[328] + amp[335] + amp[334] -
      Complex<double> (0, 1) * amp[336] + Complex<double> (0, 1) * amp[337] -
      Complex<double> (0, 1) * amp[340] - Complex<double> (0, 1) * amp[339] -
      Complex<double> (0, 1) * amp[347] - Complex<double> (0, 1) * amp[350] -
      Complex<double> (0, 1) * amp[349] + Complex<double> (0, 1) * amp[353] -
      Complex<double> (0, 1) * amp[351] - Complex<double> (0, 1) * amp[356] +
      Complex<double> (0, 1) * amp[354] + amp[365] - amp[368] + amp[366]);
  jamp[38] = +1./2. * (-amp[76] - Complex<double> (0, 1) * amp[78] - amp[79] +
      Complex<double> (0, 1) * amp[92] - amp[93] - amp[95] + Complex<double>
      (0, 1) * amp[101] + amp[107] - amp[108] + amp[111] + amp[110] +
      Complex<double> (0, 1) * amp[112] - Complex<double> (0, 1) * amp[114] -
      Complex<double> (0, 1) * amp[117] + amp[118] - Complex<double> (0, 1) *
      amp[130] - amp[132] - Complex<double> (0, 1) * amp[133] + amp[136] +
      Complex<double> (0, 1) * amp[142] + Complex<double> (0, 1) * amp[141] +
      Complex<double> (0, 1) * amp[144] + Complex<double> (0, 1) * amp[145] -
      amp[150] - amp[232] + Complex<double> (0, 1) * amp[233] - amp[241] -
      amp[245] + amp[243] + amp[303] + amp[308] + amp[307] + Complex<double>
      (0, 1) * amp[310] - amp[311] + amp[319] - Complex<double> (0, 1) *
      amp[320] - amp[322] - amp[324] - amp[325] - Complex<double> (0, 1) *
      amp[336] + Complex<double> (0, 1) * amp[337] - Complex<double> (0, 1) *
      amp[340] - Complex<double> (0, 1) * amp[339] - Complex<double> (0, 1) *
      amp[346] - Complex<double> (0, 1) * amp[348] - Complex<double> (0, 1) *
      amp[349] - Complex<double> (0, 1) * amp[356] - Complex<double> (0, 1) *
      amp[355] - Complex<double> (0, 1) * amp[359] + Complex<double> (0, 1) *
      amp[357]);
  jamp[39] = +1./2. * (+1./3. * Complex<double> (0, 1) * amp[114] + 1./3. *
      Complex<double> (0, 1) * amp[115] + 1./3. * Complex<double> (0, 1) *
      amp[119] + 1./3. * Complex<double> (0, 1) * amp[120] + 1./3. * amp[190] +
      1./3. * amp[194] + 1./3. * amp[195] + 1./3. * amp[199] + 1./3. * amp[232]
      + 1./3. * amp[250]);
  jamp[40] = +1./2. * (-Complex<double> (0, 1) * amp[115] - Complex<double> (0,
      1) * amp[116] - amp[118] + amp[134] + amp[138] - Complex<double> (0, 1) *
      amp[139] - Complex<double> (0, 1) * amp[142] + Complex<double> (0, 1) *
      amp[140] + Complex<double> (0, 1) * amp[143] - Complex<double> (0, 1) *
      amp[145] + amp[147] - amp[190] + Complex<double> (0, 1) * amp[191] -
      amp[192] + Complex<double> (0, 1) * amp[200] + Complex<double> (0, 1) *
      amp[202] - amp[204] - Complex<double> (0, 1) * amp[212] - amp[213] -
      amp[214] - amp[217] - amp[216] + amp[239] + Complex<double> (0, 1) *
      amp[240] - amp[242] + amp[245] + amp[244] - amp[264] + Complex<double>
      (0, 1) * amp[272] - Complex<double> (0, 1) * amp[270] - Complex<double>
      (0, 1) * amp[273] + Complex<double> (0, 1) * amp[275] - amp[277] +
      amp[278] + Complex<double> (0, 1) * amp[280] + Complex<double> (0, 1) *
      amp[341] + Complex<double> (0, 1) * amp[344] + Complex<double> (0, 1) *
      amp[343] + Complex<double> (0, 1) * amp[359] + Complex<double> (0, 1) *
      amp[358] - amp[381] - amp[382] - Complex<double> (0, 1) * amp[384] -
      Complex<double> (0, 1) * amp[385] - amp[387] - amp[388]);
  jamp[41] = +1./2. * (+1./3. * amp[79] + 1./3. * amp[80] - 1./3. *
      Complex<double> (0, 1) * amp[83] - 1./3. * Complex<double> (0, 1) *
      amp[85] + 1./3. * amp[113] + 1./3. * Complex<double> (0, 1) * amp[116] +
      1./3. * Complex<double> (0, 1) * amp[117] - 1./3. * amp[146] - 1./3. *
      amp[147] + 1./3. * Complex<double> (0, 1) * amp[151] + 1./3. * amp[242] +
      1./3. * amp[276] + 1./3. * amp[277] - 1./3. * Complex<double> (0, 1) *
      amp[280] + 1./3. * amp[387] + 1./3. * amp[388] + 1./3. * amp[390] + 1./3.
      * amp[391]);
  jamp[42] = +1./2. * (+1./3. * amp[88] + 1./3. * amp[89] + 1./3. * amp[93] +
      1./3. * amp[94] + 1./3. * Complex<double> (0, 1) * amp[124] + 1./3. *
      Complex<double> (0, 1) * amp[125] + 1./3. * Complex<double> (0, 1) *
      amp[129] + 1./3. * Complex<double> (0, 1) * amp[130] + 1./3. * amp[295] +
      1./3. * amp[313]);
  jamp[43] = +1./2. * (-Complex<double> (0, 1) * amp[120] - Complex<double> (0,
      1) * amp[122] - amp[123] - Complex<double> (0, 1) * amp[124] + amp[127] -
      Complex<double> (0, 1) * amp[128] - amp[138] + Complex<double> (0, 1) *
      amp[142] + Complex<double> (0, 1) * amp[141] + Complex<double> (0, 1) *
      amp[144] + Complex<double> (0, 1) * amp[145] + amp[148] - amp[195] +
      Complex<double> (0, 1) * amp[196] - amp[198] + Complex<double> (0, 1) *
      amp[205] - amp[207] - amp[208] + Complex<double> (0, 1) * amp[212] +
      amp[214] + amp[215] - amp[218] + amp[216] + Complex<double> (0, 1) *
      amp[224] - amp[247] + Complex<double> (0, 1) * amp[248] + amp[257] +
      amp[263] + amp[262] + amp[287] - amp[288] - amp[289] - amp[295] +
      Complex<double> (0, 1) * amp[296] - Complex<double> (0, 1) * amp[341] -
      Complex<double> (0, 1) * amp[342] + Complex<double> (0, 1) * amp[345] -
      Complex<double> (0, 1) * amp[343] + Complex<double> (0, 1) * amp[347] +
      Complex<double> (0, 1) * amp[351] + Complex<double> (0, 1) * amp[352] -
      Complex<double> (0, 1) * amp[354] - Complex<double> (0, 1) * amp[355] -
      Complex<double> (0, 1) * amp[359] - Complex<double> (0, 1) * amp[358] -
      Complex<double> (0, 1) * amp[360] + amp[363] - amp[365] - amp[366] -
      amp[367]);
  jamp[44] = +1./2. * (-amp[0] + amp[4] + Complex<double> (0, 1) * amp[5] -
      Complex<double> (0, 1) * amp[14] - Complex<double> (0, 1) * amp[13] -
      Complex<double> (0, 1) * amp[16] - Complex<double> (0, 1) * amp[17] +
      Complex<double> (0, 1) * amp[19] + Complex<double> (0, 1) * amp[20] +
      amp[22] - amp[37] - Complex<double> (0, 1) * amp[129] - Complex<double>
      (0, 1) * amp[131] + amp[132] - amp[134] - Complex<double> (0, 1) *
      amp[141] - Complex<double> (0, 1) * amp[140] - Complex<double> (0, 1) *
      amp[143] - Complex<double> (0, 1) * amp[144] - amp[147] - amp[148] -
      Complex<double> (0, 1) * amp[149] + Complex<double> (0, 1) * amp[300] -
      amp[301] + amp[305] - amp[306] - amp[307] - amp[312] - amp[313] +
      Complex<double> (0, 1) * amp[314] - Complex<double> (0, 1) * amp[347] +
      Complex<double> (0, 1) * amp[353] - Complex<double> (0, 1) * amp[351] +
      Complex<double> (0, 1) * amp[354] + Complex<double> (0, 1) * amp[355] +
      Complex<double> (0, 1) * amp[360] - amp[361] + amp[365] - amp[368] +
      amp[366] + amp[382] + amp[383] + Complex<double> (0, 1) * amp[385] +
      Complex<double> (0, 1) * amp[386] + amp[388] + amp[389]);
  jamp[45] = +1./2. * (-1./3. * Complex<double> (0, 1) * amp[20] - 1./3. *
      Complex<double> (0, 1) * amp[21] - 1./3. * Complex<double> (0, 1) *
      amp[34] + 1./3. * amp[36] + 1./3. * amp[37] + 1./3. * Complex<double> (0,
      1) * amp[121] + 1./3. * Complex<double> (0, 1) * amp[122] + 1./3. *
      amp[146] + 1./3. * amp[147] + 1./3. * Complex<double> (0, 1) * amp[149] +
      1./3. * amp[246] + 1./3. * amp[247] + 1./3. * amp[255] + 1./3. * amp[361]
      - 1./3. * amp[388] - 1./3. * amp[389] - 1./3. * amp[391] - 1./3. *
      amp[392]);
  jamp[46] = +1./2. * (+amp[0] - amp[4] - Complex<double> (0, 1) * amp[5] +
      Complex<double> (0, 1) * amp[14] + Complex<double> (0, 1) * amp[13] +
      Complex<double> (0, 1) * amp[16] + Complex<double> (0, 1) * amp[17] -
      Complex<double> (0, 1) * amp[19] - Complex<double> (0, 1) * amp[20] -
      amp[22] + amp[37] - Complex<double> (0, 1) * amp[200] - Complex<double>
      (0, 1) * amp[202] + amp[204] + Complex<double> (0, 1) * amp[210] -
      amp[211] - amp[215] + amp[218] + amp[217] - amp[222] - amp[223] -
      Complex<double> (0, 1) * amp[224] + amp[264] - Complex<double> (0, 1) *
      amp[272] + Complex<double> (0, 1) * amp[270] + Complex<double> (0, 1) *
      amp[273] - Complex<double> (0, 1) * amp[275] + amp[277] - amp[278] -
      Complex<double> (0, 1) * amp[280] + Complex<double> (0, 1) * amp[342] -
      Complex<double> (0, 1) * amp[345] - Complex<double> (0, 1) * amp[344] -
      Complex<double> (0, 1) * amp[353] - Complex<double> (0, 1) * amp[352] +
      Complex<double> (0, 1) * amp[362] - amp[363] - amp[364] + amp[368] +
      amp[367] + amp[381] - amp[383] + Complex<double> (0, 1) * amp[384] -
      Complex<double> (0, 1) * amp[386] + amp[387] - amp[389]);
  jamp[47] = +1./2. * (+1./3. * Complex<double> (0, 1) * amp[20] + 1./3. *
      Complex<double> (0, 1) * amp[21] + 1./3. * Complex<double> (0, 1) *
      amp[34] - 1./3. * amp[36] - 1./3. * amp[37] + 1./3. * Complex<double> (0,
      1) * amp[83] + 1./3. * Complex<double> (0, 1) * amp[85] + 1./3. * amp[96]
      + 1./3. * amp[98] + 1./3. * amp[105] - 1./3. * amp[276] - 1./3. *
      amp[277] + 1./3. * Complex<double> (0, 1) * amp[280] + 1./3. * amp[364] -
      1./3. * amp[387] + 1./3. * amp[389] - 1./3. * amp[390] + 1./3. *
      amp[392]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[0][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R16_P4_sm_gb_ttxggb::matrix_16_gbx_ttxggbx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 393;
  const int ncolor = 48; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
      9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
      9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9};
  static const double cf[ncolor][ncolor] = {{192, -24, 64, -8, 64, -8, 21, -6,
      0, -8, -8, -3, 24, 64, 1, 0, 0, 64, 1, 24, 24, -8, 10, 0, 0, 0, 0, 0, -8,
      64, 1, -8, -8, 1, 1, 10, -3, 24, -3, 24, 0, 1, 64, 24, -3, -8, -8, 0},
      {-24, 192, -8, 64, -8, 64, -6, 21, 0, 64, 1, 24, 24, -8, 10, 0, 0, -8,
      -8, -3, 24, 64, 1, 0, 0, 0, 0, 0, 1, -8, -8, 64, 1, 10, -8, 1, -3, 24,
      -3, 24, 0, -8, -8, -3, 24, 64, 1, 0}, {64, -8, 192, -24, 21, -6, 64, -8,
      -8, 0, -3, -8, 64, 24, 0, 1, 64, 0, 24, 1, -8, 24, 0, 10, -8, 64, 1, -8,
      0, 0, 0, 0, -3, 24, -3, 24, -8, 1, 1, 10, 1, 0, 24, 64, -8, -3, 0, -8},
      {-8, 64, -24, 192, -6, 21, -8, 64, 64, 0, 24, 1, -8, 24, 0, 10, -8, 0,
      -3, -8, 64, 24, 0, 1, 1, -8, -8, 64, 0, 0, 0, 0, -3, 24, -3, 24, 1, 10,
      -8, 1, -8, 0, -3, -8, 64, 24, 0, 1}, {64, -8, 21, -6, 192, -24, 64, -8,
      -8, -3, 0, -8, 1, 0, 24, 64, 1, 24, 0, 64, 10, 0, 24, -8, -8, 1, 1, 10,
      -3, 24, -3, 24, 0, 0, 0, 0, -8, 64, 1, -8, 64, 24, 0, 1, -8, 0, -3, -8},
      {-8, 64, -6, 21, -24, 192, -8, 64, 1, 24, 0, 64, 10, 0, 24, -8, -8, -3,
      0, -8, 1, 0, 24, 64, 1, 10, -8, 1, -3, 24, -3, 24, 0, 0, 0, 0, 1, -8, -8,
      64, -8, -3, 0, -8, 1, 0, 24, 64}, {21, -6, 64, -8, 64, -8, 192, -24, -3,
      -8, -8, 0, 0, 1, 64, 24, 24, 1, 64, 0, 0, 10, -8, 24, -3, 24, -3, 24, -8,
      1, 1, 10, -8, 64, 1, -8, 0, 0, 0, 0, 24, 64, 1, 0, 0, -8, -8, -3}, {-6,
      21, -8, 64, -8, 64, -24, 192, 24, 1, 64, 0, 0, 10, -8, 24, -3, -8, -8, 0,
      0, 1, 64, 24, -3, 24, -3, 24, 1, 10, -8, 1, 1, -8, -8, 64, 0, 0, 0, 0,
      -3, -8, -8, 0, 0, 1, 64, 24}, {0, 0, -8, 64, -8, 1, -3, 24, 192, 64, 64,
      21, 0, 64, 1, 24, 24, 64, 1, 0, 0, -8, -8, -3, 24, -3, 24, 24, 10, 1, 64,
      -8, -8, 1, 1, 10, 0, 0, 0, 0, -24, -8, -8, -6, 0, -8, -8, -3}, {-8, 64,
      0, 0, -3, 24, -8, 1, 64, 192, 21, 64, 64, 0, 24, 1, 64, 24, 0, 1, -8, 0,
      -3, -8, 10, 1, 64, -8, 24, -3, 24, 24, 0, 0, 0, 0, -8, 1, 1, 10, -8, -24,
      -6, -8, -8, 0, -3, -8}, {-8, 1, -3, 24, 0, 0, -8, 64, 64, 21, 192, 64, 1,
      24, 0, 64, 1, 0, 24, 64, -8, -3, 0, -8, -8, 1, 1, 10, 0, 0, 0, 0, 24, -3,
      24, 24, 10, 1, 64, -8, -8, -6, -24, -8, -8, -3, 0, -8}, {-3, 24, -8, 1,
      -8, 64, 0, 0, 21, 64, 64, 192, 24, 1, 64, 0, 0, 1, 64, 24, -3, -8, -8, 0,
      0, 0, 0, 0, -8, 1, 1, 10, 10, 1, 64, -8, 24, -3, 24, 24, -6, -8, -8, -24,
      -3, -8, -8, 0}, {24, 24, 64, -8, 1, 10, 0, 0, 0, 64, 1, 24, 192, 64, 64,
      21, 0, -8, -8, -3, -24, -8, -8, -6, -6, 21, 21, -6, -8, 1, 1, 10, -8, 1,
      1, 10, -24, 3, 3, 30, 0, -8, 10, 24, -3, 1, 1, 0}, {64, -8, 24, 24, 0, 0,
      1, 10, 64, 0, 24, 1, 64, 192, 21, 64, -8, 0, -3, -8, -8, -24, -6, -8, -8,
      1, 1, 10, -6, 21, 21, -6, -24, 3, 3, 30, -8, 1, 1, 10, -8, 0, 24, 10, 1,
      -3, 0, 1}, {1, 10, 0, 0, 24, 24, 64, -8, 1, 24, 0, 64, 64, 21, 192, 64,
      -8, -3, 0, -8, -8, -6, -24, -8, -8, 1, 1, 10, -24, 3, 3, 30, -6, 21, 21,
      -6, -8, 1, 1, 10, 10, 24, 0, -8, 1, 0, -3, 1}, {0, 0, 1, 10, 64, -8, 24,
      24, 24, 1, 64, 0, 21, 64, 64, 192, -3, -8, -8, 0, -6, -8, -8, -24, -24,
      3, 3, 30, -8, 1, 1, 10, -8, 1, 1, 10, -6, 21, 21, -6, 24, 10, -8, 0, 0,
      1, 1, -3}, {0, 0, 64, -8, 1, -8, 24, -3, 24, 64, 1, 0, 0, -8, -8, -3,
      192, 64, 64, 21, 0, 64, 1, 24, 24, 24, 24, -3, 64, -8, 10, 1, 1, 10, -8,
      1, 0, 0, 0, 0, -3, -8, -8, 0, -6, -8, -8, -24}, {64, -8, 0, 0, 24, -3, 1,
      -8, 64, 24, 0, 1, -8, 0, -3, -8, 64, 192, 21, 64, 64, 0, 24, 1, 64, -8,
      10, 1, 24, 24, 24, -3, 0, 0, 0, 0, 1, 10, -8, 1, -8, -3, 0, -8, -8, -6,
      -24, -8}, {1, -8, 24, -3, 0, 0, 64, -8, 1, 0, 24, 64, -8, -3, 0, -8, 64,
      21, 192, 64, 1, 24, 0, 64, 1, 10, -8, 1, 0, 0, 0, 0, 24, 24, 24, -3, 64,
      -8, 10, 1, -8, 0, -3, -8, -8, -24, -6, -8}, {24, -3, 1, -8, 64, -8, 0, 0,
      0, 1, 64, 24, -3, -8, -8, 0, 21, 64, 64, 192, 24, 1, 64, 0, 0, 0, 0, 0,
      1, 10, -8, 1, 64, -8, 10, 1, 24, 24, 24, -3, 0, -8, -8, -3, -24, -8, -8,
      -6}, {24, 24, -8, 64, 10, 1, 0, 0, 0, -8, -8, -3, -24, -8, -8, -6, 0, 64,
      1, 24, 192, 64, 64, 21, 21, -6, -6, 21, 1, 10, -8, 1, 1, 10, -8, 1, 3,
      30, -24, 3, 0, 1, 1, -3, 24, 10, -8, 0}, {-8, 64, 24, 24, 0, 0, 10, 1,
      -8, 0, -3, -8, -8, -24, -6, -8, 64, 0, 24, 1, 64, 192, 21, 64, 1, 10, -8,
      1, 21, -6, -6, 21, 3, 30, -24, 3, 1, 10, -8, 1, 1, 0, -3, 1, 10, 24, 0,
      -8}, {10, 1, 0, 0, 24, 24, -8, 64, -8, -3, 0, -8, -8, -6, -24, -8, 1, 24,
      0, 64, 64, 21, 192, 64, 1, 10, -8, 1, 3, 30, -24, 3, 21, -6, -6, 21, 1,
      10, -8, 1, 1, -3, 0, 1, -8, 0, 24, 10}, {0, 0, 10, 1, -8, 64, 24, 24, -3,
      -8, -8, 0, -6, -8, -8, -24, 24, 1, 64, 0, 21, 64, 64, 192, 3, 30, -24, 3,
      1, 10, -8, 1, 1, 10, -8, 1, 21, -6, -6, 21, -3, 1, 1, 0, 0, -8, 10, 24},
      {0, 0, -8, 1, -8, 1, -3, -3, 24, 10, -8, 0, -6, -8, -8, -24, 24, 64, 1,
      0, 21, 1, 1, 3, 192, -24, 30, 3, 64, -8, 10, 1, 64, -8, 10, 1, 21, -6,
      -6, 21, 24, 1, 64, 0, 0, 10, -8, 24}, {0, 0, 64, -8, 1, 10, 24, 24, -3,
      1, 1, 0, 21, 1, 1, 3, 24, -8, 10, 0, -6, 10, 10, 30, -24, 192, 3, -24,
      -8, 64, 1, -8, -8, 64, 1, -8, -6, 21, 21, -6, -3, -8, -8, 0, 0, 1, 64,
      24}, {0, 0, 1, -8, 1, -8, -3, -3, 24, 64, 1, 0, 21, 1, 1, 3, 24, 10, -8,
      0, -6, -8, -8, -24, 30, 3, 192, -24, 10, 1, 64, -8, 10, 1, 64, -8, -6,
      21, 21, -6, 24, -8, 10, 0, 0, 64, 1, 24}, {0, 0, -8, 64, 10, 1, 24, 24,
      24, -8, 10, 0, -6, 10, 10, 30, -3, 1, 1, 0, 21, 1, 1, 3, 3, -24, -24,
      192, 1, -8, -8, 64, 1, -8, -8, 64, 21, -6, -6, 21, 24, 64, 1, 0, 0, -8,
      -8, -3}, {-8, 1, 0, 0, -3, -3, -8, 1, 10, 24, 0, -8, -8, -6, -24, -8, 64,
      24, 0, 1, 1, 21, 3, 1, 64, -8, 10, 1, 192, -24, 30, 3, 21, -6, -6, 21,
      64, -8, 10, 1, 1, 24, 0, 64, 10, 0, 24, -8}, {64, -8, 0, 0, 24, 24, 1,
      10, 1, -3, 0, 1, 1, 21, 3, 1, -8, 24, 0, 10, 10, -6, 30, 10, -8, 64, 1,
      -8, -24, 192, 3, -24, -6, 21, 21, -6, -8, 64, 1, -8, -8, -3, 0, -8, 1, 0,
      24, 64}, {1, -8, 0, 0, -3, -3, 1, -8, 64, 24, 0, 1, 1, 21, 3, 1, 10, 24,
      0, -8, -8, -6, -24, -8, 10, 1, 64, -8, 30, 3, 192, -24, -6, 21, 21, -6,
      10, 1, 64, -8, -8, 24, 0, 10, 64, 0, 24, 1}, {-8, 64, 0, 0, 24, 24, 10,
      1, -8, 24, 0, 10, 10, -6, 30, 10, 1, -3, 0, 1, 1, 21, 3, 1, 1, -8, -8,
      64, 3, -24, -24, 192, 21, -6, -6, 21, 1, -8, -8, 64, 64, 24, 0, 1, -8, 0,
      -3, -8}, {-8, 1, -3, -3, 0, 0, -8, 1, -8, 0, 24, 10, -8, -24, -6, -8, 1,
      0, 24, 64, 1, 3, 21, 1, 64, -8, 10, 1, 21, -6, -6, 21, 192, -24, 30, 3,
      64, -8, 10, 1, 64, 0, 24, 1, -8, 24, 0, 10}, {1, 10, 24, 24, 0, 0, 64,
      -8, 1, 0, -3, 1, 1, 3, 21, 1, 10, 0, 24, -8, 10, 30, -6, 10, -8, 64, 1,
      -8, -6, 21, 21, -6, -24, 192, 3, -24, -8, 64, 1, -8, -8, 0, -3, -8, 64,
      24, 0, 1}, {1, -8, -3, -3, 0, 0, 1, -8, 1, 0, 24, 64, 1, 3, 21, 1, -8, 0,
      24, 10, -8, -24, -6, -8, 10, 1, 64, -8, -6, 21, 21, -6, 30, 3, 192, -24,
      10, 1, 64, -8, 10, 0, 24, -8, 1, 24, 0, 64}, {10, 1, 24, 24, 0, 0, -8,
      64, 10, 0, 24, -8, 10, 30, -6, 10, 1, 0, -3, 1, 1, 3, 21, 1, 1, -8, -8,
      64, 21, -6, -6, 21, 3, -24, -24, 192, 1, -8, -8, 64, 1, 0, 24, 64, -8,
      -3, 0, -8}, {-3, -3, -8, 1, -8, 1, 0, 0, 0, -8, 10, 24, -24, -8, -8, -6,
      0, 1, 64, 24, 3, 1, 1, 21, 21, -6, -6, 21, 64, -8, 10, 1, 64, -8, 10, 1,
      192, -24, 30, 3, 0, 64, 1, 24, 24, -8, 10, 0}, {24, 24, 1, 10, 64, -8, 0,
      0, 0, 1, 1, -3, 3, 1, 1, 21, 0, 10, -8, 24, 30, 10, 10, -6, -6, 21, 21,
      -6, -8, 64, 1, -8, -8, 64, 1, -8, -24, 192, 3, -24, 0, -8, -8, -3, 24,
      64, 1, 0}, {-3, -3, 1, -8, 1, -8, 0, 0, 0, 1, 64, 24, 3, 1, 1, 21, 0, -8,
      10, 24, -24, -8, -8, -6, -6, 21, 21, -6, 10, 1, 64, -8, 10, 1, 64, -8,
      30, 3, 192, -24, 0, 10, -8, 24, 24, 1, 64, 0}, {24, 24, 10, 1, -8, 64, 0,
      0, 0, 10, -8, 24, 30, 10, 10, -6, 0, 1, 1, -3, 3, 1, 1, 21, 21, -6, -6,
      21, 1, -8, -8, 64, 1, -8, -8, 64, 3, -24, -24, 192, 0, 1, 64, 24, -3, -8,
      -8, 0}, {0, 0, 1, -8, 64, -8, 24, -3, -24, -8, -8, -6, 0, -8, 10, 24, -3,
      -8, -8, 0, 0, 1, 1, -3, 24, -3, 24, 24, 1, -8, -8, 64, 64, -8, 10, 1, 0,
      0, 0, 0, 192, 64, 64, 21, 0, 64, 1, 24}, {1, -8, 0, 0, 24, -3, 64, -8,
      -8, -24, -6, -8, -8, 0, 24, 10, -8, -3, 0, -8, 1, 0, -3, 1, 1, -8, -8,
      64, 24, -3, 24, 24, 0, 0, 0, 0, 64, -8, 10, 1, 64, 192, 21, 64, 64, 0,
      24, 1}, {64, -8, 24, -3, 0, 0, 1, -8, -8, -6, -24, -8, 10, 24, 0, -8, -8,
      0, -3, -8, 1, -3, 0, 1, 64, -8, 10, 1, 0, 0, 0, 0, 24, -3, 24, 24, 1, -8,
      -8, 64, 64, 21, 192, 64, 1, 24, 0, 64}, {24, -3, 64, -8, 1, -8, 0, 0, -6,
      -8, -8, -24, 24, 10, -8, 0, 0, -8, -8, -3, -3, 1, 1, 0, 0, 0, 0, 0, 64,
      -8, 10, 1, 1, -8, -8, 64, 24, -3, 24, 24, 21, 64, 64, 192, 24, 1, 64, 0},
      {-3, 24, -8, 64, -8, 1, 0, 0, 0, -8, -8, -3, -3, 1, 1, 0, -6, -8, -8,
      -24, 24, 10, -8, 0, 0, 0, 0, 0, 10, 1, 64, -8, -8, 64, 1, -8, 24, 24, 24,
      -3, 0, 64, 1, 24, 192, 64, 64, 21}, {-8, 64, -3, 24, 0, 0, -8, 1, -8, 0,
      -3, -8, 1, -3, 0, 1, -8, -6, -24, -8, 10, 24, 0, -8, 10, 1, 64, -8, 0, 0,
      0, 0, 24, 24, 24, -3, -8, 64, 1, -8, 64, 0, 24, 1, 64, 192, 21, 64}, {-8,
      1, 0, 0, -3, 24, -8, 64, -8, -3, 0, -8, 1, 0, -3, 1, -8, -24, -6, -8, -8,
      0, 24, 10, -8, 64, 1, -8, 24, 24, 24, -3, 0, 0, 0, 0, 10, 1, 64, -8, 1,
      24, 0, 64, 64, 21, 192, 64}, {0, 0, -8, 1, -8, 64, -3, 24, -3, -8, -8, 0,
      0, 1, 1, -3, -24, -8, -8, -6, 0, -8, 10, 24, 24, 24, 24, -3, -8, 64, 1,
      -8, 10, 1, 64, -8, 0, 0, 0, 0, 24, 1, 64, 0, 21, 64, 64, 192}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[470] - Complex<double> (0, 1) * amp[471] + amp[473]
      - Complex<double> (0, 1) * amp[475] - amp[477] - Complex<double> (0, 1) *
      amp[478] + amp[496] - Complex<double> (0, 1) * amp[497] - amp[501] +
      amp[504] - amp[502] - Complex<double> (0, 1) * amp[546] + amp[547] +
      amp[549] - Complex<double> (0, 1) * amp[555] - Complex<double> (0, 1) *
      amp[558] + amp[559] + amp[569] + amp[572] + amp[571] + amp[575] +
      Complex<double> (0, 1) * amp[576] - Complex<double> (0, 1) * amp[629] +
      amp[632] + Complex<double> (0, 1) * amp[633] - amp[634] + amp[636] +
      amp[637] - amp[660] + Complex<double> (0, 1) * amp[665] + Complex<double>
      (0, 1) * amp[664] + Complex<double> (0, 1) * amp[667] + Complex<double>
      (0, 1) * amp[668] + amp[671] - Complex<double> (0, 1) * amp[720] -
      amp[721] + amp[724] + amp[726] + amp[727] + Complex<double> (0, 1) *
      amp[730] - Complex<double> (0, 1) * amp[733] + Complex<double> (0, 1) *
      amp[731] + Complex<double> (0, 1) * amp[734] + Complex<double> (0, 1) *
      amp[737] + Complex<double> (0, 1) * amp[736] - Complex<double> (0, 1) *
      amp[739] - Complex<double> (0, 1) * amp[741] - Complex<double> (0, 1) *
      amp[742] + Complex<double> (0, 1) * amp[750] + Complex<double> (0, 1) *
      amp[751]);
  jamp[1] = +1./2. * (+Complex<double> (0, 1) * amp[475] + amp[477] +
      Complex<double> (0, 1) * amp[478] + amp[484] + Complex<double> (0, 1) *
      amp[485] + amp[487] - Complex<double> (0, 1) * amp[490] + amp[491] +
      amp[500] + amp[503] + amp[502] + Complex<double> (0, 1) * amp[555] +
      Complex<double> (0, 1) * amp[558] - amp[559] - Complex<double> (0, 1) *
      amp[560] + amp[561] + amp[563] + amp[570] - amp[573] - amp[572] +
      amp[578] + Complex<double> (0, 1) * amp[579] + amp[660] - Complex<double>
      (0, 1) * amp[665] - Complex<double> (0, 1) * amp[664] - Complex<double>
      (0, 1) * amp[667] - Complex<double> (0, 1) * amp[668] - amp[671] -
      Complex<double> (0, 1) * amp[695] + amp[696] + amp[698] + amp[701] -
      amp[699] + Complex<double> (0, 1) * amp[707] - Complex<double> (0, 1) *
      amp[729] - Complex<double> (0, 1) * amp[732] - Complex<double> (0, 1) *
      amp[731] + Complex<double> (0, 1) * amp[735] - Complex<double> (0, 1) *
      amp[738] - Complex<double> (0, 1) * amp[737] - Complex<double> (0, 1) *
      amp[740] - Complex<double> (0, 1) * amp[744] - Complex<double> (0, 1) *
      amp[745] - Complex<double> (0, 1) * amp[749] + Complex<double> (0, 1) *
      amp[747] + Complex<double> (0, 1) * amp[755] - amp[756] + amp[758] +
      amp[759] + amp[760]);
  jamp[2] = +1./2. * (-1./3. * amp[547] - 1./3. * amp[548] + 1./3. *
      Complex<double> (0, 1) * amp[555] + 1./3. * Complex<double> (0, 1) *
      amp[556] - 1./3. * amp[581] - 1./3. * amp[585] - 1./3. * amp[586] + 1./3.
      * Complex<double> (0, 1) * amp[593] + 1./3. * Complex<double> (0, 1) *
      amp[594] - 1./3. * amp[619]);
  jamp[3] = +1./2. * (-1./3. * amp[552] - 1./3. * amp[553] - 1./3. *
      Complex<double> (0, 1) * amp[555] - 1./3. * Complex<double> (0, 1) *
      amp[556] - 1./3. * amp[578] - 1./3. * amp[590] - 1./3. * amp[591] - 1./3.
      * Complex<double> (0, 1) * amp[593] - 1./3. * Complex<double> (0, 1) *
      amp[594] - 1./3. * amp[616]);
  jamp[4] = +1./2. * (+1./3. * Complex<double> (0, 1) * amp[436] + 1./3. *
      Complex<double> (0, 1) * amp[437] - 1./3. * amp[441] - 1./3. * amp[445] -
      1./3. * amp[458] + 1./3. * Complex<double> (0, 1) * amp[474] + 1./3. *
      Complex<double> (0, 1) * amp[475] - 1./3. * amp[479] - 1./3. * amp[483] -
      1./3. * amp[496]);
  jamp[5] = +1./2. * (-1./3. * Complex<double> (0, 1) * amp[436] - 1./3. *
      Complex<double> (0, 1) * amp[437] - 1./3. * amp[446] - 1./3. * amp[450] -
      1./3. * amp[455] - 1./3. * Complex<double> (0, 1) * amp[474] - 1./3. *
      Complex<double> (0, 1) * amp[475] - 1./3. * amp[484] - 1./3. * amp[488] -
      1./3. * amp[493]);
  jamp[6] = +1./2. * (-Complex<double> (0, 1) * amp[436] + amp[439] -
      Complex<double> (0, 1) * amp[440] + amp[441] - Complex<double> (0, 1) *
      amp[442] + amp[444] + Complex<double> (0, 1) * amp[452] + amp[453] +
      amp[462] + amp[465] + amp[464] - Complex<double> (0, 1) * amp[594] -
      Complex<double> (0, 1) * amp[596] - amp[597] + Complex<double> (0, 1) *
      amp[598] + amp[599] + amp[602] + amp[608] - amp[611] - amp[610] +
      amp[619] - Complex<double> (0, 1) * amp[620] + amp[659] + Complex<double>
      (0, 1) * amp[665] + Complex<double> (0, 1) * amp[664] + Complex<double>
      (0, 1) * amp[667] + Complex<double> (0, 1) * amp[668] - amp[672] +
      Complex<double> (0, 1) * amp[677] + amp[678] + amp[680] + amp[683] -
      amp[681] - Complex<double> (0, 1) * amp[692] + Complex<double> (0, 1) *
      amp[729] + Complex<double> (0, 1) * amp[732] + Complex<double> (0, 1) *
      amp[731] - Complex<double> (0, 1) * amp[735] + Complex<double> (0, 1) *
      amp[738] + Complex<double> (0, 1) * amp[737] + Complex<double> (0, 1) *
      amp[740] + Complex<double> (0, 1) * amp[744] + Complex<double> (0, 1) *
      amp[745] + Complex<double> (0, 1) * amp[749] - Complex<double> (0, 1) *
      amp[747] - Complex<double> (0, 1) * amp[764] - amp[765] + amp[767] +
      amp[768] + amp[769]);
  jamp[7] = +1./2. * (+amp[431] + Complex<double> (0, 1) * amp[433] + amp[435]
      + Complex<double> (0, 1) * amp[436] - amp[439] + Complex<double> (0, 1) *
      amp[440] + amp[455] + Complex<double> (0, 1) * amp[456] - amp[463] +
      amp[466] - amp[464] + Complex<double> (0, 1) * amp[589] + amp[590] +
      amp[592] + Complex<double> (0, 1) * amp[594] + Complex<double> (0, 1) *
      amp[596] + amp[597] + amp[607] + amp[610] + amp[609] + amp[613] -
      Complex<double> (0, 1) * amp[614] + Complex<double> (0, 1) * amp[644] +
      amp[650] - Complex<double> (0, 1) * amp[651] - amp[652] + amp[654] +
      amp[655] - amp[659] - Complex<double> (0, 1) * amp[665] - Complex<double>
      (0, 1) * amp[664] - Complex<double> (0, 1) * amp[667] - Complex<double>
      (0, 1) * amp[668] + amp[672] + Complex<double> (0, 1) * amp[711] -
      amp[712] + amp[715] + amp[717] + amp[718] - Complex<double> (0, 1) *
      amp[730] + Complex<double> (0, 1) * amp[733] - Complex<double> (0, 1) *
      amp[731] - Complex<double> (0, 1) * amp[734] - Complex<double> (0, 1) *
      amp[737] - Complex<double> (0, 1) * amp[736] + Complex<double> (0, 1) *
      amp[739] + Complex<double> (0, 1) * amp[741] + Complex<double> (0, 1) *
      amp[742] - Complex<double> (0, 1) * amp[750] - Complex<double> (0, 1) *
      amp[751]);
  jamp[8] = +1./2. * (-amp[395] - amp[402] - Complex<double> (0, 1) * amp[404]
      - Complex<double> (0, 1) * amp[407] + Complex<double> (0, 1) * amp[405] +
      Complex<double> (0, 1) * amp[408] - Complex<double> (0, 1) * amp[410] -
      Complex<double> (0, 1) * amp[411] - Complex<double> (0, 1) * amp[414] +
      amp[415] - Complex<double> (0, 1) * amp[424] - amp[428] - Complex<double>
      (0, 1) * amp[551] + amp[553] + amp[554] - Complex<double> (0, 1) *
      amp[565] - amp[569] - amp[570] + amp[573] - amp[571] + amp[574] -
      Complex<double> (0, 1) * amp[576] + amp[577] - Complex<double> (0, 1) *
      amp[579] + amp[639] + Complex<double> (0, 1) * amp[641] + amp[645] -
      Complex<double> (0, 1) * amp[647] - Complex<double> (0, 1) * amp[649] +
      amp[650] - amp[652] + amp[654] + amp[655] - amp[724] + amp[728] -
      amp[726] - Complex<double> (0, 1) * amp[734] - Complex<double> (0, 1) *
      amp[735] + Complex<double> (0, 1) * amp[738] - Complex<double> (0, 1) *
      amp[736] + Complex<double> (0, 1) * amp[739] - Complex<double> (0, 1) *
      amp[743] + Complex<double> (0, 1) * amp[741] + Complex<double> (0, 1) *
      amp[746] + Complex<double> (0, 1) * amp[745] - Complex<double> (0, 1) *
      amp[750] - Complex<double> (0, 1) * amp[751] + amp[756] - amp[761] -
      amp[760]);
  jamp[9] = +1./2. * (+1./3. * Complex<double> (0, 1) * amp[411] + 1./3. *
      Complex<double> (0, 1) * amp[412] + 1./3. * Complex<double> (0, 1) *
      amp[416] + 1./3. * Complex<double> (0, 1) * amp[417] - 1./3. * amp[563] -
      1./3. * amp[564] - 1./3. * amp[577] - 1./3. * amp[580] - 1./3. * amp[705]
      - 1./3. * amp[708]);
  jamp[10] = +1./2. * (+1./3. * Complex<double> (0, 1) * amp[421] + 1./3. *
      Complex<double> (0, 1) * amp[422] + 1./3. * Complex<double> (0, 1) *
      amp[423] + 1./3. * Complex<double> (0, 1) * amp[424] - 1./3. * amp[431] -
      1./3. * amp[432] - 1./3. * amp[454] - 1./3. * amp[457] - 1./3. * amp[642]
      - 1./3. * amp[645]);
  jamp[11] = +1./2. * (+amp[397] + amp[399] - Complex<double> (0, 1) * amp[401]
      - Complex<double> (0, 1) * amp[407] + Complex<double> (0, 1) * amp[405] +
      Complex<double> (0, 1) * amp[408] - Complex<double> (0, 1) * amp[410] -
      Complex<double> (0, 1) * amp[417] - Complex<double> (0, 1) * amp[418] -
      amp[420] - Complex<double> (0, 1) * amp[421] + amp[426] - Complex<double>
      (0, 1) * amp[447] + amp[449] + amp[450] + amp[451] - Complex<double> (0,
      1) * amp[452] + amp[454] - Complex<double> (0, 1) * amp[456] +
      Complex<double> (0, 1) * amp[461] - amp[462] + amp[463] - amp[466] -
      amp[465] + Complex<double> (0, 1) * amp[693] + amp[696] + amp[698] +
      amp[701] - amp[699] + amp[702] + Complex<double> (0, 1) * amp[703] +
      amp[708] - Complex<double> (0, 1) * amp[710] + amp[712] - amp[719] -
      amp[718] - Complex<double> (0, 1) * amp[729] + Complex<double> (0, 1) *
      amp[730] - Complex<double> (0, 1) * amp[733] - Complex<double> (0, 1) *
      amp[732] - Complex<double> (0, 1) * amp[740] - Complex<double> (0, 1) *
      amp[743] - Complex<double> (0, 1) * amp[742] + Complex<double> (0, 1) *
      amp[746] - Complex<double> (0, 1) * amp[744] - Complex<double> (0, 1) *
      amp[749] + Complex<double> (0, 1) * amp[747] - amp[767] + amp[770] -
      amp[768]);
  jamp[12] = +1./2. * (-amp[394] + amp[395] - Complex<double> (0, 1) * amp[396]
      + Complex<double> (0, 1) * amp[407] + Complex<double> (0, 1) * amp[406] +
      Complex<double> (0, 1) * amp[409] + Complex<double> (0, 1) * amp[410] -
      Complex<double> (0, 1) * amp[416] - Complex<double> (0, 1) * amp[419] +
      amp[420] - amp[429] - Complex<double> (0, 1) * amp[556] - Complex<double>
      (0, 1) * amp[557] - amp[559] + Complex<double> (0, 1) * amp[565] +
      amp[566] + amp[570] - amp[573] - amp[572] + amp[580] + amp[581] -
      Complex<double> (0, 1) * amp[582] - amp[658] - Complex<double> (0, 1) *
      amp[665] + Complex<double> (0, 1) * amp[663] + Complex<double> (0, 1) *
      amp[666] - Complex<double> (0, 1) * amp[668] - amp[669] + amp[672] -
      Complex<double> (0, 1) * amp[674] + Complex<double> (0, 1) * amp[735] -
      Complex<double> (0, 1) * amp[738] - Complex<double> (0, 1) * amp[737] -
      Complex<double> (0, 1) * amp[746] - Complex<double> (0, 1) * amp[745] +
      Complex<double> (0, 1) * amp[764] + amp[765] + amp[766] - amp[770] -
      amp[769] - amp[771] + amp[773] + Complex<double> (0, 1) * amp[777] -
      Complex<double> (0, 1) * amp[779] - amp[783] + amp[785]);
  jamp[13] = +1./2. * (+1./3. * amp[393] + 1./3. * amp[394] + 1./3. *
      Complex<double> (0, 1) * amp[396] + 1./3. * Complex<double> (0, 1) *
      amp[403] + 1./3. * Complex<double> (0, 1) * amp[404] + 1./3. *
      Complex<double> (0, 1) * amp[557] + 1./3. * Complex<double> (0, 1) *
      amp[558] - 1./3. * amp[566] - 1./3. * amp[574] - 1./3. * amp[575] + 1./3.
      * amp[657] + 1./3. * amp[658] + 1./3. * Complex<double> (0, 1) * amp[662]
      - 1./3. * amp[725] + 1./3. * amp[771] - 1./3. * amp[773] + 1./3. *
      amp[774] - 1./3. * amp[776]);
  jamp[14] = +1./2. * (+1./3. * Complex<double> (0, 1) * amp[418] + 1./3. *
      Complex<double> (0, 1) * amp[419] + 1./3. * Complex<double> (0, 1) *
      amp[425] + 1./3. * amp[429] + 1./3. * amp[430] + 1./3. * Complex<double>
      (0, 1) * amp[438] + 1./3. * Complex<double> (0, 1) * amp[440] - 1./3. *
      amp[451] - 1./3. * amp[453] - 1./3. * amp[460] + 1./3. * amp[669] + 1./3.
      * amp[670] + 1./3. * Complex<double> (0, 1) * amp[674] - 1./3. * amp[766]
      + 1./3. * amp[780] - 1./3. * amp[782] + 1./3. * amp[783] - 1./3. *
      amp[785]);
  jamp[15] = +1./2. * (-amp[393] + amp[402] - Complex<double> (0, 1) * amp[403]
      - Complex<double> (0, 1) * amp[406] - Complex<double> (0, 1) * amp[405] -
      Complex<double> (0, 1) * amp[408] - Complex<double> (0, 1) * amp[409] -
      Complex<double> (0, 1) * amp[423] - Complex<double> (0, 1) * amp[425] -
      amp[426] - amp[430] - Complex<double> (0, 1) * amp[437] - Complex<double>
      (0, 1) * amp[438] - amp[439] + amp[457] + amp[458] - Complex<double> (0,
      1) * amp[459] + amp[460] - Complex<double> (0, 1) * amp[461] - amp[463] +
      amp[466] - amp[464] - amp[657] + amp[660] - Complex<double> (0, 1) *
      amp[662] - Complex<double> (0, 1) * amp[664] - Complex<double> (0, 1) *
      amp[663] - Complex<double> (0, 1) * amp[666] - Complex<double> (0, 1) *
      amp[667] - amp[670] + Complex<double> (0, 1) * amp[720] + amp[721] +
      amp[725] - amp[728] - amp[727] - Complex<double> (0, 1) * amp[730] +
      Complex<double> (0, 1) * amp[733] - Complex<double> (0, 1) * amp[731] +
      Complex<double> (0, 1) * amp[743] + Complex<double> (0, 1) * amp[742] -
      amp[774] + amp[776] - Complex<double> (0, 1) * amp[777] + Complex<double>
      (0, 1) * amp[779] - amp[780] + amp[782]);
  jamp[16] = +1./2. * (-Complex<double> (0, 1) * amp[507] - Complex<double> (0,
      1) * amp[510] + amp[511] - Complex<double> (0, 1) * amp[523] - amp[525] -
      Complex<double> (0, 1) * amp[526] + amp[529] + Complex<double> (0, 1) *
      amp[535] + Complex<double> (0, 1) * amp[534] + Complex<double> (0, 1) *
      amp[537] + Complex<double> (0, 1) * amp[538] - amp[543] + amp[545] +
      Complex<double> (0, 1) * amp[546] + amp[548] + Complex<double> (0, 1) *
      amp[560] + amp[562] + amp[564] + Complex<double> (0, 1) * amp[567] -
      amp[569] - amp[570] + amp[573] - amp[571] + Complex<double> (0, 1) *
      amp[582] + amp[622] + Complex<double> (0, 1) * amp[623] - amp[632] -
      amp[638] - amp[637] - amp[698] + amp[699] + amp[700] + amp[709] +
      Complex<double> (0, 1) * amp[710] - Complex<double> (0, 1) * amp[734] -
      Complex<double> (0, 1) * amp[735] + Complex<double> (0, 1) * amp[738] -
      Complex<double> (0, 1) * amp[736] + Complex<double> (0, 1) * amp[740] +
      Complex<double> (0, 1) * amp[744] + Complex<double> (0, 1) * amp[745] -
      Complex<double> (0, 1) * amp[747] - Complex<double> (0, 1) * amp[748] -
      Complex<double> (0, 1) * amp[752] - Complex<double> (0, 1) * amp[751] -
      Complex<double> (0, 1) * amp[762] - amp[765] + amp[767] + amp[768] +
      amp[769]);
  jamp[17] = +1./2. * (+1./3. * Complex<double> (0, 1) * amp[507] + 1./3. *
      Complex<double> (0, 1) * amp[508] + 1./3. * Complex<double> (0, 1) *
      amp[512] + 1./3. * Complex<double> (0, 1) * amp[513] - 1./3. * amp[545] -
      1./3. * amp[549] - 1./3. * amp[550] - 1./3. * amp[554] - 1./3. * amp[628]
      - 1./3. * amp[646]);
  jamp[18] = +1./2. * (-1./3. * amp[443] - 1./3. * amp[444] - 1./3. * amp[448]
      - 1./3. * amp[449] + 1./3. * Complex<double> (0, 1) * amp[517] + 1./3. *
      Complex<double> (0, 1) * amp[518] + 1./3. * Complex<double> (0, 1) *
      amp[522] + 1./3. * Complex<double> (0, 1) * amp[523] - 1./3. * amp[691] -
      1./3. * amp[709]);
  jamp[19] = +1./2. * (+amp[432] - Complex<double> (0, 1) * amp[433] + amp[434]
      + Complex<double> (0, 1) * amp[442] + amp[443] + amp[445] +
      Complex<double> (0, 1) * amp[459] - amp[462] + amp[463] - amp[466] -
      amp[465] + Complex<double> (0, 1) * amp[467] - Complex<double> (0, 1) *
      amp[513] - Complex<double> (0, 1) * amp[515] - amp[516] - Complex<double>
      (0, 1) * amp[517] + amp[520] - Complex<double> (0, 1) * amp[521] -
      amp[531] + Complex<double> (0, 1) * amp[535] + Complex<double> (0, 1) *
      amp[534] + Complex<double> (0, 1) * amp[537] + Complex<double> (0, 1) *
      amp[538] + amp[541] + amp[646] + Complex<double> (0, 1) * amp[647] +
      amp[652] + amp[656] - amp[654] - amp[678] - amp[683] - amp[682] +
      Complex<double> (0, 1) * amp[685] + amp[686] - amp[721] - Complex<double>
      (0, 1) * amp[722] + amp[724] + amp[726] + amp[727] - Complex<double> (0,
      1) * amp[729] + Complex<double> (0, 1) * amp[730] - Complex<double> (0,
      1) * amp[733] - Complex<double> (0, 1) * amp[732] - Complex<double> (0,
      1) * amp[739] - Complex<double> (0, 1) * amp[741] - Complex<double> (0,
      1) * amp[742] - Complex<double> (0, 1) * amp[749] - Complex<double> (0,
      1) * amp[748] - Complex<double> (0, 1) * amp[752] + Complex<double> (0,
      1) * amp[750]);
  jamp[20] = +1./2. * (-Complex<double> (0, 1) * amp[512] - Complex<double> (0,
      1) * amp[514] + amp[516] - amp[528] - amp[529] - Complex<double> (0, 1) *
      amp[530] - Complex<double> (0, 1) * amp[535] + Complex<double> (0, 1) *
      amp[533] + Complex<double> (0, 1) * amp[536] - Complex<double> (0, 1) *
      amp[538] - amp[539] + amp[550] + Complex<double> (0, 1) * amp[551] +
      amp[552] + Complex<double> (0, 1) * amp[556] + Complex<double> (0, 1) *
      amp[557] + amp[559] - Complex<double> (0, 1) * amp[567] + amp[568] +
      amp[569] + amp[572] + amp[571] - amp[650] + Complex<double> (0, 1) *
      amp[651] + amp[653] - amp[656] - amp[655] + amp[658] + Complex<double>
      (0, 1) * amp[665] - Complex<double> (0, 1) * amp[663] - Complex<double>
      (0, 1) * amp[666] + Complex<double> (0, 1) * amp[668] + amp[669] -
      amp[672] + Complex<double> (0, 1) * amp[674] + Complex<double> (0, 1) *
      amp[734] + Complex<double> (0, 1) * amp[737] + Complex<double> (0, 1) *
      amp[736] + Complex<double> (0, 1) * amp[752] + Complex<double> (0, 1) *
      amp[751] + amp[771] + amp[772] - Complex<double> (0, 1) * amp[777] -
      Complex<double> (0, 1) * amp[778] + amp[783] + amp[784]);
  jamp[21] = +1./2. * (+1./3. * Complex<double> (0, 1) * amp[524] + 1./3. *
      Complex<double> (0, 1) * amp[526] + 1./3. * amp[527] + 1./3. * amp[528] +
      1./3. * Complex<double> (0, 1) * amp[530] - 1./3. * Complex<double> (0,
      1) * amp[557] - 1./3. * Complex<double> (0, 1) * amp[558] - 1./3. *
      amp[561] - 1./3. * amp[562] - 1./3. * amp[568] - 1./3. * amp[657] - 1./3.
      * amp[658] - 1./3. * Complex<double> (0, 1) * amp[662] - 1./3. * amp[697]
      - 1./3. * amp[771] - 1./3. * amp[772] - 1./3. * amp[774] - 1./3. *
      amp[775]);
  jamp[22] = +1./2. * (-1./3. * amp[434] - 1./3. * amp[435] - 1./3. *
      Complex<double> (0, 1) * amp[438] - 1./3. * Complex<double> (0, 1) *
      amp[440] - 1./3. * amp[468] + 1./3. * Complex<double> (0, 1) * amp[514] +
      1./3. * Complex<double> (0, 1) * amp[515] + 1./3. * amp[539] + 1./3. *
      amp[540] + 1./3. * Complex<double> (0, 1) * amp[542] - 1./3. * amp[653] -
      1./3. * amp[669] - 1./3. * amp[670] - 1./3. * Complex<double> (0, 1) *
      amp[674] - 1./3. * amp[780] - 1./3. * amp[781] - 1./3. * amp[783] - 1./3.
      * amp[784]);
  jamp[23] = +1./2. * (+Complex<double> (0, 1) * amp[437] + Complex<double> (0,
      1) * amp[438] + amp[439] + amp[446] + Complex<double> (0, 1) * amp[447] +
      amp[448] + amp[462] + amp[465] + amp[464] - Complex<double> (0, 1) *
      amp[467] + amp[468] - Complex<double> (0, 1) * amp[522] - Complex<double>
      (0, 1) * amp[524] + amp[525] - amp[527] - Complex<double> (0, 1) *
      amp[534] - Complex<double> (0, 1) * amp[533] - Complex<double> (0, 1) *
      amp[536] - Complex<double> (0, 1) * amp[537] - amp[540] - amp[541] -
      Complex<double> (0, 1) * amp[542] + amp[657] - amp[660] + Complex<double>
      (0, 1) * amp[662] + Complex<double> (0, 1) * amp[664] + Complex<double>
      (0, 1) * amp[663] + Complex<double> (0, 1) * amp[666] + Complex<double>
      (0, 1) * amp[667] + amp[670] + Complex<double> (0, 1) * amp[695] -
      amp[696] + amp[697] - amp[701] - amp[700] + Complex<double> (0, 1) *
      amp[729] + Complex<double> (0, 1) * amp[732] + Complex<double> (0, 1) *
      amp[731] + Complex<double> (0, 1) * amp[749] + Complex<double> (0, 1) *
      amp[748] + amp[774] + amp[775] + Complex<double> (0, 1) * amp[777] +
      Complex<double> (0, 1) * amp[778] + amp[780] + amp[781]);
  jamp[24] = +1./2. * (+amp[393] - amp[402] + Complex<double> (0, 1) * amp[403]
      + Complex<double> (0, 1) * amp[406] + Complex<double> (0, 1) * amp[405] +
      Complex<double> (0, 1) * amp[408] + Complex<double> (0, 1) * amp[409] +
      Complex<double> (0, 1) * amp[423] + Complex<double> (0, 1) * amp[425] +
      amp[426] + amp[430] - Complex<double> (0, 1) * amp[508] - Complex<double>
      (0, 1) * amp[509] - amp[511] + amp[527] + amp[531] - Complex<double> (0,
      1) * amp[532] - Complex<double> (0, 1) * amp[535] + Complex<double> (0,
      1) * amp[533] + Complex<double> (0, 1) * amp[536] - Complex<double> (0,
      1) * amp[538] + amp[540] + amp[627] + amp[628] + Complex<double> (0, 1) *
      amp[629] + amp[630] - Complex<double> (0, 1) * amp[631] + amp[634] +
      amp[638] - amp[636] + Complex<double> (0, 1) * amp[722] + amp[723] -
      amp[724] + amp[728] - amp[726] + Complex<double> (0, 1) * amp[739] -
      Complex<double> (0, 1) * amp[743] + Complex<double> (0, 1) * amp[741] +
      Complex<double> (0, 1) * amp[752] - Complex<double> (0, 1) * amp[750] -
      amp[775] - amp[776] - Complex<double> (0, 1) * amp[778] - Complex<double>
      (0, 1) * amp[779] - amp[781] - amp[782]);
  jamp[25] = +1./2. * (+Complex<double> (0, 1) * amp[508] + Complex<double> (0,
      1) * amp[509] + amp[511] - amp[527] - amp[531] + Complex<double> (0, 1) *
      amp[532] + Complex<double> (0, 1) * amp[535] - Complex<double> (0, 1) *
      amp[533] - Complex<double> (0, 1) * amp[536] + Complex<double> (0, 1) *
      amp[538] - amp[540] + amp[583] - Complex<double> (0, 1) * amp[584] +
      amp[585] - Complex<double> (0, 1) * amp[593] - Complex<double> (0, 1) *
      amp[595] + amp[597] + Complex<double> (0, 1) * amp[605] + amp[606] +
      amp[607] + amp[610] + amp[609] - amp[632] - Complex<double> (0, 1) *
      amp[633] + amp[635] - amp[638] - amp[637] + amp[657] - Complex<double>
      (0, 1) * amp[665] + Complex<double> (0, 1) * amp[663] + Complex<double>
      (0, 1) * amp[666] - Complex<double> (0, 1) * amp[668] + amp[670] -
      amp[671] - Complex<double> (0, 1) * amp[673] - Complex<double> (0, 1) *
      amp[734] - Complex<double> (0, 1) * amp[737] - Complex<double> (0, 1) *
      amp[736] - Complex<double> (0, 1) * amp[752] - Complex<double> (0, 1) *
      amp[751] + amp[774] + amp[775] + Complex<double> (0, 1) * amp[777] +
      Complex<double> (0, 1) * amp[778] + amp[780] + amp[781]);
  jamp[26] = +1./2. * (+amp[393] - amp[397] - Complex<double> (0, 1) * amp[398]
      + Complex<double> (0, 1) * amp[407] + Complex<double> (0, 1) * amp[406] +
      Complex<double> (0, 1) * amp[409] + Complex<double> (0, 1) * amp[410] -
      Complex<double> (0, 1) * amp[412] - Complex<double> (0, 1) * amp[413] -
      amp[415] + amp[430] + Complex<double> (0, 1) * amp[522] + Complex<double>
      (0, 1) * amp[524] - amp[525] + amp[527] + Complex<double> (0, 1) *
      amp[534] + Complex<double> (0, 1) * amp[533] + Complex<double> (0, 1) *
      amp[536] + Complex<double> (0, 1) * amp[537] + amp[540] + amp[541] +
      Complex<double> (0, 1) * amp[542] - Complex<double> (0, 1) * amp[693] +
      amp[694] - amp[698] + amp[699] + amp[700] + amp[705] + amp[706] -
      Complex<double> (0, 1) * amp[707] + Complex<double> (0, 1) * amp[740] -
      Complex<double> (0, 1) * amp[746] + Complex<double> (0, 1) * amp[744] -
      Complex<double> (0, 1) * amp[747] - Complex<double> (0, 1) * amp[748] -
      Complex<double> (0, 1) * amp[753] + amp[754] - amp[758] + amp[761] -
      amp[759] - amp[775] - amp[776] - Complex<double> (0, 1) * amp[778] -
      Complex<double> (0, 1) * amp[779] - amp[781] - amp[782]);
  jamp[27] = +1./2. * (-amp[393] + amp[397] + Complex<double> (0, 1) * amp[398]
      - Complex<double> (0, 1) * amp[407] - Complex<double> (0, 1) * amp[406] -
      Complex<double> (0, 1) * amp[409] - Complex<double> (0, 1) * amp[410] +
      Complex<double> (0, 1) * amp[412] + Complex<double> (0, 1) * amp[413] +
      amp[415] - amp[430] + Complex<double> (0, 1) * amp[593] + Complex<double>
      (0, 1) * amp[595] - amp[597] - Complex<double> (0, 1) * amp[603] +
      amp[604] + amp[608] - amp[611] - amp[610] + amp[615] + amp[616] +
      Complex<double> (0, 1) * amp[617] - amp[657] + Complex<double> (0, 1) *
      amp[665] - Complex<double> (0, 1) * amp[663] - Complex<double> (0, 1) *
      amp[666] + Complex<double> (0, 1) * amp[668] - amp[670] + amp[671] +
      Complex<double> (0, 1) * amp[673] - Complex<double> (0, 1) * amp[735] +
      Complex<double> (0, 1) * amp[738] + Complex<double> (0, 1) * amp[737] +
      Complex<double> (0, 1) * amp[746] + Complex<double> (0, 1) * amp[745] -
      Complex<double> (0, 1) * amp[755] + amp[756] + amp[757] - amp[761] -
      amp[760] - amp[774] + amp[776] - Complex<double> (0, 1) * amp[777] +
      Complex<double> (0, 1) * amp[779] - amp[780] + amp[782]);
  jamp[28] = +1./2. * (-1./3. * Complex<double> (0, 1) * amp[418] - 1./3. *
      Complex<double> (0, 1) * amp[419] - 1./3. * Complex<double> (0, 1) *
      amp[425] - 1./3. * amp[429] - 1./3. * amp[430] + 1./3. * Complex<double>
      (0, 1) * amp[509] + 1./3. * Complex<double> (0, 1) * amp[510] - 1./3. *
      amp[539] - 1./3. * amp[540] + 1./3. * Complex<double> (0, 1) * amp[544] -
      1./3. * amp[621] - 1./3. * amp[622] - 1./3. * amp[630] - 1./3. * amp[763]
      + 1./3. * amp[781] + 1./3. * amp[782] + 1./3. * amp[784] + 1./3. *
      amp[785]);
  jamp[29] = +1./2. * (-1./3. * amp[472] - 1./3. * amp[473] + 1./3. *
      Complex<double> (0, 1) * amp[476] + 1./3. * Complex<double> (0, 1) *
      amp[478] - 1./3. * amp[506] - 1./3. * Complex<double> (0, 1) * amp[509] -
      1./3. * Complex<double> (0, 1) * amp[510] + 1./3. * amp[539] + 1./3. *
      amp[540] - 1./3. * Complex<double> (0, 1) * amp[544] - 1./3. * amp[635] -
      1./3. * amp[669] - 1./3. * amp[670] + 1./3. * Complex<double> (0, 1) *
      amp[673] - 1./3. * amp[780] - 1./3. * amp[781] - 1./3. * amp[783] - 1./3.
      * amp[784]);
  jamp[30] = +1./2. * (+1./3. * Complex<double> (0, 1) * amp[413] + 1./3. *
      Complex<double> (0, 1) * amp[414] + 1./3. * Complex<double> (0, 1) *
      amp[427] - 1./3. * amp[429] - 1./3. * amp[430] - 1./3. * Complex<double>
      (0, 1) * amp[514] - 1./3. * Complex<double> (0, 1) * amp[515] - 1./3. *
      amp[539] - 1./3. * amp[540] - 1./3. * Complex<double> (0, 1) * amp[542] -
      1./3. * amp[639] - 1./3. * amp[640] - 1./3. * amp[648] - 1./3. * amp[754]
      + 1./3. * amp[781] + 1./3. * amp[782] + 1./3. * amp[784] + 1./3. *
      amp[785]);
  jamp[31] = +1./2. * (-1./3. * Complex<double> (0, 1) * amp[413] - 1./3. *
      Complex<double> (0, 1) * amp[414] - 1./3. * Complex<double> (0, 1) *
      amp[427] + 1./3. * amp[429] + 1./3. * amp[430] - 1./3. * Complex<double>
      (0, 1) * amp[476] - 1./3. * Complex<double> (0, 1) * amp[478] - 1./3. *
      amp[489] - 1./3. * amp[491] - 1./3. * amp[498] + 1./3. * amp[669] + 1./3.
      * amp[670] - 1./3. * Complex<double> (0, 1) * amp[673] - 1./3. * amp[757]
      + 1./3. * amp[780] - 1./3. * amp[782] + 1./3. * amp[783] - 1./3. *
      amp[785]);
  jamp[32] = +1./2. * (-1./3. * amp[393] - 1./3. * amp[394] - 1./3. *
      Complex<double> (0, 1) * amp[396] - 1./3. * Complex<double> (0, 1) *
      amp[403] - 1./3. * Complex<double> (0, 1) * amp[404] + 1./3. *
      Complex<double> (0, 1) * amp[519] + 1./3. * Complex<double> (0, 1) *
      amp[521] - 1./3. * amp[527] - 1./3. * amp[528] + 1./3. * Complex<double>
      (0, 1) * amp[532] - 1./3. * amp[676] - 1./3. * amp[684] - 1./3. *
      amp[686] - 1./3. * amp[723] + 1./3. * amp[772] + 1./3. * amp[773] + 1./3.
      * amp[775] + 1./3. * amp[776]);
  jamp[33] = +1./2. * (-1./3. * Complex<double> (0, 1) * amp[519] - 1./3. *
      Complex<double> (0, 1) * amp[521] + 1./3. * amp[527] + 1./3. * amp[528] -
      1./3. * Complex<double> (0, 1) * amp[532] + 1./3. * Complex<double> (0,
      1) * amp[595] + 1./3. * Complex<double> (0, 1) * amp[596] - 1./3. *
      amp[599] - 1./3. * amp[600] - 1./3. * amp[606] - 1./3. * amp[657] - 1./3.
      * amp[658] + 1./3. * Complex<double> (0, 1) * amp[661] - 1./3. * amp[679]
      - 1./3. * amp[771] - 1./3. * amp[772] - 1./3. * amp[774] - 1./3. *
      amp[775]);
  jamp[34] = +1./2. * (-1./3. * amp[393] - 1./3. * amp[394] + 1./3. *
      Complex<double> (0, 1) * amp[398] + 1./3. * Complex<double> (0, 1) *
      amp[400] + 1./3. * Complex<double> (0, 1) * amp[401] - 1./3. *
      Complex<double> (0, 1) * amp[524] - 1./3. * Complex<double> (0, 1) *
      amp[526] - 1./3. * amp[527] - 1./3. * amp[528] - 1./3. * Complex<double>
      (0, 1) * amp[530] - 1./3. * amp[694] - 1./3. * amp[702] - 1./3. *
      amp[704] - 1./3. * amp[714] + 1./3. * amp[772] + 1./3. * amp[773] + 1./3.
      * amp[775] + 1./3. * amp[776]);
  jamp[35] = +1./2. * (+1./3. * amp[393] + 1./3. * amp[394] - 1./3. *
      Complex<double> (0, 1) * amp[398] - 1./3. * Complex<double> (0, 1) *
      amp[400] - 1./3. * Complex<double> (0, 1) * amp[401] - 1./3. *
      Complex<double> (0, 1) * amp[595] - 1./3. * Complex<double> (0, 1) *
      amp[596] - 1./3. * amp[604] - 1./3. * amp[612] - 1./3. * amp[613] + 1./3.
      * amp[657] + 1./3. * amp[658] - 1./3. * Complex<double> (0, 1) * amp[661]
      - 1./3. * amp[716] + 1./3. * amp[771] - 1./3. * amp[773] + 1./3. *
      amp[774] - 1./3. * amp[776]);
  jamp[36] = +1./2. * (+amp[394] - amp[395] + Complex<double> (0, 1) * amp[396]
      - Complex<double> (0, 1) * amp[407] - Complex<double> (0, 1) * amp[406] -
      Complex<double> (0, 1) * amp[409] - Complex<double> (0, 1) * amp[410] +
      Complex<double> (0, 1) * amp[416] + Complex<double> (0, 1) * amp[419] -
      amp[420] + amp[429] - Complex<double> (0, 1) * amp[518] - Complex<double>
      (0, 1) * amp[519] - amp[520] + amp[528] - Complex<double> (0, 1) *
      amp[534] - Complex<double> (0, 1) * amp[533] - Complex<double> (0, 1) *
      amp[536] - Complex<double> (0, 1) * amp[537] + amp[539] + amp[543] -
      Complex<double> (0, 1) * amp[544] + Complex<double> (0, 1) * amp[675] +
      amp[676] - amp[680] + amp[681] + amp[682] + amp[690] + amp[691] +
      Complex<double> (0, 1) * amp[692] - Complex<double> (0, 1) * amp[740] +
      Complex<double> (0, 1) * amp[746] - Complex<double> (0, 1) * amp[744] +
      Complex<double> (0, 1) * amp[747] + Complex<double> (0, 1) * amp[748] +
      Complex<double> (0, 1) * amp[762] + amp[763] - amp[767] + amp[770] -
      amp[768] - amp[772] - amp[773] + Complex<double> (0, 1) * amp[778] +
      Complex<double> (0, 1) * amp[779] - amp[784] - amp[785]);
  jamp[37] = +1./2. * (-Complex<double> (0, 1) * amp[474] - Complex<double> (0,
      1) * amp[476] + amp[477] + amp[479] - Complex<double> (0, 1) * amp[480] +
      amp[481] + amp[500] + amp[503] + amp[502] + Complex<double> (0, 1) *
      amp[505] + amp[506] + Complex<double> (0, 1) * amp[518] + Complex<double>
      (0, 1) * amp[519] + amp[520] - amp[528] + Complex<double> (0, 1) *
      amp[534] + Complex<double> (0, 1) * amp[533] + Complex<double> (0, 1) *
      amp[536] + Complex<double> (0, 1) * amp[537] - amp[539] - amp[543] +
      Complex<double> (0, 1) * amp[544] + amp[658] - amp[659] - Complex<double>
      (0, 1) * amp[661] - Complex<double> (0, 1) * amp[664] - Complex<double>
      (0, 1) * amp[663] - Complex<double> (0, 1) * amp[666] - Complex<double>
      (0, 1) * amp[667] + amp[669] - Complex<double> (0, 1) * amp[677] -
      amp[678] + amp[679] - amp[683] - amp[682] - Complex<double> (0, 1) *
      amp[729] - Complex<double> (0, 1) * amp[732] - Complex<double> (0, 1) *
      amp[731] - Complex<double> (0, 1) * amp[749] - Complex<double> (0, 1) *
      amp[748] + amp[771] + amp[772] - Complex<double> (0, 1) * amp[777] -
      Complex<double> (0, 1) * amp[778] + amp[783] + amp[784]);
  jamp[38] = +1./2. * (+amp[394] - amp[399] - Complex<double> (0, 1) * amp[400]
      - Complex<double> (0, 1) * amp[406] - Complex<double> (0, 1) * amp[405] -
      Complex<double> (0, 1) * amp[408] - Complex<double> (0, 1) * amp[409] -
      Complex<double> (0, 1) * amp[422] - Complex<double> (0, 1) * amp[427] +
      amp[428] + amp[429] + Complex<double> (0, 1) * amp[512] + Complex<double>
      (0, 1) * amp[514] - amp[516] + amp[528] + amp[529] + Complex<double> (0,
      1) * amp[530] + Complex<double> (0, 1) * amp[535] - Complex<double> (0,
      1) * amp[533] - Complex<double> (0, 1) * amp[536] + Complex<double> (0,
      1) * amp[538] + amp[539] + amp[642] + amp[643] - Complex<double> (0, 1) *
      amp[644] + amp[648] + Complex<double> (0, 1) * amp[649] + amp[652] +
      amp[656] - amp[654] - Complex<double> (0, 1) * amp[713] + amp[714] -
      amp[715] + amp[719] - amp[717] - Complex<double> (0, 1) * amp[739] +
      Complex<double> (0, 1) * amp[743] - Complex<double> (0, 1) * amp[741] -
      Complex<double> (0, 1) * amp[752] + Complex<double> (0, 1) * amp[750] -
      amp[772] - amp[773] + Complex<double> (0, 1) * amp[778] + Complex<double>
      (0, 1) * amp[779] - amp[784] - amp[785]);
  jamp[39] = +1./2. * (-amp[394] + amp[399] + Complex<double> (0, 1) * amp[400]
      + Complex<double> (0, 1) * amp[406] + Complex<double> (0, 1) * amp[405] +
      Complex<double> (0, 1) * amp[408] + Complex<double> (0, 1) * amp[409] +
      Complex<double> (0, 1) * amp[422] + Complex<double> (0, 1) * amp[427] -
      amp[428] - amp[429] + Complex<double> (0, 1) * amp[474] + Complex<double>
      (0, 1) * amp[476] - amp[477] + amp[492] + amp[493] + Complex<double> (0,
      1) * amp[494] + amp[498] + Complex<double> (0, 1) * amp[499] - amp[501] +
      amp[504] - amp[502] - amp[658] + amp[659] + Complex<double> (0, 1) *
      amp[661] + Complex<double> (0, 1) * amp[664] + Complex<double> (0, 1) *
      amp[663] + Complex<double> (0, 1) * amp[666] + Complex<double> (0, 1) *
      amp[667] - amp[669] - Complex<double> (0, 1) * amp[711] + amp[712] +
      amp[716] - amp[719] - amp[718] + Complex<double> (0, 1) * amp[730] -
      Complex<double> (0, 1) * amp[733] + Complex<double> (0, 1) * amp[731] -
      Complex<double> (0, 1) * amp[743] - Complex<double> (0, 1) * amp[742] -
      amp[771] + amp[773] + Complex<double> (0, 1) * amp[777] - Complex<double>
      (0, 1) * amp[779] - amp[783] + amp[785]);
  jamp[40] = +1./2. * (+amp[395] + amp[402] + Complex<double> (0, 1) * amp[404]
      + Complex<double> (0, 1) * amp[407] - Complex<double> (0, 1) * amp[405] -
      Complex<double> (0, 1) * amp[408] + Complex<double> (0, 1) * amp[410] +
      Complex<double> (0, 1) * amp[411] + Complex<double> (0, 1) * amp[414] -
      amp[415] + Complex<double> (0, 1) * amp[424] + amp[428] + Complex<double>
      (0, 1) * amp[480] + amp[482] + amp[483] + amp[489] + Complex<double> (0,
      1) * amp[490] + amp[495] + Complex<double> (0, 1) * amp[497] -
      Complex<double> (0, 1) * amp[499] - amp[500] + amp[501] - amp[504] -
      amp[503] - Complex<double> (0, 1) * amp[675] + amp[678] + amp[680] +
      amp[683] - amp[681] + amp[684] - Complex<double> (0, 1) * amp[685] +
      amp[687] + Complex<double> (0, 1) * amp[689] + amp[721] - amp[728] -
      amp[727] + Complex<double> (0, 1) * amp[729] - Complex<double> (0, 1) *
      amp[730] + Complex<double> (0, 1) * amp[733] + Complex<double> (0, 1) *
      amp[732] + Complex<double> (0, 1) * amp[740] + Complex<double> (0, 1) *
      amp[743] + Complex<double> (0, 1) * amp[742] - Complex<double> (0, 1) *
      amp[746] + Complex<double> (0, 1) * amp[744] + Complex<double> (0, 1) *
      amp[749] - Complex<double> (0, 1) * amp[747] - amp[758] + amp[761] -
      amp[759]);
  jamp[41] = +1./2. * (-1./3. * Complex<double> (0, 1) * amp[411] - 1./3. *
      Complex<double> (0, 1) * amp[412] - 1./3. * Complex<double> (0, 1) *
      amp[416] - 1./3. * Complex<double> (0, 1) * amp[417] - 1./3. * amp[601] -
      1./3. * amp[602] - 1./3. * amp[615] - 1./3. * amp[618] - 1./3. * amp[687]
      - 1./3. * amp[690]);
  jamp[42] = +1./2. * (-1./3. * Complex<double> (0, 1) * amp[421] - 1./3. *
      Complex<double> (0, 1) * amp[422] - 1./3. * Complex<double> (0, 1) *
      amp[423] - 1./3. * Complex<double> (0, 1) * amp[424] - 1./3. * amp[469] -
      1./3. * amp[470] - 1./3. * amp[492] - 1./3. * amp[495] - 1./3. * amp[624]
      - 1./3. * amp[627]);
  jamp[43] = +1./2. * (-amp[397] - amp[399] + Complex<double> (0, 1) * amp[401]
      + Complex<double> (0, 1) * amp[407] - Complex<double> (0, 1) * amp[405] -
      Complex<double> (0, 1) * amp[408] + Complex<double> (0, 1) * amp[410] +
      Complex<double> (0, 1) * amp[417] + Complex<double> (0, 1) * amp[418] +
      amp[420] + Complex<double> (0, 1) * amp[421] - amp[426] + Complex<double>
      (0, 1) * amp[584] + amp[586] + amp[587] + Complex<double> (0, 1) *
      amp[603] - amp[607] - amp[608] + amp[611] - amp[609] + amp[612] +
      Complex<double> (0, 1) * amp[614] + amp[618] + Complex<double> (0, 1) *
      amp[620] + amp[621] - Complex<double> (0, 1) * amp[623] + amp[624] +
      Complex<double> (0, 1) * amp[626] + Complex<double> (0, 1) * amp[631] +
      amp[632] - amp[634] + amp[636] + amp[637] - amp[715] + amp[719] -
      amp[717] + Complex<double> (0, 1) * amp[734] + Complex<double> (0, 1) *
      amp[735] - Complex<double> (0, 1) * amp[738] + Complex<double> (0, 1) *
      amp[736] - Complex<double> (0, 1) * amp[739] + Complex<double> (0, 1) *
      amp[743] - Complex<double> (0, 1) * amp[741] - Complex<double> (0, 1) *
      amp[746] - Complex<double> (0, 1) * amp[745] + Complex<double> (0, 1) *
      amp[750] + Complex<double> (0, 1) * amp[751] + amp[765] - amp[770] -
      amp[769]);
  jamp[44] = +1./2. * (+Complex<double> (0, 1) * amp[513] + Complex<double> (0,
      1) * amp[515] + amp[516] + Complex<double> (0, 1) * amp[517] - amp[520] +
      Complex<double> (0, 1) * amp[521] + amp[531] - Complex<double> (0, 1) *
      amp[535] - Complex<double> (0, 1) * amp[534] - Complex<double> (0, 1) *
      amp[537] - Complex<double> (0, 1) * amp[538] - amp[541] + amp[588] -
      Complex<double> (0, 1) * amp[589] + amp[591] - Complex<double> (0, 1) *
      amp[598] + amp[600] + amp[601] - Complex<double> (0, 1) * amp[605] -
      amp[607] - amp[608] + amp[611] - amp[609] - Complex<double> (0, 1) *
      amp[617] + amp[640] - Complex<double> (0, 1) * amp[641] - amp[650] -
      amp[656] - amp[655] - amp[680] + amp[681] + amp[682] + amp[688] -
      Complex<double> (0, 1) * amp[689] + Complex<double> (0, 1) * amp[734] +
      Complex<double> (0, 1) * amp[735] - Complex<double> (0, 1) * amp[738] +
      Complex<double> (0, 1) * amp[736] - Complex<double> (0, 1) * amp[740] -
      Complex<double> (0, 1) * amp[744] - Complex<double> (0, 1) * amp[745] +
      Complex<double> (0, 1) * amp[747] + Complex<double> (0, 1) * amp[748] +
      Complex<double> (0, 1) * amp[752] + Complex<double> (0, 1) * amp[751] +
      Complex<double> (0, 1) * amp[753] - amp[756] + amp[758] + amp[759] +
      amp[760]);
  jamp[45] = +1./2. * (-1./3. * amp[481] - 1./3. * amp[482] - 1./3. * amp[486]
      - 1./3. * amp[487] - 1./3. * Complex<double> (0, 1) * amp[517] - 1./3. *
      Complex<double> (0, 1) * amp[518] - 1./3. * Complex<double> (0, 1) *
      amp[522] - 1./3. * Complex<double> (0, 1) * amp[523] - 1./3. * amp[688] -
      1./3. * amp[706]);
  jamp[46] = +1./2. * (-1./3. * Complex<double> (0, 1) * amp[507] - 1./3. *
      Complex<double> (0, 1) * amp[508] - 1./3. * Complex<double> (0, 1) *
      amp[512] - 1./3. * Complex<double> (0, 1) * amp[513] - 1./3. * amp[583] -
      1./3. * amp[587] - 1./3. * amp[588] - 1./3. * amp[592] - 1./3. * amp[625]
      - 1./3. * amp[643]);
  jamp[47] = +1./2. * (+amp[469] + Complex<double> (0, 1) * amp[471] + amp[472]
      - Complex<double> (0, 1) * amp[485] + amp[486] + amp[488] -
      Complex<double> (0, 1) * amp[494] - amp[500] + amp[501] - amp[504] -
      amp[503] - Complex<double> (0, 1) * amp[505] + Complex<double> (0, 1) *
      amp[507] + Complex<double> (0, 1) * amp[510] - amp[511] + Complex<double>
      (0, 1) * amp[523] + amp[525] + Complex<double> (0, 1) * amp[526] -
      amp[529] - Complex<double> (0, 1) * amp[535] - Complex<double> (0, 1) *
      amp[534] - Complex<double> (0, 1) * amp[537] - Complex<double> (0, 1) *
      amp[538] + amp[543] + amp[625] - Complex<double> (0, 1) * amp[626] +
      amp[634] + amp[638] - amp[636] - amp[696] - amp[701] - amp[700] -
      Complex<double> (0, 1) * amp[703] + amp[704] - amp[712] + Complex<double>
      (0, 1) * amp[713] + amp[715] + amp[717] + amp[718] + Complex<double> (0,
      1) * amp[729] - Complex<double> (0, 1) * amp[730] + Complex<double> (0,
      1) * amp[733] + Complex<double> (0, 1) * amp[732] + Complex<double> (0,
      1) * amp[739] + Complex<double> (0, 1) * amp[741] + Complex<double> (0,
      1) * amp[742] + Complex<double> (0, 1) * amp[749] + Complex<double> (0,
      1) * amp[748] + Complex<double> (0, 1) * amp[752] - Complex<double> (0,
      1) * amp[750]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[1][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}


}  // end namespace PY8MEs_namespace

