//==========================================================================
// This file has been automatically generated for Pythia 8 by
// MadGraph5_aMC@NLO v. 2.6.0, 2017-08-16
// By the MadGraph5_aMC@NLO Development Team
// Visit launchpad.net/madgraph5 and amcatnlo.web.cern.ch
//==========================================================================

#include "PY8MEs_R4_P123_sm_qb_llgqb.h"
#include "HelAmps_sm.h"

using namespace Pythia8_sm; 

namespace PY8MEs_namespace 
{
//==========================================================================
// Class member functions for calculating the matrix elements for
// Process: u b > e+ e- g u b WEIGHTED<=7 @4
// Process: u b > mu+ mu- g u b WEIGHTED<=7 @4
// Process: c b > e+ e- g c b WEIGHTED<=7 @4
// Process: c b > mu+ mu- g c b WEIGHTED<=7 @4
// Process: u b~ > e+ e- g u b~ WEIGHTED<=7 @4
// Process: u b~ > mu+ mu- g u b~ WEIGHTED<=7 @4
// Process: c b~ > e+ e- g c b~ WEIGHTED<=7 @4
// Process: c b~ > mu+ mu- g c b~ WEIGHTED<=7 @4
// Process: d b > e+ e- g d b WEIGHTED<=7 @4
// Process: d b > mu+ mu- g d b WEIGHTED<=7 @4
// Process: s b > e+ e- g s b WEIGHTED<=7 @4
// Process: s b > mu+ mu- g s b WEIGHTED<=7 @4
// Process: d b~ > e+ e- g d b~ WEIGHTED<=7 @4
// Process: d b~ > mu+ mu- g d b~ WEIGHTED<=7 @4
// Process: s b~ > e+ e- g s b~ WEIGHTED<=7 @4
// Process: s b~ > mu+ mu- g s b~ WEIGHTED<=7 @4
// Process: u~ b > e+ e- g u~ b WEIGHTED<=7 @4
// Process: u~ b > mu+ mu- g u~ b WEIGHTED<=7 @4
// Process: c~ b > e+ e- g c~ b WEIGHTED<=7 @4
// Process: c~ b > mu+ mu- g c~ b WEIGHTED<=7 @4
// Process: u~ b~ > e+ e- g u~ b~ WEIGHTED<=7 @4
// Process: u~ b~ > mu+ mu- g u~ b~ WEIGHTED<=7 @4
// Process: c~ b~ > e+ e- g c~ b~ WEIGHTED<=7 @4
// Process: c~ b~ > mu+ mu- g c~ b~ WEIGHTED<=7 @4
// Process: d~ b > e+ e- g d~ b WEIGHTED<=7 @4
// Process: d~ b > mu+ mu- g d~ b WEIGHTED<=7 @4
// Process: s~ b > e+ e- g s~ b WEIGHTED<=7 @4
// Process: s~ b > mu+ mu- g s~ b WEIGHTED<=7 @4
// Process: d~ b~ > e+ e- g d~ b~ WEIGHTED<=7 @4
// Process: d~ b~ > mu+ mu- g d~ b~ WEIGHTED<=7 @4
// Process: s~ b~ > e+ e- g s~ b~ WEIGHTED<=7 @4
// Process: s~ b~ > mu+ mu- g s~ b~ WEIGHTED<=7 @4
// Process: u b > ve~ ve g u b WEIGHTED<=7 @4
// Process: u b > vm~ vm g u b WEIGHTED<=7 @4
// Process: u b > vt~ vt g u b WEIGHTED<=7 @4
// Process: c b > ve~ ve g c b WEIGHTED<=7 @4
// Process: c b > vm~ vm g c b WEIGHTED<=7 @4
// Process: c b > vt~ vt g c b WEIGHTED<=7 @4
// Process: u b~ > ve~ ve g u b~ WEIGHTED<=7 @4
// Process: u b~ > vm~ vm g u b~ WEIGHTED<=7 @4
// Process: u b~ > vt~ vt g u b~ WEIGHTED<=7 @4
// Process: c b~ > ve~ ve g c b~ WEIGHTED<=7 @4
// Process: c b~ > vm~ vm g c b~ WEIGHTED<=7 @4
// Process: c b~ > vt~ vt g c b~ WEIGHTED<=7 @4
// Process: d b > ve~ ve g d b WEIGHTED<=7 @4
// Process: d b > vm~ vm g d b WEIGHTED<=7 @4
// Process: d b > vt~ vt g d b WEIGHTED<=7 @4
// Process: s b > ve~ ve g s b WEIGHTED<=7 @4
// Process: s b > vm~ vm g s b WEIGHTED<=7 @4
// Process: s b > vt~ vt g s b WEIGHTED<=7 @4
// Process: d b~ > ve~ ve g d b~ WEIGHTED<=7 @4
// Process: d b~ > vm~ vm g d b~ WEIGHTED<=7 @4
// Process: d b~ > vt~ vt g d b~ WEIGHTED<=7 @4
// Process: s b~ > ve~ ve g s b~ WEIGHTED<=7 @4
// Process: s b~ > vm~ vm g s b~ WEIGHTED<=7 @4
// Process: s b~ > vt~ vt g s b~ WEIGHTED<=7 @4
// Process: u~ b > ve~ ve g u~ b WEIGHTED<=7 @4
// Process: u~ b > vm~ vm g u~ b WEIGHTED<=7 @4
// Process: u~ b > vt~ vt g u~ b WEIGHTED<=7 @4
// Process: c~ b > ve~ ve g c~ b WEIGHTED<=7 @4
// Process: c~ b > vm~ vm g c~ b WEIGHTED<=7 @4
// Process: c~ b > vt~ vt g c~ b WEIGHTED<=7 @4
// Process: u~ b~ > ve~ ve g u~ b~ WEIGHTED<=7 @4
// Process: u~ b~ > vm~ vm g u~ b~ WEIGHTED<=7 @4
// Process: u~ b~ > vt~ vt g u~ b~ WEIGHTED<=7 @4
// Process: c~ b~ > ve~ ve g c~ b~ WEIGHTED<=7 @4
// Process: c~ b~ > vm~ vm g c~ b~ WEIGHTED<=7 @4
// Process: c~ b~ > vt~ vt g c~ b~ WEIGHTED<=7 @4
// Process: d~ b > ve~ ve g d~ b WEIGHTED<=7 @4
// Process: d~ b > vm~ vm g d~ b WEIGHTED<=7 @4
// Process: d~ b > vt~ vt g d~ b WEIGHTED<=7 @4
// Process: s~ b > ve~ ve g s~ b WEIGHTED<=7 @4
// Process: s~ b > vm~ vm g s~ b WEIGHTED<=7 @4
// Process: s~ b > vt~ vt g s~ b WEIGHTED<=7 @4
// Process: d~ b~ > ve~ ve g d~ b~ WEIGHTED<=7 @4
// Process: d~ b~ > vm~ vm g d~ b~ WEIGHTED<=7 @4
// Process: d~ b~ > vt~ vt g d~ b~ WEIGHTED<=7 @4
// Process: s~ b~ > ve~ ve g s~ b~ WEIGHTED<=7 @4
// Process: s~ b~ > vm~ vm g s~ b~ WEIGHTED<=7 @4
// Process: s~ b~ > vt~ vt g s~ b~ WEIGHTED<=7 @4
// Process: u b > e+ ve g d b WEIGHTED<=7 @4
// Process: u b > mu+ vm g d b WEIGHTED<=7 @4
// Process: c b > e+ ve g s b WEIGHTED<=7 @4
// Process: c b > mu+ vm g s b WEIGHTED<=7 @4
// Process: u b~ > e+ ve g d b~ WEIGHTED<=7 @4
// Process: u b~ > mu+ vm g d b~ WEIGHTED<=7 @4
// Process: c b~ > e+ ve g s b~ WEIGHTED<=7 @4
// Process: c b~ > mu+ vm g s b~ WEIGHTED<=7 @4
// Process: d b > ve~ e- g u b WEIGHTED<=7 @4
// Process: d b > vm~ mu- g u b WEIGHTED<=7 @4
// Process: s b > ve~ e- g c b WEIGHTED<=7 @4
// Process: s b > vm~ mu- g c b WEIGHTED<=7 @4
// Process: d b~ > ve~ e- g u b~ WEIGHTED<=7 @4
// Process: d b~ > vm~ mu- g u b~ WEIGHTED<=7 @4
// Process: s b~ > ve~ e- g c b~ WEIGHTED<=7 @4
// Process: s b~ > vm~ mu- g c b~ WEIGHTED<=7 @4
// Process: u~ b > ve~ e- g d~ b WEIGHTED<=7 @4
// Process: u~ b > vm~ mu- g d~ b WEIGHTED<=7 @4
// Process: c~ b > ve~ e- g s~ b WEIGHTED<=7 @4
// Process: c~ b > vm~ mu- g s~ b WEIGHTED<=7 @4
// Process: u~ b~ > ve~ e- g d~ b~ WEIGHTED<=7 @4
// Process: u~ b~ > vm~ mu- g d~ b~ WEIGHTED<=7 @4
// Process: c~ b~ > ve~ e- g s~ b~ WEIGHTED<=7 @4
// Process: c~ b~ > vm~ mu- g s~ b~ WEIGHTED<=7 @4
// Process: d~ b > e+ ve g u~ b WEIGHTED<=7 @4
// Process: d~ b > mu+ vm g u~ b WEIGHTED<=7 @4
// Process: s~ b > e+ ve g c~ b WEIGHTED<=7 @4
// Process: s~ b > mu+ vm g c~ b WEIGHTED<=7 @4
// Process: d~ b~ > e+ ve g u~ b~ WEIGHTED<=7 @4
// Process: d~ b~ > mu+ vm g u~ b~ WEIGHTED<=7 @4
// Process: s~ b~ > e+ ve g c~ b~ WEIGHTED<=7 @4
// Process: s~ b~ > mu+ vm g c~ b~ WEIGHTED<=7 @4

// Exception class
class PY8MEs_R4_P123_sm_qb_llgqbException : public exception
{
  virtual const char * what() const throw()
  {
    return "Exception in class 'PY8MEs_R4_P123_sm_qb_llgqb'."; 
  }
}
PY8MEs_R4_P123_sm_qb_llgqb_exception; 

std::set<int> PY8MEs_R4_P123_sm_qb_llgqb::s_channel_proc = std::set<int>
    (createset<int> ());

int PY8MEs_R4_P123_sm_qb_llgqb::helicities[ncomb][nexternal] = {{-1, -1, -1,
    -1, -1, -1, -1}, {-1, -1, -1, -1, -1, -1, 1}, {-1, -1, -1, -1, -1, 1, -1},
    {-1, -1, -1, -1, -1, 1, 1}, {-1, -1, -1, -1, 1, -1, -1}, {-1, -1, -1, -1,
    1, -1, 1}, {-1, -1, -1, -1, 1, 1, -1}, {-1, -1, -1, -1, 1, 1, 1}, {-1, -1,
    -1, 1, -1, -1, -1}, {-1, -1, -1, 1, -1, -1, 1}, {-1, -1, -1, 1, -1, 1, -1},
    {-1, -1, -1, 1, -1, 1, 1}, {-1, -1, -1, 1, 1, -1, -1}, {-1, -1, -1, 1, 1,
    -1, 1}, {-1, -1, -1, 1, 1, 1, -1}, {-1, -1, -1, 1, 1, 1, 1}, {-1, -1, 1,
    -1, -1, -1, -1}, {-1, -1, 1, -1, -1, -1, 1}, {-1, -1, 1, -1, -1, 1, -1},
    {-1, -1, 1, -1, -1, 1, 1}, {-1, -1, 1, -1, 1, -1, -1}, {-1, -1, 1, -1, 1,
    -1, 1}, {-1, -1, 1, -1, 1, 1, -1}, {-1, -1, 1, -1, 1, 1, 1}, {-1, -1, 1, 1,
    -1, -1, -1}, {-1, -1, 1, 1, -1, -1, 1}, {-1, -1, 1, 1, -1, 1, -1}, {-1, -1,
    1, 1, -1, 1, 1}, {-1, -1, 1, 1, 1, -1, -1}, {-1, -1, 1, 1, 1, -1, 1}, {-1,
    -1, 1, 1, 1, 1, -1}, {-1, -1, 1, 1, 1, 1, 1}, {-1, 1, -1, -1, -1, -1, -1},
    {-1, 1, -1, -1, -1, -1, 1}, {-1, 1, -1, -1, -1, 1, -1}, {-1, 1, -1, -1, -1,
    1, 1}, {-1, 1, -1, -1, 1, -1, -1}, {-1, 1, -1, -1, 1, -1, 1}, {-1, 1, -1,
    -1, 1, 1, -1}, {-1, 1, -1, -1, 1, 1, 1}, {-1, 1, -1, 1, -1, -1, -1}, {-1,
    1, -1, 1, -1, -1, 1}, {-1, 1, -1, 1, -1, 1, -1}, {-1, 1, -1, 1, -1, 1, 1},
    {-1, 1, -1, 1, 1, -1, -1}, {-1, 1, -1, 1, 1, -1, 1}, {-1, 1, -1, 1, 1, 1,
    -1}, {-1, 1, -1, 1, 1, 1, 1}, {-1, 1, 1, -1, -1, -1, -1}, {-1, 1, 1, -1,
    -1, -1, 1}, {-1, 1, 1, -1, -1, 1, -1}, {-1, 1, 1, -1, -1, 1, 1}, {-1, 1, 1,
    -1, 1, -1, -1}, {-1, 1, 1, -1, 1, -1, 1}, {-1, 1, 1, -1, 1, 1, -1}, {-1, 1,
    1, -1, 1, 1, 1}, {-1, 1, 1, 1, -1, -1, -1}, {-1, 1, 1, 1, -1, -1, 1}, {-1,
    1, 1, 1, -1, 1, -1}, {-1, 1, 1, 1, -1, 1, 1}, {-1, 1, 1, 1, 1, -1, -1},
    {-1, 1, 1, 1, 1, -1, 1}, {-1, 1, 1, 1, 1, 1, -1}, {-1, 1, 1, 1, 1, 1, 1},
    {1, -1, -1, -1, -1, -1, -1}, {1, -1, -1, -1, -1, -1, 1}, {1, -1, -1, -1,
    -1, 1, -1}, {1, -1, -1, -1, -1, 1, 1}, {1, -1, -1, -1, 1, -1, -1}, {1, -1,
    -1, -1, 1, -1, 1}, {1, -1, -1, -1, 1, 1, -1}, {1, -1, -1, -1, 1, 1, 1}, {1,
    -1, -1, 1, -1, -1, -1}, {1, -1, -1, 1, -1, -1, 1}, {1, -1, -1, 1, -1, 1,
    -1}, {1, -1, -1, 1, -1, 1, 1}, {1, -1, -1, 1, 1, -1, -1}, {1, -1, -1, 1, 1,
    -1, 1}, {1, -1, -1, 1, 1, 1, -1}, {1, -1, -1, 1, 1, 1, 1}, {1, -1, 1, -1,
    -1, -1, -1}, {1, -1, 1, -1, -1, -1, 1}, {1, -1, 1, -1, -1, 1, -1}, {1, -1,
    1, -1, -1, 1, 1}, {1, -1, 1, -1, 1, -1, -1}, {1, -1, 1, -1, 1, -1, 1}, {1,
    -1, 1, -1, 1, 1, -1}, {1, -1, 1, -1, 1, 1, 1}, {1, -1, 1, 1, -1, -1, -1},
    {1, -1, 1, 1, -1, -1, 1}, {1, -1, 1, 1, -1, 1, -1}, {1, -1, 1, 1, -1, 1,
    1}, {1, -1, 1, 1, 1, -1, -1}, {1, -1, 1, 1, 1, -1, 1}, {1, -1, 1, 1, 1, 1,
    -1}, {1, -1, 1, 1, 1, 1, 1}, {1, 1, -1, -1, -1, -1, -1}, {1, 1, -1, -1, -1,
    -1, 1}, {1, 1, -1, -1, -1, 1, -1}, {1, 1, -1, -1, -1, 1, 1}, {1, 1, -1, -1,
    1, -1, -1}, {1, 1, -1, -1, 1, -1, 1}, {1, 1, -1, -1, 1, 1, -1}, {1, 1, -1,
    -1, 1, 1, 1}, {1, 1, -1, 1, -1, -1, -1}, {1, 1, -1, 1, -1, -1, 1}, {1, 1,
    -1, 1, -1, 1, -1}, {1, 1, -1, 1, -1, 1, 1}, {1, 1, -1, 1, 1, -1, -1}, {1,
    1, -1, 1, 1, -1, 1}, {1, 1, -1, 1, 1, 1, -1}, {1, 1, -1, 1, 1, 1, 1}, {1,
    1, 1, -1, -1, -1, -1}, {1, 1, 1, -1, -1, -1, 1}, {1, 1, 1, -1, -1, 1, -1},
    {1, 1, 1, -1, -1, 1, 1}, {1, 1, 1, -1, 1, -1, -1}, {1, 1, 1, -1, 1, -1, 1},
    {1, 1, 1, -1, 1, 1, -1}, {1, 1, 1, -1, 1, 1, 1}, {1, 1, 1, 1, -1, -1, -1},
    {1, 1, 1, 1, -1, -1, 1}, {1, 1, 1, 1, -1, 1, -1}, {1, 1, 1, 1, -1, 1, 1},
    {1, 1, 1, 1, 1, -1, -1}, {1, 1, 1, 1, 1, -1, 1}, {1, 1, 1, 1, 1, 1, -1},
    {1, 1, 1, 1, 1, 1, 1}};

// Normalization factors the various processes
// Denominators: spins, colors and identical particles
int PY8MEs_R4_P123_sm_qb_llgqb::denom_colors[nprocesses] = {9, 9, 9, 9, 9, 9,
    9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9};
int PY8MEs_R4_P123_sm_qb_llgqb::denom_hels[nprocesses] = {4, 4, 4, 4, 4, 4, 4,
    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4};
int PY8MEs_R4_P123_sm_qb_llgqb::denom_iden[nprocesses] = {1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};

//--------------------------------------------------------------------------
// Color config initialization
void PY8MEs_R4_P123_sm_qb_llgqb::initColorConfigs() 
{
  color_configs = vector < vec_vec_int > (); 
  jamp_nc_relative_power = vector < vec_int > (); 

  // Color flows of process Process: u b > e+ e- g u b WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[0].push_back(-1); 
  // JAMP #1
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #2
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #3
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[0].push_back(-1); 

  // Color flows of process Process: u b~ > e+ e- g u b~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #1
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[1].push_back(-1); 
  // JAMP #2
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #3
  color_configs[1].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[1].push_back(-1); 

  // Color flows of process Process: d b > e+ e- g d b WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[2].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[2].push_back(-1); 
  // JAMP #1
  color_configs[2].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #2
  color_configs[2].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #3
  color_configs[2].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[2].push_back(-1); 

  // Color flows of process Process: d b~ > e+ e- g d b~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[3].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #1
  color_configs[3].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[3].push_back(-1); 
  // JAMP #2
  color_configs[3].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #3
  color_configs[3].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[3].push_back(-1); 

  // Color flows of process Process: u~ b > e+ e- g u~ b WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[4].push_back(vec_int(createvector<int>
      (0)(1)(1)(0)(0)(0)(0)(0)(3)(2)(0)(3)(2)(0)));
  jamp_nc_relative_power[4].push_back(0); 
  // JAMP #1
  color_configs[4].push_back(vec_int(createvector<int>
      (0)(1)(3)(0)(0)(0)(0)(0)(3)(2)(0)(1)(2)(0)));
  jamp_nc_relative_power[4].push_back(-1); 
  // JAMP #2
  color_configs[4].push_back(vec_int(createvector<int>
      (0)(1)(3)(0)(0)(0)(0)(0)(3)(1)(0)(2)(2)(0)));
  jamp_nc_relative_power[4].push_back(0); 
  // JAMP #3
  color_configs[4].push_back(vec_int(createvector<int>
      (0)(1)(2)(0)(0)(0)(0)(0)(3)(1)(0)(3)(2)(0)));
  jamp_nc_relative_power[4].push_back(-1); 

  // Color flows of process Process: u~ b~ > e+ e- g u~ b~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[5].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[5].push_back(-1); 
  // JAMP #1
  color_configs[5].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[5].push_back(0); 
  // JAMP #2
  color_configs[5].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[5].push_back(0); 
  // JAMP #3
  color_configs[5].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[5].push_back(-1); 

  // Color flows of process Process: d~ b > e+ e- g d~ b WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[6].push_back(vec_int(createvector<int>
      (0)(1)(1)(0)(0)(0)(0)(0)(3)(2)(0)(3)(2)(0)));
  jamp_nc_relative_power[6].push_back(0); 
  // JAMP #1
  color_configs[6].push_back(vec_int(createvector<int>
      (0)(1)(3)(0)(0)(0)(0)(0)(3)(2)(0)(1)(2)(0)));
  jamp_nc_relative_power[6].push_back(-1); 
  // JAMP #2
  color_configs[6].push_back(vec_int(createvector<int>
      (0)(1)(3)(0)(0)(0)(0)(0)(3)(1)(0)(2)(2)(0)));
  jamp_nc_relative_power[6].push_back(0); 
  // JAMP #3
  color_configs[6].push_back(vec_int(createvector<int>
      (0)(1)(2)(0)(0)(0)(0)(0)(3)(1)(0)(3)(2)(0)));
  jamp_nc_relative_power[6].push_back(-1); 

  // Color flows of process Process: d~ b~ > e+ e- g d~ b~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[7].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[7].push_back(-1); 
  // JAMP #1
  color_configs[7].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[7].push_back(0); 
  // JAMP #2
  color_configs[7].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[7].push_back(0); 
  // JAMP #3
  color_configs[7].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[7].push_back(-1); 

  // Color flows of process Process: u b > ve~ ve g u b WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[8].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[8].push_back(-1); 
  // JAMP #1
  color_configs[8].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[8].push_back(0); 
  // JAMP #2
  color_configs[8].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[8].push_back(0); 
  // JAMP #3
  color_configs[8].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[8].push_back(-1); 

  // Color flows of process Process: u b~ > ve~ ve g u b~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[9].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[9].push_back(0); 
  // JAMP #1
  color_configs[9].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[9].push_back(-1); 
  // JAMP #2
  color_configs[9].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[9].push_back(0); 
  // JAMP #3
  color_configs[9].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[9].push_back(-1); 

  // Color flows of process Process: d b > ve~ ve g d b WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[10].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[10].push_back(-1); 
  // JAMP #1
  color_configs[10].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[10].push_back(0); 
  // JAMP #2
  color_configs[10].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[10].push_back(0); 
  // JAMP #3
  color_configs[10].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[10].push_back(-1); 

  // Color flows of process Process: d b~ > ve~ ve g d b~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[11].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[11].push_back(0); 
  // JAMP #1
  color_configs[11].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[11].push_back(-1); 
  // JAMP #2
  color_configs[11].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[11].push_back(0); 
  // JAMP #3
  color_configs[11].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[11].push_back(-1); 

  // Color flows of process Process: u~ b > ve~ ve g u~ b WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[12].push_back(vec_int(createvector<int>
      (0)(1)(1)(0)(0)(0)(0)(0)(3)(2)(0)(3)(2)(0)));
  jamp_nc_relative_power[12].push_back(0); 
  // JAMP #1
  color_configs[12].push_back(vec_int(createvector<int>
      (0)(1)(3)(0)(0)(0)(0)(0)(3)(2)(0)(1)(2)(0)));
  jamp_nc_relative_power[12].push_back(-1); 
  // JAMP #2
  color_configs[12].push_back(vec_int(createvector<int>
      (0)(1)(3)(0)(0)(0)(0)(0)(3)(1)(0)(2)(2)(0)));
  jamp_nc_relative_power[12].push_back(0); 
  // JAMP #3
  color_configs[12].push_back(vec_int(createvector<int>
      (0)(1)(2)(0)(0)(0)(0)(0)(3)(1)(0)(3)(2)(0)));
  jamp_nc_relative_power[12].push_back(-1); 

  // Color flows of process Process: u~ b~ > ve~ ve g u~ b~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[13].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[13].push_back(-1); 
  // JAMP #1
  color_configs[13].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[13].push_back(0); 
  // JAMP #2
  color_configs[13].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[13].push_back(0); 
  // JAMP #3
  color_configs[13].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[13].push_back(-1); 

  // Color flows of process Process: d~ b > ve~ ve g d~ b WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[14].push_back(vec_int(createvector<int>
      (0)(1)(1)(0)(0)(0)(0)(0)(3)(2)(0)(3)(2)(0)));
  jamp_nc_relative_power[14].push_back(0); 
  // JAMP #1
  color_configs[14].push_back(vec_int(createvector<int>
      (0)(1)(3)(0)(0)(0)(0)(0)(3)(2)(0)(1)(2)(0)));
  jamp_nc_relative_power[14].push_back(-1); 
  // JAMP #2
  color_configs[14].push_back(vec_int(createvector<int>
      (0)(1)(3)(0)(0)(0)(0)(0)(3)(1)(0)(2)(2)(0)));
  jamp_nc_relative_power[14].push_back(0); 
  // JAMP #3
  color_configs[14].push_back(vec_int(createvector<int>
      (0)(1)(2)(0)(0)(0)(0)(0)(3)(1)(0)(3)(2)(0)));
  jamp_nc_relative_power[14].push_back(-1); 

  // Color flows of process Process: d~ b~ > ve~ ve g d~ b~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[15].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[15].push_back(-1); 
  // JAMP #1
  color_configs[15].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[15].push_back(0); 
  // JAMP #2
  color_configs[15].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[15].push_back(0); 
  // JAMP #3
  color_configs[15].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[15].push_back(-1); 

  // Color flows of process Process: u b > e+ ve g d b WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[16].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[16].push_back(-1); 
  // JAMP #1
  color_configs[16].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[16].push_back(0); 
  // JAMP #2
  color_configs[16].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[16].push_back(0); 
  // JAMP #3
  color_configs[16].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[16].push_back(-1); 

  // Color flows of process Process: u b~ > e+ ve g d b~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[17].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[17].push_back(0); 
  // JAMP #1
  color_configs[17].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[17].push_back(-1); 
  // JAMP #2
  color_configs[17].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[17].push_back(0); 
  // JAMP #3
  color_configs[17].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[17].push_back(-1); 

  // Color flows of process Process: d b > ve~ e- g u b WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[18].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[18].push_back(-1); 
  // JAMP #1
  color_configs[18].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[18].push_back(0); 
  // JAMP #2
  color_configs[18].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[18].push_back(0); 
  // JAMP #3
  color_configs[18].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[18].push_back(-1); 

  // Color flows of process Process: d b~ > ve~ e- g u b~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[19].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[19].push_back(0); 
  // JAMP #1
  color_configs[19].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[19].push_back(-1); 
  // JAMP #2
  color_configs[19].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[19].push_back(0); 
  // JAMP #3
  color_configs[19].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[19].push_back(-1); 

  // Color flows of process Process: u~ b > ve~ e- g d~ b WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[20].push_back(vec_int(createvector<int>
      (0)(1)(1)(0)(0)(0)(0)(0)(3)(2)(0)(3)(2)(0)));
  jamp_nc_relative_power[20].push_back(0); 
  // JAMP #1
  color_configs[20].push_back(vec_int(createvector<int>
      (0)(1)(3)(0)(0)(0)(0)(0)(3)(2)(0)(1)(2)(0)));
  jamp_nc_relative_power[20].push_back(-1); 
  // JAMP #2
  color_configs[20].push_back(vec_int(createvector<int>
      (0)(1)(3)(0)(0)(0)(0)(0)(3)(1)(0)(2)(2)(0)));
  jamp_nc_relative_power[20].push_back(0); 
  // JAMP #3
  color_configs[20].push_back(vec_int(createvector<int>
      (0)(1)(2)(0)(0)(0)(0)(0)(3)(1)(0)(3)(2)(0)));
  jamp_nc_relative_power[20].push_back(-1); 

  // Color flows of process Process: u~ b~ > ve~ e- g d~ b~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[21].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[21].push_back(-1); 
  // JAMP #1
  color_configs[21].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[21].push_back(0); 
  // JAMP #2
  color_configs[21].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[21].push_back(0); 
  // JAMP #3
  color_configs[21].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[21].push_back(-1); 

  // Color flows of process Process: d~ b > e+ ve g u~ b WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[22].push_back(vec_int(createvector<int>
      (0)(1)(1)(0)(0)(0)(0)(0)(3)(2)(0)(3)(2)(0)));
  jamp_nc_relative_power[22].push_back(0); 
  // JAMP #1
  color_configs[22].push_back(vec_int(createvector<int>
      (0)(1)(3)(0)(0)(0)(0)(0)(3)(2)(0)(1)(2)(0)));
  jamp_nc_relative_power[22].push_back(-1); 
  // JAMP #2
  color_configs[22].push_back(vec_int(createvector<int>
      (0)(1)(3)(0)(0)(0)(0)(0)(3)(1)(0)(2)(2)(0)));
  jamp_nc_relative_power[22].push_back(0); 
  // JAMP #3
  color_configs[22].push_back(vec_int(createvector<int>
      (0)(1)(2)(0)(0)(0)(0)(0)(3)(1)(0)(3)(2)(0)));
  jamp_nc_relative_power[22].push_back(-1); 

  // Color flows of process Process: d~ b~ > e+ ve g u~ b~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[23].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[23].push_back(-1); 
  // JAMP #1
  color_configs[23].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[23].push_back(0); 
  // JAMP #2
  color_configs[23].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[23].push_back(0); 
  // JAMP #3
  color_configs[23].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[23].push_back(-1); 
}

//--------------------------------------------------------------------------
// Destructor.
PY8MEs_R4_P123_sm_qb_llgqb::~PY8MEs_R4_P123_sm_qb_llgqb() 
{
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    delete[] p[i]; 
    p[i] = NULL; 
  }
}

//--------------------------------------------------------------------------
// Invert the permutation mapping
vector<int> PY8MEs_R4_P123_sm_qb_llgqb::invert_mapping(vector<int> mapping) 
{
  vector<int> inverted_mapping; 
  for (unsigned int i = 0; i < mapping.size(); i++ )
  {
    for (unsigned int j = 0; j < mapping.size(); j++ )
    {
      if (mapping[j] == ((int)i))
      {
        inverted_mapping.push_back(j); 
        break; 
      }
    }
  }
  return inverted_mapping; 
}

//--------------------------------------------------------------------------
// Return the list of possible helicity configurations
vector < vec_int > PY8MEs_R4_P123_sm_qb_llgqb::getHelicityConfigs(vector<int>
    permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(ncomb, vector<int> (nexternal, 0)); 
  for (unsigned int ihel = 0; ihel < ncomb; ihel++ )
  {
    for(unsigned int j = 0; j < nexternal; j++ )
    {
      res[ihel][chosenPerm[j]] = helicities[ihel][j]; 
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Return the list of possible color configurations
vector < vec_int > PY8MEs_R4_P123_sm_qb_llgqb::getColorConfigs(int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(color_configs[chosenProcID].size(), vector<int>
      (nexternal * 2, 0));
  for (unsigned int icol = 0; icol < color_configs[chosenProcID].size(); icol++
      )
  {
    for(unsigned int j = 0; j < (2 * nexternal); j++ )
    {
      res[icol][chosenPerm[j/2] * 2 + j%2] =
          color_configs[chosenProcID][icol][j];
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Get JAMP relative N_c power
int PY8MEs_R4_P123_sm_qb_llgqb::getColorFlowRelativeNCPower(int color_flow_ID,
    int specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return jamp_nc_relative_power[chosenProcID][color_flow_ID]; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity ID -> Helicity Config
vector<int> PY8MEs_R4_P123_sm_qb_llgqb::getHelicityConfigForID(int hel_ID,
    vector<int> permutation)
{
  if (hel_ID < 0 || hel_ID >= ncomb)
  {
    cerr <<  "Error in function 'getHelicityConfigForID' of class" << 
    " 'PY8MEs_R4_P123_sm_qb_llgqb': Specified helicity ID '" << 
    hel_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R4_P123_sm_qb_llgqb_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(nexternal, 0); 
  for (unsigned int j = 0; j < nexternal; j++ )
  {
    res[chosenPerm[j]] = helicities[hel_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity Config -> Helicity ID
int PY8MEs_R4_P123_sm_qb_llgqb::getHelicityIDForConfig(vector<int> hel_config,
    vector<int> permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  int user_ihel = -1; 
  if (hel_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < ncomb; i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < nexternal; j++ )
      {
        if (helicities[i][chosenPerm[j]] != hel_config[j])
        {
          found = false; 
          break; 
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_ihel = i; 
        break; 
      }
    }
    if (user_ihel == -1)
    {
      cerr <<  "Error in function 'getHelicityIDForConfig' of class" << 
      " 'PY8MEs_R4_P123_sm_qb_llgqb': Specified helicity" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R4_P123_sm_qb_llgqb_exception; 
    }
  }
  return user_ihel; 
}


//--------------------------------------------------------------------------
// Implements the map Color ID -> Color Config
vector<int> PY8MEs_R4_P123_sm_qb_llgqb::getColorConfigForID(int color_ID, int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < 0 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getColorConfigForID' of class" << 
    " 'PY8MEs_R4_P123_sm_qb_llgqb': Specified color ID '" << 
    color_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R4_P123_sm_qb_llgqb_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(color_configs[chosenProcID][color_ID].size(), 0); 
  for (unsigned int j = 0; j < (2 * nexternal); j++ )
  {
    res[chosenPerm[j/2] * 2 + j%2] = color_configs[chosenProcID][color_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Color Config -> Color ID
int PY8MEs_R4_P123_sm_qb_llgqb::getColorIDForConfig(vector<int> color_config,
    int specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = -1; 
  if (color_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < color_configs[chosenProcID].size(); i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < (nexternal * 2); j++ )
      {

        // If colorless then make sure it matches
        // The little arithmetics in the color index is just
        // the permutation applies on the particle list which is
        // twice smaller since each particle can have two color indices.
        if (color_config[j] == 0)
        {
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] != 0)
          {
            found = false; 
            break; 
          }
          // Otherwise check that the color linked position matches
        }
        else
        {
          int color_linked_pos = -1; 
          // Find the other end of the line in the user color config
          for (unsigned int k = 0; k < (nexternal * 2); k++ )
          {
            if (k == j)
              continue; 
            if (color_config[j] == color_config[k])
            {
              color_linked_pos = k; 
              break; 
            }
          }
          if (color_linked_pos == -1)
          {
            cerr <<  "Error in function 'getColorIDForConfig' of class" << 
            " 'PY8MEs_R4_P123_sm_qb_llgqb': A color line could " << 
            " not be closed." << endl; 
            throw PY8MEs_R4_P123_sm_qb_llgqb_exception; 
          }
          // Now check whether the color line matches
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] !=
              color_configs[chosenProcID][i][chosenPerm[color_linked_pos/2] * 2
              + color_linked_pos%2])
          {
            found = false; 
            break; 
          }
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_icol = i; 
        break; 
      }
    }

    if (user_icol == -1)
    {
      cerr <<  "Error in function 'getColorIDForConfig' of class" << 
      " 'PY8MEs_R4_P123_sm_qb_llgqb': Specified color" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R4_P123_sm_qb_llgqb_exception; 
    }
  }
  return user_icol; 
}

//--------------------------------------------------------------------------
// Returns all result previously computed in SigmaKin
vector < vec_double > PY8MEs_R4_P123_sm_qb_llgqb::getAllResults(int
    specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return all_results[chosenProcID]; 
}

//--------------------------------------------------------------------------
// Returns a result previously computed in SigmaKin for a specific helicity
// and color ID. -1 means avg and summed over that characteristic.
double PY8MEs_R4_P123_sm_qb_llgqb::getResult(int helicity_ID, int color_ID, int
    specify_proc_ID)
{
  if (helicity_ID < - 1 || helicity_ID >= ncomb)
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R4_P123_sm_qb_llgqb': Specified helicity ID '" << 
    helicity_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R4_P123_sm_qb_llgqb_exception; 
  }
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < - 1 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R4_P123_sm_qb_llgqb': Specified color ID '" << 
    color_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R4_P123_sm_qb_llgqb_exception; 
  }
  return all_results[chosenProcID][helicity_ID + 1][color_ID + 1]; 
}

//--------------------------------------------------------------------------
// Check for the availability of the requested process and if available,
// If available, this returns the corresponding permutation and Proc_ID to use.
// If not available, this returns a negative Proc_ID.
pair < vector<int> , int > PY8MEs_R4_P123_sm_qb_llgqb::static_getPY8ME(vector<int> initial_pdgs, vector<int> final_pdgs, set<int> schannels) 
{

  // Not available return value
  pair < vector<int> , int > NA(vector<int> (), -1); 

  // Check if s-channel requirements match
  if (nreq_s_channels > 0)
  {
    if (schannels != s_channel_proc)
      return NA; 
  }
  else
  {
    if (schannels.size() != 0)
      return NA; 
  }

  // Check number of final state particles
  if (final_pdgs.size() != (nexternal - ninitial))
    return NA; 

  // Check number of initial state particles
  if (initial_pdgs.size() != ninitial)
    return NA; 

  // List of processes available in this class
  const int nprocs = 224; 
  const int proc_IDS[nprocs] = {0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3,
      4, 4, 4, 4, 5, 5, 5, 5, 6, 6, 6, 6, 7, 7, 7, 7, 8, 8, 8, 8, 8, 8, 9, 9,
      9, 9, 9, 9, 10, 10, 10, 10, 10, 10, 11, 11, 11, 11, 11, 11, 12, 12, 12,
      12, 12, 12, 13, 13, 13, 13, 13, 13, 14, 14, 14, 14, 14, 14, 15, 15, 15,
      15, 15, 15, 16, 16, 16, 16, 17, 17, 17, 17, 18, 18, 18, 18, 19, 19, 19,
      19, 20, 20, 20, 20, 21, 21, 21, 21, 22, 22, 22, 22, 23, 23, 23, 23, 0, 0,
      0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 6, 6,
      6, 6, 7, 7, 7, 7, 8, 8, 8, 8, 8, 8, 9, 9, 9, 9, 9, 9, 10, 10, 10, 10, 10,
      10, 11, 11, 11, 11, 11, 11, 12, 12, 12, 12, 12, 12, 13, 13, 13, 13, 13,
      13, 14, 14, 14, 14, 14, 14, 15, 15, 15, 15, 15, 15, 16, 16, 16, 16, 17,
      17, 17, 17, 18, 18, 18, 18, 19, 19, 19, 19, 20, 20, 20, 20, 21, 21, 21,
      21, 22, 22, 22, 22, 23, 23, 23, 23};
  const int in_pdgs[nprocs][ninitial] = {{2, 5}, {2, 5}, {4, 5}, {4, 5}, {2,
      -5}, {2, -5}, {4, -5}, {4, -5}, {1, 5}, {1, 5}, {3, 5}, {3, 5}, {1, -5},
      {1, -5}, {3, -5}, {3, -5}, {-2, 5}, {-2, 5}, {-4, 5}, {-4, 5}, {-2, -5},
      {-2, -5}, {-4, -5}, {-4, -5}, {-1, 5}, {-1, 5}, {-3, 5}, {-3, 5}, {-1,
      -5}, {-1, -5}, {-3, -5}, {-3, -5}, {2, 5}, {2, 5}, {2, 5}, {4, 5}, {4,
      5}, {4, 5}, {2, -5}, {2, -5}, {2, -5}, {4, -5}, {4, -5}, {4, -5}, {1, 5},
      {1, 5}, {1, 5}, {3, 5}, {3, 5}, {3, 5}, {1, -5}, {1, -5}, {1, -5}, {3,
      -5}, {3, -5}, {3, -5}, {-2, 5}, {-2, 5}, {-2, 5}, {-4, 5}, {-4, 5}, {-4,
      5}, {-2, -5}, {-2, -5}, {-2, -5}, {-4, -5}, {-4, -5}, {-4, -5}, {-1, 5},
      {-1, 5}, {-1, 5}, {-3, 5}, {-3, 5}, {-3, 5}, {-1, -5}, {-1, -5}, {-1,
      -5}, {-3, -5}, {-3, -5}, {-3, -5}, {2, 5}, {2, 5}, {4, 5}, {4, 5}, {2,
      -5}, {2, -5}, {4, -5}, {4, -5}, {1, 5}, {1, 5}, {3, 5}, {3, 5}, {1, -5},
      {1, -5}, {3, -5}, {3, -5}, {-2, 5}, {-2, 5}, {-4, 5}, {-4, 5}, {-2, -5},
      {-2, -5}, {-4, -5}, {-4, -5}, {-1, 5}, {-1, 5}, {-3, 5}, {-3, 5}, {-1,
      -5}, {-1, -5}, {-3, -5}, {-3, -5}, {5, 2}, {5, 2}, {5, 4}, {5, 4}, {-5,
      2}, {-5, 2}, {-5, 4}, {-5, 4}, {5, 1}, {5, 1}, {5, 3}, {5, 3}, {-5, 1},
      {-5, 1}, {-5, 3}, {-5, 3}, {5, -2}, {5, -2}, {5, -4}, {5, -4}, {-5, -2},
      {-5, -2}, {-5, -4}, {-5, -4}, {5, -1}, {5, -1}, {5, -3}, {5, -3}, {-5,
      -1}, {-5, -1}, {-5, -3}, {-5, -3}, {5, 2}, {5, 2}, {5, 2}, {5, 4}, {5,
      4}, {5, 4}, {-5, 2}, {-5, 2}, {-5, 2}, {-5, 4}, {-5, 4}, {-5, 4}, {5, 1},
      {5, 1}, {5, 1}, {5, 3}, {5, 3}, {5, 3}, {-5, 1}, {-5, 1}, {-5, 1}, {-5,
      3}, {-5, 3}, {-5, 3}, {5, -2}, {5, -2}, {5, -2}, {5, -4}, {5, -4}, {5,
      -4}, {-5, -2}, {-5, -2}, {-5, -2}, {-5, -4}, {-5, -4}, {-5, -4}, {5, -1},
      {5, -1}, {5, -1}, {5, -3}, {5, -3}, {5, -3}, {-5, -1}, {-5, -1}, {-5,
      -1}, {-5, -3}, {-5, -3}, {-5, -3}, {5, 2}, {5, 2}, {5, 4}, {5, 4}, {-5,
      2}, {-5, 2}, {-5, 4}, {-5, 4}, {5, 1}, {5, 1}, {5, 3}, {5, 3}, {-5, 1},
      {-5, 1}, {-5, 3}, {-5, 3}, {5, -2}, {5, -2}, {5, -4}, {5, -4}, {-5, -2},
      {-5, -2}, {-5, -4}, {-5, -4}, {5, -1}, {5, -1}, {5, -3}, {5, -3}, {-5,
      -1}, {-5, -1}, {-5, -3}, {-5, -3}};
  const int out_pdgs[nprocs][nexternal - ninitial] = {{-11, 11, 21, 2, 5},
      {-13, 13, 21, 2, 5}, {-11, 11, 21, 4, 5}, {-13, 13, 21, 4, 5}, {-11, 11,
      21, 2, -5}, {-13, 13, 21, 2, -5}, {-11, 11, 21, 4, -5}, {-13, 13, 21, 4,
      -5}, {-11, 11, 21, 1, 5}, {-13, 13, 21, 1, 5}, {-11, 11, 21, 3, 5}, {-13,
      13, 21, 3, 5}, {-11, 11, 21, 1, -5}, {-13, 13, 21, 1, -5}, {-11, 11, 21,
      3, -5}, {-13, 13, 21, 3, -5}, {-11, 11, 21, -2, 5}, {-13, 13, 21, -2, 5},
      {-11, 11, 21, -4, 5}, {-13, 13, 21, -4, 5}, {-11, 11, 21, -2, -5}, {-13,
      13, 21, -2, -5}, {-11, 11, 21, -4, -5}, {-13, 13, 21, -4, -5}, {-11, 11,
      21, -1, 5}, {-13, 13, 21, -1, 5}, {-11, 11, 21, -3, 5}, {-13, 13, 21, -3,
      5}, {-11, 11, 21, -1, -5}, {-13, 13, 21, -1, -5}, {-11, 11, 21, -3, -5},
      {-13, 13, 21, -3, -5}, {-12, 12, 21, 2, 5}, {-14, 14, 21, 2, 5}, {-16,
      16, 21, 2, 5}, {-12, 12, 21, 4, 5}, {-14, 14, 21, 4, 5}, {-16, 16, 21, 4,
      5}, {-12, 12, 21, 2, -5}, {-14, 14, 21, 2, -5}, {-16, 16, 21, 2, -5},
      {-12, 12, 21, 4, -5}, {-14, 14, 21, 4, -5}, {-16, 16, 21, 4, -5}, {-12,
      12, 21, 1, 5}, {-14, 14, 21, 1, 5}, {-16, 16, 21, 1, 5}, {-12, 12, 21, 3,
      5}, {-14, 14, 21, 3, 5}, {-16, 16, 21, 3, 5}, {-12, 12, 21, 1, -5}, {-14,
      14, 21, 1, -5}, {-16, 16, 21, 1, -5}, {-12, 12, 21, 3, -5}, {-14, 14, 21,
      3, -5}, {-16, 16, 21, 3, -5}, {-12, 12, 21, -2, 5}, {-14, 14, 21, -2, 5},
      {-16, 16, 21, -2, 5}, {-12, 12, 21, -4, 5}, {-14, 14, 21, -4, 5}, {-16,
      16, 21, -4, 5}, {-12, 12, 21, -2, -5}, {-14, 14, 21, -2, -5}, {-16, 16,
      21, -2, -5}, {-12, 12, 21, -4, -5}, {-14, 14, 21, -4, -5}, {-16, 16, 21,
      -4, -5}, {-12, 12, 21, -1, 5}, {-14, 14, 21, -1, 5}, {-16, 16, 21, -1,
      5}, {-12, 12, 21, -3, 5}, {-14, 14, 21, -3, 5}, {-16, 16, 21, -3, 5},
      {-12, 12, 21, -1, -5}, {-14, 14, 21, -1, -5}, {-16, 16, 21, -1, -5},
      {-12, 12, 21, -3, -5}, {-14, 14, 21, -3, -5}, {-16, 16, 21, -3, -5},
      {-11, 12, 21, 1, 5}, {-13, 14, 21, 1, 5}, {-11, 12, 21, 3, 5}, {-13, 14,
      21, 3, 5}, {-11, 12, 21, 1, -5}, {-13, 14, 21, 1, -5}, {-11, 12, 21, 3,
      -5}, {-13, 14, 21, 3, -5}, {-12, 11, 21, 2, 5}, {-14, 13, 21, 2, 5},
      {-12, 11, 21, 4, 5}, {-14, 13, 21, 4, 5}, {-12, 11, 21, 2, -5}, {-14, 13,
      21, 2, -5}, {-12, 11, 21, 4, -5}, {-14, 13, 21, 4, -5}, {-12, 11, 21, -1,
      5}, {-14, 13, 21, -1, 5}, {-12, 11, 21, -3, 5}, {-14, 13, 21, -3, 5},
      {-12, 11, 21, -1, -5}, {-14, 13, 21, -1, -5}, {-12, 11, 21, -3, -5},
      {-14, 13, 21, -3, -5}, {-11, 12, 21, -2, 5}, {-13, 14, 21, -2, 5}, {-11,
      12, 21, -4, 5}, {-13, 14, 21, -4, 5}, {-11, 12, 21, -2, -5}, {-13, 14,
      21, -2, -5}, {-11, 12, 21, -4, -5}, {-13, 14, 21, -4, -5}, {-11, 11, 21,
      2, 5}, {-13, 13, 21, 2, 5}, {-11, 11, 21, 4, 5}, {-13, 13, 21, 4, 5},
      {-11, 11, 21, 2, -5}, {-13, 13, 21, 2, -5}, {-11, 11, 21, 4, -5}, {-13,
      13, 21, 4, -5}, {-11, 11, 21, 1, 5}, {-13, 13, 21, 1, 5}, {-11, 11, 21,
      3, 5}, {-13, 13, 21, 3, 5}, {-11, 11, 21, 1, -5}, {-13, 13, 21, 1, -5},
      {-11, 11, 21, 3, -5}, {-13, 13, 21, 3, -5}, {-11, 11, 21, -2, 5}, {-13,
      13, 21, -2, 5}, {-11, 11, 21, -4, 5}, {-13, 13, 21, -4, 5}, {-11, 11, 21,
      -2, -5}, {-13, 13, 21, -2, -5}, {-11, 11, 21, -4, -5}, {-13, 13, 21, -4,
      -5}, {-11, 11, 21, -1, 5}, {-13, 13, 21, -1, 5}, {-11, 11, 21, -3, 5},
      {-13, 13, 21, -3, 5}, {-11, 11, 21, -1, -5}, {-13, 13, 21, -1, -5}, {-11,
      11, 21, -3, -5}, {-13, 13, 21, -3, -5}, {-12, 12, 21, 2, 5}, {-14, 14,
      21, 2, 5}, {-16, 16, 21, 2, 5}, {-12, 12, 21, 4, 5}, {-14, 14, 21, 4, 5},
      {-16, 16, 21, 4, 5}, {-12, 12, 21, 2, -5}, {-14, 14, 21, 2, -5}, {-16,
      16, 21, 2, -5}, {-12, 12, 21, 4, -5}, {-14, 14, 21, 4, -5}, {-16, 16, 21,
      4, -5}, {-12, 12, 21, 1, 5}, {-14, 14, 21, 1, 5}, {-16, 16, 21, 1, 5},
      {-12, 12, 21, 3, 5}, {-14, 14, 21, 3, 5}, {-16, 16, 21, 3, 5}, {-12, 12,
      21, 1, -5}, {-14, 14, 21, 1, -5}, {-16, 16, 21, 1, -5}, {-12, 12, 21, 3,
      -5}, {-14, 14, 21, 3, -5}, {-16, 16, 21, 3, -5}, {-12, 12, 21, -2, 5},
      {-14, 14, 21, -2, 5}, {-16, 16, 21, -2, 5}, {-12, 12, 21, -4, 5}, {-14,
      14, 21, -4, 5}, {-16, 16, 21, -4, 5}, {-12, 12, 21, -2, -5}, {-14, 14,
      21, -2, -5}, {-16, 16, 21, -2, -5}, {-12, 12, 21, -4, -5}, {-14, 14, 21,
      -4, -5}, {-16, 16, 21, -4, -5}, {-12, 12, 21, -1, 5}, {-14, 14, 21, -1,
      5}, {-16, 16, 21, -1, 5}, {-12, 12, 21, -3, 5}, {-14, 14, 21, -3, 5},
      {-16, 16, 21, -3, 5}, {-12, 12, 21, -1, -5}, {-14, 14, 21, -1, -5}, {-16,
      16, 21, -1, -5}, {-12, 12, 21, -3, -5}, {-14, 14, 21, -3, -5}, {-16, 16,
      21, -3, -5}, {-11, 12, 21, 1, 5}, {-13, 14, 21, 1, 5}, {-11, 12, 21, 3,
      5}, {-13, 14, 21, 3, 5}, {-11, 12, 21, 1, -5}, {-13, 14, 21, 1, -5},
      {-11, 12, 21, 3, -5}, {-13, 14, 21, 3, -5}, {-12, 11, 21, 2, 5}, {-14,
      13, 21, 2, 5}, {-12, 11, 21, 4, 5}, {-14, 13, 21, 4, 5}, {-12, 11, 21, 2,
      -5}, {-14, 13, 21, 2, -5}, {-12, 11, 21, 4, -5}, {-14, 13, 21, 4, -5},
      {-12, 11, 21, -1, 5}, {-14, 13, 21, -1, 5}, {-12, 11, 21, -3, 5}, {-14,
      13, 21, -3, 5}, {-12, 11, 21, -1, -5}, {-14, 13, 21, -1, -5}, {-12, 11,
      21, -3, -5}, {-14, 13, 21, -3, -5}, {-11, 12, 21, -2, 5}, {-13, 14, 21,
      -2, 5}, {-11, 12, 21, -4, 5}, {-13, 14, 21, -4, 5}, {-11, 12, 21, -2,
      -5}, {-13, 14, 21, -2, -5}, {-11, 12, 21, -4, -5}, {-13, 14, 21, -4, -5}};

  bool in_pdgs_used[ninitial]; 
  bool out_pdgs_used[nexternal - ninitial]; 
  for(unsigned int i = 0; i < nprocs; i++ )
  {
    int permutations[nexternal]; 

    // Reinitialize initial state look-up variables
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      in_pdgs_used[j] = false; 
      permutations[j] = -1; 
    }
    // Look for initial state matches
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      for(unsigned int k = 0; k < ninitial; k++ )
      {
        // Make sure it has not been used already
        if (in_pdgs_used[k])
          continue; 
        if (initial_pdgs[k] == in_pdgs[i][j])
        {
          permutations[j] = k; 
          in_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[ninitial - 1] == -1)
      continue; 

    // Reinitialize final state look-up variables
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      out_pdgs_used[j] = false; 
      permutations[ninitial + j] = -1; 
    }
    // Look for final state matches
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      for(unsigned int k = 0; k < (nexternal - ninitial); k++ )
      {
        // Make sure it has not been used already
        if (out_pdgs_used[k])
          continue; 
        if (final_pdgs[k] == out_pdgs[i][j])
        {
          permutations[ninitial + j] = ninitial + k; 
          out_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[ninitial + j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[nexternal - 1] == -1)
      continue; 

    // Return process found
    return pair < vector<int> , int > (vector<int> (permutations, permutations
        + nexternal), proc_IDS[i]);
  }

  // No process found
  return NA; 
}

//--------------------------------------------------------------------------
// Set momenta
void PY8MEs_R4_P123_sm_qb_llgqb::setMomenta(vector < vec_double >
    momenta_picked)
{
  if (momenta_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setMomenta' of class" << 
    " 'PY8MEs_R4_P123_sm_qb_llgqb': Incorrect number of" << 
    " momenta specified." << endl; 
    throw PY8MEs_R4_P123_sm_qb_llgqb_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    if (momenta_picked[i].size() != 4)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R4_P123_sm_qb_llgqb': Incorrect number of" << 
      " momenta components specified." << endl; 
      throw PY8MEs_R4_P123_sm_qb_llgqb_exception; 
    }
    if (momenta_picked[i][0] < 0.0)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R4_P123_sm_qb_llgqb': A momentum was specified" << 
      " with negative energy. Check conventions." << endl; 
      throw PY8MEs_R4_P123_sm_qb_llgqb_exception; 
    }
    for (unsigned int j = 0; j < 4; j++ )
    {
      p[i][j] = momenta_picked[i][j]; 
    }
  }
}

//--------------------------------------------------------------------------
// Set color configuration to use. An empty vector means sum over all.
void PY8MEs_R4_P123_sm_qb_llgqb::setColors(vector<int> colors_picked)
{
  if (colors_picked.size() == 0)
  {
    user_colors = vector<int> (); 
    return; 
  }
  if (colors_picked.size() != (2 * nexternal))
  {
    cerr <<  "Error in function 'setColors' of class" << 
    " 'PY8MEs_R4_P123_sm_qb_llgqb': Incorrect number" << 
    " of colors specified." << endl; 
    throw PY8MEs_R4_P123_sm_qb_llgqb_exception; 
  }
  user_colors = vector<int> ((2 * nexternal), 0); 
  for(unsigned int i = 0; i < (2 * nexternal); i++ )
  {
    user_colors[i] = colors_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the helicity configuration to use. Am empty vector means sum over all.
void PY8MEs_R4_P123_sm_qb_llgqb::setHelicities(vector<int> helicities_picked) 
{
  if (helicities_picked.size() != nexternal)
  {
    if (helicities_picked.size() == 0)
    {
      user_helicities = vector<int> (); 
      return; 
    }
    cerr <<  "Error in function 'setHelicities' of class" << 
    " 'PY8MEs_R4_P123_sm_qb_llgqb': Incorrect number" << 
    " of helicities specified." << endl; 
    throw PY8MEs_R4_P123_sm_qb_llgqb_exception; 
  }
  user_helicities = vector<int> (nexternal, 0); 
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    user_helicities[i] = helicities_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the permutation to use (will apply to momenta, colors and helicities)
void PY8MEs_R4_P123_sm_qb_llgqb::setPermutation(vector<int> perm_picked) 
{
  if (perm_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setPermutations' of class" << 
    " 'PY8MEs_R4_P123_sm_qb_llgqb': Incorrect number" << 
    " of permutations specified." << endl; 
    throw PY8MEs_R4_P123_sm_qb_llgqb_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    perm[i] = perm_picked[i]; 
  }
}

// Set the proc_ID to use
void PY8MEs_R4_P123_sm_qb_llgqb::setProcID(int procID_picked) 
{
  proc_ID = procID_picked; 
}

//--------------------------------------------------------------------------
// Initialize process.

void PY8MEs_R4_P123_sm_qb_llgqb::initProc() 
{

  // Initialize flags
  include_symmetry_factors = true; 
  include_helicity_averaging_factors = true; 
  include_color_averaging_factors = true; 

  // Initialize vectors.
  perm = vector<int> (nexternal, 0); 
  user_colors = vector<int> (2 * nexternal, 0); 
  user_helicities = vector<int> (nexternal, 0); 
  p = vector < double * > (); 
  for (unsigned int i = 0; i < nexternal; i++ )
  {
    p.push_back(new double[4]); 
  }
  initColorConfigs(); 
  // Synchronize local variables dependent on the model with the active model.
  mME = vector<double> (nexternal, 0.); 
  syncProcModelParams(); 
  jamp2 = vector < vec_double > (24); 
  jamp2[0] = vector<double> (4, 0.); 
  jamp2[1] = vector<double> (4, 0.); 
  jamp2[2] = vector<double> (4, 0.); 
  jamp2[3] = vector<double> (4, 0.); 
  jamp2[4] = vector<double> (4, 0.); 
  jamp2[5] = vector<double> (4, 0.); 
  jamp2[6] = vector<double> (4, 0.); 
  jamp2[7] = vector<double> (4, 0.); 
  jamp2[8] = vector<double> (4, 0.); 
  jamp2[9] = vector<double> (4, 0.); 
  jamp2[10] = vector<double> (4, 0.); 
  jamp2[11] = vector<double> (4, 0.); 
  jamp2[12] = vector<double> (4, 0.); 
  jamp2[13] = vector<double> (4, 0.); 
  jamp2[14] = vector<double> (4, 0.); 
  jamp2[15] = vector<double> (4, 0.); 
  jamp2[16] = vector<double> (4, 0.); 
  jamp2[17] = vector<double> (4, 0.); 
  jamp2[18] = vector<double> (4, 0.); 
  jamp2[19] = vector<double> (4, 0.); 
  jamp2[20] = vector<double> (4, 0.); 
  jamp2[21] = vector<double> (4, 0.); 
  jamp2[22] = vector<double> (4, 0.); 
  jamp2[23] = vector<double> (4, 0.); 
  all_results = vector < vec_vec_double > (24); 
  // The first entry is always the color or helicity avg/summed matrix element.
  all_results[0] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[1] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[2] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[3] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[4] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[5] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[6] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[7] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[8] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[9] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[10] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[11] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[12] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[13] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[14] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[15] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[16] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[17] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[18] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[19] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[20] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[21] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[22] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[23] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
}

// Synchronize local variables of the process that depend on the model
// parameters
void PY8MEs_R4_P123_sm_qb_llgqb::syncProcModelParams() 
{

  // Instantiate the model class and set parameters that stay fixed during run
  mME[0] = pars->ZERO; 
  mME[1] = pars->mdl_MB; 
  mME[2] = pars->ZERO; 
  mME[3] = pars->ZERO; 
  mME[4] = pars->ZERO; 
  mME[5] = pars->ZERO; 
  mME[6] = pars->mdl_MB; 
}

//--------------------------------------------------------------------------
// Setter allowing to force particular values for the external masses
void PY8MEs_R4_P123_sm_qb_llgqb::setMasses(vec_double external_masses) 
{

  if (external_masses.size() != mME.size())
  {
    cerr <<  "Error in function 'setMasses' of class" << 
    " 'PY8MEs_R4_P123_sm_qb_llgqb': Incorrect number of" << 
    " masses specified." << endl; 
    throw PY8MEs_R4_P123_sm_qb_llgqb_exception; 
  }
  for (unsigned int j = 0; j < mME.size(); j++ )
  {
    mME[j] = external_masses[perm[j]]; 
  }
}

//--------------------------------------------------------------------------
// Getter accessing external masses with the correct ordering
vector<double> PY8MEs_R4_P123_sm_qb_llgqb::getMasses() 
{

  vec_double external_masses; 
  vector<int> invertedPerm; 
  invertedPerm = invert_mapping(perm); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    external_masses.push_back(mME[invertedPerm[i]]); 
  }
  return external_masses; 

}


// Set all values of the external masses to float(-mode) where mode can be
// 0 : Mass taken from the model
// 1 : Mass taken from p_i^2 if not massless to begin with
// 2 : Mass always taken from p_i^2.
void PY8MEs_R4_P123_sm_qb_llgqb::setExternalMassesMode(int mode) 
{
  if (mode != 0 && mode != 1 && mode != 2)
  {
    cerr <<  "Error in function 'setExternalMassesMode' of class" << 
    " 'PY8MEs_R4_P123_sm_qb_llgqb': Incorrect mode selected :" << mode << 
    ". It must be either 0, 1 or 2" << endl; 
    throw PY8MEs_R4_P123_sm_qb_llgqb_exception; 
  }
  if (mode == 0)
  {
    syncProcModelParams(); 
  }
  else if (mode == 1)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      if (mME[j] != pars->ZERO)
      {
        mME[j] = -1.0; 
      }
    }
  }
  else if (mode == 2)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      mME[j] = -1.0; 
    }
  }
}

//--------------------------------------------------------------------------
// Evaluate the squared matrix element.

double PY8MEs_R4_P123_sm_qb_llgqb::sigmaKin() 
{
  // Set the parameters which change event by event
  pars->setDependentParameters(); 
  pars->setDependentCouplings(); 
  // Reset color flows
  for(int i = 0; i < 4; i++ )
    jamp2[0][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[1][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[2][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[3][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[4][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[5][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[6][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[7][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[8][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[9][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[10][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[11][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[12][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[13][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[14][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[15][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[16][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[17][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[18][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[19][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[20][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[21][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[22][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[23][i] = 0.; 

  // Save previous values of mME
  vector<double> saved_mME(mME.size(), 0.0); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (mME[i] < 0.0)
    {
      saved_mME[i] = mME[i]; 
      mME[i] = sqrt(abs(pow(p[perm[i]][0], 2) - 
      (pow(p[perm[i]][1], 2) + pow(p[perm[i]][2], 2) + pow(p[perm[i]][3],
          2))));
    }
  }

  // Local variables and constants
  const int max_tries = 10; 
  vector < vec_bool > goodhel(nprocesses, vec_bool(ncomb, false)); 
  vec_int ntry(nprocesses, 0); 
  double t = 0.; 
  double result = 0.; 

  if (ntry[proc_ID] <= max_tries)
    ntry[proc_ID] = ntry[proc_ID] + 1; 

  // Find which helicity configuration is asked for
  // -1 indicates one wants to sum over helicities
  int user_ihel = getHelicityIDForConfig(user_helicities); 

  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = getColorIDForConfig(user_colors); 

  // Reset the list of results that will be recomputed here
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      all_results[proc_ID][ihel + 1][icolor + 1] = 0.; 
    }
  }

  // Calculate the matrix element for all helicities
  // unless already detected as vanishing
  for(int ihel = 0; ihel < ncomb; ihel++ )
  {
    // Skip helicity if already detected as vanishing
    if ((ntry[proc_ID] >= max_tries) && !goodhel[proc_ID][ihel])
      continue; 

    // Also skip helicity if user asks for a specific one
    if ((ntry[proc_ID] >= max_tries) && user_ihel != -1 && user_ihel != ihel)
      continue; 

    calculate_wavefunctions(helicities[ihel]); 

    // Reset locally computed color flows
    for(int i = 0; i < 4; i++ )
      jamp2[0][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[1][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[2][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[3][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[4][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[5][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[6][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[7][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[8][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[9][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[10][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[11][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[12][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[13][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[14][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[15][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[16][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[17][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[18][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[19][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[20][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[21][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[22][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[23][i] = 0.; 

    if (proc_ID == 0)
      t = matrix_4_ub_epemgub(); 
    if (proc_ID == 1)
      t = matrix_4_ubx_epemgubx(); 
    if (proc_ID == 2)
      t = matrix_4_db_epemgdb(); 
    if (proc_ID == 3)
      t = matrix_4_dbx_epemgdbx(); 
    if (proc_ID == 4)
      t = matrix_4_uxb_epemguxb(); 
    if (proc_ID == 5)
      t = matrix_4_uxbx_epemguxbx(); 
    if (proc_ID == 6)
      t = matrix_4_dxb_epemgdxb(); 
    if (proc_ID == 7)
      t = matrix_4_dxbx_epemgdxbx(); 
    if (proc_ID == 8)
      t = matrix_4_ub_vexvegub(); 
    if (proc_ID == 9)
      t = matrix_4_ubx_vexvegubx(); 
    if (proc_ID == 10)
      t = matrix_4_db_vexvegdb(); 
    if (proc_ID == 11)
      t = matrix_4_dbx_vexvegdbx(); 
    if (proc_ID == 12)
      t = matrix_4_uxb_vexveguxb(); 
    if (proc_ID == 13)
      t = matrix_4_uxbx_vexveguxbx(); 
    if (proc_ID == 14)
      t = matrix_4_dxb_vexvegdxb(); 
    if (proc_ID == 15)
      t = matrix_4_dxbx_vexvegdxbx(); 
    if (proc_ID == 16)
      t = matrix_4_ub_epvegdb(); 
    if (proc_ID == 17)
      t = matrix_4_ubx_epvegdbx(); 
    if (proc_ID == 18)
      t = matrix_4_db_vexemgub(); 
    if (proc_ID == 19)
      t = matrix_4_dbx_vexemgubx(); 
    if (proc_ID == 20)
      t = matrix_4_uxb_vexemgdxb(); 
    if (proc_ID == 21)
      t = matrix_4_uxbx_vexemgdxbx(); 
    if (proc_ID == 22)
      t = matrix_4_dxb_epveguxb(); 
    if (proc_ID == 23)
      t = matrix_4_dxbx_epveguxbx(); 

    // Store which helicities give non-zero result
    if ((ntry[proc_ID] < max_tries) && t != 0. && !goodhel[proc_ID][ihel])
      goodhel[proc_ID][ihel] = true; 

    // Aggregate results
    if (user_ihel == -1 || user_ihel == ihel)
    {
      if (user_icol == -1)
      {
        result = result + t; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][0] += t; 
          for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
          {
            all_results[proc_ID][0][i + 1] += jamp2[proc_ID][i]; 
          }
        }
        all_results[proc_ID][ihel + 1][0] += t; 
        for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
        {
          all_results[proc_ID][ihel + 1][i + 1] += jamp2[proc_ID][i]; 
        }
      }
      else
      {
        result = result + jamp2[proc_ID][user_icol]; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][user_icol + 1] += jamp2[proc_ID][user_icol]; 
        }
        all_results[proc_ID][ihel + 1][user_icol + 1] +=
            jamp2[proc_ID][user_icol];
      }
    }

  }

  // Normalize results with the identical particle factor
  if (include_symmetry_factors)
  {
    result = result/denom_iden[proc_ID]; 
  }
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      if (include_symmetry_factors)
      {
        all_results[proc_ID][ihel + 1][icolor + 1] /= denom_iden[proc_ID]; 
      }
    }
  }


  // Normalize when when summing+averaging over helicity configurations
  if (user_ihel == -1 && include_helicity_averaging_factors)
  {
    result /= denom_hels[proc_ID]; 
    if (user_icol == -1)
    {
      all_results[proc_ID][0][0] /= denom_hels[proc_ID]; 
      for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
      {
        all_results[proc_ID][0][i + 1] /= denom_hels[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][0][user_icol + 1] /= denom_hels[proc_ID]; 
    }
  }

  // Normalize when summing+averaging over color configurations
  if (user_icol == -1 && include_color_averaging_factors)
  {
    result /= denom_colors[proc_ID]; 
    if (user_ihel == -1)
    {
      all_results[proc_ID][0][0] /= denom_colors[proc_ID]; 
      for (unsigned int i = 0; i < ncomb; i++ )
      {
        all_results[proc_ID][i + 1][0] /= denom_colors[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][user_ihel + 1][0] /= denom_colors[proc_ID]; 
    }
  }

  // Reinstate previous values of mME
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (saved_mME[i] < 0.0)
    {
      mME[i] = saved_mME[i]; 
    }
  }

  // Finally return it
  return result; 
}

//==========================================================================
// Private class member functions

//--------------------------------------------------------------------------
// Evaluate |M|^2 for each subprocess

void PY8MEs_R4_P123_sm_qb_llgqb::calculate_wavefunctions(const int hel[])
{
  // Calculate wavefunctions for all processes
  // Calculate all wavefunctions
  ixxxxx(p[perm[0]], mME[0], hel[0], +1, w[0]); 
  ixxxxx(p[perm[1]], mME[1], hel[1], +1, w[1]); 
  ixxxxx(p[perm[2]], mME[2], hel[2], -1, w[2]); 
  oxxxxx(p[perm[3]], mME[3], hel[3], +1, w[3]); 
  vxxxxx(p[perm[4]], mME[4], hel[4], +1, w[4]); 
  oxxxxx(p[perm[5]], mME[5], hel[5], +1, w[5]); 
  oxxxxx(p[perm[6]], mME[6], hel[6], +1, w[6]); 
  FFV1_2(w[0], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[7]); 
  FFV1P0_3(w[2], w[3], pars->GC_3, pars->ZERO, pars->ZERO, w[8]); 
  FFV1P0_3(w[7], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[9]); 
  FFV1_1(w[6], w[8], pars->GC_1, pars->mdl_MB, pars->ZERO, w[10]); 
  FFV1_2(w[1], w[8], pars->GC_1, pars->mdl_MB, pars->ZERO, w[11]); 
  FFV2_4_3(w[2], w[3], pars->GC_50, pars->GC_59, pars->mdl_MZ, pars->mdl_WZ,
      w[12]);
  FFV2_3_1(w[6], w[12], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[13]);
  FFV2_3_2(w[1], w[12], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[14]);
  FFV1P0_3(w[1], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[15]); 
  FFV1_2(w[7], w[8], pars->GC_2, pars->ZERO, pars->ZERO, w[16]); 
  FFV1_2(w[7], w[15], pars->GC_11, pars->ZERO, pars->ZERO, w[17]); 
  FFV2_5_2(w[7], w[12], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[18]);
  FFV1_1(w[5], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[19]); 
  FFV1P0_3(w[0], w[19], pars->GC_11, pars->ZERO, pars->ZERO, w[20]); 
  FFV1_1(w[19], w[8], pars->GC_2, pars->ZERO, pars->ZERO, w[21]); 
  FFV1_1(w[19], w[15], pars->GC_11, pars->ZERO, pars->ZERO, w[22]); 
  FFV2_5_1(w[19], w[12], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[23]);
  FFV1_1(w[6], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[24]); 
  FFV1P0_3(w[0], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[25]); 
  FFV1_1(w[24], w[25], pars->GC_11, pars->mdl_MB, pars->ZERO, w[26]); 
  FFV1_1(w[24], w[8], pars->GC_1, pars->mdl_MB, pars->ZERO, w[27]); 
  FFV2_3_1(w[24], w[12], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[28]);
  FFV1P0_3(w[1], w[24], pars->GC_11, pars->ZERO, pars->ZERO, w[29]); 
  FFV1_2(w[0], w[8], pars->GC_2, pars->ZERO, pars->ZERO, w[30]); 
  FFV1_1(w[5], w[8], pars->GC_2, pars->ZERO, pars->ZERO, w[31]); 
  FFV2_5_2(w[0], w[12], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[32]);
  FFV2_5_1(w[5], w[12], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[33]);
  FFV1_2(w[1], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[34]); 
  FFV1_2(w[34], w[25], pars->GC_11, pars->mdl_MB, pars->ZERO, w[35]); 
  FFV1_2(w[34], w[8], pars->GC_1, pars->mdl_MB, pars->ZERO, w[36]); 
  FFV2_3_2(w[34], w[12], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[37]);
  FFV1P0_3(w[34], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[38]); 
  VVV1P0_1(w[4], w[25], pars->GC_10, pars->ZERO, pars->ZERO, w[39]); 
  FFV1_1(w[6], w[25], pars->GC_11, pars->mdl_MB, pars->ZERO, w[40]); 
  FFV1_2(w[1], w[25], pars->GC_11, pars->mdl_MB, pars->ZERO, w[41]); 
  VVV1P0_1(w[4], w[15], pars->GC_10, pars->ZERO, pars->ZERO, w[42]); 
  FFV1_1(w[5], w[15], pars->GC_11, pars->ZERO, pars->ZERO, w[43]); 
  FFV1_2(w[0], w[15], pars->GC_11, pars->ZERO, pars->ZERO, w[44]); 
  oxxxxx(p[perm[1]], mME[1], hel[1], -1, w[45]); 
  ixxxxx(p[perm[6]], mME[6], hel[6], -1, w[46]); 
  FFV1_1(w[45], w[8], pars->GC_1, pars->mdl_MB, pars->ZERO, w[47]); 
  FFV1_2(w[46], w[8], pars->GC_1, pars->mdl_MB, pars->ZERO, w[48]); 
  FFV2_3_1(w[45], w[12], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[49]);
  FFV2_3_2(w[46], w[12], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[50]);
  FFV1P0_3(w[46], w[45], pars->GC_11, pars->ZERO, pars->ZERO, w[51]); 
  FFV1_2(w[7], w[51], pars->GC_11, pars->ZERO, pars->ZERO, w[52]); 
  FFV1_1(w[19], w[51], pars->GC_11, pars->ZERO, pars->ZERO, w[53]); 
  FFV1_1(w[45], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[54]); 
  FFV1_1(w[54], w[25], pars->GC_11, pars->mdl_MB, pars->ZERO, w[55]); 
  FFV1_1(w[54], w[8], pars->GC_1, pars->mdl_MB, pars->ZERO, w[56]); 
  FFV2_3_1(w[54], w[12], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[57]);
  FFV1P0_3(w[46], w[54], pars->GC_11, pars->ZERO, pars->ZERO, w[58]); 
  FFV1_2(w[46], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[59]); 
  FFV1_2(w[59], w[25], pars->GC_11, pars->mdl_MB, pars->ZERO, w[60]); 
  FFV1_2(w[59], w[8], pars->GC_1, pars->mdl_MB, pars->ZERO, w[61]); 
  FFV2_3_2(w[59], w[12], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[62]);
  FFV1P0_3(w[59], w[45], pars->GC_11, pars->ZERO, pars->ZERO, w[63]); 
  FFV1_1(w[45], w[25], pars->GC_11, pars->mdl_MB, pars->ZERO, w[64]); 
  FFV1_2(w[46], w[25], pars->GC_11, pars->mdl_MB, pars->ZERO, w[65]); 
  VVV1P0_1(w[4], w[51], pars->GC_10, pars->ZERO, pars->ZERO, w[66]); 
  FFV1_1(w[5], w[51], pars->GC_11, pars->ZERO, pars->ZERO, w[67]); 
  FFV1_2(w[0], w[51], pars->GC_11, pars->ZERO, pars->ZERO, w[68]); 
  FFV1_2(w[7], w[8], pars->GC_1, pars->ZERO, pars->ZERO, w[69]); 
  FFV2_3_2(w[7], w[12], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[70]);
  FFV1_1(w[19], w[8], pars->GC_1, pars->ZERO, pars->ZERO, w[71]); 
  FFV2_3_1(w[19], w[12], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[72]);
  FFV1_2(w[0], w[8], pars->GC_1, pars->ZERO, pars->ZERO, w[73]); 
  FFV1_1(w[5], w[8], pars->GC_1, pars->ZERO, pars->ZERO, w[74]); 
  FFV2_3_2(w[0], w[12], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[75]);
  FFV2_3_1(w[5], w[12], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[76]);
  oxxxxx(p[perm[0]], mME[0], hel[0], -1, w[77]); 
  ixxxxx(p[perm[5]], mME[5], hel[5], -1, w[78]); 
  FFV1_2(w[78], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[79]); 
  FFV1P0_3(w[79], w[77], pars->GC_11, pars->ZERO, pars->ZERO, w[80]); 
  FFV1_2(w[79], w[8], pars->GC_2, pars->ZERO, pars->ZERO, w[81]); 
  FFV1_2(w[79], w[15], pars->GC_11, pars->ZERO, pars->ZERO, w[82]); 
  FFV2_5_2(w[79], w[12], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[83]);
  FFV1_1(w[77], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[84]); 
  FFV1P0_3(w[78], w[84], pars->GC_11, pars->ZERO, pars->ZERO, w[85]); 
  FFV1_1(w[84], w[8], pars->GC_2, pars->ZERO, pars->ZERO, w[86]); 
  FFV1_1(w[84], w[15], pars->GC_11, pars->ZERO, pars->ZERO, w[87]); 
  FFV2_5_1(w[84], w[12], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[88]);
  FFV1P0_3(w[78], w[77], pars->GC_11, pars->ZERO, pars->ZERO, w[89]); 
  FFV1_1(w[24], w[89], pars->GC_11, pars->mdl_MB, pars->ZERO, w[90]); 
  FFV1_2(w[78], w[8], pars->GC_2, pars->ZERO, pars->ZERO, w[91]); 
  FFV1_1(w[77], w[8], pars->GC_2, pars->ZERO, pars->ZERO, w[92]); 
  FFV2_5_2(w[78], w[12], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[93]);
  FFV2_5_1(w[77], w[12], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[94]);
  FFV1_2(w[34], w[89], pars->GC_11, pars->mdl_MB, pars->ZERO, w[95]); 
  VVV1P0_1(w[4], w[89], pars->GC_10, pars->ZERO, pars->ZERO, w[96]); 
  FFV1_1(w[6], w[89], pars->GC_11, pars->mdl_MB, pars->ZERO, w[97]); 
  FFV1_2(w[1], w[89], pars->GC_11, pars->mdl_MB, pars->ZERO, w[98]); 
  FFV1_1(w[77], w[15], pars->GC_11, pars->ZERO, pars->ZERO, w[99]); 
  FFV1_2(w[78], w[15], pars->GC_11, pars->ZERO, pars->ZERO, w[100]); 
  FFV1_2(w[79], w[51], pars->GC_11, pars->ZERO, pars->ZERO, w[101]); 
  FFV1_1(w[84], w[51], pars->GC_11, pars->ZERO, pars->ZERO, w[102]); 
  FFV1_1(w[54], w[89], pars->GC_11, pars->mdl_MB, pars->ZERO, w[103]); 
  FFV1_2(w[59], w[89], pars->GC_11, pars->mdl_MB, pars->ZERO, w[104]); 
  FFV1_1(w[45], w[89], pars->GC_11, pars->mdl_MB, pars->ZERO, w[105]); 
  FFV1_2(w[46], w[89], pars->GC_11, pars->mdl_MB, pars->ZERO, w[106]); 
  FFV1_1(w[77], w[51], pars->GC_11, pars->ZERO, pars->ZERO, w[107]); 
  FFV1_2(w[78], w[51], pars->GC_11, pars->ZERO, pars->ZERO, w[108]); 
  FFV1_2(w[79], w[8], pars->GC_1, pars->ZERO, pars->ZERO, w[109]); 
  FFV2_3_2(w[79], w[12], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[110]);
  FFV1_1(w[84], w[8], pars->GC_1, pars->ZERO, pars->ZERO, w[111]); 
  FFV2_3_1(w[84], w[12], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[112]);
  FFV1_2(w[78], w[8], pars->GC_1, pars->ZERO, pars->ZERO, w[113]); 
  FFV1_1(w[77], w[8], pars->GC_1, pars->ZERO, pars->ZERO, w[114]); 
  FFV2_3_2(w[78], w[12], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[115]);
  FFV2_3_1(w[77], w[12], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[116]);
  FFV2_3(w[2], w[3], pars->GC_62, pars->mdl_MZ, pars->mdl_WZ, w[117]); 
  FFV2_3_1(w[6], w[117], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[118]);
  FFV2_3_2(w[1], w[117], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[119]);
  FFV2_5_2(w[7], w[117], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[120]);
  FFV2_5_1(w[19], w[117], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[121]);
  FFV2_3_1(w[24], w[117], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[122]);
  FFV2_5_2(w[0], w[117], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[123]);
  FFV2_5_1(w[5], w[117], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[124]);
  FFV2_3_2(w[34], w[117], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[125]);
  FFV2_3_1(w[45], w[117], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[126]);
  FFV2_3_2(w[46], w[117], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[127]);
  FFV2_3_1(w[54], w[117], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[128]);
  FFV2_3_2(w[59], w[117], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[129]);
  FFV2_3_2(w[7], w[117], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[130]);
  FFV2_3_1(w[19], w[117], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[131]);
  FFV2_3_2(w[0], w[117], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[132]);
  FFV2_3_1(w[5], w[117], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[133]);
  FFV2_5_2(w[79], w[117], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[134]);
  FFV2_5_1(w[84], w[117], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[135]);
  FFV2_5_2(w[78], w[117], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[136]);
  FFV2_5_1(w[77], w[117], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[137]);
  FFV2_3_2(w[79], w[117], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[138]);
  FFV2_3_1(w[84], w[117], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[139]);
  FFV2_3_2(w[78], w[117], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[140]);
  FFV2_3_1(w[77], w[117], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[141]);
  FFV2_3(w[2], w[3], pars->GC_100, pars->mdl_MW, pars->mdl_WW, w[142]); 
  FFV2_2(w[7], w[142], pars->GC_100, pars->ZERO, pars->ZERO, w[143]); 
  FFV2_1(w[19], w[142], pars->GC_100, pars->ZERO, pars->ZERO, w[144]); 
  FFV2_2(w[0], w[142], pars->GC_100, pars->ZERO, pars->ZERO, w[145]); 
  FFV2_1(w[5], w[142], pars->GC_100, pars->ZERO, pars->ZERO, w[146]); 
  FFV2_2(w[79], w[142], pars->GC_100, pars->ZERO, pars->ZERO, w[147]); 
  FFV2_1(w[84], w[142], pars->GC_100, pars->ZERO, pars->ZERO, w[148]); 
  FFV2_2(w[78], w[142], pars->GC_100, pars->ZERO, pars->ZERO, w[149]); 
  FFV2_1(w[77], w[142], pars->GC_100, pars->ZERO, pars->ZERO, w[150]); 

  // Calculate all amplitudes
  // Amplitude(s) for diagram number 0
  FFV1_0(w[1], w[10], w[9], pars->GC_11, amp[0]); 
  FFV1_0(w[11], w[6], w[9], pars->GC_11, amp[1]); 
  FFV1_0(w[1], w[13], w[9], pars->GC_11, amp[2]); 
  FFV1_0(w[14], w[6], w[9], pars->GC_11, amp[3]); 
  FFV1_0(w[16], w[5], w[15], pars->GC_11, amp[4]); 
  FFV1_0(w[17], w[5], w[8], pars->GC_2, amp[5]); 
  FFV1_0(w[18], w[5], w[15], pars->GC_11, amp[6]); 
  FFV2_5_0(w[17], w[5], w[12], pars->GC_51, pars->GC_58, amp[7]); 
  FFV1_0(w[1], w[10], w[20], pars->GC_11, amp[8]); 
  FFV1_0(w[11], w[6], w[20], pars->GC_11, amp[9]); 
  FFV1_0(w[1], w[13], w[20], pars->GC_11, amp[10]); 
  FFV1_0(w[14], w[6], w[20], pars->GC_11, amp[11]); 
  FFV1_0(w[0], w[21], w[15], pars->GC_11, amp[12]); 
  FFV1_0(w[0], w[22], w[8], pars->GC_2, amp[13]); 
  FFV1_0(w[0], w[23], w[15], pars->GC_11, amp[14]); 
  FFV2_5_0(w[0], w[22], w[12], pars->GC_51, pars->GC_58, amp[15]); 
  FFV1_0(w[1], w[26], w[8], pars->GC_1, amp[16]); 
  FFV1_0(w[1], w[27], w[25], pars->GC_11, amp[17]); 
  FFV2_3_0(w[1], w[26], w[12], pars->GC_50, pars->GC_58, amp[18]); 
  FFV1_0(w[1], w[28], w[25], pars->GC_11, amp[19]); 
  FFV1_0(w[30], w[5], w[29], pars->GC_11, amp[20]); 
  FFV1_0(w[0], w[31], w[29], pars->GC_11, amp[21]); 
  FFV1_0(w[32], w[5], w[29], pars->GC_11, amp[22]); 
  FFV1_0(w[0], w[33], w[29], pars->GC_11, amp[23]); 
  FFV1_0(w[35], w[6], w[8], pars->GC_1, amp[24]); 
  FFV1_0(w[36], w[6], w[25], pars->GC_11, amp[25]); 
  FFV2_3_0(w[35], w[6], w[12], pars->GC_50, pars->GC_58, amp[26]); 
  FFV1_0(w[37], w[6], w[25], pars->GC_11, amp[27]); 
  FFV1_0(w[30], w[5], w[38], pars->GC_11, amp[28]); 
  FFV1_0(w[0], w[31], w[38], pars->GC_11, amp[29]); 
  FFV1_0(w[32], w[5], w[38], pars->GC_11, amp[30]); 
  FFV1_0(w[0], w[33], w[38], pars->GC_11, amp[31]); 
  FFV1_0(w[1], w[10], w[39], pars->GC_11, amp[32]); 
  FFV1_0(w[11], w[6], w[39], pars->GC_11, amp[33]); 
  FFV1_0(w[11], w[40], w[4], pars->GC_11, amp[34]); 
  FFV1_0(w[41], w[10], w[4], pars->GC_11, amp[35]); 
  FFV1_0(w[1], w[13], w[39], pars->GC_11, amp[36]); 
  FFV1_0(w[14], w[6], w[39], pars->GC_11, amp[37]); 
  FFV1_0(w[14], w[40], w[4], pars->GC_11, amp[38]); 
  FFV1_0(w[41], w[13], w[4], pars->GC_11, amp[39]); 
  FFV1_0(w[30], w[5], w[42], pars->GC_11, amp[40]); 
  FFV1_0(w[0], w[31], w[42], pars->GC_11, amp[41]); 
  FFV1_0(w[30], w[43], w[4], pars->GC_11, amp[42]); 
  FFV1_0(w[44], w[31], w[4], pars->GC_11, amp[43]); 
  FFV1_0(w[32], w[5], w[42], pars->GC_11, amp[44]); 
  FFV1_0(w[0], w[33], w[42], pars->GC_11, amp[45]); 
  FFV1_0(w[32], w[43], w[4], pars->GC_11, amp[46]); 
  FFV1_0(w[44], w[33], w[4], pars->GC_11, amp[47]); 
  FFV1_0(w[46], w[47], w[9], pars->GC_11, amp[48]); 
  FFV1_0(w[48], w[45], w[9], pars->GC_11, amp[49]); 
  FFV1_0(w[46], w[49], w[9], pars->GC_11, amp[50]); 
  FFV1_0(w[50], w[45], w[9], pars->GC_11, amp[51]); 
  FFV1_0(w[16], w[5], w[51], pars->GC_11, amp[52]); 
  FFV1_0(w[52], w[5], w[8], pars->GC_2, amp[53]); 
  FFV1_0(w[18], w[5], w[51], pars->GC_11, amp[54]); 
  FFV2_5_0(w[52], w[5], w[12], pars->GC_51, pars->GC_58, amp[55]); 
  FFV1_0(w[46], w[47], w[20], pars->GC_11, amp[56]); 
  FFV1_0(w[48], w[45], w[20], pars->GC_11, amp[57]); 
  FFV1_0(w[46], w[49], w[20], pars->GC_11, amp[58]); 
  FFV1_0(w[50], w[45], w[20], pars->GC_11, amp[59]); 
  FFV1_0(w[0], w[21], w[51], pars->GC_11, amp[60]); 
  FFV1_0(w[0], w[53], w[8], pars->GC_2, amp[61]); 
  FFV1_0(w[0], w[23], w[51], pars->GC_11, amp[62]); 
  FFV2_5_0(w[0], w[53], w[12], pars->GC_51, pars->GC_58, amp[63]); 
  FFV1_0(w[46], w[55], w[8], pars->GC_1, amp[64]); 
  FFV1_0(w[46], w[56], w[25], pars->GC_11, amp[65]); 
  FFV2_3_0(w[46], w[55], w[12], pars->GC_50, pars->GC_58, amp[66]); 
  FFV1_0(w[46], w[57], w[25], pars->GC_11, amp[67]); 
  FFV1_0(w[30], w[5], w[58], pars->GC_11, amp[68]); 
  FFV1_0(w[0], w[31], w[58], pars->GC_11, amp[69]); 
  FFV1_0(w[32], w[5], w[58], pars->GC_11, amp[70]); 
  FFV1_0(w[0], w[33], w[58], pars->GC_11, amp[71]); 
  FFV1_0(w[60], w[45], w[8], pars->GC_1, amp[72]); 
  FFV1_0(w[61], w[45], w[25], pars->GC_11, amp[73]); 
  FFV2_3_0(w[60], w[45], w[12], pars->GC_50, pars->GC_58, amp[74]); 
  FFV1_0(w[62], w[45], w[25], pars->GC_11, amp[75]); 
  FFV1_0(w[30], w[5], w[63], pars->GC_11, amp[76]); 
  FFV1_0(w[0], w[31], w[63], pars->GC_11, amp[77]); 
  FFV1_0(w[32], w[5], w[63], pars->GC_11, amp[78]); 
  FFV1_0(w[0], w[33], w[63], pars->GC_11, amp[79]); 
  FFV1_0(w[46], w[47], w[39], pars->GC_11, amp[80]); 
  FFV1_0(w[48], w[45], w[39], pars->GC_11, amp[81]); 
  FFV1_0(w[48], w[64], w[4], pars->GC_11, amp[82]); 
  FFV1_0(w[65], w[47], w[4], pars->GC_11, amp[83]); 
  FFV1_0(w[46], w[49], w[39], pars->GC_11, amp[84]); 
  FFV1_0(w[50], w[45], w[39], pars->GC_11, amp[85]); 
  FFV1_0(w[50], w[64], w[4], pars->GC_11, amp[86]); 
  FFV1_0(w[65], w[49], w[4], pars->GC_11, amp[87]); 
  FFV1_0(w[30], w[5], w[66], pars->GC_11, amp[88]); 
  FFV1_0(w[0], w[31], w[66], pars->GC_11, amp[89]); 
  FFV1_0(w[30], w[67], w[4], pars->GC_11, amp[90]); 
  FFV1_0(w[68], w[31], w[4], pars->GC_11, amp[91]); 
  FFV1_0(w[32], w[5], w[66], pars->GC_11, amp[92]); 
  FFV1_0(w[0], w[33], w[66], pars->GC_11, amp[93]); 
  FFV1_0(w[32], w[67], w[4], pars->GC_11, amp[94]); 
  FFV1_0(w[68], w[33], w[4], pars->GC_11, amp[95]); 
  FFV1_0(w[69], w[5], w[15], pars->GC_11, amp[96]); 
  FFV1_0(w[17], w[5], w[8], pars->GC_1, amp[97]); 
  FFV1_0(w[70], w[5], w[15], pars->GC_11, amp[98]); 
  FFV2_3_0(w[17], w[5], w[12], pars->GC_50, pars->GC_58, amp[99]); 
  FFV1_0(w[0], w[71], w[15], pars->GC_11, amp[100]); 
  FFV1_0(w[0], w[22], w[8], pars->GC_1, amp[101]); 
  FFV1_0(w[0], w[72], w[15], pars->GC_11, amp[102]); 
  FFV2_3_0(w[0], w[22], w[12], pars->GC_50, pars->GC_58, amp[103]); 
  FFV1_0(w[73], w[5], w[29], pars->GC_11, amp[104]); 
  FFV1_0(w[0], w[74], w[29], pars->GC_11, amp[105]); 
  FFV1_0(w[75], w[5], w[29], pars->GC_11, amp[106]); 
  FFV1_0(w[0], w[76], w[29], pars->GC_11, amp[107]); 
  FFV1_0(w[73], w[5], w[38], pars->GC_11, amp[108]); 
  FFV1_0(w[0], w[74], w[38], pars->GC_11, amp[109]); 
  FFV1_0(w[75], w[5], w[38], pars->GC_11, amp[110]); 
  FFV1_0(w[0], w[76], w[38], pars->GC_11, amp[111]); 
  FFV1_0(w[73], w[5], w[42], pars->GC_11, amp[112]); 
  FFV1_0(w[0], w[74], w[42], pars->GC_11, amp[113]); 
  FFV1_0(w[73], w[43], w[4], pars->GC_11, amp[114]); 
  FFV1_0(w[44], w[74], w[4], pars->GC_11, amp[115]); 
  FFV1_0(w[75], w[5], w[42], pars->GC_11, amp[116]); 
  FFV1_0(w[0], w[76], w[42], pars->GC_11, amp[117]); 
  FFV1_0(w[75], w[43], w[4], pars->GC_11, amp[118]); 
  FFV1_0(w[44], w[76], w[4], pars->GC_11, amp[119]); 
  FFV1_0(w[46], w[47], w[9], pars->GC_11, amp[120]); 
  FFV1_0(w[48], w[45], w[9], pars->GC_11, amp[121]); 
  FFV1_0(w[46], w[49], w[9], pars->GC_11, amp[122]); 
  FFV1_0(w[50], w[45], w[9], pars->GC_11, amp[123]); 
  FFV1_0(w[69], w[5], w[51], pars->GC_11, amp[124]); 
  FFV1_0(w[52], w[5], w[8], pars->GC_1, amp[125]); 
  FFV1_0(w[70], w[5], w[51], pars->GC_11, amp[126]); 
  FFV2_3_0(w[52], w[5], w[12], pars->GC_50, pars->GC_58, amp[127]); 
  FFV1_0(w[46], w[47], w[20], pars->GC_11, amp[128]); 
  FFV1_0(w[48], w[45], w[20], pars->GC_11, amp[129]); 
  FFV1_0(w[46], w[49], w[20], pars->GC_11, amp[130]); 
  FFV1_0(w[50], w[45], w[20], pars->GC_11, amp[131]); 
  FFV1_0(w[0], w[71], w[51], pars->GC_11, amp[132]); 
  FFV1_0(w[0], w[53], w[8], pars->GC_1, amp[133]); 
  FFV1_0(w[0], w[72], w[51], pars->GC_11, amp[134]); 
  FFV2_3_0(w[0], w[53], w[12], pars->GC_50, pars->GC_58, amp[135]); 
  FFV1_0(w[46], w[55], w[8], pars->GC_1, amp[136]); 
  FFV1_0(w[46], w[56], w[25], pars->GC_11, amp[137]); 
  FFV2_3_0(w[46], w[55], w[12], pars->GC_50, pars->GC_58, amp[138]); 
  FFV1_0(w[46], w[57], w[25], pars->GC_11, amp[139]); 
  FFV1_0(w[73], w[5], w[58], pars->GC_11, amp[140]); 
  FFV1_0(w[0], w[74], w[58], pars->GC_11, amp[141]); 
  FFV1_0(w[75], w[5], w[58], pars->GC_11, amp[142]); 
  FFV1_0(w[0], w[76], w[58], pars->GC_11, amp[143]); 
  FFV1_0(w[60], w[45], w[8], pars->GC_1, amp[144]); 
  FFV1_0(w[61], w[45], w[25], pars->GC_11, amp[145]); 
  FFV2_3_0(w[60], w[45], w[12], pars->GC_50, pars->GC_58, amp[146]); 
  FFV1_0(w[62], w[45], w[25], pars->GC_11, amp[147]); 
  FFV1_0(w[73], w[5], w[63], pars->GC_11, amp[148]); 
  FFV1_0(w[0], w[74], w[63], pars->GC_11, amp[149]); 
  FFV1_0(w[75], w[5], w[63], pars->GC_11, amp[150]); 
  FFV1_0(w[0], w[76], w[63], pars->GC_11, amp[151]); 
  FFV1_0(w[46], w[47], w[39], pars->GC_11, amp[152]); 
  FFV1_0(w[48], w[45], w[39], pars->GC_11, amp[153]); 
  FFV1_0(w[48], w[64], w[4], pars->GC_11, amp[154]); 
  FFV1_0(w[65], w[47], w[4], pars->GC_11, amp[155]); 
  FFV1_0(w[46], w[49], w[39], pars->GC_11, amp[156]); 
  FFV1_0(w[50], w[45], w[39], pars->GC_11, amp[157]); 
  FFV1_0(w[50], w[64], w[4], pars->GC_11, amp[158]); 
  FFV1_0(w[65], w[49], w[4], pars->GC_11, amp[159]); 
  FFV1_0(w[73], w[5], w[66], pars->GC_11, amp[160]); 
  FFV1_0(w[0], w[74], w[66], pars->GC_11, amp[161]); 
  FFV1_0(w[73], w[67], w[4], pars->GC_11, amp[162]); 
  FFV1_0(w[68], w[74], w[4], pars->GC_11, amp[163]); 
  FFV1_0(w[75], w[5], w[66], pars->GC_11, amp[164]); 
  FFV1_0(w[0], w[76], w[66], pars->GC_11, amp[165]); 
  FFV1_0(w[75], w[67], w[4], pars->GC_11, amp[166]); 
  FFV1_0(w[68], w[76], w[4], pars->GC_11, amp[167]); 
  FFV1_0(w[1], w[10], w[80], pars->GC_11, amp[168]); 
  FFV1_0(w[11], w[6], w[80], pars->GC_11, amp[169]); 
  FFV1_0(w[1], w[13], w[80], pars->GC_11, amp[170]); 
  FFV1_0(w[14], w[6], w[80], pars->GC_11, amp[171]); 
  FFV1_0(w[81], w[77], w[15], pars->GC_11, amp[172]); 
  FFV1_0(w[82], w[77], w[8], pars->GC_2, amp[173]); 
  FFV1_0(w[83], w[77], w[15], pars->GC_11, amp[174]); 
  FFV2_5_0(w[82], w[77], w[12], pars->GC_51, pars->GC_58, amp[175]); 
  FFV1_0(w[1], w[10], w[85], pars->GC_11, amp[176]); 
  FFV1_0(w[11], w[6], w[85], pars->GC_11, amp[177]); 
  FFV1_0(w[1], w[13], w[85], pars->GC_11, amp[178]); 
  FFV1_0(w[14], w[6], w[85], pars->GC_11, amp[179]); 
  FFV1_0(w[78], w[86], w[15], pars->GC_11, amp[180]); 
  FFV1_0(w[78], w[87], w[8], pars->GC_2, amp[181]); 
  FFV1_0(w[78], w[88], w[15], pars->GC_11, amp[182]); 
  FFV2_5_0(w[78], w[87], w[12], pars->GC_51, pars->GC_58, amp[183]); 
  FFV1_0(w[1], w[90], w[8], pars->GC_1, amp[184]); 
  FFV1_0(w[1], w[27], w[89], pars->GC_11, amp[185]); 
  FFV2_3_0(w[1], w[90], w[12], pars->GC_50, pars->GC_58, amp[186]); 
  FFV1_0(w[1], w[28], w[89], pars->GC_11, amp[187]); 
  FFV1_0(w[91], w[77], w[29], pars->GC_11, amp[188]); 
  FFV1_0(w[78], w[92], w[29], pars->GC_11, amp[189]); 
  FFV1_0(w[93], w[77], w[29], pars->GC_11, amp[190]); 
  FFV1_0(w[78], w[94], w[29], pars->GC_11, amp[191]); 
  FFV1_0(w[95], w[6], w[8], pars->GC_1, amp[192]); 
  FFV1_0(w[36], w[6], w[89], pars->GC_11, amp[193]); 
  FFV2_3_0(w[95], w[6], w[12], pars->GC_50, pars->GC_58, amp[194]); 
  FFV1_0(w[37], w[6], w[89], pars->GC_11, amp[195]); 
  FFV1_0(w[91], w[77], w[38], pars->GC_11, amp[196]); 
  FFV1_0(w[78], w[92], w[38], pars->GC_11, amp[197]); 
  FFV1_0(w[93], w[77], w[38], pars->GC_11, amp[198]); 
  FFV1_0(w[78], w[94], w[38], pars->GC_11, amp[199]); 
  FFV1_0(w[1], w[10], w[96], pars->GC_11, amp[200]); 
  FFV1_0(w[11], w[6], w[96], pars->GC_11, amp[201]); 
  FFV1_0(w[11], w[97], w[4], pars->GC_11, amp[202]); 
  FFV1_0(w[98], w[10], w[4], pars->GC_11, amp[203]); 
  FFV1_0(w[1], w[13], w[96], pars->GC_11, amp[204]); 
  FFV1_0(w[14], w[6], w[96], pars->GC_11, amp[205]); 
  FFV1_0(w[14], w[97], w[4], pars->GC_11, amp[206]); 
  FFV1_0(w[98], w[13], w[4], pars->GC_11, amp[207]); 
  FFV1_0(w[91], w[77], w[42], pars->GC_11, amp[208]); 
  FFV1_0(w[78], w[92], w[42], pars->GC_11, amp[209]); 
  FFV1_0(w[91], w[99], w[4], pars->GC_11, amp[210]); 
  FFV1_0(w[100], w[92], w[4], pars->GC_11, amp[211]); 
  FFV1_0(w[93], w[77], w[42], pars->GC_11, amp[212]); 
  FFV1_0(w[78], w[94], w[42], pars->GC_11, amp[213]); 
  FFV1_0(w[93], w[99], w[4], pars->GC_11, amp[214]); 
  FFV1_0(w[100], w[94], w[4], pars->GC_11, amp[215]); 
  FFV1_0(w[46], w[47], w[80], pars->GC_11, amp[216]); 
  FFV1_0(w[48], w[45], w[80], pars->GC_11, amp[217]); 
  FFV1_0(w[46], w[49], w[80], pars->GC_11, amp[218]); 
  FFV1_0(w[50], w[45], w[80], pars->GC_11, amp[219]); 
  FFV1_0(w[81], w[77], w[51], pars->GC_11, amp[220]); 
  FFV1_0(w[101], w[77], w[8], pars->GC_2, amp[221]); 
  FFV1_0(w[83], w[77], w[51], pars->GC_11, amp[222]); 
  FFV2_5_0(w[101], w[77], w[12], pars->GC_51, pars->GC_58, amp[223]); 
  FFV1_0(w[46], w[47], w[85], pars->GC_11, amp[224]); 
  FFV1_0(w[48], w[45], w[85], pars->GC_11, amp[225]); 
  FFV1_0(w[46], w[49], w[85], pars->GC_11, amp[226]); 
  FFV1_0(w[50], w[45], w[85], pars->GC_11, amp[227]); 
  FFV1_0(w[78], w[86], w[51], pars->GC_11, amp[228]); 
  FFV1_0(w[78], w[102], w[8], pars->GC_2, amp[229]); 
  FFV1_0(w[78], w[88], w[51], pars->GC_11, amp[230]); 
  FFV2_5_0(w[78], w[102], w[12], pars->GC_51, pars->GC_58, amp[231]); 
  FFV1_0(w[46], w[103], w[8], pars->GC_1, amp[232]); 
  FFV1_0(w[46], w[56], w[89], pars->GC_11, amp[233]); 
  FFV2_3_0(w[46], w[103], w[12], pars->GC_50, pars->GC_58, amp[234]); 
  FFV1_0(w[46], w[57], w[89], pars->GC_11, amp[235]); 
  FFV1_0(w[91], w[77], w[58], pars->GC_11, amp[236]); 
  FFV1_0(w[78], w[92], w[58], pars->GC_11, amp[237]); 
  FFV1_0(w[93], w[77], w[58], pars->GC_11, amp[238]); 
  FFV1_0(w[78], w[94], w[58], pars->GC_11, amp[239]); 
  FFV1_0(w[104], w[45], w[8], pars->GC_1, amp[240]); 
  FFV1_0(w[61], w[45], w[89], pars->GC_11, amp[241]); 
  FFV2_3_0(w[104], w[45], w[12], pars->GC_50, pars->GC_58, amp[242]); 
  FFV1_0(w[62], w[45], w[89], pars->GC_11, amp[243]); 
  FFV1_0(w[91], w[77], w[63], pars->GC_11, amp[244]); 
  FFV1_0(w[78], w[92], w[63], pars->GC_11, amp[245]); 
  FFV1_0(w[93], w[77], w[63], pars->GC_11, amp[246]); 
  FFV1_0(w[78], w[94], w[63], pars->GC_11, amp[247]); 
  FFV1_0(w[46], w[47], w[96], pars->GC_11, amp[248]); 
  FFV1_0(w[48], w[45], w[96], pars->GC_11, amp[249]); 
  FFV1_0(w[48], w[105], w[4], pars->GC_11, amp[250]); 
  FFV1_0(w[106], w[47], w[4], pars->GC_11, amp[251]); 
  FFV1_0(w[46], w[49], w[96], pars->GC_11, amp[252]); 
  FFV1_0(w[50], w[45], w[96], pars->GC_11, amp[253]); 
  FFV1_0(w[50], w[105], w[4], pars->GC_11, amp[254]); 
  FFV1_0(w[106], w[49], w[4], pars->GC_11, amp[255]); 
  FFV1_0(w[91], w[77], w[66], pars->GC_11, amp[256]); 
  FFV1_0(w[78], w[92], w[66], pars->GC_11, amp[257]); 
  FFV1_0(w[91], w[107], w[4], pars->GC_11, amp[258]); 
  FFV1_0(w[108], w[92], w[4], pars->GC_11, amp[259]); 
  FFV1_0(w[93], w[77], w[66], pars->GC_11, amp[260]); 
  FFV1_0(w[78], w[94], w[66], pars->GC_11, amp[261]); 
  FFV1_0(w[93], w[107], w[4], pars->GC_11, amp[262]); 
  FFV1_0(w[108], w[94], w[4], pars->GC_11, amp[263]); 
  FFV1_0(w[1], w[10], w[80], pars->GC_11, amp[264]); 
  FFV1_0(w[11], w[6], w[80], pars->GC_11, amp[265]); 
  FFV1_0(w[1], w[13], w[80], pars->GC_11, amp[266]); 
  FFV1_0(w[14], w[6], w[80], pars->GC_11, amp[267]); 
  FFV1_0(w[109], w[77], w[15], pars->GC_11, amp[268]); 
  FFV1_0(w[82], w[77], w[8], pars->GC_1, amp[269]); 
  FFV1_0(w[110], w[77], w[15], pars->GC_11, amp[270]); 
  FFV2_3_0(w[82], w[77], w[12], pars->GC_50, pars->GC_58, amp[271]); 
  FFV1_0(w[1], w[10], w[85], pars->GC_11, amp[272]); 
  FFV1_0(w[11], w[6], w[85], pars->GC_11, amp[273]); 
  FFV1_0(w[1], w[13], w[85], pars->GC_11, amp[274]); 
  FFV1_0(w[14], w[6], w[85], pars->GC_11, amp[275]); 
  FFV1_0(w[78], w[111], w[15], pars->GC_11, amp[276]); 
  FFV1_0(w[78], w[87], w[8], pars->GC_1, amp[277]); 
  FFV1_0(w[78], w[112], w[15], pars->GC_11, amp[278]); 
  FFV2_3_0(w[78], w[87], w[12], pars->GC_50, pars->GC_58, amp[279]); 
  FFV1_0(w[1], w[90], w[8], pars->GC_1, amp[280]); 
  FFV1_0(w[1], w[27], w[89], pars->GC_11, amp[281]); 
  FFV2_3_0(w[1], w[90], w[12], pars->GC_50, pars->GC_58, amp[282]); 
  FFV1_0(w[1], w[28], w[89], pars->GC_11, amp[283]); 
  FFV1_0(w[113], w[77], w[29], pars->GC_11, amp[284]); 
  FFV1_0(w[78], w[114], w[29], pars->GC_11, amp[285]); 
  FFV1_0(w[115], w[77], w[29], pars->GC_11, amp[286]); 
  FFV1_0(w[78], w[116], w[29], pars->GC_11, amp[287]); 
  FFV1_0(w[95], w[6], w[8], pars->GC_1, amp[288]); 
  FFV1_0(w[36], w[6], w[89], pars->GC_11, amp[289]); 
  FFV2_3_0(w[95], w[6], w[12], pars->GC_50, pars->GC_58, amp[290]); 
  FFV1_0(w[37], w[6], w[89], pars->GC_11, amp[291]); 
  FFV1_0(w[113], w[77], w[38], pars->GC_11, amp[292]); 
  FFV1_0(w[78], w[114], w[38], pars->GC_11, amp[293]); 
  FFV1_0(w[115], w[77], w[38], pars->GC_11, amp[294]); 
  FFV1_0(w[78], w[116], w[38], pars->GC_11, amp[295]); 
  FFV1_0(w[1], w[10], w[96], pars->GC_11, amp[296]); 
  FFV1_0(w[11], w[6], w[96], pars->GC_11, amp[297]); 
  FFV1_0(w[11], w[97], w[4], pars->GC_11, amp[298]); 
  FFV1_0(w[98], w[10], w[4], pars->GC_11, amp[299]); 
  FFV1_0(w[1], w[13], w[96], pars->GC_11, amp[300]); 
  FFV1_0(w[14], w[6], w[96], pars->GC_11, amp[301]); 
  FFV1_0(w[14], w[97], w[4], pars->GC_11, amp[302]); 
  FFV1_0(w[98], w[13], w[4], pars->GC_11, amp[303]); 
  FFV1_0(w[113], w[77], w[42], pars->GC_11, amp[304]); 
  FFV1_0(w[78], w[114], w[42], pars->GC_11, amp[305]); 
  FFV1_0(w[113], w[99], w[4], pars->GC_11, amp[306]); 
  FFV1_0(w[100], w[114], w[4], pars->GC_11, amp[307]); 
  FFV1_0(w[115], w[77], w[42], pars->GC_11, amp[308]); 
  FFV1_0(w[78], w[116], w[42], pars->GC_11, amp[309]); 
  FFV1_0(w[115], w[99], w[4], pars->GC_11, amp[310]); 
  FFV1_0(w[100], w[116], w[4], pars->GC_11, amp[311]); 
  FFV1_0(w[46], w[47], w[80], pars->GC_11, amp[312]); 
  FFV1_0(w[48], w[45], w[80], pars->GC_11, amp[313]); 
  FFV1_0(w[46], w[49], w[80], pars->GC_11, amp[314]); 
  FFV1_0(w[50], w[45], w[80], pars->GC_11, amp[315]); 
  FFV1_0(w[109], w[77], w[51], pars->GC_11, amp[316]); 
  FFV1_0(w[101], w[77], w[8], pars->GC_1, amp[317]); 
  FFV1_0(w[110], w[77], w[51], pars->GC_11, amp[318]); 
  FFV2_3_0(w[101], w[77], w[12], pars->GC_50, pars->GC_58, amp[319]); 
  FFV1_0(w[46], w[47], w[85], pars->GC_11, amp[320]); 
  FFV1_0(w[48], w[45], w[85], pars->GC_11, amp[321]); 
  FFV1_0(w[46], w[49], w[85], pars->GC_11, amp[322]); 
  FFV1_0(w[50], w[45], w[85], pars->GC_11, amp[323]); 
  FFV1_0(w[78], w[111], w[51], pars->GC_11, amp[324]); 
  FFV1_0(w[78], w[102], w[8], pars->GC_1, amp[325]); 
  FFV1_0(w[78], w[112], w[51], pars->GC_11, amp[326]); 
  FFV2_3_0(w[78], w[102], w[12], pars->GC_50, pars->GC_58, amp[327]); 
  FFV1_0(w[46], w[103], w[8], pars->GC_1, amp[328]); 
  FFV1_0(w[46], w[56], w[89], pars->GC_11, amp[329]); 
  FFV2_3_0(w[46], w[103], w[12], pars->GC_50, pars->GC_58, amp[330]); 
  FFV1_0(w[46], w[57], w[89], pars->GC_11, amp[331]); 
  FFV1_0(w[113], w[77], w[58], pars->GC_11, amp[332]); 
  FFV1_0(w[78], w[114], w[58], pars->GC_11, amp[333]); 
  FFV1_0(w[115], w[77], w[58], pars->GC_11, amp[334]); 
  FFV1_0(w[78], w[116], w[58], pars->GC_11, amp[335]); 
  FFV1_0(w[104], w[45], w[8], pars->GC_1, amp[336]); 
  FFV1_0(w[61], w[45], w[89], pars->GC_11, amp[337]); 
  FFV2_3_0(w[104], w[45], w[12], pars->GC_50, pars->GC_58, amp[338]); 
  FFV1_0(w[62], w[45], w[89], pars->GC_11, amp[339]); 
  FFV1_0(w[113], w[77], w[63], pars->GC_11, amp[340]); 
  FFV1_0(w[78], w[114], w[63], pars->GC_11, amp[341]); 
  FFV1_0(w[115], w[77], w[63], pars->GC_11, amp[342]); 
  FFV1_0(w[78], w[116], w[63], pars->GC_11, amp[343]); 
  FFV1_0(w[46], w[47], w[96], pars->GC_11, amp[344]); 
  FFV1_0(w[48], w[45], w[96], pars->GC_11, amp[345]); 
  FFV1_0(w[48], w[105], w[4], pars->GC_11, amp[346]); 
  FFV1_0(w[106], w[47], w[4], pars->GC_11, amp[347]); 
  FFV1_0(w[46], w[49], w[96], pars->GC_11, amp[348]); 
  FFV1_0(w[50], w[45], w[96], pars->GC_11, amp[349]); 
  FFV1_0(w[50], w[105], w[4], pars->GC_11, amp[350]); 
  FFV1_0(w[106], w[49], w[4], pars->GC_11, amp[351]); 
  FFV1_0(w[113], w[77], w[66], pars->GC_11, amp[352]); 
  FFV1_0(w[78], w[114], w[66], pars->GC_11, amp[353]); 
  FFV1_0(w[113], w[107], w[4], pars->GC_11, amp[354]); 
  FFV1_0(w[108], w[114], w[4], pars->GC_11, amp[355]); 
  FFV1_0(w[115], w[77], w[66], pars->GC_11, amp[356]); 
  FFV1_0(w[78], w[116], w[66], pars->GC_11, amp[357]); 
  FFV1_0(w[115], w[107], w[4], pars->GC_11, amp[358]); 
  FFV1_0(w[108], w[116], w[4], pars->GC_11, amp[359]); 
  FFV1_0(w[1], w[118], w[9], pars->GC_11, amp[360]); 
  FFV1_0(w[119], w[6], w[9], pars->GC_11, amp[361]); 
  FFV1_0(w[120], w[5], w[15], pars->GC_11, amp[362]); 
  FFV2_5_0(w[17], w[5], w[117], pars->GC_51, pars->GC_58, amp[363]); 
  FFV1_0(w[1], w[118], w[20], pars->GC_11, amp[364]); 
  FFV1_0(w[119], w[6], w[20], pars->GC_11, amp[365]); 
  FFV1_0(w[0], w[121], w[15], pars->GC_11, amp[366]); 
  FFV2_5_0(w[0], w[22], w[117], pars->GC_51, pars->GC_58, amp[367]); 
  FFV2_3_0(w[1], w[26], w[117], pars->GC_50, pars->GC_58, amp[368]); 
  FFV1_0(w[1], w[122], w[25], pars->GC_11, amp[369]); 
  FFV1_0(w[123], w[5], w[29], pars->GC_11, amp[370]); 
  FFV1_0(w[0], w[124], w[29], pars->GC_11, amp[371]); 
  FFV2_3_0(w[35], w[6], w[117], pars->GC_50, pars->GC_58, amp[372]); 
  FFV1_0(w[125], w[6], w[25], pars->GC_11, amp[373]); 
  FFV1_0(w[123], w[5], w[38], pars->GC_11, amp[374]); 
  FFV1_0(w[0], w[124], w[38], pars->GC_11, amp[375]); 
  FFV1_0(w[1], w[118], w[39], pars->GC_11, amp[376]); 
  FFV1_0(w[119], w[6], w[39], pars->GC_11, amp[377]); 
  FFV1_0(w[119], w[40], w[4], pars->GC_11, amp[378]); 
  FFV1_0(w[41], w[118], w[4], pars->GC_11, amp[379]); 
  FFV1_0(w[123], w[5], w[42], pars->GC_11, amp[380]); 
  FFV1_0(w[0], w[124], w[42], pars->GC_11, amp[381]); 
  FFV1_0(w[123], w[43], w[4], pars->GC_11, amp[382]); 
  FFV1_0(w[44], w[124], w[4], pars->GC_11, amp[383]); 
  FFV1_0(w[46], w[126], w[9], pars->GC_11, amp[384]); 
  FFV1_0(w[127], w[45], w[9], pars->GC_11, amp[385]); 
  FFV1_0(w[120], w[5], w[51], pars->GC_11, amp[386]); 
  FFV2_5_0(w[52], w[5], w[117], pars->GC_51, pars->GC_58, amp[387]); 
  FFV1_0(w[46], w[126], w[20], pars->GC_11, amp[388]); 
  FFV1_0(w[127], w[45], w[20], pars->GC_11, amp[389]); 
  FFV1_0(w[0], w[121], w[51], pars->GC_11, amp[390]); 
  FFV2_5_0(w[0], w[53], w[117], pars->GC_51, pars->GC_58, amp[391]); 
  FFV2_3_0(w[46], w[55], w[117], pars->GC_50, pars->GC_58, amp[392]); 
  FFV1_0(w[46], w[128], w[25], pars->GC_11, amp[393]); 
  FFV1_0(w[123], w[5], w[58], pars->GC_11, amp[394]); 
  FFV1_0(w[0], w[124], w[58], pars->GC_11, amp[395]); 
  FFV2_3_0(w[60], w[45], w[117], pars->GC_50, pars->GC_58, amp[396]); 
  FFV1_0(w[129], w[45], w[25], pars->GC_11, amp[397]); 
  FFV1_0(w[123], w[5], w[63], pars->GC_11, amp[398]); 
  FFV1_0(w[0], w[124], w[63], pars->GC_11, amp[399]); 
  FFV1_0(w[46], w[126], w[39], pars->GC_11, amp[400]); 
  FFV1_0(w[127], w[45], w[39], pars->GC_11, amp[401]); 
  FFV1_0(w[127], w[64], w[4], pars->GC_11, amp[402]); 
  FFV1_0(w[65], w[126], w[4], pars->GC_11, amp[403]); 
  FFV1_0(w[123], w[5], w[66], pars->GC_11, amp[404]); 
  FFV1_0(w[0], w[124], w[66], pars->GC_11, amp[405]); 
  FFV1_0(w[123], w[67], w[4], pars->GC_11, amp[406]); 
  FFV1_0(w[68], w[124], w[4], pars->GC_11, amp[407]); 
  FFV1_0(w[1], w[118], w[9], pars->GC_11, amp[408]); 
  FFV1_0(w[119], w[6], w[9], pars->GC_11, amp[409]); 
  FFV1_0(w[130], w[5], w[15], pars->GC_11, amp[410]); 
  FFV2_3_0(w[17], w[5], w[117], pars->GC_50, pars->GC_58, amp[411]); 
  FFV1_0(w[1], w[118], w[20], pars->GC_11, amp[412]); 
  FFV1_0(w[119], w[6], w[20], pars->GC_11, amp[413]); 
  FFV1_0(w[0], w[131], w[15], pars->GC_11, amp[414]); 
  FFV2_3_0(w[0], w[22], w[117], pars->GC_50, pars->GC_58, amp[415]); 
  FFV2_3_0(w[1], w[26], w[117], pars->GC_50, pars->GC_58, amp[416]); 
  FFV1_0(w[1], w[122], w[25], pars->GC_11, amp[417]); 
  FFV1_0(w[132], w[5], w[29], pars->GC_11, amp[418]); 
  FFV1_0(w[0], w[133], w[29], pars->GC_11, amp[419]); 
  FFV2_3_0(w[35], w[6], w[117], pars->GC_50, pars->GC_58, amp[420]); 
  FFV1_0(w[125], w[6], w[25], pars->GC_11, amp[421]); 
  FFV1_0(w[132], w[5], w[38], pars->GC_11, amp[422]); 
  FFV1_0(w[0], w[133], w[38], pars->GC_11, amp[423]); 
  FFV1_0(w[1], w[118], w[39], pars->GC_11, amp[424]); 
  FFV1_0(w[119], w[6], w[39], pars->GC_11, amp[425]); 
  FFV1_0(w[119], w[40], w[4], pars->GC_11, amp[426]); 
  FFV1_0(w[41], w[118], w[4], pars->GC_11, amp[427]); 
  FFV1_0(w[132], w[5], w[42], pars->GC_11, amp[428]); 
  FFV1_0(w[0], w[133], w[42], pars->GC_11, amp[429]); 
  FFV1_0(w[132], w[43], w[4], pars->GC_11, amp[430]); 
  FFV1_0(w[44], w[133], w[4], pars->GC_11, amp[431]); 
  FFV1_0(w[46], w[126], w[9], pars->GC_11, amp[432]); 
  FFV1_0(w[127], w[45], w[9], pars->GC_11, amp[433]); 
  FFV1_0(w[130], w[5], w[51], pars->GC_11, amp[434]); 
  FFV2_3_0(w[52], w[5], w[117], pars->GC_50, pars->GC_58, amp[435]); 
  FFV1_0(w[46], w[126], w[20], pars->GC_11, amp[436]); 
  FFV1_0(w[127], w[45], w[20], pars->GC_11, amp[437]); 
  FFV1_0(w[0], w[131], w[51], pars->GC_11, amp[438]); 
  FFV2_3_0(w[0], w[53], w[117], pars->GC_50, pars->GC_58, amp[439]); 
  FFV2_3_0(w[46], w[55], w[117], pars->GC_50, pars->GC_58, amp[440]); 
  FFV1_0(w[46], w[128], w[25], pars->GC_11, amp[441]); 
  FFV1_0(w[132], w[5], w[58], pars->GC_11, amp[442]); 
  FFV1_0(w[0], w[133], w[58], pars->GC_11, amp[443]); 
  FFV2_3_0(w[60], w[45], w[117], pars->GC_50, pars->GC_58, amp[444]); 
  FFV1_0(w[129], w[45], w[25], pars->GC_11, amp[445]); 
  FFV1_0(w[132], w[5], w[63], pars->GC_11, amp[446]); 
  FFV1_0(w[0], w[133], w[63], pars->GC_11, amp[447]); 
  FFV1_0(w[46], w[126], w[39], pars->GC_11, amp[448]); 
  FFV1_0(w[127], w[45], w[39], pars->GC_11, amp[449]); 
  FFV1_0(w[127], w[64], w[4], pars->GC_11, amp[450]); 
  FFV1_0(w[65], w[126], w[4], pars->GC_11, amp[451]); 
  FFV1_0(w[132], w[5], w[66], pars->GC_11, amp[452]); 
  FFV1_0(w[0], w[133], w[66], pars->GC_11, amp[453]); 
  FFV1_0(w[132], w[67], w[4], pars->GC_11, amp[454]); 
  FFV1_0(w[68], w[133], w[4], pars->GC_11, amp[455]); 
  FFV1_0(w[1], w[118], w[80], pars->GC_11, amp[456]); 
  FFV1_0(w[119], w[6], w[80], pars->GC_11, amp[457]); 
  FFV1_0(w[134], w[77], w[15], pars->GC_11, amp[458]); 
  FFV2_5_0(w[82], w[77], w[117], pars->GC_51, pars->GC_58, amp[459]); 
  FFV1_0(w[1], w[118], w[85], pars->GC_11, amp[460]); 
  FFV1_0(w[119], w[6], w[85], pars->GC_11, amp[461]); 
  FFV1_0(w[78], w[135], w[15], pars->GC_11, amp[462]); 
  FFV2_5_0(w[78], w[87], w[117], pars->GC_51, pars->GC_58, amp[463]); 
  FFV2_3_0(w[1], w[90], w[117], pars->GC_50, pars->GC_58, amp[464]); 
  FFV1_0(w[1], w[122], w[89], pars->GC_11, amp[465]); 
  FFV1_0(w[136], w[77], w[29], pars->GC_11, amp[466]); 
  FFV1_0(w[78], w[137], w[29], pars->GC_11, amp[467]); 
  FFV2_3_0(w[95], w[6], w[117], pars->GC_50, pars->GC_58, amp[468]); 
  FFV1_0(w[125], w[6], w[89], pars->GC_11, amp[469]); 
  FFV1_0(w[136], w[77], w[38], pars->GC_11, amp[470]); 
  FFV1_0(w[78], w[137], w[38], pars->GC_11, amp[471]); 
  FFV1_0(w[1], w[118], w[96], pars->GC_11, amp[472]); 
  FFV1_0(w[119], w[6], w[96], pars->GC_11, amp[473]); 
  FFV1_0(w[119], w[97], w[4], pars->GC_11, amp[474]); 
  FFV1_0(w[98], w[118], w[4], pars->GC_11, amp[475]); 
  FFV1_0(w[136], w[77], w[42], pars->GC_11, amp[476]); 
  FFV1_0(w[78], w[137], w[42], pars->GC_11, amp[477]); 
  FFV1_0(w[136], w[99], w[4], pars->GC_11, amp[478]); 
  FFV1_0(w[100], w[137], w[4], pars->GC_11, amp[479]); 
  FFV1_0(w[46], w[126], w[80], pars->GC_11, amp[480]); 
  FFV1_0(w[127], w[45], w[80], pars->GC_11, amp[481]); 
  FFV1_0(w[134], w[77], w[51], pars->GC_11, amp[482]); 
  FFV2_5_0(w[101], w[77], w[117], pars->GC_51, pars->GC_58, amp[483]); 
  FFV1_0(w[46], w[126], w[85], pars->GC_11, amp[484]); 
  FFV1_0(w[127], w[45], w[85], pars->GC_11, amp[485]); 
  FFV1_0(w[78], w[135], w[51], pars->GC_11, amp[486]); 
  FFV2_5_0(w[78], w[102], w[117], pars->GC_51, pars->GC_58, amp[487]); 
  FFV2_3_0(w[46], w[103], w[117], pars->GC_50, pars->GC_58, amp[488]); 
  FFV1_0(w[46], w[128], w[89], pars->GC_11, amp[489]); 
  FFV1_0(w[136], w[77], w[58], pars->GC_11, amp[490]); 
  FFV1_0(w[78], w[137], w[58], pars->GC_11, amp[491]); 
  FFV2_3_0(w[104], w[45], w[117], pars->GC_50, pars->GC_58, amp[492]); 
  FFV1_0(w[129], w[45], w[89], pars->GC_11, amp[493]); 
  FFV1_0(w[136], w[77], w[63], pars->GC_11, amp[494]); 
  FFV1_0(w[78], w[137], w[63], pars->GC_11, amp[495]); 
  FFV1_0(w[46], w[126], w[96], pars->GC_11, amp[496]); 
  FFV1_0(w[127], w[45], w[96], pars->GC_11, amp[497]); 
  FFV1_0(w[127], w[105], w[4], pars->GC_11, amp[498]); 
  FFV1_0(w[106], w[126], w[4], pars->GC_11, amp[499]); 
  FFV1_0(w[136], w[77], w[66], pars->GC_11, amp[500]); 
  FFV1_0(w[78], w[137], w[66], pars->GC_11, amp[501]); 
  FFV1_0(w[136], w[107], w[4], pars->GC_11, amp[502]); 
  FFV1_0(w[108], w[137], w[4], pars->GC_11, amp[503]); 
  FFV1_0(w[1], w[118], w[80], pars->GC_11, amp[504]); 
  FFV1_0(w[119], w[6], w[80], pars->GC_11, amp[505]); 
  FFV1_0(w[138], w[77], w[15], pars->GC_11, amp[506]); 
  FFV2_3_0(w[82], w[77], w[117], pars->GC_50, pars->GC_58, amp[507]); 
  FFV1_0(w[1], w[118], w[85], pars->GC_11, amp[508]); 
  FFV1_0(w[119], w[6], w[85], pars->GC_11, amp[509]); 
  FFV1_0(w[78], w[139], w[15], pars->GC_11, amp[510]); 
  FFV2_3_0(w[78], w[87], w[117], pars->GC_50, pars->GC_58, amp[511]); 
  FFV2_3_0(w[1], w[90], w[117], pars->GC_50, pars->GC_58, amp[512]); 
  FFV1_0(w[1], w[122], w[89], pars->GC_11, amp[513]); 
  FFV1_0(w[140], w[77], w[29], pars->GC_11, amp[514]); 
  FFV1_0(w[78], w[141], w[29], pars->GC_11, amp[515]); 
  FFV2_3_0(w[95], w[6], w[117], pars->GC_50, pars->GC_58, amp[516]); 
  FFV1_0(w[125], w[6], w[89], pars->GC_11, amp[517]); 
  FFV1_0(w[140], w[77], w[38], pars->GC_11, amp[518]); 
  FFV1_0(w[78], w[141], w[38], pars->GC_11, amp[519]); 
  FFV1_0(w[1], w[118], w[96], pars->GC_11, amp[520]); 
  FFV1_0(w[119], w[6], w[96], pars->GC_11, amp[521]); 
  FFV1_0(w[119], w[97], w[4], pars->GC_11, amp[522]); 
  FFV1_0(w[98], w[118], w[4], pars->GC_11, amp[523]); 
  FFV1_0(w[140], w[77], w[42], pars->GC_11, amp[524]); 
  FFV1_0(w[78], w[141], w[42], pars->GC_11, amp[525]); 
  FFV1_0(w[140], w[99], w[4], pars->GC_11, amp[526]); 
  FFV1_0(w[100], w[141], w[4], pars->GC_11, amp[527]); 
  FFV1_0(w[46], w[126], w[80], pars->GC_11, amp[528]); 
  FFV1_0(w[127], w[45], w[80], pars->GC_11, amp[529]); 
  FFV1_0(w[138], w[77], w[51], pars->GC_11, amp[530]); 
  FFV2_3_0(w[101], w[77], w[117], pars->GC_50, pars->GC_58, amp[531]); 
  FFV1_0(w[46], w[126], w[85], pars->GC_11, amp[532]); 
  FFV1_0(w[127], w[45], w[85], pars->GC_11, amp[533]); 
  FFV1_0(w[78], w[139], w[51], pars->GC_11, amp[534]); 
  FFV2_3_0(w[78], w[102], w[117], pars->GC_50, pars->GC_58, amp[535]); 
  FFV2_3_0(w[46], w[103], w[117], pars->GC_50, pars->GC_58, amp[536]); 
  FFV1_0(w[46], w[128], w[89], pars->GC_11, amp[537]); 
  FFV1_0(w[140], w[77], w[58], pars->GC_11, amp[538]); 
  FFV1_0(w[78], w[141], w[58], pars->GC_11, amp[539]); 
  FFV2_3_0(w[104], w[45], w[117], pars->GC_50, pars->GC_58, amp[540]); 
  FFV1_0(w[129], w[45], w[89], pars->GC_11, amp[541]); 
  FFV1_0(w[140], w[77], w[63], pars->GC_11, amp[542]); 
  FFV1_0(w[78], w[141], w[63], pars->GC_11, amp[543]); 
  FFV1_0(w[46], w[126], w[96], pars->GC_11, amp[544]); 
  FFV1_0(w[127], w[45], w[96], pars->GC_11, amp[545]); 
  FFV1_0(w[127], w[105], w[4], pars->GC_11, amp[546]); 
  FFV1_0(w[106], w[126], w[4], pars->GC_11, amp[547]); 
  FFV1_0(w[140], w[77], w[66], pars->GC_11, amp[548]); 
  FFV1_0(w[78], w[141], w[66], pars->GC_11, amp[549]); 
  FFV1_0(w[140], w[107], w[4], pars->GC_11, amp[550]); 
  FFV1_0(w[108], w[141], w[4], pars->GC_11, amp[551]); 
  FFV1_0(w[143], w[5], w[15], pars->GC_11, amp[552]); 
  FFV2_0(w[17], w[5], w[142], pars->GC_100, amp[553]); 
  FFV1_0(w[0], w[144], w[15], pars->GC_11, amp[554]); 
  FFV2_0(w[0], w[22], w[142], pars->GC_100, amp[555]); 
  FFV1_0(w[145], w[5], w[29], pars->GC_11, amp[556]); 
  FFV1_0(w[0], w[146], w[29], pars->GC_11, amp[557]); 
  FFV1_0(w[145], w[5], w[38], pars->GC_11, amp[558]); 
  FFV1_0(w[0], w[146], w[38], pars->GC_11, amp[559]); 
  FFV1_0(w[145], w[5], w[42], pars->GC_11, amp[560]); 
  FFV1_0(w[0], w[146], w[42], pars->GC_11, amp[561]); 
  FFV1_0(w[145], w[43], w[4], pars->GC_11, amp[562]); 
  FFV1_0(w[44], w[146], w[4], pars->GC_11, amp[563]); 
  FFV1_0(w[143], w[5], w[51], pars->GC_11, amp[564]); 
  FFV2_0(w[52], w[5], w[142], pars->GC_100, amp[565]); 
  FFV1_0(w[0], w[144], w[51], pars->GC_11, amp[566]); 
  FFV2_0(w[0], w[53], w[142], pars->GC_100, amp[567]); 
  FFV1_0(w[145], w[5], w[58], pars->GC_11, amp[568]); 
  FFV1_0(w[0], w[146], w[58], pars->GC_11, amp[569]); 
  FFV1_0(w[145], w[5], w[63], pars->GC_11, amp[570]); 
  FFV1_0(w[0], w[146], w[63], pars->GC_11, amp[571]); 
  FFV1_0(w[145], w[5], w[66], pars->GC_11, amp[572]); 
  FFV1_0(w[0], w[146], w[66], pars->GC_11, amp[573]); 
  FFV1_0(w[145], w[67], w[4], pars->GC_11, amp[574]); 
  FFV1_0(w[68], w[146], w[4], pars->GC_11, amp[575]); 
  FFV1_0(w[143], w[5], w[15], pars->GC_11, amp[576]); 
  FFV2_0(w[17], w[5], w[142], pars->GC_100, amp[577]); 
  FFV1_0(w[0], w[144], w[15], pars->GC_11, amp[578]); 
  FFV2_0(w[0], w[22], w[142], pars->GC_100, amp[579]); 
  FFV1_0(w[145], w[5], w[29], pars->GC_11, amp[580]); 
  FFV1_0(w[0], w[146], w[29], pars->GC_11, amp[581]); 
  FFV1_0(w[145], w[5], w[38], pars->GC_11, amp[582]); 
  FFV1_0(w[0], w[146], w[38], pars->GC_11, amp[583]); 
  FFV1_0(w[145], w[5], w[42], pars->GC_11, amp[584]); 
  FFV1_0(w[0], w[146], w[42], pars->GC_11, amp[585]); 
  FFV1_0(w[145], w[43], w[4], pars->GC_11, amp[586]); 
  FFV1_0(w[44], w[146], w[4], pars->GC_11, amp[587]); 
  FFV1_0(w[143], w[5], w[51], pars->GC_11, amp[588]); 
  FFV2_0(w[52], w[5], w[142], pars->GC_100, amp[589]); 
  FFV1_0(w[0], w[144], w[51], pars->GC_11, amp[590]); 
  FFV2_0(w[0], w[53], w[142], pars->GC_100, amp[591]); 
  FFV1_0(w[145], w[5], w[58], pars->GC_11, amp[592]); 
  FFV1_0(w[0], w[146], w[58], pars->GC_11, amp[593]); 
  FFV1_0(w[145], w[5], w[63], pars->GC_11, amp[594]); 
  FFV1_0(w[0], w[146], w[63], pars->GC_11, amp[595]); 
  FFV1_0(w[145], w[5], w[66], pars->GC_11, amp[596]); 
  FFV1_0(w[0], w[146], w[66], pars->GC_11, amp[597]); 
  FFV1_0(w[145], w[67], w[4], pars->GC_11, amp[598]); 
  FFV1_0(w[68], w[146], w[4], pars->GC_11, amp[599]); 
  FFV1_0(w[147], w[77], w[15], pars->GC_11, amp[600]); 
  FFV2_0(w[82], w[77], w[142], pars->GC_100, amp[601]); 
  FFV1_0(w[78], w[148], w[15], pars->GC_11, amp[602]); 
  FFV2_0(w[78], w[87], w[142], pars->GC_100, amp[603]); 
  FFV1_0(w[149], w[77], w[29], pars->GC_11, amp[604]); 
  FFV1_0(w[78], w[150], w[29], pars->GC_11, amp[605]); 
  FFV1_0(w[149], w[77], w[38], pars->GC_11, amp[606]); 
  FFV1_0(w[78], w[150], w[38], pars->GC_11, amp[607]); 
  FFV1_0(w[149], w[77], w[42], pars->GC_11, amp[608]); 
  FFV1_0(w[78], w[150], w[42], pars->GC_11, amp[609]); 
  FFV1_0(w[149], w[99], w[4], pars->GC_11, amp[610]); 
  FFV1_0(w[100], w[150], w[4], pars->GC_11, amp[611]); 
  FFV1_0(w[147], w[77], w[51], pars->GC_11, amp[612]); 
  FFV2_0(w[101], w[77], w[142], pars->GC_100, amp[613]); 
  FFV1_0(w[78], w[148], w[51], pars->GC_11, amp[614]); 
  FFV2_0(w[78], w[102], w[142], pars->GC_100, amp[615]); 
  FFV1_0(w[149], w[77], w[58], pars->GC_11, amp[616]); 
  FFV1_0(w[78], w[150], w[58], pars->GC_11, amp[617]); 
  FFV1_0(w[149], w[77], w[63], pars->GC_11, amp[618]); 
  FFV1_0(w[78], w[150], w[63], pars->GC_11, amp[619]); 
  FFV1_0(w[149], w[77], w[66], pars->GC_11, amp[620]); 
  FFV1_0(w[78], w[150], w[66], pars->GC_11, amp[621]); 
  FFV1_0(w[149], w[107], w[4], pars->GC_11, amp[622]); 
  FFV1_0(w[108], w[150], w[4], pars->GC_11, amp[623]); 
  FFV1_0(w[147], w[77], w[15], pars->GC_11, amp[624]); 
  FFV2_0(w[82], w[77], w[142], pars->GC_100, amp[625]); 
  FFV1_0(w[78], w[148], w[15], pars->GC_11, amp[626]); 
  FFV2_0(w[78], w[87], w[142], pars->GC_100, amp[627]); 
  FFV1_0(w[149], w[77], w[29], pars->GC_11, amp[628]); 
  FFV1_0(w[78], w[150], w[29], pars->GC_11, amp[629]); 
  FFV1_0(w[149], w[77], w[38], pars->GC_11, amp[630]); 
  FFV1_0(w[78], w[150], w[38], pars->GC_11, amp[631]); 
  FFV1_0(w[149], w[77], w[42], pars->GC_11, amp[632]); 
  FFV1_0(w[78], w[150], w[42], pars->GC_11, amp[633]); 
  FFV1_0(w[149], w[99], w[4], pars->GC_11, amp[634]); 
  FFV1_0(w[100], w[150], w[4], pars->GC_11, amp[635]); 
  FFV1_0(w[147], w[77], w[51], pars->GC_11, amp[636]); 
  FFV2_0(w[101], w[77], w[142], pars->GC_100, amp[637]); 
  FFV1_0(w[78], w[148], w[51], pars->GC_11, amp[638]); 
  FFV2_0(w[78], w[102], w[142], pars->GC_100, amp[639]); 
  FFV1_0(w[149], w[77], w[58], pars->GC_11, amp[640]); 
  FFV1_0(w[78], w[150], w[58], pars->GC_11, amp[641]); 
  FFV1_0(w[149], w[77], w[63], pars->GC_11, amp[642]); 
  FFV1_0(w[78], w[150], w[63], pars->GC_11, amp[643]); 
  FFV1_0(w[149], w[77], w[66], pars->GC_11, amp[644]); 
  FFV1_0(w[78], w[150], w[66], pars->GC_11, amp[645]); 
  FFV1_0(w[149], w[107], w[4], pars->GC_11, amp[646]); 
  FFV1_0(w[108], w[150], w[4], pars->GC_11, amp[647]); 


}
double PY8MEs_R4_P123_sm_qb_llgqb::matrix_4_ub_epemgub() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 48;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[0] + 1./3. * amp[1] + 1./3. * amp[2] + 1./3.
      * amp[3] + 1./3. * amp[4] + 1./3. * amp[5] + 1./3. * amp[6] + 1./3. *
      amp[7] + 1./3. * amp[8] + 1./3. * amp[9] + 1./3. * amp[10] + 1./3. *
      amp[11] + 1./3. * amp[12] + 1./3. * amp[13] + 1./3. * amp[14] + 1./3. *
      amp[15] + 1./3. * amp[42] + 1./3. * amp[43] + 1./3. * amp[46] + 1./3. *
      amp[47]);
  jamp[1] = +1./2. * (-amp[8] - amp[9] - amp[10] - amp[11] - amp[12] - amp[13]
      - amp[14] - amp[15] - amp[24] - amp[25] - amp[26] - amp[27] - amp[28] -
      amp[29] - amp[30] - amp[31] - Complex<double> (0, 1) * amp[32] -
      Complex<double> (0, 1) * amp[33] - amp[34] - Complex<double> (0, 1) *
      amp[36] - Complex<double> (0, 1) * amp[37] - amp[38] + Complex<double>
      (0, 1) * amp[40] + Complex<double> (0, 1) * amp[41] - amp[43] +
      Complex<double> (0, 1) * amp[44] + Complex<double> (0, 1) * amp[45] -
      amp[47]);
  jamp[2] = +1./2. * (-amp[0] - amp[1] - amp[2] - amp[3] - amp[4] - amp[5] -
      amp[6] - amp[7] - amp[16] - amp[17] - amp[18] - amp[19] - amp[20] -
      amp[21] - amp[22] - amp[23] + Complex<double> (0, 1) * amp[32] +
      Complex<double> (0, 1) * amp[33] - amp[35] + Complex<double> (0, 1) *
      amp[36] + Complex<double> (0, 1) * amp[37] - amp[39] - Complex<double>
      (0, 1) * amp[40] - Complex<double> (0, 1) * amp[41] - amp[42] -
      Complex<double> (0, 1) * amp[44] - Complex<double> (0, 1) * amp[45] -
      amp[46]);
  jamp[3] = +1./2. * (+1./3. * amp[16] + 1./3. * amp[17] + 1./3. * amp[18] +
      1./3. * amp[19] + 1./3. * amp[20] + 1./3. * amp[21] + 1./3. * amp[22] +
      1./3. * amp[23] + 1./3. * amp[24] + 1./3. * amp[25] + 1./3. * amp[26] +
      1./3. * amp[27] + 1./3. * amp[28] + 1./3. * amp[29] + 1./3. * amp[30] +
      1./3. * amp[31] + 1./3. * amp[34] + 1./3. * amp[35] + 1./3. * amp[38] +
      1./3. * amp[39]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[0][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P123_sm_qb_llgqb::matrix_4_ubx_epemgubx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 48;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[56] + amp[57] + amp[58] + amp[59] + amp[60] +
      amp[61] + amp[62] + amp[63] + amp[72] + amp[73] + amp[74] + amp[75] +
      amp[76] + amp[77] + amp[78] + amp[79] + Complex<double> (0, 1) * amp[80]
      + Complex<double> (0, 1) * amp[81] + amp[82] + Complex<double> (0, 1) *
      amp[84] + Complex<double> (0, 1) * amp[85] + amp[86] - Complex<double>
      (0, 1) * amp[88] - Complex<double> (0, 1) * amp[89] + amp[91] -
      Complex<double> (0, 1) * amp[92] - Complex<double> (0, 1) * amp[93] +
      amp[95]);
  jamp[1] = +1./2. * (-1./3. * amp[48] - 1./3. * amp[49] - 1./3. * amp[50] -
      1./3. * amp[51] - 1./3. * amp[52] - 1./3. * amp[53] - 1./3. * amp[54] -
      1./3. * amp[55] - 1./3. * amp[56] - 1./3. * amp[57] - 1./3. * amp[58] -
      1./3. * amp[59] - 1./3. * amp[60] - 1./3. * amp[61] - 1./3. * amp[62] -
      1./3. * amp[63] - 1./3. * amp[90] - 1./3. * amp[91] - 1./3. * amp[94] -
      1./3. * amp[95]);
  jamp[2] = +1./2. * (+amp[48] + amp[49] + amp[50] + amp[51] + amp[52] +
      amp[53] + amp[54] + amp[55] + amp[64] + amp[65] + amp[66] + amp[67] +
      amp[68] + amp[69] + amp[70] + amp[71] - Complex<double> (0, 1) * amp[80]
      - Complex<double> (0, 1) * amp[81] + amp[83] - Complex<double> (0, 1) *
      amp[84] - Complex<double> (0, 1) * amp[85] + amp[87] + Complex<double>
      (0, 1) * amp[88] + Complex<double> (0, 1) * amp[89] + amp[90] +
      Complex<double> (0, 1) * amp[92] + Complex<double> (0, 1) * amp[93] +
      amp[94]);
  jamp[3] = +1./2. * (-1./3. * amp[64] - 1./3. * amp[65] - 1./3. * amp[66] -
      1./3. * amp[67] - 1./3. * amp[68] - 1./3. * amp[69] - 1./3. * amp[70] -
      1./3. * amp[71] - 1./3. * amp[72] - 1./3. * amp[73] - 1./3. * amp[74] -
      1./3. * amp[75] - 1./3. * amp[76] - 1./3. * amp[77] - 1./3. * amp[78] -
      1./3. * amp[79] - 1./3. * amp[82] - 1./3. * amp[83] - 1./3. * amp[86] -
      1./3. * amp[87]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[1][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P123_sm_qb_llgqb::matrix_4_db_epemgdb() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 48;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[0] + 1./3. * amp[1] + 1./3. * amp[2] + 1./3.
      * amp[3] + 1./3. * amp[96] + 1./3. * amp[97] + 1./3. * amp[98] + 1./3. *
      amp[99] + 1./3. * amp[8] + 1./3. * amp[9] + 1./3. * amp[10] + 1./3. *
      amp[11] + 1./3. * amp[100] + 1./3. * amp[101] + 1./3. * amp[102] + 1./3.
      * amp[103] + 1./3. * amp[114] + 1./3. * amp[115] + 1./3. * amp[118] +
      1./3. * amp[119]);
  jamp[1] = +1./2. * (-amp[8] - amp[9] - amp[10] - amp[11] - amp[100] -
      amp[101] - amp[102] - amp[103] - amp[24] - amp[25] - amp[26] - amp[27] -
      amp[108] - amp[109] - amp[110] - amp[111] - Complex<double> (0, 1) *
      amp[32] - Complex<double> (0, 1) * amp[33] - amp[34] - Complex<double>
      (0, 1) * amp[36] - Complex<double> (0, 1) * amp[37] - amp[38] +
      Complex<double> (0, 1) * amp[112] + Complex<double> (0, 1) * amp[113] -
      amp[115] + Complex<double> (0, 1) * amp[116] + Complex<double> (0, 1) *
      amp[117] - amp[119]);
  jamp[2] = +1./2. * (-amp[0] - amp[1] - amp[2] - amp[3] - amp[96] - amp[97] -
      amp[98] - amp[99] - amp[16] - amp[17] - amp[18] - amp[19] - amp[104] -
      amp[105] - amp[106] - amp[107] + Complex<double> (0, 1) * amp[32] +
      Complex<double> (0, 1) * amp[33] - amp[35] + Complex<double> (0, 1) *
      amp[36] + Complex<double> (0, 1) * amp[37] - amp[39] - Complex<double>
      (0, 1) * amp[112] - Complex<double> (0, 1) * amp[113] - amp[114] -
      Complex<double> (0, 1) * amp[116] - Complex<double> (0, 1) * amp[117] -
      amp[118]);
  jamp[3] = +1./2. * (+1./3. * amp[16] + 1./3. * amp[17] + 1./3. * amp[18] +
      1./3. * amp[19] + 1./3. * amp[104] + 1./3. * amp[105] + 1./3. * amp[106]
      + 1./3. * amp[107] + 1./3. * amp[24] + 1./3. * amp[25] + 1./3. * amp[26]
      + 1./3. * amp[27] + 1./3. * amp[108] + 1./3. * amp[109] + 1./3. *
      amp[110] + 1./3. * amp[111] + 1./3. * amp[34] + 1./3. * amp[35] + 1./3. *
      amp[38] + 1./3. * amp[39]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[2][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P123_sm_qb_llgqb::matrix_4_dbx_epemgdbx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 48;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[128] + amp[129] + amp[130] + amp[131] + amp[132] +
      amp[133] + amp[134] + amp[135] + amp[144] + amp[145] + amp[146] +
      amp[147] + amp[148] + amp[149] + amp[150] + amp[151] + Complex<double>
      (0, 1) * amp[152] + Complex<double> (0, 1) * amp[153] + amp[154] +
      Complex<double> (0, 1) * amp[156] + Complex<double> (0, 1) * amp[157] +
      amp[158] - Complex<double> (0, 1) * amp[160] - Complex<double> (0, 1) *
      amp[161] + amp[163] - Complex<double> (0, 1) * amp[164] - Complex<double>
      (0, 1) * amp[165] + amp[167]);
  jamp[1] = +1./2. * (-1./3. * amp[120] - 1./3. * amp[121] - 1./3. * amp[122] -
      1./3. * amp[123] - 1./3. * amp[124] - 1./3. * amp[125] - 1./3. * amp[126]
      - 1./3. * amp[127] - 1./3. * amp[128] - 1./3. * amp[129] - 1./3. *
      amp[130] - 1./3. * amp[131] - 1./3. * amp[132] - 1./3. * amp[133] - 1./3.
      * amp[134] - 1./3. * amp[135] - 1./3. * amp[162] - 1./3. * amp[163] -
      1./3. * amp[166] - 1./3. * amp[167]);
  jamp[2] = +1./2. * (+amp[120] + amp[121] + amp[122] + amp[123] + amp[124] +
      amp[125] + amp[126] + amp[127] + amp[136] + amp[137] + amp[138] +
      amp[139] + amp[140] + amp[141] + amp[142] + amp[143] - Complex<double>
      (0, 1) * amp[152] - Complex<double> (0, 1) * amp[153] + amp[155] -
      Complex<double> (0, 1) * amp[156] - Complex<double> (0, 1) * amp[157] +
      amp[159] + Complex<double> (0, 1) * amp[160] + Complex<double> (0, 1) *
      amp[161] + amp[162] + Complex<double> (0, 1) * amp[164] + Complex<double>
      (0, 1) * amp[165] + amp[166]);
  jamp[3] = +1./2. * (-1./3. * amp[136] - 1./3. * amp[137] - 1./3. * amp[138] -
      1./3. * amp[139] - 1./3. * amp[140] - 1./3. * amp[141] - 1./3. * amp[142]
      - 1./3. * amp[143] - 1./3. * amp[144] - 1./3. * amp[145] - 1./3. *
      amp[146] - 1./3. * amp[147] - 1./3. * amp[148] - 1./3. * amp[149] - 1./3.
      * amp[150] - 1./3. * amp[151] - 1./3. * amp[154] - 1./3. * amp[155] -
      1./3. * amp[158] - 1./3. * amp[159]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[3][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P123_sm_qb_llgqb::matrix_4_uxb_epemguxb() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 48;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[168] + amp[169] + amp[170] + amp[171] + amp[172] +
      amp[173] + amp[174] + amp[175] + amp[184] + amp[185] + amp[186] +
      amp[187] + amp[188] + amp[189] + amp[190] + amp[191] - Complex<double>
      (0, 1) * amp[200] - Complex<double> (0, 1) * amp[201] + amp[203] -
      Complex<double> (0, 1) * amp[204] - Complex<double> (0, 1) * amp[205] +
      amp[207] + Complex<double> (0, 1) * amp[208] + Complex<double> (0, 1) *
      amp[209] + amp[210] + Complex<double> (0, 1) * amp[212] + Complex<double>
      (0, 1) * amp[213] + amp[214]);
  jamp[1] = +1./2. * (-1./3. * amp[184] - 1./3. * amp[185] - 1./3. * amp[186] -
      1./3. * amp[187] - 1./3. * amp[188] - 1./3. * amp[189] - 1./3. * amp[190]
      - 1./3. * amp[191] - 1./3. * amp[192] - 1./3. * amp[193] - 1./3. *
      amp[194] - 1./3. * amp[195] - 1./3. * amp[196] - 1./3. * amp[197] - 1./3.
      * amp[198] - 1./3. * amp[199] - 1./3. * amp[202] - 1./3. * amp[203] -
      1./3. * amp[206] - 1./3. * amp[207]);
  jamp[2] = +1./2. * (+amp[176] + amp[177] + amp[178] + amp[179] + amp[180] +
      amp[181] + amp[182] + amp[183] + amp[192] + amp[193] + amp[194] +
      amp[195] + amp[196] + amp[197] + amp[198] + amp[199] + Complex<double>
      (0, 1) * amp[200] + Complex<double> (0, 1) * amp[201] + amp[202] +
      Complex<double> (0, 1) * amp[204] + Complex<double> (0, 1) * amp[205] +
      amp[206] - Complex<double> (0, 1) * amp[208] - Complex<double> (0, 1) *
      amp[209] + amp[211] - Complex<double> (0, 1) * amp[212] - Complex<double>
      (0, 1) * amp[213] + amp[215]);
  jamp[3] = +1./2. * (-1./3. * amp[168] - 1./3. * amp[169] - 1./3. * amp[170] -
      1./3. * amp[171] - 1./3. * amp[172] - 1./3. * amp[173] - 1./3. * amp[174]
      - 1./3. * amp[175] - 1./3. * amp[176] - 1./3. * amp[177] - 1./3. *
      amp[178] - 1./3. * amp[179] - 1./3. * amp[180] - 1./3. * amp[181] - 1./3.
      * amp[182] - 1./3. * amp[183] - 1./3. * amp[210] - 1./3. * amp[211] -
      1./3. * amp[214] - 1./3. * amp[215]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[4][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P123_sm_qb_llgqb::matrix_4_uxbx_epemguxbx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 48;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[232] + 1./3. * amp[233] + 1./3. * amp[234] +
      1./3. * amp[235] + 1./3. * amp[236] + 1./3. * amp[237] + 1./3. * amp[238]
      + 1./3. * amp[239] + 1./3. * amp[240] + 1./3. * amp[241] + 1./3. *
      amp[242] + 1./3. * amp[243] + 1./3. * amp[244] + 1./3. * amp[245] + 1./3.
      * amp[246] + 1./3. * amp[247] + 1./3. * amp[250] + 1./3. * amp[251] +
      1./3. * amp[254] + 1./3. * amp[255]);
  jamp[1] = +1./2. * (-amp[216] - amp[217] - amp[218] - amp[219] - amp[220] -
      amp[221] - amp[222] - amp[223] - amp[232] - amp[233] - amp[234] -
      amp[235] - amp[236] - amp[237] - amp[238] - amp[239] + Complex<double>
      (0, 1) * amp[248] + Complex<double> (0, 1) * amp[249] - amp[251] +
      Complex<double> (0, 1) * amp[252] + Complex<double> (0, 1) * amp[253] -
      amp[255] - Complex<double> (0, 1) * amp[256] - Complex<double> (0, 1) *
      amp[257] - amp[258] - Complex<double> (0, 1) * amp[260] - Complex<double>
      (0, 1) * amp[261] - amp[262]);
  jamp[2] = +1./2. * (-amp[224] - amp[225] - amp[226] - amp[227] - amp[228] -
      amp[229] - amp[230] - amp[231] - amp[240] - amp[241] - amp[242] -
      amp[243] - amp[244] - amp[245] - amp[246] - amp[247] - Complex<double>
      (0, 1) * amp[248] - Complex<double> (0, 1) * amp[249] - amp[250] -
      Complex<double> (0, 1) * amp[252] - Complex<double> (0, 1) * amp[253] -
      amp[254] + Complex<double> (0, 1) * amp[256] + Complex<double> (0, 1) *
      amp[257] - amp[259] + Complex<double> (0, 1) * amp[260] + Complex<double>
      (0, 1) * amp[261] - amp[263]);
  jamp[3] = +1./2. * (+1./3. * amp[216] + 1./3. * amp[217] + 1./3. * amp[218] +
      1./3. * amp[219] + 1./3. * amp[220] + 1./3. * amp[221] + 1./3. * amp[222]
      + 1./3. * amp[223] + 1./3. * amp[224] + 1./3. * amp[225] + 1./3. *
      amp[226] + 1./3. * amp[227] + 1./3. * amp[228] + 1./3. * amp[229] + 1./3.
      * amp[230] + 1./3. * amp[231] + 1./3. * amp[258] + 1./3. * amp[259] +
      1./3. * amp[262] + 1./3. * amp[263]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[5][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P123_sm_qb_llgqb::matrix_4_dxb_epemgdxb() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 48;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[264] + amp[265] + amp[266] + amp[267] + amp[268] +
      amp[269] + amp[270] + amp[271] + amp[280] + amp[281] + amp[282] +
      amp[283] + amp[284] + amp[285] + amp[286] + amp[287] - Complex<double>
      (0, 1) * amp[296] - Complex<double> (0, 1) * amp[297] + amp[299] -
      Complex<double> (0, 1) * amp[300] - Complex<double> (0, 1) * amp[301] +
      amp[303] + Complex<double> (0, 1) * amp[304] + Complex<double> (0, 1) *
      amp[305] + amp[306] + Complex<double> (0, 1) * amp[308] + Complex<double>
      (0, 1) * amp[309] + amp[310]);
  jamp[1] = +1./2. * (-1./3. * amp[280] - 1./3. * amp[281] - 1./3. * amp[282] -
      1./3. * amp[283] - 1./3. * amp[284] - 1./3. * amp[285] - 1./3. * amp[286]
      - 1./3. * amp[287] - 1./3. * amp[288] - 1./3. * amp[289] - 1./3. *
      amp[290] - 1./3. * amp[291] - 1./3. * amp[292] - 1./3. * amp[293] - 1./3.
      * amp[294] - 1./3. * amp[295] - 1./3. * amp[298] - 1./3. * amp[299] -
      1./3. * amp[302] - 1./3. * amp[303]);
  jamp[2] = +1./2. * (+amp[272] + amp[273] + amp[274] + amp[275] + amp[276] +
      amp[277] + amp[278] + amp[279] + amp[288] + amp[289] + amp[290] +
      amp[291] + amp[292] + amp[293] + amp[294] + amp[295] + Complex<double>
      (0, 1) * amp[296] + Complex<double> (0, 1) * amp[297] + amp[298] +
      Complex<double> (0, 1) * amp[300] + Complex<double> (0, 1) * amp[301] +
      amp[302] - Complex<double> (0, 1) * amp[304] - Complex<double> (0, 1) *
      amp[305] + amp[307] - Complex<double> (0, 1) * amp[308] - Complex<double>
      (0, 1) * amp[309] + amp[311]);
  jamp[3] = +1./2. * (-1./3. * amp[264] - 1./3. * amp[265] - 1./3. * amp[266] -
      1./3. * amp[267] - 1./3. * amp[268] - 1./3. * amp[269] - 1./3. * amp[270]
      - 1./3. * amp[271] - 1./3. * amp[272] - 1./3. * amp[273] - 1./3. *
      amp[274] - 1./3. * amp[275] - 1./3. * amp[276] - 1./3. * amp[277] - 1./3.
      * amp[278] - 1./3. * amp[279] - 1./3. * amp[306] - 1./3. * amp[307] -
      1./3. * amp[310] - 1./3. * amp[311]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[6][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P123_sm_qb_llgqb::matrix_4_dxbx_epemgdxbx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 48;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[328] + 1./3. * amp[329] + 1./3. * amp[330] +
      1./3. * amp[331] + 1./3. * amp[332] + 1./3. * amp[333] + 1./3. * amp[334]
      + 1./3. * amp[335] + 1./3. * amp[336] + 1./3. * amp[337] + 1./3. *
      amp[338] + 1./3. * amp[339] + 1./3. * amp[340] + 1./3. * amp[341] + 1./3.
      * amp[342] + 1./3. * amp[343] + 1./3. * amp[346] + 1./3. * amp[347] +
      1./3. * amp[350] + 1./3. * amp[351]);
  jamp[1] = +1./2. * (-amp[312] - amp[313] - amp[314] - amp[315] - amp[316] -
      amp[317] - amp[318] - amp[319] - amp[328] - amp[329] - amp[330] -
      amp[331] - amp[332] - amp[333] - amp[334] - amp[335] + Complex<double>
      (0, 1) * amp[344] + Complex<double> (0, 1) * amp[345] - amp[347] +
      Complex<double> (0, 1) * amp[348] + Complex<double> (0, 1) * amp[349] -
      amp[351] - Complex<double> (0, 1) * amp[352] - Complex<double> (0, 1) *
      amp[353] - amp[354] - Complex<double> (0, 1) * amp[356] - Complex<double>
      (0, 1) * amp[357] - amp[358]);
  jamp[2] = +1./2. * (-amp[320] - amp[321] - amp[322] - amp[323] - amp[324] -
      amp[325] - amp[326] - amp[327] - amp[336] - amp[337] - amp[338] -
      amp[339] - amp[340] - amp[341] - amp[342] - amp[343] - Complex<double>
      (0, 1) * amp[344] - Complex<double> (0, 1) * amp[345] - amp[346] -
      Complex<double> (0, 1) * amp[348] - Complex<double> (0, 1) * amp[349] -
      amp[350] + Complex<double> (0, 1) * amp[352] + Complex<double> (0, 1) *
      amp[353] - amp[355] + Complex<double> (0, 1) * amp[356] + Complex<double>
      (0, 1) * amp[357] - amp[359]);
  jamp[3] = +1./2. * (+1./3. * amp[312] + 1./3. * amp[313] + 1./3. * amp[314] +
      1./3. * amp[315] + 1./3. * amp[316] + 1./3. * amp[317] + 1./3. * amp[318]
      + 1./3. * amp[319] + 1./3. * amp[320] + 1./3. * amp[321] + 1./3. *
      amp[322] + 1./3. * amp[323] + 1./3. * amp[324] + 1./3. * amp[325] + 1./3.
      * amp[326] + 1./3. * amp[327] + 1./3. * amp[354] + 1./3. * amp[355] +
      1./3. * amp[358] + 1./3. * amp[359]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[7][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P123_sm_qb_llgqb::matrix_4_ub_vexvegub() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[360] + 1./3. * amp[361] + 1./3. * amp[362] +
      1./3. * amp[363] + 1./3. * amp[364] + 1./3. * amp[365] + 1./3. * amp[366]
      + 1./3. * amp[367] + 1./3. * amp[382] + 1./3. * amp[383]);
  jamp[1] = +1./2. * (-amp[364] - amp[365] - amp[366] - amp[367] - amp[372] -
      amp[373] - amp[374] - amp[375] - Complex<double> (0, 1) * amp[376] -
      Complex<double> (0, 1) * amp[377] - amp[378] + Complex<double> (0, 1) *
      amp[380] + Complex<double> (0, 1) * amp[381] - amp[383]);
  jamp[2] = +1./2. * (-amp[360] - amp[361] - amp[362] - amp[363] - amp[368] -
      amp[369] - amp[370] - amp[371] + Complex<double> (0, 1) * amp[376] +
      Complex<double> (0, 1) * amp[377] - amp[379] - Complex<double> (0, 1) *
      amp[380] - Complex<double> (0, 1) * amp[381] - amp[382]);
  jamp[3] = +1./2. * (+1./3. * amp[368] + 1./3. * amp[369] + 1./3. * amp[370] +
      1./3. * amp[371] + 1./3. * amp[372] + 1./3. * amp[373] + 1./3. * amp[374]
      + 1./3. * amp[375] + 1./3. * amp[378] + 1./3. * amp[379]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[8][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P123_sm_qb_llgqb::matrix_4_ubx_vexvegubx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[388] + amp[389] + amp[390] + amp[391] + amp[396] +
      amp[397] + amp[398] + amp[399] + Complex<double> (0, 1) * amp[400] +
      Complex<double> (0, 1) * amp[401] + amp[402] - Complex<double> (0, 1) *
      amp[404] - Complex<double> (0, 1) * amp[405] + amp[407]);
  jamp[1] = +1./2. * (-1./3. * amp[384] - 1./3. * amp[385] - 1./3. * amp[386] -
      1./3. * amp[387] - 1./3. * amp[388] - 1./3. * amp[389] - 1./3. * amp[390]
      - 1./3. * amp[391] - 1./3. * amp[406] - 1./3. * amp[407]);
  jamp[2] = +1./2. * (+amp[384] + amp[385] + amp[386] + amp[387] + amp[392] +
      amp[393] + amp[394] + amp[395] - Complex<double> (0, 1) * amp[400] -
      Complex<double> (0, 1) * amp[401] + amp[403] + Complex<double> (0, 1) *
      amp[404] + Complex<double> (0, 1) * amp[405] + amp[406]);
  jamp[3] = +1./2. * (-1./3. * amp[392] - 1./3. * amp[393] - 1./3. * amp[394] -
      1./3. * amp[395] - 1./3. * amp[396] - 1./3. * amp[397] - 1./3. * amp[398]
      - 1./3. * amp[399] - 1./3. * amp[402] - 1./3. * amp[403]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[9][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P123_sm_qb_llgqb::matrix_4_db_vexvegdb() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[408] + 1./3. * amp[409] + 1./3. * amp[410] +
      1./3. * amp[411] + 1./3. * amp[412] + 1./3. * amp[413] + 1./3. * amp[414]
      + 1./3. * amp[415] + 1./3. * amp[430] + 1./3. * amp[431]);
  jamp[1] = +1./2. * (-amp[412] - amp[413] - amp[414] - amp[415] - amp[420] -
      amp[421] - amp[422] - amp[423] - Complex<double> (0, 1) * amp[424] -
      Complex<double> (0, 1) * amp[425] - amp[426] + Complex<double> (0, 1) *
      amp[428] + Complex<double> (0, 1) * amp[429] - amp[431]);
  jamp[2] = +1./2. * (-amp[408] - amp[409] - amp[410] - amp[411] - amp[416] -
      amp[417] - amp[418] - amp[419] + Complex<double> (0, 1) * amp[424] +
      Complex<double> (0, 1) * amp[425] - amp[427] - Complex<double> (0, 1) *
      amp[428] - Complex<double> (0, 1) * amp[429] - amp[430]);
  jamp[3] = +1./2. * (+1./3. * amp[416] + 1./3. * amp[417] + 1./3. * amp[418] +
      1./3. * amp[419] + 1./3. * amp[420] + 1./3. * amp[421] + 1./3. * amp[422]
      + 1./3. * amp[423] + 1./3. * amp[426] + 1./3. * amp[427]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[10][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P123_sm_qb_llgqb::matrix_4_dbx_vexvegdbx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[436] + amp[437] + amp[438] + amp[439] + amp[444] +
      amp[445] + amp[446] + amp[447] + Complex<double> (0, 1) * amp[448] +
      Complex<double> (0, 1) * amp[449] + amp[450] - Complex<double> (0, 1) *
      amp[452] - Complex<double> (0, 1) * amp[453] + amp[455]);
  jamp[1] = +1./2. * (-1./3. * amp[432] - 1./3. * amp[433] - 1./3. * amp[434] -
      1./3. * amp[435] - 1./3. * amp[436] - 1./3. * amp[437] - 1./3. * amp[438]
      - 1./3. * amp[439] - 1./3. * amp[454] - 1./3. * amp[455]);
  jamp[2] = +1./2. * (+amp[432] + amp[433] + amp[434] + amp[435] + amp[440] +
      amp[441] + amp[442] + amp[443] - Complex<double> (0, 1) * amp[448] -
      Complex<double> (0, 1) * amp[449] + amp[451] + Complex<double> (0, 1) *
      amp[452] + Complex<double> (0, 1) * amp[453] + amp[454]);
  jamp[3] = +1./2. * (-1./3. * amp[440] - 1./3. * amp[441] - 1./3. * amp[442] -
      1./3. * amp[443] - 1./3. * amp[444] - 1./3. * amp[445] - 1./3. * amp[446]
      - 1./3. * amp[447] - 1./3. * amp[450] - 1./3. * amp[451]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[11][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P123_sm_qb_llgqb::matrix_4_uxb_vexveguxb() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[456] + amp[457] + amp[458] + amp[459] + amp[464] +
      amp[465] + amp[466] + amp[467] - Complex<double> (0, 1) * amp[472] -
      Complex<double> (0, 1) * amp[473] + amp[475] + Complex<double> (0, 1) *
      amp[476] + Complex<double> (0, 1) * amp[477] + amp[478]);
  jamp[1] = +1./2. * (-1./3. * amp[464] - 1./3. * amp[465] - 1./3. * amp[466] -
      1./3. * amp[467] - 1./3. * amp[468] - 1./3. * amp[469] - 1./3. * amp[470]
      - 1./3. * amp[471] - 1./3. * amp[474] - 1./3. * amp[475]);
  jamp[2] = +1./2. * (+amp[460] + amp[461] + amp[462] + amp[463] + amp[468] +
      amp[469] + amp[470] + amp[471] + Complex<double> (0, 1) * amp[472] +
      Complex<double> (0, 1) * amp[473] + amp[474] - Complex<double> (0, 1) *
      amp[476] - Complex<double> (0, 1) * amp[477] + amp[479]);
  jamp[3] = +1./2. * (-1./3. * amp[456] - 1./3. * amp[457] - 1./3. * amp[458] -
      1./3. * amp[459] - 1./3. * amp[460] - 1./3. * amp[461] - 1./3. * amp[462]
      - 1./3. * amp[463] - 1./3. * amp[478] - 1./3. * amp[479]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[12][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P123_sm_qb_llgqb::matrix_4_uxbx_vexveguxbx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[488] + 1./3. * amp[489] + 1./3. * amp[490] +
      1./3. * amp[491] + 1./3. * amp[492] + 1./3. * amp[493] + 1./3. * amp[494]
      + 1./3. * amp[495] + 1./3. * amp[498] + 1./3. * amp[499]);
  jamp[1] = +1./2. * (-amp[480] - amp[481] - amp[482] - amp[483] - amp[488] -
      amp[489] - amp[490] - amp[491] + Complex<double> (0, 1) * amp[496] +
      Complex<double> (0, 1) * amp[497] - amp[499] - Complex<double> (0, 1) *
      amp[500] - Complex<double> (0, 1) * amp[501] - amp[502]);
  jamp[2] = +1./2. * (-amp[484] - amp[485] - amp[486] - amp[487] - amp[492] -
      amp[493] - amp[494] - amp[495] - Complex<double> (0, 1) * amp[496] -
      Complex<double> (0, 1) * amp[497] - amp[498] + Complex<double> (0, 1) *
      amp[500] + Complex<double> (0, 1) * amp[501] - amp[503]);
  jamp[3] = +1./2. * (+1./3. * amp[480] + 1./3. * amp[481] + 1./3. * amp[482] +
      1./3. * amp[483] + 1./3. * amp[484] + 1./3. * amp[485] + 1./3. * amp[486]
      + 1./3. * amp[487] + 1./3. * amp[502] + 1./3. * amp[503]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[13][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P123_sm_qb_llgqb::matrix_4_dxb_vexvegdxb() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[504] + amp[505] + amp[506] + amp[507] + amp[512] +
      amp[513] + amp[514] + amp[515] - Complex<double> (0, 1) * amp[520] -
      Complex<double> (0, 1) * amp[521] + amp[523] + Complex<double> (0, 1) *
      amp[524] + Complex<double> (0, 1) * amp[525] + amp[526]);
  jamp[1] = +1./2. * (-1./3. * amp[512] - 1./3. * amp[513] - 1./3. * amp[514] -
      1./3. * amp[515] - 1./3. * amp[516] - 1./3. * amp[517] - 1./3. * amp[518]
      - 1./3. * amp[519] - 1./3. * amp[522] - 1./3. * amp[523]);
  jamp[2] = +1./2. * (+amp[508] + amp[509] + amp[510] + amp[511] + amp[516] +
      amp[517] + amp[518] + amp[519] + Complex<double> (0, 1) * amp[520] +
      Complex<double> (0, 1) * amp[521] + amp[522] - Complex<double> (0, 1) *
      amp[524] - Complex<double> (0, 1) * amp[525] + amp[527]);
  jamp[3] = +1./2. * (-1./3. * amp[504] - 1./3. * amp[505] - 1./3. * amp[506] -
      1./3. * amp[507] - 1./3. * amp[508] - 1./3. * amp[509] - 1./3. * amp[510]
      - 1./3. * amp[511] - 1./3. * amp[526] - 1./3. * amp[527]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[14][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P123_sm_qb_llgqb::matrix_4_dxbx_vexvegdxbx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[536] + 1./3. * amp[537] + 1./3. * amp[538] +
      1./3. * amp[539] + 1./3. * amp[540] + 1./3. * amp[541] + 1./3. * amp[542]
      + 1./3. * amp[543] + 1./3. * amp[546] + 1./3. * amp[547]);
  jamp[1] = +1./2. * (-amp[528] - amp[529] - amp[530] - amp[531] - amp[536] -
      amp[537] - amp[538] - amp[539] + Complex<double> (0, 1) * amp[544] +
      Complex<double> (0, 1) * amp[545] - amp[547] - Complex<double> (0, 1) *
      amp[548] - Complex<double> (0, 1) * amp[549] - amp[550]);
  jamp[2] = +1./2. * (-amp[532] - amp[533] - amp[534] - amp[535] - amp[540] -
      amp[541] - amp[542] - amp[543] - Complex<double> (0, 1) * amp[544] -
      Complex<double> (0, 1) * amp[545] - amp[546] + Complex<double> (0, 1) *
      amp[548] + Complex<double> (0, 1) * amp[549] - amp[551]);
  jamp[3] = +1./2. * (+1./3. * amp[528] + 1./3. * amp[529] + 1./3. * amp[530] +
      1./3. * amp[531] + 1./3. * amp[532] + 1./3. * amp[533] + 1./3. * amp[534]
      + 1./3. * amp[535] + 1./3. * amp[550] + 1./3. * amp[551]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[15][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P123_sm_qb_llgqb::matrix_4_ub_epvegdb() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 12;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[552] + 1./3. * amp[553] + 1./3. * amp[554] +
      1./3. * amp[555] + 1./3. * amp[562] + 1./3. * amp[563]);
  jamp[1] = +1./2. * (-amp[554] - amp[555] - amp[558] - amp[559] +
      Complex<double> (0, 1) * amp[560] + Complex<double> (0, 1) * amp[561] -
      amp[563]);
  jamp[2] = +1./2. * (-amp[552] - amp[553] - amp[556] - amp[557] -
      Complex<double> (0, 1) * amp[560] - Complex<double> (0, 1) * amp[561] -
      amp[562]);
  jamp[3] = +1./2. * (+1./3. * amp[556] + 1./3. * amp[557] + 1./3. * amp[558] +
      1./3. * amp[559]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[16][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P123_sm_qb_llgqb::matrix_4_ubx_epvegdbx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 12;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[566] + amp[567] + amp[570] + amp[571] -
      Complex<double> (0, 1) * amp[572] - Complex<double> (0, 1) * amp[573] +
      amp[575]);
  jamp[1] = +1./2. * (-1./3. * amp[564] - 1./3. * amp[565] - 1./3. * amp[566] -
      1./3. * amp[567] - 1./3. * amp[574] - 1./3. * amp[575]);
  jamp[2] = +1./2. * (+amp[564] + amp[565] + amp[568] + amp[569] +
      Complex<double> (0, 1) * amp[572] + Complex<double> (0, 1) * amp[573] +
      amp[574]);
  jamp[3] = +1./2. * (-1./3. * amp[568] - 1./3. * amp[569] - 1./3. * amp[570] -
      1./3. * amp[571]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[17][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P123_sm_qb_llgqb::matrix_4_db_vexemgub() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 12;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[576] + 1./3. * amp[577] + 1./3. * amp[578] +
      1./3. * amp[579] + 1./3. * amp[586] + 1./3. * amp[587]);
  jamp[1] = +1./2. * (-amp[578] - amp[579] - amp[582] - amp[583] +
      Complex<double> (0, 1) * amp[584] + Complex<double> (0, 1) * amp[585] -
      amp[587]);
  jamp[2] = +1./2. * (-amp[576] - amp[577] - amp[580] - amp[581] -
      Complex<double> (0, 1) * amp[584] - Complex<double> (0, 1) * amp[585] -
      amp[586]);
  jamp[3] = +1./2. * (+1./3. * amp[580] + 1./3. * amp[581] + 1./3. * amp[582] +
      1./3. * amp[583]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[18][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P123_sm_qb_llgqb::matrix_4_dbx_vexemgubx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 12;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[590] + amp[591] + amp[594] + amp[595] -
      Complex<double> (0, 1) * amp[596] - Complex<double> (0, 1) * amp[597] +
      amp[599]);
  jamp[1] = +1./2. * (-1./3. * amp[588] - 1./3. * amp[589] - 1./3. * amp[590] -
      1./3. * amp[591] - 1./3. * amp[598] - 1./3. * amp[599]);
  jamp[2] = +1./2. * (+amp[588] + amp[589] + amp[592] + amp[593] +
      Complex<double> (0, 1) * amp[596] + Complex<double> (0, 1) * amp[597] +
      amp[598]);
  jamp[3] = +1./2. * (-1./3. * amp[592] - 1./3. * amp[593] - 1./3. * amp[594] -
      1./3. * amp[595]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[19][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P123_sm_qb_llgqb::matrix_4_uxb_vexemgdxb() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 12;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[600] + amp[601] + amp[604] + amp[605] +
      Complex<double> (0, 1) * amp[608] + Complex<double> (0, 1) * amp[609] +
      amp[610]);
  jamp[1] = +1./2. * (-1./3. * amp[604] - 1./3. * amp[605] - 1./3. * amp[606] -
      1./3. * amp[607]);
  jamp[2] = +1./2. * (+amp[602] + amp[603] + amp[606] + amp[607] -
      Complex<double> (0, 1) * amp[608] - Complex<double> (0, 1) * amp[609] +
      amp[611]);
  jamp[3] = +1./2. * (-1./3. * amp[600] - 1./3. * amp[601] - 1./3. * amp[602] -
      1./3. * amp[603] - 1./3. * amp[610] - 1./3. * amp[611]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[20][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P123_sm_qb_llgqb::matrix_4_uxbx_vexemgdxbx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 12;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[616] + 1./3. * amp[617] + 1./3. * amp[618] +
      1./3. * amp[619]);
  jamp[1] = +1./2. * (-amp[612] - amp[613] - amp[616] - amp[617] -
      Complex<double> (0, 1) * amp[620] - Complex<double> (0, 1) * amp[621] -
      amp[622]);
  jamp[2] = +1./2. * (-amp[614] - amp[615] - amp[618] - amp[619] +
      Complex<double> (0, 1) * amp[620] + Complex<double> (0, 1) * amp[621] -
      amp[623]);
  jamp[3] = +1./2. * (+1./3. * amp[612] + 1./3. * amp[613] + 1./3. * amp[614] +
      1./3. * amp[615] + 1./3. * amp[622] + 1./3. * amp[623]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[21][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P123_sm_qb_llgqb::matrix_4_dxb_epveguxb() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 12;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[624] + amp[625] + amp[628] + amp[629] +
      Complex<double> (0, 1) * amp[632] + Complex<double> (0, 1) * amp[633] +
      amp[634]);
  jamp[1] = +1./2. * (-1./3. * amp[628] - 1./3. * amp[629] - 1./3. * amp[630] -
      1./3. * amp[631]);
  jamp[2] = +1./2. * (+amp[626] + amp[627] + amp[630] + amp[631] -
      Complex<double> (0, 1) * amp[632] - Complex<double> (0, 1) * amp[633] +
      amp[635]);
  jamp[3] = +1./2. * (-1./3. * amp[624] - 1./3. * amp[625] - 1./3. * amp[626] -
      1./3. * amp[627] - 1./3. * amp[634] - 1./3. * amp[635]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[22][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P123_sm_qb_llgqb::matrix_4_dxbx_epveguxbx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 12;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[640] + 1./3. * amp[641] + 1./3. * amp[642] +
      1./3. * amp[643]);
  jamp[1] = +1./2. * (-amp[636] - amp[637] - amp[640] - amp[641] -
      Complex<double> (0, 1) * amp[644] - Complex<double> (0, 1) * amp[645] -
      amp[646]);
  jamp[2] = +1./2. * (-amp[638] - amp[639] - amp[642] - amp[643] +
      Complex<double> (0, 1) * amp[644] + Complex<double> (0, 1) * amp[645] -
      amp[647]);
  jamp[3] = +1./2. * (+1./3. * amp[636] + 1./3. * amp[637] + 1./3. * amp[638] +
      1./3. * amp[639] + 1./3. * amp[646] + 1./3. * amp[647]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[23][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}


}  // end namespace PY8MEs_namespace

