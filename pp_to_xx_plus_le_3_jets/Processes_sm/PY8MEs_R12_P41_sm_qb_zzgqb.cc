//==========================================================================
// This file has been automatically generated for Pythia 8 by
// MadGraph5_aMC@NLO v. 2.6.0, 2017-08-16
// By the MadGraph5_aMC@NLO Development Team
// Visit launchpad.net/madgraph5 and amcatnlo.web.cern.ch
//==========================================================================

#include "PY8MEs_R12_P41_sm_qb_zzgqb.h"
#include "HelAmps_sm.h"

using namespace Pythia8_sm; 

namespace PY8MEs_namespace 
{
//==========================================================================
// Class member functions for calculating the matrix elements for
// Process: u b > z z g u b WEIGHTED<=7 @12
// Process: c b > z z g c b WEIGHTED<=7 @12
// Process: u b~ > z z g u b~ WEIGHTED<=7 @12
// Process: c b~ > z z g c b~ WEIGHTED<=7 @12
// Process: d b > z z g d b WEIGHTED<=7 @12
// Process: s b > z z g s b WEIGHTED<=7 @12
// Process: d b~ > z z g d b~ WEIGHTED<=7 @12
// Process: s b~ > z z g s b~ WEIGHTED<=7 @12
// Process: u~ b > z z g u~ b WEIGHTED<=7 @12
// Process: c~ b > z z g c~ b WEIGHTED<=7 @12
// Process: u~ b~ > z z g u~ b~ WEIGHTED<=7 @12
// Process: c~ b~ > z z g c~ b~ WEIGHTED<=7 @12
// Process: d~ b > z z g d~ b WEIGHTED<=7 @12
// Process: s~ b > z z g s~ b WEIGHTED<=7 @12
// Process: d~ b~ > z z g d~ b~ WEIGHTED<=7 @12
// Process: s~ b~ > z z g s~ b~ WEIGHTED<=7 @12

// Exception class
class PY8MEs_R12_P41_sm_qb_zzgqbException : public exception
{
  virtual const char * what() const throw()
  {
    return "Exception in class 'PY8MEs_R12_P41_sm_qb_zzgqb'."; 
  }
}
PY8MEs_R12_P41_sm_qb_zzgqb_exception; 

std::set<int> PY8MEs_R12_P41_sm_qb_zzgqb::s_channel_proc = std::set<int>
    (createset<int> ());

int PY8MEs_R12_P41_sm_qb_zzgqb::helicities[ncomb][nexternal] = {{-1, -1, -1,
    -1, -1, -1, -1}, {-1, -1, -1, -1, -1, -1, 1}, {-1, -1, -1, -1, -1, 1, -1},
    {-1, -1, -1, -1, -1, 1, 1}, {-1, -1, -1, -1, 1, -1, -1}, {-1, -1, -1, -1,
    1, -1, 1}, {-1, -1, -1, -1, 1, 1, -1}, {-1, -1, -1, -1, 1, 1, 1}, {-1, -1,
    -1, 0, -1, -1, -1}, {-1, -1, -1, 0, -1, -1, 1}, {-1, -1, -1, 0, -1, 1, -1},
    {-1, -1, -1, 0, -1, 1, 1}, {-1, -1, -1, 0, 1, -1, -1}, {-1, -1, -1, 0, 1,
    -1, 1}, {-1, -1, -1, 0, 1, 1, -1}, {-1, -1, -1, 0, 1, 1, 1}, {-1, -1, -1,
    1, -1, -1, -1}, {-1, -1, -1, 1, -1, -1, 1}, {-1, -1, -1, 1, -1, 1, -1},
    {-1, -1, -1, 1, -1, 1, 1}, {-1, -1, -1, 1, 1, -1, -1}, {-1, -1, -1, 1, 1,
    -1, 1}, {-1, -1, -1, 1, 1, 1, -1}, {-1, -1, -1, 1, 1, 1, 1}, {-1, -1, 0,
    -1, -1, -1, -1}, {-1, -1, 0, -1, -1, -1, 1}, {-1, -1, 0, -1, -1, 1, -1},
    {-1, -1, 0, -1, -1, 1, 1}, {-1, -1, 0, -1, 1, -1, -1}, {-1, -1, 0, -1, 1,
    -1, 1}, {-1, -1, 0, -1, 1, 1, -1}, {-1, -1, 0, -1, 1, 1, 1}, {-1, -1, 0, 0,
    -1, -1, -1}, {-1, -1, 0, 0, -1, -1, 1}, {-1, -1, 0, 0, -1, 1, -1}, {-1, -1,
    0, 0, -1, 1, 1}, {-1, -1, 0, 0, 1, -1, -1}, {-1, -1, 0, 0, 1, -1, 1}, {-1,
    -1, 0, 0, 1, 1, -1}, {-1, -1, 0, 0, 1, 1, 1}, {-1, -1, 0, 1, -1, -1, -1},
    {-1, -1, 0, 1, -1, -1, 1}, {-1, -1, 0, 1, -1, 1, -1}, {-1, -1, 0, 1, -1, 1,
    1}, {-1, -1, 0, 1, 1, -1, -1}, {-1, -1, 0, 1, 1, -1, 1}, {-1, -1, 0, 1, 1,
    1, -1}, {-1, -1, 0, 1, 1, 1, 1}, {-1, -1, 1, -1, -1, -1, -1}, {-1, -1, 1,
    -1, -1, -1, 1}, {-1, -1, 1, -1, -1, 1, -1}, {-1, -1, 1, -1, -1, 1, 1}, {-1,
    -1, 1, -1, 1, -1, -1}, {-1, -1, 1, -1, 1, -1, 1}, {-1, -1, 1, -1, 1, 1,
    -1}, {-1, -1, 1, -1, 1, 1, 1}, {-1, -1, 1, 0, -1, -1, -1}, {-1, -1, 1, 0,
    -1, -1, 1}, {-1, -1, 1, 0, -1, 1, -1}, {-1, -1, 1, 0, -1, 1, 1}, {-1, -1,
    1, 0, 1, -1, -1}, {-1, -1, 1, 0, 1, -1, 1}, {-1, -1, 1, 0, 1, 1, -1}, {-1,
    -1, 1, 0, 1, 1, 1}, {-1, -1, 1, 1, -1, -1, -1}, {-1, -1, 1, 1, -1, -1, 1},
    {-1, -1, 1, 1, -1, 1, -1}, {-1, -1, 1, 1, -1, 1, 1}, {-1, -1, 1, 1, 1, -1,
    -1}, {-1, -1, 1, 1, 1, -1, 1}, {-1, -1, 1, 1, 1, 1, -1}, {-1, -1, 1, 1, 1,
    1, 1}, {-1, 1, -1, -1, -1, -1, -1}, {-1, 1, -1, -1, -1, -1, 1}, {-1, 1, -1,
    -1, -1, 1, -1}, {-1, 1, -1, -1, -1, 1, 1}, {-1, 1, -1, -1, 1, -1, -1}, {-1,
    1, -1, -1, 1, -1, 1}, {-1, 1, -1, -1, 1, 1, -1}, {-1, 1, -1, -1, 1, 1, 1},
    {-1, 1, -1, 0, -1, -1, -1}, {-1, 1, -1, 0, -1, -1, 1}, {-1, 1, -1, 0, -1,
    1, -1}, {-1, 1, -1, 0, -1, 1, 1}, {-1, 1, -1, 0, 1, -1, -1}, {-1, 1, -1, 0,
    1, -1, 1}, {-1, 1, -1, 0, 1, 1, -1}, {-1, 1, -1, 0, 1, 1, 1}, {-1, 1, -1,
    1, -1, -1, -1}, {-1, 1, -1, 1, -1, -1, 1}, {-1, 1, -1, 1, -1, 1, -1}, {-1,
    1, -1, 1, -1, 1, 1}, {-1, 1, -1, 1, 1, -1, -1}, {-1, 1, -1, 1, 1, -1, 1},
    {-1, 1, -1, 1, 1, 1, -1}, {-1, 1, -1, 1, 1, 1, 1}, {-1, 1, 0, -1, -1, -1,
    -1}, {-1, 1, 0, -1, -1, -1, 1}, {-1, 1, 0, -1, -1, 1, -1}, {-1, 1, 0, -1,
    -1, 1, 1}, {-1, 1, 0, -1, 1, -1, -1}, {-1, 1, 0, -1, 1, -1, 1}, {-1, 1, 0,
    -1, 1, 1, -1}, {-1, 1, 0, -1, 1, 1, 1}, {-1, 1, 0, 0, -1, -1, -1}, {-1, 1,
    0, 0, -1, -1, 1}, {-1, 1, 0, 0, -1, 1, -1}, {-1, 1, 0, 0, -1, 1, 1}, {-1,
    1, 0, 0, 1, -1, -1}, {-1, 1, 0, 0, 1, -1, 1}, {-1, 1, 0, 0, 1, 1, -1}, {-1,
    1, 0, 0, 1, 1, 1}, {-1, 1, 0, 1, -1, -1, -1}, {-1, 1, 0, 1, -1, -1, 1},
    {-1, 1, 0, 1, -1, 1, -1}, {-1, 1, 0, 1, -1, 1, 1}, {-1, 1, 0, 1, 1, -1,
    -1}, {-1, 1, 0, 1, 1, -1, 1}, {-1, 1, 0, 1, 1, 1, -1}, {-1, 1, 0, 1, 1, 1,
    1}, {-1, 1, 1, -1, -1, -1, -1}, {-1, 1, 1, -1, -1, -1, 1}, {-1, 1, 1, -1,
    -1, 1, -1}, {-1, 1, 1, -1, -1, 1, 1}, {-1, 1, 1, -1, 1, -1, -1}, {-1, 1, 1,
    -1, 1, -1, 1}, {-1, 1, 1, -1, 1, 1, -1}, {-1, 1, 1, -1, 1, 1, 1}, {-1, 1,
    1, 0, -1, -1, -1}, {-1, 1, 1, 0, -1, -1, 1}, {-1, 1, 1, 0, -1, 1, -1}, {-1,
    1, 1, 0, -1, 1, 1}, {-1, 1, 1, 0, 1, -1, -1}, {-1, 1, 1, 0, 1, -1, 1}, {-1,
    1, 1, 0, 1, 1, -1}, {-1, 1, 1, 0, 1, 1, 1}, {-1, 1, 1, 1, -1, -1, -1}, {-1,
    1, 1, 1, -1, -1, 1}, {-1, 1, 1, 1, -1, 1, -1}, {-1, 1, 1, 1, -1, 1, 1},
    {-1, 1, 1, 1, 1, -1, -1}, {-1, 1, 1, 1, 1, -1, 1}, {-1, 1, 1, 1, 1, 1, -1},
    {-1, 1, 1, 1, 1, 1, 1}, {1, -1, -1, -1, -1, -1, -1}, {1, -1, -1, -1, -1,
    -1, 1}, {1, -1, -1, -1, -1, 1, -1}, {1, -1, -1, -1, -1, 1, 1}, {1, -1, -1,
    -1, 1, -1, -1}, {1, -1, -1, -1, 1, -1, 1}, {1, -1, -1, -1, 1, 1, -1}, {1,
    -1, -1, -1, 1, 1, 1}, {1, -1, -1, 0, -1, -1, -1}, {1, -1, -1, 0, -1, -1,
    1}, {1, -1, -1, 0, -1, 1, -1}, {1, -1, -1, 0, -1, 1, 1}, {1, -1, -1, 0, 1,
    -1, -1}, {1, -1, -1, 0, 1, -1, 1}, {1, -1, -1, 0, 1, 1, -1}, {1, -1, -1, 0,
    1, 1, 1}, {1, -1, -1, 1, -1, -1, -1}, {1, -1, -1, 1, -1, -1, 1}, {1, -1,
    -1, 1, -1, 1, -1}, {1, -1, -1, 1, -1, 1, 1}, {1, -1, -1, 1, 1, -1, -1}, {1,
    -1, -1, 1, 1, -1, 1}, {1, -1, -1, 1, 1, 1, -1}, {1, -1, -1, 1, 1, 1, 1},
    {1, -1, 0, -1, -1, -1, -1}, {1, -1, 0, -1, -1, -1, 1}, {1, -1, 0, -1, -1,
    1, -1}, {1, -1, 0, -1, -1, 1, 1}, {1, -1, 0, -1, 1, -1, -1}, {1, -1, 0, -1,
    1, -1, 1}, {1, -1, 0, -1, 1, 1, -1}, {1, -1, 0, -1, 1, 1, 1}, {1, -1, 0, 0,
    -1, -1, -1}, {1, -1, 0, 0, -1, -1, 1}, {1, -1, 0, 0, -1, 1, -1}, {1, -1, 0,
    0, -1, 1, 1}, {1, -1, 0, 0, 1, -1, -1}, {1, -1, 0, 0, 1, -1, 1}, {1, -1, 0,
    0, 1, 1, -1}, {1, -1, 0, 0, 1, 1, 1}, {1, -1, 0, 1, -1, -1, -1}, {1, -1, 0,
    1, -1, -1, 1}, {1, -1, 0, 1, -1, 1, -1}, {1, -1, 0, 1, -1, 1, 1}, {1, -1,
    0, 1, 1, -1, -1}, {1, -1, 0, 1, 1, -1, 1}, {1, -1, 0, 1, 1, 1, -1}, {1, -1,
    0, 1, 1, 1, 1}, {1, -1, 1, -1, -1, -1, -1}, {1, -1, 1, -1, -1, -1, 1}, {1,
    -1, 1, -1, -1, 1, -1}, {1, -1, 1, -1, -1, 1, 1}, {1, -1, 1, -1, 1, -1, -1},
    {1, -1, 1, -1, 1, -1, 1}, {1, -1, 1, -1, 1, 1, -1}, {1, -1, 1, -1, 1, 1,
    1}, {1, -1, 1, 0, -1, -1, -1}, {1, -1, 1, 0, -1, -1, 1}, {1, -1, 1, 0, -1,
    1, -1}, {1, -1, 1, 0, -1, 1, 1}, {1, -1, 1, 0, 1, -1, -1}, {1, -1, 1, 0, 1,
    -1, 1}, {1, -1, 1, 0, 1, 1, -1}, {1, -1, 1, 0, 1, 1, 1}, {1, -1, 1, 1, -1,
    -1, -1}, {1, -1, 1, 1, -1, -1, 1}, {1, -1, 1, 1, -1, 1, -1}, {1, -1, 1, 1,
    -1, 1, 1}, {1, -1, 1, 1, 1, -1, -1}, {1, -1, 1, 1, 1, -1, 1}, {1, -1, 1, 1,
    1, 1, -1}, {1, -1, 1, 1, 1, 1, 1}, {1, 1, -1, -1, -1, -1, -1}, {1, 1, -1,
    -1, -1, -1, 1}, {1, 1, -1, -1, -1, 1, -1}, {1, 1, -1, -1, -1, 1, 1}, {1, 1,
    -1, -1, 1, -1, -1}, {1, 1, -1, -1, 1, -1, 1}, {1, 1, -1, -1, 1, 1, -1}, {1,
    1, -1, -1, 1, 1, 1}, {1, 1, -1, 0, -1, -1, -1}, {1, 1, -1, 0, -1, -1, 1},
    {1, 1, -1, 0, -1, 1, -1}, {1, 1, -1, 0, -1, 1, 1}, {1, 1, -1, 0, 1, -1,
    -1}, {1, 1, -1, 0, 1, -1, 1}, {1, 1, -1, 0, 1, 1, -1}, {1, 1, -1, 0, 1, 1,
    1}, {1, 1, -1, 1, -1, -1, -1}, {1, 1, -1, 1, -1, -1, 1}, {1, 1, -1, 1, -1,
    1, -1}, {1, 1, -1, 1, -1, 1, 1}, {1, 1, -1, 1, 1, -1, -1}, {1, 1, -1, 1, 1,
    -1, 1}, {1, 1, -1, 1, 1, 1, -1}, {1, 1, -1, 1, 1, 1, 1}, {1, 1, 0, -1, -1,
    -1, -1}, {1, 1, 0, -1, -1, -1, 1}, {1, 1, 0, -1, -1, 1, -1}, {1, 1, 0, -1,
    -1, 1, 1}, {1, 1, 0, -1, 1, -1, -1}, {1, 1, 0, -1, 1, -1, 1}, {1, 1, 0, -1,
    1, 1, -1}, {1, 1, 0, -1, 1, 1, 1}, {1, 1, 0, 0, -1, -1, -1}, {1, 1, 0, 0,
    -1, -1, 1}, {1, 1, 0, 0, -1, 1, -1}, {1, 1, 0, 0, -1, 1, 1}, {1, 1, 0, 0,
    1, -1, -1}, {1, 1, 0, 0, 1, -1, 1}, {1, 1, 0, 0, 1, 1, -1}, {1, 1, 0, 0, 1,
    1, 1}, {1, 1, 0, 1, -1, -1, -1}, {1, 1, 0, 1, -1, -1, 1}, {1, 1, 0, 1, -1,
    1, -1}, {1, 1, 0, 1, -1, 1, 1}, {1, 1, 0, 1, 1, -1, -1}, {1, 1, 0, 1, 1,
    -1, 1}, {1, 1, 0, 1, 1, 1, -1}, {1, 1, 0, 1, 1, 1, 1}, {1, 1, 1, -1, -1,
    -1, -1}, {1, 1, 1, -1, -1, -1, 1}, {1, 1, 1, -1, -1, 1, -1}, {1, 1, 1, -1,
    -1, 1, 1}, {1, 1, 1, -1, 1, -1, -1}, {1, 1, 1, -1, 1, -1, 1}, {1, 1, 1, -1,
    1, 1, -1}, {1, 1, 1, -1, 1, 1, 1}, {1, 1, 1, 0, -1, -1, -1}, {1, 1, 1, 0,
    -1, -1, 1}, {1, 1, 1, 0, -1, 1, -1}, {1, 1, 1, 0, -1, 1, 1}, {1, 1, 1, 0,
    1, -1, -1}, {1, 1, 1, 0, 1, -1, 1}, {1, 1, 1, 0, 1, 1, -1}, {1, 1, 1, 0, 1,
    1, 1}, {1, 1, 1, 1, -1, -1, -1}, {1, 1, 1, 1, -1, -1, 1}, {1, 1, 1, 1, -1,
    1, -1}, {1, 1, 1, 1, -1, 1, 1}, {1, 1, 1, 1, 1, -1, -1}, {1, 1, 1, 1, 1,
    -1, 1}, {1, 1, 1, 1, 1, 1, -1}, {1, 1, 1, 1, 1, 1, 1}};

// Normalization factors the various processes
// Denominators: spins, colors and identical particles
int PY8MEs_R12_P41_sm_qb_zzgqb::denom_colors[nprocesses] = {9, 9, 9, 9, 9, 9,
    9, 9};
int PY8MEs_R12_P41_sm_qb_zzgqb::denom_hels[nprocesses] = {4, 4, 4, 4, 4, 4, 4,
    4};
int PY8MEs_R12_P41_sm_qb_zzgqb::denom_iden[nprocesses] = {2, 2, 2, 2, 2, 2, 2,
    2};

//--------------------------------------------------------------------------
// Color config initialization
void PY8MEs_R12_P41_sm_qb_zzgqb::initColorConfigs() 
{
  color_configs = vector < vec_vec_int > (); 
  jamp_nc_relative_power = vector < vec_int > (); 

  // Color flows of process Process: u b > z z g u b WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[0].push_back(-1); 
  // JAMP #1
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #2
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #3
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[0].push_back(-1); 

  // Color flows of process Process: u b~ > z z g u b~ WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #1
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[1].push_back(-1); 
  // JAMP #2
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #3
  color_configs[1].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[1].push_back(-1); 

  // Color flows of process Process: d b > z z g d b WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[2].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[2].push_back(-1); 
  // JAMP #1
  color_configs[2].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #2
  color_configs[2].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #3
  color_configs[2].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[2].push_back(-1); 

  // Color flows of process Process: d b~ > z z g d b~ WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[3].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #1
  color_configs[3].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[3].push_back(-1); 
  // JAMP #2
  color_configs[3].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #3
  color_configs[3].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[3].push_back(-1); 

  // Color flows of process Process: u~ b > z z g u~ b WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[4].push_back(vec_int(createvector<int>
      (0)(1)(1)(0)(0)(0)(0)(0)(3)(2)(0)(3)(2)(0)));
  jamp_nc_relative_power[4].push_back(0); 
  // JAMP #1
  color_configs[4].push_back(vec_int(createvector<int>
      (0)(1)(3)(0)(0)(0)(0)(0)(3)(2)(0)(1)(2)(0)));
  jamp_nc_relative_power[4].push_back(-1); 
  // JAMP #2
  color_configs[4].push_back(vec_int(createvector<int>
      (0)(1)(3)(0)(0)(0)(0)(0)(3)(1)(0)(2)(2)(0)));
  jamp_nc_relative_power[4].push_back(0); 
  // JAMP #3
  color_configs[4].push_back(vec_int(createvector<int>
      (0)(1)(2)(0)(0)(0)(0)(0)(3)(1)(0)(3)(2)(0)));
  jamp_nc_relative_power[4].push_back(-1); 

  // Color flows of process Process: u~ b~ > z z g u~ b~ WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[5].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[5].push_back(-1); 
  // JAMP #1
  color_configs[5].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[5].push_back(0); 
  // JAMP #2
  color_configs[5].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[5].push_back(0); 
  // JAMP #3
  color_configs[5].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[5].push_back(-1); 

  // Color flows of process Process: d~ b > z z g d~ b WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[6].push_back(vec_int(createvector<int>
      (0)(1)(1)(0)(0)(0)(0)(0)(3)(2)(0)(3)(2)(0)));
  jamp_nc_relative_power[6].push_back(0); 
  // JAMP #1
  color_configs[6].push_back(vec_int(createvector<int>
      (0)(1)(3)(0)(0)(0)(0)(0)(3)(2)(0)(1)(2)(0)));
  jamp_nc_relative_power[6].push_back(-1); 
  // JAMP #2
  color_configs[6].push_back(vec_int(createvector<int>
      (0)(1)(3)(0)(0)(0)(0)(0)(3)(1)(0)(2)(2)(0)));
  jamp_nc_relative_power[6].push_back(0); 
  // JAMP #3
  color_configs[6].push_back(vec_int(createvector<int>
      (0)(1)(2)(0)(0)(0)(0)(0)(3)(1)(0)(3)(2)(0)));
  jamp_nc_relative_power[6].push_back(-1); 

  // Color flows of process Process: d~ b~ > z z g d~ b~ WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[7].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[7].push_back(-1); 
  // JAMP #1
  color_configs[7].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[7].push_back(0); 
  // JAMP #2
  color_configs[7].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[7].push_back(0); 
  // JAMP #3
  color_configs[7].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[7].push_back(-1); 
}

//--------------------------------------------------------------------------
// Destructor.
PY8MEs_R12_P41_sm_qb_zzgqb::~PY8MEs_R12_P41_sm_qb_zzgqb() 
{
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    delete[] p[i]; 
    p[i] = NULL; 
  }
}

//--------------------------------------------------------------------------
// Invert the permutation mapping
vector<int> PY8MEs_R12_P41_sm_qb_zzgqb::invert_mapping(vector<int> mapping) 
{
  vector<int> inverted_mapping; 
  for (unsigned int i = 0; i < mapping.size(); i++ )
  {
    for (unsigned int j = 0; j < mapping.size(); j++ )
    {
      if (mapping[j] == ((int)i))
      {
        inverted_mapping.push_back(j); 
        break; 
      }
    }
  }
  return inverted_mapping; 
}

//--------------------------------------------------------------------------
// Return the list of possible helicity configurations
vector < vec_int > PY8MEs_R12_P41_sm_qb_zzgqb::getHelicityConfigs(vector<int>
    permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(ncomb, vector<int> (nexternal, 0)); 
  for (unsigned int ihel = 0; ihel < ncomb; ihel++ )
  {
    for(unsigned int j = 0; j < nexternal; j++ )
    {
      res[ihel][chosenPerm[j]] = helicities[ihel][j]; 
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Return the list of possible color configurations
vector < vec_int > PY8MEs_R12_P41_sm_qb_zzgqb::getColorConfigs(int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(color_configs[chosenProcID].size(), vector<int>
      (nexternal * 2, 0));
  for (unsigned int icol = 0; icol < color_configs[chosenProcID].size(); icol++
      )
  {
    for(unsigned int j = 0; j < (2 * nexternal); j++ )
    {
      res[icol][chosenPerm[j/2] * 2 + j%2] =
          color_configs[chosenProcID][icol][j];
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Get JAMP relative N_c power
int PY8MEs_R12_P41_sm_qb_zzgqb::getColorFlowRelativeNCPower(int color_flow_ID,
    int specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return jamp_nc_relative_power[chosenProcID][color_flow_ID]; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity ID -> Helicity Config
vector<int> PY8MEs_R12_P41_sm_qb_zzgqb::getHelicityConfigForID(int hel_ID,
    vector<int> permutation)
{
  if (hel_ID < 0 || hel_ID >= ncomb)
  {
    cerr <<  "Error in function 'getHelicityConfigForID' of class" << 
    " 'PY8MEs_R12_P41_sm_qb_zzgqb': Specified helicity ID '" << 
    hel_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R12_P41_sm_qb_zzgqb_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(nexternal, 0); 
  for (unsigned int j = 0; j < nexternal; j++ )
  {
    res[chosenPerm[j]] = helicities[hel_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity Config -> Helicity ID
int PY8MEs_R12_P41_sm_qb_zzgqb::getHelicityIDForConfig(vector<int> hel_config,
    vector<int> permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  int user_ihel = -1; 
  if (hel_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < ncomb; i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < nexternal; j++ )
      {
        if (helicities[i][chosenPerm[j]] != hel_config[j])
        {
          found = false; 
          break; 
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_ihel = i; 
        break; 
      }
    }
    if (user_ihel == -1)
    {
      cerr <<  "Error in function 'getHelicityIDForConfig' of class" << 
      " 'PY8MEs_R12_P41_sm_qb_zzgqb': Specified helicity" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R12_P41_sm_qb_zzgqb_exception; 
    }
  }
  return user_ihel; 
}


//--------------------------------------------------------------------------
// Implements the map Color ID -> Color Config
vector<int> PY8MEs_R12_P41_sm_qb_zzgqb::getColorConfigForID(int color_ID, int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < 0 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getColorConfigForID' of class" << 
    " 'PY8MEs_R12_P41_sm_qb_zzgqb': Specified color ID '" << 
    color_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R12_P41_sm_qb_zzgqb_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(color_configs[chosenProcID][color_ID].size(), 0); 
  for (unsigned int j = 0; j < (2 * nexternal); j++ )
  {
    res[chosenPerm[j/2] * 2 + j%2] = color_configs[chosenProcID][color_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Color Config -> Color ID
int PY8MEs_R12_P41_sm_qb_zzgqb::getColorIDForConfig(vector<int> color_config,
    int specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = -1; 
  if (color_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < color_configs[chosenProcID].size(); i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < (nexternal * 2); j++ )
      {

        // If colorless then make sure it matches
        // The little arithmetics in the color index is just
        // the permutation applies on the particle list which is
        // twice smaller since each particle can have two color indices.
        if (color_config[j] == 0)
        {
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] != 0)
          {
            found = false; 
            break; 
          }
          // Otherwise check that the color linked position matches
        }
        else
        {
          int color_linked_pos = -1; 
          // Find the other end of the line in the user color config
          for (unsigned int k = 0; k < (nexternal * 2); k++ )
          {
            if (k == j)
              continue; 
            if (color_config[j] == color_config[k])
            {
              color_linked_pos = k; 
              break; 
            }
          }
          if (color_linked_pos == -1)
          {
            cerr <<  "Error in function 'getColorIDForConfig' of class" << 
            " 'PY8MEs_R12_P41_sm_qb_zzgqb': A color line could " << 
            " not be closed." << endl; 
            throw PY8MEs_R12_P41_sm_qb_zzgqb_exception; 
          }
          // Now check whether the color line matches
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] !=
              color_configs[chosenProcID][i][chosenPerm[color_linked_pos/2] * 2
              + color_linked_pos%2])
          {
            found = false; 
            break; 
          }
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_icol = i; 
        break; 
      }
    }

    if (user_icol == -1)
    {
      cerr <<  "Error in function 'getColorIDForConfig' of class" << 
      " 'PY8MEs_R12_P41_sm_qb_zzgqb': Specified color" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R12_P41_sm_qb_zzgqb_exception; 
    }
  }
  return user_icol; 
}

//--------------------------------------------------------------------------
// Returns all result previously computed in SigmaKin
vector < vec_double > PY8MEs_R12_P41_sm_qb_zzgqb::getAllResults(int
    specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return all_results[chosenProcID]; 
}

//--------------------------------------------------------------------------
// Returns a result previously computed in SigmaKin for a specific helicity
// and color ID. -1 means avg and summed over that characteristic.
double PY8MEs_R12_P41_sm_qb_zzgqb::getResult(int helicity_ID, int color_ID, int
    specify_proc_ID)
{
  if (helicity_ID < - 1 || helicity_ID >= ncomb)
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R12_P41_sm_qb_zzgqb': Specified helicity ID '" << 
    helicity_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R12_P41_sm_qb_zzgqb_exception; 
  }
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < - 1 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R12_P41_sm_qb_zzgqb': Specified color ID '" << 
    color_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R12_P41_sm_qb_zzgqb_exception; 
  }
  return all_results[chosenProcID][helicity_ID + 1][color_ID + 1]; 
}

//--------------------------------------------------------------------------
// Check for the availability of the requested process and if available,
// If available, this returns the corresponding permutation and Proc_ID to use.
// If not available, this returns a negative Proc_ID.
pair < vector<int> , int > PY8MEs_R12_P41_sm_qb_zzgqb::static_getPY8ME(vector<int> initial_pdgs, vector<int> final_pdgs, set<int> schannels) 
{

  // Not available return value
  pair < vector<int> , int > NA(vector<int> (), -1); 

  // Check if s-channel requirements match
  if (nreq_s_channels > 0)
  {
    if (schannels != s_channel_proc)
      return NA; 
  }
  else
  {
    if (schannels.size() != 0)
      return NA; 
  }

  // Check number of final state particles
  if (final_pdgs.size() != (nexternal - ninitial))
    return NA; 

  // Check number of initial state particles
  if (initial_pdgs.size() != ninitial)
    return NA; 

  // List of processes available in this class
  const int nprocs = 32; 
  const int proc_IDS[nprocs] = {0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7,
      0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7};
  const int in_pdgs[nprocs][ninitial] = {{2, 5}, {4, 5}, {2, -5}, {4, -5}, {1,
      5}, {3, 5}, {1, -5}, {3, -5}, {-2, 5}, {-4, 5}, {-2, -5}, {-4, -5}, {-1,
      5}, {-3, 5}, {-1, -5}, {-3, -5}, {5, 2}, {5, 4}, {-5, 2}, {-5, 4}, {5,
      1}, {5, 3}, {-5, 1}, {-5, 3}, {5, -2}, {5, -4}, {-5, -2}, {-5, -4}, {5,
      -1}, {5, -3}, {-5, -1}, {-5, -3}};
  const int out_pdgs[nprocs][nexternal - ninitial] = {{23, 23, 21, 2, 5}, {23,
      23, 21, 4, 5}, {23, 23, 21, 2, -5}, {23, 23, 21, 4, -5}, {23, 23, 21, 1,
      5}, {23, 23, 21, 3, 5}, {23, 23, 21, 1, -5}, {23, 23, 21, 3, -5}, {23,
      23, 21, -2, 5}, {23, 23, 21, -4, 5}, {23, 23, 21, -2, -5}, {23, 23, 21,
      -4, -5}, {23, 23, 21, -1, 5}, {23, 23, 21, -3, 5}, {23, 23, 21, -1, -5},
      {23, 23, 21, -3, -5}, {23, 23, 21, 2, 5}, {23, 23, 21, 4, 5}, {23, 23,
      21, 2, -5}, {23, 23, 21, 4, -5}, {23, 23, 21, 1, 5}, {23, 23, 21, 3, 5},
      {23, 23, 21, 1, -5}, {23, 23, 21, 3, -5}, {23, 23, 21, -2, 5}, {23, 23,
      21, -4, 5}, {23, 23, 21, -2, -5}, {23, 23, 21, -4, -5}, {23, 23, 21, -1,
      5}, {23, 23, 21, -3, 5}, {23, 23, 21, -1, -5}, {23, 23, 21, -3, -5}};

  bool in_pdgs_used[ninitial]; 
  bool out_pdgs_used[nexternal - ninitial]; 
  for(unsigned int i = 0; i < nprocs; i++ )
  {
    int permutations[nexternal]; 

    // Reinitialize initial state look-up variables
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      in_pdgs_used[j] = false; 
      permutations[j] = -1; 
    }
    // Look for initial state matches
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      for(unsigned int k = 0; k < ninitial; k++ )
      {
        // Make sure it has not been used already
        if (in_pdgs_used[k])
          continue; 
        if (initial_pdgs[k] == in_pdgs[i][j])
        {
          permutations[j] = k; 
          in_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[ninitial - 1] == -1)
      continue; 

    // Reinitialize final state look-up variables
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      out_pdgs_used[j] = false; 
      permutations[ninitial + j] = -1; 
    }
    // Look for final state matches
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      for(unsigned int k = 0; k < (nexternal - ninitial); k++ )
      {
        // Make sure it has not been used already
        if (out_pdgs_used[k])
          continue; 
        if (final_pdgs[k] == out_pdgs[i][j])
        {
          permutations[ninitial + j] = ninitial + k; 
          out_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[ninitial + j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[nexternal - 1] == -1)
      continue; 

    // Return process found
    return pair < vector<int> , int > (vector<int> (permutations, permutations
        + nexternal), proc_IDS[i]);
  }

  // No process found
  return NA; 
}

//--------------------------------------------------------------------------
// Set momenta
void PY8MEs_R12_P41_sm_qb_zzgqb::setMomenta(vector < vec_double >
    momenta_picked)
{
  if (momenta_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setMomenta' of class" << 
    " 'PY8MEs_R12_P41_sm_qb_zzgqb': Incorrect number of" << 
    " momenta specified." << endl; 
    throw PY8MEs_R12_P41_sm_qb_zzgqb_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    if (momenta_picked[i].size() != 4)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R12_P41_sm_qb_zzgqb': Incorrect number of" << 
      " momenta components specified." << endl; 
      throw PY8MEs_R12_P41_sm_qb_zzgqb_exception; 
    }
    if (momenta_picked[i][0] < 0.0)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R12_P41_sm_qb_zzgqb': A momentum was specified" << 
      " with negative energy. Check conventions." << endl; 
      throw PY8MEs_R12_P41_sm_qb_zzgqb_exception; 
    }
    for (unsigned int j = 0; j < 4; j++ )
    {
      p[i][j] = momenta_picked[i][j]; 
    }
  }
}

//--------------------------------------------------------------------------
// Set color configuration to use. An empty vector means sum over all.
void PY8MEs_R12_P41_sm_qb_zzgqb::setColors(vector<int> colors_picked)
{
  if (colors_picked.size() == 0)
  {
    user_colors = vector<int> (); 
    return; 
  }
  if (colors_picked.size() != (2 * nexternal))
  {
    cerr <<  "Error in function 'setColors' of class" << 
    " 'PY8MEs_R12_P41_sm_qb_zzgqb': Incorrect number" << 
    " of colors specified." << endl; 
    throw PY8MEs_R12_P41_sm_qb_zzgqb_exception; 
  }
  user_colors = vector<int> ((2 * nexternal), 0); 
  for(unsigned int i = 0; i < (2 * nexternal); i++ )
  {
    user_colors[i] = colors_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the helicity configuration to use. Am empty vector means sum over all.
void PY8MEs_R12_P41_sm_qb_zzgqb::setHelicities(vector<int> helicities_picked) 
{
  if (helicities_picked.size() != nexternal)
  {
    if (helicities_picked.size() == 0)
    {
      user_helicities = vector<int> (); 
      return; 
    }
    cerr <<  "Error in function 'setHelicities' of class" << 
    " 'PY8MEs_R12_P41_sm_qb_zzgqb': Incorrect number" << 
    " of helicities specified." << endl; 
    throw PY8MEs_R12_P41_sm_qb_zzgqb_exception; 
  }
  user_helicities = vector<int> (nexternal, 0); 
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    user_helicities[i] = helicities_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the permutation to use (will apply to momenta, colors and helicities)
void PY8MEs_R12_P41_sm_qb_zzgqb::setPermutation(vector<int> perm_picked) 
{
  if (perm_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setPermutations' of class" << 
    " 'PY8MEs_R12_P41_sm_qb_zzgqb': Incorrect number" << 
    " of permutations specified." << endl; 
    throw PY8MEs_R12_P41_sm_qb_zzgqb_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    perm[i] = perm_picked[i]; 
  }
}

// Set the proc_ID to use
void PY8MEs_R12_P41_sm_qb_zzgqb::setProcID(int procID_picked) 
{
  proc_ID = procID_picked; 
}

//--------------------------------------------------------------------------
// Initialize process.

void PY8MEs_R12_P41_sm_qb_zzgqb::initProc() 
{

  // Initialize flags
  include_symmetry_factors = true; 
  include_helicity_averaging_factors = true; 
  include_color_averaging_factors = true; 

  // Initialize vectors.
  perm = vector<int> (nexternal, 0); 
  user_colors = vector<int> (2 * nexternal, 0); 
  user_helicities = vector<int> (nexternal, 0); 
  p = vector < double * > (); 
  for (unsigned int i = 0; i < nexternal; i++ )
  {
    p.push_back(new double[4]); 
  }
  initColorConfigs(); 
  // Synchronize local variables dependent on the model with the active model.
  mME = vector<double> (nexternal, 0.); 
  syncProcModelParams(); 
  jamp2 = vector < vec_double > (8); 
  jamp2[0] = vector<double> (4, 0.); 
  jamp2[1] = vector<double> (4, 0.); 
  jamp2[2] = vector<double> (4, 0.); 
  jamp2[3] = vector<double> (4, 0.); 
  jamp2[4] = vector<double> (4, 0.); 
  jamp2[5] = vector<double> (4, 0.); 
  jamp2[6] = vector<double> (4, 0.); 
  jamp2[7] = vector<double> (4, 0.); 
  all_results = vector < vec_vec_double > (8); 
  // The first entry is always the color or helicity avg/summed matrix element.
  all_results[0] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[1] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[2] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[3] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[4] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[5] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[6] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[7] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
}

// Synchronize local variables of the process that depend on the model
// parameters
void PY8MEs_R12_P41_sm_qb_zzgqb::syncProcModelParams() 
{

  // Instantiate the model class and set parameters that stay fixed during run
  mME[0] = pars->ZERO; 
  mME[1] = pars->mdl_MB; 
  mME[2] = pars->mdl_MZ; 
  mME[3] = pars->mdl_MZ; 
  mME[4] = pars->ZERO; 
  mME[5] = pars->ZERO; 
  mME[6] = pars->mdl_MB; 
}

//--------------------------------------------------------------------------
// Setter allowing to force particular values for the external masses
void PY8MEs_R12_P41_sm_qb_zzgqb::setMasses(vec_double external_masses) 
{

  if (external_masses.size() != mME.size())
  {
    cerr <<  "Error in function 'setMasses' of class" << 
    " 'PY8MEs_R12_P41_sm_qb_zzgqb': Incorrect number of" << 
    " masses specified." << endl; 
    throw PY8MEs_R12_P41_sm_qb_zzgqb_exception; 
  }
  for (unsigned int j = 0; j < mME.size(); j++ )
  {
    mME[j] = external_masses[perm[j]]; 
  }
}

//--------------------------------------------------------------------------
// Getter accessing external masses with the correct ordering
vector<double> PY8MEs_R12_P41_sm_qb_zzgqb::getMasses() 
{

  vec_double external_masses; 
  vector<int> invertedPerm; 
  invertedPerm = invert_mapping(perm); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    external_masses.push_back(mME[invertedPerm[i]]); 
  }
  return external_masses; 

}


// Set all values of the external masses to float(-mode) where mode can be
// 0 : Mass taken from the model
// 1 : Mass taken from p_i^2 if not massless to begin with
// 2 : Mass always taken from p_i^2.
void PY8MEs_R12_P41_sm_qb_zzgqb::setExternalMassesMode(int mode) 
{
  if (mode != 0 && mode != 1 && mode != 2)
  {
    cerr <<  "Error in function 'setExternalMassesMode' of class" << 
    " 'PY8MEs_R12_P41_sm_qb_zzgqb': Incorrect mode selected :" << mode << 
    ". It must be either 0, 1 or 2" << endl; 
    throw PY8MEs_R12_P41_sm_qb_zzgqb_exception; 
  }
  if (mode == 0)
  {
    syncProcModelParams(); 
  }
  else if (mode == 1)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      if (mME[j] != pars->ZERO)
      {
        mME[j] = -1.0; 
      }
    }
  }
  else if (mode == 2)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      mME[j] = -1.0; 
    }
  }
}

//--------------------------------------------------------------------------
// Evaluate the squared matrix element.

double PY8MEs_R12_P41_sm_qb_zzgqb::sigmaKin() 
{
  // Set the parameters which change event by event
  pars->setDependentParameters(); 
  pars->setDependentCouplings(); 
  // Reset color flows
  for(int i = 0; i < 4; i++ )
    jamp2[0][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[1][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[2][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[3][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[4][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[5][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[6][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[7][i] = 0.; 

  // Save previous values of mME
  vector<double> saved_mME(mME.size(), 0.0); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (mME[i] < 0.0)
    {
      saved_mME[i] = mME[i]; 
      mME[i] = sqrt(abs(pow(p[perm[i]][0], 2) - 
      (pow(p[perm[i]][1], 2) + pow(p[perm[i]][2], 2) + pow(p[perm[i]][3],
          2))));
    }
  }

  // Local variables and constants
  const int max_tries = 10; 
  vector < vec_bool > goodhel(nprocesses, vec_bool(ncomb, false)); 
  vec_int ntry(nprocesses, 0); 
  double t = 0.; 
  double result = 0.; 

  if (ntry[proc_ID] <= max_tries)
    ntry[proc_ID] = ntry[proc_ID] + 1; 

  // Find which helicity configuration is asked for
  // -1 indicates one wants to sum over helicities
  int user_ihel = getHelicityIDForConfig(user_helicities); 

  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = getColorIDForConfig(user_colors); 

  // Reset the list of results that will be recomputed here
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      all_results[proc_ID][ihel + 1][icolor + 1] = 0.; 
    }
  }

  // Calculate the matrix element for all helicities
  // unless already detected as vanishing
  for(int ihel = 0; ihel < ncomb; ihel++ )
  {
    // Skip helicity if already detected as vanishing
    if ((ntry[proc_ID] >= max_tries) && !goodhel[proc_ID][ihel])
      continue; 

    // Also skip helicity if user asks for a specific one
    if ((ntry[proc_ID] >= max_tries) && user_ihel != -1 && user_ihel != ihel)
      continue; 

    calculate_wavefunctions(helicities[ihel]); 

    // Reset locally computed color flows
    for(int i = 0; i < 4; i++ )
      jamp2[0][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[1][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[2][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[3][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[4][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[5][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[6][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[7][i] = 0.; 

    if (proc_ID == 0)
      t = matrix_12_ub_zzgub(); 
    if (proc_ID == 1)
      t = matrix_12_ubx_zzgubx(); 
    if (proc_ID == 2)
      t = matrix_12_db_zzgdb(); 
    if (proc_ID == 3)
      t = matrix_12_dbx_zzgdbx(); 
    if (proc_ID == 4)
      t = matrix_12_uxb_zzguxb(); 
    if (proc_ID == 5)
      t = matrix_12_uxbx_zzguxbx(); 
    if (proc_ID == 6)
      t = matrix_12_dxb_zzgdxb(); 
    if (proc_ID == 7)
      t = matrix_12_dxbx_zzgdxbx(); 

    // Store which helicities give non-zero result
    if ((ntry[proc_ID] < max_tries) && t != 0. && !goodhel[proc_ID][ihel])
      goodhel[proc_ID][ihel] = true; 

    // Aggregate results
    if (user_ihel == -1 || user_ihel == ihel)
    {
      if (user_icol == -1)
      {
        result = result + t; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][0] += t; 
          for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
          {
            all_results[proc_ID][0][i + 1] += jamp2[proc_ID][i]; 
          }
        }
        all_results[proc_ID][ihel + 1][0] += t; 
        for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
        {
          all_results[proc_ID][ihel + 1][i + 1] += jamp2[proc_ID][i]; 
        }
      }
      else
      {
        result = result + jamp2[proc_ID][user_icol]; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][user_icol + 1] += jamp2[proc_ID][user_icol]; 
        }
        all_results[proc_ID][ihel + 1][user_icol + 1] +=
            jamp2[proc_ID][user_icol];
      }
    }

  }

  // Normalize results with the identical particle factor
  if (include_symmetry_factors)
  {
    result = result/denom_iden[proc_ID]; 
  }
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      if (include_symmetry_factors)
      {
        all_results[proc_ID][ihel + 1][icolor + 1] /= denom_iden[proc_ID]; 
      }
    }
  }


  // Normalize when when summing+averaging over helicity configurations
  if (user_ihel == -1 && include_helicity_averaging_factors)
  {
    result /= denom_hels[proc_ID]; 
    if (user_icol == -1)
    {
      all_results[proc_ID][0][0] /= denom_hels[proc_ID]; 
      for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
      {
        all_results[proc_ID][0][i + 1] /= denom_hels[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][0][user_icol + 1] /= denom_hels[proc_ID]; 
    }
  }

  // Normalize when summing+averaging over color configurations
  if (user_icol == -1 && include_color_averaging_factors)
  {
    result /= denom_colors[proc_ID]; 
    if (user_ihel == -1)
    {
      all_results[proc_ID][0][0] /= denom_colors[proc_ID]; 
      for (unsigned int i = 0; i < ncomb; i++ )
      {
        all_results[proc_ID][i + 1][0] /= denom_colors[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][user_ihel + 1][0] /= denom_colors[proc_ID]; 
    }
  }

  // Reinstate previous values of mME
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (saved_mME[i] < 0.0)
    {
      mME[i] = saved_mME[i]; 
    }
  }

  // Finally return it
  return result; 
}

//==========================================================================
// Private class member functions

//--------------------------------------------------------------------------
// Evaluate |M|^2 for each subprocess

void PY8MEs_R12_P41_sm_qb_zzgqb::calculate_wavefunctions(const int hel[])
{
  // Calculate wavefunctions for all processes
  // Calculate all wavefunctions
  ixxxxx(p[perm[0]], mME[0], hel[0], +1, w[0]); 
  ixxxxx(p[perm[1]], mME[1], hel[1], +1, w[1]); 
  vxxxxx(p[perm[2]], mME[2], hel[2], +1, w[2]); 
  vxxxxx(p[perm[3]], mME[3], hel[3], +1, w[3]); 
  vxxxxx(p[perm[4]], mME[4], hel[4], +1, w[4]); 
  oxxxxx(p[perm[5]], mME[5], hel[5], +1, w[5]); 
  oxxxxx(p[perm[6]], mME[6], hel[6], +1, w[6]); 
  FFV1_2(w[0], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[7]); 
  VVS1_3(w[2], w[3], pars->GC_81, pars->mdl_MH, pars->mdl_WH, w[8]); 
  FFV1P0_3(w[7], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[9]); 
  FFS4_1(w[6], w[8], pars->GC_83, pars->mdl_MB, pars->ZERO, w[10]); 
  FFS4_2(w[1], w[8], pars->GC_83, pars->mdl_MB, pars->ZERO, w[11]); 
  FFV2_5_1(w[5], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[12]);
  FFV2_3_1(w[6], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[13]);
  FFV1P0_3(w[7], w[12], pars->GC_11, pars->ZERO, pars->ZERO, w[14]); 
  FFV2_3_2(w[1], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[15]);
  FFV1P0_3(w[1], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[16]); 
  FFV2_5_2(w[7], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[17]);
  FFV1_2(w[7], w[16], pars->GC_11, pars->ZERO, pars->ZERO, w[18]); 
  FFV2_3_1(w[6], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[19]);
  FFV1P0_3(w[1], w[19], pars->GC_11, pars->ZERO, pars->ZERO, w[20]); 
  FFV2_3_1(w[19], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[21]);
  FFV2_5_1(w[5], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[22]);
  FFV1P0_3(w[7], w[22], pars->GC_11, pars->ZERO, pars->ZERO, w[23]); 
  FFV2_3_2(w[1], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[24]);
  FFV1P0_3(w[24], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[25]); 
  FFV2_3_2(w[24], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[26]);
  FFV2_5_2(w[7], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[27]);
  FFV1P0_3(w[1], w[13], pars->GC_11, pars->ZERO, pars->ZERO, w[28]); 
  FFV2_3_1(w[13], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[29]);
  FFV1P0_3(w[15], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[30]); 
  FFV2_3_2(w[15], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[31]);
  FFV1_1(w[5], w[16], pars->GC_11, pars->ZERO, pars->ZERO, w[32]); 
  FFV1_1(w[5], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[33]); 
  FFV2_5_2(w[0], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[34]);
  FFV1P0_3(w[34], w[33], pars->GC_11, pars->ZERO, pars->ZERO, w[35]); 
  FFV2_5_1(w[33], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[36]);
  FFV1_1(w[33], w[16], pars->GC_11, pars->ZERO, pars->ZERO, w[37]); 
  FFV2_5_2(w[0], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[38]);
  FFV1P0_3(w[38], w[33], pars->GC_11, pars->ZERO, pars->ZERO, w[39]); 
  FFV2_5_1(w[33], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[40]);
  FFV1P0_3(w[0], w[33], pars->GC_11, pars->ZERO, pars->ZERO, w[41]); 
  FFV1_2(w[0], w[16], pars->GC_11, pars->ZERO, pars->ZERO, w[42]); 
  FFV1_1(w[6], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[43]); 
  FFV2_3_1(w[43], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[44]);
  FFV1P0_3(w[34], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[45]); 
  FFV1P0_3(w[1], w[43], pars->GC_11, pars->ZERO, pars->ZERO, w[46]); 
  FFV2_5_2(w[34], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[47]);
  FFV1P0_3(w[15], w[43], pars->GC_11, pars->ZERO, pars->ZERO, w[48]); 
  FFV2_3_1(w[43], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[49]);
  FFV1P0_3(w[38], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[50]); 
  FFV2_5_2(w[38], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[51]);
  FFV1P0_3(w[24], w[43], pars->GC_11, pars->ZERO, pars->ZERO, w[52]); 
  FFV1P0_3(w[0], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[53]); 
  FFV1_2(w[1], w[53], pars->GC_11, pars->mdl_MB, pars->ZERO, w[54]); 
  FFV1_1(w[43], w[53], pars->GC_11, pars->mdl_MB, pars->ZERO, w[55]); 
  FFS4_1(w[43], w[8], pars->GC_83, pars->mdl_MB, pars->ZERO, w[56]); 
  FFV1P0_3(w[0], w[12], pars->GC_11, pars->ZERO, pars->ZERO, w[57]); 
  FFV2_5_1(w[12], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[58]);
  FFV1P0_3(w[0], w[22], pars->GC_11, pars->ZERO, pars->ZERO, w[59]); 
  FFV2_5_1(w[22], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[60]);
  FFV1_2(w[1], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[61]); 
  FFV2_3_2(w[61], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[62]);
  FFV1P0_3(w[61], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[63]); 
  FFV1P0_3(w[61], w[13], pars->GC_11, pars->ZERO, pars->ZERO, w[64]); 
  FFV2_3_2(w[61], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[65]);
  FFV1P0_3(w[61], w[19], pars->GC_11, pars->ZERO, pars->ZERO, w[66]); 
  FFV1_1(w[6], w[53], pars->GC_11, pars->mdl_MB, pars->ZERO, w[67]); 
  FFV1_2(w[61], w[53], pars->GC_11, pars->mdl_MB, pars->ZERO, w[68]); 
  FFS4_2(w[61], w[8], pars->GC_83, pars->mdl_MB, pars->ZERO, w[69]); 
  FFV1_2(w[34], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[70]); 
  FFV1_1(w[22], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[71]); 
  VVV1P0_1(w[4], w[16], pars->GC_10, pars->ZERO, pars->ZERO, w[72]); 
  FFV1_1(w[13], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[73]); 
  FFV1_2(w[15], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[74]); 
  FFV1_2(w[38], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[75]); 
  FFV1_1(w[12], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[76]); 
  FFV1_1(w[19], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[77]); 
  FFV1_2(w[24], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[78]); 
  VVV1P0_1(w[4], w[53], pars->GC_10, pars->ZERO, pars->ZERO, w[79]); 
  oxxxxx(p[perm[1]], mME[1], hel[1], -1, w[80]); 
  ixxxxx(p[perm[6]], mME[6], hel[6], -1, w[81]); 
  FFS4_1(w[80], w[8], pars->GC_83, pars->mdl_MB, pars->ZERO, w[82]); 
  FFS4_2(w[81], w[8], pars->GC_83, pars->mdl_MB, pars->ZERO, w[83]); 
  FFV2_3_1(w[80], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[84]);
  FFV2_3_2(w[81], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[85]);
  FFV1P0_3(w[81], w[80], pars->GC_11, pars->ZERO, pars->ZERO, w[86]); 
  FFV1_2(w[7], w[86], pars->GC_11, pars->ZERO, pars->ZERO, w[87]); 
  FFV2_3_1(w[80], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[88]);
  FFV1P0_3(w[81], w[88], pars->GC_11, pars->ZERO, pars->ZERO, w[89]); 
  FFV2_3_1(w[88], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[90]);
  FFV2_3_2(w[81], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[91]);
  FFV1P0_3(w[91], w[80], pars->GC_11, pars->ZERO, pars->ZERO, w[92]); 
  FFV2_3_2(w[91], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[93]);
  FFV1P0_3(w[81], w[84], pars->GC_11, pars->ZERO, pars->ZERO, w[94]); 
  FFV2_3_1(w[84], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[95]);
  FFV1P0_3(w[85], w[80], pars->GC_11, pars->ZERO, pars->ZERO, w[96]); 
  FFV2_3_2(w[85], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[97]);
  FFV1_1(w[5], w[86], pars->GC_11, pars->ZERO, pars->ZERO, w[98]); 
  FFV1_1(w[33], w[86], pars->GC_11, pars->ZERO, pars->ZERO, w[99]); 
  FFV1_2(w[0], w[86], pars->GC_11, pars->ZERO, pars->ZERO, w[100]); 
  FFV1_1(w[80], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[101]); 
  FFV2_3_1(w[101], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[102]);
  FFV1P0_3(w[81], w[101], pars->GC_11, pars->ZERO, pars->ZERO, w[103]); 
  FFV1P0_3(w[85], w[101], pars->GC_11, pars->ZERO, pars->ZERO, w[104]); 
  FFV2_3_1(w[101], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[105]);
  FFV1P0_3(w[91], w[101], pars->GC_11, pars->ZERO, pars->ZERO, w[106]); 
  FFV1_2(w[81], w[53], pars->GC_11, pars->mdl_MB, pars->ZERO, w[107]); 
  FFV1_1(w[101], w[53], pars->GC_11, pars->mdl_MB, pars->ZERO, w[108]); 
  FFS4_1(w[101], w[8], pars->GC_83, pars->mdl_MB, pars->ZERO, w[109]); 
  FFV1_2(w[81], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[110]); 
  FFV2_3_2(w[110], w[3], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[111]);
  FFV1P0_3(w[110], w[80], pars->GC_11, pars->ZERO, pars->ZERO, w[112]); 
  FFV1P0_3(w[110], w[84], pars->GC_11, pars->ZERO, pars->ZERO, w[113]); 
  FFV2_3_2(w[110], w[2], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[114]);
  FFV1P0_3(w[110], w[88], pars->GC_11, pars->ZERO, pars->ZERO, w[115]); 
  FFV1_1(w[80], w[53], pars->GC_11, pars->mdl_MB, pars->ZERO, w[116]); 
  FFV1_2(w[110], w[53], pars->GC_11, pars->mdl_MB, pars->ZERO, w[117]); 
  FFS4_2(w[110], w[8], pars->GC_83, pars->mdl_MB, pars->ZERO, w[118]); 
  VVV1P0_1(w[4], w[86], pars->GC_10, pars->ZERO, pars->ZERO, w[119]); 
  FFV1_1(w[84], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[120]); 
  FFV1_2(w[85], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[121]); 
  FFV1_1(w[88], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[122]); 
  FFV1_2(w[91], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[123]); 
  FFV2_3_1(w[5], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[124]);
  FFV1P0_3(w[7], w[124], pars->GC_11, pars->ZERO, pars->ZERO, w[125]); 
  FFV2_3_2(w[7], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[126]);
  FFV2_3_1(w[5], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[127]);
  FFV1P0_3(w[7], w[127], pars->GC_11, pars->ZERO, pars->ZERO, w[128]); 
  FFV2_3_2(w[7], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[129]);
  FFV2_3_2(w[0], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[130]);
  FFV1P0_3(w[130], w[33], pars->GC_11, pars->ZERO, pars->ZERO, w[131]); 
  FFV2_3_1(w[33], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[132]);
  FFV2_3_2(w[0], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[133]);
  FFV1P0_3(w[133], w[33], pars->GC_11, pars->ZERO, pars->ZERO, w[134]); 
  FFV2_3_1(w[33], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[135]);
  FFV1P0_3(w[130], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[136]); 
  FFV2_3_2(w[130], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[137]);
  FFV1P0_3(w[133], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[138]); 
  FFV2_3_2(w[133], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[139]);
  FFV1P0_3(w[0], w[124], pars->GC_11, pars->ZERO, pars->ZERO, w[140]); 
  FFV2_3_1(w[124], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[141]);
  FFV1P0_3(w[0], w[127], pars->GC_11, pars->ZERO, pars->ZERO, w[142]); 
  FFV2_3_1(w[127], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[143]);
  FFV1_2(w[130], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[144]); 
  FFV1_1(w[127], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[145]); 
  FFV1_2(w[133], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[146]); 
  FFV1_1(w[124], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[147]); 
  oxxxxx(p[perm[0]], mME[0], hel[0], -1, w[148]); 
  ixxxxx(p[perm[5]], mME[5], hel[5], -1, w[149]); 
  FFV1_2(w[149], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[150]); 
  FFV1P0_3(w[150], w[148], pars->GC_11, pars->ZERO, pars->ZERO, w[151]); 
  FFV2_5_1(w[148], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[152]);
  FFV1P0_3(w[150], w[152], pars->GC_11, pars->ZERO, pars->ZERO, w[153]); 
  FFV2_5_2(w[150], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[154]);
  FFV1_2(w[150], w[16], pars->GC_11, pars->ZERO, pars->ZERO, w[155]); 
  FFV2_5_1(w[148], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[156]);
  FFV1P0_3(w[150], w[156], pars->GC_11, pars->ZERO, pars->ZERO, w[157]); 
  FFV2_5_2(w[150], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[158]);
  FFV1_1(w[148], w[16], pars->GC_11, pars->ZERO, pars->ZERO, w[159]); 
  FFV1_1(w[148], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[160]); 
  FFV2_5_2(w[149], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[161]);
  FFV1P0_3(w[161], w[160], pars->GC_11, pars->ZERO, pars->ZERO, w[162]); 
  FFV2_5_1(w[160], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[163]);
  FFV1_1(w[160], w[16], pars->GC_11, pars->ZERO, pars->ZERO, w[164]); 
  FFV2_5_2(w[149], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[165]);
  FFV1P0_3(w[165], w[160], pars->GC_11, pars->ZERO, pars->ZERO, w[166]); 
  FFV2_5_1(w[160], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[167]);
  FFV1P0_3(w[149], w[160], pars->GC_11, pars->ZERO, pars->ZERO, w[168]); 
  FFV1_2(w[149], w[16], pars->GC_11, pars->ZERO, pars->ZERO, w[169]); 
  FFV1P0_3(w[161], w[148], pars->GC_11, pars->ZERO, pars->ZERO, w[170]); 
  FFV2_5_2(w[161], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[171]);
  FFV1P0_3(w[165], w[148], pars->GC_11, pars->ZERO, pars->ZERO, w[172]); 
  FFV2_5_2(w[165], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[173]);
  FFV1P0_3(w[149], w[148], pars->GC_11, pars->ZERO, pars->ZERO, w[174]); 
  FFV1_2(w[1], w[174], pars->GC_11, pars->mdl_MB, pars->ZERO, w[175]); 
  FFV1_1(w[43], w[174], pars->GC_11, pars->mdl_MB, pars->ZERO, w[176]); 
  FFV1P0_3(w[149], w[152], pars->GC_11, pars->ZERO, pars->ZERO, w[177]); 
  FFV2_5_1(w[152], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[178]);
  FFV1P0_3(w[149], w[156], pars->GC_11, pars->ZERO, pars->ZERO, w[179]); 
  FFV2_5_1(w[156], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[180]);
  FFV1_1(w[6], w[174], pars->GC_11, pars->mdl_MB, pars->ZERO, w[181]); 
  FFV1_2(w[61], w[174], pars->GC_11, pars->mdl_MB, pars->ZERO, w[182]); 
  FFV1_2(w[161], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[183]); 
  FFV1_1(w[156], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[184]); 
  FFV1_2(w[165], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[185]); 
  FFV1_1(w[152], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[186]); 
  VVV1P0_1(w[4], w[174], pars->GC_10, pars->ZERO, pars->ZERO, w[187]); 
  FFV1_2(w[150], w[86], pars->GC_11, pars->ZERO, pars->ZERO, w[188]); 
  FFV1_1(w[148], w[86], pars->GC_11, pars->ZERO, pars->ZERO, w[189]); 
  FFV1_1(w[160], w[86], pars->GC_11, pars->ZERO, pars->ZERO, w[190]); 
  FFV1_2(w[149], w[86], pars->GC_11, pars->ZERO, pars->ZERO, w[191]); 
  FFV1_2(w[81], w[174], pars->GC_11, pars->mdl_MB, pars->ZERO, w[192]); 
  FFV1_1(w[101], w[174], pars->GC_11, pars->mdl_MB, pars->ZERO, w[193]); 
  FFV1_1(w[80], w[174], pars->GC_11, pars->mdl_MB, pars->ZERO, w[194]); 
  FFV1_2(w[110], w[174], pars->GC_11, pars->mdl_MB, pars->ZERO, w[195]); 
  FFV2_3_1(w[148], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[196]);
  FFV1P0_3(w[150], w[196], pars->GC_11, pars->ZERO, pars->ZERO, w[197]); 
  FFV2_3_2(w[150], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[198]);
  FFV2_3_1(w[148], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[199]);
  FFV1P0_3(w[150], w[199], pars->GC_11, pars->ZERO, pars->ZERO, w[200]); 
  FFV2_3_2(w[150], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[201]);
  FFV2_3_2(w[149], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[202]);
  FFV1P0_3(w[202], w[160], pars->GC_11, pars->ZERO, pars->ZERO, w[203]); 
  FFV2_3_1(w[160], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[204]);
  FFV2_3_2(w[149], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[205]);
  FFV1P0_3(w[205], w[160], pars->GC_11, pars->ZERO, pars->ZERO, w[206]); 
  FFV2_3_1(w[160], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[207]);
  FFV1P0_3(w[202], w[148], pars->GC_11, pars->ZERO, pars->ZERO, w[208]); 
  FFV2_3_2(w[202], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[209]);
  FFV1P0_3(w[205], w[148], pars->GC_11, pars->ZERO, pars->ZERO, w[210]); 
  FFV2_3_2(w[205], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[211]);
  FFV1P0_3(w[149], w[196], pars->GC_11, pars->ZERO, pars->ZERO, w[212]); 
  FFV2_3_1(w[196], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[213]);
  FFV1P0_3(w[149], w[199], pars->GC_11, pars->ZERO, pars->ZERO, w[214]); 
  FFV2_3_1(w[199], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[215]);
  FFV1_2(w[202], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[216]); 
  FFV1_1(w[199], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[217]); 
  FFV1_2(w[205], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[218]); 
  FFV1_1(w[196], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[219]); 

  // Calculate all amplitudes
  // Amplitude(s) for diagram number 0
  FFV1_0(w[1], w[10], w[9], pars->GC_11, amp[0]); 
  FFV1_0(w[11], w[6], w[9], pars->GC_11, amp[1]); 
  FFV1_0(w[1], w[13], w[14], pars->GC_11, amp[2]); 
  FFV1_0(w[15], w[6], w[14], pars->GC_11, amp[3]); 
  FFV1_0(w[17], w[12], w[16], pars->GC_11, amp[4]); 
  FFV2_5_0(w[18], w[12], w[3], pars->GC_51, pars->GC_58, amp[5]); 
  FFV1_0(w[17], w[5], w[20], pars->GC_11, amp[6]); 
  FFV1_0(w[1], w[21], w[9], pars->GC_11, amp[7]); 
  FFV1_0(w[1], w[19], w[23], pars->GC_11, amp[8]); 
  FFV1_0(w[15], w[19], w[9], pars->GC_11, amp[9]); 
  FFV1_0(w[17], w[5], w[25], pars->GC_11, amp[10]); 
  FFV1_0(w[26], w[6], w[9], pars->GC_11, amp[11]); 
  FFV1_0(w[24], w[6], w[23], pars->GC_11, amp[12]); 
  FFV1_0(w[24], w[13], w[9], pars->GC_11, amp[13]); 
  FFV1_0(w[27], w[22], w[16], pars->GC_11, amp[14]); 
  FFV2_5_0(w[18], w[22], w[2], pars->GC_51, pars->GC_58, amp[15]); 
  FFV1_0(w[27], w[5], w[28], pars->GC_11, amp[16]); 
  FFV1_0(w[1], w[29], w[9], pars->GC_11, amp[17]); 
  FFV1_0(w[27], w[5], w[30], pars->GC_11, amp[18]); 
  FFV1_0(w[31], w[6], w[9], pars->GC_11, amp[19]); 
  FFV2_5_0(w[27], w[32], w[3], pars->GC_51, pars->GC_58, amp[20]); 
  FFV2_5_0(w[17], w[32], w[2], pars->GC_51, pars->GC_58, amp[21]); 
  FFV1_0(w[1], w[13], w[35], pars->GC_11, amp[22]); 
  FFV1_0(w[15], w[6], w[35], pars->GC_11, amp[23]); 
  FFV1_0(w[34], w[36], w[16], pars->GC_11, amp[24]); 
  FFV2_5_0(w[34], w[37], w[3], pars->GC_51, pars->GC_58, amp[25]); 
  FFV1_0(w[1], w[19], w[39], pars->GC_11, amp[26]); 
  FFV1_0(w[24], w[6], w[39], pars->GC_11, amp[27]); 
  FFV1_0(w[38], w[40], w[16], pars->GC_11, amp[28]); 
  FFV2_5_0(w[38], w[37], w[2], pars->GC_51, pars->GC_58, amp[29]); 
  FFV1_0(w[1], w[10], w[41], pars->GC_11, amp[30]); 
  FFV1_0(w[11], w[6], w[41], pars->GC_11, amp[31]); 
  FFV1_0(w[1], w[21], w[41], pars->GC_11, amp[32]); 
  FFV1_0(w[0], w[36], w[20], pars->GC_11, amp[33]); 
  FFV1_0(w[15], w[19], w[41], pars->GC_11, amp[34]); 
  FFV1_0(w[26], w[6], w[41], pars->GC_11, amp[35]); 
  FFV1_0(w[0], w[36], w[25], pars->GC_11, amp[36]); 
  FFV1_0(w[24], w[13], w[41], pars->GC_11, amp[37]); 
  FFV1_0(w[1], w[29], w[41], pars->GC_11, amp[38]); 
  FFV1_0(w[0], w[40], w[28], pars->GC_11, amp[39]); 
  FFV1_0(w[31], w[6], w[41], pars->GC_11, amp[40]); 
  FFV1_0(w[0], w[40], w[30], pars->GC_11, amp[41]); 
  FFV2_5_0(w[42], w[40], w[3], pars->GC_51, pars->GC_58, amp[42]); 
  FFV2_5_0(w[42], w[36], w[2], pars->GC_51, pars->GC_58, amp[43]); 
  FFV1_0(w[1], w[44], w[45], pars->GC_11, amp[44]); 
  FFV1_0(w[47], w[5], w[46], pars->GC_11, amp[45]); 
  FFV1_0(w[34], w[22], w[46], pars->GC_11, amp[46]); 
  FFV1_0(w[34], w[5], w[48], pars->GC_11, amp[47]); 
  FFV1_0(w[1], w[49], w[50], pars->GC_11, amp[48]); 
  FFV1_0(w[51], w[5], w[46], pars->GC_11, amp[49]); 
  FFV1_0(w[38], w[12], w[46], pars->GC_11, amp[50]); 
  FFV1_0(w[38], w[5], w[52], pars->GC_11, amp[51]); 
  FFV2_3_0(w[54], w[49], w[3], pars->GC_50, pars->GC_58, amp[52]); 
  FFV2_3_0(w[54], w[44], w[2], pars->GC_50, pars->GC_58, amp[53]); 
  FFS4_0(w[1], w[55], w[8], pars->GC_83, amp[54]); 
  FFV1_0(w[1], w[56], w[53], pars->GC_11, amp[55]); 
  FFV2_3_0(w[24], w[55], w[3], pars->GC_50, pars->GC_58, amp[56]); 
  FFV1_0(w[24], w[44], w[53], pars->GC_11, amp[57]); 
  FFV2_3_0(w[15], w[55], w[2], pars->GC_50, pars->GC_58, amp[58]); 
  FFV1_0(w[15], w[49], w[53], pars->GC_11, amp[59]); 
  FFV1_0(w[1], w[44], w[57], pars->GC_11, amp[60]); 
  FFV1_0(w[0], w[58], w[46], pars->GC_11, amp[61]); 
  FFV1_0(w[0], w[12], w[48], pars->GC_11, amp[62]); 
  FFV1_0(w[0], w[22], w[52], pars->GC_11, amp[63]); 
  FFV1_0(w[1], w[49], w[59], pars->GC_11, amp[64]); 
  FFV1_0(w[0], w[60], w[46], pars->GC_11, amp[65]); 
  FFV1_0(w[62], w[6], w[45], pars->GC_11, amp[66]); 
  FFV1_0(w[47], w[5], w[63], pars->GC_11, amp[67]); 
  FFV1_0(w[34], w[22], w[63], pars->GC_11, amp[68]); 
  FFV1_0(w[34], w[5], w[64], pars->GC_11, amp[69]); 
  FFV1_0(w[65], w[6], w[50], pars->GC_11, amp[70]); 
  FFV1_0(w[51], w[5], w[63], pars->GC_11, amp[71]); 
  FFV1_0(w[38], w[12], w[63], pars->GC_11, amp[72]); 
  FFV1_0(w[38], w[5], w[66], pars->GC_11, amp[73]); 
  FFV2_3_0(w[65], w[67], w[3], pars->GC_50, pars->GC_58, amp[74]); 
  FFV2_3_0(w[62], w[67], w[2], pars->GC_50, pars->GC_58, amp[75]); 
  FFS4_0(w[68], w[6], w[8], pars->GC_83, amp[76]); 
  FFV1_0(w[69], w[6], w[53], pars->GC_11, amp[77]); 
  FFV2_3_0(w[68], w[19], w[3], pars->GC_50, pars->GC_58, amp[78]); 
  FFV1_0(w[62], w[19], w[53], pars->GC_11, amp[79]); 
  FFV2_3_0(w[68], w[13], w[2], pars->GC_50, pars->GC_58, amp[80]); 
  FFV1_0(w[65], w[13], w[53], pars->GC_11, amp[81]); 
  FFV1_0(w[62], w[6], w[57], pars->GC_11, amp[82]); 
  FFV1_0(w[0], w[58], w[63], pars->GC_11, amp[83]); 
  FFV1_0(w[0], w[12], w[64], pars->GC_11, amp[84]); 
  FFV1_0(w[0], w[22], w[66], pars->GC_11, amp[85]); 
  FFV1_0(w[65], w[6], w[59], pars->GC_11, amp[86]); 
  FFV1_0(w[0], w[60], w[63], pars->GC_11, amp[87]); 
  FFV1_0(w[70], w[22], w[16], pars->GC_11, amp[88]); 
  FFV1_0(w[34], w[71], w[16], pars->GC_11, amp[89]); 
  FFV1_0(w[34], w[22], w[72], pars->GC_11, amp[90]); 
  FFV1_0(w[70], w[5], w[28], pars->GC_11, amp[91]); 
  FFV1_0(w[1], w[73], w[45], pars->GC_11, amp[92]); 
  VVV1_0(w[4], w[45], w[28], pars->GC_10, amp[93]); 
  FFV1_0(w[70], w[5], w[30], pars->GC_11, amp[94]); 
  FFV1_0(w[74], w[6], w[45], pars->GC_11, amp[95]); 
  VVV1_0(w[4], w[45], w[30], pars->GC_10, amp[96]); 
  FFV2_5_0(w[70], w[32], w[3], pars->GC_51, pars->GC_58, amp[97]); 
  FFV1_0(w[47], w[5], w[72], pars->GC_11, amp[98]); 
  FFV1_0(w[47], w[32], w[4], pars->GC_11, amp[99]); 
  FFV1_0(w[75], w[12], w[16], pars->GC_11, amp[100]); 
  FFV1_0(w[38], w[76], w[16], pars->GC_11, amp[101]); 
  FFV1_0(w[38], w[12], w[72], pars->GC_11, amp[102]); 
  FFV1_0(w[75], w[5], w[20], pars->GC_11, amp[103]); 
  FFV1_0(w[1], w[77], w[50], pars->GC_11, amp[104]); 
  VVV1_0(w[4], w[50], w[20], pars->GC_10, amp[105]); 
  FFV1_0(w[75], w[5], w[25], pars->GC_11, amp[106]); 
  FFV1_0(w[78], w[6], w[50], pars->GC_11, amp[107]); 
  VVV1_0(w[4], w[50], w[25], pars->GC_10, amp[108]); 
  FFV2_5_0(w[75], w[32], w[2], pars->GC_51, pars->GC_58, amp[109]); 
  FFV1_0(w[51], w[5], w[72], pars->GC_11, amp[110]); 
  FFV1_0(w[51], w[32], w[4], pars->GC_11, amp[111]); 
  FFV1_0(w[1], w[10], w[79], pars->GC_11, amp[112]); 
  FFV1_0(w[11], w[6], w[79], pars->GC_11, amp[113]); 
  FFV1_0(w[11], w[67], w[4], pars->GC_11, amp[114]); 
  FFV1_0(w[54], w[10], w[4], pars->GC_11, amp[115]); 
  FFV1_0(w[1], w[21], w[79], pars->GC_11, amp[116]); 
  FFV2_3_0(w[54], w[77], w[3], pars->GC_50, pars->GC_58, amp[117]); 
  FFV1_0(w[54], w[21], w[4], pars->GC_11, amp[118]); 
  FFV1_0(w[15], w[19], w[79], pars->GC_11, amp[119]); 
  FFV1_0(w[15], w[77], w[53], pars->GC_11, amp[120]); 
  FFV1_0(w[74], w[19], w[53], pars->GC_11, amp[121]); 
  FFV1_0(w[26], w[6], w[79], pars->GC_11, amp[122]); 
  FFV2_3_0(w[78], w[67], w[3], pars->GC_50, pars->GC_58, amp[123]); 
  FFV1_0(w[26], w[67], w[4], pars->GC_11, amp[124]); 
  FFV1_0(w[24], w[13], w[79], pars->GC_11, amp[125]); 
  FFV1_0(w[78], w[13], w[53], pars->GC_11, amp[126]); 
  FFV1_0(w[24], w[73], w[53], pars->GC_11, amp[127]); 
  FFV1_0(w[1], w[29], w[79], pars->GC_11, amp[128]); 
  FFV2_3_0(w[54], w[73], w[2], pars->GC_50, pars->GC_58, amp[129]); 
  FFV1_0(w[54], w[29], w[4], pars->GC_11, amp[130]); 
  FFV1_0(w[31], w[6], w[79], pars->GC_11, amp[131]); 
  FFV2_3_0(w[74], w[67], w[2], pars->GC_50, pars->GC_58, amp[132]); 
  FFV1_0(w[31], w[67], w[4], pars->GC_11, amp[133]); 
  FFV1_0(w[0], w[76], w[28], pars->GC_11, amp[134]); 
  FFV1_0(w[1], w[73], w[57], pars->GC_11, amp[135]); 
  VVV1_0(w[4], w[57], w[28], pars->GC_10, amp[136]); 
  FFV1_0(w[0], w[76], w[30], pars->GC_11, amp[137]); 
  FFV1_0(w[74], w[6], w[57], pars->GC_11, amp[138]); 
  VVV1_0(w[4], w[57], w[30], pars->GC_10, amp[139]); 
  FFV2_5_0(w[42], w[76], w[3], pars->GC_51, pars->GC_58, amp[140]); 
  FFV1_0(w[0], w[58], w[72], pars->GC_11, amp[141]); 
  FFV1_0(w[42], w[58], w[4], pars->GC_11, amp[142]); 
  FFV1_0(w[1], w[77], w[59], pars->GC_11, amp[143]); 
  FFV1_0(w[0], w[71], w[20], pars->GC_11, amp[144]); 
  VVV1_0(w[4], w[59], w[20], pars->GC_10, amp[145]); 
  FFV1_0(w[78], w[6], w[59], pars->GC_11, amp[146]); 
  FFV1_0(w[0], w[71], w[25], pars->GC_11, amp[147]); 
  VVV1_0(w[4], w[59], w[25], pars->GC_10, amp[148]); 
  FFV2_5_0(w[42], w[71], w[2], pars->GC_51, pars->GC_58, amp[149]); 
  FFV1_0(w[0], w[60], w[72], pars->GC_11, amp[150]); 
  FFV1_0(w[42], w[60], w[4], pars->GC_11, amp[151]); 
  FFV1_0(w[81], w[82], w[9], pars->GC_11, amp[152]); 
  FFV1_0(w[83], w[80], w[9], pars->GC_11, amp[153]); 
  FFV1_0(w[81], w[84], w[14], pars->GC_11, amp[154]); 
  FFV1_0(w[85], w[80], w[14], pars->GC_11, amp[155]); 
  FFV1_0(w[17], w[12], w[86], pars->GC_11, amp[156]); 
  FFV2_5_0(w[87], w[12], w[3], pars->GC_51, pars->GC_58, amp[157]); 
  FFV1_0(w[17], w[5], w[89], pars->GC_11, amp[158]); 
  FFV1_0(w[81], w[90], w[9], pars->GC_11, amp[159]); 
  FFV1_0(w[81], w[88], w[23], pars->GC_11, amp[160]); 
  FFV1_0(w[85], w[88], w[9], pars->GC_11, amp[161]); 
  FFV1_0(w[17], w[5], w[92], pars->GC_11, amp[162]); 
  FFV1_0(w[93], w[80], w[9], pars->GC_11, amp[163]); 
  FFV1_0(w[91], w[80], w[23], pars->GC_11, amp[164]); 
  FFV1_0(w[91], w[84], w[9], pars->GC_11, amp[165]); 
  FFV1_0(w[27], w[22], w[86], pars->GC_11, amp[166]); 
  FFV2_5_0(w[87], w[22], w[2], pars->GC_51, pars->GC_58, amp[167]); 
  FFV1_0(w[27], w[5], w[94], pars->GC_11, amp[168]); 
  FFV1_0(w[81], w[95], w[9], pars->GC_11, amp[169]); 
  FFV1_0(w[27], w[5], w[96], pars->GC_11, amp[170]); 
  FFV1_0(w[97], w[80], w[9], pars->GC_11, amp[171]); 
  FFV2_5_0(w[27], w[98], w[3], pars->GC_51, pars->GC_58, amp[172]); 
  FFV2_5_0(w[17], w[98], w[2], pars->GC_51, pars->GC_58, amp[173]); 
  FFV1_0(w[81], w[84], w[35], pars->GC_11, amp[174]); 
  FFV1_0(w[85], w[80], w[35], pars->GC_11, amp[175]); 
  FFV1_0(w[34], w[36], w[86], pars->GC_11, amp[176]); 
  FFV2_5_0(w[34], w[99], w[3], pars->GC_51, pars->GC_58, amp[177]); 
  FFV1_0(w[81], w[88], w[39], pars->GC_11, amp[178]); 
  FFV1_0(w[91], w[80], w[39], pars->GC_11, amp[179]); 
  FFV1_0(w[38], w[40], w[86], pars->GC_11, amp[180]); 
  FFV2_5_0(w[38], w[99], w[2], pars->GC_51, pars->GC_58, amp[181]); 
  FFV1_0(w[81], w[82], w[41], pars->GC_11, amp[182]); 
  FFV1_0(w[83], w[80], w[41], pars->GC_11, amp[183]); 
  FFV1_0(w[81], w[90], w[41], pars->GC_11, amp[184]); 
  FFV1_0(w[0], w[36], w[89], pars->GC_11, amp[185]); 
  FFV1_0(w[85], w[88], w[41], pars->GC_11, amp[186]); 
  FFV1_0(w[93], w[80], w[41], pars->GC_11, amp[187]); 
  FFV1_0(w[0], w[36], w[92], pars->GC_11, amp[188]); 
  FFV1_0(w[91], w[84], w[41], pars->GC_11, amp[189]); 
  FFV1_0(w[81], w[95], w[41], pars->GC_11, amp[190]); 
  FFV1_0(w[0], w[40], w[94], pars->GC_11, amp[191]); 
  FFV1_0(w[97], w[80], w[41], pars->GC_11, amp[192]); 
  FFV1_0(w[0], w[40], w[96], pars->GC_11, amp[193]); 
  FFV2_5_0(w[100], w[40], w[3], pars->GC_51, pars->GC_58, amp[194]); 
  FFV2_5_0(w[100], w[36], w[2], pars->GC_51, pars->GC_58, amp[195]); 
  FFV1_0(w[81], w[102], w[45], pars->GC_11, amp[196]); 
  FFV1_0(w[47], w[5], w[103], pars->GC_11, amp[197]); 
  FFV1_0(w[34], w[22], w[103], pars->GC_11, amp[198]); 
  FFV1_0(w[34], w[5], w[104], pars->GC_11, amp[199]); 
  FFV1_0(w[81], w[105], w[50], pars->GC_11, amp[200]); 
  FFV1_0(w[51], w[5], w[103], pars->GC_11, amp[201]); 
  FFV1_0(w[38], w[12], w[103], pars->GC_11, amp[202]); 
  FFV1_0(w[38], w[5], w[106], pars->GC_11, amp[203]); 
  FFV2_3_0(w[107], w[105], w[3], pars->GC_50, pars->GC_58, amp[204]); 
  FFV2_3_0(w[107], w[102], w[2], pars->GC_50, pars->GC_58, amp[205]); 
  FFS4_0(w[81], w[108], w[8], pars->GC_83, amp[206]); 
  FFV1_0(w[81], w[109], w[53], pars->GC_11, amp[207]); 
  FFV2_3_0(w[91], w[108], w[3], pars->GC_50, pars->GC_58, amp[208]); 
  FFV1_0(w[91], w[102], w[53], pars->GC_11, amp[209]); 
  FFV2_3_0(w[85], w[108], w[2], pars->GC_50, pars->GC_58, amp[210]); 
  FFV1_0(w[85], w[105], w[53], pars->GC_11, amp[211]); 
  FFV1_0(w[81], w[102], w[57], pars->GC_11, amp[212]); 
  FFV1_0(w[0], w[58], w[103], pars->GC_11, amp[213]); 
  FFV1_0(w[0], w[12], w[104], pars->GC_11, amp[214]); 
  FFV1_0(w[0], w[22], w[106], pars->GC_11, amp[215]); 
  FFV1_0(w[81], w[105], w[59], pars->GC_11, amp[216]); 
  FFV1_0(w[0], w[60], w[103], pars->GC_11, amp[217]); 
  FFV1_0(w[111], w[80], w[45], pars->GC_11, amp[218]); 
  FFV1_0(w[47], w[5], w[112], pars->GC_11, amp[219]); 
  FFV1_0(w[34], w[22], w[112], pars->GC_11, amp[220]); 
  FFV1_0(w[34], w[5], w[113], pars->GC_11, amp[221]); 
  FFV1_0(w[114], w[80], w[50], pars->GC_11, amp[222]); 
  FFV1_0(w[51], w[5], w[112], pars->GC_11, amp[223]); 
  FFV1_0(w[38], w[12], w[112], pars->GC_11, amp[224]); 
  FFV1_0(w[38], w[5], w[115], pars->GC_11, amp[225]); 
  FFV2_3_0(w[114], w[116], w[3], pars->GC_50, pars->GC_58, amp[226]); 
  FFV2_3_0(w[111], w[116], w[2], pars->GC_50, pars->GC_58, amp[227]); 
  FFS4_0(w[117], w[80], w[8], pars->GC_83, amp[228]); 
  FFV1_0(w[118], w[80], w[53], pars->GC_11, amp[229]); 
  FFV2_3_0(w[117], w[88], w[3], pars->GC_50, pars->GC_58, amp[230]); 
  FFV1_0(w[111], w[88], w[53], pars->GC_11, amp[231]); 
  FFV2_3_0(w[117], w[84], w[2], pars->GC_50, pars->GC_58, amp[232]); 
  FFV1_0(w[114], w[84], w[53], pars->GC_11, amp[233]); 
  FFV1_0(w[111], w[80], w[57], pars->GC_11, amp[234]); 
  FFV1_0(w[0], w[58], w[112], pars->GC_11, amp[235]); 
  FFV1_0(w[0], w[12], w[113], pars->GC_11, amp[236]); 
  FFV1_0(w[0], w[22], w[115], pars->GC_11, amp[237]); 
  FFV1_0(w[114], w[80], w[59], pars->GC_11, amp[238]); 
  FFV1_0(w[0], w[60], w[112], pars->GC_11, amp[239]); 
  FFV1_0(w[70], w[22], w[86], pars->GC_11, amp[240]); 
  FFV1_0(w[34], w[71], w[86], pars->GC_11, amp[241]); 
  FFV1_0(w[34], w[22], w[119], pars->GC_11, amp[242]); 
  FFV1_0(w[70], w[5], w[94], pars->GC_11, amp[243]); 
  FFV1_0(w[81], w[120], w[45], pars->GC_11, amp[244]); 
  VVV1_0(w[4], w[45], w[94], pars->GC_10, amp[245]); 
  FFV1_0(w[70], w[5], w[96], pars->GC_11, amp[246]); 
  FFV1_0(w[121], w[80], w[45], pars->GC_11, amp[247]); 
  VVV1_0(w[4], w[45], w[96], pars->GC_10, amp[248]); 
  FFV2_5_0(w[70], w[98], w[3], pars->GC_51, pars->GC_58, amp[249]); 
  FFV1_0(w[47], w[5], w[119], pars->GC_11, amp[250]); 
  FFV1_0(w[47], w[98], w[4], pars->GC_11, amp[251]); 
  FFV1_0(w[75], w[12], w[86], pars->GC_11, amp[252]); 
  FFV1_0(w[38], w[76], w[86], pars->GC_11, amp[253]); 
  FFV1_0(w[38], w[12], w[119], pars->GC_11, amp[254]); 
  FFV1_0(w[75], w[5], w[89], pars->GC_11, amp[255]); 
  FFV1_0(w[81], w[122], w[50], pars->GC_11, amp[256]); 
  VVV1_0(w[4], w[50], w[89], pars->GC_10, amp[257]); 
  FFV1_0(w[75], w[5], w[92], pars->GC_11, amp[258]); 
  FFV1_0(w[123], w[80], w[50], pars->GC_11, amp[259]); 
  VVV1_0(w[4], w[50], w[92], pars->GC_10, amp[260]); 
  FFV2_5_0(w[75], w[98], w[2], pars->GC_51, pars->GC_58, amp[261]); 
  FFV1_0(w[51], w[5], w[119], pars->GC_11, amp[262]); 
  FFV1_0(w[51], w[98], w[4], pars->GC_11, amp[263]); 
  FFV1_0(w[81], w[82], w[79], pars->GC_11, amp[264]); 
  FFV1_0(w[83], w[80], w[79], pars->GC_11, amp[265]); 
  FFV1_0(w[83], w[116], w[4], pars->GC_11, amp[266]); 
  FFV1_0(w[107], w[82], w[4], pars->GC_11, amp[267]); 
  FFV1_0(w[81], w[90], w[79], pars->GC_11, amp[268]); 
  FFV2_3_0(w[107], w[122], w[3], pars->GC_50, pars->GC_58, amp[269]); 
  FFV1_0(w[107], w[90], w[4], pars->GC_11, amp[270]); 
  FFV1_0(w[85], w[88], w[79], pars->GC_11, amp[271]); 
  FFV1_0(w[85], w[122], w[53], pars->GC_11, amp[272]); 
  FFV1_0(w[121], w[88], w[53], pars->GC_11, amp[273]); 
  FFV1_0(w[93], w[80], w[79], pars->GC_11, amp[274]); 
  FFV2_3_0(w[123], w[116], w[3], pars->GC_50, pars->GC_58, amp[275]); 
  FFV1_0(w[93], w[116], w[4], pars->GC_11, amp[276]); 
  FFV1_0(w[91], w[84], w[79], pars->GC_11, amp[277]); 
  FFV1_0(w[123], w[84], w[53], pars->GC_11, amp[278]); 
  FFV1_0(w[91], w[120], w[53], pars->GC_11, amp[279]); 
  FFV1_0(w[81], w[95], w[79], pars->GC_11, amp[280]); 
  FFV2_3_0(w[107], w[120], w[2], pars->GC_50, pars->GC_58, amp[281]); 
  FFV1_0(w[107], w[95], w[4], pars->GC_11, amp[282]); 
  FFV1_0(w[97], w[80], w[79], pars->GC_11, amp[283]); 
  FFV2_3_0(w[121], w[116], w[2], pars->GC_50, pars->GC_58, amp[284]); 
  FFV1_0(w[97], w[116], w[4], pars->GC_11, amp[285]); 
  FFV1_0(w[0], w[76], w[94], pars->GC_11, amp[286]); 
  FFV1_0(w[81], w[120], w[57], pars->GC_11, amp[287]); 
  VVV1_0(w[4], w[57], w[94], pars->GC_10, amp[288]); 
  FFV1_0(w[0], w[76], w[96], pars->GC_11, amp[289]); 
  FFV1_0(w[121], w[80], w[57], pars->GC_11, amp[290]); 
  VVV1_0(w[4], w[57], w[96], pars->GC_10, amp[291]); 
  FFV2_5_0(w[100], w[76], w[3], pars->GC_51, pars->GC_58, amp[292]); 
  FFV1_0(w[0], w[58], w[119], pars->GC_11, amp[293]); 
  FFV1_0(w[100], w[58], w[4], pars->GC_11, amp[294]); 
  FFV1_0(w[81], w[122], w[59], pars->GC_11, amp[295]); 
  FFV1_0(w[0], w[71], w[89], pars->GC_11, amp[296]); 
  VVV1_0(w[4], w[59], w[89], pars->GC_10, amp[297]); 
  FFV1_0(w[123], w[80], w[59], pars->GC_11, amp[298]); 
  FFV1_0(w[0], w[71], w[92], pars->GC_11, amp[299]); 
  VVV1_0(w[4], w[59], w[92], pars->GC_10, amp[300]); 
  FFV2_5_0(w[100], w[71], w[2], pars->GC_51, pars->GC_58, amp[301]); 
  FFV1_0(w[0], w[60], w[119], pars->GC_11, amp[302]); 
  FFV1_0(w[100], w[60], w[4], pars->GC_11, amp[303]); 
  FFV1_0(w[1], w[13], w[125], pars->GC_11, amp[304]); 
  FFV1_0(w[15], w[6], w[125], pars->GC_11, amp[305]); 
  FFV1_0(w[126], w[124], w[16], pars->GC_11, amp[306]); 
  FFV2_3_0(w[18], w[124], w[3], pars->GC_50, pars->GC_58, amp[307]); 
  FFV1_0(w[126], w[5], w[20], pars->GC_11, amp[308]); 
  FFV1_0(w[1], w[19], w[128], pars->GC_11, amp[309]); 
  FFV1_0(w[126], w[5], w[25], pars->GC_11, amp[310]); 
  FFV1_0(w[24], w[6], w[128], pars->GC_11, amp[311]); 
  FFV1_0(w[129], w[127], w[16], pars->GC_11, amp[312]); 
  FFV2_3_0(w[18], w[127], w[2], pars->GC_50, pars->GC_58, amp[313]); 
  FFV1_0(w[129], w[5], w[28], pars->GC_11, amp[314]); 
  FFV1_0(w[129], w[5], w[30], pars->GC_11, amp[315]); 
  FFV2_3_0(w[129], w[32], w[3], pars->GC_50, pars->GC_58, amp[316]); 
  FFV2_3_0(w[126], w[32], w[2], pars->GC_50, pars->GC_58, amp[317]); 
  FFV1_0(w[1], w[13], w[131], pars->GC_11, amp[318]); 
  FFV1_0(w[15], w[6], w[131], pars->GC_11, amp[319]); 
  FFV1_0(w[130], w[132], w[16], pars->GC_11, amp[320]); 
  FFV2_3_0(w[130], w[37], w[3], pars->GC_50, pars->GC_58, amp[321]); 
  FFV1_0(w[1], w[19], w[134], pars->GC_11, amp[322]); 
  FFV1_0(w[24], w[6], w[134], pars->GC_11, amp[323]); 
  FFV1_0(w[133], w[135], w[16], pars->GC_11, amp[324]); 
  FFV2_3_0(w[133], w[37], w[2], pars->GC_50, pars->GC_58, amp[325]); 
  FFV1_0(w[0], w[132], w[20], pars->GC_11, amp[326]); 
  FFV1_0(w[0], w[132], w[25], pars->GC_11, amp[327]); 
  FFV1_0(w[0], w[135], w[28], pars->GC_11, amp[328]); 
  FFV1_0(w[0], w[135], w[30], pars->GC_11, amp[329]); 
  FFV2_3_0(w[42], w[135], w[3], pars->GC_50, pars->GC_58, amp[330]); 
  FFV2_3_0(w[42], w[132], w[2], pars->GC_50, pars->GC_58, amp[331]); 
  FFV1_0(w[1], w[44], w[136], pars->GC_11, amp[332]); 
  FFV1_0(w[137], w[5], w[46], pars->GC_11, amp[333]); 
  FFV1_0(w[130], w[127], w[46], pars->GC_11, amp[334]); 
  FFV1_0(w[130], w[5], w[48], pars->GC_11, amp[335]); 
  FFV1_0(w[1], w[49], w[138], pars->GC_11, amp[336]); 
  FFV1_0(w[139], w[5], w[46], pars->GC_11, amp[337]); 
  FFV1_0(w[133], w[124], w[46], pars->GC_11, amp[338]); 
  FFV1_0(w[133], w[5], w[52], pars->GC_11, amp[339]); 
  FFV1_0(w[1], w[44], w[140], pars->GC_11, amp[340]); 
  FFV1_0(w[0], w[141], w[46], pars->GC_11, amp[341]); 
  FFV1_0(w[0], w[124], w[48], pars->GC_11, amp[342]); 
  FFV1_0(w[0], w[127], w[52], pars->GC_11, amp[343]); 
  FFV1_0(w[1], w[49], w[142], pars->GC_11, amp[344]); 
  FFV1_0(w[0], w[143], w[46], pars->GC_11, amp[345]); 
  FFV1_0(w[62], w[6], w[136], pars->GC_11, amp[346]); 
  FFV1_0(w[137], w[5], w[63], pars->GC_11, amp[347]); 
  FFV1_0(w[130], w[127], w[63], pars->GC_11, amp[348]); 
  FFV1_0(w[130], w[5], w[64], pars->GC_11, amp[349]); 
  FFV1_0(w[65], w[6], w[138], pars->GC_11, amp[350]); 
  FFV1_0(w[139], w[5], w[63], pars->GC_11, amp[351]); 
  FFV1_0(w[133], w[124], w[63], pars->GC_11, amp[352]); 
  FFV1_0(w[133], w[5], w[66], pars->GC_11, amp[353]); 
  FFV1_0(w[62], w[6], w[140], pars->GC_11, amp[354]); 
  FFV1_0(w[0], w[141], w[63], pars->GC_11, amp[355]); 
  FFV1_0(w[0], w[124], w[64], pars->GC_11, amp[356]); 
  FFV1_0(w[0], w[127], w[66], pars->GC_11, amp[357]); 
  FFV1_0(w[65], w[6], w[142], pars->GC_11, amp[358]); 
  FFV1_0(w[0], w[143], w[63], pars->GC_11, amp[359]); 
  FFV1_0(w[144], w[127], w[16], pars->GC_11, amp[360]); 
  FFV1_0(w[130], w[145], w[16], pars->GC_11, amp[361]); 
  FFV1_0(w[130], w[127], w[72], pars->GC_11, amp[362]); 
  FFV1_0(w[144], w[5], w[28], pars->GC_11, amp[363]); 
  FFV1_0(w[1], w[73], w[136], pars->GC_11, amp[364]); 
  VVV1_0(w[4], w[136], w[28], pars->GC_10, amp[365]); 
  FFV1_0(w[144], w[5], w[30], pars->GC_11, amp[366]); 
  FFV1_0(w[74], w[6], w[136], pars->GC_11, amp[367]); 
  VVV1_0(w[4], w[136], w[30], pars->GC_10, amp[368]); 
  FFV2_3_0(w[144], w[32], w[3], pars->GC_50, pars->GC_58, amp[369]); 
  FFV1_0(w[137], w[5], w[72], pars->GC_11, amp[370]); 
  FFV1_0(w[137], w[32], w[4], pars->GC_11, amp[371]); 
  FFV1_0(w[146], w[124], w[16], pars->GC_11, amp[372]); 
  FFV1_0(w[133], w[147], w[16], pars->GC_11, amp[373]); 
  FFV1_0(w[133], w[124], w[72], pars->GC_11, amp[374]); 
  FFV1_0(w[146], w[5], w[20], pars->GC_11, amp[375]); 
  FFV1_0(w[1], w[77], w[138], pars->GC_11, amp[376]); 
  VVV1_0(w[4], w[138], w[20], pars->GC_10, amp[377]); 
  FFV1_0(w[146], w[5], w[25], pars->GC_11, amp[378]); 
  FFV1_0(w[78], w[6], w[138], pars->GC_11, amp[379]); 
  VVV1_0(w[4], w[138], w[25], pars->GC_10, amp[380]); 
  FFV2_3_0(w[146], w[32], w[2], pars->GC_50, pars->GC_58, amp[381]); 
  FFV1_0(w[139], w[5], w[72], pars->GC_11, amp[382]); 
  FFV1_0(w[139], w[32], w[4], pars->GC_11, amp[383]); 
  FFV1_0(w[0], w[147], w[28], pars->GC_11, amp[384]); 
  FFV1_0(w[1], w[73], w[140], pars->GC_11, amp[385]); 
  VVV1_0(w[4], w[140], w[28], pars->GC_10, amp[386]); 
  FFV1_0(w[0], w[147], w[30], pars->GC_11, amp[387]); 
  FFV1_0(w[74], w[6], w[140], pars->GC_11, amp[388]); 
  VVV1_0(w[4], w[140], w[30], pars->GC_10, amp[389]); 
  FFV2_3_0(w[42], w[147], w[3], pars->GC_50, pars->GC_58, amp[390]); 
  FFV1_0(w[0], w[141], w[72], pars->GC_11, amp[391]); 
  FFV1_0(w[42], w[141], w[4], pars->GC_11, amp[392]); 
  FFV1_0(w[1], w[77], w[142], pars->GC_11, amp[393]); 
  FFV1_0(w[0], w[145], w[20], pars->GC_11, amp[394]); 
  VVV1_0(w[4], w[142], w[20], pars->GC_10, amp[395]); 
  FFV1_0(w[78], w[6], w[142], pars->GC_11, amp[396]); 
  FFV1_0(w[0], w[145], w[25], pars->GC_11, amp[397]); 
  VVV1_0(w[4], w[142], w[25], pars->GC_10, amp[398]); 
  FFV2_3_0(w[42], w[145], w[2], pars->GC_50, pars->GC_58, amp[399]); 
  FFV1_0(w[0], w[143], w[72], pars->GC_11, amp[400]); 
  FFV1_0(w[42], w[143], w[4], pars->GC_11, amp[401]); 
  FFV1_0(w[81], w[82], w[9], pars->GC_11, amp[402]); 
  FFV1_0(w[83], w[80], w[9], pars->GC_11, amp[403]); 
  FFV1_0(w[81], w[84], w[125], pars->GC_11, amp[404]); 
  FFV1_0(w[85], w[80], w[125], pars->GC_11, amp[405]); 
  FFV1_0(w[126], w[124], w[86], pars->GC_11, amp[406]); 
  FFV2_3_0(w[87], w[124], w[3], pars->GC_50, pars->GC_58, amp[407]); 
  FFV1_0(w[126], w[5], w[89], pars->GC_11, amp[408]); 
  FFV1_0(w[81], w[90], w[9], pars->GC_11, amp[409]); 
  FFV1_0(w[81], w[88], w[128], pars->GC_11, amp[410]); 
  FFV1_0(w[85], w[88], w[9], pars->GC_11, amp[411]); 
  FFV1_0(w[126], w[5], w[92], pars->GC_11, amp[412]); 
  FFV1_0(w[93], w[80], w[9], pars->GC_11, amp[413]); 
  FFV1_0(w[91], w[80], w[128], pars->GC_11, amp[414]); 
  FFV1_0(w[91], w[84], w[9], pars->GC_11, amp[415]); 
  FFV1_0(w[129], w[127], w[86], pars->GC_11, amp[416]); 
  FFV2_3_0(w[87], w[127], w[2], pars->GC_50, pars->GC_58, amp[417]); 
  FFV1_0(w[129], w[5], w[94], pars->GC_11, amp[418]); 
  FFV1_0(w[81], w[95], w[9], pars->GC_11, amp[419]); 
  FFV1_0(w[129], w[5], w[96], pars->GC_11, amp[420]); 
  FFV1_0(w[97], w[80], w[9], pars->GC_11, amp[421]); 
  FFV2_3_0(w[129], w[98], w[3], pars->GC_50, pars->GC_58, amp[422]); 
  FFV2_3_0(w[126], w[98], w[2], pars->GC_50, pars->GC_58, amp[423]); 
  FFV1_0(w[81], w[84], w[131], pars->GC_11, amp[424]); 
  FFV1_0(w[85], w[80], w[131], pars->GC_11, amp[425]); 
  FFV1_0(w[130], w[132], w[86], pars->GC_11, amp[426]); 
  FFV2_3_0(w[130], w[99], w[3], pars->GC_50, pars->GC_58, amp[427]); 
  FFV1_0(w[81], w[88], w[134], pars->GC_11, amp[428]); 
  FFV1_0(w[91], w[80], w[134], pars->GC_11, amp[429]); 
  FFV1_0(w[133], w[135], w[86], pars->GC_11, amp[430]); 
  FFV2_3_0(w[133], w[99], w[2], pars->GC_50, pars->GC_58, amp[431]); 
  FFV1_0(w[81], w[82], w[41], pars->GC_11, amp[432]); 
  FFV1_0(w[83], w[80], w[41], pars->GC_11, amp[433]); 
  FFV1_0(w[81], w[90], w[41], pars->GC_11, amp[434]); 
  FFV1_0(w[0], w[132], w[89], pars->GC_11, amp[435]); 
  FFV1_0(w[85], w[88], w[41], pars->GC_11, amp[436]); 
  FFV1_0(w[93], w[80], w[41], pars->GC_11, amp[437]); 
  FFV1_0(w[0], w[132], w[92], pars->GC_11, amp[438]); 
  FFV1_0(w[91], w[84], w[41], pars->GC_11, amp[439]); 
  FFV1_0(w[81], w[95], w[41], pars->GC_11, amp[440]); 
  FFV1_0(w[0], w[135], w[94], pars->GC_11, amp[441]); 
  FFV1_0(w[97], w[80], w[41], pars->GC_11, amp[442]); 
  FFV1_0(w[0], w[135], w[96], pars->GC_11, amp[443]); 
  FFV2_3_0(w[100], w[135], w[3], pars->GC_50, pars->GC_58, amp[444]); 
  FFV2_3_0(w[100], w[132], w[2], pars->GC_50, pars->GC_58, amp[445]); 
  FFV1_0(w[81], w[102], w[136], pars->GC_11, amp[446]); 
  FFV1_0(w[137], w[5], w[103], pars->GC_11, amp[447]); 
  FFV1_0(w[130], w[127], w[103], pars->GC_11, amp[448]); 
  FFV1_0(w[130], w[5], w[104], pars->GC_11, amp[449]); 
  FFV1_0(w[81], w[105], w[138], pars->GC_11, amp[450]); 
  FFV1_0(w[139], w[5], w[103], pars->GC_11, amp[451]); 
  FFV1_0(w[133], w[124], w[103], pars->GC_11, amp[452]); 
  FFV1_0(w[133], w[5], w[106], pars->GC_11, amp[453]); 
  FFV2_3_0(w[107], w[105], w[3], pars->GC_50, pars->GC_58, amp[454]); 
  FFV2_3_0(w[107], w[102], w[2], pars->GC_50, pars->GC_58, amp[455]); 
  FFS4_0(w[81], w[108], w[8], pars->GC_83, amp[456]); 
  FFV1_0(w[81], w[109], w[53], pars->GC_11, amp[457]); 
  FFV2_3_0(w[91], w[108], w[3], pars->GC_50, pars->GC_58, amp[458]); 
  FFV1_0(w[91], w[102], w[53], pars->GC_11, amp[459]); 
  FFV2_3_0(w[85], w[108], w[2], pars->GC_50, pars->GC_58, amp[460]); 
  FFV1_0(w[85], w[105], w[53], pars->GC_11, amp[461]); 
  FFV1_0(w[81], w[102], w[140], pars->GC_11, amp[462]); 
  FFV1_0(w[0], w[141], w[103], pars->GC_11, amp[463]); 
  FFV1_0(w[0], w[124], w[104], pars->GC_11, amp[464]); 
  FFV1_0(w[0], w[127], w[106], pars->GC_11, amp[465]); 
  FFV1_0(w[81], w[105], w[142], pars->GC_11, amp[466]); 
  FFV1_0(w[0], w[143], w[103], pars->GC_11, amp[467]); 
  FFV1_0(w[111], w[80], w[136], pars->GC_11, amp[468]); 
  FFV1_0(w[137], w[5], w[112], pars->GC_11, amp[469]); 
  FFV1_0(w[130], w[127], w[112], pars->GC_11, amp[470]); 
  FFV1_0(w[130], w[5], w[113], pars->GC_11, amp[471]); 
  FFV1_0(w[114], w[80], w[138], pars->GC_11, amp[472]); 
  FFV1_0(w[139], w[5], w[112], pars->GC_11, amp[473]); 
  FFV1_0(w[133], w[124], w[112], pars->GC_11, amp[474]); 
  FFV1_0(w[133], w[5], w[115], pars->GC_11, amp[475]); 
  FFV2_3_0(w[114], w[116], w[3], pars->GC_50, pars->GC_58, amp[476]); 
  FFV2_3_0(w[111], w[116], w[2], pars->GC_50, pars->GC_58, amp[477]); 
  FFS4_0(w[117], w[80], w[8], pars->GC_83, amp[478]); 
  FFV1_0(w[118], w[80], w[53], pars->GC_11, amp[479]); 
  FFV2_3_0(w[117], w[88], w[3], pars->GC_50, pars->GC_58, amp[480]); 
  FFV1_0(w[111], w[88], w[53], pars->GC_11, amp[481]); 
  FFV2_3_0(w[117], w[84], w[2], pars->GC_50, pars->GC_58, amp[482]); 
  FFV1_0(w[114], w[84], w[53], pars->GC_11, amp[483]); 
  FFV1_0(w[111], w[80], w[140], pars->GC_11, amp[484]); 
  FFV1_0(w[0], w[141], w[112], pars->GC_11, amp[485]); 
  FFV1_0(w[0], w[124], w[113], pars->GC_11, amp[486]); 
  FFV1_0(w[0], w[127], w[115], pars->GC_11, amp[487]); 
  FFV1_0(w[114], w[80], w[142], pars->GC_11, amp[488]); 
  FFV1_0(w[0], w[143], w[112], pars->GC_11, amp[489]); 
  FFV1_0(w[144], w[127], w[86], pars->GC_11, amp[490]); 
  FFV1_0(w[130], w[145], w[86], pars->GC_11, amp[491]); 
  FFV1_0(w[130], w[127], w[119], pars->GC_11, amp[492]); 
  FFV1_0(w[144], w[5], w[94], pars->GC_11, amp[493]); 
  FFV1_0(w[81], w[120], w[136], pars->GC_11, amp[494]); 
  VVV1_0(w[4], w[136], w[94], pars->GC_10, amp[495]); 
  FFV1_0(w[144], w[5], w[96], pars->GC_11, amp[496]); 
  FFV1_0(w[121], w[80], w[136], pars->GC_11, amp[497]); 
  VVV1_0(w[4], w[136], w[96], pars->GC_10, amp[498]); 
  FFV2_3_0(w[144], w[98], w[3], pars->GC_50, pars->GC_58, amp[499]); 
  FFV1_0(w[137], w[5], w[119], pars->GC_11, amp[500]); 
  FFV1_0(w[137], w[98], w[4], pars->GC_11, amp[501]); 
  FFV1_0(w[146], w[124], w[86], pars->GC_11, amp[502]); 
  FFV1_0(w[133], w[147], w[86], pars->GC_11, amp[503]); 
  FFV1_0(w[133], w[124], w[119], pars->GC_11, amp[504]); 
  FFV1_0(w[146], w[5], w[89], pars->GC_11, amp[505]); 
  FFV1_0(w[81], w[122], w[138], pars->GC_11, amp[506]); 
  VVV1_0(w[4], w[138], w[89], pars->GC_10, amp[507]); 
  FFV1_0(w[146], w[5], w[92], pars->GC_11, amp[508]); 
  FFV1_0(w[123], w[80], w[138], pars->GC_11, amp[509]); 
  VVV1_0(w[4], w[138], w[92], pars->GC_10, amp[510]); 
  FFV2_3_0(w[146], w[98], w[2], pars->GC_50, pars->GC_58, amp[511]); 
  FFV1_0(w[139], w[5], w[119], pars->GC_11, amp[512]); 
  FFV1_0(w[139], w[98], w[4], pars->GC_11, amp[513]); 
  FFV1_0(w[81], w[82], w[79], pars->GC_11, amp[514]); 
  FFV1_0(w[83], w[80], w[79], pars->GC_11, amp[515]); 
  FFV1_0(w[83], w[116], w[4], pars->GC_11, amp[516]); 
  FFV1_0(w[107], w[82], w[4], pars->GC_11, amp[517]); 
  FFV1_0(w[81], w[90], w[79], pars->GC_11, amp[518]); 
  FFV2_3_0(w[107], w[122], w[3], pars->GC_50, pars->GC_58, amp[519]); 
  FFV1_0(w[107], w[90], w[4], pars->GC_11, amp[520]); 
  FFV1_0(w[85], w[88], w[79], pars->GC_11, amp[521]); 
  FFV1_0(w[85], w[122], w[53], pars->GC_11, amp[522]); 
  FFV1_0(w[121], w[88], w[53], pars->GC_11, amp[523]); 
  FFV1_0(w[93], w[80], w[79], pars->GC_11, amp[524]); 
  FFV2_3_0(w[123], w[116], w[3], pars->GC_50, pars->GC_58, amp[525]); 
  FFV1_0(w[93], w[116], w[4], pars->GC_11, amp[526]); 
  FFV1_0(w[91], w[84], w[79], pars->GC_11, amp[527]); 
  FFV1_0(w[123], w[84], w[53], pars->GC_11, amp[528]); 
  FFV1_0(w[91], w[120], w[53], pars->GC_11, amp[529]); 
  FFV1_0(w[81], w[95], w[79], pars->GC_11, amp[530]); 
  FFV2_3_0(w[107], w[120], w[2], pars->GC_50, pars->GC_58, amp[531]); 
  FFV1_0(w[107], w[95], w[4], pars->GC_11, amp[532]); 
  FFV1_0(w[97], w[80], w[79], pars->GC_11, amp[533]); 
  FFV2_3_0(w[121], w[116], w[2], pars->GC_50, pars->GC_58, amp[534]); 
  FFV1_0(w[97], w[116], w[4], pars->GC_11, amp[535]); 
  FFV1_0(w[0], w[147], w[94], pars->GC_11, amp[536]); 
  FFV1_0(w[81], w[120], w[140], pars->GC_11, amp[537]); 
  VVV1_0(w[4], w[140], w[94], pars->GC_10, amp[538]); 
  FFV1_0(w[0], w[147], w[96], pars->GC_11, amp[539]); 
  FFV1_0(w[121], w[80], w[140], pars->GC_11, amp[540]); 
  VVV1_0(w[4], w[140], w[96], pars->GC_10, amp[541]); 
  FFV2_3_0(w[100], w[147], w[3], pars->GC_50, pars->GC_58, amp[542]); 
  FFV1_0(w[0], w[141], w[119], pars->GC_11, amp[543]); 
  FFV1_0(w[100], w[141], w[4], pars->GC_11, amp[544]); 
  FFV1_0(w[81], w[122], w[142], pars->GC_11, amp[545]); 
  FFV1_0(w[0], w[145], w[89], pars->GC_11, amp[546]); 
  VVV1_0(w[4], w[142], w[89], pars->GC_10, amp[547]); 
  FFV1_0(w[123], w[80], w[142], pars->GC_11, amp[548]); 
  FFV1_0(w[0], w[145], w[92], pars->GC_11, amp[549]); 
  VVV1_0(w[4], w[142], w[92], pars->GC_10, amp[550]); 
  FFV2_3_0(w[100], w[145], w[2], pars->GC_50, pars->GC_58, amp[551]); 
  FFV1_0(w[0], w[143], w[119], pars->GC_11, amp[552]); 
  FFV1_0(w[100], w[143], w[4], pars->GC_11, amp[553]); 
  FFV1_0(w[1], w[10], w[151], pars->GC_11, amp[554]); 
  FFV1_0(w[11], w[6], w[151], pars->GC_11, amp[555]); 
  FFV1_0(w[1], w[13], w[153], pars->GC_11, amp[556]); 
  FFV1_0(w[15], w[6], w[153], pars->GC_11, amp[557]); 
  FFV1_0(w[154], w[152], w[16], pars->GC_11, amp[558]); 
  FFV2_5_0(w[155], w[152], w[3], pars->GC_51, pars->GC_58, amp[559]); 
  FFV1_0(w[154], w[148], w[20], pars->GC_11, amp[560]); 
  FFV1_0(w[1], w[21], w[151], pars->GC_11, amp[561]); 
  FFV1_0(w[1], w[19], w[157], pars->GC_11, amp[562]); 
  FFV1_0(w[15], w[19], w[151], pars->GC_11, amp[563]); 
  FFV1_0(w[154], w[148], w[25], pars->GC_11, amp[564]); 
  FFV1_0(w[26], w[6], w[151], pars->GC_11, amp[565]); 
  FFV1_0(w[24], w[6], w[157], pars->GC_11, amp[566]); 
  FFV1_0(w[24], w[13], w[151], pars->GC_11, amp[567]); 
  FFV1_0(w[158], w[156], w[16], pars->GC_11, amp[568]); 
  FFV2_5_0(w[155], w[156], w[2], pars->GC_51, pars->GC_58, amp[569]); 
  FFV1_0(w[158], w[148], w[28], pars->GC_11, amp[570]); 
  FFV1_0(w[1], w[29], w[151], pars->GC_11, amp[571]); 
  FFV1_0(w[158], w[148], w[30], pars->GC_11, amp[572]); 
  FFV1_0(w[31], w[6], w[151], pars->GC_11, amp[573]); 
  FFV2_5_0(w[158], w[159], w[3], pars->GC_51, pars->GC_58, amp[574]); 
  FFV2_5_0(w[154], w[159], w[2], pars->GC_51, pars->GC_58, amp[575]); 
  FFV1_0(w[1], w[13], w[162], pars->GC_11, amp[576]); 
  FFV1_0(w[15], w[6], w[162], pars->GC_11, amp[577]); 
  FFV1_0(w[161], w[163], w[16], pars->GC_11, amp[578]); 
  FFV2_5_0(w[161], w[164], w[3], pars->GC_51, pars->GC_58, amp[579]); 
  FFV1_0(w[1], w[19], w[166], pars->GC_11, amp[580]); 
  FFV1_0(w[24], w[6], w[166], pars->GC_11, amp[581]); 
  FFV1_0(w[165], w[167], w[16], pars->GC_11, amp[582]); 
  FFV2_5_0(w[165], w[164], w[2], pars->GC_51, pars->GC_58, amp[583]); 
  FFV1_0(w[1], w[10], w[168], pars->GC_11, amp[584]); 
  FFV1_0(w[11], w[6], w[168], pars->GC_11, amp[585]); 
  FFV1_0(w[1], w[21], w[168], pars->GC_11, amp[586]); 
  FFV1_0(w[149], w[163], w[20], pars->GC_11, amp[587]); 
  FFV1_0(w[15], w[19], w[168], pars->GC_11, amp[588]); 
  FFV1_0(w[26], w[6], w[168], pars->GC_11, amp[589]); 
  FFV1_0(w[149], w[163], w[25], pars->GC_11, amp[590]); 
  FFV1_0(w[24], w[13], w[168], pars->GC_11, amp[591]); 
  FFV1_0(w[1], w[29], w[168], pars->GC_11, amp[592]); 
  FFV1_0(w[149], w[167], w[28], pars->GC_11, amp[593]); 
  FFV1_0(w[31], w[6], w[168], pars->GC_11, amp[594]); 
  FFV1_0(w[149], w[167], w[30], pars->GC_11, amp[595]); 
  FFV2_5_0(w[169], w[167], w[3], pars->GC_51, pars->GC_58, amp[596]); 
  FFV2_5_0(w[169], w[163], w[2], pars->GC_51, pars->GC_58, amp[597]); 
  FFV1_0(w[1], w[44], w[170], pars->GC_11, amp[598]); 
  FFV1_0(w[171], w[148], w[46], pars->GC_11, amp[599]); 
  FFV1_0(w[161], w[156], w[46], pars->GC_11, amp[600]); 
  FFV1_0(w[161], w[148], w[48], pars->GC_11, amp[601]); 
  FFV1_0(w[1], w[49], w[172], pars->GC_11, amp[602]); 
  FFV1_0(w[173], w[148], w[46], pars->GC_11, amp[603]); 
  FFV1_0(w[165], w[152], w[46], pars->GC_11, amp[604]); 
  FFV1_0(w[165], w[148], w[52], pars->GC_11, amp[605]); 
  FFV2_3_0(w[175], w[49], w[3], pars->GC_50, pars->GC_58, amp[606]); 
  FFV2_3_0(w[175], w[44], w[2], pars->GC_50, pars->GC_58, amp[607]); 
  FFS4_0(w[1], w[176], w[8], pars->GC_83, amp[608]); 
  FFV1_0(w[1], w[56], w[174], pars->GC_11, amp[609]); 
  FFV2_3_0(w[24], w[176], w[3], pars->GC_50, pars->GC_58, amp[610]); 
  FFV1_0(w[24], w[44], w[174], pars->GC_11, amp[611]); 
  FFV2_3_0(w[15], w[176], w[2], pars->GC_50, pars->GC_58, amp[612]); 
  FFV1_0(w[15], w[49], w[174], pars->GC_11, amp[613]); 
  FFV1_0(w[1], w[44], w[177], pars->GC_11, amp[614]); 
  FFV1_0(w[149], w[178], w[46], pars->GC_11, amp[615]); 
  FFV1_0(w[149], w[152], w[48], pars->GC_11, amp[616]); 
  FFV1_0(w[149], w[156], w[52], pars->GC_11, amp[617]); 
  FFV1_0(w[1], w[49], w[179], pars->GC_11, amp[618]); 
  FFV1_0(w[149], w[180], w[46], pars->GC_11, amp[619]); 
  FFV1_0(w[62], w[6], w[170], pars->GC_11, amp[620]); 
  FFV1_0(w[171], w[148], w[63], pars->GC_11, amp[621]); 
  FFV1_0(w[161], w[156], w[63], pars->GC_11, amp[622]); 
  FFV1_0(w[161], w[148], w[64], pars->GC_11, amp[623]); 
  FFV1_0(w[65], w[6], w[172], pars->GC_11, amp[624]); 
  FFV1_0(w[173], w[148], w[63], pars->GC_11, amp[625]); 
  FFV1_0(w[165], w[152], w[63], pars->GC_11, amp[626]); 
  FFV1_0(w[165], w[148], w[66], pars->GC_11, amp[627]); 
  FFV2_3_0(w[65], w[181], w[3], pars->GC_50, pars->GC_58, amp[628]); 
  FFV2_3_0(w[62], w[181], w[2], pars->GC_50, pars->GC_58, amp[629]); 
  FFS4_0(w[182], w[6], w[8], pars->GC_83, amp[630]); 
  FFV1_0(w[69], w[6], w[174], pars->GC_11, amp[631]); 
  FFV2_3_0(w[182], w[19], w[3], pars->GC_50, pars->GC_58, amp[632]); 
  FFV1_0(w[62], w[19], w[174], pars->GC_11, amp[633]); 
  FFV2_3_0(w[182], w[13], w[2], pars->GC_50, pars->GC_58, amp[634]); 
  FFV1_0(w[65], w[13], w[174], pars->GC_11, amp[635]); 
  FFV1_0(w[62], w[6], w[177], pars->GC_11, amp[636]); 
  FFV1_0(w[149], w[178], w[63], pars->GC_11, amp[637]); 
  FFV1_0(w[149], w[152], w[64], pars->GC_11, amp[638]); 
  FFV1_0(w[149], w[156], w[66], pars->GC_11, amp[639]); 
  FFV1_0(w[65], w[6], w[179], pars->GC_11, amp[640]); 
  FFV1_0(w[149], w[180], w[63], pars->GC_11, amp[641]); 
  FFV1_0(w[183], w[156], w[16], pars->GC_11, amp[642]); 
  FFV1_0(w[161], w[184], w[16], pars->GC_11, amp[643]); 
  FFV1_0(w[161], w[156], w[72], pars->GC_11, amp[644]); 
  FFV1_0(w[183], w[148], w[28], pars->GC_11, amp[645]); 
  FFV1_0(w[1], w[73], w[170], pars->GC_11, amp[646]); 
  VVV1_0(w[4], w[170], w[28], pars->GC_10, amp[647]); 
  FFV1_0(w[183], w[148], w[30], pars->GC_11, amp[648]); 
  FFV1_0(w[74], w[6], w[170], pars->GC_11, amp[649]); 
  VVV1_0(w[4], w[170], w[30], pars->GC_10, amp[650]); 
  FFV2_5_0(w[183], w[159], w[3], pars->GC_51, pars->GC_58, amp[651]); 
  FFV1_0(w[171], w[148], w[72], pars->GC_11, amp[652]); 
  FFV1_0(w[171], w[159], w[4], pars->GC_11, amp[653]); 
  FFV1_0(w[185], w[152], w[16], pars->GC_11, amp[654]); 
  FFV1_0(w[165], w[186], w[16], pars->GC_11, amp[655]); 
  FFV1_0(w[165], w[152], w[72], pars->GC_11, amp[656]); 
  FFV1_0(w[185], w[148], w[20], pars->GC_11, amp[657]); 
  FFV1_0(w[1], w[77], w[172], pars->GC_11, amp[658]); 
  VVV1_0(w[4], w[172], w[20], pars->GC_10, amp[659]); 
  FFV1_0(w[185], w[148], w[25], pars->GC_11, amp[660]); 
  FFV1_0(w[78], w[6], w[172], pars->GC_11, amp[661]); 
  VVV1_0(w[4], w[172], w[25], pars->GC_10, amp[662]); 
  FFV2_5_0(w[185], w[159], w[2], pars->GC_51, pars->GC_58, amp[663]); 
  FFV1_0(w[173], w[148], w[72], pars->GC_11, amp[664]); 
  FFV1_0(w[173], w[159], w[4], pars->GC_11, amp[665]); 
  FFV1_0(w[1], w[10], w[187], pars->GC_11, amp[666]); 
  FFV1_0(w[11], w[6], w[187], pars->GC_11, amp[667]); 
  FFV1_0(w[11], w[181], w[4], pars->GC_11, amp[668]); 
  FFV1_0(w[175], w[10], w[4], pars->GC_11, amp[669]); 
  FFV1_0(w[1], w[21], w[187], pars->GC_11, amp[670]); 
  FFV2_3_0(w[175], w[77], w[3], pars->GC_50, pars->GC_58, amp[671]); 
  FFV1_0(w[175], w[21], w[4], pars->GC_11, amp[672]); 
  FFV1_0(w[15], w[19], w[187], pars->GC_11, amp[673]); 
  FFV1_0(w[15], w[77], w[174], pars->GC_11, amp[674]); 
  FFV1_0(w[74], w[19], w[174], pars->GC_11, amp[675]); 
  FFV1_0(w[26], w[6], w[187], pars->GC_11, amp[676]); 
  FFV2_3_0(w[78], w[181], w[3], pars->GC_50, pars->GC_58, amp[677]); 
  FFV1_0(w[26], w[181], w[4], pars->GC_11, amp[678]); 
  FFV1_0(w[24], w[13], w[187], pars->GC_11, amp[679]); 
  FFV1_0(w[78], w[13], w[174], pars->GC_11, amp[680]); 
  FFV1_0(w[24], w[73], w[174], pars->GC_11, amp[681]); 
  FFV1_0(w[1], w[29], w[187], pars->GC_11, amp[682]); 
  FFV2_3_0(w[175], w[73], w[2], pars->GC_50, pars->GC_58, amp[683]); 
  FFV1_0(w[175], w[29], w[4], pars->GC_11, amp[684]); 
  FFV1_0(w[31], w[6], w[187], pars->GC_11, amp[685]); 
  FFV2_3_0(w[74], w[181], w[2], pars->GC_50, pars->GC_58, amp[686]); 
  FFV1_0(w[31], w[181], w[4], pars->GC_11, amp[687]); 
  FFV1_0(w[149], w[186], w[28], pars->GC_11, amp[688]); 
  FFV1_0(w[1], w[73], w[177], pars->GC_11, amp[689]); 
  VVV1_0(w[4], w[177], w[28], pars->GC_10, amp[690]); 
  FFV1_0(w[149], w[186], w[30], pars->GC_11, amp[691]); 
  FFV1_0(w[74], w[6], w[177], pars->GC_11, amp[692]); 
  VVV1_0(w[4], w[177], w[30], pars->GC_10, amp[693]); 
  FFV2_5_0(w[169], w[186], w[3], pars->GC_51, pars->GC_58, amp[694]); 
  FFV1_0(w[149], w[178], w[72], pars->GC_11, amp[695]); 
  FFV1_0(w[169], w[178], w[4], pars->GC_11, amp[696]); 
  FFV1_0(w[1], w[77], w[179], pars->GC_11, amp[697]); 
  FFV1_0(w[149], w[184], w[20], pars->GC_11, amp[698]); 
  VVV1_0(w[4], w[179], w[20], pars->GC_10, amp[699]); 
  FFV1_0(w[78], w[6], w[179], pars->GC_11, amp[700]); 
  FFV1_0(w[149], w[184], w[25], pars->GC_11, amp[701]); 
  VVV1_0(w[4], w[179], w[25], pars->GC_10, amp[702]); 
  FFV2_5_0(w[169], w[184], w[2], pars->GC_51, pars->GC_58, amp[703]); 
  FFV1_0(w[149], w[180], w[72], pars->GC_11, amp[704]); 
  FFV1_0(w[169], w[180], w[4], pars->GC_11, amp[705]); 
  FFV1_0(w[81], w[82], w[151], pars->GC_11, amp[706]); 
  FFV1_0(w[83], w[80], w[151], pars->GC_11, amp[707]); 
  FFV1_0(w[81], w[84], w[153], pars->GC_11, amp[708]); 
  FFV1_0(w[85], w[80], w[153], pars->GC_11, amp[709]); 
  FFV1_0(w[154], w[152], w[86], pars->GC_11, amp[710]); 
  FFV2_5_0(w[188], w[152], w[3], pars->GC_51, pars->GC_58, amp[711]); 
  FFV1_0(w[154], w[148], w[89], pars->GC_11, amp[712]); 
  FFV1_0(w[81], w[90], w[151], pars->GC_11, amp[713]); 
  FFV1_0(w[81], w[88], w[157], pars->GC_11, amp[714]); 
  FFV1_0(w[85], w[88], w[151], pars->GC_11, amp[715]); 
  FFV1_0(w[154], w[148], w[92], pars->GC_11, amp[716]); 
  FFV1_0(w[93], w[80], w[151], pars->GC_11, amp[717]); 
  FFV1_0(w[91], w[80], w[157], pars->GC_11, amp[718]); 
  FFV1_0(w[91], w[84], w[151], pars->GC_11, amp[719]); 
  FFV1_0(w[158], w[156], w[86], pars->GC_11, amp[720]); 
  FFV2_5_0(w[188], w[156], w[2], pars->GC_51, pars->GC_58, amp[721]); 
  FFV1_0(w[158], w[148], w[94], pars->GC_11, amp[722]); 
  FFV1_0(w[81], w[95], w[151], pars->GC_11, amp[723]); 
  FFV1_0(w[158], w[148], w[96], pars->GC_11, amp[724]); 
  FFV1_0(w[97], w[80], w[151], pars->GC_11, amp[725]); 
  FFV2_5_0(w[158], w[189], w[3], pars->GC_51, pars->GC_58, amp[726]); 
  FFV2_5_0(w[154], w[189], w[2], pars->GC_51, pars->GC_58, amp[727]); 
  FFV1_0(w[81], w[84], w[162], pars->GC_11, amp[728]); 
  FFV1_0(w[85], w[80], w[162], pars->GC_11, amp[729]); 
  FFV1_0(w[161], w[163], w[86], pars->GC_11, amp[730]); 
  FFV2_5_0(w[161], w[190], w[3], pars->GC_51, pars->GC_58, amp[731]); 
  FFV1_0(w[81], w[88], w[166], pars->GC_11, amp[732]); 
  FFV1_0(w[91], w[80], w[166], pars->GC_11, amp[733]); 
  FFV1_0(w[165], w[167], w[86], pars->GC_11, amp[734]); 
  FFV2_5_0(w[165], w[190], w[2], pars->GC_51, pars->GC_58, amp[735]); 
  FFV1_0(w[81], w[82], w[168], pars->GC_11, amp[736]); 
  FFV1_0(w[83], w[80], w[168], pars->GC_11, amp[737]); 
  FFV1_0(w[81], w[90], w[168], pars->GC_11, amp[738]); 
  FFV1_0(w[149], w[163], w[89], pars->GC_11, amp[739]); 
  FFV1_0(w[85], w[88], w[168], pars->GC_11, amp[740]); 
  FFV1_0(w[93], w[80], w[168], pars->GC_11, amp[741]); 
  FFV1_0(w[149], w[163], w[92], pars->GC_11, amp[742]); 
  FFV1_0(w[91], w[84], w[168], pars->GC_11, amp[743]); 
  FFV1_0(w[81], w[95], w[168], pars->GC_11, amp[744]); 
  FFV1_0(w[149], w[167], w[94], pars->GC_11, amp[745]); 
  FFV1_0(w[97], w[80], w[168], pars->GC_11, amp[746]); 
  FFV1_0(w[149], w[167], w[96], pars->GC_11, amp[747]); 
  FFV2_5_0(w[191], w[167], w[3], pars->GC_51, pars->GC_58, amp[748]); 
  FFV2_5_0(w[191], w[163], w[2], pars->GC_51, pars->GC_58, amp[749]); 
  FFV1_0(w[81], w[102], w[170], pars->GC_11, amp[750]); 
  FFV1_0(w[171], w[148], w[103], pars->GC_11, amp[751]); 
  FFV1_0(w[161], w[156], w[103], pars->GC_11, amp[752]); 
  FFV1_0(w[161], w[148], w[104], pars->GC_11, amp[753]); 
  FFV1_0(w[81], w[105], w[172], pars->GC_11, amp[754]); 
  FFV1_0(w[173], w[148], w[103], pars->GC_11, amp[755]); 
  FFV1_0(w[165], w[152], w[103], pars->GC_11, amp[756]); 
  FFV1_0(w[165], w[148], w[106], pars->GC_11, amp[757]); 
  FFV2_3_0(w[192], w[105], w[3], pars->GC_50, pars->GC_58, amp[758]); 
  FFV2_3_0(w[192], w[102], w[2], pars->GC_50, pars->GC_58, amp[759]); 
  FFS4_0(w[81], w[193], w[8], pars->GC_83, amp[760]); 
  FFV1_0(w[81], w[109], w[174], pars->GC_11, amp[761]); 
  FFV2_3_0(w[91], w[193], w[3], pars->GC_50, pars->GC_58, amp[762]); 
  FFV1_0(w[91], w[102], w[174], pars->GC_11, amp[763]); 
  FFV2_3_0(w[85], w[193], w[2], pars->GC_50, pars->GC_58, amp[764]); 
  FFV1_0(w[85], w[105], w[174], pars->GC_11, amp[765]); 
  FFV1_0(w[81], w[102], w[177], pars->GC_11, amp[766]); 
  FFV1_0(w[149], w[178], w[103], pars->GC_11, amp[767]); 
  FFV1_0(w[149], w[152], w[104], pars->GC_11, amp[768]); 
  FFV1_0(w[149], w[156], w[106], pars->GC_11, amp[769]); 
  FFV1_0(w[81], w[105], w[179], pars->GC_11, amp[770]); 
  FFV1_0(w[149], w[180], w[103], pars->GC_11, amp[771]); 
  FFV1_0(w[111], w[80], w[170], pars->GC_11, amp[772]); 
  FFV1_0(w[171], w[148], w[112], pars->GC_11, amp[773]); 
  FFV1_0(w[161], w[156], w[112], pars->GC_11, amp[774]); 
  FFV1_0(w[161], w[148], w[113], pars->GC_11, amp[775]); 
  FFV1_0(w[114], w[80], w[172], pars->GC_11, amp[776]); 
  FFV1_0(w[173], w[148], w[112], pars->GC_11, amp[777]); 
  FFV1_0(w[165], w[152], w[112], pars->GC_11, amp[778]); 
  FFV1_0(w[165], w[148], w[115], pars->GC_11, amp[779]); 
  FFV2_3_0(w[114], w[194], w[3], pars->GC_50, pars->GC_58, amp[780]); 
  FFV2_3_0(w[111], w[194], w[2], pars->GC_50, pars->GC_58, amp[781]); 
  FFS4_0(w[195], w[80], w[8], pars->GC_83, amp[782]); 
  FFV1_0(w[118], w[80], w[174], pars->GC_11, amp[783]); 
  FFV2_3_0(w[195], w[88], w[3], pars->GC_50, pars->GC_58, amp[784]); 
  FFV1_0(w[111], w[88], w[174], pars->GC_11, amp[785]); 
  FFV2_3_0(w[195], w[84], w[2], pars->GC_50, pars->GC_58, amp[786]); 
  FFV1_0(w[114], w[84], w[174], pars->GC_11, amp[787]); 
  FFV1_0(w[111], w[80], w[177], pars->GC_11, amp[788]); 
  FFV1_0(w[149], w[178], w[112], pars->GC_11, amp[789]); 
  FFV1_0(w[149], w[152], w[113], pars->GC_11, amp[790]); 
  FFV1_0(w[149], w[156], w[115], pars->GC_11, amp[791]); 
  FFV1_0(w[114], w[80], w[179], pars->GC_11, amp[792]); 
  FFV1_0(w[149], w[180], w[112], pars->GC_11, amp[793]); 
  FFV1_0(w[183], w[156], w[86], pars->GC_11, amp[794]); 
  FFV1_0(w[161], w[184], w[86], pars->GC_11, amp[795]); 
  FFV1_0(w[161], w[156], w[119], pars->GC_11, amp[796]); 
  FFV1_0(w[183], w[148], w[94], pars->GC_11, amp[797]); 
  FFV1_0(w[81], w[120], w[170], pars->GC_11, amp[798]); 
  VVV1_0(w[4], w[170], w[94], pars->GC_10, amp[799]); 
  FFV1_0(w[183], w[148], w[96], pars->GC_11, amp[800]); 
  FFV1_0(w[121], w[80], w[170], pars->GC_11, amp[801]); 
  VVV1_0(w[4], w[170], w[96], pars->GC_10, amp[802]); 
  FFV2_5_0(w[183], w[189], w[3], pars->GC_51, pars->GC_58, amp[803]); 
  FFV1_0(w[171], w[148], w[119], pars->GC_11, amp[804]); 
  FFV1_0(w[171], w[189], w[4], pars->GC_11, amp[805]); 
  FFV1_0(w[185], w[152], w[86], pars->GC_11, amp[806]); 
  FFV1_0(w[165], w[186], w[86], pars->GC_11, amp[807]); 
  FFV1_0(w[165], w[152], w[119], pars->GC_11, amp[808]); 
  FFV1_0(w[185], w[148], w[89], pars->GC_11, amp[809]); 
  FFV1_0(w[81], w[122], w[172], pars->GC_11, amp[810]); 
  VVV1_0(w[4], w[172], w[89], pars->GC_10, amp[811]); 
  FFV1_0(w[185], w[148], w[92], pars->GC_11, amp[812]); 
  FFV1_0(w[123], w[80], w[172], pars->GC_11, amp[813]); 
  VVV1_0(w[4], w[172], w[92], pars->GC_10, amp[814]); 
  FFV2_5_0(w[185], w[189], w[2], pars->GC_51, pars->GC_58, amp[815]); 
  FFV1_0(w[173], w[148], w[119], pars->GC_11, amp[816]); 
  FFV1_0(w[173], w[189], w[4], pars->GC_11, amp[817]); 
  FFV1_0(w[81], w[82], w[187], pars->GC_11, amp[818]); 
  FFV1_0(w[83], w[80], w[187], pars->GC_11, amp[819]); 
  FFV1_0(w[83], w[194], w[4], pars->GC_11, amp[820]); 
  FFV1_0(w[192], w[82], w[4], pars->GC_11, amp[821]); 
  FFV1_0(w[81], w[90], w[187], pars->GC_11, amp[822]); 
  FFV2_3_0(w[192], w[122], w[3], pars->GC_50, pars->GC_58, amp[823]); 
  FFV1_0(w[192], w[90], w[4], pars->GC_11, amp[824]); 
  FFV1_0(w[85], w[88], w[187], pars->GC_11, amp[825]); 
  FFV1_0(w[85], w[122], w[174], pars->GC_11, amp[826]); 
  FFV1_0(w[121], w[88], w[174], pars->GC_11, amp[827]); 
  FFV1_0(w[93], w[80], w[187], pars->GC_11, amp[828]); 
  FFV2_3_0(w[123], w[194], w[3], pars->GC_50, pars->GC_58, amp[829]); 
  FFV1_0(w[93], w[194], w[4], pars->GC_11, amp[830]); 
  FFV1_0(w[91], w[84], w[187], pars->GC_11, amp[831]); 
  FFV1_0(w[123], w[84], w[174], pars->GC_11, amp[832]); 
  FFV1_0(w[91], w[120], w[174], pars->GC_11, amp[833]); 
  FFV1_0(w[81], w[95], w[187], pars->GC_11, amp[834]); 
  FFV2_3_0(w[192], w[120], w[2], pars->GC_50, pars->GC_58, amp[835]); 
  FFV1_0(w[192], w[95], w[4], pars->GC_11, amp[836]); 
  FFV1_0(w[97], w[80], w[187], pars->GC_11, amp[837]); 
  FFV2_3_0(w[121], w[194], w[2], pars->GC_50, pars->GC_58, amp[838]); 
  FFV1_0(w[97], w[194], w[4], pars->GC_11, amp[839]); 
  FFV1_0(w[149], w[186], w[94], pars->GC_11, amp[840]); 
  FFV1_0(w[81], w[120], w[177], pars->GC_11, amp[841]); 
  VVV1_0(w[4], w[177], w[94], pars->GC_10, amp[842]); 
  FFV1_0(w[149], w[186], w[96], pars->GC_11, amp[843]); 
  FFV1_0(w[121], w[80], w[177], pars->GC_11, amp[844]); 
  VVV1_0(w[4], w[177], w[96], pars->GC_10, amp[845]); 
  FFV2_5_0(w[191], w[186], w[3], pars->GC_51, pars->GC_58, amp[846]); 
  FFV1_0(w[149], w[178], w[119], pars->GC_11, amp[847]); 
  FFV1_0(w[191], w[178], w[4], pars->GC_11, amp[848]); 
  FFV1_0(w[81], w[122], w[179], pars->GC_11, amp[849]); 
  FFV1_0(w[149], w[184], w[89], pars->GC_11, amp[850]); 
  VVV1_0(w[4], w[179], w[89], pars->GC_10, amp[851]); 
  FFV1_0(w[123], w[80], w[179], pars->GC_11, amp[852]); 
  FFV1_0(w[149], w[184], w[92], pars->GC_11, amp[853]); 
  VVV1_0(w[4], w[179], w[92], pars->GC_10, amp[854]); 
  FFV2_5_0(w[191], w[184], w[2], pars->GC_51, pars->GC_58, amp[855]); 
  FFV1_0(w[149], w[180], w[119], pars->GC_11, amp[856]); 
  FFV1_0(w[191], w[180], w[4], pars->GC_11, amp[857]); 
  FFV1_0(w[1], w[10], w[151], pars->GC_11, amp[858]); 
  FFV1_0(w[11], w[6], w[151], pars->GC_11, amp[859]); 
  FFV1_0(w[1], w[13], w[197], pars->GC_11, amp[860]); 
  FFV1_0(w[15], w[6], w[197], pars->GC_11, amp[861]); 
  FFV1_0(w[198], w[196], w[16], pars->GC_11, amp[862]); 
  FFV2_3_0(w[155], w[196], w[3], pars->GC_50, pars->GC_58, amp[863]); 
  FFV1_0(w[198], w[148], w[20], pars->GC_11, amp[864]); 
  FFV1_0(w[1], w[21], w[151], pars->GC_11, amp[865]); 
  FFV1_0(w[1], w[19], w[200], pars->GC_11, amp[866]); 
  FFV1_0(w[15], w[19], w[151], pars->GC_11, amp[867]); 
  FFV1_0(w[198], w[148], w[25], pars->GC_11, amp[868]); 
  FFV1_0(w[26], w[6], w[151], pars->GC_11, amp[869]); 
  FFV1_0(w[24], w[6], w[200], pars->GC_11, amp[870]); 
  FFV1_0(w[24], w[13], w[151], pars->GC_11, amp[871]); 
  FFV1_0(w[201], w[199], w[16], pars->GC_11, amp[872]); 
  FFV2_3_0(w[155], w[199], w[2], pars->GC_50, pars->GC_58, amp[873]); 
  FFV1_0(w[201], w[148], w[28], pars->GC_11, amp[874]); 
  FFV1_0(w[1], w[29], w[151], pars->GC_11, amp[875]); 
  FFV1_0(w[201], w[148], w[30], pars->GC_11, amp[876]); 
  FFV1_0(w[31], w[6], w[151], pars->GC_11, amp[877]); 
  FFV2_3_0(w[201], w[159], w[3], pars->GC_50, pars->GC_58, amp[878]); 
  FFV2_3_0(w[198], w[159], w[2], pars->GC_50, pars->GC_58, amp[879]); 
  FFV1_0(w[1], w[13], w[203], pars->GC_11, amp[880]); 
  FFV1_0(w[15], w[6], w[203], pars->GC_11, amp[881]); 
  FFV1_0(w[202], w[204], w[16], pars->GC_11, amp[882]); 
  FFV2_3_0(w[202], w[164], w[3], pars->GC_50, pars->GC_58, amp[883]); 
  FFV1_0(w[1], w[19], w[206], pars->GC_11, amp[884]); 
  FFV1_0(w[24], w[6], w[206], pars->GC_11, amp[885]); 
  FFV1_0(w[205], w[207], w[16], pars->GC_11, amp[886]); 
  FFV2_3_0(w[205], w[164], w[2], pars->GC_50, pars->GC_58, amp[887]); 
  FFV1_0(w[1], w[10], w[168], pars->GC_11, amp[888]); 
  FFV1_0(w[11], w[6], w[168], pars->GC_11, amp[889]); 
  FFV1_0(w[1], w[21], w[168], pars->GC_11, amp[890]); 
  FFV1_0(w[149], w[204], w[20], pars->GC_11, amp[891]); 
  FFV1_0(w[15], w[19], w[168], pars->GC_11, amp[892]); 
  FFV1_0(w[26], w[6], w[168], pars->GC_11, amp[893]); 
  FFV1_0(w[149], w[204], w[25], pars->GC_11, amp[894]); 
  FFV1_0(w[24], w[13], w[168], pars->GC_11, amp[895]); 
  FFV1_0(w[1], w[29], w[168], pars->GC_11, amp[896]); 
  FFV1_0(w[149], w[207], w[28], pars->GC_11, amp[897]); 
  FFV1_0(w[31], w[6], w[168], pars->GC_11, amp[898]); 
  FFV1_0(w[149], w[207], w[30], pars->GC_11, amp[899]); 
  FFV2_3_0(w[169], w[207], w[3], pars->GC_50, pars->GC_58, amp[900]); 
  FFV2_3_0(w[169], w[204], w[2], pars->GC_50, pars->GC_58, amp[901]); 
  FFV1_0(w[1], w[44], w[208], pars->GC_11, amp[902]); 
  FFV1_0(w[209], w[148], w[46], pars->GC_11, amp[903]); 
  FFV1_0(w[202], w[199], w[46], pars->GC_11, amp[904]); 
  FFV1_0(w[202], w[148], w[48], pars->GC_11, amp[905]); 
  FFV1_0(w[1], w[49], w[210], pars->GC_11, amp[906]); 
  FFV1_0(w[211], w[148], w[46], pars->GC_11, amp[907]); 
  FFV1_0(w[205], w[196], w[46], pars->GC_11, amp[908]); 
  FFV1_0(w[205], w[148], w[52], pars->GC_11, amp[909]); 
  FFV2_3_0(w[175], w[49], w[3], pars->GC_50, pars->GC_58, amp[910]); 
  FFV2_3_0(w[175], w[44], w[2], pars->GC_50, pars->GC_58, amp[911]); 
  FFS4_0(w[1], w[176], w[8], pars->GC_83, amp[912]); 
  FFV1_0(w[1], w[56], w[174], pars->GC_11, amp[913]); 
  FFV2_3_0(w[24], w[176], w[3], pars->GC_50, pars->GC_58, amp[914]); 
  FFV1_0(w[24], w[44], w[174], pars->GC_11, amp[915]); 
  FFV2_3_0(w[15], w[176], w[2], pars->GC_50, pars->GC_58, amp[916]); 
  FFV1_0(w[15], w[49], w[174], pars->GC_11, amp[917]); 
  FFV1_0(w[1], w[44], w[212], pars->GC_11, amp[918]); 
  FFV1_0(w[149], w[213], w[46], pars->GC_11, amp[919]); 
  FFV1_0(w[149], w[196], w[48], pars->GC_11, amp[920]); 
  FFV1_0(w[149], w[199], w[52], pars->GC_11, amp[921]); 
  FFV1_0(w[1], w[49], w[214], pars->GC_11, amp[922]); 
  FFV1_0(w[149], w[215], w[46], pars->GC_11, amp[923]); 
  FFV1_0(w[62], w[6], w[208], pars->GC_11, amp[924]); 
  FFV1_0(w[209], w[148], w[63], pars->GC_11, amp[925]); 
  FFV1_0(w[202], w[199], w[63], pars->GC_11, amp[926]); 
  FFV1_0(w[202], w[148], w[64], pars->GC_11, amp[927]); 
  FFV1_0(w[65], w[6], w[210], pars->GC_11, amp[928]); 
  FFV1_0(w[211], w[148], w[63], pars->GC_11, amp[929]); 
  FFV1_0(w[205], w[196], w[63], pars->GC_11, amp[930]); 
  FFV1_0(w[205], w[148], w[66], pars->GC_11, amp[931]); 
  FFV2_3_0(w[65], w[181], w[3], pars->GC_50, pars->GC_58, amp[932]); 
  FFV2_3_0(w[62], w[181], w[2], pars->GC_50, pars->GC_58, amp[933]); 
  FFS4_0(w[182], w[6], w[8], pars->GC_83, amp[934]); 
  FFV1_0(w[69], w[6], w[174], pars->GC_11, amp[935]); 
  FFV2_3_0(w[182], w[19], w[3], pars->GC_50, pars->GC_58, amp[936]); 
  FFV1_0(w[62], w[19], w[174], pars->GC_11, amp[937]); 
  FFV2_3_0(w[182], w[13], w[2], pars->GC_50, pars->GC_58, amp[938]); 
  FFV1_0(w[65], w[13], w[174], pars->GC_11, amp[939]); 
  FFV1_0(w[62], w[6], w[212], pars->GC_11, amp[940]); 
  FFV1_0(w[149], w[213], w[63], pars->GC_11, amp[941]); 
  FFV1_0(w[149], w[196], w[64], pars->GC_11, amp[942]); 
  FFV1_0(w[149], w[199], w[66], pars->GC_11, amp[943]); 
  FFV1_0(w[65], w[6], w[214], pars->GC_11, amp[944]); 
  FFV1_0(w[149], w[215], w[63], pars->GC_11, amp[945]); 
  FFV1_0(w[216], w[199], w[16], pars->GC_11, amp[946]); 
  FFV1_0(w[202], w[217], w[16], pars->GC_11, amp[947]); 
  FFV1_0(w[202], w[199], w[72], pars->GC_11, amp[948]); 
  FFV1_0(w[216], w[148], w[28], pars->GC_11, amp[949]); 
  FFV1_0(w[1], w[73], w[208], pars->GC_11, amp[950]); 
  VVV1_0(w[4], w[208], w[28], pars->GC_10, amp[951]); 
  FFV1_0(w[216], w[148], w[30], pars->GC_11, amp[952]); 
  FFV1_0(w[74], w[6], w[208], pars->GC_11, amp[953]); 
  VVV1_0(w[4], w[208], w[30], pars->GC_10, amp[954]); 
  FFV2_3_0(w[216], w[159], w[3], pars->GC_50, pars->GC_58, amp[955]); 
  FFV1_0(w[209], w[148], w[72], pars->GC_11, amp[956]); 
  FFV1_0(w[209], w[159], w[4], pars->GC_11, amp[957]); 
  FFV1_0(w[218], w[196], w[16], pars->GC_11, amp[958]); 
  FFV1_0(w[205], w[219], w[16], pars->GC_11, amp[959]); 
  FFV1_0(w[205], w[196], w[72], pars->GC_11, amp[960]); 
  FFV1_0(w[218], w[148], w[20], pars->GC_11, amp[961]); 
  FFV1_0(w[1], w[77], w[210], pars->GC_11, amp[962]); 
  VVV1_0(w[4], w[210], w[20], pars->GC_10, amp[963]); 
  FFV1_0(w[218], w[148], w[25], pars->GC_11, amp[964]); 
  FFV1_0(w[78], w[6], w[210], pars->GC_11, amp[965]); 
  VVV1_0(w[4], w[210], w[25], pars->GC_10, amp[966]); 
  FFV2_3_0(w[218], w[159], w[2], pars->GC_50, pars->GC_58, amp[967]); 
  FFV1_0(w[211], w[148], w[72], pars->GC_11, amp[968]); 
  FFV1_0(w[211], w[159], w[4], pars->GC_11, amp[969]); 
  FFV1_0(w[1], w[10], w[187], pars->GC_11, amp[970]); 
  FFV1_0(w[11], w[6], w[187], pars->GC_11, amp[971]); 
  FFV1_0(w[11], w[181], w[4], pars->GC_11, amp[972]); 
  FFV1_0(w[175], w[10], w[4], pars->GC_11, amp[973]); 
  FFV1_0(w[1], w[21], w[187], pars->GC_11, amp[974]); 
  FFV2_3_0(w[175], w[77], w[3], pars->GC_50, pars->GC_58, amp[975]); 
  FFV1_0(w[175], w[21], w[4], pars->GC_11, amp[976]); 
  FFV1_0(w[15], w[19], w[187], pars->GC_11, amp[977]); 
  FFV1_0(w[15], w[77], w[174], pars->GC_11, amp[978]); 
  FFV1_0(w[74], w[19], w[174], pars->GC_11, amp[979]); 
  FFV1_0(w[26], w[6], w[187], pars->GC_11, amp[980]); 
  FFV2_3_0(w[78], w[181], w[3], pars->GC_50, pars->GC_58, amp[981]); 
  FFV1_0(w[26], w[181], w[4], pars->GC_11, amp[982]); 
  FFV1_0(w[24], w[13], w[187], pars->GC_11, amp[983]); 
  FFV1_0(w[78], w[13], w[174], pars->GC_11, amp[984]); 
  FFV1_0(w[24], w[73], w[174], pars->GC_11, amp[985]); 
  FFV1_0(w[1], w[29], w[187], pars->GC_11, amp[986]); 
  FFV2_3_0(w[175], w[73], w[2], pars->GC_50, pars->GC_58, amp[987]); 
  FFV1_0(w[175], w[29], w[4], pars->GC_11, amp[988]); 
  FFV1_0(w[31], w[6], w[187], pars->GC_11, amp[989]); 
  FFV2_3_0(w[74], w[181], w[2], pars->GC_50, pars->GC_58, amp[990]); 
  FFV1_0(w[31], w[181], w[4], pars->GC_11, amp[991]); 
  FFV1_0(w[149], w[219], w[28], pars->GC_11, amp[992]); 
  FFV1_0(w[1], w[73], w[212], pars->GC_11, amp[993]); 
  VVV1_0(w[4], w[212], w[28], pars->GC_10, amp[994]); 
  FFV1_0(w[149], w[219], w[30], pars->GC_11, amp[995]); 
  FFV1_0(w[74], w[6], w[212], pars->GC_11, amp[996]); 
  VVV1_0(w[4], w[212], w[30], pars->GC_10, amp[997]); 
  FFV2_3_0(w[169], w[219], w[3], pars->GC_50, pars->GC_58, amp[998]); 
  FFV1_0(w[149], w[213], w[72], pars->GC_11, amp[999]); 
  FFV1_0(w[169], w[213], w[4], pars->GC_11, amp[1000]); 
  FFV1_0(w[1], w[77], w[214], pars->GC_11, amp[1001]); 
  FFV1_0(w[149], w[217], w[20], pars->GC_11, amp[1002]); 
  VVV1_0(w[4], w[214], w[20], pars->GC_10, amp[1003]); 
  FFV1_0(w[78], w[6], w[214], pars->GC_11, amp[1004]); 
  FFV1_0(w[149], w[217], w[25], pars->GC_11, amp[1005]); 
  VVV1_0(w[4], w[214], w[25], pars->GC_10, amp[1006]); 
  FFV2_3_0(w[169], w[217], w[2], pars->GC_50, pars->GC_58, amp[1007]); 
  FFV1_0(w[149], w[215], w[72], pars->GC_11, amp[1008]); 
  FFV1_0(w[169], w[215], w[4], pars->GC_11, amp[1009]); 
  FFV1_0(w[81], w[82], w[151], pars->GC_11, amp[1010]); 
  FFV1_0(w[83], w[80], w[151], pars->GC_11, amp[1011]); 
  FFV1_0(w[81], w[84], w[197], pars->GC_11, amp[1012]); 
  FFV1_0(w[85], w[80], w[197], pars->GC_11, amp[1013]); 
  FFV1_0(w[198], w[196], w[86], pars->GC_11, amp[1014]); 
  FFV2_3_0(w[188], w[196], w[3], pars->GC_50, pars->GC_58, amp[1015]); 
  FFV1_0(w[198], w[148], w[89], pars->GC_11, amp[1016]); 
  FFV1_0(w[81], w[90], w[151], pars->GC_11, amp[1017]); 
  FFV1_0(w[81], w[88], w[200], pars->GC_11, amp[1018]); 
  FFV1_0(w[85], w[88], w[151], pars->GC_11, amp[1019]); 
  FFV1_0(w[198], w[148], w[92], pars->GC_11, amp[1020]); 
  FFV1_0(w[93], w[80], w[151], pars->GC_11, amp[1021]); 
  FFV1_0(w[91], w[80], w[200], pars->GC_11, amp[1022]); 
  FFV1_0(w[91], w[84], w[151], pars->GC_11, amp[1023]); 
  FFV1_0(w[201], w[199], w[86], pars->GC_11, amp[1024]); 
  FFV2_3_0(w[188], w[199], w[2], pars->GC_50, pars->GC_58, amp[1025]); 
  FFV1_0(w[201], w[148], w[94], pars->GC_11, amp[1026]); 
  FFV1_0(w[81], w[95], w[151], pars->GC_11, amp[1027]); 
  FFV1_0(w[201], w[148], w[96], pars->GC_11, amp[1028]); 
  FFV1_0(w[97], w[80], w[151], pars->GC_11, amp[1029]); 
  FFV2_3_0(w[201], w[189], w[3], pars->GC_50, pars->GC_58, amp[1030]); 
  FFV2_3_0(w[198], w[189], w[2], pars->GC_50, pars->GC_58, amp[1031]); 
  FFV1_0(w[81], w[84], w[203], pars->GC_11, amp[1032]); 
  FFV1_0(w[85], w[80], w[203], pars->GC_11, amp[1033]); 
  FFV1_0(w[202], w[204], w[86], pars->GC_11, amp[1034]); 
  FFV2_3_0(w[202], w[190], w[3], pars->GC_50, pars->GC_58, amp[1035]); 
  FFV1_0(w[81], w[88], w[206], pars->GC_11, amp[1036]); 
  FFV1_0(w[91], w[80], w[206], pars->GC_11, amp[1037]); 
  FFV1_0(w[205], w[207], w[86], pars->GC_11, amp[1038]); 
  FFV2_3_0(w[205], w[190], w[2], pars->GC_50, pars->GC_58, amp[1039]); 
  FFV1_0(w[81], w[82], w[168], pars->GC_11, amp[1040]); 
  FFV1_0(w[83], w[80], w[168], pars->GC_11, amp[1041]); 
  FFV1_0(w[81], w[90], w[168], pars->GC_11, amp[1042]); 
  FFV1_0(w[149], w[204], w[89], pars->GC_11, amp[1043]); 
  FFV1_0(w[85], w[88], w[168], pars->GC_11, amp[1044]); 
  FFV1_0(w[93], w[80], w[168], pars->GC_11, amp[1045]); 
  FFV1_0(w[149], w[204], w[92], pars->GC_11, amp[1046]); 
  FFV1_0(w[91], w[84], w[168], pars->GC_11, amp[1047]); 
  FFV1_0(w[81], w[95], w[168], pars->GC_11, amp[1048]); 
  FFV1_0(w[149], w[207], w[94], pars->GC_11, amp[1049]); 
  FFV1_0(w[97], w[80], w[168], pars->GC_11, amp[1050]); 
  FFV1_0(w[149], w[207], w[96], pars->GC_11, amp[1051]); 
  FFV2_3_0(w[191], w[207], w[3], pars->GC_50, pars->GC_58, amp[1052]); 
  FFV2_3_0(w[191], w[204], w[2], pars->GC_50, pars->GC_58, amp[1053]); 
  FFV1_0(w[81], w[102], w[208], pars->GC_11, amp[1054]); 
  FFV1_0(w[209], w[148], w[103], pars->GC_11, amp[1055]); 
  FFV1_0(w[202], w[199], w[103], pars->GC_11, amp[1056]); 
  FFV1_0(w[202], w[148], w[104], pars->GC_11, amp[1057]); 
  FFV1_0(w[81], w[105], w[210], pars->GC_11, amp[1058]); 
  FFV1_0(w[211], w[148], w[103], pars->GC_11, amp[1059]); 
  FFV1_0(w[205], w[196], w[103], pars->GC_11, amp[1060]); 
  FFV1_0(w[205], w[148], w[106], pars->GC_11, amp[1061]); 
  FFV2_3_0(w[192], w[105], w[3], pars->GC_50, pars->GC_58, amp[1062]); 
  FFV2_3_0(w[192], w[102], w[2], pars->GC_50, pars->GC_58, amp[1063]); 
  FFS4_0(w[81], w[193], w[8], pars->GC_83, amp[1064]); 
  FFV1_0(w[81], w[109], w[174], pars->GC_11, amp[1065]); 
  FFV2_3_0(w[91], w[193], w[3], pars->GC_50, pars->GC_58, amp[1066]); 
  FFV1_0(w[91], w[102], w[174], pars->GC_11, amp[1067]); 
  FFV2_3_0(w[85], w[193], w[2], pars->GC_50, pars->GC_58, amp[1068]); 
  FFV1_0(w[85], w[105], w[174], pars->GC_11, amp[1069]); 
  FFV1_0(w[81], w[102], w[212], pars->GC_11, amp[1070]); 
  FFV1_0(w[149], w[213], w[103], pars->GC_11, amp[1071]); 
  FFV1_0(w[149], w[196], w[104], pars->GC_11, amp[1072]); 
  FFV1_0(w[149], w[199], w[106], pars->GC_11, amp[1073]); 
  FFV1_0(w[81], w[105], w[214], pars->GC_11, amp[1074]); 
  FFV1_0(w[149], w[215], w[103], pars->GC_11, amp[1075]); 
  FFV1_0(w[111], w[80], w[208], pars->GC_11, amp[1076]); 
  FFV1_0(w[209], w[148], w[112], pars->GC_11, amp[1077]); 
  FFV1_0(w[202], w[199], w[112], pars->GC_11, amp[1078]); 
  FFV1_0(w[202], w[148], w[113], pars->GC_11, amp[1079]); 
  FFV1_0(w[114], w[80], w[210], pars->GC_11, amp[1080]); 
  FFV1_0(w[211], w[148], w[112], pars->GC_11, amp[1081]); 
  FFV1_0(w[205], w[196], w[112], pars->GC_11, amp[1082]); 
  FFV1_0(w[205], w[148], w[115], pars->GC_11, amp[1083]); 
  FFV2_3_0(w[114], w[194], w[3], pars->GC_50, pars->GC_58, amp[1084]); 
  FFV2_3_0(w[111], w[194], w[2], pars->GC_50, pars->GC_58, amp[1085]); 
  FFS4_0(w[195], w[80], w[8], pars->GC_83, amp[1086]); 
  FFV1_0(w[118], w[80], w[174], pars->GC_11, amp[1087]); 
  FFV2_3_0(w[195], w[88], w[3], pars->GC_50, pars->GC_58, amp[1088]); 
  FFV1_0(w[111], w[88], w[174], pars->GC_11, amp[1089]); 
  FFV2_3_0(w[195], w[84], w[2], pars->GC_50, pars->GC_58, amp[1090]); 
  FFV1_0(w[114], w[84], w[174], pars->GC_11, amp[1091]); 
  FFV1_0(w[111], w[80], w[212], pars->GC_11, amp[1092]); 
  FFV1_0(w[149], w[213], w[112], pars->GC_11, amp[1093]); 
  FFV1_0(w[149], w[196], w[113], pars->GC_11, amp[1094]); 
  FFV1_0(w[149], w[199], w[115], pars->GC_11, amp[1095]); 
  FFV1_0(w[114], w[80], w[214], pars->GC_11, amp[1096]); 
  FFV1_0(w[149], w[215], w[112], pars->GC_11, amp[1097]); 
  FFV1_0(w[216], w[199], w[86], pars->GC_11, amp[1098]); 
  FFV1_0(w[202], w[217], w[86], pars->GC_11, amp[1099]); 
  FFV1_0(w[202], w[199], w[119], pars->GC_11, amp[1100]); 
  FFV1_0(w[216], w[148], w[94], pars->GC_11, amp[1101]); 
  FFV1_0(w[81], w[120], w[208], pars->GC_11, amp[1102]); 
  VVV1_0(w[4], w[208], w[94], pars->GC_10, amp[1103]); 
  FFV1_0(w[216], w[148], w[96], pars->GC_11, amp[1104]); 
  FFV1_0(w[121], w[80], w[208], pars->GC_11, amp[1105]); 
  VVV1_0(w[4], w[208], w[96], pars->GC_10, amp[1106]); 
  FFV2_3_0(w[216], w[189], w[3], pars->GC_50, pars->GC_58, amp[1107]); 
  FFV1_0(w[209], w[148], w[119], pars->GC_11, amp[1108]); 
  FFV1_0(w[209], w[189], w[4], pars->GC_11, amp[1109]); 
  FFV1_0(w[218], w[196], w[86], pars->GC_11, amp[1110]); 
  FFV1_0(w[205], w[219], w[86], pars->GC_11, amp[1111]); 
  FFV1_0(w[205], w[196], w[119], pars->GC_11, amp[1112]); 
  FFV1_0(w[218], w[148], w[89], pars->GC_11, amp[1113]); 
  FFV1_0(w[81], w[122], w[210], pars->GC_11, amp[1114]); 
  VVV1_0(w[4], w[210], w[89], pars->GC_10, amp[1115]); 
  FFV1_0(w[218], w[148], w[92], pars->GC_11, amp[1116]); 
  FFV1_0(w[123], w[80], w[210], pars->GC_11, amp[1117]); 
  VVV1_0(w[4], w[210], w[92], pars->GC_10, amp[1118]); 
  FFV2_3_0(w[218], w[189], w[2], pars->GC_50, pars->GC_58, amp[1119]); 
  FFV1_0(w[211], w[148], w[119], pars->GC_11, amp[1120]); 
  FFV1_0(w[211], w[189], w[4], pars->GC_11, amp[1121]); 
  FFV1_0(w[81], w[82], w[187], pars->GC_11, amp[1122]); 
  FFV1_0(w[83], w[80], w[187], pars->GC_11, amp[1123]); 
  FFV1_0(w[83], w[194], w[4], pars->GC_11, amp[1124]); 
  FFV1_0(w[192], w[82], w[4], pars->GC_11, amp[1125]); 
  FFV1_0(w[81], w[90], w[187], pars->GC_11, amp[1126]); 
  FFV2_3_0(w[192], w[122], w[3], pars->GC_50, pars->GC_58, amp[1127]); 
  FFV1_0(w[192], w[90], w[4], pars->GC_11, amp[1128]); 
  FFV1_0(w[85], w[88], w[187], pars->GC_11, amp[1129]); 
  FFV1_0(w[85], w[122], w[174], pars->GC_11, amp[1130]); 
  FFV1_0(w[121], w[88], w[174], pars->GC_11, amp[1131]); 
  FFV1_0(w[93], w[80], w[187], pars->GC_11, amp[1132]); 
  FFV2_3_0(w[123], w[194], w[3], pars->GC_50, pars->GC_58, amp[1133]); 
  FFV1_0(w[93], w[194], w[4], pars->GC_11, amp[1134]); 
  FFV1_0(w[91], w[84], w[187], pars->GC_11, amp[1135]); 
  FFV1_0(w[123], w[84], w[174], pars->GC_11, amp[1136]); 
  FFV1_0(w[91], w[120], w[174], pars->GC_11, amp[1137]); 
  FFV1_0(w[81], w[95], w[187], pars->GC_11, amp[1138]); 
  FFV2_3_0(w[192], w[120], w[2], pars->GC_50, pars->GC_58, amp[1139]); 
  FFV1_0(w[192], w[95], w[4], pars->GC_11, amp[1140]); 
  FFV1_0(w[97], w[80], w[187], pars->GC_11, amp[1141]); 
  FFV2_3_0(w[121], w[194], w[2], pars->GC_50, pars->GC_58, amp[1142]); 
  FFV1_0(w[97], w[194], w[4], pars->GC_11, amp[1143]); 
  FFV1_0(w[149], w[219], w[94], pars->GC_11, amp[1144]); 
  FFV1_0(w[81], w[120], w[212], pars->GC_11, amp[1145]); 
  VVV1_0(w[4], w[212], w[94], pars->GC_10, amp[1146]); 
  FFV1_0(w[149], w[219], w[96], pars->GC_11, amp[1147]); 
  FFV1_0(w[121], w[80], w[212], pars->GC_11, amp[1148]); 
  VVV1_0(w[4], w[212], w[96], pars->GC_10, amp[1149]); 
  FFV2_3_0(w[191], w[219], w[3], pars->GC_50, pars->GC_58, amp[1150]); 
  FFV1_0(w[149], w[213], w[119], pars->GC_11, amp[1151]); 
  FFV1_0(w[191], w[213], w[4], pars->GC_11, amp[1152]); 
  FFV1_0(w[81], w[122], w[214], pars->GC_11, amp[1153]); 
  FFV1_0(w[149], w[217], w[89], pars->GC_11, amp[1154]); 
  VVV1_0(w[4], w[214], w[89], pars->GC_10, amp[1155]); 
  FFV1_0(w[123], w[80], w[214], pars->GC_11, amp[1156]); 
  FFV1_0(w[149], w[217], w[92], pars->GC_11, amp[1157]); 
  VVV1_0(w[4], w[214], w[92], pars->GC_10, amp[1158]); 
  FFV2_3_0(w[191], w[217], w[2], pars->GC_50, pars->GC_58, amp[1159]); 
  FFV1_0(w[149], w[215], w[119], pars->GC_11, amp[1160]); 
  FFV1_0(w[191], w[215], w[4], pars->GC_11, amp[1161]); 


}
double PY8MEs_R12_P41_sm_qb_zzgqb::matrix_12_ub_zzgub() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 152;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[0] + 1./3. * amp[1] + 1./3. * amp[2] + 1./3.
      * amp[3] + 1./3. * amp[4] + 1./3. * amp[5] + 1./3. * amp[6] + 1./3. *
      amp[7] + 1./3. * amp[8] + 1./3. * amp[9] + 1./3. * amp[10] + 1./3. *
      amp[11] + 1./3. * amp[12] + 1./3. * amp[13] + 1./3. * amp[14] + 1./3. *
      amp[15] + 1./3. * amp[16] + 1./3. * amp[17] + 1./3. * amp[18] + 1./3. *
      amp[19] + 1./3. * amp[20] + 1./3. * amp[21] + 1./3. * amp[22] + 1./3. *
      amp[23] + 1./3. * amp[24] + 1./3. * amp[25] + 1./3. * amp[26] + 1./3. *
      amp[27] + 1./3. * amp[28] + 1./3. * amp[29] + 1./3. * amp[30] + 1./3. *
      amp[31] + 1./3. * amp[32] + 1./3. * amp[33] + 1./3. * amp[34] + 1./3. *
      amp[35] + 1./3. * amp[36] + 1./3. * amp[37] + 1./3. * amp[38] + 1./3. *
      amp[39] + 1./3. * amp[40] + 1./3. * amp[41] + 1./3. * amp[42] + 1./3. *
      amp[43] + 1./3. * amp[88] + 1./3. * amp[89] + 1./3. * amp[91] + 1./3. *
      amp[94] + 1./3. * amp[97] + 1./3. * amp[99] + 1./3. * amp[100] + 1./3. *
      amp[101] + 1./3. * amp[103] + 1./3. * amp[106] + 1./3. * amp[109] + 1./3.
      * amp[111] + 1./3. * amp[134] + 1./3. * amp[137] + 1./3. * amp[140] +
      1./3. * amp[142] + 1./3. * amp[144] + 1./3. * amp[147] + 1./3. * amp[149]
      + 1./3. * amp[151]);
  jamp[1] = +1./2. * (-amp[22] - amp[23] - amp[24] - amp[25] - amp[26] -
      amp[27] - amp[28] - amp[29] - amp[30] - amp[31] - amp[32] - amp[33] -
      amp[34] - amp[35] - amp[36] - amp[37] - amp[38] - amp[39] - amp[40] -
      amp[41] - amp[42] - amp[43] - amp[66] - amp[67] - amp[68] - amp[69] -
      amp[70] - amp[71] - amp[72] - amp[73] - amp[74] - amp[75] - amp[76] -
      amp[77] - amp[78] - amp[79] - amp[80] - amp[81] - amp[82] - amp[83] -
      amp[84] - amp[85] - amp[86] - amp[87] - amp[89] + Complex<double> (0, 1)
      * amp[90] - Complex<double> (0, 1) * amp[93] - amp[95] - Complex<double>
      (0, 1) * amp[96] + Complex<double> (0, 1) * amp[98] - amp[101] +
      Complex<double> (0, 1) * amp[102] - Complex<double> (0, 1) * amp[105] -
      amp[107] - Complex<double> (0, 1) * amp[108] + Complex<double> (0, 1) *
      amp[110] - Complex<double> (0, 1) * amp[112] - Complex<double> (0, 1) *
      amp[113] - amp[114] - Complex<double> (0, 1) * amp[116] - Complex<double>
      (0, 1) * amp[119] - amp[121] - Complex<double> (0, 1) * amp[122] -
      amp[123] - amp[124] - Complex<double> (0, 1) * amp[125] - amp[126] -
      Complex<double> (0, 1) * amp[128] - Complex<double> (0, 1) * amp[131] -
      amp[132] - amp[133] - amp[134] - Complex<double> (0, 1) * amp[136] -
      amp[137] - amp[138] - Complex<double> (0, 1) * amp[139] - amp[140] +
      Complex<double> (0, 1) * amp[141] - amp[142] - amp[144] - Complex<double>
      (0, 1) * amp[145] - amp[146] - amp[147] - Complex<double> (0, 1) *
      amp[148] - amp[149] + Complex<double> (0, 1) * amp[150] - amp[151]);
  jamp[2] = +1./2. * (-amp[0] - amp[1] - amp[2] - amp[3] - amp[4] - amp[5] -
      amp[6] - amp[7] - amp[8] - amp[9] - amp[10] - amp[11] - amp[12] - amp[13]
      - amp[14] - amp[15] - amp[16] - amp[17] - amp[18] - amp[19] - amp[20] -
      amp[21] - amp[44] - amp[45] - amp[46] - amp[47] - amp[48] - amp[49] -
      amp[50] - amp[51] - amp[52] - amp[53] - amp[54] - amp[55] - amp[56] -
      amp[57] - amp[58] - amp[59] - amp[60] - amp[61] - amp[62] - amp[63] -
      amp[64] - amp[65] - amp[88] - Complex<double> (0, 1) * amp[90] - amp[91]
      - amp[92] + Complex<double> (0, 1) * amp[93] - amp[94] + Complex<double>
      (0, 1) * amp[96] - amp[97] - Complex<double> (0, 1) * amp[98] - amp[99] -
      amp[100] - Complex<double> (0, 1) * amp[102] - amp[103] - amp[104] +
      Complex<double> (0, 1) * amp[105] - amp[106] + Complex<double> (0, 1) *
      amp[108] - amp[109] - Complex<double> (0, 1) * amp[110] - amp[111] +
      Complex<double> (0, 1) * amp[112] + Complex<double> (0, 1) * amp[113] -
      amp[115] + Complex<double> (0, 1) * amp[116] - amp[117] - amp[118] +
      Complex<double> (0, 1) * amp[119] - amp[120] + Complex<double> (0, 1) *
      amp[122] + Complex<double> (0, 1) * amp[125] - amp[127] + Complex<double>
      (0, 1) * amp[128] - amp[129] - amp[130] + Complex<double> (0, 1) *
      amp[131] - amp[135] + Complex<double> (0, 1) * amp[136] + Complex<double>
      (0, 1) * amp[139] - Complex<double> (0, 1) * amp[141] - amp[143] +
      Complex<double> (0, 1) * amp[145] + Complex<double> (0, 1) * amp[148] -
      Complex<double> (0, 1) * amp[150]);
  jamp[3] = +1./2. * (+1./3. * amp[44] + 1./3. * amp[45] + 1./3. * amp[46] +
      1./3. * amp[47] + 1./3. * amp[48] + 1./3. * amp[49] + 1./3. * amp[50] +
      1./3. * amp[51] + 1./3. * amp[52] + 1./3. * amp[53] + 1./3. * amp[54] +
      1./3. * amp[55] + 1./3. * amp[56] + 1./3. * amp[57] + 1./3. * amp[58] +
      1./3. * amp[59] + 1./3. * amp[60] + 1./3. * amp[61] + 1./3. * amp[62] +
      1./3. * amp[63] + 1./3. * amp[64] + 1./3. * amp[65] + 1./3. * amp[66] +
      1./3. * amp[67] + 1./3. * amp[68] + 1./3. * amp[69] + 1./3. * amp[70] +
      1./3. * amp[71] + 1./3. * amp[72] + 1./3. * amp[73] + 1./3. * amp[74] +
      1./3. * amp[75] + 1./3. * amp[76] + 1./3. * amp[77] + 1./3. * amp[78] +
      1./3. * amp[79] + 1./3. * amp[80] + 1./3. * amp[81] + 1./3. * amp[82] +
      1./3. * amp[83] + 1./3. * amp[84] + 1./3. * amp[85] + 1./3. * amp[86] +
      1./3. * amp[87] + 1./3. * amp[92] + 1./3. * amp[95] + 1./3. * amp[104] +
      1./3. * amp[107] + 1./3. * amp[114] + 1./3. * amp[115] + 1./3. * amp[117]
      + 1./3. * amp[118] + 1./3. * amp[120] + 1./3. * amp[121] + 1./3. *
      amp[123] + 1./3. * amp[124] + 1./3. * amp[126] + 1./3. * amp[127] + 1./3.
      * amp[129] + 1./3. * amp[130] + 1./3. * amp[132] + 1./3. * amp[133] +
      1./3. * amp[135] + 1./3. * amp[138] + 1./3. * amp[143] + 1./3. *
      amp[146]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[0][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P41_sm_qb_zzgqb::matrix_12_ubx_zzgubx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 152;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[174] + amp[175] + amp[176] + amp[177] + amp[178] +
      amp[179] + amp[180] + amp[181] + amp[182] + amp[183] + amp[184] +
      amp[185] + amp[186] + amp[187] + amp[188] + amp[189] + amp[190] +
      amp[191] + amp[192] + amp[193] + amp[194] + amp[195] + amp[218] +
      amp[219] + amp[220] + amp[221] + amp[222] + amp[223] + amp[224] +
      amp[225] + amp[226] + amp[227] + amp[228] + amp[229] + amp[230] +
      amp[231] + amp[232] + amp[233] + amp[234] + amp[235] + amp[236] +
      amp[237] + amp[238] + amp[239] + amp[241] - Complex<double> (0, 1) *
      amp[242] + Complex<double> (0, 1) * amp[245] + amp[247] + Complex<double>
      (0, 1) * amp[248] - Complex<double> (0, 1) * amp[250] + amp[253] -
      Complex<double> (0, 1) * amp[254] + Complex<double> (0, 1) * amp[257] +
      amp[259] + Complex<double> (0, 1) * amp[260] - Complex<double> (0, 1) *
      amp[262] + Complex<double> (0, 1) * amp[264] + Complex<double> (0, 1) *
      amp[265] + amp[266] + Complex<double> (0, 1) * amp[268] + Complex<double>
      (0, 1) * amp[271] + amp[273] + Complex<double> (0, 1) * amp[274] +
      amp[275] + amp[276] + Complex<double> (0, 1) * amp[277] + amp[278] +
      Complex<double> (0, 1) * amp[280] + Complex<double> (0, 1) * amp[283] +
      amp[284] + amp[285] + amp[286] + Complex<double> (0, 1) * amp[288] +
      amp[289] + amp[290] + Complex<double> (0, 1) * amp[291] + amp[292] -
      Complex<double> (0, 1) * amp[293] + amp[294] + amp[296] + Complex<double>
      (0, 1) * amp[297] + amp[298] + amp[299] + Complex<double> (0, 1) *
      amp[300] + amp[301] - Complex<double> (0, 1) * amp[302] + amp[303]);
  jamp[1] = +1./2. * (-1./3. * amp[152] - 1./3. * amp[153] - 1./3. * amp[154] -
      1./3. * amp[155] - 1./3. * amp[156] - 1./3. * amp[157] - 1./3. * amp[158]
      - 1./3. * amp[159] - 1./3. * amp[160] - 1./3. * amp[161] - 1./3. *
      amp[162] - 1./3. * amp[163] - 1./3. * amp[164] - 1./3. * amp[165] - 1./3.
      * amp[166] - 1./3. * amp[167] - 1./3. * amp[168] - 1./3. * amp[169] -
      1./3. * amp[170] - 1./3. * amp[171] - 1./3. * amp[172] - 1./3. * amp[173]
      - 1./3. * amp[174] - 1./3. * amp[175] - 1./3. * amp[176] - 1./3. *
      amp[177] - 1./3. * amp[178] - 1./3. * amp[179] - 1./3. * amp[180] - 1./3.
      * amp[181] - 1./3. * amp[182] - 1./3. * amp[183] - 1./3. * amp[184] -
      1./3. * amp[185] - 1./3. * amp[186] - 1./3. * amp[187] - 1./3. * amp[188]
      - 1./3. * amp[189] - 1./3. * amp[190] - 1./3. * amp[191] - 1./3. *
      amp[192] - 1./3. * amp[193] - 1./3. * amp[194] - 1./3. * amp[195] - 1./3.
      * amp[240] - 1./3. * amp[241] - 1./3. * amp[243] - 1./3. * amp[246] -
      1./3. * amp[249] - 1./3. * amp[251] - 1./3. * amp[252] - 1./3. * amp[253]
      - 1./3. * amp[255] - 1./3. * amp[258] - 1./3. * amp[261] - 1./3. *
      amp[263] - 1./3. * amp[286] - 1./3. * amp[289] - 1./3. * amp[292] - 1./3.
      * amp[294] - 1./3. * amp[296] - 1./3. * amp[299] - 1./3. * amp[301] -
      1./3. * amp[303]);
  jamp[2] = +1./2. * (+amp[152] + amp[153] + amp[154] + amp[155] + amp[156] +
      amp[157] + amp[158] + amp[159] + amp[160] + amp[161] + amp[162] +
      amp[163] + amp[164] + amp[165] + amp[166] + amp[167] + amp[168] +
      amp[169] + amp[170] + amp[171] + amp[172] + amp[173] + amp[196] +
      amp[197] + amp[198] + amp[199] + amp[200] + amp[201] + amp[202] +
      amp[203] + amp[204] + amp[205] + amp[206] + amp[207] + amp[208] +
      amp[209] + amp[210] + amp[211] + amp[212] + amp[213] + amp[214] +
      amp[215] + amp[216] + amp[217] + amp[240] + Complex<double> (0, 1) *
      amp[242] + amp[243] + amp[244] - Complex<double> (0, 1) * amp[245] +
      amp[246] - Complex<double> (0, 1) * amp[248] + amp[249] + Complex<double>
      (0, 1) * amp[250] + amp[251] + amp[252] + Complex<double> (0, 1) *
      amp[254] + amp[255] + amp[256] - Complex<double> (0, 1) * amp[257] +
      amp[258] - Complex<double> (0, 1) * amp[260] + amp[261] + Complex<double>
      (0, 1) * amp[262] + amp[263] - Complex<double> (0, 1) * amp[264] -
      Complex<double> (0, 1) * amp[265] + amp[267] - Complex<double> (0, 1) *
      amp[268] + amp[269] + amp[270] - Complex<double> (0, 1) * amp[271] +
      amp[272] - Complex<double> (0, 1) * amp[274] - Complex<double> (0, 1) *
      amp[277] + amp[279] - Complex<double> (0, 1) * amp[280] + amp[281] +
      amp[282] - Complex<double> (0, 1) * amp[283] + amp[287] - Complex<double>
      (0, 1) * amp[288] - Complex<double> (0, 1) * amp[291] + Complex<double>
      (0, 1) * amp[293] + amp[295] - Complex<double> (0, 1) * amp[297] -
      Complex<double> (0, 1) * amp[300] + Complex<double> (0, 1) * amp[302]);
  jamp[3] = +1./2. * (-1./3. * amp[196] - 1./3. * amp[197] - 1./3. * amp[198] -
      1./3. * amp[199] - 1./3. * amp[200] - 1./3. * amp[201] - 1./3. * amp[202]
      - 1./3. * amp[203] - 1./3. * amp[204] - 1./3. * amp[205] - 1./3. *
      amp[206] - 1./3. * amp[207] - 1./3. * amp[208] - 1./3. * amp[209] - 1./3.
      * amp[210] - 1./3. * amp[211] - 1./3. * amp[212] - 1./3. * amp[213] -
      1./3. * amp[214] - 1./3. * amp[215] - 1./3. * amp[216] - 1./3. * amp[217]
      - 1./3. * amp[218] - 1./3. * amp[219] - 1./3. * amp[220] - 1./3. *
      amp[221] - 1./3. * amp[222] - 1./3. * amp[223] - 1./3. * amp[224] - 1./3.
      * amp[225] - 1./3. * amp[226] - 1./3. * amp[227] - 1./3. * amp[228] -
      1./3. * amp[229] - 1./3. * amp[230] - 1./3. * amp[231] - 1./3. * amp[232]
      - 1./3. * amp[233] - 1./3. * amp[234] - 1./3. * amp[235] - 1./3. *
      amp[236] - 1./3. * amp[237] - 1./3. * amp[238] - 1./3. * amp[239] - 1./3.
      * amp[244] - 1./3. * amp[247] - 1./3. * amp[256] - 1./3. * amp[259] -
      1./3. * amp[266] - 1./3. * amp[267] - 1./3. * amp[269] - 1./3. * amp[270]
      - 1./3. * amp[272] - 1./3. * amp[273] - 1./3. * amp[275] - 1./3. *
      amp[276] - 1./3. * amp[278] - 1./3. * amp[279] - 1./3. * amp[281] - 1./3.
      * amp[282] - 1./3. * amp[284] - 1./3. * amp[285] - 1./3. * amp[287] -
      1./3. * amp[290] - 1./3. * amp[295] - 1./3. * amp[298]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[1][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P41_sm_qb_zzgqb::matrix_12_db_zzgdb() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 152;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[0] + 1./3. * amp[1] + 1./3. * amp[304] +
      1./3. * amp[305] + 1./3. * amp[306] + 1./3. * amp[307] + 1./3. * amp[308]
      + 1./3. * amp[7] + 1./3. * amp[309] + 1./3. * amp[9] + 1./3. * amp[310] +
      1./3. * amp[11] + 1./3. * amp[311] + 1./3. * amp[13] + 1./3. * amp[312] +
      1./3. * amp[313] + 1./3. * amp[314] + 1./3. * amp[17] + 1./3. * amp[315]
      + 1./3. * amp[19] + 1./3. * amp[316] + 1./3. * amp[317] + 1./3. *
      amp[318] + 1./3. * amp[319] + 1./3. * amp[320] + 1./3. * amp[321] + 1./3.
      * amp[322] + 1./3. * amp[323] + 1./3. * amp[324] + 1./3. * amp[325] +
      1./3. * amp[30] + 1./3. * amp[31] + 1./3. * amp[32] + 1./3. * amp[326] +
      1./3. * amp[34] + 1./3. * amp[35] + 1./3. * amp[327] + 1./3. * amp[37] +
      1./3. * amp[38] + 1./3. * amp[328] + 1./3. * amp[40] + 1./3. * amp[329] +
      1./3. * amp[330] + 1./3. * amp[331] + 1./3. * amp[360] + 1./3. * amp[361]
      + 1./3. * amp[363] + 1./3. * amp[366] + 1./3. * amp[369] + 1./3. *
      amp[371] + 1./3. * amp[372] + 1./3. * amp[373] + 1./3. * amp[375] + 1./3.
      * amp[378] + 1./3. * amp[381] + 1./3. * amp[383] + 1./3. * amp[384] +
      1./3. * amp[387] + 1./3. * amp[390] + 1./3. * amp[392] + 1./3. * amp[394]
      + 1./3. * amp[397] + 1./3. * amp[399] + 1./3. * amp[401]);
  jamp[1] = +1./2. * (-amp[318] - amp[319] - amp[320] - amp[321] - amp[322] -
      amp[323] - amp[324] - amp[325] - amp[30] - amp[31] - amp[32] - amp[326] -
      amp[34] - amp[35] - amp[327] - amp[37] - amp[38] - amp[328] - amp[40] -
      amp[329] - amp[330] - amp[331] - amp[346] - amp[347] - amp[348] -
      amp[349] - amp[350] - amp[351] - amp[352] - amp[353] - amp[74] - amp[75]
      - amp[76] - amp[77] - amp[78] - amp[79] - amp[80] - amp[81] - amp[354] -
      amp[355] - amp[356] - amp[357] - amp[358] - amp[359] - amp[361] +
      Complex<double> (0, 1) * amp[362] - Complex<double> (0, 1) * amp[365] -
      amp[367] - Complex<double> (0, 1) * amp[368] + Complex<double> (0, 1) *
      amp[370] - amp[373] + Complex<double> (0, 1) * amp[374] - Complex<double>
      (0, 1) * amp[377] - amp[379] - Complex<double> (0, 1) * amp[380] +
      Complex<double> (0, 1) * amp[382] - Complex<double> (0, 1) * amp[112] -
      Complex<double> (0, 1) * amp[113] - amp[114] - Complex<double> (0, 1) *
      amp[116] - Complex<double> (0, 1) * amp[119] - amp[121] - Complex<double>
      (0, 1) * amp[122] - amp[123] - amp[124] - Complex<double> (0, 1) *
      amp[125] - amp[126] - Complex<double> (0, 1) * amp[128] - Complex<double>
      (0, 1) * amp[131] - amp[132] - amp[133] - amp[384] - Complex<double> (0,
      1) * amp[386] - amp[387] - amp[388] - Complex<double> (0, 1) * amp[389] -
      amp[390] + Complex<double> (0, 1) * amp[391] - amp[392] - amp[394] -
      Complex<double> (0, 1) * amp[395] - amp[396] - amp[397] - Complex<double>
      (0, 1) * amp[398] - amp[399] + Complex<double> (0, 1) * amp[400] -
      amp[401]);
  jamp[2] = +1./2. * (-amp[0] - amp[1] - amp[304] - amp[305] - amp[306] -
      amp[307] - amp[308] - amp[7] - amp[309] - amp[9] - amp[310] - amp[11] -
      amp[311] - amp[13] - amp[312] - amp[313] - amp[314] - amp[17] - amp[315]
      - amp[19] - amp[316] - amp[317] - amp[332] - amp[333] - amp[334] -
      amp[335] - amp[336] - amp[337] - amp[338] - amp[339] - amp[52] - amp[53]
      - amp[54] - amp[55] - amp[56] - amp[57] - amp[58] - amp[59] - amp[340] -
      amp[341] - amp[342] - amp[343] - amp[344] - amp[345] - amp[360] -
      Complex<double> (0, 1) * amp[362] - amp[363] - amp[364] + Complex<double>
      (0, 1) * amp[365] - amp[366] + Complex<double> (0, 1) * amp[368] -
      amp[369] - Complex<double> (0, 1) * amp[370] - amp[371] - amp[372] -
      Complex<double> (0, 1) * amp[374] - amp[375] - amp[376] + Complex<double>
      (0, 1) * amp[377] - amp[378] + Complex<double> (0, 1) * amp[380] -
      amp[381] - Complex<double> (0, 1) * amp[382] - amp[383] + Complex<double>
      (0, 1) * amp[112] + Complex<double> (0, 1) * amp[113] - amp[115] +
      Complex<double> (0, 1) * amp[116] - amp[117] - amp[118] + Complex<double>
      (0, 1) * amp[119] - amp[120] + Complex<double> (0, 1) * amp[122] +
      Complex<double> (0, 1) * amp[125] - amp[127] + Complex<double> (0, 1) *
      amp[128] - amp[129] - amp[130] + Complex<double> (0, 1) * amp[131] -
      amp[385] + Complex<double> (0, 1) * amp[386] + Complex<double> (0, 1) *
      amp[389] - Complex<double> (0, 1) * amp[391] - amp[393] + Complex<double>
      (0, 1) * amp[395] + Complex<double> (0, 1) * amp[398] - Complex<double>
      (0, 1) * amp[400]);
  jamp[3] = +1./2. * (+1./3. * amp[332] + 1./3. * amp[333] + 1./3. * amp[334] +
      1./3. * amp[335] + 1./3. * amp[336] + 1./3. * amp[337] + 1./3. * amp[338]
      + 1./3. * amp[339] + 1./3. * amp[52] + 1./3. * amp[53] + 1./3. * amp[54]
      + 1./3. * amp[55] + 1./3. * amp[56] + 1./3. * amp[57] + 1./3. * amp[58] +
      1./3. * amp[59] + 1./3. * amp[340] + 1./3. * amp[341] + 1./3. * amp[342]
      + 1./3. * amp[343] + 1./3. * amp[344] + 1./3. * amp[345] + 1./3. *
      amp[346] + 1./3. * amp[347] + 1./3. * amp[348] + 1./3. * amp[349] + 1./3.
      * amp[350] + 1./3. * amp[351] + 1./3. * amp[352] + 1./3. * amp[353] +
      1./3. * amp[74] + 1./3. * amp[75] + 1./3. * amp[76] + 1./3. * amp[77] +
      1./3. * amp[78] + 1./3. * amp[79] + 1./3. * amp[80] + 1./3. * amp[81] +
      1./3. * amp[354] + 1./3. * amp[355] + 1./3. * amp[356] + 1./3. * amp[357]
      + 1./3. * amp[358] + 1./3. * amp[359] + 1./3. * amp[364] + 1./3. *
      amp[367] + 1./3. * amp[376] + 1./3. * amp[379] + 1./3. * amp[114] + 1./3.
      * amp[115] + 1./3. * amp[117] + 1./3. * amp[118] + 1./3. * amp[120] +
      1./3. * amp[121] + 1./3. * amp[123] + 1./3. * amp[124] + 1./3. * amp[126]
      + 1./3. * amp[127] + 1./3. * amp[129] + 1./3. * amp[130] + 1./3. *
      amp[132] + 1./3. * amp[133] + 1./3. * amp[385] + 1./3. * amp[388] + 1./3.
      * amp[393] + 1./3. * amp[396]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[2][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P41_sm_qb_zzgqb::matrix_12_dbx_zzgdbx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 152;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[424] + amp[425] + amp[426] + amp[427] + amp[428] +
      amp[429] + amp[430] + amp[431] + amp[432] + amp[433] + amp[434] +
      amp[435] + amp[436] + amp[437] + amp[438] + amp[439] + amp[440] +
      amp[441] + amp[442] + amp[443] + amp[444] + amp[445] + amp[468] +
      amp[469] + amp[470] + amp[471] + amp[472] + amp[473] + amp[474] +
      amp[475] + amp[476] + amp[477] + amp[478] + amp[479] + amp[480] +
      amp[481] + amp[482] + amp[483] + amp[484] + amp[485] + amp[486] +
      amp[487] + amp[488] + amp[489] + amp[491] - Complex<double> (0, 1) *
      amp[492] + Complex<double> (0, 1) * amp[495] + amp[497] + Complex<double>
      (0, 1) * amp[498] - Complex<double> (0, 1) * amp[500] + amp[503] -
      Complex<double> (0, 1) * amp[504] + Complex<double> (0, 1) * amp[507] +
      amp[509] + Complex<double> (0, 1) * amp[510] - Complex<double> (0, 1) *
      amp[512] + Complex<double> (0, 1) * amp[514] + Complex<double> (0, 1) *
      amp[515] + amp[516] + Complex<double> (0, 1) * amp[518] + Complex<double>
      (0, 1) * amp[521] + amp[523] + Complex<double> (0, 1) * amp[524] +
      amp[525] + amp[526] + Complex<double> (0, 1) * amp[527] + amp[528] +
      Complex<double> (0, 1) * amp[530] + Complex<double> (0, 1) * amp[533] +
      amp[534] + amp[535] + amp[536] + Complex<double> (0, 1) * amp[538] +
      amp[539] + amp[540] + Complex<double> (0, 1) * amp[541] + amp[542] -
      Complex<double> (0, 1) * amp[543] + amp[544] + amp[546] + Complex<double>
      (0, 1) * amp[547] + amp[548] + amp[549] + Complex<double> (0, 1) *
      amp[550] + amp[551] - Complex<double> (0, 1) * amp[552] + amp[553]);
  jamp[1] = +1./2. * (-1./3. * amp[402] - 1./3. * amp[403] - 1./3. * amp[404] -
      1./3. * amp[405] - 1./3. * amp[406] - 1./3. * amp[407] - 1./3. * amp[408]
      - 1./3. * amp[409] - 1./3. * amp[410] - 1./3. * amp[411] - 1./3. *
      amp[412] - 1./3. * amp[413] - 1./3. * amp[414] - 1./3. * amp[415] - 1./3.
      * amp[416] - 1./3. * amp[417] - 1./3. * amp[418] - 1./3. * amp[419] -
      1./3. * amp[420] - 1./3. * amp[421] - 1./3. * amp[422] - 1./3. * amp[423]
      - 1./3. * amp[424] - 1./3. * amp[425] - 1./3. * amp[426] - 1./3. *
      amp[427] - 1./3. * amp[428] - 1./3. * amp[429] - 1./3. * amp[430] - 1./3.
      * amp[431] - 1./3. * amp[432] - 1./3. * amp[433] - 1./3. * amp[434] -
      1./3. * amp[435] - 1./3. * amp[436] - 1./3. * amp[437] - 1./3. * amp[438]
      - 1./3. * amp[439] - 1./3. * amp[440] - 1./3. * amp[441] - 1./3. *
      amp[442] - 1./3. * amp[443] - 1./3. * amp[444] - 1./3. * amp[445] - 1./3.
      * amp[490] - 1./3. * amp[491] - 1./3. * amp[493] - 1./3. * amp[496] -
      1./3. * amp[499] - 1./3. * amp[501] - 1./3. * amp[502] - 1./3. * amp[503]
      - 1./3. * amp[505] - 1./3. * amp[508] - 1./3. * amp[511] - 1./3. *
      amp[513] - 1./3. * amp[536] - 1./3. * amp[539] - 1./3. * amp[542] - 1./3.
      * amp[544] - 1./3. * amp[546] - 1./3. * amp[549] - 1./3. * amp[551] -
      1./3. * amp[553]);
  jamp[2] = +1./2. * (+amp[402] + amp[403] + amp[404] + amp[405] + amp[406] +
      amp[407] + amp[408] + amp[409] + amp[410] + amp[411] + amp[412] +
      amp[413] + amp[414] + amp[415] + amp[416] + amp[417] + amp[418] +
      amp[419] + amp[420] + amp[421] + amp[422] + amp[423] + amp[446] +
      amp[447] + amp[448] + amp[449] + amp[450] + amp[451] + amp[452] +
      amp[453] + amp[454] + amp[455] + amp[456] + amp[457] + amp[458] +
      amp[459] + amp[460] + amp[461] + amp[462] + amp[463] + amp[464] +
      amp[465] + amp[466] + amp[467] + amp[490] + Complex<double> (0, 1) *
      amp[492] + amp[493] + amp[494] - Complex<double> (0, 1) * amp[495] +
      amp[496] - Complex<double> (0, 1) * amp[498] + amp[499] + Complex<double>
      (0, 1) * amp[500] + amp[501] + amp[502] + Complex<double> (0, 1) *
      amp[504] + amp[505] + amp[506] - Complex<double> (0, 1) * amp[507] +
      amp[508] - Complex<double> (0, 1) * amp[510] + amp[511] + Complex<double>
      (0, 1) * amp[512] + amp[513] - Complex<double> (0, 1) * amp[514] -
      Complex<double> (0, 1) * amp[515] + amp[517] - Complex<double> (0, 1) *
      amp[518] + amp[519] + amp[520] - Complex<double> (0, 1) * amp[521] +
      amp[522] - Complex<double> (0, 1) * amp[524] - Complex<double> (0, 1) *
      amp[527] + amp[529] - Complex<double> (0, 1) * amp[530] + amp[531] +
      amp[532] - Complex<double> (0, 1) * amp[533] + amp[537] - Complex<double>
      (0, 1) * amp[538] - Complex<double> (0, 1) * amp[541] + Complex<double>
      (0, 1) * amp[543] + amp[545] - Complex<double> (0, 1) * amp[547] -
      Complex<double> (0, 1) * amp[550] + Complex<double> (0, 1) * amp[552]);
  jamp[3] = +1./2. * (-1./3. * amp[446] - 1./3. * amp[447] - 1./3. * amp[448] -
      1./3. * amp[449] - 1./3. * amp[450] - 1./3. * amp[451] - 1./3. * amp[452]
      - 1./3. * amp[453] - 1./3. * amp[454] - 1./3. * amp[455] - 1./3. *
      amp[456] - 1./3. * amp[457] - 1./3. * amp[458] - 1./3. * amp[459] - 1./3.
      * amp[460] - 1./3. * amp[461] - 1./3. * amp[462] - 1./3. * amp[463] -
      1./3. * amp[464] - 1./3. * amp[465] - 1./3. * amp[466] - 1./3. * amp[467]
      - 1./3. * amp[468] - 1./3. * amp[469] - 1./3. * amp[470] - 1./3. *
      amp[471] - 1./3. * amp[472] - 1./3. * amp[473] - 1./3. * amp[474] - 1./3.
      * amp[475] - 1./3. * amp[476] - 1./3. * amp[477] - 1./3. * amp[478] -
      1./3. * amp[479] - 1./3. * amp[480] - 1./3. * amp[481] - 1./3. * amp[482]
      - 1./3. * amp[483] - 1./3. * amp[484] - 1./3. * amp[485] - 1./3. *
      amp[486] - 1./3. * amp[487] - 1./3. * amp[488] - 1./3. * amp[489] - 1./3.
      * amp[494] - 1./3. * amp[497] - 1./3. * amp[506] - 1./3. * amp[509] -
      1./3. * amp[516] - 1./3. * amp[517] - 1./3. * amp[519] - 1./3. * amp[520]
      - 1./3. * amp[522] - 1./3. * amp[523] - 1./3. * amp[525] - 1./3. *
      amp[526] - 1./3. * amp[528] - 1./3. * amp[529] - 1./3. * amp[531] - 1./3.
      * amp[532] - 1./3. * amp[534] - 1./3. * amp[535] - 1./3. * amp[537] -
      1./3. * amp[540] - 1./3. * amp[545] - 1./3. * amp[548]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[3][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P41_sm_qb_zzgqb::matrix_12_uxb_zzguxb() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 152;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[554] + amp[555] + amp[556] + amp[557] + amp[558] +
      amp[559] + amp[560] + amp[561] + amp[562] + amp[563] + amp[564] +
      amp[565] + amp[566] + amp[567] + amp[568] + amp[569] + amp[570] +
      amp[571] + amp[572] + amp[573] + amp[574] + amp[575] + amp[598] +
      amp[599] + amp[600] + amp[601] + amp[602] + amp[603] + amp[604] +
      amp[605] + amp[606] + amp[607] + amp[608] + amp[609] + amp[610] +
      amp[611] + amp[612] + amp[613] + amp[614] + amp[615] + amp[616] +
      amp[617] + amp[618] + amp[619] + amp[642] + Complex<double> (0, 1) *
      amp[644] + amp[645] + amp[646] - Complex<double> (0, 1) * amp[647] +
      amp[648] - Complex<double> (0, 1) * amp[650] + amp[651] + Complex<double>
      (0, 1) * amp[652] + amp[653] + amp[654] + Complex<double> (0, 1) *
      amp[656] + amp[657] + amp[658] - Complex<double> (0, 1) * amp[659] +
      amp[660] - Complex<double> (0, 1) * amp[662] + amp[663] + Complex<double>
      (0, 1) * amp[664] + amp[665] - Complex<double> (0, 1) * amp[666] -
      Complex<double> (0, 1) * amp[667] + amp[669] - Complex<double> (0, 1) *
      amp[670] + amp[671] + amp[672] - Complex<double> (0, 1) * amp[673] +
      amp[674] - Complex<double> (0, 1) * amp[676] - Complex<double> (0, 1) *
      amp[679] + amp[681] - Complex<double> (0, 1) * amp[682] + amp[683] +
      amp[684] - Complex<double> (0, 1) * amp[685] + amp[689] - Complex<double>
      (0, 1) * amp[690] - Complex<double> (0, 1) * amp[693] + Complex<double>
      (0, 1) * amp[695] + amp[697] - Complex<double> (0, 1) * amp[699] -
      Complex<double> (0, 1) * amp[702] + Complex<double> (0, 1) * amp[704]);
  jamp[1] = +1./2. * (-1./3. * amp[598] - 1./3. * amp[599] - 1./3. * amp[600] -
      1./3. * amp[601] - 1./3. * amp[602] - 1./3. * amp[603] - 1./3. * amp[604]
      - 1./3. * amp[605] - 1./3. * amp[606] - 1./3. * amp[607] - 1./3. *
      amp[608] - 1./3. * amp[609] - 1./3. * amp[610] - 1./3. * amp[611] - 1./3.
      * amp[612] - 1./3. * amp[613] - 1./3. * amp[614] - 1./3. * amp[615] -
      1./3. * amp[616] - 1./3. * amp[617] - 1./3. * amp[618] - 1./3. * amp[619]
      - 1./3. * amp[620] - 1./3. * amp[621] - 1./3. * amp[622] - 1./3. *
      amp[623] - 1./3. * amp[624] - 1./3. * amp[625] - 1./3. * amp[626] - 1./3.
      * amp[627] - 1./3. * amp[628] - 1./3. * amp[629] - 1./3. * amp[630] -
      1./3. * amp[631] - 1./3. * amp[632] - 1./3. * amp[633] - 1./3. * amp[634]
      - 1./3. * amp[635] - 1./3. * amp[636] - 1./3. * amp[637] - 1./3. *
      amp[638] - 1./3. * amp[639] - 1./3. * amp[640] - 1./3. * amp[641] - 1./3.
      * amp[646] - 1./3. * amp[649] - 1./3. * amp[658] - 1./3. * amp[661] -
      1./3. * amp[668] - 1./3. * amp[669] - 1./3. * amp[671] - 1./3. * amp[672]
      - 1./3. * amp[674] - 1./3. * amp[675] - 1./3. * amp[677] - 1./3. *
      amp[678] - 1./3. * amp[680] - 1./3. * amp[681] - 1./3. * amp[683] - 1./3.
      * amp[684] - 1./3. * amp[686] - 1./3. * amp[687] - 1./3. * amp[689] -
      1./3. * amp[692] - 1./3. * amp[697] - 1./3. * amp[700]);
  jamp[2] = +1./2. * (+amp[576] + amp[577] + amp[578] + amp[579] + amp[580] +
      amp[581] + amp[582] + amp[583] + amp[584] + amp[585] + amp[586] +
      amp[587] + amp[588] + amp[589] + amp[590] + amp[591] + amp[592] +
      amp[593] + amp[594] + amp[595] + amp[596] + amp[597] + amp[620] +
      amp[621] + amp[622] + amp[623] + amp[624] + amp[625] + amp[626] +
      amp[627] + amp[628] + amp[629] + amp[630] + amp[631] + amp[632] +
      amp[633] + amp[634] + amp[635] + amp[636] + amp[637] + amp[638] +
      amp[639] + amp[640] + amp[641] + amp[643] - Complex<double> (0, 1) *
      amp[644] + Complex<double> (0, 1) * amp[647] + amp[649] + Complex<double>
      (0, 1) * amp[650] - Complex<double> (0, 1) * amp[652] + amp[655] -
      Complex<double> (0, 1) * amp[656] + Complex<double> (0, 1) * amp[659] +
      amp[661] + Complex<double> (0, 1) * amp[662] - Complex<double> (0, 1) *
      amp[664] + Complex<double> (0, 1) * amp[666] + Complex<double> (0, 1) *
      amp[667] + amp[668] + Complex<double> (0, 1) * amp[670] + Complex<double>
      (0, 1) * amp[673] + amp[675] + Complex<double> (0, 1) * amp[676] +
      amp[677] + amp[678] + Complex<double> (0, 1) * amp[679] + amp[680] +
      Complex<double> (0, 1) * amp[682] + Complex<double> (0, 1) * amp[685] +
      amp[686] + amp[687] + amp[688] + Complex<double> (0, 1) * amp[690] +
      amp[691] + amp[692] + Complex<double> (0, 1) * amp[693] + amp[694] -
      Complex<double> (0, 1) * amp[695] + amp[696] + amp[698] + Complex<double>
      (0, 1) * amp[699] + amp[700] + amp[701] + Complex<double> (0, 1) *
      amp[702] + amp[703] - Complex<double> (0, 1) * amp[704] + amp[705]);
  jamp[3] = +1./2. * (-1./3. * amp[554] - 1./3. * amp[555] - 1./3. * amp[556] -
      1./3. * amp[557] - 1./3. * amp[558] - 1./3. * amp[559] - 1./3. * amp[560]
      - 1./3. * amp[561] - 1./3. * amp[562] - 1./3. * amp[563] - 1./3. *
      amp[564] - 1./3. * amp[565] - 1./3. * amp[566] - 1./3. * amp[567] - 1./3.
      * amp[568] - 1./3. * amp[569] - 1./3. * amp[570] - 1./3. * amp[571] -
      1./3. * amp[572] - 1./3. * amp[573] - 1./3. * amp[574] - 1./3. * amp[575]
      - 1./3. * amp[576] - 1./3. * amp[577] - 1./3. * amp[578] - 1./3. *
      amp[579] - 1./3. * amp[580] - 1./3. * amp[581] - 1./3. * amp[582] - 1./3.
      * amp[583] - 1./3. * amp[584] - 1./3. * amp[585] - 1./3. * amp[586] -
      1./3. * amp[587] - 1./3. * amp[588] - 1./3. * amp[589] - 1./3. * amp[590]
      - 1./3. * amp[591] - 1./3. * amp[592] - 1./3. * amp[593] - 1./3. *
      amp[594] - 1./3. * amp[595] - 1./3. * amp[596] - 1./3. * amp[597] - 1./3.
      * amp[642] - 1./3. * amp[643] - 1./3. * amp[645] - 1./3. * amp[648] -
      1./3. * amp[651] - 1./3. * amp[653] - 1./3. * amp[654] - 1./3. * amp[655]
      - 1./3. * amp[657] - 1./3. * amp[660] - 1./3. * amp[663] - 1./3. *
      amp[665] - 1./3. * amp[688] - 1./3. * amp[691] - 1./3. * amp[694] - 1./3.
      * amp[696] - 1./3. * amp[698] - 1./3. * amp[701] - 1./3. * amp[703] -
      1./3. * amp[705]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[4][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P41_sm_qb_zzgqb::matrix_12_uxbx_zzguxbx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 152;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[750] + 1./3. * amp[751] + 1./3. * amp[752] +
      1./3. * amp[753] + 1./3. * amp[754] + 1./3. * amp[755] + 1./3. * amp[756]
      + 1./3. * amp[757] + 1./3. * amp[758] + 1./3. * amp[759] + 1./3. *
      amp[760] + 1./3. * amp[761] + 1./3. * amp[762] + 1./3. * amp[763] + 1./3.
      * amp[764] + 1./3. * amp[765] + 1./3. * amp[766] + 1./3. * amp[767] +
      1./3. * amp[768] + 1./3. * amp[769] + 1./3. * amp[770] + 1./3. * amp[771]
      + 1./3. * amp[772] + 1./3. * amp[773] + 1./3. * amp[774] + 1./3. *
      amp[775] + 1./3. * amp[776] + 1./3. * amp[777] + 1./3. * amp[778] + 1./3.
      * amp[779] + 1./3. * amp[780] + 1./3. * amp[781] + 1./3. * amp[782] +
      1./3. * amp[783] + 1./3. * amp[784] + 1./3. * amp[785] + 1./3. * amp[786]
      + 1./3. * amp[787] + 1./3. * amp[788] + 1./3. * amp[789] + 1./3. *
      amp[790] + 1./3. * amp[791] + 1./3. * amp[792] + 1./3. * amp[793] + 1./3.
      * amp[798] + 1./3. * amp[801] + 1./3. * amp[810] + 1./3. * amp[813] +
      1./3. * amp[820] + 1./3. * amp[821] + 1./3. * amp[823] + 1./3. * amp[824]
      + 1./3. * amp[826] + 1./3. * amp[827] + 1./3. * amp[829] + 1./3. *
      amp[830] + 1./3. * amp[832] + 1./3. * amp[833] + 1./3. * amp[835] + 1./3.
      * amp[836] + 1./3. * amp[838] + 1./3. * amp[839] + 1./3. * amp[841] +
      1./3. * amp[844] + 1./3. * amp[849] + 1./3. * amp[852]);
  jamp[1] = +1./2. * (-amp[706] - amp[707] - amp[708] - amp[709] - amp[710] -
      amp[711] - amp[712] - amp[713] - amp[714] - amp[715] - amp[716] -
      amp[717] - amp[718] - amp[719] - amp[720] - amp[721] - amp[722] -
      amp[723] - amp[724] - amp[725] - amp[726] - amp[727] - amp[750] -
      amp[751] - amp[752] - amp[753] - amp[754] - amp[755] - amp[756] -
      amp[757] - amp[758] - amp[759] - amp[760] - amp[761] - amp[762] -
      amp[763] - amp[764] - amp[765] - amp[766] - amp[767] - amp[768] -
      amp[769] - amp[770] - amp[771] - amp[794] - Complex<double> (0, 1) *
      amp[796] - amp[797] - amp[798] + Complex<double> (0, 1) * amp[799] -
      amp[800] + Complex<double> (0, 1) * amp[802] - amp[803] - Complex<double>
      (0, 1) * amp[804] - amp[805] - amp[806] - Complex<double> (0, 1) *
      amp[808] - amp[809] - amp[810] + Complex<double> (0, 1) * amp[811] -
      amp[812] + Complex<double> (0, 1) * amp[814] - amp[815] - Complex<double>
      (0, 1) * amp[816] - amp[817] + Complex<double> (0, 1) * amp[818] +
      Complex<double> (0, 1) * amp[819] - amp[821] + Complex<double> (0, 1) *
      amp[822] - amp[823] - amp[824] + Complex<double> (0, 1) * amp[825] -
      amp[826] + Complex<double> (0, 1) * amp[828] + Complex<double> (0, 1) *
      amp[831] - amp[833] + Complex<double> (0, 1) * amp[834] - amp[835] -
      amp[836] + Complex<double> (0, 1) * amp[837] - amp[841] + Complex<double>
      (0, 1) * amp[842] + Complex<double> (0, 1) * amp[845] - Complex<double>
      (0, 1) * amp[847] - amp[849] + Complex<double> (0, 1) * amp[851] +
      Complex<double> (0, 1) * amp[854] - Complex<double> (0, 1) * amp[856]);
  jamp[2] = +1./2. * (-amp[728] - amp[729] - amp[730] - amp[731] - amp[732] -
      amp[733] - amp[734] - amp[735] - amp[736] - amp[737] - amp[738] -
      amp[739] - amp[740] - amp[741] - amp[742] - amp[743] - amp[744] -
      amp[745] - amp[746] - amp[747] - amp[748] - amp[749] - amp[772] -
      amp[773] - amp[774] - amp[775] - amp[776] - amp[777] - amp[778] -
      amp[779] - amp[780] - amp[781] - amp[782] - amp[783] - amp[784] -
      amp[785] - amp[786] - amp[787] - amp[788] - amp[789] - amp[790] -
      amp[791] - amp[792] - amp[793] - amp[795] + Complex<double> (0, 1) *
      amp[796] - Complex<double> (0, 1) * amp[799] - amp[801] - Complex<double>
      (0, 1) * amp[802] + Complex<double> (0, 1) * amp[804] - amp[807] +
      Complex<double> (0, 1) * amp[808] - Complex<double> (0, 1) * amp[811] -
      amp[813] - Complex<double> (0, 1) * amp[814] + Complex<double> (0, 1) *
      amp[816] - Complex<double> (0, 1) * amp[818] - Complex<double> (0, 1) *
      amp[819] - amp[820] - Complex<double> (0, 1) * amp[822] - Complex<double>
      (0, 1) * amp[825] - amp[827] - Complex<double> (0, 1) * amp[828] -
      amp[829] - amp[830] - Complex<double> (0, 1) * amp[831] - amp[832] -
      Complex<double> (0, 1) * amp[834] - Complex<double> (0, 1) * amp[837] -
      amp[838] - amp[839] - amp[840] - Complex<double> (0, 1) * amp[842] -
      amp[843] - amp[844] - Complex<double> (0, 1) * amp[845] - amp[846] +
      Complex<double> (0, 1) * amp[847] - amp[848] - amp[850] - Complex<double>
      (0, 1) * amp[851] - amp[852] - amp[853] - Complex<double> (0, 1) *
      amp[854] - amp[855] + Complex<double> (0, 1) * amp[856] - amp[857]);
  jamp[3] = +1./2. * (+1./3. * amp[706] + 1./3. * amp[707] + 1./3. * amp[708] +
      1./3. * amp[709] + 1./3. * amp[710] + 1./3. * amp[711] + 1./3. * amp[712]
      + 1./3. * amp[713] + 1./3. * amp[714] + 1./3. * amp[715] + 1./3. *
      amp[716] + 1./3. * amp[717] + 1./3. * amp[718] + 1./3. * amp[719] + 1./3.
      * amp[720] + 1./3. * amp[721] + 1./3. * amp[722] + 1./3. * amp[723] +
      1./3. * amp[724] + 1./3. * amp[725] + 1./3. * amp[726] + 1./3. * amp[727]
      + 1./3. * amp[728] + 1./3. * amp[729] + 1./3. * amp[730] + 1./3. *
      amp[731] + 1./3. * amp[732] + 1./3. * amp[733] + 1./3. * amp[734] + 1./3.
      * amp[735] + 1./3. * amp[736] + 1./3. * amp[737] + 1./3. * amp[738] +
      1./3. * amp[739] + 1./3. * amp[740] + 1./3. * amp[741] + 1./3. * amp[742]
      + 1./3. * amp[743] + 1./3. * amp[744] + 1./3. * amp[745] + 1./3. *
      amp[746] + 1./3. * amp[747] + 1./3. * amp[748] + 1./3. * amp[749] + 1./3.
      * amp[794] + 1./3. * amp[795] + 1./3. * amp[797] + 1./3. * amp[800] +
      1./3. * amp[803] + 1./3. * amp[805] + 1./3. * amp[806] + 1./3. * amp[807]
      + 1./3. * amp[809] + 1./3. * amp[812] + 1./3. * amp[815] + 1./3. *
      amp[817] + 1./3. * amp[840] + 1./3. * amp[843] + 1./3. * amp[846] + 1./3.
      * amp[848] + 1./3. * amp[850] + 1./3. * amp[853] + 1./3. * amp[855] +
      1./3. * amp[857]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[5][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P41_sm_qb_zzgqb::matrix_12_dxb_zzgdxb() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 152;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[858] + amp[859] + amp[860] + amp[861] + amp[862] +
      amp[863] + amp[864] + amp[865] + amp[866] + amp[867] + amp[868] +
      amp[869] + amp[870] + amp[871] + amp[872] + amp[873] + amp[874] +
      amp[875] + amp[876] + amp[877] + amp[878] + amp[879] + amp[902] +
      amp[903] + amp[904] + amp[905] + amp[906] + amp[907] + amp[908] +
      amp[909] + amp[910] + amp[911] + amp[912] + amp[913] + amp[914] +
      amp[915] + amp[916] + amp[917] + amp[918] + amp[919] + amp[920] +
      amp[921] + amp[922] + amp[923] + amp[946] + Complex<double> (0, 1) *
      amp[948] + amp[949] + amp[950] - Complex<double> (0, 1) * amp[951] +
      amp[952] - Complex<double> (0, 1) * amp[954] + amp[955] + Complex<double>
      (0, 1) * amp[956] + amp[957] + amp[958] + Complex<double> (0, 1) *
      amp[960] + amp[961] + amp[962] - Complex<double> (0, 1) * amp[963] +
      amp[964] - Complex<double> (0, 1) * amp[966] + amp[967] + Complex<double>
      (0, 1) * amp[968] + amp[969] - Complex<double> (0, 1) * amp[970] -
      Complex<double> (0, 1) * amp[971] + amp[973] - Complex<double> (0, 1) *
      amp[974] + amp[975] + amp[976] - Complex<double> (0, 1) * amp[977] +
      amp[978] - Complex<double> (0, 1) * amp[980] - Complex<double> (0, 1) *
      amp[983] + amp[985] - Complex<double> (0, 1) * amp[986] + amp[987] +
      amp[988] - Complex<double> (0, 1) * amp[989] + amp[993] - Complex<double>
      (0, 1) * amp[994] - Complex<double> (0, 1) * amp[997] + Complex<double>
      (0, 1) * amp[999] + amp[1001] - Complex<double> (0, 1) * amp[1003] -
      Complex<double> (0, 1) * amp[1006] + Complex<double> (0, 1) * amp[1008]);
  jamp[1] = +1./2. * (-1./3. * amp[902] - 1./3. * amp[903] - 1./3. * amp[904] -
      1./3. * amp[905] - 1./3. * amp[906] - 1./3. * amp[907] - 1./3. * amp[908]
      - 1./3. * amp[909] - 1./3. * amp[910] - 1./3. * amp[911] - 1./3. *
      amp[912] - 1./3. * amp[913] - 1./3. * amp[914] - 1./3. * amp[915] - 1./3.
      * amp[916] - 1./3. * amp[917] - 1./3. * amp[918] - 1./3. * amp[919] -
      1./3. * amp[920] - 1./3. * amp[921] - 1./3. * amp[922] - 1./3. * amp[923]
      - 1./3. * amp[924] - 1./3. * amp[925] - 1./3. * amp[926] - 1./3. *
      amp[927] - 1./3. * amp[928] - 1./3. * amp[929] - 1./3. * amp[930] - 1./3.
      * amp[931] - 1./3. * amp[932] - 1./3. * amp[933] - 1./3. * amp[934] -
      1./3. * amp[935] - 1./3. * amp[936] - 1./3. * amp[937] - 1./3. * amp[938]
      - 1./3. * amp[939] - 1./3. * amp[940] - 1./3. * amp[941] - 1./3. *
      amp[942] - 1./3. * amp[943] - 1./3. * amp[944] - 1./3. * amp[945] - 1./3.
      * amp[950] - 1./3. * amp[953] - 1./3. * amp[962] - 1./3. * amp[965] -
      1./3. * amp[972] - 1./3. * amp[973] - 1./3. * amp[975] - 1./3. * amp[976]
      - 1./3. * amp[978] - 1./3. * amp[979] - 1./3. * amp[981] - 1./3. *
      amp[982] - 1./3. * amp[984] - 1./3. * amp[985] - 1./3. * amp[987] - 1./3.
      * amp[988] - 1./3. * amp[990] - 1./3. * amp[991] - 1./3. * amp[993] -
      1./3. * amp[996] - 1./3. * amp[1001] - 1./3. * amp[1004]);
  jamp[2] = +1./2. * (+amp[880] + amp[881] + amp[882] + amp[883] + amp[884] +
      amp[885] + amp[886] + amp[887] + amp[888] + amp[889] + amp[890] +
      amp[891] + amp[892] + amp[893] + amp[894] + amp[895] + amp[896] +
      amp[897] + amp[898] + amp[899] + amp[900] + amp[901] + amp[924] +
      amp[925] + amp[926] + amp[927] + amp[928] + amp[929] + amp[930] +
      amp[931] + amp[932] + amp[933] + amp[934] + amp[935] + amp[936] +
      amp[937] + amp[938] + amp[939] + amp[940] + amp[941] + amp[942] +
      amp[943] + amp[944] + amp[945] + amp[947] - Complex<double> (0, 1) *
      amp[948] + Complex<double> (0, 1) * amp[951] + amp[953] + Complex<double>
      (0, 1) * amp[954] - Complex<double> (0, 1) * amp[956] + amp[959] -
      Complex<double> (0, 1) * amp[960] + Complex<double> (0, 1) * amp[963] +
      amp[965] + Complex<double> (0, 1) * amp[966] - Complex<double> (0, 1) *
      amp[968] + Complex<double> (0, 1) * amp[970] + Complex<double> (0, 1) *
      amp[971] + amp[972] + Complex<double> (0, 1) * amp[974] + Complex<double>
      (0, 1) * amp[977] + amp[979] + Complex<double> (0, 1) * amp[980] +
      amp[981] + amp[982] + Complex<double> (0, 1) * amp[983] + amp[984] +
      Complex<double> (0, 1) * amp[986] + Complex<double> (0, 1) * amp[989] +
      amp[990] + amp[991] + amp[992] + Complex<double> (0, 1) * amp[994] +
      amp[995] + amp[996] + Complex<double> (0, 1) * amp[997] + amp[998] -
      Complex<double> (0, 1) * amp[999] + amp[1000] + amp[1002] +
      Complex<double> (0, 1) * amp[1003] + amp[1004] + amp[1005] +
      Complex<double> (0, 1) * amp[1006] + amp[1007] - Complex<double> (0, 1) *
      amp[1008] + amp[1009]);
  jamp[3] = +1./2. * (-1./3. * amp[858] - 1./3. * amp[859] - 1./3. * amp[860] -
      1./3. * amp[861] - 1./3. * amp[862] - 1./3. * amp[863] - 1./3. * amp[864]
      - 1./3. * amp[865] - 1./3. * amp[866] - 1./3. * amp[867] - 1./3. *
      amp[868] - 1./3. * amp[869] - 1./3. * amp[870] - 1./3. * amp[871] - 1./3.
      * amp[872] - 1./3. * amp[873] - 1./3. * amp[874] - 1./3. * amp[875] -
      1./3. * amp[876] - 1./3. * amp[877] - 1./3. * amp[878] - 1./3. * amp[879]
      - 1./3. * amp[880] - 1./3. * amp[881] - 1./3. * amp[882] - 1./3. *
      amp[883] - 1./3. * amp[884] - 1./3. * amp[885] - 1./3. * amp[886] - 1./3.
      * amp[887] - 1./3. * amp[888] - 1./3. * amp[889] - 1./3. * amp[890] -
      1./3. * amp[891] - 1./3. * amp[892] - 1./3. * amp[893] - 1./3. * amp[894]
      - 1./3. * amp[895] - 1./3. * amp[896] - 1./3. * amp[897] - 1./3. *
      amp[898] - 1./3. * amp[899] - 1./3. * amp[900] - 1./3. * amp[901] - 1./3.
      * amp[946] - 1./3. * amp[947] - 1./3. * amp[949] - 1./3. * amp[952] -
      1./3. * amp[955] - 1./3. * amp[957] - 1./3. * amp[958] - 1./3. * amp[959]
      - 1./3. * amp[961] - 1./3. * amp[964] - 1./3. * amp[967] - 1./3. *
      amp[969] - 1./3. * amp[992] - 1./3. * amp[995] - 1./3. * amp[998] - 1./3.
      * amp[1000] - 1./3. * amp[1002] - 1./3. * amp[1005] - 1./3. * amp[1007] -
      1./3. * amp[1009]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[6][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P41_sm_qb_zzgqb::matrix_12_dxbx_zzgdxbx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 152;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[1054] + 1./3. * amp[1055] + 1./3. *
      amp[1056] + 1./3. * amp[1057] + 1./3. * amp[1058] + 1./3. * amp[1059] +
      1./3. * amp[1060] + 1./3. * amp[1061] + 1./3. * amp[1062] + 1./3. *
      amp[1063] + 1./3. * amp[1064] + 1./3. * amp[1065] + 1./3. * amp[1066] +
      1./3. * amp[1067] + 1./3. * amp[1068] + 1./3. * amp[1069] + 1./3. *
      amp[1070] + 1./3. * amp[1071] + 1./3. * amp[1072] + 1./3. * amp[1073] +
      1./3. * amp[1074] + 1./3. * amp[1075] + 1./3. * amp[1076] + 1./3. *
      amp[1077] + 1./3. * amp[1078] + 1./3. * amp[1079] + 1./3. * amp[1080] +
      1./3. * amp[1081] + 1./3. * amp[1082] + 1./3. * amp[1083] + 1./3. *
      amp[1084] + 1./3. * amp[1085] + 1./3. * amp[1086] + 1./3. * amp[1087] +
      1./3. * amp[1088] + 1./3. * amp[1089] + 1./3. * amp[1090] + 1./3. *
      amp[1091] + 1./3. * amp[1092] + 1./3. * amp[1093] + 1./3. * amp[1094] +
      1./3. * amp[1095] + 1./3. * amp[1096] + 1./3. * amp[1097] + 1./3. *
      amp[1102] + 1./3. * amp[1105] + 1./3. * amp[1114] + 1./3. * amp[1117] +
      1./3. * amp[1124] + 1./3. * amp[1125] + 1./3. * amp[1127] + 1./3. *
      amp[1128] + 1./3. * amp[1130] + 1./3. * amp[1131] + 1./3. * amp[1133] +
      1./3. * amp[1134] + 1./3. * amp[1136] + 1./3. * amp[1137] + 1./3. *
      amp[1139] + 1./3. * amp[1140] + 1./3. * amp[1142] + 1./3. * amp[1143] +
      1./3. * amp[1145] + 1./3. * amp[1148] + 1./3. * amp[1153] + 1./3. *
      amp[1156]);
  jamp[1] = +1./2. * (-amp[1010] - amp[1011] - amp[1012] - amp[1013] -
      amp[1014] - amp[1015] - amp[1016] - amp[1017] - amp[1018] - amp[1019] -
      amp[1020] - amp[1021] - amp[1022] - amp[1023] - amp[1024] - amp[1025] -
      amp[1026] - amp[1027] - amp[1028] - amp[1029] - amp[1030] - amp[1031] -
      amp[1054] - amp[1055] - amp[1056] - amp[1057] - amp[1058] - amp[1059] -
      amp[1060] - amp[1061] - amp[1062] - amp[1063] - amp[1064] - amp[1065] -
      amp[1066] - amp[1067] - amp[1068] - amp[1069] - amp[1070] - amp[1071] -
      amp[1072] - amp[1073] - amp[1074] - amp[1075] - amp[1098] -
      Complex<double> (0, 1) * amp[1100] - amp[1101] - amp[1102] +
      Complex<double> (0, 1) * amp[1103] - amp[1104] + Complex<double> (0, 1) *
      amp[1106] - amp[1107] - Complex<double> (0, 1) * amp[1108] - amp[1109] -
      amp[1110] - Complex<double> (0, 1) * amp[1112] - amp[1113] - amp[1114] +
      Complex<double> (0, 1) * amp[1115] - amp[1116] + Complex<double> (0, 1) *
      amp[1118] - amp[1119] - Complex<double> (0, 1) * amp[1120] - amp[1121] +
      Complex<double> (0, 1) * amp[1122] + Complex<double> (0, 1) * amp[1123] -
      amp[1125] + Complex<double> (0, 1) * amp[1126] - amp[1127] - amp[1128] +
      Complex<double> (0, 1) * amp[1129] - amp[1130] + Complex<double> (0, 1) *
      amp[1132] + Complex<double> (0, 1) * amp[1135] - amp[1137] +
      Complex<double> (0, 1) * amp[1138] - amp[1139] - amp[1140] +
      Complex<double> (0, 1) * amp[1141] - amp[1145] + Complex<double> (0, 1) *
      amp[1146] + Complex<double> (0, 1) * amp[1149] - Complex<double> (0, 1) *
      amp[1151] - amp[1153] + Complex<double> (0, 1) * amp[1155] +
      Complex<double> (0, 1) * amp[1158] - Complex<double> (0, 1) * amp[1160]);
  jamp[2] = +1./2. * (-amp[1032] - amp[1033] - amp[1034] - amp[1035] -
      amp[1036] - amp[1037] - amp[1038] - amp[1039] - amp[1040] - amp[1041] -
      amp[1042] - amp[1043] - amp[1044] - amp[1045] - amp[1046] - amp[1047] -
      amp[1048] - amp[1049] - amp[1050] - amp[1051] - amp[1052] - amp[1053] -
      amp[1076] - amp[1077] - amp[1078] - amp[1079] - amp[1080] - amp[1081] -
      amp[1082] - amp[1083] - amp[1084] - amp[1085] - amp[1086] - amp[1087] -
      amp[1088] - amp[1089] - amp[1090] - amp[1091] - amp[1092] - amp[1093] -
      amp[1094] - amp[1095] - amp[1096] - amp[1097] - amp[1099] +
      Complex<double> (0, 1) * amp[1100] - Complex<double> (0, 1) * amp[1103] -
      amp[1105] - Complex<double> (0, 1) * amp[1106] + Complex<double> (0, 1) *
      amp[1108] - amp[1111] + Complex<double> (0, 1) * amp[1112] -
      Complex<double> (0, 1) * amp[1115] - amp[1117] - Complex<double> (0, 1) *
      amp[1118] + Complex<double> (0, 1) * amp[1120] - Complex<double> (0, 1) *
      amp[1122] - Complex<double> (0, 1) * amp[1123] - amp[1124] -
      Complex<double> (0, 1) * amp[1126] - Complex<double> (0, 1) * amp[1129] -
      amp[1131] - Complex<double> (0, 1) * amp[1132] - amp[1133] - amp[1134] -
      Complex<double> (0, 1) * amp[1135] - amp[1136] - Complex<double> (0, 1) *
      amp[1138] - Complex<double> (0, 1) * amp[1141] - amp[1142] - amp[1143] -
      amp[1144] - Complex<double> (0, 1) * amp[1146] - amp[1147] - amp[1148] -
      Complex<double> (0, 1) * amp[1149] - amp[1150] + Complex<double> (0, 1) *
      amp[1151] - amp[1152] - amp[1154] - Complex<double> (0, 1) * amp[1155] -
      amp[1156] - amp[1157] - Complex<double> (0, 1) * amp[1158] - amp[1159] +
      Complex<double> (0, 1) * amp[1160] - amp[1161]);
  jamp[3] = +1./2. * (+1./3. * amp[1010] + 1./3. * amp[1011] + 1./3. *
      amp[1012] + 1./3. * amp[1013] + 1./3. * amp[1014] + 1./3. * amp[1015] +
      1./3. * amp[1016] + 1./3. * amp[1017] + 1./3. * amp[1018] + 1./3. *
      amp[1019] + 1./3. * amp[1020] + 1./3. * amp[1021] + 1./3. * amp[1022] +
      1./3. * amp[1023] + 1./3. * amp[1024] + 1./3. * amp[1025] + 1./3. *
      amp[1026] + 1./3. * amp[1027] + 1./3. * amp[1028] + 1./3. * amp[1029] +
      1./3. * amp[1030] + 1./3. * amp[1031] + 1./3. * amp[1032] + 1./3. *
      amp[1033] + 1./3. * amp[1034] + 1./3. * amp[1035] + 1./3. * amp[1036] +
      1./3. * amp[1037] + 1./3. * amp[1038] + 1./3. * amp[1039] + 1./3. *
      amp[1040] + 1./3. * amp[1041] + 1./3. * amp[1042] + 1./3. * amp[1043] +
      1./3. * amp[1044] + 1./3. * amp[1045] + 1./3. * amp[1046] + 1./3. *
      amp[1047] + 1./3. * amp[1048] + 1./3. * amp[1049] + 1./3. * amp[1050] +
      1./3. * amp[1051] + 1./3. * amp[1052] + 1./3. * amp[1053] + 1./3. *
      amp[1098] + 1./3. * amp[1099] + 1./3. * amp[1101] + 1./3. * amp[1104] +
      1./3. * amp[1107] + 1./3. * amp[1109] + 1./3. * amp[1110] + 1./3. *
      amp[1111] + 1./3. * amp[1113] + 1./3. * amp[1116] + 1./3. * amp[1119] +
      1./3. * amp[1121] + 1./3. * amp[1144] + 1./3. * amp[1147] + 1./3. *
      amp[1150] + 1./3. * amp[1152] + 1./3. * amp[1154] + 1./3. * amp[1157] +
      1./3. * amp[1159] + 1./3. * amp[1161]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[7][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}


}  // end namespace PY8MEs_namespace

