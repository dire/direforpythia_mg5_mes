//==========================================================================
// This file has been automatically generated for Pythia 8 by
// MadGraph5_aMC@NLO v. 2.6.0, 2017-08-16
// By the MadGraph5_aMC@NLO Development Team
// Visit launchpad.net/madgraph5 and amcatnlo.web.cern.ch
//==========================================================================

#include "PY8MEs_R8_P177_sm_qq_wpgqq.h"
#include "HelAmps_sm.h"

using namespace Pythia8_sm; 

namespace PY8MEs_namespace 
{
//==========================================================================
// Class member functions for calculating the matrix elements for
// Process: u u > w+ g u d WEIGHTED<=5 @8
// Process: c c > w+ g c s WEIGHTED<=5 @8
// Process: u d > w+ g d d WEIGHTED<=5 @8
// Process: c s > w+ g s s WEIGHTED<=5 @8
// Process: u d > w- g u u WEIGHTED<=5 @8
// Process: c s > w- g c c WEIGHTED<=5 @8
// Process: u u~ > w+ g d u~ WEIGHTED<=5 @8
// Process: c c~ > w+ g s c~ WEIGHTED<=5 @8
// Process: u u~ > w- g u d~ WEIGHTED<=5 @8
// Process: c c~ > w- g c s~ WEIGHTED<=5 @8
// Process: u d~ > w+ g u u~ WEIGHTED<=5 @8
// Process: c s~ > w+ g c c~ WEIGHTED<=5 @8
// Process: u d~ > w+ g d d~ WEIGHTED<=5 @8
// Process: c s~ > w+ g s s~ WEIGHTED<=5 @8
// Process: d d > w- g u d WEIGHTED<=5 @8
// Process: s s > w- g c s WEIGHTED<=5 @8
// Process: d u~ > w- g u u~ WEIGHTED<=5 @8
// Process: s c~ > w- g c c~ WEIGHTED<=5 @8
// Process: d u~ > w- g d d~ WEIGHTED<=5 @8
// Process: s c~ > w- g s s~ WEIGHTED<=5 @8
// Process: d d~ > w+ g d u~ WEIGHTED<=5 @8
// Process: s s~ > w+ g s c~ WEIGHTED<=5 @8
// Process: d d~ > w- g u d~ WEIGHTED<=5 @8
// Process: s s~ > w- g c s~ WEIGHTED<=5 @8
// Process: u~ u~ > w- g u~ d~ WEIGHTED<=5 @8
// Process: c~ c~ > w- g c~ s~ WEIGHTED<=5 @8
// Process: u~ d~ > w+ g u~ u~ WEIGHTED<=5 @8
// Process: c~ s~ > w+ g c~ c~ WEIGHTED<=5 @8
// Process: u~ d~ > w- g d~ d~ WEIGHTED<=5 @8
// Process: c~ s~ > w- g s~ s~ WEIGHTED<=5 @8
// Process: d~ d~ > w+ g u~ d~ WEIGHTED<=5 @8
// Process: s~ s~ > w+ g c~ s~ WEIGHTED<=5 @8
// Process: u c > w+ g u s WEIGHTED<=5 @8
// Process: u c > w+ g c d WEIGHTED<=5 @8
// Process: u s > w+ g s d WEIGHTED<=5 @8
// Process: c d > w+ g d s WEIGHTED<=5 @8
// Process: u s > w- g u c WEIGHTED<=5 @8
// Process: c d > w- g c u WEIGHTED<=5 @8
// Process: d s > w- g d c WEIGHTED<=5 @8
// Process: u u~ > w+ g s c~ WEIGHTED<=5 @8
// Process: c c~ > w+ g d u~ WEIGHTED<=5 @8
// Process: d d~ > w+ g s c~ WEIGHTED<=5 @8
// Process: s s~ > w+ g d u~ WEIGHTED<=5 @8
// Process: u u~ > w- g c s~ WEIGHTED<=5 @8
// Process: c c~ > w- g u d~ WEIGHTED<=5 @8
// Process: d d~ > w- g c s~ WEIGHTED<=5 @8
// Process: s s~ > w- g u d~ WEIGHTED<=5 @8
// Process: u c~ > w+ g d c~ WEIGHTED<=5 @8
// Process: u s~ > w+ g d s~ WEIGHTED<=5 @8
// Process: c u~ > w+ g s u~ WEIGHTED<=5 @8
// Process: c d~ > w+ g s d~ WEIGHTED<=5 @8
// Process: u c~ > w- g u s~ WEIGHTED<=5 @8
// Process: c u~ > w- g c d~ WEIGHTED<=5 @8
// Process: d c~ > w- g d s~ WEIGHTED<=5 @8
// Process: s u~ > w- g s d~ WEIGHTED<=5 @8
// Process: u d~ > w+ g c c~ WEIGHTED<=5 @8
// Process: u d~ > w+ g s s~ WEIGHTED<=5 @8
// Process: c s~ > w+ g u u~ WEIGHTED<=5 @8
// Process: c s~ > w+ g d d~ WEIGHTED<=5 @8
// Process: u s~ > w+ g u c~ WEIGHTED<=5 @8
// Process: c d~ > w+ g c u~ WEIGHTED<=5 @8
// Process: d s~ > w+ g d c~ WEIGHTED<=5 @8
// Process: s d~ > w+ g s u~ WEIGHTED<=5 @8
// Process: d s > w- g u s WEIGHTED<=5 @8
// Process: d u~ > w- g c c~ WEIGHTED<=5 @8
// Process: d u~ > w- g s s~ WEIGHTED<=5 @8
// Process: s c~ > w- g u u~ WEIGHTED<=5 @8
// Process: s c~ > w- g d d~ WEIGHTED<=5 @8
// Process: d c~ > w- g u c~ WEIGHTED<=5 @8
// Process: d s~ > w- g u s~ WEIGHTED<=5 @8
// Process: s u~ > w- g c u~ WEIGHTED<=5 @8
// Process: s d~ > w- g c d~ WEIGHTED<=5 @8
// Process: u~ c~ > w- g u~ s~ WEIGHTED<=5 @8
// Process: u~ c~ > w- g c~ d~ WEIGHTED<=5 @8
// Process: u~ s~ > w- g s~ d~ WEIGHTED<=5 @8
// Process: c~ d~ > w- g d~ s~ WEIGHTED<=5 @8
// Process: u~ s~ > w+ g u~ c~ WEIGHTED<=5 @8
// Process: c~ d~ > w+ g c~ u~ WEIGHTED<=5 @8
// Process: d~ s~ > w+ g d~ c~ WEIGHTED<=5 @8
// Process: d~ s~ > w+ g u~ s~ WEIGHTED<=5 @8

// Exception class
class PY8MEs_R8_P177_sm_qq_wpgqqException : public exception
{
  virtual const char * what() const throw()
  {
    return "Exception in class 'PY8MEs_R8_P177_sm_qq_wpgqq'."; 
  }
}
PY8MEs_R8_P177_sm_qq_wpgqq_exception; 

std::set<int> PY8MEs_R8_P177_sm_qq_wpgqq::s_channel_proc = std::set<int>
    (createset<int> ());

int PY8MEs_R8_P177_sm_qq_wpgqq::helicities[ncomb][nexternal] = {{-1, -1, -1,
    -1, -1, -1}, {-1, -1, -1, -1, -1, 1}, {-1, -1, -1, -1, 1, -1}, {-1, -1, -1,
    -1, 1, 1}, {-1, -1, -1, 1, -1, -1}, {-1, -1, -1, 1, -1, 1}, {-1, -1, -1, 1,
    1, -1}, {-1, -1, -1, 1, 1, 1}, {-1, -1, 0, -1, -1, -1}, {-1, -1, 0, -1, -1,
    1}, {-1, -1, 0, -1, 1, -1}, {-1, -1, 0, -1, 1, 1}, {-1, -1, 0, 1, -1, -1},
    {-1, -1, 0, 1, -1, 1}, {-1, -1, 0, 1, 1, -1}, {-1, -1, 0, 1, 1, 1}, {-1,
    -1, 1, -1, -1, -1}, {-1, -1, 1, -1, -1, 1}, {-1, -1, 1, -1, 1, -1}, {-1,
    -1, 1, -1, 1, 1}, {-1, -1, 1, 1, -1, -1}, {-1, -1, 1, 1, -1, 1}, {-1, -1,
    1, 1, 1, -1}, {-1, -1, 1, 1, 1, 1}, {-1, 1, -1, -1, -1, -1}, {-1, 1, -1,
    -1, -1, 1}, {-1, 1, -1, -1, 1, -1}, {-1, 1, -1, -1, 1, 1}, {-1, 1, -1, 1,
    -1, -1}, {-1, 1, -1, 1, -1, 1}, {-1, 1, -1, 1, 1, -1}, {-1, 1, -1, 1, 1,
    1}, {-1, 1, 0, -1, -1, -1}, {-1, 1, 0, -1, -1, 1}, {-1, 1, 0, -1, 1, -1},
    {-1, 1, 0, -1, 1, 1}, {-1, 1, 0, 1, -1, -1}, {-1, 1, 0, 1, -1, 1}, {-1, 1,
    0, 1, 1, -1}, {-1, 1, 0, 1, 1, 1}, {-1, 1, 1, -1, -1, -1}, {-1, 1, 1, -1,
    -1, 1}, {-1, 1, 1, -1, 1, -1}, {-1, 1, 1, -1, 1, 1}, {-1, 1, 1, 1, -1, -1},
    {-1, 1, 1, 1, -1, 1}, {-1, 1, 1, 1, 1, -1}, {-1, 1, 1, 1, 1, 1}, {1, -1,
    -1, -1, -1, -1}, {1, -1, -1, -1, -1, 1}, {1, -1, -1, -1, 1, -1}, {1, -1,
    -1, -1, 1, 1}, {1, -1, -1, 1, -1, -1}, {1, -1, -1, 1, -1, 1}, {1, -1, -1,
    1, 1, -1}, {1, -1, -1, 1, 1, 1}, {1, -1, 0, -1, -1, -1}, {1, -1, 0, -1, -1,
    1}, {1, -1, 0, -1, 1, -1}, {1, -1, 0, -1, 1, 1}, {1, -1, 0, 1, -1, -1}, {1,
    -1, 0, 1, -1, 1}, {1, -1, 0, 1, 1, -1}, {1, -1, 0, 1, 1, 1}, {1, -1, 1, -1,
    -1, -1}, {1, -1, 1, -1, -1, 1}, {1, -1, 1, -1, 1, -1}, {1, -1, 1, -1, 1,
    1}, {1, -1, 1, 1, -1, -1}, {1, -1, 1, 1, -1, 1}, {1, -1, 1, 1, 1, -1}, {1,
    -1, 1, 1, 1, 1}, {1, 1, -1, -1, -1, -1}, {1, 1, -1, -1, -1, 1}, {1, 1, -1,
    -1, 1, -1}, {1, 1, -1, -1, 1, 1}, {1, 1, -1, 1, -1, -1}, {1, 1, -1, 1, -1,
    1}, {1, 1, -1, 1, 1, -1}, {1, 1, -1, 1, 1, 1}, {1, 1, 0, -1, -1, -1}, {1,
    1, 0, -1, -1, 1}, {1, 1, 0, -1, 1, -1}, {1, 1, 0, -1, 1, 1}, {1, 1, 0, 1,
    -1, -1}, {1, 1, 0, 1, -1, 1}, {1, 1, 0, 1, 1, -1}, {1, 1, 0, 1, 1, 1}, {1,
    1, 1, -1, -1, -1}, {1, 1, 1, -1, -1, 1}, {1, 1, 1, -1, 1, -1}, {1, 1, 1,
    -1, 1, 1}, {1, 1, 1, 1, -1, -1}, {1, 1, 1, 1, -1, 1}, {1, 1, 1, 1, 1, -1},
    {1, 1, 1, 1, 1, 1}};

// Normalization factors the various processes
// Denominators: spins, colors and identical particles
int PY8MEs_R8_P177_sm_qq_wpgqq::denom_colors[nprocesses] = {9, 9, 9, 9, 9, 9,
    9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
    9};
int PY8MEs_R8_P177_sm_qq_wpgqq::denom_hels[nprocesses] = {4, 4, 4, 4, 4, 4, 4,
    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4};
int PY8MEs_R8_P177_sm_qq_wpgqq::denom_iden[nprocesses] = {1, 2, 2, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};

//--------------------------------------------------------------------------
// Color config initialization
void PY8MEs_R8_P177_sm_qq_wpgqq::initColorConfigs() 
{
  color_configs = vector < vec_vec_int > (); 
  jamp_nc_relative_power = vector < vec_int > (); 

  // Color flows of process Process: u u > w+ g u d WEIGHTED<=5 @8
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #1
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #2
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #3
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 

  // Color flows of process Process: u d > w+ g d d WEIGHTED<=5 @8
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #1
  color_configs[1].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #2
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #3
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[1].push_back(0); 

  // Color flows of process Process: u d > w- g u u WEIGHTED<=5 @8
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[2].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #1
  color_configs[2].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #2
  color_configs[2].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #3
  color_configs[2].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[2].push_back(0); 

  // Color flows of process Process: u u~ > w+ g d u~ WEIGHTED<=5 @8
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[3].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #1
  color_configs[3].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #2
  color_configs[3].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #3
  color_configs[3].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[3].push_back(0); 

  // Color flows of process Process: u u~ > w- g u d~ WEIGHTED<=5 @8
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[4].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[4].push_back(0); 
  // JAMP #1
  color_configs[4].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[4].push_back(0); 
  // JAMP #2
  color_configs[4].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[4].push_back(0); 
  // JAMP #3
  color_configs[4].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[4].push_back(0); 

  // Color flows of process Process: u d~ > w+ g u u~ WEIGHTED<=5 @8
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[5].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[5].push_back(0); 
  // JAMP #1
  color_configs[5].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[5].push_back(0); 
  // JAMP #2
  color_configs[5].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[5].push_back(0); 
  // JAMP #3
  color_configs[5].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[5].push_back(0); 

  // Color flows of process Process: u d~ > w+ g d d~ WEIGHTED<=5 @8
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[6].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[6].push_back(0); 
  // JAMP #1
  color_configs[6].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[6].push_back(0); 
  // JAMP #2
  color_configs[6].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[6].push_back(0); 
  // JAMP #3
  color_configs[6].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[6].push_back(0); 

  // Color flows of process Process: d d > w- g u d WEIGHTED<=5 @8
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[7].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[7].push_back(0); 
  // JAMP #1
  color_configs[7].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[7].push_back(0); 
  // JAMP #2
  color_configs[7].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[7].push_back(0); 
  // JAMP #3
  color_configs[7].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[7].push_back(0); 

  // Color flows of process Process: d u~ > w- g u u~ WEIGHTED<=5 @8
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[8].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[8].push_back(0); 
  // JAMP #1
  color_configs[8].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[8].push_back(0); 
  // JAMP #2
  color_configs[8].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[8].push_back(0); 
  // JAMP #3
  color_configs[8].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[8].push_back(0); 

  // Color flows of process Process: d u~ > w- g d d~ WEIGHTED<=5 @8
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[9].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[9].push_back(0); 
  // JAMP #1
  color_configs[9].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[9].push_back(0); 
  // JAMP #2
  color_configs[9].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[9].push_back(0); 
  // JAMP #3
  color_configs[9].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[9].push_back(0); 

  // Color flows of process Process: d d~ > w+ g d u~ WEIGHTED<=5 @8
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[10].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[10].push_back(0); 
  // JAMP #1
  color_configs[10].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[10].push_back(0); 
  // JAMP #2
  color_configs[10].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[10].push_back(0); 
  // JAMP #3
  color_configs[10].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[10].push_back(0); 

  // Color flows of process Process: d d~ > w- g u d~ WEIGHTED<=5 @8
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[11].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[11].push_back(0); 
  // JAMP #1
  color_configs[11].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[11].push_back(0); 
  // JAMP #2
  color_configs[11].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[11].push_back(0); 
  // JAMP #3
  color_configs[11].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[11].push_back(0); 

  // Color flows of process Process: u~ u~ > w- g u~ d~ WEIGHTED<=5 @8
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[12].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(3)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[12].push_back(0); 
  // JAMP #1
  color_configs[12].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(3)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[12].push_back(0); 
  // JAMP #2
  color_configs[12].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(3)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[12].push_back(0); 
  // JAMP #3
  color_configs[12].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(3)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[12].push_back(0); 

  // Color flows of process Process: u~ d~ > w+ g u~ u~ WEIGHTED<=5 @8
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[13].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(3)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[13].push_back(0); 
  // JAMP #1
  color_configs[13].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(3)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[13].push_back(0); 
  // JAMP #2
  color_configs[13].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(3)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[13].push_back(0); 
  // JAMP #3
  color_configs[13].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(3)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[13].push_back(0); 

  // Color flows of process Process: u~ d~ > w- g d~ d~ WEIGHTED<=5 @8
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[14].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(3)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[14].push_back(0); 
  // JAMP #1
  color_configs[14].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(3)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[14].push_back(0); 
  // JAMP #2
  color_configs[14].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(3)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[14].push_back(0); 
  // JAMP #3
  color_configs[14].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(3)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[14].push_back(0); 

  // Color flows of process Process: d~ d~ > w+ g u~ d~ WEIGHTED<=5 @8
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[15].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(3)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[15].push_back(0); 
  // JAMP #1
  color_configs[15].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(3)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[15].push_back(0); 
  // JAMP #2
  color_configs[15].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(3)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[15].push_back(0); 
  // JAMP #3
  color_configs[15].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(3)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[15].push_back(0); 

  // Color flows of process Process: u c > w+ g u s WEIGHTED<=5 @8
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[16].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[16].push_back(-1); 
  // JAMP #1
  color_configs[16].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[16].push_back(0); 
  // JAMP #2
  color_configs[16].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[16].push_back(0); 
  // JAMP #3
  color_configs[16].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[16].push_back(-1); 

  // Color flows of process Process: u c > w+ g c d WEIGHTED<=5 @8
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[17].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[17].push_back(0); 
  // JAMP #1
  color_configs[17].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[17].push_back(-1); 
  // JAMP #2
  color_configs[17].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[17].push_back(-1); 
  // JAMP #3
  color_configs[17].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[17].push_back(0); 

  // Color flows of process Process: u s > w- g u c WEIGHTED<=5 @8
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[18].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[18].push_back(-1); 
  // JAMP #1
  color_configs[18].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[18].push_back(0); 
  // JAMP #2
  color_configs[18].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[18].push_back(0); 
  // JAMP #3
  color_configs[18].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[18].push_back(-1); 

  // Color flows of process Process: u u~ > w+ g s c~ WEIGHTED<=5 @8
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[19].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[19].push_back(-1); 
  // JAMP #1
  color_configs[19].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[19].push_back(0); 
  // JAMP #2
  color_configs[19].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[19].push_back(-1); 
  // JAMP #3
  color_configs[19].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[19].push_back(0); 

  // Color flows of process Process: u u~ > w- g c s~ WEIGHTED<=5 @8
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[20].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[20].push_back(-1); 
  // JAMP #1
  color_configs[20].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[20].push_back(0); 
  // JAMP #2
  color_configs[20].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[20].push_back(-1); 
  // JAMP #3
  color_configs[20].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[20].push_back(0); 

  // Color flows of process Process: u c~ > w+ g d c~ WEIGHTED<=5 @8
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[21].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[21].push_back(0); 
  // JAMP #1
  color_configs[21].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[21].push_back(-1); 
  // JAMP #2
  color_configs[21].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[21].push_back(0); 
  // JAMP #3
  color_configs[21].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[21].push_back(-1); 

  // Color flows of process Process: u c~ > w- g u s~ WEIGHTED<=5 @8
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[22].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[22].push_back(0); 
  // JAMP #1
  color_configs[22].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[22].push_back(-1); 
  // JAMP #2
  color_configs[22].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[22].push_back(0); 
  // JAMP #3
  color_configs[22].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[22].push_back(-1); 

  // Color flows of process Process: u d~ > w+ g c c~ WEIGHTED<=5 @8
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[23].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[23].push_back(-1); 
  // JAMP #1
  color_configs[23].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[23].push_back(0); 
  // JAMP #2
  color_configs[23].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[23].push_back(-1); 
  // JAMP #3
  color_configs[23].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[23].push_back(0); 

  // Color flows of process Process: u s~ > w+ g u c~ WEIGHTED<=5 @8
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[24].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[24].push_back(0); 
  // JAMP #1
  color_configs[24].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[24].push_back(-1); 
  // JAMP #2
  color_configs[24].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[24].push_back(0); 
  // JAMP #3
  color_configs[24].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[24].push_back(-1); 

  // Color flows of process Process: d s > w- g u s WEIGHTED<=5 @8
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[25].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[25].push_back(-1); 
  // JAMP #1
  color_configs[25].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[25].push_back(0); 
  // JAMP #2
  color_configs[25].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[25].push_back(0); 
  // JAMP #3
  color_configs[25].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[25].push_back(-1); 

  // Color flows of process Process: d u~ > w- g c c~ WEIGHTED<=5 @8
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[26].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[26].push_back(-1); 
  // JAMP #1
  color_configs[26].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[26].push_back(0); 
  // JAMP #2
  color_configs[26].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[26].push_back(-1); 
  // JAMP #3
  color_configs[26].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[26].push_back(0); 

  // Color flows of process Process: d c~ > w- g u c~ WEIGHTED<=5 @8
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[27].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[27].push_back(0); 
  // JAMP #1
  color_configs[27].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[27].push_back(-1); 
  // JAMP #2
  color_configs[27].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[27].push_back(0); 
  // JAMP #3
  color_configs[27].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[27].push_back(-1); 

  // Color flows of process Process: u~ c~ > w- g u~ s~ WEIGHTED<=5 @8
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[28].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(3)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[28].push_back(-1); 
  // JAMP #1
  color_configs[28].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(3)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[28].push_back(0); 
  // JAMP #2
  color_configs[28].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(3)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[28].push_back(0); 
  // JAMP #3
  color_configs[28].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(3)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[28].push_back(-1); 

  // Color flows of process Process: u~ c~ > w- g c~ d~ WEIGHTED<=5 @8
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[29].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(3)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[29].push_back(0); 
  // JAMP #1
  color_configs[29].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(3)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[29].push_back(-1); 
  // JAMP #2
  color_configs[29].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(3)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[29].push_back(-1); 
  // JAMP #3
  color_configs[29].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(3)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[29].push_back(0); 

  // Color flows of process Process: u~ s~ > w+ g u~ c~ WEIGHTED<=5 @8
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[30].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(3)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[30].push_back(-1); 
  // JAMP #1
  color_configs[30].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(3)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[30].push_back(0); 
  // JAMP #2
  color_configs[30].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(3)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[30].push_back(0); 
  // JAMP #3
  color_configs[30].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(3)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[30].push_back(-1); 

  // Color flows of process Process: d~ s~ > w+ g u~ s~ WEIGHTED<=5 @8
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[31].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(3)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[31].push_back(-1); 
  // JAMP #1
  color_configs[31].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(3)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[31].push_back(0); 
  // JAMP #2
  color_configs[31].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(3)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[31].push_back(0); 
  // JAMP #3
  color_configs[31].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(3)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[31].push_back(-1); 
}

//--------------------------------------------------------------------------
// Destructor.
PY8MEs_R8_P177_sm_qq_wpgqq::~PY8MEs_R8_P177_sm_qq_wpgqq() 
{
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    delete[] p[i]; 
    p[i] = NULL; 
  }
}

//--------------------------------------------------------------------------
// Invert the permutation mapping
vector<int> PY8MEs_R8_P177_sm_qq_wpgqq::invert_mapping(vector<int> mapping) 
{
  vector<int> inverted_mapping; 
  for (unsigned int i = 0; i < mapping.size(); i++ )
  {
    for (unsigned int j = 0; j < mapping.size(); j++ )
    {
      if (mapping[j] == ((int)i))
      {
        inverted_mapping.push_back(j); 
        break; 
      }
    }
  }
  return inverted_mapping; 
}

//--------------------------------------------------------------------------
// Return the list of possible helicity configurations
vector < vec_int > PY8MEs_R8_P177_sm_qq_wpgqq::getHelicityConfigs(vector<int>
    permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(ncomb, vector<int> (nexternal, 0)); 
  for (unsigned int ihel = 0; ihel < ncomb; ihel++ )
  {
    for(unsigned int j = 0; j < nexternal; j++ )
    {
      res[ihel][chosenPerm[j]] = helicities[ihel][j]; 
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Return the list of possible color configurations
vector < vec_int > PY8MEs_R8_P177_sm_qq_wpgqq::getColorConfigs(int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(color_configs[chosenProcID].size(), vector<int>
      (nexternal * 2, 0));
  for (unsigned int icol = 0; icol < color_configs[chosenProcID].size(); icol++
      )
  {
    for(unsigned int j = 0; j < (2 * nexternal); j++ )
    {
      res[icol][chosenPerm[j/2] * 2 + j%2] =
          color_configs[chosenProcID][icol][j];
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Get JAMP relative N_c power
int PY8MEs_R8_P177_sm_qq_wpgqq::getColorFlowRelativeNCPower(int color_flow_ID,
    int specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return jamp_nc_relative_power[chosenProcID][color_flow_ID]; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity ID -> Helicity Config
vector<int> PY8MEs_R8_P177_sm_qq_wpgqq::getHelicityConfigForID(int hel_ID,
    vector<int> permutation)
{
  if (hel_ID < 0 || hel_ID >= ncomb)
  {
    cerr <<  "Error in function 'getHelicityConfigForID' of class" << 
    " 'PY8MEs_R8_P177_sm_qq_wpgqq': Specified helicity ID '" << 
    hel_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R8_P177_sm_qq_wpgqq_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(nexternal, 0); 
  for (unsigned int j = 0; j < nexternal; j++ )
  {
    res[chosenPerm[j]] = helicities[hel_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity Config -> Helicity ID
int PY8MEs_R8_P177_sm_qq_wpgqq::getHelicityIDForConfig(vector<int> hel_config,
    vector<int> permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  int user_ihel = -1; 
  if (hel_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < ncomb; i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < nexternal; j++ )
      {
        if (helicities[i][chosenPerm[j]] != hel_config[j])
        {
          found = false; 
          break; 
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_ihel = i; 
        break; 
      }
    }
    if (user_ihel == -1)
    {
      cerr <<  "Error in function 'getHelicityIDForConfig' of class" << 
      " 'PY8MEs_R8_P177_sm_qq_wpgqq': Specified helicity" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R8_P177_sm_qq_wpgqq_exception; 
    }
  }
  return user_ihel; 
}


//--------------------------------------------------------------------------
// Implements the map Color ID -> Color Config
vector<int> PY8MEs_R8_P177_sm_qq_wpgqq::getColorConfigForID(int color_ID, int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < 0 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getColorConfigForID' of class" << 
    " 'PY8MEs_R8_P177_sm_qq_wpgqq': Specified color ID '" << 
    color_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R8_P177_sm_qq_wpgqq_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(color_configs[chosenProcID][color_ID].size(), 0); 
  for (unsigned int j = 0; j < (2 * nexternal); j++ )
  {
    res[chosenPerm[j/2] * 2 + j%2] = color_configs[chosenProcID][color_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Color Config -> Color ID
int PY8MEs_R8_P177_sm_qq_wpgqq::getColorIDForConfig(vector<int> color_config,
    int specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = -1; 
  if (color_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < color_configs[chosenProcID].size(); i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < (nexternal * 2); j++ )
      {

        // If colorless then make sure it matches
        // The little arithmetics in the color index is just
        // the permutation applies on the particle list which is
        // twice smaller since each particle can have two color indices.
        if (color_config[j] == 0)
        {
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] != 0)
          {
            found = false; 
            break; 
          }
          // Otherwise check that the color linked position matches
        }
        else
        {
          int color_linked_pos = -1; 
          // Find the other end of the line in the user color config
          for (unsigned int k = 0; k < (nexternal * 2); k++ )
          {
            if (k == j)
              continue; 
            if (color_config[j] == color_config[k])
            {
              color_linked_pos = k; 
              break; 
            }
          }
          if (color_linked_pos == -1)
          {
            cerr <<  "Error in function 'getColorIDForConfig' of class" << 
            " 'PY8MEs_R8_P177_sm_qq_wpgqq': A color line could " << 
            " not be closed." << endl; 
            throw PY8MEs_R8_P177_sm_qq_wpgqq_exception; 
          }
          // Now check whether the color line matches
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] !=
              color_configs[chosenProcID][i][chosenPerm[color_linked_pos/2] * 2
              + color_linked_pos%2])
          {
            found = false; 
            break; 
          }
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_icol = i; 
        break; 
      }
    }

    if (user_icol == -1)
    {
      cerr <<  "Error in function 'getColorIDForConfig' of class" << 
      " 'PY8MEs_R8_P177_sm_qq_wpgqq': Specified color" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R8_P177_sm_qq_wpgqq_exception; 
    }
  }
  return user_icol; 
}

//--------------------------------------------------------------------------
// Returns all result previously computed in SigmaKin
vector < vec_double > PY8MEs_R8_P177_sm_qq_wpgqq::getAllResults(int
    specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return all_results[chosenProcID]; 
}

//--------------------------------------------------------------------------
// Returns a result previously computed in SigmaKin for a specific helicity
// and color ID. -1 means avg and summed over that characteristic.
double PY8MEs_R8_P177_sm_qq_wpgqq::getResult(int helicity_ID, int color_ID, int
    specify_proc_ID)
{
  if (helicity_ID < - 1 || helicity_ID >= ncomb)
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R8_P177_sm_qq_wpgqq': Specified helicity ID '" << 
    helicity_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R8_P177_sm_qq_wpgqq_exception; 
  }
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < - 1 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R8_P177_sm_qq_wpgqq': Specified color ID '" << 
    color_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R8_P177_sm_qq_wpgqq_exception; 
  }
  return all_results[chosenProcID][helicity_ID + 1][color_ID + 1]; 
}

//--------------------------------------------------------------------------
// Check for the availability of the requested process and if available,
// If available, this returns the corresponding permutation and Proc_ID to use.
// If not available, this returns a negative Proc_ID.
pair < vector<int> , int > PY8MEs_R8_P177_sm_qq_wpgqq::static_getPY8ME(vector<int> initial_pdgs, vector<int> final_pdgs, set<int> schannels) 
{

  // Not available return value
  pair < vector<int> , int > NA(vector<int> (), -1); 

  // Check if s-channel requirements match
  if (nreq_s_channels > 0)
  {
    if (schannels != s_channel_proc)
      return NA; 
  }
  else
  {
    if (schannels.size() != 0)
      return NA; 
  }

  // Check number of final state particles
  if (final_pdgs.size() != (nexternal - ninitial))
    return NA; 

  // Check number of initial state particles
  if (initial_pdgs.size() != ninitial)
    return NA; 

  // List of processes available in this class
  const int nprocs = 152; 
  const int proc_IDS[nprocs] = {0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7,
      8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13, 14, 14, 15, 15, 16, 17, 17,
      17, 18, 18, 18, 19, 19, 19, 19, 20, 20, 20, 20, 21, 21, 21, 21, 22, 22,
      22, 22, 23, 23, 23, 23, 24, 24, 24, 24, 25, 26, 26, 26, 26, 27, 27, 27,
      27, 28, 29, 29, 29, 30, 30, 30, 31, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6,
      8, 8, 9, 9, 10, 10, 11, 11, 13, 13, 14, 14, 16, 17, 17, 17, 18, 18, 18,
      19, 19, 19, 19, 20, 20, 20, 20, 21, 21, 21, 21, 22, 22, 22, 22, 23, 23,
      23, 23, 24, 24, 24, 24, 25, 26, 26, 26, 26, 27, 27, 27, 27, 28, 29, 29,
      29, 30, 30, 30, 31};
  const int in_pdgs[nprocs][ninitial] = {{2, 2}, {4, 4}, {2, 1}, {4, 3}, {2,
      1}, {4, 3}, {2, -2}, {4, -4}, {2, -2}, {4, -4}, {2, -1}, {4, -3}, {2,
      -1}, {4, -3}, {1, 1}, {3, 3}, {1, -2}, {3, -4}, {1, -2}, {3, -4}, {1,
      -1}, {3, -3}, {1, -1}, {3, -3}, {-2, -2}, {-4, -4}, {-2, -1}, {-4, -3},
      {-2, -1}, {-4, -3}, {-1, -1}, {-3, -3}, {2, 4}, {2, 4}, {2, 3}, {4, 1},
      {2, 3}, {4, 1}, {1, 3}, {2, -2}, {4, -4}, {1, -1}, {3, -3}, {2, -2}, {4,
      -4}, {1, -1}, {3, -3}, {2, -4}, {2, -3}, {4, -2}, {4, -1}, {2, -4}, {4,
      -2}, {1, -4}, {3, -2}, {2, -1}, {2, -1}, {4, -3}, {4, -3}, {2, -3}, {4,
      -1}, {1, -3}, {3, -1}, {1, 3}, {1, -2}, {1, -2}, {3, -4}, {3, -4}, {1,
      -4}, {1, -3}, {3, -2}, {3, -1}, {-2, -4}, {-2, -4}, {-2, -3}, {-4, -1},
      {-2, -3}, {-4, -1}, {-1, -3}, {-1, -3}, {1, 2}, {3, 4}, {1, 2}, {3, 4},
      {-2, 2}, {-4, 4}, {-2, 2}, {-4, 4}, {-1, 2}, {-3, 4}, {-1, 2}, {-3, 4},
      {-2, 1}, {-4, 3}, {-2, 1}, {-4, 3}, {-1, 1}, {-3, 3}, {-1, 1}, {-3, 3},
      {-1, -2}, {-3, -4}, {-1, -2}, {-3, -4}, {4, 2}, {4, 2}, {3, 2}, {1, 4},
      {3, 2}, {1, 4}, {3, 1}, {-2, 2}, {-4, 4}, {-1, 1}, {-3, 3}, {-2, 2}, {-4,
      4}, {-1, 1}, {-3, 3}, {-4, 2}, {-3, 2}, {-2, 4}, {-1, 4}, {-4, 2}, {-2,
      4}, {-4, 1}, {-2, 3}, {-1, 2}, {-1, 2}, {-3, 4}, {-3, 4}, {-3, 2}, {-1,
      4}, {-3, 1}, {-1, 3}, {3, 1}, {-2, 1}, {-2, 1}, {-4, 3}, {-4, 3}, {-4,
      1}, {-3, 1}, {-2, 3}, {-1, 3}, {-4, -2}, {-4, -2}, {-3, -2}, {-1, -4},
      {-3, -2}, {-1, -4}, {-3, -1}, {-3, -1}};
  const int out_pdgs[nprocs][nexternal - ninitial] = {{24, 21, 2, 1}, {24, 21,
      4, 3}, {24, 21, 1, 1}, {24, 21, 3, 3}, {-24, 21, 2, 2}, {-24, 21, 4, 4},
      {24, 21, 1, -2}, {24, 21, 3, -4}, {-24, 21, 2, -1}, {-24, 21, 4, -3},
      {24, 21, 2, -2}, {24, 21, 4, -4}, {24, 21, 1, -1}, {24, 21, 3, -3}, {-24,
      21, 2, 1}, {-24, 21, 4, 3}, {-24, 21, 2, -2}, {-24, 21, 4, -4}, {-24, 21,
      1, -1}, {-24, 21, 3, -3}, {24, 21, 1, -2}, {24, 21, 3, -4}, {-24, 21, 2,
      -1}, {-24, 21, 4, -3}, {-24, 21, -2, -1}, {-24, 21, -4, -3}, {24, 21, -2,
      -2}, {24, 21, -4, -4}, {-24, 21, -1, -1}, {-24, 21, -3, -3}, {24, 21, -2,
      -1}, {24, 21, -4, -3}, {24, 21, 2, 3}, {24, 21, 4, 1}, {24, 21, 3, 1},
      {24, 21, 1, 3}, {-24, 21, 2, 4}, {-24, 21, 4, 2}, {-24, 21, 1, 4}, {24,
      21, 3, -4}, {24, 21, 1, -2}, {24, 21, 3, -4}, {24, 21, 1, -2}, {-24, 21,
      4, -3}, {-24, 21, 2, -1}, {-24, 21, 4, -3}, {-24, 21, 2, -1}, {24, 21, 1,
      -4}, {24, 21, 1, -3}, {24, 21, 3, -2}, {24, 21, 3, -1}, {-24, 21, 2, -3},
      {-24, 21, 4, -1}, {-24, 21, 1, -3}, {-24, 21, 3, -1}, {24, 21, 4, -4},
      {24, 21, 3, -3}, {24, 21, 2, -2}, {24, 21, 1, -1}, {24, 21, 2, -4}, {24,
      21, 4, -2}, {24, 21, 1, -4}, {24, 21, 3, -2}, {-24, 21, 2, 3}, {-24, 21,
      4, -4}, {-24, 21, 3, -3}, {-24, 21, 2, -2}, {-24, 21, 1, -1}, {-24, 21,
      2, -4}, {-24, 21, 2, -3}, {-24, 21, 4, -2}, {-24, 21, 4, -1}, {-24, 21,
      -2, -3}, {-24, 21, -4, -1}, {-24, 21, -3, -1}, {-24, 21, -1, -3}, {24,
      21, -2, -4}, {24, 21, -4, -2}, {24, 21, -1, -4}, {24, 21, -2, -3}, {24,
      21, 1, 1}, {24, 21, 3, 3}, {-24, 21, 2, 2}, {-24, 21, 4, 4}, {24, 21, 1,
      -2}, {24, 21, 3, -4}, {-24, 21, 2, -1}, {-24, 21, 4, -3}, {24, 21, 2,
      -2}, {24, 21, 4, -4}, {24, 21, 1, -1}, {24, 21, 3, -3}, {-24, 21, 2, -2},
      {-24, 21, 4, -4}, {-24, 21, 1, -1}, {-24, 21, 3, -3}, {24, 21, 1, -2},
      {24, 21, 3, -4}, {-24, 21, 2, -1}, {-24, 21, 4, -3}, {24, 21, -2, -2},
      {24, 21, -4, -4}, {-24, 21, -1, -1}, {-24, 21, -3, -3}, {24, 21, 2, 3},
      {24, 21, 4, 1}, {24, 21, 3, 1}, {24, 21, 1, 3}, {-24, 21, 2, 4}, {-24,
      21, 4, 2}, {-24, 21, 1, 4}, {24, 21, 3, -4}, {24, 21, 1, -2}, {24, 21, 3,
      -4}, {24, 21, 1, -2}, {-24, 21, 4, -3}, {-24, 21, 2, -1}, {-24, 21, 4,
      -3}, {-24, 21, 2, -1}, {24, 21, 1, -4}, {24, 21, 1, -3}, {24, 21, 3, -2},
      {24, 21, 3, -1}, {-24, 21, 2, -3}, {-24, 21, 4, -1}, {-24, 21, 1, -3},
      {-24, 21, 3, -1}, {24, 21, 4, -4}, {24, 21, 3, -3}, {24, 21, 2, -2}, {24,
      21, 1, -1}, {24, 21, 2, -4}, {24, 21, 4, -2}, {24, 21, 1, -4}, {24, 21,
      3, -2}, {-24, 21, 2, 3}, {-24, 21, 4, -4}, {-24, 21, 3, -3}, {-24, 21, 2,
      -2}, {-24, 21, 1, -1}, {-24, 21, 2, -4}, {-24, 21, 2, -3}, {-24, 21, 4,
      -2}, {-24, 21, 4, -1}, {-24, 21, -2, -3}, {-24, 21, -4, -1}, {-24, 21,
      -3, -1}, {-24, 21, -1, -3}, {24, 21, -2, -4}, {24, 21, -4, -2}, {24, 21,
      -1, -4}, {24, 21, -2, -3}};

  bool in_pdgs_used[ninitial]; 
  bool out_pdgs_used[nexternal - ninitial]; 
  for(unsigned int i = 0; i < nprocs; i++ )
  {
    int permutations[nexternal]; 

    // Reinitialize initial state look-up variables
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      in_pdgs_used[j] = false; 
      permutations[j] = -1; 
    }
    // Look for initial state matches
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      for(unsigned int k = 0; k < ninitial; k++ )
      {
        // Make sure it has not been used already
        if (in_pdgs_used[k])
          continue; 
        if (initial_pdgs[k] == in_pdgs[i][j])
        {
          permutations[j] = k; 
          in_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[ninitial - 1] == -1)
      continue; 

    // Reinitialize final state look-up variables
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      out_pdgs_used[j] = false; 
      permutations[ninitial + j] = -1; 
    }
    // Look for final state matches
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      for(unsigned int k = 0; k < (nexternal - ninitial); k++ )
      {
        // Make sure it has not been used already
        if (out_pdgs_used[k])
          continue; 
        if (final_pdgs[k] == out_pdgs[i][j])
        {
          permutations[ninitial + j] = ninitial + k; 
          out_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[ninitial + j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[nexternal - 1] == -1)
      continue; 

    // Return process found
    return pair < vector<int> , int > (vector<int> (permutations, permutations
        + nexternal), proc_IDS[i]);
  }

  // No process found
  return NA; 
}

//--------------------------------------------------------------------------
// Set momenta
void PY8MEs_R8_P177_sm_qq_wpgqq::setMomenta(vector < vec_double >
    momenta_picked)
{
  if (momenta_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setMomenta' of class" << 
    " 'PY8MEs_R8_P177_sm_qq_wpgqq': Incorrect number of" << 
    " momenta specified." << endl; 
    throw PY8MEs_R8_P177_sm_qq_wpgqq_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    if (momenta_picked[i].size() != 4)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R8_P177_sm_qq_wpgqq': Incorrect number of" << 
      " momenta components specified." << endl; 
      throw PY8MEs_R8_P177_sm_qq_wpgqq_exception; 
    }
    if (momenta_picked[i][0] < 0.0)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R8_P177_sm_qq_wpgqq': A momentum was specified" << 
      " with negative energy. Check conventions." << endl; 
      throw PY8MEs_R8_P177_sm_qq_wpgqq_exception; 
    }
    for (unsigned int j = 0; j < 4; j++ )
    {
      p[i][j] = momenta_picked[i][j]; 
    }
  }
}

//--------------------------------------------------------------------------
// Set color configuration to use. An empty vector means sum over all.
void PY8MEs_R8_P177_sm_qq_wpgqq::setColors(vector<int> colors_picked)
{
  if (colors_picked.size() == 0)
  {
    user_colors = vector<int> (); 
    return; 
  }
  if (colors_picked.size() != (2 * nexternal))
  {
    cerr <<  "Error in function 'setColors' of class" << 
    " 'PY8MEs_R8_P177_sm_qq_wpgqq': Incorrect number" << 
    " of colors specified." << endl; 
    throw PY8MEs_R8_P177_sm_qq_wpgqq_exception; 
  }
  user_colors = vector<int> ((2 * nexternal), 0); 
  for(unsigned int i = 0; i < (2 * nexternal); i++ )
  {
    user_colors[i] = colors_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the helicity configuration to use. Am empty vector means sum over all.
void PY8MEs_R8_P177_sm_qq_wpgqq::setHelicities(vector<int> helicities_picked) 
{
  if (helicities_picked.size() != nexternal)
  {
    if (helicities_picked.size() == 0)
    {
      user_helicities = vector<int> (); 
      return; 
    }
    cerr <<  "Error in function 'setHelicities' of class" << 
    " 'PY8MEs_R8_P177_sm_qq_wpgqq': Incorrect number" << 
    " of helicities specified." << endl; 
    throw PY8MEs_R8_P177_sm_qq_wpgqq_exception; 
  }
  user_helicities = vector<int> (nexternal, 0); 
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    user_helicities[i] = helicities_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the permutation to use (will apply to momenta, colors and helicities)
void PY8MEs_R8_P177_sm_qq_wpgqq::setPermutation(vector<int> perm_picked) 
{
  if (perm_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setPermutations' of class" << 
    " 'PY8MEs_R8_P177_sm_qq_wpgqq': Incorrect number" << 
    " of permutations specified." << endl; 
    throw PY8MEs_R8_P177_sm_qq_wpgqq_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    perm[i] = perm_picked[i]; 
  }
}

// Set the proc_ID to use
void PY8MEs_R8_P177_sm_qq_wpgqq::setProcID(int procID_picked) 
{
  proc_ID = procID_picked; 
}

//--------------------------------------------------------------------------
// Initialize process.

void PY8MEs_R8_P177_sm_qq_wpgqq::initProc() 
{

  // Initialize flags
  include_symmetry_factors = true; 
  include_helicity_averaging_factors = true; 
  include_color_averaging_factors = true; 

  // Initialize vectors.
  perm = vector<int> (nexternal, 0); 
  user_colors = vector<int> (2 * nexternal, 0); 
  user_helicities = vector<int> (nexternal, 0); 
  p = vector < double * > (); 
  for (unsigned int i = 0; i < nexternal; i++ )
  {
    p.push_back(new double[4]); 
  }
  initColorConfigs(); 
  // Synchronize local variables dependent on the model with the active model.
  mME = vector<double> (nexternal, 0.); 
  syncProcModelParams(); 
  jamp2 = vector < vec_double > (32); 
  jamp2[0] = vector<double> (4, 0.); 
  jamp2[1] = vector<double> (4, 0.); 
  jamp2[2] = vector<double> (4, 0.); 
  jamp2[3] = vector<double> (4, 0.); 
  jamp2[4] = vector<double> (4, 0.); 
  jamp2[5] = vector<double> (4, 0.); 
  jamp2[6] = vector<double> (4, 0.); 
  jamp2[7] = vector<double> (4, 0.); 
  jamp2[8] = vector<double> (4, 0.); 
  jamp2[9] = vector<double> (4, 0.); 
  jamp2[10] = vector<double> (4, 0.); 
  jamp2[11] = vector<double> (4, 0.); 
  jamp2[12] = vector<double> (4, 0.); 
  jamp2[13] = vector<double> (4, 0.); 
  jamp2[14] = vector<double> (4, 0.); 
  jamp2[15] = vector<double> (4, 0.); 
  jamp2[16] = vector<double> (4, 0.); 
  jamp2[17] = vector<double> (4, 0.); 
  jamp2[18] = vector<double> (4, 0.); 
  jamp2[19] = vector<double> (4, 0.); 
  jamp2[20] = vector<double> (4, 0.); 
  jamp2[21] = vector<double> (4, 0.); 
  jamp2[22] = vector<double> (4, 0.); 
  jamp2[23] = vector<double> (4, 0.); 
  jamp2[24] = vector<double> (4, 0.); 
  jamp2[25] = vector<double> (4, 0.); 
  jamp2[26] = vector<double> (4, 0.); 
  jamp2[27] = vector<double> (4, 0.); 
  jamp2[28] = vector<double> (4, 0.); 
  jamp2[29] = vector<double> (4, 0.); 
  jamp2[30] = vector<double> (4, 0.); 
  jamp2[31] = vector<double> (4, 0.); 
  all_results = vector < vec_vec_double > (32); 
  // The first entry is always the color or helicity avg/summed matrix element.
  all_results[0] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[1] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[2] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[3] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[4] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[5] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[6] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[7] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[8] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[9] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[10] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[11] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[12] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[13] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[14] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[15] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[16] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[17] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[18] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[19] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[20] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[21] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[22] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[23] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[24] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[25] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[26] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[27] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[28] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[29] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[30] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[31] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
}

// Synchronize local variables of the process that depend on the model
// parameters
void PY8MEs_R8_P177_sm_qq_wpgqq::syncProcModelParams() 
{

  // Instantiate the model class and set parameters that stay fixed during run
  mME[0] = pars->ZERO; 
  mME[1] = pars->ZERO; 
  mME[2] = pars->mdl_MW; 
  mME[3] = pars->ZERO; 
  mME[4] = pars->ZERO; 
  mME[5] = pars->ZERO; 
}

//--------------------------------------------------------------------------
// Setter allowing to force particular values for the external masses
void PY8MEs_R8_P177_sm_qq_wpgqq::setMasses(vec_double external_masses) 
{

  if (external_masses.size() != mME.size())
  {
    cerr <<  "Error in function 'setMasses' of class" << 
    " 'PY8MEs_R8_P177_sm_qq_wpgqq': Incorrect number of" << 
    " masses specified." << endl; 
    throw PY8MEs_R8_P177_sm_qq_wpgqq_exception; 
  }
  for (unsigned int j = 0; j < mME.size(); j++ )
  {
    mME[j] = external_masses[perm[j]]; 
  }
}

//--------------------------------------------------------------------------
// Getter accessing external masses with the correct ordering
vector<double> PY8MEs_R8_P177_sm_qq_wpgqq::getMasses() 
{

  vec_double external_masses; 
  vector<int> invertedPerm; 
  invertedPerm = invert_mapping(perm); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    external_masses.push_back(mME[invertedPerm[i]]); 
  }
  return external_masses; 

}


// Set all values of the external masses to float(-mode) where mode can be
// 0 : Mass taken from the model
// 1 : Mass taken from p_i^2 if not massless to begin with
// 2 : Mass always taken from p_i^2.
void PY8MEs_R8_P177_sm_qq_wpgqq::setExternalMassesMode(int mode) 
{
  if (mode != 0 && mode != 1 && mode != 2)
  {
    cerr <<  "Error in function 'setExternalMassesMode' of class" << 
    " 'PY8MEs_R8_P177_sm_qq_wpgqq': Incorrect mode selected :" << mode << 
    ". It must be either 0, 1 or 2" << endl; 
    throw PY8MEs_R8_P177_sm_qq_wpgqq_exception; 
  }
  if (mode == 0)
  {
    syncProcModelParams(); 
  }
  else if (mode == 1)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      if (mME[j] != pars->ZERO)
      {
        mME[j] = -1.0; 
      }
    }
  }
  else if (mode == 2)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      mME[j] = -1.0; 
    }
  }
}

//--------------------------------------------------------------------------
// Evaluate the squared matrix element.

double PY8MEs_R8_P177_sm_qq_wpgqq::sigmaKin() 
{
  // Set the parameters which change event by event
  pars->setDependentParameters(); 
  pars->setDependentCouplings(); 
  // Reset color flows
  for(int i = 0; i < 4; i++ )
    jamp2[0][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[1][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[2][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[3][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[4][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[5][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[6][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[7][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[8][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[9][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[10][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[11][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[12][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[13][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[14][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[15][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[16][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[17][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[18][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[19][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[20][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[21][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[22][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[23][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[24][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[25][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[26][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[27][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[28][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[29][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[30][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[31][i] = 0.; 

  // Save previous values of mME
  vector<double> saved_mME(mME.size(), 0.0); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (mME[i] < 0.0)
    {
      saved_mME[i] = mME[i]; 
      mME[i] = sqrt(abs(pow(p[perm[i]][0], 2) - 
      (pow(p[perm[i]][1], 2) + pow(p[perm[i]][2], 2) + pow(p[perm[i]][3],
          2))));
    }
  }

  // Local variables and constants
  const int max_tries = 10; 
  vector < vec_bool > goodhel(nprocesses, vec_bool(ncomb, false)); 
  vec_int ntry(nprocesses, 0); 
  double t = 0.; 
  double result = 0.; 

  if (ntry[proc_ID] <= max_tries)
    ntry[proc_ID] = ntry[proc_ID] + 1; 

  // Find which helicity configuration is asked for
  // -1 indicates one wants to sum over helicities
  int user_ihel = getHelicityIDForConfig(user_helicities); 

  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = getColorIDForConfig(user_colors); 

  // Reset the list of results that will be recomputed here
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      all_results[proc_ID][ihel + 1][icolor + 1] = 0.; 
    }
  }

  // Calculate the matrix element for all helicities
  // unless already detected as vanishing
  for(int ihel = 0; ihel < ncomb; ihel++ )
  {
    // Skip helicity if already detected as vanishing
    if ((ntry[proc_ID] >= max_tries) && !goodhel[proc_ID][ihel])
      continue; 

    // Also skip helicity if user asks for a specific one
    if ((ntry[proc_ID] >= max_tries) && user_ihel != -1 && user_ihel != ihel)
      continue; 

    calculate_wavefunctions(helicities[ihel]); 

    // Reset locally computed color flows
    for(int i = 0; i < 4; i++ )
      jamp2[0][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[1][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[2][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[3][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[4][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[5][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[6][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[7][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[8][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[9][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[10][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[11][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[12][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[13][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[14][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[15][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[16][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[17][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[18][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[19][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[20][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[21][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[22][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[23][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[24][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[25][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[26][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[27][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[28][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[29][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[30][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[31][i] = 0.; 

    if (proc_ID == 0)
      t = matrix_8_uu_wpgud(); 
    if (proc_ID == 1)
      t = matrix_8_ud_wpgdd(); 
    if (proc_ID == 2)
      t = matrix_8_ud_wmguu(); 
    if (proc_ID == 3)
      t = matrix_8_uux_wpgdux(); 
    if (proc_ID == 4)
      t = matrix_8_uux_wmgudx(); 
    if (proc_ID == 5)
      t = matrix_8_udx_wpguux(); 
    if (proc_ID == 6)
      t = matrix_8_udx_wpgddx(); 
    if (proc_ID == 7)
      t = matrix_8_dd_wmgud(); 
    if (proc_ID == 8)
      t = matrix_8_dux_wmguux(); 
    if (proc_ID == 9)
      t = matrix_8_dux_wmgddx(); 
    if (proc_ID == 10)
      t = matrix_8_ddx_wpgdux(); 
    if (proc_ID == 11)
      t = matrix_8_ddx_wmgudx(); 
    if (proc_ID == 12)
      t = matrix_8_uxux_wmguxdx(); 
    if (proc_ID == 13)
      t = matrix_8_uxdx_wpguxux(); 
    if (proc_ID == 14)
      t = matrix_8_uxdx_wmgdxdx(); 
    if (proc_ID == 15)
      t = matrix_8_dxdx_wpguxdx(); 
    if (proc_ID == 16)
      t = matrix_8_uc_wpgus(); 
    if (proc_ID == 17)
      t = matrix_8_uc_wpgcd(); 
    if (proc_ID == 18)
      t = matrix_8_us_wmguc(); 
    if (proc_ID == 19)
      t = matrix_8_uux_wpgscx(); 
    if (proc_ID == 20)
      t = matrix_8_uux_wmgcsx(); 
    if (proc_ID == 21)
      t = matrix_8_ucx_wpgdcx(); 
    if (proc_ID == 22)
      t = matrix_8_ucx_wmgusx(); 
    if (proc_ID == 23)
      t = matrix_8_udx_wpgccx(); 
    if (proc_ID == 24)
      t = matrix_8_usx_wpgucx(); 
    if (proc_ID == 25)
      t = matrix_8_ds_wmgus(); 
    if (proc_ID == 26)
      t = matrix_8_dux_wmgccx(); 
    if (proc_ID == 27)
      t = matrix_8_dcx_wmgucx(); 
    if (proc_ID == 28)
      t = matrix_8_uxcx_wmguxsx(); 
    if (proc_ID == 29)
      t = matrix_8_uxcx_wmgcxdx(); 
    if (proc_ID == 30)
      t = matrix_8_uxsx_wpguxcx(); 
    if (proc_ID == 31)
      t = matrix_8_dxsx_wpguxsx(); 

    // Store which helicities give non-zero result
    if ((ntry[proc_ID] < max_tries) && t != 0. && !goodhel[proc_ID][ihel])
      goodhel[proc_ID][ihel] = true; 

    // Aggregate results
    if (user_ihel == -1 || user_ihel == ihel)
    {
      if (user_icol == -1)
      {
        result = result + t; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][0] += t; 
          for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
          {
            all_results[proc_ID][0][i + 1] += jamp2[proc_ID][i]; 
          }
        }
        all_results[proc_ID][ihel + 1][0] += t; 
        for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
        {
          all_results[proc_ID][ihel + 1][i + 1] += jamp2[proc_ID][i]; 
        }
      }
      else
      {
        result = result + jamp2[proc_ID][user_icol]; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][user_icol + 1] += jamp2[proc_ID][user_icol]; 
        }
        all_results[proc_ID][ihel + 1][user_icol + 1] +=
            jamp2[proc_ID][user_icol];
      }
    }

  }

  // Normalize results with the identical particle factor
  if (include_symmetry_factors)
  {
    result = result/denom_iden[proc_ID]; 
  }
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      if (include_symmetry_factors)
      {
        all_results[proc_ID][ihel + 1][icolor + 1] /= denom_iden[proc_ID]; 
      }
    }
  }


  // Normalize when when summing+averaging over helicity configurations
  if (user_ihel == -1 && include_helicity_averaging_factors)
  {
    result /= denom_hels[proc_ID]; 
    if (user_icol == -1)
    {
      all_results[proc_ID][0][0] /= denom_hels[proc_ID]; 
      for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
      {
        all_results[proc_ID][0][i + 1] /= denom_hels[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][0][user_icol + 1] /= denom_hels[proc_ID]; 
    }
  }

  // Normalize when summing+averaging over color configurations
  if (user_icol == -1 && include_color_averaging_factors)
  {
    result /= denom_colors[proc_ID]; 
    if (user_ihel == -1)
    {
      all_results[proc_ID][0][0] /= denom_colors[proc_ID]; 
      for (unsigned int i = 0; i < ncomb; i++ )
      {
        all_results[proc_ID][i + 1][0] /= denom_colors[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][user_ihel + 1][0] /= denom_colors[proc_ID]; 
    }
  }

  // Reinstate previous values of mME
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (saved_mME[i] < 0.0)
    {
      mME[i] = saved_mME[i]; 
    }
  }

  // Finally return it
  return result; 
}

//==========================================================================
// Private class member functions

//--------------------------------------------------------------------------
// Evaluate |M|^2 for each subprocess

void PY8MEs_R8_P177_sm_qq_wpgqq::calculate_wavefunctions(const int hel[])
{
  // Calculate wavefunctions for all processes
  // Calculate all wavefunctions
  ixxxxx(p[perm[0]], mME[0], hel[0], +1, w[0]); 
  ixxxxx(p[perm[1]], mME[1], hel[1], +1, w[1]); 
  vxxxxx(p[perm[2]], mME[2], hel[2], +1, w[2]); 
  vxxxxx(p[perm[3]], mME[3], hel[3], +1, w[3]); 
  oxxxxx(p[perm[4]], mME[4], hel[4], +1, w[4]); 
  oxxxxx(p[perm[5]], mME[5], hel[5], +1, w[5]); 
  FFV1_2(w[0], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[6]); 
  FFV2_1(w[5], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[7]); 
  FFV1P0_3(w[6], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[8]); 
  FFV1P0_3(w[1], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[9]); 
  FFV2_2(w[1], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[10]); 
  FFV2_2(w[6], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[11]); 
  FFV1_1(w[4], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[12]); 
  FFV2_2(w[0], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[13]); 
  FFV1P0_3(w[1], w[12], pars->GC_11, pars->ZERO, pars->ZERO, w[14]); 
  FFV1P0_3(w[0], w[12], pars->GC_11, pars->ZERO, pars->ZERO, w[15]); 
  FFV1_1(w[5], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[16]); 
  FFV1P0_3(w[0], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[17]); 
  FFV2_1(w[16], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[18]); 
  FFV1_2(w[1], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[19]); 
  FFV1P0_3(w[19], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[20]); 
  FFV2_2(w[19], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[21]); 
  FFV1_2(w[13], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[22]); 
  VVV1P0_1(w[3], w[9], pars->GC_10, pars->ZERO, pars->ZERO, w[23]); 
  VVV1P0_1(w[3], w[17], pars->GC_10, pars->ZERO, pars->ZERO, w[24]); 
  FFV1_1(w[7], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[25]); 
  FFV1_2(w[10], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[26]); 
  FFV2_1(w[4], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[27]); 
  FFV1P0_3(w[1], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[28]); 
  FFV2_1(w[12], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[29]); 
  FFV1P0_3(w[1], w[16], pars->GC_11, pars->ZERO, pars->ZERO, w[30]); 
  FFV1P0_3(w[19], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[31]); 
  VVV1P0_1(w[3], w[28], pars->GC_10, pars->ZERO, pars->ZERO, w[32]); 
  FFV1_1(w[27], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[33]); 
  FFV1P0_3(w[6], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[34]); 
  FFV1P0_3(w[0], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[35]); 
  FFV1P0_3(w[0], w[16], pars->GC_11, pars->ZERO, pars->ZERO, w[36]); 
  VVV1P0_1(w[3], w[35], pars->GC_10, pars->ZERO, pars->ZERO, w[37]); 
  oxxxxx(p[perm[1]], mME[1], hel[1], -1, w[38]); 
  ixxxxx(p[perm[5]], mME[5], hel[5], -1, w[39]); 
  FFV1P0_3(w[6], w[38], pars->GC_11, pars->ZERO, pars->ZERO, w[40]); 
  FFV1P0_3(w[39], w[38], pars->GC_11, pars->ZERO, pars->ZERO, w[41]); 
  FFV2_2(w[39], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[42]); 
  FFV1_1(w[38], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[43]); 
  FFV1P0_3(w[39], w[43], pars->GC_11, pars->ZERO, pars->ZERO, w[44]); 
  FFV1P0_3(w[0], w[43], pars->GC_11, pars->ZERO, pars->ZERO, w[45]); 
  FFV1P0_3(w[0], w[38], pars->GC_11, pars->ZERO, pars->ZERO, w[46]); 
  FFV1_2(w[39], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[47]); 
  FFV1P0_3(w[47], w[38], pars->GC_11, pars->ZERO, pars->ZERO, w[48]); 
  FFV2_2(w[47], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[49]); 
  VVV1P0_1(w[3], w[41], pars->GC_10, pars->ZERO, pars->ZERO, w[50]); 
  VVV1P0_1(w[3], w[46], pars->GC_10, pars->ZERO, pars->ZERO, w[51]); 
  FFV1_2(w[42], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[52]); 
  FFV2_1(w[38], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[53]); 
  FFV2_1(w[43], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[54]); 
  FFV1_1(w[53], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[55]); 
  FFV1P0_3(w[39], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[56]); 
  FFV1P0_3(w[39], w[12], pars->GC_11, pars->ZERO, pars->ZERO, w[57]); 
  FFV1P0_3(w[47], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[58]); 
  VVV1P0_1(w[3], w[56], pars->GC_10, pars->ZERO, pars->ZERO, w[59]); 
  oxxxxx(p[perm[0]], mME[0], hel[0], -1, w[60]); 
  ixxxxx(p[perm[4]], mME[4], hel[4], -1, w[61]); 
  FFV1_2(w[61], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[62]); 
  FFV2_1(w[60], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[63]); 
  FFV1P0_3(w[62], w[38], pars->GC_11, pars->ZERO, pars->ZERO, w[64]); 
  FFV1P0_3(w[62], w[60], pars->GC_11, pars->ZERO, pars->ZERO, w[65]); 
  FFV1_1(w[60], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[66]); 
  FFV1P0_3(w[61], w[38], pars->GC_11, pars->ZERO, pars->ZERO, w[67]); 
  FFV2_1(w[66], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[68]); 
  FFV1P0_3(w[61], w[66], pars->GC_11, pars->ZERO, pars->ZERO, w[69]); 
  FFV1P0_3(w[61], w[60], pars->GC_11, pars->ZERO, pars->ZERO, w[70]); 
  FFV1P0_3(w[61], w[43], pars->GC_11, pars->ZERO, pars->ZERO, w[71]); 
  VVV1P0_1(w[3], w[70], pars->GC_10, pars->ZERO, pars->ZERO, w[72]); 
  VVV1P0_1(w[3], w[67], pars->GC_10, pars->ZERO, pars->ZERO, w[73]); 
  FFV1_1(w[63], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[74]); 
  FFV1P0_3(w[39], w[60], pars->GC_11, pars->ZERO, pars->ZERO, w[75]); 
  FFV2_2(w[62], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[76]); 
  FFV2_2(w[61], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[77]); 
  FFV1P0_3(w[39], w[66], pars->GC_11, pars->ZERO, pars->ZERO, w[78]); 
  FFV1P0_3(w[47], w[60], pars->GC_11, pars->ZERO, pars->ZERO, w[79]); 
  FFV1_2(w[77], w[3], pars->GC_11, pars->ZERO, pars->ZERO, w[80]); 
  VVV1P0_1(w[3], w[75], pars->GC_10, pars->ZERO, pars->ZERO, w[81]); 

  // Calculate all amplitudes
  // Amplitude(s) for diagram number 0
  FFV1_0(w[1], w[7], w[8], pars->GC_11, amp[0]); 
  FFV1_0(w[6], w[7], w[9], pars->GC_11, amp[1]); 
  FFV1_0(w[10], w[5], w[8], pars->GC_11, amp[2]); 
  FFV1_0(w[11], w[5], w[9], pars->GC_11, amp[3]); 
  FFV1_0(w[13], w[5], w[14], pars->GC_11, amp[4]); 
  FFV1_0(w[1], w[7], w[15], pars->GC_11, amp[5]); 
  FFV1_0(w[0], w[7], w[14], pars->GC_11, amp[6]); 
  FFV1_0(w[10], w[5], w[15], pars->GC_11, amp[7]); 
  FFV1_0(w[13], w[16], w[9], pars->GC_11, amp[8]); 
  FFV1_0(w[1], w[18], w[17], pars->GC_11, amp[9]); 
  FFV1_0(w[10], w[16], w[17], pars->GC_11, amp[10]); 
  FFV1_0(w[0], w[18], w[9], pars->GC_11, amp[11]); 
  FFV1_0(w[13], w[5], w[20], pars->GC_11, amp[12]); 
  FFV1_0(w[21], w[5], w[17], pars->GC_11, amp[13]); 
  FFV1_0(w[19], w[7], w[17], pars->GC_11, amp[14]); 
  FFV1_0(w[0], w[7], w[20], pars->GC_11, amp[15]); 
  FFV1_0(w[22], w[5], w[9], pars->GC_11, amp[16]); 
  FFV1_0(w[13], w[5], w[23], pars->GC_11, amp[17]); 
  FFV1_0(w[1], w[7], w[24], pars->GC_11, amp[18]); 
  FFV1_0(w[1], w[25], w[17], pars->GC_11, amp[19]); 
  FFV1_0(w[10], w[5], w[24], pars->GC_11, amp[20]); 
  FFV1_0(w[26], w[5], w[17], pars->GC_11, amp[21]); 
  FFV1_0(w[0], w[25], w[9], pars->GC_11, amp[22]); 
  FFV1_0(w[0], w[7], w[23], pars->GC_11, amp[23]); 
  FFV1_0(w[6], w[27], w[28], pars->GC_11, amp[24]); 
  FFV1_0(w[11], w[5], w[9], pars->GC_11, amp[25]); 
  FFV1_0(w[11], w[4], w[28], pars->GC_11, amp[26]); 
  FFV1_0(w[13], w[12], w[28], pars->GC_11, amp[27]); 
  FFV1_0(w[0], w[29], w[28], pars->GC_11, amp[28]); 
  FFV1_0(w[13], w[4], w[30], pars->GC_11, amp[29]); 
  FFV1_0(w[13], w[16], w[9], pars->GC_11, amp[30]); 
  FFV1_0(w[0], w[27], w[30], pars->GC_11, amp[31]); 
  FFV1_0(w[13], w[4], w[31], pars->GC_11, amp[32]); 
  FFV1_0(w[0], w[27], w[31], pars->GC_11, amp[33]); 
  FFV1_0(w[22], w[4], w[28], pars->GC_11, amp[34]); 
  FFV1_0(w[13], w[4], w[32], pars->GC_11, amp[35]); 
  FFV1_0(w[0], w[33], w[28], pars->GC_11, amp[36]); 
  FFV1_0(w[0], w[27], w[32], pars->GC_11, amp[37]); 
  FFV1_0(w[1], w[27], w[34], pars->GC_11, amp[38]); 
  FFV1_0(w[1], w[7], w[8], pars->GC_11, amp[39]); 
  FFV1_0(w[10], w[4], w[34], pars->GC_11, amp[40]); 
  FFV1_0(w[1], w[29], w[35], pars->GC_11, amp[41]); 
  FFV1_0(w[10], w[12], w[35], pars->GC_11, amp[42]); 
  FFV1_0(w[1], w[7], w[15], pars->GC_11, amp[43]); 
  FFV1_0(w[1], w[18], w[17], pars->GC_11, amp[44]); 
  FFV1_0(w[10], w[16], w[17], pars->GC_11, amp[45]); 
  FFV1_0(w[1], w[27], w[36], pars->GC_11, amp[46]); 
  FFV1_0(w[10], w[4], w[36], pars->GC_11, amp[47]); 
  FFV1_0(w[21], w[5], w[17], pars->GC_11, amp[48]); 
  FFV1_0(w[19], w[7], w[17], pars->GC_11, amp[49]); 
  FFV1_0(w[21], w[4], w[35], pars->GC_11, amp[50]); 
  FFV1_0(w[19], w[27], w[35], pars->GC_11, amp[51]); 
  FFV1_0(w[1], w[7], w[24], pars->GC_11, amp[52]); 
  FFV1_0(w[1], w[25], w[17], pars->GC_11, amp[53]); 
  FFV1_0(w[10], w[5], w[24], pars->GC_11, amp[54]); 
  FFV1_0(w[26], w[5], w[17], pars->GC_11, amp[55]); 
  FFV1_0(w[1], w[27], w[37], pars->GC_11, amp[56]); 
  FFV1_0(w[1], w[33], w[35], pars->GC_11, amp[57]); 
  FFV1_0(w[10], w[4], w[37], pars->GC_11, amp[58]); 
  FFV1_0(w[26], w[4], w[35], pars->GC_11, amp[59]); 
  FFV1_0(w[39], w[27], w[40], pars->GC_11, amp[60]); 
  FFV1_0(w[6], w[27], w[41], pars->GC_11, amp[61]); 
  FFV1_0(w[42], w[4], w[40], pars->GC_11, amp[62]); 
  FFV1_0(w[11], w[4], w[41], pars->GC_11, amp[63]); 
  FFV1_0(w[13], w[4], w[44], pars->GC_11, amp[64]); 
  FFV1_0(w[39], w[27], w[45], pars->GC_11, amp[65]); 
  FFV1_0(w[0], w[27], w[44], pars->GC_11, amp[66]); 
  FFV1_0(w[42], w[4], w[45], pars->GC_11, amp[67]); 
  FFV1_0(w[13], w[12], w[41], pars->GC_11, amp[68]); 
  FFV1_0(w[39], w[29], w[46], pars->GC_11, amp[69]); 
  FFV1_0(w[42], w[12], w[46], pars->GC_11, amp[70]); 
  FFV1_0(w[0], w[29], w[41], pars->GC_11, amp[71]); 
  FFV1_0(w[13], w[4], w[48], pars->GC_11, amp[72]); 
  FFV1_0(w[49], w[4], w[46], pars->GC_11, amp[73]); 
  FFV1_0(w[47], w[27], w[46], pars->GC_11, amp[74]); 
  FFV1_0(w[0], w[27], w[48], pars->GC_11, amp[75]); 
  FFV1_0(w[22], w[4], w[41], pars->GC_11, amp[76]); 
  FFV1_0(w[13], w[4], w[50], pars->GC_11, amp[77]); 
  FFV1_0(w[39], w[27], w[51], pars->GC_11, amp[78]); 
  FFV1_0(w[39], w[33], w[46], pars->GC_11, amp[79]); 
  FFV1_0(w[42], w[4], w[51], pars->GC_11, amp[80]); 
  FFV1_0(w[52], w[4], w[46], pars->GC_11, amp[81]); 
  FFV1_0(w[0], w[33], w[41], pars->GC_11, amp[82]); 
  FFV1_0(w[0], w[27], w[50], pars->GC_11, amp[83]); 
  FFV1_0(w[39], w[53], w[8], pars->GC_11, amp[84]); 
  FFV1_0(w[39], w[27], w[40], pars->GC_11, amp[85]); 
  FFV1_0(w[42], w[4], w[40], pars->GC_11, amp[86]); 
  FFV1_0(w[42], w[38], w[8], pars->GC_11, amp[87]); 
  FFV1_0(w[39], w[54], w[17], pars->GC_11, amp[88]); 
  FFV1_0(w[42], w[43], w[17], pars->GC_11, amp[89]); 
  FFV1_0(w[39], w[27], w[45], pars->GC_11, amp[90]); 
  FFV1_0(w[42], w[4], w[45], pars->GC_11, amp[91]); 
  FFV1_0(w[39], w[29], w[46], pars->GC_11, amp[92]); 
  FFV1_0(w[42], w[12], w[46], pars->GC_11, amp[93]); 
  FFV1_0(w[39], w[53], w[15], pars->GC_11, amp[94]); 
  FFV1_0(w[42], w[38], w[15], pars->GC_11, amp[95]); 
  FFV1_0(w[49], w[4], w[46], pars->GC_11, amp[96]); 
  FFV1_0(w[47], w[27], w[46], pars->GC_11, amp[97]); 
  FFV1_0(w[49], w[38], w[17], pars->GC_11, amp[98]); 
  FFV1_0(w[47], w[53], w[17], pars->GC_11, amp[99]); 
  FFV1_0(w[39], w[27], w[51], pars->GC_11, amp[100]); 
  FFV1_0(w[39], w[33], w[46], pars->GC_11, amp[101]); 
  FFV1_0(w[42], w[4], w[51], pars->GC_11, amp[102]); 
  FFV1_0(w[52], w[4], w[46], pars->GC_11, amp[103]); 
  FFV1_0(w[39], w[53], w[24], pars->GC_11, amp[104]); 
  FFV1_0(w[39], w[55], w[17], pars->GC_11, amp[105]); 
  FFV1_0(w[42], w[38], w[24], pars->GC_11, amp[106]); 
  FFV1_0(w[52], w[38], w[17], pars->GC_11, amp[107]); 
  FFV1_0(w[39], w[53], w[8], pars->GC_11, amp[108]); 
  FFV1_0(w[6], w[53], w[56], pars->GC_11, amp[109]); 
  FFV1_0(w[42], w[38], w[8], pars->GC_11, amp[110]); 
  FFV1_0(w[11], w[38], w[56], pars->GC_11, amp[111]); 
  FFV1_0(w[13], w[38], w[57], pars->GC_11, amp[112]); 
  FFV1_0(w[39], w[53], w[15], pars->GC_11, amp[113]); 
  FFV1_0(w[0], w[53], w[57], pars->GC_11, amp[114]); 
  FFV1_0(w[42], w[38], w[15], pars->GC_11, amp[115]); 
  FFV1_0(w[13], w[43], w[56], pars->GC_11, amp[116]); 
  FFV1_0(w[39], w[54], w[17], pars->GC_11, amp[117]); 
  FFV1_0(w[42], w[43], w[17], pars->GC_11, amp[118]); 
  FFV1_0(w[0], w[54], w[56], pars->GC_11, amp[119]); 
  FFV1_0(w[13], w[38], w[58], pars->GC_11, amp[120]); 
  FFV1_0(w[49], w[38], w[17], pars->GC_11, amp[121]); 
  FFV1_0(w[47], w[53], w[17], pars->GC_11, amp[122]); 
  FFV1_0(w[0], w[53], w[58], pars->GC_11, amp[123]); 
  FFV1_0(w[22], w[38], w[56], pars->GC_11, amp[124]); 
  FFV1_0(w[13], w[38], w[59], pars->GC_11, amp[125]); 
  FFV1_0(w[39], w[53], w[24], pars->GC_11, amp[126]); 
  FFV1_0(w[39], w[55], w[17], pars->GC_11, amp[127]); 
  FFV1_0(w[42], w[38], w[24], pars->GC_11, amp[128]); 
  FFV1_0(w[52], w[38], w[17], pars->GC_11, amp[129]); 
  FFV1_0(w[0], w[55], w[56], pars->GC_11, amp[130]); 
  FFV1_0(w[0], w[53], w[59], pars->GC_11, amp[131]); 
  FFV1_0(w[6], w[53], w[56], pars->GC_11, amp[132]); 
  FFV1_0(w[6], w[27], w[41], pars->GC_11, amp[133]); 
  FFV1_0(w[11], w[4], w[41], pars->GC_11, amp[134]); 
  FFV1_0(w[11], w[38], w[56], pars->GC_11, amp[135]); 
  FFV1_0(w[13], w[4], w[44], pars->GC_11, amp[136]); 
  FFV1_0(w[13], w[43], w[56], pars->GC_11, amp[137]); 
  FFV1_0(w[0], w[27], w[44], pars->GC_11, amp[138]); 
  FFV1_0(w[0], w[54], w[56], pars->GC_11, amp[139]); 
  FFV1_0(w[13], w[38], w[57], pars->GC_11, amp[140]); 
  FFV1_0(w[13], w[12], w[41], pars->GC_11, amp[141]); 
  FFV1_0(w[0], w[53], w[57], pars->GC_11, amp[142]); 
  FFV1_0(w[0], w[29], w[41], pars->GC_11, amp[143]); 
  FFV1_0(w[13], w[4], w[48], pars->GC_11, amp[144]); 
  FFV1_0(w[13], w[38], w[58], pars->GC_11, amp[145]); 
  FFV1_0(w[0], w[53], w[58], pars->GC_11, amp[146]); 
  FFV1_0(w[0], w[27], w[48], pars->GC_11, amp[147]); 
  FFV1_0(w[22], w[4], w[41], pars->GC_11, amp[148]); 
  FFV1_0(w[13], w[4], w[50], pars->GC_11, amp[149]); 
  FFV1_0(w[22], w[38], w[56], pars->GC_11, amp[150]); 
  FFV1_0(w[13], w[38], w[59], pars->GC_11, amp[151]); 
  FFV1_0(w[0], w[55], w[56], pars->GC_11, amp[152]); 
  FFV1_0(w[0], w[53], w[59], pars->GC_11, amp[153]); 
  FFV1_0(w[0], w[33], w[41], pars->GC_11, amp[154]); 
  FFV1_0(w[0], w[27], w[50], pars->GC_11, amp[155]); 
  FFV1_0(w[1], w[27], w[34], pars->GC_11, amp[156]); 
  FFV1_0(w[6], w[27], w[28], pars->GC_11, amp[157]); 
  FFV1_0(w[10], w[4], w[34], pars->GC_11, amp[158]); 
  FFV1_0(w[11], w[4], w[28], pars->GC_11, amp[159]); 
  FFV1_0(w[13], w[12], w[28], pars->GC_11, amp[160]); 
  FFV1_0(w[1], w[29], w[35], pars->GC_11, amp[161]); 
  FFV1_0(w[10], w[12], w[35], pars->GC_11, amp[162]); 
  FFV1_0(w[0], w[29], w[28], pars->GC_11, amp[163]); 
  FFV1_0(w[13], w[4], w[30], pars->GC_11, amp[164]); 
  FFV1_0(w[1], w[27], w[36], pars->GC_11, amp[165]); 
  FFV1_0(w[0], w[27], w[30], pars->GC_11, amp[166]); 
  FFV1_0(w[10], w[4], w[36], pars->GC_11, amp[167]); 
  FFV1_0(w[13], w[4], w[31], pars->GC_11, amp[168]); 
  FFV1_0(w[21], w[4], w[35], pars->GC_11, amp[169]); 
  FFV1_0(w[19], w[27], w[35], pars->GC_11, amp[170]); 
  FFV1_0(w[0], w[27], w[31], pars->GC_11, amp[171]); 
  FFV1_0(w[22], w[4], w[28], pars->GC_11, amp[172]); 
  FFV1_0(w[13], w[4], w[32], pars->GC_11, amp[173]); 
  FFV1_0(w[1], w[27], w[37], pars->GC_11, amp[174]); 
  FFV1_0(w[1], w[33], w[35], pars->GC_11, amp[175]); 
  FFV1_0(w[10], w[4], w[37], pars->GC_11, amp[176]); 
  FFV1_0(w[26], w[4], w[35], pars->GC_11, amp[177]); 
  FFV1_0(w[0], w[33], w[28], pars->GC_11, amp[178]); 
  FFV1_0(w[0], w[27], w[32], pars->GC_11, amp[179]); 
  FFV1_0(w[0], w[53], w[58], pars->GC_11, amp[180]); 
  FFV1_0(w[0], w[27], w[48], pars->GC_11, amp[181]); 
  FFV1_0(w[13], w[4], w[48], pars->GC_11, amp[182]); 
  FFV1_0(w[13], w[38], w[58], pars->GC_11, amp[183]); 
  FFV1_0(w[0], w[54], w[56], pars->GC_11, amp[184]); 
  FFV1_0(w[13], w[43], w[56], pars->GC_11, amp[185]); 
  FFV1_0(w[0], w[27], w[44], pars->GC_11, amp[186]); 
  FFV1_0(w[13], w[4], w[44], pars->GC_11, amp[187]); 
  FFV1_0(w[0], w[29], w[41], pars->GC_11, amp[188]); 
  FFV1_0(w[13], w[12], w[41], pars->GC_11, amp[189]); 
  FFV1_0(w[0], w[53], w[57], pars->GC_11, amp[190]); 
  FFV1_0(w[13], w[38], w[57], pars->GC_11, amp[191]); 
  FFV1_0(w[11], w[4], w[41], pars->GC_11, amp[192]); 
  FFV1_0(w[6], w[27], w[41], pars->GC_11, amp[193]); 
  FFV1_0(w[11], w[38], w[56], pars->GC_11, amp[194]); 
  FFV1_0(w[6], w[53], w[56], pars->GC_11, amp[195]); 
  FFV1_0(w[0], w[27], w[50], pars->GC_11, amp[196]); 
  FFV1_0(w[0], w[33], w[41], pars->GC_11, amp[197]); 
  FFV1_0(w[13], w[4], w[50], pars->GC_11, amp[198]); 
  FFV1_0(w[22], w[4], w[41], pars->GC_11, amp[199]); 
  FFV1_0(w[0], w[53], w[59], pars->GC_11, amp[200]); 
  FFV1_0(w[0], w[55], w[56], pars->GC_11, amp[201]); 
  FFV1_0(w[13], w[38], w[59], pars->GC_11, amp[202]); 
  FFV1_0(w[22], w[38], w[56], pars->GC_11, amp[203]); 
  FFV1_0(w[39], w[53], w[8], pars->GC_11, amp[204]); 
  FFV1_0(w[6], w[53], w[56], pars->GC_11, amp[205]); 
  FFV1_0(w[42], w[38], w[8], pars->GC_11, amp[206]); 
  FFV1_0(w[11], w[38], w[56], pars->GC_11, amp[207]); 
  FFV1_0(w[13], w[43], w[56], pars->GC_11, amp[208]); 
  FFV1_0(w[39], w[54], w[17], pars->GC_11, amp[209]); 
  FFV1_0(w[42], w[43], w[17], pars->GC_11, amp[210]); 
  FFV1_0(w[0], w[54], w[56], pars->GC_11, amp[211]); 
  FFV1_0(w[13], w[38], w[57], pars->GC_11, amp[212]); 
  FFV1_0(w[39], w[53], w[15], pars->GC_11, amp[213]); 
  FFV1_0(w[0], w[53], w[57], pars->GC_11, amp[214]); 
  FFV1_0(w[42], w[38], w[15], pars->GC_11, amp[215]); 
  FFV1_0(w[13], w[38], w[58], pars->GC_11, amp[216]); 
  FFV1_0(w[49], w[38], w[17], pars->GC_11, amp[217]); 
  FFV1_0(w[47], w[53], w[17], pars->GC_11, amp[218]); 
  FFV1_0(w[0], w[53], w[58], pars->GC_11, amp[219]); 
  FFV1_0(w[22], w[38], w[56], pars->GC_11, amp[220]); 
  FFV1_0(w[13], w[38], w[59], pars->GC_11, amp[221]); 
  FFV1_0(w[39], w[53], w[24], pars->GC_11, amp[222]); 
  FFV1_0(w[39], w[55], w[17], pars->GC_11, amp[223]); 
  FFV1_0(w[42], w[38], w[24], pars->GC_11, amp[224]); 
  FFV1_0(w[52], w[38], w[17], pars->GC_11, amp[225]); 
  FFV1_0(w[0], w[55], w[56], pars->GC_11, amp[226]); 
  FFV1_0(w[0], w[53], w[59], pars->GC_11, amp[227]); 
  FFV1_0(w[47], w[53], w[17], pars->GC_11, amp[228]); 
  FFV1_0(w[47], w[27], w[46], pars->GC_11, amp[229]); 
  FFV1_0(w[49], w[4], w[46], pars->GC_11, amp[230]); 
  FFV1_0(w[49], w[38], w[17], pars->GC_11, amp[231]); 
  FFV1_0(w[42], w[4], w[45], pars->GC_11, amp[232]); 
  FFV1_0(w[42], w[43], w[17], pars->GC_11, amp[233]); 
  FFV1_0(w[39], w[27], w[45], pars->GC_11, amp[234]); 
  FFV1_0(w[39], w[54], w[17], pars->GC_11, amp[235]); 
  FFV1_0(w[42], w[38], w[15], pars->GC_11, amp[236]); 
  FFV1_0(w[42], w[12], w[46], pars->GC_11, amp[237]); 
  FFV1_0(w[39], w[53], w[15], pars->GC_11, amp[238]); 
  FFV1_0(w[39], w[29], w[46], pars->GC_11, amp[239]); 
  FFV1_0(w[42], w[4], w[40], pars->GC_11, amp[240]); 
  FFV1_0(w[42], w[38], w[8], pars->GC_11, amp[241]); 
  FFV1_0(w[39], w[53], w[8], pars->GC_11, amp[242]); 
  FFV1_0(w[39], w[27], w[40], pars->GC_11, amp[243]); 
  FFV1_0(w[52], w[4], w[46], pars->GC_11, amp[244]); 
  FFV1_0(w[42], w[4], w[51], pars->GC_11, amp[245]); 
  FFV1_0(w[52], w[38], w[17], pars->GC_11, amp[246]); 
  FFV1_0(w[42], w[38], w[24], pars->GC_11, amp[247]); 
  FFV1_0(w[39], w[55], w[17], pars->GC_11, amp[248]); 
  FFV1_0(w[39], w[53], w[24], pars->GC_11, amp[249]); 
  FFV1_0(w[39], w[33], w[46], pars->GC_11, amp[250]); 
  FFV1_0(w[39], w[27], w[51], pars->GC_11, amp[251]); 
  FFV1_0(w[39], w[27], w[40], pars->GC_11, amp[252]); 
  FFV1_0(w[6], w[27], w[41], pars->GC_11, amp[253]); 
  FFV1_0(w[42], w[4], w[40], pars->GC_11, amp[254]); 
  FFV1_0(w[11], w[4], w[41], pars->GC_11, amp[255]); 
  FFV1_0(w[13], w[12], w[41], pars->GC_11, amp[256]); 
  FFV1_0(w[39], w[29], w[46], pars->GC_11, amp[257]); 
  FFV1_0(w[42], w[12], w[46], pars->GC_11, amp[258]); 
  FFV1_0(w[0], w[29], w[41], pars->GC_11, amp[259]); 
  FFV1_0(w[13], w[4], w[44], pars->GC_11, amp[260]); 
  FFV1_0(w[39], w[27], w[45], pars->GC_11, amp[261]); 
  FFV1_0(w[0], w[27], w[44], pars->GC_11, amp[262]); 
  FFV1_0(w[42], w[4], w[45], pars->GC_11, amp[263]); 
  FFV1_0(w[13], w[4], w[48], pars->GC_11, amp[264]); 
  FFV1_0(w[49], w[4], w[46], pars->GC_11, amp[265]); 
  FFV1_0(w[47], w[27], w[46], pars->GC_11, amp[266]); 
  FFV1_0(w[0], w[27], w[48], pars->GC_11, amp[267]); 
  FFV1_0(w[22], w[4], w[41], pars->GC_11, amp[268]); 
  FFV1_0(w[13], w[4], w[50], pars->GC_11, amp[269]); 
  FFV1_0(w[39], w[27], w[51], pars->GC_11, amp[270]); 
  FFV1_0(w[39], w[33], w[46], pars->GC_11, amp[271]); 
  FFV1_0(w[42], w[4], w[51], pars->GC_11, amp[272]); 
  FFV1_0(w[52], w[4], w[46], pars->GC_11, amp[273]); 
  FFV1_0(w[0], w[33], w[41], pars->GC_11, amp[274]); 
  FFV1_0(w[0], w[27], w[50], pars->GC_11, amp[275]); 
  FFV1_0(w[39], w[63], w[64], pars->GC_11, amp[276]); 
  FFV1_0(w[39], w[53], w[65], pars->GC_11, amp[277]); 
  FFV1_0(w[42], w[38], w[65], pars->GC_11, amp[278]); 
  FFV1_0(w[42], w[60], w[64], pars->GC_11, amp[279]); 
  FFV1_0(w[39], w[68], w[67], pars->GC_11, amp[280]); 
  FFV1_0(w[42], w[66], w[67], pars->GC_11, amp[281]); 
  FFV1_0(w[39], w[53], w[69], pars->GC_11, amp[282]); 
  FFV1_0(w[42], w[38], w[69], pars->GC_11, amp[283]); 
  FFV1_0(w[39], w[54], w[70], pars->GC_11, amp[284]); 
  FFV1_0(w[42], w[43], w[70], pars->GC_11, amp[285]); 
  FFV1_0(w[39], w[63], w[71], pars->GC_11, amp[286]); 
  FFV1_0(w[42], w[60], w[71], pars->GC_11, amp[287]); 
  FFV1_0(w[49], w[38], w[70], pars->GC_11, amp[288]); 
  FFV1_0(w[47], w[53], w[70], pars->GC_11, amp[289]); 
  FFV1_0(w[49], w[60], w[67], pars->GC_11, amp[290]); 
  FFV1_0(w[47], w[63], w[67], pars->GC_11, amp[291]); 
  FFV1_0(w[39], w[53], w[72], pars->GC_11, amp[292]); 
  FFV1_0(w[39], w[55], w[70], pars->GC_11, amp[293]); 
  FFV1_0(w[42], w[38], w[72], pars->GC_11, amp[294]); 
  FFV1_0(w[52], w[38], w[70], pars->GC_11, amp[295]); 
  FFV1_0(w[39], w[63], w[73], pars->GC_11, amp[296]); 
  FFV1_0(w[39], w[74], w[67], pars->GC_11, amp[297]); 
  FFV1_0(w[42], w[60], w[73], pars->GC_11, amp[298]); 
  FFV1_0(w[52], w[60], w[67], pars->GC_11, amp[299]); 
  FFV1_0(w[39], w[53], w[65], pars->GC_11, amp[300]); 
  FFV1_0(w[62], w[53], w[75], pars->GC_11, amp[301]); 
  FFV1_0(w[42], w[38], w[65], pars->GC_11, amp[302]); 
  FFV1_0(w[76], w[38], w[75], pars->GC_11, amp[303]); 
  FFV1_0(w[77], w[38], w[78], pars->GC_11, amp[304]); 
  FFV1_0(w[39], w[53], w[69], pars->GC_11, amp[305]); 
  FFV1_0(w[61], w[53], w[78], pars->GC_11, amp[306]); 
  FFV1_0(w[42], w[38], w[69], pars->GC_11, amp[307]); 
  FFV1_0(w[77], w[43], w[75], pars->GC_11, amp[308]); 
  FFV1_0(w[39], w[54], w[70], pars->GC_11, amp[309]); 
  FFV1_0(w[42], w[43], w[70], pars->GC_11, amp[310]); 
  FFV1_0(w[61], w[54], w[75], pars->GC_11, amp[311]); 
  FFV1_0(w[77], w[38], w[79], pars->GC_11, amp[312]); 
  FFV1_0(w[49], w[38], w[70], pars->GC_11, amp[313]); 
  FFV1_0(w[47], w[53], w[70], pars->GC_11, amp[314]); 
  FFV1_0(w[61], w[53], w[79], pars->GC_11, amp[315]); 
  FFV1_0(w[80], w[38], w[75], pars->GC_11, amp[316]); 
  FFV1_0(w[77], w[38], w[81], pars->GC_11, amp[317]); 
  FFV1_0(w[39], w[53], w[72], pars->GC_11, amp[318]); 
  FFV1_0(w[39], w[55], w[70], pars->GC_11, amp[319]); 
  FFV1_0(w[42], w[38], w[72], pars->GC_11, amp[320]); 
  FFV1_0(w[52], w[38], w[70], pars->GC_11, amp[321]); 
  FFV1_0(w[61], w[55], w[75], pars->GC_11, amp[322]); 
  FFV1_0(w[61], w[53], w[81], pars->GC_11, amp[323]); 
  FFV1_0(w[39], w[63], w[64], pars->GC_11, amp[324]); 
  FFV1_0(w[62], w[63], w[41], pars->GC_11, amp[325]); 
  FFV1_0(w[42], w[60], w[64], pars->GC_11, amp[326]); 
  FFV1_0(w[76], w[60], w[41], pars->GC_11, amp[327]); 
  FFV1_0(w[77], w[66], w[41], pars->GC_11, amp[328]); 
  FFV1_0(w[39], w[68], w[67], pars->GC_11, amp[329]); 
  FFV1_0(w[42], w[66], w[67], pars->GC_11, amp[330]); 
  FFV1_0(w[61], w[68], w[41], pars->GC_11, amp[331]); 
  FFV1_0(w[77], w[60], w[44], pars->GC_11, amp[332]); 
  FFV1_0(w[39], w[63], w[71], pars->GC_11, amp[333]); 
  FFV1_0(w[61], w[63], w[44], pars->GC_11, amp[334]); 
  FFV1_0(w[42], w[60], w[71], pars->GC_11, amp[335]); 
  FFV1_0(w[77], w[60], w[48], pars->GC_11, amp[336]); 
  FFV1_0(w[49], w[60], w[67], pars->GC_11, amp[337]); 
  FFV1_0(w[47], w[63], w[67], pars->GC_11, amp[338]); 
  FFV1_0(w[61], w[63], w[48], pars->GC_11, amp[339]); 
  FFV1_0(w[80], w[60], w[41], pars->GC_11, amp[340]); 
  FFV1_0(w[77], w[60], w[50], pars->GC_11, amp[341]); 
  FFV1_0(w[39], w[63], w[73], pars->GC_11, amp[342]); 
  FFV1_0(w[39], w[74], w[67], pars->GC_11, amp[343]); 
  FFV1_0(w[42], w[60], w[73], pars->GC_11, amp[344]); 
  FFV1_0(w[52], w[60], w[67], pars->GC_11, amp[345]); 
  FFV1_0(w[61], w[74], w[41], pars->GC_11, amp[346]); 
  FFV1_0(w[61], w[63], w[50], pars->GC_11, amp[347]); 
  FFV1_0(w[62], w[63], w[41], pars->GC_11, amp[348]); 
  FFV1_0(w[62], w[53], w[75], pars->GC_11, amp[349]); 
  FFV1_0(w[76], w[38], w[75], pars->GC_11, amp[350]); 
  FFV1_0(w[76], w[60], w[41], pars->GC_11, amp[351]); 
  FFV1_0(w[77], w[38], w[78], pars->GC_11, amp[352]); 
  FFV1_0(w[77], w[66], w[41], pars->GC_11, amp[353]); 
  FFV1_0(w[61], w[53], w[78], pars->GC_11, amp[354]); 
  FFV1_0(w[61], w[68], w[41], pars->GC_11, amp[355]); 
  FFV1_0(w[77], w[60], w[44], pars->GC_11, amp[356]); 
  FFV1_0(w[77], w[43], w[75], pars->GC_11, amp[357]); 
  FFV1_0(w[61], w[63], w[44], pars->GC_11, amp[358]); 
  FFV1_0(w[61], w[54], w[75], pars->GC_11, amp[359]); 
  FFV1_0(w[77], w[38], w[79], pars->GC_11, amp[360]); 
  FFV1_0(w[77], w[60], w[48], pars->GC_11, amp[361]); 
  FFV1_0(w[61], w[63], w[48], pars->GC_11, amp[362]); 
  FFV1_0(w[61], w[53], w[79], pars->GC_11, amp[363]); 
  FFV1_0(w[80], w[38], w[75], pars->GC_11, amp[364]); 
  FFV1_0(w[77], w[38], w[81], pars->GC_11, amp[365]); 
  FFV1_0(w[80], w[60], w[41], pars->GC_11, amp[366]); 
  FFV1_0(w[77], w[60], w[50], pars->GC_11, amp[367]); 
  FFV1_0(w[61], w[74], w[41], pars->GC_11, amp[368]); 
  FFV1_0(w[61], w[63], w[50], pars->GC_11, amp[369]); 
  FFV1_0(w[61], w[55], w[75], pars->GC_11, amp[370]); 
  FFV1_0(w[61], w[53], w[81], pars->GC_11, amp[371]); 
  FFV1_0(w[10], w[5], w[8], pars->GC_11, amp[372]); 
  FFV1_0(w[1], w[7], w[15], pars->GC_11, amp[373]); 
  FFV1_0(w[10], w[5], w[15], pars->GC_11, amp[374]); 
  FFV1_0(w[1], w[18], w[17], pars->GC_11, amp[375]); 
  FFV1_0(w[10], w[16], w[17], pars->GC_11, amp[376]); 
  FFV1_0(w[21], w[5], w[17], pars->GC_11, amp[377]); 
  FFV1_0(w[19], w[7], w[17], pars->GC_11, amp[378]); 
  FFV1_0(w[1], w[7], w[24], pars->GC_11, amp[379]); 
  FFV1_0(w[1], w[25], w[17], pars->GC_11, amp[380]); 
  FFV1_0(w[10], w[5], w[24], pars->GC_11, amp[381]); 
  FFV1_0(w[26], w[5], w[17], pars->GC_11, amp[382]); 
  FFV1_0(w[6], w[7], w[9], pars->GC_11, amp[383]); 
  FFV1_0(w[11], w[5], w[9], pars->GC_11, amp[384]); 
  FFV1_0(w[13], w[5], w[14], pars->GC_11, amp[385]); 
  FFV1_0(w[0], w[7], w[14], pars->GC_11, amp[386]); 
  FFV1_0(w[13], w[16], w[9], pars->GC_11, amp[387]); 
  FFV1_0(w[0], w[18], w[9], pars->GC_11, amp[388]); 
  FFV1_0(w[13], w[5], w[20], pars->GC_11, amp[389]); 
  FFV1_0(w[0], w[7], w[20], pars->GC_11, amp[390]); 
  FFV1_0(w[22], w[5], w[9], pars->GC_11, amp[391]); 
  FFV1_0(w[13], w[5], w[23], pars->GC_11, amp[392]); 
  FFV1_0(w[0], w[25], w[9], pars->GC_11, amp[393]); 
  FFV1_0(w[0], w[7], w[23], pars->GC_11, amp[394]); 
  FFV1_0(w[10], w[5], w[8], pars->GC_11, amp[395]); 
  FFV1_0(w[1], w[7], w[15], pars->GC_11, amp[396]); 
  FFV1_0(w[10], w[5], w[15], pars->GC_11, amp[397]); 
  FFV1_0(w[1], w[18], w[17], pars->GC_11, amp[398]); 
  FFV1_0(w[10], w[16], w[17], pars->GC_11, amp[399]); 
  FFV1_0(w[21], w[5], w[17], pars->GC_11, amp[400]); 
  FFV1_0(w[19], w[7], w[17], pars->GC_11, amp[401]); 
  FFV1_0(w[1], w[7], w[24], pars->GC_11, amp[402]); 
  FFV1_0(w[1], w[25], w[17], pars->GC_11, amp[403]); 
  FFV1_0(w[10], w[5], w[24], pars->GC_11, amp[404]); 
  FFV1_0(w[26], w[5], w[17], pars->GC_11, amp[405]); 
  FFV1_0(w[39], w[27], w[40], pars->GC_11, amp[406]); 
  FFV1_0(w[42], w[4], w[40], pars->GC_11, amp[407]); 
  FFV1_0(w[39], w[27], w[45], pars->GC_11, amp[408]); 
  FFV1_0(w[42], w[4], w[45], pars->GC_11, amp[409]); 
  FFV1_0(w[39], w[29], w[46], pars->GC_11, amp[410]); 
  FFV1_0(w[42], w[12], w[46], pars->GC_11, amp[411]); 
  FFV1_0(w[49], w[4], w[46], pars->GC_11, amp[412]); 
  FFV1_0(w[47], w[27], w[46], pars->GC_11, amp[413]); 
  FFV1_0(w[39], w[27], w[51], pars->GC_11, amp[414]); 
  FFV1_0(w[39], w[33], w[46], pars->GC_11, amp[415]); 
  FFV1_0(w[42], w[4], w[51], pars->GC_11, amp[416]); 
  FFV1_0(w[52], w[4], w[46], pars->GC_11, amp[417]); 
  FFV1_0(w[39], w[27], w[40], pars->GC_11, amp[418]); 
  FFV1_0(w[42], w[4], w[40], pars->GC_11, amp[419]); 
  FFV1_0(w[39], w[27], w[45], pars->GC_11, amp[420]); 
  FFV1_0(w[42], w[4], w[45], pars->GC_11, amp[421]); 
  FFV1_0(w[39], w[29], w[46], pars->GC_11, amp[422]); 
  FFV1_0(w[42], w[12], w[46], pars->GC_11, amp[423]); 
  FFV1_0(w[49], w[4], w[46], pars->GC_11, amp[424]); 
  FFV1_0(w[47], w[27], w[46], pars->GC_11, amp[425]); 
  FFV1_0(w[39], w[27], w[51], pars->GC_11, amp[426]); 
  FFV1_0(w[39], w[33], w[46], pars->GC_11, amp[427]); 
  FFV1_0(w[42], w[4], w[51], pars->GC_11, amp[428]); 
  FFV1_0(w[52], w[4], w[46], pars->GC_11, amp[429]); 
  FFV1_0(w[6], w[27], w[41], pars->GC_11, amp[430]); 
  FFV1_0(w[11], w[4], w[41], pars->GC_11, amp[431]); 
  FFV1_0(w[13], w[4], w[44], pars->GC_11, amp[432]); 
  FFV1_0(w[0], w[27], w[44], pars->GC_11, amp[433]); 
  FFV1_0(w[13], w[12], w[41], pars->GC_11, amp[434]); 
  FFV1_0(w[0], w[29], w[41], pars->GC_11, amp[435]); 
  FFV1_0(w[13], w[4], w[48], pars->GC_11, amp[436]); 
  FFV1_0(w[0], w[27], w[48], pars->GC_11, amp[437]); 
  FFV1_0(w[22], w[4], w[41], pars->GC_11, amp[438]); 
  FFV1_0(w[13], w[4], w[50], pars->GC_11, amp[439]); 
  FFV1_0(w[0], w[33], w[41], pars->GC_11, amp[440]); 
  FFV1_0(w[0], w[27], w[50], pars->GC_11, amp[441]); 
  FFV1_0(w[39], w[53], w[8], pars->GC_11, amp[442]); 
  FFV1_0(w[42], w[38], w[8], pars->GC_11, amp[443]); 
  FFV1_0(w[39], w[53], w[15], pars->GC_11, amp[444]); 
  FFV1_0(w[42], w[38], w[15], pars->GC_11, amp[445]); 
  FFV1_0(w[39], w[54], w[17], pars->GC_11, amp[446]); 
  FFV1_0(w[42], w[43], w[17], pars->GC_11, amp[447]); 
  FFV1_0(w[49], w[38], w[17], pars->GC_11, amp[448]); 
  FFV1_0(w[47], w[53], w[17], pars->GC_11, amp[449]); 
  FFV1_0(w[39], w[53], w[24], pars->GC_11, amp[450]); 
  FFV1_0(w[39], w[55], w[17], pars->GC_11, amp[451]); 
  FFV1_0(w[42], w[38], w[24], pars->GC_11, amp[452]); 
  FFV1_0(w[52], w[38], w[17], pars->GC_11, amp[453]); 
  FFV1_0(w[6], w[53], w[56], pars->GC_11, amp[454]); 
  FFV1_0(w[11], w[38], w[56], pars->GC_11, amp[455]); 
  FFV1_0(w[13], w[38], w[57], pars->GC_11, amp[456]); 
  FFV1_0(w[0], w[53], w[57], pars->GC_11, amp[457]); 
  FFV1_0(w[13], w[43], w[56], pars->GC_11, amp[458]); 
  FFV1_0(w[0], w[54], w[56], pars->GC_11, amp[459]); 
  FFV1_0(w[13], w[38], w[58], pars->GC_11, amp[460]); 
  FFV1_0(w[0], w[53], w[58], pars->GC_11, amp[461]); 
  FFV1_0(w[22], w[38], w[56], pars->GC_11, amp[462]); 
  FFV1_0(w[13], w[38], w[59], pars->GC_11, amp[463]); 
  FFV1_0(w[0], w[55], w[56], pars->GC_11, amp[464]); 
  FFV1_0(w[0], w[53], w[59], pars->GC_11, amp[465]); 
  FFV1_0(w[39], w[53], w[8], pars->GC_11, amp[466]); 
  FFV1_0(w[42], w[38], w[8], pars->GC_11, amp[467]); 
  FFV1_0(w[39], w[53], w[15], pars->GC_11, amp[468]); 
  FFV1_0(w[42], w[38], w[15], pars->GC_11, amp[469]); 
  FFV1_0(w[39], w[54], w[17], pars->GC_11, amp[470]); 
  FFV1_0(w[42], w[43], w[17], pars->GC_11, amp[471]); 
  FFV1_0(w[49], w[38], w[17], pars->GC_11, amp[472]); 
  FFV1_0(w[47], w[53], w[17], pars->GC_11, amp[473]); 
  FFV1_0(w[39], w[53], w[24], pars->GC_11, amp[474]); 
  FFV1_0(w[39], w[55], w[17], pars->GC_11, amp[475]); 
  FFV1_0(w[42], w[38], w[24], pars->GC_11, amp[476]); 
  FFV1_0(w[52], w[38], w[17], pars->GC_11, amp[477]); 
  FFV1_0(w[6], w[27], w[28], pars->GC_11, amp[478]); 
  FFV1_0(w[11], w[4], w[28], pars->GC_11, amp[479]); 
  FFV1_0(w[13], w[12], w[28], pars->GC_11, amp[480]); 
  FFV1_0(w[0], w[29], w[28], pars->GC_11, amp[481]); 
  FFV1_0(w[13], w[4], w[30], pars->GC_11, amp[482]); 
  FFV1_0(w[0], w[27], w[30], pars->GC_11, amp[483]); 
  FFV1_0(w[13], w[4], w[31], pars->GC_11, amp[484]); 
  FFV1_0(w[0], w[27], w[31], pars->GC_11, amp[485]); 
  FFV1_0(w[22], w[4], w[28], pars->GC_11, amp[486]); 
  FFV1_0(w[13], w[4], w[32], pars->GC_11, amp[487]); 
  FFV1_0(w[0], w[33], w[28], pars->GC_11, amp[488]); 
  FFV1_0(w[0], w[27], w[32], pars->GC_11, amp[489]); 
  FFV1_0(w[0], w[53], w[58], pars->GC_11, amp[490]); 
  FFV1_0(w[13], w[38], w[58], pars->GC_11, amp[491]); 
  FFV1_0(w[0], w[54], w[56], pars->GC_11, amp[492]); 
  FFV1_0(w[13], w[43], w[56], pars->GC_11, amp[493]); 
  FFV1_0(w[0], w[53], w[57], pars->GC_11, amp[494]); 
  FFV1_0(w[13], w[38], w[57], pars->GC_11, amp[495]); 
  FFV1_0(w[11], w[38], w[56], pars->GC_11, amp[496]); 
  FFV1_0(w[6], w[53], w[56], pars->GC_11, amp[497]); 
  FFV1_0(w[0], w[53], w[59], pars->GC_11, amp[498]); 
  FFV1_0(w[0], w[55], w[56], pars->GC_11, amp[499]); 
  FFV1_0(w[13], w[38], w[59], pars->GC_11, amp[500]); 
  FFV1_0(w[22], w[38], w[56], pars->GC_11, amp[501]); 
  FFV1_0(w[0], w[27], w[48], pars->GC_11, amp[502]); 
  FFV1_0(w[13], w[4], w[48], pars->GC_11, amp[503]); 
  FFV1_0(w[0], w[29], w[41], pars->GC_11, amp[504]); 
  FFV1_0(w[13], w[12], w[41], pars->GC_11, amp[505]); 
  FFV1_0(w[0], w[27], w[44], pars->GC_11, amp[506]); 
  FFV1_0(w[13], w[4], w[44], pars->GC_11, amp[507]); 
  FFV1_0(w[11], w[4], w[41], pars->GC_11, amp[508]); 
  FFV1_0(w[6], w[27], w[41], pars->GC_11, amp[509]); 
  FFV1_0(w[0], w[27], w[50], pars->GC_11, amp[510]); 
  FFV1_0(w[0], w[33], w[41], pars->GC_11, amp[511]); 
  FFV1_0(w[13], w[4], w[50], pars->GC_11, amp[512]); 
  FFV1_0(w[22], w[4], w[41], pars->GC_11, amp[513]); 
  FFV1_0(w[39], w[53], w[65], pars->GC_11, amp[514]); 
  FFV1_0(w[42], w[38], w[65], pars->GC_11, amp[515]); 
  FFV1_0(w[39], w[53], w[69], pars->GC_11, amp[516]); 
  FFV1_0(w[42], w[38], w[69], pars->GC_11, amp[517]); 
  FFV1_0(w[39], w[54], w[70], pars->GC_11, amp[518]); 
  FFV1_0(w[42], w[43], w[70], pars->GC_11, amp[519]); 
  FFV1_0(w[49], w[38], w[70], pars->GC_11, amp[520]); 
  FFV1_0(w[47], w[53], w[70], pars->GC_11, amp[521]); 
  FFV1_0(w[39], w[53], w[72], pars->GC_11, amp[522]); 
  FFV1_0(w[39], w[55], w[70], pars->GC_11, amp[523]); 
  FFV1_0(w[42], w[38], w[72], pars->GC_11, amp[524]); 
  FFV1_0(w[52], w[38], w[70], pars->GC_11, amp[525]); 
  FFV1_0(w[39], w[63], w[64], pars->GC_11, amp[526]); 
  FFV1_0(w[42], w[60], w[64], pars->GC_11, amp[527]); 
  FFV1_0(w[39], w[68], w[67], pars->GC_11, amp[528]); 
  FFV1_0(w[42], w[66], w[67], pars->GC_11, amp[529]); 
  FFV1_0(w[39], w[63], w[71], pars->GC_11, amp[530]); 
  FFV1_0(w[42], w[60], w[71], pars->GC_11, amp[531]); 
  FFV1_0(w[49], w[60], w[67], pars->GC_11, amp[532]); 
  FFV1_0(w[47], w[63], w[67], pars->GC_11, amp[533]); 
  FFV1_0(w[39], w[63], w[73], pars->GC_11, amp[534]); 
  FFV1_0(w[39], w[74], w[67], pars->GC_11, amp[535]); 
  FFV1_0(w[42], w[60], w[73], pars->GC_11, amp[536]); 
  FFV1_0(w[52], w[60], w[67], pars->GC_11, amp[537]); 
  FFV1_0(w[39], w[53], w[65], pars->GC_11, amp[538]); 
  FFV1_0(w[42], w[38], w[65], pars->GC_11, amp[539]); 
  FFV1_0(w[39], w[53], w[69], pars->GC_11, amp[540]); 
  FFV1_0(w[42], w[38], w[69], pars->GC_11, amp[541]); 
  FFV1_0(w[39], w[54], w[70], pars->GC_11, amp[542]); 
  FFV1_0(w[42], w[43], w[70], pars->GC_11, amp[543]); 
  FFV1_0(w[49], w[38], w[70], pars->GC_11, amp[544]); 
  FFV1_0(w[47], w[53], w[70], pars->GC_11, amp[545]); 
  FFV1_0(w[39], w[53], w[72], pars->GC_11, amp[546]); 
  FFV1_0(w[39], w[55], w[70], pars->GC_11, amp[547]); 
  FFV1_0(w[42], w[38], w[72], pars->GC_11, amp[548]); 
  FFV1_0(w[52], w[38], w[70], pars->GC_11, amp[549]); 
  FFV1_0(w[62], w[63], w[41], pars->GC_11, amp[550]); 
  FFV1_0(w[76], w[60], w[41], pars->GC_11, amp[551]); 
  FFV1_0(w[77], w[66], w[41], pars->GC_11, amp[552]); 
  FFV1_0(w[61], w[68], w[41], pars->GC_11, amp[553]); 
  FFV1_0(w[77], w[60], w[44], pars->GC_11, amp[554]); 
  FFV1_0(w[61], w[63], w[44], pars->GC_11, amp[555]); 
  FFV1_0(w[77], w[60], w[48], pars->GC_11, amp[556]); 
  FFV1_0(w[61], w[63], w[48], pars->GC_11, amp[557]); 
  FFV1_0(w[80], w[60], w[41], pars->GC_11, amp[558]); 
  FFV1_0(w[77], w[60], w[50], pars->GC_11, amp[559]); 
  FFV1_0(w[61], w[74], w[41], pars->GC_11, amp[560]); 
  FFV1_0(w[61], w[63], w[50], pars->GC_11, amp[561]); 


}
double PY8MEs_R8_P177_sm_qq_wpgqq::matrix_8_uu_wpgud() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[0] + amp[1] + 1./3. * amp[2] + amp[3] +
      amp[4] + 1./3. * amp[5] + amp[6] + 1./3. * amp[7] + amp[16] +
      Complex<double> (0, 1) * amp[17] + Complex<double> (0, 1) * amp[23]);
  jamp[1] = +1./2. * (-1./3. * amp[4] - amp[5] - 1./3. * amp[6] - amp[7] -
      1./3. * amp[12] - amp[13] - amp[14] - 1./3. * amp[15] - Complex<double>
      (0, 1) * amp[18] - Complex<double> (0, 1) * amp[20] - amp[21]);
  jamp[2] = +1./2. * (-amp[0] - 1./3. * amp[1] - amp[2] - 1./3. * amp[3] -
      1./3. * amp[8] - amp[9] - amp[10] - 1./3. * amp[11] - 1./3. * amp[16] +
      Complex<double> (0, 1) * amp[18] - amp[19] + Complex<double> (0, 1) *
      amp[20] - 1./3. * amp[22]);
  jamp[3] = +1./2. * (+amp[8] + 1./3. * amp[9] + 1./3. * amp[10] + amp[11] +
      amp[12] + 1./3. * amp[13] + 1./3. * amp[14] + amp[15] - Complex<double>
      (0, 1) * amp[17] + 1./3. * amp[19] + 1./3. * amp[21] + amp[22] -
      Complex<double> (0, 1) * amp[23]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[0][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R8_P177_sm_qq_wpgqq::matrix_8_ud_wpgdd() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[24] + amp[1] + amp[25] + 1./3. * amp[26] +
      amp[4] + 1./3. * amp[27] + amp[6] + 1./3. * amp[28] + amp[16] +
      Complex<double> (0, 1) * amp[17] + 1./3. * amp[34] + 1./3. * amp[36] +
      Complex<double> (0, 1) * amp[23]);
  jamp[1] = +1./2. * (-1./3. * amp[4] - amp[27] - 1./3. * amp[6] - amp[28] -
      1./3. * amp[12] - amp[32] - amp[33] - 1./3. * amp[15] + Complex<double>
      (0, 1) * amp[35] - amp[36] + Complex<double> (0, 1) * amp[37]);
  jamp[2] = +1./2. * (-amp[24] - 1./3. * amp[1] - 1./3. * amp[25] - amp[26] -
      amp[29] - 1./3. * amp[30] - amp[31] - 1./3. * amp[11] - 1./3. * amp[16] -
      amp[34] - Complex<double> (0, 1) * amp[35] - Complex<double> (0, 1) *
      amp[37] - 1./3. * amp[22]);
  jamp[3] = +1./2. * (+1./3. * amp[29] + amp[30] + 1./3. * amp[31] + amp[11] +
      amp[12] + 1./3. * amp[32] + 1./3. * amp[33] + amp[15] - Complex<double>
      (0, 1) * amp[17] + amp[22] - Complex<double> (0, 1) * amp[23]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[1][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R8_P177_sm_qq_wpgqq::matrix_8_ud_wmguu() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[38] + 1./3. * amp[39] + 1./3. * amp[2] + amp[40] +
      amp[41] + amp[42] + 1./3. * amp[43] + 1./3. * amp[7] - Complex<double>
      (0, 1) * amp[56] + amp[57] - Complex<double> (0, 1) * amp[58]);
  jamp[1] = +1./2. * (-1./3. * amp[41] - 1./3. * amp[42] - amp[43] - amp[7] -
      amp[48] - amp[49] - 1./3. * amp[50] - 1./3. * amp[51] - Complex<double>
      (0, 1) * amp[52] - Complex<double> (0, 1) * amp[54] - amp[55] - 1./3. *
      amp[57] - 1./3. * amp[59]);
  jamp[2] = +1./2. * (-1./3. * amp[38] - amp[39] - amp[2] - 1./3. * amp[40] -
      amp[44] - amp[45] - 1./3. * amp[46] - 1./3. * amp[47] + Complex<double>
      (0, 1) * amp[52] - amp[53] + Complex<double> (0, 1) * amp[54]);
  jamp[3] = +1./2. * (+1./3. * amp[44] + 1./3. * amp[45] + amp[46] + amp[47] +
      1./3. * amp[48] + 1./3. * amp[49] + amp[50] + amp[51] + 1./3. * amp[53] +
      1./3. * amp[55] + Complex<double> (0, 1) * amp[56] + Complex<double> (0,
      1) * amp[58] + amp[59]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[2][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R8_P177_sm_qq_wpgqq::matrix_8_uux_wpgdux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[68] + 1./3. * amp[69] + 1./3. * amp[70] + amp[71] +
      amp[72] + 1./3. * amp[73] + 1./3. * amp[74] + amp[75] - Complex<double>
      (0, 1) * amp[77] + 1./3. * amp[79] + 1./3. * amp[81] + amp[82] -
      Complex<double> (0, 1) * amp[83]);
  jamp[1] = +1./2. * (-amp[60] - 1./3. * amp[61] - amp[62] - 1./3. * amp[63] -
      1./3. * amp[68] - amp[69] - amp[70] - 1./3. * amp[71] - 1./3. * amp[76] +
      Complex<double> (0, 1) * amp[78] - amp[79] + Complex<double> (0, 1) *
      amp[80] - 1./3. * amp[82]);
  jamp[2] = +1./2. * (+1./3. * amp[60] + amp[61] + 1./3. * amp[62] + amp[63] +
      amp[64] + 1./3. * amp[65] + amp[66] + 1./3. * amp[67] + amp[76] +
      Complex<double> (0, 1) * amp[77] + Complex<double> (0, 1) * amp[83]);
  jamp[3] = +1./2. * (-1./3. * amp[64] - amp[65] - 1./3. * amp[66] - amp[67] -
      1./3. * amp[72] - amp[73] - amp[74] - 1./3. * amp[75] - Complex<double>
      (0, 1) * amp[78] - Complex<double> (0, 1) * amp[80] - amp[81]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[3][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R8_P177_sm_qq_wpgqq::matrix_8_uux_wmgudx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[92] + 1./3. * amp[93] + amp[94] + amp[95] +
      1./3. * amp[96] + 1./3. * amp[97] + amp[98] + amp[99] + 1./3. * amp[101]
      + 1./3. * amp[103] + Complex<double> (0, 1) * amp[104] + Complex<double>
      (0, 1) * amp[106] + amp[107]);
  jamp[1] = +1./2. * (-1./3. * amp[84] - amp[85] - amp[86] - 1./3. * amp[87] -
      amp[92] - amp[93] - 1./3. * amp[94] - 1./3. * amp[95] + Complex<double>
      (0, 1) * amp[100] - amp[101] + Complex<double> (0, 1) * amp[102]);
  jamp[2] = +1./2. * (+amp[84] + 1./3. * amp[85] + 1./3. * amp[86] + amp[87] +
      amp[88] + amp[89] + 1./3. * amp[90] + 1./3. * amp[91] - Complex<double>
      (0, 1) * amp[104] + amp[105] - Complex<double> (0, 1) * amp[106]);
  jamp[3] = +1./2. * (-1./3. * amp[88] - 1./3. * amp[89] - amp[90] - amp[91] -
      amp[96] - amp[97] - 1./3. * amp[98] - 1./3. * amp[99] - Complex<double>
      (0, 1) * amp[100] - Complex<double> (0, 1) * amp[102] - amp[103] - 1./3.
      * amp[105] - 1./3. * amp[107]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[4][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R8_P177_sm_qq_wpgqq::matrix_8_udx_wpguux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[112] + amp[113] + 1./3. * amp[114] +
      amp[115] + 1./3. * amp[120] + amp[121] + amp[122] + 1./3. * amp[123] +
      Complex<double> (0, 1) * amp[126] + Complex<double> (0, 1) * amp[128] +
      amp[129]);
  jamp[1] = +1./2. * (-1./3. * amp[108] - amp[109] - 1./3. * amp[110] -
      amp[111] - amp[112] - 1./3. * amp[113] - amp[114] - 1./3. * amp[115] -
      amp[124] - Complex<double> (0, 1) * amp[125] - Complex<double> (0, 1) *
      amp[131]);
  jamp[2] = +1./2. * (+amp[108] + 1./3. * amp[109] + amp[110] + 1./3. *
      amp[111] + 1./3. * amp[116] + amp[117] + amp[118] + 1./3. * amp[119] +
      1./3. * amp[124] - Complex<double> (0, 1) * amp[126] + amp[127] -
      Complex<double> (0, 1) * amp[128] + 1./3. * amp[130]);
  jamp[3] = +1./2. * (-amp[116] - 1./3. * amp[117] - 1./3. * amp[118] -
      amp[119] - amp[120] - 1./3. * amp[121] - 1./3. * amp[122] - amp[123] +
      Complex<double> (0, 1) * amp[125] - 1./3. * amp[127] - 1./3. * amp[129] -
      amp[130] + Complex<double> (0, 1) * amp[131]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[5][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R8_P177_sm_qq_wpgqq::matrix_8_udx_wpgddx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[140] + amp[141] + 1./3. * amp[142] +
      amp[143] + amp[144] + 1./3. * amp[145] + 1./3. * amp[146] + amp[147] -
      Complex<double> (0, 1) * amp[149] + amp[154] - Complex<double> (0, 1) *
      amp[155]);
  jamp[1] = +1./2. * (-amp[132] - 1./3. * amp[133] - 1./3. * amp[134] -
      amp[135] - amp[140] - 1./3. * amp[141] - amp[142] - 1./3. * amp[143] -
      1./3. * amp[148] - amp[150] - Complex<double> (0, 1) * amp[151] -
      Complex<double> (0, 1) * amp[153] - 1./3. * amp[154]);
  jamp[2] = +1./2. * (+1./3. * amp[132] + amp[133] + amp[134] + 1./3. *
      amp[135] + amp[136] + 1./3. * amp[137] + amp[138] + 1./3. * amp[139] +
      amp[148] + Complex<double> (0, 1) * amp[149] + 1./3. * amp[150] + 1./3. *
      amp[152] + Complex<double> (0, 1) * amp[155]);
  jamp[3] = +1./2. * (-1./3. * amp[136] - amp[137] - 1./3. * amp[138] -
      amp[139] - 1./3. * amp[144] - amp[145] - amp[146] - 1./3. * amp[147] +
      Complex<double> (0, 1) * amp[151] - amp[152] + Complex<double> (0, 1) *
      amp[153]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[6][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R8_P177_sm_qq_wpgqq::matrix_8_dd_wmgud() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[156] + 1./3. * amp[157] + amp[158] + 1./3. *
      amp[159] + 1./3. * amp[160] + amp[161] + amp[162] + 1./3. * amp[163] +
      1./3. * amp[172] - Complex<double> (0, 1) * amp[174] + amp[175] -
      Complex<double> (0, 1) * amp[176] + 1./3. * amp[178]);
  jamp[1] = +1./2. * (-amp[160] - 1./3. * amp[161] - 1./3. * amp[162] -
      amp[163] - amp[168] - 1./3. * amp[169] - 1./3. * amp[170] - amp[171] +
      Complex<double> (0, 1) * amp[173] - 1./3. * amp[175] - 1./3. * amp[177] -
      amp[178] + Complex<double> (0, 1) * amp[179]);
  jamp[2] = +1./2. * (-1./3. * amp[156] - amp[157] - 1./3. * amp[158] -
      amp[159] - amp[164] - 1./3. * amp[165] - amp[166] - 1./3. * amp[167] -
      amp[172] - Complex<double> (0, 1) * amp[173] - Complex<double> (0, 1) *
      amp[179]);
  jamp[3] = +1./2. * (+1./3. * amp[164] + amp[165] + 1./3. * amp[166] +
      amp[167] + 1./3. * amp[168] + amp[169] + amp[170] + 1./3. * amp[171] +
      Complex<double> (0, 1) * amp[174] + Complex<double> (0, 1) * amp[176] +
      amp[177]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[7][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R8_P177_sm_qq_wpgqq::matrix_8_dux_wmguux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[180] + amp[181] + amp[182] + 1./3. *
      amp[183] + amp[188] + amp[189] + 1./3. * amp[190] + 1./3. * amp[191] -
      Complex<double> (0, 1) * amp[196] + amp[197] - Complex<double> (0, 1) *
      amp[198]);
  jamp[1] = +1./2. * (-1./3. * amp[188] - 1./3. * amp[189] - amp[190] -
      amp[191] - 1./3. * amp[192] - 1./3. * amp[193] - amp[194] - amp[195] -
      1./3. * amp[197] - 1./3. * amp[199] - Complex<double> (0, 1) * amp[200] -
      Complex<double> (0, 1) * amp[202] - amp[203]);
  jamp[2] = +1./2. * (+1./3. * amp[184] + 1./3. * amp[185] + amp[186] +
      amp[187] + amp[192] + amp[193] + 1./3. * amp[194] + 1./3. * amp[195] +
      Complex<double> (0, 1) * amp[196] + Complex<double> (0, 1) * amp[198] +
      amp[199] + 1./3. * amp[201] + 1./3. * amp[203]);
  jamp[3] = +1./2. * (-amp[180] - 1./3. * amp[181] - 1./3. * amp[182] -
      amp[183] - amp[184] - amp[185] - 1./3. * amp[186] - 1./3. * amp[187] +
      Complex<double> (0, 1) * amp[200] - amp[201] + Complex<double> (0, 1) *
      amp[202]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[8][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R8_P177_sm_qq_wpgqq::matrix_8_dux_wmgddx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[212] + amp[213] + 1./3. * amp[214] +
      amp[215] + 1./3. * amp[216] + amp[217] + amp[218] + 1./3. * amp[219] +
      Complex<double> (0, 1) * amp[222] + Complex<double> (0, 1) * amp[224] +
      amp[225]);
  jamp[1] = +1./2. * (-1./3. * amp[204] - amp[205] - 1./3. * amp[206] -
      amp[207] - amp[212] - 1./3. * amp[213] - amp[214] - 1./3. * amp[215] -
      amp[220] - Complex<double> (0, 1) * amp[221] - Complex<double> (0, 1) *
      amp[227]);
  jamp[2] = +1./2. * (+amp[204] + 1./3. * amp[205] + amp[206] + 1./3. *
      amp[207] + 1./3. * amp[208] + amp[209] + amp[210] + 1./3. * amp[211] +
      1./3. * amp[220] - Complex<double> (0, 1) * amp[222] + amp[223] -
      Complex<double> (0, 1) * amp[224] + 1./3. * amp[226]);
  jamp[3] = +1./2. * (-amp[208] - 1./3. * amp[209] - 1./3. * amp[210] -
      amp[211] - amp[216] - 1./3. * amp[217] - 1./3. * amp[218] - amp[219] +
      Complex<double> (0, 1) * amp[221] - 1./3. * amp[223] - 1./3. * amp[225] -
      amp[226] + Complex<double> (0, 1) * amp[227]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[9][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R8_P177_sm_qq_wpgqq::matrix_8_ddx_wpgdux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[228] + 1./3. * amp[229] + 1./3. * amp[230] +
      amp[231] + amp[236] + 1./3. * amp[237] + amp[238] + 1./3. * amp[239] +
      1./3. * amp[244] + amp[246] + Complex<double> (0, 1) * amp[247] +
      Complex<double> (0, 1) * amp[249] + 1./3. * amp[250]);
  jamp[1] = +1./2. * (-1./3. * amp[236] - amp[237] - 1./3. * amp[238] -
      amp[239] - amp[240] - 1./3. * amp[241] - 1./3. * amp[242] - amp[243] +
      Complex<double> (0, 1) * amp[245] - amp[250] + Complex<double> (0, 1) *
      amp[251]);
  jamp[2] = +1./2. * (+1./3. * amp[232] + amp[233] + 1./3. * amp[234] +
      amp[235] + 1./3. * amp[240] + amp[241] + amp[242] + 1./3. * amp[243] -
      Complex<double> (0, 1) * amp[247] + amp[248] - Complex<double> (0, 1) *
      amp[249]);
  jamp[3] = +1./2. * (-1./3. * amp[228] - amp[229] - amp[230] - 1./3. *
      amp[231] - amp[232] - 1./3. * amp[233] - amp[234] - 1./3. * amp[235] -
      amp[244] - Complex<double> (0, 1) * amp[245] - 1./3. * amp[246] - 1./3. *
      amp[248] - Complex<double> (0, 1) * amp[251]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[10][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R8_P177_sm_qq_wpgqq::matrix_8_ddx_wmgudx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[256] + 1./3. * amp[257] + 1./3. * amp[258] +
      amp[259] + amp[264] + 1./3. * amp[265] + 1./3. * amp[266] + amp[267] -
      Complex<double> (0, 1) * amp[269] + 1./3. * amp[271] + 1./3. * amp[273] +
      amp[274] - Complex<double> (0, 1) * amp[275]);
  jamp[1] = +1./2. * (-amp[252] - 1./3. * amp[253] - amp[254] - 1./3. *
      amp[255] - 1./3. * amp[256] - amp[257] - amp[258] - 1./3. * amp[259] -
      1./3. * amp[268] + Complex<double> (0, 1) * amp[270] - amp[271] +
      Complex<double> (0, 1) * amp[272] - 1./3. * amp[274]);
  jamp[2] = +1./2. * (+1./3. * amp[252] + amp[253] + 1./3. * amp[254] +
      amp[255] + amp[260] + 1./3. * amp[261] + amp[262] + 1./3. * amp[263] +
      amp[268] + Complex<double> (0, 1) * amp[269] + Complex<double> (0, 1) *
      amp[275]);
  jamp[3] = +1./2. * (-1./3. * amp[260] - amp[261] - 1./3. * amp[262] -
      amp[263] - 1./3. * amp[264] - amp[265] - amp[266] - 1./3. * amp[267] -
      Complex<double> (0, 1) * amp[270] - Complex<double> (0, 1) * amp[272] -
      amp[273]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[11][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R8_P177_sm_qq_wpgqq::matrix_8_uxux_wmguxdx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[284] + 1./3. * amp[285] + amp[286] +
      amp[287] + 1./3. * amp[288] + 1./3. * amp[289] + amp[290] + amp[291] +
      1./3. * amp[293] + 1./3. * amp[295] + Complex<double> (0, 1) * amp[296] +
      Complex<double> (0, 1) * amp[298] + amp[299]);
  jamp[1] = +1./2. * (-1./3. * amp[276] - amp[277] - amp[278] - 1./3. *
      amp[279] - amp[284] - amp[285] - 1./3. * amp[286] - 1./3. * amp[287] +
      Complex<double> (0, 1) * amp[292] - amp[293] + Complex<double> (0, 1) *
      amp[294]);
  jamp[2] = +1./2. * (-1./3. * amp[280] - 1./3. * amp[281] - amp[282] -
      amp[283] - amp[288] - amp[289] - 1./3. * amp[290] - 1./3. * amp[291] -
      Complex<double> (0, 1) * amp[292] - Complex<double> (0, 1) * amp[294] -
      amp[295] - 1./3. * amp[297] - 1./3. * amp[299]);
  jamp[3] = +1./2. * (+amp[276] + 1./3. * amp[277] + 1./3. * amp[278] +
      amp[279] + amp[280] + amp[281] + 1./3. * amp[282] + 1./3. * amp[283] -
      Complex<double> (0, 1) * amp[296] + amp[297] - Complex<double> (0, 1) *
      amp[298]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[12][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R8_P177_sm_qq_wpgqq::matrix_8_uxdx_wpguxux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[308] + 1./3. * amp[309] + 1./3. * amp[310] +
      amp[311] + amp[312] + 1./3. * amp[313] + 1./3. * amp[314] + amp[315] -
      Complex<double> (0, 1) * amp[317] + 1./3. * amp[319] + 1./3. * amp[321] +
      amp[322] - Complex<double> (0, 1) * amp[323]);
  jamp[1] = +1./2. * (-amp[300] - 1./3. * amp[301] - amp[302] - 1./3. *
      amp[303] - 1./3. * amp[308] - amp[309] - amp[310] - 1./3. * amp[311] -
      1./3. * amp[316] + Complex<double> (0, 1) * amp[318] - amp[319] +
      Complex<double> (0, 1) * amp[320] - 1./3. * amp[322]);
  jamp[2] = +1./2. * (-1./3. * amp[304] - amp[305] - 1./3. * amp[306] -
      amp[307] - 1./3. * amp[312] - amp[313] - amp[314] - 1./3. * amp[315] -
      Complex<double> (0, 1) * amp[318] - Complex<double> (0, 1) * amp[320] -
      amp[321]);
  jamp[3] = +1./2. * (+1./3. * amp[300] + amp[301] + 1./3. * amp[302] +
      amp[303] + amp[304] + 1./3. * amp[305] + amp[306] + 1./3. * amp[307] +
      amp[316] + Complex<double> (0, 1) * amp[317] + Complex<double> (0, 1) *
      amp[323]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[13][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R8_P177_sm_qq_wpgqq::matrix_8_uxdx_wmgdxdx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[332] + amp[333] + 1./3. * amp[334] +
      amp[335] + 1./3. * amp[336] + amp[337] + amp[338] + 1./3. * amp[339] +
      Complex<double> (0, 1) * amp[342] + Complex<double> (0, 1) * amp[344] +
      amp[345]);
  jamp[1] = +1./2. * (-1./3. * amp[324] - amp[325] - 1./3. * amp[326] -
      amp[327] - amp[332] - 1./3. * amp[333] - amp[334] - 1./3. * amp[335] -
      amp[340] - Complex<double> (0, 1) * amp[341] - Complex<double> (0, 1) *
      amp[347]);
  jamp[2] = +1./2. * (-amp[328] - 1./3. * amp[329] - 1./3. * amp[330] -
      amp[331] - amp[336] - 1./3. * amp[337] - 1./3. * amp[338] - amp[339] +
      Complex<double> (0, 1) * amp[341] - 1./3. * amp[343] - 1./3. * amp[345] -
      amp[346] + Complex<double> (0, 1) * amp[347]);
  jamp[3] = +1./2. * (+amp[324] + 1./3. * amp[325] + amp[326] + 1./3. *
      amp[327] + 1./3. * amp[328] + amp[329] + amp[330] + 1./3. * amp[331] +
      1./3. * amp[340] - Complex<double> (0, 1) * amp[342] + amp[343] -
      Complex<double> (0, 1) * amp[344] + 1./3. * amp[346]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[14][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R8_P177_sm_qq_wpgqq::matrix_8_dxdx_wpguxdx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 24;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[356] + amp[357] + 1./3. * amp[358] +
      amp[359] + amp[360] + 1./3. * amp[361] + 1./3. * amp[362] + amp[363] -
      Complex<double> (0, 1) * amp[365] + amp[370] - Complex<double> (0, 1) *
      amp[371]);
  jamp[1] = +1./2. * (-amp[348] - 1./3. * amp[349] - 1./3. * amp[350] -
      amp[351] - amp[356] - 1./3. * amp[357] - amp[358] - 1./3. * amp[359] -
      1./3. * amp[364] - amp[366] - Complex<double> (0, 1) * amp[367] -
      Complex<double> (0, 1) * amp[369] - 1./3. * amp[370]);
  jamp[2] = +1./2. * (-1./3. * amp[352] - amp[353] - 1./3. * amp[354] -
      amp[355] - 1./3. * amp[360] - amp[361] - amp[362] - 1./3. * amp[363] +
      Complex<double> (0, 1) * amp[367] - amp[368] + Complex<double> (0, 1) *
      amp[369]);
  jamp[3] = +1./2. * (+1./3. * amp[348] + amp[349] + amp[350] + 1./3. *
      amp[351] + amp[352] + 1./3. * amp[353] + amp[354] + 1./3. * amp[355] +
      amp[364] + Complex<double> (0, 1) * amp[365] + 1./3. * amp[366] + 1./3. *
      amp[368] + Complex<double> (0, 1) * amp[371]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[15][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R8_P177_sm_qq_wpgqq::matrix_8_uc_wpgus() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 12;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[0] + 1./3. * amp[372] + 1./3. * amp[373] +
      1./3. * amp[374]);
  jamp[1] = +1./2. * (-amp[373] - amp[374] - amp[377] - amp[378] -
      Complex<double> (0, 1) * amp[379] - Complex<double> (0, 1) * amp[381] -
      amp[382]);
  jamp[2] = +1./2. * (-amp[0] - amp[372] - amp[375] - amp[376] +
      Complex<double> (0, 1) * amp[379] - amp[380] + Complex<double> (0, 1) *
      amp[381]);
  jamp[3] = +1./2. * (+1./3. * amp[375] + 1./3. * amp[376] + 1./3. * amp[377] +
      1./3. * amp[378] + 1./3. * amp[380] + 1./3. * amp[382]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[16][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R8_P177_sm_qq_wpgqq::matrix_8_uc_wpgcd() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 12;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[383] + amp[384] + amp[385] + amp[386] + amp[391] +
      Complex<double> (0, 1) * amp[392] + Complex<double> (0, 1) * amp[394]);
  jamp[1] = +1./2. * (-1./3. * amp[385] - 1./3. * amp[386] - 1./3. * amp[389] -
      1./3. * amp[390]);
  jamp[2] = +1./2. * (-1./3. * amp[383] - 1./3. * amp[384] - 1./3. * amp[387] -
      1./3. * amp[388] - 1./3. * amp[391] - 1./3. * amp[393]);
  jamp[3] = +1./2. * (+amp[387] + amp[388] + amp[389] + amp[390] -
      Complex<double> (0, 1) * amp[392] + amp[393] - Complex<double> (0, 1) *
      amp[394]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[17][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R8_P177_sm_qq_wpgqq::matrix_8_us_wmguc() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 12;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[0] + 1./3. * amp[395] + 1./3. * amp[396] +
      1./3. * amp[397]);
  jamp[1] = +1./2. * (-amp[396] - amp[397] - amp[400] - amp[401] -
      Complex<double> (0, 1) * amp[402] - Complex<double> (0, 1) * amp[404] -
      amp[405]);
  jamp[2] = +1./2. * (-amp[0] - amp[395] - amp[398] - amp[399] +
      Complex<double> (0, 1) * amp[402] - amp[403] + Complex<double> (0, 1) *
      amp[404]);
  jamp[3] = +1./2. * (+1./3. * amp[398] + 1./3. * amp[399] + 1./3. * amp[400] +
      1./3. * amp[401] + 1./3. * amp[403] + 1./3. * amp[405]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[18][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R8_P177_sm_qq_wpgqq::matrix_8_uux_wpgscx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 12;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[410] + 1./3. * amp[411] + 1./3. * amp[412] +
      1./3. * amp[413] + 1./3. * amp[415] + 1./3. * amp[417]);
  jamp[1] = +1./2. * (-amp[406] - amp[407] - amp[410] - amp[411] +
      Complex<double> (0, 1) * amp[414] - amp[415] + Complex<double> (0, 1) *
      amp[416]);
  jamp[2] = +1./2. * (+1./3. * amp[406] + 1./3. * amp[407] + 1./3. * amp[408] +
      1./3. * amp[409]);
  jamp[3] = +1./2. * (-amp[408] - amp[409] - amp[412] - amp[413] -
      Complex<double> (0, 1) * amp[414] - Complex<double> (0, 1) * amp[416] -
      amp[417]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[19][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R8_P177_sm_qq_wpgqq::matrix_8_uux_wmgcsx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 12;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[422] + 1./3. * amp[423] + 1./3. * amp[424] +
      1./3. * amp[425] + 1./3. * amp[427] + 1./3. * amp[429]);
  jamp[1] = +1./2. * (-amp[418] - amp[419] - amp[422] - amp[423] +
      Complex<double> (0, 1) * amp[426] - amp[427] + Complex<double> (0, 1) *
      amp[428]);
  jamp[2] = +1./2. * (+1./3. * amp[418] + 1./3. * amp[419] + 1./3. * amp[420] +
      1./3. * amp[421]);
  jamp[3] = +1./2. * (-amp[420] - amp[421] - amp[424] - amp[425] -
      Complex<double> (0, 1) * amp[426] - Complex<double> (0, 1) * amp[428] -
      amp[429]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[20][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R8_P177_sm_qq_wpgqq::matrix_8_ucx_wpgdcx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 12;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[434] + amp[435] + amp[436] + amp[437] -
      Complex<double> (0, 1) * amp[439] + amp[440] - Complex<double> (0, 1) *
      amp[441]);
  jamp[1] = +1./2. * (-1./3. * amp[430] - 1./3. * amp[431] - 1./3. * amp[434] -
      1./3. * amp[435] - 1./3. * amp[438] - 1./3. * amp[440]);
  jamp[2] = +1./2. * (+amp[430] + amp[431] + amp[432] + amp[433] + amp[438] +
      Complex<double> (0, 1) * amp[439] + Complex<double> (0, 1) * amp[441]);
  jamp[3] = +1./2. * (-1./3. * amp[432] - 1./3. * amp[433] - 1./3. * amp[436] -
      1./3. * amp[437]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[21][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R8_P177_sm_qq_wpgqq::matrix_8_ucx_wmgusx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 12;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[444] + amp[445] + amp[448] + amp[449] +
      Complex<double> (0, 1) * amp[450] + Complex<double> (0, 1) * amp[452] +
      amp[453]);
  jamp[1] = +1./2. * (-1./3. * amp[442] - 1./3. * amp[443] - 1./3. * amp[444] -
      1./3. * amp[445]);
  jamp[2] = +1./2. * (+amp[442] + amp[443] + amp[446] + amp[447] -
      Complex<double> (0, 1) * amp[450] + amp[451] - Complex<double> (0, 1) *
      amp[452]);
  jamp[3] = +1./2. * (-1./3. * amp[446] - 1./3. * amp[447] - 1./3. * amp[448] -
      1./3. * amp[449] - 1./3. * amp[451] - 1./3. * amp[453]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[22][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R8_P177_sm_qq_wpgqq::matrix_8_udx_wpgccx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 12;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[456] + 1./3. * amp[457] + 1./3. * amp[460] +
      1./3. * amp[461]);
  jamp[1] = +1./2. * (-amp[454] - amp[455] - amp[456] - amp[457] - amp[462] -
      Complex<double> (0, 1) * amp[463] - Complex<double> (0, 1) * amp[465]);
  jamp[2] = +1./2. * (+1./3. * amp[454] + 1./3. * amp[455] + 1./3. * amp[458] +
      1./3. * amp[459] + 1./3. * amp[462] + 1./3. * amp[464]);
  jamp[3] = +1./2. * (-amp[458] - amp[459] - amp[460] - amp[461] +
      Complex<double> (0, 1) * amp[463] - amp[464] + Complex<double> (0, 1) *
      amp[465]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[23][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R8_P177_sm_qq_wpgqq::matrix_8_usx_wpgucx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 12;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[468] + amp[469] + amp[472] + amp[473] +
      Complex<double> (0, 1) * amp[474] + Complex<double> (0, 1) * amp[476] +
      amp[477]);
  jamp[1] = +1./2. * (-1./3. * amp[466] - 1./3. * amp[467] - 1./3. * amp[468] -
      1./3. * amp[469]);
  jamp[2] = +1./2. * (+amp[466] + amp[467] + amp[470] + amp[471] -
      Complex<double> (0, 1) * amp[474] + amp[475] - Complex<double> (0, 1) *
      amp[476]);
  jamp[3] = +1./2. * (-1./3. * amp[470] - 1./3. * amp[471] - 1./3. * amp[472] -
      1./3. * amp[473] - 1./3. * amp[475] - 1./3. * amp[477]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[24][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R8_P177_sm_qq_wpgqq::matrix_8_ds_wmgus() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 12;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[478] + 1./3. * amp[479] + 1./3. * amp[480] +
      1./3. * amp[481] + 1./3. * amp[486] + 1./3. * amp[488]);
  jamp[1] = +1./2. * (-amp[480] - amp[481] - amp[484] - amp[485] +
      Complex<double> (0, 1) * amp[487] - amp[488] + Complex<double> (0, 1) *
      amp[489]);
  jamp[2] = +1./2. * (-amp[478] - amp[479] - amp[482] - amp[483] - amp[486] -
      Complex<double> (0, 1) * amp[487] - Complex<double> (0, 1) * amp[489]);
  jamp[3] = +1./2. * (+1./3. * amp[482] + 1./3. * amp[483] + 1./3. * amp[484] +
      1./3. * amp[485]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[25][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R8_P177_sm_qq_wpgqq::matrix_8_dux_wmgccx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 12;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[490] + 1./3. * amp[491] + 1./3. * amp[494] +
      1./3. * amp[495]);
  jamp[1] = +1./2. * (-amp[494] - amp[495] - amp[496] - amp[497] -
      Complex<double> (0, 1) * amp[498] - Complex<double> (0, 1) * amp[500] -
      amp[501]);
  jamp[2] = +1./2. * (+1./3. * amp[492] + 1./3. * amp[493] + 1./3. * amp[496] +
      1./3. * amp[497] + 1./3. * amp[499] + 1./3. * amp[501]);
  jamp[3] = +1./2. * (-amp[490] - amp[491] - amp[492] - amp[493] +
      Complex<double> (0, 1) * amp[498] - amp[499] + Complex<double> (0, 1) *
      amp[500]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[26][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R8_P177_sm_qq_wpgqq::matrix_8_dcx_wmgucx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 12;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[502] + amp[503] + amp[504] + amp[505] -
      Complex<double> (0, 1) * amp[510] + amp[511] - Complex<double> (0, 1) *
      amp[512]);
  jamp[1] = +1./2. * (-1./3. * amp[504] - 1./3. * amp[505] - 1./3. * amp[508] -
      1./3. * amp[509] - 1./3. * amp[511] - 1./3. * amp[513]);
  jamp[2] = +1./2. * (+amp[506] + amp[507] + amp[508] + amp[509] +
      Complex<double> (0, 1) * amp[510] + Complex<double> (0, 1) * amp[512] +
      amp[513]);
  jamp[3] = +1./2. * (-1./3. * amp[502] - 1./3. * amp[503] - 1./3. * amp[506] -
      1./3. * amp[507]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[27][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R8_P177_sm_qq_wpgqq::matrix_8_uxcx_wmguxsx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 12;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[518] + 1./3. * amp[519] + 1./3. * amp[520] +
      1./3. * amp[521] + 1./3. * amp[523] + 1./3. * amp[525]);
  jamp[1] = +1./2. * (-amp[514] - amp[515] - amp[518] - amp[519] +
      Complex<double> (0, 1) * amp[522] - amp[523] + Complex<double> (0, 1) *
      amp[524]);
  jamp[2] = +1./2. * (-amp[516] - amp[517] - amp[520] - amp[521] -
      Complex<double> (0, 1) * amp[522] - Complex<double> (0, 1) * amp[524] -
      amp[525]);
  jamp[3] = +1./2. * (+1./3. * amp[514] + 1./3. * amp[515] + 1./3. * amp[516] +
      1./3. * amp[517]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[28][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R8_P177_sm_qq_wpgqq::matrix_8_uxcx_wmgcxdx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 12;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[530] + amp[531] + amp[532] + amp[533] +
      Complex<double> (0, 1) * amp[534] + Complex<double> (0, 1) * amp[536] +
      amp[537]);
  jamp[1] = +1./2. * (-1./3. * amp[526] - 1./3. * amp[527] - 1./3. * amp[530] -
      1./3. * amp[531]);
  jamp[2] = +1./2. * (-1./3. * amp[528] - 1./3. * amp[529] - 1./3. * amp[532] -
      1./3. * amp[533] - 1./3. * amp[535] - 1./3. * amp[537]);
  jamp[3] = +1./2. * (+amp[526] + amp[527] + amp[528] + amp[529] -
      Complex<double> (0, 1) * amp[534] + amp[535] - Complex<double> (0, 1) *
      amp[536]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[29][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R8_P177_sm_qq_wpgqq::matrix_8_uxsx_wpguxcx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 12;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[542] + 1./3. * amp[543] + 1./3. * amp[544] +
      1./3. * amp[545] + 1./3. * amp[547] + 1./3. * amp[549]);
  jamp[1] = +1./2. * (-amp[538] - amp[539] - amp[542] - amp[543] +
      Complex<double> (0, 1) * amp[546] - amp[547] + Complex<double> (0, 1) *
      amp[548]);
  jamp[2] = +1./2. * (-amp[540] - amp[541] - amp[544] - amp[545] -
      Complex<double> (0, 1) * amp[546] - Complex<double> (0, 1) * amp[548] -
      amp[549]);
  jamp[3] = +1./2. * (+1./3. * amp[538] + 1./3. * amp[539] + 1./3. * amp[540] +
      1./3. * amp[541]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[30][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R8_P177_sm_qq_wpgqq::matrix_8_dxsx_wpguxsx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 12;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[554] + 1./3. * amp[555] + 1./3. * amp[556] +
      1./3. * amp[557]);
  jamp[1] = +1./2. * (-amp[550] - amp[551] - amp[554] - amp[555] - amp[558] -
      Complex<double> (0, 1) * amp[559] - Complex<double> (0, 1) * amp[561]);
  jamp[2] = +1./2. * (-amp[552] - amp[553] - amp[556] - amp[557] +
      Complex<double> (0, 1) * amp[559] - amp[560] + Complex<double> (0, 1) *
      amp[561]);
  jamp[3] = +1./2. * (+1./3. * amp[550] + 1./3. * amp[551] + 1./3. * amp[552] +
      1./3. * amp[553] + 1./3. * amp[558] + 1./3. * amp[560]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[31][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}


}  // end namespace PY8MEs_namespace

