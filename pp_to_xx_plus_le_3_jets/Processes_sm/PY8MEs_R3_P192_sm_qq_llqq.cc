//==========================================================================
// This file has been automatically generated for Pythia 8 by
// MadGraph5_aMC@NLO v. 2.6.0, 2017-08-16
// By the MadGraph5_aMC@NLO Development Team
// Visit launchpad.net/madgraph5 and amcatnlo.web.cern.ch
//==========================================================================

#include "PY8MEs_R3_P192_sm_qq_llqq.h"
#include "HelAmps_sm.h"

using namespace Pythia8_sm; 

namespace PY8MEs_namespace 
{
//==========================================================================
// Class member functions for calculating the matrix elements for
// Process: u u > e+ e- u u WEIGHTED<=6 @3
// Process: u u > mu+ mu- u u WEIGHTED<=6 @3
// Process: c c > e+ e- c c WEIGHTED<=6 @3
// Process: c c > mu+ mu- c c WEIGHTED<=6 @3
// Process: u u~ > e+ e- u u~ WEIGHTED<=6 @3
// Process: u u~ > mu+ mu- u u~ WEIGHTED<=6 @3
// Process: c c~ > e+ e- c c~ WEIGHTED<=6 @3
// Process: c c~ > mu+ mu- c c~ WEIGHTED<=6 @3
// Process: d d > e+ e- d d WEIGHTED<=6 @3
// Process: d d > mu+ mu- d d WEIGHTED<=6 @3
// Process: s s > e+ e- s s WEIGHTED<=6 @3
// Process: s s > mu+ mu- s s WEIGHTED<=6 @3
// Process: d d~ > e+ e- d d~ WEIGHTED<=6 @3
// Process: d d~ > mu+ mu- d d~ WEIGHTED<=6 @3
// Process: s s~ > e+ e- s s~ WEIGHTED<=6 @3
// Process: s s~ > mu+ mu- s s~ WEIGHTED<=6 @3
// Process: u~ u~ > e+ e- u~ u~ WEIGHTED<=6 @3
// Process: u~ u~ > mu+ mu- u~ u~ WEIGHTED<=6 @3
// Process: c~ c~ > e+ e- c~ c~ WEIGHTED<=6 @3
// Process: c~ c~ > mu+ mu- c~ c~ WEIGHTED<=6 @3
// Process: d~ d~ > e+ e- d~ d~ WEIGHTED<=6 @3
// Process: d~ d~ > mu+ mu- d~ d~ WEIGHTED<=6 @3
// Process: s~ s~ > e+ e- s~ s~ WEIGHTED<=6 @3
// Process: s~ s~ > mu+ mu- s~ s~ WEIGHTED<=6 @3
// Process: u u > ve~ ve u u WEIGHTED<=6 @3
// Process: u u > vm~ vm u u WEIGHTED<=6 @3
// Process: u u > vt~ vt u u WEIGHTED<=6 @3
// Process: c c > ve~ ve c c WEIGHTED<=6 @3
// Process: c c > vm~ vm c c WEIGHTED<=6 @3
// Process: c c > vt~ vt c c WEIGHTED<=6 @3
// Process: u c > e+ e- u c WEIGHTED<=6 @3
// Process: u c > mu+ mu- u c WEIGHTED<=6 @3
// Process: u d > e+ e- u d WEIGHTED<=6 @3
// Process: u d > mu+ mu- u d WEIGHTED<=6 @3
// Process: u s > e+ e- u s WEIGHTED<=6 @3
// Process: u s > mu+ mu- u s WEIGHTED<=6 @3
// Process: c d > e+ e- c d WEIGHTED<=6 @3
// Process: c d > mu+ mu- c d WEIGHTED<=6 @3
// Process: c s > e+ e- c s WEIGHTED<=6 @3
// Process: c s > mu+ mu- c s WEIGHTED<=6 @3
// Process: u u~ > e+ e- c c~ WEIGHTED<=6 @3
// Process: u u~ > mu+ mu- c c~ WEIGHTED<=6 @3
// Process: c c~ > e+ e- u u~ WEIGHTED<=6 @3
// Process: c c~ > mu+ mu- u u~ WEIGHTED<=6 @3
// Process: u u~ > e+ e- d d~ WEIGHTED<=6 @3
// Process: u u~ > e+ e- s s~ WEIGHTED<=6 @3
// Process: u u~ > mu+ mu- d d~ WEIGHTED<=6 @3
// Process: u u~ > mu+ mu- s s~ WEIGHTED<=6 @3
// Process: c c~ > e+ e- d d~ WEIGHTED<=6 @3
// Process: c c~ > e+ e- s s~ WEIGHTED<=6 @3
// Process: c c~ > mu+ mu- d d~ WEIGHTED<=6 @3
// Process: c c~ > mu+ mu- s s~ WEIGHTED<=6 @3
// Process: u u~ > ve~ ve u u~ WEIGHTED<=6 @3
// Process: u u~ > vm~ vm u u~ WEIGHTED<=6 @3
// Process: u u~ > vt~ vt u u~ WEIGHTED<=6 @3
// Process: c c~ > ve~ ve c c~ WEIGHTED<=6 @3
// Process: c c~ > vm~ vm c c~ WEIGHTED<=6 @3
// Process: c c~ > vt~ vt c c~ WEIGHTED<=6 @3
// Process: u c~ > e+ e- u c~ WEIGHTED<=6 @3
// Process: u c~ > mu+ mu- u c~ WEIGHTED<=6 @3
// Process: c u~ > e+ e- c u~ WEIGHTED<=6 @3
// Process: c u~ > mu+ mu- c u~ WEIGHTED<=6 @3
// Process: u d~ > e+ e- u d~ WEIGHTED<=6 @3
// Process: u d~ > mu+ mu- u d~ WEIGHTED<=6 @3
// Process: u s~ > e+ e- u s~ WEIGHTED<=6 @3
// Process: u s~ > mu+ mu- u s~ WEIGHTED<=6 @3
// Process: c d~ > e+ e- c d~ WEIGHTED<=6 @3
// Process: c d~ > mu+ mu- c d~ WEIGHTED<=6 @3
// Process: c s~ > e+ e- c s~ WEIGHTED<=6 @3
// Process: c s~ > mu+ mu- c s~ WEIGHTED<=6 @3
// Process: d d > ve~ ve d d WEIGHTED<=6 @3
// Process: d d > vm~ vm d d WEIGHTED<=6 @3
// Process: d d > vt~ vt d d WEIGHTED<=6 @3
// Process: s s > ve~ ve s s WEIGHTED<=6 @3
// Process: s s > vm~ vm s s WEIGHTED<=6 @3
// Process: s s > vt~ vt s s WEIGHTED<=6 @3
// Process: d s > e+ e- d s WEIGHTED<=6 @3
// Process: d s > mu+ mu- d s WEIGHTED<=6 @3
// Process: d u~ > e+ e- d u~ WEIGHTED<=6 @3
// Process: d u~ > mu+ mu- d u~ WEIGHTED<=6 @3
// Process: d c~ > e+ e- d c~ WEIGHTED<=6 @3
// Process: d c~ > mu+ mu- d c~ WEIGHTED<=6 @3
// Process: s u~ > e+ e- s u~ WEIGHTED<=6 @3
// Process: s u~ > mu+ mu- s u~ WEIGHTED<=6 @3
// Process: s c~ > e+ e- s c~ WEIGHTED<=6 @3
// Process: s c~ > mu+ mu- s c~ WEIGHTED<=6 @3
// Process: d d~ > e+ e- u u~ WEIGHTED<=6 @3
// Process: d d~ > e+ e- c c~ WEIGHTED<=6 @3
// Process: d d~ > mu+ mu- u u~ WEIGHTED<=6 @3
// Process: d d~ > mu+ mu- c c~ WEIGHTED<=6 @3
// Process: s s~ > e+ e- u u~ WEIGHTED<=6 @3
// Process: s s~ > e+ e- c c~ WEIGHTED<=6 @3
// Process: s s~ > mu+ mu- u u~ WEIGHTED<=6 @3
// Process: s s~ > mu+ mu- c c~ WEIGHTED<=6 @3
// Process: d d~ > e+ e- s s~ WEIGHTED<=6 @3
// Process: d d~ > mu+ mu- s s~ WEIGHTED<=6 @3
// Process: s s~ > e+ e- d d~ WEIGHTED<=6 @3
// Process: s s~ > mu+ mu- d d~ WEIGHTED<=6 @3
// Process: d d~ > ve~ ve d d~ WEIGHTED<=6 @3
// Process: d d~ > vm~ vm d d~ WEIGHTED<=6 @3
// Process: d d~ > vt~ vt d d~ WEIGHTED<=6 @3
// Process: s s~ > ve~ ve s s~ WEIGHTED<=6 @3
// Process: s s~ > vm~ vm s s~ WEIGHTED<=6 @3
// Process: s s~ > vt~ vt s s~ WEIGHTED<=6 @3
// Process: d s~ > e+ e- d s~ WEIGHTED<=6 @3
// Process: d s~ > mu+ mu- d s~ WEIGHTED<=6 @3
// Process: s d~ > e+ e- s d~ WEIGHTED<=6 @3
// Process: s d~ > mu+ mu- s d~ WEIGHTED<=6 @3
// Process: u~ u~ > ve~ ve u~ u~ WEIGHTED<=6 @3
// Process: u~ u~ > vm~ vm u~ u~ WEIGHTED<=6 @3
// Process: u~ u~ > vt~ vt u~ u~ WEIGHTED<=6 @3
// Process: c~ c~ > ve~ ve c~ c~ WEIGHTED<=6 @3
// Process: c~ c~ > vm~ vm c~ c~ WEIGHTED<=6 @3
// Process: c~ c~ > vt~ vt c~ c~ WEIGHTED<=6 @3
// Process: u~ c~ > e+ e- u~ c~ WEIGHTED<=6 @3
// Process: u~ c~ > mu+ mu- u~ c~ WEIGHTED<=6 @3
// Process: u~ d~ > e+ e- u~ d~ WEIGHTED<=6 @3
// Process: u~ d~ > mu+ mu- u~ d~ WEIGHTED<=6 @3
// Process: u~ s~ > e+ e- u~ s~ WEIGHTED<=6 @3
// Process: u~ s~ > mu+ mu- u~ s~ WEIGHTED<=6 @3
// Process: c~ d~ > e+ e- c~ d~ WEIGHTED<=6 @3
// Process: c~ d~ > mu+ mu- c~ d~ WEIGHTED<=6 @3
// Process: c~ s~ > e+ e- c~ s~ WEIGHTED<=6 @3
// Process: c~ s~ > mu+ mu- c~ s~ WEIGHTED<=6 @3
// Process: d~ d~ > ve~ ve d~ d~ WEIGHTED<=6 @3
// Process: d~ d~ > vm~ vm d~ d~ WEIGHTED<=6 @3
// Process: d~ d~ > vt~ vt d~ d~ WEIGHTED<=6 @3
// Process: s~ s~ > ve~ ve s~ s~ WEIGHTED<=6 @3
// Process: s~ s~ > vm~ vm s~ s~ WEIGHTED<=6 @3
// Process: s~ s~ > vt~ vt s~ s~ WEIGHTED<=6 @3
// Process: d~ s~ > e+ e- d~ s~ WEIGHTED<=6 @3
// Process: d~ s~ > mu+ mu- d~ s~ WEIGHTED<=6 @3
// Process: u u > e+ ve u d WEIGHTED<=6 @3
// Process: u u > mu+ vm u d WEIGHTED<=6 @3
// Process: c c > e+ ve c s WEIGHTED<=6 @3
// Process: c c > mu+ vm c s WEIGHTED<=6 @3
// Process: u c > ve~ ve u c WEIGHTED<=6 @3
// Process: u c > vm~ vm u c WEIGHTED<=6 @3
// Process: u c > vt~ vt u c WEIGHTED<=6 @3
// Process: u d > e+ ve d d WEIGHTED<=6 @3
// Process: u d > mu+ vm d d WEIGHTED<=6 @3
// Process: c s > e+ ve s s WEIGHTED<=6 @3
// Process: c s > mu+ vm s s WEIGHTED<=6 @3
// Process: u d > ve~ e- u u WEIGHTED<=6 @3
// Process: u d > vm~ mu- u u WEIGHTED<=6 @3
// Process: c s > ve~ e- c c WEIGHTED<=6 @3
// Process: c s > vm~ mu- c c WEIGHTED<=6 @3
// Process: u d > ve~ ve u d WEIGHTED<=6 @3
// Process: u d > vm~ vm u d WEIGHTED<=6 @3
// Process: u d > vt~ vt u d WEIGHTED<=6 @3
// Process: u s > ve~ ve u s WEIGHTED<=6 @3
// Process: u s > vm~ vm u s WEIGHTED<=6 @3
// Process: u s > vt~ vt u s WEIGHTED<=6 @3
// Process: c d > ve~ ve c d WEIGHTED<=6 @3
// Process: c d > vm~ vm c d WEIGHTED<=6 @3
// Process: c d > vt~ vt c d WEIGHTED<=6 @3
// Process: c s > ve~ ve c s WEIGHTED<=6 @3
// Process: c s > vm~ vm c s WEIGHTED<=6 @3
// Process: c s > vt~ vt c s WEIGHTED<=6 @3
// Process: u u~ > e+ ve d u~ WEIGHTED<=6 @3
// Process: u u~ > mu+ vm d u~ WEIGHTED<=6 @3
// Process: c c~ > e+ ve s c~ WEIGHTED<=6 @3
// Process: c c~ > mu+ vm s c~ WEIGHTED<=6 @3
// Process: u u~ > ve~ e- u d~ WEIGHTED<=6 @3
// Process: u u~ > vm~ mu- u d~ WEIGHTED<=6 @3
// Process: c c~ > ve~ e- c s~ WEIGHTED<=6 @3
// Process: c c~ > vm~ mu- c s~ WEIGHTED<=6 @3
// Process: u u~ > ve~ ve c c~ WEIGHTED<=6 @3
// Process: u u~ > vm~ vm c c~ WEIGHTED<=6 @3
// Process: u u~ > vt~ vt c c~ WEIGHTED<=6 @3
// Process: c c~ > ve~ ve u u~ WEIGHTED<=6 @3
// Process: c c~ > vm~ vm u u~ WEIGHTED<=6 @3
// Process: c c~ > vt~ vt u u~ WEIGHTED<=6 @3
// Process: u u~ > ve~ ve d d~ WEIGHTED<=6 @3
// Process: u u~ > ve~ ve s s~ WEIGHTED<=6 @3
// Process: u u~ > vm~ vm d d~ WEIGHTED<=6 @3
// Process: u u~ > vm~ vm s s~ WEIGHTED<=6 @3
// Process: u u~ > vt~ vt d d~ WEIGHTED<=6 @3
// Process: u u~ > vt~ vt s s~ WEIGHTED<=6 @3
// Process: c c~ > ve~ ve d d~ WEIGHTED<=6 @3
// Process: c c~ > ve~ ve s s~ WEIGHTED<=6 @3
// Process: c c~ > vm~ vm d d~ WEIGHTED<=6 @3
// Process: c c~ > vm~ vm s s~ WEIGHTED<=6 @3
// Process: c c~ > vt~ vt d d~ WEIGHTED<=6 @3
// Process: c c~ > vt~ vt s s~ WEIGHTED<=6 @3
// Process: u c~ > ve~ ve u c~ WEIGHTED<=6 @3
// Process: u c~ > vm~ vm u c~ WEIGHTED<=6 @3
// Process: u c~ > vt~ vt u c~ WEIGHTED<=6 @3
// Process: c u~ > ve~ ve c u~ WEIGHTED<=6 @3
// Process: c u~ > vm~ vm c u~ WEIGHTED<=6 @3
// Process: c u~ > vt~ vt c u~ WEIGHTED<=6 @3
// Process: u d~ > e+ ve u u~ WEIGHTED<=6 @3
// Process: u d~ > mu+ vm u u~ WEIGHTED<=6 @3
// Process: c s~ > e+ ve c c~ WEIGHTED<=6 @3
// Process: c s~ > mu+ vm c c~ WEIGHTED<=6 @3
// Process: u d~ > e+ ve d d~ WEIGHTED<=6 @3
// Process: u d~ > mu+ vm d d~ WEIGHTED<=6 @3
// Process: c s~ > e+ ve s s~ WEIGHTED<=6 @3
// Process: c s~ > mu+ vm s s~ WEIGHTED<=6 @3
// Process: u d~ > ve~ ve u d~ WEIGHTED<=6 @3
// Process: u d~ > vm~ vm u d~ WEIGHTED<=6 @3
// Process: u d~ > vt~ vt u d~ WEIGHTED<=6 @3
// Process: u s~ > ve~ ve u s~ WEIGHTED<=6 @3
// Process: u s~ > vm~ vm u s~ WEIGHTED<=6 @3
// Process: u s~ > vt~ vt u s~ WEIGHTED<=6 @3
// Process: c d~ > ve~ ve c d~ WEIGHTED<=6 @3
// Process: c d~ > vm~ vm c d~ WEIGHTED<=6 @3
// Process: c d~ > vt~ vt c d~ WEIGHTED<=6 @3
// Process: c s~ > ve~ ve c s~ WEIGHTED<=6 @3
// Process: c s~ > vm~ vm c s~ WEIGHTED<=6 @3
// Process: c s~ > vt~ vt c s~ WEIGHTED<=6 @3
// Process: d d > ve~ e- u d WEIGHTED<=6 @3
// Process: d d > vm~ mu- u d WEIGHTED<=6 @3
// Process: s s > ve~ e- c s WEIGHTED<=6 @3
// Process: s s > vm~ mu- c s WEIGHTED<=6 @3
// Process: d s > ve~ ve d s WEIGHTED<=6 @3
// Process: d s > vm~ vm d s WEIGHTED<=6 @3
// Process: d s > vt~ vt d s WEIGHTED<=6 @3
// Process: d u~ > ve~ e- u u~ WEIGHTED<=6 @3
// Process: d u~ > vm~ mu- u u~ WEIGHTED<=6 @3
// Process: s c~ > ve~ e- c c~ WEIGHTED<=6 @3
// Process: s c~ > vm~ mu- c c~ WEIGHTED<=6 @3
// Process: d u~ > ve~ e- d d~ WEIGHTED<=6 @3
// Process: d u~ > vm~ mu- d d~ WEIGHTED<=6 @3
// Process: s c~ > ve~ e- s s~ WEIGHTED<=6 @3
// Process: s c~ > vm~ mu- s s~ WEIGHTED<=6 @3
// Process: d u~ > ve~ ve d u~ WEIGHTED<=6 @3
// Process: d u~ > vm~ vm d u~ WEIGHTED<=6 @3
// Process: d u~ > vt~ vt d u~ WEIGHTED<=6 @3
// Process: d c~ > ve~ ve d c~ WEIGHTED<=6 @3
// Process: d c~ > vm~ vm d c~ WEIGHTED<=6 @3
// Process: d c~ > vt~ vt d c~ WEIGHTED<=6 @3
// Process: s u~ > ve~ ve s u~ WEIGHTED<=6 @3
// Process: s u~ > vm~ vm s u~ WEIGHTED<=6 @3
// Process: s u~ > vt~ vt s u~ WEIGHTED<=6 @3
// Process: s c~ > ve~ ve s c~ WEIGHTED<=6 @3
// Process: s c~ > vm~ vm s c~ WEIGHTED<=6 @3
// Process: s c~ > vt~ vt s c~ WEIGHTED<=6 @3
// Process: d d~ > e+ ve d u~ WEIGHTED<=6 @3
// Process: d d~ > mu+ vm d u~ WEIGHTED<=6 @3
// Process: s s~ > e+ ve s c~ WEIGHTED<=6 @3
// Process: s s~ > mu+ vm s c~ WEIGHTED<=6 @3
// Process: d d~ > ve~ e- u d~ WEIGHTED<=6 @3
// Process: d d~ > vm~ mu- u d~ WEIGHTED<=6 @3
// Process: s s~ > ve~ e- c s~ WEIGHTED<=6 @3
// Process: s s~ > vm~ mu- c s~ WEIGHTED<=6 @3
// Process: d d~ > ve~ ve u u~ WEIGHTED<=6 @3
// Process: d d~ > ve~ ve c c~ WEIGHTED<=6 @3
// Process: d d~ > vm~ vm u u~ WEIGHTED<=6 @3
// Process: d d~ > vm~ vm c c~ WEIGHTED<=6 @3
// Process: d d~ > vt~ vt u u~ WEIGHTED<=6 @3
// Process: d d~ > vt~ vt c c~ WEIGHTED<=6 @3
// Process: s s~ > ve~ ve u u~ WEIGHTED<=6 @3
// Process: s s~ > ve~ ve c c~ WEIGHTED<=6 @3
// Process: s s~ > vm~ vm u u~ WEIGHTED<=6 @3
// Process: s s~ > vm~ vm c c~ WEIGHTED<=6 @3
// Process: s s~ > vt~ vt u u~ WEIGHTED<=6 @3
// Process: s s~ > vt~ vt c c~ WEIGHTED<=6 @3
// Process: d d~ > ve~ ve s s~ WEIGHTED<=6 @3
// Process: d d~ > vm~ vm s s~ WEIGHTED<=6 @3
// Process: d d~ > vt~ vt s s~ WEIGHTED<=6 @3
// Process: s s~ > ve~ ve d d~ WEIGHTED<=6 @3
// Process: s s~ > vm~ vm d d~ WEIGHTED<=6 @3
// Process: s s~ > vt~ vt d d~ WEIGHTED<=6 @3
// Process: d s~ > ve~ ve d s~ WEIGHTED<=6 @3
// Process: d s~ > vm~ vm d s~ WEIGHTED<=6 @3
// Process: d s~ > vt~ vt d s~ WEIGHTED<=6 @3
// Process: s d~ > ve~ ve s d~ WEIGHTED<=6 @3
// Process: s d~ > vm~ vm s d~ WEIGHTED<=6 @3
// Process: s d~ > vt~ vt s d~ WEIGHTED<=6 @3
// Process: u~ u~ > ve~ e- u~ d~ WEIGHTED<=6 @3
// Process: u~ u~ > vm~ mu- u~ d~ WEIGHTED<=6 @3
// Process: c~ c~ > ve~ e- c~ s~ WEIGHTED<=6 @3
// Process: c~ c~ > vm~ mu- c~ s~ WEIGHTED<=6 @3
// Process: u~ c~ > ve~ ve u~ c~ WEIGHTED<=6 @3
// Process: u~ c~ > vm~ vm u~ c~ WEIGHTED<=6 @3
// Process: u~ c~ > vt~ vt u~ c~ WEIGHTED<=6 @3
// Process: u~ d~ > e+ ve u~ u~ WEIGHTED<=6 @3
// Process: u~ d~ > mu+ vm u~ u~ WEIGHTED<=6 @3
// Process: c~ s~ > e+ ve c~ c~ WEIGHTED<=6 @3
// Process: c~ s~ > mu+ vm c~ c~ WEIGHTED<=6 @3
// Process: u~ d~ > ve~ e- d~ d~ WEIGHTED<=6 @3
// Process: u~ d~ > vm~ mu- d~ d~ WEIGHTED<=6 @3
// Process: c~ s~ > ve~ e- s~ s~ WEIGHTED<=6 @3
// Process: c~ s~ > vm~ mu- s~ s~ WEIGHTED<=6 @3
// Process: u~ d~ > ve~ ve u~ d~ WEIGHTED<=6 @3
// Process: u~ d~ > vm~ vm u~ d~ WEIGHTED<=6 @3
// Process: u~ d~ > vt~ vt u~ d~ WEIGHTED<=6 @3
// Process: u~ s~ > ve~ ve u~ s~ WEIGHTED<=6 @3
// Process: u~ s~ > vm~ vm u~ s~ WEIGHTED<=6 @3
// Process: u~ s~ > vt~ vt u~ s~ WEIGHTED<=6 @3
// Process: c~ d~ > ve~ ve c~ d~ WEIGHTED<=6 @3
// Process: c~ d~ > vm~ vm c~ d~ WEIGHTED<=6 @3
// Process: c~ d~ > vt~ vt c~ d~ WEIGHTED<=6 @3
// Process: c~ s~ > ve~ ve c~ s~ WEIGHTED<=6 @3
// Process: c~ s~ > vm~ vm c~ s~ WEIGHTED<=6 @3
// Process: c~ s~ > vt~ vt c~ s~ WEIGHTED<=6 @3
// Process: d~ d~ > e+ ve u~ d~ WEIGHTED<=6 @3
// Process: d~ d~ > mu+ vm u~ d~ WEIGHTED<=6 @3
// Process: s~ s~ > e+ ve c~ s~ WEIGHTED<=6 @3
// Process: s~ s~ > mu+ vm c~ s~ WEIGHTED<=6 @3
// Process: d~ s~ > ve~ ve d~ s~ WEIGHTED<=6 @3
// Process: d~ s~ > vm~ vm d~ s~ WEIGHTED<=6 @3
// Process: d~ s~ > vt~ vt d~ s~ WEIGHTED<=6 @3
// Process: u c > e+ ve u s WEIGHTED<=6 @3
// Process: u c > mu+ vm u s WEIGHTED<=6 @3
// Process: u c > e+ ve c d WEIGHTED<=6 @3
// Process: u c > mu+ vm c d WEIGHTED<=6 @3
// Process: u s > e+ ve s d WEIGHTED<=6 @3
// Process: u s > mu+ vm s d WEIGHTED<=6 @3
// Process: c d > e+ ve d s WEIGHTED<=6 @3
// Process: c d > mu+ vm d s WEIGHTED<=6 @3
// Process: u s > ve~ e- u c WEIGHTED<=6 @3
// Process: u s > vm~ mu- u c WEIGHTED<=6 @3
// Process: c d > ve~ e- c u WEIGHTED<=6 @3
// Process: c d > vm~ mu- c u WEIGHTED<=6 @3
// Process: d s > ve~ e- d c WEIGHTED<=6 @3
// Process: d s > vm~ mu- d c WEIGHTED<=6 @3
// Process: u u~ > e+ ve s c~ WEIGHTED<=6 @3
// Process: u u~ > mu+ vm s c~ WEIGHTED<=6 @3
// Process: c c~ > e+ ve d u~ WEIGHTED<=6 @3
// Process: c c~ > mu+ vm d u~ WEIGHTED<=6 @3
// Process: d d~ > e+ ve s c~ WEIGHTED<=6 @3
// Process: d d~ > mu+ vm s c~ WEIGHTED<=6 @3
// Process: s s~ > e+ ve d u~ WEIGHTED<=6 @3
// Process: s s~ > mu+ vm d u~ WEIGHTED<=6 @3
// Process: u u~ > ve~ e- c s~ WEIGHTED<=6 @3
// Process: u u~ > vm~ mu- c s~ WEIGHTED<=6 @3
// Process: c c~ > ve~ e- u d~ WEIGHTED<=6 @3
// Process: c c~ > vm~ mu- u d~ WEIGHTED<=6 @3
// Process: d d~ > ve~ e- c s~ WEIGHTED<=6 @3
// Process: d d~ > vm~ mu- c s~ WEIGHTED<=6 @3
// Process: s s~ > ve~ e- u d~ WEIGHTED<=6 @3
// Process: s s~ > vm~ mu- u d~ WEIGHTED<=6 @3
// Process: u c~ > e+ ve d c~ WEIGHTED<=6 @3
// Process: u c~ > mu+ vm d c~ WEIGHTED<=6 @3
// Process: u s~ > e+ ve d s~ WEIGHTED<=6 @3
// Process: u s~ > mu+ vm d s~ WEIGHTED<=6 @3
// Process: c u~ > e+ ve s u~ WEIGHTED<=6 @3
// Process: c u~ > mu+ vm s u~ WEIGHTED<=6 @3
// Process: c d~ > e+ ve s d~ WEIGHTED<=6 @3
// Process: c d~ > mu+ vm s d~ WEIGHTED<=6 @3
// Process: u c~ > ve~ e- u s~ WEIGHTED<=6 @3
// Process: u c~ > vm~ mu- u s~ WEIGHTED<=6 @3
// Process: c u~ > ve~ e- c d~ WEIGHTED<=6 @3
// Process: c u~ > vm~ mu- c d~ WEIGHTED<=6 @3
// Process: d c~ > ve~ e- d s~ WEIGHTED<=6 @3
// Process: d c~ > vm~ mu- d s~ WEIGHTED<=6 @3
// Process: s u~ > ve~ e- s d~ WEIGHTED<=6 @3
// Process: s u~ > vm~ mu- s d~ WEIGHTED<=6 @3
// Process: u d~ > e+ ve c c~ WEIGHTED<=6 @3
// Process: u d~ > e+ ve s s~ WEIGHTED<=6 @3
// Process: u d~ > mu+ vm c c~ WEIGHTED<=6 @3
// Process: u d~ > mu+ vm s s~ WEIGHTED<=6 @3
// Process: c s~ > e+ ve u u~ WEIGHTED<=6 @3
// Process: c s~ > e+ ve d d~ WEIGHTED<=6 @3
// Process: c s~ > mu+ vm u u~ WEIGHTED<=6 @3
// Process: c s~ > mu+ vm d d~ WEIGHTED<=6 @3
// Process: u s~ > e+ ve u c~ WEIGHTED<=6 @3
// Process: u s~ > mu+ vm u c~ WEIGHTED<=6 @3
// Process: c d~ > e+ ve c u~ WEIGHTED<=6 @3
// Process: c d~ > mu+ vm c u~ WEIGHTED<=6 @3
// Process: d s~ > e+ ve d c~ WEIGHTED<=6 @3
// Process: d s~ > mu+ vm d c~ WEIGHTED<=6 @3
// Process: s d~ > e+ ve s u~ WEIGHTED<=6 @3
// Process: s d~ > mu+ vm s u~ WEIGHTED<=6 @3
// Process: d s > ve~ e- u s WEIGHTED<=6 @3
// Process: d s > vm~ mu- u s WEIGHTED<=6 @3
// Process: d u~ > ve~ e- c c~ WEIGHTED<=6 @3
// Process: d u~ > ve~ e- s s~ WEIGHTED<=6 @3
// Process: d u~ > vm~ mu- c c~ WEIGHTED<=6 @3
// Process: d u~ > vm~ mu- s s~ WEIGHTED<=6 @3
// Process: s c~ > ve~ e- u u~ WEIGHTED<=6 @3
// Process: s c~ > ve~ e- d d~ WEIGHTED<=6 @3
// Process: s c~ > vm~ mu- u u~ WEIGHTED<=6 @3
// Process: s c~ > vm~ mu- d d~ WEIGHTED<=6 @3
// Process: d c~ > ve~ e- u c~ WEIGHTED<=6 @3
// Process: d c~ > vm~ mu- u c~ WEIGHTED<=6 @3
// Process: d s~ > ve~ e- u s~ WEIGHTED<=6 @3
// Process: d s~ > vm~ mu- u s~ WEIGHTED<=6 @3
// Process: s u~ > ve~ e- c u~ WEIGHTED<=6 @3
// Process: s u~ > vm~ mu- c u~ WEIGHTED<=6 @3
// Process: s d~ > ve~ e- c d~ WEIGHTED<=6 @3
// Process: s d~ > vm~ mu- c d~ WEIGHTED<=6 @3
// Process: u~ c~ > ve~ e- u~ s~ WEIGHTED<=6 @3
// Process: u~ c~ > vm~ mu- u~ s~ WEIGHTED<=6 @3
// Process: u~ c~ > ve~ e- c~ d~ WEIGHTED<=6 @3
// Process: u~ c~ > vm~ mu- c~ d~ WEIGHTED<=6 @3
// Process: u~ s~ > ve~ e- s~ d~ WEIGHTED<=6 @3
// Process: u~ s~ > vm~ mu- s~ d~ WEIGHTED<=6 @3
// Process: c~ d~ > ve~ e- d~ s~ WEIGHTED<=6 @3
// Process: c~ d~ > vm~ mu- d~ s~ WEIGHTED<=6 @3
// Process: u~ s~ > e+ ve u~ c~ WEIGHTED<=6 @3
// Process: u~ s~ > mu+ vm u~ c~ WEIGHTED<=6 @3
// Process: c~ d~ > e+ ve c~ u~ WEIGHTED<=6 @3
// Process: c~ d~ > mu+ vm c~ u~ WEIGHTED<=6 @3
// Process: d~ s~ > e+ ve d~ c~ WEIGHTED<=6 @3
// Process: d~ s~ > mu+ vm d~ c~ WEIGHTED<=6 @3
// Process: d~ s~ > e+ ve u~ s~ WEIGHTED<=6 @3
// Process: d~ s~ > mu+ vm u~ s~ WEIGHTED<=6 @3

// Exception class
class PY8MEs_R3_P192_sm_qq_llqqException : public exception
{
  virtual const char * what() const throw()
  {
    return "Exception in class 'PY8MEs_R3_P192_sm_qq_llqq'."; 
  }
}
PY8MEs_R3_P192_sm_qq_llqq_exception; 

std::set<int> PY8MEs_R3_P192_sm_qq_llqq::s_channel_proc = std::set<int>
    (createset<int> ());

int PY8MEs_R3_P192_sm_qq_llqq::helicities[ncomb][nexternal] = {{-1, -1, -1, -1,
    -1, -1}, {-1, -1, -1, -1, -1, 1}, {-1, -1, -1, -1, 1, -1}, {-1, -1, -1, -1,
    1, 1}, {-1, -1, -1, 1, -1, -1}, {-1, -1, -1, 1, -1, 1}, {-1, -1, -1, 1, 1,
    -1}, {-1, -1, -1, 1, 1, 1}, {-1, -1, 1, -1, -1, -1}, {-1, -1, 1, -1, -1,
    1}, {-1, -1, 1, -1, 1, -1}, {-1, -1, 1, -1, 1, 1}, {-1, -1, 1, 1, -1, -1},
    {-1, -1, 1, 1, -1, 1}, {-1, -1, 1, 1, 1, -1}, {-1, -1, 1, 1, 1, 1}, {-1, 1,
    -1, -1, -1, -1}, {-1, 1, -1, -1, -1, 1}, {-1, 1, -1, -1, 1, -1}, {-1, 1,
    -1, -1, 1, 1}, {-1, 1, -1, 1, -1, -1}, {-1, 1, -1, 1, -1, 1}, {-1, 1, -1,
    1, 1, -1}, {-1, 1, -1, 1, 1, 1}, {-1, 1, 1, -1, -1, -1}, {-1, 1, 1, -1, -1,
    1}, {-1, 1, 1, -1, 1, -1}, {-1, 1, 1, -1, 1, 1}, {-1, 1, 1, 1, -1, -1},
    {-1, 1, 1, 1, -1, 1}, {-1, 1, 1, 1, 1, -1}, {-1, 1, 1, 1, 1, 1}, {1, -1,
    -1, -1, -1, -1}, {1, -1, -1, -1, -1, 1}, {1, -1, -1, -1, 1, -1}, {1, -1,
    -1, -1, 1, 1}, {1, -1, -1, 1, -1, -1}, {1, -1, -1, 1, -1, 1}, {1, -1, -1,
    1, 1, -1}, {1, -1, -1, 1, 1, 1}, {1, -1, 1, -1, -1, -1}, {1, -1, 1, -1, -1,
    1}, {1, -1, 1, -1, 1, -1}, {1, -1, 1, -1, 1, 1}, {1, -1, 1, 1, -1, -1}, {1,
    -1, 1, 1, -1, 1}, {1, -1, 1, 1, 1, -1}, {1, -1, 1, 1, 1, 1}, {1, 1, -1, -1,
    -1, -1}, {1, 1, -1, -1, -1, 1}, {1, 1, -1, -1, 1, -1}, {1, 1, -1, -1, 1,
    1}, {1, 1, -1, 1, -1, -1}, {1, 1, -1, 1, -1, 1}, {1, 1, -1, 1, 1, -1}, {1,
    1, -1, 1, 1, 1}, {1, 1, 1, -1, -1, -1}, {1, 1, 1, -1, -1, 1}, {1, 1, 1, -1,
    1, -1}, {1, 1, 1, -1, 1, 1}, {1, 1, 1, 1, -1, -1}, {1, 1, 1, 1, -1, 1}, {1,
    1, 1, 1, 1, -1}, {1, 1, 1, 1, 1, 1}};

// Normalization factors the various processes
// Denominators: spins, colors and identical particles
int PY8MEs_R3_P192_sm_qq_llqq::denom_colors[nprocesses] = {9, 9, 9, 9, 9, 9, 9,
    9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
    9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
    9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9};
int PY8MEs_R3_P192_sm_qq_llqq::denom_hels[nprocesses] = {4, 4, 4, 4, 4, 4, 4,
    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4};
int PY8MEs_R3_P192_sm_qq_llqq::denom_iden[nprocesses] = {2, 1, 2, 1, 2, 2, 2,
    1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 2, 1, 1, 2, 1, 1, 1, 2, 2, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};

//--------------------------------------------------------------------------
// Color config initialization
void PY8MEs_R3_P192_sm_qq_llqq::initColorConfigs() 
{
  color_configs = vector < vec_vec_int > (); 
  jamp_nc_relative_power = vector < vec_int > (); 

  // Color flows of process Process: u u > e+ e- u u WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #1
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(0)(1)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 

  // Color flows of process Process: u u~ > e+ e- u u~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #1
  color_configs[1].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[1].push_back(0); 

  // Color flows of process Process: d d > e+ e- d d WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[2].push_back(vec_int(createvector<int>
      (1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #1
  color_configs[2].push_back(vec_int(createvector<int>
      (2)(0)(1)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[2].push_back(0); 

  // Color flows of process Process: d d~ > e+ e- d d~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[3].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #1
  color_configs[3].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[3].push_back(0); 

  // Color flows of process Process: u~ u~ > e+ e- u~ u~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[4].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)));
  jamp_nc_relative_power[4].push_back(0); 
  // JAMP #1
  color_configs[4].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(2)(0)(1)));
  jamp_nc_relative_power[4].push_back(0); 

  // Color flows of process Process: d~ d~ > e+ e- d~ d~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[5].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)));
  jamp_nc_relative_power[5].push_back(0); 
  // JAMP #1
  color_configs[5].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(2)(0)(1)));
  jamp_nc_relative_power[5].push_back(0); 

  // Color flows of process Process: u u > ve~ ve u u WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[6].push_back(vec_int(createvector<int>
      (1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[6].push_back(0); 
  // JAMP #1
  color_configs[6].push_back(vec_int(createvector<int>
      (2)(0)(1)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[6].push_back(0); 

  // Color flows of process Process: u c > e+ e- u c WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[7].push_back(vec_int(createvector<int>
      (1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[7].push_back(-1); 
  // JAMP #1
  color_configs[7].push_back(vec_int(createvector<int>
      (2)(0)(1)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[7].push_back(0); 

  // Color flows of process Process: u d > e+ e- u d WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[8].push_back(vec_int(createvector<int>
      (1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[8].push_back(-1); 
  // JAMP #1
  color_configs[8].push_back(vec_int(createvector<int>
      (2)(0)(1)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[8].push_back(0); 

  // Color flows of process Process: u u~ > e+ e- c c~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[9].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[9].push_back(-1); 
  // JAMP #1
  color_configs[9].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[9].push_back(0); 

  // Color flows of process Process: u u~ > e+ e- d d~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[10].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[10].push_back(-1); 
  // JAMP #1
  color_configs[10].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[10].push_back(0); 

  // Color flows of process Process: u u~ > ve~ ve u u~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[11].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[11].push_back(0); 
  // JAMP #1
  color_configs[11].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[11].push_back(0); 

  // Color flows of process Process: u c~ > e+ e- u c~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[12].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[12].push_back(0); 
  // JAMP #1
  color_configs[12].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[12].push_back(-1); 

  // Color flows of process Process: u d~ > e+ e- u d~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[13].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[13].push_back(0); 
  // JAMP #1
  color_configs[13].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[13].push_back(-1); 

  // Color flows of process Process: d d > ve~ ve d d WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[14].push_back(vec_int(createvector<int>
      (1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[14].push_back(0); 
  // JAMP #1
  color_configs[14].push_back(vec_int(createvector<int>
      (2)(0)(1)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[14].push_back(0); 

  // Color flows of process Process: d s > e+ e- d s WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[15].push_back(vec_int(createvector<int>
      (1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[15].push_back(-1); 
  // JAMP #1
  color_configs[15].push_back(vec_int(createvector<int>
      (2)(0)(1)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[15].push_back(0); 

  // Color flows of process Process: d u~ > e+ e- d u~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[16].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[16].push_back(0); 
  // JAMP #1
  color_configs[16].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[16].push_back(-1); 

  // Color flows of process Process: d d~ > e+ e- u u~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[17].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[17].push_back(-1); 
  // JAMP #1
  color_configs[17].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[17].push_back(0); 

  // Color flows of process Process: d d~ > e+ e- s s~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[18].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[18].push_back(-1); 
  // JAMP #1
  color_configs[18].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[18].push_back(0); 

  // Color flows of process Process: d d~ > ve~ ve d d~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[19].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[19].push_back(0); 
  // JAMP #1
  color_configs[19].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[19].push_back(0); 

  // Color flows of process Process: d s~ > e+ e- d s~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[20].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[20].push_back(0); 
  // JAMP #1
  color_configs[20].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[20].push_back(-1); 

  // Color flows of process Process: u~ u~ > ve~ ve u~ u~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[21].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)));
  jamp_nc_relative_power[21].push_back(0); 
  // JAMP #1
  color_configs[21].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(2)(0)(1)));
  jamp_nc_relative_power[21].push_back(0); 

  // Color flows of process Process: u~ c~ > e+ e- u~ c~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[22].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)));
  jamp_nc_relative_power[22].push_back(-1); 
  // JAMP #1
  color_configs[22].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(2)(0)(1)));
  jamp_nc_relative_power[22].push_back(0); 

  // Color flows of process Process: u~ d~ > e+ e- u~ d~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[23].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)));
  jamp_nc_relative_power[23].push_back(-1); 
  // JAMP #1
  color_configs[23].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(2)(0)(1)));
  jamp_nc_relative_power[23].push_back(0); 

  // Color flows of process Process: d~ d~ > ve~ ve d~ d~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[24].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)));
  jamp_nc_relative_power[24].push_back(0); 
  // JAMP #1
  color_configs[24].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(2)(0)(1)));
  jamp_nc_relative_power[24].push_back(0); 

  // Color flows of process Process: d~ s~ > e+ e- d~ s~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[25].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)));
  jamp_nc_relative_power[25].push_back(-1); 
  // JAMP #1
  color_configs[25].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(2)(0)(1)));
  jamp_nc_relative_power[25].push_back(0); 

  // Color flows of process Process: u u > e+ ve u d WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[26].push_back(vec_int(createvector<int>
      (1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[26].push_back(0); 
  // JAMP #1
  color_configs[26].push_back(vec_int(createvector<int>
      (2)(0)(1)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[26].push_back(0); 

  // Color flows of process Process: u c > ve~ ve u c WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[27].push_back(vec_int(createvector<int>
      (1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[27].push_back(-1); 
  // JAMP #1
  color_configs[27].push_back(vec_int(createvector<int>
      (2)(0)(1)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[27].push_back(0); 

  // Color flows of process Process: u d > e+ ve d d WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[28].push_back(vec_int(createvector<int>
      (1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[28].push_back(0); 
  // JAMP #1
  color_configs[28].push_back(vec_int(createvector<int>
      (2)(0)(1)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[28].push_back(0); 

  // Color flows of process Process: u d > ve~ e- u u WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[29].push_back(vec_int(createvector<int>
      (1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[29].push_back(0); 
  // JAMP #1
  color_configs[29].push_back(vec_int(createvector<int>
      (2)(0)(1)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[29].push_back(0); 

  // Color flows of process Process: u d > ve~ ve u d WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[30].push_back(vec_int(createvector<int>
      (1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[30].push_back(-1); 
  // JAMP #1
  color_configs[30].push_back(vec_int(createvector<int>
      (2)(0)(1)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[30].push_back(0); 

  // Color flows of process Process: u u~ > e+ ve d u~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[31].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[31].push_back(0); 
  // JAMP #1
  color_configs[31].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[31].push_back(0); 

  // Color flows of process Process: u u~ > ve~ e- u d~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[32].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[32].push_back(0); 
  // JAMP #1
  color_configs[32].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[32].push_back(0); 

  // Color flows of process Process: u u~ > ve~ ve c c~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[33].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[33].push_back(-1); 
  // JAMP #1
  color_configs[33].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[33].push_back(0); 

  // Color flows of process Process: u u~ > ve~ ve d d~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[34].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[34].push_back(-1); 
  // JAMP #1
  color_configs[34].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[34].push_back(0); 

  // Color flows of process Process: u c~ > ve~ ve u c~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[35].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[35].push_back(0); 
  // JAMP #1
  color_configs[35].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[35].push_back(-1); 

  // Color flows of process Process: u d~ > e+ ve u u~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[36].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[36].push_back(0); 
  // JAMP #1
  color_configs[36].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[36].push_back(0); 

  // Color flows of process Process: u d~ > e+ ve d d~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[37].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[37].push_back(0); 
  // JAMP #1
  color_configs[37].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[37].push_back(0); 

  // Color flows of process Process: u d~ > ve~ ve u d~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[38].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[38].push_back(0); 
  // JAMP #1
  color_configs[38].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[38].push_back(-1); 

  // Color flows of process Process: d d > ve~ e- u d WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[39].push_back(vec_int(createvector<int>
      (1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[39].push_back(0); 
  // JAMP #1
  color_configs[39].push_back(vec_int(createvector<int>
      (2)(0)(1)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[39].push_back(0); 

  // Color flows of process Process: d s > ve~ ve d s WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[40].push_back(vec_int(createvector<int>
      (1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[40].push_back(-1); 
  // JAMP #1
  color_configs[40].push_back(vec_int(createvector<int>
      (2)(0)(1)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[40].push_back(0); 

  // Color flows of process Process: d u~ > ve~ e- u u~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[41].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[41].push_back(0); 
  // JAMP #1
  color_configs[41].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[41].push_back(0); 

  // Color flows of process Process: d u~ > ve~ e- d d~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[42].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[42].push_back(0); 
  // JAMP #1
  color_configs[42].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[42].push_back(0); 

  // Color flows of process Process: d u~ > ve~ ve d u~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[43].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[43].push_back(0); 
  // JAMP #1
  color_configs[43].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[43].push_back(-1); 

  // Color flows of process Process: d d~ > e+ ve d u~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[44].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[44].push_back(0); 
  // JAMP #1
  color_configs[44].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[44].push_back(0); 

  // Color flows of process Process: d d~ > ve~ e- u d~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[45].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[45].push_back(0); 
  // JAMP #1
  color_configs[45].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[45].push_back(0); 

  // Color flows of process Process: d d~ > ve~ ve u u~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[46].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[46].push_back(-1); 
  // JAMP #1
  color_configs[46].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[46].push_back(0); 

  // Color flows of process Process: d d~ > ve~ ve s s~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[47].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[47].push_back(-1); 
  // JAMP #1
  color_configs[47].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[47].push_back(0); 

  // Color flows of process Process: d s~ > ve~ ve d s~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[48].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[48].push_back(0); 
  // JAMP #1
  color_configs[48].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[48].push_back(-1); 

  // Color flows of process Process: u~ u~ > ve~ e- u~ d~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[49].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)));
  jamp_nc_relative_power[49].push_back(0); 
  // JAMP #1
  color_configs[49].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(2)(0)(1)));
  jamp_nc_relative_power[49].push_back(0); 

  // Color flows of process Process: u~ c~ > ve~ ve u~ c~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[50].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)));
  jamp_nc_relative_power[50].push_back(-1); 
  // JAMP #1
  color_configs[50].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(2)(0)(1)));
  jamp_nc_relative_power[50].push_back(0); 

  // Color flows of process Process: u~ d~ > e+ ve u~ u~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[51].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)));
  jamp_nc_relative_power[51].push_back(0); 
  // JAMP #1
  color_configs[51].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(2)(0)(1)));
  jamp_nc_relative_power[51].push_back(0); 

  // Color flows of process Process: u~ d~ > ve~ e- d~ d~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[52].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)));
  jamp_nc_relative_power[52].push_back(0); 
  // JAMP #1
  color_configs[52].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(2)(0)(1)));
  jamp_nc_relative_power[52].push_back(0); 

  // Color flows of process Process: u~ d~ > ve~ ve u~ d~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[53].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)));
  jamp_nc_relative_power[53].push_back(-1); 
  // JAMP #1
  color_configs[53].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(2)(0)(1)));
  jamp_nc_relative_power[53].push_back(0); 

  // Color flows of process Process: d~ d~ > e+ ve u~ d~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[54].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)));
  jamp_nc_relative_power[54].push_back(0); 
  // JAMP #1
  color_configs[54].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(2)(0)(1)));
  jamp_nc_relative_power[54].push_back(0); 

  // Color flows of process Process: d~ s~ > ve~ ve d~ s~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[55].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)));
  jamp_nc_relative_power[55].push_back(-1); 
  // JAMP #1
  color_configs[55].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(2)(0)(1)));
  jamp_nc_relative_power[55].push_back(0); 

  // Color flows of process Process: u c > e+ ve u s WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[56].push_back(vec_int(createvector<int>
      (1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[56].push_back(-1); 
  // JAMP #1
  color_configs[56].push_back(vec_int(createvector<int>
      (2)(0)(1)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[56].push_back(0); 

  // Color flows of process Process: u c > e+ ve c d WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[57].push_back(vec_int(createvector<int>
      (1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[57].push_back(0); 
  // JAMP #1
  color_configs[57].push_back(vec_int(createvector<int>
      (2)(0)(1)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[57].push_back(-1); 

  // Color flows of process Process: u s > ve~ e- u c WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[58].push_back(vec_int(createvector<int>
      (1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[58].push_back(-1); 
  // JAMP #1
  color_configs[58].push_back(vec_int(createvector<int>
      (2)(0)(1)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[58].push_back(0); 

  // Color flows of process Process: u u~ > e+ ve s c~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[59].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[59].push_back(-1); 
  // JAMP #1
  color_configs[59].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[59].push_back(0); 

  // Color flows of process Process: u u~ > ve~ e- c s~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[60].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[60].push_back(-1); 
  // JAMP #1
  color_configs[60].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[60].push_back(0); 

  // Color flows of process Process: u c~ > e+ ve d c~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[61].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[61].push_back(0); 
  // JAMP #1
  color_configs[61].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[61].push_back(-1); 

  // Color flows of process Process: u c~ > ve~ e- u s~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[62].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[62].push_back(0); 
  // JAMP #1
  color_configs[62].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[62].push_back(-1); 

  // Color flows of process Process: u d~ > e+ ve c c~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[63].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[63].push_back(-1); 
  // JAMP #1
  color_configs[63].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[63].push_back(0); 

  // Color flows of process Process: u s~ > e+ ve u c~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[64].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[64].push_back(0); 
  // JAMP #1
  color_configs[64].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[64].push_back(-1); 

  // Color flows of process Process: d s > ve~ e- u s WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[65].push_back(vec_int(createvector<int>
      (1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[65].push_back(-1); 
  // JAMP #1
  color_configs[65].push_back(vec_int(createvector<int>
      (2)(0)(1)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[65].push_back(0); 

  // Color flows of process Process: d u~ > ve~ e- c c~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[66].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[66].push_back(-1); 
  // JAMP #1
  color_configs[66].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[66].push_back(0); 

  // Color flows of process Process: d c~ > ve~ e- u c~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[67].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[67].push_back(0); 
  // JAMP #1
  color_configs[67].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[67].push_back(-1); 

  // Color flows of process Process: u~ c~ > ve~ e- u~ s~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[68].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)));
  jamp_nc_relative_power[68].push_back(-1); 
  // JAMP #1
  color_configs[68].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(2)(0)(1)));
  jamp_nc_relative_power[68].push_back(0); 

  // Color flows of process Process: u~ c~ > ve~ e- c~ d~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[69].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)));
  jamp_nc_relative_power[69].push_back(0); 
  // JAMP #1
  color_configs[69].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(2)(0)(1)));
  jamp_nc_relative_power[69].push_back(-1); 

  // Color flows of process Process: u~ s~ > e+ ve u~ c~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[70].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)));
  jamp_nc_relative_power[70].push_back(-1); 
  // JAMP #1
  color_configs[70].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(2)(0)(1)));
  jamp_nc_relative_power[70].push_back(0); 

  // Color flows of process Process: d~ s~ > e+ ve u~ s~ WEIGHTED<=6 @3
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[71].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)));
  jamp_nc_relative_power[71].push_back(-1); 
  // JAMP #1
  color_configs[71].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(2)(0)(1)));
  jamp_nc_relative_power[71].push_back(0); 
}

//--------------------------------------------------------------------------
// Destructor.
PY8MEs_R3_P192_sm_qq_llqq::~PY8MEs_R3_P192_sm_qq_llqq() 
{
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    delete[] p[i]; 
    p[i] = NULL; 
  }
}

//--------------------------------------------------------------------------
// Invert the permutation mapping
vector<int> PY8MEs_R3_P192_sm_qq_llqq::invert_mapping(vector<int> mapping) 
{
  vector<int> inverted_mapping; 
  for (unsigned int i = 0; i < mapping.size(); i++ )
  {
    for (unsigned int j = 0; j < mapping.size(); j++ )
    {
      if (mapping[j] == ((int)i))
      {
        inverted_mapping.push_back(j); 
        break; 
      }
    }
  }
  return inverted_mapping; 
}

//--------------------------------------------------------------------------
// Return the list of possible helicity configurations
vector < vec_int > PY8MEs_R3_P192_sm_qq_llqq::getHelicityConfigs(vector<int>
    permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(ncomb, vector<int> (nexternal, 0)); 
  for (unsigned int ihel = 0; ihel < ncomb; ihel++ )
  {
    for(unsigned int j = 0; j < nexternal; j++ )
    {
      res[ihel][chosenPerm[j]] = helicities[ihel][j]; 
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Return the list of possible color configurations
vector < vec_int > PY8MEs_R3_P192_sm_qq_llqq::getColorConfigs(int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(color_configs[chosenProcID].size(), vector<int>
      (nexternal * 2, 0));
  for (unsigned int icol = 0; icol < color_configs[chosenProcID].size(); icol++
      )
  {
    for(unsigned int j = 0; j < (2 * nexternal); j++ )
    {
      res[icol][chosenPerm[j/2] * 2 + j%2] =
          color_configs[chosenProcID][icol][j];
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Get JAMP relative N_c power
int PY8MEs_R3_P192_sm_qq_llqq::getColorFlowRelativeNCPower(int color_flow_ID,
    int specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return jamp_nc_relative_power[chosenProcID][color_flow_ID]; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity ID -> Helicity Config
vector<int> PY8MEs_R3_P192_sm_qq_llqq::getHelicityConfigForID(int hel_ID,
    vector<int> permutation)
{
  if (hel_ID < 0 || hel_ID >= ncomb)
  {
    cerr <<  "Error in function 'getHelicityConfigForID' of class" << 
    " 'PY8MEs_R3_P192_sm_qq_llqq': Specified helicity ID '" << 
    hel_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R3_P192_sm_qq_llqq_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(nexternal, 0); 
  for (unsigned int j = 0; j < nexternal; j++ )
  {
    res[chosenPerm[j]] = helicities[hel_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity Config -> Helicity ID
int PY8MEs_R3_P192_sm_qq_llqq::getHelicityIDForConfig(vector<int> hel_config,
    vector<int> permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  int user_ihel = -1; 
  if (hel_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < ncomb; i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < nexternal; j++ )
      {
        if (helicities[i][chosenPerm[j]] != hel_config[j])
        {
          found = false; 
          break; 
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_ihel = i; 
        break; 
      }
    }
    if (user_ihel == -1)
    {
      cerr <<  "Error in function 'getHelicityIDForConfig' of class" << 
      " 'PY8MEs_R3_P192_sm_qq_llqq': Specified helicity" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R3_P192_sm_qq_llqq_exception; 
    }
  }
  return user_ihel; 
}


//--------------------------------------------------------------------------
// Implements the map Color ID -> Color Config
vector<int> PY8MEs_R3_P192_sm_qq_llqq::getColorConfigForID(int color_ID, int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < 0 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getColorConfigForID' of class" << 
    " 'PY8MEs_R3_P192_sm_qq_llqq': Specified color ID '" << 
    color_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R3_P192_sm_qq_llqq_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(color_configs[chosenProcID][color_ID].size(), 0); 
  for (unsigned int j = 0; j < (2 * nexternal); j++ )
  {
    res[chosenPerm[j/2] * 2 + j%2] = color_configs[chosenProcID][color_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Color Config -> Color ID
int PY8MEs_R3_P192_sm_qq_llqq::getColorIDForConfig(vector<int> color_config,
    int specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = -1; 
  if (color_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < color_configs[chosenProcID].size(); i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < (nexternal * 2); j++ )
      {

        // If colorless then make sure it matches
        // The little arithmetics in the color index is just
        // the permutation applies on the particle list which is
        // twice smaller since each particle can have two color indices.
        if (color_config[j] == 0)
        {
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] != 0)
          {
            found = false; 
            break; 
          }
          // Otherwise check that the color linked position matches
        }
        else
        {
          int color_linked_pos = -1; 
          // Find the other end of the line in the user color config
          for (unsigned int k = 0; k < (nexternal * 2); k++ )
          {
            if (k == j)
              continue; 
            if (color_config[j] == color_config[k])
            {
              color_linked_pos = k; 
              break; 
            }
          }
          if (color_linked_pos == -1)
          {
            cerr <<  "Error in function 'getColorIDForConfig' of class" << 
            " 'PY8MEs_R3_P192_sm_qq_llqq': A color line could " << 
            " not be closed." << endl; 
            throw PY8MEs_R3_P192_sm_qq_llqq_exception; 
          }
          // Now check whether the color line matches
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] !=
              color_configs[chosenProcID][i][chosenPerm[color_linked_pos/2] * 2
              + color_linked_pos%2])
          {
            found = false; 
            break; 
          }
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_icol = i; 
        break; 
      }
    }

    if (user_icol == -1)
    {
      cerr <<  "Error in function 'getColorIDForConfig' of class" << 
      " 'PY8MEs_R3_P192_sm_qq_llqq': Specified color" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R3_P192_sm_qq_llqq_exception; 
    }
  }
  return user_icol; 
}

//--------------------------------------------------------------------------
// Returns all result previously computed in SigmaKin
vector < vec_double > PY8MEs_R3_P192_sm_qq_llqq::getAllResults(int
    specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return all_results[chosenProcID]; 
}

//--------------------------------------------------------------------------
// Returns a result previously computed in SigmaKin for a specific helicity
// and color ID. -1 means avg and summed over that characteristic.
double PY8MEs_R3_P192_sm_qq_llqq::getResult(int helicity_ID, int color_ID, int
    specify_proc_ID)
{
  if (helicity_ID < - 1 || helicity_ID >= ncomb)
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R3_P192_sm_qq_llqq': Specified helicity ID '" << 
    helicity_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R3_P192_sm_qq_llqq_exception; 
  }
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < - 1 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R3_P192_sm_qq_llqq': Specified color ID '" << 
    color_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R3_P192_sm_qq_llqq_exception; 
  }
  return all_results[chosenProcID][helicity_ID + 1][color_ID + 1]; 
}

//--------------------------------------------------------------------------
// Check for the availability of the requested process and if available,
// If available, this returns the corresponding permutation and Proc_ID to use.
// If not available, this returns a negative Proc_ID.
pair < vector<int> , int > PY8MEs_R3_P192_sm_qq_llqq::static_getPY8ME(vector<int> initial_pdgs, vector<int> final_pdgs, set<int> schannels) 
{

  // Not available return value
  pair < vector<int> , int > NA(vector<int> (), -1); 

  // Check if s-channel requirements match
  if (nreq_s_channels > 0)
  {
    if (schannels != s_channel_proc)
      return NA; 
  }
  else
  {
    if (schannels.size() != 0)
      return NA; 
  }

  // Check number of final state particles
  if (final_pdgs.size() != (nexternal - ninitial))
    return NA; 

  // Check number of initial state particles
  if (initial_pdgs.size() != ninitial)
    return NA; 

  // List of processes available in this class
  const int nprocs = 744; 
  const int proc_IDS[nprocs] = {0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3,
      4, 4, 4, 4, 5, 5, 5, 5, 6, 6, 6, 6, 6, 6, 7, 7, 8, 8, 8, 8, 8, 8, 8, 8,
      9, 9, 9, 9, 10, 10, 10, 10, 10, 10, 10, 10, 11, 11, 11, 11, 11, 11, 12,
      12, 12, 12, 13, 13, 13, 13, 13, 13, 13, 13, 14, 14, 14, 14, 14, 14, 15,
      15, 16, 16, 16, 16, 16, 16, 16, 16, 17, 17, 17, 17, 17, 17, 17, 17, 18,
      18, 18, 18, 19, 19, 19, 19, 19, 19, 20, 20, 20, 20, 21, 21, 21, 21, 21,
      21, 22, 22, 23, 23, 23, 23, 23, 23, 23, 23, 24, 24, 24, 24, 24, 24, 25,
      25, 26, 26, 26, 26, 27, 27, 27, 28, 28, 28, 28, 29, 29, 29, 29, 30, 30,
      30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 31, 31, 31, 31, 32, 32, 32, 32,
      33, 33, 33, 33, 33, 33, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34,
      35, 35, 35, 35, 35, 35, 36, 36, 36, 36, 37, 37, 37, 37, 38, 38, 38, 38,
      38, 38, 38, 38, 38, 38, 38, 38, 39, 39, 39, 39, 40, 40, 40, 41, 41, 41,
      41, 42, 42, 42, 42, 43, 43, 43, 43, 43, 43, 43, 43, 43, 43, 43, 43, 44,
      44, 44, 44, 45, 45, 45, 45, 46, 46, 46, 46, 46, 46, 46, 46, 46, 46, 46,
      46, 47, 47, 47, 47, 47, 47, 48, 48, 48, 48, 48, 48, 49, 49, 49, 49, 50,
      50, 50, 51, 51, 51, 51, 52, 52, 52, 52, 53, 53, 53, 53, 53, 53, 53, 53,
      53, 53, 53, 53, 54, 54, 54, 54, 55, 55, 55, 56, 56, 57, 57, 57, 57, 57,
      57, 58, 58, 58, 58, 58, 58, 59, 59, 59, 59, 59, 59, 59, 59, 60, 60, 60,
      60, 60, 60, 60, 60, 61, 61, 61, 61, 61, 61, 61, 61, 62, 62, 62, 62, 62,
      62, 62, 62, 63, 63, 63, 63, 63, 63, 63, 63, 64, 64, 64, 64, 64, 64, 64,
      64, 65, 65, 66, 66, 66, 66, 66, 66, 66, 66, 67, 67, 67, 67, 67, 67, 67,
      67, 68, 68, 69, 69, 69, 69, 69, 69, 70, 70, 70, 70, 70, 70, 71, 71, 1, 1,
      1, 1, 3, 3, 3, 3, 7, 7, 8, 8, 8, 8, 8, 8, 8, 8, 9, 9, 9, 9, 10, 10, 10,
      10, 10, 10, 10, 10, 11, 11, 11, 11, 11, 11, 12, 12, 12, 12, 13, 13, 13,
      13, 13, 13, 13, 13, 15, 15, 16, 16, 16, 16, 16, 16, 16, 16, 17, 17, 17,
      17, 17, 17, 17, 17, 18, 18, 18, 18, 19, 19, 19, 19, 19, 19, 20, 20, 20,
      20, 22, 22, 23, 23, 23, 23, 23, 23, 23, 23, 25, 25, 27, 27, 27, 28, 28,
      28, 28, 29, 29, 29, 29, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30,
      31, 31, 31, 31, 32, 32, 32, 32, 33, 33, 33, 33, 33, 33, 34, 34, 34, 34,
      34, 34, 34, 34, 34, 34, 34, 34, 35, 35, 35, 35, 35, 35, 36, 36, 36, 36,
      37, 37, 37, 37, 38, 38, 38, 38, 38, 38, 38, 38, 38, 38, 38, 38, 40, 40,
      40, 41, 41, 41, 41, 42, 42, 42, 42, 43, 43, 43, 43, 43, 43, 43, 43, 43,
      43, 43, 43, 44, 44, 44, 44, 45, 45, 45, 45, 46, 46, 46, 46, 46, 46, 46,
      46, 46, 46, 46, 46, 47, 47, 47, 47, 47, 47, 48, 48, 48, 48, 48, 48, 50,
      50, 50, 51, 51, 51, 51, 52, 52, 52, 52, 53, 53, 53, 53, 53, 53, 53, 53,
      53, 53, 53, 53, 55, 55, 55, 56, 56, 57, 57, 57, 57, 57, 57, 58, 58, 58,
      58, 58, 58, 59, 59, 59, 59, 59, 59, 59, 59, 60, 60, 60, 60, 60, 60, 60,
      60, 61, 61, 61, 61, 61, 61, 61, 61, 62, 62, 62, 62, 62, 62, 62, 62, 63,
      63, 63, 63, 63, 63, 63, 63, 64, 64, 64, 64, 64, 64, 64, 64, 65, 65, 66,
      66, 66, 66, 66, 66, 66, 66, 67, 67, 67, 67, 67, 67, 67, 67, 68, 68, 69,
      69, 69, 69, 69, 69, 70, 70, 70, 70, 70, 70, 71, 71};
  const int in_pdgs[nprocs][ninitial] = {{2, 2}, {2, 2}, {4, 4}, {4, 4}, {2,
      -2}, {2, -2}, {4, -4}, {4, -4}, {1, 1}, {1, 1}, {3, 3}, {3, 3}, {1, -1},
      {1, -1}, {3, -3}, {3, -3}, {-2, -2}, {-2, -2}, {-4, -4}, {-4, -4}, {-1,
      -1}, {-1, -1}, {-3, -3}, {-3, -3}, {2, 2}, {2, 2}, {2, 2}, {4, 4}, {4,
      4}, {4, 4}, {2, 4}, {2, 4}, {2, 1}, {2, 1}, {2, 3}, {2, 3}, {4, 1}, {4,
      1}, {4, 3}, {4, 3}, {2, -2}, {2, -2}, {4, -4}, {4, -4}, {2, -2}, {2, -2},
      {2, -2}, {2, -2}, {4, -4}, {4, -4}, {4, -4}, {4, -4}, {2, -2}, {2, -2},
      {2, -2}, {4, -4}, {4, -4}, {4, -4}, {2, -4}, {2, -4}, {4, -2}, {4, -2},
      {2, -1}, {2, -1}, {2, -3}, {2, -3}, {4, -1}, {4, -1}, {4, -3}, {4, -3},
      {1, 1}, {1, 1}, {1, 1}, {3, 3}, {3, 3}, {3, 3}, {1, 3}, {1, 3}, {1, -2},
      {1, -2}, {1, -4}, {1, -4}, {3, -2}, {3, -2}, {3, -4}, {3, -4}, {1, -1},
      {1, -1}, {1, -1}, {1, -1}, {3, -3}, {3, -3}, {3, -3}, {3, -3}, {1, -1},
      {1, -1}, {3, -3}, {3, -3}, {1, -1}, {1, -1}, {1, -1}, {3, -3}, {3, -3},
      {3, -3}, {1, -3}, {1, -3}, {3, -1}, {3, -1}, {-2, -2}, {-2, -2}, {-2,
      -2}, {-4, -4}, {-4, -4}, {-4, -4}, {-2, -4}, {-2, -4}, {-2, -1}, {-2,
      -1}, {-2, -3}, {-2, -3}, {-4, -1}, {-4, -1}, {-4, -3}, {-4, -3}, {-1,
      -1}, {-1, -1}, {-1, -1}, {-3, -3}, {-3, -3}, {-3, -3}, {-1, -3}, {-1,
      -3}, {2, 2}, {2, 2}, {4, 4}, {4, 4}, {2, 4}, {2, 4}, {2, 4}, {2, 1}, {2,
      1}, {4, 3}, {4, 3}, {2, 1}, {2, 1}, {4, 3}, {4, 3}, {2, 1}, {2, 1}, {2,
      1}, {2, 3}, {2, 3}, {2, 3}, {4, 1}, {4, 1}, {4, 1}, {4, 3}, {4, 3}, {4,
      3}, {2, -2}, {2, -2}, {4, -4}, {4, -4}, {2, -2}, {2, -2}, {4, -4}, {4,
      -4}, {2, -2}, {2, -2}, {2, -2}, {4, -4}, {4, -4}, {4, -4}, {2, -2}, {2,
      -2}, {2, -2}, {2, -2}, {2, -2}, {2, -2}, {4, -4}, {4, -4}, {4, -4}, {4,
      -4}, {4, -4}, {4, -4}, {2, -4}, {2, -4}, {2, -4}, {4, -2}, {4, -2}, {4,
      -2}, {2, -1}, {2, -1}, {4, -3}, {4, -3}, {2, -1}, {2, -1}, {4, -3}, {4,
      -3}, {2, -1}, {2, -1}, {2, -1}, {2, -3}, {2, -3}, {2, -3}, {4, -1}, {4,
      -1}, {4, -1}, {4, -3}, {4, -3}, {4, -3}, {1, 1}, {1, 1}, {3, 3}, {3, 3},
      {1, 3}, {1, 3}, {1, 3}, {1, -2}, {1, -2}, {3, -4}, {3, -4}, {1, -2}, {1,
      -2}, {3, -4}, {3, -4}, {1, -2}, {1, -2}, {1, -2}, {1, -4}, {1, -4}, {1,
      -4}, {3, -2}, {3, -2}, {3, -2}, {3, -4}, {3, -4}, {3, -4}, {1, -1}, {1,
      -1}, {3, -3}, {3, -3}, {1, -1}, {1, -1}, {3, -3}, {3, -3}, {1, -1}, {1,
      -1}, {1, -1}, {1, -1}, {1, -1}, {1, -1}, {3, -3}, {3, -3}, {3, -3}, {3,
      -3}, {3, -3}, {3, -3}, {1, -1}, {1, -1}, {1, -1}, {3, -3}, {3, -3}, {3,
      -3}, {1, -3}, {1, -3}, {1, -3}, {3, -1}, {3, -1}, {3, -1}, {-2, -2}, {-2,
      -2}, {-4, -4}, {-4, -4}, {-2, -4}, {-2, -4}, {-2, -4}, {-2, -1}, {-2,
      -1}, {-4, -3}, {-4, -3}, {-2, -1}, {-2, -1}, {-4, -3}, {-4, -3}, {-2,
      -1}, {-2, -1}, {-2, -1}, {-2, -3}, {-2, -3}, {-2, -3}, {-4, -1}, {-4,
      -1}, {-4, -1}, {-4, -3}, {-4, -3}, {-4, -3}, {-1, -1}, {-1, -1}, {-3,
      -3}, {-3, -3}, {-1, -3}, {-1, -3}, {-1, -3}, {2, 4}, {2, 4}, {2, 4}, {2,
      4}, {2, 3}, {2, 3}, {4, 1}, {4, 1}, {2, 3}, {2, 3}, {4, 1}, {4, 1}, {1,
      3}, {1, 3}, {2, -2}, {2, -2}, {4, -4}, {4, -4}, {1, -1}, {1, -1}, {3,
      -3}, {3, -3}, {2, -2}, {2, -2}, {4, -4}, {4, -4}, {1, -1}, {1, -1}, {3,
      -3}, {3, -3}, {2, -4}, {2, -4}, {2, -3}, {2, -3}, {4, -2}, {4, -2}, {4,
      -1}, {4, -1}, {2, -4}, {2, -4}, {4, -2}, {4, -2}, {1, -4}, {1, -4}, {3,
      -2}, {3, -2}, {2, -1}, {2, -1}, {2, -1}, {2, -1}, {4, -3}, {4, -3}, {4,
      -3}, {4, -3}, {2, -3}, {2, -3}, {4, -1}, {4, -1}, {1, -3}, {1, -3}, {3,
      -1}, {3, -1}, {1, 3}, {1, 3}, {1, -2}, {1, -2}, {1, -2}, {1, -2}, {3,
      -4}, {3, -4}, {3, -4}, {3, -4}, {1, -4}, {1, -4}, {1, -3}, {1, -3}, {3,
      -2}, {3, -2}, {3, -1}, {3, -1}, {-2, -4}, {-2, -4}, {-2, -4}, {-2, -4},
      {-2, -3}, {-2, -3}, {-4, -1}, {-4, -1}, {-2, -3}, {-2, -3}, {-4, -1},
      {-4, -1}, {-1, -3}, {-1, -3}, {-1, -3}, {-1, -3}, {-2, 2}, {-2, 2}, {-4,
      4}, {-4, 4}, {-1, 1}, {-1, 1}, {-3, 3}, {-3, 3}, {4, 2}, {4, 2}, {1, 2},
      {1, 2}, {3, 2}, {3, 2}, {1, 4}, {1, 4}, {3, 4}, {3, 4}, {-2, 2}, {-2, 2},
      {-4, 4}, {-4, 4}, {-2, 2}, {-2, 2}, {-2, 2}, {-2, 2}, {-4, 4}, {-4, 4},
      {-4, 4}, {-4, 4}, {-2, 2}, {-2, 2}, {-2, 2}, {-4, 4}, {-4, 4}, {-4, 4},
      {-4, 2}, {-4, 2}, {-2, 4}, {-2, 4}, {-1, 2}, {-1, 2}, {-3, 2}, {-3, 2},
      {-1, 4}, {-1, 4}, {-3, 4}, {-3, 4}, {3, 1}, {3, 1}, {-2, 1}, {-2, 1},
      {-4, 1}, {-4, 1}, {-2, 3}, {-2, 3}, {-4, 3}, {-4, 3}, {-1, 1}, {-1, 1},
      {-1, 1}, {-1, 1}, {-3, 3}, {-3, 3}, {-3, 3}, {-3, 3}, {-1, 1}, {-1, 1},
      {-3, 3}, {-3, 3}, {-1, 1}, {-1, 1}, {-1, 1}, {-3, 3}, {-3, 3}, {-3, 3},
      {-3, 1}, {-3, 1}, {-1, 3}, {-1, 3}, {-4, -2}, {-4, -2}, {-1, -2}, {-1,
      -2}, {-3, -2}, {-3, -2}, {-1, -4}, {-1, -4}, {-3, -4}, {-3, -4}, {-3,
      -1}, {-3, -1}, {4, 2}, {4, 2}, {4, 2}, {1, 2}, {1, 2}, {3, 4}, {3, 4},
      {1, 2}, {1, 2}, {3, 4}, {3, 4}, {1, 2}, {1, 2}, {1, 2}, {3, 2}, {3, 2},
      {3, 2}, {1, 4}, {1, 4}, {1, 4}, {3, 4}, {3, 4}, {3, 4}, {-2, 2}, {-2, 2},
      {-4, 4}, {-4, 4}, {-2, 2}, {-2, 2}, {-4, 4}, {-4, 4}, {-2, 2}, {-2, 2},
      {-2, 2}, {-4, 4}, {-4, 4}, {-4, 4}, {-2, 2}, {-2, 2}, {-2, 2}, {-2, 2},
      {-2, 2}, {-2, 2}, {-4, 4}, {-4, 4}, {-4, 4}, {-4, 4}, {-4, 4}, {-4, 4},
      {-4, 2}, {-4, 2}, {-4, 2}, {-2, 4}, {-2, 4}, {-2, 4}, {-1, 2}, {-1, 2},
      {-3, 4}, {-3, 4}, {-1, 2}, {-1, 2}, {-3, 4}, {-3, 4}, {-1, 2}, {-1, 2},
      {-1, 2}, {-3, 2}, {-3, 2}, {-3, 2}, {-1, 4}, {-1, 4}, {-1, 4}, {-3, 4},
      {-3, 4}, {-3, 4}, {3, 1}, {3, 1}, {3, 1}, {-2, 1}, {-2, 1}, {-4, 3}, {-4,
      3}, {-2, 1}, {-2, 1}, {-4, 3}, {-4, 3}, {-2, 1}, {-2, 1}, {-2, 1}, {-4,
      1}, {-4, 1}, {-4, 1}, {-2, 3}, {-2, 3}, {-2, 3}, {-4, 3}, {-4, 3}, {-4,
      3}, {-1, 1}, {-1, 1}, {-3, 3}, {-3, 3}, {-1, 1}, {-1, 1}, {-3, 3}, {-3,
      3}, {-1, 1}, {-1, 1}, {-1, 1}, {-1, 1}, {-1, 1}, {-1, 1}, {-3, 3}, {-3,
      3}, {-3, 3}, {-3, 3}, {-3, 3}, {-3, 3}, {-1, 1}, {-1, 1}, {-1, 1}, {-3,
      3}, {-3, 3}, {-3, 3}, {-3, 1}, {-3, 1}, {-3, 1}, {-1, 3}, {-1, 3}, {-1,
      3}, {-4, -2}, {-4, -2}, {-4, -2}, {-1, -2}, {-1, -2}, {-3, -4}, {-3, -4},
      {-1, -2}, {-1, -2}, {-3, -4}, {-3, -4}, {-1, -2}, {-1, -2}, {-1, -2},
      {-3, -2}, {-3, -2}, {-3, -2}, {-1, -4}, {-1, -4}, {-1, -4}, {-3, -4},
      {-3, -4}, {-3, -4}, {-3, -1}, {-3, -1}, {-3, -1}, {4, 2}, {4, 2}, {4, 2},
      {4, 2}, {3, 2}, {3, 2}, {1, 4}, {1, 4}, {3, 2}, {3, 2}, {1, 4}, {1, 4},
      {3, 1}, {3, 1}, {-2, 2}, {-2, 2}, {-4, 4}, {-4, 4}, {-1, 1}, {-1, 1},
      {-3, 3}, {-3, 3}, {-2, 2}, {-2, 2}, {-4, 4}, {-4, 4}, {-1, 1}, {-1, 1},
      {-3, 3}, {-3, 3}, {-4, 2}, {-4, 2}, {-3, 2}, {-3, 2}, {-2, 4}, {-2, 4},
      {-1, 4}, {-1, 4}, {-4, 2}, {-4, 2}, {-2, 4}, {-2, 4}, {-4, 1}, {-4, 1},
      {-2, 3}, {-2, 3}, {-1, 2}, {-1, 2}, {-1, 2}, {-1, 2}, {-3, 4}, {-3, 4},
      {-3, 4}, {-3, 4}, {-3, 2}, {-3, 2}, {-1, 4}, {-1, 4}, {-3, 1}, {-3, 1},
      {-1, 3}, {-1, 3}, {3, 1}, {3, 1}, {-2, 1}, {-2, 1}, {-2, 1}, {-2, 1},
      {-4, 3}, {-4, 3}, {-4, 3}, {-4, 3}, {-4, 1}, {-4, 1}, {-3, 1}, {-3, 1},
      {-2, 3}, {-2, 3}, {-1, 3}, {-1, 3}, {-4, -2}, {-4, -2}, {-4, -2}, {-4,
      -2}, {-3, -2}, {-3, -2}, {-1, -4}, {-1, -4}, {-3, -2}, {-3, -2}, {-1,
      -4}, {-1, -4}, {-3, -1}, {-3, -1}, {-3, -1}, {-3, -1}};
  const int out_pdgs[nprocs][nexternal - ninitial] = {{-11, 11, 2, 2}, {-13,
      13, 2, 2}, {-11, 11, 4, 4}, {-13, 13, 4, 4}, {-11, 11, 2, -2}, {-13, 13,
      2, -2}, {-11, 11, 4, -4}, {-13, 13, 4, -4}, {-11, 11, 1, 1}, {-13, 13, 1,
      1}, {-11, 11, 3, 3}, {-13, 13, 3, 3}, {-11, 11, 1, -1}, {-13, 13, 1, -1},
      {-11, 11, 3, -3}, {-13, 13, 3, -3}, {-11, 11, -2, -2}, {-13, 13, -2, -2},
      {-11, 11, -4, -4}, {-13, 13, -4, -4}, {-11, 11, -1, -1}, {-13, 13, -1,
      -1}, {-11, 11, -3, -3}, {-13, 13, -3, -3}, {-12, 12, 2, 2}, {-14, 14, 2,
      2}, {-16, 16, 2, 2}, {-12, 12, 4, 4}, {-14, 14, 4, 4}, {-16, 16, 4, 4},
      {-11, 11, 2, 4}, {-13, 13, 2, 4}, {-11, 11, 2, 1}, {-13, 13, 2, 1}, {-11,
      11, 2, 3}, {-13, 13, 2, 3}, {-11, 11, 4, 1}, {-13, 13, 4, 1}, {-11, 11,
      4, 3}, {-13, 13, 4, 3}, {-11, 11, 4, -4}, {-13, 13, 4, -4}, {-11, 11, 2,
      -2}, {-13, 13, 2, -2}, {-11, 11, 1, -1}, {-11, 11, 3, -3}, {-13, 13, 1,
      -1}, {-13, 13, 3, -3}, {-11, 11, 1, -1}, {-11, 11, 3, -3}, {-13, 13, 1,
      -1}, {-13, 13, 3, -3}, {-12, 12, 2, -2}, {-14, 14, 2, -2}, {-16, 16, 2,
      -2}, {-12, 12, 4, -4}, {-14, 14, 4, -4}, {-16, 16, 4, -4}, {-11, 11, 2,
      -4}, {-13, 13, 2, -4}, {-11, 11, 4, -2}, {-13, 13, 4, -2}, {-11, 11, 2,
      -1}, {-13, 13, 2, -1}, {-11, 11, 2, -3}, {-13, 13, 2, -3}, {-11, 11, 4,
      -1}, {-13, 13, 4, -1}, {-11, 11, 4, -3}, {-13, 13, 4, -3}, {-12, 12, 1,
      1}, {-14, 14, 1, 1}, {-16, 16, 1, 1}, {-12, 12, 3, 3}, {-14, 14, 3, 3},
      {-16, 16, 3, 3}, {-11, 11, 1, 3}, {-13, 13, 1, 3}, {-11, 11, 1, -2},
      {-13, 13, 1, -2}, {-11, 11, 1, -4}, {-13, 13, 1, -4}, {-11, 11, 3, -2},
      {-13, 13, 3, -2}, {-11, 11, 3, -4}, {-13, 13, 3, -4}, {-11, 11, 2, -2},
      {-11, 11, 4, -4}, {-13, 13, 2, -2}, {-13, 13, 4, -4}, {-11, 11, 2, -2},
      {-11, 11, 4, -4}, {-13, 13, 2, -2}, {-13, 13, 4, -4}, {-11, 11, 3, -3},
      {-13, 13, 3, -3}, {-11, 11, 1, -1}, {-13, 13, 1, -1}, {-12, 12, 1, -1},
      {-14, 14, 1, -1}, {-16, 16, 1, -1}, {-12, 12, 3, -3}, {-14, 14, 3, -3},
      {-16, 16, 3, -3}, {-11, 11, 1, -3}, {-13, 13, 1, -3}, {-11, 11, 3, -1},
      {-13, 13, 3, -1}, {-12, 12, -2, -2}, {-14, 14, -2, -2}, {-16, 16, -2,
      -2}, {-12, 12, -4, -4}, {-14, 14, -4, -4}, {-16, 16, -4, -4}, {-11, 11,
      -2, -4}, {-13, 13, -2, -4}, {-11, 11, -2, -1}, {-13, 13, -2, -1}, {-11,
      11, -2, -3}, {-13, 13, -2, -3}, {-11, 11, -4, -1}, {-13, 13, -4, -1},
      {-11, 11, -4, -3}, {-13, 13, -4, -3}, {-12, 12, -1, -1}, {-14, 14, -1,
      -1}, {-16, 16, -1, -1}, {-12, 12, -3, -3}, {-14, 14, -3, -3}, {-16, 16,
      -3, -3}, {-11, 11, -1, -3}, {-13, 13, -1, -3}, {-11, 12, 2, 1}, {-13, 14,
      2, 1}, {-11, 12, 4, 3}, {-13, 14, 4, 3}, {-12, 12, 2, 4}, {-14, 14, 2,
      4}, {-16, 16, 2, 4}, {-11, 12, 1, 1}, {-13, 14, 1, 1}, {-11, 12, 3, 3},
      {-13, 14, 3, 3}, {-12, 11, 2, 2}, {-14, 13, 2, 2}, {-12, 11, 4, 4}, {-14,
      13, 4, 4}, {-12, 12, 2, 1}, {-14, 14, 2, 1}, {-16, 16, 2, 1}, {-12, 12,
      2, 3}, {-14, 14, 2, 3}, {-16, 16, 2, 3}, {-12, 12, 4, 1}, {-14, 14, 4,
      1}, {-16, 16, 4, 1}, {-12, 12, 4, 3}, {-14, 14, 4, 3}, {-16, 16, 4, 3},
      {-11, 12, 1, -2}, {-13, 14, 1, -2}, {-11, 12, 3, -4}, {-13, 14, 3, -4},
      {-12, 11, 2, -1}, {-14, 13, 2, -1}, {-12, 11, 4, -3}, {-14, 13, 4, -3},
      {-12, 12, 4, -4}, {-14, 14, 4, -4}, {-16, 16, 4, -4}, {-12, 12, 2, -2},
      {-14, 14, 2, -2}, {-16, 16, 2, -2}, {-12, 12, 1, -1}, {-12, 12, 3, -3},
      {-14, 14, 1, -1}, {-14, 14, 3, -3}, {-16, 16, 1, -1}, {-16, 16, 3, -3},
      {-12, 12, 1, -1}, {-12, 12, 3, -3}, {-14, 14, 1, -1}, {-14, 14, 3, -3},
      {-16, 16, 1, -1}, {-16, 16, 3, -3}, {-12, 12, 2, -4}, {-14, 14, 2, -4},
      {-16, 16, 2, -4}, {-12, 12, 4, -2}, {-14, 14, 4, -2}, {-16, 16, 4, -2},
      {-11, 12, 2, -2}, {-13, 14, 2, -2}, {-11, 12, 4, -4}, {-13, 14, 4, -4},
      {-11, 12, 1, -1}, {-13, 14, 1, -1}, {-11, 12, 3, -3}, {-13, 14, 3, -3},
      {-12, 12, 2, -1}, {-14, 14, 2, -1}, {-16, 16, 2, -1}, {-12, 12, 2, -3},
      {-14, 14, 2, -3}, {-16, 16, 2, -3}, {-12, 12, 4, -1}, {-14, 14, 4, -1},
      {-16, 16, 4, -1}, {-12, 12, 4, -3}, {-14, 14, 4, -3}, {-16, 16, 4, -3},
      {-12, 11, 2, 1}, {-14, 13, 2, 1}, {-12, 11, 4, 3}, {-14, 13, 4, 3}, {-12,
      12, 1, 3}, {-14, 14, 1, 3}, {-16, 16, 1, 3}, {-12, 11, 2, -2}, {-14, 13,
      2, -2}, {-12, 11, 4, -4}, {-14, 13, 4, -4}, {-12, 11, 1, -1}, {-14, 13,
      1, -1}, {-12, 11, 3, -3}, {-14, 13, 3, -3}, {-12, 12, 1, -2}, {-14, 14,
      1, -2}, {-16, 16, 1, -2}, {-12, 12, 1, -4}, {-14, 14, 1, -4}, {-16, 16,
      1, -4}, {-12, 12, 3, -2}, {-14, 14, 3, -2}, {-16, 16, 3, -2}, {-12, 12,
      3, -4}, {-14, 14, 3, -4}, {-16, 16, 3, -4}, {-11, 12, 1, -2}, {-13, 14,
      1, -2}, {-11, 12, 3, -4}, {-13, 14, 3, -4}, {-12, 11, 2, -1}, {-14, 13,
      2, -1}, {-12, 11, 4, -3}, {-14, 13, 4, -3}, {-12, 12, 2, -2}, {-12, 12,
      4, -4}, {-14, 14, 2, -2}, {-14, 14, 4, -4}, {-16, 16, 2, -2}, {-16, 16,
      4, -4}, {-12, 12, 2, -2}, {-12, 12, 4, -4}, {-14, 14, 2, -2}, {-14, 14,
      4, -4}, {-16, 16, 2, -2}, {-16, 16, 4, -4}, {-12, 12, 3, -3}, {-14, 14,
      3, -3}, {-16, 16, 3, -3}, {-12, 12, 1, -1}, {-14, 14, 1, -1}, {-16, 16,
      1, -1}, {-12, 12, 1, -3}, {-14, 14, 1, -3}, {-16, 16, 1, -3}, {-12, 12,
      3, -1}, {-14, 14, 3, -1}, {-16, 16, 3, -1}, {-12, 11, -2, -1}, {-14, 13,
      -2, -1}, {-12, 11, -4, -3}, {-14, 13, -4, -3}, {-12, 12, -2, -4}, {-14,
      14, -2, -4}, {-16, 16, -2, -4}, {-11, 12, -2, -2}, {-13, 14, -2, -2},
      {-11, 12, -4, -4}, {-13, 14, -4, -4}, {-12, 11, -1, -1}, {-14, 13, -1,
      -1}, {-12, 11, -3, -3}, {-14, 13, -3, -3}, {-12, 12, -2, -1}, {-14, 14,
      -2, -1}, {-16, 16, -2, -1}, {-12, 12, -2, -3}, {-14, 14, -2, -3}, {-16,
      16, -2, -3}, {-12, 12, -4, -1}, {-14, 14, -4, -1}, {-16, 16, -4, -1},
      {-12, 12, -4, -3}, {-14, 14, -4, -3}, {-16, 16, -4, -3}, {-11, 12, -2,
      -1}, {-13, 14, -2, -1}, {-11, 12, -4, -3}, {-13, 14, -4, -3}, {-12, 12,
      -1, -3}, {-14, 14, -1, -3}, {-16, 16, -1, -3}, {-11, 12, 2, 3}, {-13, 14,
      2, 3}, {-11, 12, 4, 1}, {-13, 14, 4, 1}, {-11, 12, 3, 1}, {-13, 14, 3,
      1}, {-11, 12, 1, 3}, {-13, 14, 1, 3}, {-12, 11, 2, 4}, {-14, 13, 2, 4},
      {-12, 11, 4, 2}, {-14, 13, 4, 2}, {-12, 11, 1, 4}, {-14, 13, 1, 4}, {-11,
      12, 3, -4}, {-13, 14, 3, -4}, {-11, 12, 1, -2}, {-13, 14, 1, -2}, {-11,
      12, 3, -4}, {-13, 14, 3, -4}, {-11, 12, 1, -2}, {-13, 14, 1, -2}, {-12,
      11, 4, -3}, {-14, 13, 4, -3}, {-12, 11, 2, -1}, {-14, 13, 2, -1}, {-12,
      11, 4, -3}, {-14, 13, 4, -3}, {-12, 11, 2, -1}, {-14, 13, 2, -1}, {-11,
      12, 1, -4}, {-13, 14, 1, -4}, {-11, 12, 1, -3}, {-13, 14, 1, -3}, {-11,
      12, 3, -2}, {-13, 14, 3, -2}, {-11, 12, 3, -1}, {-13, 14, 3, -1}, {-12,
      11, 2, -3}, {-14, 13, 2, -3}, {-12, 11, 4, -1}, {-14, 13, 4, -1}, {-12,
      11, 1, -3}, {-14, 13, 1, -3}, {-12, 11, 3, -1}, {-14, 13, 3, -1}, {-11,
      12, 4, -4}, {-11, 12, 3, -3}, {-13, 14, 4, -4}, {-13, 14, 3, -3}, {-11,
      12, 2, -2}, {-11, 12, 1, -1}, {-13, 14, 2, -2}, {-13, 14, 1, -1}, {-11,
      12, 2, -4}, {-13, 14, 2, -4}, {-11, 12, 4, -2}, {-13, 14, 4, -2}, {-11,
      12, 1, -4}, {-13, 14, 1, -4}, {-11, 12, 3, -2}, {-13, 14, 3, -2}, {-12,
      11, 2, 3}, {-14, 13, 2, 3}, {-12, 11, 4, -4}, {-12, 11, 3, -3}, {-14, 13,
      4, -4}, {-14, 13, 3, -3}, {-12, 11, 2, -2}, {-12, 11, 1, -1}, {-14, 13,
      2, -2}, {-14, 13, 1, -1}, {-12, 11, 2, -4}, {-14, 13, 2, -4}, {-12, 11,
      2, -3}, {-14, 13, 2, -3}, {-12, 11, 4, -2}, {-14, 13, 4, -2}, {-12, 11,
      4, -1}, {-14, 13, 4, -1}, {-12, 11, -2, -3}, {-14, 13, -2, -3}, {-12, 11,
      -4, -1}, {-14, 13, -4, -1}, {-12, 11, -3, -1}, {-14, 13, -3, -1}, {-12,
      11, -1, -3}, {-14, 13, -1, -3}, {-11, 12, -2, -4}, {-13, 14, -2, -4},
      {-11, 12, -4, -2}, {-13, 14, -4, -2}, {-11, 12, -1, -4}, {-13, 14, -1,
      -4}, {-11, 12, -2, -3}, {-13, 14, -2, -3}, {-11, 11, 2, -2}, {-13, 13, 2,
      -2}, {-11, 11, 4, -4}, {-13, 13, 4, -4}, {-11, 11, 1, -1}, {-13, 13, 1,
      -1}, {-11, 11, 3, -3}, {-13, 13, 3, -3}, {-11, 11, 2, 4}, {-13, 13, 2,
      4}, {-11, 11, 2, 1}, {-13, 13, 2, 1}, {-11, 11, 2, 3}, {-13, 13, 2, 3},
      {-11, 11, 4, 1}, {-13, 13, 4, 1}, {-11, 11, 4, 3}, {-13, 13, 4, 3}, {-11,
      11, 4, -4}, {-13, 13, 4, -4}, {-11, 11, 2, -2}, {-13, 13, 2, -2}, {-11,
      11, 1, -1}, {-11, 11, 3, -3}, {-13, 13, 1, -1}, {-13, 13, 3, -3}, {-11,
      11, 1, -1}, {-11, 11, 3, -3}, {-13, 13, 1, -1}, {-13, 13, 3, -3}, {-12,
      12, 2, -2}, {-14, 14, 2, -2}, {-16, 16, 2, -2}, {-12, 12, 4, -4}, {-14,
      14, 4, -4}, {-16, 16, 4, -4}, {-11, 11, 2, -4}, {-13, 13, 2, -4}, {-11,
      11, 4, -2}, {-13, 13, 4, -2}, {-11, 11, 2, -1}, {-13, 13, 2, -1}, {-11,
      11, 2, -3}, {-13, 13, 2, -3}, {-11, 11, 4, -1}, {-13, 13, 4, -1}, {-11,
      11, 4, -3}, {-13, 13, 4, -3}, {-11, 11, 1, 3}, {-13, 13, 1, 3}, {-11, 11,
      1, -2}, {-13, 13, 1, -2}, {-11, 11, 1, -4}, {-13, 13, 1, -4}, {-11, 11,
      3, -2}, {-13, 13, 3, -2}, {-11, 11, 3, -4}, {-13, 13, 3, -4}, {-11, 11,
      2, -2}, {-11, 11, 4, -4}, {-13, 13, 2, -2}, {-13, 13, 4, -4}, {-11, 11,
      2, -2}, {-11, 11, 4, -4}, {-13, 13, 2, -2}, {-13, 13, 4, -4}, {-11, 11,
      3, -3}, {-13, 13, 3, -3}, {-11, 11, 1, -1}, {-13, 13, 1, -1}, {-12, 12,
      1, -1}, {-14, 14, 1, -1}, {-16, 16, 1, -1}, {-12, 12, 3, -3}, {-14, 14,
      3, -3}, {-16, 16, 3, -3}, {-11, 11, 1, -3}, {-13, 13, 1, -3}, {-11, 11,
      3, -1}, {-13, 13, 3, -1}, {-11, 11, -2, -4}, {-13, 13, -2, -4}, {-11, 11,
      -2, -1}, {-13, 13, -2, -1}, {-11, 11, -2, -3}, {-13, 13, -2, -3}, {-11,
      11, -4, -1}, {-13, 13, -4, -1}, {-11, 11, -4, -3}, {-13, 13, -4, -3},
      {-11, 11, -1, -3}, {-13, 13, -1, -3}, {-12, 12, 2, 4}, {-14, 14, 2, 4},
      {-16, 16, 2, 4}, {-11, 12, 1, 1}, {-13, 14, 1, 1}, {-11, 12, 3, 3}, {-13,
      14, 3, 3}, {-12, 11, 2, 2}, {-14, 13, 2, 2}, {-12, 11, 4, 4}, {-14, 13,
      4, 4}, {-12, 12, 2, 1}, {-14, 14, 2, 1}, {-16, 16, 2, 1}, {-12, 12, 2,
      3}, {-14, 14, 2, 3}, {-16, 16, 2, 3}, {-12, 12, 4, 1}, {-14, 14, 4, 1},
      {-16, 16, 4, 1}, {-12, 12, 4, 3}, {-14, 14, 4, 3}, {-16, 16, 4, 3}, {-11,
      12, 1, -2}, {-13, 14, 1, -2}, {-11, 12, 3, -4}, {-13, 14, 3, -4}, {-12,
      11, 2, -1}, {-14, 13, 2, -1}, {-12, 11, 4, -3}, {-14, 13, 4, -3}, {-12,
      12, 4, -4}, {-14, 14, 4, -4}, {-16, 16, 4, -4}, {-12, 12, 2, -2}, {-14,
      14, 2, -2}, {-16, 16, 2, -2}, {-12, 12, 1, -1}, {-12, 12, 3, -3}, {-14,
      14, 1, -1}, {-14, 14, 3, -3}, {-16, 16, 1, -1}, {-16, 16, 3, -3}, {-12,
      12, 1, -1}, {-12, 12, 3, -3}, {-14, 14, 1, -1}, {-14, 14, 3, -3}, {-16,
      16, 1, -1}, {-16, 16, 3, -3}, {-12, 12, 2, -4}, {-14, 14, 2, -4}, {-16,
      16, 2, -4}, {-12, 12, 4, -2}, {-14, 14, 4, -2}, {-16, 16, 4, -2}, {-11,
      12, 2, -2}, {-13, 14, 2, -2}, {-11, 12, 4, -4}, {-13, 14, 4, -4}, {-11,
      12, 1, -1}, {-13, 14, 1, -1}, {-11, 12, 3, -3}, {-13, 14, 3, -3}, {-12,
      12, 2, -1}, {-14, 14, 2, -1}, {-16, 16, 2, -1}, {-12, 12, 2, -3}, {-14,
      14, 2, -3}, {-16, 16, 2, -3}, {-12, 12, 4, -1}, {-14, 14, 4, -1}, {-16,
      16, 4, -1}, {-12, 12, 4, -3}, {-14, 14, 4, -3}, {-16, 16, 4, -3}, {-12,
      12, 1, 3}, {-14, 14, 1, 3}, {-16, 16, 1, 3}, {-12, 11, 2, -2}, {-14, 13,
      2, -2}, {-12, 11, 4, -4}, {-14, 13, 4, -4}, {-12, 11, 1, -1}, {-14, 13,
      1, -1}, {-12, 11, 3, -3}, {-14, 13, 3, -3}, {-12, 12, 1, -2}, {-14, 14,
      1, -2}, {-16, 16, 1, -2}, {-12, 12, 1, -4}, {-14, 14, 1, -4}, {-16, 16,
      1, -4}, {-12, 12, 3, -2}, {-14, 14, 3, -2}, {-16, 16, 3, -2}, {-12, 12,
      3, -4}, {-14, 14, 3, -4}, {-16, 16, 3, -4}, {-11, 12, 1, -2}, {-13, 14,
      1, -2}, {-11, 12, 3, -4}, {-13, 14, 3, -4}, {-12, 11, 2, -1}, {-14, 13,
      2, -1}, {-12, 11, 4, -3}, {-14, 13, 4, -3}, {-12, 12, 2, -2}, {-12, 12,
      4, -4}, {-14, 14, 2, -2}, {-14, 14, 4, -4}, {-16, 16, 2, -2}, {-16, 16,
      4, -4}, {-12, 12, 2, -2}, {-12, 12, 4, -4}, {-14, 14, 2, -2}, {-14, 14,
      4, -4}, {-16, 16, 2, -2}, {-16, 16, 4, -4}, {-12, 12, 3, -3}, {-14, 14,
      3, -3}, {-16, 16, 3, -3}, {-12, 12, 1, -1}, {-14, 14, 1, -1}, {-16, 16,
      1, -1}, {-12, 12, 1, -3}, {-14, 14, 1, -3}, {-16, 16, 1, -3}, {-12, 12,
      3, -1}, {-14, 14, 3, -1}, {-16, 16, 3, -1}, {-12, 12, -2, -4}, {-14, 14,
      -2, -4}, {-16, 16, -2, -4}, {-11, 12, -2, -2}, {-13, 14, -2, -2}, {-11,
      12, -4, -4}, {-13, 14, -4, -4}, {-12, 11, -1, -1}, {-14, 13, -1, -1},
      {-12, 11, -3, -3}, {-14, 13, -3, -3}, {-12, 12, -2, -1}, {-14, 14, -2,
      -1}, {-16, 16, -2, -1}, {-12, 12, -2, -3}, {-14, 14, -2, -3}, {-16, 16,
      -2, -3}, {-12, 12, -4, -1}, {-14, 14, -4, -1}, {-16, 16, -4, -1}, {-12,
      12, -4, -3}, {-14, 14, -4, -3}, {-16, 16, -4, -3}, {-12, 12, -1, -3},
      {-14, 14, -1, -3}, {-16, 16, -1, -3}, {-11, 12, 2, 3}, {-13, 14, 2, 3},
      {-11, 12, 4, 1}, {-13, 14, 4, 1}, {-11, 12, 3, 1}, {-13, 14, 3, 1}, {-11,
      12, 1, 3}, {-13, 14, 1, 3}, {-12, 11, 2, 4}, {-14, 13, 2, 4}, {-12, 11,
      4, 2}, {-14, 13, 4, 2}, {-12, 11, 1, 4}, {-14, 13, 1, 4}, {-11, 12, 3,
      -4}, {-13, 14, 3, -4}, {-11, 12, 1, -2}, {-13, 14, 1, -2}, {-11, 12, 3,
      -4}, {-13, 14, 3, -4}, {-11, 12, 1, -2}, {-13, 14, 1, -2}, {-12, 11, 4,
      -3}, {-14, 13, 4, -3}, {-12, 11, 2, -1}, {-14, 13, 2, -1}, {-12, 11, 4,
      -3}, {-14, 13, 4, -3}, {-12, 11, 2, -1}, {-14, 13, 2, -1}, {-11, 12, 1,
      -4}, {-13, 14, 1, -4}, {-11, 12, 1, -3}, {-13, 14, 1, -3}, {-11, 12, 3,
      -2}, {-13, 14, 3, -2}, {-11, 12, 3, -1}, {-13, 14, 3, -1}, {-12, 11, 2,
      -3}, {-14, 13, 2, -3}, {-12, 11, 4, -1}, {-14, 13, 4, -1}, {-12, 11, 1,
      -3}, {-14, 13, 1, -3}, {-12, 11, 3, -1}, {-14, 13, 3, -1}, {-11, 12, 4,
      -4}, {-11, 12, 3, -3}, {-13, 14, 4, -4}, {-13, 14, 3, -3}, {-11, 12, 2,
      -2}, {-11, 12, 1, -1}, {-13, 14, 2, -2}, {-13, 14, 1, -1}, {-11, 12, 2,
      -4}, {-13, 14, 2, -4}, {-11, 12, 4, -2}, {-13, 14, 4, -2}, {-11, 12, 1,
      -4}, {-13, 14, 1, -4}, {-11, 12, 3, -2}, {-13, 14, 3, -2}, {-12, 11, 2,
      3}, {-14, 13, 2, 3}, {-12, 11, 4, -4}, {-12, 11, 3, -3}, {-14, 13, 4,
      -4}, {-14, 13, 3, -3}, {-12, 11, 2, -2}, {-12, 11, 1, -1}, {-14, 13, 2,
      -2}, {-14, 13, 1, -1}, {-12, 11, 2, -4}, {-14, 13, 2, -4}, {-12, 11, 2,
      -3}, {-14, 13, 2, -3}, {-12, 11, 4, -2}, {-14, 13, 4, -2}, {-12, 11, 4,
      -1}, {-14, 13, 4, -1}, {-12, 11, -2, -3}, {-14, 13, -2, -3}, {-12, 11,
      -4, -1}, {-14, 13, -4, -1}, {-12, 11, -3, -1}, {-14, 13, -3, -1}, {-12,
      11, -1, -3}, {-14, 13, -1, -3}, {-11, 12, -2, -4}, {-13, 14, -2, -4},
      {-11, 12, -4, -2}, {-13, 14, -4, -2}, {-11, 12, -1, -4}, {-13, 14, -1,
      -4}, {-11, 12, -2, -3}, {-13, 14, -2, -3}};

  bool in_pdgs_used[ninitial]; 
  bool out_pdgs_used[nexternal - ninitial]; 
  for(unsigned int i = 0; i < nprocs; i++ )
  {
    int permutations[nexternal]; 

    // Reinitialize initial state look-up variables
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      in_pdgs_used[j] = false; 
      permutations[j] = -1; 
    }
    // Look for initial state matches
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      for(unsigned int k = 0; k < ninitial; k++ )
      {
        // Make sure it has not been used already
        if (in_pdgs_used[k])
          continue; 
        if (initial_pdgs[k] == in_pdgs[i][j])
        {
          permutations[j] = k; 
          in_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[ninitial - 1] == -1)
      continue; 

    // Reinitialize final state look-up variables
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      out_pdgs_used[j] = false; 
      permutations[ninitial + j] = -1; 
    }
    // Look for final state matches
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      for(unsigned int k = 0; k < (nexternal - ninitial); k++ )
      {
        // Make sure it has not been used already
        if (out_pdgs_used[k])
          continue; 
        if (final_pdgs[k] == out_pdgs[i][j])
        {
          permutations[ninitial + j] = ninitial + k; 
          out_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[ninitial + j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[nexternal - 1] == -1)
      continue; 

    // Return process found
    return pair < vector<int> , int > (vector<int> (permutations, permutations
        + nexternal), proc_IDS[i]);
  }

  // No process found
  return NA; 
}

//--------------------------------------------------------------------------
// Set momenta
void PY8MEs_R3_P192_sm_qq_llqq::setMomenta(vector < vec_double > momenta_picked)
{
  if (momenta_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setMomenta' of class" << 
    " 'PY8MEs_R3_P192_sm_qq_llqq': Incorrect number of" << 
    " momenta specified." << endl; 
    throw PY8MEs_R3_P192_sm_qq_llqq_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    if (momenta_picked[i].size() != 4)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R3_P192_sm_qq_llqq': Incorrect number of" << 
      " momenta components specified." << endl; 
      throw PY8MEs_R3_P192_sm_qq_llqq_exception; 
    }
    if (momenta_picked[i][0] < 0.0)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R3_P192_sm_qq_llqq': A momentum was specified" << 
      " with negative energy. Check conventions." << endl; 
      throw PY8MEs_R3_P192_sm_qq_llqq_exception; 
    }
    for (unsigned int j = 0; j < 4; j++ )
    {
      p[i][j] = momenta_picked[i][j]; 
    }
  }
}

//--------------------------------------------------------------------------
// Set color configuration to use. An empty vector means sum over all.
void PY8MEs_R3_P192_sm_qq_llqq::setColors(vector<int> colors_picked)
{
  if (colors_picked.size() == 0)
  {
    user_colors = vector<int> (); 
    return; 
  }
  if (colors_picked.size() != (2 * nexternal))
  {
    cerr <<  "Error in function 'setColors' of class" << 
    " 'PY8MEs_R3_P192_sm_qq_llqq': Incorrect number" << 
    " of colors specified." << endl; 
    throw PY8MEs_R3_P192_sm_qq_llqq_exception; 
  }
  user_colors = vector<int> ((2 * nexternal), 0); 
  for(unsigned int i = 0; i < (2 * nexternal); i++ )
  {
    user_colors[i] = colors_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the helicity configuration to use. Am empty vector means sum over all.
void PY8MEs_R3_P192_sm_qq_llqq::setHelicities(vector<int> helicities_picked) 
{
  if (helicities_picked.size() != nexternal)
  {
    if (helicities_picked.size() == 0)
    {
      user_helicities = vector<int> (); 
      return; 
    }
    cerr <<  "Error in function 'setHelicities' of class" << 
    " 'PY8MEs_R3_P192_sm_qq_llqq': Incorrect number" << 
    " of helicities specified." << endl; 
    throw PY8MEs_R3_P192_sm_qq_llqq_exception; 
  }
  user_helicities = vector<int> (nexternal, 0); 
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    user_helicities[i] = helicities_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the permutation to use (will apply to momenta, colors and helicities)
void PY8MEs_R3_P192_sm_qq_llqq::setPermutation(vector<int> perm_picked) 
{
  if (perm_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setPermutations' of class" << 
    " 'PY8MEs_R3_P192_sm_qq_llqq': Incorrect number" << 
    " of permutations specified." << endl; 
    throw PY8MEs_R3_P192_sm_qq_llqq_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    perm[i] = perm_picked[i]; 
  }
}

// Set the proc_ID to use
void PY8MEs_R3_P192_sm_qq_llqq::setProcID(int procID_picked) 
{
  proc_ID = procID_picked; 
}

//--------------------------------------------------------------------------
// Initialize process.

void PY8MEs_R3_P192_sm_qq_llqq::initProc() 
{

  // Initialize flags
  include_symmetry_factors = true; 
  include_helicity_averaging_factors = true; 
  include_color_averaging_factors = true; 

  // Initialize vectors.
  perm = vector<int> (nexternal, 0); 
  user_colors = vector<int> (2 * nexternal, 0); 
  user_helicities = vector<int> (nexternal, 0); 
  p = vector < double * > (); 
  for (unsigned int i = 0; i < nexternal; i++ )
  {
    p.push_back(new double[4]); 
  }
  initColorConfigs(); 
  // Synchronize local variables dependent on the model with the active model.
  mME = vector<double> (nexternal, 0.); 
  syncProcModelParams(); 
  jamp2 = vector < vec_double > (72); 
  jamp2[0] = vector<double> (2, 0.); 
  jamp2[1] = vector<double> (2, 0.); 
  jamp2[2] = vector<double> (2, 0.); 
  jamp2[3] = vector<double> (2, 0.); 
  jamp2[4] = vector<double> (2, 0.); 
  jamp2[5] = vector<double> (2, 0.); 
  jamp2[6] = vector<double> (2, 0.); 
  jamp2[7] = vector<double> (2, 0.); 
  jamp2[8] = vector<double> (2, 0.); 
  jamp2[9] = vector<double> (2, 0.); 
  jamp2[10] = vector<double> (2, 0.); 
  jamp2[11] = vector<double> (2, 0.); 
  jamp2[12] = vector<double> (2, 0.); 
  jamp2[13] = vector<double> (2, 0.); 
  jamp2[14] = vector<double> (2, 0.); 
  jamp2[15] = vector<double> (2, 0.); 
  jamp2[16] = vector<double> (2, 0.); 
  jamp2[17] = vector<double> (2, 0.); 
  jamp2[18] = vector<double> (2, 0.); 
  jamp2[19] = vector<double> (2, 0.); 
  jamp2[20] = vector<double> (2, 0.); 
  jamp2[21] = vector<double> (2, 0.); 
  jamp2[22] = vector<double> (2, 0.); 
  jamp2[23] = vector<double> (2, 0.); 
  jamp2[24] = vector<double> (2, 0.); 
  jamp2[25] = vector<double> (2, 0.); 
  jamp2[26] = vector<double> (2, 0.); 
  jamp2[27] = vector<double> (2, 0.); 
  jamp2[28] = vector<double> (2, 0.); 
  jamp2[29] = vector<double> (2, 0.); 
  jamp2[30] = vector<double> (2, 0.); 
  jamp2[31] = vector<double> (2, 0.); 
  jamp2[32] = vector<double> (2, 0.); 
  jamp2[33] = vector<double> (2, 0.); 
  jamp2[34] = vector<double> (2, 0.); 
  jamp2[35] = vector<double> (2, 0.); 
  jamp2[36] = vector<double> (2, 0.); 
  jamp2[37] = vector<double> (2, 0.); 
  jamp2[38] = vector<double> (2, 0.); 
  jamp2[39] = vector<double> (2, 0.); 
  jamp2[40] = vector<double> (2, 0.); 
  jamp2[41] = vector<double> (2, 0.); 
  jamp2[42] = vector<double> (2, 0.); 
  jamp2[43] = vector<double> (2, 0.); 
  jamp2[44] = vector<double> (2, 0.); 
  jamp2[45] = vector<double> (2, 0.); 
  jamp2[46] = vector<double> (2, 0.); 
  jamp2[47] = vector<double> (2, 0.); 
  jamp2[48] = vector<double> (2, 0.); 
  jamp2[49] = vector<double> (2, 0.); 
  jamp2[50] = vector<double> (2, 0.); 
  jamp2[51] = vector<double> (2, 0.); 
  jamp2[52] = vector<double> (2, 0.); 
  jamp2[53] = vector<double> (2, 0.); 
  jamp2[54] = vector<double> (2, 0.); 
  jamp2[55] = vector<double> (2, 0.); 
  jamp2[56] = vector<double> (2, 0.); 
  jamp2[57] = vector<double> (2, 0.); 
  jamp2[58] = vector<double> (2, 0.); 
  jamp2[59] = vector<double> (2, 0.); 
  jamp2[60] = vector<double> (2, 0.); 
  jamp2[61] = vector<double> (2, 0.); 
  jamp2[62] = vector<double> (2, 0.); 
  jamp2[63] = vector<double> (2, 0.); 
  jamp2[64] = vector<double> (2, 0.); 
  jamp2[65] = vector<double> (2, 0.); 
  jamp2[66] = vector<double> (2, 0.); 
  jamp2[67] = vector<double> (2, 0.); 
  jamp2[68] = vector<double> (2, 0.); 
  jamp2[69] = vector<double> (2, 0.); 
  jamp2[70] = vector<double> (2, 0.); 
  jamp2[71] = vector<double> (2, 0.); 
  all_results = vector < vec_vec_double > (72); 
  // The first entry is always the color or helicity avg/summed matrix element.
  all_results[0] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[1] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[2] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[3] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[4] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[5] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[6] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[7] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[8] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[9] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[10] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[11] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[12] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[13] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[14] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[15] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[16] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[17] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[18] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[19] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[20] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[21] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[22] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[23] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[24] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[25] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[26] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[27] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[28] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[29] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[30] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[31] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[32] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[33] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[34] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[35] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[36] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[37] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[38] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[39] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[40] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[41] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[42] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[43] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[44] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[45] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[46] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[47] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[48] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[49] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[50] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[51] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[52] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[53] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[54] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[55] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[56] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[57] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[58] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[59] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[60] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[61] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[62] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[63] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[64] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[65] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[66] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[67] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[68] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[69] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[70] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[71] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
}

// Synchronize local variables of the process that depend on the model
// parameters
void PY8MEs_R3_P192_sm_qq_llqq::syncProcModelParams() 
{

  // Instantiate the model class and set parameters that stay fixed during run
  mME[0] = pars->ZERO; 
  mME[1] = pars->ZERO; 
  mME[2] = pars->ZERO; 
  mME[3] = pars->ZERO; 
  mME[4] = pars->ZERO; 
  mME[5] = pars->ZERO; 
}

//--------------------------------------------------------------------------
// Setter allowing to force particular values for the external masses
void PY8MEs_R3_P192_sm_qq_llqq::setMasses(vec_double external_masses) 
{

  if (external_masses.size() != mME.size())
  {
    cerr <<  "Error in function 'setMasses' of class" << 
    " 'PY8MEs_R3_P192_sm_qq_llqq': Incorrect number of" << 
    " masses specified." << endl; 
    throw PY8MEs_R3_P192_sm_qq_llqq_exception; 
  }
  for (unsigned int j = 0; j < mME.size(); j++ )
  {
    mME[j] = external_masses[perm[j]]; 
  }
}

//--------------------------------------------------------------------------
// Getter accessing external masses with the correct ordering
vector<double> PY8MEs_R3_P192_sm_qq_llqq::getMasses() 
{

  vec_double external_masses; 
  vector<int> invertedPerm; 
  invertedPerm = invert_mapping(perm); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    external_masses.push_back(mME[invertedPerm[i]]); 
  }
  return external_masses; 

}


// Set all values of the external masses to float(-mode) where mode can be
// 0 : Mass taken from the model
// 1 : Mass taken from p_i^2 if not massless to begin with
// 2 : Mass always taken from p_i^2.
void PY8MEs_R3_P192_sm_qq_llqq::setExternalMassesMode(int mode) 
{
  if (mode != 0 && mode != 1 && mode != 2)
  {
    cerr <<  "Error in function 'setExternalMassesMode' of class" << 
    " 'PY8MEs_R3_P192_sm_qq_llqq': Incorrect mode selected :" << mode << 
    ". It must be either 0, 1 or 2" << endl; 
    throw PY8MEs_R3_P192_sm_qq_llqq_exception; 
  }
  if (mode == 0)
  {
    syncProcModelParams(); 
  }
  else if (mode == 1)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      if (mME[j] != pars->ZERO)
      {
        mME[j] = -1.0; 
      }
    }
  }
  else if (mode == 2)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      mME[j] = -1.0; 
    }
  }
}

//--------------------------------------------------------------------------
// Evaluate the squared matrix element.

double PY8MEs_R3_P192_sm_qq_llqq::sigmaKin() 
{
  // Set the parameters which change event by event
  pars->setDependentParameters(); 
  pars->setDependentCouplings(); 
  // Reset color flows
  for(int i = 0; i < 2; i++ )
    jamp2[0][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[1][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[2][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[3][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[4][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[5][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[6][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[7][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[8][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[9][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[10][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[11][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[12][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[13][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[14][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[15][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[16][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[17][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[18][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[19][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[20][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[21][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[22][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[23][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[24][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[25][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[26][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[27][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[28][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[29][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[30][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[31][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[32][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[33][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[34][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[35][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[36][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[37][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[38][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[39][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[40][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[41][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[42][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[43][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[44][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[45][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[46][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[47][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[48][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[49][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[50][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[51][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[52][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[53][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[54][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[55][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[56][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[57][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[58][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[59][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[60][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[61][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[62][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[63][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[64][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[65][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[66][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[67][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[68][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[69][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[70][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[71][i] = 0.; 

  // Save previous values of mME
  vector<double> saved_mME(mME.size(), 0.0); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (mME[i] < 0.0)
    {
      saved_mME[i] = mME[i]; 
      mME[i] = sqrt(abs(pow(p[perm[i]][0], 2) - 
      (pow(p[perm[i]][1], 2) + pow(p[perm[i]][2], 2) + pow(p[perm[i]][3],
          2))));
    }
  }

  // Local variables and constants
  const int max_tries = 10; 
  vector < vec_bool > goodhel(nprocesses, vec_bool(ncomb, false)); 
  vec_int ntry(nprocesses, 0); 
  double t = 0.; 
  double result = 0.; 

  if (ntry[proc_ID] <= max_tries)
    ntry[proc_ID] = ntry[proc_ID] + 1; 

  // Find which helicity configuration is asked for
  // -1 indicates one wants to sum over helicities
  int user_ihel = getHelicityIDForConfig(user_helicities); 

  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = getColorIDForConfig(user_colors); 

  // Reset the list of results that will be recomputed here
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      all_results[proc_ID][ihel + 1][icolor + 1] = 0.; 
    }
  }

  // Calculate the matrix element for all helicities
  // unless already detected as vanishing
  for(int ihel = 0; ihel < ncomb; ihel++ )
  {
    // Skip helicity if already detected as vanishing
    if ((ntry[proc_ID] >= max_tries) && !goodhel[proc_ID][ihel])
      continue; 

    // Also skip helicity if user asks for a specific one
    if ((ntry[proc_ID] >= max_tries) && user_ihel != -1 && user_ihel != ihel)
      continue; 

    calculate_wavefunctions(helicities[ihel]); 

    // Reset locally computed color flows
    for(int i = 0; i < 2; i++ )
      jamp2[0][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[1][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[2][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[3][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[4][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[5][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[6][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[7][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[8][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[9][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[10][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[11][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[12][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[13][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[14][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[15][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[16][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[17][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[18][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[19][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[20][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[21][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[22][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[23][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[24][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[25][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[26][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[27][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[28][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[29][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[30][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[31][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[32][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[33][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[34][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[35][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[36][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[37][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[38][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[39][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[40][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[41][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[42][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[43][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[44][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[45][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[46][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[47][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[48][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[49][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[50][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[51][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[52][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[53][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[54][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[55][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[56][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[57][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[58][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[59][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[60][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[61][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[62][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[63][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[64][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[65][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[66][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[67][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[68][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[69][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[70][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[71][i] = 0.; 

    if (proc_ID == 0)
      t = matrix_3_uu_epemuu(); 
    if (proc_ID == 1)
      t = matrix_3_uux_epemuux(); 
    if (proc_ID == 2)
      t = matrix_3_dd_epemdd(); 
    if (proc_ID == 3)
      t = matrix_3_ddx_epemddx(); 
    if (proc_ID == 4)
      t = matrix_3_uxux_epemuxux(); 
    if (proc_ID == 5)
      t = matrix_3_dxdx_epemdxdx(); 
    if (proc_ID == 6)
      t = matrix_3_uu_vexveuu(); 
    if (proc_ID == 7)
      t = matrix_3_uc_epemuc(); 
    if (proc_ID == 8)
      t = matrix_3_ud_epemud(); 
    if (proc_ID == 9)
      t = matrix_3_uux_epemccx(); 
    if (proc_ID == 10)
      t = matrix_3_uux_epemddx(); 
    if (proc_ID == 11)
      t = matrix_3_uux_vexveuux(); 
    if (proc_ID == 12)
      t = matrix_3_ucx_epemucx(); 
    if (proc_ID == 13)
      t = matrix_3_udx_epemudx(); 
    if (proc_ID == 14)
      t = matrix_3_dd_vexvedd(); 
    if (proc_ID == 15)
      t = matrix_3_ds_epemds(); 
    if (proc_ID == 16)
      t = matrix_3_dux_epemdux(); 
    if (proc_ID == 17)
      t = matrix_3_ddx_epemuux(); 
    if (proc_ID == 18)
      t = matrix_3_ddx_epemssx(); 
    if (proc_ID == 19)
      t = matrix_3_ddx_vexveddx(); 
    if (proc_ID == 20)
      t = matrix_3_dsx_epemdsx(); 
    if (proc_ID == 21)
      t = matrix_3_uxux_vexveuxux(); 
    if (proc_ID == 22)
      t = matrix_3_uxcx_epemuxcx(); 
    if (proc_ID == 23)
      t = matrix_3_uxdx_epemuxdx(); 
    if (proc_ID == 24)
      t = matrix_3_dxdx_vexvedxdx(); 
    if (proc_ID == 25)
      t = matrix_3_dxsx_epemdxsx(); 
    if (proc_ID == 26)
      t = matrix_3_uu_epveud(); 
    if (proc_ID == 27)
      t = matrix_3_uc_vexveuc(); 
    if (proc_ID == 28)
      t = matrix_3_ud_epvedd(); 
    if (proc_ID == 29)
      t = matrix_3_ud_vexemuu(); 
    if (proc_ID == 30)
      t = matrix_3_ud_vexveud(); 
    if (proc_ID == 31)
      t = matrix_3_uux_epvedux(); 
    if (proc_ID == 32)
      t = matrix_3_uux_vexemudx(); 
    if (proc_ID == 33)
      t = matrix_3_uux_vexveccx(); 
    if (proc_ID == 34)
      t = matrix_3_uux_vexveddx(); 
    if (proc_ID == 35)
      t = matrix_3_ucx_vexveucx(); 
    if (proc_ID == 36)
      t = matrix_3_udx_epveuux(); 
    if (proc_ID == 37)
      t = matrix_3_udx_epveddx(); 
    if (proc_ID == 38)
      t = matrix_3_udx_vexveudx(); 
    if (proc_ID == 39)
      t = matrix_3_dd_vexemud(); 
    if (proc_ID == 40)
      t = matrix_3_ds_vexveds(); 
    if (proc_ID == 41)
      t = matrix_3_dux_vexemuux(); 
    if (proc_ID == 42)
      t = matrix_3_dux_vexemddx(); 
    if (proc_ID == 43)
      t = matrix_3_dux_vexvedux(); 
    if (proc_ID == 44)
      t = matrix_3_ddx_epvedux(); 
    if (proc_ID == 45)
      t = matrix_3_ddx_vexemudx(); 
    if (proc_ID == 46)
      t = matrix_3_ddx_vexveuux(); 
    if (proc_ID == 47)
      t = matrix_3_ddx_vexvessx(); 
    if (proc_ID == 48)
      t = matrix_3_dsx_vexvedsx(); 
    if (proc_ID == 49)
      t = matrix_3_uxux_vexemuxdx(); 
    if (proc_ID == 50)
      t = matrix_3_uxcx_vexveuxcx(); 
    if (proc_ID == 51)
      t = matrix_3_uxdx_epveuxux(); 
    if (proc_ID == 52)
      t = matrix_3_uxdx_vexemdxdx(); 
    if (proc_ID == 53)
      t = matrix_3_uxdx_vexveuxdx(); 
    if (proc_ID == 54)
      t = matrix_3_dxdx_epveuxdx(); 
    if (proc_ID == 55)
      t = matrix_3_dxsx_vexvedxsx(); 
    if (proc_ID == 56)
      t = matrix_3_uc_epveus(); 
    if (proc_ID == 57)
      t = matrix_3_uc_epvecd(); 
    if (proc_ID == 58)
      t = matrix_3_us_vexemuc(); 
    if (proc_ID == 59)
      t = matrix_3_uux_epvescx(); 
    if (proc_ID == 60)
      t = matrix_3_uux_vexemcsx(); 
    if (proc_ID == 61)
      t = matrix_3_ucx_epvedcx(); 
    if (proc_ID == 62)
      t = matrix_3_ucx_vexemusx(); 
    if (proc_ID == 63)
      t = matrix_3_udx_epveccx(); 
    if (proc_ID == 64)
      t = matrix_3_usx_epveucx(); 
    if (proc_ID == 65)
      t = matrix_3_ds_vexemus(); 
    if (proc_ID == 66)
      t = matrix_3_dux_vexemccx(); 
    if (proc_ID == 67)
      t = matrix_3_dcx_vexemucx(); 
    if (proc_ID == 68)
      t = matrix_3_uxcx_vexemuxsx(); 
    if (proc_ID == 69)
      t = matrix_3_uxcx_vexemcxdx(); 
    if (proc_ID == 70)
      t = matrix_3_uxsx_epveuxcx(); 
    if (proc_ID == 71)
      t = matrix_3_dxsx_epveuxsx(); 

    // Store which helicities give non-zero result
    if ((ntry[proc_ID] < max_tries) && t != 0. && !goodhel[proc_ID][ihel])
      goodhel[proc_ID][ihel] = true; 

    // Aggregate results
    if (user_ihel == -1 || user_ihel == ihel)
    {
      if (user_icol == -1)
      {
        result = result + t; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][0] += t; 
          for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
          {
            all_results[proc_ID][0][i + 1] += jamp2[proc_ID][i]; 
          }
        }
        all_results[proc_ID][ihel + 1][0] += t; 
        for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
        {
          all_results[proc_ID][ihel + 1][i + 1] += jamp2[proc_ID][i]; 
        }
      }
      else
      {
        result = result + jamp2[proc_ID][user_icol]; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][user_icol + 1] += jamp2[proc_ID][user_icol]; 
        }
        all_results[proc_ID][ihel + 1][user_icol + 1] +=
            jamp2[proc_ID][user_icol];
      }
    }

  }

  // Normalize results with the identical particle factor
  if (include_symmetry_factors)
  {
    result = result/denom_iden[proc_ID]; 
  }
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      if (include_symmetry_factors)
      {
        all_results[proc_ID][ihel + 1][icolor + 1] /= denom_iden[proc_ID]; 
      }
    }
  }


  // Normalize when when summing+averaging over helicity configurations
  if (user_ihel == -1 && include_helicity_averaging_factors)
  {
    result /= denom_hels[proc_ID]; 
    if (user_icol == -1)
    {
      all_results[proc_ID][0][0] /= denom_hels[proc_ID]; 
      for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
      {
        all_results[proc_ID][0][i + 1] /= denom_hels[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][0][user_icol + 1] /= denom_hels[proc_ID]; 
    }
  }

  // Normalize when summing+averaging over color configurations
  if (user_icol == -1 && include_color_averaging_factors)
  {
    result /= denom_colors[proc_ID]; 
    if (user_ihel == -1)
    {
      all_results[proc_ID][0][0] /= denom_colors[proc_ID]; 
      for (unsigned int i = 0; i < ncomb; i++ )
      {
        all_results[proc_ID][i + 1][0] /= denom_colors[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][user_ihel + 1][0] /= denom_colors[proc_ID]; 
    }
  }

  // Reinstate previous values of mME
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (saved_mME[i] < 0.0)
    {
      mME[i] = saved_mME[i]; 
    }
  }

  // Finally return it
  return result; 
}

//==========================================================================
// Private class member functions

//--------------------------------------------------------------------------
// Evaluate |M|^2 for each subprocess

void PY8MEs_R3_P192_sm_qq_llqq::calculate_wavefunctions(const int hel[])
{
  // Calculate wavefunctions for all processes
  // Calculate all wavefunctions
  ixxxxx(p[perm[0]], mME[0], hel[0], +1, w[0]); 
  ixxxxx(p[perm[1]], mME[1], hel[1], +1, w[1]); 
  ixxxxx(p[perm[2]], mME[2], hel[2], -1, w[2]); 
  oxxxxx(p[perm[3]], mME[3], hel[3], +1, w[3]); 
  oxxxxx(p[perm[4]], mME[4], hel[4], +1, w[4]); 
  oxxxxx(p[perm[5]], mME[5], hel[5], +1, w[5]); 
  FFV1P0_3(w[0], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[6]); 
  FFV1P0_3(w[2], w[3], pars->GC_3, pars->ZERO, pars->ZERO, w[7]); 
  FFV1_2(w[1], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[8]); 
  FFV1_1(w[5], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[9]); 
  FFV2_4_3(w[2], w[3], pars->GC_50, pars->GC_59, pars->mdl_MZ, pars->mdl_WZ,
      w[10]);
  FFV1P0_3(w[0], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[11]); 
  FFV1_2(w[1], w[11], pars->GC_11, pars->ZERO, pars->ZERO, w[12]); 
  FFV1_1(w[4], w[11], pars->GC_11, pars->ZERO, pars->ZERO, w[13]); 
  FFV1P0_3(w[1], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[14]); 
  FFV1_2(w[0], w[14], pars->GC_11, pars->ZERO, pars->ZERO, w[15]); 
  FFV1_2(w[0], w[7], pars->GC_2, pars->ZERO, pars->ZERO, w[16]); 
  FFV2_5_2(w[0], w[10], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[17]);
  FFV1P0_3(w[1], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[18]); 
  FFV1_2(w[0], w[18], pars->GC_11, pars->ZERO, pars->ZERO, w[19]); 
  oxxxxx(p[perm[1]], mME[1], hel[1], -1, w[20]); 
  ixxxxx(p[perm[5]], mME[5], hel[5], -1, w[21]); 
  FFV1P0_3(w[0], w[20], pars->GC_11, pars->ZERO, pars->ZERO, w[22]); 
  FFV1_2(w[21], w[22], pars->GC_11, pars->ZERO, pars->ZERO, w[23]); 
  FFV1_1(w[4], w[22], pars->GC_11, pars->ZERO, pars->ZERO, w[24]); 
  FFV1_2(w[21], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[25]); 
  FFV1_1(w[20], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[26]); 
  FFV1P0_3(w[21], w[20], pars->GC_11, pars->ZERO, pars->ZERO, w[27]); 
  FFV1_2(w[0], w[27], pars->GC_11, pars->ZERO, pars->ZERO, w[28]); 
  FFV1P0_3(w[21], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[29]); 
  FFV1_2(w[0], w[29], pars->GC_11, pars->ZERO, pars->ZERO, w[30]); 
  FFV1_2(w[0], w[7], pars->GC_1, pars->ZERO, pars->ZERO, w[31]); 
  FFV2_3_2(w[0], w[10], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[32]);
  oxxxxx(p[perm[0]], mME[0], hel[0], -1, w[33]); 
  ixxxxx(p[perm[4]], mME[4], hel[4], -1, w[34]); 
  FFV1P0_3(w[34], w[33], pars->GC_11, pars->ZERO, pars->ZERO, w[35]); 
  FFV1_2(w[21], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[36]); 
  FFV1_1(w[20], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[37]); 
  FFV1P0_3(w[34], w[20], pars->GC_11, pars->ZERO, pars->ZERO, w[38]); 
  FFV1_2(w[21], w[38], pars->GC_11, pars->ZERO, pars->ZERO, w[39]); 
  FFV1_1(w[33], w[38], pars->GC_11, pars->ZERO, pars->ZERO, w[40]); 
  FFV1P0_3(w[21], w[33], pars->GC_11, pars->ZERO, pars->ZERO, w[41]); 
  FFV1_2(w[34], w[41], pars->GC_11, pars->ZERO, pars->ZERO, w[42]); 
  FFV1_2(w[34], w[7], pars->GC_2, pars->ZERO, pars->ZERO, w[43]); 
  FFV2_5_2(w[34], w[10], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[44]);
  FFV1_2(w[34], w[27], pars->GC_11, pars->ZERO, pars->ZERO, w[45]); 
  FFV1_2(w[34], w[7], pars->GC_1, pars->ZERO, pars->ZERO, w[46]); 
  FFV2_3_2(w[34], w[10], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[47]);
  FFV2_3(w[2], w[3], pars->GC_62, pars->mdl_MZ, pars->mdl_WZ, w[48]); 
  FFV2_5_2(w[0], w[48], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[49]);
  FFV2_3_2(w[0], w[48], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[50]);
  FFV1_1(w[4], w[27], pars->GC_11, pars->ZERO, pars->ZERO, w[51]); 
  FFV1_2(w[21], w[7], pars->GC_2, pars->ZERO, pars->ZERO, w[52]); 
  FFV2_5_2(w[21], w[10], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[53]);
  FFV1_1(w[20], w[29], pars->GC_11, pars->ZERO, pars->ZERO, w[54]); 
  FFV2_5_2(w[34], w[48], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[55]);
  FFV2_3_2(w[34], w[48], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[56]);
  FFV2_3(w[2], w[3], pars->GC_100, pars->mdl_MW, pars->mdl_WW, w[57]); 
  FFV2_2(w[0], w[57], pars->GC_100, pars->ZERO, pars->ZERO, w[58]); 
  FFV2_5_2(w[21], w[48], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[59]);
  FFV2_2(w[21], w[57], pars->GC_100, pars->ZERO, pars->ZERO, w[60]); 
  FFV2_2(w[34], w[57], pars->GC_100, pars->ZERO, pars->ZERO, w[61]); 

  // Calculate all amplitudes
  // Amplitude(s) for diagram number 0
  FFV1_0(w[8], w[5], w[7], pars->GC_2, amp[0]); 
  FFV1_0(w[1], w[9], w[7], pars->GC_2, amp[1]); 
  FFV2_5_0(w[8], w[5], w[10], pars->GC_51, pars->GC_58, amp[2]); 
  FFV2_5_0(w[1], w[9], w[10], pars->GC_51, pars->GC_58, amp[3]); 
  FFV1_0(w[12], w[4], w[7], pars->GC_2, amp[4]); 
  FFV1_0(w[1], w[13], w[7], pars->GC_2, amp[5]); 
  FFV2_5_0(w[12], w[4], w[10], pars->GC_51, pars->GC_58, amp[6]); 
  FFV2_5_0(w[1], w[13], w[10], pars->GC_51, pars->GC_58, amp[7]); 
  FFV1_0(w[15], w[5], w[7], pars->GC_2, amp[8]); 
  FFV1_0(w[16], w[5], w[14], pars->GC_11, amp[9]); 
  FFV2_5_0(w[15], w[5], w[10], pars->GC_51, pars->GC_58, amp[10]); 
  FFV1_0(w[17], w[5], w[14], pars->GC_11, amp[11]); 
  FFV1_0(w[19], w[4], w[7], pars->GC_2, amp[12]); 
  FFV1_0(w[16], w[4], w[18], pars->GC_11, amp[13]); 
  FFV2_5_0(w[19], w[4], w[10], pars->GC_51, pars->GC_58, amp[14]); 
  FFV1_0(w[17], w[4], w[18], pars->GC_11, amp[15]); 
  FFV1_0(w[23], w[4], w[7], pars->GC_2, amp[16]); 
  FFV1_0(w[21], w[24], w[7], pars->GC_2, amp[17]); 
  FFV2_5_0(w[23], w[4], w[10], pars->GC_51, pars->GC_58, amp[18]); 
  FFV2_5_0(w[21], w[24], w[10], pars->GC_51, pars->GC_58, amp[19]); 
  FFV1_0(w[25], w[20], w[7], pars->GC_2, amp[20]); 
  FFV1_0(w[21], w[26], w[7], pars->GC_2, amp[21]); 
  FFV2_5_0(w[25], w[20], w[10], pars->GC_51, pars->GC_58, amp[22]); 
  FFV2_5_0(w[21], w[26], w[10], pars->GC_51, pars->GC_58, amp[23]); 
  FFV1_0(w[28], w[4], w[7], pars->GC_2, amp[24]); 
  FFV1_0(w[16], w[4], w[27], pars->GC_11, amp[25]); 
  FFV2_5_0(w[28], w[4], w[10], pars->GC_51, pars->GC_58, amp[26]); 
  FFV1_0(w[17], w[4], w[27], pars->GC_11, amp[27]); 
  FFV1_0(w[30], w[20], w[7], pars->GC_2, amp[28]); 
  FFV1_0(w[16], w[20], w[29], pars->GC_11, amp[29]); 
  FFV2_5_0(w[30], w[20], w[10], pars->GC_51, pars->GC_58, amp[30]); 
  FFV1_0(w[17], w[20], w[29], pars->GC_11, amp[31]); 
  FFV1_0(w[8], w[5], w[7], pars->GC_1, amp[32]); 
  FFV1_0(w[1], w[9], w[7], pars->GC_1, amp[33]); 
  FFV2_3_0(w[8], w[5], w[10], pars->GC_50, pars->GC_58, amp[34]); 
  FFV2_3_0(w[1], w[9], w[10], pars->GC_50, pars->GC_58, amp[35]); 
  FFV1_0(w[12], w[4], w[7], pars->GC_1, amp[36]); 
  FFV1_0(w[1], w[13], w[7], pars->GC_1, amp[37]); 
  FFV2_3_0(w[12], w[4], w[10], pars->GC_50, pars->GC_58, amp[38]); 
  FFV2_3_0(w[1], w[13], w[10], pars->GC_50, pars->GC_58, amp[39]); 
  FFV1_0(w[15], w[5], w[7], pars->GC_1, amp[40]); 
  FFV1_0(w[31], w[5], w[14], pars->GC_11, amp[41]); 
  FFV2_3_0(w[15], w[5], w[10], pars->GC_50, pars->GC_58, amp[42]); 
  FFV1_0(w[32], w[5], w[14], pars->GC_11, amp[43]); 
  FFV1_0(w[19], w[4], w[7], pars->GC_1, amp[44]); 
  FFV1_0(w[31], w[4], w[18], pars->GC_11, amp[45]); 
  FFV2_3_0(w[19], w[4], w[10], pars->GC_50, pars->GC_58, amp[46]); 
  FFV1_0(w[32], w[4], w[18], pars->GC_11, amp[47]); 
  FFV1_0(w[23], w[4], w[7], pars->GC_1, amp[48]); 
  FFV1_0(w[21], w[24], w[7], pars->GC_1, amp[49]); 
  FFV2_3_0(w[23], w[4], w[10], pars->GC_50, pars->GC_58, amp[50]); 
  FFV2_3_0(w[21], w[24], w[10], pars->GC_50, pars->GC_58, amp[51]); 
  FFV1_0(w[25], w[20], w[7], pars->GC_1, amp[52]); 
  FFV1_0(w[21], w[26], w[7], pars->GC_1, amp[53]); 
  FFV2_3_0(w[25], w[20], w[10], pars->GC_50, pars->GC_58, amp[54]); 
  FFV2_3_0(w[21], w[26], w[10], pars->GC_50, pars->GC_58, amp[55]); 
  FFV1_0(w[28], w[4], w[7], pars->GC_1, amp[56]); 
  FFV1_0(w[31], w[4], w[27], pars->GC_11, amp[57]); 
  FFV2_3_0(w[28], w[4], w[10], pars->GC_50, pars->GC_58, amp[58]); 
  FFV1_0(w[32], w[4], w[27], pars->GC_11, amp[59]); 
  FFV1_0(w[30], w[20], w[7], pars->GC_1, amp[60]); 
  FFV1_0(w[31], w[20], w[29], pars->GC_11, amp[61]); 
  FFV2_3_0(w[30], w[20], w[10], pars->GC_50, pars->GC_58, amp[62]); 
  FFV1_0(w[32], w[20], w[29], pars->GC_11, amp[63]); 
  FFV1_0(w[36], w[20], w[7], pars->GC_2, amp[64]); 
  FFV1_0(w[21], w[37], w[7], pars->GC_2, amp[65]); 
  FFV2_5_0(w[36], w[20], w[10], pars->GC_51, pars->GC_58, amp[66]); 
  FFV2_5_0(w[21], w[37], w[10], pars->GC_51, pars->GC_58, amp[67]); 
  FFV1_0(w[39], w[33], w[7], pars->GC_2, amp[68]); 
  FFV1_0(w[21], w[40], w[7], pars->GC_2, amp[69]); 
  FFV2_5_0(w[39], w[33], w[10], pars->GC_51, pars->GC_58, amp[70]); 
  FFV2_5_0(w[21], w[40], w[10], pars->GC_51, pars->GC_58, amp[71]); 
  FFV1_0(w[42], w[20], w[7], pars->GC_2, amp[72]); 
  FFV1_0(w[43], w[20], w[41], pars->GC_11, amp[73]); 
  FFV2_5_0(w[42], w[20], w[10], pars->GC_51, pars->GC_58, amp[74]); 
  FFV1_0(w[44], w[20], w[41], pars->GC_11, amp[75]); 
  FFV1_0(w[45], w[33], w[7], pars->GC_2, amp[76]); 
  FFV1_0(w[43], w[33], w[27], pars->GC_11, amp[77]); 
  FFV2_5_0(w[45], w[33], w[10], pars->GC_51, pars->GC_58, amp[78]); 
  FFV1_0(w[44], w[33], w[27], pars->GC_11, amp[79]); 
  FFV1_0(w[36], w[20], w[7], pars->GC_1, amp[80]); 
  FFV1_0(w[21], w[37], w[7], pars->GC_1, amp[81]); 
  FFV2_3_0(w[36], w[20], w[10], pars->GC_50, pars->GC_58, amp[82]); 
  FFV2_3_0(w[21], w[37], w[10], pars->GC_50, pars->GC_58, amp[83]); 
  FFV1_0(w[39], w[33], w[7], pars->GC_1, amp[84]); 
  FFV1_0(w[21], w[40], w[7], pars->GC_1, amp[85]); 
  FFV2_3_0(w[39], w[33], w[10], pars->GC_50, pars->GC_58, amp[86]); 
  FFV2_3_0(w[21], w[40], w[10], pars->GC_50, pars->GC_58, amp[87]); 
  FFV1_0(w[42], w[20], w[7], pars->GC_1, amp[88]); 
  FFV1_0(w[46], w[20], w[41], pars->GC_11, amp[89]); 
  FFV2_3_0(w[42], w[20], w[10], pars->GC_50, pars->GC_58, amp[90]); 
  FFV1_0(w[47], w[20], w[41], pars->GC_11, amp[91]); 
  FFV1_0(w[45], w[33], w[7], pars->GC_1, amp[92]); 
  FFV1_0(w[46], w[33], w[27], pars->GC_11, amp[93]); 
  FFV2_3_0(w[45], w[33], w[10], pars->GC_50, pars->GC_58, amp[94]); 
  FFV1_0(w[47], w[33], w[27], pars->GC_11, amp[95]); 
  FFV2_5_0(w[8], w[5], w[48], pars->GC_51, pars->GC_58, amp[96]); 
  FFV2_5_0(w[1], w[9], w[48], pars->GC_51, pars->GC_58, amp[97]); 
  FFV2_5_0(w[12], w[4], w[48], pars->GC_51, pars->GC_58, amp[98]); 
  FFV2_5_0(w[1], w[13], w[48], pars->GC_51, pars->GC_58, amp[99]); 
  FFV2_5_0(w[15], w[5], w[48], pars->GC_51, pars->GC_58, amp[100]); 
  FFV1_0(w[49], w[5], w[14], pars->GC_11, amp[101]); 
  FFV2_5_0(w[19], w[4], w[48], pars->GC_51, pars->GC_58, amp[102]); 
  FFV1_0(w[49], w[4], w[18], pars->GC_11, amp[103]); 
  FFV1_0(w[19], w[4], w[7], pars->GC_2, amp[104]); 
  FFV1_0(w[16], w[4], w[18], pars->GC_11, amp[105]); 
  FFV2_5_0(w[19], w[4], w[10], pars->GC_51, pars->GC_58, amp[106]); 
  FFV1_0(w[17], w[4], w[18], pars->GC_11, amp[107]); 
  FFV1_0(w[8], w[5], w[7], pars->GC_1, amp[108]); 
  FFV1_0(w[1], w[9], w[7], pars->GC_1, amp[109]); 
  FFV2_3_0(w[8], w[5], w[10], pars->GC_50, pars->GC_58, amp[110]); 
  FFV2_3_0(w[1], w[9], w[10], pars->GC_50, pars->GC_58, amp[111]); 
  FFV1_0(w[19], w[4], w[7], pars->GC_2, amp[112]); 
  FFV1_0(w[16], w[4], w[18], pars->GC_11, amp[113]); 
  FFV2_5_0(w[19], w[4], w[10], pars->GC_51, pars->GC_58, amp[114]); 
  FFV1_0(w[17], w[4], w[18], pars->GC_11, amp[115]); 
  FFV1_0(w[23], w[4], w[7], pars->GC_2, amp[116]); 
  FFV1_0(w[21], w[24], w[7], pars->GC_2, amp[117]); 
  FFV2_5_0(w[23], w[4], w[10], pars->GC_51, pars->GC_58, amp[118]); 
  FFV2_5_0(w[21], w[24], w[10], pars->GC_51, pars->GC_58, amp[119]); 
  FFV1_0(w[30], w[20], w[7], pars->GC_2, amp[120]); 
  FFV1_0(w[16], w[20], w[29], pars->GC_11, amp[121]); 
  FFV2_5_0(w[30], w[20], w[10], pars->GC_51, pars->GC_58, amp[122]); 
  FFV1_0(w[17], w[20], w[29], pars->GC_11, amp[123]); 
  FFV1_0(w[23], w[4], w[7], pars->GC_1, amp[124]); 
  FFV1_0(w[21], w[24], w[7], pars->GC_1, amp[125]); 
  FFV2_3_0(w[23], w[4], w[10], pars->GC_50, pars->GC_58, amp[126]); 
  FFV2_3_0(w[21], w[24], w[10], pars->GC_50, pars->GC_58, amp[127]); 
  FFV1_0(w[30], w[20], w[7], pars->GC_2, amp[128]); 
  FFV1_0(w[16], w[20], w[29], pars->GC_11, amp[129]); 
  FFV2_5_0(w[30], w[20], w[10], pars->GC_51, pars->GC_58, amp[130]); 
  FFV1_0(w[17], w[20], w[29], pars->GC_11, amp[131]); 
  FFV2_5_0(w[23], w[4], w[48], pars->GC_51, pars->GC_58, amp[132]); 
  FFV2_5_0(w[21], w[24], w[48], pars->GC_51, pars->GC_58, amp[133]); 
  FFV2_5_0(w[25], w[20], w[48], pars->GC_51, pars->GC_58, amp[134]); 
  FFV2_5_0(w[21], w[26], w[48], pars->GC_51, pars->GC_58, amp[135]); 
  FFV2_5_0(w[28], w[4], w[48], pars->GC_51, pars->GC_58, amp[136]); 
  FFV1_0(w[49], w[4], w[27], pars->GC_11, amp[137]); 
  FFV2_5_0(w[30], w[20], w[48], pars->GC_51, pars->GC_58, amp[138]); 
  FFV1_0(w[49], w[20], w[29], pars->GC_11, amp[139]); 
  FFV1_0(w[25], w[20], w[7], pars->GC_2, amp[140]); 
  FFV1_0(w[21], w[26], w[7], pars->GC_2, amp[141]); 
  FFV2_5_0(w[25], w[20], w[10], pars->GC_51, pars->GC_58, amp[142]); 
  FFV2_5_0(w[21], w[26], w[10], pars->GC_51, pars->GC_58, amp[143]); 
  FFV1_0(w[28], w[4], w[7], pars->GC_2, amp[144]); 
  FFV1_0(w[16], w[4], w[27], pars->GC_11, amp[145]); 
  FFV2_5_0(w[28], w[4], w[10], pars->GC_51, pars->GC_58, amp[146]); 
  FFV1_0(w[17], w[4], w[27], pars->GC_11, amp[147]); 
  FFV1_0(w[25], w[20], w[7], pars->GC_1, amp[148]); 
  FFV1_0(w[21], w[26], w[7], pars->GC_1, amp[149]); 
  FFV2_3_0(w[25], w[20], w[10], pars->GC_50, pars->GC_58, amp[150]); 
  FFV2_3_0(w[21], w[26], w[10], pars->GC_50, pars->GC_58, amp[151]); 
  FFV1_0(w[28], w[4], w[7], pars->GC_2, amp[152]); 
  FFV1_0(w[16], w[4], w[27], pars->GC_11, amp[153]); 
  FFV2_5_0(w[28], w[4], w[10], pars->GC_51, pars->GC_58, amp[154]); 
  FFV1_0(w[17], w[4], w[27], pars->GC_11, amp[155]); 
  FFV2_3_0(w[8], w[5], w[48], pars->GC_50, pars->GC_58, amp[156]); 
  FFV2_3_0(w[1], w[9], w[48], pars->GC_50, pars->GC_58, amp[157]); 
  FFV2_3_0(w[12], w[4], w[48], pars->GC_50, pars->GC_58, amp[158]); 
  FFV2_3_0(w[1], w[13], w[48], pars->GC_50, pars->GC_58, amp[159]); 
  FFV2_3_0(w[15], w[5], w[48], pars->GC_50, pars->GC_58, amp[160]); 
  FFV1_0(w[50], w[5], w[14], pars->GC_11, amp[161]); 
  FFV2_3_0(w[19], w[4], w[48], pars->GC_50, pars->GC_58, amp[162]); 
  FFV1_0(w[50], w[4], w[18], pars->GC_11, amp[163]); 
  FFV1_0(w[8], w[5], w[7], pars->GC_1, amp[164]); 
  FFV1_0(w[1], w[9], w[7], pars->GC_1, amp[165]); 
  FFV2_3_0(w[8], w[5], w[10], pars->GC_50, pars->GC_58, amp[166]); 
  FFV2_3_0(w[1], w[9], w[10], pars->GC_50, pars->GC_58, amp[167]); 
  FFV1_0(w[19], w[4], w[7], pars->GC_1, amp[168]); 
  FFV1_0(w[31], w[4], w[18], pars->GC_11, amp[169]); 
  FFV2_3_0(w[19], w[4], w[10], pars->GC_50, pars->GC_58, amp[170]); 
  FFV1_0(w[32], w[4], w[18], pars->GC_11, amp[171]); 
  FFV1_0(w[28], w[4], w[7], pars->GC_1, amp[172]); 
  FFV1_0(w[0], w[51], w[7], pars->GC_1, amp[173]); 
  FFV2_3_0(w[28], w[4], w[10], pars->GC_50, pars->GC_58, amp[174]); 
  FFV2_3_0(w[0], w[51], w[10], pars->GC_50, pars->GC_58, amp[175]); 
  FFV1_0(w[25], w[20], w[7], pars->GC_2, amp[176]); 
  FFV1_0(w[52], w[20], w[6], pars->GC_11, amp[177]); 
  FFV2_5_0(w[25], w[20], w[10], pars->GC_51, pars->GC_58, amp[178]); 
  FFV1_0(w[53], w[20], w[6], pars->GC_11, amp[179]); 
  FFV1_0(w[30], w[20], w[7], pars->GC_1, amp[180]); 
  FFV1_0(w[0], w[54], w[7], pars->GC_1, amp[181]); 
  FFV2_3_0(w[30], w[20], w[10], pars->GC_50, pars->GC_58, amp[182]); 
  FFV2_3_0(w[0], w[54], w[10], pars->GC_50, pars->GC_58, amp[183]); 
  FFV1_0(w[23], w[4], w[7], pars->GC_2, amp[184]); 
  FFV1_0(w[52], w[4], w[22], pars->GC_11, amp[185]); 
  FFV2_5_0(w[23], w[4], w[10], pars->GC_51, pars->GC_58, amp[186]); 
  FFV1_0(w[53], w[4], w[22], pars->GC_11, amp[187]); 
  FFV1_0(w[23], w[4], w[7], pars->GC_1, amp[188]); 
  FFV1_0(w[21], w[24], w[7], pars->GC_1, amp[189]); 
  FFV2_3_0(w[23], w[4], w[10], pars->GC_50, pars->GC_58, amp[190]); 
  FFV2_3_0(w[21], w[24], w[10], pars->GC_50, pars->GC_58, amp[191]); 
  FFV1_0(w[30], w[20], w[7], pars->GC_1, amp[192]); 
  FFV1_0(w[31], w[20], w[29], pars->GC_11, amp[193]); 
  FFV2_3_0(w[30], w[20], w[10], pars->GC_50, pars->GC_58, amp[194]); 
  FFV1_0(w[32], w[20], w[29], pars->GC_11, amp[195]); 
  FFV2_3_0(w[23], w[4], w[48], pars->GC_50, pars->GC_58, amp[196]); 
  FFV2_3_0(w[21], w[24], w[48], pars->GC_50, pars->GC_58, amp[197]); 
  FFV2_3_0(w[25], w[20], w[48], pars->GC_50, pars->GC_58, amp[198]); 
  FFV2_3_0(w[21], w[26], w[48], pars->GC_50, pars->GC_58, amp[199]); 
  FFV2_3_0(w[28], w[4], w[48], pars->GC_50, pars->GC_58, amp[200]); 
  FFV1_0(w[50], w[4], w[27], pars->GC_11, amp[201]); 
  FFV2_3_0(w[30], w[20], w[48], pars->GC_50, pars->GC_58, amp[202]); 
  FFV1_0(w[50], w[20], w[29], pars->GC_11, amp[203]); 
  FFV1_0(w[25], w[20], w[7], pars->GC_1, amp[204]); 
  FFV1_0(w[21], w[26], w[7], pars->GC_1, amp[205]); 
  FFV2_3_0(w[25], w[20], w[10], pars->GC_50, pars->GC_58, amp[206]); 
  FFV2_3_0(w[21], w[26], w[10], pars->GC_50, pars->GC_58, amp[207]); 
  FFV1_0(w[28], w[4], w[7], pars->GC_1, amp[208]); 
  FFV1_0(w[31], w[4], w[27], pars->GC_11, amp[209]); 
  FFV2_3_0(w[28], w[4], w[10], pars->GC_50, pars->GC_58, amp[210]); 
  FFV1_0(w[32], w[4], w[27], pars->GC_11, amp[211]); 
  FFV2_5_0(w[36], w[20], w[48], pars->GC_51, pars->GC_58, amp[212]); 
  FFV2_5_0(w[21], w[37], w[48], pars->GC_51, pars->GC_58, amp[213]); 
  FFV2_5_0(w[39], w[33], w[48], pars->GC_51, pars->GC_58, amp[214]); 
  FFV2_5_0(w[21], w[40], w[48], pars->GC_51, pars->GC_58, amp[215]); 
  FFV2_5_0(w[42], w[20], w[48], pars->GC_51, pars->GC_58, amp[216]); 
  FFV1_0(w[55], w[20], w[41], pars->GC_11, amp[217]); 
  FFV2_5_0(w[45], w[33], w[48], pars->GC_51, pars->GC_58, amp[218]); 
  FFV1_0(w[55], w[33], w[27], pars->GC_11, amp[219]); 
  FFV1_0(w[36], w[20], w[7], pars->GC_2, amp[220]); 
  FFV1_0(w[21], w[37], w[7], pars->GC_2, amp[221]); 
  FFV2_5_0(w[36], w[20], w[10], pars->GC_51, pars->GC_58, amp[222]); 
  FFV2_5_0(w[21], w[37], w[10], pars->GC_51, pars->GC_58, amp[223]); 
  FFV1_0(w[45], w[33], w[7], pars->GC_2, amp[224]); 
  FFV1_0(w[43], w[33], w[27], pars->GC_11, amp[225]); 
  FFV2_5_0(w[45], w[33], w[10], pars->GC_51, pars->GC_58, amp[226]); 
  FFV1_0(w[44], w[33], w[27], pars->GC_11, amp[227]); 
  FFV1_0(w[36], w[20], w[7], pars->GC_1, amp[228]); 
  FFV1_0(w[21], w[37], w[7], pars->GC_1, amp[229]); 
  FFV2_3_0(w[36], w[20], w[10], pars->GC_50, pars->GC_58, amp[230]); 
  FFV2_3_0(w[21], w[37], w[10], pars->GC_50, pars->GC_58, amp[231]); 
  FFV1_0(w[45], w[33], w[7], pars->GC_2, amp[232]); 
  FFV1_0(w[43], w[33], w[27], pars->GC_11, amp[233]); 
  FFV2_5_0(w[45], w[33], w[10], pars->GC_51, pars->GC_58, amp[234]); 
  FFV1_0(w[44], w[33], w[27], pars->GC_11, amp[235]); 
  FFV2_3_0(w[36], w[20], w[48], pars->GC_50, pars->GC_58, amp[236]); 
  FFV2_3_0(w[21], w[37], w[48], pars->GC_50, pars->GC_58, amp[237]); 
  FFV2_3_0(w[39], w[33], w[48], pars->GC_50, pars->GC_58, amp[238]); 
  FFV2_3_0(w[21], w[40], w[48], pars->GC_50, pars->GC_58, amp[239]); 
  FFV2_3_0(w[42], w[20], w[48], pars->GC_50, pars->GC_58, amp[240]); 
  FFV1_0(w[56], w[20], w[41], pars->GC_11, amp[241]); 
  FFV2_3_0(w[45], w[33], w[48], pars->GC_50, pars->GC_58, amp[242]); 
  FFV1_0(w[56], w[33], w[27], pars->GC_11, amp[243]); 
  FFV1_0(w[36], w[20], w[7], pars->GC_1, amp[244]); 
  FFV1_0(w[21], w[37], w[7], pars->GC_1, amp[245]); 
  FFV2_3_0(w[36], w[20], w[10], pars->GC_50, pars->GC_58, amp[246]); 
  FFV2_3_0(w[21], w[37], w[10], pars->GC_50, pars->GC_58, amp[247]); 
  FFV1_0(w[45], w[33], w[7], pars->GC_1, amp[248]); 
  FFV1_0(w[46], w[33], w[27], pars->GC_11, amp[249]); 
  FFV2_3_0(w[45], w[33], w[10], pars->GC_50, pars->GC_58, amp[250]); 
  FFV1_0(w[47], w[33], w[27], pars->GC_11, amp[251]); 
  FFV2_0(w[8], w[5], w[57], pars->GC_100, amp[252]); 
  FFV2_0(w[1], w[9], w[57], pars->GC_100, amp[253]); 
  FFV2_0(w[15], w[5], w[57], pars->GC_100, amp[254]); 
  FFV1_0(w[58], w[5], w[14], pars->GC_11, amp[255]); 
  FFV2_5_0(w[8], w[5], w[48], pars->GC_51, pars->GC_58, amp[256]); 
  FFV2_5_0(w[1], w[9], w[48], pars->GC_51, pars->GC_58, amp[257]); 
  FFV2_5_0(w[19], w[4], w[48], pars->GC_51, pars->GC_58, amp[258]); 
  FFV1_0(w[49], w[4], w[18], pars->GC_11, amp[259]); 
  FFV2_0(w[15], w[5], w[57], pars->GC_100, amp[260]); 
  FFV1_0(w[58], w[5], w[14], pars->GC_11, amp[261]); 
  FFV2_0(w[19], w[4], w[57], pars->GC_100, amp[262]); 
  FFV1_0(w[58], w[4], w[18], pars->GC_11, amp[263]); 
  FFV2_0(w[8], w[5], w[57], pars->GC_100, amp[264]); 
  FFV2_0(w[1], w[9], w[57], pars->GC_100, amp[265]); 
  FFV2_0(w[12], w[4], w[57], pars->GC_100, amp[266]); 
  FFV2_0(w[1], w[13], w[57], pars->GC_100, amp[267]); 
  FFV2_3_0(w[8], w[5], w[48], pars->GC_50, pars->GC_58, amp[268]); 
  FFV2_3_0(w[1], w[9], w[48], pars->GC_50, pars->GC_58, amp[269]); 
  FFV2_5_0(w[19], w[4], w[48], pars->GC_51, pars->GC_58, amp[270]); 
  FFV1_0(w[49], w[4], w[18], pars->GC_11, amp[271]); 
  FFV2_0(w[23], w[4], w[57], pars->GC_100, amp[272]); 
  FFV2_0(w[21], w[24], w[57], pars->GC_100, amp[273]); 
  FFV2_0(w[28], w[4], w[57], pars->GC_100, amp[274]); 
  FFV1_0(w[58], w[4], w[27], pars->GC_11, amp[275]); 
  FFV2_0(w[23], w[4], w[57], pars->GC_100, amp[276]); 
  FFV2_0(w[21], w[24], w[57], pars->GC_100, amp[277]); 
  FFV2_0(w[25], w[20], w[57], pars->GC_100, amp[278]); 
  FFV2_0(w[21], w[26], w[57], pars->GC_100, amp[279]); 
  FFV2_5_0(w[23], w[4], w[48], pars->GC_51, pars->GC_58, amp[280]); 
  FFV2_5_0(w[21], w[24], w[48], pars->GC_51, pars->GC_58, amp[281]); 
  FFV2_5_0(w[30], w[20], w[48], pars->GC_51, pars->GC_58, amp[282]); 
  FFV1_0(w[49], w[20], w[29], pars->GC_11, amp[283]); 
  FFV2_3_0(w[23], w[4], w[48], pars->GC_50, pars->GC_58, amp[284]); 
  FFV2_3_0(w[21], w[24], w[48], pars->GC_50, pars->GC_58, amp[285]); 
  FFV2_5_0(w[30], w[20], w[48], pars->GC_51, pars->GC_58, amp[286]); 
  FFV1_0(w[49], w[20], w[29], pars->GC_11, amp[287]); 
  FFV2_5_0(w[25], w[20], w[48], pars->GC_51, pars->GC_58, amp[288]); 
  FFV2_5_0(w[21], w[26], w[48], pars->GC_51, pars->GC_58, amp[289]); 
  FFV2_5_0(w[28], w[4], w[48], pars->GC_51, pars->GC_58, amp[290]); 
  FFV1_0(w[49], w[4], w[27], pars->GC_11, amp[291]); 
  FFV2_0(w[25], w[20], w[57], pars->GC_100, amp[292]); 
  FFV2_0(w[21], w[26], w[57], pars->GC_100, amp[293]); 
  FFV2_0(w[30], w[20], w[57], pars->GC_100, amp[294]); 
  FFV1_0(w[58], w[20], w[29], pars->GC_11, amp[295]); 
  FFV2_0(w[28], w[4], w[57], pars->GC_100, amp[296]); 
  FFV1_0(w[58], w[4], w[27], pars->GC_11, amp[297]); 
  FFV2_0(w[30], w[20], w[57], pars->GC_100, amp[298]); 
  FFV1_0(w[58], w[20], w[29], pars->GC_11, amp[299]); 
  FFV2_3_0(w[25], w[20], w[48], pars->GC_50, pars->GC_58, amp[300]); 
  FFV2_3_0(w[21], w[26], w[48], pars->GC_50, pars->GC_58, amp[301]); 
  FFV2_5_0(w[28], w[4], w[48], pars->GC_51, pars->GC_58, amp[302]); 
  FFV1_0(w[49], w[4], w[27], pars->GC_11, amp[303]); 
  FFV2_0(w[12], w[4], w[57], pars->GC_100, amp[304]); 
  FFV2_0(w[1], w[13], w[57], pars->GC_100, amp[305]); 
  FFV2_0(w[19], w[4], w[57], pars->GC_100, amp[306]); 
  FFV1_0(w[58], w[4], w[18], pars->GC_11, amp[307]); 
  FFV2_3_0(w[8], w[5], w[48], pars->GC_50, pars->GC_58, amp[308]); 
  FFV2_3_0(w[1], w[9], w[48], pars->GC_50, pars->GC_58, amp[309]); 
  FFV2_3_0(w[19], w[4], w[48], pars->GC_50, pars->GC_58, amp[310]); 
  FFV1_0(w[50], w[4], w[18], pars->GC_11, amp[311]); 
  FFV2_0(w[28], w[4], w[57], pars->GC_100, amp[312]); 
  FFV2_0(w[0], w[51], w[57], pars->GC_100, amp[313]); 
  FFV2_0(w[30], w[20], w[57], pars->GC_100, amp[314]); 
  FFV2_0(w[0], w[54], w[57], pars->GC_100, amp[315]); 
  FFV2_0(w[25], w[20], w[57], pars->GC_100, amp[316]); 
  FFV2_0(w[21], w[26], w[57], pars->GC_100, amp[317]); 
  FFV2_0(w[30], w[20], w[57], pars->GC_100, amp[318]); 
  FFV1_0(w[58], w[20], w[29], pars->GC_11, amp[319]); 
  FFV2_3_0(w[28], w[4], w[48], pars->GC_50, pars->GC_58, amp[320]); 
  FFV2_3_0(w[0], w[51], w[48], pars->GC_50, pars->GC_58, amp[321]); 
  FFV2_5_0(w[25], w[20], w[48], pars->GC_51, pars->GC_58, amp[322]); 
  FFV1_0(w[59], w[20], w[6], pars->GC_11, amp[323]); 
  FFV2_0(w[23], w[4], w[57], pars->GC_100, amp[324]); 
  FFV1_0(w[60], w[4], w[22], pars->GC_11, amp[325]); 
  FFV2_0(w[25], w[20], w[57], pars->GC_100, amp[326]); 
  FFV1_0(w[60], w[20], w[6], pars->GC_11, amp[327]); 
  FFV2_0(w[23], w[4], w[57], pars->GC_100, amp[328]); 
  FFV2_0(w[21], w[24], w[57], pars->GC_100, amp[329]); 
  FFV2_0(w[28], w[4], w[57], pars->GC_100, amp[330]); 
  FFV1_0(w[58], w[4], w[27], pars->GC_11, amp[331]); 
  FFV2_3_0(w[30], w[20], w[48], pars->GC_50, pars->GC_58, amp[332]); 
  FFV2_3_0(w[0], w[54], w[48], pars->GC_50, pars->GC_58, amp[333]); 
  FFV2_5_0(w[23], w[4], w[48], pars->GC_51, pars->GC_58, amp[334]); 
  FFV1_0(w[59], w[4], w[22], pars->GC_11, amp[335]); 
  FFV2_3_0(w[23], w[4], w[48], pars->GC_50, pars->GC_58, amp[336]); 
  FFV2_3_0(w[21], w[24], w[48], pars->GC_50, pars->GC_58, amp[337]); 
  FFV2_3_0(w[30], w[20], w[48], pars->GC_50, pars->GC_58, amp[338]); 
  FFV1_0(w[50], w[20], w[29], pars->GC_11, amp[339]); 
  FFV2_3_0(w[25], w[20], w[48], pars->GC_50, pars->GC_58, amp[340]); 
  FFV2_3_0(w[21], w[26], w[48], pars->GC_50, pars->GC_58, amp[341]); 
  FFV2_3_0(w[28], w[4], w[48], pars->GC_50, pars->GC_58, amp[342]); 
  FFV1_0(w[50], w[4], w[27], pars->GC_11, amp[343]); 
  FFV2_0(w[36], w[20], w[57], pars->GC_100, amp[344]); 
  FFV2_0(w[21], w[37], w[57], pars->GC_100, amp[345]); 
  FFV2_0(w[39], w[33], w[57], pars->GC_100, amp[346]); 
  FFV2_0(w[21], w[40], w[57], pars->GC_100, amp[347]); 
  FFV2_5_0(w[36], w[20], w[48], pars->GC_51, pars->GC_58, amp[348]); 
  FFV2_5_0(w[21], w[37], w[48], pars->GC_51, pars->GC_58, amp[349]); 
  FFV2_5_0(w[45], w[33], w[48], pars->GC_51, pars->GC_58, amp[350]); 
  FFV1_0(w[55], w[33], w[27], pars->GC_11, amp[351]); 
  FFV2_0(w[36], w[20], w[57], pars->GC_100, amp[352]); 
  FFV2_0(w[21], w[37], w[57], pars->GC_100, amp[353]); 
  FFV2_0(w[42], w[20], w[57], pars->GC_100, amp[354]); 
  FFV1_0(w[61], w[20], w[41], pars->GC_11, amp[355]); 
  FFV2_0(w[39], w[33], w[57], pars->GC_100, amp[356]); 
  FFV2_0(w[21], w[40], w[57], pars->GC_100, amp[357]); 
  FFV2_0(w[45], w[33], w[57], pars->GC_100, amp[358]); 
  FFV1_0(w[61], w[33], w[27], pars->GC_11, amp[359]); 
  FFV2_3_0(w[36], w[20], w[48], pars->GC_50, pars->GC_58, amp[360]); 
  FFV2_3_0(w[21], w[37], w[48], pars->GC_50, pars->GC_58, amp[361]); 
  FFV2_5_0(w[45], w[33], w[48], pars->GC_51, pars->GC_58, amp[362]); 
  FFV1_0(w[55], w[33], w[27], pars->GC_11, amp[363]); 
  FFV2_0(w[42], w[20], w[57], pars->GC_100, amp[364]); 
  FFV1_0(w[61], w[20], w[41], pars->GC_11, amp[365]); 
  FFV2_0(w[45], w[33], w[57], pars->GC_100, amp[366]); 
  FFV1_0(w[61], w[33], w[27], pars->GC_11, amp[367]); 
  FFV2_3_0(w[36], w[20], w[48], pars->GC_50, pars->GC_58, amp[368]); 
  FFV2_3_0(w[21], w[37], w[48], pars->GC_50, pars->GC_58, amp[369]); 
  FFV2_3_0(w[45], w[33], w[48], pars->GC_50, pars->GC_58, amp[370]); 
  FFV1_0(w[56], w[33], w[27], pars->GC_11, amp[371]); 
  FFV2_0(w[8], w[5], w[57], pars->GC_100, amp[372]); 
  FFV2_0(w[1], w[9], w[57], pars->GC_100, amp[373]); 
  FFV2_0(w[15], w[5], w[57], pars->GC_100, amp[374]); 
  FFV1_0(w[58], w[5], w[14], pars->GC_11, amp[375]); 
  FFV2_0(w[8], w[5], w[57], pars->GC_100, amp[376]); 
  FFV2_0(w[1], w[9], w[57], pars->GC_100, amp[377]); 
  FFV2_0(w[23], w[4], w[57], pars->GC_100, amp[378]); 
  FFV2_0(w[21], w[24], w[57], pars->GC_100, amp[379]); 
  FFV2_0(w[23], w[4], w[57], pars->GC_100, amp[380]); 
  FFV2_0(w[21], w[24], w[57], pars->GC_100, amp[381]); 
  FFV2_0(w[28], w[4], w[57], pars->GC_100, amp[382]); 
  FFV1_0(w[58], w[4], w[27], pars->GC_11, amp[383]); 
  FFV2_0(w[25], w[20], w[57], pars->GC_100, amp[384]); 
  FFV2_0(w[21], w[26], w[57], pars->GC_100, amp[385]); 
  FFV2_0(w[30], w[20], w[57], pars->GC_100, amp[386]); 
  FFV1_0(w[58], w[20], w[29], pars->GC_11, amp[387]); 
  FFV2_0(w[25], w[20], w[57], pars->GC_100, amp[388]); 
  FFV2_0(w[21], w[26], w[57], pars->GC_100, amp[389]); 
  FFV2_0(w[19], w[4], w[57], pars->GC_100, amp[390]); 
  FFV1_0(w[58], w[4], w[18], pars->GC_11, amp[391]); 
  FFV2_0(w[30], w[20], w[57], pars->GC_100, amp[392]); 
  FFV2_0(w[0], w[54], w[57], pars->GC_100, amp[393]); 
  FFV2_0(w[28], w[4], w[57], pars->GC_100, amp[394]); 
  FFV2_0(w[0], w[51], w[57], pars->GC_100, amp[395]); 
  FFV2_0(w[36], w[20], w[57], pars->GC_100, amp[396]); 
  FFV2_0(w[21], w[37], w[57], pars->GC_100, amp[397]); 
  FFV2_0(w[39], w[33], w[57], pars->GC_100, amp[398]); 
  FFV2_0(w[21], w[40], w[57], pars->GC_100, amp[399]); 
  FFV2_0(w[36], w[20], w[57], pars->GC_100, amp[400]); 
  FFV2_0(w[21], w[37], w[57], pars->GC_100, amp[401]); 
  FFV2_0(w[45], w[33], w[57], pars->GC_100, amp[402]); 
  FFV1_0(w[61], w[33], w[27], pars->GC_11, amp[403]); 


}
double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_uu_epemuu() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 16;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[0] + 1./3. * amp[1] + 1./3. * amp[2] + 1./3.
      * amp[3] + amp[4] + amp[5] + amp[6] + amp[7] + amp[8] + amp[9] + amp[10]
      + amp[11] + 1./3. * amp[12] + 1./3. * amp[13] + 1./3. * amp[14] + 1./3. *
      amp[15]);
  jamp[1] = +1./2. * (-amp[0] - amp[1] - amp[2] - amp[3] - 1./3. * amp[4] -
      1./3. * amp[5] - 1./3. * amp[6] - 1./3. * amp[7] - 1./3. * amp[8] - 1./3.
      * amp[9] - 1./3. * amp[10] - 1./3. * amp[11] - amp[12] - amp[13] -
      amp[14] - amp[15]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[0][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_uux_epemuux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 16;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[16] + 1./3. * amp[17] + 1./3. * amp[18] +
      1./3. * amp[19] + amp[20] + amp[21] + amp[22] + amp[23] + amp[24] +
      amp[25] + amp[26] + amp[27] + 1./3. * amp[28] + 1./3. * amp[29] + 1./3. *
      amp[30] + 1./3. * amp[31]);
  jamp[1] = +1./2. * (-amp[16] - amp[17] - amp[18] - amp[19] - 1./3. * amp[20]
      - 1./3. * amp[21] - 1./3. * amp[22] - 1./3. * amp[23] - 1./3. * amp[24] -
      1./3. * amp[25] - 1./3. * amp[26] - 1./3. * amp[27] - amp[28] - amp[29] -
      amp[30] - amp[31]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[1][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_dd_epemdd() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 16;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[32] + 1./3. * amp[33] + 1./3. * amp[34] +
      1./3. * amp[35] + amp[36] + amp[37] + amp[38] + amp[39] + amp[40] +
      amp[41] + amp[42] + amp[43] + 1./3. * amp[44] + 1./3. * amp[45] + 1./3. *
      amp[46] + 1./3. * amp[47]);
  jamp[1] = +1./2. * (-amp[32] - amp[33] - amp[34] - amp[35] - 1./3. * amp[36]
      - 1./3. * amp[37] - 1./3. * amp[38] - 1./3. * amp[39] - 1./3. * amp[40] -
      1./3. * amp[41] - 1./3. * amp[42] - 1./3. * amp[43] - amp[44] - amp[45] -
      amp[46] - amp[47]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[2][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_ddx_epemddx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 16;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[48] + 1./3. * amp[49] + 1./3. * amp[50] +
      1./3. * amp[51] + amp[52] + amp[53] + amp[54] + amp[55] + amp[56] +
      amp[57] + amp[58] + amp[59] + 1./3. * amp[60] + 1./3. * amp[61] + 1./3. *
      amp[62] + 1./3. * amp[63]);
  jamp[1] = +1./2. * (-amp[48] - amp[49] - amp[50] - amp[51] - 1./3. * amp[52]
      - 1./3. * amp[53] - 1./3. * amp[54] - 1./3. * amp[55] - 1./3. * amp[56] -
      1./3. * amp[57] - 1./3. * amp[58] - 1./3. * amp[59] - amp[60] - amp[61] -
      amp[62] - amp[63]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[3][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_uxux_epemuxux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 16;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[64] + 1./3. * amp[65] + 1./3. * amp[66] +
      1./3. * amp[67] + amp[68] + amp[69] + amp[70] + amp[71] + amp[72] +
      amp[73] + amp[74] + amp[75] + 1./3. * amp[76] + 1./3. * amp[77] + 1./3. *
      amp[78] + 1./3. * amp[79]);
  jamp[1] = +1./2. * (-amp[64] - amp[65] - amp[66] - amp[67] - 1./3. * amp[68]
      - 1./3. * amp[69] - 1./3. * amp[70] - 1./3. * amp[71] - 1./3. * amp[72] -
      1./3. * amp[73] - 1./3. * amp[74] - 1./3. * amp[75] - amp[76] - amp[77] -
      amp[78] - amp[79]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[4][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_dxdx_epemdxdx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 16;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[80] + 1./3. * amp[81] + 1./3. * amp[82] +
      1./3. * amp[83] + amp[84] + amp[85] + amp[86] + amp[87] + amp[88] +
      amp[89] + amp[90] + amp[91] + 1./3. * amp[92] + 1./3. * amp[93] + 1./3. *
      amp[94] + 1./3. * amp[95]);
  jamp[1] = +1./2. * (-amp[80] - amp[81] - amp[82] - amp[83] - 1./3. * amp[84]
      - 1./3. * amp[85] - 1./3. * amp[86] - 1./3. * amp[87] - 1./3. * amp[88] -
      1./3. * amp[89] - 1./3. * amp[90] - 1./3. * amp[91] - amp[92] - amp[93] -
      amp[94] - amp[95]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[5][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_uu_vexveuu() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 8;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[96] + 1./3. * amp[97] + amp[98] + amp[99] +
      amp[100] + amp[101] + 1./3. * amp[102] + 1./3. * amp[103]);
  jamp[1] = +1./2. * (-amp[96] - amp[97] - 1./3. * amp[98] - 1./3. * amp[99] -
      1./3. * amp[100] - 1./3. * amp[101] - amp[102] - amp[103]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[6][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_uc_epemuc() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 8;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[0] + 1./3. * amp[1] + 1./3. * amp[2] + 1./3.
      * amp[3] + 1./3. * amp[104] + 1./3. * amp[105] + 1./3. * amp[106] + 1./3.
      * amp[107]);
  jamp[1] = +1./2. * (-amp[0] - amp[1] - amp[2] - amp[3] - amp[104] - amp[105]
      - amp[106] - amp[107]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[7][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_ud_epemud() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 8;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[108] + 1./3. * amp[109] + 1./3. * amp[110] +
      1./3. * amp[111] + 1./3. * amp[112] + 1./3. * amp[113] + 1./3. * amp[114]
      + 1./3. * amp[115]);
  jamp[1] = +1./2. * (-amp[108] - amp[109] - amp[110] - amp[111] - amp[112] -
      amp[113] - amp[114] - amp[115]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[8][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_uux_epemccx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 8;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[116] + 1./3. * amp[117] + 1./3. * amp[118] +
      1./3. * amp[119] + 1./3. * amp[120] + 1./3. * amp[121] + 1./3. * amp[122]
      + 1./3. * amp[123]);
  jamp[1] = +1./2. * (-amp[116] - amp[117] - amp[118] - amp[119] - amp[120] -
      amp[121] - amp[122] - amp[123]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[9][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_uux_epemddx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 8;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[124] + 1./3. * amp[125] + 1./3. * amp[126] +
      1./3. * amp[127] + 1./3. * amp[128] + 1./3. * amp[129] + 1./3. * amp[130]
      + 1./3. * amp[131]);
  jamp[1] = +1./2. * (-amp[124] - amp[125] - amp[126] - amp[127] - amp[128] -
      amp[129] - amp[130] - amp[131]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[10][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_uux_vexveuux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 8;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[132] + 1./3. * amp[133] + amp[134] +
      amp[135] + amp[136] + amp[137] + 1./3. * amp[138] + 1./3. * amp[139]);
  jamp[1] = +1./2. * (-amp[132] - amp[133] - 1./3. * amp[134] - 1./3. *
      amp[135] - 1./3. * amp[136] - 1./3. * amp[137] - amp[138] - amp[139]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[11][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_ucx_epemucx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 8;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[140] + amp[141] + amp[142] + amp[143] + amp[144] +
      amp[145] + amp[146] + amp[147]);
  jamp[1] = +1./2. * (-1./3. * amp[140] - 1./3. * amp[141] - 1./3. * amp[142] -
      1./3. * amp[143] - 1./3. * amp[144] - 1./3. * amp[145] - 1./3. * amp[146]
      - 1./3. * amp[147]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[12][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_udx_epemudx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 8;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[148] + amp[149] + amp[150] + amp[151] + amp[152] +
      amp[153] + amp[154] + amp[155]);
  jamp[1] = +1./2. * (-1./3. * amp[148] - 1./3. * amp[149] - 1./3. * amp[150] -
      1./3. * amp[151] - 1./3. * amp[152] - 1./3. * amp[153] - 1./3. * amp[154]
      - 1./3. * amp[155]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[13][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_dd_vexvedd() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 8;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[156] + 1./3. * amp[157] + amp[158] +
      amp[159] + amp[160] + amp[161] + 1./3. * amp[162] + 1./3. * amp[163]);
  jamp[1] = +1./2. * (-amp[156] - amp[157] - 1./3. * amp[158] - 1./3. *
      amp[159] - 1./3. * amp[160] - 1./3. * amp[161] - amp[162] - amp[163]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[14][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_ds_epemds() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 8;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[164] + 1./3. * amp[165] + 1./3. * amp[166] +
      1./3. * amp[167] + 1./3. * amp[168] + 1./3. * amp[169] + 1./3. * amp[170]
      + 1./3. * amp[171]);
  jamp[1] = +1./2. * (-amp[164] - amp[165] - amp[166] - amp[167] - amp[168] -
      amp[169] - amp[170] - amp[171]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[15][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_dux_epemdux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 8;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[172] + amp[173] + amp[174] + amp[175] + amp[176] +
      amp[177] + amp[178] + amp[179]);
  jamp[1] = +1./2. * (-1./3. * amp[172] - 1./3. * amp[173] - 1./3. * amp[174] -
      1./3. * amp[175] - 1./3. * amp[176] - 1./3. * amp[177] - 1./3. * amp[178]
      - 1./3. * amp[179]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[16][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_ddx_epemuux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 8;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[180] + 1./3. * amp[181] + 1./3. * amp[182] +
      1./3. * amp[183] + 1./3. * amp[184] + 1./3. * amp[185] + 1./3. * amp[186]
      + 1./3. * amp[187]);
  jamp[1] = +1./2. * (-amp[180] - amp[181] - amp[182] - amp[183] - amp[184] -
      amp[185] - amp[186] - amp[187]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[17][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_ddx_epemssx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 8;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[188] + 1./3. * amp[189] + 1./3. * amp[190] +
      1./3. * amp[191] + 1./3. * amp[192] + 1./3. * amp[193] + 1./3. * amp[194]
      + 1./3. * amp[195]);
  jamp[1] = +1./2. * (-amp[188] - amp[189] - amp[190] - amp[191] - amp[192] -
      amp[193] - amp[194] - amp[195]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[18][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_ddx_vexveddx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 8;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[196] + 1./3. * amp[197] + amp[198] +
      amp[199] + amp[200] + amp[201] + 1./3. * amp[202] + 1./3. * amp[203]);
  jamp[1] = +1./2. * (-amp[196] - amp[197] - 1./3. * amp[198] - 1./3. *
      amp[199] - 1./3. * amp[200] - 1./3. * amp[201] - amp[202] - amp[203]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[19][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_dsx_epemdsx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 8;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[204] + amp[205] + amp[206] + amp[207] + amp[208] +
      amp[209] + amp[210] + amp[211]);
  jamp[1] = +1./2. * (-1./3. * amp[204] - 1./3. * amp[205] - 1./3. * amp[206] -
      1./3. * amp[207] - 1./3. * amp[208] - 1./3. * amp[209] - 1./3. * amp[210]
      - 1./3. * amp[211]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[20][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_uxux_vexveuxux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 8;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[212] + 1./3. * amp[213] + amp[214] +
      amp[215] + amp[216] + amp[217] + 1./3. * amp[218] + 1./3. * amp[219]);
  jamp[1] = +1./2. * (-amp[212] - amp[213] - 1./3. * amp[214] - 1./3. *
      amp[215] - 1./3. * amp[216] - 1./3. * amp[217] - amp[218] - amp[219]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[21][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_uxcx_epemuxcx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 8;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[220] + 1./3. * amp[221] + 1./3. * amp[222] +
      1./3. * amp[223] + 1./3. * amp[224] + 1./3. * amp[225] + 1./3. * amp[226]
      + 1./3. * amp[227]);
  jamp[1] = +1./2. * (-amp[220] - amp[221] - amp[222] - amp[223] - amp[224] -
      amp[225] - amp[226] - amp[227]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[22][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_uxdx_epemuxdx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 8;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[228] + 1./3. * amp[229] + 1./3. * amp[230] +
      1./3. * amp[231] + 1./3. * amp[232] + 1./3. * amp[233] + 1./3. * amp[234]
      + 1./3. * amp[235]);
  jamp[1] = +1./2. * (-amp[228] - amp[229] - amp[230] - amp[231] - amp[232] -
      amp[233] - amp[234] - amp[235]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[23][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_dxdx_vexvedxdx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 8;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[236] + 1./3. * amp[237] + amp[238] +
      amp[239] + amp[240] + amp[241] + 1./3. * amp[242] + 1./3. * amp[243]);
  jamp[1] = +1./2. * (-amp[236] - amp[237] - 1./3. * amp[238] - 1./3. *
      amp[239] - 1./3. * amp[240] - 1./3. * amp[241] - amp[242] - amp[243]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[24][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_dxsx_epemdxsx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 8;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[244] + 1./3. * amp[245] + 1./3. * amp[246] +
      1./3. * amp[247] + 1./3. * amp[248] + 1./3. * amp[249] + 1./3. * amp[250]
      + 1./3. * amp[251]);
  jamp[1] = +1./2. * (-amp[244] - amp[245] - amp[246] - amp[247] - amp[248] -
      amp[249] - amp[250] - amp[251]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[25][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_uu_epveud() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 4;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[252] + 1./3. * amp[253] + amp[254] +
      amp[255]);
  jamp[1] = +1./2. * (-amp[252] - amp[253] - 1./3. * amp[254] - 1./3. *
      amp[255]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[26][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_uc_vexveuc() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 4;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[256] + 1./3. * amp[257] + 1./3. * amp[258] +
      1./3. * amp[259]);
  jamp[1] = +1./2. * (-amp[256] - amp[257] - amp[258] - amp[259]); 

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[27][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_ud_epvedd() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 4;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[260] + amp[261] + 1./3. * amp[262] + 1./3. *
      amp[263]);
  jamp[1] = +1./2. * (-1./3. * amp[260] - 1./3. * amp[261] - amp[262] -
      amp[263]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[28][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_ud_vexemuu() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 4;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[264] + 1./3. * amp[265] + amp[266] +
      amp[267]);
  jamp[1] = +1./2. * (-amp[264] - amp[265] - 1./3. * amp[266] - 1./3. *
      amp[267]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[29][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_ud_vexveud() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 4;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[268] + 1./3. * amp[269] + 1./3. * amp[270] +
      1./3. * amp[271]);
  jamp[1] = +1./2. * (-amp[268] - amp[269] - amp[270] - amp[271]); 

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[30][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_uux_epvedux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 4;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[272] + 1./3. * amp[273] + amp[274] +
      amp[275]);
  jamp[1] = +1./2. * (-amp[272] - amp[273] - 1./3. * amp[274] - 1./3. *
      amp[275]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[31][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_uux_vexemudx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 4;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[276] + 1./3. * amp[277] + amp[278] +
      amp[279]);
  jamp[1] = +1./2. * (-amp[276] - amp[277] - 1./3. * amp[278] - 1./3. *
      amp[279]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[32][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_uux_vexveccx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 4;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[280] + 1./3. * amp[281] + 1./3. * amp[282] +
      1./3. * amp[283]);
  jamp[1] = +1./2. * (-amp[280] - amp[281] - amp[282] - amp[283]); 

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[33][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_uux_vexveddx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 4;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[284] + 1./3. * amp[285] + 1./3. * amp[286] +
      1./3. * amp[287]);
  jamp[1] = +1./2. * (-amp[284] - amp[285] - amp[286] - amp[287]); 

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[34][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_ucx_vexveucx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 4;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[288] + amp[289] + amp[290] + amp[291]); 
  jamp[1] = +1./2. * (-1./3. * amp[288] - 1./3. * amp[289] - 1./3. * amp[290] -
      1./3. * amp[291]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[35][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_udx_epveuux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 4;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[292] + amp[293] + 1./3. * amp[294] + 1./3. *
      amp[295]);
  jamp[1] = +1./2. * (-1./3. * amp[292] - 1./3. * amp[293] - amp[294] -
      amp[295]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[36][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_udx_epveddx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 4;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[296] + amp[297] + 1./3. * amp[298] + 1./3. *
      amp[299]);
  jamp[1] = +1./2. * (-1./3. * amp[296] - 1./3. * amp[297] - amp[298] -
      amp[299]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[37][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_udx_vexveudx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 4;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[300] + amp[301] + amp[302] + amp[303]); 
  jamp[1] = +1./2. * (-1./3. * amp[300] - 1./3. * amp[301] - 1./3. * amp[302] -
      1./3. * amp[303]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[38][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_dd_vexemud() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 4;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[304] + amp[305] + 1./3. * amp[306] + 1./3. *
      amp[307]);
  jamp[1] = +1./2. * (-1./3. * amp[304] - 1./3. * amp[305] - amp[306] -
      amp[307]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[39][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_ds_vexveds() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 4;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[308] + 1./3. * amp[309] + 1./3. * amp[310] +
      1./3. * amp[311]);
  jamp[1] = +1./2. * (-amp[308] - amp[309] - amp[310] - amp[311]); 

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[40][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_dux_vexemuux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 4;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[312] + amp[313] + 1./3. * amp[314] + 1./3. *
      amp[315]);
  jamp[1] = +1./2. * (-1./3. * amp[312] - 1./3. * amp[313] - amp[314] -
      amp[315]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[41][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_dux_vexemddx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 4;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[316] + amp[317] + 1./3. * amp[318] + 1./3. *
      amp[319]);
  jamp[1] = +1./2. * (-1./3. * amp[316] - 1./3. * amp[317] - amp[318] -
      amp[319]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[42][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_dux_vexvedux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 4;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[320] + amp[321] + amp[322] + amp[323]); 
  jamp[1] = +1./2. * (-1./3. * amp[320] - 1./3. * amp[321] - 1./3. * amp[322] -
      1./3. * amp[323]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[43][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_ddx_epvedux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 4;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[324] + 1./3. * amp[325] + amp[326] +
      amp[327]);
  jamp[1] = +1./2. * (-amp[324] - amp[325] - 1./3. * amp[326] - 1./3. *
      amp[327]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[44][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_ddx_vexemudx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 4;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[328] + 1./3. * amp[329] + amp[330] +
      amp[331]);
  jamp[1] = +1./2. * (-amp[328] - amp[329] - 1./3. * amp[330] - 1./3. *
      amp[331]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[45][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_ddx_vexveuux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 4;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[332] + 1./3. * amp[333] + 1./3. * amp[334] +
      1./3. * amp[335]);
  jamp[1] = +1./2. * (-amp[332] - amp[333] - amp[334] - amp[335]); 

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[46][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_ddx_vexvessx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 4;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[336] + 1./3. * amp[337] + 1./3. * amp[338] +
      1./3. * amp[339]);
  jamp[1] = +1./2. * (-amp[336] - amp[337] - amp[338] - amp[339]); 

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[47][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_dsx_vexvedsx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 4;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[340] + amp[341] + amp[342] + amp[343]); 
  jamp[1] = +1./2. * (-1./3. * amp[340] - 1./3. * amp[341] - 1./3. * amp[342] -
      1./3. * amp[343]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[48][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_uxux_vexemuxdx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 4;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[344] + 1./3. * amp[345] + amp[346] +
      amp[347]);
  jamp[1] = +1./2. * (-amp[344] - amp[345] - 1./3. * amp[346] - 1./3. *
      amp[347]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[49][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_uxcx_vexveuxcx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 4;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[348] + 1./3. * amp[349] + 1./3. * amp[350] +
      1./3. * amp[351]);
  jamp[1] = +1./2. * (-amp[348] - amp[349] - amp[350] - amp[351]); 

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[50][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_uxdx_epveuxux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 4;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[352] + 1./3. * amp[353] + amp[354] +
      amp[355]);
  jamp[1] = +1./2. * (-amp[352] - amp[353] - 1./3. * amp[354] - 1./3. *
      amp[355]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[51][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_uxdx_vexemdxdx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 4;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[356] + amp[357] + 1./3. * amp[358] + 1./3. *
      amp[359]);
  jamp[1] = +1./2. * (-1./3. * amp[356] - 1./3. * amp[357] - amp[358] -
      amp[359]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[52][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_uxdx_vexveuxdx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 4;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[360] + 1./3. * amp[361] + 1./3. * amp[362] +
      1./3. * amp[363]);
  jamp[1] = +1./2. * (-amp[360] - amp[361] - amp[362] - amp[363]); 

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[53][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_dxdx_epveuxdx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 4;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[364] + amp[365] + 1./3. * amp[366] + 1./3. *
      amp[367]);
  jamp[1] = +1./2. * (-1./3. * amp[364] - 1./3. * amp[365] - amp[366] -
      amp[367]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[54][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_dxsx_vexvedxsx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 4;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[368] + 1./3. * amp[369] + 1./3. * amp[370] +
      1./3. * amp[371]);
  jamp[1] = +1./2. * (-amp[368] - amp[369] - amp[370] - amp[371]); 

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[55][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_uc_epveus() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 2;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[372] + 1./3. * amp[373]); 
  jamp[1] = +1./2. * (-amp[372] - amp[373]); 

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[56][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_uc_epvecd() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 2;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[374] + amp[375]); 
  jamp[1] = +1./2. * (-1./3. * amp[374] - 1./3. * amp[375]); 

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[57][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_us_vexemuc() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 2;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[376] + 1./3. * amp[377]); 
  jamp[1] = +1./2. * (-amp[376] - amp[377]); 

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[58][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_uux_epvescx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 2;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[378] + 1./3. * amp[379]); 
  jamp[1] = +1./2. * (-amp[378] - amp[379]); 

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[59][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_uux_vexemcsx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 2;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[380] + 1./3. * amp[381]); 
  jamp[1] = +1./2. * (-amp[380] - amp[381]); 

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[60][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_ucx_epvedcx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 2;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[382] + amp[383]); 
  jamp[1] = +1./2. * (-1./3. * amp[382] - 1./3. * amp[383]); 

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[61][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_ucx_vexemusx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 2;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[384] + amp[385]); 
  jamp[1] = +1./2. * (-1./3. * amp[384] - 1./3. * amp[385]); 

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[62][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_udx_epveccx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 2;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[386] + 1./3. * amp[387]); 
  jamp[1] = +1./2. * (-amp[386] - amp[387]); 

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[63][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_usx_epveucx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 2;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[388] + amp[389]); 
  jamp[1] = +1./2. * (-1./3. * amp[388] - 1./3. * amp[389]); 

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[64][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_ds_vexemus() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 2;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[390] + 1./3. * amp[391]); 
  jamp[1] = +1./2. * (-amp[390] - amp[391]); 

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[65][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_dux_vexemccx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 2;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[392] + 1./3. * amp[393]); 
  jamp[1] = +1./2. * (-amp[392] - amp[393]); 

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[66][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_dcx_vexemucx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 2;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[394] + amp[395]); 
  jamp[1] = +1./2. * (-1./3. * amp[394] - 1./3. * amp[395]); 

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[67][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_uxcx_vexemuxsx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 2;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[396] + 1./3. * amp[397]); 
  jamp[1] = +1./2. * (-amp[396] - amp[397]); 

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[68][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_uxcx_vexemcxdx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 2;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[398] + amp[399]); 
  jamp[1] = +1./2. * (-1./3. * amp[398] - 1./3. * amp[399]); 

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[69][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_uxsx_epveuxcx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 2;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[400] + 1./3. * amp[401]); 
  jamp[1] = +1./2. * (-amp[400] - amp[401]); 

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[70][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R3_P192_sm_qq_llqq::matrix_3_dxsx_epveuxsx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 2;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[402] + 1./3. * amp[403]); 
  jamp[1] = +1./2. * (-amp[402] - amp[403]); 

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[71][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}


}  // end namespace PY8MEs_namespace

