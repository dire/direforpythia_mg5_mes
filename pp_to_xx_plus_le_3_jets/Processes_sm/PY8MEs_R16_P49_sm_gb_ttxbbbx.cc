//==========================================================================
// This file has been automatically generated for Pythia 8 by
// MadGraph5_aMC@NLO v. 2.6.0, 2017-08-16
// By the MadGraph5_aMC@NLO Development Team
// Visit launchpad.net/madgraph5 and amcatnlo.web.cern.ch
//==========================================================================

#include "PY8MEs_R16_P49_sm_gb_ttxbbbx.h"
#include "HelAmps_sm.h"

using namespace Pythia8_sm; 

namespace PY8MEs_namespace 
{
//==========================================================================
// Class member functions for calculating the matrix elements for
// Process: g b > t t~ b b b~ WEIGHTED<=5 @16
// Process: g b~ > t t~ b b~ b~ WEIGHTED<=5 @16

// Exception class
class PY8MEs_R16_P49_sm_gb_ttxbbbxException : public exception
{
  virtual const char * what() const throw()
  {
    return "Exception in class 'PY8MEs_R16_P49_sm_gb_ttxbbbx'."; 
  }
}
PY8MEs_R16_P49_sm_gb_ttxbbbx_exception; 

std::set<int> PY8MEs_R16_P49_sm_gb_ttxbbbx::s_channel_proc = std::set<int>
    (createset<int> ());

int PY8MEs_R16_P49_sm_gb_ttxbbbx::helicities[ncomb][nexternal] = {{-1, -1, -1,
    -1, -1, -1, -1}, {-1, -1, -1, -1, -1, -1, 1}, {-1, -1, -1, -1, -1, 1, -1},
    {-1, -1, -1, -1, -1, 1, 1}, {-1, -1, -1, -1, 1, -1, -1}, {-1, -1, -1, -1,
    1, -1, 1}, {-1, -1, -1, -1, 1, 1, -1}, {-1, -1, -1, -1, 1, 1, 1}, {-1, -1,
    -1, 1, -1, -1, -1}, {-1, -1, -1, 1, -1, -1, 1}, {-1, -1, -1, 1, -1, 1, -1},
    {-1, -1, -1, 1, -1, 1, 1}, {-1, -1, -1, 1, 1, -1, -1}, {-1, -1, -1, 1, 1,
    -1, 1}, {-1, -1, -1, 1, 1, 1, -1}, {-1, -1, -1, 1, 1, 1, 1}, {-1, -1, 1,
    -1, -1, -1, -1}, {-1, -1, 1, -1, -1, -1, 1}, {-1, -1, 1, -1, -1, 1, -1},
    {-1, -1, 1, -1, -1, 1, 1}, {-1, -1, 1, -1, 1, -1, -1}, {-1, -1, 1, -1, 1,
    -1, 1}, {-1, -1, 1, -1, 1, 1, -1}, {-1, -1, 1, -1, 1, 1, 1}, {-1, -1, 1, 1,
    -1, -1, -1}, {-1, -1, 1, 1, -1, -1, 1}, {-1, -1, 1, 1, -1, 1, -1}, {-1, -1,
    1, 1, -1, 1, 1}, {-1, -1, 1, 1, 1, -1, -1}, {-1, -1, 1, 1, 1, -1, 1}, {-1,
    -1, 1, 1, 1, 1, -1}, {-1, -1, 1, 1, 1, 1, 1}, {-1, 1, -1, -1, -1, -1, -1},
    {-1, 1, -1, -1, -1, -1, 1}, {-1, 1, -1, -1, -1, 1, -1}, {-1, 1, -1, -1, -1,
    1, 1}, {-1, 1, -1, -1, 1, -1, -1}, {-1, 1, -1, -1, 1, -1, 1}, {-1, 1, -1,
    -1, 1, 1, -1}, {-1, 1, -1, -1, 1, 1, 1}, {-1, 1, -1, 1, -1, -1, -1}, {-1,
    1, -1, 1, -1, -1, 1}, {-1, 1, -1, 1, -1, 1, -1}, {-1, 1, -1, 1, -1, 1, 1},
    {-1, 1, -1, 1, 1, -1, -1}, {-1, 1, -1, 1, 1, -1, 1}, {-1, 1, -1, 1, 1, 1,
    -1}, {-1, 1, -1, 1, 1, 1, 1}, {-1, 1, 1, -1, -1, -1, -1}, {-1, 1, 1, -1,
    -1, -1, 1}, {-1, 1, 1, -1, -1, 1, -1}, {-1, 1, 1, -1, -1, 1, 1}, {-1, 1, 1,
    -1, 1, -1, -1}, {-1, 1, 1, -1, 1, -1, 1}, {-1, 1, 1, -1, 1, 1, -1}, {-1, 1,
    1, -1, 1, 1, 1}, {-1, 1, 1, 1, -1, -1, -1}, {-1, 1, 1, 1, -1, -1, 1}, {-1,
    1, 1, 1, -1, 1, -1}, {-1, 1, 1, 1, -1, 1, 1}, {-1, 1, 1, 1, 1, -1, -1},
    {-1, 1, 1, 1, 1, -1, 1}, {-1, 1, 1, 1, 1, 1, -1}, {-1, 1, 1, 1, 1, 1, 1},
    {1, -1, -1, -1, -1, -1, -1}, {1, -1, -1, -1, -1, -1, 1}, {1, -1, -1, -1,
    -1, 1, -1}, {1, -1, -1, -1, -1, 1, 1}, {1, -1, -1, -1, 1, -1, -1}, {1, -1,
    -1, -1, 1, -1, 1}, {1, -1, -1, -1, 1, 1, -1}, {1, -1, -1, -1, 1, 1, 1}, {1,
    -1, -1, 1, -1, -1, -1}, {1, -1, -1, 1, -1, -1, 1}, {1, -1, -1, 1, -1, 1,
    -1}, {1, -1, -1, 1, -1, 1, 1}, {1, -1, -1, 1, 1, -1, -1}, {1, -1, -1, 1, 1,
    -1, 1}, {1, -1, -1, 1, 1, 1, -1}, {1, -1, -1, 1, 1, 1, 1}, {1, -1, 1, -1,
    -1, -1, -1}, {1, -1, 1, -1, -1, -1, 1}, {1, -1, 1, -1, -1, 1, -1}, {1, -1,
    1, -1, -1, 1, 1}, {1, -1, 1, -1, 1, -1, -1}, {1, -1, 1, -1, 1, -1, 1}, {1,
    -1, 1, -1, 1, 1, -1}, {1, -1, 1, -1, 1, 1, 1}, {1, -1, 1, 1, -1, -1, -1},
    {1, -1, 1, 1, -1, -1, 1}, {1, -1, 1, 1, -1, 1, -1}, {1, -1, 1, 1, -1, 1,
    1}, {1, -1, 1, 1, 1, -1, -1}, {1, -1, 1, 1, 1, -1, 1}, {1, -1, 1, 1, 1, 1,
    -1}, {1, -1, 1, 1, 1, 1, 1}, {1, 1, -1, -1, -1, -1, -1}, {1, 1, -1, -1, -1,
    -1, 1}, {1, 1, -1, -1, -1, 1, -1}, {1, 1, -1, -1, -1, 1, 1}, {1, 1, -1, -1,
    1, -1, -1}, {1, 1, -1, -1, 1, -1, 1}, {1, 1, -1, -1, 1, 1, -1}, {1, 1, -1,
    -1, 1, 1, 1}, {1, 1, -1, 1, -1, -1, -1}, {1, 1, -1, 1, -1, -1, 1}, {1, 1,
    -1, 1, -1, 1, -1}, {1, 1, -1, 1, -1, 1, 1}, {1, 1, -1, 1, 1, -1, -1}, {1,
    1, -1, 1, 1, -1, 1}, {1, 1, -1, 1, 1, 1, -1}, {1, 1, -1, 1, 1, 1, 1}, {1,
    1, 1, -1, -1, -1, -1}, {1, 1, 1, -1, -1, -1, 1}, {1, 1, 1, -1, -1, 1, -1},
    {1, 1, 1, -1, -1, 1, 1}, {1, 1, 1, -1, 1, -1, -1}, {1, 1, 1, -1, 1, -1, 1},
    {1, 1, 1, -1, 1, 1, -1}, {1, 1, 1, -1, 1, 1, 1}, {1, 1, 1, 1, -1, -1, -1},
    {1, 1, 1, 1, -1, -1, 1}, {1, 1, 1, 1, -1, 1, -1}, {1, 1, 1, 1, -1, 1, 1},
    {1, 1, 1, 1, 1, -1, -1}, {1, 1, 1, 1, 1, -1, 1}, {1, 1, 1, 1, 1, 1, -1},
    {1, 1, 1, 1, 1, 1, 1}};

// Normalization factors the various processes
// Denominators: spins, colors and identical particles
int PY8MEs_R16_P49_sm_gb_ttxbbbx::denom_colors[nprocesses] = {24, 24}; 
int PY8MEs_R16_P49_sm_gb_ttxbbbx::denom_hels[nprocesses] = {4, 4}; 
int PY8MEs_R16_P49_sm_gb_ttxbbbx::denom_iden[nprocesses] = {2, 2}; 

//--------------------------------------------------------------------------
// Color config initialization
void PY8MEs_R16_P49_sm_gb_ttxbbbx::initColorConfigs() 
{
  color_configs = vector < vec_vec_int > (); 
  jamp_nc_relative_power = vector < vec_int > (); 

  // Color flows of process Process: g b > t t~ b b b~ WEIGHTED<=5 @16
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(4)(4)(0)(1)(0)(0)(2)(2)(0)(3)(0)(0)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #1
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(4)(4)(0)(1)(0)(0)(3)(2)(0)(3)(0)(0)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #2
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(4)(2)(0)(1)(0)(0)(4)(2)(0)(3)(0)(0)(3)));
  jamp_nc_relative_power[0].push_back(-1); 
  // JAMP #3
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(4)(3)(0)(1)(0)(0)(4)(2)(0)(3)(0)(0)(2)));
  jamp_nc_relative_power[0].push_back(-1); 
  // JAMP #4
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(4)(2)(0)(1)(0)(0)(3)(2)(0)(3)(0)(0)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #5
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(4)(3)(0)(1)(0)(0)(2)(2)(0)(3)(0)(0)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #6
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(4)(4)(0)(1)(0)(0)(1)(2)(0)(3)(0)(0)(3)));
  jamp_nc_relative_power[0].push_back(-1); 
  // JAMP #7
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(4)(4)(0)(1)(0)(0)(3)(2)(0)(3)(0)(0)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #8
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(4)(1)(0)(1)(0)(0)(4)(2)(0)(3)(0)(0)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #9
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(4)(3)(0)(1)(0)(0)(4)(2)(0)(3)(0)(0)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #10
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(4)(1)(0)(1)(0)(0)(3)(2)(0)(3)(0)(0)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #11
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(4)(3)(0)(1)(0)(0)(1)(2)(0)(3)(0)(0)(4)));
  jamp_nc_relative_power[0].push_back(-1); 
  // JAMP #12
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(4)(4)(0)(1)(0)(0)(1)(2)(0)(3)(0)(0)(2)));
  jamp_nc_relative_power[0].push_back(-1); 
  // JAMP #13
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(4)(4)(0)(1)(0)(0)(2)(2)(0)(3)(0)(0)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #14
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(4)(1)(0)(1)(0)(0)(4)(2)(0)(3)(0)(0)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #15
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(4)(2)(0)(1)(0)(0)(4)(2)(0)(3)(0)(0)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #16
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(4)(1)(0)(1)(0)(0)(2)(2)(0)(3)(0)(0)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #17
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(4)(2)(0)(1)(0)(0)(1)(2)(0)(3)(0)(0)(4)));
  jamp_nc_relative_power[0].push_back(-1); 

  // Color flows of process Process: g b~ > t t~ b b~ b~ WEIGHTED<=5 @16
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(4)(0)(1)(2)(0)(0)(4)(3)(0)(0)(2)(0)(3)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #1
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(4)(0)(1)(2)(0)(0)(4)(3)(0)(0)(3)(0)(2)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #2
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(4)(0)(1)(2)(0)(0)(2)(3)(0)(0)(4)(0)(3)));
  jamp_nc_relative_power[1].push_back(-1); 
  // JAMP #3
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(4)(0)(1)(2)(0)(0)(3)(3)(0)(0)(4)(0)(2)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #4
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(4)(0)(1)(2)(0)(0)(2)(3)(0)(0)(3)(0)(4)));
  jamp_nc_relative_power[1].push_back(-1); 
  // JAMP #5
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(4)(0)(1)(2)(0)(0)(3)(3)(0)(0)(2)(0)(4)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #6
  color_configs[1].push_back(vec_int(createvector<int>
      (2)(4)(0)(1)(2)(0)(0)(4)(3)(0)(0)(1)(0)(3)));
  jamp_nc_relative_power[1].push_back(-1); 
  // JAMP #7
  color_configs[1].push_back(vec_int(createvector<int>
      (2)(4)(0)(1)(2)(0)(0)(4)(3)(0)(0)(3)(0)(1)));
  jamp_nc_relative_power[1].push_back(-1); 
  // JAMP #8
  color_configs[1].push_back(vec_int(createvector<int>
      (2)(4)(0)(1)(2)(0)(0)(1)(3)(0)(0)(4)(0)(3)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #9
  color_configs[1].push_back(vec_int(createvector<int>
      (2)(4)(0)(1)(2)(0)(0)(3)(3)(0)(0)(4)(0)(1)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #10
  color_configs[1].push_back(vec_int(createvector<int>
      (2)(4)(0)(1)(2)(0)(0)(1)(3)(0)(0)(3)(0)(4)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #11
  color_configs[1].push_back(vec_int(createvector<int>
      (2)(4)(0)(1)(2)(0)(0)(3)(3)(0)(0)(1)(0)(4)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #12
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(4)(0)(1)(2)(0)(0)(4)(3)(0)(0)(1)(0)(2)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #13
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(4)(0)(1)(2)(0)(0)(4)(3)(0)(0)(2)(0)(1)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #14
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(4)(0)(1)(2)(0)(0)(1)(3)(0)(0)(4)(0)(2)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #15
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(4)(0)(1)(2)(0)(0)(2)(3)(0)(0)(4)(0)(1)));
  jamp_nc_relative_power[1].push_back(-1); 
  // JAMP #16
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(4)(0)(1)(2)(0)(0)(1)(3)(0)(0)(2)(0)(4)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #17
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(4)(0)(1)(2)(0)(0)(2)(3)(0)(0)(1)(0)(4)));
  jamp_nc_relative_power[1].push_back(-1); 
}

//--------------------------------------------------------------------------
// Destructor.
PY8MEs_R16_P49_sm_gb_ttxbbbx::~PY8MEs_R16_P49_sm_gb_ttxbbbx() 
{
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    delete[] p[i]; 
    p[i] = NULL; 
  }
}

//--------------------------------------------------------------------------
// Invert the permutation mapping
vector<int> PY8MEs_R16_P49_sm_gb_ttxbbbx::invert_mapping(vector<int> mapping) 
{
  vector<int> inverted_mapping; 
  for (unsigned int i = 0; i < mapping.size(); i++ )
  {
    for (unsigned int j = 0; j < mapping.size(); j++ )
    {
      if (mapping[j] == ((int)i))
      {
        inverted_mapping.push_back(j); 
        break; 
      }
    }
  }
  return inverted_mapping; 
}

//--------------------------------------------------------------------------
// Return the list of possible helicity configurations
vector < vec_int > PY8MEs_R16_P49_sm_gb_ttxbbbx::getHelicityConfigs(vector<int>
    permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(ncomb, vector<int> (nexternal, 0)); 
  for (unsigned int ihel = 0; ihel < ncomb; ihel++ )
  {
    for(unsigned int j = 0; j < nexternal; j++ )
    {
      res[ihel][chosenPerm[j]] = helicities[ihel][j]; 
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Return the list of possible color configurations
vector < vec_int > PY8MEs_R16_P49_sm_gb_ttxbbbx::getColorConfigs(int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(color_configs[chosenProcID].size(), vector<int>
      (nexternal * 2, 0));
  for (unsigned int icol = 0; icol < color_configs[chosenProcID].size(); icol++
      )
  {
    for(unsigned int j = 0; j < (2 * nexternal); j++ )
    {
      res[icol][chosenPerm[j/2] * 2 + j%2] =
          color_configs[chosenProcID][icol][j];
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Get JAMP relative N_c power
int PY8MEs_R16_P49_sm_gb_ttxbbbx::getColorFlowRelativeNCPower(int
    color_flow_ID, int specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return jamp_nc_relative_power[chosenProcID][color_flow_ID]; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity ID -> Helicity Config
vector<int> PY8MEs_R16_P49_sm_gb_ttxbbbx::getHelicityConfigForID(int hel_ID,
    vector<int> permutation)
{
  if (hel_ID < 0 || hel_ID >= ncomb)
  {
    cerr <<  "Error in function 'getHelicityConfigForID' of class" << 
    " 'PY8MEs_R16_P49_sm_gb_ttxbbbx': Specified helicity ID '" << 
    hel_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R16_P49_sm_gb_ttxbbbx_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(nexternal, 0); 
  for (unsigned int j = 0; j < nexternal; j++ )
  {
    res[chosenPerm[j]] = helicities[hel_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity Config -> Helicity ID
int PY8MEs_R16_P49_sm_gb_ttxbbbx::getHelicityIDForConfig(vector<int>
    hel_config, vector<int> permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  int user_ihel = -1; 
  if (hel_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < ncomb; i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < nexternal; j++ )
      {
        if (helicities[i][chosenPerm[j]] != hel_config[j])
        {
          found = false; 
          break; 
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_ihel = i; 
        break; 
      }
    }
    if (user_ihel == -1)
    {
      cerr <<  "Error in function 'getHelicityIDForConfig' of class" << 
      " 'PY8MEs_R16_P49_sm_gb_ttxbbbx': Specified helicity" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R16_P49_sm_gb_ttxbbbx_exception; 
    }
  }
  return user_ihel; 
}


//--------------------------------------------------------------------------
// Implements the map Color ID -> Color Config
vector<int> PY8MEs_R16_P49_sm_gb_ttxbbbx::getColorConfigForID(int color_ID, int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < 0 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getColorConfigForID' of class" << 
    " 'PY8MEs_R16_P49_sm_gb_ttxbbbx': Specified color ID '" << 
    color_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R16_P49_sm_gb_ttxbbbx_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(color_configs[chosenProcID][color_ID].size(), 0); 
  for (unsigned int j = 0; j < (2 * nexternal); j++ )
  {
    res[chosenPerm[j/2] * 2 + j%2] = color_configs[chosenProcID][color_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Color Config -> Color ID
int PY8MEs_R16_P49_sm_gb_ttxbbbx::getColorIDForConfig(vector<int> color_config,
    int specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = -1; 
  if (color_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < color_configs[chosenProcID].size(); i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < (nexternal * 2); j++ )
      {

        // If colorless then make sure it matches
        // The little arithmetics in the color index is just
        // the permutation applies on the particle list which is
        // twice smaller since each particle can have two color indices.
        if (color_config[j] == 0)
        {
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] != 0)
          {
            found = false; 
            break; 
          }
          // Otherwise check that the color linked position matches
        }
        else
        {
          int color_linked_pos = -1; 
          // Find the other end of the line in the user color config
          for (unsigned int k = 0; k < (nexternal * 2); k++ )
          {
            if (k == j)
              continue; 
            if (color_config[j] == color_config[k])
            {
              color_linked_pos = k; 
              break; 
            }
          }
          if (color_linked_pos == -1)
          {
            cerr <<  "Error in function 'getColorIDForConfig' of class" << 
            " 'PY8MEs_R16_P49_sm_gb_ttxbbbx': A color line could " << 
            " not be closed." << endl; 
            throw PY8MEs_R16_P49_sm_gb_ttxbbbx_exception; 
          }
          // Now check whether the color line matches
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] !=
              color_configs[chosenProcID][i][chosenPerm[color_linked_pos/2] * 2
              + color_linked_pos%2])
          {
            found = false; 
            break; 
          }
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_icol = i; 
        break; 
      }
    }

    if (user_icol == -1)
    {
      cerr <<  "Error in function 'getColorIDForConfig' of class" << 
      " 'PY8MEs_R16_P49_sm_gb_ttxbbbx': Specified color" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R16_P49_sm_gb_ttxbbbx_exception; 
    }
  }
  return user_icol; 
}

//--------------------------------------------------------------------------
// Returns all result previously computed in SigmaKin
vector < vec_double > PY8MEs_R16_P49_sm_gb_ttxbbbx::getAllResults(int
    specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return all_results[chosenProcID]; 
}

//--------------------------------------------------------------------------
// Returns a result previously computed in SigmaKin for a specific helicity
// and color ID. -1 means avg and summed over that characteristic.
double PY8MEs_R16_P49_sm_gb_ttxbbbx::getResult(int helicity_ID, int color_ID,
    int specify_proc_ID)
{
  if (helicity_ID < - 1 || helicity_ID >= ncomb)
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R16_P49_sm_gb_ttxbbbx': Specified helicity ID '" << 
    helicity_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R16_P49_sm_gb_ttxbbbx_exception; 
  }
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < - 1 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R16_P49_sm_gb_ttxbbbx': Specified color ID '" << 
    color_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R16_P49_sm_gb_ttxbbbx_exception; 
  }
  return all_results[chosenProcID][helicity_ID + 1][color_ID + 1]; 
}

//--------------------------------------------------------------------------
// Check for the availability of the requested process and if available,
// If available, this returns the corresponding permutation and Proc_ID to use.
// If not available, this returns a negative Proc_ID.
pair < vector<int> , int > PY8MEs_R16_P49_sm_gb_ttxbbbx::static_getPY8ME(vector<int> initial_pdgs, vector<int> final_pdgs, set<int> schannels) 
{

  // Not available return value
  pair < vector<int> , int > NA(vector<int> (), -1); 

  // Check if s-channel requirements match
  if (nreq_s_channels > 0)
  {
    if (schannels != s_channel_proc)
      return NA; 
  }
  else
  {
    if (schannels.size() != 0)
      return NA; 
  }

  // Check number of final state particles
  if (final_pdgs.size() != (nexternal - ninitial))
    return NA; 

  // Check number of initial state particles
  if (initial_pdgs.size() != ninitial)
    return NA; 

  // List of processes available in this class
  const int nprocs = 4; 
  const int proc_IDS[nprocs] = {0, 1, 0, 1}; 
  const int in_pdgs[nprocs][ninitial] = {{21, 5}, {21, -5}, {5, 21}, {-5, 21}}; 
  const int out_pdgs[nprocs][nexternal - ninitial] = {{6, -6, 5, 5, -5}, {6,
      -6, 5, -5, -5}, {6, -6, 5, 5, -5}, {6, -6, 5, -5, -5}};

  bool in_pdgs_used[ninitial]; 
  bool out_pdgs_used[nexternal - ninitial]; 
  for(unsigned int i = 0; i < nprocs; i++ )
  {
    int permutations[nexternal]; 

    // Reinitialize initial state look-up variables
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      in_pdgs_used[j] = false; 
      permutations[j] = -1; 
    }
    // Look for initial state matches
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      for(unsigned int k = 0; k < ninitial; k++ )
      {
        // Make sure it has not been used already
        if (in_pdgs_used[k])
          continue; 
        if (initial_pdgs[k] == in_pdgs[i][j])
        {
          permutations[j] = k; 
          in_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[ninitial - 1] == -1)
      continue; 

    // Reinitialize final state look-up variables
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      out_pdgs_used[j] = false; 
      permutations[ninitial + j] = -1; 
    }
    // Look for final state matches
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      for(unsigned int k = 0; k < (nexternal - ninitial); k++ )
      {
        // Make sure it has not been used already
        if (out_pdgs_used[k])
          continue; 
        if (final_pdgs[k] == out_pdgs[i][j])
        {
          permutations[ninitial + j] = ninitial + k; 
          out_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[ninitial + j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[nexternal - 1] == -1)
      continue; 

    // Return process found
    return pair < vector<int> , int > (vector<int> (permutations, permutations
        + nexternal), proc_IDS[i]);
  }

  // No process found
  return NA; 
}

//--------------------------------------------------------------------------
// Set momenta
void PY8MEs_R16_P49_sm_gb_ttxbbbx::setMomenta(vector < vec_double >
    momenta_picked)
{
  if (momenta_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setMomenta' of class" << 
    " 'PY8MEs_R16_P49_sm_gb_ttxbbbx': Incorrect number of" << 
    " momenta specified." << endl; 
    throw PY8MEs_R16_P49_sm_gb_ttxbbbx_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    if (momenta_picked[i].size() != 4)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R16_P49_sm_gb_ttxbbbx': Incorrect number of" << 
      " momenta components specified." << endl; 
      throw PY8MEs_R16_P49_sm_gb_ttxbbbx_exception; 
    }
    if (momenta_picked[i][0] < 0.0)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R16_P49_sm_gb_ttxbbbx': A momentum was specified" << 
      " with negative energy. Check conventions." << endl; 
      throw PY8MEs_R16_P49_sm_gb_ttxbbbx_exception; 
    }
    for (unsigned int j = 0; j < 4; j++ )
    {
      p[i][j] = momenta_picked[i][j]; 
    }
  }
}

//--------------------------------------------------------------------------
// Set color configuration to use. An empty vector means sum over all.
void PY8MEs_R16_P49_sm_gb_ttxbbbx::setColors(vector<int> colors_picked)
{
  if (colors_picked.size() == 0)
  {
    user_colors = vector<int> (); 
    return; 
  }
  if (colors_picked.size() != (2 * nexternal))
  {
    cerr <<  "Error in function 'setColors' of class" << 
    " 'PY8MEs_R16_P49_sm_gb_ttxbbbx': Incorrect number" << 
    " of colors specified." << endl; 
    throw PY8MEs_R16_P49_sm_gb_ttxbbbx_exception; 
  }
  user_colors = vector<int> ((2 * nexternal), 0); 
  for(unsigned int i = 0; i < (2 * nexternal); i++ )
  {
    user_colors[i] = colors_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the helicity configuration to use. Am empty vector means sum over all.
void PY8MEs_R16_P49_sm_gb_ttxbbbx::setHelicities(vector<int> helicities_picked) 
{
  if (helicities_picked.size() != nexternal)
  {
    if (helicities_picked.size() == 0)
    {
      user_helicities = vector<int> (); 
      return; 
    }
    cerr <<  "Error in function 'setHelicities' of class" << 
    " 'PY8MEs_R16_P49_sm_gb_ttxbbbx': Incorrect number" << 
    " of helicities specified." << endl; 
    throw PY8MEs_R16_P49_sm_gb_ttxbbbx_exception; 
  }
  user_helicities = vector<int> (nexternal, 0); 
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    user_helicities[i] = helicities_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the permutation to use (will apply to momenta, colors and helicities)
void PY8MEs_R16_P49_sm_gb_ttxbbbx::setPermutation(vector<int> perm_picked) 
{
  if (perm_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setPermutations' of class" << 
    " 'PY8MEs_R16_P49_sm_gb_ttxbbbx': Incorrect number" << 
    " of permutations specified." << endl; 
    throw PY8MEs_R16_P49_sm_gb_ttxbbbx_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    perm[i] = perm_picked[i]; 
  }
}

// Set the proc_ID to use
void PY8MEs_R16_P49_sm_gb_ttxbbbx::setProcID(int procID_picked) 
{
  proc_ID = procID_picked; 
}

//--------------------------------------------------------------------------
// Initialize process.

void PY8MEs_R16_P49_sm_gb_ttxbbbx::initProc() 
{

  // Initialize flags
  include_symmetry_factors = true; 
  include_helicity_averaging_factors = true; 
  include_color_averaging_factors = true; 

  // Initialize vectors.
  perm = vector<int> (nexternal, 0); 
  user_colors = vector<int> (2 * nexternal, 0); 
  user_helicities = vector<int> (nexternal, 0); 
  p = vector < double * > (); 
  for (unsigned int i = 0; i < nexternal; i++ )
  {
    p.push_back(new double[4]); 
  }
  initColorConfigs(); 
  // Synchronize local variables dependent on the model with the active model.
  mME = vector<double> (nexternal, 0.); 
  syncProcModelParams(); 
  jamp2 = vector < vec_double > (2); 
  jamp2[0] = vector<double> (18, 0.); 
  jamp2[1] = vector<double> (18, 0.); 
  all_results = vector < vec_vec_double > (2); 
  // The first entry is always the color or helicity avg/summed matrix element.
  all_results[0] = vector < vec_double > (ncomb + 1, vector<double> (18 + 1,
      0.));
  all_results[1] = vector < vec_double > (ncomb + 1, vector<double> (18 + 1,
      0.));
}

// Synchronize local variables of the process that depend on the model
// parameters
void PY8MEs_R16_P49_sm_gb_ttxbbbx::syncProcModelParams() 
{

  // Instantiate the model class and set parameters that stay fixed during run
  mME[0] = pars->ZERO; 
  mME[1] = pars->mdl_MB; 
  mME[2] = pars->mdl_MT; 
  mME[3] = pars->mdl_MT; 
  mME[4] = pars->mdl_MB; 
  mME[5] = pars->mdl_MB; 
  mME[6] = pars->mdl_MB; 
}

//--------------------------------------------------------------------------
// Setter allowing to force particular values for the external masses
void PY8MEs_R16_P49_sm_gb_ttxbbbx::setMasses(vec_double external_masses) 
{

  if (external_masses.size() != mME.size())
  {
    cerr <<  "Error in function 'setMasses' of class" << 
    " 'PY8MEs_R16_P49_sm_gb_ttxbbbx': Incorrect number of" << 
    " masses specified." << endl; 
    throw PY8MEs_R16_P49_sm_gb_ttxbbbx_exception; 
  }
  for (unsigned int j = 0; j < mME.size(); j++ )
  {
    mME[j] = external_masses[perm[j]]; 
  }
}

//--------------------------------------------------------------------------
// Getter accessing external masses with the correct ordering
vector<double> PY8MEs_R16_P49_sm_gb_ttxbbbx::getMasses() 
{

  vec_double external_masses; 
  vector<int> invertedPerm; 
  invertedPerm = invert_mapping(perm); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    external_masses.push_back(mME[invertedPerm[i]]); 
  }
  return external_masses; 

}


// Set all values of the external masses to float(-mode) where mode can be
// 0 : Mass taken from the model
// 1 : Mass taken from p_i^2 if not massless to begin with
// 2 : Mass always taken from p_i^2.
void PY8MEs_R16_P49_sm_gb_ttxbbbx::setExternalMassesMode(int mode) 
{
  if (mode != 0 && mode != 1 && mode != 2)
  {
    cerr <<  "Error in function 'setExternalMassesMode' of class" << 
    " 'PY8MEs_R16_P49_sm_gb_ttxbbbx': Incorrect mode selected :" << mode << 
    ". It must be either 0, 1 or 2" << endl; 
    throw PY8MEs_R16_P49_sm_gb_ttxbbbx_exception; 
  }
  if (mode == 0)
  {
    syncProcModelParams(); 
  }
  else if (mode == 1)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      if (mME[j] != pars->ZERO)
      {
        mME[j] = -1.0; 
      }
    }
  }
  else if (mode == 2)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      mME[j] = -1.0; 
    }
  }
}

//--------------------------------------------------------------------------
// Evaluate the squared matrix element.

double PY8MEs_R16_P49_sm_gb_ttxbbbx::sigmaKin() 
{
  // Set the parameters which change event by event
  pars->setDependentParameters(); 
  pars->setDependentCouplings(); 
  // Reset color flows
  for(int i = 0; i < 18; i++ )
    jamp2[0][i] = 0.; 
  for(int i = 0; i < 18; i++ )
    jamp2[1][i] = 0.; 

  // Save previous values of mME
  vector<double> saved_mME(mME.size(), 0.0); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (mME[i] < 0.0)
    {
      saved_mME[i] = mME[i]; 
      mME[i] = sqrt(abs(pow(p[perm[i]][0], 2) - 
      (pow(p[perm[i]][1], 2) + pow(p[perm[i]][2], 2) + pow(p[perm[i]][3],
          2))));
    }
  }

  // Local variables and constants
  const int max_tries = 10; 
  vector < vec_bool > goodhel(nprocesses, vec_bool(ncomb, false)); 
  vec_int ntry(nprocesses, 0); 
  double t = 0.; 
  double result = 0.; 

  if (ntry[proc_ID] <= max_tries)
    ntry[proc_ID] = ntry[proc_ID] + 1; 

  // Find which helicity configuration is asked for
  // -1 indicates one wants to sum over helicities
  int user_ihel = getHelicityIDForConfig(user_helicities); 

  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = getColorIDForConfig(user_colors); 

  // Reset the list of results that will be recomputed here
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      all_results[proc_ID][ihel + 1][icolor + 1] = 0.; 
    }
  }

  // Calculate the matrix element for all helicities
  // unless already detected as vanishing
  for(int ihel = 0; ihel < ncomb; ihel++ )
  {
    // Skip helicity if already detected as vanishing
    if ((ntry[proc_ID] >= max_tries) && !goodhel[proc_ID][ihel])
      continue; 

    // Also skip helicity if user asks for a specific one
    if ((ntry[proc_ID] >= max_tries) && user_ihel != -1 && user_ihel != ihel)
      continue; 

    calculate_wavefunctions(helicities[ihel]); 

    // Reset locally computed color flows
    for(int i = 0; i < 18; i++ )
      jamp2[0][i] = 0.; 
    for(int i = 0; i < 18; i++ )
      jamp2[1][i] = 0.; 

    if (proc_ID == 0)
      t = matrix_16_gb_ttxbbbx(); 
    if (proc_ID == 1)
      t = matrix_16_gbx_ttxbbxbx(); 

    // Store which helicities give non-zero result
    if ((ntry[proc_ID] < max_tries) && t != 0. && !goodhel[proc_ID][ihel])
      goodhel[proc_ID][ihel] = true; 

    // Aggregate results
    if (user_ihel == -1 || user_ihel == ihel)
    {
      if (user_icol == -1)
      {
        result = result + t; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][0] += t; 
          for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
          {
            all_results[proc_ID][0][i + 1] += jamp2[proc_ID][i]; 
          }
        }
        all_results[proc_ID][ihel + 1][0] += t; 
        for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
        {
          all_results[proc_ID][ihel + 1][i + 1] += jamp2[proc_ID][i]; 
        }
      }
      else
      {
        result = result + jamp2[proc_ID][user_icol]; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][user_icol + 1] += jamp2[proc_ID][user_icol]; 
        }
        all_results[proc_ID][ihel + 1][user_icol + 1] +=
            jamp2[proc_ID][user_icol];
      }
    }

  }

  // Normalize results with the identical particle factor
  if (include_symmetry_factors)
  {
    result = result/denom_iden[proc_ID]; 
  }
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      if (include_symmetry_factors)
      {
        all_results[proc_ID][ihel + 1][icolor + 1] /= denom_iden[proc_ID]; 
      }
    }
  }


  // Normalize when when summing+averaging over helicity configurations
  if (user_ihel == -1 && include_helicity_averaging_factors)
  {
    result /= denom_hels[proc_ID]; 
    if (user_icol == -1)
    {
      all_results[proc_ID][0][0] /= denom_hels[proc_ID]; 
      for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
      {
        all_results[proc_ID][0][i + 1] /= denom_hels[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][0][user_icol + 1] /= denom_hels[proc_ID]; 
    }
  }

  // Normalize when summing+averaging over color configurations
  if (user_icol == -1 && include_color_averaging_factors)
  {
    result /= denom_colors[proc_ID]; 
    if (user_ihel == -1)
    {
      all_results[proc_ID][0][0] /= denom_colors[proc_ID]; 
      for (unsigned int i = 0; i < ncomb; i++ )
      {
        all_results[proc_ID][i + 1][0] /= denom_colors[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][user_ihel + 1][0] /= denom_colors[proc_ID]; 
    }
  }

  // Reinstate previous values of mME
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (saved_mME[i] < 0.0)
    {
      mME[i] = saved_mME[i]; 
    }
  }

  // Finally return it
  return result; 
}

//==========================================================================
// Private class member functions

//--------------------------------------------------------------------------
// Evaluate |M|^2 for each subprocess

void PY8MEs_R16_P49_sm_gb_ttxbbbx::calculate_wavefunctions(const int hel[])
{
  // Calculate wavefunctions for all processes
  // Calculate all wavefunctions
  vxxxxx(p[perm[0]], mME[0], hel[0], -1, w[0]); 
  ixxxxx(p[perm[1]], mME[1], hel[1], +1, w[1]); 
  oxxxxx(p[perm[2]], mME[2], hel[2], +1, w[2]); 
  ixxxxx(p[perm[3]], mME[3], hel[3], -1, w[3]); 
  oxxxxx(p[perm[4]], mME[4], hel[4], +1, w[4]); 
  oxxxxx(p[perm[5]], mME[5], hel[5], +1, w[5]); 
  ixxxxx(p[perm[6]], mME[6], hel[6], -1, w[6]); 
  FFV1_2(w[1], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[7]); 
  FFV1P0_3(w[3], w[2], pars->GC_11, pars->ZERO, pars->ZERO, w[8]); 
  FFV1P0_3(w[7], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[9]); 
  FFV1_1(w[5], w[8], pars->GC_11, pars->mdl_MB, pars->ZERO, w[10]); 
  FFV1_2(w[6], w[8], pars->GC_11, pars->mdl_MB, pars->ZERO, w[11]); 
  FFV1P0_3(w[7], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[12]); 
  FFV1_1(w[4], w[8], pars->GC_11, pars->mdl_MB, pars->ZERO, w[13]); 
  FFV1P0_3(w[6], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[14]); 
  FFV1_2(w[7], w[8], pars->GC_11, pars->mdl_MB, pars->ZERO, w[15]); 
  FFV1_2(w[7], w[14], pars->GC_11, pars->mdl_MB, pars->ZERO, w[16]); 
  FFV1P0_3(w[6], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[17]); 
  FFV1_2(w[7], w[17], pars->GC_11, pars->mdl_MB, pars->ZERO, w[18]); 
  FFV1_1(w[2], w[14], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[19]); 
  FFV1_2(w[3], w[14], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[20]); 
  FFV1_1(w[2], w[17], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[21]); 
  FFV1_2(w[3], w[17], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[22]); 
  FFV1_1(w[2], w[0], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[23]); 
  FFV1P0_3(w[1], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[24]); 
  FFV1P0_3(w[3], w[23], pars->GC_11, pars->ZERO, pars->ZERO, w[25]); 
  FFV1_1(w[5], w[24], pars->GC_11, pars->mdl_MB, pars->ZERO, w[26]); 
  FFV1_2(w[6], w[24], pars->GC_11, pars->mdl_MB, pars->ZERO, w[27]); 
  FFV1_1(w[23], w[24], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[28]); 
  FFV1_1(w[23], w[17], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[29]); 
  FFV1P0_3(w[1], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[30]); 
  FFV1_1(w[4], w[30], pars->GC_11, pars->mdl_MB, pars->ZERO, w[31]); 
  FFV1_2(w[6], w[30], pars->GC_11, pars->mdl_MB, pars->ZERO, w[32]); 
  FFV1_1(w[23], w[30], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[33]); 
  FFV1_1(w[23], w[14], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[34]); 
  FFV1_2(w[1], w[14], pars->GC_11, pars->mdl_MB, pars->ZERO, w[35]); 
  FFV1_1(w[5], w[14], pars->GC_11, pars->mdl_MB, pars->ZERO, w[36]); 
  FFV1_2(w[1], w[17], pars->GC_11, pars->mdl_MB, pars->ZERO, w[37]); 
  FFV1_1(w[4], w[17], pars->GC_11, pars->mdl_MB, pars->ZERO, w[38]); 
  FFV1_2(w[3], w[0], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[39]); 
  FFV1P0_3(w[39], w[2], pars->GC_11, pars->ZERO, pars->ZERO, w[40]); 
  FFV1_2(w[39], w[24], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[41]); 
  FFV1_2(w[39], w[17], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[42]); 
  FFV1_2(w[39], w[30], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[43]); 
  FFV1_2(w[39], w[14], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[44]); 
  FFV1_1(w[4], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[45]); 
  FFV1P0_3(w[6], w[45], pars->GC_11, pars->ZERO, pars->ZERO, w[46]); 
  FFV1_1(w[2], w[30], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[47]); 
  FFV1_2(w[3], w[30], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[48]); 
  FFV1_1(w[45], w[30], pars->GC_11, pars->mdl_MB, pars->ZERO, w[49]); 
  FFV1_1(w[45], w[8], pars->GC_11, pars->mdl_MB, pars->ZERO, w[50]); 
  FFV1P0_3(w[1], w[45], pars->GC_11, pars->ZERO, pars->ZERO, w[51]); 
  FFV1_2(w[1], w[8], pars->GC_11, pars->mdl_MB, pars->ZERO, w[52]); 
  FFV1_1(w[45], w[17], pars->GC_11, pars->mdl_MB, pars->ZERO, w[53]); 
  FFV1_1(w[5], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[54]); 
  FFV1P0_3(w[6], w[54], pars->GC_11, pars->ZERO, pars->ZERO, w[55]); 
  FFV1_1(w[2], w[24], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[56]); 
  FFV1_2(w[3], w[24], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[57]); 
  FFV1_1(w[54], w[24], pars->GC_11, pars->mdl_MB, pars->ZERO, w[58]); 
  FFV1_1(w[54], w[8], pars->GC_11, pars->mdl_MB, pars->ZERO, w[59]); 
  FFV1P0_3(w[1], w[54], pars->GC_11, pars->ZERO, pars->ZERO, w[60]); 
  FFV1_1(w[54], w[14], pars->GC_11, pars->mdl_MB, pars->ZERO, w[61]); 
  FFV1_2(w[6], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[62]); 
  FFV1P0_3(w[62], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[63]); 
  FFV1_2(w[62], w[24], pars->GC_11, pars->mdl_MB, pars->ZERO, w[64]); 
  FFV1_2(w[62], w[8], pars->GC_11, pars->mdl_MB, pars->ZERO, w[65]); 
  FFV1P0_3(w[62], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[66]); 
  FFV1_2(w[62], w[30], pars->GC_11, pars->mdl_MB, pars->ZERO, w[67]); 
  VVV1P0_1(w[0], w[24], pars->GC_10, pars->ZERO, pars->ZERO, w[68]); 
  VVV1P0_1(w[0], w[8], pars->GC_10, pars->ZERO, pars->ZERO, w[69]); 
  VVV1P0_1(w[0], w[17], pars->GC_10, pars->ZERO, pars->ZERO, w[70]); 
  VVV1P0_1(w[0], w[30], pars->GC_10, pars->ZERO, pars->ZERO, w[71]); 
  VVV1P0_1(w[0], w[14], pars->GC_10, pars->ZERO, pars->ZERO, w[72]); 
  oxxxxx(p[perm[1]], mME[1], hel[1], -1, w[73]); 
  ixxxxx(p[perm[5]], mME[5], hel[5], -1, w[74]); 
  FFV1_2(w[74], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[75]); 
  FFV1P0_3(w[75], w[73], pars->GC_11, pars->ZERO, pars->ZERO, w[76]); 
  FFV1P0_3(w[75], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[77]); 
  FFV1_1(w[73], w[8], pars->GC_11, pars->mdl_MB, pars->ZERO, w[78]); 
  FFV1P0_3(w[6], w[73], pars->GC_11, pars->ZERO, pars->ZERO, w[79]); 
  FFV1_2(w[75], w[8], pars->GC_11, pars->mdl_MB, pars->ZERO, w[80]); 
  FFV1_2(w[75], w[79], pars->GC_11, pars->mdl_MB, pars->ZERO, w[81]); 
  FFV1_2(w[75], w[14], pars->GC_11, pars->mdl_MB, pars->ZERO, w[82]); 
  FFV1_1(w[2], w[79], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[83]); 
  FFV1_2(w[3], w[79], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[84]); 
  FFV1P0_3(w[74], w[73], pars->GC_11, pars->ZERO, pars->ZERO, w[85]); 
  FFV1_1(w[4], w[85], pars->GC_11, pars->mdl_MB, pars->ZERO, w[86]); 
  FFV1_2(w[6], w[85], pars->GC_11, pars->mdl_MB, pars->ZERO, w[87]); 
  FFV1_1(w[23], w[85], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[88]); 
  FFV1P0_3(w[74], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[89]); 
  FFV1_1(w[73], w[89], pars->GC_11, pars->mdl_MB, pars->ZERO, w[90]); 
  FFV1_2(w[6], w[89], pars->GC_11, pars->mdl_MB, pars->ZERO, w[91]); 
  FFV1_1(w[23], w[89], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[92]); 
  FFV1_1(w[23], w[79], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[93]); 
  FFV1_2(w[74], w[79], pars->GC_11, pars->mdl_MB, pars->ZERO, w[94]); 
  FFV1_1(w[4], w[79], pars->GC_11, pars->mdl_MB, pars->ZERO, w[95]); 
  FFV1_2(w[74], w[14], pars->GC_11, pars->mdl_MB, pars->ZERO, w[96]); 
  FFV1_1(w[73], w[14], pars->GC_11, pars->mdl_MB, pars->ZERO, w[97]); 
  FFV1_2(w[39], w[85], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[98]); 
  FFV1_2(w[39], w[89], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[99]); 
  FFV1_2(w[39], w[79], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[100]); 
  FFV1_1(w[73], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[101]); 
  FFV1P0_3(w[6], w[101], pars->GC_11, pars->ZERO, pars->ZERO, w[102]); 
  FFV1_1(w[2], w[89], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[103]); 
  FFV1_2(w[3], w[89], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[104]); 
  FFV1_1(w[101], w[89], pars->GC_11, pars->mdl_MB, pars->ZERO, w[105]); 
  FFV1_1(w[101], w[8], pars->GC_11, pars->mdl_MB, pars->ZERO, w[106]); 
  FFV1P0_3(w[74], w[101], pars->GC_11, pars->ZERO, pars->ZERO, w[107]); 
  FFV1_2(w[74], w[8], pars->GC_11, pars->mdl_MB, pars->ZERO, w[108]); 
  FFV1_1(w[101], w[14], pars->GC_11, pars->mdl_MB, pars->ZERO, w[109]); 
  FFV1_1(w[2], w[85], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[110]); 
  FFV1_2(w[3], w[85], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[111]); 
  FFV1_1(w[45], w[85], pars->GC_11, pars->mdl_MB, pars->ZERO, w[112]); 
  FFV1P0_3(w[74], w[45], pars->GC_11, pars->ZERO, pars->ZERO, w[113]); 
  FFV1_1(w[45], w[79], pars->GC_11, pars->mdl_MB, pars->ZERO, w[114]); 
  FFV1_2(w[62], w[85], pars->GC_11, pars->mdl_MB, pars->ZERO, w[115]); 
  FFV1P0_3(w[62], w[73], pars->GC_11, pars->ZERO, pars->ZERO, w[116]); 
  FFV1_2(w[62], w[89], pars->GC_11, pars->mdl_MB, pars->ZERO, w[117]); 
  VVV1P0_1(w[0], w[85], pars->GC_10, pars->ZERO, pars->ZERO, w[118]); 
  VVV1P0_1(w[0], w[89], pars->GC_10, pars->ZERO, pars->ZERO, w[119]); 
  VVV1P0_1(w[0], w[79], pars->GC_10, pars->ZERO, pars->ZERO, w[120]); 

  // Calculate all amplitudes
  // Amplitude(s) for diagram number 0
  FFV1_0(w[6], w[10], w[9], pars->GC_11, amp[0]); 
  FFV1_0(w[11], w[5], w[9], pars->GC_11, amp[1]); 
  FFV1_0(w[6], w[13], w[12], pars->GC_11, amp[2]); 
  FFV1_0(w[11], w[4], w[12], pars->GC_11, amp[3]); 
  FFV1_0(w[15], w[5], w[14], pars->GC_11, amp[4]); 
  FFV1_0(w[16], w[5], w[8], pars->GC_11, amp[5]); 
  VVV1_0(w[8], w[14], w[12], pars->GC_10, amp[6]); 
  FFV1_0(w[15], w[4], w[17], pars->GC_11, amp[7]); 
  VVV1_0(w[8], w[17], w[9], pars->GC_10, amp[8]); 
  FFV1_0(w[18], w[4], w[8], pars->GC_11, amp[9]); 
  FFV1_0(w[3], w[19], w[12], pars->GC_11, amp[10]); 
  FFV1_0(w[20], w[2], w[12], pars->GC_11, amp[11]); 
  FFV1_0(w[3], w[21], w[9], pars->GC_11, amp[12]); 
  FFV1_0(w[22], w[2], w[9], pars->GC_11, amp[13]); 
  FFV1_0(w[6], w[26], w[25], pars->GC_11, amp[14]); 
  FFV1_0(w[27], w[5], w[25], pars->GC_11, amp[15]); 
  FFV1_0(w[3], w[28], w[17], pars->GC_11, amp[16]); 
  VVV1_0(w[24], w[17], w[25], pars->GC_10, amp[17]); 
  FFV1_0(w[3], w[29], w[24], pars->GC_11, amp[18]); 
  FFV1_0(w[6], w[31], w[25], pars->GC_11, amp[19]); 
  FFV1_0(w[32], w[4], w[25], pars->GC_11, amp[20]); 
  FFV1_0(w[3], w[33], w[14], pars->GC_11, amp[21]); 
  VVV1_0(w[30], w[14], w[25], pars->GC_10, amp[22]); 
  FFV1_0(w[3], w[34], w[30], pars->GC_11, amp[23]); 
  FFV1_0(w[35], w[5], w[25], pars->GC_11, amp[24]); 
  FFV1_0(w[1], w[36], w[25], pars->GC_11, amp[25]); 
  FFV1_0(w[37], w[4], w[25], pars->GC_11, amp[26]); 
  FFV1_0(w[1], w[38], w[25], pars->GC_11, amp[27]); 
  FFV1_0(w[6], w[26], w[40], pars->GC_11, amp[28]); 
  FFV1_0(w[27], w[5], w[40], pars->GC_11, amp[29]); 
  FFV1_0(w[41], w[2], w[17], pars->GC_11, amp[30]); 
  VVV1_0(w[24], w[17], w[40], pars->GC_10, amp[31]); 
  FFV1_0(w[42], w[2], w[24], pars->GC_11, amp[32]); 
  FFV1_0(w[6], w[31], w[40], pars->GC_11, amp[33]); 
  FFV1_0(w[32], w[4], w[40], pars->GC_11, amp[34]); 
  FFV1_0(w[43], w[2], w[14], pars->GC_11, amp[35]); 
  VVV1_0(w[30], w[14], w[40], pars->GC_10, amp[36]); 
  FFV1_0(w[44], w[2], w[30], pars->GC_11, amp[37]); 
  FFV1_0(w[35], w[5], w[40], pars->GC_11, amp[38]); 
  FFV1_0(w[1], w[36], w[40], pars->GC_11, amp[39]); 
  FFV1_0(w[37], w[4], w[40], pars->GC_11, amp[40]); 
  FFV1_0(w[1], w[38], w[40], pars->GC_11, amp[41]); 
  FFV1_0(w[3], w[47], w[46], pars->GC_11, amp[42]); 
  FFV1_0(w[48], w[2], w[46], pars->GC_11, amp[43]); 
  FFV1_0(w[6], w[49], w[8], pars->GC_11, amp[44]); 
  FFV1_0(w[6], w[50], w[30], pars->GC_11, amp[45]); 
  VVV1_0(w[30], w[8], w[46], pars->GC_10, amp[46]); 
  FFV1_0(w[6], w[10], w[51], pars->GC_11, amp[47]); 
  FFV1_0(w[11], w[5], w[51], pars->GC_11, amp[48]); 
  FFV1_0(w[52], w[5], w[46], pars->GC_11, amp[49]); 
  FFV1_0(w[1], w[10], w[46], pars->GC_11, amp[50]); 
  VVV1_0(w[8], w[17], w[51], pars->GC_10, amp[51]); 
  FFV1_0(w[1], w[50], w[17], pars->GC_11, amp[52]); 
  FFV1_0(w[1], w[53], w[8], pars->GC_11, amp[53]); 
  FFV1_0(w[3], w[21], w[51], pars->GC_11, amp[54]); 
  FFV1_0(w[22], w[2], w[51], pars->GC_11, amp[55]); 
  FFV1_0(w[3], w[56], w[55], pars->GC_11, amp[56]); 
  FFV1_0(w[57], w[2], w[55], pars->GC_11, amp[57]); 
  FFV1_0(w[6], w[58], w[8], pars->GC_11, amp[58]); 
  FFV1_0(w[6], w[59], w[24], pars->GC_11, amp[59]); 
  VVV1_0(w[24], w[8], w[55], pars->GC_10, amp[60]); 
  FFV1_0(w[6], w[13], w[60], pars->GC_11, amp[61]); 
  FFV1_0(w[11], w[4], w[60], pars->GC_11, amp[62]); 
  FFV1_0(w[52], w[4], w[55], pars->GC_11, amp[63]); 
  FFV1_0(w[1], w[13], w[55], pars->GC_11, amp[64]); 
  VVV1_0(w[8], w[14], w[60], pars->GC_10, amp[65]); 
  FFV1_0(w[1], w[59], w[14], pars->GC_11, amp[66]); 
  FFV1_0(w[1], w[61], w[8], pars->GC_11, amp[67]); 
  FFV1_0(w[3], w[19], w[60], pars->GC_11, amp[68]); 
  FFV1_0(w[20], w[2], w[60], pars->GC_11, amp[69]); 
  FFV1_0(w[3], w[56], w[63], pars->GC_11, amp[70]); 
  FFV1_0(w[57], w[2], w[63], pars->GC_11, amp[71]); 
  FFV1_0(w[64], w[5], w[8], pars->GC_11, amp[72]); 
  FFV1_0(w[65], w[5], w[24], pars->GC_11, amp[73]); 
  VVV1_0(w[24], w[8], w[63], pars->GC_10, amp[74]); 
  FFV1_0(w[3], w[47], w[66], pars->GC_11, amp[75]); 
  FFV1_0(w[48], w[2], w[66], pars->GC_11, amp[76]); 
  FFV1_0(w[67], w[4], w[8], pars->GC_11, amp[77]); 
  FFV1_0(w[65], w[4], w[30], pars->GC_11, amp[78]); 
  VVV1_0(w[30], w[8], w[66], pars->GC_10, amp[79]); 
  FFV1_0(w[52], w[5], w[66], pars->GC_11, amp[80]); 
  FFV1_0(w[1], w[10], w[66], pars->GC_11, amp[81]); 
  FFV1_0(w[52], w[4], w[63], pars->GC_11, amp[82]); 
  FFV1_0(w[1], w[13], w[63], pars->GC_11, amp[83]); 
  FFV1_0(w[6], w[10], w[68], pars->GC_11, amp[84]); 
  FFV1_0(w[11], w[5], w[68], pars->GC_11, amp[85]); 
  FFV1_0(w[6], w[26], w[69], pars->GC_11, amp[86]); 
  FFV1_0(w[27], w[5], w[69], pars->GC_11, amp[87]); 
  FFV1_0(w[11], w[26], w[0], pars->GC_11, amp[88]); 
  FFV1_0(w[27], w[10], w[0], pars->GC_11, amp[89]); 
  VVVV1_0(w[0], w[24], w[8], w[17], pars->GC_12, amp[90]); 
  VVVV3_0(w[0], w[24], w[8], w[17], pars->GC_12, amp[91]); 
  VVVV4_0(w[0], w[24], w[8], w[17], pars->GC_12, amp[92]); 
  VVV1_0(w[8], w[17], w[68], pars->GC_10, amp[93]); 
  VVV1_0(w[24], w[17], w[69], pars->GC_10, amp[94]); 
  VVV1_0(w[24], w[8], w[70], pars->GC_10, amp[95]); 
  FFV1_0(w[3], w[21], w[68], pars->GC_11, amp[96]); 
  FFV1_0(w[22], w[2], w[68], pars->GC_11, amp[97]); 
  FFV1_0(w[3], w[56], w[70], pars->GC_11, amp[98]); 
  FFV1_0(w[57], w[2], w[70], pars->GC_11, amp[99]); 
  FFV1_0(w[22], w[56], w[0], pars->GC_11, amp[100]); 
  FFV1_0(w[57], w[21], w[0], pars->GC_11, amp[101]); 
  FFV1_0(w[6], w[13], w[71], pars->GC_11, amp[102]); 
  FFV1_0(w[11], w[4], w[71], pars->GC_11, amp[103]); 
  FFV1_0(w[6], w[31], w[69], pars->GC_11, amp[104]); 
  FFV1_0(w[32], w[4], w[69], pars->GC_11, amp[105]); 
  FFV1_0(w[11], w[31], w[0], pars->GC_11, amp[106]); 
  FFV1_0(w[32], w[13], w[0], pars->GC_11, amp[107]); 
  VVVV1_0(w[0], w[30], w[8], w[14], pars->GC_12, amp[108]); 
  VVVV3_0(w[0], w[30], w[8], w[14], pars->GC_12, amp[109]); 
  VVVV4_0(w[0], w[30], w[8], w[14], pars->GC_12, amp[110]); 
  VVV1_0(w[8], w[14], w[71], pars->GC_10, amp[111]); 
  VVV1_0(w[30], w[14], w[69], pars->GC_10, amp[112]); 
  VVV1_0(w[30], w[8], w[72], pars->GC_10, amp[113]); 
  FFV1_0(w[3], w[19], w[71], pars->GC_11, amp[114]); 
  FFV1_0(w[20], w[2], w[71], pars->GC_11, amp[115]); 
  FFV1_0(w[3], w[47], w[72], pars->GC_11, amp[116]); 
  FFV1_0(w[48], w[2], w[72], pars->GC_11, amp[117]); 
  FFV1_0(w[20], w[47], w[0], pars->GC_11, amp[118]); 
  FFV1_0(w[48], w[19], w[0], pars->GC_11, amp[119]); 
  FFV1_0(w[35], w[5], w[69], pars->GC_11, amp[120]); 
  FFV1_0(w[1], w[36], w[69], pars->GC_11, amp[121]); 
  FFV1_0(w[52], w[5], w[72], pars->GC_11, amp[122]); 
  FFV1_0(w[1], w[10], w[72], pars->GC_11, amp[123]); 
  FFV1_0(w[52], w[36], w[0], pars->GC_11, amp[124]); 
  FFV1_0(w[35], w[10], w[0], pars->GC_11, amp[125]); 
  FFV1_0(w[37], w[4], w[69], pars->GC_11, amp[126]); 
  FFV1_0(w[1], w[38], w[69], pars->GC_11, amp[127]); 
  FFV1_0(w[52], w[4], w[70], pars->GC_11, amp[128]); 
  FFV1_0(w[1], w[13], w[70], pars->GC_11, amp[129]); 
  FFV1_0(w[52], w[38], w[0], pars->GC_11, amp[130]); 
  FFV1_0(w[37], w[13], w[0], pars->GC_11, amp[131]); 
  FFV1_0(w[6], w[13], w[76], pars->GC_11, amp[132]); 
  FFV1_0(w[11], w[4], w[76], pars->GC_11, amp[133]); 
  FFV1_0(w[6], w[78], w[77], pars->GC_11, amp[134]); 
  FFV1_0(w[11], w[73], w[77], pars->GC_11, amp[135]); 
  FFV1_0(w[80], w[4], w[79], pars->GC_11, amp[136]); 
  FFV1_0(w[81], w[4], w[8], pars->GC_11, amp[137]); 
  VVV1_0(w[8], w[79], w[77], pars->GC_10, amp[138]); 
  FFV1_0(w[80], w[73], w[14], pars->GC_11, amp[139]); 
  VVV1_0(w[8], w[14], w[76], pars->GC_10, amp[140]); 
  FFV1_0(w[82], w[73], w[8], pars->GC_11, amp[141]); 
  FFV1_0(w[3], w[83], w[77], pars->GC_11, amp[142]); 
  FFV1_0(w[84], w[2], w[77], pars->GC_11, amp[143]); 
  FFV1_0(w[3], w[19], w[76], pars->GC_11, amp[144]); 
  FFV1_0(w[20], w[2], w[76], pars->GC_11, amp[145]); 
  FFV1_0(w[6], w[86], w[25], pars->GC_11, amp[146]); 
  FFV1_0(w[87], w[4], w[25], pars->GC_11, amp[147]); 
  FFV1_0(w[3], w[88], w[14], pars->GC_11, amp[148]); 
  VVV1_0(w[85], w[14], w[25], pars->GC_10, amp[149]); 
  FFV1_0(w[3], w[34], w[85], pars->GC_11, amp[150]); 
  FFV1_0(w[6], w[90], w[25], pars->GC_11, amp[151]); 
  FFV1_0(w[91], w[73], w[25], pars->GC_11, amp[152]); 
  FFV1_0(w[3], w[92], w[79], pars->GC_11, amp[153]); 
  VVV1_0(w[89], w[79], w[25], pars->GC_10, amp[154]); 
  FFV1_0(w[3], w[93], w[89], pars->GC_11, amp[155]); 
  FFV1_0(w[94], w[4], w[25], pars->GC_11, amp[156]); 
  FFV1_0(w[74], w[95], w[25], pars->GC_11, amp[157]); 
  FFV1_0(w[96], w[73], w[25], pars->GC_11, amp[158]); 
  FFV1_0(w[74], w[97], w[25], pars->GC_11, amp[159]); 
  FFV1_0(w[6], w[86], w[40], pars->GC_11, amp[160]); 
  FFV1_0(w[87], w[4], w[40], pars->GC_11, amp[161]); 
  FFV1_0(w[98], w[2], w[14], pars->GC_11, amp[162]); 
  VVV1_0(w[85], w[14], w[40], pars->GC_10, amp[163]); 
  FFV1_0(w[44], w[2], w[85], pars->GC_11, amp[164]); 
  FFV1_0(w[6], w[90], w[40], pars->GC_11, amp[165]); 
  FFV1_0(w[91], w[73], w[40], pars->GC_11, amp[166]); 
  FFV1_0(w[99], w[2], w[79], pars->GC_11, amp[167]); 
  VVV1_0(w[89], w[79], w[40], pars->GC_10, amp[168]); 
  FFV1_0(w[100], w[2], w[89], pars->GC_11, amp[169]); 
  FFV1_0(w[94], w[4], w[40], pars->GC_11, amp[170]); 
  FFV1_0(w[74], w[95], w[40], pars->GC_11, amp[171]); 
  FFV1_0(w[96], w[73], w[40], pars->GC_11, amp[172]); 
  FFV1_0(w[74], w[97], w[40], pars->GC_11, amp[173]); 
  FFV1_0(w[3], w[103], w[102], pars->GC_11, amp[174]); 
  FFV1_0(w[104], w[2], w[102], pars->GC_11, amp[175]); 
  FFV1_0(w[6], w[105], w[8], pars->GC_11, amp[176]); 
  FFV1_0(w[6], w[106], w[89], pars->GC_11, amp[177]); 
  VVV1_0(w[89], w[8], w[102], pars->GC_10, amp[178]); 
  FFV1_0(w[6], w[13], w[107], pars->GC_11, amp[179]); 
  FFV1_0(w[11], w[4], w[107], pars->GC_11, amp[180]); 
  FFV1_0(w[108], w[4], w[102], pars->GC_11, amp[181]); 
  FFV1_0(w[74], w[13], w[102], pars->GC_11, amp[182]); 
  VVV1_0(w[8], w[14], w[107], pars->GC_10, amp[183]); 
  FFV1_0(w[74], w[106], w[14], pars->GC_11, amp[184]); 
  FFV1_0(w[74], w[109], w[8], pars->GC_11, amp[185]); 
  FFV1_0(w[3], w[19], w[107], pars->GC_11, amp[186]); 
  FFV1_0(w[20], w[2], w[107], pars->GC_11, amp[187]); 
  FFV1_0(w[3], w[110], w[46], pars->GC_11, amp[188]); 
  FFV1_0(w[111], w[2], w[46], pars->GC_11, amp[189]); 
  FFV1_0(w[6], w[112], w[8], pars->GC_11, amp[190]); 
  FFV1_0(w[6], w[50], w[85], pars->GC_11, amp[191]); 
  VVV1_0(w[85], w[8], w[46], pars->GC_10, amp[192]); 
  FFV1_0(w[6], w[78], w[113], pars->GC_11, amp[193]); 
  FFV1_0(w[11], w[73], w[113], pars->GC_11, amp[194]); 
  FFV1_0(w[108], w[73], w[46], pars->GC_11, amp[195]); 
  FFV1_0(w[74], w[78], w[46], pars->GC_11, amp[196]); 
  VVV1_0(w[8], w[79], w[113], pars->GC_10, amp[197]); 
  FFV1_0(w[74], w[50], w[79], pars->GC_11, amp[198]); 
  FFV1_0(w[74], w[114], w[8], pars->GC_11, amp[199]); 
  FFV1_0(w[3], w[83], w[113], pars->GC_11, amp[200]); 
  FFV1_0(w[84], w[2], w[113], pars->GC_11, amp[201]); 
  FFV1_0(w[3], w[110], w[66], pars->GC_11, amp[202]); 
  FFV1_0(w[111], w[2], w[66], pars->GC_11, amp[203]); 
  FFV1_0(w[115], w[4], w[8], pars->GC_11, amp[204]); 
  FFV1_0(w[65], w[4], w[85], pars->GC_11, amp[205]); 
  VVV1_0(w[85], w[8], w[66], pars->GC_10, amp[206]); 
  FFV1_0(w[3], w[103], w[116], pars->GC_11, amp[207]); 
  FFV1_0(w[104], w[2], w[116], pars->GC_11, amp[208]); 
  FFV1_0(w[117], w[73], w[8], pars->GC_11, amp[209]); 
  FFV1_0(w[65], w[73], w[89], pars->GC_11, amp[210]); 
  VVV1_0(w[89], w[8], w[116], pars->GC_10, amp[211]); 
  FFV1_0(w[108], w[4], w[116], pars->GC_11, amp[212]); 
  FFV1_0(w[74], w[13], w[116], pars->GC_11, amp[213]); 
  FFV1_0(w[108], w[73], w[66], pars->GC_11, amp[214]); 
  FFV1_0(w[74], w[78], w[66], pars->GC_11, amp[215]); 
  FFV1_0(w[6], w[13], w[118], pars->GC_11, amp[216]); 
  FFV1_0(w[11], w[4], w[118], pars->GC_11, amp[217]); 
  FFV1_0(w[6], w[86], w[69], pars->GC_11, amp[218]); 
  FFV1_0(w[87], w[4], w[69], pars->GC_11, amp[219]); 
  FFV1_0(w[11], w[86], w[0], pars->GC_11, amp[220]); 
  FFV1_0(w[87], w[13], w[0], pars->GC_11, amp[221]); 
  VVVV1_0(w[0], w[85], w[8], w[14], pars->GC_12, amp[222]); 
  VVVV3_0(w[0], w[85], w[8], w[14], pars->GC_12, amp[223]); 
  VVVV4_0(w[0], w[85], w[8], w[14], pars->GC_12, amp[224]); 
  VVV1_0(w[8], w[14], w[118], pars->GC_10, amp[225]); 
  VVV1_0(w[85], w[14], w[69], pars->GC_10, amp[226]); 
  VVV1_0(w[85], w[8], w[72], pars->GC_10, amp[227]); 
  FFV1_0(w[3], w[19], w[118], pars->GC_11, amp[228]); 
  FFV1_0(w[20], w[2], w[118], pars->GC_11, amp[229]); 
  FFV1_0(w[3], w[110], w[72], pars->GC_11, amp[230]); 
  FFV1_0(w[111], w[2], w[72], pars->GC_11, amp[231]); 
  FFV1_0(w[20], w[110], w[0], pars->GC_11, amp[232]); 
  FFV1_0(w[111], w[19], w[0], pars->GC_11, amp[233]); 
  FFV1_0(w[6], w[78], w[119], pars->GC_11, amp[234]); 
  FFV1_0(w[11], w[73], w[119], pars->GC_11, amp[235]); 
  FFV1_0(w[6], w[90], w[69], pars->GC_11, amp[236]); 
  FFV1_0(w[91], w[73], w[69], pars->GC_11, amp[237]); 
  FFV1_0(w[11], w[90], w[0], pars->GC_11, amp[238]); 
  FFV1_0(w[91], w[78], w[0], pars->GC_11, amp[239]); 
  VVVV1_0(w[0], w[89], w[8], w[79], pars->GC_12, amp[240]); 
  VVVV3_0(w[0], w[89], w[8], w[79], pars->GC_12, amp[241]); 
  VVVV4_0(w[0], w[89], w[8], w[79], pars->GC_12, amp[242]); 
  VVV1_0(w[8], w[79], w[119], pars->GC_10, amp[243]); 
  VVV1_0(w[89], w[79], w[69], pars->GC_10, amp[244]); 
  VVV1_0(w[89], w[8], w[120], pars->GC_10, amp[245]); 
  FFV1_0(w[3], w[83], w[119], pars->GC_11, amp[246]); 
  FFV1_0(w[84], w[2], w[119], pars->GC_11, amp[247]); 
  FFV1_0(w[3], w[103], w[120], pars->GC_11, amp[248]); 
  FFV1_0(w[104], w[2], w[120], pars->GC_11, amp[249]); 
  FFV1_0(w[84], w[103], w[0], pars->GC_11, amp[250]); 
  FFV1_0(w[104], w[83], w[0], pars->GC_11, amp[251]); 
  FFV1_0(w[94], w[4], w[69], pars->GC_11, amp[252]); 
  FFV1_0(w[74], w[95], w[69], pars->GC_11, amp[253]); 
  FFV1_0(w[108], w[4], w[120], pars->GC_11, amp[254]); 
  FFV1_0(w[74], w[13], w[120], pars->GC_11, amp[255]); 
  FFV1_0(w[108], w[95], w[0], pars->GC_11, amp[256]); 
  FFV1_0(w[94], w[13], w[0], pars->GC_11, amp[257]); 
  FFV1_0(w[96], w[73], w[69], pars->GC_11, amp[258]); 
  FFV1_0(w[74], w[97], w[69], pars->GC_11, amp[259]); 
  FFV1_0(w[108], w[73], w[72], pars->GC_11, amp[260]); 
  FFV1_0(w[74], w[78], w[72], pars->GC_11, amp[261]); 
  FFV1_0(w[108], w[97], w[0], pars->GC_11, amp[262]); 
  FFV1_0(w[96], w[78], w[0], pars->GC_11, amp[263]); 


}
double PY8MEs_R16_P49_sm_gb_ttxbbbx::matrix_16_gb_ttxbbbx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 132;
  const int ncolor = 18; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
      1, 1, 1, 1, 1};
  static const double cf[ncolor][ncolor] = {{36, 12, 12, 4, 4, 12, 12, 4, 0, 0,
      0, 4, 4, 12, 0, 4, 0, 0}, {12, 36, 4, 12, 12, 4, 4, 12, 0, 4, 0, 0, 12,
      4, 0, 0, 0, 4}, {12, 4, 36, 12, 12, 4, 0, 0, 12, 4, 4, 0, 0, 4, 4, 12, 0,
      0}, {4, 12, 12, 36, 4, 12, 0, 4, 4, 12, 0, 0, 0, 0, 12, 4, 4, 0}, {4, 12,
      12, 4, 36, 12, 0, 0, 4, 0, 12, 4, 4, 0, 0, 0, 4, 12}, {12, 4, 4, 12, 12,
      36, 4, 0, 0, 0, 4, 12, 0, 0, 4, 0, 12, 4}, {12, 4, 0, 0, 0, 4, 36, 12,
      12, 4, 4, 12, 12, 4, 4, 0, 0, 0}, {4, 12, 0, 4, 0, 0, 12, 36, 4, 12, 12,
      4, 4, 12, 0, 0, 4, 0}, {0, 0, 12, 4, 4, 0, 12, 4, 36, 12, 12, 4, 4, 0,
      12, 4, 0, 0}, {0, 4, 4, 12, 0, 0, 4, 12, 12, 36, 4, 12, 0, 0, 4, 12, 0,
      4}, {0, 0, 4, 0, 12, 4, 4, 12, 12, 4, 36, 12, 0, 4, 0, 0, 12, 4}, {4, 0,
      0, 0, 4, 12, 12, 4, 4, 12, 12, 36, 0, 0, 0, 4, 4, 12}, {4, 12, 0, 0, 4,
      0, 12, 4, 4, 0, 0, 0, 36, 12, 12, 4, 4, 12}, {12, 4, 4, 0, 0, 0, 4, 12,
      0, 0, 4, 0, 12, 36, 4, 12, 12, 4}, {0, 0, 4, 12, 0, 4, 4, 0, 12, 4, 0, 0,
      12, 4, 36, 12, 12, 4}, {4, 0, 12, 4, 0, 0, 0, 0, 4, 12, 0, 4, 4, 12, 12,
      36, 4, 12}, {0, 0, 0, 4, 4, 12, 0, 4, 0, 0, 12, 4, 4, 12, 12, 4, 36, 12},
      {0, 4, 0, 0, 12, 4, 0, 0, 0, 4, 4, 12, 12, 4, 4, 12, 12, 36}};

  // Calculate color flows
  jamp[0] = +1./4. * (-amp[2] - amp[4] - Complex<double> (0, 1) * amp[6] -
      1./3. * amp[7] - 1./3. * amp[9] - amp[11] - 1./3. * amp[12] - 1./3. *
      amp[13] - 1./3. * amp[16] - 1./3. * amp[18] - amp[20] - amp[21] +
      Complex<double> (0, 1) * amp[22] - amp[25] - 1./3. * amp[26] - 1./3. *
      amp[27] + 1./3. * Complex<double> (0, 1) * amp[96] + 1./3. *
      Complex<double> (0, 1) * amp[97] - 1./3. * amp[101] + Complex<double> (0,
      1) * amp[102] - Complex<double> (0, 1) * amp[105] - amp[107] - amp[109] -
      amp[108] - amp[111] - amp[112] + Complex<double> (0, 1) * amp[115] -
      Complex<double> (0, 1) * amp[121] - 1./3. * Complex<double> (0, 1) *
      amp[126] - 1./3. * Complex<double> (0, 1) * amp[127] - 1./3. * amp[131]);
  jamp[1] = +1./4. * (+amp[0] + 1./3. * amp[4] + 1./3. * amp[5] + amp[7] +
      Complex<double> (0, 1) * amp[8] + 1./3. * amp[10] + 1./3. * amp[11] +
      amp[13] + amp[15] + amp[16] - Complex<double> (0, 1) * amp[17] + 1./3. *
      amp[21] + 1./3. * amp[23] + 1./3. * amp[24] + 1./3. * amp[25] + amp[27] -
      Complex<double> (0, 1) * amp[84] + Complex<double> (0, 1) * amp[87] +
      amp[89] + amp[91] + amp[90] + amp[93] + amp[94] - Complex<double> (0, 1)
      * amp[97] - 1./3. * Complex<double> (0, 1) * amp[114] - 1./3. *
      Complex<double> (0, 1) * amp[115] + 1./3. * amp[119] + 1./3. *
      Complex<double> (0, 1) * amp[120] + 1./3. * Complex<double> (0, 1) *
      amp[121] + 1./3. * amp[125] + Complex<double> (0, 1) * amp[127]);
  jamp[2] = +1./4. * (+1./9. * amp[14] + 1./9. * amp[15] + 1./9. * amp[16] +
      1./9. * amp[18] + 1./3. * amp[19] + 1./3. * amp[20] + 1./3. * amp[24] +
      1./3. * amp[25] + 1./9. * amp[26] + 1./9. * amp[27] + 1./9. * amp[28] +
      1./9. * amp[29] + 1./9. * amp[30] + 1./9. * amp[32] + 1./3. * amp[33] +
      1./3. * amp[34] + 1./3. * amp[38] + 1./3. * amp[39] + 1./9. * amp[40] +
      1./9. * amp[41] + 1./9. * amp[100] + 1./9. * amp[101]);
  jamp[3] = +1./4. * (-1./3. * amp[14] - 1./3. * amp[15] - 1./9. * amp[19] -
      1./9. * amp[20] - 1./9. * amp[21] - 1./9. * amp[23] - 1./9. * amp[24] -
      1./9. * amp[25] - 1./3. * amp[26] - 1./3. * amp[27] - 1./3. * amp[28] -
      1./3. * amp[29] - 1./9. * amp[33] - 1./9. * amp[34] - 1./9. * amp[35] -
      1./9. * amp[37] - 1./9. * amp[38] - 1./9. * amp[39] - 1./3. * amp[40] -
      1./3. * amp[41] - 1./9. * amp[118] - 1./9. * amp[119]);
  jamp[4] = +1./4. * (-1./3. * amp[14] - 1./3. * amp[15] - 1./3. * amp[16] -
      1./3. * amp[18] - amp[19] - Complex<double> (0, 1) * amp[22] - amp[23] -
      amp[24] - 1./3. * amp[70] - 1./3. * amp[71] - 1./3. * amp[72] - 1./3. *
      amp[73] - amp[76] - amp[78] + Complex<double> (0, 1) * amp[79] - amp[81]
      - 1./3. * Complex<double> (0, 1) * amp[86] - 1./3. * Complex<double> (0,
      1) * amp[87] - 1./3. * amp[89] + 1./3. * Complex<double> (0, 1) * amp[98]
      + 1./3. * Complex<double> (0, 1) * amp[99] - 1./3. * amp[100] -
      Complex<double> (0, 1) * amp[104] + amp[110] + amp[109] + amp[112] +
      amp[113] + Complex<double> (0, 1) * amp[117] - Complex<double> (0, 1) *
      amp[120] + Complex<double> (0, 1) * amp[123] - amp[125]);
  jamp[5] = +1./4. * (+amp[14] + Complex<double> (0, 1) * amp[17] + amp[18] +
      1./3. * amp[19] + 1./3. * amp[20] + 1./3. * amp[21] + 1./3. * amp[23] +
      amp[26] + amp[71] + amp[73] - Complex<double> (0, 1) * amp[74] + 1./3. *
      amp[75] + 1./3. * amp[76] + 1./3. * amp[77] + 1./3. * amp[78] + amp[83] +
      Complex<double> (0, 1) * amp[86] - amp[92] - amp[91] - amp[94] - amp[95]
      - Complex<double> (0, 1) * amp[99] + 1./3. * Complex<double> (0, 1) *
      amp[104] + 1./3. * Complex<double> (0, 1) * amp[105] + 1./3. * amp[107] -
      1./3. * Complex<double> (0, 1) * amp[116] - 1./3. * Complex<double> (0,
      1) * amp[117] + 1./3. * amp[118] + Complex<double> (0, 1) * amp[126] -
      Complex<double> (0, 1) * amp[129] + amp[131]);
  jamp[6] = +1./4. * (+1./9. * amp[0] + 1./9. * amp[1] + 1./3. * amp[2] + 1./3.
      * amp[3] + 1./3. * amp[4] + 1./3. * amp[5] + 1./9. * amp[7] + 1./9. *
      amp[9] + 1./9. * amp[12] + 1./9. * amp[13] + 1./3. * amp[44] + 1./3. *
      amp[45] + 1./9. * amp[47] + 1./9. * amp[48] + 1./3. * amp[49] + 1./3. *
      amp[50] + 1./9. * amp[52] + 1./9. * amp[53] + 1./9. * amp[54] + 1./9. *
      amp[55] - 1./3. * Complex<double> (0, 1) * amp[102] - 1./3. *
      Complex<double> (0, 1) * amp[103] + 1./3. * amp[107] + 1./3. *
      Complex<double> (0, 1) * amp[122] + 1./3. * Complex<double> (0, 1) *
      amp[123] + 1./3. * amp[124] + 1./9. * amp[130] + 1./9. * amp[131]);
  jamp[7] = +1./4. * (-1./3. * amp[0] - 1./3. * amp[1] - amp[3] - amp[5] +
      Complex<double> (0, 1) * amp[6] - amp[10] - 1./3. * amp[12] - 1./3. *
      amp[13] - amp[43] - amp[44] + Complex<double> (0, 1) * amp[46] - 1./3. *
      amp[47] - 1./3. * amp[48] - amp[50] - 1./3. * amp[54] - 1./3. * amp[55] +
      Complex<double> (0, 1) * amp[103] - amp[110] + amp[108] + amp[111] -
      amp[113] + Complex<double> (0, 1) * amp[114] - Complex<double> (0, 1) *
      amp[117] - amp[119] - Complex<double> (0, 1) * amp[123]);
  jamp[8] = +1./4. * (-1./3. * amp[30] - 1./3. * amp[32] - amp[34] +
      Complex<double> (0, 1) * amp[36] - amp[37] - amp[39] - 1./3. * amp[40] -
      1./3. * amp[41] - amp[42] - amp[45] - Complex<double> (0, 1) * amp[46] -
      amp[49] - 1./3. * amp[52] - 1./3. * amp[53] - 1./3. * amp[54] - 1./3. *
      amp[55] - 1./3. * Complex<double> (0, 1) * amp[96] - 1./3. *
      Complex<double> (0, 1) * amp[97] - 1./3. * amp[100] + Complex<double> (0,
      1) * amp[105] + amp[110] + amp[109] + amp[112] + amp[113] -
      Complex<double> (0, 1) * amp[116] + Complex<double> (0, 1) * amp[121] -
      Complex<double> (0, 1) * amp[122] - amp[124] + 1./3. * Complex<double>
      (0, 1) * amp[126] + 1./3. * Complex<double> (0, 1) * amp[127] - 1./3. *
      amp[130]);
  jamp[9] = +1./4. * (+amp[28] + amp[30] + Complex<double> (0, 1) * amp[31] +
      1./3. * amp[33] + 1./3. * amp[34] + 1./3. * amp[35] + 1./3. * amp[37] +
      amp[40] + 1./3. * amp[42] + 1./3. * amp[43] + 1./3. * amp[44] + 1./3. *
      amp[45] + amp[48] - Complex<double> (0, 1) * amp[51] + amp[52] + amp[54]
      + Complex<double> (0, 1) * amp[85] - Complex<double> (0, 1) * amp[86] +
      amp[88] + amp[91] + amp[90] + amp[93] + amp[94] + Complex<double> (0, 1)
      * amp[96] - 1./3. * Complex<double> (0, 1) * amp[104] - 1./3. *
      Complex<double> (0, 1) * amp[105] + 1./3. * amp[106] + 1./3. *
      Complex<double> (0, 1) * amp[116] + 1./3. * Complex<double> (0, 1) *
      amp[117] + 1./3. * amp[119] - Complex<double> (0, 1) * amp[126]);
  jamp[10] = +1./4. * (+1./3. * amp[42] + 1./3. * amp[43] + amp[47] + 1./3. *
      amp[49] + 1./3. * amp[50] + Complex<double> (0, 1) * amp[51] + amp[53] +
      amp[55] + amp[70] + amp[72] + Complex<double> (0, 1) * amp[74] + 1./3. *
      amp[75] + 1./3. * amp[76] + 1./3. * amp[80] + 1./3. * amp[81] + amp[82] +
      Complex<double> (0, 1) * amp[84] + amp[92] - amp[90] - amp[93] + amp[95]
      + Complex<double> (0, 1) * amp[97] - Complex<double> (0, 1) * amp[98] +
      amp[100] - Complex<double> (0, 1) * amp[128]);
  jamp[11] = +1./4. * (-1./9. * amp[42] - 1./9. * amp[43] - 1./9. * amp[44] -
      1./9. * amp[45] - 1./3. * amp[47] - 1./3. * amp[48] - 1./9. * amp[49] -
      1./9. * amp[50] - 1./3. * amp[52] - 1./3. * amp[53] - 1./3. * amp[72] -
      1./3. * amp[73] - 1./9. * amp[75] - 1./9. * amp[76] - 1./9. * amp[77] -
      1./9. * amp[78] - 1./9. * amp[80] - 1./9. * amp[81] - 1./3. * amp[82] -
      1./3. * amp[83] - 1./3. * Complex<double> (0, 1) * amp[84] - 1./3. *
      Complex<double> (0, 1) * amp[85] - 1./3. * amp[88] - 1./9. * amp[106] -
      1./9. * amp[107] + 1./3. * Complex<double> (0, 1) * amp[128] + 1./3. *
      Complex<double> (0, 1) * amp[129] - 1./3. * amp[131]);
  jamp[12] = +1./4. * (-1./3. * amp[0] - 1./3. * amp[1] - 1./9. * amp[2] -
      1./9. * amp[3] - 1./9. * amp[4] - 1./9. * amp[5] - 1./3. * amp[7] - 1./3.
      * amp[9] - 1./9. * amp[10] - 1./9. * amp[11] - 1./3. * amp[58] - 1./3. *
      amp[59] - 1./9. * amp[61] - 1./9. * amp[62] - 1./3. * amp[63] - 1./3. *
      amp[64] - 1./9. * amp[66] - 1./9. * amp[67] - 1./9. * amp[68] - 1./9. *
      amp[69] + 1./3. * Complex<double> (0, 1) * amp[84] + 1./3. *
      Complex<double> (0, 1) * amp[85] - 1./3. * amp[89] - 1./9. * amp[124] -
      1./9. * amp[125] - 1./3. * Complex<double> (0, 1) * amp[128] - 1./3. *
      Complex<double> (0, 1) * amp[129] - 1./3. * amp[130]);
  jamp[13] = +1./4. * (+amp[1] + 1./3. * amp[2] + 1./3. * amp[3] -
      Complex<double> (0, 1) * amp[8] + amp[9] + 1./3. * amp[10] + 1./3. *
      amp[11] + amp[12] + amp[57] + amp[58] - Complex<double> (0, 1) * amp[60]
      + 1./3. * amp[61] + 1./3. * amp[62] + amp[64] + 1./3. * amp[68] + 1./3. *
      amp[69] - Complex<double> (0, 1) * amp[85] + amp[92] - amp[90] - amp[93]
      + amp[95] - Complex<double> (0, 1) * amp[96] + Complex<double> (0, 1) *
      amp[99] + amp[101] + Complex<double> (0, 1) * amp[129]);
  jamp[14] = +1./4. * (+amp[29] - Complex<double> (0, 1) * amp[31] + amp[32] +
      1./3. * amp[35] + 1./3. * amp[37] + 1./3. * amp[38] + 1./3. * amp[39] +
      amp[41] + amp[56] + amp[59] + Complex<double> (0, 1) * amp[60] + amp[63]
      + 1./3. * amp[66] + 1./3. * amp[67] + 1./3. * amp[68] + 1./3. * amp[69] -
      Complex<double> (0, 1) * amp[87] - amp[92] - amp[91] - amp[94] - amp[95]
      + Complex<double> (0, 1) * amp[98] + 1./3. * Complex<double> (0, 1) *
      amp[114] + 1./3. * Complex<double> (0, 1) * amp[115] + 1./3. * amp[118] -
      1./3. * Complex<double> (0, 1) * amp[120] - 1./3. * Complex<double> (0,
      1) * amp[121] + 1./3. * amp[124] - Complex<double> (0, 1) * amp[127] +
      Complex<double> (0, 1) * amp[128] + amp[130]);
  jamp[15] = +1./4. * (-1./3. * amp[28] - 1./3. * amp[29] - 1./3. * amp[30] -
      1./3. * amp[32] - amp[33] - amp[35] - Complex<double> (0, 1) * amp[36] -
      amp[38] - 1./3. * amp[56] - 1./3. * amp[57] - 1./3. * amp[58] - 1./3. *
      amp[59] - amp[62] + Complex<double> (0, 1) * amp[65] - amp[66] - amp[68]
      + 1./3. * Complex<double> (0, 1) * amp[86] + 1./3. * Complex<double> (0,
      1) * amp[87] - 1./3. * amp[88] - 1./3. * Complex<double> (0, 1) * amp[98]
      - 1./3. * Complex<double> (0, 1) * amp[99] - 1./3. * amp[101] -
      Complex<double> (0, 1) * amp[103] + Complex<double> (0, 1) * amp[104] -
      amp[106] - amp[109] - amp[108] - amp[111] - amp[112] - Complex<double>
      (0, 1) * amp[114] + Complex<double> (0, 1) * amp[120]);
  jamp[16] = +1./4. * (-1./3. * amp[56] - 1./3. * amp[57] - amp[61] - 1./3. *
      amp[63] - 1./3. * amp[64] - Complex<double> (0, 1) * amp[65] - amp[67] -
      amp[69] - 1./3. * amp[70] - 1./3. * amp[71] - amp[75] - amp[77] -
      Complex<double> (0, 1) * amp[79] - amp[80] - 1./3. * amp[82] - 1./3. *
      amp[83] - Complex<double> (0, 1) * amp[102] - amp[110] + amp[108] +
      amp[111] - amp[113] - Complex<double> (0, 1) * amp[115] + Complex<double>
      (0, 1) * amp[116] - amp[118] + Complex<double> (0, 1) * amp[122]);
  jamp[17] = +1./4. * (+1./9. * amp[56] + 1./9. * amp[57] + 1./9. * amp[58] +
      1./9. * amp[59] + 1./3. * amp[61] + 1./3. * amp[62] + 1./9. * amp[63] +
      1./9. * amp[64] + 1./3. * amp[66] + 1./3. * amp[67] + 1./9. * amp[70] +
      1./9. * amp[71] + 1./9. * amp[72] + 1./9. * amp[73] + 1./3. * amp[77] +
      1./3. * amp[78] + 1./3. * amp[80] + 1./3. * amp[81] + 1./9. * amp[82] +
      1./9. * amp[83] + 1./9. * amp[88] + 1./9. * amp[89] + 1./3. *
      Complex<double> (0, 1) * amp[102] + 1./3. * Complex<double> (0, 1) *
      amp[103] + 1./3. * amp[106] - 1./3. * Complex<double> (0, 1) * amp[122] -
      1./3. * Complex<double> (0, 1) * amp[123] + 1./3. * amp[125]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[0][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R16_P49_sm_gb_ttxbbbx::matrix_16_gbx_ttxbbxbx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 132;
  const int ncolor = 18; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
      1, 1, 1, 1, 1};
  static const double cf[ncolor][ncolor] = {{36, 12, 12, 4, 4, 12, 12, 4, 0, 0,
      0, 4, 4, 12, 0, 4, 0, 0}, {12, 36, 4, 12, 12, 4, 4, 12, 0, 4, 0, 0, 12,
      4, 0, 0, 0, 4}, {12, 4, 36, 12, 12, 4, 0, 0, 12, 4, 4, 0, 0, 4, 4, 12, 0,
      0}, {4, 12, 12, 36, 4, 12, 0, 4, 4, 12, 0, 0, 0, 0, 12, 4, 4, 0}, {4, 12,
      12, 4, 36, 12, 0, 0, 4, 0, 12, 4, 4, 0, 0, 0, 4, 12}, {12, 4, 4, 12, 12,
      36, 4, 0, 0, 0, 4, 12, 0, 0, 4, 0, 12, 4}, {12, 4, 0, 0, 0, 4, 36, 12,
      12, 4, 4, 12, 12, 4, 4, 0, 0, 0}, {4, 12, 0, 4, 0, 0, 12, 36, 4, 12, 12,
      4, 4, 12, 0, 0, 4, 0}, {0, 0, 12, 4, 4, 0, 12, 4, 36, 12, 12, 4, 4, 0,
      12, 4, 0, 0}, {0, 4, 4, 12, 0, 0, 4, 12, 12, 36, 4, 12, 0, 0, 4, 12, 0,
      4}, {0, 0, 4, 0, 12, 4, 4, 12, 12, 4, 36, 12, 0, 4, 0, 0, 12, 4}, {4, 0,
      0, 0, 4, 12, 12, 4, 4, 12, 12, 36, 0, 0, 0, 4, 4, 12}, {4, 12, 0, 0, 4,
      0, 12, 4, 4, 0, 0, 0, 36, 12, 12, 4, 4, 12}, {12, 4, 4, 0, 0, 0, 4, 12,
      0, 0, 4, 0, 12, 36, 4, 12, 12, 4}, {0, 0, 4, 12, 0, 4, 4, 0, 12, 4, 0, 0,
      12, 4, 36, 12, 12, 4}, {4, 0, 12, 4, 0, 0, 0, 0, 4, 12, 0, 4, 4, 12, 12,
      36, 4, 12}, {0, 0, 0, 4, 4, 12, 0, 4, 0, 0, 12, 4, 4, 12, 12, 4, 36, 12},
      {0, 4, 0, 0, 12, 4, 0, 0, 0, 4, 4, 12, 12, 4, 4, 12, 12, 36}};

  // Calculate color flows
  jamp[0] = +1./4. * (-1./3. * amp[162] - 1./3. * amp[164] - amp[166] +
      Complex<double> (0, 1) * amp[168] - amp[169] - amp[171] - 1./3. *
      amp[172] - 1./3. * amp[173] - amp[174] - amp[177] - Complex<double> (0,
      1) * amp[178] - amp[181] - 1./3. * amp[184] - 1./3. * amp[185] - 1./3. *
      amp[186] - 1./3. * amp[187] - 1./3. * Complex<double> (0, 1) * amp[228] -
      1./3. * Complex<double> (0, 1) * amp[229] - 1./3. * amp[232] +
      Complex<double> (0, 1) * amp[237] + amp[242] + amp[241] + amp[244] +
      amp[245] - Complex<double> (0, 1) * amp[248] + Complex<double> (0, 1) *
      amp[253] - Complex<double> (0, 1) * amp[254] - amp[256] + 1./3. *
      Complex<double> (0, 1) * amp[258] + 1./3. * Complex<double> (0, 1) *
      amp[259] - 1./3. * amp[262]);
  jamp[1] = +1./4. * (+amp[160] + amp[162] + Complex<double> (0, 1) * amp[163]
      + 1./3. * amp[165] + 1./3. * amp[166] + 1./3. * amp[167] + 1./3. *
      amp[169] + amp[172] + 1./3. * amp[174] + 1./3. * amp[175] + 1./3. *
      amp[176] + 1./3. * amp[177] + amp[180] - Complex<double> (0, 1) *
      amp[183] + amp[184] + amp[186] + Complex<double> (0, 1) * amp[217] -
      Complex<double> (0, 1) * amp[218] + amp[220] + amp[223] + amp[222] +
      amp[225] + amp[226] + Complex<double> (0, 1) * amp[228] - 1./3. *
      Complex<double> (0, 1) * amp[236] - 1./3. * Complex<double> (0, 1) *
      amp[237] + 1./3. * amp[238] + 1./3. * Complex<double> (0, 1) * amp[248] +
      1./3. * Complex<double> (0, 1) * amp[249] + 1./3. * amp[251] -
      Complex<double> (0, 1) * amp[258]);
  jamp[2] = +1./4. * (+1./9. * amp[132] + 1./9. * amp[133] + 1./3. * amp[134] +
      1./3. * amp[135] + 1./3. * amp[136] + 1./3. * amp[137] + 1./9. * amp[139]
      + 1./9. * amp[141] + 1./9. * amp[144] + 1./9. * amp[145] + 1./3. *
      amp[176] + 1./3. * amp[177] + 1./9. * amp[179] + 1./9. * amp[180] + 1./3.
      * amp[181] + 1./3. * amp[182] + 1./9. * amp[184] + 1./9. * amp[185] +
      1./9. * amp[186] + 1./9. * amp[187] - 1./3. * Complex<double> (0, 1) *
      amp[234] - 1./3. * Complex<double> (0, 1) * amp[235] + 1./3. * amp[239] +
      1./3. * Complex<double> (0, 1) * amp[254] + 1./3. * Complex<double> (0,
      1) * amp[255] + 1./3. * amp[256] + 1./9. * amp[262] + 1./9. * amp[263]);
  jamp[3] = +1./4. * (-1./3. * amp[132] - 1./3. * amp[133] - amp[135] -
      amp[137] + Complex<double> (0, 1) * amp[138] - amp[142] - 1./3. *
      amp[144] - 1./3. * amp[145] - amp[175] - amp[176] + Complex<double> (0,
      1) * amp[178] - 1./3. * amp[179] - 1./3. * amp[180] - amp[182] - 1./3. *
      amp[186] - 1./3. * amp[187] + Complex<double> (0, 1) * amp[235] -
      amp[242] + amp[240] + amp[243] - amp[245] + Complex<double> (0, 1) *
      amp[246] - Complex<double> (0, 1) * amp[249] - amp[251] - Complex<double>
      (0, 1) * amp[255]);
  jamp[4] = +1./4. * (-1./9. * amp[174] - 1./9. * amp[175] - 1./9. * amp[176] -
      1./9. * amp[177] - 1./3. * amp[179] - 1./3. * amp[180] - 1./9. * amp[181]
      - 1./9. * amp[182] - 1./3. * amp[184] - 1./3. * amp[185] - 1./3. *
      amp[204] - 1./3. * amp[205] - 1./9. * amp[207] - 1./9. * amp[208] - 1./9.
      * amp[209] - 1./9. * amp[210] - 1./9. * amp[212] - 1./9. * amp[213] -
      1./3. * amp[214] - 1./3. * amp[215] - 1./3. * Complex<double> (0, 1) *
      amp[216] - 1./3. * Complex<double> (0, 1) * amp[217] - 1./3. * amp[220] -
      1./9. * amp[238] - 1./9. * amp[239] + 1./3. * Complex<double> (0, 1) *
      amp[260] + 1./3. * Complex<double> (0, 1) * amp[261] - 1./3. * amp[263]);
  jamp[5] = +1./4. * (+1./3. * amp[174] + 1./3. * amp[175] + amp[179] + 1./3. *
      amp[181] + 1./3. * amp[182] + Complex<double> (0, 1) * amp[183] +
      amp[185] + amp[187] + amp[202] + amp[204] + Complex<double> (0, 1) *
      amp[206] + 1./3. * amp[207] + 1./3. * amp[208] + 1./3. * amp[212] + 1./3.
      * amp[213] + amp[214] + Complex<double> (0, 1) * amp[216] + amp[224] -
      amp[222] - amp[225] + amp[227] + Complex<double> (0, 1) * amp[229] -
      Complex<double> (0, 1) * amp[230] + amp[232] - Complex<double> (0, 1) *
      amp[260]);
  jamp[6] = +1./4. * (+1./9. * amp[146] + 1./9. * amp[147] + 1./9. * amp[148] +
      1./9. * amp[150] + 1./3. * amp[151] + 1./3. * amp[152] + 1./3. * amp[156]
      + 1./3. * amp[157] + 1./9. * amp[158] + 1./9. * amp[159] + 1./9. *
      amp[160] + 1./9. * amp[161] + 1./9. * amp[162] + 1./9. * amp[164] + 1./3.
      * amp[165] + 1./3. * amp[166] + 1./3. * amp[170] + 1./3. * amp[171] +
      1./9. * amp[172] + 1./9. * amp[173] + 1./9. * amp[232] + 1./9. *
      amp[233]);
  jamp[7] = +1./4. * (-1./3. * amp[146] - 1./3. * amp[147] - 1./9. * amp[151] -
      1./9. * amp[152] - 1./9. * amp[153] - 1./9. * amp[155] - 1./9. * amp[156]
      - 1./9. * amp[157] - 1./3. * amp[158] - 1./3. * amp[159] - 1./3. *
      amp[160] - 1./3. * amp[161] - 1./9. * amp[165] - 1./9. * amp[166] - 1./9.
      * amp[167] - 1./9. * amp[169] - 1./9. * amp[170] - 1./9. * amp[171] -
      1./3. * amp[172] - 1./3. * amp[173] - 1./9. * amp[250] - 1./9. *
      amp[251]);
  jamp[8] = +1./4. * (-amp[134] - amp[136] - Complex<double> (0, 1) * amp[138]
      - 1./3. * amp[139] - 1./3. * amp[141] - amp[143] - 1./3. * amp[144] -
      1./3. * amp[145] - 1./3. * amp[148] - 1./3. * amp[150] - amp[152] -
      amp[153] + Complex<double> (0, 1) * amp[154] - amp[157] - 1./3. *
      amp[158] - 1./3. * amp[159] + 1./3. * Complex<double> (0, 1) * amp[228] +
      1./3. * Complex<double> (0, 1) * amp[229] - 1./3. * amp[233] +
      Complex<double> (0, 1) * amp[234] - Complex<double> (0, 1) * amp[237] -
      amp[239] - amp[241] - amp[240] - amp[243] - amp[244] + Complex<double>
      (0, 1) * amp[247] - Complex<double> (0, 1) * amp[253] - 1./3. *
      Complex<double> (0, 1) * amp[258] - 1./3. * Complex<double> (0, 1) *
      amp[259] - 1./3. * amp[263]);
  jamp[9] = +1./4. * (+amp[132] + 1./3. * amp[136] + 1./3. * amp[137] +
      amp[139] + Complex<double> (0, 1) * amp[140] + 1./3. * amp[142] + 1./3. *
      amp[143] + amp[145] + amp[147] + amp[148] - Complex<double> (0, 1) *
      amp[149] + 1./3. * amp[153] + 1./3. * amp[155] + 1./3. * amp[156] + 1./3.
      * amp[157] + amp[159] - Complex<double> (0, 1) * amp[216] +
      Complex<double> (0, 1) * amp[219] + amp[221] + amp[223] + amp[222] +
      amp[225] + amp[226] - Complex<double> (0, 1) * amp[229] - 1./3. *
      Complex<double> (0, 1) * amp[246] - 1./3. * Complex<double> (0, 1) *
      amp[247] + 1./3. * amp[251] + 1./3. * Complex<double> (0, 1) * amp[252] +
      1./3. * Complex<double> (0, 1) * amp[253] + 1./3. * amp[257] +
      Complex<double> (0, 1) * amp[259]);
  jamp[10] = +1./4. * (+amp[146] + Complex<double> (0, 1) * amp[149] + amp[150]
      + 1./3. * amp[151] + 1./3. * amp[152] + 1./3. * amp[153] + 1./3. *
      amp[155] + amp[158] + amp[203] + amp[205] - Complex<double> (0, 1) *
      amp[206] + 1./3. * amp[207] + 1./3. * amp[208] + 1./3. * amp[209] + 1./3.
      * amp[210] + amp[215] + Complex<double> (0, 1) * amp[218] - amp[224] -
      amp[223] - amp[226] - amp[227] - Complex<double> (0, 1) * amp[231] +
      1./3. * Complex<double> (0, 1) * amp[236] + 1./3. * Complex<double> (0,
      1) * amp[237] + 1./3. * amp[239] - 1./3. * Complex<double> (0, 1) *
      amp[248] - 1./3. * Complex<double> (0, 1) * amp[249] + 1./3. * amp[250] +
      Complex<double> (0, 1) * amp[258] - Complex<double> (0, 1) * amp[261] +
      amp[263]);
  jamp[11] = +1./4. * (-1./3. * amp[146] - 1./3. * amp[147] - 1./3. * amp[148]
      - 1./3. * amp[150] - amp[151] - Complex<double> (0, 1) * amp[154] -
      amp[155] - amp[156] - 1./3. * amp[202] - 1./3. * amp[203] - 1./3. *
      amp[204] - 1./3. * amp[205] - amp[208] - amp[210] + Complex<double> (0,
      1) * amp[211] - amp[213] - 1./3. * Complex<double> (0, 1) * amp[218] -
      1./3. * Complex<double> (0, 1) * amp[219] - 1./3. * amp[221] + 1./3. *
      Complex<double> (0, 1) * amp[230] + 1./3. * Complex<double> (0, 1) *
      amp[231] - 1./3. * amp[232] - Complex<double> (0, 1) * amp[236] +
      amp[242] + amp[241] + amp[244] + amp[245] + Complex<double> (0, 1) *
      amp[249] - Complex<double> (0, 1) * amp[252] + Complex<double> (0, 1) *
      amp[255] - amp[257]);
  jamp[12] = +1./4. * (-1./3. * amp[160] - 1./3. * amp[161] - 1./3. * amp[162]
      - 1./3. * amp[164] - amp[165] - amp[167] - Complex<double> (0, 1) *
      amp[168] - amp[170] - 1./3. * amp[188] - 1./3. * amp[189] - 1./3. *
      amp[190] - 1./3. * amp[191] - amp[194] + Complex<double> (0, 1) *
      amp[197] - amp[198] - amp[200] + 1./3. * Complex<double> (0, 1) *
      amp[218] + 1./3. * Complex<double> (0, 1) * amp[219] - 1./3. * amp[220] -
      1./3. * Complex<double> (0, 1) * amp[230] - 1./3. * Complex<double> (0,
      1) * amp[231] - 1./3. * amp[233] - Complex<double> (0, 1) * amp[235] +
      Complex<double> (0, 1) * amp[236] - amp[238] - amp[241] - amp[240] -
      amp[243] - amp[244] - Complex<double> (0, 1) * amp[246] + Complex<double>
      (0, 1) * amp[252]);
  jamp[13] = +1./4. * (+amp[161] - Complex<double> (0, 1) * amp[163] + amp[164]
      + 1./3. * amp[167] + 1./3. * amp[169] + 1./3. * amp[170] + 1./3. *
      amp[171] + amp[173] + amp[188] + amp[191] + Complex<double> (0, 1) *
      amp[192] + amp[195] + 1./3. * amp[198] + 1./3. * amp[199] + 1./3. *
      amp[200] + 1./3. * amp[201] - Complex<double> (0, 1) * amp[219] -
      amp[224] - amp[223] - amp[226] - amp[227] + Complex<double> (0, 1) *
      amp[230] + 1./3. * Complex<double> (0, 1) * amp[246] + 1./3. *
      Complex<double> (0, 1) * amp[247] + 1./3. * amp[250] - 1./3. *
      Complex<double> (0, 1) * amp[252] - 1./3. * Complex<double> (0, 1) *
      amp[253] + 1./3. * amp[256] - Complex<double> (0, 1) * amp[259] +
      Complex<double> (0, 1) * amp[260] + amp[262]);
  jamp[14] = +1./4. * (+amp[133] + 1./3. * amp[134] + 1./3. * amp[135] -
      Complex<double> (0, 1) * amp[140] + amp[141] + 1./3. * amp[142] + 1./3. *
      amp[143] + amp[144] + amp[189] + amp[190] - Complex<double> (0, 1) *
      amp[192] + 1./3. * amp[193] + 1./3. * amp[194] + amp[196] + 1./3. *
      amp[200] + 1./3. * amp[201] - Complex<double> (0, 1) * amp[217] +
      amp[224] - amp[222] - amp[225] + amp[227] - Complex<double> (0, 1) *
      amp[228] + Complex<double> (0, 1) * amp[231] + amp[233] + Complex<double>
      (0, 1) * amp[261]);
  jamp[15] = +1./4. * (-1./3. * amp[132] - 1./3. * amp[133] - 1./9. * amp[134]
      - 1./9. * amp[135] - 1./9. * amp[136] - 1./9. * amp[137] - 1./3. *
      amp[139] - 1./3. * amp[141] - 1./9. * amp[142] - 1./9. * amp[143] - 1./3.
      * amp[190] - 1./3. * amp[191] - 1./9. * amp[193] - 1./9. * amp[194] -
      1./3. * amp[195] - 1./3. * amp[196] - 1./9. * amp[198] - 1./9. * amp[199]
      - 1./9. * amp[200] - 1./9. * amp[201] + 1./3. * Complex<double> (0, 1) *
      amp[216] + 1./3. * Complex<double> (0, 1) * amp[217] - 1./3. * amp[221] -
      1./9. * amp[256] - 1./9. * amp[257] - 1./3. * Complex<double> (0, 1) *
      amp[260] - 1./3. * Complex<double> (0, 1) * amp[261] - 1./3. * amp[262]);
  jamp[16] = +1./4. * (-1./3. * amp[188] - 1./3. * amp[189] - amp[193] - 1./3.
      * amp[195] - 1./3. * amp[196] - Complex<double> (0, 1) * amp[197] -
      amp[199] - amp[201] - 1./3. * amp[202] - 1./3. * amp[203] - amp[207] -
      amp[209] - Complex<double> (0, 1) * amp[211] - amp[212] - 1./3. *
      amp[214] - 1./3. * amp[215] - Complex<double> (0, 1) * amp[234] -
      amp[242] + amp[240] + amp[243] - amp[245] - Complex<double> (0, 1) *
      amp[247] + Complex<double> (0, 1) * amp[248] - amp[250] + Complex<double>
      (0, 1) * amp[254]);
  jamp[17] = +1./4. * (+1./9. * amp[188] + 1./9. * amp[189] + 1./9. * amp[190]
      + 1./9. * amp[191] + 1./3. * amp[193] + 1./3. * amp[194] + 1./9. *
      amp[195] + 1./9. * amp[196] + 1./3. * amp[198] + 1./3. * amp[199] + 1./9.
      * amp[202] + 1./9. * amp[203] + 1./9. * amp[204] + 1./9. * amp[205] +
      1./3. * amp[209] + 1./3. * amp[210] + 1./3. * amp[212] + 1./3. * amp[213]
      + 1./9. * amp[214] + 1./9. * amp[215] + 1./9. * amp[220] + 1./9. *
      amp[221] + 1./3. * Complex<double> (0, 1) * amp[234] + 1./3. *
      Complex<double> (0, 1) * amp[235] + 1./3. * amp[238] - 1./3. *
      Complex<double> (0, 1) * amp[254] - 1./3. * Complex<double> (0, 1) *
      amp[255] + 1./3. * amp[257]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[1][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}


}  // end namespace PY8MEs_namespace

