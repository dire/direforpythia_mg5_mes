//==========================================================================
// This file has been automatically generated for Pythia 8
// MadGraph5_aMC@NLO v. 2.6.0, 2017-08-16
// By the MadGraph5_aMC@NLO Development Team
// Visit launchpad.net/madgraph5 and amcatnlo.web.cern.ch
//==========================================================================

#ifndef PY8MEs_R4_P73_sm_gq_llqqq_H
#define PY8MEs_R4_P73_sm_gq_llqqq_H

#include "Complex.h" 
#include <vector> 
#include <set> 
#include <exception> 
#include <iostream> 

#include "Parameters_sm.h"
#include "PY8MEs.h"

using namespace std; 

namespace PY8MEs_namespace 
{
//==========================================================================
// A class for calculating the matrix elements for
// Process: g u > e+ e- u u u~ WEIGHTED<=7 @4
// Process: g u > mu+ mu- u u u~ WEIGHTED<=7 @4
// Process: g c > e+ e- c c c~ WEIGHTED<=7 @4
// Process: g c > mu+ mu- c c c~ WEIGHTED<=7 @4
// Process: g d > e+ e- d d d~ WEIGHTED<=7 @4
// Process: g d > mu+ mu- d d d~ WEIGHTED<=7 @4
// Process: g s > e+ e- s s s~ WEIGHTED<=7 @4
// Process: g s > mu+ mu- s s s~ WEIGHTED<=7 @4
// Process: g u~ > e+ e- u u~ u~ WEIGHTED<=7 @4
// Process: g u~ > mu+ mu- u u~ u~ WEIGHTED<=7 @4
// Process: g c~ > e+ e- c c~ c~ WEIGHTED<=7 @4
// Process: g c~ > mu+ mu- c c~ c~ WEIGHTED<=7 @4
// Process: g d~ > e+ e- d d~ d~ WEIGHTED<=7 @4
// Process: g d~ > mu+ mu- d d~ d~ WEIGHTED<=7 @4
// Process: g s~ > e+ e- s s~ s~ WEIGHTED<=7 @4
// Process: g s~ > mu+ mu- s s~ s~ WEIGHTED<=7 @4
// Process: g u > e+ e- u c c~ WEIGHTED<=7 @4
// Process: g u > mu+ mu- u c c~ WEIGHTED<=7 @4
// Process: g c > e+ e- c u u~ WEIGHTED<=7 @4
// Process: g c > mu+ mu- c u u~ WEIGHTED<=7 @4
// Process: g u > e+ e- u d d~ WEIGHTED<=7 @4
// Process: g u > e+ e- u s s~ WEIGHTED<=7 @4
// Process: g u > mu+ mu- u d d~ WEIGHTED<=7 @4
// Process: g u > mu+ mu- u s s~ WEIGHTED<=7 @4
// Process: g c > e+ e- c d d~ WEIGHTED<=7 @4
// Process: g c > e+ e- c s s~ WEIGHTED<=7 @4
// Process: g c > mu+ mu- c d d~ WEIGHTED<=7 @4
// Process: g c > mu+ mu- c s s~ WEIGHTED<=7 @4
// Process: g u > ve~ ve u u u~ WEIGHTED<=7 @4
// Process: g u > vm~ vm u u u~ WEIGHTED<=7 @4
// Process: g u > vt~ vt u u u~ WEIGHTED<=7 @4
// Process: g c > ve~ ve c c c~ WEIGHTED<=7 @4
// Process: g c > vm~ vm c c c~ WEIGHTED<=7 @4
// Process: g c > vt~ vt c c c~ WEIGHTED<=7 @4
// Process: g d > e+ e- u d u~ WEIGHTED<=7 @4
// Process: g d > e+ e- c d c~ WEIGHTED<=7 @4
// Process: g d > mu+ mu- u d u~ WEIGHTED<=7 @4
// Process: g d > mu+ mu- c d c~ WEIGHTED<=7 @4
// Process: g s > e+ e- u s u~ WEIGHTED<=7 @4
// Process: g s > e+ e- c s c~ WEIGHTED<=7 @4
// Process: g s > mu+ mu- u s u~ WEIGHTED<=7 @4
// Process: g s > mu+ mu- c s c~ WEIGHTED<=7 @4
// Process: g d > e+ e- d s s~ WEIGHTED<=7 @4
// Process: g d > mu+ mu- d s s~ WEIGHTED<=7 @4
// Process: g s > e+ e- s d d~ WEIGHTED<=7 @4
// Process: g s > mu+ mu- s d d~ WEIGHTED<=7 @4
// Process: g d > ve~ ve d d d~ WEIGHTED<=7 @4
// Process: g d > vm~ vm d d d~ WEIGHTED<=7 @4
// Process: g d > vt~ vt d d d~ WEIGHTED<=7 @4
// Process: g s > ve~ ve s s s~ WEIGHTED<=7 @4
// Process: g s > vm~ vm s s s~ WEIGHTED<=7 @4
// Process: g s > vt~ vt s s s~ WEIGHTED<=7 @4
// Process: g u~ > e+ e- c u~ c~ WEIGHTED<=7 @4
// Process: g u~ > mu+ mu- c u~ c~ WEIGHTED<=7 @4
// Process: g c~ > e+ e- u c~ u~ WEIGHTED<=7 @4
// Process: g c~ > mu+ mu- u c~ u~ WEIGHTED<=7 @4
// Process: g u~ > e+ e- d u~ d~ WEIGHTED<=7 @4
// Process: g u~ > e+ e- s u~ s~ WEIGHTED<=7 @4
// Process: g u~ > mu+ mu- d u~ d~ WEIGHTED<=7 @4
// Process: g u~ > mu+ mu- s u~ s~ WEIGHTED<=7 @4
// Process: g c~ > e+ e- d c~ d~ WEIGHTED<=7 @4
// Process: g c~ > e+ e- s c~ s~ WEIGHTED<=7 @4
// Process: g c~ > mu+ mu- d c~ d~ WEIGHTED<=7 @4
// Process: g c~ > mu+ mu- s c~ s~ WEIGHTED<=7 @4
// Process: g u~ > ve~ ve u u~ u~ WEIGHTED<=7 @4
// Process: g u~ > vm~ vm u u~ u~ WEIGHTED<=7 @4
// Process: g u~ > vt~ vt u u~ u~ WEIGHTED<=7 @4
// Process: g c~ > ve~ ve c c~ c~ WEIGHTED<=7 @4
// Process: g c~ > vm~ vm c c~ c~ WEIGHTED<=7 @4
// Process: g c~ > vt~ vt c c~ c~ WEIGHTED<=7 @4
// Process: g d~ > e+ e- u u~ d~ WEIGHTED<=7 @4
// Process: g d~ > e+ e- c c~ d~ WEIGHTED<=7 @4
// Process: g d~ > mu+ mu- u u~ d~ WEIGHTED<=7 @4
// Process: g d~ > mu+ mu- c c~ d~ WEIGHTED<=7 @4
// Process: g s~ > e+ e- u u~ s~ WEIGHTED<=7 @4
// Process: g s~ > e+ e- c c~ s~ WEIGHTED<=7 @4
// Process: g s~ > mu+ mu- u u~ s~ WEIGHTED<=7 @4
// Process: g s~ > mu+ mu- c c~ s~ WEIGHTED<=7 @4
// Process: g d~ > e+ e- s d~ s~ WEIGHTED<=7 @4
// Process: g d~ > mu+ mu- s d~ s~ WEIGHTED<=7 @4
// Process: g s~ > e+ e- d s~ d~ WEIGHTED<=7 @4
// Process: g s~ > mu+ mu- d s~ d~ WEIGHTED<=7 @4
// Process: g d~ > ve~ ve d d~ d~ WEIGHTED<=7 @4
// Process: g d~ > vm~ vm d d~ d~ WEIGHTED<=7 @4
// Process: g d~ > vt~ vt d d~ d~ WEIGHTED<=7 @4
// Process: g s~ > ve~ ve s s~ s~ WEIGHTED<=7 @4
// Process: g s~ > vm~ vm s s~ s~ WEIGHTED<=7 @4
// Process: g s~ > vt~ vt s s~ s~ WEIGHTED<=7 @4
// Process: g u > e+ ve u d u~ WEIGHTED<=7 @4
// Process: g u > mu+ vm u d u~ WEIGHTED<=7 @4
// Process: g c > e+ ve c s c~ WEIGHTED<=7 @4
// Process: g c > mu+ vm c s c~ WEIGHTED<=7 @4
// Process: g u > e+ ve d d d~ WEIGHTED<=7 @4
// Process: g u > mu+ vm d d d~ WEIGHTED<=7 @4
// Process: g c > e+ ve s s s~ WEIGHTED<=7 @4
// Process: g c > mu+ vm s s s~ WEIGHTED<=7 @4
// Process: g u > ve~ e- u u d~ WEIGHTED<=7 @4
// Process: g u > vm~ mu- u u d~ WEIGHTED<=7 @4
// Process: g c > ve~ e- c c s~ WEIGHTED<=7 @4
// Process: g c > vm~ mu- c c s~ WEIGHTED<=7 @4
// Process: g u > ve~ ve u c c~ WEIGHTED<=7 @4
// Process: g u > vm~ vm u c c~ WEIGHTED<=7 @4
// Process: g u > vt~ vt u c c~ WEIGHTED<=7 @4
// Process: g c > ve~ ve c u u~ WEIGHTED<=7 @4
// Process: g c > vm~ vm c u u~ WEIGHTED<=7 @4
// Process: g c > vt~ vt c u u~ WEIGHTED<=7 @4
// Process: g u > ve~ ve u d d~ WEIGHTED<=7 @4
// Process: g u > ve~ ve u s s~ WEIGHTED<=7 @4
// Process: g u > vm~ vm u d d~ WEIGHTED<=7 @4
// Process: g u > vm~ vm u s s~ WEIGHTED<=7 @4
// Process: g u > vt~ vt u d d~ WEIGHTED<=7 @4
// Process: g u > vt~ vt u s s~ WEIGHTED<=7 @4
// Process: g c > ve~ ve c d d~ WEIGHTED<=7 @4
// Process: g c > ve~ ve c s s~ WEIGHTED<=7 @4
// Process: g c > vm~ vm c d d~ WEIGHTED<=7 @4
// Process: g c > vm~ vm c s s~ WEIGHTED<=7 @4
// Process: g c > vt~ vt c d d~ WEIGHTED<=7 @4
// Process: g c > vt~ vt c s s~ WEIGHTED<=7 @4
// Process: g d > e+ ve d d u~ WEIGHTED<=7 @4
// Process: g d > mu+ vm d d u~ WEIGHTED<=7 @4
// Process: g s > e+ ve s s c~ WEIGHTED<=7 @4
// Process: g s > mu+ vm s s c~ WEIGHTED<=7 @4
// Process: g d > ve~ e- u u u~ WEIGHTED<=7 @4
// Process: g d > vm~ mu- u u u~ WEIGHTED<=7 @4
// Process: g s > ve~ e- c c c~ WEIGHTED<=7 @4
// Process: g s > vm~ mu- c c c~ WEIGHTED<=7 @4
// Process: g d > ve~ e- u d d~ WEIGHTED<=7 @4
// Process: g d > vm~ mu- u d d~ WEIGHTED<=7 @4
// Process: g s > ve~ e- c s s~ WEIGHTED<=7 @4
// Process: g s > vm~ mu- c s s~ WEIGHTED<=7 @4
// Process: g d > ve~ ve u d u~ WEIGHTED<=7 @4
// Process: g d > ve~ ve c d c~ WEIGHTED<=7 @4
// Process: g d > vm~ vm u d u~ WEIGHTED<=7 @4
// Process: g d > vm~ vm c d c~ WEIGHTED<=7 @4
// Process: g d > vt~ vt u d u~ WEIGHTED<=7 @4
// Process: g d > vt~ vt c d c~ WEIGHTED<=7 @4
// Process: g s > ve~ ve u s u~ WEIGHTED<=7 @4
// Process: g s > ve~ ve c s c~ WEIGHTED<=7 @4
// Process: g s > vm~ vm u s u~ WEIGHTED<=7 @4
// Process: g s > vm~ vm c s c~ WEIGHTED<=7 @4
// Process: g s > vt~ vt u s u~ WEIGHTED<=7 @4
// Process: g s > vt~ vt c s c~ WEIGHTED<=7 @4
// Process: g d > ve~ ve d s s~ WEIGHTED<=7 @4
// Process: g d > vm~ vm d s s~ WEIGHTED<=7 @4
// Process: g d > vt~ vt d s s~ WEIGHTED<=7 @4
// Process: g s > ve~ ve s d d~ WEIGHTED<=7 @4
// Process: g s > vm~ vm s d d~ WEIGHTED<=7 @4
// Process: g s > vt~ vt s d d~ WEIGHTED<=7 @4
// Process: g u~ > e+ ve d u~ u~ WEIGHTED<=7 @4
// Process: g u~ > mu+ vm d u~ u~ WEIGHTED<=7 @4
// Process: g c~ > e+ ve s c~ c~ WEIGHTED<=7 @4
// Process: g c~ > mu+ vm s c~ c~ WEIGHTED<=7 @4
// Process: g u~ > ve~ e- u u~ d~ WEIGHTED<=7 @4
// Process: g u~ > vm~ mu- u u~ d~ WEIGHTED<=7 @4
// Process: g c~ > ve~ e- c c~ s~ WEIGHTED<=7 @4
// Process: g c~ > vm~ mu- c c~ s~ WEIGHTED<=7 @4
// Process: g u~ > ve~ e- d d~ d~ WEIGHTED<=7 @4
// Process: g u~ > vm~ mu- d d~ d~ WEIGHTED<=7 @4
// Process: g c~ > ve~ e- s s~ s~ WEIGHTED<=7 @4
// Process: g c~ > vm~ mu- s s~ s~ WEIGHTED<=7 @4
// Process: g u~ > ve~ ve c u~ c~ WEIGHTED<=7 @4
// Process: g u~ > vm~ vm c u~ c~ WEIGHTED<=7 @4
// Process: g u~ > vt~ vt c u~ c~ WEIGHTED<=7 @4
// Process: g c~ > ve~ ve u c~ u~ WEIGHTED<=7 @4
// Process: g c~ > vm~ vm u c~ u~ WEIGHTED<=7 @4
// Process: g c~ > vt~ vt u c~ u~ WEIGHTED<=7 @4
// Process: g u~ > ve~ ve d u~ d~ WEIGHTED<=7 @4
// Process: g u~ > ve~ ve s u~ s~ WEIGHTED<=7 @4
// Process: g u~ > vm~ vm d u~ d~ WEIGHTED<=7 @4
// Process: g u~ > vm~ vm s u~ s~ WEIGHTED<=7 @4
// Process: g u~ > vt~ vt d u~ d~ WEIGHTED<=7 @4
// Process: g u~ > vt~ vt s u~ s~ WEIGHTED<=7 @4
// Process: g c~ > ve~ ve d c~ d~ WEIGHTED<=7 @4
// Process: g c~ > ve~ ve s c~ s~ WEIGHTED<=7 @4
// Process: g c~ > vm~ vm d c~ d~ WEIGHTED<=7 @4
// Process: g c~ > vm~ vm s c~ s~ WEIGHTED<=7 @4
// Process: g c~ > vt~ vt d c~ d~ WEIGHTED<=7 @4
// Process: g c~ > vt~ vt s c~ s~ WEIGHTED<=7 @4
// Process: g d~ > e+ ve u u~ u~ WEIGHTED<=7 @4
// Process: g d~ > mu+ vm u u~ u~ WEIGHTED<=7 @4
// Process: g s~ > e+ ve c c~ c~ WEIGHTED<=7 @4
// Process: g s~ > mu+ vm c c~ c~ WEIGHTED<=7 @4
// Process: g d~ > e+ ve d u~ d~ WEIGHTED<=7 @4
// Process: g d~ > mu+ vm d u~ d~ WEIGHTED<=7 @4
// Process: g s~ > e+ ve s c~ s~ WEIGHTED<=7 @4
// Process: g s~ > mu+ vm s c~ s~ WEIGHTED<=7 @4
// Process: g d~ > ve~ e- u d~ d~ WEIGHTED<=7 @4
// Process: g d~ > vm~ mu- u d~ d~ WEIGHTED<=7 @4
// Process: g s~ > ve~ e- c s~ s~ WEIGHTED<=7 @4
// Process: g s~ > vm~ mu- c s~ s~ WEIGHTED<=7 @4
// Process: g d~ > ve~ ve u u~ d~ WEIGHTED<=7 @4
// Process: g d~ > ve~ ve c c~ d~ WEIGHTED<=7 @4
// Process: g d~ > vm~ vm u u~ d~ WEIGHTED<=7 @4
// Process: g d~ > vm~ vm c c~ d~ WEIGHTED<=7 @4
// Process: g d~ > vt~ vt u u~ d~ WEIGHTED<=7 @4
// Process: g d~ > vt~ vt c c~ d~ WEIGHTED<=7 @4
// Process: g s~ > ve~ ve u u~ s~ WEIGHTED<=7 @4
// Process: g s~ > ve~ ve c c~ s~ WEIGHTED<=7 @4
// Process: g s~ > vm~ vm u u~ s~ WEIGHTED<=7 @4
// Process: g s~ > vm~ vm c c~ s~ WEIGHTED<=7 @4
// Process: g s~ > vt~ vt u u~ s~ WEIGHTED<=7 @4
// Process: g s~ > vt~ vt c c~ s~ WEIGHTED<=7 @4
// Process: g d~ > ve~ ve s d~ s~ WEIGHTED<=7 @4
// Process: g d~ > vm~ vm s d~ s~ WEIGHTED<=7 @4
// Process: g d~ > vt~ vt s d~ s~ WEIGHTED<=7 @4
// Process: g s~ > ve~ ve d s~ d~ WEIGHTED<=7 @4
// Process: g s~ > vm~ vm d s~ d~ WEIGHTED<=7 @4
// Process: g s~ > vt~ vt d s~ d~ WEIGHTED<=7 @4
// Process: g u > e+ ve u s c~ WEIGHTED<=7 @4
// Process: g u > mu+ vm u s c~ WEIGHTED<=7 @4
// Process: g c > e+ ve c d u~ WEIGHTED<=7 @4
// Process: g c > mu+ vm c d u~ WEIGHTED<=7 @4
// Process: g d > e+ ve d s c~ WEIGHTED<=7 @4
// Process: g d > mu+ vm d s c~ WEIGHTED<=7 @4
// Process: g s > e+ ve s d u~ WEIGHTED<=7 @4
// Process: g s > mu+ vm s d u~ WEIGHTED<=7 @4
// Process: g u > e+ ve c d c~ WEIGHTED<=7 @4
// Process: g u > e+ ve s d s~ WEIGHTED<=7 @4
// Process: g u > mu+ vm c d c~ WEIGHTED<=7 @4
// Process: g u > mu+ vm s d s~ WEIGHTED<=7 @4
// Process: g c > e+ ve u s u~ WEIGHTED<=7 @4
// Process: g c > e+ ve d s d~ WEIGHTED<=7 @4
// Process: g c > mu+ vm u s u~ WEIGHTED<=7 @4
// Process: g c > mu+ vm d s d~ WEIGHTED<=7 @4
// Process: g u > ve~ e- u c s~ WEIGHTED<=7 @4
// Process: g u > vm~ mu- u c s~ WEIGHTED<=7 @4
// Process: g c > ve~ e- c u d~ WEIGHTED<=7 @4
// Process: g c > vm~ mu- c u d~ WEIGHTED<=7 @4
// Process: g d > ve~ e- d c s~ WEIGHTED<=7 @4
// Process: g d > vm~ mu- d c s~ WEIGHTED<=7 @4
// Process: g s > ve~ e- s u d~ WEIGHTED<=7 @4
// Process: g s > vm~ mu- s u d~ WEIGHTED<=7 @4
// Process: g d > ve~ e- u c c~ WEIGHTED<=7 @4
// Process: g d > ve~ e- u s s~ WEIGHTED<=7 @4
// Process: g d > vm~ mu- u c c~ WEIGHTED<=7 @4
// Process: g d > vm~ mu- u s s~ WEIGHTED<=7 @4
// Process: g s > ve~ e- c u u~ WEIGHTED<=7 @4
// Process: g s > ve~ e- c d d~ WEIGHTED<=7 @4
// Process: g s > vm~ mu- c u u~ WEIGHTED<=7 @4
// Process: g s > vm~ mu- c d d~ WEIGHTED<=7 @4
// Process: g u~ > e+ ve s u~ c~ WEIGHTED<=7 @4
// Process: g u~ > mu+ vm s u~ c~ WEIGHTED<=7 @4
// Process: g c~ > e+ ve d c~ u~ WEIGHTED<=7 @4
// Process: g c~ > mu+ vm d c~ u~ WEIGHTED<=7 @4
// Process: g d~ > e+ ve s d~ c~ WEIGHTED<=7 @4
// Process: g d~ > mu+ vm s d~ c~ WEIGHTED<=7 @4
// Process: g s~ > e+ ve d s~ u~ WEIGHTED<=7 @4
// Process: g s~ > mu+ vm d s~ u~ WEIGHTED<=7 @4
// Process: g u~ > ve~ e- c u~ s~ WEIGHTED<=7 @4
// Process: g u~ > vm~ mu- c u~ s~ WEIGHTED<=7 @4
// Process: g c~ > ve~ e- u c~ d~ WEIGHTED<=7 @4
// Process: g c~ > vm~ mu- u c~ d~ WEIGHTED<=7 @4
// Process: g d~ > ve~ e- c d~ s~ WEIGHTED<=7 @4
// Process: g d~ > vm~ mu- c d~ s~ WEIGHTED<=7 @4
// Process: g s~ > ve~ e- u s~ d~ WEIGHTED<=7 @4
// Process: g s~ > vm~ mu- u s~ d~ WEIGHTED<=7 @4
// Process: g u~ > ve~ e- c c~ d~ WEIGHTED<=7 @4
// Process: g u~ > ve~ e- s s~ d~ WEIGHTED<=7 @4
// Process: g u~ > vm~ mu- c c~ d~ WEIGHTED<=7 @4
// Process: g u~ > vm~ mu- s s~ d~ WEIGHTED<=7 @4
// Process: g c~ > ve~ e- u u~ s~ WEIGHTED<=7 @4
// Process: g c~ > ve~ e- d d~ s~ WEIGHTED<=7 @4
// Process: g c~ > vm~ mu- u u~ s~ WEIGHTED<=7 @4
// Process: g c~ > vm~ mu- d d~ s~ WEIGHTED<=7 @4
// Process: g d~ > e+ ve c u~ c~ WEIGHTED<=7 @4
// Process: g d~ > e+ ve s u~ s~ WEIGHTED<=7 @4
// Process: g d~ > mu+ vm c u~ c~ WEIGHTED<=7 @4
// Process: g d~ > mu+ vm s u~ s~ WEIGHTED<=7 @4
// Process: g s~ > e+ ve u c~ u~ WEIGHTED<=7 @4
// Process: g s~ > e+ ve d c~ d~ WEIGHTED<=7 @4
// Process: g s~ > mu+ vm u c~ u~ WEIGHTED<=7 @4
// Process: g s~ > mu+ vm d c~ d~ WEIGHTED<=7 @4
//--------------------------------------------------------------------------

typedef vector<double> vec_double; 
typedef vector < vec_double > vec_vec_double; 
typedef vector<int> vec_int; 
typedef vector<bool> vec_bool; 
typedef vector < vec_int > vec_vec_int; 

class PY8MEs_R4_P73_sm_gq_llqqq : public PY8ME
{
  public:

    // Check for the availability of the requested proces.
    // If available, this returns the corresponding permutation and Proc_ID  to
    // use.
    // If not available, this returns a negative Proc_ID.
    static pair < vector<int> , int > static_getPY8ME(vector<int> initial_pdgs,
        vector<int> final_pdgs, set<int> schannels = set<int> ());

    // Constructor.
    PY8MEs_R4_P73_sm_gq_llqqq(Parameters_sm * model) : pars(model) {initProc();}

    // Destructor.
    ~PY8MEs_R4_P73_sm_gq_llqqq(); 

    // Initialize process.
    virtual void initProc(); 

    // Calculate squared ME.
    virtual double sigmaKin(); 

    // Info on the subprocess.
    virtual string name() const {return "gq_llqqq (sm)";}

    virtual int code() const {return 10473;}

    virtual string inFlux() const {return "qg";}

    virtual vector<double> getMasses(); 

    virtual void setMasses(vec_double external_masses); 
    // Set all values of the external masses to an integer mode:
    // 0 : Mass taken from the model
    // 1 : Mass taken from p_i^2 if not massless to begin with
    // 2 : Mass always taken from p_i^2.
    virtual void setExternalMassesMode(int mode); 

    // Synchronize local variables of the process that depend on the model
    // parameters
    virtual void syncProcModelParams(); 

    // Tell Pythia that sigmaHat returns the ME^2
    virtual bool convertM2() const {return true;}

    // Access to getPY8ME with polymorphism from a non-static context
    virtual pair < vector<int> , int > getPY8ME(vector<int> initial_pdgs,
        vector<int> final_pdgs, set<int> schannels = set<int> ())
    {
      return static_getPY8ME(initial_pdgs, final_pdgs, schannels); 
    }

    // Set momenta
    virtual void setMomenta(vector < vec_double > momenta_picked); 

    // Set color configuration to use. An empty vector means sum over all.
    virtual void setColors(vector<int> colors_picked); 

    // Set the helicity configuration to use. Am empty vector means sum over
    // all.
    virtual void setHelicities(vector<int> helicities_picked); 

    // Set the permutation to use (will apply to momenta, colors and helicities)
    virtual void setPermutation(vector<int> perm_picked); 

    // Set the proc_ID to use
    virtual void setProcID(int procID_picked); 

    // Access to all the helicity and color configurations for a given process
    virtual vector < vec_int > getColorConfigs(int specify_proc_ID = -1,
        vector<int> permutation = vector<int> ());
    virtual vector < vec_int > getHelicityConfigs(vector<int> permutation =
        vector<int> ());

    // Maps of Helicity <-> hel_ID and ColorConfig <-> colorConfig_ID.
    virtual vector<int> getHelicityConfigForID(int hel_ID, vector<int>
        permutation = vector<int> ());
    virtual int getHelicityIDForConfig(vector<int> hel_config, vector<int>
        permutation = vector<int> ());
    virtual vector<int> getColorConfigForID(int color_ID, int specify_proc_ID =
        -1, vector<int> permutation = vector<int> ());
    virtual int getColorIDForConfig(vector<int> color_config, int
        specify_proc_ID = -1, vector<int> permutation = vector<int> ());
    virtual int getColorFlowRelativeNCPower(int color_flow_ID, int
        specify_proc_ID = -1);

    // Access previously computed results
    virtual vector < vec_double > getAllResults(int specify_proc_ID = -1); 
    virtual double getResult(int helicity_ID, int color_ID, int specify_proc_ID
        = -1);

    // Accessors
    Parameters_sm * getModel() {return pars;}
    void setModel(Parameters_sm * model) {pars = model;}

    // Invert the permutation mapping
    vector<int> invert_mapping(vector<int> mapping); 

    // Control whether to include the symmetry factors or not
    virtual void setIncludeSymmetryFactors(bool OnOff) 
    {
      include_symmetry_factors = OnOff; 
    }
    virtual bool getIncludeSymmetryFactors() {return include_symmetry_factors;}
    virtual int getSymmetryFactor() {return denom_iden[proc_ID];}

    // Control whether to include helicity averaging factors or not
    virtual void setIncludeHelicityAveragingFactors(bool OnOff) 
    {
      include_helicity_averaging_factors = OnOff; 
    }
    virtual bool getIncludeHelicityAveragingFactors() 
    {
      return include_helicity_averaging_factors; 
    }
    virtual int getHelicityAveragingFactor() {return denom_hels[proc_ID];}

    // Control whether to include color averaging factors or not
    virtual void setIncludeColorAveragingFactors(bool OnOff) 
    {
      include_color_averaging_factors = OnOff; 
    }
    virtual bool getIncludeColorAveragingFactors() 
    {
      return include_color_averaging_factors; 
    }
    virtual int getColorAveragingFactor() {return denom_colors[proc_ID];}

  private:

    // Private functions to calculate the matrix element for all subprocesses
    // Calculate wavefunctions
    void calculate_wavefunctions(const int hel[]); 
    static const int nwavefuncs = 163; 
    Complex<double> w[nwavefuncs][18]; 
    static const int namplitudes = 1534; 
    Complex<double> amp[namplitudes]; 
    double matrix_4_gu_epemuuux(); 
    double matrix_4_gd_epemdddx(); 
    double matrix_4_gux_epemuuxux(); 
    double matrix_4_gdx_epemddxdx(); 
    double matrix_4_gu_epemuccx(); 
    double matrix_4_gu_epemuddx(); 
    double matrix_4_gu_vexveuuux(); 
    double matrix_4_gd_epemudux(); 
    double matrix_4_gd_epemdssx(); 
    double matrix_4_gd_vexvedddx(); 
    double matrix_4_gux_epemcuxcx(); 
    double matrix_4_gux_epemduxdx(); 
    double matrix_4_gux_vexveuuxux(); 
    double matrix_4_gdx_epemuuxdx(); 
    double matrix_4_gdx_epemsdxsx(); 
    double matrix_4_gdx_vexveddxdx(); 
    double matrix_4_gu_epveudux(); 
    double matrix_4_gu_epvedddx(); 
    double matrix_4_gu_vexemuudx(); 
    double matrix_4_gu_vexveuccx(); 
    double matrix_4_gu_vexveuddx(); 
    double matrix_4_gd_epveddux(); 
    double matrix_4_gd_vexemuuux(); 
    double matrix_4_gd_vexemuddx(); 
    double matrix_4_gd_vexveudux(); 
    double matrix_4_gd_vexvedssx(); 
    double matrix_4_gux_epveduxux(); 
    double matrix_4_gux_vexemuuxdx(); 
    double matrix_4_gux_vexemddxdx(); 
    double matrix_4_gux_vexvecuxcx(); 
    double matrix_4_gux_vexveduxdx(); 
    double matrix_4_gdx_epveuuxux(); 
    double matrix_4_gdx_epveduxdx(); 
    double matrix_4_gdx_vexemudxdx(); 
    double matrix_4_gdx_vexveuuxdx(); 
    double matrix_4_gdx_vexvesdxsx(); 
    double matrix_4_gu_epveuscx(); 
    double matrix_4_gu_epvecdcx(); 
    double matrix_4_gu_vexemucsx(); 
    double matrix_4_gd_vexemuccx(); 
    double matrix_4_gux_epvesuxcx(); 
    double matrix_4_gux_vexemcuxsx(); 
    double matrix_4_gux_vexemccxdx(); 
    double matrix_4_gdx_epvecuxcx(); 

    // Constants for array limits
    static const int nexternal = 7; 
    static const int ninitial = 2; 
    static const int nprocesses = 44; 
    static const int nreq_s_channels = 0; 
    static const int ncomb = 128; 

    // Helicities for the process
    static int helicities[ncomb][nexternal]; 

    // Normalization factors the various processes
    static int denom_colors[nprocesses]; 
    static int denom_hels[nprocesses]; 
    static int denom_iden[nprocesses]; 

    // Control whether to include symmetry factors or not
    bool include_symmetry_factors; 
    // Control whether to include helicity averaging factors or not
    bool include_helicity_averaging_factors; 
    // Control whether to include color averaging factors or not
    bool include_color_averaging_factors; 

    // Color flows, used when selecting color
    vector < vec_double > jamp2; 

    // Store individual results (for each color flow, helicity configurations
    // and proc_ID)
    // computed in the last call to sigmaKin().
    vector < vec_vec_double > all_results; 

    // required s-channels specified
    static std::set<int> s_channel_proc; 

    // vector with external particle masses
    vector<double> mME; 

    // vector with momenta (to be changed for each event)
    vector < double * > p; 

    // external particles permutation (to be changed for each event)
    vector<int> perm; 

    // vector with colors (to be changed for each event)
    vector<int> user_colors; 

    // vector with helicities (to be changed for each event)
    vector<int> user_helicities; 

    // Process ID (to be changed for each event)
    int proc_ID; 

    // All color configurations
    void initColorConfigs(); 
    vector < vec_vec_int > color_configs; 

    // Color flows relative N_c power (conventions are such that all elements
    // on the color matrix diagonal are identical).
    vector < vec_int > jamp_nc_relative_power; 

    // Model pointer to be used by this matrix element
    Parameters_sm * pars; 

}; 

}  // end namespace PY8MEs_namespace

#endif  // PY8MEs_R4_P73_sm_gq_llqqq_H

