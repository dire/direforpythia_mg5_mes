//==========================================================================
// This file has been automatically generated for Pythia 8 by
// MadGraph5_aMC@NLO v. 2.6.0, 2017-08-16
// By the MadGraph5_aMC@NLO Development Team
// Visit launchpad.net/madgraph5 and amcatnlo.web.cern.ch
//==========================================================================

#include "PY8MEs_R4_P69_sm_gb_llggb.h"
#include "HelAmps_sm.h"

using namespace Pythia8_sm; 

namespace PY8MEs_namespace 
{
//==========================================================================
// Class member functions for calculating the matrix elements for
// Process: g b > e+ e- g g b WEIGHTED<=7 @4
// Process: g b > mu+ mu- g g b WEIGHTED<=7 @4
// Process: g b~ > e+ e- g g b~ WEIGHTED<=7 @4
// Process: g b~ > mu+ mu- g g b~ WEIGHTED<=7 @4
// Process: g b > ve~ ve g g b WEIGHTED<=7 @4
// Process: g b > vm~ vm g g b WEIGHTED<=7 @4
// Process: g b > vt~ vt g g b WEIGHTED<=7 @4
// Process: g b~ > ve~ ve g g b~ WEIGHTED<=7 @4
// Process: g b~ > vm~ vm g g b~ WEIGHTED<=7 @4
// Process: g b~ > vt~ vt g g b~ WEIGHTED<=7 @4

// Exception class
class PY8MEs_R4_P69_sm_gb_llggbException : public exception
{
  virtual const char * what() const throw()
  {
    return "Exception in class 'PY8MEs_R4_P69_sm_gb_llggb'."; 
  }
}
PY8MEs_R4_P69_sm_gb_llggb_exception; 

std::set<int> PY8MEs_R4_P69_sm_gb_llggb::s_channel_proc = std::set<int>
    (createset<int> ());

int PY8MEs_R4_P69_sm_gb_llggb::helicities[ncomb][nexternal] = {{-1, -1, -1, -1,
    -1, -1, -1}, {-1, -1, -1, -1, -1, -1, 1}, {-1, -1, -1, -1, -1, 1, -1}, {-1,
    -1, -1, -1, -1, 1, 1}, {-1, -1, -1, -1, 1, -1, -1}, {-1, -1, -1, -1, 1, -1,
    1}, {-1, -1, -1, -1, 1, 1, -1}, {-1, -1, -1, -1, 1, 1, 1}, {-1, -1, -1, 1,
    -1, -1, -1}, {-1, -1, -1, 1, -1, -1, 1}, {-1, -1, -1, 1, -1, 1, -1}, {-1,
    -1, -1, 1, -1, 1, 1}, {-1, -1, -1, 1, 1, -1, -1}, {-1, -1, -1, 1, 1, -1,
    1}, {-1, -1, -1, 1, 1, 1, -1}, {-1, -1, -1, 1, 1, 1, 1}, {-1, -1, 1, -1,
    -1, -1, -1}, {-1, -1, 1, -1, -1, -1, 1}, {-1, -1, 1, -1, -1, 1, -1}, {-1,
    -1, 1, -1, -1, 1, 1}, {-1, -1, 1, -1, 1, -1, -1}, {-1, -1, 1, -1, 1, -1,
    1}, {-1, -1, 1, -1, 1, 1, -1}, {-1, -1, 1, -1, 1, 1, 1}, {-1, -1, 1, 1, -1,
    -1, -1}, {-1, -1, 1, 1, -1, -1, 1}, {-1, -1, 1, 1, -1, 1, -1}, {-1, -1, 1,
    1, -1, 1, 1}, {-1, -1, 1, 1, 1, -1, -1}, {-1, -1, 1, 1, 1, -1, 1}, {-1, -1,
    1, 1, 1, 1, -1}, {-1, -1, 1, 1, 1, 1, 1}, {-1, 1, -1, -1, -1, -1, -1}, {-1,
    1, -1, -1, -1, -1, 1}, {-1, 1, -1, -1, -1, 1, -1}, {-1, 1, -1, -1, -1, 1,
    1}, {-1, 1, -1, -1, 1, -1, -1}, {-1, 1, -1, -1, 1, -1, 1}, {-1, 1, -1, -1,
    1, 1, -1}, {-1, 1, -1, -1, 1, 1, 1}, {-1, 1, -1, 1, -1, -1, -1}, {-1, 1,
    -1, 1, -1, -1, 1}, {-1, 1, -1, 1, -1, 1, -1}, {-1, 1, -1, 1, -1, 1, 1},
    {-1, 1, -1, 1, 1, -1, -1}, {-1, 1, -1, 1, 1, -1, 1}, {-1, 1, -1, 1, 1, 1,
    -1}, {-1, 1, -1, 1, 1, 1, 1}, {-1, 1, 1, -1, -1, -1, -1}, {-1, 1, 1, -1,
    -1, -1, 1}, {-1, 1, 1, -1, -1, 1, -1}, {-1, 1, 1, -1, -1, 1, 1}, {-1, 1, 1,
    -1, 1, -1, -1}, {-1, 1, 1, -1, 1, -1, 1}, {-1, 1, 1, -1, 1, 1, -1}, {-1, 1,
    1, -1, 1, 1, 1}, {-1, 1, 1, 1, -1, -1, -1}, {-1, 1, 1, 1, -1, -1, 1}, {-1,
    1, 1, 1, -1, 1, -1}, {-1, 1, 1, 1, -1, 1, 1}, {-1, 1, 1, 1, 1, -1, -1},
    {-1, 1, 1, 1, 1, -1, 1}, {-1, 1, 1, 1, 1, 1, -1}, {-1, 1, 1, 1, 1, 1, 1},
    {1, -1, -1, -1, -1, -1, -1}, {1, -1, -1, -1, -1, -1, 1}, {1, -1, -1, -1,
    -1, 1, -1}, {1, -1, -1, -1, -1, 1, 1}, {1, -1, -1, -1, 1, -1, -1}, {1, -1,
    -1, -1, 1, -1, 1}, {1, -1, -1, -1, 1, 1, -1}, {1, -1, -1, -1, 1, 1, 1}, {1,
    -1, -1, 1, -1, -1, -1}, {1, -1, -1, 1, -1, -1, 1}, {1, -1, -1, 1, -1, 1,
    -1}, {1, -1, -1, 1, -1, 1, 1}, {1, -1, -1, 1, 1, -1, -1}, {1, -1, -1, 1, 1,
    -1, 1}, {1, -1, -1, 1, 1, 1, -1}, {1, -1, -1, 1, 1, 1, 1}, {1, -1, 1, -1,
    -1, -1, -1}, {1, -1, 1, -1, -1, -1, 1}, {1, -1, 1, -1, -1, 1, -1}, {1, -1,
    1, -1, -1, 1, 1}, {1, -1, 1, -1, 1, -1, -1}, {1, -1, 1, -1, 1, -1, 1}, {1,
    -1, 1, -1, 1, 1, -1}, {1, -1, 1, -1, 1, 1, 1}, {1, -1, 1, 1, -1, -1, -1},
    {1, -1, 1, 1, -1, -1, 1}, {1, -1, 1, 1, -1, 1, -1}, {1, -1, 1, 1, -1, 1,
    1}, {1, -1, 1, 1, 1, -1, -1}, {1, -1, 1, 1, 1, -1, 1}, {1, -1, 1, 1, 1, 1,
    -1}, {1, -1, 1, 1, 1, 1, 1}, {1, 1, -1, -1, -1, -1, -1}, {1, 1, -1, -1, -1,
    -1, 1}, {1, 1, -1, -1, -1, 1, -1}, {1, 1, -1, -1, -1, 1, 1}, {1, 1, -1, -1,
    1, -1, -1}, {1, 1, -1, -1, 1, -1, 1}, {1, 1, -1, -1, 1, 1, -1}, {1, 1, -1,
    -1, 1, 1, 1}, {1, 1, -1, 1, -1, -1, -1}, {1, 1, -1, 1, -1, -1, 1}, {1, 1,
    -1, 1, -1, 1, -1}, {1, 1, -1, 1, -1, 1, 1}, {1, 1, -1, 1, 1, -1, -1}, {1,
    1, -1, 1, 1, -1, 1}, {1, 1, -1, 1, 1, 1, -1}, {1, 1, -1, 1, 1, 1, 1}, {1,
    1, 1, -1, -1, -1, -1}, {1, 1, 1, -1, -1, -1, 1}, {1, 1, 1, -1, -1, 1, -1},
    {1, 1, 1, -1, -1, 1, 1}, {1, 1, 1, -1, 1, -1, -1}, {1, 1, 1, -1, 1, -1, 1},
    {1, 1, 1, -1, 1, 1, -1}, {1, 1, 1, -1, 1, 1, 1}, {1, 1, 1, 1, -1, -1, -1},
    {1, 1, 1, 1, -1, -1, 1}, {1, 1, 1, 1, -1, 1, -1}, {1, 1, 1, 1, -1, 1, 1},
    {1, 1, 1, 1, 1, -1, -1}, {1, 1, 1, 1, 1, -1, 1}, {1, 1, 1, 1, 1, 1, -1},
    {1, 1, 1, 1, 1, 1, 1}};

// Normalization factors the various processes
// Denominators: spins, colors and identical particles
int PY8MEs_R4_P69_sm_gb_llggb::denom_colors[nprocesses] = {24, 24, 24, 24}; 
int PY8MEs_R4_P69_sm_gb_llggb::denom_hels[nprocesses] = {4, 4, 4, 4}; 
int PY8MEs_R4_P69_sm_gb_llggb::denom_iden[nprocesses] = {2, 2, 2, 2}; 

//--------------------------------------------------------------------------
// Color config initialization
void PY8MEs_R4_P69_sm_gb_llggb::initColorConfigs() 
{
  color_configs = vector < vec_vec_int > (); 
  jamp_nc_relative_power = vector < vec_int > (); 

  // Color flows of process Process: g b > e+ e- g g b WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(2)(4)(0)(0)(0)(0)(0)(3)(2)(4)(3)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #1
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(2)(3)(0)(0)(0)(0)(0)(3)(4)(4)(2)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #2
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(2)(4)(0)(0)(0)(0)(0)(3)(1)(4)(2)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #3
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(2)(2)(0)(0)(0)(0)(0)(3)(1)(4)(3)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #4
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(2)(3)(0)(0)(0)(0)(0)(3)(2)(4)(1)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #5
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(2)(2)(0)(0)(0)(0)(0)(3)(4)(4)(1)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 

  // Color flows of process Process: g b~ > e+ e- g g b~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(2)(0)(1)(0)(0)(0)(0)(3)(2)(4)(3)(0)(4)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #1
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(2)(0)(1)(0)(0)(0)(0)(3)(4)(4)(2)(0)(3)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #2
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(2)(0)(1)(0)(0)(0)(0)(3)(1)(4)(2)(0)(4)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #3
  color_configs[1].push_back(vec_int(createvector<int>
      (4)(2)(0)(1)(0)(0)(0)(0)(3)(1)(4)(3)(0)(2)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #4
  color_configs[1].push_back(vec_int(createvector<int>
      (4)(2)(0)(1)(0)(0)(0)(0)(3)(2)(4)(1)(0)(3)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #5
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(2)(0)(1)(0)(0)(0)(0)(3)(4)(4)(1)(0)(2)));
  jamp_nc_relative_power[1].push_back(0); 

  // Color flows of process Process: g b > ve~ ve g g b WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[2].push_back(vec_int(createvector<int>
      (1)(2)(4)(0)(0)(0)(0)(0)(3)(2)(4)(3)(1)(0)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #1
  color_configs[2].push_back(vec_int(createvector<int>
      (1)(2)(3)(0)(0)(0)(0)(0)(3)(4)(4)(2)(1)(0)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #2
  color_configs[2].push_back(vec_int(createvector<int>
      (3)(2)(4)(0)(0)(0)(0)(0)(3)(1)(4)(2)(1)(0)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #3
  color_configs[2].push_back(vec_int(createvector<int>
      (4)(2)(2)(0)(0)(0)(0)(0)(3)(1)(4)(3)(1)(0)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #4
  color_configs[2].push_back(vec_int(createvector<int>
      (4)(2)(3)(0)(0)(0)(0)(0)(3)(2)(4)(1)(1)(0)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #5
  color_configs[2].push_back(vec_int(createvector<int>
      (3)(2)(2)(0)(0)(0)(0)(0)(3)(4)(4)(1)(1)(0)));
  jamp_nc_relative_power[2].push_back(0); 

  // Color flows of process Process: g b~ > ve~ ve g g b~ WEIGHTED<=7 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[3].push_back(vec_int(createvector<int>
      (1)(2)(0)(1)(0)(0)(0)(0)(3)(2)(4)(3)(0)(4)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #1
  color_configs[3].push_back(vec_int(createvector<int>
      (1)(2)(0)(1)(0)(0)(0)(0)(3)(4)(4)(2)(0)(3)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #2
  color_configs[3].push_back(vec_int(createvector<int>
      (3)(2)(0)(1)(0)(0)(0)(0)(3)(1)(4)(2)(0)(4)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #3
  color_configs[3].push_back(vec_int(createvector<int>
      (4)(2)(0)(1)(0)(0)(0)(0)(3)(1)(4)(3)(0)(2)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #4
  color_configs[3].push_back(vec_int(createvector<int>
      (4)(2)(0)(1)(0)(0)(0)(0)(3)(2)(4)(1)(0)(3)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #5
  color_configs[3].push_back(vec_int(createvector<int>
      (3)(2)(0)(1)(0)(0)(0)(0)(3)(4)(4)(1)(0)(2)));
  jamp_nc_relative_power[3].push_back(0); 
}

//--------------------------------------------------------------------------
// Destructor.
PY8MEs_R4_P69_sm_gb_llggb::~PY8MEs_R4_P69_sm_gb_llggb() 
{
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    delete[] p[i]; 
    p[i] = NULL; 
  }
}

//--------------------------------------------------------------------------
// Invert the permutation mapping
vector<int> PY8MEs_R4_P69_sm_gb_llggb::invert_mapping(vector<int> mapping) 
{
  vector<int> inverted_mapping; 
  for (unsigned int i = 0; i < mapping.size(); i++ )
  {
    for (unsigned int j = 0; j < mapping.size(); j++ )
    {
      if (mapping[j] == ((int)i))
      {
        inverted_mapping.push_back(j); 
        break; 
      }
    }
  }
  return inverted_mapping; 
}

//--------------------------------------------------------------------------
// Return the list of possible helicity configurations
vector < vec_int > PY8MEs_R4_P69_sm_gb_llggb::getHelicityConfigs(vector<int>
    permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(ncomb, vector<int> (nexternal, 0)); 
  for (unsigned int ihel = 0; ihel < ncomb; ihel++ )
  {
    for(unsigned int j = 0; j < nexternal; j++ )
    {
      res[ihel][chosenPerm[j]] = helicities[ihel][j]; 
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Return the list of possible color configurations
vector < vec_int > PY8MEs_R4_P69_sm_gb_llggb::getColorConfigs(int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(color_configs[chosenProcID].size(), vector<int>
      (nexternal * 2, 0));
  for (unsigned int icol = 0; icol < color_configs[chosenProcID].size(); icol++
      )
  {
    for(unsigned int j = 0; j < (2 * nexternal); j++ )
    {
      res[icol][chosenPerm[j/2] * 2 + j%2] =
          color_configs[chosenProcID][icol][j];
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Get JAMP relative N_c power
int PY8MEs_R4_P69_sm_gb_llggb::getColorFlowRelativeNCPower(int color_flow_ID,
    int specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return jamp_nc_relative_power[chosenProcID][color_flow_ID]; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity ID -> Helicity Config
vector<int> PY8MEs_R4_P69_sm_gb_llggb::getHelicityConfigForID(int hel_ID,
    vector<int> permutation)
{
  if (hel_ID < 0 || hel_ID >= ncomb)
  {
    cerr <<  "Error in function 'getHelicityConfigForID' of class" << 
    " 'PY8MEs_R4_P69_sm_gb_llggb': Specified helicity ID '" << 
    hel_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R4_P69_sm_gb_llggb_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(nexternal, 0); 
  for (unsigned int j = 0; j < nexternal; j++ )
  {
    res[chosenPerm[j]] = helicities[hel_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity Config -> Helicity ID
int PY8MEs_R4_P69_sm_gb_llggb::getHelicityIDForConfig(vector<int> hel_config,
    vector<int> permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  int user_ihel = -1; 
  if (hel_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < ncomb; i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < nexternal; j++ )
      {
        if (helicities[i][chosenPerm[j]] != hel_config[j])
        {
          found = false; 
          break; 
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_ihel = i; 
        break; 
      }
    }
    if (user_ihel == -1)
    {
      cerr <<  "Error in function 'getHelicityIDForConfig' of class" << 
      " 'PY8MEs_R4_P69_sm_gb_llggb': Specified helicity" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R4_P69_sm_gb_llggb_exception; 
    }
  }
  return user_ihel; 
}


//--------------------------------------------------------------------------
// Implements the map Color ID -> Color Config
vector<int> PY8MEs_R4_P69_sm_gb_llggb::getColorConfigForID(int color_ID, int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < 0 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getColorConfigForID' of class" << 
    " 'PY8MEs_R4_P69_sm_gb_llggb': Specified color ID '" << 
    color_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R4_P69_sm_gb_llggb_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(color_configs[chosenProcID][color_ID].size(), 0); 
  for (unsigned int j = 0; j < (2 * nexternal); j++ )
  {
    res[chosenPerm[j/2] * 2 + j%2] = color_configs[chosenProcID][color_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Color Config -> Color ID
int PY8MEs_R4_P69_sm_gb_llggb::getColorIDForConfig(vector<int> color_config,
    int specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = -1; 
  if (color_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < color_configs[chosenProcID].size(); i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < (nexternal * 2); j++ )
      {

        // If colorless then make sure it matches
        // The little arithmetics in the color index is just
        // the permutation applies on the particle list which is
        // twice smaller since each particle can have two color indices.
        if (color_config[j] == 0)
        {
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] != 0)
          {
            found = false; 
            break; 
          }
          // Otherwise check that the color linked position matches
        }
        else
        {
          int color_linked_pos = -1; 
          // Find the other end of the line in the user color config
          for (unsigned int k = 0; k < (nexternal * 2); k++ )
          {
            if (k == j)
              continue; 
            if (color_config[j] == color_config[k])
            {
              color_linked_pos = k; 
              break; 
            }
          }
          if (color_linked_pos == -1)
          {
            cerr <<  "Error in function 'getColorIDForConfig' of class" << 
            " 'PY8MEs_R4_P69_sm_gb_llggb': A color line could " << 
            " not be closed." << endl; 
            throw PY8MEs_R4_P69_sm_gb_llggb_exception; 
          }
          // Now check whether the color line matches
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] !=
              color_configs[chosenProcID][i][chosenPerm[color_linked_pos/2] * 2
              + color_linked_pos%2])
          {
            found = false; 
            break; 
          }
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_icol = i; 
        break; 
      }
    }

    if (user_icol == -1)
    {
      cerr <<  "Error in function 'getColorIDForConfig' of class" << 
      " 'PY8MEs_R4_P69_sm_gb_llggb': Specified color" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R4_P69_sm_gb_llggb_exception; 
    }
  }
  return user_icol; 
}

//--------------------------------------------------------------------------
// Returns all result previously computed in SigmaKin
vector < vec_double > PY8MEs_R4_P69_sm_gb_llggb::getAllResults(int
    specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return all_results[chosenProcID]; 
}

//--------------------------------------------------------------------------
// Returns a result previously computed in SigmaKin for a specific helicity
// and color ID. -1 means avg and summed over that characteristic.
double PY8MEs_R4_P69_sm_gb_llggb::getResult(int helicity_ID, int color_ID, int
    specify_proc_ID)
{
  if (helicity_ID < - 1 || helicity_ID >= ncomb)
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R4_P69_sm_gb_llggb': Specified helicity ID '" << 
    helicity_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R4_P69_sm_gb_llggb_exception; 
  }
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < - 1 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R4_P69_sm_gb_llggb': Specified color ID '" << 
    color_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R4_P69_sm_gb_llggb_exception; 
  }
  return all_results[chosenProcID][helicity_ID + 1][color_ID + 1]; 
}

//--------------------------------------------------------------------------
// Check for the availability of the requested process and if available,
// If available, this returns the corresponding permutation and Proc_ID to use.
// If not available, this returns a negative Proc_ID.
pair < vector<int> , int > PY8MEs_R4_P69_sm_gb_llggb::static_getPY8ME(vector<int> initial_pdgs, vector<int> final_pdgs, set<int> schannels) 
{

  // Not available return value
  pair < vector<int> , int > NA(vector<int> (), -1); 

  // Check if s-channel requirements match
  if (nreq_s_channels > 0)
  {
    if (schannels != s_channel_proc)
      return NA; 
  }
  else
  {
    if (schannels.size() != 0)
      return NA; 
  }

  // Check number of final state particles
  if (final_pdgs.size() != (nexternal - ninitial))
    return NA; 

  // Check number of initial state particles
  if (initial_pdgs.size() != ninitial)
    return NA; 

  // List of processes available in this class
  const int nprocs = 20; 
  const int proc_IDS[nprocs] = {0, 0, 1, 1, 2, 2, 2, 3, 3, 3, 0, 0, 1, 1, 2, 2,
      2, 3, 3, 3};
  const int in_pdgs[nprocs][ninitial] = {{21, 5}, {21, 5}, {21, -5}, {21, -5},
      {21, 5}, {21, 5}, {21, 5}, {21, -5}, {21, -5}, {21, -5}, {5, 21}, {5,
      21}, {-5, 21}, {-5, 21}, {5, 21}, {5, 21}, {5, 21}, {-5, 21}, {-5, 21},
      {-5, 21}};
  const int out_pdgs[nprocs][nexternal - ninitial] = {{-11, 11, 21, 21, 5},
      {-13, 13, 21, 21, 5}, {-11, 11, 21, 21, -5}, {-13, 13, 21, 21, -5}, {-12,
      12, 21, 21, 5}, {-14, 14, 21, 21, 5}, {-16, 16, 21, 21, 5}, {-12, 12, 21,
      21, -5}, {-14, 14, 21, 21, -5}, {-16, 16, 21, 21, -5}, {-11, 11, 21, 21,
      5}, {-13, 13, 21, 21, 5}, {-11, 11, 21, 21, -5}, {-13, 13, 21, 21, -5},
      {-12, 12, 21, 21, 5}, {-14, 14, 21, 21, 5}, {-16, 16, 21, 21, 5}, {-12,
      12, 21, 21, -5}, {-14, 14, 21, 21, -5}, {-16, 16, 21, 21, -5}};

  bool in_pdgs_used[ninitial]; 
  bool out_pdgs_used[nexternal - ninitial]; 
  for(unsigned int i = 0; i < nprocs; i++ )
  {
    int permutations[nexternal]; 

    // Reinitialize initial state look-up variables
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      in_pdgs_used[j] = false; 
      permutations[j] = -1; 
    }
    // Look for initial state matches
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      for(unsigned int k = 0; k < ninitial; k++ )
      {
        // Make sure it has not been used already
        if (in_pdgs_used[k])
          continue; 
        if (initial_pdgs[k] == in_pdgs[i][j])
        {
          permutations[j] = k; 
          in_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[ninitial - 1] == -1)
      continue; 

    // Reinitialize final state look-up variables
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      out_pdgs_used[j] = false; 
      permutations[ninitial + j] = -1; 
    }
    // Look for final state matches
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      for(unsigned int k = 0; k < (nexternal - ninitial); k++ )
      {
        // Make sure it has not been used already
        if (out_pdgs_used[k])
          continue; 
        if (final_pdgs[k] == out_pdgs[i][j])
        {
          permutations[ninitial + j] = ninitial + k; 
          out_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[ninitial + j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[nexternal - 1] == -1)
      continue; 

    // Return process found
    return pair < vector<int> , int > (vector<int> (permutations, permutations
        + nexternal), proc_IDS[i]);
  }

  // No process found
  return NA; 
}

//--------------------------------------------------------------------------
// Set momenta
void PY8MEs_R4_P69_sm_gb_llggb::setMomenta(vector < vec_double > momenta_picked)
{
  if (momenta_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setMomenta' of class" << 
    " 'PY8MEs_R4_P69_sm_gb_llggb': Incorrect number of" << 
    " momenta specified." << endl; 
    throw PY8MEs_R4_P69_sm_gb_llggb_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    if (momenta_picked[i].size() != 4)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R4_P69_sm_gb_llggb': Incorrect number of" << 
      " momenta components specified." << endl; 
      throw PY8MEs_R4_P69_sm_gb_llggb_exception; 
    }
    if (momenta_picked[i][0] < 0.0)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R4_P69_sm_gb_llggb': A momentum was specified" << 
      " with negative energy. Check conventions." << endl; 
      throw PY8MEs_R4_P69_sm_gb_llggb_exception; 
    }
    for (unsigned int j = 0; j < 4; j++ )
    {
      p[i][j] = momenta_picked[i][j]; 
    }
  }
}

//--------------------------------------------------------------------------
// Set color configuration to use. An empty vector means sum over all.
void PY8MEs_R4_P69_sm_gb_llggb::setColors(vector<int> colors_picked)
{
  if (colors_picked.size() == 0)
  {
    user_colors = vector<int> (); 
    return; 
  }
  if (colors_picked.size() != (2 * nexternal))
  {
    cerr <<  "Error in function 'setColors' of class" << 
    " 'PY8MEs_R4_P69_sm_gb_llggb': Incorrect number" << 
    " of colors specified." << endl; 
    throw PY8MEs_R4_P69_sm_gb_llggb_exception; 
  }
  user_colors = vector<int> ((2 * nexternal), 0); 
  for(unsigned int i = 0; i < (2 * nexternal); i++ )
  {
    user_colors[i] = colors_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the helicity configuration to use. Am empty vector means sum over all.
void PY8MEs_R4_P69_sm_gb_llggb::setHelicities(vector<int> helicities_picked) 
{
  if (helicities_picked.size() != nexternal)
  {
    if (helicities_picked.size() == 0)
    {
      user_helicities = vector<int> (); 
      return; 
    }
    cerr <<  "Error in function 'setHelicities' of class" << 
    " 'PY8MEs_R4_P69_sm_gb_llggb': Incorrect number" << 
    " of helicities specified." << endl; 
    throw PY8MEs_R4_P69_sm_gb_llggb_exception; 
  }
  user_helicities = vector<int> (nexternal, 0); 
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    user_helicities[i] = helicities_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the permutation to use (will apply to momenta, colors and helicities)
void PY8MEs_R4_P69_sm_gb_llggb::setPermutation(vector<int> perm_picked) 
{
  if (perm_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setPermutations' of class" << 
    " 'PY8MEs_R4_P69_sm_gb_llggb': Incorrect number" << 
    " of permutations specified." << endl; 
    throw PY8MEs_R4_P69_sm_gb_llggb_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    perm[i] = perm_picked[i]; 
  }
}

// Set the proc_ID to use
void PY8MEs_R4_P69_sm_gb_llggb::setProcID(int procID_picked) 
{
  proc_ID = procID_picked; 
}

//--------------------------------------------------------------------------
// Initialize process.

void PY8MEs_R4_P69_sm_gb_llggb::initProc() 
{

  // Initialize flags
  include_symmetry_factors = true; 
  include_helicity_averaging_factors = true; 
  include_color_averaging_factors = true; 

  // Initialize vectors.
  perm = vector<int> (nexternal, 0); 
  user_colors = vector<int> (2 * nexternal, 0); 
  user_helicities = vector<int> (nexternal, 0); 
  p = vector < double * > (); 
  for (unsigned int i = 0; i < nexternal; i++ )
  {
    p.push_back(new double[4]); 
  }
  initColorConfigs(); 
  // Synchronize local variables dependent on the model with the active model.
  mME = vector<double> (nexternal, 0.); 
  syncProcModelParams(); 
  jamp2 = vector < vec_double > (4); 
  jamp2[0] = vector<double> (6, 0.); 
  jamp2[1] = vector<double> (6, 0.); 
  jamp2[2] = vector<double> (6, 0.); 
  jamp2[3] = vector<double> (6, 0.); 
  all_results = vector < vec_vec_double > (4); 
  // The first entry is always the color or helicity avg/summed matrix element.
  all_results[0] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[1] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[2] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[3] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
}

// Synchronize local variables of the process that depend on the model
// parameters
void PY8MEs_R4_P69_sm_gb_llggb::syncProcModelParams() 
{

  // Instantiate the model class and set parameters that stay fixed during run
  mME[0] = pars->ZERO; 
  mME[1] = pars->mdl_MB; 
  mME[2] = pars->ZERO; 
  mME[3] = pars->ZERO; 
  mME[4] = pars->ZERO; 
  mME[5] = pars->ZERO; 
  mME[6] = pars->mdl_MB; 
}

//--------------------------------------------------------------------------
// Setter allowing to force particular values for the external masses
void PY8MEs_R4_P69_sm_gb_llggb::setMasses(vec_double external_masses) 
{

  if (external_masses.size() != mME.size())
  {
    cerr <<  "Error in function 'setMasses' of class" << 
    " 'PY8MEs_R4_P69_sm_gb_llggb': Incorrect number of" << 
    " masses specified." << endl; 
    throw PY8MEs_R4_P69_sm_gb_llggb_exception; 
  }
  for (unsigned int j = 0; j < mME.size(); j++ )
  {
    mME[j] = external_masses[perm[j]]; 
  }
}

//--------------------------------------------------------------------------
// Getter accessing external masses with the correct ordering
vector<double> PY8MEs_R4_P69_sm_gb_llggb::getMasses() 
{

  vec_double external_masses; 
  vector<int> invertedPerm; 
  invertedPerm = invert_mapping(perm); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    external_masses.push_back(mME[invertedPerm[i]]); 
  }
  return external_masses; 

}


// Set all values of the external masses to float(-mode) where mode can be
// 0 : Mass taken from the model
// 1 : Mass taken from p_i^2 if not massless to begin with
// 2 : Mass always taken from p_i^2.
void PY8MEs_R4_P69_sm_gb_llggb::setExternalMassesMode(int mode) 
{
  if (mode != 0 && mode != 1 && mode != 2)
  {
    cerr <<  "Error in function 'setExternalMassesMode' of class" << 
    " 'PY8MEs_R4_P69_sm_gb_llggb': Incorrect mode selected :" << mode << 
    ". It must be either 0, 1 or 2" << endl; 
    throw PY8MEs_R4_P69_sm_gb_llggb_exception; 
  }
  if (mode == 0)
  {
    syncProcModelParams(); 
  }
  else if (mode == 1)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      if (mME[j] != pars->ZERO)
      {
        mME[j] = -1.0; 
      }
    }
  }
  else if (mode == 2)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      mME[j] = -1.0; 
    }
  }
}

//--------------------------------------------------------------------------
// Evaluate the squared matrix element.

double PY8MEs_R4_P69_sm_gb_llggb::sigmaKin() 
{
  // Set the parameters which change event by event
  pars->setDependentParameters(); 
  pars->setDependentCouplings(); 
  // Reset color flows
  for(int i = 0; i < 6; i++ )
    jamp2[0][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[1][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[2][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[3][i] = 0.; 

  // Save previous values of mME
  vector<double> saved_mME(mME.size(), 0.0); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (mME[i] < 0.0)
    {
      saved_mME[i] = mME[i]; 
      mME[i] = sqrt(abs(pow(p[perm[i]][0], 2) - 
      (pow(p[perm[i]][1], 2) + pow(p[perm[i]][2], 2) + pow(p[perm[i]][3],
          2))));
    }
  }

  // Local variables and constants
  const int max_tries = 10; 
  vector < vec_bool > goodhel(nprocesses, vec_bool(ncomb, false)); 
  vec_int ntry(nprocesses, 0); 
  double t = 0.; 
  double result = 0.; 

  if (ntry[proc_ID] <= max_tries)
    ntry[proc_ID] = ntry[proc_ID] + 1; 

  // Find which helicity configuration is asked for
  // -1 indicates one wants to sum over helicities
  int user_ihel = getHelicityIDForConfig(user_helicities); 

  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = getColorIDForConfig(user_colors); 

  // Reset the list of results that will be recomputed here
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      all_results[proc_ID][ihel + 1][icolor + 1] = 0.; 
    }
  }

  // Calculate the matrix element for all helicities
  // unless already detected as vanishing
  for(int ihel = 0; ihel < ncomb; ihel++ )
  {
    // Skip helicity if already detected as vanishing
    if ((ntry[proc_ID] >= max_tries) && !goodhel[proc_ID][ihel])
      continue; 

    // Also skip helicity if user asks for a specific one
    if ((ntry[proc_ID] >= max_tries) && user_ihel != -1 && user_ihel != ihel)
      continue; 

    calculate_wavefunctions(helicities[ihel]); 

    // Reset locally computed color flows
    for(int i = 0; i < 6; i++ )
      jamp2[0][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[1][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[2][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[3][i] = 0.; 

    if (proc_ID == 0)
      t = matrix_4_gb_epemggb(); 
    if (proc_ID == 1)
      t = matrix_4_gbx_epemggbx(); 
    if (proc_ID == 2)
      t = matrix_4_gb_vexveggb(); 
    if (proc_ID == 3)
      t = matrix_4_gbx_vexveggbx(); 

    // Store which helicities give non-zero result
    if ((ntry[proc_ID] < max_tries) && t != 0. && !goodhel[proc_ID][ihel])
      goodhel[proc_ID][ihel] = true; 

    // Aggregate results
    if (user_ihel == -1 || user_ihel == ihel)
    {
      if (user_icol == -1)
      {
        result = result + t; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][0] += t; 
          for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
          {
            all_results[proc_ID][0][i + 1] += jamp2[proc_ID][i]; 
          }
        }
        all_results[proc_ID][ihel + 1][0] += t; 
        for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
        {
          all_results[proc_ID][ihel + 1][i + 1] += jamp2[proc_ID][i]; 
        }
      }
      else
      {
        result = result + jamp2[proc_ID][user_icol]; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][user_icol + 1] += jamp2[proc_ID][user_icol]; 
        }
        all_results[proc_ID][ihel + 1][user_icol + 1] +=
            jamp2[proc_ID][user_icol];
      }
    }

  }

  // Normalize results with the identical particle factor
  if (include_symmetry_factors)
  {
    result = result/denom_iden[proc_ID]; 
  }
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      if (include_symmetry_factors)
      {
        all_results[proc_ID][ihel + 1][icolor + 1] /= denom_iden[proc_ID]; 
      }
    }
  }


  // Normalize when when summing+averaging over helicity configurations
  if (user_ihel == -1 && include_helicity_averaging_factors)
  {
    result /= denom_hels[proc_ID]; 
    if (user_icol == -1)
    {
      all_results[proc_ID][0][0] /= denom_hels[proc_ID]; 
      for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
      {
        all_results[proc_ID][0][i + 1] /= denom_hels[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][0][user_icol + 1] /= denom_hels[proc_ID]; 
    }
  }

  // Normalize when summing+averaging over color configurations
  if (user_icol == -1 && include_color_averaging_factors)
  {
    result /= denom_colors[proc_ID]; 
    if (user_ihel == -1)
    {
      all_results[proc_ID][0][0] /= denom_colors[proc_ID]; 
      for (unsigned int i = 0; i < ncomb; i++ )
      {
        all_results[proc_ID][i + 1][0] /= denom_colors[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][user_ihel + 1][0] /= denom_colors[proc_ID]; 
    }
  }

  // Reinstate previous values of mME
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (saved_mME[i] < 0.0)
    {
      mME[i] = saved_mME[i]; 
    }
  }

  // Finally return it
  return result; 
}

//==========================================================================
// Private class member functions

//--------------------------------------------------------------------------
// Evaluate |M|^2 for each subprocess

void PY8MEs_R4_P69_sm_gb_llggb::calculate_wavefunctions(const int hel[])
{
  // Calculate wavefunctions for all processes
  // Calculate all wavefunctions
  vxxxxx(p[perm[0]], mME[0], hel[0], -1, w[0]); 
  ixxxxx(p[perm[1]], mME[1], hel[1], +1, w[1]); 
  ixxxxx(p[perm[2]], mME[2], hel[2], -1, w[2]); 
  oxxxxx(p[perm[3]], mME[3], hel[3], +1, w[3]); 
  vxxxxx(p[perm[4]], mME[4], hel[4], +1, w[4]); 
  vxxxxx(p[perm[5]], mME[5], hel[5], +1, w[5]); 
  oxxxxx(p[perm[6]], mME[6], hel[6], +1, w[6]); 
  VVV1P0_1(w[0], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[7]); 
  FFV1P0_3(w[2], w[3], pars->GC_3, pars->ZERO, pars->ZERO, w[8]); 
  VVV1P0_1(w[7], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[9]); 
  FFV1_1(w[6], w[8], pars->GC_1, pars->mdl_MB, pars->ZERO, w[10]); 
  FFV1_2(w[1], w[8], pars->GC_1, pars->mdl_MB, pars->ZERO, w[11]); 
  FFV1_1(w[6], w[7], pars->GC_11, pars->mdl_MB, pars->ZERO, w[12]); 
  FFV1_2(w[1], w[7], pars->GC_11, pars->mdl_MB, pars->ZERO, w[13]); 
  FFV2_4_3(w[2], w[3], pars->GC_50, pars->GC_59, pars->mdl_MZ, pars->mdl_WZ,
      w[14]);
  FFV2_3_1(w[6], w[14], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[15]);
  FFV2_3_2(w[1], w[14], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[16]);
  FFV1_1(w[6], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[17]); 
  FFV1_1(w[17], w[7], pars->GC_11, pars->mdl_MB, pars->ZERO, w[18]); 
  FFV1_2(w[1], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[19]); 
  FFV1_2(w[19], w[7], pars->GC_11, pars->mdl_MB, pars->ZERO, w[20]); 
  VVV1P0_1(w[0], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[21]); 
  FFV1_1(w[6], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[22]); 
  FFV1_1(w[22], w[21], pars->GC_11, pars->mdl_MB, pars->ZERO, w[23]); 
  FFV1_2(w[1], w[21], pars->GC_11, pars->mdl_MB, pars->ZERO, w[24]); 
  FFV1_2(w[1], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[25]); 
  FFV1_2(w[25], w[21], pars->GC_11, pars->mdl_MB, pars->ZERO, w[26]); 
  FFV1_1(w[6], w[21], pars->GC_11, pars->mdl_MB, pars->ZERO, w[27]); 
  VVV1P0_1(w[21], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[28]); 
  FFV1_1(w[6], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[29]); 
  VVV1P0_1(w[4], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[30]); 
  FFV1_1(w[29], w[30], pars->GC_11, pars->mdl_MB, pars->ZERO, w[31]); 
  FFV1_1(w[29], w[8], pars->GC_1, pars->mdl_MB, pars->ZERO, w[32]); 
  FFV2_3_1(w[29], w[14], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[33]);
  FFV1_1(w[29], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[34]); 
  FFV1_1(w[29], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[35]); 
  FFV1_2(w[1], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[36]); 
  FFV1_2(w[36], w[30], pars->GC_11, pars->mdl_MB, pars->ZERO, w[37]); 
  FFV1_2(w[36], w[8], pars->GC_1, pars->mdl_MB, pars->ZERO, w[38]); 
  FFV2_3_2(w[36], w[14], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[39]);
  FFV1_2(w[36], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[40]); 
  FFV1_2(w[36], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[41]); 
  VVV1P0_1(w[0], w[30], pars->GC_10, pars->ZERO, pars->ZERO, w[42]); 
  FFV1_1(w[6], w[30], pars->GC_11, pars->mdl_MB, pars->ZERO, w[43]); 
  FFV1_2(w[1], w[30], pars->GC_11, pars->mdl_MB, pars->ZERO, w[44]); 
  FFV1_1(w[22], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[45]); 
  FFV1_1(w[22], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[46]); 
  FFV1_2(w[19], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[47]); 
  FFV1_2(w[25], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[48]); 
  FFV1_2(w[25], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[49]); 
  FFV1_1(w[17], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[50]); 
  FFV1_1(w[17], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[51]); 
  FFV1_2(w[19], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[52]); 
  VVVV1P0_1(w[0], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[53]); 
  VVVV3P0_1(w[0], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[54]); 
  VVVV4P0_1(w[0], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[55]); 
  FFV1_1(w[6], w[53], pars->GC_11, pars->mdl_MB, pars->ZERO, w[56]); 
  FFV1_1(w[6], w[54], pars->GC_11, pars->mdl_MB, pars->ZERO, w[57]); 
  FFV1_1(w[6], w[55], pars->GC_11, pars->mdl_MB, pars->ZERO, w[58]); 
  FFV1_2(w[1], w[53], pars->GC_11, pars->mdl_MB, pars->ZERO, w[59]); 
  FFV1_2(w[1], w[54], pars->GC_11, pars->mdl_MB, pars->ZERO, w[60]); 
  FFV1_2(w[1], w[55], pars->GC_11, pars->mdl_MB, pars->ZERO, w[61]); 
  oxxxxx(p[perm[1]], mME[1], hel[1], -1, w[62]); 
  ixxxxx(p[perm[6]], mME[6], hel[6], -1, w[63]); 
  FFV1_1(w[62], w[8], pars->GC_1, pars->mdl_MB, pars->ZERO, w[64]); 
  FFV1_2(w[63], w[8], pars->GC_1, pars->mdl_MB, pars->ZERO, w[65]); 
  FFV1_1(w[62], w[7], pars->GC_11, pars->mdl_MB, pars->ZERO, w[66]); 
  FFV1_2(w[63], w[7], pars->GC_11, pars->mdl_MB, pars->ZERO, w[67]); 
  FFV2_3_1(w[62], w[14], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[68]);
  FFV2_3_2(w[63], w[14], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[69]);
  FFV1_1(w[62], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[70]); 
  FFV1_1(w[70], w[7], pars->GC_11, pars->mdl_MB, pars->ZERO, w[71]); 
  FFV1_2(w[63], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[72]); 
  FFV1_2(w[72], w[7], pars->GC_11, pars->mdl_MB, pars->ZERO, w[73]); 
  FFV1_1(w[62], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[74]); 
  FFV1_1(w[74], w[21], pars->GC_11, pars->mdl_MB, pars->ZERO, w[75]); 
  FFV1_2(w[63], w[21], pars->GC_11, pars->mdl_MB, pars->ZERO, w[76]); 
  FFV1_2(w[63], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[77]); 
  FFV1_2(w[77], w[21], pars->GC_11, pars->mdl_MB, pars->ZERO, w[78]); 
  FFV1_1(w[62], w[21], pars->GC_11, pars->mdl_MB, pars->ZERO, w[79]); 
  FFV1_1(w[62], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[80]); 
  FFV1_1(w[80], w[30], pars->GC_11, pars->mdl_MB, pars->ZERO, w[81]); 
  FFV1_1(w[80], w[8], pars->GC_1, pars->mdl_MB, pars->ZERO, w[82]); 
  FFV2_3_1(w[80], w[14], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[83]);
  FFV1_1(w[80], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[84]); 
  FFV1_1(w[80], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[85]); 
  FFV1_2(w[63], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[86]); 
  FFV1_2(w[86], w[30], pars->GC_11, pars->mdl_MB, pars->ZERO, w[87]); 
  FFV1_2(w[86], w[8], pars->GC_1, pars->mdl_MB, pars->ZERO, w[88]); 
  FFV2_3_2(w[86], w[14], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[89]);
  FFV1_2(w[86], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[90]); 
  FFV1_2(w[86], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[91]); 
  FFV1_1(w[62], w[30], pars->GC_11, pars->mdl_MB, pars->ZERO, w[92]); 
  FFV1_2(w[63], w[30], pars->GC_11, pars->mdl_MB, pars->ZERO, w[93]); 
  FFV1_1(w[74], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[94]); 
  FFV1_1(w[74], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[95]); 
  FFV1_2(w[72], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[96]); 
  FFV1_2(w[77], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[97]); 
  FFV1_2(w[77], w[5], pars->GC_11, pars->mdl_MB, pars->ZERO, w[98]); 
  FFV1_1(w[70], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[99]); 
  FFV1_1(w[70], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[100]); 
  FFV1_2(w[72], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[101]); 
  FFV1_1(w[62], w[53], pars->GC_11, pars->mdl_MB, pars->ZERO, w[102]); 
  FFV1_1(w[62], w[54], pars->GC_11, pars->mdl_MB, pars->ZERO, w[103]); 
  FFV1_1(w[62], w[55], pars->GC_11, pars->mdl_MB, pars->ZERO, w[104]); 
  FFV1_2(w[63], w[53], pars->GC_11, pars->mdl_MB, pars->ZERO, w[105]); 
  FFV1_2(w[63], w[54], pars->GC_11, pars->mdl_MB, pars->ZERO, w[106]); 
  FFV1_2(w[63], w[55], pars->GC_11, pars->mdl_MB, pars->ZERO, w[107]); 
  FFV2_3(w[2], w[3], pars->GC_62, pars->mdl_MZ, pars->mdl_WZ, w[108]); 
  FFV2_3_1(w[6], w[108], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[109]);
  FFV2_3_2(w[1], w[108], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[110]);
  FFV2_3_1(w[29], w[108], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[111]);
  FFV2_3_2(w[36], w[108], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[112]);
  FFV2_3_1(w[62], w[108], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[113]);
  FFV2_3_2(w[63], w[108], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[114]);
  FFV2_3_1(w[80], w[108], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[115]);
  FFV2_3_2(w[86], w[108], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[116]);

  // Calculate all amplitudes
  // Amplitude(s) for diagram number 0
  FFV1_0(w[1], w[10], w[9], pars->GC_11, amp[0]); 
  FFV1_0(w[11], w[6], w[9], pars->GC_11, amp[1]); 
  FFV1_0(w[11], w[12], w[5], pars->GC_11, amp[2]); 
  FFV1_0(w[13], w[10], w[5], pars->GC_11, amp[3]); 
  FFV1_0(w[1], w[15], w[9], pars->GC_11, amp[4]); 
  FFV1_0(w[16], w[6], w[9], pars->GC_11, amp[5]); 
  FFV1_0(w[16], w[12], w[5], pars->GC_11, amp[6]); 
  FFV1_0(w[13], w[15], w[5], pars->GC_11, amp[7]); 
  FFV1_0(w[1], w[18], w[8], pars->GC_1, amp[8]); 
  FFV1_0(w[13], w[17], w[8], pars->GC_1, amp[9]); 
  FFV2_3_0(w[1], w[18], w[14], pars->GC_50, pars->GC_58, amp[10]); 
  FFV2_3_0(w[13], w[17], w[14], pars->GC_50, pars->GC_58, amp[11]); 
  FFV1_0(w[20], w[6], w[8], pars->GC_1, amp[12]); 
  FFV1_0(w[19], w[12], w[8], pars->GC_1, amp[13]); 
  FFV2_3_0(w[20], w[6], w[14], pars->GC_50, pars->GC_58, amp[14]); 
  FFV2_3_0(w[19], w[12], w[14], pars->GC_50, pars->GC_58, amp[15]); 
  FFV1_0(w[1], w[23], w[8], pars->GC_1, amp[16]); 
  FFV1_0(w[24], w[22], w[8], pars->GC_1, amp[17]); 
  FFV2_3_0(w[1], w[23], w[14], pars->GC_50, pars->GC_58, amp[18]); 
  FFV2_3_0(w[24], w[22], w[14], pars->GC_50, pars->GC_58, amp[19]); 
  FFV1_0(w[26], w[6], w[8], pars->GC_1, amp[20]); 
  FFV1_0(w[25], w[27], w[8], pars->GC_1, amp[21]); 
  FFV2_3_0(w[26], w[6], w[14], pars->GC_50, pars->GC_58, amp[22]); 
  FFV2_3_0(w[25], w[27], w[14], pars->GC_50, pars->GC_58, amp[23]); 
  FFV1_0(w[1], w[10], w[28], pars->GC_11, amp[24]); 
  FFV1_0(w[11], w[6], w[28], pars->GC_11, amp[25]); 
  FFV1_0(w[11], w[27], w[4], pars->GC_11, amp[26]); 
  FFV1_0(w[24], w[10], w[4], pars->GC_11, amp[27]); 
  FFV1_0(w[1], w[15], w[28], pars->GC_11, amp[28]); 
  FFV1_0(w[16], w[6], w[28], pars->GC_11, amp[29]); 
  FFV1_0(w[16], w[27], w[4], pars->GC_11, amp[30]); 
  FFV1_0(w[24], w[15], w[4], pars->GC_11, amp[31]); 
  FFV1_0(w[1], w[31], w[8], pars->GC_1, amp[32]); 
  FFV1_0(w[1], w[32], w[30], pars->GC_11, amp[33]); 
  FFV2_3_0(w[1], w[31], w[14], pars->GC_50, pars->GC_58, amp[34]); 
  FFV1_0(w[1], w[33], w[30], pars->GC_11, amp[35]); 
  FFV1_0(w[25], w[32], w[5], pars->GC_11, amp[36]); 
  FFV1_0(w[25], w[34], w[8], pars->GC_1, amp[37]); 
  FFV1_0(w[25], w[33], w[5], pars->GC_11, amp[38]); 
  FFV2_3_0(w[25], w[34], w[14], pars->GC_50, pars->GC_58, amp[39]); 
  FFV1_0(w[11], w[35], w[5], pars->GC_11, amp[40]); 
  FFV1_0(w[11], w[34], w[4], pars->GC_11, amp[41]); 
  FFV1_0(w[16], w[35], w[5], pars->GC_11, amp[42]); 
  FFV1_0(w[16], w[34], w[4], pars->GC_11, amp[43]); 
  FFV1_0(w[19], w[35], w[8], pars->GC_1, amp[44]); 
  FFV1_0(w[19], w[32], w[4], pars->GC_11, amp[45]); 
  FFV2_3_0(w[19], w[35], w[14], pars->GC_50, pars->GC_58, amp[46]); 
  FFV1_0(w[19], w[33], w[4], pars->GC_11, amp[47]); 
  FFV1_0(w[37], w[6], w[8], pars->GC_1, amp[48]); 
  FFV1_0(w[38], w[6], w[30], pars->GC_11, amp[49]); 
  FFV2_3_0(w[37], w[6], w[14], pars->GC_50, pars->GC_58, amp[50]); 
  FFV1_0(w[39], w[6], w[30], pars->GC_11, amp[51]); 
  FFV1_0(w[38], w[22], w[5], pars->GC_11, amp[52]); 
  FFV1_0(w[40], w[22], w[8], pars->GC_1, amp[53]); 
  FFV1_0(w[39], w[22], w[5], pars->GC_11, amp[54]); 
  FFV2_3_0(w[40], w[22], w[14], pars->GC_50, pars->GC_58, amp[55]); 
  FFV1_0(w[41], w[10], w[5], pars->GC_11, amp[56]); 
  FFV1_0(w[40], w[10], w[4], pars->GC_11, amp[57]); 
  FFV1_0(w[41], w[15], w[5], pars->GC_11, amp[58]); 
  FFV1_0(w[40], w[15], w[4], pars->GC_11, amp[59]); 
  FFV1_0(w[41], w[17], w[8], pars->GC_1, amp[60]); 
  FFV1_0(w[38], w[17], w[4], pars->GC_11, amp[61]); 
  FFV2_3_0(w[41], w[17], w[14], pars->GC_50, pars->GC_58, amp[62]); 
  FFV1_0(w[39], w[17], w[4], pars->GC_11, amp[63]); 
  FFV1_0(w[1], w[10], w[42], pars->GC_11, amp[64]); 
  FFV1_0(w[11], w[6], w[42], pars->GC_11, amp[65]); 
  FFV1_0(w[11], w[43], w[0], pars->GC_11, amp[66]); 
  FFV1_0(w[44], w[10], w[0], pars->GC_11, amp[67]); 
  FFV1_0(w[1], w[15], w[42], pars->GC_11, amp[68]); 
  FFV1_0(w[16], w[6], w[42], pars->GC_11, amp[69]); 
  FFV1_0(w[16], w[43], w[0], pars->GC_11, amp[70]); 
  FFV1_0(w[44], w[15], w[0], pars->GC_11, amp[71]); 
  FFV1_0(w[11], w[45], w[5], pars->GC_11, amp[72]); 
  FFV1_0(w[11], w[46], w[0], pars->GC_11, amp[73]); 
  FFV1_0(w[16], w[45], w[5], pars->GC_11, amp[74]); 
  FFV1_0(w[16], w[46], w[0], pars->GC_11, amp[75]); 
  FFV1_0(w[19], w[45], w[8], pars->GC_1, amp[76]); 
  FFV1_0(w[47], w[22], w[8], pars->GC_1, amp[77]); 
  FFV2_3_0(w[19], w[45], w[14], pars->GC_50, pars->GC_58, amp[78]); 
  FFV2_3_0(w[47], w[22], w[14], pars->GC_50, pars->GC_58, amp[79]); 
  FFV1_0(w[48], w[10], w[5], pars->GC_11, amp[80]); 
  FFV1_0(w[49], w[10], w[0], pars->GC_11, amp[81]); 
  FFV1_0(w[48], w[15], w[5], pars->GC_11, amp[82]); 
  FFV1_0(w[49], w[15], w[0], pars->GC_11, amp[83]); 
  FFV1_0(w[48], w[17], w[8], pars->GC_1, amp[84]); 
  FFV1_0(w[25], w[50], w[8], pars->GC_1, amp[85]); 
  FFV2_3_0(w[48], w[17], w[14], pars->GC_50, pars->GC_58, amp[86]); 
  FFV2_3_0(w[25], w[50], w[14], pars->GC_50, pars->GC_58, amp[87]); 
  FFV1_0(w[11], w[50], w[4], pars->GC_11, amp[88]); 
  FFV1_0(w[11], w[51], w[0], pars->GC_11, amp[89]); 
  FFV1_0(w[16], w[50], w[4], pars->GC_11, amp[90]); 
  FFV1_0(w[16], w[51], w[0], pars->GC_11, amp[91]); 
  FFV1_0(w[47], w[10], w[4], pars->GC_11, amp[92]); 
  FFV1_0(w[52], w[10], w[0], pars->GC_11, amp[93]); 
  FFV1_0(w[47], w[15], w[4], pars->GC_11, amp[94]); 
  FFV1_0(w[52], w[15], w[0], pars->GC_11, amp[95]); 
  FFV1_0(w[1], w[56], w[8], pars->GC_1, amp[96]); 
  FFV1_0(w[1], w[57], w[8], pars->GC_1, amp[97]); 
  FFV1_0(w[1], w[58], w[8], pars->GC_1, amp[98]); 
  FFV1_0(w[59], w[6], w[8], pars->GC_1, amp[99]); 
  FFV1_0(w[60], w[6], w[8], pars->GC_1, amp[100]); 
  FFV1_0(w[61], w[6], w[8], pars->GC_1, amp[101]); 
  FFV2_3_0(w[1], w[56], w[14], pars->GC_50, pars->GC_58, amp[102]); 
  FFV2_3_0(w[1], w[57], w[14], pars->GC_50, pars->GC_58, amp[103]); 
  FFV2_3_0(w[1], w[58], w[14], pars->GC_50, pars->GC_58, amp[104]); 
  FFV2_3_0(w[59], w[6], w[14], pars->GC_50, pars->GC_58, amp[105]); 
  FFV2_3_0(w[60], w[6], w[14], pars->GC_50, pars->GC_58, amp[106]); 
  FFV2_3_0(w[61], w[6], w[14], pars->GC_50, pars->GC_58, amp[107]); 
  FFV1_0(w[63], w[64], w[9], pars->GC_11, amp[108]); 
  FFV1_0(w[65], w[62], w[9], pars->GC_11, amp[109]); 
  FFV1_0(w[65], w[66], w[5], pars->GC_11, amp[110]); 
  FFV1_0(w[67], w[64], w[5], pars->GC_11, amp[111]); 
  FFV1_0(w[63], w[68], w[9], pars->GC_11, amp[112]); 
  FFV1_0(w[69], w[62], w[9], pars->GC_11, amp[113]); 
  FFV1_0(w[69], w[66], w[5], pars->GC_11, amp[114]); 
  FFV1_0(w[67], w[68], w[5], pars->GC_11, amp[115]); 
  FFV1_0(w[63], w[71], w[8], pars->GC_1, amp[116]); 
  FFV1_0(w[67], w[70], w[8], pars->GC_1, amp[117]); 
  FFV2_3_0(w[63], w[71], w[14], pars->GC_50, pars->GC_58, amp[118]); 
  FFV2_3_0(w[67], w[70], w[14], pars->GC_50, pars->GC_58, amp[119]); 
  FFV1_0(w[73], w[62], w[8], pars->GC_1, amp[120]); 
  FFV1_0(w[72], w[66], w[8], pars->GC_1, amp[121]); 
  FFV2_3_0(w[73], w[62], w[14], pars->GC_50, pars->GC_58, amp[122]); 
  FFV2_3_0(w[72], w[66], w[14], pars->GC_50, pars->GC_58, amp[123]); 
  FFV1_0(w[63], w[75], w[8], pars->GC_1, amp[124]); 
  FFV1_0(w[76], w[74], w[8], pars->GC_1, amp[125]); 
  FFV2_3_0(w[63], w[75], w[14], pars->GC_50, pars->GC_58, amp[126]); 
  FFV2_3_0(w[76], w[74], w[14], pars->GC_50, pars->GC_58, amp[127]); 
  FFV1_0(w[78], w[62], w[8], pars->GC_1, amp[128]); 
  FFV1_0(w[77], w[79], w[8], pars->GC_1, amp[129]); 
  FFV2_3_0(w[78], w[62], w[14], pars->GC_50, pars->GC_58, amp[130]); 
  FFV2_3_0(w[77], w[79], w[14], pars->GC_50, pars->GC_58, amp[131]); 
  FFV1_0(w[63], w[64], w[28], pars->GC_11, amp[132]); 
  FFV1_0(w[65], w[62], w[28], pars->GC_11, amp[133]); 
  FFV1_0(w[65], w[79], w[4], pars->GC_11, amp[134]); 
  FFV1_0(w[76], w[64], w[4], pars->GC_11, amp[135]); 
  FFV1_0(w[63], w[68], w[28], pars->GC_11, amp[136]); 
  FFV1_0(w[69], w[62], w[28], pars->GC_11, amp[137]); 
  FFV1_0(w[69], w[79], w[4], pars->GC_11, amp[138]); 
  FFV1_0(w[76], w[68], w[4], pars->GC_11, amp[139]); 
  FFV1_0(w[63], w[81], w[8], pars->GC_1, amp[140]); 
  FFV1_0(w[63], w[82], w[30], pars->GC_11, amp[141]); 
  FFV2_3_0(w[63], w[81], w[14], pars->GC_50, pars->GC_58, amp[142]); 
  FFV1_0(w[63], w[83], w[30], pars->GC_11, amp[143]); 
  FFV1_0(w[77], w[82], w[5], pars->GC_11, amp[144]); 
  FFV1_0(w[77], w[84], w[8], pars->GC_1, amp[145]); 
  FFV1_0(w[77], w[83], w[5], pars->GC_11, amp[146]); 
  FFV2_3_0(w[77], w[84], w[14], pars->GC_50, pars->GC_58, amp[147]); 
  FFV1_0(w[65], w[85], w[5], pars->GC_11, amp[148]); 
  FFV1_0(w[65], w[84], w[4], pars->GC_11, amp[149]); 
  FFV1_0(w[69], w[85], w[5], pars->GC_11, amp[150]); 
  FFV1_0(w[69], w[84], w[4], pars->GC_11, amp[151]); 
  FFV1_0(w[72], w[85], w[8], pars->GC_1, amp[152]); 
  FFV1_0(w[72], w[82], w[4], pars->GC_11, amp[153]); 
  FFV2_3_0(w[72], w[85], w[14], pars->GC_50, pars->GC_58, amp[154]); 
  FFV1_0(w[72], w[83], w[4], pars->GC_11, amp[155]); 
  FFV1_0(w[87], w[62], w[8], pars->GC_1, amp[156]); 
  FFV1_0(w[88], w[62], w[30], pars->GC_11, amp[157]); 
  FFV2_3_0(w[87], w[62], w[14], pars->GC_50, pars->GC_58, amp[158]); 
  FFV1_0(w[89], w[62], w[30], pars->GC_11, amp[159]); 
  FFV1_0(w[88], w[74], w[5], pars->GC_11, amp[160]); 
  FFV1_0(w[90], w[74], w[8], pars->GC_1, amp[161]); 
  FFV1_0(w[89], w[74], w[5], pars->GC_11, amp[162]); 
  FFV2_3_0(w[90], w[74], w[14], pars->GC_50, pars->GC_58, amp[163]); 
  FFV1_0(w[91], w[64], w[5], pars->GC_11, amp[164]); 
  FFV1_0(w[90], w[64], w[4], pars->GC_11, amp[165]); 
  FFV1_0(w[91], w[68], w[5], pars->GC_11, amp[166]); 
  FFV1_0(w[90], w[68], w[4], pars->GC_11, amp[167]); 
  FFV1_0(w[91], w[70], w[8], pars->GC_1, amp[168]); 
  FFV1_0(w[88], w[70], w[4], pars->GC_11, amp[169]); 
  FFV2_3_0(w[91], w[70], w[14], pars->GC_50, pars->GC_58, amp[170]); 
  FFV1_0(w[89], w[70], w[4], pars->GC_11, amp[171]); 
  FFV1_0(w[63], w[64], w[42], pars->GC_11, amp[172]); 
  FFV1_0(w[65], w[62], w[42], pars->GC_11, amp[173]); 
  FFV1_0(w[65], w[92], w[0], pars->GC_11, amp[174]); 
  FFV1_0(w[93], w[64], w[0], pars->GC_11, amp[175]); 
  FFV1_0(w[63], w[68], w[42], pars->GC_11, amp[176]); 
  FFV1_0(w[69], w[62], w[42], pars->GC_11, amp[177]); 
  FFV1_0(w[69], w[92], w[0], pars->GC_11, amp[178]); 
  FFV1_0(w[93], w[68], w[0], pars->GC_11, amp[179]); 
  FFV1_0(w[65], w[94], w[5], pars->GC_11, amp[180]); 
  FFV1_0(w[65], w[95], w[0], pars->GC_11, amp[181]); 
  FFV1_0(w[69], w[94], w[5], pars->GC_11, amp[182]); 
  FFV1_0(w[69], w[95], w[0], pars->GC_11, amp[183]); 
  FFV1_0(w[72], w[94], w[8], pars->GC_1, amp[184]); 
  FFV1_0(w[96], w[74], w[8], pars->GC_1, amp[185]); 
  FFV2_3_0(w[72], w[94], w[14], pars->GC_50, pars->GC_58, amp[186]); 
  FFV2_3_0(w[96], w[74], w[14], pars->GC_50, pars->GC_58, amp[187]); 
  FFV1_0(w[97], w[64], w[5], pars->GC_11, amp[188]); 
  FFV1_0(w[98], w[64], w[0], pars->GC_11, amp[189]); 
  FFV1_0(w[97], w[68], w[5], pars->GC_11, amp[190]); 
  FFV1_0(w[98], w[68], w[0], pars->GC_11, amp[191]); 
  FFV1_0(w[97], w[70], w[8], pars->GC_1, amp[192]); 
  FFV1_0(w[77], w[99], w[8], pars->GC_1, amp[193]); 
  FFV2_3_0(w[97], w[70], w[14], pars->GC_50, pars->GC_58, amp[194]); 
  FFV2_3_0(w[77], w[99], w[14], pars->GC_50, pars->GC_58, amp[195]); 
  FFV1_0(w[65], w[99], w[4], pars->GC_11, amp[196]); 
  FFV1_0(w[65], w[100], w[0], pars->GC_11, amp[197]); 
  FFV1_0(w[69], w[99], w[4], pars->GC_11, amp[198]); 
  FFV1_0(w[69], w[100], w[0], pars->GC_11, amp[199]); 
  FFV1_0(w[96], w[64], w[4], pars->GC_11, amp[200]); 
  FFV1_0(w[101], w[64], w[0], pars->GC_11, amp[201]); 
  FFV1_0(w[96], w[68], w[4], pars->GC_11, amp[202]); 
  FFV1_0(w[101], w[68], w[0], pars->GC_11, amp[203]); 
  FFV1_0(w[63], w[102], w[8], pars->GC_1, amp[204]); 
  FFV1_0(w[63], w[103], w[8], pars->GC_1, amp[205]); 
  FFV1_0(w[63], w[104], w[8], pars->GC_1, amp[206]); 
  FFV1_0(w[105], w[62], w[8], pars->GC_1, amp[207]); 
  FFV1_0(w[106], w[62], w[8], pars->GC_1, amp[208]); 
  FFV1_0(w[107], w[62], w[8], pars->GC_1, amp[209]); 
  FFV2_3_0(w[63], w[102], w[14], pars->GC_50, pars->GC_58, amp[210]); 
  FFV2_3_0(w[63], w[103], w[14], pars->GC_50, pars->GC_58, amp[211]); 
  FFV2_3_0(w[63], w[104], w[14], pars->GC_50, pars->GC_58, amp[212]); 
  FFV2_3_0(w[105], w[62], w[14], pars->GC_50, pars->GC_58, amp[213]); 
  FFV2_3_0(w[106], w[62], w[14], pars->GC_50, pars->GC_58, amp[214]); 
  FFV2_3_0(w[107], w[62], w[14], pars->GC_50, pars->GC_58, amp[215]); 
  FFV1_0(w[1], w[109], w[9], pars->GC_11, amp[216]); 
  FFV1_0(w[110], w[6], w[9], pars->GC_11, amp[217]); 
  FFV1_0(w[110], w[12], w[5], pars->GC_11, amp[218]); 
  FFV1_0(w[13], w[109], w[5], pars->GC_11, amp[219]); 
  FFV2_3_0(w[1], w[18], w[108], pars->GC_50, pars->GC_58, amp[220]); 
  FFV2_3_0(w[13], w[17], w[108], pars->GC_50, pars->GC_58, amp[221]); 
  FFV2_3_0(w[20], w[6], w[108], pars->GC_50, pars->GC_58, amp[222]); 
  FFV2_3_0(w[19], w[12], w[108], pars->GC_50, pars->GC_58, amp[223]); 
  FFV2_3_0(w[1], w[23], w[108], pars->GC_50, pars->GC_58, amp[224]); 
  FFV2_3_0(w[24], w[22], w[108], pars->GC_50, pars->GC_58, amp[225]); 
  FFV2_3_0(w[26], w[6], w[108], pars->GC_50, pars->GC_58, amp[226]); 
  FFV2_3_0(w[25], w[27], w[108], pars->GC_50, pars->GC_58, amp[227]); 
  FFV1_0(w[1], w[109], w[28], pars->GC_11, amp[228]); 
  FFV1_0(w[110], w[6], w[28], pars->GC_11, amp[229]); 
  FFV1_0(w[110], w[27], w[4], pars->GC_11, amp[230]); 
  FFV1_0(w[24], w[109], w[4], pars->GC_11, amp[231]); 
  FFV2_3_0(w[1], w[31], w[108], pars->GC_50, pars->GC_58, amp[232]); 
  FFV1_0(w[1], w[111], w[30], pars->GC_11, amp[233]); 
  FFV1_0(w[25], w[111], w[5], pars->GC_11, amp[234]); 
  FFV2_3_0(w[25], w[34], w[108], pars->GC_50, pars->GC_58, amp[235]); 
  FFV1_0(w[110], w[35], w[5], pars->GC_11, amp[236]); 
  FFV1_0(w[110], w[34], w[4], pars->GC_11, amp[237]); 
  FFV2_3_0(w[19], w[35], w[108], pars->GC_50, pars->GC_58, amp[238]); 
  FFV1_0(w[19], w[111], w[4], pars->GC_11, amp[239]); 
  FFV2_3_0(w[37], w[6], w[108], pars->GC_50, pars->GC_58, amp[240]); 
  FFV1_0(w[112], w[6], w[30], pars->GC_11, amp[241]); 
  FFV1_0(w[112], w[22], w[5], pars->GC_11, amp[242]); 
  FFV2_3_0(w[40], w[22], w[108], pars->GC_50, pars->GC_58, amp[243]); 
  FFV1_0(w[41], w[109], w[5], pars->GC_11, amp[244]); 
  FFV1_0(w[40], w[109], w[4], pars->GC_11, amp[245]); 
  FFV2_3_0(w[41], w[17], w[108], pars->GC_50, pars->GC_58, amp[246]); 
  FFV1_0(w[112], w[17], w[4], pars->GC_11, amp[247]); 
  FFV1_0(w[1], w[109], w[42], pars->GC_11, amp[248]); 
  FFV1_0(w[110], w[6], w[42], pars->GC_11, amp[249]); 
  FFV1_0(w[110], w[43], w[0], pars->GC_11, amp[250]); 
  FFV1_0(w[44], w[109], w[0], pars->GC_11, amp[251]); 
  FFV1_0(w[110], w[45], w[5], pars->GC_11, amp[252]); 
  FFV1_0(w[110], w[46], w[0], pars->GC_11, amp[253]); 
  FFV2_3_0(w[19], w[45], w[108], pars->GC_50, pars->GC_58, amp[254]); 
  FFV2_3_0(w[47], w[22], w[108], pars->GC_50, pars->GC_58, amp[255]); 
  FFV1_0(w[48], w[109], w[5], pars->GC_11, amp[256]); 
  FFV1_0(w[49], w[109], w[0], pars->GC_11, amp[257]); 
  FFV2_3_0(w[48], w[17], w[108], pars->GC_50, pars->GC_58, amp[258]); 
  FFV2_3_0(w[25], w[50], w[108], pars->GC_50, pars->GC_58, amp[259]); 
  FFV1_0(w[110], w[50], w[4], pars->GC_11, amp[260]); 
  FFV1_0(w[110], w[51], w[0], pars->GC_11, amp[261]); 
  FFV1_0(w[47], w[109], w[4], pars->GC_11, amp[262]); 
  FFV1_0(w[52], w[109], w[0], pars->GC_11, amp[263]); 
  FFV2_3_0(w[1], w[56], w[108], pars->GC_50, pars->GC_58, amp[264]); 
  FFV2_3_0(w[1], w[57], w[108], pars->GC_50, pars->GC_58, amp[265]); 
  FFV2_3_0(w[1], w[58], w[108], pars->GC_50, pars->GC_58, amp[266]); 
  FFV2_3_0(w[59], w[6], w[108], pars->GC_50, pars->GC_58, amp[267]); 
  FFV2_3_0(w[60], w[6], w[108], pars->GC_50, pars->GC_58, amp[268]); 
  FFV2_3_0(w[61], w[6], w[108], pars->GC_50, pars->GC_58, amp[269]); 
  FFV1_0(w[63], w[113], w[9], pars->GC_11, amp[270]); 
  FFV1_0(w[114], w[62], w[9], pars->GC_11, amp[271]); 
  FFV1_0(w[114], w[66], w[5], pars->GC_11, amp[272]); 
  FFV1_0(w[67], w[113], w[5], pars->GC_11, amp[273]); 
  FFV2_3_0(w[63], w[71], w[108], pars->GC_50, pars->GC_58, amp[274]); 
  FFV2_3_0(w[67], w[70], w[108], pars->GC_50, pars->GC_58, amp[275]); 
  FFV2_3_0(w[73], w[62], w[108], pars->GC_50, pars->GC_58, amp[276]); 
  FFV2_3_0(w[72], w[66], w[108], pars->GC_50, pars->GC_58, amp[277]); 
  FFV2_3_0(w[63], w[75], w[108], pars->GC_50, pars->GC_58, amp[278]); 
  FFV2_3_0(w[76], w[74], w[108], pars->GC_50, pars->GC_58, amp[279]); 
  FFV2_3_0(w[78], w[62], w[108], pars->GC_50, pars->GC_58, amp[280]); 
  FFV2_3_0(w[77], w[79], w[108], pars->GC_50, pars->GC_58, amp[281]); 
  FFV1_0(w[63], w[113], w[28], pars->GC_11, amp[282]); 
  FFV1_0(w[114], w[62], w[28], pars->GC_11, amp[283]); 
  FFV1_0(w[114], w[79], w[4], pars->GC_11, amp[284]); 
  FFV1_0(w[76], w[113], w[4], pars->GC_11, amp[285]); 
  FFV2_3_0(w[63], w[81], w[108], pars->GC_50, pars->GC_58, amp[286]); 
  FFV1_0(w[63], w[115], w[30], pars->GC_11, amp[287]); 
  FFV1_0(w[77], w[115], w[5], pars->GC_11, amp[288]); 
  FFV2_3_0(w[77], w[84], w[108], pars->GC_50, pars->GC_58, amp[289]); 
  FFV1_0(w[114], w[85], w[5], pars->GC_11, amp[290]); 
  FFV1_0(w[114], w[84], w[4], pars->GC_11, amp[291]); 
  FFV2_3_0(w[72], w[85], w[108], pars->GC_50, pars->GC_58, amp[292]); 
  FFV1_0(w[72], w[115], w[4], pars->GC_11, amp[293]); 
  FFV2_3_0(w[87], w[62], w[108], pars->GC_50, pars->GC_58, amp[294]); 
  FFV1_0(w[116], w[62], w[30], pars->GC_11, amp[295]); 
  FFV1_0(w[116], w[74], w[5], pars->GC_11, amp[296]); 
  FFV2_3_0(w[90], w[74], w[108], pars->GC_50, pars->GC_58, amp[297]); 
  FFV1_0(w[91], w[113], w[5], pars->GC_11, amp[298]); 
  FFV1_0(w[90], w[113], w[4], pars->GC_11, amp[299]); 
  FFV2_3_0(w[91], w[70], w[108], pars->GC_50, pars->GC_58, amp[300]); 
  FFV1_0(w[116], w[70], w[4], pars->GC_11, amp[301]); 
  FFV1_0(w[63], w[113], w[42], pars->GC_11, amp[302]); 
  FFV1_0(w[114], w[62], w[42], pars->GC_11, amp[303]); 
  FFV1_0(w[114], w[92], w[0], pars->GC_11, amp[304]); 
  FFV1_0(w[93], w[113], w[0], pars->GC_11, amp[305]); 
  FFV1_0(w[114], w[94], w[5], pars->GC_11, amp[306]); 
  FFV1_0(w[114], w[95], w[0], pars->GC_11, amp[307]); 
  FFV2_3_0(w[72], w[94], w[108], pars->GC_50, pars->GC_58, amp[308]); 
  FFV2_3_0(w[96], w[74], w[108], pars->GC_50, pars->GC_58, amp[309]); 
  FFV1_0(w[97], w[113], w[5], pars->GC_11, amp[310]); 
  FFV1_0(w[98], w[113], w[0], pars->GC_11, amp[311]); 
  FFV2_3_0(w[97], w[70], w[108], pars->GC_50, pars->GC_58, amp[312]); 
  FFV2_3_0(w[77], w[99], w[108], pars->GC_50, pars->GC_58, amp[313]); 
  FFV1_0(w[114], w[99], w[4], pars->GC_11, amp[314]); 
  FFV1_0(w[114], w[100], w[0], pars->GC_11, amp[315]); 
  FFV1_0(w[96], w[113], w[4], pars->GC_11, amp[316]); 
  FFV1_0(w[101], w[113], w[0], pars->GC_11, amp[317]); 
  FFV2_3_0(w[63], w[102], w[108], pars->GC_50, pars->GC_58, amp[318]); 
  FFV2_3_0(w[63], w[103], w[108], pars->GC_50, pars->GC_58, amp[319]); 
  FFV2_3_0(w[63], w[104], w[108], pars->GC_50, pars->GC_58, amp[320]); 
  FFV2_3_0(w[105], w[62], w[108], pars->GC_50, pars->GC_58, amp[321]); 
  FFV2_3_0(w[106], w[62], w[108], pars->GC_50, pars->GC_58, amp[322]); 
  FFV2_3_0(w[107], w[62], w[108], pars->GC_50, pars->GC_58, amp[323]); 


}
double PY8MEs_R4_P69_sm_gb_llggb::matrix_4_gb_epemggb() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 108;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = -amp[0] - amp[1] - Complex<double> (0, 1) * amp[2] - amp[4] -
      amp[5] - Complex<double> (0, 1) * amp[6] - Complex<double> (0, 1) *
      amp[12] - Complex<double> (0, 1) * amp[13] - Complex<double> (0, 1) *
      amp[14] - Complex<double> (0, 1) * amp[15] - Complex<double> (0, 1) *
      amp[32] - Complex<double> (0, 1) * amp[33] - Complex<double> (0, 1) *
      amp[34] - Complex<double> (0, 1) * amp[35] + amp[40] + amp[42] + amp[44]
      + amp[45] + amp[46] + amp[47] - amp[64] - amp[65] - Complex<double> (0,
      1) * amp[67] - amp[68] - amp[69] - Complex<double> (0, 1) * amp[71] +
      amp[93] + amp[95] - amp[96] + amp[98] - amp[99] + amp[101] - amp[102] +
      amp[104] - amp[105] + amp[107];
  jamp[1] = -Complex<double> (0, 1) * amp[20] - Complex<double> (0, 1) *
      amp[21] - Complex<double> (0, 1) * amp[22] - Complex<double> (0, 1) *
      amp[23] - amp[24] - amp[25] - Complex<double> (0, 1) * amp[26] - amp[28]
      - amp[29] - Complex<double> (0, 1) * amp[30] + Complex<double> (0, 1) *
      amp[32] + Complex<double> (0, 1) * amp[33] + Complex<double> (0, 1) *
      amp[34] + Complex<double> (0, 1) * amp[35] + amp[36] + amp[37] + amp[38]
      + amp[39] + amp[41] + amp[43] + amp[64] + amp[65] + Complex<double> (0,
      1) * amp[67] + amp[68] + amp[69] + Complex<double> (0, 1) * amp[71] +
      amp[81] + amp[83] + amp[96] + amp[97] + amp[99] + amp[100] + amp[102] +
      amp[103] + amp[105] + amp[106];
  jamp[2] = +amp[0] + amp[1] + Complex<double> (0, 1) * amp[2] + amp[4] +
      amp[5] + Complex<double> (0, 1) * amp[6] + Complex<double> (0, 1) *
      amp[12] + Complex<double> (0, 1) * amp[13] + Complex<double> (0, 1) *
      amp[14] + Complex<double> (0, 1) * amp[15] - Complex<double> (0, 1) *
      amp[16] - Complex<double> (0, 1) * amp[17] - Complex<double> (0, 1) *
      amp[18] - Complex<double> (0, 1) * amp[19] + amp[24] + amp[25] -
      Complex<double> (0, 1) * amp[27] + amp[28] + amp[29] - Complex<double>
      (0, 1) * amp[31] + amp[72] + amp[74] + amp[76] + amp[77] + amp[78] +
      amp[79] + amp[92] + amp[94] - amp[97] - amp[98] - amp[100] - amp[101] -
      amp[103] - amp[104] - amp[106] - amp[107];
  jamp[3] = +Complex<double> (0, 1) * amp[16] + Complex<double> (0, 1) *
      amp[17] + Complex<double> (0, 1) * amp[18] + Complex<double> (0, 1) *
      amp[19] - amp[24] - amp[25] + Complex<double> (0, 1) * amp[27] - amp[28]
      - amp[29] + Complex<double> (0, 1) * amp[31] - Complex<double> (0, 1) *
      amp[48] - Complex<double> (0, 1) * amp[49] - Complex<double> (0, 1) *
      amp[50] - Complex<double> (0, 1) * amp[51] + amp[52] + amp[53] + amp[54]
      + amp[55] + amp[57] + amp[59] + amp[64] + amp[65] - Complex<double> (0,
      1) * amp[66] + amp[68] + amp[69] - Complex<double> (0, 1) * amp[70] +
      amp[73] + amp[75] + amp[96] + amp[97] + amp[99] + amp[100] + amp[102] +
      amp[103] + amp[105] + amp[106];
  jamp[4] = +amp[0] + amp[1] - Complex<double> (0, 1) * amp[3] + amp[4] +
      amp[5] - Complex<double> (0, 1) * amp[7] - Complex<double> (0, 1) *
      amp[8] - Complex<double> (0, 1) * amp[9] - Complex<double> (0, 1) *
      amp[10] - Complex<double> (0, 1) * amp[11] + Complex<double> (0, 1) *
      amp[20] + Complex<double> (0, 1) * amp[21] + Complex<double> (0, 1) *
      amp[22] + Complex<double> (0, 1) * amp[23] + amp[24] + amp[25] +
      Complex<double> (0, 1) * amp[26] + amp[28] + amp[29] + Complex<double>
      (0, 1) * amp[30] + amp[80] + amp[82] + amp[84] + amp[85] + amp[86] +
      amp[87] + amp[88] + amp[90] - amp[97] - amp[98] - amp[100] - amp[101] -
      amp[103] - amp[104] - amp[106] - amp[107];
  jamp[5] = -amp[0] - amp[1] + Complex<double> (0, 1) * amp[3] - amp[4] -
      amp[5] + Complex<double> (0, 1) * amp[7] + Complex<double> (0, 1) *
      amp[8] + Complex<double> (0, 1) * amp[9] + Complex<double> (0, 1) *
      amp[10] + Complex<double> (0, 1) * amp[11] + Complex<double> (0, 1) *
      amp[48] + Complex<double> (0, 1) * amp[49] + Complex<double> (0, 1) *
      amp[50] + Complex<double> (0, 1) * amp[51] + amp[56] + amp[58] + amp[60]
      + amp[61] + amp[62] + amp[63] - amp[64] - amp[65] + Complex<double> (0,
      1) * amp[66] - amp[68] - amp[69] + Complex<double> (0, 1) * amp[70] +
      amp[89] + amp[91] - amp[96] + amp[98] - amp[99] + amp[101] - amp[102] +
      amp[104] - amp[105] + amp[107];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[0][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P69_sm_gb_llggb::matrix_4_gbx_epemggbx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 108;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = +amp[108] + amp[109] + Complex<double> (0, 1) * amp[110] + amp[112]
      + amp[113] + Complex<double> (0, 1) * amp[114] + Complex<double> (0, 1) *
      amp[120] + Complex<double> (0, 1) * amp[121] + Complex<double> (0, 1) *
      amp[122] + Complex<double> (0, 1) * amp[123] + Complex<double> (0, 1) *
      amp[140] + Complex<double> (0, 1) * amp[141] + Complex<double> (0, 1) *
      amp[142] + Complex<double> (0, 1) * amp[143] - amp[148] - amp[150] -
      amp[152] - amp[153] - amp[154] - amp[155] + amp[172] + amp[173] +
      Complex<double> (0, 1) * amp[175] + amp[176] + amp[177] + Complex<double>
      (0, 1) * amp[179] - amp[201] - amp[203] + amp[204] - amp[206] + amp[207]
      - amp[209] + amp[210] - amp[212] + amp[213] - amp[215];
  jamp[1] = +Complex<double> (0, 1) * amp[128] + Complex<double> (0, 1) *
      amp[129] + Complex<double> (0, 1) * amp[130] + Complex<double> (0, 1) *
      amp[131] + amp[132] + amp[133] + Complex<double> (0, 1) * amp[134] +
      amp[136] + amp[137] + Complex<double> (0, 1) * amp[138] - Complex<double>
      (0, 1) * amp[140] - Complex<double> (0, 1) * amp[141] - Complex<double>
      (0, 1) * amp[142] - Complex<double> (0, 1) * amp[143] - amp[144] -
      amp[145] - amp[146] - amp[147] - amp[149] - amp[151] - amp[172] -
      amp[173] - Complex<double> (0, 1) * amp[175] - amp[176] - amp[177] -
      Complex<double> (0, 1) * amp[179] - amp[189] - amp[191] - amp[204] -
      amp[205] - amp[207] - amp[208] - amp[210] - amp[211] - amp[213] -
      amp[214];
  jamp[2] = -amp[108] - amp[109] - Complex<double> (0, 1) * amp[110] - amp[112]
      - amp[113] - Complex<double> (0, 1) * amp[114] - Complex<double> (0, 1) *
      amp[120] - Complex<double> (0, 1) * amp[121] - Complex<double> (0, 1) *
      amp[122] - Complex<double> (0, 1) * amp[123] + Complex<double> (0, 1) *
      amp[124] + Complex<double> (0, 1) * amp[125] + Complex<double> (0, 1) *
      amp[126] + Complex<double> (0, 1) * amp[127] - amp[132] - amp[133] +
      Complex<double> (0, 1) * amp[135] - amp[136] - amp[137] + Complex<double>
      (0, 1) * amp[139] - amp[180] - amp[182] - amp[184] - amp[185] - amp[186]
      - amp[187] - amp[200] - amp[202] + amp[205] + amp[206] + amp[208] +
      amp[209] + amp[211] + amp[212] + amp[214] + amp[215];
  jamp[3] = -Complex<double> (0, 1) * amp[124] - Complex<double> (0, 1) *
      amp[125] - Complex<double> (0, 1) * amp[126] - Complex<double> (0, 1) *
      amp[127] + amp[132] + amp[133] - Complex<double> (0, 1) * amp[135] +
      amp[136] + amp[137] - Complex<double> (0, 1) * amp[139] + Complex<double>
      (0, 1) * amp[156] + Complex<double> (0, 1) * amp[157] + Complex<double>
      (0, 1) * amp[158] + Complex<double> (0, 1) * amp[159] - amp[160] -
      amp[161] - amp[162] - amp[163] - amp[165] - amp[167] - amp[172] -
      amp[173] + Complex<double> (0, 1) * amp[174] - amp[176] - amp[177] +
      Complex<double> (0, 1) * amp[178] - amp[181] - amp[183] - amp[204] -
      amp[205] - amp[207] - amp[208] - amp[210] - amp[211] - amp[213] -
      amp[214];
  jamp[4] = -amp[108] - amp[109] + Complex<double> (0, 1) * amp[111] - amp[112]
      - amp[113] + Complex<double> (0, 1) * amp[115] + Complex<double> (0, 1) *
      amp[116] + Complex<double> (0, 1) * amp[117] + Complex<double> (0, 1) *
      amp[118] + Complex<double> (0, 1) * amp[119] - Complex<double> (0, 1) *
      amp[128] - Complex<double> (0, 1) * amp[129] - Complex<double> (0, 1) *
      amp[130] - Complex<double> (0, 1) * amp[131] - amp[132] - amp[133] -
      Complex<double> (0, 1) * amp[134] - amp[136] - amp[137] - Complex<double>
      (0, 1) * amp[138] - amp[188] - amp[190] - amp[192] - amp[193] - amp[194]
      - amp[195] - amp[196] - amp[198] + amp[205] + amp[206] + amp[208] +
      amp[209] + amp[211] + amp[212] + amp[214] + amp[215];
  jamp[5] = +amp[108] + amp[109] - Complex<double> (0, 1) * amp[111] + amp[112]
      + amp[113] - Complex<double> (0, 1) * amp[115] - Complex<double> (0, 1) *
      amp[116] - Complex<double> (0, 1) * amp[117] - Complex<double> (0, 1) *
      amp[118] - Complex<double> (0, 1) * amp[119] - Complex<double> (0, 1) *
      amp[156] - Complex<double> (0, 1) * amp[157] - Complex<double> (0, 1) *
      amp[158] - Complex<double> (0, 1) * amp[159] - amp[164] - amp[166] -
      amp[168] - amp[169] - amp[170] - amp[171] + amp[172] + amp[173] -
      Complex<double> (0, 1) * amp[174] + amp[176] + amp[177] - Complex<double>
      (0, 1) * amp[178] - amp[197] - amp[199] + amp[204] - amp[206] + amp[207]
      - amp[209] + amp[210] - amp[212] + amp[213] - amp[215];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[1][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P69_sm_gb_llggb::matrix_4_gb_vexveggb() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 54;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = -amp[216] - amp[217] - Complex<double> (0, 1) * amp[218] -
      Complex<double> (0, 1) * amp[222] - Complex<double> (0, 1) * amp[223] -
      Complex<double> (0, 1) * amp[232] - Complex<double> (0, 1) * amp[233] +
      amp[236] + amp[238] + amp[239] - amp[248] - amp[249] - Complex<double>
      (0, 1) * amp[251] + amp[263] - amp[264] + amp[266] - amp[267] + amp[269];
  jamp[1] = -Complex<double> (0, 1) * amp[226] - Complex<double> (0, 1) *
      amp[227] - amp[228] - amp[229] - Complex<double> (0, 1) * amp[230] +
      Complex<double> (0, 1) * amp[232] + Complex<double> (0, 1) * amp[233] +
      amp[234] + amp[235] + amp[237] + amp[248] + amp[249] + Complex<double>
      (0, 1) * amp[251] + amp[257] + amp[264] + amp[265] + amp[267] + amp[268];
  jamp[2] = +amp[216] + amp[217] + Complex<double> (0, 1) * amp[218] +
      Complex<double> (0, 1) * amp[222] + Complex<double> (0, 1) * amp[223] -
      Complex<double> (0, 1) * amp[224] - Complex<double> (0, 1) * amp[225] +
      amp[228] + amp[229] - Complex<double> (0, 1) * amp[231] + amp[252] +
      amp[254] + amp[255] + amp[262] - amp[265] - amp[266] - amp[268] -
      amp[269];
  jamp[3] = +Complex<double> (0, 1) * amp[224] + Complex<double> (0, 1) *
      amp[225] - amp[228] - amp[229] + Complex<double> (0, 1) * amp[231] -
      Complex<double> (0, 1) * amp[240] - Complex<double> (0, 1) * amp[241] +
      amp[242] + amp[243] + amp[245] + amp[248] + amp[249] - Complex<double>
      (0, 1) * amp[250] + amp[253] + amp[264] + amp[265] + amp[267] + amp[268];
  jamp[4] = +amp[216] + amp[217] - Complex<double> (0, 1) * amp[219] -
      Complex<double> (0, 1) * amp[220] - Complex<double> (0, 1) * amp[221] +
      Complex<double> (0, 1) * amp[226] + Complex<double> (0, 1) * amp[227] +
      amp[228] + amp[229] + Complex<double> (0, 1) * amp[230] + amp[256] +
      amp[258] + amp[259] + amp[260] - amp[265] - amp[266] - amp[268] -
      amp[269];
  jamp[5] = -amp[216] - amp[217] + Complex<double> (0, 1) * amp[219] +
      Complex<double> (0, 1) * amp[220] + Complex<double> (0, 1) * amp[221] +
      Complex<double> (0, 1) * amp[240] + Complex<double> (0, 1) * amp[241] +
      amp[244] + amp[246] + amp[247] - amp[248] - amp[249] + Complex<double>
      (0, 1) * amp[250] + amp[261] - amp[264] + amp[266] - amp[267] + amp[269];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[2][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R4_P69_sm_gb_llggb::matrix_4_gbx_vexveggbx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 54;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = +amp[270] + amp[271] + Complex<double> (0, 1) * amp[272] +
      Complex<double> (0, 1) * amp[276] + Complex<double> (0, 1) * amp[277] +
      Complex<double> (0, 1) * amp[286] + Complex<double> (0, 1) * amp[287] -
      amp[290] - amp[292] - amp[293] + amp[302] + amp[303] + Complex<double>
      (0, 1) * amp[305] - amp[317] + amp[318] - amp[320] + amp[321] - amp[323];
  jamp[1] = +Complex<double> (0, 1) * amp[280] + Complex<double> (0, 1) *
      amp[281] + amp[282] + amp[283] + Complex<double> (0, 1) * amp[284] -
      Complex<double> (0, 1) * amp[286] - Complex<double> (0, 1) * amp[287] -
      amp[288] - amp[289] - amp[291] - amp[302] - amp[303] - Complex<double>
      (0, 1) * amp[305] - amp[311] - amp[318] - amp[319] - amp[321] - amp[322];
  jamp[2] = -amp[270] - amp[271] - Complex<double> (0, 1) * amp[272] -
      Complex<double> (0, 1) * amp[276] - Complex<double> (0, 1) * amp[277] +
      Complex<double> (0, 1) * amp[278] + Complex<double> (0, 1) * amp[279] -
      amp[282] - amp[283] + Complex<double> (0, 1) * amp[285] - amp[306] -
      amp[308] - amp[309] - amp[316] + amp[319] + amp[320] + amp[322] +
      amp[323];
  jamp[3] = -Complex<double> (0, 1) * amp[278] - Complex<double> (0, 1) *
      amp[279] + amp[282] + amp[283] - Complex<double> (0, 1) * amp[285] +
      Complex<double> (0, 1) * amp[294] + Complex<double> (0, 1) * amp[295] -
      amp[296] - amp[297] - amp[299] - amp[302] - amp[303] + Complex<double>
      (0, 1) * amp[304] - amp[307] - amp[318] - amp[319] - amp[321] - amp[322];
  jamp[4] = -amp[270] - amp[271] + Complex<double> (0, 1) * amp[273] +
      Complex<double> (0, 1) * amp[274] + Complex<double> (0, 1) * amp[275] -
      Complex<double> (0, 1) * amp[280] - Complex<double> (0, 1) * amp[281] -
      amp[282] - amp[283] - Complex<double> (0, 1) * amp[284] - amp[310] -
      amp[312] - amp[313] - amp[314] + amp[319] + amp[320] + amp[322] +
      amp[323];
  jamp[5] = +amp[270] + amp[271] - Complex<double> (0, 1) * amp[273] -
      Complex<double> (0, 1) * amp[274] - Complex<double> (0, 1) * amp[275] -
      Complex<double> (0, 1) * amp[294] - Complex<double> (0, 1) * amp[295] -
      amp[298] - amp[300] - amp[301] + amp[302] + amp[303] - Complex<double>
      (0, 1) * amp[304] - amp[315] + amp[318] - amp[320] + amp[321] - amp[323];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[3][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}


}  // end namespace PY8MEs_namespace

