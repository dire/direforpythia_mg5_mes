//==========================================================================
// This file has been automatically generated for Pythia 8 by
// MadGraph5_aMC@NLO v. 2.6.0, 2017-08-16
// By the MadGraph5_aMC@NLO Development Team
// Visit launchpad.net/madgraph5 and amcatnlo.web.cern.ch
//==========================================================================

#include "PY8MEs_R12_P28_sm_qq_zzggg.h"
#include "HelAmps_sm.h"

using namespace Pythia8_sm; 

namespace PY8MEs_namespace 
{
//==========================================================================
// Class member functions for calculating the matrix elements for
// Process: u u~ > z z g g g WEIGHTED<=7 @12
// Process: c c~ > z z g g g WEIGHTED<=7 @12
// Process: d d~ > z z g g g WEIGHTED<=7 @12
// Process: s s~ > z z g g g WEIGHTED<=7 @12

// Exception class
class PY8MEs_R12_P28_sm_qq_zzgggException : public exception
{
  virtual const char * what() const throw()
  {
    return "Exception in class 'PY8MEs_R12_P28_sm_qq_zzggg'."; 
  }
}
PY8MEs_R12_P28_sm_qq_zzggg_exception; 

std::set<int> PY8MEs_R12_P28_sm_qq_zzggg::s_channel_proc = std::set<int>
    (createset<int> ());

int PY8MEs_R12_P28_sm_qq_zzggg::helicities[ncomb][nexternal] = {{-1, -1, -1,
    -1, -1, -1, -1}, {-1, -1, -1, -1, -1, -1, 1}, {-1, -1, -1, -1, -1, 1, -1},
    {-1, -1, -1, -1, -1, 1, 1}, {-1, -1, -1, -1, 1, -1, -1}, {-1, -1, -1, -1,
    1, -1, 1}, {-1, -1, -1, -1, 1, 1, -1}, {-1, -1, -1, -1, 1, 1, 1}, {-1, -1,
    -1, 0, -1, -1, -1}, {-1, -1, -1, 0, -1, -1, 1}, {-1, -1, -1, 0, -1, 1, -1},
    {-1, -1, -1, 0, -1, 1, 1}, {-1, -1, -1, 0, 1, -1, -1}, {-1, -1, -1, 0, 1,
    -1, 1}, {-1, -1, -1, 0, 1, 1, -1}, {-1, -1, -1, 0, 1, 1, 1}, {-1, -1, -1,
    1, -1, -1, -1}, {-1, -1, -1, 1, -1, -1, 1}, {-1, -1, -1, 1, -1, 1, -1},
    {-1, -1, -1, 1, -1, 1, 1}, {-1, -1, -1, 1, 1, -1, -1}, {-1, -1, -1, 1, 1,
    -1, 1}, {-1, -1, -1, 1, 1, 1, -1}, {-1, -1, -1, 1, 1, 1, 1}, {-1, -1, 0,
    -1, -1, -1, -1}, {-1, -1, 0, -1, -1, -1, 1}, {-1, -1, 0, -1, -1, 1, -1},
    {-1, -1, 0, -1, -1, 1, 1}, {-1, -1, 0, -1, 1, -1, -1}, {-1, -1, 0, -1, 1,
    -1, 1}, {-1, -1, 0, -1, 1, 1, -1}, {-1, -1, 0, -1, 1, 1, 1}, {-1, -1, 0, 0,
    -1, -1, -1}, {-1, -1, 0, 0, -1, -1, 1}, {-1, -1, 0, 0, -1, 1, -1}, {-1, -1,
    0, 0, -1, 1, 1}, {-1, -1, 0, 0, 1, -1, -1}, {-1, -1, 0, 0, 1, -1, 1}, {-1,
    -1, 0, 0, 1, 1, -1}, {-1, -1, 0, 0, 1, 1, 1}, {-1, -1, 0, 1, -1, -1, -1},
    {-1, -1, 0, 1, -1, -1, 1}, {-1, -1, 0, 1, -1, 1, -1}, {-1, -1, 0, 1, -1, 1,
    1}, {-1, -1, 0, 1, 1, -1, -1}, {-1, -1, 0, 1, 1, -1, 1}, {-1, -1, 0, 1, 1,
    1, -1}, {-1, -1, 0, 1, 1, 1, 1}, {-1, -1, 1, -1, -1, -1, -1}, {-1, -1, 1,
    -1, -1, -1, 1}, {-1, -1, 1, -1, -1, 1, -1}, {-1, -1, 1, -1, -1, 1, 1}, {-1,
    -1, 1, -1, 1, -1, -1}, {-1, -1, 1, -1, 1, -1, 1}, {-1, -1, 1, -1, 1, 1,
    -1}, {-1, -1, 1, -1, 1, 1, 1}, {-1, -1, 1, 0, -1, -1, -1}, {-1, -1, 1, 0,
    -1, -1, 1}, {-1, -1, 1, 0, -1, 1, -1}, {-1, -1, 1, 0, -1, 1, 1}, {-1, -1,
    1, 0, 1, -1, -1}, {-1, -1, 1, 0, 1, -1, 1}, {-1, -1, 1, 0, 1, 1, -1}, {-1,
    -1, 1, 0, 1, 1, 1}, {-1, -1, 1, 1, -1, -1, -1}, {-1, -1, 1, 1, -1, -1, 1},
    {-1, -1, 1, 1, -1, 1, -1}, {-1, -1, 1, 1, -1, 1, 1}, {-1, -1, 1, 1, 1, -1,
    -1}, {-1, -1, 1, 1, 1, -1, 1}, {-1, -1, 1, 1, 1, 1, -1}, {-1, -1, 1, 1, 1,
    1, 1}, {-1, 1, -1, -1, -1, -1, -1}, {-1, 1, -1, -1, -1, -1, 1}, {-1, 1, -1,
    -1, -1, 1, -1}, {-1, 1, -1, -1, -1, 1, 1}, {-1, 1, -1, -1, 1, -1, -1}, {-1,
    1, -1, -1, 1, -1, 1}, {-1, 1, -1, -1, 1, 1, -1}, {-1, 1, -1, -1, 1, 1, 1},
    {-1, 1, -1, 0, -1, -1, -1}, {-1, 1, -1, 0, -1, -1, 1}, {-1, 1, -1, 0, -1,
    1, -1}, {-1, 1, -1, 0, -1, 1, 1}, {-1, 1, -1, 0, 1, -1, -1}, {-1, 1, -1, 0,
    1, -1, 1}, {-1, 1, -1, 0, 1, 1, -1}, {-1, 1, -1, 0, 1, 1, 1}, {-1, 1, -1,
    1, -1, -1, -1}, {-1, 1, -1, 1, -1, -1, 1}, {-1, 1, -1, 1, -1, 1, -1}, {-1,
    1, -1, 1, -1, 1, 1}, {-1, 1, -1, 1, 1, -1, -1}, {-1, 1, -1, 1, 1, -1, 1},
    {-1, 1, -1, 1, 1, 1, -1}, {-1, 1, -1, 1, 1, 1, 1}, {-1, 1, 0, -1, -1, -1,
    -1}, {-1, 1, 0, -1, -1, -1, 1}, {-1, 1, 0, -1, -1, 1, -1}, {-1, 1, 0, -1,
    -1, 1, 1}, {-1, 1, 0, -1, 1, -1, -1}, {-1, 1, 0, -1, 1, -1, 1}, {-1, 1, 0,
    -1, 1, 1, -1}, {-1, 1, 0, -1, 1, 1, 1}, {-1, 1, 0, 0, -1, -1, -1}, {-1, 1,
    0, 0, -1, -1, 1}, {-1, 1, 0, 0, -1, 1, -1}, {-1, 1, 0, 0, -1, 1, 1}, {-1,
    1, 0, 0, 1, -1, -1}, {-1, 1, 0, 0, 1, -1, 1}, {-1, 1, 0, 0, 1, 1, -1}, {-1,
    1, 0, 0, 1, 1, 1}, {-1, 1, 0, 1, -1, -1, -1}, {-1, 1, 0, 1, -1, -1, 1},
    {-1, 1, 0, 1, -1, 1, -1}, {-1, 1, 0, 1, -1, 1, 1}, {-1, 1, 0, 1, 1, -1,
    -1}, {-1, 1, 0, 1, 1, -1, 1}, {-1, 1, 0, 1, 1, 1, -1}, {-1, 1, 0, 1, 1, 1,
    1}, {-1, 1, 1, -1, -1, -1, -1}, {-1, 1, 1, -1, -1, -1, 1}, {-1, 1, 1, -1,
    -1, 1, -1}, {-1, 1, 1, -1, -1, 1, 1}, {-1, 1, 1, -1, 1, -1, -1}, {-1, 1, 1,
    -1, 1, -1, 1}, {-1, 1, 1, -1, 1, 1, -1}, {-1, 1, 1, -1, 1, 1, 1}, {-1, 1,
    1, 0, -1, -1, -1}, {-1, 1, 1, 0, -1, -1, 1}, {-1, 1, 1, 0, -1, 1, -1}, {-1,
    1, 1, 0, -1, 1, 1}, {-1, 1, 1, 0, 1, -1, -1}, {-1, 1, 1, 0, 1, -1, 1}, {-1,
    1, 1, 0, 1, 1, -1}, {-1, 1, 1, 0, 1, 1, 1}, {-1, 1, 1, 1, -1, -1, -1}, {-1,
    1, 1, 1, -1, -1, 1}, {-1, 1, 1, 1, -1, 1, -1}, {-1, 1, 1, 1, -1, 1, 1},
    {-1, 1, 1, 1, 1, -1, -1}, {-1, 1, 1, 1, 1, -1, 1}, {-1, 1, 1, 1, 1, 1, -1},
    {-1, 1, 1, 1, 1, 1, 1}, {1, -1, -1, -1, -1, -1, -1}, {1, -1, -1, -1, -1,
    -1, 1}, {1, -1, -1, -1, -1, 1, -1}, {1, -1, -1, -1, -1, 1, 1}, {1, -1, -1,
    -1, 1, -1, -1}, {1, -1, -1, -1, 1, -1, 1}, {1, -1, -1, -1, 1, 1, -1}, {1,
    -1, -1, -1, 1, 1, 1}, {1, -1, -1, 0, -1, -1, -1}, {1, -1, -1, 0, -1, -1,
    1}, {1, -1, -1, 0, -1, 1, -1}, {1, -1, -1, 0, -1, 1, 1}, {1, -1, -1, 0, 1,
    -1, -1}, {1, -1, -1, 0, 1, -1, 1}, {1, -1, -1, 0, 1, 1, -1}, {1, -1, -1, 0,
    1, 1, 1}, {1, -1, -1, 1, -1, -1, -1}, {1, -1, -1, 1, -1, -1, 1}, {1, -1,
    -1, 1, -1, 1, -1}, {1, -1, -1, 1, -1, 1, 1}, {1, -1, -1, 1, 1, -1, -1}, {1,
    -1, -1, 1, 1, -1, 1}, {1, -1, -1, 1, 1, 1, -1}, {1, -1, -1, 1, 1, 1, 1},
    {1, -1, 0, -1, -1, -1, -1}, {1, -1, 0, -1, -1, -1, 1}, {1, -1, 0, -1, -1,
    1, -1}, {1, -1, 0, -1, -1, 1, 1}, {1, -1, 0, -1, 1, -1, -1}, {1, -1, 0, -1,
    1, -1, 1}, {1, -1, 0, -1, 1, 1, -1}, {1, -1, 0, -1, 1, 1, 1}, {1, -1, 0, 0,
    -1, -1, -1}, {1, -1, 0, 0, -1, -1, 1}, {1, -1, 0, 0, -1, 1, -1}, {1, -1, 0,
    0, -1, 1, 1}, {1, -1, 0, 0, 1, -1, -1}, {1, -1, 0, 0, 1, -1, 1}, {1, -1, 0,
    0, 1, 1, -1}, {1, -1, 0, 0, 1, 1, 1}, {1, -1, 0, 1, -1, -1, -1}, {1, -1, 0,
    1, -1, -1, 1}, {1, -1, 0, 1, -1, 1, -1}, {1, -1, 0, 1, -1, 1, 1}, {1, -1,
    0, 1, 1, -1, -1}, {1, -1, 0, 1, 1, -1, 1}, {1, -1, 0, 1, 1, 1, -1}, {1, -1,
    0, 1, 1, 1, 1}, {1, -1, 1, -1, -1, -1, -1}, {1, -1, 1, -1, -1, -1, 1}, {1,
    -1, 1, -1, -1, 1, -1}, {1, -1, 1, -1, -1, 1, 1}, {1, -1, 1, -1, 1, -1, -1},
    {1, -1, 1, -1, 1, -1, 1}, {1, -1, 1, -1, 1, 1, -1}, {1, -1, 1, -1, 1, 1,
    1}, {1, -1, 1, 0, -1, -1, -1}, {1, -1, 1, 0, -1, -1, 1}, {1, -1, 1, 0, -1,
    1, -1}, {1, -1, 1, 0, -1, 1, 1}, {1, -1, 1, 0, 1, -1, -1}, {1, -1, 1, 0, 1,
    -1, 1}, {1, -1, 1, 0, 1, 1, -1}, {1, -1, 1, 0, 1, 1, 1}, {1, -1, 1, 1, -1,
    -1, -1}, {1, -1, 1, 1, -1, -1, 1}, {1, -1, 1, 1, -1, 1, -1}, {1, -1, 1, 1,
    -1, 1, 1}, {1, -1, 1, 1, 1, -1, -1}, {1, -1, 1, 1, 1, -1, 1}, {1, -1, 1, 1,
    1, 1, -1}, {1, -1, 1, 1, 1, 1, 1}, {1, 1, -1, -1, -1, -1, -1}, {1, 1, -1,
    -1, -1, -1, 1}, {1, 1, -1, -1, -1, 1, -1}, {1, 1, -1, -1, -1, 1, 1}, {1, 1,
    -1, -1, 1, -1, -1}, {1, 1, -1, -1, 1, -1, 1}, {1, 1, -1, -1, 1, 1, -1}, {1,
    1, -1, -1, 1, 1, 1}, {1, 1, -1, 0, -1, -1, -1}, {1, 1, -1, 0, -1, -1, 1},
    {1, 1, -1, 0, -1, 1, -1}, {1, 1, -1, 0, -1, 1, 1}, {1, 1, -1, 0, 1, -1,
    -1}, {1, 1, -1, 0, 1, -1, 1}, {1, 1, -1, 0, 1, 1, -1}, {1, 1, -1, 0, 1, 1,
    1}, {1, 1, -1, 1, -1, -1, -1}, {1, 1, -1, 1, -1, -1, 1}, {1, 1, -1, 1, -1,
    1, -1}, {1, 1, -1, 1, -1, 1, 1}, {1, 1, -1, 1, 1, -1, -1}, {1, 1, -1, 1, 1,
    -1, 1}, {1, 1, -1, 1, 1, 1, -1}, {1, 1, -1, 1, 1, 1, 1}, {1, 1, 0, -1, -1,
    -1, -1}, {1, 1, 0, -1, -1, -1, 1}, {1, 1, 0, -1, -1, 1, -1}, {1, 1, 0, -1,
    -1, 1, 1}, {1, 1, 0, -1, 1, -1, -1}, {1, 1, 0, -1, 1, -1, 1}, {1, 1, 0, -1,
    1, 1, -1}, {1, 1, 0, -1, 1, 1, 1}, {1, 1, 0, 0, -1, -1, -1}, {1, 1, 0, 0,
    -1, -1, 1}, {1, 1, 0, 0, -1, 1, -1}, {1, 1, 0, 0, -1, 1, 1}, {1, 1, 0, 0,
    1, -1, -1}, {1, 1, 0, 0, 1, -1, 1}, {1, 1, 0, 0, 1, 1, -1}, {1, 1, 0, 0, 1,
    1, 1}, {1, 1, 0, 1, -1, -1, -1}, {1, 1, 0, 1, -1, -1, 1}, {1, 1, 0, 1, -1,
    1, -1}, {1, 1, 0, 1, -1, 1, 1}, {1, 1, 0, 1, 1, -1, -1}, {1, 1, 0, 1, 1,
    -1, 1}, {1, 1, 0, 1, 1, 1, -1}, {1, 1, 0, 1, 1, 1, 1}, {1, 1, 1, -1, -1,
    -1, -1}, {1, 1, 1, -1, -1, -1, 1}, {1, 1, 1, -1, -1, 1, -1}, {1, 1, 1, -1,
    -1, 1, 1}, {1, 1, 1, -1, 1, -1, -1}, {1, 1, 1, -1, 1, -1, 1}, {1, 1, 1, -1,
    1, 1, -1}, {1, 1, 1, -1, 1, 1, 1}, {1, 1, 1, 0, -1, -1, -1}, {1, 1, 1, 0,
    -1, -1, 1}, {1, 1, 1, 0, -1, 1, -1}, {1, 1, 1, 0, -1, 1, 1}, {1, 1, 1, 0,
    1, -1, -1}, {1, 1, 1, 0, 1, -1, 1}, {1, 1, 1, 0, 1, 1, -1}, {1, 1, 1, 0, 1,
    1, 1}, {1, 1, 1, 1, -1, -1, -1}, {1, 1, 1, 1, -1, -1, 1}, {1, 1, 1, 1, -1,
    1, -1}, {1, 1, 1, 1, -1, 1, 1}, {1, 1, 1, 1, 1, -1, -1}, {1, 1, 1, 1, 1,
    -1, 1}, {1, 1, 1, 1, 1, 1, -1}, {1, 1, 1, 1, 1, 1, 1}};

// Normalization factors the various processes
// Denominators: spins, colors and identical particles
int PY8MEs_R12_P28_sm_qq_zzggg::denom_colors[nprocesses] = {9, 9}; 
int PY8MEs_R12_P28_sm_qq_zzggg::denom_hels[nprocesses] = {4, 4}; 
int PY8MEs_R12_P28_sm_qq_zzggg::denom_iden[nprocesses] = {12, 12}; 

//--------------------------------------------------------------------------
// Color config initialization
void PY8MEs_R12_P28_sm_qq_zzggg::initColorConfigs() 
{
  color_configs = vector < vec_vec_int > (); 
  jamp_nc_relative_power = vector < vec_int > (); 

  // Color flows of process Process: u u~ > z z g g g WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(0)(0)(1)(0)(0)(0)(0)(2)(1)(3)(2)(4)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #1
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(2)(1)(3)(4)(4)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #2
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(0)(0)(1)(0)(0)(0)(0)(2)(3)(3)(1)(4)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #3
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(4)(3)(1)(4)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #4
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(2)(4)(3)(2)(4)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #5
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(3)(3)(4)(4)(1)));
  jamp_nc_relative_power[0].push_back(0); 

  // Color flows of process Process: d d~ > z z g g g WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[1].push_back(vec_int(createvector<int>
      (4)(0)(0)(1)(0)(0)(0)(0)(2)(1)(3)(2)(4)(3)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #1
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(2)(1)(3)(4)(4)(2)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #2
  color_configs[1].push_back(vec_int(createvector<int>
      (4)(0)(0)(1)(0)(0)(0)(0)(2)(3)(3)(1)(4)(2)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #3
  color_configs[1].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(4)(3)(1)(4)(3)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #4
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(2)(4)(3)(2)(4)(1)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #5
  color_configs[1].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(3)(3)(4)(4)(1)));
  jamp_nc_relative_power[1].push_back(0); 
}

//--------------------------------------------------------------------------
// Destructor.
PY8MEs_R12_P28_sm_qq_zzggg::~PY8MEs_R12_P28_sm_qq_zzggg() 
{
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    delete[] p[i]; 
    p[i] = NULL; 
  }
}

//--------------------------------------------------------------------------
// Invert the permutation mapping
vector<int> PY8MEs_R12_P28_sm_qq_zzggg::invert_mapping(vector<int> mapping) 
{
  vector<int> inverted_mapping; 
  for (unsigned int i = 0; i < mapping.size(); i++ )
  {
    for (unsigned int j = 0; j < mapping.size(); j++ )
    {
      if (mapping[j] == ((int)i))
      {
        inverted_mapping.push_back(j); 
        break; 
      }
    }
  }
  return inverted_mapping; 
}

//--------------------------------------------------------------------------
// Return the list of possible helicity configurations
vector < vec_int > PY8MEs_R12_P28_sm_qq_zzggg::getHelicityConfigs(vector<int>
    permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(ncomb, vector<int> (nexternal, 0)); 
  for (unsigned int ihel = 0; ihel < ncomb; ihel++ )
  {
    for(unsigned int j = 0; j < nexternal; j++ )
    {
      res[ihel][chosenPerm[j]] = helicities[ihel][j]; 
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Return the list of possible color configurations
vector < vec_int > PY8MEs_R12_P28_sm_qq_zzggg::getColorConfigs(int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(color_configs[chosenProcID].size(), vector<int>
      (nexternal * 2, 0));
  for (unsigned int icol = 0; icol < color_configs[chosenProcID].size(); icol++
      )
  {
    for(unsigned int j = 0; j < (2 * nexternal); j++ )
    {
      res[icol][chosenPerm[j/2] * 2 + j%2] =
          color_configs[chosenProcID][icol][j];
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Get JAMP relative N_c power
int PY8MEs_R12_P28_sm_qq_zzggg::getColorFlowRelativeNCPower(int color_flow_ID,
    int specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return jamp_nc_relative_power[chosenProcID][color_flow_ID]; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity ID -> Helicity Config
vector<int> PY8MEs_R12_P28_sm_qq_zzggg::getHelicityConfigForID(int hel_ID,
    vector<int> permutation)
{
  if (hel_ID < 0 || hel_ID >= ncomb)
  {
    cerr <<  "Error in function 'getHelicityConfigForID' of class" << 
    " 'PY8MEs_R12_P28_sm_qq_zzggg': Specified helicity ID '" << 
    hel_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R12_P28_sm_qq_zzggg_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(nexternal, 0); 
  for (unsigned int j = 0; j < nexternal; j++ )
  {
    res[chosenPerm[j]] = helicities[hel_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity Config -> Helicity ID
int PY8MEs_R12_P28_sm_qq_zzggg::getHelicityIDForConfig(vector<int> hel_config,
    vector<int> permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  int user_ihel = -1; 
  if (hel_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < ncomb; i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < nexternal; j++ )
      {
        if (helicities[i][chosenPerm[j]] != hel_config[j])
        {
          found = false; 
          break; 
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_ihel = i; 
        break; 
      }
    }
    if (user_ihel == -1)
    {
      cerr <<  "Error in function 'getHelicityIDForConfig' of class" << 
      " 'PY8MEs_R12_P28_sm_qq_zzggg': Specified helicity" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R12_P28_sm_qq_zzggg_exception; 
    }
  }
  return user_ihel; 
}


//--------------------------------------------------------------------------
// Implements the map Color ID -> Color Config
vector<int> PY8MEs_R12_P28_sm_qq_zzggg::getColorConfigForID(int color_ID, int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < 0 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getColorConfigForID' of class" << 
    " 'PY8MEs_R12_P28_sm_qq_zzggg': Specified color ID '" << 
    color_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R12_P28_sm_qq_zzggg_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(color_configs[chosenProcID][color_ID].size(), 0); 
  for (unsigned int j = 0; j < (2 * nexternal); j++ )
  {
    res[chosenPerm[j/2] * 2 + j%2] = color_configs[chosenProcID][color_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Color Config -> Color ID
int PY8MEs_R12_P28_sm_qq_zzggg::getColorIDForConfig(vector<int> color_config,
    int specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = -1; 
  if (color_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < color_configs[chosenProcID].size(); i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < (nexternal * 2); j++ )
      {

        // If colorless then make sure it matches
        // The little arithmetics in the color index is just
        // the permutation applies on the particle list which is
        // twice smaller since each particle can have two color indices.
        if (color_config[j] == 0)
        {
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] != 0)
          {
            found = false; 
            break; 
          }
          // Otherwise check that the color linked position matches
        }
        else
        {
          int color_linked_pos = -1; 
          // Find the other end of the line in the user color config
          for (unsigned int k = 0; k < (nexternal * 2); k++ )
          {
            if (k == j)
              continue; 
            if (color_config[j] == color_config[k])
            {
              color_linked_pos = k; 
              break; 
            }
          }
          if (color_linked_pos == -1)
          {
            cerr <<  "Error in function 'getColorIDForConfig' of class" << 
            " 'PY8MEs_R12_P28_sm_qq_zzggg': A color line could " << 
            " not be closed." << endl; 
            throw PY8MEs_R12_P28_sm_qq_zzggg_exception; 
          }
          // Now check whether the color line matches
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] !=
              color_configs[chosenProcID][i][chosenPerm[color_linked_pos/2] * 2
              + color_linked_pos%2])
          {
            found = false; 
            break; 
          }
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_icol = i; 
        break; 
      }
    }

    if (user_icol == -1)
    {
      cerr <<  "Error in function 'getColorIDForConfig' of class" << 
      " 'PY8MEs_R12_P28_sm_qq_zzggg': Specified color" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R12_P28_sm_qq_zzggg_exception; 
    }
  }
  return user_icol; 
}

//--------------------------------------------------------------------------
// Returns all result previously computed in SigmaKin
vector < vec_double > PY8MEs_R12_P28_sm_qq_zzggg::getAllResults(int
    specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return all_results[chosenProcID]; 
}

//--------------------------------------------------------------------------
// Returns a result previously computed in SigmaKin for a specific helicity
// and color ID. -1 means avg and summed over that characteristic.
double PY8MEs_R12_P28_sm_qq_zzggg::getResult(int helicity_ID, int color_ID, int
    specify_proc_ID)
{
  if (helicity_ID < - 1 || helicity_ID >= ncomb)
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R12_P28_sm_qq_zzggg': Specified helicity ID '" << 
    helicity_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R12_P28_sm_qq_zzggg_exception; 
  }
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < - 1 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R12_P28_sm_qq_zzggg': Specified color ID '" << 
    color_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R12_P28_sm_qq_zzggg_exception; 
  }
  return all_results[chosenProcID][helicity_ID + 1][color_ID + 1]; 
}

//--------------------------------------------------------------------------
// Check for the availability of the requested process and if available,
// If available, this returns the corresponding permutation and Proc_ID to use.
// If not available, this returns a negative Proc_ID.
pair < vector<int> , int > PY8MEs_R12_P28_sm_qq_zzggg::static_getPY8ME(vector<int> initial_pdgs, vector<int> final_pdgs, set<int> schannels) 
{

  // Not available return value
  pair < vector<int> , int > NA(vector<int> (), -1); 

  // Check if s-channel requirements match
  if (nreq_s_channels > 0)
  {
    if (schannels != s_channel_proc)
      return NA; 
  }
  else
  {
    if (schannels.size() != 0)
      return NA; 
  }

  // Check number of final state particles
  if (final_pdgs.size() != (nexternal - ninitial))
    return NA; 

  // Check number of initial state particles
  if (initial_pdgs.size() != ninitial)
    return NA; 

  // List of processes available in this class
  const int nprocs = 8; 
  const int proc_IDS[nprocs] = {0, 0, 1, 1, 0, 0, 1, 1}; 
  const int in_pdgs[nprocs][ninitial] = {{2, -2}, {4, -4}, {1, -1}, {3, -3},
      {-2, 2}, {-4, 4}, {-1, 1}, {-3, 3}};
  const int out_pdgs[nprocs][nexternal - ninitial] = {{23, 23, 21, 21, 21},
      {23, 23, 21, 21, 21}, {23, 23, 21, 21, 21}, {23, 23, 21, 21, 21}, {23,
      23, 21, 21, 21}, {23, 23, 21, 21, 21}, {23, 23, 21, 21, 21}, {23, 23, 21,
      21, 21}};

  bool in_pdgs_used[ninitial]; 
  bool out_pdgs_used[nexternal - ninitial]; 
  for(unsigned int i = 0; i < nprocs; i++ )
  {
    int permutations[nexternal]; 

    // Reinitialize initial state look-up variables
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      in_pdgs_used[j] = false; 
      permutations[j] = -1; 
    }
    // Look for initial state matches
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      for(unsigned int k = 0; k < ninitial; k++ )
      {
        // Make sure it has not been used already
        if (in_pdgs_used[k])
          continue; 
        if (initial_pdgs[k] == in_pdgs[i][j])
        {
          permutations[j] = k; 
          in_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[ninitial - 1] == -1)
      continue; 

    // Reinitialize final state look-up variables
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      out_pdgs_used[j] = false; 
      permutations[ninitial + j] = -1; 
    }
    // Look for final state matches
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      for(unsigned int k = 0; k < (nexternal - ninitial); k++ )
      {
        // Make sure it has not been used already
        if (out_pdgs_used[k])
          continue; 
        if (final_pdgs[k] == out_pdgs[i][j])
        {
          permutations[ninitial + j] = ninitial + k; 
          out_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[ninitial + j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[nexternal - 1] == -1)
      continue; 

    // Return process found
    return pair < vector<int> , int > (vector<int> (permutations, permutations
        + nexternal), proc_IDS[i]);
  }

  // No process found
  return NA; 
}

//--------------------------------------------------------------------------
// Set momenta
void PY8MEs_R12_P28_sm_qq_zzggg::setMomenta(vector < vec_double >
    momenta_picked)
{
  if (momenta_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setMomenta' of class" << 
    " 'PY8MEs_R12_P28_sm_qq_zzggg': Incorrect number of" << 
    " momenta specified." << endl; 
    throw PY8MEs_R12_P28_sm_qq_zzggg_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    if (momenta_picked[i].size() != 4)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R12_P28_sm_qq_zzggg': Incorrect number of" << 
      " momenta components specified." << endl; 
      throw PY8MEs_R12_P28_sm_qq_zzggg_exception; 
    }
    if (momenta_picked[i][0] < 0.0)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R12_P28_sm_qq_zzggg': A momentum was specified" << 
      " with negative energy. Check conventions." << endl; 
      throw PY8MEs_R12_P28_sm_qq_zzggg_exception; 
    }
    for (unsigned int j = 0; j < 4; j++ )
    {
      p[i][j] = momenta_picked[i][j]; 
    }
  }
}

//--------------------------------------------------------------------------
// Set color configuration to use. An empty vector means sum over all.
void PY8MEs_R12_P28_sm_qq_zzggg::setColors(vector<int> colors_picked)
{
  if (colors_picked.size() == 0)
  {
    user_colors = vector<int> (); 
    return; 
  }
  if (colors_picked.size() != (2 * nexternal))
  {
    cerr <<  "Error in function 'setColors' of class" << 
    " 'PY8MEs_R12_P28_sm_qq_zzggg': Incorrect number" << 
    " of colors specified." << endl; 
    throw PY8MEs_R12_P28_sm_qq_zzggg_exception; 
  }
  user_colors = vector<int> ((2 * nexternal), 0); 
  for(unsigned int i = 0; i < (2 * nexternal); i++ )
  {
    user_colors[i] = colors_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the helicity configuration to use. Am empty vector means sum over all.
void PY8MEs_R12_P28_sm_qq_zzggg::setHelicities(vector<int> helicities_picked) 
{
  if (helicities_picked.size() != nexternal)
  {
    if (helicities_picked.size() == 0)
    {
      user_helicities = vector<int> (); 
      return; 
    }
    cerr <<  "Error in function 'setHelicities' of class" << 
    " 'PY8MEs_R12_P28_sm_qq_zzggg': Incorrect number" << 
    " of helicities specified." << endl; 
    throw PY8MEs_R12_P28_sm_qq_zzggg_exception; 
  }
  user_helicities = vector<int> (nexternal, 0); 
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    user_helicities[i] = helicities_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the permutation to use (will apply to momenta, colors and helicities)
void PY8MEs_R12_P28_sm_qq_zzggg::setPermutation(vector<int> perm_picked) 
{
  if (perm_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setPermutations' of class" << 
    " 'PY8MEs_R12_P28_sm_qq_zzggg': Incorrect number" << 
    " of permutations specified." << endl; 
    throw PY8MEs_R12_P28_sm_qq_zzggg_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    perm[i] = perm_picked[i]; 
  }
}

// Set the proc_ID to use
void PY8MEs_R12_P28_sm_qq_zzggg::setProcID(int procID_picked) 
{
  proc_ID = procID_picked; 
}

//--------------------------------------------------------------------------
// Initialize process.

void PY8MEs_R12_P28_sm_qq_zzggg::initProc() 
{

  // Initialize flags
  include_symmetry_factors = true; 
  include_helicity_averaging_factors = true; 
  include_color_averaging_factors = true; 

  // Initialize vectors.
  perm = vector<int> (nexternal, 0); 
  user_colors = vector<int> (2 * nexternal, 0); 
  user_helicities = vector<int> (nexternal, 0); 
  p = vector < double * > (); 
  for (unsigned int i = 0; i < nexternal; i++ )
  {
    p.push_back(new double[4]); 
  }
  initColorConfigs(); 
  // Synchronize local variables dependent on the model with the active model.
  mME = vector<double> (nexternal, 0.); 
  syncProcModelParams(); 
  jamp2 = vector < vec_double > (2); 
  jamp2[0] = vector<double> (6, 0.); 
  jamp2[1] = vector<double> (6, 0.); 
  all_results = vector < vec_vec_double > (2); 
  // The first entry is always the color or helicity avg/summed matrix element.
  all_results[0] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[1] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
}

// Synchronize local variables of the process that depend on the model
// parameters
void PY8MEs_R12_P28_sm_qq_zzggg::syncProcModelParams() 
{

  // Instantiate the model class and set parameters that stay fixed during run
  mME[0] = pars->ZERO; 
  mME[1] = pars->ZERO; 
  mME[2] = pars->mdl_MZ; 
  mME[3] = pars->mdl_MZ; 
  mME[4] = pars->ZERO; 
  mME[5] = pars->ZERO; 
  mME[6] = pars->ZERO; 
}

//--------------------------------------------------------------------------
// Setter allowing to force particular values for the external masses
void PY8MEs_R12_P28_sm_qq_zzggg::setMasses(vec_double external_masses) 
{

  if (external_masses.size() != mME.size())
  {
    cerr <<  "Error in function 'setMasses' of class" << 
    " 'PY8MEs_R12_P28_sm_qq_zzggg': Incorrect number of" << 
    " masses specified." << endl; 
    throw PY8MEs_R12_P28_sm_qq_zzggg_exception; 
  }
  for (unsigned int j = 0; j < mME.size(); j++ )
  {
    mME[j] = external_masses[perm[j]]; 
  }
}

//--------------------------------------------------------------------------
// Getter accessing external masses with the correct ordering
vector<double> PY8MEs_R12_P28_sm_qq_zzggg::getMasses() 
{

  vec_double external_masses; 
  vector<int> invertedPerm; 
  invertedPerm = invert_mapping(perm); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    external_masses.push_back(mME[invertedPerm[i]]); 
  }
  return external_masses; 

}


// Set all values of the external masses to float(-mode) where mode can be
// 0 : Mass taken from the model
// 1 : Mass taken from p_i^2 if not massless to begin with
// 2 : Mass always taken from p_i^2.
void PY8MEs_R12_P28_sm_qq_zzggg::setExternalMassesMode(int mode) 
{
  if (mode != 0 && mode != 1 && mode != 2)
  {
    cerr <<  "Error in function 'setExternalMassesMode' of class" << 
    " 'PY8MEs_R12_P28_sm_qq_zzggg': Incorrect mode selected :" << mode << 
    ". It must be either 0, 1 or 2" << endl; 
    throw PY8MEs_R12_P28_sm_qq_zzggg_exception; 
  }
  if (mode == 0)
  {
    syncProcModelParams(); 
  }
  else if (mode == 1)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      if (mME[j] != pars->ZERO)
      {
        mME[j] = -1.0; 
      }
    }
  }
  else if (mode == 2)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      mME[j] = -1.0; 
    }
  }
}

//--------------------------------------------------------------------------
// Evaluate the squared matrix element.

double PY8MEs_R12_P28_sm_qq_zzggg::sigmaKin() 
{
  // Set the parameters which change event by event
  pars->setDependentParameters(); 
  pars->setDependentCouplings(); 
  // Reset color flows
  for(int i = 0; i < 6; i++ )
    jamp2[0][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[1][i] = 0.; 

  // Save previous values of mME
  vector<double> saved_mME(mME.size(), 0.0); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (mME[i] < 0.0)
    {
      saved_mME[i] = mME[i]; 
      mME[i] = sqrt(abs(pow(p[perm[i]][0], 2) - 
      (pow(p[perm[i]][1], 2) + pow(p[perm[i]][2], 2) + pow(p[perm[i]][3],
          2))));
    }
  }

  // Local variables and constants
  const int max_tries = 10; 
  vector < vec_bool > goodhel(nprocesses, vec_bool(ncomb, false)); 
  vec_int ntry(nprocesses, 0); 
  double t = 0.; 
  double result = 0.; 

  if (ntry[proc_ID] <= max_tries)
    ntry[proc_ID] = ntry[proc_ID] + 1; 

  // Find which helicity configuration is asked for
  // -1 indicates one wants to sum over helicities
  int user_ihel = getHelicityIDForConfig(user_helicities); 

  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = getColorIDForConfig(user_colors); 

  // Reset the list of results that will be recomputed here
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      all_results[proc_ID][ihel + 1][icolor + 1] = 0.; 
    }
  }

  // Calculate the matrix element for all helicities
  // unless already detected as vanishing
  for(int ihel = 0; ihel < ncomb; ihel++ )
  {
    // Skip helicity if already detected as vanishing
    if ((ntry[proc_ID] >= max_tries) && !goodhel[proc_ID][ihel])
      continue; 

    // Also skip helicity if user asks for a specific one
    if ((ntry[proc_ID] >= max_tries) && user_ihel != -1 && user_ihel != ihel)
      continue; 

    calculate_wavefunctions(helicities[ihel]); 

    // Reset locally computed color flows
    for(int i = 0; i < 6; i++ )
      jamp2[0][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[1][i] = 0.; 

    if (proc_ID == 0)
      t = matrix_12_uux_zzggg(); 
    if (proc_ID == 1)
      t = matrix_12_ddx_zzggg(); 

    // Store which helicities give non-zero result
    if ((ntry[proc_ID] < max_tries) && t != 0. && !goodhel[proc_ID][ihel])
      goodhel[proc_ID][ihel] = true; 

    // Aggregate results
    if (user_ihel == -1 || user_ihel == ihel)
    {
      if (user_icol == -1)
      {
        result = result + t; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][0] += t; 
          for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
          {
            all_results[proc_ID][0][i + 1] += jamp2[proc_ID][i]; 
          }
        }
        all_results[proc_ID][ihel + 1][0] += t; 
        for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
        {
          all_results[proc_ID][ihel + 1][i + 1] += jamp2[proc_ID][i]; 
        }
      }
      else
      {
        result = result + jamp2[proc_ID][user_icol]; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][user_icol + 1] += jamp2[proc_ID][user_icol]; 
        }
        all_results[proc_ID][ihel + 1][user_icol + 1] +=
            jamp2[proc_ID][user_icol];
      }
    }

  }

  // Normalize results with the identical particle factor
  if (include_symmetry_factors)
  {
    result = result/denom_iden[proc_ID]; 
  }
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      if (include_symmetry_factors)
      {
        all_results[proc_ID][ihel + 1][icolor + 1] /= denom_iden[proc_ID]; 
      }
    }
  }


  // Normalize when when summing+averaging over helicity configurations
  if (user_ihel == -1 && include_helicity_averaging_factors)
  {
    result /= denom_hels[proc_ID]; 
    if (user_icol == -1)
    {
      all_results[proc_ID][0][0] /= denom_hels[proc_ID]; 
      for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
      {
        all_results[proc_ID][0][i + 1] /= denom_hels[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][0][user_icol + 1] /= denom_hels[proc_ID]; 
    }
  }

  // Normalize when summing+averaging over color configurations
  if (user_icol == -1 && include_color_averaging_factors)
  {
    result /= denom_colors[proc_ID]; 
    if (user_ihel == -1)
    {
      all_results[proc_ID][0][0] /= denom_colors[proc_ID]; 
      for (unsigned int i = 0; i < ncomb; i++ )
      {
        all_results[proc_ID][i + 1][0] /= denom_colors[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][user_ihel + 1][0] /= denom_colors[proc_ID]; 
    }
  }

  // Reinstate previous values of mME
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (saved_mME[i] < 0.0)
    {
      mME[i] = saved_mME[i]; 
    }
  }

  // Finally return it
  return result; 
}

//==========================================================================
// Private class member functions

//--------------------------------------------------------------------------
// Evaluate |M|^2 for each subprocess

void PY8MEs_R12_P28_sm_qq_zzggg::calculate_wavefunctions(const int hel[])
{
  // Calculate wavefunctions for all processes
  // Calculate all wavefunctions
  ixxxxx(p[perm[0]], mME[0], hel[0], +1, w[0]); 
  oxxxxx(p[perm[1]], mME[1], hel[1], -1, w[1]); 
  vxxxxx(p[perm[2]], mME[2], hel[2], +1, w[2]); 
  vxxxxx(p[perm[3]], mME[3], hel[3], +1, w[3]); 
  vxxxxx(p[perm[4]], mME[4], hel[4], +1, w[4]); 
  vxxxxx(p[perm[5]], mME[5], hel[5], +1, w[5]); 
  vxxxxx(p[perm[6]], mME[6], hel[6], +1, w[6]); 
  VVV1P0_1(w[4], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[7]); 
  FFV2_5_1(w[1], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO, w[8]); 
  VVV1P0_1(w[7], w[6], pars->GC_10, pars->ZERO, pars->ZERO, w[9]); 
  FFV2_5_1(w[8], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[10]);
  FFV1_2(w[0], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[11]); 
  FFV1_1(w[8], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[12]); 
  FFV2_5_2(w[0], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[13]);
  FFV1_1(w[8], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[14]); 
  FFV1_2(w[13], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[15]); 
  FFV1_2(w[0], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[16]); 
  FFV1_2(w[16], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[17]); 
  FFV2_5_2(w[0], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[18]);
  FFV2_5_2(w[18], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[19]);
  FFV1_1(w[1], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[20]); 
  FFV1_2(w[18], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[21]); 
  FFV2_5_1(w[1], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[22]);
  FFV1_2(w[18], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[23]); 
  FFV1_1(w[22], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[24]); 
  FFV1_1(w[1], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[25]); 
  FFV1_1(w[25], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[26]); 
  FFV2_5_1(w[22], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[27]);
  FFV1_1(w[22], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[28]); 
  FFV2_5_2(w[13], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[29]);
  FFV1_2(w[13], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[30]); 
  FFV2_5_1(w[25], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[31]);
  FFV2_5_1(w[25], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[32]);
  FFV2_5_2(w[16], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[33]);
  FFV2_5_2(w[16], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[34]);
  VVV1P0_1(w[4], w[6], pars->GC_10, pars->ZERO, pars->ZERO, w[35]); 
  FFV1_1(w[1], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[36]); 
  FFV1_2(w[0], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[37]); 
  FFV2_5_1(w[36], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[38]);
  FFV2_5_1(w[36], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[39]);
  FFV1_1(w[36], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[40]); 
  FFV1_2(w[18], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[41]); 
  FFV1_2(w[13], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[42]); 
  FFV1_2(w[0], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[43]); 
  FFV1_1(w[1], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[44]); 
  FFV2_5_2(w[43], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[45]);
  FFV2_5_2(w[43], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[46]);
  FFV1_2(w[43], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[47]); 
  FFV1_1(w[8], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[48]); 
  FFV1_1(w[22], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[49]); 
  VVV1P0_1(w[35], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[50]); 
  FFV1_1(w[8], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[51]); 
  FFV1_2(w[18], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[52]); 
  FFV1_1(w[22], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[53]); 
  FFV1_2(w[13], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[54]); 
  FFV1_1(w[1], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[55]); 
  VVV1P0_1(w[5], w[6], pars->GC_10, pars->ZERO, pars->ZERO, w[56]); 
  FFV2_5_1(w[55], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[57]);
  FFV1_2(w[0], w[56], pars->GC_11, pars->ZERO, pars->ZERO, w[58]); 
  FFV2_5_1(w[55], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[59]);
  FFV1_1(w[55], w[56], pars->GC_11, pars->ZERO, pars->ZERO, w[60]); 
  FFV1_2(w[43], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[61]); 
  FFV1_1(w[55], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[62]); 
  FFV1_1(w[55], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[63]); 
  FFV1_2(w[16], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[64]); 
  FFV1_2(w[0], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[65]); 
  FFV2_5_2(w[65], w[2], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[66]);
  FFV1_1(w[1], w[56], pars->GC_11, pars->ZERO, pars->ZERO, w[67]); 
  FFV2_5_2(w[65], w[3], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[68]);
  FFV1_2(w[65], w[56], pars->GC_11, pars->ZERO, pars->ZERO, w[69]); 
  FFV1_1(w[36], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[70]); 
  FFV1_2(w[65], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[71]); 
  FFV1_2(w[65], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[72]); 
  FFV1_1(w[25], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[73]); 
  VVV1P0_1(w[4], w[56], pars->GC_10, pars->ZERO, pars->ZERO, w[74]); 
  FFV1_1(w[8], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[75]); 
  FFV1_2(w[13], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[76]); 
  FFV1_2(w[18], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[77]); 
  FFV1_1(w[22], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[78]); 
  FFV1_1(w[36], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[79]); 
  FFV1_2(w[16], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[80]); 
  FFV1_2(w[43], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[81]); 
  FFV1_1(w[25], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[82]); 
  VVVV1P0_1(w[4], w[5], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[83]); 
  VVVV3P0_1(w[4], w[5], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[84]); 
  VVVV4P0_1(w[4], w[5], w[6], pars->GC_12, pars->ZERO, pars->ZERO, w[85]); 
  FFV1_2(w[0], w[83], pars->GC_11, pars->ZERO, pars->ZERO, w[86]); 
  FFV1_2(w[0], w[84], pars->GC_11, pars->ZERO, pars->ZERO, w[87]); 
  FFV1_2(w[0], w[85], pars->GC_11, pars->ZERO, pars->ZERO, w[88]); 
  FFV1_1(w[1], w[83], pars->GC_11, pars->ZERO, pars->ZERO, w[89]); 
  FFV1_1(w[1], w[84], pars->GC_11, pars->ZERO, pars->ZERO, w[90]); 
  FFV1_1(w[1], w[85], pars->GC_11, pars->ZERO, pars->ZERO, w[91]); 
  FFV2_3_1(w[1], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[92]);
  FFV2_3_1(w[92], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[93]);
  FFV1_1(w[92], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[94]); 
  FFV2_3_2(w[0], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[95]);
  FFV1_1(w[92], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[96]); 
  FFV1_2(w[95], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[97]); 
  FFV2_3_2(w[0], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[98]);
  FFV2_3_2(w[98], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[99]);
  FFV1_2(w[98], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[100]); 
  FFV2_3_1(w[1], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[101]);
  FFV1_2(w[98], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[102]); 
  FFV1_1(w[101], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[103]); 
  FFV2_3_1(w[101], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[104]);
  FFV1_1(w[101], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[105]); 
  FFV2_3_2(w[95], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[106]);
  FFV1_2(w[95], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[107]); 
  FFV2_3_1(w[25], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[108]);
  FFV2_3_1(w[25], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[109]);
  FFV2_3_2(w[16], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[110]);
  FFV2_3_2(w[16], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[111]);
  FFV2_3_1(w[36], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[112]);
  FFV2_3_1(w[36], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[113]);
  FFV1_2(w[98], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[114]); 
  FFV1_2(w[95], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[115]); 
  FFV2_3_2(w[43], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[116]);
  FFV2_3_2(w[43], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[117]);
  FFV1_1(w[92], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[118]); 
  FFV1_1(w[101], w[35], pars->GC_11, pars->ZERO, pars->ZERO, w[119]); 
  FFV1_1(w[92], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[120]); 
  FFV1_2(w[98], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[121]); 
  FFV1_1(w[101], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[122]); 
  FFV1_2(w[95], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[123]); 
  FFV2_3_1(w[55], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[124]);
  FFV2_3_1(w[55], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[125]);
  FFV2_3_2(w[65], w[2], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[126]);
  FFV2_3_2(w[65], w[3], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[127]);
  FFV1_1(w[92], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[128]); 
  FFV1_2(w[95], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[129]); 
  FFV1_2(w[98], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[130]); 
  FFV1_1(w[101], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[131]); 

  // Calculate all amplitudes
  // Amplitude(s) for diagram number 0
  FFV1_0(w[0], w[10], w[9], pars->GC_11, amp[0]); 
  FFV1_0(w[11], w[10], w[6], pars->GC_11, amp[1]); 
  FFV2_5_0(w[11], w[12], w[3], pars->GC_51, pars->GC_58, amp[2]); 
  FFV1_0(w[13], w[14], w[6], pars->GC_11, amp[3]); 
  FFV1_0(w[15], w[8], w[6], pars->GC_11, amp[4]); 
  FFV1_0(w[13], w[8], w[9], pars->GC_11, amp[5]); 
  FFV2_5_0(w[16], w[14], w[3], pars->GC_51, pars->GC_58, amp[6]); 
  FFV2_5_0(w[17], w[8], w[3], pars->GC_51, pars->GC_58, amp[7]); 
  FFV1_0(w[19], w[1], w[9], pars->GC_11, amp[8]); 
  FFV1_0(w[19], w[20], w[6], pars->GC_11, amp[9]); 
  FFV2_5_0(w[21], w[20], w[3], pars->GC_51, pars->GC_58, amp[10]); 
  FFV1_0(w[23], w[22], w[6], pars->GC_11, amp[11]); 
  FFV1_0(w[18], w[24], w[6], pars->GC_11, amp[12]); 
  FFV1_0(w[18], w[22], w[9], pars->GC_11, amp[13]); 
  FFV2_5_0(w[23], w[25], w[3], pars->GC_51, pars->GC_58, amp[14]); 
  FFV2_5_0(w[18], w[26], w[3], pars->GC_51, pars->GC_58, amp[15]); 
  FFV1_0(w[0], w[27], w[9], pars->GC_11, amp[16]); 
  FFV1_0(w[11], w[27], w[6], pars->GC_11, amp[17]); 
  FFV2_5_0(w[11], w[28], w[2], pars->GC_51, pars->GC_58, amp[18]); 
  FFV2_5_0(w[16], w[24], w[2], pars->GC_51, pars->GC_58, amp[19]); 
  FFV2_5_0(w[17], w[22], w[2], pars->GC_51, pars->GC_58, amp[20]); 
  FFV1_0(w[29], w[1], w[9], pars->GC_11, amp[21]); 
  FFV1_0(w[29], w[20], w[6], pars->GC_11, amp[22]); 
  FFV2_5_0(w[30], w[20], w[2], pars->GC_51, pars->GC_58, amp[23]); 
  FFV2_5_0(w[15], w[25], w[2], pars->GC_51, pars->GC_58, amp[24]); 
  FFV2_5_0(w[13], w[26], w[2], pars->GC_51, pars->GC_58, amp[25]); 
  FFV2_5_0(w[11], w[31], w[3], pars->GC_51, pars->GC_58, amp[26]); 
  FFV2_5_0(w[11], w[32], w[2], pars->GC_51, pars->GC_58, amp[27]); 
  FFV2_5_0(w[33], w[20], w[3], pars->GC_51, pars->GC_58, amp[28]); 
  FFV2_5_0(w[34], w[20], w[2], pars->GC_51, pars->GC_58, amp[29]); 
  FFV2_5_0(w[37], w[38], w[3], pars->GC_51, pars->GC_58, amp[30]); 
  FFV2_5_0(w[37], w[39], w[2], pars->GC_51, pars->GC_58, amp[31]); 
  FFV2_5_0(w[18], w[40], w[3], pars->GC_51, pars->GC_58, amp[32]); 
  FFV2_5_0(w[41], w[36], w[3], pars->GC_51, pars->GC_58, amp[33]); 
  FFV2_5_0(w[13], w[40], w[2], pars->GC_51, pars->GC_58, amp[34]); 
  FFV2_5_0(w[42], w[36], w[2], pars->GC_51, pars->GC_58, amp[35]); 
  FFV2_5_0(w[45], w[44], w[3], pars->GC_51, pars->GC_58, amp[36]); 
  FFV2_5_0(w[46], w[44], w[2], pars->GC_51, pars->GC_58, amp[37]); 
  FFV2_5_0(w[47], w[8], w[3], pars->GC_51, pars->GC_58, amp[38]); 
  FFV2_5_0(w[43], w[48], w[3], pars->GC_51, pars->GC_58, amp[39]); 
  FFV2_5_0(w[47], w[22], w[2], pars->GC_51, pars->GC_58, amp[40]); 
  FFV2_5_0(w[43], w[49], w[2], pars->GC_51, pars->GC_58, amp[41]); 
  FFV1_0(w[0], w[10], w[50], pars->GC_11, amp[42]); 
  FFV2_5_0(w[37], w[51], w[3], pars->GC_51, pars->GC_58, amp[43]); 
  FFV1_0(w[37], w[10], w[5], pars->GC_11, amp[44]); 
  FFV1_0(w[13], w[8], w[50], pars->GC_11, amp[45]); 
  FFV1_0(w[13], w[48], w[5], pars->GC_11, amp[46]); 
  FFV1_0(w[42], w[8], w[5], pars->GC_11, amp[47]); 
  FFV1_0(w[19], w[1], w[50], pars->GC_11, amp[48]); 
  FFV2_5_0(w[52], w[44], w[3], pars->GC_51, pars->GC_58, amp[49]); 
  FFV1_0(w[19], w[44], w[5], pars->GC_11, amp[50]); 
  FFV1_0(w[18], w[22], w[50], pars->GC_11, amp[51]); 
  FFV1_0(w[41], w[22], w[5], pars->GC_11, amp[52]); 
  FFV1_0(w[18], w[49], w[5], pars->GC_11, amp[53]); 
  FFV1_0(w[0], w[27], w[50], pars->GC_11, amp[54]); 
  FFV2_5_0(w[37], w[53], w[2], pars->GC_51, pars->GC_58, amp[55]); 
  FFV1_0(w[37], w[27], w[5], pars->GC_11, amp[56]); 
  FFV1_0(w[29], w[1], w[50], pars->GC_11, amp[57]); 
  FFV2_5_0(w[54], w[44], w[2], pars->GC_51, pars->GC_58, amp[58]); 
  FFV1_0(w[29], w[44], w[5], pars->GC_11, amp[59]); 
  FFV2_5_0(w[58], w[57], w[3], pars->GC_51, pars->GC_58, amp[60]); 
  FFV2_5_0(w[58], w[59], w[2], pars->GC_51, pars->GC_58, amp[61]); 
  FFV2_5_0(w[18], w[60], w[3], pars->GC_51, pars->GC_58, amp[62]); 
  FFV1_0(w[18], w[59], w[56], pars->GC_11, amp[63]); 
  FFV2_5_0(w[13], w[60], w[2], pars->GC_51, pars->GC_58, amp[64]); 
  FFV1_0(w[13], w[57], w[56], pars->GC_11, amp[65]); 
  FFV1_0(w[46], w[57], w[6], pars->GC_11, amp[66]); 
  FFV2_5_0(w[61], w[57], w[3], pars->GC_51, pars->GC_58, amp[67]); 
  FFV1_0(w[45], w[59], w[6], pars->GC_11, amp[68]); 
  FFV2_5_0(w[61], w[59], w[2], pars->GC_51, pars->GC_58, amp[69]); 
  FFV2_5_0(w[45], w[62], w[3], pars->GC_51, pars->GC_58, amp[70]); 
  FFV2_5_0(w[46], w[62], w[2], pars->GC_51, pars->GC_58, amp[71]); 
  FFV1_0(w[19], w[63], w[6], pars->GC_11, amp[72]); 
  FFV2_5_0(w[21], w[63], w[3], pars->GC_51, pars->GC_58, amp[73]); 
  FFV1_0(w[52], w[59], w[6], pars->GC_11, amp[74]); 
  FFV1_0(w[21], w[59], w[5], pars->GC_11, amp[75]); 
  FFV2_5_0(w[52], w[62], w[3], pars->GC_51, pars->GC_58, amp[76]); 
  FFV1_0(w[19], w[62], w[5], pars->GC_11, amp[77]); 
  FFV1_0(w[29], w[63], w[6], pars->GC_11, amp[78]); 
  FFV2_5_0(w[30], w[63], w[2], pars->GC_51, pars->GC_58, amp[79]); 
  FFV1_0(w[54], w[57], w[6], pars->GC_11, amp[80]); 
  FFV1_0(w[30], w[57], w[5], pars->GC_11, amp[81]); 
  FFV2_5_0(w[54], w[62], w[2], pars->GC_51, pars->GC_58, amp[82]); 
  FFV1_0(w[29], w[62], w[5], pars->GC_11, amp[83]); 
  FFV2_5_0(w[33], w[63], w[3], pars->GC_51, pars->GC_58, amp[84]); 
  FFV2_5_0(w[34], w[63], w[2], pars->GC_51, pars->GC_58, amp[85]); 
  FFV2_5_0(w[64], w[57], w[3], pars->GC_51, pars->GC_58, amp[86]); 
  FFV1_0(w[34], w[57], w[5], pars->GC_11, amp[87]); 
  FFV2_5_0(w[64], w[59], w[2], pars->GC_51, pars->GC_58, amp[88]); 
  FFV1_0(w[33], w[59], w[5], pars->GC_11, amp[89]); 
  FFV2_5_0(w[66], w[67], w[3], pars->GC_51, pars->GC_58, amp[90]); 
  FFV2_5_0(w[68], w[67], w[2], pars->GC_51, pars->GC_58, amp[91]); 
  FFV2_5_0(w[69], w[8], w[3], pars->GC_51, pars->GC_58, amp[92]); 
  FFV1_0(w[68], w[8], w[56], pars->GC_11, amp[93]); 
  FFV2_5_0(w[69], w[22], w[2], pars->GC_51, pars->GC_58, amp[94]); 
  FFV1_0(w[66], w[22], w[56], pars->GC_11, amp[95]); 
  FFV1_0(w[66], w[39], w[6], pars->GC_11, amp[96]); 
  FFV2_5_0(w[66], w[70], w[3], pars->GC_51, pars->GC_58, amp[97]); 
  FFV1_0(w[68], w[38], w[6], pars->GC_11, amp[98]); 
  FFV2_5_0(w[68], w[70], w[2], pars->GC_51, pars->GC_58, amp[99]); 
  FFV2_5_0(w[71], w[38], w[3], pars->GC_51, pars->GC_58, amp[100]); 
  FFV2_5_0(w[71], w[39], w[2], pars->GC_51, pars->GC_58, amp[101]); 
  FFV1_0(w[72], w[10], w[6], pars->GC_11, amp[102]); 
  FFV2_5_0(w[72], w[12], w[3], pars->GC_51, pars->GC_58, amp[103]); 
  FFV1_0(w[68], w[51], w[6], pars->GC_11, amp[104]); 
  FFV1_0(w[68], w[12], w[5], pars->GC_11, amp[105]); 
  FFV2_5_0(w[71], w[51], w[3], pars->GC_51, pars->GC_58, amp[106]); 
  FFV1_0(w[71], w[10], w[5], pars->GC_11, amp[107]); 
  FFV1_0(w[72], w[27], w[6], pars->GC_11, amp[108]); 
  FFV2_5_0(w[72], w[28], w[2], pars->GC_51, pars->GC_58, amp[109]); 
  FFV1_0(w[66], w[53], w[6], pars->GC_11, amp[110]); 
  FFV1_0(w[66], w[28], w[5], pars->GC_11, amp[111]); 
  FFV2_5_0(w[71], w[53], w[2], pars->GC_51, pars->GC_58, amp[112]); 
  FFV1_0(w[71], w[27], w[5], pars->GC_11, amp[113]); 
  FFV2_5_0(w[72], w[31], w[3], pars->GC_51, pars->GC_58, amp[114]); 
  FFV2_5_0(w[72], w[32], w[2], pars->GC_51, pars->GC_58, amp[115]); 
  FFV2_5_0(w[66], w[73], w[3], pars->GC_51, pars->GC_58, amp[116]); 
  FFV1_0(w[66], w[32], w[5], pars->GC_11, amp[117]); 
  FFV2_5_0(w[68], w[73], w[2], pars->GC_51, pars->GC_58, amp[118]); 
  FFV1_0(w[68], w[31], w[5], pars->GC_11, amp[119]); 
  FFV1_0(w[0], w[10], w[74], pars->GC_11, amp[120]); 
  FFV2_5_0(w[58], w[75], w[3], pars->GC_51, pars->GC_58, amp[121]); 
  FFV1_0(w[58], w[10], w[4], pars->GC_11, amp[122]); 
  FFV1_0(w[13], w[8], w[74], pars->GC_11, amp[123]); 
  FFV1_0(w[13], w[75], w[56], pars->GC_11, amp[124]); 
  FFV1_0(w[76], w[8], w[56], pars->GC_11, amp[125]); 
  FFV1_0(w[19], w[1], w[74], pars->GC_11, amp[126]); 
  FFV2_5_0(w[77], w[67], w[3], pars->GC_51, pars->GC_58, amp[127]); 
  FFV1_0(w[19], w[67], w[4], pars->GC_11, amp[128]); 
  FFV1_0(w[18], w[22], w[74], pars->GC_11, amp[129]); 
  FFV1_0(w[77], w[22], w[56], pars->GC_11, amp[130]); 
  FFV1_0(w[18], w[78], w[56], pars->GC_11, amp[131]); 
  FFV1_0(w[0], w[27], w[74], pars->GC_11, amp[132]); 
  FFV2_5_0(w[58], w[78], w[2], pars->GC_51, pars->GC_58, amp[133]); 
  FFV1_0(w[58], w[27], w[4], pars->GC_11, amp[134]); 
  FFV1_0(w[29], w[1], w[74], pars->GC_11, amp[135]); 
  FFV2_5_0(w[76], w[67], w[2], pars->GC_51, pars->GC_58, amp[136]); 
  FFV1_0(w[29], w[67], w[4], pars->GC_11, amp[137]); 
  FFV1_0(w[19], w[79], w[6], pars->GC_11, amp[138]); 
  FFV2_5_0(w[21], w[79], w[3], pars->GC_51, pars->GC_58, amp[139]); 
  FFV1_0(w[77], w[39], w[6], pars->GC_11, amp[140]); 
  FFV2_5_0(w[77], w[70], w[3], pars->GC_51, pars->GC_58, amp[141]); 
  FFV1_0(w[21], w[39], w[4], pars->GC_11, amp[142]); 
  FFV1_0(w[19], w[70], w[4], pars->GC_11, amp[143]); 
  FFV1_0(w[29], w[79], w[6], pars->GC_11, amp[144]); 
  FFV2_5_0(w[30], w[79], w[2], pars->GC_51, pars->GC_58, amp[145]); 
  FFV1_0(w[76], w[38], w[6], pars->GC_11, amp[146]); 
  FFV2_5_0(w[76], w[70], w[2], pars->GC_51, pars->GC_58, amp[147]); 
  FFV1_0(w[30], w[38], w[4], pars->GC_11, amp[148]); 
  FFV1_0(w[29], w[70], w[4], pars->GC_11, amp[149]); 
  FFV2_5_0(w[33], w[79], w[3], pars->GC_51, pars->GC_58, amp[150]); 
  FFV2_5_0(w[34], w[79], w[2], pars->GC_51, pars->GC_58, amp[151]); 
  FFV2_5_0(w[80], w[38], w[3], pars->GC_51, pars->GC_58, amp[152]); 
  FFV2_5_0(w[80], w[39], w[2], pars->GC_51, pars->GC_58, amp[153]); 
  FFV1_0(w[34], w[38], w[4], pars->GC_11, amp[154]); 
  FFV1_0(w[33], w[39], w[4], pars->GC_11, amp[155]); 
  FFV1_0(w[81], w[10], w[6], pars->GC_11, amp[156]); 
  FFV2_5_0(w[81], w[12], w[3], pars->GC_51, pars->GC_58, amp[157]); 
  FFV1_0(w[46], w[75], w[6], pars->GC_11, amp[158]); 
  FFV2_5_0(w[61], w[75], w[3], pars->GC_51, pars->GC_58, amp[159]); 
  FFV1_0(w[46], w[12], w[4], pars->GC_11, amp[160]); 
  FFV1_0(w[61], w[10], w[4], pars->GC_11, amp[161]); 
  FFV1_0(w[81], w[27], w[6], pars->GC_11, amp[162]); 
  FFV2_5_0(w[81], w[28], w[2], pars->GC_51, pars->GC_58, amp[163]); 
  FFV1_0(w[45], w[78], w[6], pars->GC_11, amp[164]); 
  FFV2_5_0(w[61], w[78], w[2], pars->GC_51, pars->GC_58, amp[165]); 
  FFV1_0(w[45], w[28], w[4], pars->GC_11, amp[166]); 
  FFV1_0(w[61], w[27], w[4], pars->GC_11, amp[167]); 
  FFV2_5_0(w[81], w[31], w[3], pars->GC_51, pars->GC_58, amp[168]); 
  FFV2_5_0(w[81], w[32], w[2], pars->GC_51, pars->GC_58, amp[169]); 
  FFV2_5_0(w[45], w[82], w[3], pars->GC_51, pars->GC_58, amp[170]); 
  FFV2_5_0(w[46], w[82], w[2], pars->GC_51, pars->GC_58, amp[171]); 
  FFV1_0(w[45], w[32], w[4], pars->GC_11, amp[172]); 
  FFV1_0(w[46], w[31], w[4], pars->GC_11, amp[173]); 
  FFV1_0(w[54], w[75], w[6], pars->GC_11, amp[174]); 
  FFV1_0(w[30], w[75], w[5], pars->GC_11, amp[175]); 
  FFV1_0(w[76], w[51], w[6], pars->GC_11, amp[176]); 
  FFV1_0(w[76], w[12], w[5], pars->GC_11, amp[177]); 
  FFV1_0(w[30], w[51], w[4], pars->GC_11, amp[178]); 
  FFV1_0(w[54], w[12], w[4], pars->GC_11, amp[179]); 
  FFV2_5_0(w[64], w[75], w[3], pars->GC_51, pars->GC_58, amp[180]); 
  FFV1_0(w[34], w[75], w[5], pars->GC_11, amp[181]); 
  FFV2_5_0(w[80], w[51], w[3], pars->GC_51, pars->GC_58, amp[182]); 
  FFV1_0(w[80], w[10], w[5], pars->GC_11, amp[183]); 
  FFV1_0(w[34], w[51], w[4], pars->GC_11, amp[184]); 
  FFV1_0(w[64], w[10], w[4], pars->GC_11, amp[185]); 
  FFV1_0(w[77], w[53], w[6], pars->GC_11, amp[186]); 
  FFV1_0(w[77], w[28], w[5], pars->GC_11, amp[187]); 
  FFV1_0(w[52], w[78], w[6], pars->GC_11, amp[188]); 
  FFV1_0(w[21], w[78], w[5], pars->GC_11, amp[189]); 
  FFV1_0(w[52], w[28], w[4], pars->GC_11, amp[190]); 
  FFV1_0(w[21], w[53], w[4], pars->GC_11, amp[191]); 
  FFV2_5_0(w[77], w[73], w[3], pars->GC_51, pars->GC_58, amp[192]); 
  FFV1_0(w[77], w[32], w[5], pars->GC_11, amp[193]); 
  FFV2_5_0(w[52], w[82], w[3], pars->GC_51, pars->GC_58, amp[194]); 
  FFV1_0(w[19], w[82], w[5], pars->GC_11, amp[195]); 
  FFV1_0(w[52], w[32], w[4], pars->GC_11, amp[196]); 
  FFV1_0(w[19], w[73], w[4], pars->GC_11, amp[197]); 
  FFV2_5_0(w[64], w[78], w[2], pars->GC_51, pars->GC_58, amp[198]); 
  FFV1_0(w[33], w[78], w[5], pars->GC_11, amp[199]); 
  FFV2_5_0(w[80], w[53], w[2], pars->GC_51, pars->GC_58, amp[200]); 
  FFV1_0(w[80], w[27], w[5], pars->GC_11, amp[201]); 
  FFV1_0(w[33], w[53], w[4], pars->GC_11, amp[202]); 
  FFV1_0(w[64], w[27], w[4], pars->GC_11, amp[203]); 
  FFV2_5_0(w[76], w[73], w[2], pars->GC_51, pars->GC_58, amp[204]); 
  FFV1_0(w[76], w[31], w[5], pars->GC_11, amp[205]); 
  FFV2_5_0(w[54], w[82], w[2], pars->GC_51, pars->GC_58, amp[206]); 
  FFV1_0(w[29], w[82], w[5], pars->GC_11, amp[207]); 
  FFV1_0(w[54], w[31], w[4], pars->GC_11, amp[208]); 
  FFV1_0(w[29], w[73], w[4], pars->GC_11, amp[209]); 
  FFV2_5_0(w[86], w[8], w[3], pars->GC_51, pars->GC_58, amp[210]); 
  FFV2_5_0(w[87], w[8], w[3], pars->GC_51, pars->GC_58, amp[211]); 
  FFV2_5_0(w[88], w[8], w[3], pars->GC_51, pars->GC_58, amp[212]); 
  FFV1_0(w[13], w[8], w[83], pars->GC_11, amp[213]); 
  FFV1_0(w[13], w[8], w[84], pars->GC_11, amp[214]); 
  FFV1_0(w[13], w[8], w[85], pars->GC_11, amp[215]); 
  FFV2_5_0(w[18], w[89], w[3], pars->GC_51, pars->GC_58, amp[216]); 
  FFV2_5_0(w[18], w[90], w[3], pars->GC_51, pars->GC_58, amp[217]); 
  FFV2_5_0(w[18], w[91], w[3], pars->GC_51, pars->GC_58, amp[218]); 
  FFV1_0(w[18], w[22], w[83], pars->GC_11, amp[219]); 
  FFV1_0(w[18], w[22], w[84], pars->GC_11, amp[220]); 
  FFV1_0(w[18], w[22], w[85], pars->GC_11, amp[221]); 
  FFV2_5_0(w[86], w[22], w[2], pars->GC_51, pars->GC_58, amp[222]); 
  FFV2_5_0(w[87], w[22], w[2], pars->GC_51, pars->GC_58, amp[223]); 
  FFV2_5_0(w[88], w[22], w[2], pars->GC_51, pars->GC_58, amp[224]); 
  FFV2_5_0(w[13], w[89], w[2], pars->GC_51, pars->GC_58, amp[225]); 
  FFV2_5_0(w[13], w[90], w[2], pars->GC_51, pars->GC_58, amp[226]); 
  FFV2_5_0(w[13], w[91], w[2], pars->GC_51, pars->GC_58, amp[227]); 
  FFV1_0(w[0], w[93], w[9], pars->GC_11, amp[228]); 
  FFV1_0(w[11], w[93], w[6], pars->GC_11, amp[229]); 
  FFV2_3_0(w[11], w[94], w[3], pars->GC_50, pars->GC_58, amp[230]); 
  FFV1_0(w[95], w[96], w[6], pars->GC_11, amp[231]); 
  FFV1_0(w[97], w[92], w[6], pars->GC_11, amp[232]); 
  FFV1_0(w[95], w[92], w[9], pars->GC_11, amp[233]); 
  FFV2_3_0(w[16], w[96], w[3], pars->GC_50, pars->GC_58, amp[234]); 
  FFV2_3_0(w[17], w[92], w[3], pars->GC_50, pars->GC_58, amp[235]); 
  FFV1_0(w[99], w[1], w[9], pars->GC_11, amp[236]); 
  FFV1_0(w[99], w[20], w[6], pars->GC_11, amp[237]); 
  FFV2_3_0(w[100], w[20], w[3], pars->GC_50, pars->GC_58, amp[238]); 
  FFV1_0(w[102], w[101], w[6], pars->GC_11, amp[239]); 
  FFV1_0(w[98], w[103], w[6], pars->GC_11, amp[240]); 
  FFV1_0(w[98], w[101], w[9], pars->GC_11, amp[241]); 
  FFV2_3_0(w[102], w[25], w[3], pars->GC_50, pars->GC_58, amp[242]); 
  FFV2_3_0(w[98], w[26], w[3], pars->GC_50, pars->GC_58, amp[243]); 
  FFV1_0(w[0], w[104], w[9], pars->GC_11, amp[244]); 
  FFV1_0(w[11], w[104], w[6], pars->GC_11, amp[245]); 
  FFV2_3_0(w[11], w[105], w[2], pars->GC_50, pars->GC_58, amp[246]); 
  FFV2_3_0(w[16], w[103], w[2], pars->GC_50, pars->GC_58, amp[247]); 
  FFV2_3_0(w[17], w[101], w[2], pars->GC_50, pars->GC_58, amp[248]); 
  FFV1_0(w[106], w[1], w[9], pars->GC_11, amp[249]); 
  FFV1_0(w[106], w[20], w[6], pars->GC_11, amp[250]); 
  FFV2_3_0(w[107], w[20], w[2], pars->GC_50, pars->GC_58, amp[251]); 
  FFV2_3_0(w[97], w[25], w[2], pars->GC_50, pars->GC_58, amp[252]); 
  FFV2_3_0(w[95], w[26], w[2], pars->GC_50, pars->GC_58, amp[253]); 
  FFV2_3_0(w[11], w[108], w[3], pars->GC_50, pars->GC_58, amp[254]); 
  FFV2_3_0(w[11], w[109], w[2], pars->GC_50, pars->GC_58, amp[255]); 
  FFV2_3_0(w[110], w[20], w[3], pars->GC_50, pars->GC_58, amp[256]); 
  FFV2_3_0(w[111], w[20], w[2], pars->GC_50, pars->GC_58, amp[257]); 
  FFV2_3_0(w[37], w[112], w[3], pars->GC_50, pars->GC_58, amp[258]); 
  FFV2_3_0(w[37], w[113], w[2], pars->GC_50, pars->GC_58, amp[259]); 
  FFV2_3_0(w[98], w[40], w[3], pars->GC_50, pars->GC_58, amp[260]); 
  FFV2_3_0(w[114], w[36], w[3], pars->GC_50, pars->GC_58, amp[261]); 
  FFV2_3_0(w[95], w[40], w[2], pars->GC_50, pars->GC_58, amp[262]); 
  FFV2_3_0(w[115], w[36], w[2], pars->GC_50, pars->GC_58, amp[263]); 
  FFV2_3_0(w[116], w[44], w[3], pars->GC_50, pars->GC_58, amp[264]); 
  FFV2_3_0(w[117], w[44], w[2], pars->GC_50, pars->GC_58, amp[265]); 
  FFV2_3_0(w[47], w[92], w[3], pars->GC_50, pars->GC_58, amp[266]); 
  FFV2_3_0(w[43], w[118], w[3], pars->GC_50, pars->GC_58, amp[267]); 
  FFV2_3_0(w[47], w[101], w[2], pars->GC_50, pars->GC_58, amp[268]); 
  FFV2_3_0(w[43], w[119], w[2], pars->GC_50, pars->GC_58, amp[269]); 
  FFV1_0(w[0], w[93], w[50], pars->GC_11, amp[270]); 
  FFV2_3_0(w[37], w[120], w[3], pars->GC_50, pars->GC_58, amp[271]); 
  FFV1_0(w[37], w[93], w[5], pars->GC_11, amp[272]); 
  FFV1_0(w[95], w[92], w[50], pars->GC_11, amp[273]); 
  FFV1_0(w[95], w[118], w[5], pars->GC_11, amp[274]); 
  FFV1_0(w[115], w[92], w[5], pars->GC_11, amp[275]); 
  FFV1_0(w[99], w[1], w[50], pars->GC_11, amp[276]); 
  FFV2_3_0(w[121], w[44], w[3], pars->GC_50, pars->GC_58, amp[277]); 
  FFV1_0(w[99], w[44], w[5], pars->GC_11, amp[278]); 
  FFV1_0(w[98], w[101], w[50], pars->GC_11, amp[279]); 
  FFV1_0(w[114], w[101], w[5], pars->GC_11, amp[280]); 
  FFV1_0(w[98], w[119], w[5], pars->GC_11, amp[281]); 
  FFV1_0(w[0], w[104], w[50], pars->GC_11, amp[282]); 
  FFV2_3_0(w[37], w[122], w[2], pars->GC_50, pars->GC_58, amp[283]); 
  FFV1_0(w[37], w[104], w[5], pars->GC_11, amp[284]); 
  FFV1_0(w[106], w[1], w[50], pars->GC_11, amp[285]); 
  FFV2_3_0(w[123], w[44], w[2], pars->GC_50, pars->GC_58, amp[286]); 
  FFV1_0(w[106], w[44], w[5], pars->GC_11, amp[287]); 
  FFV2_3_0(w[58], w[124], w[3], pars->GC_50, pars->GC_58, amp[288]); 
  FFV2_3_0(w[58], w[125], w[2], pars->GC_50, pars->GC_58, amp[289]); 
  FFV2_3_0(w[98], w[60], w[3], pars->GC_50, pars->GC_58, amp[290]); 
  FFV1_0(w[98], w[125], w[56], pars->GC_11, amp[291]); 
  FFV2_3_0(w[95], w[60], w[2], pars->GC_50, pars->GC_58, amp[292]); 
  FFV1_0(w[95], w[124], w[56], pars->GC_11, amp[293]); 
  FFV1_0(w[117], w[124], w[6], pars->GC_11, amp[294]); 
  FFV2_3_0(w[61], w[124], w[3], pars->GC_50, pars->GC_58, amp[295]); 
  FFV1_0(w[116], w[125], w[6], pars->GC_11, amp[296]); 
  FFV2_3_0(w[61], w[125], w[2], pars->GC_50, pars->GC_58, amp[297]); 
  FFV2_3_0(w[116], w[62], w[3], pars->GC_50, pars->GC_58, amp[298]); 
  FFV2_3_0(w[117], w[62], w[2], pars->GC_50, pars->GC_58, amp[299]); 
  FFV1_0(w[99], w[63], w[6], pars->GC_11, amp[300]); 
  FFV2_3_0(w[100], w[63], w[3], pars->GC_50, pars->GC_58, amp[301]); 
  FFV1_0(w[121], w[125], w[6], pars->GC_11, amp[302]); 
  FFV1_0(w[100], w[125], w[5], pars->GC_11, amp[303]); 
  FFV2_3_0(w[121], w[62], w[3], pars->GC_50, pars->GC_58, amp[304]); 
  FFV1_0(w[99], w[62], w[5], pars->GC_11, amp[305]); 
  FFV1_0(w[106], w[63], w[6], pars->GC_11, amp[306]); 
  FFV2_3_0(w[107], w[63], w[2], pars->GC_50, pars->GC_58, amp[307]); 
  FFV1_0(w[123], w[124], w[6], pars->GC_11, amp[308]); 
  FFV1_0(w[107], w[124], w[5], pars->GC_11, amp[309]); 
  FFV2_3_0(w[123], w[62], w[2], pars->GC_50, pars->GC_58, amp[310]); 
  FFV1_0(w[106], w[62], w[5], pars->GC_11, amp[311]); 
  FFV2_3_0(w[110], w[63], w[3], pars->GC_50, pars->GC_58, amp[312]); 
  FFV2_3_0(w[111], w[63], w[2], pars->GC_50, pars->GC_58, amp[313]); 
  FFV2_3_0(w[64], w[124], w[3], pars->GC_50, pars->GC_58, amp[314]); 
  FFV1_0(w[111], w[124], w[5], pars->GC_11, amp[315]); 
  FFV2_3_0(w[64], w[125], w[2], pars->GC_50, pars->GC_58, amp[316]); 
  FFV1_0(w[110], w[125], w[5], pars->GC_11, amp[317]); 
  FFV2_3_0(w[126], w[67], w[3], pars->GC_50, pars->GC_58, amp[318]); 
  FFV2_3_0(w[127], w[67], w[2], pars->GC_50, pars->GC_58, amp[319]); 
  FFV2_3_0(w[69], w[92], w[3], pars->GC_50, pars->GC_58, amp[320]); 
  FFV1_0(w[127], w[92], w[56], pars->GC_11, amp[321]); 
  FFV2_3_0(w[69], w[101], w[2], pars->GC_50, pars->GC_58, amp[322]); 
  FFV1_0(w[126], w[101], w[56], pars->GC_11, amp[323]); 
  FFV1_0(w[126], w[113], w[6], pars->GC_11, amp[324]); 
  FFV2_3_0(w[126], w[70], w[3], pars->GC_50, pars->GC_58, amp[325]); 
  FFV1_0(w[127], w[112], w[6], pars->GC_11, amp[326]); 
  FFV2_3_0(w[127], w[70], w[2], pars->GC_50, pars->GC_58, amp[327]); 
  FFV2_3_0(w[71], w[112], w[3], pars->GC_50, pars->GC_58, amp[328]); 
  FFV2_3_0(w[71], w[113], w[2], pars->GC_50, pars->GC_58, amp[329]); 
  FFV1_0(w[72], w[93], w[6], pars->GC_11, amp[330]); 
  FFV2_3_0(w[72], w[94], w[3], pars->GC_50, pars->GC_58, amp[331]); 
  FFV1_0(w[127], w[120], w[6], pars->GC_11, amp[332]); 
  FFV1_0(w[127], w[94], w[5], pars->GC_11, amp[333]); 
  FFV2_3_0(w[71], w[120], w[3], pars->GC_50, pars->GC_58, amp[334]); 
  FFV1_0(w[71], w[93], w[5], pars->GC_11, amp[335]); 
  FFV1_0(w[72], w[104], w[6], pars->GC_11, amp[336]); 
  FFV2_3_0(w[72], w[105], w[2], pars->GC_50, pars->GC_58, amp[337]); 
  FFV1_0(w[126], w[122], w[6], pars->GC_11, amp[338]); 
  FFV1_0(w[126], w[105], w[5], pars->GC_11, amp[339]); 
  FFV2_3_0(w[71], w[122], w[2], pars->GC_50, pars->GC_58, amp[340]); 
  FFV1_0(w[71], w[104], w[5], pars->GC_11, amp[341]); 
  FFV2_3_0(w[72], w[108], w[3], pars->GC_50, pars->GC_58, amp[342]); 
  FFV2_3_0(w[72], w[109], w[2], pars->GC_50, pars->GC_58, amp[343]); 
  FFV2_3_0(w[126], w[73], w[3], pars->GC_50, pars->GC_58, amp[344]); 
  FFV1_0(w[126], w[109], w[5], pars->GC_11, amp[345]); 
  FFV2_3_0(w[127], w[73], w[2], pars->GC_50, pars->GC_58, amp[346]); 
  FFV1_0(w[127], w[108], w[5], pars->GC_11, amp[347]); 
  FFV1_0(w[0], w[93], w[74], pars->GC_11, amp[348]); 
  FFV2_3_0(w[58], w[128], w[3], pars->GC_50, pars->GC_58, amp[349]); 
  FFV1_0(w[58], w[93], w[4], pars->GC_11, amp[350]); 
  FFV1_0(w[95], w[92], w[74], pars->GC_11, amp[351]); 
  FFV1_0(w[95], w[128], w[56], pars->GC_11, amp[352]); 
  FFV1_0(w[129], w[92], w[56], pars->GC_11, amp[353]); 
  FFV1_0(w[99], w[1], w[74], pars->GC_11, amp[354]); 
  FFV2_3_0(w[130], w[67], w[3], pars->GC_50, pars->GC_58, amp[355]); 
  FFV1_0(w[99], w[67], w[4], pars->GC_11, amp[356]); 
  FFV1_0(w[98], w[101], w[74], pars->GC_11, amp[357]); 
  FFV1_0(w[130], w[101], w[56], pars->GC_11, amp[358]); 
  FFV1_0(w[98], w[131], w[56], pars->GC_11, amp[359]); 
  FFV1_0(w[0], w[104], w[74], pars->GC_11, amp[360]); 
  FFV2_3_0(w[58], w[131], w[2], pars->GC_50, pars->GC_58, amp[361]); 
  FFV1_0(w[58], w[104], w[4], pars->GC_11, amp[362]); 
  FFV1_0(w[106], w[1], w[74], pars->GC_11, amp[363]); 
  FFV2_3_0(w[129], w[67], w[2], pars->GC_50, pars->GC_58, amp[364]); 
  FFV1_0(w[106], w[67], w[4], pars->GC_11, amp[365]); 
  FFV1_0(w[99], w[79], w[6], pars->GC_11, amp[366]); 
  FFV2_3_0(w[100], w[79], w[3], pars->GC_50, pars->GC_58, amp[367]); 
  FFV1_0(w[130], w[113], w[6], pars->GC_11, amp[368]); 
  FFV2_3_0(w[130], w[70], w[3], pars->GC_50, pars->GC_58, amp[369]); 
  FFV1_0(w[100], w[113], w[4], pars->GC_11, amp[370]); 
  FFV1_0(w[99], w[70], w[4], pars->GC_11, amp[371]); 
  FFV1_0(w[106], w[79], w[6], pars->GC_11, amp[372]); 
  FFV2_3_0(w[107], w[79], w[2], pars->GC_50, pars->GC_58, amp[373]); 
  FFV1_0(w[129], w[112], w[6], pars->GC_11, amp[374]); 
  FFV2_3_0(w[129], w[70], w[2], pars->GC_50, pars->GC_58, amp[375]); 
  FFV1_0(w[107], w[112], w[4], pars->GC_11, amp[376]); 
  FFV1_0(w[106], w[70], w[4], pars->GC_11, amp[377]); 
  FFV2_3_0(w[110], w[79], w[3], pars->GC_50, pars->GC_58, amp[378]); 
  FFV2_3_0(w[111], w[79], w[2], pars->GC_50, pars->GC_58, amp[379]); 
  FFV2_3_0(w[80], w[112], w[3], pars->GC_50, pars->GC_58, amp[380]); 
  FFV2_3_0(w[80], w[113], w[2], pars->GC_50, pars->GC_58, amp[381]); 
  FFV1_0(w[111], w[112], w[4], pars->GC_11, amp[382]); 
  FFV1_0(w[110], w[113], w[4], pars->GC_11, amp[383]); 
  FFV1_0(w[81], w[93], w[6], pars->GC_11, amp[384]); 
  FFV2_3_0(w[81], w[94], w[3], pars->GC_50, pars->GC_58, amp[385]); 
  FFV1_0(w[117], w[128], w[6], pars->GC_11, amp[386]); 
  FFV2_3_0(w[61], w[128], w[3], pars->GC_50, pars->GC_58, amp[387]); 
  FFV1_0(w[117], w[94], w[4], pars->GC_11, amp[388]); 
  FFV1_0(w[61], w[93], w[4], pars->GC_11, amp[389]); 
  FFV1_0(w[81], w[104], w[6], pars->GC_11, amp[390]); 
  FFV2_3_0(w[81], w[105], w[2], pars->GC_50, pars->GC_58, amp[391]); 
  FFV1_0(w[116], w[131], w[6], pars->GC_11, amp[392]); 
  FFV2_3_0(w[61], w[131], w[2], pars->GC_50, pars->GC_58, amp[393]); 
  FFV1_0(w[116], w[105], w[4], pars->GC_11, amp[394]); 
  FFV1_0(w[61], w[104], w[4], pars->GC_11, amp[395]); 
  FFV2_3_0(w[81], w[108], w[3], pars->GC_50, pars->GC_58, amp[396]); 
  FFV2_3_0(w[81], w[109], w[2], pars->GC_50, pars->GC_58, amp[397]); 
  FFV2_3_0(w[116], w[82], w[3], pars->GC_50, pars->GC_58, amp[398]); 
  FFV2_3_0(w[117], w[82], w[2], pars->GC_50, pars->GC_58, amp[399]); 
  FFV1_0(w[116], w[109], w[4], pars->GC_11, amp[400]); 
  FFV1_0(w[117], w[108], w[4], pars->GC_11, amp[401]); 
  FFV1_0(w[123], w[128], w[6], pars->GC_11, amp[402]); 
  FFV1_0(w[107], w[128], w[5], pars->GC_11, amp[403]); 
  FFV1_0(w[129], w[120], w[6], pars->GC_11, amp[404]); 
  FFV1_0(w[129], w[94], w[5], pars->GC_11, amp[405]); 
  FFV1_0(w[107], w[120], w[4], pars->GC_11, amp[406]); 
  FFV1_0(w[123], w[94], w[4], pars->GC_11, amp[407]); 
  FFV2_3_0(w[64], w[128], w[3], pars->GC_50, pars->GC_58, amp[408]); 
  FFV1_0(w[111], w[128], w[5], pars->GC_11, amp[409]); 
  FFV2_3_0(w[80], w[120], w[3], pars->GC_50, pars->GC_58, amp[410]); 
  FFV1_0(w[80], w[93], w[5], pars->GC_11, amp[411]); 
  FFV1_0(w[111], w[120], w[4], pars->GC_11, amp[412]); 
  FFV1_0(w[64], w[93], w[4], pars->GC_11, amp[413]); 
  FFV1_0(w[130], w[122], w[6], pars->GC_11, amp[414]); 
  FFV1_0(w[130], w[105], w[5], pars->GC_11, amp[415]); 
  FFV1_0(w[121], w[131], w[6], pars->GC_11, amp[416]); 
  FFV1_0(w[100], w[131], w[5], pars->GC_11, amp[417]); 
  FFV1_0(w[121], w[105], w[4], pars->GC_11, amp[418]); 
  FFV1_0(w[100], w[122], w[4], pars->GC_11, amp[419]); 
  FFV2_3_0(w[130], w[73], w[3], pars->GC_50, pars->GC_58, amp[420]); 
  FFV1_0(w[130], w[109], w[5], pars->GC_11, amp[421]); 
  FFV2_3_0(w[121], w[82], w[3], pars->GC_50, pars->GC_58, amp[422]); 
  FFV1_0(w[99], w[82], w[5], pars->GC_11, amp[423]); 
  FFV1_0(w[121], w[109], w[4], pars->GC_11, amp[424]); 
  FFV1_0(w[99], w[73], w[4], pars->GC_11, amp[425]); 
  FFV2_3_0(w[64], w[131], w[2], pars->GC_50, pars->GC_58, amp[426]); 
  FFV1_0(w[110], w[131], w[5], pars->GC_11, amp[427]); 
  FFV2_3_0(w[80], w[122], w[2], pars->GC_50, pars->GC_58, amp[428]); 
  FFV1_0(w[80], w[104], w[5], pars->GC_11, amp[429]); 
  FFV1_0(w[110], w[122], w[4], pars->GC_11, amp[430]); 
  FFV1_0(w[64], w[104], w[4], pars->GC_11, amp[431]); 
  FFV2_3_0(w[129], w[73], w[2], pars->GC_50, pars->GC_58, amp[432]); 
  FFV1_0(w[129], w[108], w[5], pars->GC_11, amp[433]); 
  FFV2_3_0(w[123], w[82], w[2], pars->GC_50, pars->GC_58, amp[434]); 
  FFV1_0(w[106], w[82], w[5], pars->GC_11, amp[435]); 
  FFV1_0(w[123], w[108], w[4], pars->GC_11, amp[436]); 
  FFV1_0(w[106], w[73], w[4], pars->GC_11, amp[437]); 
  FFV2_3_0(w[86], w[92], w[3], pars->GC_50, pars->GC_58, amp[438]); 
  FFV2_3_0(w[87], w[92], w[3], pars->GC_50, pars->GC_58, amp[439]); 
  FFV2_3_0(w[88], w[92], w[3], pars->GC_50, pars->GC_58, amp[440]); 
  FFV1_0(w[95], w[92], w[83], pars->GC_11, amp[441]); 
  FFV1_0(w[95], w[92], w[84], pars->GC_11, amp[442]); 
  FFV1_0(w[95], w[92], w[85], pars->GC_11, amp[443]); 
  FFV2_3_0(w[98], w[89], w[3], pars->GC_50, pars->GC_58, amp[444]); 
  FFV2_3_0(w[98], w[90], w[3], pars->GC_50, pars->GC_58, amp[445]); 
  FFV2_3_0(w[98], w[91], w[3], pars->GC_50, pars->GC_58, amp[446]); 
  FFV1_0(w[98], w[101], w[83], pars->GC_11, amp[447]); 
  FFV1_0(w[98], w[101], w[84], pars->GC_11, amp[448]); 
  FFV1_0(w[98], w[101], w[85], pars->GC_11, amp[449]); 
  FFV2_3_0(w[86], w[101], w[2], pars->GC_50, pars->GC_58, amp[450]); 
  FFV2_3_0(w[87], w[101], w[2], pars->GC_50, pars->GC_58, amp[451]); 
  FFV2_3_0(w[88], w[101], w[2], pars->GC_50, pars->GC_58, amp[452]); 
  FFV2_3_0(w[95], w[89], w[2], pars->GC_50, pars->GC_58, amp[453]); 
  FFV2_3_0(w[95], w[90], w[2], pars->GC_50, pars->GC_58, amp[454]); 
  FFV2_3_0(w[95], w[91], w[2], pars->GC_50, pars->GC_58, amp[455]); 


}
double PY8MEs_R12_P28_sm_qq_zzggg::matrix_12_uux_zzggg() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 228;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = -amp[0] - Complex<double> (0, 1) * amp[3] - amp[5] -
      Complex<double> (0, 1) * amp[6] - Complex<double> (0, 1) * amp[7] -
      amp[8] - Complex<double> (0, 1) * amp[9] - Complex<double> (0, 1) *
      amp[10] - Complex<double> (0, 1) * amp[12] - amp[13] - amp[16] -
      Complex<double> (0, 1) * amp[19] - Complex<double> (0, 1) * amp[20] -
      amp[21] - Complex<double> (0, 1) * amp[22] - Complex<double> (0, 1) *
      amp[23] - Complex<double> (0, 1) * amp[28] - Complex<double> (0, 1) *
      amp[29] - Complex<double> (0, 1) * amp[60] - Complex<double> (0, 1) *
      amp[61] - Complex<double> (0, 1) * amp[62] - Complex<double> (0, 1) *
      amp[63] - Complex<double> (0, 1) * amp[64] - Complex<double> (0, 1) *
      amp[65] + amp[72] + amp[73] + amp[75] + amp[78] + amp[79] + amp[81] +
      amp[84] + amp[85] + amp[86] + amp[87] + amp[88] + amp[89] - amp[120] -
      Complex<double> (0, 1) * amp[121] - Complex<double> (0, 1) * amp[122] -
      amp[123] - Complex<double> (0, 1) * amp[124] - amp[126] - amp[129] -
      Complex<double> (0, 1) * amp[131] - amp[132] - Complex<double> (0, 1) *
      amp[133] - Complex<double> (0, 1) * amp[134] - amp[135] + amp[175] +
      amp[180] + amp[181] + amp[185] + amp[189] + amp[198] + amp[199] +
      amp[203] - amp[210] + amp[212] + amp[215] - amp[213] + amp[218] -
      amp[216] + amp[221] - amp[219] - amp[222] + amp[224] + amp[227] -
      amp[225];
  jamp[1] = -Complex<double> (0, 1) * amp[36] - Complex<double> (0, 1) *
      amp[37] - Complex<double> (0, 1) * amp[38] - Complex<double> (0, 1) *
      amp[39] - Complex<double> (0, 1) * amp[40] - Complex<double> (0, 1) *
      amp[41] - amp[42] - amp[45] - Complex<double> (0, 1) * amp[46] - amp[48]
      - Complex<double> (0, 1) * amp[49] - Complex<double> (0, 1) * amp[50] -
      amp[51] - Complex<double> (0, 1) * amp[53] - amp[54] - amp[57] -
      Complex<double> (0, 1) * amp[58] - Complex<double> (0, 1) * amp[59] +
      Complex<double> (0, 1) * amp[60] + Complex<double> (0, 1) * amp[61] +
      Complex<double> (0, 1) * amp[62] + Complex<double> (0, 1) * amp[63] +
      Complex<double> (0, 1) * amp[64] + Complex<double> (0, 1) * amp[65] +
      amp[66] + amp[67] + amp[68] + amp[69] + amp[70] + amp[71] + amp[74] +
      amp[76] + amp[77] + amp[80] + amp[82] + amp[83] + amp[120] +
      Complex<double> (0, 1) * amp[121] + Complex<double> (0, 1) * amp[122] +
      amp[123] + Complex<double> (0, 1) * amp[124] + amp[126] + amp[129] +
      Complex<double> (0, 1) * amp[131] + amp[132] + Complex<double> (0, 1) *
      amp[133] + Complex<double> (0, 1) * amp[134] + amp[135] + amp[158] +
      amp[159] + amp[161] + amp[164] + amp[165] + amp[167] + amp[174] +
      amp[188] + amp[210] + amp[211] + amp[213] + amp[214] + amp[216] +
      amp[217] + amp[219] + amp[220] + amp[222] + amp[223] + amp[225] +
      amp[226];
  jamp[2] = +amp[0] + Complex<double> (0, 1) * amp[3] + amp[5] +
      Complex<double> (0, 1) * amp[6] + Complex<double> (0, 1) * amp[7] +
      amp[8] + Complex<double> (0, 1) * amp[9] + Complex<double> (0, 1) *
      amp[10] + Complex<double> (0, 1) * amp[12] + amp[13] + amp[16] +
      Complex<double> (0, 1) * amp[19] + Complex<double> (0, 1) * amp[20] +
      amp[21] + Complex<double> (0, 1) * amp[22] + Complex<double> (0, 1) *
      amp[23] + Complex<double> (0, 1) * amp[28] + Complex<double> (0, 1) *
      amp[29] - Complex<double> (0, 1) * amp[30] - Complex<double> (0, 1) *
      amp[31] - Complex<double> (0, 1) * amp[32] - Complex<double> (0, 1) *
      amp[33] - Complex<double> (0, 1) * amp[34] - Complex<double> (0, 1) *
      amp[35] + amp[42] - Complex<double> (0, 1) * amp[43] - Complex<double>
      (0, 1) * amp[44] + amp[45] - Complex<double> (0, 1) * amp[47] + amp[48] +
      amp[51] - Complex<double> (0, 1) * amp[52] + amp[54] - Complex<double>
      (0, 1) * amp[55] - Complex<double> (0, 1) * amp[56] + amp[57] + amp[138]
      + amp[139] + amp[142] + amp[144] + amp[145] + amp[148] + amp[150] +
      amp[151] + amp[152] + amp[153] + amp[154] + amp[155] + amp[178] +
      amp[182] + amp[183] + amp[184] + amp[191] + amp[200] + amp[201] +
      amp[202] - amp[211] - amp[212] - amp[215] - amp[214] - amp[218] -
      amp[217] - amp[221] - amp[220] - amp[223] - amp[224] - amp[227] -
      amp[226];
  jamp[3] = +Complex<double> (0, 1) * amp[30] + Complex<double> (0, 1) *
      amp[31] + Complex<double> (0, 1) * amp[32] + Complex<double> (0, 1) *
      amp[33] + Complex<double> (0, 1) * amp[34] + Complex<double> (0, 1) *
      amp[35] - amp[42] + Complex<double> (0, 1) * amp[43] + Complex<double>
      (0, 1) * amp[44] - amp[45] + Complex<double> (0, 1) * amp[47] - amp[48] -
      amp[51] + Complex<double> (0, 1) * amp[52] - amp[54] + Complex<double>
      (0, 1) * amp[55] + Complex<double> (0, 1) * amp[56] - amp[57] -
      Complex<double> (0, 1) * amp[90] - Complex<double> (0, 1) * amp[91] -
      Complex<double> (0, 1) * amp[92] - Complex<double> (0, 1) * amp[93] -
      Complex<double> (0, 1) * amp[94] - Complex<double> (0, 1) * amp[95] +
      amp[96] + amp[97] + amp[98] + amp[99] + amp[100] + amp[101] + amp[104] +
      amp[106] + amp[107] + amp[110] + amp[112] + amp[113] + amp[120] +
      amp[123] - Complex<double> (0, 1) * amp[125] + amp[126] - Complex<double>
      (0, 1) * amp[127] - Complex<double> (0, 1) * amp[128] + amp[129] -
      Complex<double> (0, 1) * amp[130] + amp[132] + amp[135] - Complex<double>
      (0, 1) * amp[136] - Complex<double> (0, 1) * amp[137] + amp[140] +
      amp[141] + amp[143] + amp[146] + amp[147] + amp[149] + amp[176] +
      amp[186] + amp[210] + amp[211] + amp[213] + amp[214] + amp[216] +
      amp[217] + amp[219] + amp[220] + amp[222] + amp[223] + amp[225] +
      amp[226];
  jamp[4] = +amp[0] - Complex<double> (0, 1) * amp[1] - Complex<double> (0, 1)
      * amp[2] - Complex<double> (0, 1) * amp[4] + amp[5] + amp[8] -
      Complex<double> (0, 1) * amp[11] + amp[13] - Complex<double> (0, 1) *
      amp[14] - Complex<double> (0, 1) * amp[15] + amp[16] - Complex<double>
      (0, 1) * amp[17] - Complex<double> (0, 1) * amp[18] + amp[21] -
      Complex<double> (0, 1) * amp[24] - Complex<double> (0, 1) * amp[25] -
      Complex<double> (0, 1) * amp[26] - Complex<double> (0, 1) * amp[27] +
      Complex<double> (0, 1) * amp[36] + Complex<double> (0, 1) * amp[37] +
      Complex<double> (0, 1) * amp[38] + Complex<double> (0, 1) * amp[39] +
      Complex<double> (0, 1) * amp[40] + Complex<double> (0, 1) * amp[41] +
      amp[42] + amp[45] + Complex<double> (0, 1) * amp[46] + amp[48] +
      Complex<double> (0, 1) * amp[49] + Complex<double> (0, 1) * amp[50] +
      amp[51] + Complex<double> (0, 1) * amp[53] + amp[54] + amp[57] +
      Complex<double> (0, 1) * amp[58] + Complex<double> (0, 1) * amp[59] +
      amp[156] + amp[157] + amp[160] + amp[162] + amp[163] + amp[166] +
      amp[168] + amp[169] + amp[170] + amp[171] + amp[172] + amp[173] +
      amp[179] + amp[190] + amp[194] + amp[195] + amp[196] + amp[206] +
      amp[207] + amp[208] - amp[211] - amp[212] - amp[215] - amp[214] -
      amp[218] - amp[217] - amp[221] - amp[220] - amp[223] - amp[224] -
      amp[227] - amp[226];
  jamp[5] = -amp[0] + Complex<double> (0, 1) * amp[1] + Complex<double> (0, 1)
      * amp[2] + Complex<double> (0, 1) * amp[4] - amp[5] - amp[8] +
      Complex<double> (0, 1) * amp[11] - amp[13] + Complex<double> (0, 1) *
      amp[14] + Complex<double> (0, 1) * amp[15] - amp[16] + Complex<double>
      (0, 1) * amp[17] + Complex<double> (0, 1) * amp[18] - amp[21] +
      Complex<double> (0, 1) * amp[24] + Complex<double> (0, 1) * amp[25] +
      Complex<double> (0, 1) * amp[26] + Complex<double> (0, 1) * amp[27] +
      Complex<double> (0, 1) * amp[90] + Complex<double> (0, 1) * amp[91] +
      Complex<double> (0, 1) * amp[92] + Complex<double> (0, 1) * amp[93] +
      Complex<double> (0, 1) * amp[94] + Complex<double> (0, 1) * amp[95] +
      amp[102] + amp[103] + amp[105] + amp[108] + amp[109] + amp[111] +
      amp[114] + amp[115] + amp[116] + amp[117] + amp[118] + amp[119] -
      amp[120] - amp[123] + Complex<double> (0, 1) * amp[125] - amp[126] +
      Complex<double> (0, 1) * amp[127] + Complex<double> (0, 1) * amp[128] -
      amp[129] + Complex<double> (0, 1) * amp[130] - amp[132] - amp[135] +
      Complex<double> (0, 1) * amp[136] + Complex<double> (0, 1) * amp[137] +
      amp[177] + amp[187] + amp[192] + amp[193] + amp[197] + amp[204] +
      amp[205] + amp[209] - amp[210] + amp[212] + amp[215] - amp[213] +
      amp[218] - amp[216] + amp[221] - amp[219] - amp[222] + amp[224] +
      amp[227] - amp[225];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[0][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P28_sm_qq_zzggg::matrix_12_ddx_zzggg() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 228;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = -amp[228] - Complex<double> (0, 1) * amp[231] - amp[233] -
      Complex<double> (0, 1) * amp[234] - Complex<double> (0, 1) * amp[235] -
      amp[236] - Complex<double> (0, 1) * amp[237] - Complex<double> (0, 1) *
      amp[238] - Complex<double> (0, 1) * amp[240] - amp[241] - amp[244] -
      Complex<double> (0, 1) * amp[247] - Complex<double> (0, 1) * amp[248] -
      amp[249] - Complex<double> (0, 1) * amp[250] - Complex<double> (0, 1) *
      amp[251] - Complex<double> (0, 1) * amp[256] - Complex<double> (0, 1) *
      amp[257] - Complex<double> (0, 1) * amp[288] - Complex<double> (0, 1) *
      amp[289] - Complex<double> (0, 1) * amp[290] - Complex<double> (0, 1) *
      amp[291] - Complex<double> (0, 1) * amp[292] - Complex<double> (0, 1) *
      amp[293] + amp[300] + amp[301] + amp[303] + amp[306] + amp[307] +
      amp[309] + amp[312] + amp[313] + amp[314] + amp[315] + amp[316] +
      amp[317] - amp[348] - Complex<double> (0, 1) * amp[349] - Complex<double>
      (0, 1) * amp[350] - amp[351] - Complex<double> (0, 1) * amp[352] -
      amp[354] - amp[357] - Complex<double> (0, 1) * amp[359] - amp[360] -
      Complex<double> (0, 1) * amp[361] - Complex<double> (0, 1) * amp[362] -
      amp[363] + amp[403] + amp[408] + amp[409] + amp[413] + amp[417] +
      amp[426] + amp[427] + amp[431] - amp[438] + amp[440] + amp[443] -
      amp[441] + amp[446] - amp[444] + amp[449] - amp[447] - amp[450] +
      amp[452] + amp[455] - amp[453];
  jamp[1] = -Complex<double> (0, 1) * amp[264] - Complex<double> (0, 1) *
      amp[265] - Complex<double> (0, 1) * amp[266] - Complex<double> (0, 1) *
      amp[267] - Complex<double> (0, 1) * amp[268] - Complex<double> (0, 1) *
      amp[269] - amp[270] - amp[273] - Complex<double> (0, 1) * amp[274] -
      amp[276] - Complex<double> (0, 1) * amp[277] - Complex<double> (0, 1) *
      amp[278] - amp[279] - Complex<double> (0, 1) * amp[281] - amp[282] -
      amp[285] - Complex<double> (0, 1) * amp[286] - Complex<double> (0, 1) *
      amp[287] + Complex<double> (0, 1) * amp[288] + Complex<double> (0, 1) *
      amp[289] + Complex<double> (0, 1) * amp[290] + Complex<double> (0, 1) *
      amp[291] + Complex<double> (0, 1) * amp[292] + Complex<double> (0, 1) *
      amp[293] + amp[294] + amp[295] + amp[296] + amp[297] + amp[298] +
      amp[299] + amp[302] + amp[304] + amp[305] + amp[308] + amp[310] +
      amp[311] + amp[348] + Complex<double> (0, 1) * amp[349] + Complex<double>
      (0, 1) * amp[350] + amp[351] + Complex<double> (0, 1) * amp[352] +
      amp[354] + amp[357] + Complex<double> (0, 1) * amp[359] + amp[360] +
      Complex<double> (0, 1) * amp[361] + Complex<double> (0, 1) * amp[362] +
      amp[363] + amp[386] + amp[387] + amp[389] + amp[392] + amp[393] +
      amp[395] + amp[402] + amp[416] + amp[438] + amp[439] + amp[441] +
      amp[442] + amp[444] + amp[445] + amp[447] + amp[448] + amp[450] +
      amp[451] + amp[453] + amp[454];
  jamp[2] = +amp[228] + Complex<double> (0, 1) * amp[231] + amp[233] +
      Complex<double> (0, 1) * amp[234] + Complex<double> (0, 1) * amp[235] +
      amp[236] + Complex<double> (0, 1) * amp[237] + Complex<double> (0, 1) *
      amp[238] + Complex<double> (0, 1) * amp[240] + amp[241] + amp[244] +
      Complex<double> (0, 1) * amp[247] + Complex<double> (0, 1) * amp[248] +
      amp[249] + Complex<double> (0, 1) * amp[250] + Complex<double> (0, 1) *
      amp[251] + Complex<double> (0, 1) * amp[256] + Complex<double> (0, 1) *
      amp[257] - Complex<double> (0, 1) * amp[258] - Complex<double> (0, 1) *
      amp[259] - Complex<double> (0, 1) * amp[260] - Complex<double> (0, 1) *
      amp[261] - Complex<double> (0, 1) * amp[262] - Complex<double> (0, 1) *
      amp[263] + amp[270] - Complex<double> (0, 1) * amp[271] - Complex<double>
      (0, 1) * amp[272] + amp[273] - Complex<double> (0, 1) * amp[275] +
      amp[276] + amp[279] - Complex<double> (0, 1) * amp[280] + amp[282] -
      Complex<double> (0, 1) * amp[283] - Complex<double> (0, 1) * amp[284] +
      amp[285] + amp[366] + amp[367] + amp[370] + amp[372] + amp[373] +
      amp[376] + amp[378] + amp[379] + amp[380] + amp[381] + amp[382] +
      amp[383] + amp[406] + amp[410] + amp[411] + amp[412] + amp[419] +
      amp[428] + amp[429] + amp[430] - amp[439] - amp[440] - amp[443] -
      amp[442] - amp[446] - amp[445] - amp[449] - amp[448] - amp[451] -
      amp[452] - amp[455] - amp[454];
  jamp[3] = +Complex<double> (0, 1) * amp[258] + Complex<double> (0, 1) *
      amp[259] + Complex<double> (0, 1) * amp[260] + Complex<double> (0, 1) *
      amp[261] + Complex<double> (0, 1) * amp[262] + Complex<double> (0, 1) *
      amp[263] - amp[270] + Complex<double> (0, 1) * amp[271] + Complex<double>
      (0, 1) * amp[272] - amp[273] + Complex<double> (0, 1) * amp[275] -
      amp[276] - amp[279] + Complex<double> (0, 1) * amp[280] - amp[282] +
      Complex<double> (0, 1) * amp[283] + Complex<double> (0, 1) * amp[284] -
      amp[285] - Complex<double> (0, 1) * amp[318] - Complex<double> (0, 1) *
      amp[319] - Complex<double> (0, 1) * amp[320] - Complex<double> (0, 1) *
      amp[321] - Complex<double> (0, 1) * amp[322] - Complex<double> (0, 1) *
      amp[323] + amp[324] + amp[325] + amp[326] + amp[327] + amp[328] +
      amp[329] + amp[332] + amp[334] + amp[335] + amp[338] + amp[340] +
      amp[341] + amp[348] + amp[351] - Complex<double> (0, 1) * amp[353] +
      amp[354] - Complex<double> (0, 1) * amp[355] - Complex<double> (0, 1) *
      amp[356] + amp[357] - Complex<double> (0, 1) * amp[358] + amp[360] +
      amp[363] - Complex<double> (0, 1) * amp[364] - Complex<double> (0, 1) *
      amp[365] + amp[368] + amp[369] + amp[371] + amp[374] + amp[375] +
      amp[377] + amp[404] + amp[414] + amp[438] + amp[439] + amp[441] +
      amp[442] + amp[444] + amp[445] + amp[447] + amp[448] + amp[450] +
      amp[451] + amp[453] + amp[454];
  jamp[4] = +amp[228] - Complex<double> (0, 1) * amp[229] - Complex<double> (0,
      1) * amp[230] - Complex<double> (0, 1) * amp[232] + amp[233] + amp[236] -
      Complex<double> (0, 1) * amp[239] + amp[241] - Complex<double> (0, 1) *
      amp[242] - Complex<double> (0, 1) * amp[243] + amp[244] - Complex<double>
      (0, 1) * amp[245] - Complex<double> (0, 1) * amp[246] + amp[249] -
      Complex<double> (0, 1) * amp[252] - Complex<double> (0, 1) * amp[253] -
      Complex<double> (0, 1) * amp[254] - Complex<double> (0, 1) * amp[255] +
      Complex<double> (0, 1) * amp[264] + Complex<double> (0, 1) * amp[265] +
      Complex<double> (0, 1) * amp[266] + Complex<double> (0, 1) * amp[267] +
      Complex<double> (0, 1) * amp[268] + Complex<double> (0, 1) * amp[269] +
      amp[270] + amp[273] + Complex<double> (0, 1) * amp[274] + amp[276] +
      Complex<double> (0, 1) * amp[277] + Complex<double> (0, 1) * amp[278] +
      amp[279] + Complex<double> (0, 1) * amp[281] + amp[282] + amp[285] +
      Complex<double> (0, 1) * amp[286] + Complex<double> (0, 1) * amp[287] +
      amp[384] + amp[385] + amp[388] + amp[390] + amp[391] + amp[394] +
      amp[396] + amp[397] + amp[398] + amp[399] + amp[400] + amp[401] +
      amp[407] + amp[418] + amp[422] + amp[423] + amp[424] + amp[434] +
      amp[435] + amp[436] - amp[439] - amp[440] - amp[443] - amp[442] -
      amp[446] - amp[445] - amp[449] - amp[448] - amp[451] - amp[452] -
      amp[455] - amp[454];
  jamp[5] = -amp[228] + Complex<double> (0, 1) * amp[229] + Complex<double> (0,
      1) * amp[230] + Complex<double> (0, 1) * amp[232] - amp[233] - amp[236] +
      Complex<double> (0, 1) * amp[239] - amp[241] + Complex<double> (0, 1) *
      amp[242] + Complex<double> (0, 1) * amp[243] - amp[244] + Complex<double>
      (0, 1) * amp[245] + Complex<double> (0, 1) * amp[246] - amp[249] +
      Complex<double> (0, 1) * amp[252] + Complex<double> (0, 1) * amp[253] +
      Complex<double> (0, 1) * amp[254] + Complex<double> (0, 1) * amp[255] +
      Complex<double> (0, 1) * amp[318] + Complex<double> (0, 1) * amp[319] +
      Complex<double> (0, 1) * amp[320] + Complex<double> (0, 1) * amp[321] +
      Complex<double> (0, 1) * amp[322] + Complex<double> (0, 1) * amp[323] +
      amp[330] + amp[331] + amp[333] + amp[336] + amp[337] + amp[339] +
      amp[342] + amp[343] + amp[344] + amp[345] + amp[346] + amp[347] -
      amp[348] - amp[351] + Complex<double> (0, 1) * amp[353] - amp[354] +
      Complex<double> (0, 1) * amp[355] + Complex<double> (0, 1) * amp[356] -
      amp[357] + Complex<double> (0, 1) * amp[358] - amp[360] - amp[363] +
      Complex<double> (0, 1) * amp[364] + Complex<double> (0, 1) * amp[365] +
      amp[405] + amp[415] + amp[420] + amp[421] + amp[425] + amp[432] +
      amp[433] + amp[437] - amp[438] + amp[440] + amp[443] - amp[441] +
      amp[446] - amp[444] + amp[449] - amp[447] - amp[450] + amp[452] +
      amp[455] - amp[453];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[1][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}


}  // end namespace PY8MEs_namespace

