//==========================================================================
// This file has been automatically generated for Pythia 8
// MadGraph5_aMC@NLO v. 2.6.0, 2017-08-16
// By the MadGraph5_aMC@NLO Development Team
// Visit launchpad.net/madgraph5 and amcatnlo.web.cern.ch
//==========================================================================

#ifndef PY8MEs_R4_P76_sm_qq_llgqq_H
#define PY8MEs_R4_P76_sm_qq_llgqq_H

#include "Complex.h" 
#include <vector> 
#include <set> 
#include <exception> 
#include <iostream> 

#include "Parameters_sm.h"
#include "PY8MEs.h"

using namespace std; 

namespace PY8MEs_namespace 
{
//==========================================================================
// A class for calculating the matrix elements for
// Process: u u > e+ e- g u u WEIGHTED<=7 @4
// Process: u u > mu+ mu- g u u WEIGHTED<=7 @4
// Process: c c > e+ e- g c c WEIGHTED<=7 @4
// Process: c c > mu+ mu- g c c WEIGHTED<=7 @4
// Process: u u~ > e+ e- g u u~ WEIGHTED<=7 @4
// Process: u u~ > mu+ mu- g u u~ WEIGHTED<=7 @4
// Process: c c~ > e+ e- g c c~ WEIGHTED<=7 @4
// Process: c c~ > mu+ mu- g c c~ WEIGHTED<=7 @4
// Process: d d > e+ e- g d d WEIGHTED<=7 @4
// Process: d d > mu+ mu- g d d WEIGHTED<=7 @4
// Process: s s > e+ e- g s s WEIGHTED<=7 @4
// Process: s s > mu+ mu- g s s WEIGHTED<=7 @4
// Process: d d~ > e+ e- g d d~ WEIGHTED<=7 @4
// Process: d d~ > mu+ mu- g d d~ WEIGHTED<=7 @4
// Process: s s~ > e+ e- g s s~ WEIGHTED<=7 @4
// Process: s s~ > mu+ mu- g s s~ WEIGHTED<=7 @4
// Process: u~ u~ > e+ e- g u~ u~ WEIGHTED<=7 @4
// Process: u~ u~ > mu+ mu- g u~ u~ WEIGHTED<=7 @4
// Process: c~ c~ > e+ e- g c~ c~ WEIGHTED<=7 @4
// Process: c~ c~ > mu+ mu- g c~ c~ WEIGHTED<=7 @4
// Process: d~ d~ > e+ e- g d~ d~ WEIGHTED<=7 @4
// Process: d~ d~ > mu+ mu- g d~ d~ WEIGHTED<=7 @4
// Process: s~ s~ > e+ e- g s~ s~ WEIGHTED<=7 @4
// Process: s~ s~ > mu+ mu- g s~ s~ WEIGHTED<=7 @4
// Process: u u > ve~ ve g u u WEIGHTED<=7 @4
// Process: u u > vm~ vm g u u WEIGHTED<=7 @4
// Process: u u > vt~ vt g u u WEIGHTED<=7 @4
// Process: c c > ve~ ve g c c WEIGHTED<=7 @4
// Process: c c > vm~ vm g c c WEIGHTED<=7 @4
// Process: c c > vt~ vt g c c WEIGHTED<=7 @4
// Process: u c > e+ e- g u c WEIGHTED<=7 @4
// Process: u c > mu+ mu- g u c WEIGHTED<=7 @4
// Process: u d > e+ e- g u d WEIGHTED<=7 @4
// Process: u d > mu+ mu- g u d WEIGHTED<=7 @4
// Process: u s > e+ e- g u s WEIGHTED<=7 @4
// Process: u s > mu+ mu- g u s WEIGHTED<=7 @4
// Process: c d > e+ e- g c d WEIGHTED<=7 @4
// Process: c d > mu+ mu- g c d WEIGHTED<=7 @4
// Process: c s > e+ e- g c s WEIGHTED<=7 @4
// Process: c s > mu+ mu- g c s WEIGHTED<=7 @4
// Process: u u~ > e+ e- g c c~ WEIGHTED<=7 @4
// Process: u u~ > mu+ mu- g c c~ WEIGHTED<=7 @4
// Process: c c~ > e+ e- g u u~ WEIGHTED<=7 @4
// Process: c c~ > mu+ mu- g u u~ WEIGHTED<=7 @4
// Process: u u~ > e+ e- g d d~ WEIGHTED<=7 @4
// Process: u u~ > e+ e- g s s~ WEIGHTED<=7 @4
// Process: u u~ > mu+ mu- g d d~ WEIGHTED<=7 @4
// Process: u u~ > mu+ mu- g s s~ WEIGHTED<=7 @4
// Process: c c~ > e+ e- g d d~ WEIGHTED<=7 @4
// Process: c c~ > e+ e- g s s~ WEIGHTED<=7 @4
// Process: c c~ > mu+ mu- g d d~ WEIGHTED<=7 @4
// Process: c c~ > mu+ mu- g s s~ WEIGHTED<=7 @4
// Process: u u~ > ve~ ve g u u~ WEIGHTED<=7 @4
// Process: u u~ > vm~ vm g u u~ WEIGHTED<=7 @4
// Process: u u~ > vt~ vt g u u~ WEIGHTED<=7 @4
// Process: c c~ > ve~ ve g c c~ WEIGHTED<=7 @4
// Process: c c~ > vm~ vm g c c~ WEIGHTED<=7 @4
// Process: c c~ > vt~ vt g c c~ WEIGHTED<=7 @4
// Process: u c~ > e+ e- g u c~ WEIGHTED<=7 @4
// Process: u c~ > mu+ mu- g u c~ WEIGHTED<=7 @4
// Process: c u~ > e+ e- g c u~ WEIGHTED<=7 @4
// Process: c u~ > mu+ mu- g c u~ WEIGHTED<=7 @4
// Process: u d~ > e+ e- g u d~ WEIGHTED<=7 @4
// Process: u d~ > mu+ mu- g u d~ WEIGHTED<=7 @4
// Process: u s~ > e+ e- g u s~ WEIGHTED<=7 @4
// Process: u s~ > mu+ mu- g u s~ WEIGHTED<=7 @4
// Process: c d~ > e+ e- g c d~ WEIGHTED<=7 @4
// Process: c d~ > mu+ mu- g c d~ WEIGHTED<=7 @4
// Process: c s~ > e+ e- g c s~ WEIGHTED<=7 @4
// Process: c s~ > mu+ mu- g c s~ WEIGHTED<=7 @4
// Process: d d > ve~ ve g d d WEIGHTED<=7 @4
// Process: d d > vm~ vm g d d WEIGHTED<=7 @4
// Process: d d > vt~ vt g d d WEIGHTED<=7 @4
// Process: s s > ve~ ve g s s WEIGHTED<=7 @4
// Process: s s > vm~ vm g s s WEIGHTED<=7 @4
// Process: s s > vt~ vt g s s WEIGHTED<=7 @4
// Process: d s > e+ e- g d s WEIGHTED<=7 @4
// Process: d s > mu+ mu- g d s WEIGHTED<=7 @4
// Process: d u~ > e+ e- g d u~ WEIGHTED<=7 @4
// Process: d u~ > mu+ mu- g d u~ WEIGHTED<=7 @4
// Process: d c~ > e+ e- g d c~ WEIGHTED<=7 @4
// Process: d c~ > mu+ mu- g d c~ WEIGHTED<=7 @4
// Process: s u~ > e+ e- g s u~ WEIGHTED<=7 @4
// Process: s u~ > mu+ mu- g s u~ WEIGHTED<=7 @4
// Process: s c~ > e+ e- g s c~ WEIGHTED<=7 @4
// Process: s c~ > mu+ mu- g s c~ WEIGHTED<=7 @4
// Process: d d~ > e+ e- g u u~ WEIGHTED<=7 @4
// Process: d d~ > e+ e- g c c~ WEIGHTED<=7 @4
// Process: d d~ > mu+ mu- g u u~ WEIGHTED<=7 @4
// Process: d d~ > mu+ mu- g c c~ WEIGHTED<=7 @4
// Process: s s~ > e+ e- g u u~ WEIGHTED<=7 @4
// Process: s s~ > e+ e- g c c~ WEIGHTED<=7 @4
// Process: s s~ > mu+ mu- g u u~ WEIGHTED<=7 @4
// Process: s s~ > mu+ mu- g c c~ WEIGHTED<=7 @4
// Process: d d~ > e+ e- g s s~ WEIGHTED<=7 @4
// Process: d d~ > mu+ mu- g s s~ WEIGHTED<=7 @4
// Process: s s~ > e+ e- g d d~ WEIGHTED<=7 @4
// Process: s s~ > mu+ mu- g d d~ WEIGHTED<=7 @4
// Process: d d~ > ve~ ve g d d~ WEIGHTED<=7 @4
// Process: d d~ > vm~ vm g d d~ WEIGHTED<=7 @4
// Process: d d~ > vt~ vt g d d~ WEIGHTED<=7 @4
// Process: s s~ > ve~ ve g s s~ WEIGHTED<=7 @4
// Process: s s~ > vm~ vm g s s~ WEIGHTED<=7 @4
// Process: s s~ > vt~ vt g s s~ WEIGHTED<=7 @4
// Process: d s~ > e+ e- g d s~ WEIGHTED<=7 @4
// Process: d s~ > mu+ mu- g d s~ WEIGHTED<=7 @4
// Process: s d~ > e+ e- g s d~ WEIGHTED<=7 @4
// Process: s d~ > mu+ mu- g s d~ WEIGHTED<=7 @4
// Process: u~ u~ > ve~ ve g u~ u~ WEIGHTED<=7 @4
// Process: u~ u~ > vm~ vm g u~ u~ WEIGHTED<=7 @4
// Process: u~ u~ > vt~ vt g u~ u~ WEIGHTED<=7 @4
// Process: c~ c~ > ve~ ve g c~ c~ WEIGHTED<=7 @4
// Process: c~ c~ > vm~ vm g c~ c~ WEIGHTED<=7 @4
// Process: c~ c~ > vt~ vt g c~ c~ WEIGHTED<=7 @4
// Process: u~ c~ > e+ e- g u~ c~ WEIGHTED<=7 @4
// Process: u~ c~ > mu+ mu- g u~ c~ WEIGHTED<=7 @4
// Process: u~ d~ > e+ e- g u~ d~ WEIGHTED<=7 @4
// Process: u~ d~ > mu+ mu- g u~ d~ WEIGHTED<=7 @4
// Process: u~ s~ > e+ e- g u~ s~ WEIGHTED<=7 @4
// Process: u~ s~ > mu+ mu- g u~ s~ WEIGHTED<=7 @4
// Process: c~ d~ > e+ e- g c~ d~ WEIGHTED<=7 @4
// Process: c~ d~ > mu+ mu- g c~ d~ WEIGHTED<=7 @4
// Process: c~ s~ > e+ e- g c~ s~ WEIGHTED<=7 @4
// Process: c~ s~ > mu+ mu- g c~ s~ WEIGHTED<=7 @4
// Process: d~ d~ > ve~ ve g d~ d~ WEIGHTED<=7 @4
// Process: d~ d~ > vm~ vm g d~ d~ WEIGHTED<=7 @4
// Process: d~ d~ > vt~ vt g d~ d~ WEIGHTED<=7 @4
// Process: s~ s~ > ve~ ve g s~ s~ WEIGHTED<=7 @4
// Process: s~ s~ > vm~ vm g s~ s~ WEIGHTED<=7 @4
// Process: s~ s~ > vt~ vt g s~ s~ WEIGHTED<=7 @4
// Process: d~ s~ > e+ e- g d~ s~ WEIGHTED<=7 @4
// Process: d~ s~ > mu+ mu- g d~ s~ WEIGHTED<=7 @4
// Process: u u > e+ ve g u d WEIGHTED<=7 @4
// Process: u u > mu+ vm g u d WEIGHTED<=7 @4
// Process: c c > e+ ve g c s WEIGHTED<=7 @4
// Process: c c > mu+ vm g c s WEIGHTED<=7 @4
// Process: u c > ve~ ve g u c WEIGHTED<=7 @4
// Process: u c > vm~ vm g u c WEIGHTED<=7 @4
// Process: u c > vt~ vt g u c WEIGHTED<=7 @4
// Process: u d > e+ ve g d d WEIGHTED<=7 @4
// Process: u d > mu+ vm g d d WEIGHTED<=7 @4
// Process: c s > e+ ve g s s WEIGHTED<=7 @4
// Process: c s > mu+ vm g s s WEIGHTED<=7 @4
// Process: u d > ve~ e- g u u WEIGHTED<=7 @4
// Process: u d > vm~ mu- g u u WEIGHTED<=7 @4
// Process: c s > ve~ e- g c c WEIGHTED<=7 @4
// Process: c s > vm~ mu- g c c WEIGHTED<=7 @4
// Process: u d > ve~ ve g u d WEIGHTED<=7 @4
// Process: u d > vm~ vm g u d WEIGHTED<=7 @4
// Process: u d > vt~ vt g u d WEIGHTED<=7 @4
// Process: u s > ve~ ve g u s WEIGHTED<=7 @4
// Process: u s > vm~ vm g u s WEIGHTED<=7 @4
// Process: u s > vt~ vt g u s WEIGHTED<=7 @4
// Process: c d > ve~ ve g c d WEIGHTED<=7 @4
// Process: c d > vm~ vm g c d WEIGHTED<=7 @4
// Process: c d > vt~ vt g c d WEIGHTED<=7 @4
// Process: c s > ve~ ve g c s WEIGHTED<=7 @4
// Process: c s > vm~ vm g c s WEIGHTED<=7 @4
// Process: c s > vt~ vt g c s WEIGHTED<=7 @4
// Process: u u~ > e+ ve g d u~ WEIGHTED<=7 @4
// Process: u u~ > mu+ vm g d u~ WEIGHTED<=7 @4
// Process: c c~ > e+ ve g s c~ WEIGHTED<=7 @4
// Process: c c~ > mu+ vm g s c~ WEIGHTED<=7 @4
// Process: u u~ > ve~ e- g u d~ WEIGHTED<=7 @4
// Process: u u~ > vm~ mu- g u d~ WEIGHTED<=7 @4
// Process: c c~ > ve~ e- g c s~ WEIGHTED<=7 @4
// Process: c c~ > vm~ mu- g c s~ WEIGHTED<=7 @4
// Process: u u~ > ve~ ve g c c~ WEIGHTED<=7 @4
// Process: u u~ > vm~ vm g c c~ WEIGHTED<=7 @4
// Process: u u~ > vt~ vt g c c~ WEIGHTED<=7 @4
// Process: c c~ > ve~ ve g u u~ WEIGHTED<=7 @4
// Process: c c~ > vm~ vm g u u~ WEIGHTED<=7 @4
// Process: c c~ > vt~ vt g u u~ WEIGHTED<=7 @4
// Process: u u~ > ve~ ve g d d~ WEIGHTED<=7 @4
// Process: u u~ > ve~ ve g s s~ WEIGHTED<=7 @4
// Process: u u~ > vm~ vm g d d~ WEIGHTED<=7 @4
// Process: u u~ > vm~ vm g s s~ WEIGHTED<=7 @4
// Process: u u~ > vt~ vt g d d~ WEIGHTED<=7 @4
// Process: u u~ > vt~ vt g s s~ WEIGHTED<=7 @4
// Process: c c~ > ve~ ve g d d~ WEIGHTED<=7 @4
// Process: c c~ > ve~ ve g s s~ WEIGHTED<=7 @4
// Process: c c~ > vm~ vm g d d~ WEIGHTED<=7 @4
// Process: c c~ > vm~ vm g s s~ WEIGHTED<=7 @4
// Process: c c~ > vt~ vt g d d~ WEIGHTED<=7 @4
// Process: c c~ > vt~ vt g s s~ WEIGHTED<=7 @4
// Process: u c~ > ve~ ve g u c~ WEIGHTED<=7 @4
// Process: u c~ > vm~ vm g u c~ WEIGHTED<=7 @4
// Process: u c~ > vt~ vt g u c~ WEIGHTED<=7 @4
// Process: c u~ > ve~ ve g c u~ WEIGHTED<=7 @4
// Process: c u~ > vm~ vm g c u~ WEIGHTED<=7 @4
// Process: c u~ > vt~ vt g c u~ WEIGHTED<=7 @4
// Process: u d~ > e+ ve g u u~ WEIGHTED<=7 @4
// Process: u d~ > mu+ vm g u u~ WEIGHTED<=7 @4
// Process: c s~ > e+ ve g c c~ WEIGHTED<=7 @4
// Process: c s~ > mu+ vm g c c~ WEIGHTED<=7 @4
// Process: u d~ > e+ ve g d d~ WEIGHTED<=7 @4
// Process: u d~ > mu+ vm g d d~ WEIGHTED<=7 @4
// Process: c s~ > e+ ve g s s~ WEIGHTED<=7 @4
// Process: c s~ > mu+ vm g s s~ WEIGHTED<=7 @4
// Process: u d~ > ve~ ve g u d~ WEIGHTED<=7 @4
// Process: u d~ > vm~ vm g u d~ WEIGHTED<=7 @4
// Process: u d~ > vt~ vt g u d~ WEIGHTED<=7 @4
// Process: u s~ > ve~ ve g u s~ WEIGHTED<=7 @4
// Process: u s~ > vm~ vm g u s~ WEIGHTED<=7 @4
// Process: u s~ > vt~ vt g u s~ WEIGHTED<=7 @4
// Process: c d~ > ve~ ve g c d~ WEIGHTED<=7 @4
// Process: c d~ > vm~ vm g c d~ WEIGHTED<=7 @4
// Process: c d~ > vt~ vt g c d~ WEIGHTED<=7 @4
// Process: c s~ > ve~ ve g c s~ WEIGHTED<=7 @4
// Process: c s~ > vm~ vm g c s~ WEIGHTED<=7 @4
// Process: c s~ > vt~ vt g c s~ WEIGHTED<=7 @4
// Process: d d > ve~ e- g u d WEIGHTED<=7 @4
// Process: d d > vm~ mu- g u d WEIGHTED<=7 @4
// Process: s s > ve~ e- g c s WEIGHTED<=7 @4
// Process: s s > vm~ mu- g c s WEIGHTED<=7 @4
// Process: d s > ve~ ve g d s WEIGHTED<=7 @4
// Process: d s > vm~ vm g d s WEIGHTED<=7 @4
// Process: d s > vt~ vt g d s WEIGHTED<=7 @4
// Process: d u~ > ve~ e- g u u~ WEIGHTED<=7 @4
// Process: d u~ > vm~ mu- g u u~ WEIGHTED<=7 @4
// Process: s c~ > ve~ e- g c c~ WEIGHTED<=7 @4
// Process: s c~ > vm~ mu- g c c~ WEIGHTED<=7 @4
// Process: d u~ > ve~ e- g d d~ WEIGHTED<=7 @4
// Process: d u~ > vm~ mu- g d d~ WEIGHTED<=7 @4
// Process: s c~ > ve~ e- g s s~ WEIGHTED<=7 @4
// Process: s c~ > vm~ mu- g s s~ WEIGHTED<=7 @4
// Process: d u~ > ve~ ve g d u~ WEIGHTED<=7 @4
// Process: d u~ > vm~ vm g d u~ WEIGHTED<=7 @4
// Process: d u~ > vt~ vt g d u~ WEIGHTED<=7 @4
// Process: d c~ > ve~ ve g d c~ WEIGHTED<=7 @4
// Process: d c~ > vm~ vm g d c~ WEIGHTED<=7 @4
// Process: d c~ > vt~ vt g d c~ WEIGHTED<=7 @4
// Process: s u~ > ve~ ve g s u~ WEIGHTED<=7 @4
// Process: s u~ > vm~ vm g s u~ WEIGHTED<=7 @4
// Process: s u~ > vt~ vt g s u~ WEIGHTED<=7 @4
// Process: s c~ > ve~ ve g s c~ WEIGHTED<=7 @4
// Process: s c~ > vm~ vm g s c~ WEIGHTED<=7 @4
// Process: s c~ > vt~ vt g s c~ WEIGHTED<=7 @4
// Process: d d~ > e+ ve g d u~ WEIGHTED<=7 @4
// Process: d d~ > mu+ vm g d u~ WEIGHTED<=7 @4
// Process: s s~ > e+ ve g s c~ WEIGHTED<=7 @4
// Process: s s~ > mu+ vm g s c~ WEIGHTED<=7 @4
// Process: d d~ > ve~ e- g u d~ WEIGHTED<=7 @4
// Process: d d~ > vm~ mu- g u d~ WEIGHTED<=7 @4
// Process: s s~ > ve~ e- g c s~ WEIGHTED<=7 @4
// Process: s s~ > vm~ mu- g c s~ WEIGHTED<=7 @4
// Process: d d~ > ve~ ve g u u~ WEIGHTED<=7 @4
// Process: d d~ > ve~ ve g c c~ WEIGHTED<=7 @4
// Process: d d~ > vm~ vm g u u~ WEIGHTED<=7 @4
// Process: d d~ > vm~ vm g c c~ WEIGHTED<=7 @4
// Process: d d~ > vt~ vt g u u~ WEIGHTED<=7 @4
// Process: d d~ > vt~ vt g c c~ WEIGHTED<=7 @4
// Process: s s~ > ve~ ve g u u~ WEIGHTED<=7 @4
// Process: s s~ > ve~ ve g c c~ WEIGHTED<=7 @4
// Process: s s~ > vm~ vm g u u~ WEIGHTED<=7 @4
// Process: s s~ > vm~ vm g c c~ WEIGHTED<=7 @4
// Process: s s~ > vt~ vt g u u~ WEIGHTED<=7 @4
// Process: s s~ > vt~ vt g c c~ WEIGHTED<=7 @4
// Process: d d~ > ve~ ve g s s~ WEIGHTED<=7 @4
// Process: d d~ > vm~ vm g s s~ WEIGHTED<=7 @4
// Process: d d~ > vt~ vt g s s~ WEIGHTED<=7 @4
// Process: s s~ > ve~ ve g d d~ WEIGHTED<=7 @4
// Process: s s~ > vm~ vm g d d~ WEIGHTED<=7 @4
// Process: s s~ > vt~ vt g d d~ WEIGHTED<=7 @4
// Process: d s~ > ve~ ve g d s~ WEIGHTED<=7 @4
// Process: d s~ > vm~ vm g d s~ WEIGHTED<=7 @4
// Process: d s~ > vt~ vt g d s~ WEIGHTED<=7 @4
// Process: s d~ > ve~ ve g s d~ WEIGHTED<=7 @4
// Process: s d~ > vm~ vm g s d~ WEIGHTED<=7 @4
// Process: s d~ > vt~ vt g s d~ WEIGHTED<=7 @4
// Process: u~ u~ > ve~ e- g u~ d~ WEIGHTED<=7 @4
// Process: u~ u~ > vm~ mu- g u~ d~ WEIGHTED<=7 @4
// Process: c~ c~ > ve~ e- g c~ s~ WEIGHTED<=7 @4
// Process: c~ c~ > vm~ mu- g c~ s~ WEIGHTED<=7 @4
// Process: u~ c~ > ve~ ve g u~ c~ WEIGHTED<=7 @4
// Process: u~ c~ > vm~ vm g u~ c~ WEIGHTED<=7 @4
// Process: u~ c~ > vt~ vt g u~ c~ WEIGHTED<=7 @4
// Process: u~ d~ > e+ ve g u~ u~ WEIGHTED<=7 @4
// Process: u~ d~ > mu+ vm g u~ u~ WEIGHTED<=7 @4
// Process: c~ s~ > e+ ve g c~ c~ WEIGHTED<=7 @4
// Process: c~ s~ > mu+ vm g c~ c~ WEIGHTED<=7 @4
// Process: u~ d~ > ve~ e- g d~ d~ WEIGHTED<=7 @4
// Process: u~ d~ > vm~ mu- g d~ d~ WEIGHTED<=7 @4
// Process: c~ s~ > ve~ e- g s~ s~ WEIGHTED<=7 @4
// Process: c~ s~ > vm~ mu- g s~ s~ WEIGHTED<=7 @4
// Process: u~ d~ > ve~ ve g u~ d~ WEIGHTED<=7 @4
// Process: u~ d~ > vm~ vm g u~ d~ WEIGHTED<=7 @4
// Process: u~ d~ > vt~ vt g u~ d~ WEIGHTED<=7 @4
// Process: u~ s~ > ve~ ve g u~ s~ WEIGHTED<=7 @4
// Process: u~ s~ > vm~ vm g u~ s~ WEIGHTED<=7 @4
// Process: u~ s~ > vt~ vt g u~ s~ WEIGHTED<=7 @4
// Process: c~ d~ > ve~ ve g c~ d~ WEIGHTED<=7 @4
// Process: c~ d~ > vm~ vm g c~ d~ WEIGHTED<=7 @4
// Process: c~ d~ > vt~ vt g c~ d~ WEIGHTED<=7 @4
// Process: c~ s~ > ve~ ve g c~ s~ WEIGHTED<=7 @4
// Process: c~ s~ > vm~ vm g c~ s~ WEIGHTED<=7 @4
// Process: c~ s~ > vt~ vt g c~ s~ WEIGHTED<=7 @4
// Process: d~ d~ > e+ ve g u~ d~ WEIGHTED<=7 @4
// Process: d~ d~ > mu+ vm g u~ d~ WEIGHTED<=7 @4
// Process: s~ s~ > e+ ve g c~ s~ WEIGHTED<=7 @4
// Process: s~ s~ > mu+ vm g c~ s~ WEIGHTED<=7 @4
// Process: d~ s~ > ve~ ve g d~ s~ WEIGHTED<=7 @4
// Process: d~ s~ > vm~ vm g d~ s~ WEIGHTED<=7 @4
// Process: d~ s~ > vt~ vt g d~ s~ WEIGHTED<=7 @4
// Process: u c > e+ ve g u s WEIGHTED<=7 @4
// Process: u c > mu+ vm g u s WEIGHTED<=7 @4
// Process: u c > e+ ve g c d WEIGHTED<=7 @4
// Process: u c > mu+ vm g c d WEIGHTED<=7 @4
// Process: u s > e+ ve g s d WEIGHTED<=7 @4
// Process: u s > mu+ vm g s d WEIGHTED<=7 @4
// Process: c d > e+ ve g d s WEIGHTED<=7 @4
// Process: c d > mu+ vm g d s WEIGHTED<=7 @4
// Process: u s > ve~ e- g u c WEIGHTED<=7 @4
// Process: u s > vm~ mu- g u c WEIGHTED<=7 @4
// Process: c d > ve~ e- g c u WEIGHTED<=7 @4
// Process: c d > vm~ mu- g c u WEIGHTED<=7 @4
// Process: d s > ve~ e- g d c WEIGHTED<=7 @4
// Process: d s > vm~ mu- g d c WEIGHTED<=7 @4
// Process: u u~ > e+ ve g s c~ WEIGHTED<=7 @4
// Process: u u~ > mu+ vm g s c~ WEIGHTED<=7 @4
// Process: c c~ > e+ ve g d u~ WEIGHTED<=7 @4
// Process: c c~ > mu+ vm g d u~ WEIGHTED<=7 @4
// Process: d d~ > e+ ve g s c~ WEIGHTED<=7 @4
// Process: d d~ > mu+ vm g s c~ WEIGHTED<=7 @4
// Process: s s~ > e+ ve g d u~ WEIGHTED<=7 @4
// Process: s s~ > mu+ vm g d u~ WEIGHTED<=7 @4
// Process: u u~ > ve~ e- g c s~ WEIGHTED<=7 @4
// Process: u u~ > vm~ mu- g c s~ WEIGHTED<=7 @4
// Process: c c~ > ve~ e- g u d~ WEIGHTED<=7 @4
// Process: c c~ > vm~ mu- g u d~ WEIGHTED<=7 @4
// Process: d d~ > ve~ e- g c s~ WEIGHTED<=7 @4
// Process: d d~ > vm~ mu- g c s~ WEIGHTED<=7 @4
// Process: s s~ > ve~ e- g u d~ WEIGHTED<=7 @4
// Process: s s~ > vm~ mu- g u d~ WEIGHTED<=7 @4
// Process: u c~ > e+ ve g d c~ WEIGHTED<=7 @4
// Process: u c~ > mu+ vm g d c~ WEIGHTED<=7 @4
// Process: u s~ > e+ ve g d s~ WEIGHTED<=7 @4
// Process: u s~ > mu+ vm g d s~ WEIGHTED<=7 @4
// Process: c u~ > e+ ve g s u~ WEIGHTED<=7 @4
// Process: c u~ > mu+ vm g s u~ WEIGHTED<=7 @4
// Process: c d~ > e+ ve g s d~ WEIGHTED<=7 @4
// Process: c d~ > mu+ vm g s d~ WEIGHTED<=7 @4
// Process: u c~ > ve~ e- g u s~ WEIGHTED<=7 @4
// Process: u c~ > vm~ mu- g u s~ WEIGHTED<=7 @4
// Process: c u~ > ve~ e- g c d~ WEIGHTED<=7 @4
// Process: c u~ > vm~ mu- g c d~ WEIGHTED<=7 @4
// Process: d c~ > ve~ e- g d s~ WEIGHTED<=7 @4
// Process: d c~ > vm~ mu- g d s~ WEIGHTED<=7 @4
// Process: s u~ > ve~ e- g s d~ WEIGHTED<=7 @4
// Process: s u~ > vm~ mu- g s d~ WEIGHTED<=7 @4
// Process: u d~ > e+ ve g c c~ WEIGHTED<=7 @4
// Process: u d~ > e+ ve g s s~ WEIGHTED<=7 @4
// Process: u d~ > mu+ vm g c c~ WEIGHTED<=7 @4
// Process: u d~ > mu+ vm g s s~ WEIGHTED<=7 @4
// Process: c s~ > e+ ve g u u~ WEIGHTED<=7 @4
// Process: c s~ > e+ ve g d d~ WEIGHTED<=7 @4
// Process: c s~ > mu+ vm g u u~ WEIGHTED<=7 @4
// Process: c s~ > mu+ vm g d d~ WEIGHTED<=7 @4
// Process: u s~ > e+ ve g u c~ WEIGHTED<=7 @4
// Process: u s~ > mu+ vm g u c~ WEIGHTED<=7 @4
// Process: c d~ > e+ ve g c u~ WEIGHTED<=7 @4
// Process: c d~ > mu+ vm g c u~ WEIGHTED<=7 @4
// Process: d s~ > e+ ve g d c~ WEIGHTED<=7 @4
// Process: d s~ > mu+ vm g d c~ WEIGHTED<=7 @4
// Process: s d~ > e+ ve g s u~ WEIGHTED<=7 @4
// Process: s d~ > mu+ vm g s u~ WEIGHTED<=7 @4
// Process: d s > ve~ e- g u s WEIGHTED<=7 @4
// Process: d s > vm~ mu- g u s WEIGHTED<=7 @4
// Process: d u~ > ve~ e- g c c~ WEIGHTED<=7 @4
// Process: d u~ > ve~ e- g s s~ WEIGHTED<=7 @4
// Process: d u~ > vm~ mu- g c c~ WEIGHTED<=7 @4
// Process: d u~ > vm~ mu- g s s~ WEIGHTED<=7 @4
// Process: s c~ > ve~ e- g u u~ WEIGHTED<=7 @4
// Process: s c~ > ve~ e- g d d~ WEIGHTED<=7 @4
// Process: s c~ > vm~ mu- g u u~ WEIGHTED<=7 @4
// Process: s c~ > vm~ mu- g d d~ WEIGHTED<=7 @4
// Process: d c~ > ve~ e- g u c~ WEIGHTED<=7 @4
// Process: d c~ > vm~ mu- g u c~ WEIGHTED<=7 @4
// Process: d s~ > ve~ e- g u s~ WEIGHTED<=7 @4
// Process: d s~ > vm~ mu- g u s~ WEIGHTED<=7 @4
// Process: s u~ > ve~ e- g c u~ WEIGHTED<=7 @4
// Process: s u~ > vm~ mu- g c u~ WEIGHTED<=7 @4
// Process: s d~ > ve~ e- g c d~ WEIGHTED<=7 @4
// Process: s d~ > vm~ mu- g c d~ WEIGHTED<=7 @4
// Process: u~ c~ > ve~ e- g u~ s~ WEIGHTED<=7 @4
// Process: u~ c~ > vm~ mu- g u~ s~ WEIGHTED<=7 @4
// Process: u~ c~ > ve~ e- g c~ d~ WEIGHTED<=7 @4
// Process: u~ c~ > vm~ mu- g c~ d~ WEIGHTED<=7 @4
// Process: u~ s~ > ve~ e- g s~ d~ WEIGHTED<=7 @4
// Process: u~ s~ > vm~ mu- g s~ d~ WEIGHTED<=7 @4
// Process: c~ d~ > ve~ e- g d~ s~ WEIGHTED<=7 @4
// Process: c~ d~ > vm~ mu- g d~ s~ WEIGHTED<=7 @4
// Process: u~ s~ > e+ ve g u~ c~ WEIGHTED<=7 @4
// Process: u~ s~ > mu+ vm g u~ c~ WEIGHTED<=7 @4
// Process: c~ d~ > e+ ve g c~ u~ WEIGHTED<=7 @4
// Process: c~ d~ > mu+ vm g c~ u~ WEIGHTED<=7 @4
// Process: d~ s~ > e+ ve g d~ c~ WEIGHTED<=7 @4
// Process: d~ s~ > mu+ vm g d~ c~ WEIGHTED<=7 @4
// Process: d~ s~ > e+ ve g u~ s~ WEIGHTED<=7 @4
// Process: d~ s~ > mu+ vm g u~ s~ WEIGHTED<=7 @4
//--------------------------------------------------------------------------

typedef vector<double> vec_double; 
typedef vector < vec_double > vec_vec_double; 
typedef vector<int> vec_int; 
typedef vector<bool> vec_bool; 
typedef vector < vec_int > vec_vec_int; 

class PY8MEs_R4_P76_sm_qq_llgqq : public PY8ME
{
  public:

    // Check for the availability of the requested proces.
    // If available, this returns the corresponding permutation and Proc_ID  to
    // use.
    // If not available, this returns a negative Proc_ID.
    static pair < vector<int> , int > static_getPY8ME(vector<int> initial_pdgs,
        vector<int> final_pdgs, set<int> schannels = set<int> ());

    // Constructor.
    PY8MEs_R4_P76_sm_qq_llgqq(Parameters_sm * model) : pars(model) {initProc();}

    // Destructor.
    ~PY8MEs_R4_P76_sm_qq_llgqq(); 

    // Initialize process.
    virtual void initProc(); 

    // Calculate squared ME.
    virtual double sigmaKin(); 

    // Info on the subprocess.
    virtual string name() const {return "qq_llgqq (sm)";}

    virtual int code() const {return 10476;}

    virtual string inFlux() const {return "qq";}

    virtual vector<double> getMasses(); 

    virtual void setMasses(vec_double external_masses); 
    // Set all values of the external masses to an integer mode:
    // 0 : Mass taken from the model
    // 1 : Mass taken from p_i^2 if not massless to begin with
    // 2 : Mass always taken from p_i^2.
    virtual void setExternalMassesMode(int mode); 

    // Synchronize local variables of the process that depend on the model
    // parameters
    virtual void syncProcModelParams(); 

    // Tell Pythia that sigmaHat returns the ME^2
    virtual bool convertM2() const {return true;}

    // Access to getPY8ME with polymorphism from a non-static context
    virtual pair < vector<int> , int > getPY8ME(vector<int> initial_pdgs,
        vector<int> final_pdgs, set<int> schannels = set<int> ())
    {
      return static_getPY8ME(initial_pdgs, final_pdgs, schannels); 
    }

    // Set momenta
    virtual void setMomenta(vector < vec_double > momenta_picked); 

    // Set color configuration to use. An empty vector means sum over all.
    virtual void setColors(vector<int> colors_picked); 

    // Set the helicity configuration to use. Am empty vector means sum over
    // all.
    virtual void setHelicities(vector<int> helicities_picked); 

    // Set the permutation to use (will apply to momenta, colors and helicities)
    virtual void setPermutation(vector<int> perm_picked); 

    // Set the proc_ID to use
    virtual void setProcID(int procID_picked); 

    // Access to all the helicity and color configurations for a given process
    virtual vector < vec_int > getColorConfigs(int specify_proc_ID = -1,
        vector<int> permutation = vector<int> ());
    virtual vector < vec_int > getHelicityConfigs(vector<int> permutation =
        vector<int> ());

    // Maps of Helicity <-> hel_ID and ColorConfig <-> colorConfig_ID.
    virtual vector<int> getHelicityConfigForID(int hel_ID, vector<int>
        permutation = vector<int> ());
    virtual int getHelicityIDForConfig(vector<int> hel_config, vector<int>
        permutation = vector<int> ());
    virtual vector<int> getColorConfigForID(int color_ID, int specify_proc_ID =
        -1, vector<int> permutation = vector<int> ());
    virtual int getColorIDForConfig(vector<int> color_config, int
        specify_proc_ID = -1, vector<int> permutation = vector<int> ());
    virtual int getColorFlowRelativeNCPower(int color_flow_ID, int
        specify_proc_ID = -1);

    // Access previously computed results
    virtual vector < vec_double > getAllResults(int specify_proc_ID = -1); 
    virtual double getResult(int helicity_ID, int color_ID, int specify_proc_ID
        = -1);

    // Accessors
    Parameters_sm * getModel() {return pars;}
    void setModel(Parameters_sm * model) {pars = model;}

    // Invert the permutation mapping
    vector<int> invert_mapping(vector<int> mapping); 

    // Control whether to include the symmetry factors or not
    virtual void setIncludeSymmetryFactors(bool OnOff) 
    {
      include_symmetry_factors = OnOff; 
    }
    virtual bool getIncludeSymmetryFactors() {return include_symmetry_factors;}
    virtual int getSymmetryFactor() {return denom_iden[proc_ID];}

    // Control whether to include helicity averaging factors or not
    virtual void setIncludeHelicityAveragingFactors(bool OnOff) 
    {
      include_helicity_averaging_factors = OnOff; 
    }
    virtual bool getIncludeHelicityAveragingFactors() 
    {
      return include_helicity_averaging_factors; 
    }
    virtual int getHelicityAveragingFactor() {return denom_hels[proc_ID];}

    // Control whether to include color averaging factors or not
    virtual void setIncludeColorAveragingFactors(bool OnOff) 
    {
      include_color_averaging_factors = OnOff; 
    }
    virtual bool getIncludeColorAveragingFactors() 
    {
      return include_color_averaging_factors; 
    }
    virtual int getColorAveragingFactor() {return denom_colors[proc_ID];}

  private:

    // Private functions to calculate the matrix element for all subprocesses
    // Calculate wavefunctions
    void calculate_wavefunctions(const int hel[]); 
    static const int nwavefuncs = 223; 
    Complex<double> w[nwavefuncs][18]; 
    static const int namplitudes = 2446; 
    Complex<double> amp[namplitudes]; 
    double matrix_4_uu_epemguu(); 
    double matrix_4_uux_epemguux(); 
    double matrix_4_dd_epemgdd(); 
    double matrix_4_ddx_epemgddx(); 
    double matrix_4_uxux_epemguxux(); 
    double matrix_4_dxdx_epemgdxdx(); 
    double matrix_4_uu_vexveguu(); 
    double matrix_4_uc_epemguc(); 
    double matrix_4_ud_epemgud(); 
    double matrix_4_uux_epemgccx(); 
    double matrix_4_uux_epemgddx(); 
    double matrix_4_uux_vexveguux(); 
    double matrix_4_ucx_epemgucx(); 
    double matrix_4_udx_epemgudx(); 
    double matrix_4_dd_vexvegdd(); 
    double matrix_4_ds_epemgds(); 
    double matrix_4_dux_epemgdux(); 
    double matrix_4_ddx_epemguux(); 
    double matrix_4_ddx_epemgssx(); 
    double matrix_4_ddx_vexvegddx(); 
    double matrix_4_dsx_epemgdsx(); 
    double matrix_4_uxux_vexveguxux(); 
    double matrix_4_uxcx_epemguxcx(); 
    double matrix_4_uxdx_epemguxdx(); 
    double matrix_4_dxdx_vexvegdxdx(); 
    double matrix_4_dxsx_epemgdxsx(); 
    double matrix_4_uu_epvegud(); 
    double matrix_4_uc_vexveguc(); 
    double matrix_4_ud_epvegdd(); 
    double matrix_4_ud_vexemguu(); 
    double matrix_4_ud_vexvegud(); 
    double matrix_4_uux_epvegdux(); 
    double matrix_4_uux_vexemgudx(); 
    double matrix_4_uux_vexvegccx(); 
    double matrix_4_uux_vexvegddx(); 
    double matrix_4_ucx_vexvegucx(); 
    double matrix_4_udx_epveguux(); 
    double matrix_4_udx_epvegddx(); 
    double matrix_4_udx_vexvegudx(); 
    double matrix_4_dd_vexemgud(); 
    double matrix_4_ds_vexvegds(); 
    double matrix_4_dux_vexemguux(); 
    double matrix_4_dux_vexemgddx(); 
    double matrix_4_dux_vexvegdux(); 
    double matrix_4_ddx_epvegdux(); 
    double matrix_4_ddx_vexemgudx(); 
    double matrix_4_ddx_vexveguux(); 
    double matrix_4_ddx_vexvegssx(); 
    double matrix_4_dsx_vexvegdsx(); 
    double matrix_4_uxux_vexemguxdx(); 
    double matrix_4_uxcx_vexveguxcx(); 
    double matrix_4_uxdx_epveguxux(); 
    double matrix_4_uxdx_vexemgdxdx(); 
    double matrix_4_uxdx_vexveguxdx(); 
    double matrix_4_dxdx_epveguxdx(); 
    double matrix_4_dxsx_vexvegdxsx(); 
    double matrix_4_uc_epvegus(); 
    double matrix_4_uc_epvegcd(); 
    double matrix_4_us_vexemguc(); 
    double matrix_4_uux_epvegscx(); 
    double matrix_4_uux_vexemgcsx(); 
    double matrix_4_ucx_epvegdcx(); 
    double matrix_4_ucx_vexemgusx(); 
    double matrix_4_udx_epvegccx(); 
    double matrix_4_usx_epvegucx(); 
    double matrix_4_ds_vexemgus(); 
    double matrix_4_dux_vexemgccx(); 
    double matrix_4_dcx_vexemgucx(); 
    double matrix_4_uxcx_vexemguxsx(); 
    double matrix_4_uxcx_vexemgcxdx(); 
    double matrix_4_uxsx_epveguxcx(); 
    double matrix_4_dxsx_epveguxsx(); 

    // Constants for array limits
    static const int nexternal = 7; 
    static const int ninitial = 2; 
    static const int nprocesses = 72; 
    static const int nreq_s_channels = 0; 
    static const int ncomb = 128; 

    // Helicities for the process
    static int helicities[ncomb][nexternal]; 

    // Normalization factors the various processes
    static int denom_colors[nprocesses]; 
    static int denom_hels[nprocesses]; 
    static int denom_iden[nprocesses]; 

    // Control whether to include symmetry factors or not
    bool include_symmetry_factors; 
    // Control whether to include helicity averaging factors or not
    bool include_helicity_averaging_factors; 
    // Control whether to include color averaging factors or not
    bool include_color_averaging_factors; 

    // Color flows, used when selecting color
    vector < vec_double > jamp2; 

    // Store individual results (for each color flow, helicity configurations
    // and proc_ID)
    // computed in the last call to sigmaKin().
    vector < vec_vec_double > all_results; 

    // required s-channels specified
    static std::set<int> s_channel_proc; 

    // vector with external particle masses
    vector<double> mME; 

    // vector with momenta (to be changed for each event)
    vector < double * > p; 

    // external particles permutation (to be changed for each event)
    vector<int> perm; 

    // vector with colors (to be changed for each event)
    vector<int> user_colors; 

    // vector with helicities (to be changed for each event)
    vector<int> user_helicities; 

    // Process ID (to be changed for each event)
    int proc_ID; 

    // All color configurations
    void initColorConfigs(); 
    vector < vec_vec_int > color_configs; 

    // Color flows relative N_c power (conventions are such that all elements
    // on the color matrix diagonal are identical).
    vector < vec_int > jamp_nc_relative_power; 

    // Model pointer to be used by this matrix element
    Parameters_sm * pars; 

}; 

}  // end namespace PY8MEs_namespace

#endif  // PY8MEs_R4_P76_sm_qq_llgqq_H

