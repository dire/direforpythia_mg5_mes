//==========================================================================
// This file has been automatically generated for Pythia 8 by
// MadGraph5_aMC@NLO v. 2.6.0, 2017-08-16
// By the MadGraph5_aMC@NLO Development Team
// Visit launchpad.net/madgraph5 and amcatnlo.web.cern.ch
//==========================================================================

#include "PY8MEs_R12_P62_sm_qb_wpwmgqb.h"
#include "HelAmps_sm.h"

using namespace Pythia8_sm; 

namespace PY8MEs_namespace 
{
//==========================================================================
// Class member functions for calculating the matrix elements for
// Process: u b > w+ w- g u b WEIGHTED<=7 @12
// Process: c b > w+ w- g c b WEIGHTED<=7 @12
// Process: u b~ > w+ w- g u b~ WEIGHTED<=7 @12
// Process: c b~ > w+ w- g c b~ WEIGHTED<=7 @12
// Process: d b > w+ w- g d b WEIGHTED<=7 @12
// Process: s b > w+ w- g s b WEIGHTED<=7 @12
// Process: d b~ > w+ w- g d b~ WEIGHTED<=7 @12
// Process: s b~ > w+ w- g s b~ WEIGHTED<=7 @12
// Process: u~ b > w+ w- g u~ b WEIGHTED<=7 @12
// Process: c~ b > w+ w- g c~ b WEIGHTED<=7 @12
// Process: u~ b~ > w+ w- g u~ b~ WEIGHTED<=7 @12
// Process: c~ b~ > w+ w- g c~ b~ WEIGHTED<=7 @12
// Process: d~ b > w+ w- g d~ b WEIGHTED<=7 @12
// Process: s~ b > w+ w- g s~ b WEIGHTED<=7 @12
// Process: d~ b~ > w+ w- g d~ b~ WEIGHTED<=7 @12
// Process: s~ b~ > w+ w- g s~ b~ WEIGHTED<=7 @12

// Exception class
class PY8MEs_R12_P62_sm_qb_wpwmgqbException : public exception
{
  virtual const char * what() const throw()
  {
    return "Exception in class 'PY8MEs_R12_P62_sm_qb_wpwmgqb'."; 
  }
}
PY8MEs_R12_P62_sm_qb_wpwmgqb_exception; 

std::set<int> PY8MEs_R12_P62_sm_qb_wpwmgqb::s_channel_proc = std::set<int>
    (createset<int> ());

int PY8MEs_R12_P62_sm_qb_wpwmgqb::helicities[ncomb][nexternal] = {{-1, -1, -1,
    -1, -1, -1, -1}, {-1, -1, -1, -1, -1, -1, 1}, {-1, -1, -1, -1, -1, 1, -1},
    {-1, -1, -1, -1, -1, 1, 1}, {-1, -1, -1, -1, 1, -1, -1}, {-1, -1, -1, -1,
    1, -1, 1}, {-1, -1, -1, -1, 1, 1, -1}, {-1, -1, -1, -1, 1, 1, 1}, {-1, -1,
    -1, 0, -1, -1, -1}, {-1, -1, -1, 0, -1, -1, 1}, {-1, -1, -1, 0, -1, 1, -1},
    {-1, -1, -1, 0, -1, 1, 1}, {-1, -1, -1, 0, 1, -1, -1}, {-1, -1, -1, 0, 1,
    -1, 1}, {-1, -1, -1, 0, 1, 1, -1}, {-1, -1, -1, 0, 1, 1, 1}, {-1, -1, -1,
    1, -1, -1, -1}, {-1, -1, -1, 1, -1, -1, 1}, {-1, -1, -1, 1, -1, 1, -1},
    {-1, -1, -1, 1, -1, 1, 1}, {-1, -1, -1, 1, 1, -1, -1}, {-1, -1, -1, 1, 1,
    -1, 1}, {-1, -1, -1, 1, 1, 1, -1}, {-1, -1, -1, 1, 1, 1, 1}, {-1, -1, 0,
    -1, -1, -1, -1}, {-1, -1, 0, -1, -1, -1, 1}, {-1, -1, 0, -1, -1, 1, -1},
    {-1, -1, 0, -1, -1, 1, 1}, {-1, -1, 0, -1, 1, -1, -1}, {-1, -1, 0, -1, 1,
    -1, 1}, {-1, -1, 0, -1, 1, 1, -1}, {-1, -1, 0, -1, 1, 1, 1}, {-1, -1, 0, 0,
    -1, -1, -1}, {-1, -1, 0, 0, -1, -1, 1}, {-1, -1, 0, 0, -1, 1, -1}, {-1, -1,
    0, 0, -1, 1, 1}, {-1, -1, 0, 0, 1, -1, -1}, {-1, -1, 0, 0, 1, -1, 1}, {-1,
    -1, 0, 0, 1, 1, -1}, {-1, -1, 0, 0, 1, 1, 1}, {-1, -1, 0, 1, -1, -1, -1},
    {-1, -1, 0, 1, -1, -1, 1}, {-1, -1, 0, 1, -1, 1, -1}, {-1, -1, 0, 1, -1, 1,
    1}, {-1, -1, 0, 1, 1, -1, -1}, {-1, -1, 0, 1, 1, -1, 1}, {-1, -1, 0, 1, 1,
    1, -1}, {-1, -1, 0, 1, 1, 1, 1}, {-1, -1, 1, -1, -1, -1, -1}, {-1, -1, 1,
    -1, -1, -1, 1}, {-1, -1, 1, -1, -1, 1, -1}, {-1, -1, 1, -1, -1, 1, 1}, {-1,
    -1, 1, -1, 1, -1, -1}, {-1, -1, 1, -1, 1, -1, 1}, {-1, -1, 1, -1, 1, 1,
    -1}, {-1, -1, 1, -1, 1, 1, 1}, {-1, -1, 1, 0, -1, -1, -1}, {-1, -1, 1, 0,
    -1, -1, 1}, {-1, -1, 1, 0, -1, 1, -1}, {-1, -1, 1, 0, -1, 1, 1}, {-1, -1,
    1, 0, 1, -1, -1}, {-1, -1, 1, 0, 1, -1, 1}, {-1, -1, 1, 0, 1, 1, -1}, {-1,
    -1, 1, 0, 1, 1, 1}, {-1, -1, 1, 1, -1, -1, -1}, {-1, -1, 1, 1, -1, -1, 1},
    {-1, -1, 1, 1, -1, 1, -1}, {-1, -1, 1, 1, -1, 1, 1}, {-1, -1, 1, 1, 1, -1,
    -1}, {-1, -1, 1, 1, 1, -1, 1}, {-1, -1, 1, 1, 1, 1, -1}, {-1, -1, 1, 1, 1,
    1, 1}, {-1, 1, -1, -1, -1, -1, -1}, {-1, 1, -1, -1, -1, -1, 1}, {-1, 1, -1,
    -1, -1, 1, -1}, {-1, 1, -1, -1, -1, 1, 1}, {-1, 1, -1, -1, 1, -1, -1}, {-1,
    1, -1, -1, 1, -1, 1}, {-1, 1, -1, -1, 1, 1, -1}, {-1, 1, -1, -1, 1, 1, 1},
    {-1, 1, -1, 0, -1, -1, -1}, {-1, 1, -1, 0, -1, -1, 1}, {-1, 1, -1, 0, -1,
    1, -1}, {-1, 1, -1, 0, -1, 1, 1}, {-1, 1, -1, 0, 1, -1, -1}, {-1, 1, -1, 0,
    1, -1, 1}, {-1, 1, -1, 0, 1, 1, -1}, {-1, 1, -1, 0, 1, 1, 1}, {-1, 1, -1,
    1, -1, -1, -1}, {-1, 1, -1, 1, -1, -1, 1}, {-1, 1, -1, 1, -1, 1, -1}, {-1,
    1, -1, 1, -1, 1, 1}, {-1, 1, -1, 1, 1, -1, -1}, {-1, 1, -1, 1, 1, -1, 1},
    {-1, 1, -1, 1, 1, 1, -1}, {-1, 1, -1, 1, 1, 1, 1}, {-1, 1, 0, -1, -1, -1,
    -1}, {-1, 1, 0, -1, -1, -1, 1}, {-1, 1, 0, -1, -1, 1, -1}, {-1, 1, 0, -1,
    -1, 1, 1}, {-1, 1, 0, -1, 1, -1, -1}, {-1, 1, 0, -1, 1, -1, 1}, {-1, 1, 0,
    -1, 1, 1, -1}, {-1, 1, 0, -1, 1, 1, 1}, {-1, 1, 0, 0, -1, -1, -1}, {-1, 1,
    0, 0, -1, -1, 1}, {-1, 1, 0, 0, -1, 1, -1}, {-1, 1, 0, 0, -1, 1, 1}, {-1,
    1, 0, 0, 1, -1, -1}, {-1, 1, 0, 0, 1, -1, 1}, {-1, 1, 0, 0, 1, 1, -1}, {-1,
    1, 0, 0, 1, 1, 1}, {-1, 1, 0, 1, -1, -1, -1}, {-1, 1, 0, 1, -1, -1, 1},
    {-1, 1, 0, 1, -1, 1, -1}, {-1, 1, 0, 1, -1, 1, 1}, {-1, 1, 0, 1, 1, -1,
    -1}, {-1, 1, 0, 1, 1, -1, 1}, {-1, 1, 0, 1, 1, 1, -1}, {-1, 1, 0, 1, 1, 1,
    1}, {-1, 1, 1, -1, -1, -1, -1}, {-1, 1, 1, -1, -1, -1, 1}, {-1, 1, 1, -1,
    -1, 1, -1}, {-1, 1, 1, -1, -1, 1, 1}, {-1, 1, 1, -1, 1, -1, -1}, {-1, 1, 1,
    -1, 1, -1, 1}, {-1, 1, 1, -1, 1, 1, -1}, {-1, 1, 1, -1, 1, 1, 1}, {-1, 1,
    1, 0, -1, -1, -1}, {-1, 1, 1, 0, -1, -1, 1}, {-1, 1, 1, 0, -1, 1, -1}, {-1,
    1, 1, 0, -1, 1, 1}, {-1, 1, 1, 0, 1, -1, -1}, {-1, 1, 1, 0, 1, -1, 1}, {-1,
    1, 1, 0, 1, 1, -1}, {-1, 1, 1, 0, 1, 1, 1}, {-1, 1, 1, 1, -1, -1, -1}, {-1,
    1, 1, 1, -1, -1, 1}, {-1, 1, 1, 1, -1, 1, -1}, {-1, 1, 1, 1, -1, 1, 1},
    {-1, 1, 1, 1, 1, -1, -1}, {-1, 1, 1, 1, 1, -1, 1}, {-1, 1, 1, 1, 1, 1, -1},
    {-1, 1, 1, 1, 1, 1, 1}, {1, -1, -1, -1, -1, -1, -1}, {1, -1, -1, -1, -1,
    -1, 1}, {1, -1, -1, -1, -1, 1, -1}, {1, -1, -1, -1, -1, 1, 1}, {1, -1, -1,
    -1, 1, -1, -1}, {1, -1, -1, -1, 1, -1, 1}, {1, -1, -1, -1, 1, 1, -1}, {1,
    -1, -1, -1, 1, 1, 1}, {1, -1, -1, 0, -1, -1, -1}, {1, -1, -1, 0, -1, -1,
    1}, {1, -1, -1, 0, -1, 1, -1}, {1, -1, -1, 0, -1, 1, 1}, {1, -1, -1, 0, 1,
    -1, -1}, {1, -1, -1, 0, 1, -1, 1}, {1, -1, -1, 0, 1, 1, -1}, {1, -1, -1, 0,
    1, 1, 1}, {1, -1, -1, 1, -1, -1, -1}, {1, -1, -1, 1, -1, -1, 1}, {1, -1,
    -1, 1, -1, 1, -1}, {1, -1, -1, 1, -1, 1, 1}, {1, -1, -1, 1, 1, -1, -1}, {1,
    -1, -1, 1, 1, -1, 1}, {1, -1, -1, 1, 1, 1, -1}, {1, -1, -1, 1, 1, 1, 1},
    {1, -1, 0, -1, -1, -1, -1}, {1, -1, 0, -1, -1, -1, 1}, {1, -1, 0, -1, -1,
    1, -1}, {1, -1, 0, -1, -1, 1, 1}, {1, -1, 0, -1, 1, -1, -1}, {1, -1, 0, -1,
    1, -1, 1}, {1, -1, 0, -1, 1, 1, -1}, {1, -1, 0, -1, 1, 1, 1}, {1, -1, 0, 0,
    -1, -1, -1}, {1, -1, 0, 0, -1, -1, 1}, {1, -1, 0, 0, -1, 1, -1}, {1, -1, 0,
    0, -1, 1, 1}, {1, -1, 0, 0, 1, -1, -1}, {1, -1, 0, 0, 1, -1, 1}, {1, -1, 0,
    0, 1, 1, -1}, {1, -1, 0, 0, 1, 1, 1}, {1, -1, 0, 1, -1, -1, -1}, {1, -1, 0,
    1, -1, -1, 1}, {1, -1, 0, 1, -1, 1, -1}, {1, -1, 0, 1, -1, 1, 1}, {1, -1,
    0, 1, 1, -1, -1}, {1, -1, 0, 1, 1, -1, 1}, {1, -1, 0, 1, 1, 1, -1}, {1, -1,
    0, 1, 1, 1, 1}, {1, -1, 1, -1, -1, -1, -1}, {1, -1, 1, -1, -1, -1, 1}, {1,
    -1, 1, -1, -1, 1, -1}, {1, -1, 1, -1, -1, 1, 1}, {1, -1, 1, -1, 1, -1, -1},
    {1, -1, 1, -1, 1, -1, 1}, {1, -1, 1, -1, 1, 1, -1}, {1, -1, 1, -1, 1, 1,
    1}, {1, -1, 1, 0, -1, -1, -1}, {1, -1, 1, 0, -1, -1, 1}, {1, -1, 1, 0, -1,
    1, -1}, {1, -1, 1, 0, -1, 1, 1}, {1, -1, 1, 0, 1, -1, -1}, {1, -1, 1, 0, 1,
    -1, 1}, {1, -1, 1, 0, 1, 1, -1}, {1, -1, 1, 0, 1, 1, 1}, {1, -1, 1, 1, -1,
    -1, -1}, {1, -1, 1, 1, -1, -1, 1}, {1, -1, 1, 1, -1, 1, -1}, {1, -1, 1, 1,
    -1, 1, 1}, {1, -1, 1, 1, 1, -1, -1}, {1, -1, 1, 1, 1, -1, 1}, {1, -1, 1, 1,
    1, 1, -1}, {1, -1, 1, 1, 1, 1, 1}, {1, 1, -1, -1, -1, -1, -1}, {1, 1, -1,
    -1, -1, -1, 1}, {1, 1, -1, -1, -1, 1, -1}, {1, 1, -1, -1, -1, 1, 1}, {1, 1,
    -1, -1, 1, -1, -1}, {1, 1, -1, -1, 1, -1, 1}, {1, 1, -1, -1, 1, 1, -1}, {1,
    1, -1, -1, 1, 1, 1}, {1, 1, -1, 0, -1, -1, -1}, {1, 1, -1, 0, -1, -1, 1},
    {1, 1, -1, 0, -1, 1, -1}, {1, 1, -1, 0, -1, 1, 1}, {1, 1, -1, 0, 1, -1,
    -1}, {1, 1, -1, 0, 1, -1, 1}, {1, 1, -1, 0, 1, 1, -1}, {1, 1, -1, 0, 1, 1,
    1}, {1, 1, -1, 1, -1, -1, -1}, {1, 1, -1, 1, -1, -1, 1}, {1, 1, -1, 1, -1,
    1, -1}, {1, 1, -1, 1, -1, 1, 1}, {1, 1, -1, 1, 1, -1, -1}, {1, 1, -1, 1, 1,
    -1, 1}, {1, 1, -1, 1, 1, 1, -1}, {1, 1, -1, 1, 1, 1, 1}, {1, 1, 0, -1, -1,
    -1, -1}, {1, 1, 0, -1, -1, -1, 1}, {1, 1, 0, -1, -1, 1, -1}, {1, 1, 0, -1,
    -1, 1, 1}, {1, 1, 0, -1, 1, -1, -1}, {1, 1, 0, -1, 1, -1, 1}, {1, 1, 0, -1,
    1, 1, -1}, {1, 1, 0, -1, 1, 1, 1}, {1, 1, 0, 0, -1, -1, -1}, {1, 1, 0, 0,
    -1, -1, 1}, {1, 1, 0, 0, -1, 1, -1}, {1, 1, 0, 0, -1, 1, 1}, {1, 1, 0, 0,
    1, -1, -1}, {1, 1, 0, 0, 1, -1, 1}, {1, 1, 0, 0, 1, 1, -1}, {1, 1, 0, 0, 1,
    1, 1}, {1, 1, 0, 1, -1, -1, -1}, {1, 1, 0, 1, -1, -1, 1}, {1, 1, 0, 1, -1,
    1, -1}, {1, 1, 0, 1, -1, 1, 1}, {1, 1, 0, 1, 1, -1, -1}, {1, 1, 0, 1, 1,
    -1, 1}, {1, 1, 0, 1, 1, 1, -1}, {1, 1, 0, 1, 1, 1, 1}, {1, 1, 1, -1, -1,
    -1, -1}, {1, 1, 1, -1, -1, -1, 1}, {1, 1, 1, -1, -1, 1, -1}, {1, 1, 1, -1,
    -1, 1, 1}, {1, 1, 1, -1, 1, -1, -1}, {1, 1, 1, -1, 1, -1, 1}, {1, 1, 1, -1,
    1, 1, -1}, {1, 1, 1, -1, 1, 1, 1}, {1, 1, 1, 0, -1, -1, -1}, {1, 1, 1, 0,
    -1, -1, 1}, {1, 1, 1, 0, -1, 1, -1}, {1, 1, 1, 0, -1, 1, 1}, {1, 1, 1, 0,
    1, -1, -1}, {1, 1, 1, 0, 1, -1, 1}, {1, 1, 1, 0, 1, 1, -1}, {1, 1, 1, 0, 1,
    1, 1}, {1, 1, 1, 1, -1, -1, -1}, {1, 1, 1, 1, -1, -1, 1}, {1, 1, 1, 1, -1,
    1, -1}, {1, 1, 1, 1, -1, 1, 1}, {1, 1, 1, 1, 1, -1, -1}, {1, 1, 1, 1, 1,
    -1, 1}, {1, 1, 1, 1, 1, 1, -1}, {1, 1, 1, 1, 1, 1, 1}};

// Normalization factors the various processes
// Denominators: spins, colors and identical particles
int PY8MEs_R12_P62_sm_qb_wpwmgqb::denom_colors[nprocesses] = {9, 9, 9, 9, 9, 9,
    9, 9};
int PY8MEs_R12_P62_sm_qb_wpwmgqb::denom_hels[nprocesses] = {4, 4, 4, 4, 4, 4,
    4, 4};
int PY8MEs_R12_P62_sm_qb_wpwmgqb::denom_iden[nprocesses] = {1, 1, 1, 1, 1, 1,
    1, 1};

//--------------------------------------------------------------------------
// Color config initialization
void PY8MEs_R12_P62_sm_qb_wpwmgqb::initColorConfigs() 
{
  color_configs = vector < vec_vec_int > (); 
  jamp_nc_relative_power = vector < vec_int > (); 

  // Color flows of process Process: u b > w+ w- g u b WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[0].push_back(-1); 
  // JAMP #1
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #2
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #3
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[0].push_back(-1); 

  // Color flows of process Process: u b~ > w+ w- g u b~ WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #1
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[1].push_back(-1); 
  // JAMP #2
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #3
  color_configs[1].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[1].push_back(-1); 

  // Color flows of process Process: d b > w+ w- g d b WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[2].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[2].push_back(-1); 
  // JAMP #1
  color_configs[2].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #2
  color_configs[2].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #3
  color_configs[2].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[2].push_back(-1); 

  // Color flows of process Process: d b~ > w+ w- g d b~ WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[3].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #1
  color_configs[3].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[3].push_back(-1); 
  // JAMP #2
  color_configs[3].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[3].push_back(0); 
  // JAMP #3
  color_configs[3].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[3].push_back(-1); 

  // Color flows of process Process: u~ b > w+ w- g u~ b WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[4].push_back(vec_int(createvector<int>
      (0)(1)(1)(0)(0)(0)(0)(0)(3)(2)(0)(3)(2)(0)));
  jamp_nc_relative_power[4].push_back(0); 
  // JAMP #1
  color_configs[4].push_back(vec_int(createvector<int>
      (0)(1)(3)(0)(0)(0)(0)(0)(3)(2)(0)(1)(2)(0)));
  jamp_nc_relative_power[4].push_back(-1); 
  // JAMP #2
  color_configs[4].push_back(vec_int(createvector<int>
      (0)(1)(3)(0)(0)(0)(0)(0)(3)(1)(0)(2)(2)(0)));
  jamp_nc_relative_power[4].push_back(0); 
  // JAMP #3
  color_configs[4].push_back(vec_int(createvector<int>
      (0)(1)(2)(0)(0)(0)(0)(0)(3)(1)(0)(3)(2)(0)));
  jamp_nc_relative_power[4].push_back(-1); 

  // Color flows of process Process: u~ b~ > w+ w- g u~ b~ WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[5].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[5].push_back(-1); 
  // JAMP #1
  color_configs[5].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[5].push_back(0); 
  // JAMP #2
  color_configs[5].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[5].push_back(0); 
  // JAMP #3
  color_configs[5].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[5].push_back(-1); 

  // Color flows of process Process: d~ b > w+ w- g d~ b WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[6].push_back(vec_int(createvector<int>
      (0)(1)(1)(0)(0)(0)(0)(0)(3)(2)(0)(3)(2)(0)));
  jamp_nc_relative_power[6].push_back(0); 
  // JAMP #1
  color_configs[6].push_back(vec_int(createvector<int>
      (0)(1)(3)(0)(0)(0)(0)(0)(3)(2)(0)(1)(2)(0)));
  jamp_nc_relative_power[6].push_back(-1); 
  // JAMP #2
  color_configs[6].push_back(vec_int(createvector<int>
      (0)(1)(3)(0)(0)(0)(0)(0)(3)(1)(0)(2)(2)(0)));
  jamp_nc_relative_power[6].push_back(0); 
  // JAMP #3
  color_configs[6].push_back(vec_int(createvector<int>
      (0)(1)(2)(0)(0)(0)(0)(0)(3)(1)(0)(3)(2)(0)));
  jamp_nc_relative_power[6].push_back(-1); 

  // Color flows of process Process: d~ b~ > w+ w- g d~ b~ WEIGHTED<=7 @12
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[7].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[7].push_back(-1); 
  // JAMP #1
  color_configs[7].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[7].push_back(0); 
  // JAMP #2
  color_configs[7].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[7].push_back(0); 
  // JAMP #3
  color_configs[7].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[7].push_back(-1); 
}

//--------------------------------------------------------------------------
// Destructor.
PY8MEs_R12_P62_sm_qb_wpwmgqb::~PY8MEs_R12_P62_sm_qb_wpwmgqb() 
{
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    delete[] p[i]; 
    p[i] = NULL; 
  }
}

//--------------------------------------------------------------------------
// Invert the permutation mapping
vector<int> PY8MEs_R12_P62_sm_qb_wpwmgqb::invert_mapping(vector<int> mapping) 
{
  vector<int> inverted_mapping; 
  for (unsigned int i = 0; i < mapping.size(); i++ )
  {
    for (unsigned int j = 0; j < mapping.size(); j++ )
    {
      if (mapping[j] == ((int)i))
      {
        inverted_mapping.push_back(j); 
        break; 
      }
    }
  }
  return inverted_mapping; 
}

//--------------------------------------------------------------------------
// Return the list of possible helicity configurations
vector < vec_int > PY8MEs_R12_P62_sm_qb_wpwmgqb::getHelicityConfigs(vector<int>
    permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(ncomb, vector<int> (nexternal, 0)); 
  for (unsigned int ihel = 0; ihel < ncomb; ihel++ )
  {
    for(unsigned int j = 0; j < nexternal; j++ )
    {
      res[ihel][chosenPerm[j]] = helicities[ihel][j]; 
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Return the list of possible color configurations
vector < vec_int > PY8MEs_R12_P62_sm_qb_wpwmgqb::getColorConfigs(int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(color_configs[chosenProcID].size(), vector<int>
      (nexternal * 2, 0));
  for (unsigned int icol = 0; icol < color_configs[chosenProcID].size(); icol++
      )
  {
    for(unsigned int j = 0; j < (2 * nexternal); j++ )
    {
      res[icol][chosenPerm[j/2] * 2 + j%2] =
          color_configs[chosenProcID][icol][j];
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Get JAMP relative N_c power
int PY8MEs_R12_P62_sm_qb_wpwmgqb::getColorFlowRelativeNCPower(int
    color_flow_ID, int specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return jamp_nc_relative_power[chosenProcID][color_flow_ID]; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity ID -> Helicity Config
vector<int> PY8MEs_R12_P62_sm_qb_wpwmgqb::getHelicityConfigForID(int hel_ID,
    vector<int> permutation)
{
  if (hel_ID < 0 || hel_ID >= ncomb)
  {
    cerr <<  "Error in function 'getHelicityConfigForID' of class" << 
    " 'PY8MEs_R12_P62_sm_qb_wpwmgqb': Specified helicity ID '" << 
    hel_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R12_P62_sm_qb_wpwmgqb_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(nexternal, 0); 
  for (unsigned int j = 0; j < nexternal; j++ )
  {
    res[chosenPerm[j]] = helicities[hel_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity Config -> Helicity ID
int PY8MEs_R12_P62_sm_qb_wpwmgqb::getHelicityIDForConfig(vector<int>
    hel_config, vector<int> permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  int user_ihel = -1; 
  if (hel_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < ncomb; i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < nexternal; j++ )
      {
        if (helicities[i][chosenPerm[j]] != hel_config[j])
        {
          found = false; 
          break; 
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_ihel = i; 
        break; 
      }
    }
    if (user_ihel == -1)
    {
      cerr <<  "Error in function 'getHelicityIDForConfig' of class" << 
      " 'PY8MEs_R12_P62_sm_qb_wpwmgqb': Specified helicity" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R12_P62_sm_qb_wpwmgqb_exception; 
    }
  }
  return user_ihel; 
}


//--------------------------------------------------------------------------
// Implements the map Color ID -> Color Config
vector<int> PY8MEs_R12_P62_sm_qb_wpwmgqb::getColorConfigForID(int color_ID, int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < 0 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getColorConfigForID' of class" << 
    " 'PY8MEs_R12_P62_sm_qb_wpwmgqb': Specified color ID '" << 
    color_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R12_P62_sm_qb_wpwmgqb_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(color_configs[chosenProcID][color_ID].size(), 0); 
  for (unsigned int j = 0; j < (2 * nexternal); j++ )
  {
    res[chosenPerm[j/2] * 2 + j%2] = color_configs[chosenProcID][color_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Color Config -> Color ID
int PY8MEs_R12_P62_sm_qb_wpwmgqb::getColorIDForConfig(vector<int> color_config,
    int specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = -1; 
  if (color_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < color_configs[chosenProcID].size(); i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < (nexternal * 2); j++ )
      {

        // If colorless then make sure it matches
        // The little arithmetics in the color index is just
        // the permutation applies on the particle list which is
        // twice smaller since each particle can have two color indices.
        if (color_config[j] == 0)
        {
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] != 0)
          {
            found = false; 
            break; 
          }
          // Otherwise check that the color linked position matches
        }
        else
        {
          int color_linked_pos = -1; 
          // Find the other end of the line in the user color config
          for (unsigned int k = 0; k < (nexternal * 2); k++ )
          {
            if (k == j)
              continue; 
            if (color_config[j] == color_config[k])
            {
              color_linked_pos = k; 
              break; 
            }
          }
          if (color_linked_pos == -1)
          {
            cerr <<  "Error in function 'getColorIDForConfig' of class" << 
            " 'PY8MEs_R12_P62_sm_qb_wpwmgqb': A color line could " << 
            " not be closed." << endl; 
            throw PY8MEs_R12_P62_sm_qb_wpwmgqb_exception; 
          }
          // Now check whether the color line matches
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] !=
              color_configs[chosenProcID][i][chosenPerm[color_linked_pos/2] * 2
              + color_linked_pos%2])
          {
            found = false; 
            break; 
          }
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_icol = i; 
        break; 
      }
    }

    if (user_icol == -1)
    {
      cerr <<  "Error in function 'getColorIDForConfig' of class" << 
      " 'PY8MEs_R12_P62_sm_qb_wpwmgqb': Specified color" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R12_P62_sm_qb_wpwmgqb_exception; 
    }
  }
  return user_icol; 
}

//--------------------------------------------------------------------------
// Returns all result previously computed in SigmaKin
vector < vec_double > PY8MEs_R12_P62_sm_qb_wpwmgqb::getAllResults(int
    specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return all_results[chosenProcID]; 
}

//--------------------------------------------------------------------------
// Returns a result previously computed in SigmaKin for a specific helicity
// and color ID. -1 means avg and summed over that characteristic.
double PY8MEs_R12_P62_sm_qb_wpwmgqb::getResult(int helicity_ID, int color_ID,
    int specify_proc_ID)
{
  if (helicity_ID < - 1 || helicity_ID >= ncomb)
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R12_P62_sm_qb_wpwmgqb': Specified helicity ID '" << 
    helicity_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R12_P62_sm_qb_wpwmgqb_exception; 
  }
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < - 1 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R12_P62_sm_qb_wpwmgqb': Specified color ID '" << 
    color_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R12_P62_sm_qb_wpwmgqb_exception; 
  }
  return all_results[chosenProcID][helicity_ID + 1][color_ID + 1]; 
}

//--------------------------------------------------------------------------
// Check for the availability of the requested process and if available,
// If available, this returns the corresponding permutation and Proc_ID to use.
// If not available, this returns a negative Proc_ID.
pair < vector<int> , int > PY8MEs_R12_P62_sm_qb_wpwmgqb::static_getPY8ME(vector<int> initial_pdgs, vector<int> final_pdgs, set<int> schannels) 
{

  // Not available return value
  pair < vector<int> , int > NA(vector<int> (), -1); 

  // Check if s-channel requirements match
  if (nreq_s_channels > 0)
  {
    if (schannels != s_channel_proc)
      return NA; 
  }
  else
  {
    if (schannels.size() != 0)
      return NA; 
  }

  // Check number of final state particles
  if (final_pdgs.size() != (nexternal - ninitial))
    return NA; 

  // Check number of initial state particles
  if (initial_pdgs.size() != ninitial)
    return NA; 

  // List of processes available in this class
  const int nprocs = 32; 
  const int proc_IDS[nprocs] = {0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7,
      0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7};
  const int in_pdgs[nprocs][ninitial] = {{2, 5}, {4, 5}, {2, -5}, {4, -5}, {1,
      5}, {3, 5}, {1, -5}, {3, -5}, {-2, 5}, {-4, 5}, {-2, -5}, {-4, -5}, {-1,
      5}, {-3, 5}, {-1, -5}, {-3, -5}, {5, 2}, {5, 4}, {-5, 2}, {-5, 4}, {5,
      1}, {5, 3}, {-5, 1}, {-5, 3}, {5, -2}, {5, -4}, {-5, -2}, {-5, -4}, {5,
      -1}, {5, -3}, {-5, -1}, {-5, -3}};
  const int out_pdgs[nprocs][nexternal - ninitial] = {{24, -24, 21, 2, 5}, {24,
      -24, 21, 4, 5}, {24, -24, 21, 2, -5}, {24, -24, 21, 4, -5}, {24, -24, 21,
      1, 5}, {24, -24, 21, 3, 5}, {24, -24, 21, 1, -5}, {24, -24, 21, 3, -5},
      {24, -24, 21, -2, 5}, {24, -24, 21, -4, 5}, {24, -24, 21, -2, -5}, {24,
      -24, 21, -4, -5}, {24, -24, 21, -1, 5}, {24, -24, 21, -3, 5}, {24, -24,
      21, -1, -5}, {24, -24, 21, -3, -5}, {24, -24, 21, 2, 5}, {24, -24, 21, 4,
      5}, {24, -24, 21, 2, -5}, {24, -24, 21, 4, -5}, {24, -24, 21, 1, 5}, {24,
      -24, 21, 3, 5}, {24, -24, 21, 1, -5}, {24, -24, 21, 3, -5}, {24, -24, 21,
      -2, 5}, {24, -24, 21, -4, 5}, {24, -24, 21, -2, -5}, {24, -24, 21, -4,
      -5}, {24, -24, 21, -1, 5}, {24, -24, 21, -3, 5}, {24, -24, 21, -1, -5},
      {24, -24, 21, -3, -5}};

  bool in_pdgs_used[ninitial]; 
  bool out_pdgs_used[nexternal - ninitial]; 
  for(unsigned int i = 0; i < nprocs; i++ )
  {
    int permutations[nexternal]; 

    // Reinitialize initial state look-up variables
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      in_pdgs_used[j] = false; 
      permutations[j] = -1; 
    }
    // Look for initial state matches
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      for(unsigned int k = 0; k < ninitial; k++ )
      {
        // Make sure it has not been used already
        if (in_pdgs_used[k])
          continue; 
        if (initial_pdgs[k] == in_pdgs[i][j])
        {
          permutations[j] = k; 
          in_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[ninitial - 1] == -1)
      continue; 

    // Reinitialize final state look-up variables
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      out_pdgs_used[j] = false; 
      permutations[ninitial + j] = -1; 
    }
    // Look for final state matches
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      for(unsigned int k = 0; k < (nexternal - ninitial); k++ )
      {
        // Make sure it has not been used already
        if (out_pdgs_used[k])
          continue; 
        if (final_pdgs[k] == out_pdgs[i][j])
        {
          permutations[ninitial + j] = ninitial + k; 
          out_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[ninitial + j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[nexternal - 1] == -1)
      continue; 

    // Return process found
    return pair < vector<int> , int > (vector<int> (permutations, permutations
        + nexternal), proc_IDS[i]);
  }

  // No process found
  return NA; 
}

//--------------------------------------------------------------------------
// Set momenta
void PY8MEs_R12_P62_sm_qb_wpwmgqb::setMomenta(vector < vec_double >
    momenta_picked)
{
  if (momenta_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setMomenta' of class" << 
    " 'PY8MEs_R12_P62_sm_qb_wpwmgqb': Incorrect number of" << 
    " momenta specified." << endl; 
    throw PY8MEs_R12_P62_sm_qb_wpwmgqb_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    if (momenta_picked[i].size() != 4)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R12_P62_sm_qb_wpwmgqb': Incorrect number of" << 
      " momenta components specified." << endl; 
      throw PY8MEs_R12_P62_sm_qb_wpwmgqb_exception; 
    }
    if (momenta_picked[i][0] < 0.0)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R12_P62_sm_qb_wpwmgqb': A momentum was specified" << 
      " with negative energy. Check conventions." << endl; 
      throw PY8MEs_R12_P62_sm_qb_wpwmgqb_exception; 
    }
    for (unsigned int j = 0; j < 4; j++ )
    {
      p[i][j] = momenta_picked[i][j]; 
    }
  }
}

//--------------------------------------------------------------------------
// Set color configuration to use. An empty vector means sum over all.
void PY8MEs_R12_P62_sm_qb_wpwmgqb::setColors(vector<int> colors_picked)
{
  if (colors_picked.size() == 0)
  {
    user_colors = vector<int> (); 
    return; 
  }
  if (colors_picked.size() != (2 * nexternal))
  {
    cerr <<  "Error in function 'setColors' of class" << 
    " 'PY8MEs_R12_P62_sm_qb_wpwmgqb': Incorrect number" << 
    " of colors specified." << endl; 
    throw PY8MEs_R12_P62_sm_qb_wpwmgqb_exception; 
  }
  user_colors = vector<int> ((2 * nexternal), 0); 
  for(unsigned int i = 0; i < (2 * nexternal); i++ )
  {
    user_colors[i] = colors_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the helicity configuration to use. Am empty vector means sum over all.
void PY8MEs_R12_P62_sm_qb_wpwmgqb::setHelicities(vector<int> helicities_picked) 
{
  if (helicities_picked.size() != nexternal)
  {
    if (helicities_picked.size() == 0)
    {
      user_helicities = vector<int> (); 
      return; 
    }
    cerr <<  "Error in function 'setHelicities' of class" << 
    " 'PY8MEs_R12_P62_sm_qb_wpwmgqb': Incorrect number" << 
    " of helicities specified." << endl; 
    throw PY8MEs_R12_P62_sm_qb_wpwmgqb_exception; 
  }
  user_helicities = vector<int> (nexternal, 0); 
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    user_helicities[i] = helicities_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the permutation to use (will apply to momenta, colors and helicities)
void PY8MEs_R12_P62_sm_qb_wpwmgqb::setPermutation(vector<int> perm_picked) 
{
  if (perm_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setPermutations' of class" << 
    " 'PY8MEs_R12_P62_sm_qb_wpwmgqb': Incorrect number" << 
    " of permutations specified." << endl; 
    throw PY8MEs_R12_P62_sm_qb_wpwmgqb_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    perm[i] = perm_picked[i]; 
  }
}

// Set the proc_ID to use
void PY8MEs_R12_P62_sm_qb_wpwmgqb::setProcID(int procID_picked) 
{
  proc_ID = procID_picked; 
}

//--------------------------------------------------------------------------
// Initialize process.

void PY8MEs_R12_P62_sm_qb_wpwmgqb::initProc() 
{

  // Initialize flags
  include_symmetry_factors = true; 
  include_helicity_averaging_factors = true; 
  include_color_averaging_factors = true; 

  // Initialize vectors.
  perm = vector<int> (nexternal, 0); 
  user_colors = vector<int> (2 * nexternal, 0); 
  user_helicities = vector<int> (nexternal, 0); 
  p = vector < double * > (); 
  for (unsigned int i = 0; i < nexternal; i++ )
  {
    p.push_back(new double[4]); 
  }
  initColorConfigs(); 
  // Synchronize local variables dependent on the model with the active model.
  mME = vector<double> (nexternal, 0.); 
  syncProcModelParams(); 
  jamp2 = vector < vec_double > (8); 
  jamp2[0] = vector<double> (4, 0.); 
  jamp2[1] = vector<double> (4, 0.); 
  jamp2[2] = vector<double> (4, 0.); 
  jamp2[3] = vector<double> (4, 0.); 
  jamp2[4] = vector<double> (4, 0.); 
  jamp2[5] = vector<double> (4, 0.); 
  jamp2[6] = vector<double> (4, 0.); 
  jamp2[7] = vector<double> (4, 0.); 
  all_results = vector < vec_vec_double > (8); 
  // The first entry is always the color or helicity avg/summed matrix element.
  all_results[0] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[1] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[2] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[3] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[4] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[5] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[6] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[7] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
}

// Synchronize local variables of the process that depend on the model
// parameters
void PY8MEs_R12_P62_sm_qb_wpwmgqb::syncProcModelParams() 
{

  // Instantiate the model class and set parameters that stay fixed during run
  mME[0] = pars->ZERO; 
  mME[1] = pars->mdl_MB; 
  mME[2] = pars->mdl_MW; 
  mME[3] = pars->mdl_MW; 
  mME[4] = pars->ZERO; 
  mME[5] = pars->ZERO; 
  mME[6] = pars->mdl_MB; 
}

//--------------------------------------------------------------------------
// Setter allowing to force particular values for the external masses
void PY8MEs_R12_P62_sm_qb_wpwmgqb::setMasses(vec_double external_masses) 
{

  if (external_masses.size() != mME.size())
  {
    cerr <<  "Error in function 'setMasses' of class" << 
    " 'PY8MEs_R12_P62_sm_qb_wpwmgqb': Incorrect number of" << 
    " masses specified." << endl; 
    throw PY8MEs_R12_P62_sm_qb_wpwmgqb_exception; 
  }
  for (unsigned int j = 0; j < mME.size(); j++ )
  {
    mME[j] = external_masses[perm[j]]; 
  }
}

//--------------------------------------------------------------------------
// Getter accessing external masses with the correct ordering
vector<double> PY8MEs_R12_P62_sm_qb_wpwmgqb::getMasses() 
{

  vec_double external_masses; 
  vector<int> invertedPerm; 
  invertedPerm = invert_mapping(perm); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    external_masses.push_back(mME[invertedPerm[i]]); 
  }
  return external_masses; 

}


// Set all values of the external masses to float(-mode) where mode can be
// 0 : Mass taken from the model
// 1 : Mass taken from p_i^2 if not massless to begin with
// 2 : Mass always taken from p_i^2.
void PY8MEs_R12_P62_sm_qb_wpwmgqb::setExternalMassesMode(int mode) 
{
  if (mode != 0 && mode != 1 && mode != 2)
  {
    cerr <<  "Error in function 'setExternalMassesMode' of class" << 
    " 'PY8MEs_R12_P62_sm_qb_wpwmgqb': Incorrect mode selected :" << mode << 
    ". It must be either 0, 1 or 2" << endl; 
    throw PY8MEs_R12_P62_sm_qb_wpwmgqb_exception; 
  }
  if (mode == 0)
  {
    syncProcModelParams(); 
  }
  else if (mode == 1)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      if (mME[j] != pars->ZERO)
      {
        mME[j] = -1.0; 
      }
    }
  }
  else if (mode == 2)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      mME[j] = -1.0; 
    }
  }
}

//--------------------------------------------------------------------------
// Evaluate the squared matrix element.

double PY8MEs_R12_P62_sm_qb_wpwmgqb::sigmaKin() 
{
  // Set the parameters which change event by event
  pars->setDependentParameters(); 
  pars->setDependentCouplings(); 
  // Reset color flows
  for(int i = 0; i < 4; i++ )
    jamp2[0][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[1][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[2][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[3][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[4][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[5][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[6][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[7][i] = 0.; 

  // Save previous values of mME
  vector<double> saved_mME(mME.size(), 0.0); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (mME[i] < 0.0)
    {
      saved_mME[i] = mME[i]; 
      mME[i] = sqrt(abs(pow(p[perm[i]][0], 2) - 
      (pow(p[perm[i]][1], 2) + pow(p[perm[i]][2], 2) + pow(p[perm[i]][3],
          2))));
    }
  }

  // Local variables and constants
  const int max_tries = 10; 
  vector < vec_bool > goodhel(nprocesses, vec_bool(ncomb, false)); 
  vec_int ntry(nprocesses, 0); 
  double t = 0.; 
  double result = 0.; 

  if (ntry[proc_ID] <= max_tries)
    ntry[proc_ID] = ntry[proc_ID] + 1; 

  // Find which helicity configuration is asked for
  // -1 indicates one wants to sum over helicities
  int user_ihel = getHelicityIDForConfig(user_helicities); 

  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = getColorIDForConfig(user_colors); 

  // Reset the list of results that will be recomputed here
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      all_results[proc_ID][ihel + 1][icolor + 1] = 0.; 
    }
  }

  // Calculate the matrix element for all helicities
  // unless already detected as vanishing
  for(int ihel = 0; ihel < ncomb; ihel++ )
  {
    // Skip helicity if already detected as vanishing
    if ((ntry[proc_ID] >= max_tries) && !goodhel[proc_ID][ihel])
      continue; 

    // Also skip helicity if user asks for a specific one
    if ((ntry[proc_ID] >= max_tries) && user_ihel != -1 && user_ihel != ihel)
      continue; 

    calculate_wavefunctions(helicities[ihel]); 

    // Reset locally computed color flows
    for(int i = 0; i < 4; i++ )
      jamp2[0][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[1][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[2][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[3][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[4][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[5][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[6][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[7][i] = 0.; 

    if (proc_ID == 0)
      t = matrix_12_ub_wpwmgub(); 
    if (proc_ID == 1)
      t = matrix_12_ubx_wpwmgubx(); 
    if (proc_ID == 2)
      t = matrix_12_db_wpwmgdb(); 
    if (proc_ID == 3)
      t = matrix_12_dbx_wpwmgdbx(); 
    if (proc_ID == 4)
      t = matrix_12_uxb_wpwmguxb(); 
    if (proc_ID == 5)
      t = matrix_12_uxbx_wpwmguxbx(); 
    if (proc_ID == 6)
      t = matrix_12_dxb_wpwmgdxb(); 
    if (proc_ID == 7)
      t = matrix_12_dxbx_wpwmgdxbx(); 

    // Store which helicities give non-zero result
    if ((ntry[proc_ID] < max_tries) && t != 0. && !goodhel[proc_ID][ihel])
      goodhel[proc_ID][ihel] = true; 

    // Aggregate results
    if (user_ihel == -1 || user_ihel == ihel)
    {
      if (user_icol == -1)
      {
        result = result + t; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][0] += t; 
          for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
          {
            all_results[proc_ID][0][i + 1] += jamp2[proc_ID][i]; 
          }
        }
        all_results[proc_ID][ihel + 1][0] += t; 
        for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
        {
          all_results[proc_ID][ihel + 1][i + 1] += jamp2[proc_ID][i]; 
        }
      }
      else
      {
        result = result + jamp2[proc_ID][user_icol]; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][user_icol + 1] += jamp2[proc_ID][user_icol]; 
        }
        all_results[proc_ID][ihel + 1][user_icol + 1] +=
            jamp2[proc_ID][user_icol];
      }
    }

  }

  // Normalize results with the identical particle factor
  if (include_symmetry_factors)
  {
    result = result/denom_iden[proc_ID]; 
  }
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      if (include_symmetry_factors)
      {
        all_results[proc_ID][ihel + 1][icolor + 1] /= denom_iden[proc_ID]; 
      }
    }
  }


  // Normalize when when summing+averaging over helicity configurations
  if (user_ihel == -1 && include_helicity_averaging_factors)
  {
    result /= denom_hels[proc_ID]; 
    if (user_icol == -1)
    {
      all_results[proc_ID][0][0] /= denom_hels[proc_ID]; 
      for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
      {
        all_results[proc_ID][0][i + 1] /= denom_hels[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][0][user_icol + 1] /= denom_hels[proc_ID]; 
    }
  }

  // Normalize when summing+averaging over color configurations
  if (user_icol == -1 && include_color_averaging_factors)
  {
    result /= denom_colors[proc_ID]; 
    if (user_ihel == -1)
    {
      all_results[proc_ID][0][0] /= denom_colors[proc_ID]; 
      for (unsigned int i = 0; i < ncomb; i++ )
      {
        all_results[proc_ID][i + 1][0] /= denom_colors[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][user_ihel + 1][0] /= denom_colors[proc_ID]; 
    }
  }

  // Reinstate previous values of mME
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (saved_mME[i] < 0.0)
    {
      mME[i] = saved_mME[i]; 
    }
  }

  // Finally return it
  return result; 
}

//==========================================================================
// Private class member functions

//--------------------------------------------------------------------------
// Evaluate |M|^2 for each subprocess

void PY8MEs_R12_P62_sm_qb_wpwmgqb::calculate_wavefunctions(const int hel[])
{
  // Calculate wavefunctions for all processes
  // Calculate all wavefunctions
  ixxxxx(p[perm[0]], mME[0], hel[0], +1, w[0]); 
  ixxxxx(p[perm[1]], mME[1], hel[1], +1, w[1]); 
  vxxxxx(p[perm[2]], mME[2], hel[2], +1, w[2]); 
  vxxxxx(p[perm[3]], mME[3], hel[3], +1, w[3]); 
  vxxxxx(p[perm[4]], mME[4], hel[4], +1, w[4]); 
  oxxxxx(p[perm[5]], mME[5], hel[5], +1, w[5]); 
  oxxxxx(p[perm[6]], mME[6], hel[6], +1, w[6]); 
  FFV1_2(w[0], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[7]); 
  VVV1P0_1(w[3], w[2], pars->GC_4, pars->ZERO, pars->ZERO, w[8]); 
  FFV1P0_3(w[7], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[9]); 
  FFV1_1(w[6], w[8], pars->GC_1, pars->mdl_MB, pars->ZERO, w[10]); 
  FFV1_2(w[1], w[8], pars->GC_1, pars->mdl_MB, pars->ZERO, w[11]); 
  VVS1_3(w[3], w[2], pars->GC_72, pars->mdl_MH, pars->mdl_WH, w[12]); 
  FFS4_1(w[6], w[12], pars->GC_83, pars->mdl_MB, pars->ZERO, w[13]); 
  FFS4_2(w[1], w[12], pars->GC_83, pars->mdl_MB, pars->ZERO, w[14]); 
  VVV1_3(w[3], w[2], pars->GC_53, pars->mdl_MZ, pars->mdl_WZ, w[15]); 
  FFV2_3_1(w[6], w[15], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[16]);
  FFV2_3_2(w[1], w[15], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[17]);
  FFV1P0_3(w[1], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[18]); 
  FFV1_2(w[7], w[8], pars->GC_2, pars->ZERO, pars->ZERO, w[19]); 
  FFV1_2(w[7], w[18], pars->GC_11, pars->ZERO, pars->ZERO, w[20]); 
  FFV2_5_2(w[7], w[15], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[21]);
  FFV2_1(w[6], w[2], pars->GC_100, pars->mdl_MT, pars->mdl_WT, w[22]); 
  FFV2_1(w[22], w[3], pars->GC_100, pars->mdl_MB, pars->ZERO, w[23]); 
  FFV2_2(w[1], w[3], pars->GC_100, pars->mdl_MT, pars->mdl_WT, w[24]); 
  FFV2_1(w[5], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[25]); 
  FFV2_2(w[7], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[26]); 
  FFV2_2(w[24], w[2], pars->GC_100, pars->mdl_MB, pars->ZERO, w[27]); 
  FFV1_1(w[5], w[18], pars->GC_11, pars->ZERO, pars->ZERO, w[28]); 
  FFV1_1(w[5], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[29]); 
  FFV2_2(w[0], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[30]); 
  FFV2_1(w[29], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[31]); 
  FFV1_1(w[29], w[18], pars->GC_11, pars->ZERO, pars->ZERO, w[32]); 
  FFV1P0_3(w[0], w[29], pars->GC_11, pars->ZERO, pars->ZERO, w[33]); 
  FFV1_1(w[29], w[8], pars->GC_2, pars->ZERO, pars->ZERO, w[34]); 
  FFV2_5_1(w[29], w[15], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[35]);
  FFV1_2(w[0], w[18], pars->GC_11, pars->ZERO, pars->ZERO, w[36]); 
  FFV1_1(w[6], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[37]); 
  FFV1P0_3(w[1], w[37], pars->GC_11, pars->ZERO, pars->ZERO, w[38]); 
  FFV2_2(w[30], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[39]); 
  FFV1P0_3(w[0], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[40]); 
  FFV2_1(w[37], w[2], pars->GC_100, pars->mdl_MT, pars->mdl_WT, w[41]); 
  FFV1_2(w[1], w[40], pars->GC_11, pars->mdl_MB, pars->ZERO, w[42]); 
  FFV1_1(w[37], w[40], pars->GC_11, pars->mdl_MB, pars->ZERO, w[43]); 
  FFV1_1(w[37], w[8], pars->GC_1, pars->mdl_MB, pars->ZERO, w[44]); 
  FFS4_1(w[37], w[12], pars->GC_83, pars->mdl_MB, pars->ZERO, w[45]); 
  FFV2_3_1(w[37], w[15], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[46]);
  FFV1_2(w[0], w[8], pars->GC_2, pars->ZERO, pars->ZERO, w[47]); 
  FFV1_1(w[5], w[8], pars->GC_2, pars->ZERO, pars->ZERO, w[48]); 
  FFV2_5_2(w[0], w[15], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[49]);
  FFV2_5_1(w[5], w[15], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[50]);
  FFV2_1(w[25], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[51]); 
  FFV1_2(w[1], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[52]); 
  FFV1P0_3(w[52], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[53]); 
  FFV2_2(w[52], w[3], pars->GC_100, pars->mdl_MT, pars->mdl_WT, w[54]); 
  FFV1_1(w[6], w[40], pars->GC_11, pars->mdl_MB, pars->ZERO, w[55]); 
  FFV1_2(w[52], w[40], pars->GC_11, pars->mdl_MB, pars->ZERO, w[56]); 
  FFV1_2(w[52], w[8], pars->GC_1, pars->mdl_MB, pars->ZERO, w[57]); 
  FFS4_2(w[52], w[12], pars->GC_83, pars->mdl_MB, pars->ZERO, w[58]); 
  FFV2_3_2(w[52], w[15], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[59]);
  FFV1_2(w[30], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[60]); 
  FFV1_1(w[25], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[61]); 
  VVV1P0_1(w[4], w[18], pars->GC_10, pars->ZERO, pars->ZERO, w[62]); 
  VVV1P0_1(w[4], w[40], pars->GC_10, pars->ZERO, pars->ZERO, w[63]); 
  FFV1_1(w[22], w[4], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[64]); 
  FFV1_2(w[24], w[4], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[65]); 
  oxxxxx(p[perm[1]], mME[1], hel[1], -1, w[66]); 
  ixxxxx(p[perm[6]], mME[6], hel[6], -1, w[67]); 
  FFV1_1(w[66], w[8], pars->GC_1, pars->mdl_MB, pars->ZERO, w[68]); 
  FFV1_2(w[67], w[8], pars->GC_1, pars->mdl_MB, pars->ZERO, w[69]); 
  FFS4_1(w[66], w[12], pars->GC_83, pars->mdl_MB, pars->ZERO, w[70]); 
  FFS4_2(w[67], w[12], pars->GC_83, pars->mdl_MB, pars->ZERO, w[71]); 
  FFV2_3_1(w[66], w[15], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[72]);
  FFV2_3_2(w[67], w[15], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[73]);
  FFV1P0_3(w[67], w[66], pars->GC_11, pars->ZERO, pars->ZERO, w[74]); 
  FFV1_2(w[7], w[74], pars->GC_11, pars->ZERO, pars->ZERO, w[75]); 
  FFV2_1(w[66], w[2], pars->GC_100, pars->mdl_MT, pars->mdl_WT, w[76]); 
  FFV2_1(w[76], w[3], pars->GC_100, pars->mdl_MB, pars->ZERO, w[77]); 
  FFV2_2(w[67], w[3], pars->GC_100, pars->mdl_MT, pars->mdl_WT, w[78]); 
  FFV2_2(w[78], w[2], pars->GC_100, pars->mdl_MB, pars->ZERO, w[79]); 
  FFV1_1(w[5], w[74], pars->GC_11, pars->ZERO, pars->ZERO, w[80]); 
  FFV1_1(w[29], w[74], pars->GC_11, pars->ZERO, pars->ZERO, w[81]); 
  FFV1_2(w[0], w[74], pars->GC_11, pars->ZERO, pars->ZERO, w[82]); 
  FFV1_1(w[66], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[83]); 
  FFV1P0_3(w[67], w[83], pars->GC_11, pars->ZERO, pars->ZERO, w[84]); 
  FFV2_1(w[83], w[2], pars->GC_100, pars->mdl_MT, pars->mdl_WT, w[85]); 
  FFV1_2(w[67], w[40], pars->GC_11, pars->mdl_MB, pars->ZERO, w[86]); 
  FFV1_1(w[83], w[40], pars->GC_11, pars->mdl_MB, pars->ZERO, w[87]); 
  FFV1_1(w[83], w[8], pars->GC_1, pars->mdl_MB, pars->ZERO, w[88]); 
  FFS4_1(w[83], w[12], pars->GC_83, pars->mdl_MB, pars->ZERO, w[89]); 
  FFV2_3_1(w[83], w[15], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[90]);
  FFV1_2(w[67], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[91]); 
  FFV1P0_3(w[91], w[66], pars->GC_11, pars->ZERO, pars->ZERO, w[92]); 
  FFV2_2(w[91], w[3], pars->GC_100, pars->mdl_MT, pars->mdl_WT, w[93]); 
  FFV1_1(w[66], w[40], pars->GC_11, pars->mdl_MB, pars->ZERO, w[94]); 
  FFV1_2(w[91], w[40], pars->GC_11, pars->mdl_MB, pars->ZERO, w[95]); 
  FFV1_2(w[91], w[8], pars->GC_1, pars->mdl_MB, pars->ZERO, w[96]); 
  FFS4_2(w[91], w[12], pars->GC_83, pars->mdl_MB, pars->ZERO, w[97]); 
  FFV2_3_2(w[91], w[15], pars->GC_50, pars->GC_58, pars->mdl_MB, pars->ZERO,
      w[98]);
  VVV1P0_1(w[4], w[74], pars->GC_10, pars->ZERO, pars->ZERO, w[99]); 
  FFV1_1(w[76], w[4], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[100]); 
  FFV1_2(w[78], w[4], pars->GC_11, pars->mdl_MT, pars->mdl_WT, w[101]); 
  FFV1_2(w[7], w[8], pars->GC_1, pars->ZERO, pars->ZERO, w[102]); 
  FFV2_3_2(w[7], w[15], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[103]);
  FFV2_1(w[5], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[104]); 
  FFV2_2(w[7], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[105]); 
  FFV2_2(w[0], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[106]); 
  FFV2_1(w[29], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[107]); 
  FFV1_1(w[29], w[8], pars->GC_1, pars->ZERO, pars->ZERO, w[108]); 
  FFV2_3_1(w[29], w[15], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[109]);
  FFV2_2(w[106], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[110]); 
  FFV1_2(w[0], w[8], pars->GC_1, pars->ZERO, pars->ZERO, w[111]); 
  FFV1_1(w[5], w[8], pars->GC_1, pars->ZERO, pars->ZERO, w[112]); 
  FFV2_3_2(w[0], w[15], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[113]);
  FFV2_3_1(w[5], w[15], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[114]);
  FFV2_1(w[104], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[115]); 
  FFV1_2(w[106], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[116]); 
  FFV1_1(w[104], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[117]); 
  oxxxxx(p[perm[0]], mME[0], hel[0], -1, w[118]); 
  ixxxxx(p[perm[5]], mME[5], hel[5], -1, w[119]); 
  FFV1_2(w[119], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[120]); 
  FFV1P0_3(w[120], w[118], pars->GC_11, pars->ZERO, pars->ZERO, w[121]); 
  FFV1_2(w[120], w[8], pars->GC_2, pars->ZERO, pars->ZERO, w[122]); 
  FFV1_2(w[120], w[18], pars->GC_11, pars->ZERO, pars->ZERO, w[123]); 
  FFV2_5_2(w[120], w[15], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[124]);
  FFV2_1(w[118], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[125]); 
  FFV2_2(w[120], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[126]); 
  FFV1_1(w[118], w[18], pars->GC_11, pars->ZERO, pars->ZERO, w[127]); 
  FFV1_1(w[118], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[128]); 
  FFV2_2(w[119], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[129]); 
  FFV2_1(w[128], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[130]); 
  FFV1_1(w[128], w[18], pars->GC_11, pars->ZERO, pars->ZERO, w[131]); 
  FFV1P0_3(w[119], w[128], pars->GC_11, pars->ZERO, pars->ZERO, w[132]); 
  FFV1_1(w[128], w[8], pars->GC_2, pars->ZERO, pars->ZERO, w[133]); 
  FFV2_5_1(w[128], w[15], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[134]);
  FFV1_2(w[119], w[18], pars->GC_11, pars->ZERO, pars->ZERO, w[135]); 
  FFV2_2(w[129], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[136]); 
  FFV1P0_3(w[119], w[118], pars->GC_11, pars->ZERO, pars->ZERO, w[137]); 
  FFV1_2(w[1], w[137], pars->GC_11, pars->mdl_MB, pars->ZERO, w[138]); 
  FFV1_1(w[37], w[137], pars->GC_11, pars->mdl_MB, pars->ZERO, w[139]); 
  FFV1_2(w[119], w[8], pars->GC_2, pars->ZERO, pars->ZERO, w[140]); 
  FFV1_1(w[118], w[8], pars->GC_2, pars->ZERO, pars->ZERO, w[141]); 
  FFV2_5_2(w[119], w[15], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[142]);
  FFV2_5_1(w[118], w[15], pars->GC_51, pars->GC_58, pars->ZERO, pars->ZERO,
      w[143]);
  FFV2_1(w[125], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[144]); 
  FFV1_1(w[6], w[137], pars->GC_11, pars->mdl_MB, pars->ZERO, w[145]); 
  FFV1_2(w[52], w[137], pars->GC_11, pars->mdl_MB, pars->ZERO, w[146]); 
  FFV1_2(w[129], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[147]); 
  FFV1_1(w[125], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[148]); 
  VVV1P0_1(w[4], w[137], pars->GC_10, pars->ZERO, pars->ZERO, w[149]); 
  FFV1_2(w[120], w[74], pars->GC_11, pars->ZERO, pars->ZERO, w[150]); 
  FFV1_1(w[118], w[74], pars->GC_11, pars->ZERO, pars->ZERO, w[151]); 
  FFV1_1(w[128], w[74], pars->GC_11, pars->ZERO, pars->ZERO, w[152]); 
  FFV1_2(w[119], w[74], pars->GC_11, pars->ZERO, pars->ZERO, w[153]); 
  FFV1_2(w[67], w[137], pars->GC_11, pars->mdl_MB, pars->ZERO, w[154]); 
  FFV1_1(w[83], w[137], pars->GC_11, pars->mdl_MB, pars->ZERO, w[155]); 
  FFV1_1(w[66], w[137], pars->GC_11, pars->mdl_MB, pars->ZERO, w[156]); 
  FFV1_2(w[91], w[137], pars->GC_11, pars->mdl_MB, pars->ZERO, w[157]); 
  FFV1_2(w[120], w[8], pars->GC_1, pars->ZERO, pars->ZERO, w[158]); 
  FFV2_3_2(w[120], w[15], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[159]);
  FFV2_1(w[118], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[160]); 
  FFV2_2(w[120], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[161]); 
  FFV2_2(w[119], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[162]); 
  FFV2_1(w[128], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[163]); 
  FFV1_1(w[128], w[8], pars->GC_1, pars->ZERO, pars->ZERO, w[164]); 
  FFV2_3_1(w[128], w[15], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[165]);
  FFV2_2(w[162], w[2], pars->GC_100, pars->ZERO, pars->ZERO, w[166]); 
  FFV1_2(w[119], w[8], pars->GC_1, pars->ZERO, pars->ZERO, w[167]); 
  FFV1_1(w[118], w[8], pars->GC_1, pars->ZERO, pars->ZERO, w[168]); 
  FFV2_3_2(w[119], w[15], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[169]);
  FFV2_3_1(w[118], w[15], pars->GC_50, pars->GC_58, pars->ZERO, pars->ZERO,
      w[170]);
  FFV2_1(w[160], w[3], pars->GC_100, pars->ZERO, pars->ZERO, w[171]); 
  FFV1_2(w[162], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[172]); 
  FFV1_1(w[160], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[173]); 

  // Calculate all amplitudes
  // Amplitude(s) for diagram number 0
  FFV1_0(w[1], w[10], w[9], pars->GC_11, amp[0]); 
  FFV1_0(w[11], w[6], w[9], pars->GC_11, amp[1]); 
  FFV1_0(w[1], w[13], w[9], pars->GC_11, amp[2]); 
  FFV1_0(w[14], w[6], w[9], pars->GC_11, amp[3]); 
  FFV1_0(w[1], w[16], w[9], pars->GC_11, amp[4]); 
  FFV1_0(w[17], w[6], w[9], pars->GC_11, amp[5]); 
  FFV1_0(w[19], w[5], w[18], pars->GC_11, amp[6]); 
  FFV1_0(w[20], w[5], w[8], pars->GC_2, amp[7]); 
  FFV1_0(w[21], w[5], w[18], pars->GC_11, amp[8]); 
  FFV2_5_0(w[20], w[5], w[15], pars->GC_51, pars->GC_58, amp[9]); 
  FFV1_0(w[1], w[23], w[9], pars->GC_11, amp[10]); 
  FFV1_0(w[24], w[22], w[9], pars->GC_11, amp[11]); 
  FFV1_0(w[26], w[25], w[18], pars->GC_11, amp[12]); 
  FFV2_0(w[20], w[25], w[2], pars->GC_100, amp[13]); 
  FFV1_0(w[27], w[6], w[9], pars->GC_11, amp[14]); 
  FFV2_0(w[26], w[28], w[3], pars->GC_100, amp[15]); 
  FFV1_0(w[30], w[31], w[18], pars->GC_11, amp[16]); 
  FFV2_0(w[30], w[32], w[3], pars->GC_100, amp[17]); 
  FFV1_0(w[1], w[10], w[33], pars->GC_11, amp[18]); 
  FFV1_0(w[11], w[6], w[33], pars->GC_11, amp[19]); 
  FFV1_0(w[1], w[13], w[33], pars->GC_11, amp[20]); 
  FFV1_0(w[14], w[6], w[33], pars->GC_11, amp[21]); 
  FFV1_0(w[1], w[16], w[33], pars->GC_11, amp[22]); 
  FFV1_0(w[17], w[6], w[33], pars->GC_11, amp[23]); 
  FFV1_0(w[0], w[34], w[18], pars->GC_11, amp[24]); 
  FFV1_0(w[0], w[32], w[8], pars->GC_2, amp[25]); 
  FFV1_0(w[0], w[35], w[18], pars->GC_11, amp[26]); 
  FFV2_5_0(w[0], w[32], w[15], pars->GC_51, pars->GC_58, amp[27]); 
  FFV1_0(w[1], w[23], w[33], pars->GC_11, amp[28]); 
  FFV1_0(w[24], w[22], w[33], pars->GC_11, amp[29]); 
  FFV1_0(w[27], w[6], w[33], pars->GC_11, amp[30]); 
  FFV2_0(w[36], w[31], w[2], pars->GC_100, amp[31]); 
  FFV1_0(w[39], w[5], w[38], pars->GC_11, amp[32]); 
  FFV1_0(w[30], w[25], w[38], pars->GC_11, amp[33]); 
  FFV2_0(w[42], w[41], w[3], pars->GC_100, amp[34]); 
  FFV1_0(w[1], w[43], w[8], pars->GC_1, amp[35]); 
  FFV1_0(w[1], w[44], w[40], pars->GC_11, amp[36]); 
  FFS4_0(w[1], w[43], w[12], pars->GC_83, amp[37]); 
  FFV1_0(w[1], w[45], w[40], pars->GC_11, amp[38]); 
  FFV2_3_0(w[1], w[43], w[15], pars->GC_50, pars->GC_58, amp[39]); 
  FFV1_0(w[1], w[46], w[40], pars->GC_11, amp[40]); 
  FFV2_0(w[24], w[43], w[2], pars->GC_100, amp[41]); 
  FFV1_0(w[24], w[41], w[40], pars->GC_11, amp[42]); 
  FFV1_0(w[47], w[5], w[38], pars->GC_11, amp[43]); 
  FFV1_0(w[0], w[48], w[38], pars->GC_11, amp[44]); 
  FFV1_0(w[49], w[5], w[38], pars->GC_11, amp[45]); 
  FFV1_0(w[0], w[50], w[38], pars->GC_11, amp[46]); 
  FFV1_0(w[0], w[51], w[38], pars->GC_11, amp[47]); 
  FFV1_0(w[39], w[5], w[53], pars->GC_11, amp[48]); 
  FFV1_0(w[30], w[25], w[53], pars->GC_11, amp[49]); 
  FFV2_0(w[54], w[55], w[2], pars->GC_100, amp[50]); 
  FFV1_0(w[56], w[6], w[8], pars->GC_1, amp[51]); 
  FFV1_0(w[57], w[6], w[40], pars->GC_11, amp[52]); 
  FFS4_0(w[56], w[6], w[12], pars->GC_83, amp[53]); 
  FFV1_0(w[58], w[6], w[40], pars->GC_11, amp[54]); 
  FFV2_3_0(w[56], w[6], w[15], pars->GC_50, pars->GC_58, amp[55]); 
  FFV1_0(w[59], w[6], w[40], pars->GC_11, amp[56]); 
  FFV2_0(w[56], w[22], w[3], pars->GC_100, amp[57]); 
  FFV1_0(w[54], w[22], w[40], pars->GC_11, amp[58]); 
  FFV1_0(w[47], w[5], w[53], pars->GC_11, amp[59]); 
  FFV1_0(w[0], w[48], w[53], pars->GC_11, amp[60]); 
  FFV1_0(w[49], w[5], w[53], pars->GC_11, amp[61]); 
  FFV1_0(w[0], w[50], w[53], pars->GC_11, amp[62]); 
  FFV1_0(w[0], w[51], w[53], pars->GC_11, amp[63]); 
  FFV1_0(w[60], w[25], w[18], pars->GC_11, amp[64]); 
  FFV1_0(w[30], w[61], w[18], pars->GC_11, amp[65]); 
  FFV1_0(w[30], w[25], w[62], pars->GC_11, amp[66]); 
  FFV2_0(w[60], w[28], w[3], pars->GC_100, amp[67]); 
  FFV1_0(w[39], w[5], w[62], pars->GC_11, amp[68]); 
  FFV1_0(w[39], w[28], w[4], pars->GC_11, amp[69]); 
  FFV1_0(w[1], w[10], w[63], pars->GC_11, amp[70]); 
  FFV1_0(w[11], w[6], w[63], pars->GC_11, amp[71]); 
  FFV1_0(w[11], w[55], w[4], pars->GC_11, amp[72]); 
  FFV1_0(w[42], w[10], w[4], pars->GC_11, amp[73]); 
  FFV1_0(w[1], w[13], w[63], pars->GC_11, amp[74]); 
  FFV1_0(w[14], w[6], w[63], pars->GC_11, amp[75]); 
  FFV1_0(w[14], w[55], w[4], pars->GC_11, amp[76]); 
  FFV1_0(w[42], w[13], w[4], pars->GC_11, amp[77]); 
  FFV1_0(w[1], w[16], w[63], pars->GC_11, amp[78]); 
  FFV1_0(w[17], w[6], w[63], pars->GC_11, amp[79]); 
  FFV1_0(w[17], w[55], w[4], pars->GC_11, amp[80]); 
  FFV1_0(w[42], w[16], w[4], pars->GC_11, amp[81]); 
  FFV1_0(w[1], w[23], w[63], pars->GC_11, amp[82]); 
  FFV2_0(w[42], w[64], w[3], pars->GC_100, amp[83]); 
  FFV1_0(w[42], w[23], w[4], pars->GC_11, amp[84]); 
  FFV1_0(w[24], w[22], w[63], pars->GC_11, amp[85]); 
  FFV1_0(w[24], w[64], w[40], pars->GC_11, amp[86]); 
  FFV1_0(w[65], w[22], w[40], pars->GC_11, amp[87]); 
  FFV1_0(w[27], w[6], w[63], pars->GC_11, amp[88]); 
  FFV2_0(w[65], w[55], w[2], pars->GC_100, amp[89]); 
  FFV1_0(w[27], w[55], w[4], pars->GC_11, amp[90]); 
  FFV1_0(w[47], w[5], w[62], pars->GC_11, amp[91]); 
  FFV1_0(w[0], w[48], w[62], pars->GC_11, amp[92]); 
  FFV1_0(w[47], w[28], w[4], pars->GC_11, amp[93]); 
  FFV1_0(w[36], w[48], w[4], pars->GC_11, amp[94]); 
  FFV1_0(w[49], w[5], w[62], pars->GC_11, amp[95]); 
  FFV1_0(w[0], w[50], w[62], pars->GC_11, amp[96]); 
  FFV1_0(w[49], w[28], w[4], pars->GC_11, amp[97]); 
  FFV1_0(w[36], w[50], w[4], pars->GC_11, amp[98]); 
  FFV2_0(w[36], w[61], w[2], pars->GC_100, amp[99]); 
  FFV1_0(w[0], w[51], w[62], pars->GC_11, amp[100]); 
  FFV1_0(w[36], w[51], w[4], pars->GC_11, amp[101]); 
  FFV1_0(w[67], w[68], w[9], pars->GC_11, amp[102]); 
  FFV1_0(w[69], w[66], w[9], pars->GC_11, amp[103]); 
  FFV1_0(w[67], w[70], w[9], pars->GC_11, amp[104]); 
  FFV1_0(w[71], w[66], w[9], pars->GC_11, amp[105]); 
  FFV1_0(w[67], w[72], w[9], pars->GC_11, amp[106]); 
  FFV1_0(w[73], w[66], w[9], pars->GC_11, amp[107]); 
  FFV1_0(w[19], w[5], w[74], pars->GC_11, amp[108]); 
  FFV1_0(w[75], w[5], w[8], pars->GC_2, amp[109]); 
  FFV1_0(w[21], w[5], w[74], pars->GC_11, amp[110]); 
  FFV2_5_0(w[75], w[5], w[15], pars->GC_51, pars->GC_58, amp[111]); 
  FFV1_0(w[67], w[77], w[9], pars->GC_11, amp[112]); 
  FFV1_0(w[78], w[76], w[9], pars->GC_11, amp[113]); 
  FFV1_0(w[26], w[25], w[74], pars->GC_11, amp[114]); 
  FFV2_0(w[75], w[25], w[2], pars->GC_100, amp[115]); 
  FFV1_0(w[79], w[66], w[9], pars->GC_11, amp[116]); 
  FFV2_0(w[26], w[80], w[3], pars->GC_100, amp[117]); 
  FFV1_0(w[30], w[31], w[74], pars->GC_11, amp[118]); 
  FFV2_0(w[30], w[81], w[3], pars->GC_100, amp[119]); 
  FFV1_0(w[67], w[68], w[33], pars->GC_11, amp[120]); 
  FFV1_0(w[69], w[66], w[33], pars->GC_11, amp[121]); 
  FFV1_0(w[67], w[70], w[33], pars->GC_11, amp[122]); 
  FFV1_0(w[71], w[66], w[33], pars->GC_11, amp[123]); 
  FFV1_0(w[67], w[72], w[33], pars->GC_11, amp[124]); 
  FFV1_0(w[73], w[66], w[33], pars->GC_11, amp[125]); 
  FFV1_0(w[0], w[34], w[74], pars->GC_11, amp[126]); 
  FFV1_0(w[0], w[81], w[8], pars->GC_2, amp[127]); 
  FFV1_0(w[0], w[35], w[74], pars->GC_11, amp[128]); 
  FFV2_5_0(w[0], w[81], w[15], pars->GC_51, pars->GC_58, amp[129]); 
  FFV1_0(w[67], w[77], w[33], pars->GC_11, amp[130]); 
  FFV1_0(w[78], w[76], w[33], pars->GC_11, amp[131]); 
  FFV1_0(w[79], w[66], w[33], pars->GC_11, amp[132]); 
  FFV2_0(w[82], w[31], w[2], pars->GC_100, amp[133]); 
  FFV1_0(w[39], w[5], w[84], pars->GC_11, amp[134]); 
  FFV1_0(w[30], w[25], w[84], pars->GC_11, amp[135]); 
  FFV2_0(w[86], w[85], w[3], pars->GC_100, amp[136]); 
  FFV1_0(w[67], w[87], w[8], pars->GC_1, amp[137]); 
  FFV1_0(w[67], w[88], w[40], pars->GC_11, amp[138]); 
  FFS4_0(w[67], w[87], w[12], pars->GC_83, amp[139]); 
  FFV1_0(w[67], w[89], w[40], pars->GC_11, amp[140]); 
  FFV2_3_0(w[67], w[87], w[15], pars->GC_50, pars->GC_58, amp[141]); 
  FFV1_0(w[67], w[90], w[40], pars->GC_11, amp[142]); 
  FFV2_0(w[78], w[87], w[2], pars->GC_100, amp[143]); 
  FFV1_0(w[78], w[85], w[40], pars->GC_11, amp[144]); 
  FFV1_0(w[47], w[5], w[84], pars->GC_11, amp[145]); 
  FFV1_0(w[0], w[48], w[84], pars->GC_11, amp[146]); 
  FFV1_0(w[49], w[5], w[84], pars->GC_11, amp[147]); 
  FFV1_0(w[0], w[50], w[84], pars->GC_11, amp[148]); 
  FFV1_0(w[0], w[51], w[84], pars->GC_11, amp[149]); 
  FFV1_0(w[39], w[5], w[92], pars->GC_11, amp[150]); 
  FFV1_0(w[30], w[25], w[92], pars->GC_11, amp[151]); 
  FFV2_0(w[93], w[94], w[2], pars->GC_100, amp[152]); 
  FFV1_0(w[95], w[66], w[8], pars->GC_1, amp[153]); 
  FFV1_0(w[96], w[66], w[40], pars->GC_11, amp[154]); 
  FFS4_0(w[95], w[66], w[12], pars->GC_83, amp[155]); 
  FFV1_0(w[97], w[66], w[40], pars->GC_11, amp[156]); 
  FFV2_3_0(w[95], w[66], w[15], pars->GC_50, pars->GC_58, amp[157]); 
  FFV1_0(w[98], w[66], w[40], pars->GC_11, amp[158]); 
  FFV2_0(w[95], w[76], w[3], pars->GC_100, amp[159]); 
  FFV1_0(w[93], w[76], w[40], pars->GC_11, amp[160]); 
  FFV1_0(w[47], w[5], w[92], pars->GC_11, amp[161]); 
  FFV1_0(w[0], w[48], w[92], pars->GC_11, amp[162]); 
  FFV1_0(w[49], w[5], w[92], pars->GC_11, amp[163]); 
  FFV1_0(w[0], w[50], w[92], pars->GC_11, amp[164]); 
  FFV1_0(w[0], w[51], w[92], pars->GC_11, amp[165]); 
  FFV1_0(w[60], w[25], w[74], pars->GC_11, amp[166]); 
  FFV1_0(w[30], w[61], w[74], pars->GC_11, amp[167]); 
  FFV1_0(w[30], w[25], w[99], pars->GC_11, amp[168]); 
  FFV2_0(w[60], w[80], w[3], pars->GC_100, amp[169]); 
  FFV1_0(w[39], w[5], w[99], pars->GC_11, amp[170]); 
  FFV1_0(w[39], w[80], w[4], pars->GC_11, amp[171]); 
  FFV1_0(w[67], w[68], w[63], pars->GC_11, amp[172]); 
  FFV1_0(w[69], w[66], w[63], pars->GC_11, amp[173]); 
  FFV1_0(w[69], w[94], w[4], pars->GC_11, amp[174]); 
  FFV1_0(w[86], w[68], w[4], pars->GC_11, amp[175]); 
  FFV1_0(w[67], w[70], w[63], pars->GC_11, amp[176]); 
  FFV1_0(w[71], w[66], w[63], pars->GC_11, amp[177]); 
  FFV1_0(w[71], w[94], w[4], pars->GC_11, amp[178]); 
  FFV1_0(w[86], w[70], w[4], pars->GC_11, amp[179]); 
  FFV1_0(w[67], w[72], w[63], pars->GC_11, amp[180]); 
  FFV1_0(w[73], w[66], w[63], pars->GC_11, amp[181]); 
  FFV1_0(w[73], w[94], w[4], pars->GC_11, amp[182]); 
  FFV1_0(w[86], w[72], w[4], pars->GC_11, amp[183]); 
  FFV1_0(w[67], w[77], w[63], pars->GC_11, amp[184]); 
  FFV2_0(w[86], w[100], w[3], pars->GC_100, amp[185]); 
  FFV1_0(w[86], w[77], w[4], pars->GC_11, amp[186]); 
  FFV1_0(w[78], w[76], w[63], pars->GC_11, amp[187]); 
  FFV1_0(w[78], w[100], w[40], pars->GC_11, amp[188]); 
  FFV1_0(w[101], w[76], w[40], pars->GC_11, amp[189]); 
  FFV1_0(w[79], w[66], w[63], pars->GC_11, amp[190]); 
  FFV2_0(w[101], w[94], w[2], pars->GC_100, amp[191]); 
  FFV1_0(w[79], w[94], w[4], pars->GC_11, amp[192]); 
  FFV1_0(w[47], w[5], w[99], pars->GC_11, amp[193]); 
  FFV1_0(w[0], w[48], w[99], pars->GC_11, amp[194]); 
  FFV1_0(w[47], w[80], w[4], pars->GC_11, amp[195]); 
  FFV1_0(w[82], w[48], w[4], pars->GC_11, amp[196]); 
  FFV1_0(w[49], w[5], w[99], pars->GC_11, amp[197]); 
  FFV1_0(w[0], w[50], w[99], pars->GC_11, amp[198]); 
  FFV1_0(w[49], w[80], w[4], pars->GC_11, amp[199]); 
  FFV1_0(w[82], w[50], w[4], pars->GC_11, amp[200]); 
  FFV2_0(w[82], w[61], w[2], pars->GC_100, amp[201]); 
  FFV1_0(w[0], w[51], w[99], pars->GC_11, amp[202]); 
  FFV1_0(w[82], w[51], w[4], pars->GC_11, amp[203]); 
  FFV1_0(w[102], w[5], w[18], pars->GC_11, amp[204]); 
  FFV1_0(w[20], w[5], w[8], pars->GC_1, amp[205]); 
  FFV1_0(w[103], w[5], w[18], pars->GC_11, amp[206]); 
  FFV2_3_0(w[20], w[5], w[15], pars->GC_50, pars->GC_58, amp[207]); 
  FFV1_0(w[105], w[104], w[18], pars->GC_11, amp[208]); 
  FFV2_0(w[20], w[104], w[3], pars->GC_100, amp[209]); 
  FFV1_0(w[1], w[23], w[9], pars->GC_11, amp[210]); 
  FFV1_0(w[24], w[22], w[9], pars->GC_11, amp[211]); 
  FFV2_0(w[105], w[28], w[2], pars->GC_100, amp[212]); 
  FFV1_0(w[106], w[107], w[18], pars->GC_11, amp[213]); 
  FFV2_0(w[106], w[32], w[2], pars->GC_100, amp[214]); 
  FFV1_0(w[0], w[108], w[18], pars->GC_11, amp[215]); 
  FFV1_0(w[0], w[32], w[8], pars->GC_1, amp[216]); 
  FFV1_0(w[0], w[109], w[18], pars->GC_11, amp[217]); 
  FFV2_3_0(w[0], w[32], w[15], pars->GC_50, pars->GC_58, amp[218]); 
  FFV2_0(w[36], w[107], w[3], pars->GC_100, amp[219]); 
  FFV1_0(w[110], w[5], w[38], pars->GC_11, amp[220]); 
  FFV1_0(w[106], w[104], w[38], pars->GC_11, amp[221]); 
  FFV1_0(w[111], w[5], w[38], pars->GC_11, amp[222]); 
  FFV1_0(w[0], w[112], w[38], pars->GC_11, amp[223]); 
  FFV1_0(w[113], w[5], w[38], pars->GC_11, amp[224]); 
  FFV1_0(w[0], w[114], w[38], pars->GC_11, amp[225]); 
  FFV1_0(w[0], w[115], w[38], pars->GC_11, amp[226]); 
  FFV1_0(w[110], w[5], w[53], pars->GC_11, amp[227]); 
  FFV1_0(w[106], w[104], w[53], pars->GC_11, amp[228]); 
  FFV1_0(w[111], w[5], w[53], pars->GC_11, amp[229]); 
  FFV1_0(w[0], w[112], w[53], pars->GC_11, amp[230]); 
  FFV1_0(w[113], w[5], w[53], pars->GC_11, amp[231]); 
  FFV1_0(w[0], w[114], w[53], pars->GC_11, amp[232]); 
  FFV1_0(w[0], w[115], w[53], pars->GC_11, amp[233]); 
  FFV1_0(w[116], w[104], w[18], pars->GC_11, amp[234]); 
  FFV1_0(w[106], w[117], w[18], pars->GC_11, amp[235]); 
  FFV1_0(w[106], w[104], w[62], pars->GC_11, amp[236]); 
  FFV2_0(w[116], w[28], w[2], pars->GC_100, amp[237]); 
  FFV1_0(w[110], w[5], w[62], pars->GC_11, amp[238]); 
  FFV1_0(w[110], w[28], w[4], pars->GC_11, amp[239]); 
  FFV1_0(w[111], w[5], w[62], pars->GC_11, amp[240]); 
  FFV1_0(w[0], w[112], w[62], pars->GC_11, amp[241]); 
  FFV1_0(w[111], w[28], w[4], pars->GC_11, amp[242]); 
  FFV1_0(w[36], w[112], w[4], pars->GC_11, amp[243]); 
  FFV1_0(w[113], w[5], w[62], pars->GC_11, amp[244]); 
  FFV1_0(w[0], w[114], w[62], pars->GC_11, amp[245]); 
  FFV1_0(w[113], w[28], w[4], pars->GC_11, amp[246]); 
  FFV1_0(w[36], w[114], w[4], pars->GC_11, amp[247]); 
  FFV2_0(w[36], w[117], w[3], pars->GC_100, amp[248]); 
  FFV1_0(w[0], w[115], w[62], pars->GC_11, amp[249]); 
  FFV1_0(w[36], w[115], w[4], pars->GC_11, amp[250]); 
  FFV1_0(w[67], w[68], w[9], pars->GC_11, amp[251]); 
  FFV1_0(w[69], w[66], w[9], pars->GC_11, amp[252]); 
  FFV1_0(w[67], w[70], w[9], pars->GC_11, amp[253]); 
  FFV1_0(w[71], w[66], w[9], pars->GC_11, amp[254]); 
  FFV1_0(w[67], w[72], w[9], pars->GC_11, amp[255]); 
  FFV1_0(w[73], w[66], w[9], pars->GC_11, amp[256]); 
  FFV1_0(w[102], w[5], w[74], pars->GC_11, amp[257]); 
  FFV1_0(w[75], w[5], w[8], pars->GC_1, amp[258]); 
  FFV1_0(w[103], w[5], w[74], pars->GC_11, amp[259]); 
  FFV2_3_0(w[75], w[5], w[15], pars->GC_50, pars->GC_58, amp[260]); 
  FFV1_0(w[105], w[104], w[74], pars->GC_11, amp[261]); 
  FFV2_0(w[75], w[104], w[3], pars->GC_100, amp[262]); 
  FFV1_0(w[67], w[77], w[9], pars->GC_11, amp[263]); 
  FFV1_0(w[78], w[76], w[9], pars->GC_11, amp[264]); 
  FFV1_0(w[79], w[66], w[9], pars->GC_11, amp[265]); 
  FFV2_0(w[105], w[80], w[2], pars->GC_100, amp[266]); 
  FFV1_0(w[106], w[107], w[74], pars->GC_11, amp[267]); 
  FFV2_0(w[106], w[81], w[2], pars->GC_100, amp[268]); 
  FFV1_0(w[67], w[68], w[33], pars->GC_11, amp[269]); 
  FFV1_0(w[69], w[66], w[33], pars->GC_11, amp[270]); 
  FFV1_0(w[67], w[70], w[33], pars->GC_11, amp[271]); 
  FFV1_0(w[71], w[66], w[33], pars->GC_11, amp[272]); 
  FFV1_0(w[67], w[72], w[33], pars->GC_11, amp[273]); 
  FFV1_0(w[73], w[66], w[33], pars->GC_11, amp[274]); 
  FFV1_0(w[0], w[108], w[74], pars->GC_11, amp[275]); 
  FFV1_0(w[0], w[81], w[8], pars->GC_1, amp[276]); 
  FFV1_0(w[0], w[109], w[74], pars->GC_11, amp[277]); 
  FFV2_3_0(w[0], w[81], w[15], pars->GC_50, pars->GC_58, amp[278]); 
  FFV1_0(w[67], w[77], w[33], pars->GC_11, amp[279]); 
  FFV1_0(w[78], w[76], w[33], pars->GC_11, amp[280]); 
  FFV1_0(w[79], w[66], w[33], pars->GC_11, amp[281]); 
  FFV2_0(w[82], w[107], w[3], pars->GC_100, amp[282]); 
  FFV1_0(w[110], w[5], w[84], pars->GC_11, amp[283]); 
  FFV1_0(w[106], w[104], w[84], pars->GC_11, amp[284]); 
  FFV2_0(w[86], w[85], w[3], pars->GC_100, amp[285]); 
  FFV1_0(w[67], w[87], w[8], pars->GC_1, amp[286]); 
  FFV1_0(w[67], w[88], w[40], pars->GC_11, amp[287]); 
  FFS4_0(w[67], w[87], w[12], pars->GC_83, amp[288]); 
  FFV1_0(w[67], w[89], w[40], pars->GC_11, amp[289]); 
  FFV2_3_0(w[67], w[87], w[15], pars->GC_50, pars->GC_58, amp[290]); 
  FFV1_0(w[67], w[90], w[40], pars->GC_11, amp[291]); 
  FFV2_0(w[78], w[87], w[2], pars->GC_100, amp[292]); 
  FFV1_0(w[78], w[85], w[40], pars->GC_11, amp[293]); 
  FFV1_0(w[111], w[5], w[84], pars->GC_11, amp[294]); 
  FFV1_0(w[0], w[112], w[84], pars->GC_11, amp[295]); 
  FFV1_0(w[113], w[5], w[84], pars->GC_11, amp[296]); 
  FFV1_0(w[0], w[114], w[84], pars->GC_11, amp[297]); 
  FFV1_0(w[0], w[115], w[84], pars->GC_11, amp[298]); 
  FFV1_0(w[110], w[5], w[92], pars->GC_11, amp[299]); 
  FFV1_0(w[106], w[104], w[92], pars->GC_11, amp[300]); 
  FFV2_0(w[93], w[94], w[2], pars->GC_100, amp[301]); 
  FFV1_0(w[95], w[66], w[8], pars->GC_1, amp[302]); 
  FFV1_0(w[96], w[66], w[40], pars->GC_11, amp[303]); 
  FFS4_0(w[95], w[66], w[12], pars->GC_83, amp[304]); 
  FFV1_0(w[97], w[66], w[40], pars->GC_11, amp[305]); 
  FFV2_3_0(w[95], w[66], w[15], pars->GC_50, pars->GC_58, amp[306]); 
  FFV1_0(w[98], w[66], w[40], pars->GC_11, amp[307]); 
  FFV2_0(w[95], w[76], w[3], pars->GC_100, amp[308]); 
  FFV1_0(w[93], w[76], w[40], pars->GC_11, amp[309]); 
  FFV1_0(w[111], w[5], w[92], pars->GC_11, amp[310]); 
  FFV1_0(w[0], w[112], w[92], pars->GC_11, amp[311]); 
  FFV1_0(w[113], w[5], w[92], pars->GC_11, amp[312]); 
  FFV1_0(w[0], w[114], w[92], pars->GC_11, amp[313]); 
  FFV1_0(w[0], w[115], w[92], pars->GC_11, amp[314]); 
  FFV1_0(w[116], w[104], w[74], pars->GC_11, amp[315]); 
  FFV1_0(w[106], w[117], w[74], pars->GC_11, amp[316]); 
  FFV1_0(w[106], w[104], w[99], pars->GC_11, amp[317]); 
  FFV2_0(w[116], w[80], w[2], pars->GC_100, amp[318]); 
  FFV1_0(w[110], w[5], w[99], pars->GC_11, amp[319]); 
  FFV1_0(w[110], w[80], w[4], pars->GC_11, amp[320]); 
  FFV1_0(w[67], w[68], w[63], pars->GC_11, amp[321]); 
  FFV1_0(w[69], w[66], w[63], pars->GC_11, amp[322]); 
  FFV1_0(w[69], w[94], w[4], pars->GC_11, amp[323]); 
  FFV1_0(w[86], w[68], w[4], pars->GC_11, amp[324]); 
  FFV1_0(w[67], w[70], w[63], pars->GC_11, amp[325]); 
  FFV1_0(w[71], w[66], w[63], pars->GC_11, amp[326]); 
  FFV1_0(w[71], w[94], w[4], pars->GC_11, amp[327]); 
  FFV1_0(w[86], w[70], w[4], pars->GC_11, amp[328]); 
  FFV1_0(w[67], w[72], w[63], pars->GC_11, amp[329]); 
  FFV1_0(w[73], w[66], w[63], pars->GC_11, amp[330]); 
  FFV1_0(w[73], w[94], w[4], pars->GC_11, amp[331]); 
  FFV1_0(w[86], w[72], w[4], pars->GC_11, amp[332]); 
  FFV1_0(w[67], w[77], w[63], pars->GC_11, amp[333]); 
  FFV2_0(w[86], w[100], w[3], pars->GC_100, amp[334]); 
  FFV1_0(w[86], w[77], w[4], pars->GC_11, amp[335]); 
  FFV1_0(w[78], w[76], w[63], pars->GC_11, amp[336]); 
  FFV1_0(w[78], w[100], w[40], pars->GC_11, amp[337]); 
  FFV1_0(w[101], w[76], w[40], pars->GC_11, amp[338]); 
  FFV1_0(w[79], w[66], w[63], pars->GC_11, amp[339]); 
  FFV2_0(w[101], w[94], w[2], pars->GC_100, amp[340]); 
  FFV1_0(w[79], w[94], w[4], pars->GC_11, amp[341]); 
  FFV1_0(w[111], w[5], w[99], pars->GC_11, amp[342]); 
  FFV1_0(w[0], w[112], w[99], pars->GC_11, amp[343]); 
  FFV1_0(w[111], w[80], w[4], pars->GC_11, amp[344]); 
  FFV1_0(w[82], w[112], w[4], pars->GC_11, amp[345]); 
  FFV1_0(w[113], w[5], w[99], pars->GC_11, amp[346]); 
  FFV1_0(w[0], w[114], w[99], pars->GC_11, amp[347]); 
  FFV1_0(w[113], w[80], w[4], pars->GC_11, amp[348]); 
  FFV1_0(w[82], w[114], w[4], pars->GC_11, amp[349]); 
  FFV2_0(w[82], w[117], w[3], pars->GC_100, amp[350]); 
  FFV1_0(w[0], w[115], w[99], pars->GC_11, amp[351]); 
  FFV1_0(w[82], w[115], w[4], pars->GC_11, amp[352]); 
  FFV1_0(w[1], w[10], w[121], pars->GC_11, amp[353]); 
  FFV1_0(w[11], w[6], w[121], pars->GC_11, amp[354]); 
  FFV1_0(w[1], w[13], w[121], pars->GC_11, amp[355]); 
  FFV1_0(w[14], w[6], w[121], pars->GC_11, amp[356]); 
  FFV1_0(w[1], w[16], w[121], pars->GC_11, amp[357]); 
  FFV1_0(w[17], w[6], w[121], pars->GC_11, amp[358]); 
  FFV1_0(w[122], w[118], w[18], pars->GC_11, amp[359]); 
  FFV1_0(w[123], w[118], w[8], pars->GC_2, amp[360]); 
  FFV1_0(w[124], w[118], w[18], pars->GC_11, amp[361]); 
  FFV2_5_0(w[123], w[118], w[15], pars->GC_51, pars->GC_58, amp[362]); 
  FFV1_0(w[1], w[23], w[121], pars->GC_11, amp[363]); 
  FFV1_0(w[24], w[22], w[121], pars->GC_11, amp[364]); 
  FFV1_0(w[126], w[125], w[18], pars->GC_11, amp[365]); 
  FFV2_0(w[123], w[125], w[2], pars->GC_100, amp[366]); 
  FFV1_0(w[27], w[6], w[121], pars->GC_11, amp[367]); 
  FFV2_0(w[126], w[127], w[3], pars->GC_100, amp[368]); 
  FFV1_0(w[129], w[130], w[18], pars->GC_11, amp[369]); 
  FFV2_0(w[129], w[131], w[3], pars->GC_100, amp[370]); 
  FFV1_0(w[1], w[10], w[132], pars->GC_11, amp[371]); 
  FFV1_0(w[11], w[6], w[132], pars->GC_11, amp[372]); 
  FFV1_0(w[1], w[13], w[132], pars->GC_11, amp[373]); 
  FFV1_0(w[14], w[6], w[132], pars->GC_11, amp[374]); 
  FFV1_0(w[1], w[16], w[132], pars->GC_11, amp[375]); 
  FFV1_0(w[17], w[6], w[132], pars->GC_11, amp[376]); 
  FFV1_0(w[119], w[133], w[18], pars->GC_11, amp[377]); 
  FFV1_0(w[119], w[131], w[8], pars->GC_2, amp[378]); 
  FFV1_0(w[119], w[134], w[18], pars->GC_11, amp[379]); 
  FFV2_5_0(w[119], w[131], w[15], pars->GC_51, pars->GC_58, amp[380]); 
  FFV1_0(w[1], w[23], w[132], pars->GC_11, amp[381]); 
  FFV1_0(w[24], w[22], w[132], pars->GC_11, amp[382]); 
  FFV1_0(w[27], w[6], w[132], pars->GC_11, amp[383]); 
  FFV2_0(w[135], w[130], w[2], pars->GC_100, amp[384]); 
  FFV1_0(w[136], w[118], w[38], pars->GC_11, amp[385]); 
  FFV1_0(w[129], w[125], w[38], pars->GC_11, amp[386]); 
  FFV2_0(w[138], w[41], w[3], pars->GC_100, amp[387]); 
  FFV1_0(w[1], w[139], w[8], pars->GC_1, amp[388]); 
  FFV1_0(w[1], w[44], w[137], pars->GC_11, amp[389]); 
  FFS4_0(w[1], w[139], w[12], pars->GC_83, amp[390]); 
  FFV1_0(w[1], w[45], w[137], pars->GC_11, amp[391]); 
  FFV2_3_0(w[1], w[139], w[15], pars->GC_50, pars->GC_58, amp[392]); 
  FFV1_0(w[1], w[46], w[137], pars->GC_11, amp[393]); 
  FFV2_0(w[24], w[139], w[2], pars->GC_100, amp[394]); 
  FFV1_0(w[24], w[41], w[137], pars->GC_11, amp[395]); 
  FFV1_0(w[140], w[118], w[38], pars->GC_11, amp[396]); 
  FFV1_0(w[119], w[141], w[38], pars->GC_11, amp[397]); 
  FFV1_0(w[142], w[118], w[38], pars->GC_11, amp[398]); 
  FFV1_0(w[119], w[143], w[38], pars->GC_11, amp[399]); 
  FFV1_0(w[119], w[144], w[38], pars->GC_11, amp[400]); 
  FFV1_0(w[136], w[118], w[53], pars->GC_11, amp[401]); 
  FFV1_0(w[129], w[125], w[53], pars->GC_11, amp[402]); 
  FFV2_0(w[54], w[145], w[2], pars->GC_100, amp[403]); 
  FFV1_0(w[146], w[6], w[8], pars->GC_1, amp[404]); 
  FFV1_0(w[57], w[6], w[137], pars->GC_11, amp[405]); 
  FFS4_0(w[146], w[6], w[12], pars->GC_83, amp[406]); 
  FFV1_0(w[58], w[6], w[137], pars->GC_11, amp[407]); 
  FFV2_3_0(w[146], w[6], w[15], pars->GC_50, pars->GC_58, amp[408]); 
  FFV1_0(w[59], w[6], w[137], pars->GC_11, amp[409]); 
  FFV2_0(w[146], w[22], w[3], pars->GC_100, amp[410]); 
  FFV1_0(w[54], w[22], w[137], pars->GC_11, amp[411]); 
  FFV1_0(w[140], w[118], w[53], pars->GC_11, amp[412]); 
  FFV1_0(w[119], w[141], w[53], pars->GC_11, amp[413]); 
  FFV1_0(w[142], w[118], w[53], pars->GC_11, amp[414]); 
  FFV1_0(w[119], w[143], w[53], pars->GC_11, amp[415]); 
  FFV1_0(w[119], w[144], w[53], pars->GC_11, amp[416]); 
  FFV1_0(w[147], w[125], w[18], pars->GC_11, amp[417]); 
  FFV1_0(w[129], w[148], w[18], pars->GC_11, amp[418]); 
  FFV1_0(w[129], w[125], w[62], pars->GC_11, amp[419]); 
  FFV2_0(w[147], w[127], w[3], pars->GC_100, amp[420]); 
  FFV1_0(w[136], w[118], w[62], pars->GC_11, amp[421]); 
  FFV1_0(w[136], w[127], w[4], pars->GC_11, amp[422]); 
  FFV1_0(w[1], w[10], w[149], pars->GC_11, amp[423]); 
  FFV1_0(w[11], w[6], w[149], pars->GC_11, amp[424]); 
  FFV1_0(w[11], w[145], w[4], pars->GC_11, amp[425]); 
  FFV1_0(w[138], w[10], w[4], pars->GC_11, amp[426]); 
  FFV1_0(w[1], w[13], w[149], pars->GC_11, amp[427]); 
  FFV1_0(w[14], w[6], w[149], pars->GC_11, amp[428]); 
  FFV1_0(w[14], w[145], w[4], pars->GC_11, amp[429]); 
  FFV1_0(w[138], w[13], w[4], pars->GC_11, amp[430]); 
  FFV1_0(w[1], w[16], w[149], pars->GC_11, amp[431]); 
  FFV1_0(w[17], w[6], w[149], pars->GC_11, amp[432]); 
  FFV1_0(w[17], w[145], w[4], pars->GC_11, amp[433]); 
  FFV1_0(w[138], w[16], w[4], pars->GC_11, amp[434]); 
  FFV1_0(w[1], w[23], w[149], pars->GC_11, amp[435]); 
  FFV2_0(w[138], w[64], w[3], pars->GC_100, amp[436]); 
  FFV1_0(w[138], w[23], w[4], pars->GC_11, amp[437]); 
  FFV1_0(w[24], w[22], w[149], pars->GC_11, amp[438]); 
  FFV1_0(w[24], w[64], w[137], pars->GC_11, amp[439]); 
  FFV1_0(w[65], w[22], w[137], pars->GC_11, amp[440]); 
  FFV1_0(w[27], w[6], w[149], pars->GC_11, amp[441]); 
  FFV2_0(w[65], w[145], w[2], pars->GC_100, amp[442]); 
  FFV1_0(w[27], w[145], w[4], pars->GC_11, amp[443]); 
  FFV1_0(w[140], w[118], w[62], pars->GC_11, amp[444]); 
  FFV1_0(w[119], w[141], w[62], pars->GC_11, amp[445]); 
  FFV1_0(w[140], w[127], w[4], pars->GC_11, amp[446]); 
  FFV1_0(w[135], w[141], w[4], pars->GC_11, amp[447]); 
  FFV1_0(w[142], w[118], w[62], pars->GC_11, amp[448]); 
  FFV1_0(w[119], w[143], w[62], pars->GC_11, amp[449]); 
  FFV1_0(w[142], w[127], w[4], pars->GC_11, amp[450]); 
  FFV1_0(w[135], w[143], w[4], pars->GC_11, amp[451]); 
  FFV2_0(w[135], w[148], w[2], pars->GC_100, amp[452]); 
  FFV1_0(w[119], w[144], w[62], pars->GC_11, amp[453]); 
  FFV1_0(w[135], w[144], w[4], pars->GC_11, amp[454]); 
  FFV1_0(w[67], w[68], w[121], pars->GC_11, amp[455]); 
  FFV1_0(w[69], w[66], w[121], pars->GC_11, amp[456]); 
  FFV1_0(w[67], w[70], w[121], pars->GC_11, amp[457]); 
  FFV1_0(w[71], w[66], w[121], pars->GC_11, amp[458]); 
  FFV1_0(w[67], w[72], w[121], pars->GC_11, amp[459]); 
  FFV1_0(w[73], w[66], w[121], pars->GC_11, amp[460]); 
  FFV1_0(w[122], w[118], w[74], pars->GC_11, amp[461]); 
  FFV1_0(w[150], w[118], w[8], pars->GC_2, amp[462]); 
  FFV1_0(w[124], w[118], w[74], pars->GC_11, amp[463]); 
  FFV2_5_0(w[150], w[118], w[15], pars->GC_51, pars->GC_58, amp[464]); 
  FFV1_0(w[67], w[77], w[121], pars->GC_11, amp[465]); 
  FFV1_0(w[78], w[76], w[121], pars->GC_11, amp[466]); 
  FFV1_0(w[126], w[125], w[74], pars->GC_11, amp[467]); 
  FFV2_0(w[150], w[125], w[2], pars->GC_100, amp[468]); 
  FFV1_0(w[79], w[66], w[121], pars->GC_11, amp[469]); 
  FFV2_0(w[126], w[151], w[3], pars->GC_100, amp[470]); 
  FFV1_0(w[129], w[130], w[74], pars->GC_11, amp[471]); 
  FFV2_0(w[129], w[152], w[3], pars->GC_100, amp[472]); 
  FFV1_0(w[67], w[68], w[132], pars->GC_11, amp[473]); 
  FFV1_0(w[69], w[66], w[132], pars->GC_11, amp[474]); 
  FFV1_0(w[67], w[70], w[132], pars->GC_11, amp[475]); 
  FFV1_0(w[71], w[66], w[132], pars->GC_11, amp[476]); 
  FFV1_0(w[67], w[72], w[132], pars->GC_11, amp[477]); 
  FFV1_0(w[73], w[66], w[132], pars->GC_11, amp[478]); 
  FFV1_0(w[119], w[133], w[74], pars->GC_11, amp[479]); 
  FFV1_0(w[119], w[152], w[8], pars->GC_2, amp[480]); 
  FFV1_0(w[119], w[134], w[74], pars->GC_11, amp[481]); 
  FFV2_5_0(w[119], w[152], w[15], pars->GC_51, pars->GC_58, amp[482]); 
  FFV1_0(w[67], w[77], w[132], pars->GC_11, amp[483]); 
  FFV1_0(w[78], w[76], w[132], pars->GC_11, amp[484]); 
  FFV1_0(w[79], w[66], w[132], pars->GC_11, amp[485]); 
  FFV2_0(w[153], w[130], w[2], pars->GC_100, amp[486]); 
  FFV1_0(w[136], w[118], w[84], pars->GC_11, amp[487]); 
  FFV1_0(w[129], w[125], w[84], pars->GC_11, amp[488]); 
  FFV2_0(w[154], w[85], w[3], pars->GC_100, amp[489]); 
  FFV1_0(w[67], w[155], w[8], pars->GC_1, amp[490]); 
  FFV1_0(w[67], w[88], w[137], pars->GC_11, amp[491]); 
  FFS4_0(w[67], w[155], w[12], pars->GC_83, amp[492]); 
  FFV1_0(w[67], w[89], w[137], pars->GC_11, amp[493]); 
  FFV2_3_0(w[67], w[155], w[15], pars->GC_50, pars->GC_58, amp[494]); 
  FFV1_0(w[67], w[90], w[137], pars->GC_11, amp[495]); 
  FFV2_0(w[78], w[155], w[2], pars->GC_100, amp[496]); 
  FFV1_0(w[78], w[85], w[137], pars->GC_11, amp[497]); 
  FFV1_0(w[140], w[118], w[84], pars->GC_11, amp[498]); 
  FFV1_0(w[119], w[141], w[84], pars->GC_11, amp[499]); 
  FFV1_0(w[142], w[118], w[84], pars->GC_11, amp[500]); 
  FFV1_0(w[119], w[143], w[84], pars->GC_11, amp[501]); 
  FFV1_0(w[119], w[144], w[84], pars->GC_11, amp[502]); 
  FFV1_0(w[136], w[118], w[92], pars->GC_11, amp[503]); 
  FFV1_0(w[129], w[125], w[92], pars->GC_11, amp[504]); 
  FFV2_0(w[93], w[156], w[2], pars->GC_100, amp[505]); 
  FFV1_0(w[157], w[66], w[8], pars->GC_1, amp[506]); 
  FFV1_0(w[96], w[66], w[137], pars->GC_11, amp[507]); 
  FFS4_0(w[157], w[66], w[12], pars->GC_83, amp[508]); 
  FFV1_0(w[97], w[66], w[137], pars->GC_11, amp[509]); 
  FFV2_3_0(w[157], w[66], w[15], pars->GC_50, pars->GC_58, amp[510]); 
  FFV1_0(w[98], w[66], w[137], pars->GC_11, amp[511]); 
  FFV2_0(w[157], w[76], w[3], pars->GC_100, amp[512]); 
  FFV1_0(w[93], w[76], w[137], pars->GC_11, amp[513]); 
  FFV1_0(w[140], w[118], w[92], pars->GC_11, amp[514]); 
  FFV1_0(w[119], w[141], w[92], pars->GC_11, amp[515]); 
  FFV1_0(w[142], w[118], w[92], pars->GC_11, amp[516]); 
  FFV1_0(w[119], w[143], w[92], pars->GC_11, amp[517]); 
  FFV1_0(w[119], w[144], w[92], pars->GC_11, amp[518]); 
  FFV1_0(w[147], w[125], w[74], pars->GC_11, amp[519]); 
  FFV1_0(w[129], w[148], w[74], pars->GC_11, amp[520]); 
  FFV1_0(w[129], w[125], w[99], pars->GC_11, amp[521]); 
  FFV2_0(w[147], w[151], w[3], pars->GC_100, amp[522]); 
  FFV1_0(w[136], w[118], w[99], pars->GC_11, amp[523]); 
  FFV1_0(w[136], w[151], w[4], pars->GC_11, amp[524]); 
  FFV1_0(w[67], w[68], w[149], pars->GC_11, amp[525]); 
  FFV1_0(w[69], w[66], w[149], pars->GC_11, amp[526]); 
  FFV1_0(w[69], w[156], w[4], pars->GC_11, amp[527]); 
  FFV1_0(w[154], w[68], w[4], pars->GC_11, amp[528]); 
  FFV1_0(w[67], w[70], w[149], pars->GC_11, amp[529]); 
  FFV1_0(w[71], w[66], w[149], pars->GC_11, amp[530]); 
  FFV1_0(w[71], w[156], w[4], pars->GC_11, amp[531]); 
  FFV1_0(w[154], w[70], w[4], pars->GC_11, amp[532]); 
  FFV1_0(w[67], w[72], w[149], pars->GC_11, amp[533]); 
  FFV1_0(w[73], w[66], w[149], pars->GC_11, amp[534]); 
  FFV1_0(w[73], w[156], w[4], pars->GC_11, amp[535]); 
  FFV1_0(w[154], w[72], w[4], pars->GC_11, amp[536]); 
  FFV1_0(w[67], w[77], w[149], pars->GC_11, amp[537]); 
  FFV2_0(w[154], w[100], w[3], pars->GC_100, amp[538]); 
  FFV1_0(w[154], w[77], w[4], pars->GC_11, amp[539]); 
  FFV1_0(w[78], w[76], w[149], pars->GC_11, amp[540]); 
  FFV1_0(w[78], w[100], w[137], pars->GC_11, amp[541]); 
  FFV1_0(w[101], w[76], w[137], pars->GC_11, amp[542]); 
  FFV1_0(w[79], w[66], w[149], pars->GC_11, amp[543]); 
  FFV2_0(w[101], w[156], w[2], pars->GC_100, amp[544]); 
  FFV1_0(w[79], w[156], w[4], pars->GC_11, amp[545]); 
  FFV1_0(w[140], w[118], w[99], pars->GC_11, amp[546]); 
  FFV1_0(w[119], w[141], w[99], pars->GC_11, amp[547]); 
  FFV1_0(w[140], w[151], w[4], pars->GC_11, amp[548]); 
  FFV1_0(w[153], w[141], w[4], pars->GC_11, amp[549]); 
  FFV1_0(w[142], w[118], w[99], pars->GC_11, amp[550]); 
  FFV1_0(w[119], w[143], w[99], pars->GC_11, amp[551]); 
  FFV1_0(w[142], w[151], w[4], pars->GC_11, amp[552]); 
  FFV1_0(w[153], w[143], w[4], pars->GC_11, amp[553]); 
  FFV2_0(w[153], w[148], w[2], pars->GC_100, amp[554]); 
  FFV1_0(w[119], w[144], w[99], pars->GC_11, amp[555]); 
  FFV1_0(w[153], w[144], w[4], pars->GC_11, amp[556]); 
  FFV1_0(w[1], w[10], w[121], pars->GC_11, amp[557]); 
  FFV1_0(w[11], w[6], w[121], pars->GC_11, amp[558]); 
  FFV1_0(w[1], w[13], w[121], pars->GC_11, amp[559]); 
  FFV1_0(w[14], w[6], w[121], pars->GC_11, amp[560]); 
  FFV1_0(w[1], w[16], w[121], pars->GC_11, amp[561]); 
  FFV1_0(w[17], w[6], w[121], pars->GC_11, amp[562]); 
  FFV1_0(w[158], w[118], w[18], pars->GC_11, amp[563]); 
  FFV1_0(w[123], w[118], w[8], pars->GC_1, amp[564]); 
  FFV1_0(w[159], w[118], w[18], pars->GC_11, amp[565]); 
  FFV2_3_0(w[123], w[118], w[15], pars->GC_50, pars->GC_58, amp[566]); 
  FFV1_0(w[161], w[160], w[18], pars->GC_11, amp[567]); 
  FFV2_0(w[123], w[160], w[3], pars->GC_100, amp[568]); 
  FFV1_0(w[1], w[23], w[121], pars->GC_11, amp[569]); 
  FFV1_0(w[24], w[22], w[121], pars->GC_11, amp[570]); 
  FFV1_0(w[27], w[6], w[121], pars->GC_11, amp[571]); 
  FFV2_0(w[161], w[127], w[2], pars->GC_100, amp[572]); 
  FFV1_0(w[162], w[163], w[18], pars->GC_11, amp[573]); 
  FFV2_0(w[162], w[131], w[2], pars->GC_100, amp[574]); 
  FFV1_0(w[1], w[10], w[132], pars->GC_11, amp[575]); 
  FFV1_0(w[11], w[6], w[132], pars->GC_11, amp[576]); 
  FFV1_0(w[1], w[13], w[132], pars->GC_11, amp[577]); 
  FFV1_0(w[14], w[6], w[132], pars->GC_11, amp[578]); 
  FFV1_0(w[1], w[16], w[132], pars->GC_11, amp[579]); 
  FFV1_0(w[17], w[6], w[132], pars->GC_11, amp[580]); 
  FFV1_0(w[119], w[164], w[18], pars->GC_11, amp[581]); 
  FFV1_0(w[119], w[131], w[8], pars->GC_1, amp[582]); 
  FFV1_0(w[119], w[165], w[18], pars->GC_11, amp[583]); 
  FFV2_3_0(w[119], w[131], w[15], pars->GC_50, pars->GC_58, amp[584]); 
  FFV1_0(w[1], w[23], w[132], pars->GC_11, amp[585]); 
  FFV1_0(w[24], w[22], w[132], pars->GC_11, amp[586]); 
  FFV1_0(w[27], w[6], w[132], pars->GC_11, amp[587]); 
  FFV2_0(w[135], w[163], w[3], pars->GC_100, amp[588]); 
  FFV1_0(w[166], w[118], w[38], pars->GC_11, amp[589]); 
  FFV1_0(w[162], w[160], w[38], pars->GC_11, amp[590]); 
  FFV2_0(w[138], w[41], w[3], pars->GC_100, amp[591]); 
  FFV1_0(w[1], w[139], w[8], pars->GC_1, amp[592]); 
  FFV1_0(w[1], w[44], w[137], pars->GC_11, amp[593]); 
  FFS4_0(w[1], w[139], w[12], pars->GC_83, amp[594]); 
  FFV1_0(w[1], w[45], w[137], pars->GC_11, amp[595]); 
  FFV2_3_0(w[1], w[139], w[15], pars->GC_50, pars->GC_58, amp[596]); 
  FFV1_0(w[1], w[46], w[137], pars->GC_11, amp[597]); 
  FFV2_0(w[24], w[139], w[2], pars->GC_100, amp[598]); 
  FFV1_0(w[24], w[41], w[137], pars->GC_11, amp[599]); 
  FFV1_0(w[167], w[118], w[38], pars->GC_11, amp[600]); 
  FFV1_0(w[119], w[168], w[38], pars->GC_11, amp[601]); 
  FFV1_0(w[169], w[118], w[38], pars->GC_11, amp[602]); 
  FFV1_0(w[119], w[170], w[38], pars->GC_11, amp[603]); 
  FFV1_0(w[119], w[171], w[38], pars->GC_11, amp[604]); 
  FFV1_0(w[166], w[118], w[53], pars->GC_11, amp[605]); 
  FFV1_0(w[162], w[160], w[53], pars->GC_11, amp[606]); 
  FFV2_0(w[54], w[145], w[2], pars->GC_100, amp[607]); 
  FFV1_0(w[146], w[6], w[8], pars->GC_1, amp[608]); 
  FFV1_0(w[57], w[6], w[137], pars->GC_11, amp[609]); 
  FFS4_0(w[146], w[6], w[12], pars->GC_83, amp[610]); 
  FFV1_0(w[58], w[6], w[137], pars->GC_11, amp[611]); 
  FFV2_3_0(w[146], w[6], w[15], pars->GC_50, pars->GC_58, amp[612]); 
  FFV1_0(w[59], w[6], w[137], pars->GC_11, amp[613]); 
  FFV2_0(w[146], w[22], w[3], pars->GC_100, amp[614]); 
  FFV1_0(w[54], w[22], w[137], pars->GC_11, amp[615]); 
  FFV1_0(w[167], w[118], w[53], pars->GC_11, amp[616]); 
  FFV1_0(w[119], w[168], w[53], pars->GC_11, amp[617]); 
  FFV1_0(w[169], w[118], w[53], pars->GC_11, amp[618]); 
  FFV1_0(w[119], w[170], w[53], pars->GC_11, amp[619]); 
  FFV1_0(w[119], w[171], w[53], pars->GC_11, amp[620]); 
  FFV1_0(w[172], w[160], w[18], pars->GC_11, amp[621]); 
  FFV1_0(w[162], w[173], w[18], pars->GC_11, amp[622]); 
  FFV1_0(w[162], w[160], w[62], pars->GC_11, amp[623]); 
  FFV2_0(w[172], w[127], w[2], pars->GC_100, amp[624]); 
  FFV1_0(w[166], w[118], w[62], pars->GC_11, amp[625]); 
  FFV1_0(w[166], w[127], w[4], pars->GC_11, amp[626]); 
  FFV1_0(w[1], w[10], w[149], pars->GC_11, amp[627]); 
  FFV1_0(w[11], w[6], w[149], pars->GC_11, amp[628]); 
  FFV1_0(w[11], w[145], w[4], pars->GC_11, amp[629]); 
  FFV1_0(w[138], w[10], w[4], pars->GC_11, amp[630]); 
  FFV1_0(w[1], w[13], w[149], pars->GC_11, amp[631]); 
  FFV1_0(w[14], w[6], w[149], pars->GC_11, amp[632]); 
  FFV1_0(w[14], w[145], w[4], pars->GC_11, amp[633]); 
  FFV1_0(w[138], w[13], w[4], pars->GC_11, amp[634]); 
  FFV1_0(w[1], w[16], w[149], pars->GC_11, amp[635]); 
  FFV1_0(w[17], w[6], w[149], pars->GC_11, amp[636]); 
  FFV1_0(w[17], w[145], w[4], pars->GC_11, amp[637]); 
  FFV1_0(w[138], w[16], w[4], pars->GC_11, amp[638]); 
  FFV1_0(w[1], w[23], w[149], pars->GC_11, amp[639]); 
  FFV2_0(w[138], w[64], w[3], pars->GC_100, amp[640]); 
  FFV1_0(w[138], w[23], w[4], pars->GC_11, amp[641]); 
  FFV1_0(w[24], w[22], w[149], pars->GC_11, amp[642]); 
  FFV1_0(w[24], w[64], w[137], pars->GC_11, amp[643]); 
  FFV1_0(w[65], w[22], w[137], pars->GC_11, amp[644]); 
  FFV1_0(w[27], w[6], w[149], pars->GC_11, amp[645]); 
  FFV2_0(w[65], w[145], w[2], pars->GC_100, amp[646]); 
  FFV1_0(w[27], w[145], w[4], pars->GC_11, amp[647]); 
  FFV1_0(w[167], w[118], w[62], pars->GC_11, amp[648]); 
  FFV1_0(w[119], w[168], w[62], pars->GC_11, amp[649]); 
  FFV1_0(w[167], w[127], w[4], pars->GC_11, amp[650]); 
  FFV1_0(w[135], w[168], w[4], pars->GC_11, amp[651]); 
  FFV1_0(w[169], w[118], w[62], pars->GC_11, amp[652]); 
  FFV1_0(w[119], w[170], w[62], pars->GC_11, amp[653]); 
  FFV1_0(w[169], w[127], w[4], pars->GC_11, amp[654]); 
  FFV1_0(w[135], w[170], w[4], pars->GC_11, amp[655]); 
  FFV2_0(w[135], w[173], w[3], pars->GC_100, amp[656]); 
  FFV1_0(w[119], w[171], w[62], pars->GC_11, amp[657]); 
  FFV1_0(w[135], w[171], w[4], pars->GC_11, amp[658]); 
  FFV1_0(w[67], w[68], w[121], pars->GC_11, amp[659]); 
  FFV1_0(w[69], w[66], w[121], pars->GC_11, amp[660]); 
  FFV1_0(w[67], w[70], w[121], pars->GC_11, amp[661]); 
  FFV1_0(w[71], w[66], w[121], pars->GC_11, amp[662]); 
  FFV1_0(w[67], w[72], w[121], pars->GC_11, amp[663]); 
  FFV1_0(w[73], w[66], w[121], pars->GC_11, amp[664]); 
  FFV1_0(w[158], w[118], w[74], pars->GC_11, amp[665]); 
  FFV1_0(w[150], w[118], w[8], pars->GC_1, amp[666]); 
  FFV1_0(w[159], w[118], w[74], pars->GC_11, amp[667]); 
  FFV2_3_0(w[150], w[118], w[15], pars->GC_50, pars->GC_58, amp[668]); 
  FFV1_0(w[161], w[160], w[74], pars->GC_11, amp[669]); 
  FFV2_0(w[150], w[160], w[3], pars->GC_100, amp[670]); 
  FFV1_0(w[67], w[77], w[121], pars->GC_11, amp[671]); 
  FFV1_0(w[78], w[76], w[121], pars->GC_11, amp[672]); 
  FFV1_0(w[79], w[66], w[121], pars->GC_11, amp[673]); 
  FFV2_0(w[161], w[151], w[2], pars->GC_100, amp[674]); 
  FFV1_0(w[162], w[163], w[74], pars->GC_11, amp[675]); 
  FFV2_0(w[162], w[152], w[2], pars->GC_100, amp[676]); 
  FFV1_0(w[67], w[68], w[132], pars->GC_11, amp[677]); 
  FFV1_0(w[69], w[66], w[132], pars->GC_11, amp[678]); 
  FFV1_0(w[67], w[70], w[132], pars->GC_11, amp[679]); 
  FFV1_0(w[71], w[66], w[132], pars->GC_11, amp[680]); 
  FFV1_0(w[67], w[72], w[132], pars->GC_11, amp[681]); 
  FFV1_0(w[73], w[66], w[132], pars->GC_11, amp[682]); 
  FFV1_0(w[119], w[164], w[74], pars->GC_11, amp[683]); 
  FFV1_0(w[119], w[152], w[8], pars->GC_1, amp[684]); 
  FFV1_0(w[119], w[165], w[74], pars->GC_11, amp[685]); 
  FFV2_3_0(w[119], w[152], w[15], pars->GC_50, pars->GC_58, amp[686]); 
  FFV1_0(w[67], w[77], w[132], pars->GC_11, amp[687]); 
  FFV1_0(w[78], w[76], w[132], pars->GC_11, amp[688]); 
  FFV1_0(w[79], w[66], w[132], pars->GC_11, amp[689]); 
  FFV2_0(w[153], w[163], w[3], pars->GC_100, amp[690]); 
  FFV1_0(w[166], w[118], w[84], pars->GC_11, amp[691]); 
  FFV1_0(w[162], w[160], w[84], pars->GC_11, amp[692]); 
  FFV2_0(w[154], w[85], w[3], pars->GC_100, amp[693]); 
  FFV1_0(w[67], w[155], w[8], pars->GC_1, amp[694]); 
  FFV1_0(w[67], w[88], w[137], pars->GC_11, amp[695]); 
  FFS4_0(w[67], w[155], w[12], pars->GC_83, amp[696]); 
  FFV1_0(w[67], w[89], w[137], pars->GC_11, amp[697]); 
  FFV2_3_0(w[67], w[155], w[15], pars->GC_50, pars->GC_58, amp[698]); 
  FFV1_0(w[67], w[90], w[137], pars->GC_11, amp[699]); 
  FFV2_0(w[78], w[155], w[2], pars->GC_100, amp[700]); 
  FFV1_0(w[78], w[85], w[137], pars->GC_11, amp[701]); 
  FFV1_0(w[167], w[118], w[84], pars->GC_11, amp[702]); 
  FFV1_0(w[119], w[168], w[84], pars->GC_11, amp[703]); 
  FFV1_0(w[169], w[118], w[84], pars->GC_11, amp[704]); 
  FFV1_0(w[119], w[170], w[84], pars->GC_11, amp[705]); 
  FFV1_0(w[119], w[171], w[84], pars->GC_11, amp[706]); 
  FFV1_0(w[166], w[118], w[92], pars->GC_11, amp[707]); 
  FFV1_0(w[162], w[160], w[92], pars->GC_11, amp[708]); 
  FFV2_0(w[93], w[156], w[2], pars->GC_100, amp[709]); 
  FFV1_0(w[157], w[66], w[8], pars->GC_1, amp[710]); 
  FFV1_0(w[96], w[66], w[137], pars->GC_11, amp[711]); 
  FFS4_0(w[157], w[66], w[12], pars->GC_83, amp[712]); 
  FFV1_0(w[97], w[66], w[137], pars->GC_11, amp[713]); 
  FFV2_3_0(w[157], w[66], w[15], pars->GC_50, pars->GC_58, amp[714]); 
  FFV1_0(w[98], w[66], w[137], pars->GC_11, amp[715]); 
  FFV2_0(w[157], w[76], w[3], pars->GC_100, amp[716]); 
  FFV1_0(w[93], w[76], w[137], pars->GC_11, amp[717]); 
  FFV1_0(w[167], w[118], w[92], pars->GC_11, amp[718]); 
  FFV1_0(w[119], w[168], w[92], pars->GC_11, amp[719]); 
  FFV1_0(w[169], w[118], w[92], pars->GC_11, amp[720]); 
  FFV1_0(w[119], w[170], w[92], pars->GC_11, amp[721]); 
  FFV1_0(w[119], w[171], w[92], pars->GC_11, amp[722]); 
  FFV1_0(w[172], w[160], w[74], pars->GC_11, amp[723]); 
  FFV1_0(w[162], w[173], w[74], pars->GC_11, amp[724]); 
  FFV1_0(w[162], w[160], w[99], pars->GC_11, amp[725]); 
  FFV2_0(w[172], w[151], w[2], pars->GC_100, amp[726]); 
  FFV1_0(w[166], w[118], w[99], pars->GC_11, amp[727]); 
  FFV1_0(w[166], w[151], w[4], pars->GC_11, amp[728]); 
  FFV1_0(w[67], w[68], w[149], pars->GC_11, amp[729]); 
  FFV1_0(w[69], w[66], w[149], pars->GC_11, amp[730]); 
  FFV1_0(w[69], w[156], w[4], pars->GC_11, amp[731]); 
  FFV1_0(w[154], w[68], w[4], pars->GC_11, amp[732]); 
  FFV1_0(w[67], w[70], w[149], pars->GC_11, amp[733]); 
  FFV1_0(w[71], w[66], w[149], pars->GC_11, amp[734]); 
  FFV1_0(w[71], w[156], w[4], pars->GC_11, amp[735]); 
  FFV1_0(w[154], w[70], w[4], pars->GC_11, amp[736]); 
  FFV1_0(w[67], w[72], w[149], pars->GC_11, amp[737]); 
  FFV1_0(w[73], w[66], w[149], pars->GC_11, amp[738]); 
  FFV1_0(w[73], w[156], w[4], pars->GC_11, amp[739]); 
  FFV1_0(w[154], w[72], w[4], pars->GC_11, amp[740]); 
  FFV1_0(w[67], w[77], w[149], pars->GC_11, amp[741]); 
  FFV2_0(w[154], w[100], w[3], pars->GC_100, amp[742]); 
  FFV1_0(w[154], w[77], w[4], pars->GC_11, amp[743]); 
  FFV1_0(w[78], w[76], w[149], pars->GC_11, amp[744]); 
  FFV1_0(w[78], w[100], w[137], pars->GC_11, amp[745]); 
  FFV1_0(w[101], w[76], w[137], pars->GC_11, amp[746]); 
  FFV1_0(w[79], w[66], w[149], pars->GC_11, amp[747]); 
  FFV2_0(w[101], w[156], w[2], pars->GC_100, amp[748]); 
  FFV1_0(w[79], w[156], w[4], pars->GC_11, amp[749]); 
  FFV1_0(w[167], w[118], w[99], pars->GC_11, amp[750]); 
  FFV1_0(w[119], w[168], w[99], pars->GC_11, amp[751]); 
  FFV1_0(w[167], w[151], w[4], pars->GC_11, amp[752]); 
  FFV1_0(w[153], w[168], w[4], pars->GC_11, amp[753]); 
  FFV1_0(w[169], w[118], w[99], pars->GC_11, amp[754]); 
  FFV1_0(w[119], w[170], w[99], pars->GC_11, amp[755]); 
  FFV1_0(w[169], w[151], w[4], pars->GC_11, amp[756]); 
  FFV1_0(w[153], w[170], w[4], pars->GC_11, amp[757]); 
  FFV2_0(w[153], w[173], w[3], pars->GC_100, amp[758]); 
  FFV1_0(w[119], w[171], w[99], pars->GC_11, amp[759]); 
  FFV1_0(w[153], w[171], w[4], pars->GC_11, amp[760]); 


}
double PY8MEs_R12_P62_sm_qb_wpwmgqb::matrix_12_ub_wpwmgub() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 102;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[0] + 1./3. * amp[1] + 1./3. * amp[2] + 1./3.
      * amp[3] + 1./3. * amp[4] + 1./3. * amp[5] + 1./3. * amp[6] + 1./3. *
      amp[7] + 1./3. * amp[8] + 1./3. * amp[9] + 1./3. * amp[10] + 1./3. *
      amp[11] + 1./3. * amp[12] + 1./3. * amp[13] + 1./3. * amp[14] + 1./3. *
      amp[15] + 1./3. * amp[16] + 1./3. * amp[17] + 1./3. * amp[18] + 1./3. *
      amp[19] + 1./3. * amp[20] + 1./3. * amp[21] + 1./3. * amp[22] + 1./3. *
      amp[23] + 1./3. * amp[24] + 1./3. * amp[25] + 1./3. * amp[26] + 1./3. *
      amp[27] + 1./3. * amp[28] + 1./3. * amp[29] + 1./3. * amp[30] + 1./3. *
      amp[31] + 1./3. * amp[64] + 1./3. * amp[65] + 1./3. * amp[67] + 1./3. *
      amp[69] + 1./3. * amp[93] + 1./3. * amp[94] + 1./3. * amp[97] + 1./3. *
      amp[98] + 1./3. * amp[99] + 1./3. * amp[101]);
  jamp[1] = +1./2. * (-amp[16] - amp[17] - amp[18] - amp[19] - amp[20] -
      amp[21] - amp[22] - amp[23] - amp[24] - amp[25] - amp[26] - amp[27] -
      amp[28] - amp[29] - amp[30] - amp[31] - amp[48] - amp[49] - amp[50] -
      amp[51] - amp[52] - amp[53] - amp[54] - amp[55] - amp[56] - amp[57] -
      amp[58] - amp[59] - amp[60] - amp[61] - amp[62] - amp[63] - amp[65] +
      Complex<double> (0, 1) * amp[66] + Complex<double> (0, 1) * amp[68] -
      Complex<double> (0, 1) * amp[70] - Complex<double> (0, 1) * amp[71] -
      amp[72] - Complex<double> (0, 1) * amp[74] - Complex<double> (0, 1) *
      amp[75] - amp[76] - Complex<double> (0, 1) * amp[78] - Complex<double>
      (0, 1) * amp[79] - amp[80] - Complex<double> (0, 1) * amp[82] -
      Complex<double> (0, 1) * amp[85] - amp[87] - Complex<double> (0, 1) *
      amp[88] - amp[89] - amp[90] + Complex<double> (0, 1) * amp[91] +
      Complex<double> (0, 1) * amp[92] - amp[94] + Complex<double> (0, 1) *
      amp[95] + Complex<double> (0, 1) * amp[96] - amp[98] - amp[99] +
      Complex<double> (0, 1) * amp[100] - amp[101]);
  jamp[2] = +1./2. * (-amp[0] - amp[1] - amp[2] - amp[3] - amp[4] - amp[5] -
      amp[6] - amp[7] - amp[8] - amp[9] - amp[10] - amp[11] - amp[12] - amp[13]
      - amp[14] - amp[15] - amp[32] - amp[33] - amp[34] - amp[35] - amp[36] -
      amp[37] - amp[38] - amp[39] - amp[40] - amp[41] - amp[42] - amp[43] -
      amp[44] - amp[45] - amp[46] - amp[47] - amp[64] - Complex<double> (0, 1)
      * amp[66] - amp[67] - Complex<double> (0, 1) * amp[68] - amp[69] +
      Complex<double> (0, 1) * amp[70] + Complex<double> (0, 1) * amp[71] -
      amp[73] + Complex<double> (0, 1) * amp[74] + Complex<double> (0, 1) *
      amp[75] - amp[77] + Complex<double> (0, 1) * amp[78] + Complex<double>
      (0, 1) * amp[79] - amp[81] + Complex<double> (0, 1) * amp[82] - amp[83] -
      amp[84] + Complex<double> (0, 1) * amp[85] - amp[86] + Complex<double>
      (0, 1) * amp[88] - Complex<double> (0, 1) * amp[91] - Complex<double> (0,
      1) * amp[92] - amp[93] - Complex<double> (0, 1) * amp[95] -
      Complex<double> (0, 1) * amp[96] - amp[97] - Complex<double> (0, 1) *
      amp[100]);
  jamp[3] = +1./2. * (+1./3. * amp[32] + 1./3. * amp[33] + 1./3. * amp[34] +
      1./3. * amp[35] + 1./3. * amp[36] + 1./3. * amp[37] + 1./3. * amp[38] +
      1./3. * amp[39] + 1./3. * amp[40] + 1./3. * amp[41] + 1./3. * amp[42] +
      1./3. * amp[43] + 1./3. * amp[44] + 1./3. * amp[45] + 1./3. * amp[46] +
      1./3. * amp[47] + 1./3. * amp[48] + 1./3. * amp[49] + 1./3. * amp[50] +
      1./3. * amp[51] + 1./3. * amp[52] + 1./3. * amp[53] + 1./3. * amp[54] +
      1./3. * amp[55] + 1./3. * amp[56] + 1./3. * amp[57] + 1./3. * amp[58] +
      1./3. * amp[59] + 1./3. * amp[60] + 1./3. * amp[61] + 1./3. * amp[62] +
      1./3. * amp[63] + 1./3. * amp[72] + 1./3. * amp[73] + 1./3. * amp[76] +
      1./3. * amp[77] + 1./3. * amp[80] + 1./3. * amp[81] + 1./3. * amp[83] +
      1./3. * amp[84] + 1./3. * amp[86] + 1./3. * amp[87] + 1./3. * amp[89] +
      1./3. * amp[90]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[0][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P62_sm_qb_wpwmgqb::matrix_12_ubx_wpwmgubx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 102;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[118] + amp[119] + amp[120] + amp[121] + amp[122] +
      amp[123] + amp[124] + amp[125] + amp[126] + amp[127] + amp[128] +
      amp[129] + amp[130] + amp[131] + amp[132] + amp[133] + amp[150] +
      amp[151] + amp[152] + amp[153] + amp[154] + amp[155] + amp[156] +
      amp[157] + amp[158] + amp[159] + amp[160] + amp[161] + amp[162] +
      amp[163] + amp[164] + amp[165] + amp[167] - Complex<double> (0, 1) *
      amp[168] - Complex<double> (0, 1) * amp[170] + Complex<double> (0, 1) *
      amp[172] + Complex<double> (0, 1) * amp[173] + amp[174] + Complex<double>
      (0, 1) * amp[176] + Complex<double> (0, 1) * amp[177] + amp[178] +
      Complex<double> (0, 1) * amp[180] + Complex<double> (0, 1) * amp[181] +
      amp[182] + Complex<double> (0, 1) * amp[184] + Complex<double> (0, 1) *
      amp[187] + amp[189] + Complex<double> (0, 1) * amp[190] + amp[191] +
      amp[192] - Complex<double> (0, 1) * amp[193] - Complex<double> (0, 1) *
      amp[194] + amp[196] - Complex<double> (0, 1) * amp[197] - Complex<double>
      (0, 1) * amp[198] + amp[200] + amp[201] - Complex<double> (0, 1) *
      amp[202] + amp[203]);
  jamp[1] = +1./2. * (-1./3. * amp[102] - 1./3. * amp[103] - 1./3. * amp[104] -
      1./3. * amp[105] - 1./3. * amp[106] - 1./3. * amp[107] - 1./3. * amp[108]
      - 1./3. * amp[109] - 1./3. * amp[110] - 1./3. * amp[111] - 1./3. *
      amp[112] - 1./3. * amp[113] - 1./3. * amp[114] - 1./3. * amp[115] - 1./3.
      * amp[116] - 1./3. * amp[117] - 1./3. * amp[118] - 1./3. * amp[119] -
      1./3. * amp[120] - 1./3. * amp[121] - 1./3. * amp[122] - 1./3. * amp[123]
      - 1./3. * amp[124] - 1./3. * amp[125] - 1./3. * amp[126] - 1./3. *
      amp[127] - 1./3. * amp[128] - 1./3. * amp[129] - 1./3. * amp[130] - 1./3.
      * amp[131] - 1./3. * amp[132] - 1./3. * amp[133] - 1./3. * amp[166] -
      1./3. * amp[167] - 1./3. * amp[169] - 1./3. * amp[171] - 1./3. * amp[195]
      - 1./3. * amp[196] - 1./3. * amp[199] - 1./3. * amp[200] - 1./3. *
      amp[201] - 1./3. * amp[203]);
  jamp[2] = +1./2. * (+amp[102] + amp[103] + amp[104] + amp[105] + amp[106] +
      amp[107] + amp[108] + amp[109] + amp[110] + amp[111] + amp[112] +
      amp[113] + amp[114] + amp[115] + amp[116] + amp[117] + amp[134] +
      amp[135] + amp[136] + amp[137] + amp[138] + amp[139] + amp[140] +
      amp[141] + amp[142] + amp[143] + amp[144] + amp[145] + amp[146] +
      amp[147] + amp[148] + amp[149] + amp[166] + Complex<double> (0, 1) *
      amp[168] + amp[169] + Complex<double> (0, 1) * amp[170] + amp[171] -
      Complex<double> (0, 1) * amp[172] - Complex<double> (0, 1) * amp[173] +
      amp[175] - Complex<double> (0, 1) * amp[176] - Complex<double> (0, 1) *
      amp[177] + amp[179] - Complex<double> (0, 1) * amp[180] - Complex<double>
      (0, 1) * amp[181] + amp[183] - Complex<double> (0, 1) * amp[184] +
      amp[185] + amp[186] - Complex<double> (0, 1) * amp[187] + amp[188] -
      Complex<double> (0, 1) * amp[190] + Complex<double> (0, 1) * amp[193] +
      Complex<double> (0, 1) * amp[194] + amp[195] + Complex<double> (0, 1) *
      amp[197] + Complex<double> (0, 1) * amp[198] + amp[199] + Complex<double>
      (0, 1) * amp[202]);
  jamp[3] = +1./2. * (-1./3. * amp[134] - 1./3. * amp[135] - 1./3. * amp[136] -
      1./3. * amp[137] - 1./3. * amp[138] - 1./3. * amp[139] - 1./3. * amp[140]
      - 1./3. * amp[141] - 1./3. * amp[142] - 1./3. * amp[143] - 1./3. *
      amp[144] - 1./3. * amp[145] - 1./3. * amp[146] - 1./3. * amp[147] - 1./3.
      * amp[148] - 1./3. * amp[149] - 1./3. * amp[150] - 1./3. * amp[151] -
      1./3. * amp[152] - 1./3. * amp[153] - 1./3. * amp[154] - 1./3. * amp[155]
      - 1./3. * amp[156] - 1./3. * amp[157] - 1./3. * amp[158] - 1./3. *
      amp[159] - 1./3. * amp[160] - 1./3. * amp[161] - 1./3. * amp[162] - 1./3.
      * amp[163] - 1./3. * amp[164] - 1./3. * amp[165] - 1./3. * amp[174] -
      1./3. * amp[175] - 1./3. * amp[178] - 1./3. * amp[179] - 1./3. * amp[182]
      - 1./3. * amp[183] - 1./3. * amp[185] - 1./3. * amp[186] - 1./3. *
      amp[188] - 1./3. * amp[189] - 1./3. * amp[191] - 1./3. * amp[192]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[1][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P62_sm_qb_wpwmgqb::matrix_12_db_wpwmgdb() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 102;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[0] + 1./3. * amp[1] + 1./3. * amp[2] + 1./3.
      * amp[3] + 1./3. * amp[4] + 1./3. * amp[5] + 1./3. * amp[204] + 1./3. *
      amp[205] + 1./3. * amp[206] + 1./3. * amp[207] + 1./3. * amp[208] + 1./3.
      * amp[209] + 1./3. * amp[210] + 1./3. * amp[211] + 1./3. * amp[14] +
      1./3. * amp[212] + 1./3. * amp[213] + 1./3. * amp[214] + 1./3. * amp[18]
      + 1./3. * amp[19] + 1./3. * amp[20] + 1./3. * amp[21] + 1./3. * amp[22] +
      1./3. * amp[23] + 1./3. * amp[215] + 1./3. * amp[216] + 1./3. * amp[217]
      + 1./3. * amp[218] + 1./3. * amp[28] + 1./3. * amp[29] + 1./3. * amp[30]
      + 1./3. * amp[219] + 1./3. * amp[234] + 1./3. * amp[235] + 1./3. *
      amp[237] + 1./3. * amp[239] + 1./3. * amp[242] + 1./3. * amp[243] + 1./3.
      * amp[246] + 1./3. * amp[247] + 1./3. * amp[248] + 1./3. * amp[250]);
  jamp[1] = +1./2. * (-amp[213] - amp[214] - amp[18] - amp[19] - amp[20] -
      amp[21] - amp[22] - amp[23] - amp[215] - amp[216] - amp[217] - amp[218] -
      amp[28] - amp[29] - amp[30] - amp[219] - amp[227] - amp[228] - amp[50] -
      amp[51] - amp[52] - amp[53] - amp[54] - amp[55] - amp[56] - amp[57] -
      amp[58] - amp[229] - amp[230] - amp[231] - amp[232] - amp[233] - amp[235]
      + Complex<double> (0, 1) * amp[236] + Complex<double> (0, 1) * amp[238] -
      Complex<double> (0, 1) * amp[70] - Complex<double> (0, 1) * amp[71] -
      amp[72] - Complex<double> (0, 1) * amp[74] - Complex<double> (0, 1) *
      amp[75] - amp[76] - Complex<double> (0, 1) * amp[78] - Complex<double>
      (0, 1) * amp[79] - amp[80] - Complex<double> (0, 1) * amp[82] -
      Complex<double> (0, 1) * amp[85] - amp[87] - Complex<double> (0, 1) *
      amp[88] - amp[89] - amp[90] + Complex<double> (0, 1) * amp[240] +
      Complex<double> (0, 1) * amp[241] - amp[243] + Complex<double> (0, 1) *
      amp[244] + Complex<double> (0, 1) * amp[245] - amp[247] - amp[248] +
      Complex<double> (0, 1) * amp[249] - amp[250]);
  jamp[2] = +1./2. * (-amp[0] - amp[1] - amp[2] - amp[3] - amp[4] - amp[5] -
      amp[204] - amp[205] - amp[206] - amp[207] - amp[208] - amp[209] -
      amp[210] - amp[211] - amp[14] - amp[212] - amp[220] - amp[221] - amp[34]
      - amp[35] - amp[36] - amp[37] - amp[38] - amp[39] - amp[40] - amp[41] -
      amp[42] - amp[222] - amp[223] - amp[224] - amp[225] - amp[226] - amp[234]
      - Complex<double> (0, 1) * amp[236] - amp[237] - Complex<double> (0, 1) *
      amp[238] - amp[239] + Complex<double> (0, 1) * amp[70] + Complex<double>
      (0, 1) * amp[71] - amp[73] + Complex<double> (0, 1) * amp[74] +
      Complex<double> (0, 1) * amp[75] - amp[77] + Complex<double> (0, 1) *
      amp[78] + Complex<double> (0, 1) * amp[79] - amp[81] + Complex<double>
      (0, 1) * amp[82] - amp[83] - amp[84] + Complex<double> (0, 1) * amp[85] -
      amp[86] + Complex<double> (0, 1) * amp[88] - Complex<double> (0, 1) *
      amp[240] - Complex<double> (0, 1) * amp[241] - amp[242] - Complex<double>
      (0, 1) * amp[244] - Complex<double> (0, 1) * amp[245] - amp[246] -
      Complex<double> (0, 1) * amp[249]);
  jamp[3] = +1./2. * (+1./3. * amp[220] + 1./3. * amp[221] + 1./3. * amp[34] +
      1./3. * amp[35] + 1./3. * amp[36] + 1./3. * amp[37] + 1./3. * amp[38] +
      1./3. * amp[39] + 1./3. * amp[40] + 1./3. * amp[41] + 1./3. * amp[42] +
      1./3. * amp[222] + 1./3. * amp[223] + 1./3. * amp[224] + 1./3. * amp[225]
      + 1./3. * amp[226] + 1./3. * amp[227] + 1./3. * amp[228] + 1./3. *
      amp[50] + 1./3. * amp[51] + 1./3. * amp[52] + 1./3. * amp[53] + 1./3. *
      amp[54] + 1./3. * amp[55] + 1./3. * amp[56] + 1./3. * amp[57] + 1./3. *
      amp[58] + 1./3. * amp[229] + 1./3. * amp[230] + 1./3. * amp[231] + 1./3.
      * amp[232] + 1./3. * amp[233] + 1./3. * amp[72] + 1./3. * amp[73] + 1./3.
      * amp[76] + 1./3. * amp[77] + 1./3. * amp[80] + 1./3. * amp[81] + 1./3. *
      amp[83] + 1./3. * amp[84] + 1./3. * amp[86] + 1./3. * amp[87] + 1./3. *
      amp[89] + 1./3. * amp[90]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[2][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P62_sm_qb_wpwmgqb::matrix_12_dbx_wpwmgdbx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 102;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[267] + amp[268] + amp[269] + amp[270] + amp[271] +
      amp[272] + amp[273] + amp[274] + amp[275] + amp[276] + amp[277] +
      amp[278] + amp[279] + amp[280] + amp[281] + amp[282] + amp[299] +
      amp[300] + amp[301] + amp[302] + amp[303] + amp[304] + amp[305] +
      amp[306] + amp[307] + amp[308] + amp[309] + amp[310] + amp[311] +
      amp[312] + amp[313] + amp[314] + amp[316] - Complex<double> (0, 1) *
      amp[317] - Complex<double> (0, 1) * amp[319] + Complex<double> (0, 1) *
      amp[321] + Complex<double> (0, 1) * amp[322] + amp[323] + Complex<double>
      (0, 1) * amp[325] + Complex<double> (0, 1) * amp[326] + amp[327] +
      Complex<double> (0, 1) * amp[329] + Complex<double> (0, 1) * amp[330] +
      amp[331] + Complex<double> (0, 1) * amp[333] + Complex<double> (0, 1) *
      amp[336] + amp[338] + Complex<double> (0, 1) * amp[339] + amp[340] +
      amp[341] - Complex<double> (0, 1) * amp[342] - Complex<double> (0, 1) *
      amp[343] + amp[345] - Complex<double> (0, 1) * amp[346] - Complex<double>
      (0, 1) * amp[347] + amp[349] + amp[350] - Complex<double> (0, 1) *
      amp[351] + amp[352]);
  jamp[1] = +1./2. * (-1./3. * amp[251] - 1./3. * amp[252] - 1./3. * amp[253] -
      1./3. * amp[254] - 1./3. * amp[255] - 1./3. * amp[256] - 1./3. * amp[257]
      - 1./3. * amp[258] - 1./3. * amp[259] - 1./3. * amp[260] - 1./3. *
      amp[261] - 1./3. * amp[262] - 1./3. * amp[263] - 1./3. * amp[264] - 1./3.
      * amp[265] - 1./3. * amp[266] - 1./3. * amp[267] - 1./3. * amp[268] -
      1./3. * amp[269] - 1./3. * amp[270] - 1./3. * amp[271] - 1./3. * amp[272]
      - 1./3. * amp[273] - 1./3. * amp[274] - 1./3. * amp[275] - 1./3. *
      amp[276] - 1./3. * amp[277] - 1./3. * amp[278] - 1./3. * amp[279] - 1./3.
      * amp[280] - 1./3. * amp[281] - 1./3. * amp[282] - 1./3. * amp[315] -
      1./3. * amp[316] - 1./3. * amp[318] - 1./3. * amp[320] - 1./3. * amp[344]
      - 1./3. * amp[345] - 1./3. * amp[348] - 1./3. * amp[349] - 1./3. *
      amp[350] - 1./3. * amp[352]);
  jamp[2] = +1./2. * (+amp[251] + amp[252] + amp[253] + amp[254] + amp[255] +
      amp[256] + amp[257] + amp[258] + amp[259] + amp[260] + amp[261] +
      amp[262] + amp[263] + amp[264] + amp[265] + amp[266] + amp[283] +
      amp[284] + amp[285] + amp[286] + amp[287] + amp[288] + amp[289] +
      amp[290] + amp[291] + amp[292] + amp[293] + amp[294] + amp[295] +
      amp[296] + amp[297] + amp[298] + amp[315] + Complex<double> (0, 1) *
      amp[317] + amp[318] + Complex<double> (0, 1) * amp[319] + amp[320] -
      Complex<double> (0, 1) * amp[321] - Complex<double> (0, 1) * amp[322] +
      amp[324] - Complex<double> (0, 1) * amp[325] - Complex<double> (0, 1) *
      amp[326] + amp[328] - Complex<double> (0, 1) * amp[329] - Complex<double>
      (0, 1) * amp[330] + amp[332] - Complex<double> (0, 1) * amp[333] +
      amp[334] + amp[335] - Complex<double> (0, 1) * amp[336] + amp[337] -
      Complex<double> (0, 1) * amp[339] + Complex<double> (0, 1) * amp[342] +
      Complex<double> (0, 1) * amp[343] + amp[344] + Complex<double> (0, 1) *
      amp[346] + Complex<double> (0, 1) * amp[347] + amp[348] + Complex<double>
      (0, 1) * amp[351]);
  jamp[3] = +1./2. * (-1./3. * amp[283] - 1./3. * amp[284] - 1./3. * amp[285] -
      1./3. * amp[286] - 1./3. * amp[287] - 1./3. * amp[288] - 1./3. * amp[289]
      - 1./3. * amp[290] - 1./3. * amp[291] - 1./3. * amp[292] - 1./3. *
      amp[293] - 1./3. * amp[294] - 1./3. * amp[295] - 1./3. * amp[296] - 1./3.
      * amp[297] - 1./3. * amp[298] - 1./3. * amp[299] - 1./3. * amp[300] -
      1./3. * amp[301] - 1./3. * amp[302] - 1./3. * amp[303] - 1./3. * amp[304]
      - 1./3. * amp[305] - 1./3. * amp[306] - 1./3. * amp[307] - 1./3. *
      amp[308] - 1./3. * amp[309] - 1./3. * amp[310] - 1./3. * amp[311] - 1./3.
      * amp[312] - 1./3. * amp[313] - 1./3. * amp[314] - 1./3. * amp[323] -
      1./3. * amp[324] - 1./3. * amp[327] - 1./3. * amp[328] - 1./3. * amp[331]
      - 1./3. * amp[332] - 1./3. * amp[334] - 1./3. * amp[335] - 1./3. *
      amp[337] - 1./3. * amp[338] - 1./3. * amp[340] - 1./3. * amp[341]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[3][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P62_sm_qb_wpwmgqb::matrix_12_uxb_wpwmguxb() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 102;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[353] + amp[354] + amp[355] + amp[356] + amp[357] +
      amp[358] + amp[359] + amp[360] + amp[361] + amp[362] + amp[363] +
      amp[364] + amp[365] + amp[366] + amp[367] + amp[368] + amp[385] +
      amp[386] + amp[387] + amp[388] + amp[389] + amp[390] + amp[391] +
      amp[392] + amp[393] + amp[394] + amp[395] + amp[396] + amp[397] +
      amp[398] + amp[399] + amp[400] + amp[417] + Complex<double> (0, 1) *
      amp[419] + amp[420] + Complex<double> (0, 1) * amp[421] + amp[422] -
      Complex<double> (0, 1) * amp[423] - Complex<double> (0, 1) * amp[424] +
      amp[426] - Complex<double> (0, 1) * amp[427] - Complex<double> (0, 1) *
      amp[428] + amp[430] - Complex<double> (0, 1) * amp[431] - Complex<double>
      (0, 1) * amp[432] + amp[434] - Complex<double> (0, 1) * amp[435] +
      amp[436] + amp[437] - Complex<double> (0, 1) * amp[438] + amp[439] -
      Complex<double> (0, 1) * amp[441] + Complex<double> (0, 1) * amp[444] +
      Complex<double> (0, 1) * amp[445] + amp[446] + Complex<double> (0, 1) *
      amp[448] + Complex<double> (0, 1) * amp[449] + amp[450] + Complex<double>
      (0, 1) * amp[453]);
  jamp[1] = +1./2. * (-1./3. * amp[385] - 1./3. * amp[386] - 1./3. * amp[387] -
      1./3. * amp[388] - 1./3. * amp[389] - 1./3. * amp[390] - 1./3. * amp[391]
      - 1./3. * amp[392] - 1./3. * amp[393] - 1./3. * amp[394] - 1./3. *
      amp[395] - 1./3. * amp[396] - 1./3. * amp[397] - 1./3. * amp[398] - 1./3.
      * amp[399] - 1./3. * amp[400] - 1./3. * amp[401] - 1./3. * amp[402] -
      1./3. * amp[403] - 1./3. * amp[404] - 1./3. * amp[405] - 1./3. * amp[406]
      - 1./3. * amp[407] - 1./3. * amp[408] - 1./3. * amp[409] - 1./3. *
      amp[410] - 1./3. * amp[411] - 1./3. * amp[412] - 1./3. * amp[413] - 1./3.
      * amp[414] - 1./3. * amp[415] - 1./3. * amp[416] - 1./3. * amp[425] -
      1./3. * amp[426] - 1./3. * amp[429] - 1./3. * amp[430] - 1./3. * amp[433]
      - 1./3. * amp[434] - 1./3. * amp[436] - 1./3. * amp[437] - 1./3. *
      amp[439] - 1./3. * amp[440] - 1./3. * amp[442] - 1./3. * amp[443]);
  jamp[2] = +1./2. * (+amp[369] + amp[370] + amp[371] + amp[372] + amp[373] +
      amp[374] + amp[375] + amp[376] + amp[377] + amp[378] + amp[379] +
      amp[380] + amp[381] + amp[382] + amp[383] + amp[384] + amp[401] +
      amp[402] + amp[403] + amp[404] + amp[405] + amp[406] + amp[407] +
      amp[408] + amp[409] + amp[410] + amp[411] + amp[412] + amp[413] +
      amp[414] + amp[415] + amp[416] + amp[418] - Complex<double> (0, 1) *
      amp[419] - Complex<double> (0, 1) * amp[421] + Complex<double> (0, 1) *
      amp[423] + Complex<double> (0, 1) * amp[424] + amp[425] + Complex<double>
      (0, 1) * amp[427] + Complex<double> (0, 1) * amp[428] + amp[429] +
      Complex<double> (0, 1) * amp[431] + Complex<double> (0, 1) * amp[432] +
      amp[433] + Complex<double> (0, 1) * amp[435] + Complex<double> (0, 1) *
      amp[438] + amp[440] + Complex<double> (0, 1) * amp[441] + amp[442] +
      amp[443] - Complex<double> (0, 1) * amp[444] - Complex<double> (0, 1) *
      amp[445] + amp[447] - Complex<double> (0, 1) * amp[448] - Complex<double>
      (0, 1) * amp[449] + amp[451] + amp[452] - Complex<double> (0, 1) *
      amp[453] + amp[454]);
  jamp[3] = +1./2. * (-1./3. * amp[353] - 1./3. * amp[354] - 1./3. * amp[355] -
      1./3. * amp[356] - 1./3. * amp[357] - 1./3. * amp[358] - 1./3. * amp[359]
      - 1./3. * amp[360] - 1./3. * amp[361] - 1./3. * amp[362] - 1./3. *
      amp[363] - 1./3. * amp[364] - 1./3. * amp[365] - 1./3. * amp[366] - 1./3.
      * amp[367] - 1./3. * amp[368] - 1./3. * amp[369] - 1./3. * amp[370] -
      1./3. * amp[371] - 1./3. * amp[372] - 1./3. * amp[373] - 1./3. * amp[374]
      - 1./3. * amp[375] - 1./3. * amp[376] - 1./3. * amp[377] - 1./3. *
      amp[378] - 1./3. * amp[379] - 1./3. * amp[380] - 1./3. * amp[381] - 1./3.
      * amp[382] - 1./3. * amp[383] - 1./3. * amp[384] - 1./3. * amp[417] -
      1./3. * amp[418] - 1./3. * amp[420] - 1./3. * amp[422] - 1./3. * amp[446]
      - 1./3. * amp[447] - 1./3. * amp[450] - 1./3. * amp[451] - 1./3. *
      amp[452] - 1./3. * amp[454]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[4][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P62_sm_qb_wpwmgqb::matrix_12_uxbx_wpwmguxbx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 102;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[487] + 1./3. * amp[488] + 1./3. * amp[489] +
      1./3. * amp[490] + 1./3. * amp[491] + 1./3. * amp[492] + 1./3. * amp[493]
      + 1./3. * amp[494] + 1./3. * amp[495] + 1./3. * amp[496] + 1./3. *
      amp[497] + 1./3. * amp[498] + 1./3. * amp[499] + 1./3. * amp[500] + 1./3.
      * amp[501] + 1./3. * amp[502] + 1./3. * amp[503] + 1./3. * amp[504] +
      1./3. * amp[505] + 1./3. * amp[506] + 1./3. * amp[507] + 1./3. * amp[508]
      + 1./3. * amp[509] + 1./3. * amp[510] + 1./3. * amp[511] + 1./3. *
      amp[512] + 1./3. * amp[513] + 1./3. * amp[514] + 1./3. * amp[515] + 1./3.
      * amp[516] + 1./3. * amp[517] + 1./3. * amp[518] + 1./3. * amp[527] +
      1./3. * amp[528] + 1./3. * amp[531] + 1./3. * amp[532] + 1./3. * amp[535]
      + 1./3. * amp[536] + 1./3. * amp[538] + 1./3. * amp[539] + 1./3. *
      amp[541] + 1./3. * amp[542] + 1./3. * amp[544] + 1./3. * amp[545]);
  jamp[1] = +1./2. * (-amp[455] - amp[456] - amp[457] - amp[458] - amp[459] -
      amp[460] - amp[461] - amp[462] - amp[463] - amp[464] - amp[465] -
      amp[466] - amp[467] - amp[468] - amp[469] - amp[470] - amp[487] -
      amp[488] - amp[489] - amp[490] - amp[491] - amp[492] - amp[493] -
      amp[494] - amp[495] - amp[496] - amp[497] - amp[498] - amp[499] -
      amp[500] - amp[501] - amp[502] - amp[519] - Complex<double> (0, 1) *
      amp[521] - amp[522] - Complex<double> (0, 1) * amp[523] - amp[524] +
      Complex<double> (0, 1) * amp[525] + Complex<double> (0, 1) * amp[526] -
      amp[528] + Complex<double> (0, 1) * amp[529] + Complex<double> (0, 1) *
      amp[530] - amp[532] + Complex<double> (0, 1) * amp[533] + Complex<double>
      (0, 1) * amp[534] - amp[536] + Complex<double> (0, 1) * amp[537] -
      amp[538] - amp[539] + Complex<double> (0, 1) * amp[540] - amp[541] +
      Complex<double> (0, 1) * amp[543] - Complex<double> (0, 1) * amp[546] -
      Complex<double> (0, 1) * amp[547] - amp[548] - Complex<double> (0, 1) *
      amp[550] - Complex<double> (0, 1) * amp[551] - amp[552] - Complex<double>
      (0, 1) * amp[555]);
  jamp[2] = +1./2. * (-amp[471] - amp[472] - amp[473] - amp[474] - amp[475] -
      amp[476] - amp[477] - amp[478] - amp[479] - amp[480] - amp[481] -
      amp[482] - amp[483] - amp[484] - amp[485] - amp[486] - amp[503] -
      amp[504] - amp[505] - amp[506] - amp[507] - amp[508] - amp[509] -
      amp[510] - amp[511] - amp[512] - amp[513] - amp[514] - amp[515] -
      amp[516] - amp[517] - amp[518] - amp[520] + Complex<double> (0, 1) *
      amp[521] + Complex<double> (0, 1) * amp[523] - Complex<double> (0, 1) *
      amp[525] - Complex<double> (0, 1) * amp[526] - amp[527] - Complex<double>
      (0, 1) * amp[529] - Complex<double> (0, 1) * amp[530] - amp[531] -
      Complex<double> (0, 1) * amp[533] - Complex<double> (0, 1) * amp[534] -
      amp[535] - Complex<double> (0, 1) * amp[537] - Complex<double> (0, 1) *
      amp[540] - amp[542] - Complex<double> (0, 1) * amp[543] - amp[544] -
      amp[545] + Complex<double> (0, 1) * amp[546] + Complex<double> (0, 1) *
      amp[547] - amp[549] + Complex<double> (0, 1) * amp[550] + Complex<double>
      (0, 1) * amp[551] - amp[553] - amp[554] + Complex<double> (0, 1) *
      amp[555] - amp[556]);
  jamp[3] = +1./2. * (+1./3. * amp[455] + 1./3. * amp[456] + 1./3. * amp[457] +
      1./3. * amp[458] + 1./3. * amp[459] + 1./3. * amp[460] + 1./3. * amp[461]
      + 1./3. * amp[462] + 1./3. * amp[463] + 1./3. * amp[464] + 1./3. *
      amp[465] + 1./3. * amp[466] + 1./3. * amp[467] + 1./3. * amp[468] + 1./3.
      * amp[469] + 1./3. * amp[470] + 1./3. * amp[471] + 1./3. * amp[472] +
      1./3. * amp[473] + 1./3. * amp[474] + 1./3. * amp[475] + 1./3. * amp[476]
      + 1./3. * amp[477] + 1./3. * amp[478] + 1./3. * amp[479] + 1./3. *
      amp[480] + 1./3. * amp[481] + 1./3. * amp[482] + 1./3. * amp[483] + 1./3.
      * amp[484] + 1./3. * amp[485] + 1./3. * amp[486] + 1./3. * amp[519] +
      1./3. * amp[520] + 1./3. * amp[522] + 1./3. * amp[524] + 1./3. * amp[548]
      + 1./3. * amp[549] + 1./3. * amp[552] + 1./3. * amp[553] + 1./3. *
      amp[554] + 1./3. * amp[556]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[5][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P62_sm_qb_wpwmgqb::matrix_12_dxb_wpwmgdxb() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 102;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+amp[557] + amp[558] + amp[559] + amp[560] + amp[561] +
      amp[562] + amp[563] + amp[564] + amp[565] + amp[566] + amp[567] +
      amp[568] + amp[569] + amp[570] + amp[571] + amp[572] + amp[589] +
      amp[590] + amp[591] + amp[592] + amp[593] + amp[594] + amp[595] +
      amp[596] + amp[597] + amp[598] + amp[599] + amp[600] + amp[601] +
      amp[602] + amp[603] + amp[604] + amp[621] + Complex<double> (0, 1) *
      amp[623] + amp[624] + Complex<double> (0, 1) * amp[625] + amp[626] -
      Complex<double> (0, 1) * amp[627] - Complex<double> (0, 1) * amp[628] +
      amp[630] - Complex<double> (0, 1) * amp[631] - Complex<double> (0, 1) *
      amp[632] + amp[634] - Complex<double> (0, 1) * amp[635] - Complex<double>
      (0, 1) * amp[636] + amp[638] - Complex<double> (0, 1) * amp[639] +
      amp[640] + amp[641] - Complex<double> (0, 1) * amp[642] + amp[643] -
      Complex<double> (0, 1) * amp[645] + Complex<double> (0, 1) * amp[648] +
      Complex<double> (0, 1) * amp[649] + amp[650] + Complex<double> (0, 1) *
      amp[652] + Complex<double> (0, 1) * amp[653] + amp[654] + Complex<double>
      (0, 1) * amp[657]);
  jamp[1] = +1./2. * (-1./3. * amp[589] - 1./3. * amp[590] - 1./3. * amp[591] -
      1./3. * amp[592] - 1./3. * amp[593] - 1./3. * amp[594] - 1./3. * amp[595]
      - 1./3. * amp[596] - 1./3. * amp[597] - 1./3. * amp[598] - 1./3. *
      amp[599] - 1./3. * amp[600] - 1./3. * amp[601] - 1./3. * amp[602] - 1./3.
      * amp[603] - 1./3. * amp[604] - 1./3. * amp[605] - 1./3. * amp[606] -
      1./3. * amp[607] - 1./3. * amp[608] - 1./3. * amp[609] - 1./3. * amp[610]
      - 1./3. * amp[611] - 1./3. * amp[612] - 1./3. * amp[613] - 1./3. *
      amp[614] - 1./3. * amp[615] - 1./3. * amp[616] - 1./3. * amp[617] - 1./3.
      * amp[618] - 1./3. * amp[619] - 1./3. * amp[620] - 1./3. * amp[629] -
      1./3. * amp[630] - 1./3. * amp[633] - 1./3. * amp[634] - 1./3. * amp[637]
      - 1./3. * amp[638] - 1./3. * amp[640] - 1./3. * amp[641] - 1./3. *
      amp[643] - 1./3. * amp[644] - 1./3. * amp[646] - 1./3. * amp[647]);
  jamp[2] = +1./2. * (+amp[573] + amp[574] + amp[575] + amp[576] + amp[577] +
      amp[578] + amp[579] + amp[580] + amp[581] + amp[582] + amp[583] +
      amp[584] + amp[585] + amp[586] + amp[587] + amp[588] + amp[605] +
      amp[606] + amp[607] + amp[608] + amp[609] + amp[610] + amp[611] +
      amp[612] + amp[613] + amp[614] + amp[615] + amp[616] + amp[617] +
      amp[618] + amp[619] + amp[620] + amp[622] - Complex<double> (0, 1) *
      amp[623] - Complex<double> (0, 1) * amp[625] + Complex<double> (0, 1) *
      amp[627] + Complex<double> (0, 1) * amp[628] + amp[629] + Complex<double>
      (0, 1) * amp[631] + Complex<double> (0, 1) * amp[632] + amp[633] +
      Complex<double> (0, 1) * amp[635] + Complex<double> (0, 1) * amp[636] +
      amp[637] + Complex<double> (0, 1) * amp[639] + Complex<double> (0, 1) *
      amp[642] + amp[644] + Complex<double> (0, 1) * amp[645] + amp[646] +
      amp[647] - Complex<double> (0, 1) * amp[648] - Complex<double> (0, 1) *
      amp[649] + amp[651] - Complex<double> (0, 1) * amp[652] - Complex<double>
      (0, 1) * amp[653] + amp[655] + amp[656] - Complex<double> (0, 1) *
      amp[657] + amp[658]);
  jamp[3] = +1./2. * (-1./3. * amp[557] - 1./3. * amp[558] - 1./3. * amp[559] -
      1./3. * amp[560] - 1./3. * amp[561] - 1./3. * amp[562] - 1./3. * amp[563]
      - 1./3. * amp[564] - 1./3. * amp[565] - 1./3. * amp[566] - 1./3. *
      amp[567] - 1./3. * amp[568] - 1./3. * amp[569] - 1./3. * amp[570] - 1./3.
      * amp[571] - 1./3. * amp[572] - 1./3. * amp[573] - 1./3. * amp[574] -
      1./3. * amp[575] - 1./3. * amp[576] - 1./3. * amp[577] - 1./3. * amp[578]
      - 1./3. * amp[579] - 1./3. * amp[580] - 1./3. * amp[581] - 1./3. *
      amp[582] - 1./3. * amp[583] - 1./3. * amp[584] - 1./3. * amp[585] - 1./3.
      * amp[586] - 1./3. * amp[587] - 1./3. * amp[588] - 1./3. * amp[621] -
      1./3. * amp[622] - 1./3. * amp[624] - 1./3. * amp[626] - 1./3. * amp[650]
      - 1./3. * amp[651] - 1./3. * amp[654] - 1./3. * amp[655] - 1./3. *
      amp[656] - 1./3. * amp[658]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[6][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R12_P62_sm_qb_wpwmgqb::matrix_12_dxbx_wpwmgdxbx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 102;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[691] + 1./3. * amp[692] + 1./3. * amp[693] +
      1./3. * amp[694] + 1./3. * amp[695] + 1./3. * amp[696] + 1./3. * amp[697]
      + 1./3. * amp[698] + 1./3. * amp[699] + 1./3. * amp[700] + 1./3. *
      amp[701] + 1./3. * amp[702] + 1./3. * amp[703] + 1./3. * amp[704] + 1./3.
      * amp[705] + 1./3. * amp[706] + 1./3. * amp[707] + 1./3. * amp[708] +
      1./3. * amp[709] + 1./3. * amp[710] + 1./3. * amp[711] + 1./3. * amp[712]
      + 1./3. * amp[713] + 1./3. * amp[714] + 1./3. * amp[715] + 1./3. *
      amp[716] + 1./3. * amp[717] + 1./3. * amp[718] + 1./3. * amp[719] + 1./3.
      * amp[720] + 1./3. * amp[721] + 1./3. * amp[722] + 1./3. * amp[731] +
      1./3. * amp[732] + 1./3. * amp[735] + 1./3. * amp[736] + 1./3. * amp[739]
      + 1./3. * amp[740] + 1./3. * amp[742] + 1./3. * amp[743] + 1./3. *
      amp[745] + 1./3. * amp[746] + 1./3. * amp[748] + 1./3. * amp[749]);
  jamp[1] = +1./2. * (-amp[659] - amp[660] - amp[661] - amp[662] - amp[663] -
      amp[664] - amp[665] - amp[666] - amp[667] - amp[668] - amp[669] -
      amp[670] - amp[671] - amp[672] - amp[673] - amp[674] - amp[691] -
      amp[692] - amp[693] - amp[694] - amp[695] - amp[696] - amp[697] -
      amp[698] - amp[699] - amp[700] - amp[701] - amp[702] - amp[703] -
      amp[704] - amp[705] - amp[706] - amp[723] - Complex<double> (0, 1) *
      amp[725] - amp[726] - Complex<double> (0, 1) * amp[727] - amp[728] +
      Complex<double> (0, 1) * amp[729] + Complex<double> (0, 1) * amp[730] -
      amp[732] + Complex<double> (0, 1) * amp[733] + Complex<double> (0, 1) *
      amp[734] - amp[736] + Complex<double> (0, 1) * amp[737] + Complex<double>
      (0, 1) * amp[738] - amp[740] + Complex<double> (0, 1) * amp[741] -
      amp[742] - amp[743] + Complex<double> (0, 1) * amp[744] - amp[745] +
      Complex<double> (0, 1) * amp[747] - Complex<double> (0, 1) * amp[750] -
      Complex<double> (0, 1) * amp[751] - amp[752] - Complex<double> (0, 1) *
      amp[754] - Complex<double> (0, 1) * amp[755] - amp[756] - Complex<double>
      (0, 1) * amp[759]);
  jamp[2] = +1./2. * (-amp[675] - amp[676] - amp[677] - amp[678] - amp[679] -
      amp[680] - amp[681] - amp[682] - amp[683] - amp[684] - amp[685] -
      amp[686] - amp[687] - amp[688] - amp[689] - amp[690] - amp[707] -
      amp[708] - amp[709] - amp[710] - amp[711] - amp[712] - amp[713] -
      amp[714] - amp[715] - amp[716] - amp[717] - amp[718] - amp[719] -
      amp[720] - amp[721] - amp[722] - amp[724] + Complex<double> (0, 1) *
      amp[725] + Complex<double> (0, 1) * amp[727] - Complex<double> (0, 1) *
      amp[729] - Complex<double> (0, 1) * amp[730] - amp[731] - Complex<double>
      (0, 1) * amp[733] - Complex<double> (0, 1) * amp[734] - amp[735] -
      Complex<double> (0, 1) * amp[737] - Complex<double> (0, 1) * amp[738] -
      amp[739] - Complex<double> (0, 1) * amp[741] - Complex<double> (0, 1) *
      amp[744] - amp[746] - Complex<double> (0, 1) * amp[747] - amp[748] -
      amp[749] + Complex<double> (0, 1) * amp[750] + Complex<double> (0, 1) *
      amp[751] - amp[753] + Complex<double> (0, 1) * amp[754] + Complex<double>
      (0, 1) * amp[755] - amp[757] - amp[758] + Complex<double> (0, 1) *
      amp[759] - amp[760]);
  jamp[3] = +1./2. * (+1./3. * amp[659] + 1./3. * amp[660] + 1./3. * amp[661] +
      1./3. * amp[662] + 1./3. * amp[663] + 1./3. * amp[664] + 1./3. * amp[665]
      + 1./3. * amp[666] + 1./3. * amp[667] + 1./3. * amp[668] + 1./3. *
      amp[669] + 1./3. * amp[670] + 1./3. * amp[671] + 1./3. * amp[672] + 1./3.
      * amp[673] + 1./3. * amp[674] + 1./3. * amp[675] + 1./3. * amp[676] +
      1./3. * amp[677] + 1./3. * amp[678] + 1./3. * amp[679] + 1./3. * amp[680]
      + 1./3. * amp[681] + 1./3. * amp[682] + 1./3. * amp[683] + 1./3. *
      amp[684] + 1./3. * amp[685] + 1./3. * amp[686] + 1./3. * amp[687] + 1./3.
      * amp[688] + 1./3. * amp[689] + 1./3. * amp[690] + 1./3. * amp[723] +
      1./3. * amp[724] + 1./3. * amp[726] + 1./3. * amp[728] + 1./3. * amp[752]
      + 1./3. * amp[753] + 1./3. * amp[756] + 1./3. * amp[757] + 1./3. *
      amp[758] + 1./3. * amp[760]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[7][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}


}  // end namespace PY8MEs_namespace

