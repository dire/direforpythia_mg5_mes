//==========================================================================
// This file has been automatically generated for Pythia 8 by
// MadGraph5_aMC@NLO v. 2.6.0, 2017-08-16
// By the MadGraph5_aMC@NLO Development Team
// Visit launchpad.net/madgraph5 and amcatnlo.web.cern.ch
//==========================================================================

#include "PY8MEs_R7_P26_heft_bb_hhbb.h"
#include "HelAmps_heft.h"

using namespace Pythia8_heft; 

namespace PY8MEs_namespace 
{
//==========================================================================
// Class member functions for calculating the matrix elements for
// Process: b b > h h b b HIG<=1 HIW<=1 WEIGHTED<=6 @7
// Process: b b~ > h h b b~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
// Process: b~ b~ > h h b~ b~ HIG<=1 HIW<=1 WEIGHTED<=6 @7

// Exception class
class PY8MEs_R7_P26_heft_bb_hhbbException : public exception
{
  virtual const char * what() const throw()
  {
    return "Exception in class 'PY8MEs_R7_P26_heft_bb_hhbb'."; 
  }
}
PY8MEs_R7_P26_heft_bb_hhbb_exception; 

std::set<int> PY8MEs_R7_P26_heft_bb_hhbb::s_channel_proc = std::set<int>
    (createset<int> ());

int PY8MEs_R7_P26_heft_bb_hhbb::helicities[ncomb][nexternal] = {{-1, -1, 0, 0,
    -1, -1}, {-1, -1, 0, 0, -1, 1}, {-1, -1, 0, 0, 1, -1}, {-1, -1, 0, 0, 1,
    1}, {-1, 1, 0, 0, -1, -1}, {-1, 1, 0, 0, -1, 1}, {-1, 1, 0, 0, 1, -1}, {-1,
    1, 0, 0, 1, 1}, {1, -1, 0, 0, -1, -1}, {1, -1, 0, 0, -1, 1}, {1, -1, 0, 0,
    1, -1}, {1, -1, 0, 0, 1, 1}, {1, 1, 0, 0, -1, -1}, {1, 1, 0, 0, -1, 1}, {1,
    1, 0, 0, 1, -1}, {1, 1, 0, 0, 1, 1}};

// Normalization factors the various processes
// Denominators: spins, colors and identical particles
int PY8MEs_R7_P26_heft_bb_hhbb::denom_colors[nprocesses] = {9, 9, 9}; 
int PY8MEs_R7_P26_heft_bb_hhbb::denom_hels[nprocesses] = {4, 4, 4}; 
int PY8MEs_R7_P26_heft_bb_hhbb::denom_iden[nprocesses] = {4, 2, 4}; 

//--------------------------------------------------------------------------
// Color config initialization
void PY8MEs_R7_P26_heft_bb_hhbb::initColorConfigs() 
{
  color_configs = vector < vec_vec_int > (); 
  jamp_nc_relative_power = vector < vec_int > (); 

  // Color flows of process Process: b b > h h b b HIG<=1 HIW<=1 WEIGHTED<=6 @7
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #1
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(0)(1)(0)(0)(0)(0)(0)(1)(0)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 

  // Color flows of process Process: b b~ > h h b b~ HIG<=1 HIW<=1 WEIGHTED<=6
  // @7
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(2)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #1
  color_configs[1].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(2)(0)(0)(1)));
  jamp_nc_relative_power[1].push_back(0); 

  // Color flows of process Process: b~ b~ > h h b~ b~ HIG<=1 HIW<=1
  // WEIGHTED<=6 @7
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(1)(0)(2)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #1
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(0)(2)(0)(1)));
  jamp_nc_relative_power[2].push_back(0); 
}

//--------------------------------------------------------------------------
// Destructor.
PY8MEs_R7_P26_heft_bb_hhbb::~PY8MEs_R7_P26_heft_bb_hhbb() 
{
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    delete[] p[i]; 
    p[i] = NULL; 
  }
}

//--------------------------------------------------------------------------
// Invert the permutation mapping
vector<int> PY8MEs_R7_P26_heft_bb_hhbb::invert_mapping(vector<int> mapping) 
{
  vector<int> inverted_mapping; 
  for (unsigned int i = 0; i < mapping.size(); i++ )
  {
    for (unsigned int j = 0; j < mapping.size(); j++ )
    {
      if (mapping[j] == ((int)i))
      {
        inverted_mapping.push_back(j); 
        break; 
      }
    }
  }
  return inverted_mapping; 
}

//--------------------------------------------------------------------------
// Return the list of possible helicity configurations
vector < vec_int > PY8MEs_R7_P26_heft_bb_hhbb::getHelicityConfigs(vector<int>
    permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(ncomb, vector<int> (nexternal, 0)); 
  for (unsigned int ihel = 0; ihel < ncomb; ihel++ )
  {
    for(unsigned int j = 0; j < nexternal; j++ )
    {
      res[ihel][chosenPerm[j]] = helicities[ihel][j]; 
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Return the list of possible color configurations
vector < vec_int > PY8MEs_R7_P26_heft_bb_hhbb::getColorConfigs(int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(color_configs[chosenProcID].size(), vector<int>
      (nexternal * 2, 0));
  for (unsigned int icol = 0; icol < color_configs[chosenProcID].size(); icol++
      )
  {
    for(unsigned int j = 0; j < (2 * nexternal); j++ )
    {
      res[icol][chosenPerm[j/2] * 2 + j%2] =
          color_configs[chosenProcID][icol][j];
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Get JAMP relative N_c power
int PY8MEs_R7_P26_heft_bb_hhbb::getColorFlowRelativeNCPower(int color_flow_ID,
    int specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return jamp_nc_relative_power[chosenProcID][color_flow_ID]; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity ID -> Helicity Config
vector<int> PY8MEs_R7_P26_heft_bb_hhbb::getHelicityConfigForID(int hel_ID,
    vector<int> permutation)
{
  if (hel_ID < 0 || hel_ID >= ncomb)
  {
    cerr <<  "Error in function 'getHelicityConfigForID' of class" << 
    " 'PY8MEs_R7_P26_heft_bb_hhbb': Specified helicity ID '" << 
    hel_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R7_P26_heft_bb_hhbb_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(nexternal, 0); 
  for (unsigned int j = 0; j < nexternal; j++ )
  {
    res[chosenPerm[j]] = helicities[hel_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity Config -> Helicity ID
int PY8MEs_R7_P26_heft_bb_hhbb::getHelicityIDForConfig(vector<int> hel_config,
    vector<int> permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  int user_ihel = -1; 
  if (hel_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < ncomb; i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < nexternal; j++ )
      {
        if (helicities[i][chosenPerm[j]] != hel_config[j])
        {
          found = false; 
          break; 
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_ihel = i; 
        break; 
      }
    }
    if (user_ihel == -1)
    {
      cerr <<  "Error in function 'getHelicityIDForConfig' of class" << 
      " 'PY8MEs_R7_P26_heft_bb_hhbb': Specified helicity" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R7_P26_heft_bb_hhbb_exception; 
    }
  }
  return user_ihel; 
}


//--------------------------------------------------------------------------
// Implements the map Color ID -> Color Config
vector<int> PY8MEs_R7_P26_heft_bb_hhbb::getColorConfigForID(int color_ID, int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < 0 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getColorConfigForID' of class" << 
    " 'PY8MEs_R7_P26_heft_bb_hhbb': Specified color ID '" << 
    color_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R7_P26_heft_bb_hhbb_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(color_configs[chosenProcID][color_ID].size(), 0); 
  for (unsigned int j = 0; j < (2 * nexternal); j++ )
  {
    res[chosenPerm[j/2] * 2 + j%2] = color_configs[chosenProcID][color_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Color Config -> Color ID
int PY8MEs_R7_P26_heft_bb_hhbb::getColorIDForConfig(vector<int> color_config,
    int specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = -1; 
  if (color_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < color_configs[chosenProcID].size(); i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < (nexternal * 2); j++ )
      {

        // If colorless then make sure it matches
        // The little arithmetics in the color index is just
        // the permutation applies on the particle list which is
        // twice smaller since each particle can have two color indices.
        if (color_config[j] == 0)
        {
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] != 0)
          {
            found = false; 
            break; 
          }
          // Otherwise check that the color linked position matches
        }
        else
        {
          int color_linked_pos = -1; 
          // Find the other end of the line in the user color config
          for (unsigned int k = 0; k < (nexternal * 2); k++ )
          {
            if (k == j)
              continue; 
            if (color_config[j] == color_config[k])
            {
              color_linked_pos = k; 
              break; 
            }
          }
          if (color_linked_pos == -1)
          {
            cerr <<  "Error in function 'getColorIDForConfig' of class" << 
            " 'PY8MEs_R7_P26_heft_bb_hhbb': A color line could " << 
            " not be closed." << endl; 
            throw PY8MEs_R7_P26_heft_bb_hhbb_exception; 
          }
          // Now check whether the color line matches
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] !=
              color_configs[chosenProcID][i][chosenPerm[color_linked_pos/2] * 2
              + color_linked_pos%2])
          {
            found = false; 
            break; 
          }
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_icol = i; 
        break; 
      }
    }

    if (user_icol == -1)
    {
      cerr <<  "Error in function 'getColorIDForConfig' of class" << 
      " 'PY8MEs_R7_P26_heft_bb_hhbb': Specified color" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R7_P26_heft_bb_hhbb_exception; 
    }
  }
  return user_icol; 
}

//--------------------------------------------------------------------------
// Returns all result previously computed in SigmaKin
vector < vec_double > PY8MEs_R7_P26_heft_bb_hhbb::getAllResults(int
    specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return all_results[chosenProcID]; 
}

//--------------------------------------------------------------------------
// Returns a result previously computed in SigmaKin for a specific helicity
// and color ID. -1 means avg and summed over that characteristic.
double PY8MEs_R7_P26_heft_bb_hhbb::getResult(int helicity_ID, int color_ID, int
    specify_proc_ID)
{
  if (helicity_ID < - 1 || helicity_ID >= ncomb)
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R7_P26_heft_bb_hhbb': Specified helicity ID '" << 
    helicity_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R7_P26_heft_bb_hhbb_exception; 
  }
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < - 1 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R7_P26_heft_bb_hhbb': Specified color ID '" << 
    color_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R7_P26_heft_bb_hhbb_exception; 
  }
  return all_results[chosenProcID][helicity_ID + 1][color_ID + 1]; 
}

//--------------------------------------------------------------------------
// Check for the availability of the requested process and if available,
// If available, this returns the corresponding permutation and Proc_ID to use.
// If not available, this returns a negative Proc_ID.
pair < vector<int> , int > PY8MEs_R7_P26_heft_bb_hhbb::static_getPY8ME(vector<int> initial_pdgs, vector<int> final_pdgs, set<int> schannels) 
{

  // Not available return value
  pair < vector<int> , int > NA(vector<int> (), -1); 

  // Check if s-channel requirements match
  if (nreq_s_channels > 0)
  {
    if (schannels != s_channel_proc)
      return NA; 
  }
  else
  {
    if (schannels.size() != 0)
      return NA; 
  }

  // Check number of final state particles
  if (final_pdgs.size() != (nexternal - ninitial))
    return NA; 

  // Check number of initial state particles
  if (initial_pdgs.size() != ninitial)
    return NA; 

  // List of processes available in this class
  const int nprocs = 4; 
  const int proc_IDS[nprocs] = {0, 1, 2, 1}; 
  const int in_pdgs[nprocs][ninitial] = {{5, 5}, {5, -5}, {-5, -5}, {-5, 5}}; 
  const int out_pdgs[nprocs][nexternal - ninitial] = {{25, 25, 5, 5}, {25, 25,
      5, -5}, {25, 25, -5, -5}, {25, 25, 5, -5}};

  bool in_pdgs_used[ninitial]; 
  bool out_pdgs_used[nexternal - ninitial]; 
  for(unsigned int i = 0; i < nprocs; i++ )
  {
    int permutations[nexternal]; 

    // Reinitialize initial state look-up variables
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      in_pdgs_used[j] = false; 
      permutations[j] = -1; 
    }
    // Look for initial state matches
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      for(unsigned int k = 0; k < ninitial; k++ )
      {
        // Make sure it has not been used already
        if (in_pdgs_used[k])
          continue; 
        if (initial_pdgs[k] == in_pdgs[i][j])
        {
          permutations[j] = k; 
          in_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[ninitial - 1] == -1)
      continue; 

    // Reinitialize final state look-up variables
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      out_pdgs_used[j] = false; 
      permutations[ninitial + j] = -1; 
    }
    // Look for final state matches
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      for(unsigned int k = 0; k < (nexternal - ninitial); k++ )
      {
        // Make sure it has not been used already
        if (out_pdgs_used[k])
          continue; 
        if (final_pdgs[k] == out_pdgs[i][j])
        {
          permutations[ninitial + j] = ninitial + k; 
          out_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[ninitial + j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[nexternal - 1] == -1)
      continue; 

    // Return process found
    return pair < vector<int> , int > (vector<int> (permutations, permutations
        + nexternal), proc_IDS[i]);
  }

  // No process found
  return NA; 
}

//--------------------------------------------------------------------------
// Set momenta
void PY8MEs_R7_P26_heft_bb_hhbb::setMomenta(vector < vec_double >
    momenta_picked)
{
  if (momenta_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setMomenta' of class" << 
    " 'PY8MEs_R7_P26_heft_bb_hhbb': Incorrect number of" << 
    " momenta specified." << endl; 
    throw PY8MEs_R7_P26_heft_bb_hhbb_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    if (momenta_picked[i].size() != 4)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R7_P26_heft_bb_hhbb': Incorrect number of" << 
      " momenta components specified." << endl; 
      throw PY8MEs_R7_P26_heft_bb_hhbb_exception; 
    }
    if (momenta_picked[i][0] < 0.0)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R7_P26_heft_bb_hhbb': A momentum was specified" << 
      " with negative energy. Check conventions." << endl; 
      throw PY8MEs_R7_P26_heft_bb_hhbb_exception; 
    }
    for (unsigned int j = 0; j < 4; j++ )
    {
      p[i][j] = momenta_picked[i][j]; 
    }
  }
}

//--------------------------------------------------------------------------
// Set color configuration to use. An empty vector means sum over all.
void PY8MEs_R7_P26_heft_bb_hhbb::setColors(vector<int> colors_picked)
{
  if (colors_picked.size() == 0)
  {
    user_colors = vector<int> (); 
    return; 
  }
  if (colors_picked.size() != (2 * nexternal))
  {
    cerr <<  "Error in function 'setColors' of class" << 
    " 'PY8MEs_R7_P26_heft_bb_hhbb': Incorrect number" << 
    " of colors specified." << endl; 
    throw PY8MEs_R7_P26_heft_bb_hhbb_exception; 
  }
  user_colors = vector<int> ((2 * nexternal), 0); 
  for(unsigned int i = 0; i < (2 * nexternal); i++ )
  {
    user_colors[i] = colors_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the helicity configuration to use. Am empty vector means sum over all.
void PY8MEs_R7_P26_heft_bb_hhbb::setHelicities(vector<int> helicities_picked) 
{
  if (helicities_picked.size() != nexternal)
  {
    if (helicities_picked.size() == 0)
    {
      user_helicities = vector<int> (); 
      return; 
    }
    cerr <<  "Error in function 'setHelicities' of class" << 
    " 'PY8MEs_R7_P26_heft_bb_hhbb': Incorrect number" << 
    " of helicities specified." << endl; 
    throw PY8MEs_R7_P26_heft_bb_hhbb_exception; 
  }
  user_helicities = vector<int> (nexternal, 0); 
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    user_helicities[i] = helicities_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the permutation to use (will apply to momenta, colors and helicities)
void PY8MEs_R7_P26_heft_bb_hhbb::setPermutation(vector<int> perm_picked) 
{
  if (perm_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setPermutations' of class" << 
    " 'PY8MEs_R7_P26_heft_bb_hhbb': Incorrect number" << 
    " of permutations specified." << endl; 
    throw PY8MEs_R7_P26_heft_bb_hhbb_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    perm[i] = perm_picked[i]; 
  }
}

// Set the proc_ID to use
void PY8MEs_R7_P26_heft_bb_hhbb::setProcID(int procID_picked) 
{
  proc_ID = procID_picked; 
}

//--------------------------------------------------------------------------
// Initialize process.

void PY8MEs_R7_P26_heft_bb_hhbb::initProc() 
{

  // Initialize flags
  include_symmetry_factors = true; 
  include_helicity_averaging_factors = true; 
  include_color_averaging_factors = true; 

  // Initialize vectors.
  perm = vector<int> (nexternal, 0); 
  user_colors = vector<int> (2 * nexternal, 0); 
  user_helicities = vector<int> (nexternal, 0); 
  p = vector < double * > (); 
  for (unsigned int i = 0; i < nexternal; i++ )
  {
    p.push_back(new double[4]); 
  }
  initColorConfigs(); 
  // Synchronize local variables dependent on the model with the active model.
  mME = vector<double> (nexternal, 0.); 
  syncProcModelParams(); 
  jamp2 = vector < vec_double > (3); 
  jamp2[0] = vector<double> (2, 0.); 
  jamp2[1] = vector<double> (2, 0.); 
  jamp2[2] = vector<double> (2, 0.); 
  all_results = vector < vec_vec_double > (3); 
  // The first entry is always the color or helicity avg/summed matrix element.
  all_results[0] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[1] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
  all_results[2] = vector < vec_double > (ncomb + 1, vector<double> (2 + 1,
      0.));
}

// Synchronize local variables of the process that depend on the model
// parameters
void PY8MEs_R7_P26_heft_bb_hhbb::syncProcModelParams() 
{

  // Instantiate the model class and set parameters that stay fixed during run
  mME[0] = pars->mdl_MB; 
  mME[1] = pars->mdl_MB; 
  mME[2] = pars->mdl_MH; 
  mME[3] = pars->mdl_MH; 
  mME[4] = pars->mdl_MB; 
  mME[5] = pars->mdl_MB; 
}

//--------------------------------------------------------------------------
// Setter allowing to force particular values for the external masses
void PY8MEs_R7_P26_heft_bb_hhbb::setMasses(vec_double external_masses) 
{

  if (external_masses.size() != mME.size())
  {
    cerr <<  "Error in function 'setMasses' of class" << 
    " 'PY8MEs_R7_P26_heft_bb_hhbb': Incorrect number of" << 
    " masses specified." << endl; 
    throw PY8MEs_R7_P26_heft_bb_hhbb_exception; 
  }
  for (unsigned int j = 0; j < mME.size(); j++ )
  {
    mME[j] = external_masses[perm[j]]; 
  }
}

//--------------------------------------------------------------------------
// Getter accessing external masses with the correct ordering
vector<double> PY8MEs_R7_P26_heft_bb_hhbb::getMasses() 
{

  vec_double external_masses; 
  vector<int> invertedPerm; 
  invertedPerm = invert_mapping(perm); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    external_masses.push_back(mME[invertedPerm[i]]); 
  }
  return external_masses; 

}


// Set all values of the external masses to float(-mode) where mode can be
// 0 : Mass taken from the model
// 1 : Mass taken from p_i^2 if not massless to begin with
// 2 : Mass always taken from p_i^2.
void PY8MEs_R7_P26_heft_bb_hhbb::setExternalMassesMode(int mode) 
{
  if (mode != 0 && mode != 1 && mode != 2)
  {
    cerr <<  "Error in function 'setExternalMassesMode' of class" << 
    " 'PY8MEs_R7_P26_heft_bb_hhbb': Incorrect mode selected :" << mode << 
    ". It must be either 0, 1 or 2" << endl; 
    throw PY8MEs_R7_P26_heft_bb_hhbb_exception; 
  }
  if (mode == 0)
  {
    syncProcModelParams(); 
  }
  else if (mode == 1)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      if (mME[j] != pars->ZERO)
      {
        mME[j] = -1.0; 
      }
    }
  }
  else if (mode == 2)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      mME[j] = -1.0; 
    }
  }
}

//--------------------------------------------------------------------------
// Evaluate the squared matrix element.

double PY8MEs_R7_P26_heft_bb_hhbb::sigmaKin() 
{
  // Set the parameters which change event by event
  pars->setDependentParameters(); 
  pars->setDependentCouplings(); 
  // Reset color flows
  for(int i = 0; i < 2; i++ )
    jamp2[0][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[1][i] = 0.; 
  for(int i = 0; i < 2; i++ )
    jamp2[2][i] = 0.; 

  // Save previous values of mME
  vector<double> saved_mME(mME.size(), 0.0); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (mME[i] < 0.0)
    {
      saved_mME[i] = mME[i]; 
      mME[i] = sqrt(abs(pow(p[perm[i]][0], 2) - 
      (pow(p[perm[i]][1], 2) + pow(p[perm[i]][2], 2) + pow(p[perm[i]][3],
          2))));
    }
  }

  // Local variables and constants
  const int max_tries = 10; 
  vector < vec_bool > goodhel(nprocesses, vec_bool(ncomb, false)); 
  vec_int ntry(nprocesses, 0); 
  double t = 0.; 
  double result = 0.; 

  if (ntry[proc_ID] <= max_tries)
    ntry[proc_ID] = ntry[proc_ID] + 1; 

  // Find which helicity configuration is asked for
  // -1 indicates one wants to sum over helicities
  int user_ihel = getHelicityIDForConfig(user_helicities); 

  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = getColorIDForConfig(user_colors); 

  // Reset the list of results that will be recomputed here
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      all_results[proc_ID][ihel + 1][icolor + 1] = 0.; 
    }
  }

  // Calculate the matrix element for all helicities
  // unless already detected as vanishing
  for(int ihel = 0; ihel < ncomb; ihel++ )
  {
    // Skip helicity if already detected as vanishing
    if ((ntry[proc_ID] >= max_tries) && !goodhel[proc_ID][ihel])
      continue; 

    // Also skip helicity if user asks for a specific one
    if ((ntry[proc_ID] >= max_tries) && user_ihel != -1 && user_ihel != ihel)
      continue; 

    calculate_wavefunctions(helicities[ihel]); 

    // Reset locally computed color flows
    for(int i = 0; i < 2; i++ )
      jamp2[0][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[1][i] = 0.; 
    for(int i = 0; i < 2; i++ )
      jamp2[2][i] = 0.; 

    if (proc_ID == 0)
      t = matrix_7_bb_hhbb(); 
    if (proc_ID == 1)
      t = matrix_7_bbx_hhbbx(); 
    if (proc_ID == 2)
      t = matrix_7_bxbx_hhbxbx(); 

    // Store which helicities give non-zero result
    if ((ntry[proc_ID] < max_tries) && t != 0. && !goodhel[proc_ID][ihel])
      goodhel[proc_ID][ihel] = true; 

    // Aggregate results
    if (user_ihel == -1 || user_ihel == ihel)
    {
      if (user_icol == -1)
      {
        result = result + t; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][0] += t; 
          for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
          {
            all_results[proc_ID][0][i + 1] += jamp2[proc_ID][i]; 
          }
        }
        all_results[proc_ID][ihel + 1][0] += t; 
        for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
        {
          all_results[proc_ID][ihel + 1][i + 1] += jamp2[proc_ID][i]; 
        }
      }
      else
      {
        result = result + jamp2[proc_ID][user_icol]; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][user_icol + 1] += jamp2[proc_ID][user_icol]; 
        }
        all_results[proc_ID][ihel + 1][user_icol + 1] +=
            jamp2[proc_ID][user_icol];
      }
    }

  }

  // Normalize results with the identical particle factor
  if (include_symmetry_factors)
  {
    result = result/denom_iden[proc_ID]; 
  }
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      if (include_symmetry_factors)
      {
        all_results[proc_ID][ihel + 1][icolor + 1] /= denom_iden[proc_ID]; 
      }
    }
  }


  // Normalize when when summing+averaging over helicity configurations
  if (user_ihel == -1 && include_helicity_averaging_factors)
  {
    result /= denom_hels[proc_ID]; 
    if (user_icol == -1)
    {
      all_results[proc_ID][0][0] /= denom_hels[proc_ID]; 
      for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
      {
        all_results[proc_ID][0][i + 1] /= denom_hels[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][0][user_icol + 1] /= denom_hels[proc_ID]; 
    }
  }

  // Normalize when summing+averaging over color configurations
  if (user_icol == -1 && include_color_averaging_factors)
  {
    result /= denom_colors[proc_ID]; 
    if (user_ihel == -1)
    {
      all_results[proc_ID][0][0] /= denom_colors[proc_ID]; 
      for (unsigned int i = 0; i < ncomb; i++ )
      {
        all_results[proc_ID][i + 1][0] /= denom_colors[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][user_ihel + 1][0] /= denom_colors[proc_ID]; 
    }
  }

  // Reinstate previous values of mME
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (saved_mME[i] < 0.0)
    {
      mME[i] = saved_mME[i]; 
    }
  }

  // Finally return it
  return result; 
}

//==========================================================================
// Private class member functions

//--------------------------------------------------------------------------
// Evaluate |M|^2 for each subprocess

void PY8MEs_R7_P26_heft_bb_hhbb::calculate_wavefunctions(const int hel[])
{
  // Calculate wavefunctions for all processes
  // Calculate all wavefunctions
  ixxxxx(p[perm[0]], mME[0], hel[0], +1, w[0]); 
  ixxxxx(p[perm[1]], mME[1], hel[1], +1, w[1]); 
  sxxxxx(p[perm[2]], +1, w[2]); 
  sxxxxx(p[perm[3]], +1, w[3]); 
  oxxxxx(p[perm[4]], mME[4], hel[4], +1, w[4]); 
  oxxxxx(p[perm[5]], mME[5], hel[5], +1, w[5]); 
  FFS2_2(w[0], w[2], pars->GC_74, pars->mdl_MB, pars->ZERO, w[6]); 
  FFS2_2(w[1], w[3], pars->GC_74, pars->mdl_MB, pars->ZERO, w[7]); 
  FFV1P0_3(w[6], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[8]); 
  FFV1P0_3(w[6], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[9]); 
  FFV1P0_3(w[1], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[10]); 
  FFS2_2(w[6], w[3], pars->GC_74, pars->mdl_MB, pars->ZERO, w[11]); 
  FFS2_1(w[5], w[3], pars->GC_74, pars->mdl_MB, pars->ZERO, w[12]); 
  FFV1P0_3(w[1], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[13]); 
  FFS2_1(w[4], w[3], pars->GC_74, pars->mdl_MB, pars->ZERO, w[14]); 
  FFS2_2(w[0], w[3], pars->GC_74, pars->mdl_MB, pars->ZERO, w[15]); 
  FFS2_2(w[1], w[2], pars->GC_74, pars->mdl_MB, pars->ZERO, w[16]); 
  FFV1P0_3(w[15], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[17]); 
  FFV1P0_3(w[15], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[18]); 
  FFS2_2(w[15], w[2], pars->GC_74, pars->mdl_MB, pars->ZERO, w[19]); 
  FFS2_1(w[5], w[2], pars->GC_74, pars->mdl_MB, pars->ZERO, w[20]); 
  FFS2_1(w[4], w[2], pars->GC_74, pars->mdl_MB, pars->ZERO, w[21]); 
  FFV1P0_3(w[0], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[22]); 
  VVS3P0_1(w[22], w[3], pars->GC_13, pars->ZERO, pars->ZERO, w[23]); 
  FFV1_1(w[5], w[22], pars->GC_11, pars->mdl_MB, pars->ZERO, w[24]); 
  VVS3P0_1(w[22], w[2], pars->GC_13, pars->ZERO, pars->ZERO, w[25]); 
  SSS1_1(w[2], w[3], pars->GC_69, pars->mdl_MH, pars->mdl_WH, w[26]); 
  FFV1_2(w[1], w[22], pars->GC_11, pars->mdl_MB, pars->ZERO, w[27]); 
  FFV1P0_3(w[0], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[28]); 
  VVS3P0_1(w[28], w[3], pars->GC_13, pars->ZERO, pars->ZERO, w[29]); 
  FFV1_1(w[4], w[28], pars->GC_11, pars->mdl_MB, pars->ZERO, w[30]); 
  VVS3P0_1(w[28], w[2], pars->GC_13, pars->ZERO, pars->ZERO, w[31]); 
  FFV1_2(w[1], w[28], pars->GC_11, pars->mdl_MB, pars->ZERO, w[32]); 
  FFV1P0_3(w[0], w[14], pars->GC_11, pars->ZERO, pars->ZERO, w[33]); 
  FFV1P0_3(w[0], w[12], pars->GC_11, pars->ZERO, pars->ZERO, w[34]); 
  FFV1P0_3(w[0], w[21], pars->GC_11, pars->ZERO, pars->ZERO, w[35]); 
  FFV1P0_3(w[0], w[20], pars->GC_11, pars->ZERO, pars->ZERO, w[36]); 
  FFV1_2(w[0], w[10], pars->GC_11, pars->mdl_MB, pars->ZERO, w[37]); 
  FFS2_2(w[0], w[26], pars->GC_74, pars->mdl_MB, pars->ZERO, w[38]); 
  FFV1_2(w[0], w[13], pars->GC_11, pars->mdl_MB, pars->ZERO, w[39]); 
  oxxxxx(p[perm[1]], mME[1], hel[1], -1, w[40]); 
  ixxxxx(p[perm[5]], mME[5], hel[5], -1, w[41]); 
  FFS2_2(w[41], w[3], pars->GC_74, pars->mdl_MB, pars->ZERO, w[42]); 
  FFV1P0_3(w[6], w[40], pars->GC_11, pars->ZERO, pars->ZERO, w[43]); 
  FFV1P0_3(w[41], w[40], pars->GC_11, pars->ZERO, pars->ZERO, w[44]); 
  FFV1P0_3(w[41], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[45]); 
  FFS2_1(w[40], w[3], pars->GC_74, pars->mdl_MB, pars->ZERO, w[46]); 
  FFS2_2(w[41], w[2], pars->GC_74, pars->mdl_MB, pars->ZERO, w[47]); 
  FFV1P0_3(w[15], w[40], pars->GC_11, pars->ZERO, pars->ZERO, w[48]); 
  FFS2_1(w[40], w[2], pars->GC_74, pars->mdl_MB, pars->ZERO, w[49]); 
  FFV1P0_3(w[0], w[40], pars->GC_11, pars->ZERO, pars->ZERO, w[50]); 
  VVS3P0_1(w[50], w[3], pars->GC_13, pars->ZERO, pars->ZERO, w[51]); 
  FFV1_1(w[4], w[50], pars->GC_11, pars->mdl_MB, pars->ZERO, w[52]); 
  VVS3P0_1(w[50], w[2], pars->GC_13, pars->ZERO, pars->ZERO, w[53]); 
  FFV1_2(w[41], w[50], pars->GC_11, pars->mdl_MB, pars->ZERO, w[54]); 
  FFV1_1(w[40], w[22], pars->GC_11, pars->mdl_MB, pars->ZERO, w[55]); 
  FFV1_2(w[41], w[22], pars->GC_11, pars->mdl_MB, pars->ZERO, w[56]); 
  FFV1P0_3(w[0], w[46], pars->GC_11, pars->ZERO, pars->ZERO, w[57]); 
  FFV1P0_3(w[0], w[49], pars->GC_11, pars->ZERO, pars->ZERO, w[58]); 
  FFV1_2(w[0], w[44], pars->GC_11, pars->mdl_MB, pars->ZERO, w[59]); 
  FFV1_2(w[0], w[45], pars->GC_11, pars->mdl_MB, pars->ZERO, w[60]); 
  oxxxxx(p[perm[0]], mME[0], hel[0], -1, w[61]); 
  ixxxxx(p[perm[4]], mME[4], hel[4], -1, w[62]); 
  FFS2_2(w[62], w[2], pars->GC_74, pars->mdl_MB, pars->ZERO, w[63]); 
  FFV1P0_3(w[63], w[61], pars->GC_11, pars->ZERO, pars->ZERO, w[64]); 
  FFV1P0_3(w[63], w[40], pars->GC_11, pars->ZERO, pars->ZERO, w[65]); 
  FFV1P0_3(w[41], w[61], pars->GC_11, pars->ZERO, pars->ZERO, w[66]); 
  FFS2_2(w[63], w[3], pars->GC_74, pars->mdl_MB, pars->ZERO, w[67]); 
  FFS2_1(w[61], w[3], pars->GC_74, pars->mdl_MB, pars->ZERO, w[68]); 
  FFS2_2(w[62], w[3], pars->GC_74, pars->mdl_MB, pars->ZERO, w[69]); 
  FFV1P0_3(w[69], w[61], pars->GC_11, pars->ZERO, pars->ZERO, w[70]); 
  FFV1P0_3(w[69], w[40], pars->GC_11, pars->ZERO, pars->ZERO, w[71]); 
  FFS2_2(w[69], w[2], pars->GC_74, pars->mdl_MB, pars->ZERO, w[72]); 
  FFS2_1(w[61], w[2], pars->GC_74, pars->mdl_MB, pars->ZERO, w[73]); 
  FFV1P0_3(w[62], w[61], pars->GC_11, pars->ZERO, pars->ZERO, w[74]); 
  VVS3P0_1(w[74], w[3], pars->GC_13, pars->ZERO, pars->ZERO, w[75]); 
  FFV1_1(w[40], w[74], pars->GC_11, pars->mdl_MB, pars->ZERO, w[76]); 
  VVS3P0_1(w[74], w[2], pars->GC_13, pars->ZERO, pars->ZERO, w[77]); 
  FFV1_2(w[41], w[74], pars->GC_11, pars->mdl_MB, pars->ZERO, w[78]); 
  FFV1P0_3(w[62], w[40], pars->GC_11, pars->ZERO, pars->ZERO, w[79]); 
  VVS3P0_1(w[79], w[3], pars->GC_13, pars->ZERO, pars->ZERO, w[80]); 
  FFV1_1(w[61], w[79], pars->GC_11, pars->mdl_MB, pars->ZERO, w[81]); 
  VVS3P0_1(w[79], w[2], pars->GC_13, pars->ZERO, pars->ZERO, w[82]); 
  FFV1_2(w[41], w[79], pars->GC_11, pars->mdl_MB, pars->ZERO, w[83]); 
  FFV1P0_3(w[62], w[68], pars->GC_11, pars->ZERO, pars->ZERO, w[84]); 
  FFV1P0_3(w[62], w[46], pars->GC_11, pars->ZERO, pars->ZERO, w[85]); 
  FFV1P0_3(w[62], w[73], pars->GC_11, pars->ZERO, pars->ZERO, w[86]); 
  FFV1P0_3(w[62], w[49], pars->GC_11, pars->ZERO, pars->ZERO, w[87]); 
  FFV1_2(w[62], w[66], pars->GC_11, pars->mdl_MB, pars->ZERO, w[88]); 
  FFS2_2(w[62], w[26], pars->GC_74, pars->mdl_MB, pars->ZERO, w[89]); 
  FFV1_2(w[62], w[44], pars->GC_11, pars->mdl_MB, pars->ZERO, w[90]); 

  // Calculate all amplitudes
  // Amplitude(s) for diagram number 0
  FFV1_0(w[7], w[5], w[8], pars->GC_11, amp[0]); 
  FFV1_0(w[7], w[4], w[9], pars->GC_11, amp[1]); 
  FFV1_0(w[11], w[5], w[10], pars->GC_11, amp[2]); 
  VVS3_0(w[10], w[9], w[3], pars->GC_13, amp[3]); 
  FFV1_0(w[6], w[12], w[10], pars->GC_11, amp[4]); 
  FFV1_0(w[11], w[4], w[13], pars->GC_11, amp[5]); 
  VVS3_0(w[13], w[8], w[3], pars->GC_13, amp[6]); 
  FFV1_0(w[6], w[14], w[13], pars->GC_11, amp[7]); 
  FFV1_0(w[1], w[14], w[9], pars->GC_11, amp[8]); 
  FFV1_0(w[1], w[12], w[8], pars->GC_11, amp[9]); 
  FFV1_0(w[16], w[5], w[17], pars->GC_11, amp[10]); 
  FFV1_0(w[16], w[4], w[18], pars->GC_11, amp[11]); 
  FFV1_0(w[19], w[5], w[10], pars->GC_11, amp[12]); 
  VVS3_0(w[10], w[18], w[2], pars->GC_13, amp[13]); 
  FFV1_0(w[15], w[20], w[10], pars->GC_11, amp[14]); 
  FFV1_0(w[19], w[4], w[13], pars->GC_11, amp[15]); 
  VVS3_0(w[13], w[17], w[2], pars->GC_13, amp[16]); 
  FFV1_0(w[15], w[21], w[13], pars->GC_11, amp[17]); 
  FFV1_0(w[1], w[21], w[18], pars->GC_11, amp[18]); 
  FFV1_0(w[1], w[20], w[17], pars->GC_11, amp[19]); 
  FFV1_0(w[16], w[5], w[23], pars->GC_11, amp[20]); 
  FFS2_0(w[16], w[24], w[3], pars->GC_74, amp[21]); 
  FFV1_0(w[16], w[12], w[22], pars->GC_11, amp[22]); 
  FFV1_0(w[7], w[5], w[25], pars->GC_11, amp[23]); 
  FFS2_0(w[7], w[24], w[2], pars->GC_74, amp[24]); 
  FFV1_0(w[7], w[20], w[22], pars->GC_11, amp[25]); 
  VVS3_0(w[22], w[13], w[26], pars->GC_13, amp[26]); 
  FFS2_0(w[27], w[5], w[26], pars->GC_74, amp[27]); 
  FFS2_0(w[1], w[24], w[26], pars->GC_74, amp[28]); 
  FFS2_0(w[27], w[20], w[3], pars->GC_74, amp[29]); 
  FFV1_0(w[1], w[20], w[23], pars->GC_11, amp[30]); 
  FFS2_0(w[27], w[12], w[2], pars->GC_74, amp[31]); 
  FFV1_0(w[1], w[12], w[25], pars->GC_11, amp[32]); 
  FFV1_0(w[16], w[4], w[29], pars->GC_11, amp[33]); 
  FFS2_0(w[16], w[30], w[3], pars->GC_74, amp[34]); 
  FFV1_0(w[16], w[14], w[28], pars->GC_11, amp[35]); 
  FFV1_0(w[7], w[4], w[31], pars->GC_11, amp[36]); 
  FFS2_0(w[7], w[30], w[2], pars->GC_74, amp[37]); 
  FFV1_0(w[7], w[21], w[28], pars->GC_11, amp[38]); 
  VVS3_0(w[28], w[10], w[26], pars->GC_13, amp[39]); 
  FFS2_0(w[32], w[4], w[26], pars->GC_74, amp[40]); 
  FFS2_0(w[1], w[30], w[26], pars->GC_74, amp[41]); 
  FFS2_0(w[32], w[21], w[3], pars->GC_74, amp[42]); 
  FFV1_0(w[1], w[21], w[29], pars->GC_11, amp[43]); 
  FFS2_0(w[32], w[14], w[2], pars->GC_74, amp[44]); 
  FFV1_0(w[1], w[14], w[31], pars->GC_11, amp[45]); 
  FFV1_0(w[16], w[5], w[33], pars->GC_11, amp[46]); 
  FFV1_0(w[16], w[4], w[34], pars->GC_11, amp[47]); 
  FFV1_0(w[7], w[5], w[35], pars->GC_11, amp[48]); 
  FFV1_0(w[7], w[4], w[36], pars->GC_11, amp[49]); 
  FFS2_0(w[37], w[5], w[26], pars->GC_74, amp[50]); 
  FFV1_0(w[38], w[5], w[10], pars->GC_11, amp[51]); 
  FFS2_0(w[37], w[20], w[3], pars->GC_74, amp[52]); 
  VVS3_0(w[10], w[36], w[3], pars->GC_13, amp[53]); 
  FFS2_0(w[37], w[12], w[2], pars->GC_74, amp[54]); 
  VVS3_0(w[10], w[34], w[2], pars->GC_13, amp[55]); 
  FFS2_0(w[39], w[4], w[26], pars->GC_74, amp[56]); 
  FFV1_0(w[38], w[4], w[13], pars->GC_11, amp[57]); 
  FFS2_0(w[39], w[21], w[3], pars->GC_74, amp[58]); 
  VVS3_0(w[13], w[35], w[3], pars->GC_13, amp[59]); 
  FFS2_0(w[39], w[14], w[2], pars->GC_74, amp[60]); 
  VVS3_0(w[13], w[33], w[2], pars->GC_13, amp[61]); 
  FFV1_0(w[1], w[12], w[35], pars->GC_11, amp[62]); 
  FFV1_0(w[1], w[21], w[34], pars->GC_11, amp[63]); 
  FFV1_0(w[1], w[14], w[36], pars->GC_11, amp[64]); 
  FFV1_0(w[1], w[20], w[33], pars->GC_11, amp[65]); 
  FFV1_0(w[42], w[4], w[43], pars->GC_11, amp[66]); 
  FFV1_0(w[42], w[40], w[8], pars->GC_11, amp[67]); 
  FFV1_0(w[11], w[4], w[44], pars->GC_11, amp[68]); 
  VVS3_0(w[44], w[8], w[3], pars->GC_13, amp[69]); 
  FFV1_0(w[6], w[14], w[44], pars->GC_11, amp[70]); 
  FFV1_0(w[11], w[40], w[45], pars->GC_11, amp[71]); 
  VVS3_0(w[45], w[43], w[3], pars->GC_13, amp[72]); 
  FFV1_0(w[6], w[46], w[45], pars->GC_11, amp[73]); 
  FFV1_0(w[41], w[46], w[8], pars->GC_11, amp[74]); 
  FFV1_0(w[41], w[14], w[43], pars->GC_11, amp[75]); 
  FFV1_0(w[47], w[4], w[48], pars->GC_11, amp[76]); 
  FFV1_0(w[47], w[40], w[17], pars->GC_11, amp[77]); 
  FFV1_0(w[19], w[4], w[44], pars->GC_11, amp[78]); 
  VVS3_0(w[44], w[17], w[2], pars->GC_13, amp[79]); 
  FFV1_0(w[15], w[21], w[44], pars->GC_11, amp[80]); 
  FFV1_0(w[19], w[40], w[45], pars->GC_11, amp[81]); 
  VVS3_0(w[45], w[48], w[2], pars->GC_13, amp[82]); 
  FFV1_0(w[15], w[49], w[45], pars->GC_11, amp[83]); 
  FFV1_0(w[41], w[49], w[17], pars->GC_11, amp[84]); 
  FFV1_0(w[41], w[21], w[48], pars->GC_11, amp[85]); 
  FFV1_0(w[47], w[4], w[51], pars->GC_11, amp[86]); 
  FFS2_0(w[47], w[52], w[3], pars->GC_74, amp[87]); 
  FFV1_0(w[47], w[14], w[50], pars->GC_11, amp[88]); 
  FFV1_0(w[42], w[4], w[53], pars->GC_11, amp[89]); 
  FFS2_0(w[42], w[52], w[2], pars->GC_74, amp[90]); 
  FFV1_0(w[42], w[21], w[50], pars->GC_11, amp[91]); 
  VVS3_0(w[50], w[45], w[26], pars->GC_13, amp[92]); 
  FFS2_0(w[54], w[4], w[26], pars->GC_74, amp[93]); 
  FFS2_0(w[41], w[52], w[26], pars->GC_74, amp[94]); 
  FFS2_0(w[54], w[21], w[3], pars->GC_74, amp[95]); 
  FFV1_0(w[41], w[21], w[51], pars->GC_11, amp[96]); 
  FFS2_0(w[54], w[14], w[2], pars->GC_74, amp[97]); 
  FFV1_0(w[41], w[14], w[53], pars->GC_11, amp[98]); 
  FFV1_0(w[47], w[40], w[23], pars->GC_11, amp[99]); 
  FFS2_0(w[47], w[55], w[3], pars->GC_74, amp[100]); 
  FFV1_0(w[47], w[46], w[22], pars->GC_11, amp[101]); 
  FFV1_0(w[42], w[40], w[25], pars->GC_11, amp[102]); 
  FFS2_0(w[42], w[55], w[2], pars->GC_74, amp[103]); 
  FFV1_0(w[42], w[49], w[22], pars->GC_11, amp[104]); 
  VVS3_0(w[22], w[44], w[26], pars->GC_13, amp[105]); 
  FFS2_0(w[56], w[40], w[26], pars->GC_74, amp[106]); 
  FFS2_0(w[41], w[55], w[26], pars->GC_74, amp[107]); 
  FFS2_0(w[56], w[49], w[3], pars->GC_74, amp[108]); 
  FFV1_0(w[41], w[49], w[23], pars->GC_11, amp[109]); 
  FFS2_0(w[56], w[46], w[2], pars->GC_74, amp[110]); 
  FFV1_0(w[41], w[46], w[25], pars->GC_11, amp[111]); 
  FFV1_0(w[47], w[4], w[57], pars->GC_11, amp[112]); 
  FFV1_0(w[47], w[40], w[33], pars->GC_11, amp[113]); 
  FFV1_0(w[42], w[4], w[58], pars->GC_11, amp[114]); 
  FFV1_0(w[42], w[40], w[35], pars->GC_11, amp[115]); 
  FFS2_0(w[59], w[4], w[26], pars->GC_74, amp[116]); 
  FFV1_0(w[38], w[4], w[44], pars->GC_11, amp[117]); 
  FFS2_0(w[59], w[21], w[3], pars->GC_74, amp[118]); 
  VVS3_0(w[44], w[35], w[3], pars->GC_13, amp[119]); 
  FFS2_0(w[59], w[14], w[2], pars->GC_74, amp[120]); 
  VVS3_0(w[44], w[33], w[2], pars->GC_13, amp[121]); 
  FFS2_0(w[60], w[40], w[26], pars->GC_74, amp[122]); 
  FFV1_0(w[38], w[40], w[45], pars->GC_11, amp[123]); 
  FFS2_0(w[60], w[49], w[3], pars->GC_74, amp[124]); 
  VVS3_0(w[45], w[58], w[3], pars->GC_13, amp[125]); 
  FFS2_0(w[60], w[46], w[2], pars->GC_74, amp[126]); 
  VVS3_0(w[45], w[57], w[2], pars->GC_13, amp[127]); 
  FFV1_0(w[41], w[14], w[58], pars->GC_11, amp[128]); 
  FFV1_0(w[41], w[49], w[33], pars->GC_11, amp[129]); 
  FFV1_0(w[41], w[46], w[35], pars->GC_11, amp[130]); 
  FFV1_0(w[41], w[21], w[57], pars->GC_11, amp[131]); 
  FFV1_0(w[42], w[40], w[64], pars->GC_11, amp[132]); 
  FFV1_0(w[42], w[61], w[65], pars->GC_11, amp[133]); 
  FFV1_0(w[67], w[40], w[66], pars->GC_11, amp[134]); 
  VVS3_0(w[66], w[65], w[3], pars->GC_13, amp[135]); 
  FFV1_0(w[63], w[46], w[66], pars->GC_11, amp[136]); 
  FFV1_0(w[67], w[61], w[44], pars->GC_11, amp[137]); 
  VVS3_0(w[44], w[64], w[3], pars->GC_13, amp[138]); 
  FFV1_0(w[63], w[68], w[44], pars->GC_11, amp[139]); 
  FFV1_0(w[41], w[68], w[65], pars->GC_11, amp[140]); 
  FFV1_0(w[41], w[46], w[64], pars->GC_11, amp[141]); 
  FFV1_0(w[47], w[40], w[70], pars->GC_11, amp[142]); 
  FFV1_0(w[47], w[61], w[71], pars->GC_11, amp[143]); 
  FFV1_0(w[72], w[40], w[66], pars->GC_11, amp[144]); 
  VVS3_0(w[66], w[71], w[2], pars->GC_13, amp[145]); 
  FFV1_0(w[69], w[49], w[66], pars->GC_11, amp[146]); 
  FFV1_0(w[72], w[61], w[44], pars->GC_11, amp[147]); 
  VVS3_0(w[44], w[70], w[2], pars->GC_13, amp[148]); 
  FFV1_0(w[69], w[73], w[44], pars->GC_11, amp[149]); 
  FFV1_0(w[41], w[73], w[71], pars->GC_11, amp[150]); 
  FFV1_0(w[41], w[49], w[70], pars->GC_11, amp[151]); 
  FFV1_0(w[47], w[40], w[75], pars->GC_11, amp[152]); 
  FFS2_0(w[47], w[76], w[3], pars->GC_74, amp[153]); 
  FFV1_0(w[47], w[46], w[74], pars->GC_11, amp[154]); 
  FFV1_0(w[42], w[40], w[77], pars->GC_11, amp[155]); 
  FFS2_0(w[42], w[76], w[2], pars->GC_74, amp[156]); 
  FFV1_0(w[42], w[49], w[74], pars->GC_11, amp[157]); 
  VVS3_0(w[74], w[44], w[26], pars->GC_13, amp[158]); 
  FFS2_0(w[78], w[40], w[26], pars->GC_74, amp[159]); 
  FFS2_0(w[41], w[76], w[26], pars->GC_74, amp[160]); 
  FFS2_0(w[78], w[49], w[3], pars->GC_74, amp[161]); 
  FFV1_0(w[41], w[49], w[75], pars->GC_11, amp[162]); 
  FFS2_0(w[78], w[46], w[2], pars->GC_74, amp[163]); 
  FFV1_0(w[41], w[46], w[77], pars->GC_11, amp[164]); 
  FFV1_0(w[47], w[61], w[80], pars->GC_11, amp[165]); 
  FFS2_0(w[47], w[81], w[3], pars->GC_74, amp[166]); 
  FFV1_0(w[47], w[68], w[79], pars->GC_11, amp[167]); 
  FFV1_0(w[42], w[61], w[82], pars->GC_11, amp[168]); 
  FFS2_0(w[42], w[81], w[2], pars->GC_74, amp[169]); 
  FFV1_0(w[42], w[73], w[79], pars->GC_11, amp[170]); 
  VVS3_0(w[79], w[66], w[26], pars->GC_13, amp[171]); 
  FFS2_0(w[83], w[61], w[26], pars->GC_74, amp[172]); 
  FFS2_0(w[41], w[81], w[26], pars->GC_74, amp[173]); 
  FFS2_0(w[83], w[73], w[3], pars->GC_74, amp[174]); 
  FFV1_0(w[41], w[73], w[80], pars->GC_11, amp[175]); 
  FFS2_0(w[83], w[68], w[2], pars->GC_74, amp[176]); 
  FFV1_0(w[41], w[68], w[82], pars->GC_11, amp[177]); 
  FFV1_0(w[47], w[40], w[84], pars->GC_11, amp[178]); 
  FFV1_0(w[47], w[61], w[85], pars->GC_11, amp[179]); 
  FFV1_0(w[42], w[40], w[86], pars->GC_11, amp[180]); 
  FFV1_0(w[42], w[61], w[87], pars->GC_11, amp[181]); 
  FFS2_0(w[88], w[40], w[26], pars->GC_74, amp[182]); 
  FFV1_0(w[89], w[40], w[66], pars->GC_11, amp[183]); 
  FFS2_0(w[88], w[49], w[3], pars->GC_74, amp[184]); 
  VVS3_0(w[66], w[87], w[3], pars->GC_13, amp[185]); 
  FFS2_0(w[88], w[46], w[2], pars->GC_74, amp[186]); 
  VVS3_0(w[66], w[85], w[2], pars->GC_13, amp[187]); 
  FFS2_0(w[90], w[61], w[26], pars->GC_74, amp[188]); 
  FFV1_0(w[89], w[61], w[44], pars->GC_11, amp[189]); 
  FFS2_0(w[90], w[73], w[3], pars->GC_74, amp[190]); 
  VVS3_0(w[44], w[86], w[3], pars->GC_13, amp[191]); 
  FFS2_0(w[90], w[68], w[2], pars->GC_74, amp[192]); 
  VVS3_0(w[44], w[84], w[2], pars->GC_13, amp[193]); 
  FFV1_0(w[41], w[46], w[86], pars->GC_11, amp[194]); 
  FFV1_0(w[41], w[73], w[85], pars->GC_11, amp[195]); 
  FFV1_0(w[41], w[68], w[87], pars->GC_11, amp[196]); 
  FFV1_0(w[41], w[49], w[84], pars->GC_11, amp[197]); 


}
double PY8MEs_R7_P26_heft_bb_hhbb::matrix_7_bb_hhbb() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 66;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[0] + amp[1] + amp[2] + amp[3] + amp[4] +
      1./3. * amp[5] + 1./3. * amp[6] + 1./3. * amp[7] + amp[8] + 1./3. *
      amp[9] + 1./3. * amp[10] + amp[11] + amp[12] + amp[13] + amp[14] + 1./3.
      * amp[15] + 1./3. * amp[16] + 1./3. * amp[17] + amp[18] + 1./3. * amp[19]
      + 1./3. * amp[20] + 1./3. * amp[21] + 1./3. * amp[22] + 1./3. * amp[23] +
      1./3. * amp[24] + 1./3. * amp[25] + 1./3. * amp[26] + 1./3. * amp[27] +
      1./3. * amp[28] + 1./3. * amp[29] + 1./3. * amp[30] + 1./3. * amp[31] +
      1./3. * amp[32] + amp[33] + amp[34] + amp[35] + amp[36] + amp[37] +
      amp[38] + amp[39] + amp[40] + amp[41] + amp[42] + amp[43] + amp[44] +
      amp[45] + 1./3. * amp[46] + amp[47] + 1./3. * amp[48] + amp[49] + amp[50]
      + amp[51] + amp[52] + amp[53] + amp[54] + amp[55] + 1./3. * amp[56] +
      1./3. * amp[57] + 1./3. * amp[58] + 1./3. * amp[59] + 1./3. * amp[60] +
      1./3. * amp[61] + 1./3. * amp[62] + amp[63] + amp[64] + 1./3. * amp[65]);
  jamp[1] = +1./2. * (-amp[0] - 1./3. * amp[1] - 1./3. * amp[2] - 1./3. *
      amp[3] - 1./3. * amp[4] - amp[5] - amp[6] - amp[7] - 1./3. * amp[8] -
      amp[9] - amp[10] - 1./3. * amp[11] - 1./3. * amp[12] - 1./3. * amp[13] -
      1./3. * amp[14] - amp[15] - amp[16] - amp[17] - 1./3. * amp[18] - amp[19]
      - amp[20] - amp[21] - amp[22] - amp[23] - amp[24] - amp[25] - amp[26] -
      amp[27] - amp[28] - amp[29] - amp[30] - amp[31] - amp[32] - 1./3. *
      amp[33] - 1./3. * amp[34] - 1./3. * amp[35] - 1./3. * amp[36] - 1./3. *
      amp[37] - 1./3. * amp[38] - 1./3. * amp[39] - 1./3. * amp[40] - 1./3. *
      amp[41] - 1./3. * amp[42] - 1./3. * amp[43] - 1./3. * amp[44] - 1./3. *
      amp[45] - amp[46] - 1./3. * amp[47] - amp[48] - 1./3. * amp[49] - 1./3. *
      amp[50] - 1./3. * amp[51] - 1./3. * amp[52] - 1./3. * amp[53] - 1./3. *
      amp[54] - 1./3. * amp[55] - amp[56] - amp[57] - amp[58] - amp[59] -
      amp[60] - amp[61] - amp[62] - 1./3. * amp[63] - 1./3. * amp[64] -
      amp[65]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[0][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R7_P26_heft_bb_hhbb::matrix_7_bbx_hhbbx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 66;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[66] + amp[67] + amp[68] + amp[69] + amp[70]
      + 1./3. * amp[71] + 1./3. * amp[72] + 1./3. * amp[73] + amp[74] + 1./3. *
      amp[75] + 1./3. * amp[76] + amp[77] + amp[78] + amp[79] + amp[80] + 1./3.
      * amp[81] + 1./3. * amp[82] + 1./3. * amp[83] + amp[84] + 1./3. * amp[85]
      + 1./3. * amp[86] + 1./3. * amp[87] + 1./3. * amp[88] + 1./3. * amp[89] +
      1./3. * amp[90] + 1./3. * amp[91] + 1./3. * amp[92] + 1./3. * amp[93] +
      1./3. * amp[94] + 1./3. * amp[95] + 1./3. * amp[96] + 1./3. * amp[97] +
      1./3. * amp[98] + amp[99] + amp[100] + amp[101] + amp[102] + amp[103] +
      amp[104] + amp[105] + amp[106] + amp[107] + amp[108] + amp[109] +
      amp[110] + amp[111] + 1./3. * amp[112] + amp[113] + 1./3. * amp[114] +
      amp[115] + amp[116] + amp[117] + amp[118] + amp[119] + amp[120] +
      amp[121] + 1./3. * amp[122] + 1./3. * amp[123] + 1./3. * amp[124] + 1./3.
      * amp[125] + 1./3. * amp[126] + 1./3. * amp[127] + 1./3. * amp[128] +
      amp[129] + amp[130] + 1./3. * amp[131]);
  jamp[1] = +1./2. * (-amp[66] - 1./3. * amp[67] - 1./3. * amp[68] - 1./3. *
      amp[69] - 1./3. * amp[70] - amp[71] - amp[72] - amp[73] - 1./3. * amp[74]
      - amp[75] - amp[76] - 1./3. * amp[77] - 1./3. * amp[78] - 1./3. * amp[79]
      - 1./3. * amp[80] - amp[81] - amp[82] - amp[83] - 1./3. * amp[84] -
      amp[85] - amp[86] - amp[87] - amp[88] - amp[89] - amp[90] - amp[91] -
      amp[92] - amp[93] - amp[94] - amp[95] - amp[96] - amp[97] - amp[98] -
      1./3. * amp[99] - 1./3. * amp[100] - 1./3. * amp[101] - 1./3. * amp[102]
      - 1./3. * amp[103] - 1./3. * amp[104] - 1./3. * amp[105] - 1./3. *
      amp[106] - 1./3. * amp[107] - 1./3. * amp[108] - 1./3. * amp[109] - 1./3.
      * amp[110] - 1./3. * amp[111] - amp[112] - 1./3. * amp[113] - amp[114] -
      1./3. * amp[115] - 1./3. * amp[116] - 1./3. * amp[117] - 1./3. * amp[118]
      - 1./3. * amp[119] - 1./3. * amp[120] - 1./3. * amp[121] - amp[122] -
      amp[123] - amp[124] - amp[125] - amp[126] - amp[127] - amp[128] - 1./3. *
      amp[129] - 1./3. * amp[130] - amp[131]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[1][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R7_P26_heft_bb_hhbb::matrix_7_bxbx_hhbxbx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 66;
  const int ncolor = 2; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1}; 
  static const double cf[ncolor][ncolor] = {{9, 3}, {3, 9}}; 

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[132] + amp[133] + amp[134] + amp[135] +
      amp[136] + 1./3. * amp[137] + 1./3. * amp[138] + 1./3. * amp[139] +
      amp[140] + 1./3. * amp[141] + 1./3. * amp[142] + amp[143] + amp[144] +
      amp[145] + amp[146] + 1./3. * amp[147] + 1./3. * amp[148] + 1./3. *
      amp[149] + amp[150] + 1./3. * amp[151] + 1./3. * amp[152] + 1./3. *
      amp[153] + 1./3. * amp[154] + 1./3. * amp[155] + 1./3. * amp[156] + 1./3.
      * amp[157] + 1./3. * amp[158] + 1./3. * amp[159] + 1./3. * amp[160] +
      1./3. * amp[161] + 1./3. * amp[162] + 1./3. * amp[163] + 1./3. * amp[164]
      + amp[165] + amp[166] + amp[167] + amp[168] + amp[169] + amp[170] +
      amp[171] + amp[172] + amp[173] + amp[174] + amp[175] + amp[176] +
      amp[177] + 1./3. * amp[178] + amp[179] + 1./3. * amp[180] + amp[181] +
      amp[182] + amp[183] + amp[184] + amp[185] + amp[186] + amp[187] + 1./3. *
      amp[188] + 1./3. * amp[189] + 1./3. * amp[190] + 1./3. * amp[191] + 1./3.
      * amp[192] + 1./3. * amp[193] + 1./3. * amp[194] + amp[195] + amp[196] +
      1./3. * amp[197]);
  jamp[1] = +1./2. * (-amp[132] - 1./3. * amp[133] - 1./3. * amp[134] - 1./3. *
      amp[135] - 1./3. * amp[136] - amp[137] - amp[138] - amp[139] - 1./3. *
      amp[140] - amp[141] - amp[142] - 1./3. * amp[143] - 1./3. * amp[144] -
      1./3. * amp[145] - 1./3. * amp[146] - amp[147] - amp[148] - amp[149] -
      1./3. * amp[150] - amp[151] - amp[152] - amp[153] - amp[154] - amp[155] -
      amp[156] - amp[157] - amp[158] - amp[159] - amp[160] - amp[161] -
      amp[162] - amp[163] - amp[164] - 1./3. * amp[165] - 1./3. * amp[166] -
      1./3. * amp[167] - 1./3. * amp[168] - 1./3. * amp[169] - 1./3. * amp[170]
      - 1./3. * amp[171] - 1./3. * amp[172] - 1./3. * amp[173] - 1./3. *
      amp[174] - 1./3. * amp[175] - 1./3. * amp[176] - 1./3. * amp[177] -
      amp[178] - 1./3. * amp[179] - amp[180] - 1./3. * amp[181] - 1./3. *
      amp[182] - 1./3. * amp[183] - 1./3. * amp[184] - 1./3. * amp[185] - 1./3.
      * amp[186] - 1./3. * amp[187] - amp[188] - amp[189] - amp[190] - amp[191]
      - amp[192] - amp[193] - amp[194] - 1./3. * amp[195] - 1./3. * amp[196] -
      amp[197]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[2][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}


}  // end namespace PY8MEs_namespace

