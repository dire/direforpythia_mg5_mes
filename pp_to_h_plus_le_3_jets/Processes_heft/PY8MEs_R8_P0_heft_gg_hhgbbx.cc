//==========================================================================
// This file has been automatically generated for Pythia 8 by
// MadGraph5_aMC@NLO v. 2.6.0, 2017-08-16
// By the MadGraph5_aMC@NLO Development Team
// Visit launchpad.net/madgraph5 and amcatnlo.web.cern.ch
//==========================================================================

#include "PY8MEs_R8_P0_heft_gg_hhgbbx.h"
#include "HelAmps_heft.h"

using namespace Pythia8_heft; 

namespace PY8MEs_namespace 
{
//==========================================================================
// Class member functions for calculating the matrix elements for
// Process: g g > h h g b b~ HIG<=1 HIW<=1 WEIGHTED<=7 @8

// Exception class
class PY8MEs_R8_P0_heft_gg_hhgbbxException : public exception
{
  virtual const char * what() const throw()
  {
    return "Exception in class 'PY8MEs_R8_P0_heft_gg_hhgbbx'."; 
  }
}
PY8MEs_R8_P0_heft_gg_hhgbbx_exception; 

std::set<int> PY8MEs_R8_P0_heft_gg_hhgbbx::s_channel_proc = std::set<int>
    (createset<int> ());

int PY8MEs_R8_P0_heft_gg_hhgbbx::helicities[ncomb][nexternal] = {{-1, -1, 0, 0,
    -1, -1, -1}, {-1, -1, 0, 0, -1, -1, 1}, {-1, -1, 0, 0, -1, 1, -1}, {-1, -1,
    0, 0, -1, 1, 1}, {-1, -1, 0, 0, 1, -1, -1}, {-1, -1, 0, 0, 1, -1, 1}, {-1,
    -1, 0, 0, 1, 1, -1}, {-1, -1, 0, 0, 1, 1, 1}, {-1, 1, 0, 0, -1, -1, -1},
    {-1, 1, 0, 0, -1, -1, 1}, {-1, 1, 0, 0, -1, 1, -1}, {-1, 1, 0, 0, -1, 1,
    1}, {-1, 1, 0, 0, 1, -1, -1}, {-1, 1, 0, 0, 1, -1, 1}, {-1, 1, 0, 0, 1, 1,
    -1}, {-1, 1, 0, 0, 1, 1, 1}, {1, -1, 0, 0, -1, -1, -1}, {1, -1, 0, 0, -1,
    -1, 1}, {1, -1, 0, 0, -1, 1, -1}, {1, -1, 0, 0, -1, 1, 1}, {1, -1, 0, 0, 1,
    -1, -1}, {1, -1, 0, 0, 1, -1, 1}, {1, -1, 0, 0, 1, 1, -1}, {1, -1, 0, 0, 1,
    1, 1}, {1, 1, 0, 0, -1, -1, -1}, {1, 1, 0, 0, -1, -1, 1}, {1, 1, 0, 0, -1,
    1, -1}, {1, 1, 0, 0, -1, 1, 1}, {1, 1, 0, 0, 1, -1, -1}, {1, 1, 0, 0, 1,
    -1, 1}, {1, 1, 0, 0, 1, 1, -1}, {1, 1, 0, 0, 1, 1, 1}};

// Normalization factors the various processes
// Denominators: spins, colors and identical particles
int PY8MEs_R8_P0_heft_gg_hhgbbx::denom_colors[nprocesses] = {64}; 
int PY8MEs_R8_P0_heft_gg_hhgbbx::denom_hels[nprocesses] = {4}; 
int PY8MEs_R8_P0_heft_gg_hhgbbx::denom_iden[nprocesses] = {2}; 

//--------------------------------------------------------------------------
// Color config initialization
void PY8MEs_R8_P0_heft_gg_hhgbbx::initColorConfigs() 
{
  color_configs = vector < vec_vec_int > (); 
  jamp_nc_relative_power = vector < vec_int > (); 

  // Color flows of process Process: g g > h h g b b~ HIG<=1 HIW<=1 WEIGHTED<=7
  // @8
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(2)(2)(3)(0)(0)(0)(0)(4)(3)(1)(0)(0)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #1
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(2)(4)(3)(0)(0)(0)(0)(4)(2)(1)(0)(0)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #2
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(2)(1)(3)(0)(0)(0)(0)(4)(2)(1)(0)(0)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #3
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(2)(1)(3)(0)(0)(0)(0)(4)(3)(1)(0)(0)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #4
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(2)(2)(3)(0)(0)(0)(0)(4)(1)(1)(0)(0)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #5
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(2)(4)(3)(0)(0)(0)(0)(4)(1)(1)(0)(0)(2)));
  jamp_nc_relative_power[0].push_back(0); 
}

//--------------------------------------------------------------------------
// Destructor.
PY8MEs_R8_P0_heft_gg_hhgbbx::~PY8MEs_R8_P0_heft_gg_hhgbbx() 
{
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    delete[] p[i]; 
    p[i] = NULL; 
  }
}

//--------------------------------------------------------------------------
// Invert the permutation mapping
vector<int> PY8MEs_R8_P0_heft_gg_hhgbbx::invert_mapping(vector<int> mapping) 
{
  vector<int> inverted_mapping; 
  for (unsigned int i = 0; i < mapping.size(); i++ )
  {
    for (unsigned int j = 0; j < mapping.size(); j++ )
    {
      if (mapping[j] == ((int)i))
      {
        inverted_mapping.push_back(j); 
        break; 
      }
    }
  }
  return inverted_mapping; 
}

//--------------------------------------------------------------------------
// Return the list of possible helicity configurations
vector < vec_int > PY8MEs_R8_P0_heft_gg_hhgbbx::getHelicityConfigs(vector<int>
    permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(ncomb, vector<int> (nexternal, 0)); 
  for (unsigned int ihel = 0; ihel < ncomb; ihel++ )
  {
    for(unsigned int j = 0; j < nexternal; j++ )
    {
      res[ihel][chosenPerm[j]] = helicities[ihel][j]; 
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Return the list of possible color configurations
vector < vec_int > PY8MEs_R8_P0_heft_gg_hhgbbx::getColorConfigs(int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(color_configs[chosenProcID].size(), vector<int>
      (nexternal * 2, 0));
  for (unsigned int icol = 0; icol < color_configs[chosenProcID].size(); icol++
      )
  {
    for(unsigned int j = 0; j < (2 * nexternal); j++ )
    {
      res[icol][chosenPerm[j/2] * 2 + j%2] =
          color_configs[chosenProcID][icol][j];
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Get JAMP relative N_c power
int PY8MEs_R8_P0_heft_gg_hhgbbx::getColorFlowRelativeNCPower(int color_flow_ID,
    int specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return jamp_nc_relative_power[chosenProcID][color_flow_ID]; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity ID -> Helicity Config
vector<int> PY8MEs_R8_P0_heft_gg_hhgbbx::getHelicityConfigForID(int hel_ID,
    vector<int> permutation)
{
  if (hel_ID < 0 || hel_ID >= ncomb)
  {
    cerr <<  "Error in function 'getHelicityConfigForID' of class" << 
    " 'PY8MEs_R8_P0_heft_gg_hhgbbx': Specified helicity ID '" << 
    hel_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R8_P0_heft_gg_hhgbbx_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(nexternal, 0); 
  for (unsigned int j = 0; j < nexternal; j++ )
  {
    res[chosenPerm[j]] = helicities[hel_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity Config -> Helicity ID
int PY8MEs_R8_P0_heft_gg_hhgbbx::getHelicityIDForConfig(vector<int> hel_config,
    vector<int> permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  int user_ihel = -1; 
  if (hel_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < ncomb; i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < nexternal; j++ )
      {
        if (helicities[i][chosenPerm[j]] != hel_config[j])
        {
          found = false; 
          break; 
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_ihel = i; 
        break; 
      }
    }
    if (user_ihel == -1)
    {
      cerr <<  "Error in function 'getHelicityIDForConfig' of class" << 
      " 'PY8MEs_R8_P0_heft_gg_hhgbbx': Specified helicity" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R8_P0_heft_gg_hhgbbx_exception; 
    }
  }
  return user_ihel; 
}


//--------------------------------------------------------------------------
// Implements the map Color ID -> Color Config
vector<int> PY8MEs_R8_P0_heft_gg_hhgbbx::getColorConfigForID(int color_ID, int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < 0 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getColorConfigForID' of class" << 
    " 'PY8MEs_R8_P0_heft_gg_hhgbbx': Specified color ID '" << 
    color_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R8_P0_heft_gg_hhgbbx_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(color_configs[chosenProcID][color_ID].size(), 0); 
  for (unsigned int j = 0; j < (2 * nexternal); j++ )
  {
    res[chosenPerm[j/2] * 2 + j%2] = color_configs[chosenProcID][color_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Color Config -> Color ID
int PY8MEs_R8_P0_heft_gg_hhgbbx::getColorIDForConfig(vector<int> color_config,
    int specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = -1; 
  if (color_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < color_configs[chosenProcID].size(); i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < (nexternal * 2); j++ )
      {

        // If colorless then make sure it matches
        // The little arithmetics in the color index is just
        // the permutation applies on the particle list which is
        // twice smaller since each particle can have two color indices.
        if (color_config[j] == 0)
        {
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] != 0)
          {
            found = false; 
            break; 
          }
          // Otherwise check that the color linked position matches
        }
        else
        {
          int color_linked_pos = -1; 
          // Find the other end of the line in the user color config
          for (unsigned int k = 0; k < (nexternal * 2); k++ )
          {
            if (k == j)
              continue; 
            if (color_config[j] == color_config[k])
            {
              color_linked_pos = k; 
              break; 
            }
          }
          if (color_linked_pos == -1)
          {
            cerr <<  "Error in function 'getColorIDForConfig' of class" << 
            " 'PY8MEs_R8_P0_heft_gg_hhgbbx': A color line could " << 
            " not be closed." << endl; 
            throw PY8MEs_R8_P0_heft_gg_hhgbbx_exception; 
          }
          // Now check whether the color line matches
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] !=
              color_configs[chosenProcID][i][chosenPerm[color_linked_pos/2] * 2
              + color_linked_pos%2])
          {
            found = false; 
            break; 
          }
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_icol = i; 
        break; 
      }
    }

    if (user_icol == -1)
    {
      cerr <<  "Error in function 'getColorIDForConfig' of class" << 
      " 'PY8MEs_R8_P0_heft_gg_hhgbbx': Specified color" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R8_P0_heft_gg_hhgbbx_exception; 
    }
  }
  return user_icol; 
}

//--------------------------------------------------------------------------
// Returns all result previously computed in SigmaKin
vector < vec_double > PY8MEs_R8_P0_heft_gg_hhgbbx::getAllResults(int
    specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return all_results[chosenProcID]; 
}

//--------------------------------------------------------------------------
// Returns a result previously computed in SigmaKin for a specific helicity
// and color ID. -1 means avg and summed over that characteristic.
double PY8MEs_R8_P0_heft_gg_hhgbbx::getResult(int helicity_ID, int color_ID,
    int specify_proc_ID)
{
  if (helicity_ID < - 1 || helicity_ID >= ncomb)
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R8_P0_heft_gg_hhgbbx': Specified helicity ID '" << 
    helicity_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R8_P0_heft_gg_hhgbbx_exception; 
  }
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < - 1 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R8_P0_heft_gg_hhgbbx': Specified color ID '" << 
    color_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R8_P0_heft_gg_hhgbbx_exception; 
  }
  return all_results[chosenProcID][helicity_ID + 1][color_ID + 1]; 
}

//--------------------------------------------------------------------------
// Check for the availability of the requested process and if available,
// If available, this returns the corresponding permutation and Proc_ID to use.
// If not available, this returns a negative Proc_ID.
pair < vector<int> , int > PY8MEs_R8_P0_heft_gg_hhgbbx::static_getPY8ME(vector<int> initial_pdgs, vector<int> final_pdgs, set<int> schannels) 
{

  // Not available return value
  pair < vector<int> , int > NA(vector<int> (), -1); 

  // Check if s-channel requirements match
  if (nreq_s_channels > 0)
  {
    if (schannels != s_channel_proc)
      return NA; 
  }
  else
  {
    if (schannels.size() != 0)
      return NA; 
  }

  // Check number of final state particles
  if (final_pdgs.size() != (nexternal - ninitial))
    return NA; 

  // Check number of initial state particles
  if (initial_pdgs.size() != ninitial)
    return NA; 

  // List of processes available in this class
  const int nprocs = 1; 
  const int proc_IDS[nprocs] = {0}; 
  const int in_pdgs[nprocs][ninitial] = {{21, 21}}; 
  const int out_pdgs[nprocs][nexternal - ninitial] = {{25, 25, 21, 5, -5}}; 

  bool in_pdgs_used[ninitial]; 
  bool out_pdgs_used[nexternal - ninitial]; 
  for(unsigned int i = 0; i < nprocs; i++ )
  {
    int permutations[nexternal]; 

    // Reinitialize initial state look-up variables
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      in_pdgs_used[j] = false; 
      permutations[j] = -1; 
    }
    // Look for initial state matches
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      for(unsigned int k = 0; k < ninitial; k++ )
      {
        // Make sure it has not been used already
        if (in_pdgs_used[k])
          continue; 
        if (initial_pdgs[k] == in_pdgs[i][j])
        {
          permutations[j] = k; 
          in_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[ninitial - 1] == -1)
      continue; 

    // Reinitialize final state look-up variables
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      out_pdgs_used[j] = false; 
      permutations[ninitial + j] = -1; 
    }
    // Look for final state matches
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      for(unsigned int k = 0; k < (nexternal - ninitial); k++ )
      {
        // Make sure it has not been used already
        if (out_pdgs_used[k])
          continue; 
        if (final_pdgs[k] == out_pdgs[i][j])
        {
          permutations[ninitial + j] = ninitial + k; 
          out_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[ninitial + j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[nexternal - 1] == -1)
      continue; 

    // Return process found
    return pair < vector<int> , int > (vector<int> (permutations, permutations
        + nexternal), proc_IDS[i]);
  }

  // No process found
  return NA; 
}

//--------------------------------------------------------------------------
// Set momenta
void PY8MEs_R8_P0_heft_gg_hhgbbx::setMomenta(vector < vec_double >
    momenta_picked)
{
  if (momenta_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setMomenta' of class" << 
    " 'PY8MEs_R8_P0_heft_gg_hhgbbx': Incorrect number of" << 
    " momenta specified." << endl; 
    throw PY8MEs_R8_P0_heft_gg_hhgbbx_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    if (momenta_picked[i].size() != 4)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R8_P0_heft_gg_hhgbbx': Incorrect number of" << 
      " momenta components specified." << endl; 
      throw PY8MEs_R8_P0_heft_gg_hhgbbx_exception; 
    }
    if (momenta_picked[i][0] < 0.0)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R8_P0_heft_gg_hhgbbx': A momentum was specified" << 
      " with negative energy. Check conventions." << endl; 
      throw PY8MEs_R8_P0_heft_gg_hhgbbx_exception; 
    }
    for (unsigned int j = 0; j < 4; j++ )
    {
      p[i][j] = momenta_picked[i][j]; 
    }
  }
}

//--------------------------------------------------------------------------
// Set color configuration to use. An empty vector means sum over all.
void PY8MEs_R8_P0_heft_gg_hhgbbx::setColors(vector<int> colors_picked)
{
  if (colors_picked.size() == 0)
  {
    user_colors = vector<int> (); 
    return; 
  }
  if (colors_picked.size() != (2 * nexternal))
  {
    cerr <<  "Error in function 'setColors' of class" << 
    " 'PY8MEs_R8_P0_heft_gg_hhgbbx': Incorrect number" << 
    " of colors specified." << endl; 
    throw PY8MEs_R8_P0_heft_gg_hhgbbx_exception; 
  }
  user_colors = vector<int> ((2 * nexternal), 0); 
  for(unsigned int i = 0; i < (2 * nexternal); i++ )
  {
    user_colors[i] = colors_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the helicity configuration to use. Am empty vector means sum over all.
void PY8MEs_R8_P0_heft_gg_hhgbbx::setHelicities(vector<int> helicities_picked) 
{
  if (helicities_picked.size() != nexternal)
  {
    if (helicities_picked.size() == 0)
    {
      user_helicities = vector<int> (); 
      return; 
    }
    cerr <<  "Error in function 'setHelicities' of class" << 
    " 'PY8MEs_R8_P0_heft_gg_hhgbbx': Incorrect number" << 
    " of helicities specified." << endl; 
    throw PY8MEs_R8_P0_heft_gg_hhgbbx_exception; 
  }
  user_helicities = vector<int> (nexternal, 0); 
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    user_helicities[i] = helicities_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the permutation to use (will apply to momenta, colors and helicities)
void PY8MEs_R8_P0_heft_gg_hhgbbx::setPermutation(vector<int> perm_picked) 
{
  if (perm_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setPermutations' of class" << 
    " 'PY8MEs_R8_P0_heft_gg_hhgbbx': Incorrect number" << 
    " of permutations specified." << endl; 
    throw PY8MEs_R8_P0_heft_gg_hhgbbx_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    perm[i] = perm_picked[i]; 
  }
}

// Set the proc_ID to use
void PY8MEs_R8_P0_heft_gg_hhgbbx::setProcID(int procID_picked) 
{
  proc_ID = procID_picked; 
}

//--------------------------------------------------------------------------
// Initialize process.

void PY8MEs_R8_P0_heft_gg_hhgbbx::initProc() 
{

  // Initialize flags
  include_symmetry_factors = true; 
  include_helicity_averaging_factors = true; 
  include_color_averaging_factors = true; 

  // Initialize vectors.
  perm = vector<int> (nexternal, 0); 
  user_colors = vector<int> (2 * nexternal, 0); 
  user_helicities = vector<int> (nexternal, 0); 
  p = vector < double * > (); 
  for (unsigned int i = 0; i < nexternal; i++ )
  {
    p.push_back(new double[4]); 
  }
  initColorConfigs(); 
  // Synchronize local variables dependent on the model with the active model.
  mME = vector<double> (nexternal, 0.); 
  syncProcModelParams(); 
  jamp2 = vector < vec_double > (1); 
  jamp2[0] = vector<double> (6, 0.); 
  all_results = vector < vec_vec_double > (1); 
  // The first entry is always the color or helicity avg/summed matrix element.
  all_results[0] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
}

// Synchronize local variables of the process that depend on the model
// parameters
void PY8MEs_R8_P0_heft_gg_hhgbbx::syncProcModelParams() 
{

  // Instantiate the model class and set parameters that stay fixed during run
  mME[0] = pars->ZERO; 
  mME[1] = pars->ZERO; 
  mME[2] = pars->mdl_MH; 
  mME[3] = pars->mdl_MH; 
  mME[4] = pars->ZERO; 
  mME[5] = pars->mdl_MB; 
  mME[6] = pars->mdl_MB; 
}

//--------------------------------------------------------------------------
// Setter allowing to force particular values for the external masses
void PY8MEs_R8_P0_heft_gg_hhgbbx::setMasses(vec_double external_masses) 
{

  if (external_masses.size() != mME.size())
  {
    cerr <<  "Error in function 'setMasses' of class" << 
    " 'PY8MEs_R8_P0_heft_gg_hhgbbx': Incorrect number of" << 
    " masses specified." << endl; 
    throw PY8MEs_R8_P0_heft_gg_hhgbbx_exception; 
  }
  for (unsigned int j = 0; j < mME.size(); j++ )
  {
    mME[j] = external_masses[perm[j]]; 
  }
}

//--------------------------------------------------------------------------
// Getter accessing external masses with the correct ordering
vector<double> PY8MEs_R8_P0_heft_gg_hhgbbx::getMasses() 
{

  vec_double external_masses; 
  vector<int> invertedPerm; 
  invertedPerm = invert_mapping(perm); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    external_masses.push_back(mME[invertedPerm[i]]); 
  }
  return external_masses; 

}


// Set all values of the external masses to float(-mode) where mode can be
// 0 : Mass taken from the model
// 1 : Mass taken from p_i^2 if not massless to begin with
// 2 : Mass always taken from p_i^2.
void PY8MEs_R8_P0_heft_gg_hhgbbx::setExternalMassesMode(int mode) 
{
  if (mode != 0 && mode != 1 && mode != 2)
  {
    cerr <<  "Error in function 'setExternalMassesMode' of class" << 
    " 'PY8MEs_R8_P0_heft_gg_hhgbbx': Incorrect mode selected :" << mode << 
    ". It must be either 0, 1 or 2" << endl; 
    throw PY8MEs_R8_P0_heft_gg_hhgbbx_exception; 
  }
  if (mode == 0)
  {
    syncProcModelParams(); 
  }
  else if (mode == 1)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      if (mME[j] != pars->ZERO)
      {
        mME[j] = -1.0; 
      }
    }
  }
  else if (mode == 2)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      mME[j] = -1.0; 
    }
  }
}

//--------------------------------------------------------------------------
// Evaluate the squared matrix element.

double PY8MEs_R8_P0_heft_gg_hhgbbx::sigmaKin() 
{
  // Set the parameters which change event by event
  pars->setDependentParameters(); 
  pars->setDependentCouplings(); 
  // Reset color flows
  for(int i = 0; i < 6; i++ )
    jamp2[0][i] = 0.; 

  // Save previous values of mME
  vector<double> saved_mME(mME.size(), 0.0); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (mME[i] < 0.0)
    {
      saved_mME[i] = mME[i]; 
      mME[i] = sqrt(abs(pow(p[perm[i]][0], 2) - 
      (pow(p[perm[i]][1], 2) + pow(p[perm[i]][2], 2) + pow(p[perm[i]][3],
          2))));
    }
  }

  // Local variables and constants
  const int max_tries = 10; 
  vector < vec_bool > goodhel(nprocesses, vec_bool(ncomb, false)); 
  vec_int ntry(nprocesses, 0); 
  double t = 0.; 
  double result = 0.; 

  if (ntry[proc_ID] <= max_tries)
    ntry[proc_ID] = ntry[proc_ID] + 1; 

  // Find which helicity configuration is asked for
  // -1 indicates one wants to sum over helicities
  int user_ihel = getHelicityIDForConfig(user_helicities); 

  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = getColorIDForConfig(user_colors); 

  // Reset the list of results that will be recomputed here
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      all_results[proc_ID][ihel + 1][icolor + 1] = 0.; 
    }
  }

  // Calculate the matrix element for all helicities
  // unless already detected as vanishing
  for(int ihel = 0; ihel < ncomb; ihel++ )
  {
    // Skip helicity if already detected as vanishing
    if ((ntry[proc_ID] >= max_tries) && !goodhel[proc_ID][ihel])
      continue; 

    // Also skip helicity if user asks for a specific one
    if ((ntry[proc_ID] >= max_tries) && user_ihel != -1 && user_ihel != ihel)
      continue; 

    calculate_wavefunctions(helicities[ihel]); 

    // Reset locally computed color flows
    for(int i = 0; i < 6; i++ )
      jamp2[0][i] = 0.; 

    if (proc_ID == 0)
      t = matrix_8_gg_hhgbbx(); 

    // Store which helicities give non-zero result
    if ((ntry[proc_ID] < max_tries) && t != 0. && !goodhel[proc_ID][ihel])
      goodhel[proc_ID][ihel] = true; 

    // Aggregate results
    if (user_ihel == -1 || user_ihel == ihel)
    {
      if (user_icol == -1)
      {
        result = result + t; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][0] += t; 
          for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
          {
            all_results[proc_ID][0][i + 1] += jamp2[proc_ID][i]; 
          }
        }
        all_results[proc_ID][ihel + 1][0] += t; 
        for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
        {
          all_results[proc_ID][ihel + 1][i + 1] += jamp2[proc_ID][i]; 
        }
      }
      else
      {
        result = result + jamp2[proc_ID][user_icol]; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][user_icol + 1] += jamp2[proc_ID][user_icol]; 
        }
        all_results[proc_ID][ihel + 1][user_icol + 1] +=
            jamp2[proc_ID][user_icol];
      }
    }

  }

  // Normalize results with the identical particle factor
  if (include_symmetry_factors)
  {
    result = result/denom_iden[proc_ID]; 
  }
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      if (include_symmetry_factors)
      {
        all_results[proc_ID][ihel + 1][icolor + 1] /= denom_iden[proc_ID]; 
      }
    }
  }


  // Normalize when when summing+averaging over helicity configurations
  if (user_ihel == -1 && include_helicity_averaging_factors)
  {
    result /= denom_hels[proc_ID]; 
    if (user_icol == -1)
    {
      all_results[proc_ID][0][0] /= denom_hels[proc_ID]; 
      for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
      {
        all_results[proc_ID][0][i + 1] /= denom_hels[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][0][user_icol + 1] /= denom_hels[proc_ID]; 
    }
  }

  // Normalize when summing+averaging over color configurations
  if (user_icol == -1 && include_color_averaging_factors)
  {
    result /= denom_colors[proc_ID]; 
    if (user_ihel == -1)
    {
      all_results[proc_ID][0][0] /= denom_colors[proc_ID]; 
      for (unsigned int i = 0; i < ncomb; i++ )
      {
        all_results[proc_ID][i + 1][0] /= denom_colors[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][user_ihel + 1][0] /= denom_colors[proc_ID]; 
    }
  }

  // Reinstate previous values of mME
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (saved_mME[i] < 0.0)
    {
      mME[i] = saved_mME[i]; 
    }
  }

  // Finally return it
  return result; 
}

//==========================================================================
// Private class member functions

//--------------------------------------------------------------------------
// Evaluate |M|^2 for each subprocess

void PY8MEs_R8_P0_heft_gg_hhgbbx::calculate_wavefunctions(const int hel[])
{
  // Calculate wavefunctions for all processes
  // Calculate all wavefunctions
  vxxxxx(p[perm[0]], mME[0], hel[0], -1, w[0]); 
  vxxxxx(p[perm[1]], mME[1], hel[1], -1, w[1]); 
  sxxxxx(p[perm[2]], +1, w[2]); 
  sxxxxx(p[perm[3]], +1, w[3]); 
  vxxxxx(p[perm[4]], mME[4], hel[4], +1, w[4]); 
  oxxxxx(p[perm[5]], mME[5], hel[5], +1, w[5]); 
  ixxxxx(p[perm[6]], mME[6], hel[6], -1, w[6]); 
  VVV1P0_1(w[0], w[1], pars->GC_10, pars->ZERO, pars->ZERO, w[7]); 
  SSS1_1(w[2], w[3], pars->GC_69, pars->mdl_MH, pars->mdl_WH, w[8]); 
  VVV1P0_1(w[7], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[9]); 
  FFS2_1(w[5], w[8], pars->GC_74, pars->mdl_MB, pars->ZERO, w[10]); 
  FFS2_2(w[6], w[8], pars->GC_74, pars->mdl_MB, pars->ZERO, w[11]); 
  FFV1_1(w[5], w[7], pars->GC_11, pars->mdl_MB, pars->ZERO, w[12]); 
  VVS3P0_1(w[4], w[8], pars->GC_13, pars->ZERO, pars->ZERO, w[13]); 
  FFV1_2(w[6], w[7], pars->GC_11, pars->mdl_MB, pars->ZERO, w[14]); 
  FFV1_1(w[5], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[15]); 
  VVS3P0_1(w[7], w[8], pars->GC_13, pars->ZERO, pars->ZERO, w[16]); 
  FFV1_1(w[15], w[7], pars->GC_11, pars->mdl_MB, pars->ZERO, w[17]); 
  FFV1_2(w[6], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[18]); 
  FFV1_2(w[18], w[7], pars->GC_11, pars->mdl_MB, pars->ZERO, w[19]); 
  FFV1P0_3(w[6], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[20]); 
  VVV1P0_1(w[7], w[20], pars->GC_10, pars->ZERO, pars->ZERO, w[21]); 
  VVS3P0_1(w[4], w[2], pars->GC_13, pars->ZERO, pars->ZERO, w[22]); 
  FFV1_2(w[6], w[22], pars->GC_11, pars->mdl_MB, pars->ZERO, w[23]); 
  FFV1_1(w[5], w[22], pars->GC_11, pars->mdl_MB, pars->ZERO, w[24]); 
  FFS2_1(w[5], w[3], pars->GC_74, pars->mdl_MB, pars->ZERO, w[25]); 
  VVV1P0_1(w[7], w[22], pars->GC_10, pars->ZERO, pars->ZERO, w[26]); 
  FFV1_1(w[25], w[7], pars->GC_11, pars->mdl_MB, pars->ZERO, w[27]); 
  FFS2_2(w[6], w[3], pars->GC_74, pars->mdl_MB, pars->ZERO, w[28]); 
  FFV1_2(w[28], w[7], pars->GC_11, pars->mdl_MB, pars->ZERO, w[29]); 
  FFS2_1(w[5], w[2], pars->GC_74, pars->mdl_MB, pars->ZERO, w[30]); 
  VVS3P0_1(w[7], w[3], pars->GC_13, pars->ZERO, pars->ZERO, w[31]); 
  FFV1_1(w[30], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[32]); 
  FFV1P0_3(w[6], w[30], pars->GC_11, pars->ZERO, pars->ZERO, w[33]); 
  FFS2_1(w[30], w[3], pars->GC_74, pars->mdl_MB, pars->ZERO, w[34]); 
  VVVS2P0_1(w[7], w[4], w[3], pars->GC_14, pars->ZERO, pars->ZERO, w[35]); 
  VVS3P0_1(w[4], w[3], pars->GC_13, pars->ZERO, pars->ZERO, w[36]); 
  FFV1_1(w[30], w[7], pars->GC_11, pars->mdl_MB, pars->ZERO, w[37]); 
  VVV1P0_1(w[7], w[36], pars->GC_10, pars->ZERO, pars->ZERO, w[38]); 
  FFS2_2(w[6], w[2], pars->GC_74, pars->mdl_MB, pars->ZERO, w[39]); 
  FFV1_2(w[39], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[40]); 
  FFV1P0_3(w[39], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[41]); 
  FFS2_2(w[39], w[3], pars->GC_74, pars->mdl_MB, pars->ZERO, w[42]); 
  FFV1_2(w[39], w[7], pars->GC_11, pars->mdl_MB, pars->ZERO, w[43]); 
  FFV1_2(w[6], w[36], pars->GC_11, pars->mdl_MB, pars->ZERO, w[44]); 
  FFV1_1(w[5], w[36], pars->GC_11, pars->mdl_MB, pars->ZERO, w[45]); 
  VVS3P0_1(w[7], w[2], pars->GC_13, pars->ZERO, pars->ZERO, w[46]); 
  FFV1_1(w[25], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[47]); 
  FFV1P0_3(w[6], w[25], pars->GC_11, pars->ZERO, pars->ZERO, w[48]); 
  FFS2_1(w[25], w[2], pars->GC_74, pars->mdl_MB, pars->ZERO, w[49]); 
  VVVS2P0_1(w[7], w[4], w[2], pars->GC_14, pars->ZERO, pars->ZERO, w[50]); 
  FFV1_2(w[28], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[51]); 
  FFV1P0_3(w[28], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[52]); 
  FFS2_2(w[28], w[2], pars->GC_74, pars->mdl_MB, pars->ZERO, w[53]); 
  FFS2_1(w[15], w[3], pars->GC_74, pars->mdl_MB, pars->ZERO, w[54]); 
  FFS2_1(w[15], w[2], pars->GC_74, pars->mdl_MB, pars->ZERO, w[55]); 
  FFS2_2(w[18], w[3], pars->GC_74, pars->mdl_MB, pars->ZERO, w[56]); 
  FFS2_2(w[18], w[2], pars->GC_74, pars->mdl_MB, pars->ZERO, w[57]); 
  VVS3P0_1(w[0], w[2], pars->GC_13, pars->ZERO, pars->ZERO, w[58]); 
  VVV1P0_1(w[1], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[59]); 
  FFV1_1(w[5], w[58], pars->GC_11, pars->mdl_MB, pars->ZERO, w[60]); 
  FFV1_2(w[6], w[59], pars->GC_11, pars->mdl_MB, pars->ZERO, w[61]); 
  FFV1_2(w[6], w[58], pars->GC_11, pars->mdl_MB, pars->ZERO, w[62]); 
  FFV1_1(w[5], w[59], pars->GC_11, pars->mdl_MB, pars->ZERO, w[63]); 
  VVV1P0_1(w[58], w[59], pars->GC_10, pars->ZERO, pars->ZERO, w[64]); 
  FFV1_1(w[25], w[58], pars->GC_11, pars->mdl_MB, pars->ZERO, w[65]); 
  FFV1_2(w[28], w[58], pars->GC_11, pars->mdl_MB, pars->ZERO, w[66]); 
  FFV1_1(w[5], w[1], pars->GC_11, pars->mdl_MB, pars->ZERO, w[67]); 
  VVV1P0_1(w[58], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[68]); 
  FFS2_1(w[67], w[3], pars->GC_74, pars->mdl_MB, pars->ZERO, w[69]); 
  FFV1_1(w[67], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[70]); 
  FFV1_1(w[67], w[58], pars->GC_11, pars->mdl_MB, pars->ZERO, w[71]); 
  FFV1_2(w[18], w[58], pars->GC_11, pars->mdl_MB, pars->ZERO, w[72]); 
  FFV1_2(w[6], w[1], pars->GC_11, pars->mdl_MB, pars->ZERO, w[73]); 
  FFS2_2(w[73], w[3], pars->GC_74, pars->mdl_MB, pars->ZERO, w[74]); 
  FFV1_2(w[73], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[75]); 
  FFV1_2(w[73], w[58], pars->GC_11, pars->mdl_MB, pars->ZERO, w[76]); 
  FFV1_1(w[15], w[58], pars->GC_11, pars->mdl_MB, pars->ZERO, w[77]); 
  VVV1P0_1(w[58], w[1], pars->GC_10, pars->ZERO, pars->ZERO, w[78]); 
  FFV1_1(w[25], w[1], pars->GC_11, pars->mdl_MB, pars->ZERO, w[79]); 
  VVVV1P0_1(w[58], w[1], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[80]); 
  VVVV3P0_1(w[58], w[1], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[81]); 
  VVVV4P0_1(w[58], w[1], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[82]); 
  FFV1_2(w[28], w[1], pars->GC_11, pars->mdl_MB, pars->ZERO, w[83]); 
  FFV1_1(w[15], w[1], pars->GC_11, pars->mdl_MB, pars->ZERO, w[84]); 
  FFV1_2(w[18], w[1], pars->GC_11, pars->mdl_MB, pars->ZERO, w[85]); 
  VVS3P0_1(w[0], w[3], pars->GC_13, pars->ZERO, pars->ZERO, w[86]); 
  FFV1_1(w[5], w[86], pars->GC_11, pars->mdl_MB, pars->ZERO, w[87]); 
  FFV1_2(w[6], w[86], pars->GC_11, pars->mdl_MB, pars->ZERO, w[88]); 
  VVV1P0_1(w[86], w[59], pars->GC_10, pars->ZERO, pars->ZERO, w[89]); 
  FFV1_1(w[30], w[86], pars->GC_11, pars->mdl_MB, pars->ZERO, w[90]); 
  FFV1_2(w[39], w[86], pars->GC_11, pars->mdl_MB, pars->ZERO, w[91]); 
  VVV1P0_1(w[86], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[92]); 
  FFS2_1(w[67], w[2], pars->GC_74, pars->mdl_MB, pars->ZERO, w[93]); 
  FFV1_1(w[67], w[86], pars->GC_11, pars->mdl_MB, pars->ZERO, w[94]); 
  FFV1_2(w[18], w[86], pars->GC_11, pars->mdl_MB, pars->ZERO, w[95]); 
  FFS2_2(w[73], w[2], pars->GC_74, pars->mdl_MB, pars->ZERO, w[96]); 
  FFV1_2(w[73], w[86], pars->GC_11, pars->mdl_MB, pars->ZERO, w[97]); 
  FFV1_1(w[15], w[86], pars->GC_11, pars->mdl_MB, pars->ZERO, w[98]); 
  VVV1P0_1(w[86], w[1], pars->GC_10, pars->ZERO, pars->ZERO, w[99]); 
  FFV1_1(w[30], w[1], pars->GC_11, pars->mdl_MB, pars->ZERO, w[100]); 
  VVVV1P0_1(w[86], w[1], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[101]); 
  VVVV3P0_1(w[86], w[1], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[102]); 
  VVVV4P0_1(w[86], w[1], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[103]); 
  FFV1_2(w[39], w[1], pars->GC_11, pars->mdl_MB, pars->ZERO, w[104]); 
  VVV1P0_1(w[0], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[105]); 
  VVS3P0_1(w[1], w[2], pars->GC_13, pars->ZERO, pars->ZERO, w[106]); 
  FFV1_1(w[5], w[105], pars->GC_11, pars->mdl_MB, pars->ZERO, w[107]); 
  FFV1_2(w[6], w[106], pars->GC_11, pars->mdl_MB, pars->ZERO, w[108]); 
  FFV1_2(w[6], w[105], pars->GC_11, pars->mdl_MB, pars->ZERO, w[109]); 
  FFV1_1(w[5], w[106], pars->GC_11, pars->mdl_MB, pars->ZERO, w[110]); 
  VVV1P0_1(w[105], w[106], pars->GC_10, pars->ZERO, pars->ZERO, w[111]); 
  FFV1_1(w[25], w[105], pars->GC_11, pars->mdl_MB, pars->ZERO, w[112]); 
  FFV1_2(w[28], w[105], pars->GC_11, pars->mdl_MB, pars->ZERO, w[113]); 
  VVS3P0_1(w[1], w[3], pars->GC_13, pars->ZERO, pars->ZERO, w[114]); 
  FFV1_2(w[6], w[114], pars->GC_11, pars->mdl_MB, pars->ZERO, w[115]); 
  FFV1_1(w[5], w[114], pars->GC_11, pars->mdl_MB, pars->ZERO, w[116]); 
  VVV1P0_1(w[105], w[114], pars->GC_10, pars->ZERO, pars->ZERO, w[117]); 
  FFV1_1(w[30], w[105], pars->GC_11, pars->mdl_MB, pars->ZERO, w[118]); 
  FFV1_2(w[39], w[105], pars->GC_11, pars->mdl_MB, pars->ZERO, w[119]); 
  VVS3P0_1(w[105], w[2], pars->GC_13, pars->ZERO, pars->ZERO, w[120]); 
  VVS3P0_1(w[105], w[3], pars->GC_13, pars->ZERO, pars->ZERO, w[121]); 
  FFV1_1(w[67], w[105], pars->GC_11, pars->mdl_MB, pars->ZERO, w[122]); 
  VVS3P0_1(w[105], w[8], pars->GC_13, pars->ZERO, pars->ZERO, w[123]); 
  FFV1_2(w[73], w[105], pars->GC_11, pars->mdl_MB, pars->ZERO, w[124]); 
  VVV1P0_1(w[105], w[1], pars->GC_10, pars->ZERO, pars->ZERO, w[125]); 
  VVS3P0_1(w[1], w[8], pars->GC_13, pars->ZERO, pars->ZERO, w[126]); 
  VVV1P0_1(w[105], w[20], pars->GC_10, pars->ZERO, pars->ZERO, w[127]); 
  VVVS2P0_1(w[105], w[1], w[3], pars->GC_14, pars->ZERO, pars->ZERO, w[128]); 
  VVVS2P0_1(w[105], w[1], w[2], pars->GC_14, pars->ZERO, pars->ZERO, w[129]); 
  FFV1_1(w[5], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[130]); 
  FFS2_1(w[130], w[3], pars->GC_74, pars->mdl_MB, pars->ZERO, w[131]); 
  VVV1P0_1(w[106], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[132]); 
  FFV1_1(w[130], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[133]); 
  FFV1_1(w[130], w[106], pars->GC_11, pars->mdl_MB, pars->ZERO, w[134]); 
  FFV1P0_3(w[28], w[130], pars->GC_11, pars->ZERO, pars->ZERO, w[135]); 
  FFS2_1(w[130], w[2], pars->GC_74, pars->mdl_MB, pars->ZERO, w[136]); 
  VVV1P0_1(w[114], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[137]); 
  FFV1_1(w[130], w[114], pars->GC_11, pars->mdl_MB, pars->ZERO, w[138]); 
  FFV1P0_3(w[39], w[130], pars->GC_11, pars->ZERO, pars->ZERO, w[139]); 
  VVS3P0_1(w[59], w[3], pars->GC_13, pars->ZERO, pars->ZERO, w[140]); 
  VVS3P0_1(w[59], w[2], pars->GC_13, pars->ZERO, pars->ZERO, w[141]); 
  FFV1_1(w[130], w[59], pars->GC_11, pars->mdl_MB, pars->ZERO, w[142]); 
  FFS2_1(w[130], w[8], pars->GC_74, pars->mdl_MB, pars->ZERO, w[143]); 
  FFV1P0_3(w[6], w[130], pars->GC_11, pars->ZERO, pars->ZERO, w[144]); 
  FFV1P0_3(w[73], w[130], pars->GC_11, pars->ZERO, pars->ZERO, w[145]); 
  FFV1_1(w[130], w[22], pars->GC_11, pars->mdl_MB, pars->ZERO, w[146]); 
  FFV1_1(w[130], w[36], pars->GC_11, pars->mdl_MB, pars->ZERO, w[147]); 
  FFV1_1(w[130], w[1], pars->GC_11, pars->mdl_MB, pars->ZERO, w[148]); 
  FFV1P0_3(w[18], w[130], pars->GC_11, pars->ZERO, pars->ZERO, w[149]); 
  VVV1P0_1(w[1], w[22], pars->GC_10, pars->ZERO, pars->ZERO, w[150]); 
  VVV1P0_1(w[1], w[36], pars->GC_10, pars->ZERO, pars->ZERO, w[151]); 
  VVVS2P0_1(w[1], w[4], w[2], pars->GC_14, pars->ZERO, pars->ZERO, w[152]); 
  VVVS2P0_1(w[1], w[4], w[3], pars->GC_14, pars->ZERO, pars->ZERO, w[153]); 
  FFV1_2(w[6], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[154]); 
  FFS2_2(w[154], w[3], pars->GC_74, pars->mdl_MB, pars->ZERO, w[155]); 
  FFV1_2(w[154], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[156]); 
  FFV1_2(w[154], w[106], pars->GC_11, pars->mdl_MB, pars->ZERO, w[157]); 
  FFV1P0_3(w[154], w[25], pars->GC_11, pars->ZERO, pars->ZERO, w[158]); 
  FFS2_2(w[154], w[2], pars->GC_74, pars->mdl_MB, pars->ZERO, w[159]); 
  FFV1_2(w[154], w[114], pars->GC_11, pars->mdl_MB, pars->ZERO, w[160]); 
  FFV1P0_3(w[154], w[30], pars->GC_11, pars->ZERO, pars->ZERO, w[161]); 
  FFV1_2(w[154], w[59], pars->GC_11, pars->mdl_MB, pars->ZERO, w[162]); 
  FFS2_2(w[154], w[8], pars->GC_74, pars->mdl_MB, pars->ZERO, w[163]); 
  FFV1P0_3(w[154], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[164]); 
  FFV1P0_3(w[154], w[67], pars->GC_11, pars->ZERO, pars->ZERO, w[165]); 
  FFV1_2(w[154], w[22], pars->GC_11, pars->mdl_MB, pars->ZERO, w[166]); 
  FFV1_2(w[154], w[36], pars->GC_11, pars->mdl_MB, pars->ZERO, w[167]); 
  FFV1_2(w[154], w[1], pars->GC_11, pars->mdl_MB, pars->ZERO, w[168]); 
  FFV1P0_3(w[154], w[15], pars->GC_11, pars->ZERO, pars->ZERO, w[169]); 
  VVV1P0_1(w[0], w[106], pars->GC_10, pars->ZERO, pars->ZERO, w[170]); 
  FFV1_1(w[25], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[171]); 
  VVVV1P0_1(w[0], w[106], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[172]); 
  VVVV3P0_1(w[0], w[106], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[173]); 
  VVVV4P0_1(w[0], w[106], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[174]); 
  FFV1_2(w[18], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[175]); 
  FFV1_2(w[28], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[176]); 
  FFV1_1(w[15], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[177]); 
  VVV1P0_1(w[0], w[114], pars->GC_10, pars->ZERO, pars->ZERO, w[178]); 
  FFV1_1(w[30], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[179]); 
  VVVV1P0_1(w[0], w[114], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[180]); 
  VVVV3P0_1(w[0], w[114], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[181]); 
  VVVV4P0_1(w[0], w[114], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[182]); 
  FFV1_2(w[39], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[183]); 
  VVV1P0_1(w[0], w[59], pars->GC_10, pars->ZERO, pars->ZERO, w[184]); 
  VVS3P0_1(w[0], w[8], pars->GC_13, pars->ZERO, pars->ZERO, w[185]); 
  VVV1P0_1(w[0], w[20], pars->GC_10, pars->ZERO, pars->ZERO, w[186]); 
  VVVS2P0_1(w[0], w[59], w[3], pars->GC_14, pars->ZERO, pars->ZERO, w[187]); 
  VVVS2P0_1(w[0], w[59], w[2], pars->GC_14, pars->ZERO, pars->ZERO, w[188]); 
  FFV1_1(w[67], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[189]); 
  FFV1P0_3(w[6], w[67], pars->GC_11, pars->ZERO, pars->ZERO, w[190]); 
  VVVS2P0_1(w[0], w[4], w[8], pars->GC_14, pars->ZERO, pars->ZERO, w[191]); 
  VVV1P0_1(w[0], w[22], pars->GC_10, pars->ZERO, pars->ZERO, w[192]); 
  VVV1P0_1(w[0], w[36], pars->GC_10, pars->ZERO, pars->ZERO, w[193]); 
  FFV1_2(w[73], w[0], pars->GC_11, pars->mdl_MB, pars->ZERO, w[194]); 
  FFV1P0_3(w[73], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[195]); 
  FFV1P0_3(w[6], w[15], pars->GC_11, pars->ZERO, pars->ZERO, w[196]); 
  VVVS2P0_1(w[0], w[1], w[8], pars->GC_14, pars->ZERO, pars->ZERO, w[197]); 
  FFV1P0_3(w[18], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[198]); 
  VVV1P0_1(w[1], w[20], pars->GC_10, pars->ZERO, pars->ZERO, w[199]); 
  VVV1P0_1(w[4], w[20], pars->GC_10, pars->ZERO, pars->ZERO, w[200]); 
  VVVV1P0_1(w[0], w[1], w[20], pars->GC_12, pars->ZERO, pars->ZERO, w[201]); 
  VVVV3P0_1(w[0], w[1], w[20], pars->GC_12, pars->ZERO, pars->ZERO, w[202]); 
  VVVV4P0_1(w[0], w[1], w[20], pars->GC_12, pars->ZERO, pars->ZERO, w[203]); 
  VVVV1P0_1(w[0], w[4], w[20], pars->GC_12, pars->ZERO, pars->ZERO, w[204]); 
  VVVV3P0_1(w[0], w[4], w[20], pars->GC_12, pars->ZERO, pars->ZERO, w[205]); 
  VVVV4P0_1(w[0], w[4], w[20], pars->GC_12, pars->ZERO, pars->ZERO, w[206]); 
  VVVV1P0_1(w[0], w[1], w[22], pars->GC_12, pars->ZERO, pars->ZERO, w[207]); 
  VVVV3P0_1(w[0], w[1], w[22], pars->GC_12, pars->ZERO, pars->ZERO, w[208]); 
  VVVV4P0_1(w[0], w[1], w[22], pars->GC_12, pars->ZERO, pars->ZERO, w[209]); 
  VVVV1P0_1(w[0], w[1], w[36], pars->GC_12, pars->ZERO, pars->ZERO, w[210]); 
  VVVV3P0_1(w[0], w[1], w[36], pars->GC_12, pars->ZERO, pars->ZERO, w[211]); 
  VVVV4P0_1(w[0], w[1], w[36], pars->GC_12, pars->ZERO, pars->ZERO, w[212]); 
  VVVS2P0_1(w[0], w[1], w[2], pars->GC_14, pars->ZERO, pars->ZERO, w[213]); 
  VVV1P0_1(w[213], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[214]); 
  FFV1_2(w[6], w[213], pars->GC_11, pars->mdl_MB, pars->ZERO, w[215]); 
  FFV1_1(w[5], w[213], pars->GC_11, pars->mdl_MB, pars->ZERO, w[216]); 
  VVVS2P0_1(w[0], w[1], w[3], pars->GC_14, pars->ZERO, pars->ZERO, w[217]); 
  VVV1P0_1(w[217], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[218]); 
  FFV1_2(w[6], w[217], pars->GC_11, pars->mdl_MB, pars->ZERO, w[219]); 
  FFV1_1(w[5], w[217], pars->GC_11, pars->mdl_MB, pars->ZERO, w[220]); 
  VVVV1P0_1(w[0], w[1], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[221]); 
  VVVV3P0_1(w[0], w[1], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[222]); 
  VVVV4P0_1(w[0], w[1], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[223]); 
  FFV1_1(w[5], w[221], pars->GC_11, pars->mdl_MB, pars->ZERO, w[224]); 
  FFV1_1(w[5], w[222], pars->GC_11, pars->mdl_MB, pars->ZERO, w[225]); 
  FFV1_1(w[5], w[223], pars->GC_11, pars->mdl_MB, pars->ZERO, w[226]); 
  FFV1_2(w[6], w[221], pars->GC_11, pars->mdl_MB, pars->ZERO, w[227]); 
  FFV1_2(w[6], w[222], pars->GC_11, pars->mdl_MB, pars->ZERO, w[228]); 
  FFV1_2(w[6], w[223], pars->GC_11, pars->mdl_MB, pars->ZERO, w[229]); 
  VVS3P0_1(w[221], w[3], pars->GC_13, pars->ZERO, pars->ZERO, w[230]); 
  VVS3P0_1(w[222], w[3], pars->GC_13, pars->ZERO, pars->ZERO, w[231]); 
  VVS3P0_1(w[223], w[3], pars->GC_13, pars->ZERO, pars->ZERO, w[232]); 
  VVS3P0_1(w[221], w[2], pars->GC_13, pars->ZERO, pars->ZERO, w[233]); 
  VVS3P0_1(w[222], w[2], pars->GC_13, pars->ZERO, pars->ZERO, w[234]); 
  VVS3P0_1(w[223], w[2], pars->GC_13, pars->ZERO, pars->ZERO, w[235]); 
  VVVS2P0_1(w[0], w[4], w[2], pars->GC_14, pars->ZERO, pars->ZERO, w[236]); 
  FFV1_2(w[6], w[236], pars->GC_11, pars->mdl_MB, pars->ZERO, w[237]); 
  FFV1_1(w[5], w[236], pars->GC_11, pars->mdl_MB, pars->ZERO, w[238]); 
  VVV1P0_1(w[236], w[1], pars->GC_10, pars->ZERO, pars->ZERO, w[239]); 
  VVVS2P0_1(w[0], w[4], w[3], pars->GC_14, pars->ZERO, pars->ZERO, w[240]); 
  FFV1_2(w[6], w[240], pars->GC_11, pars->mdl_MB, pars->ZERO, w[241]); 
  FFV1_1(w[5], w[240], pars->GC_11, pars->mdl_MB, pars->ZERO, w[242]); 
  VVV1P0_1(w[240], w[1], pars->GC_10, pars->ZERO, pars->ZERO, w[243]); 
  VVV1P0_1(w[0], w[152], pars->GC_10, pars->ZERO, pars->ZERO, w[244]); 
  VVV1P0_1(w[0], w[153], pars->GC_10, pars->ZERO, pars->ZERO, w[245]); 
  VVVVS1P0_1(w[0], w[1], w[4], w[2], pars->GC_15, pars->ZERO, pars->ZERO,
      w[246]);
  VVVVS2P0_1(w[0], w[1], w[4], w[2], pars->GC_15, pars->ZERO, pars->ZERO,
      w[247]);
  VVVVS3P0_1(w[0], w[1], w[4], w[2], pars->GC_15, pars->ZERO, pars->ZERO,
      w[248]);
  VVVVS1P0_1(w[0], w[1], w[4], w[3], pars->GC_15, pars->ZERO, pars->ZERO,
      w[249]);
  VVVVS2P0_1(w[0], w[1], w[4], w[3], pars->GC_15, pars->ZERO, pars->ZERO,
      w[250]);
  VVVVS3P0_1(w[0], w[1], w[4], w[3], pars->GC_15, pars->ZERO, pars->ZERO,
      w[251]);

  // Calculate all amplitudes
  // Amplitude(s) for diagram number 0
  FFV1_0(w[6], w[10], w[9], pars->GC_11, amp[0]); 
  FFV1_0(w[11], w[5], w[9], pars->GC_11, amp[1]); 
  FFV1_0(w[6], w[12], w[13], pars->GC_11, amp[2]); 
  FFV1_0(w[11], w[12], w[4], pars->GC_11, amp[3]); 
  FFV1_0(w[14], w[5], w[13], pars->GC_11, amp[4]); 
  FFV1_0(w[14], w[10], w[4], pars->GC_11, amp[5]); 
  FFV1_0(w[6], w[15], w[16], pars->GC_11, amp[6]); 
  FFS2_0(w[6], w[17], w[8], pars->GC_74, amp[7]); 
  FFS2_0(w[14], w[15], w[8], pars->GC_74, amp[8]); 
  FFV1_0(w[18], w[5], w[16], pars->GC_11, amp[9]); 
  FFS2_0(w[19], w[5], w[8], pars->GC_74, amp[10]); 
  FFS2_0(w[18], w[12], w[8], pars->GC_74, amp[11]); 
  VVVS2_0(w[7], w[4], w[20], w[8], pars->GC_14, amp[12]); 
  VVV1_0(w[4], w[20], w[16], pars->GC_10, amp[13]); 
  VVS3_0(w[20], w[9], w[8], pars->GC_13, amp[14]); 
  VVS3_0(w[4], w[21], w[8], pars->GC_13, amp[15]); 
  FFS2_0(w[23], w[12], w[3], pars->GC_74, amp[16]); 
  FFS2_0(w[14], w[24], w[3], pars->GC_74, amp[17]); 
  FFV1_0(w[6], w[25], w[26], pars->GC_11, amp[18]); 
  FFV1_0(w[6], w[27], w[22], pars->GC_11, amp[19]); 
  FFV1_0(w[14], w[25], w[22], pars->GC_11, amp[20]); 
  FFV1_0(w[28], w[5], w[26], pars->GC_11, amp[21]); 
  FFV1_0(w[29], w[5], w[22], pars->GC_11, amp[22]); 
  FFV1_0(w[28], w[12], w[22], pars->GC_11, amp[23]); 
  FFV1_0(w[6], w[32], w[31], pars->GC_11, amp[24]); 
  VVV1_0(w[31], w[33], w[4], pars->GC_10, amp[25]); 
  FFV1_0(w[6], w[34], w[9], pars->GC_11, amp[26]); 
  VVS3_0(w[9], w[33], w[3], pars->GC_13, amp[27]); 
  FFV1_0(w[14], w[34], w[4], pars->GC_11, amp[28]); 
  FFS2_0(w[14], w[32], w[3], pars->GC_74, amp[29]); 
  FFV1_0(w[6], w[30], w[35], pars->GC_11, amp[30]); 
  FFV1_0(w[6], w[37], w[36], pars->GC_11, amp[31]); 
  FFV1_0(w[6], w[30], w[38], pars->GC_11, amp[32]); 
  FFV1_0(w[14], w[30], w[36], pars->GC_11, amp[33]); 
  FFV1_0(w[28], w[37], w[4], pars->GC_11, amp[34]); 
  FFV1_0(w[29], w[30], w[4], pars->GC_11, amp[35]); 
  FFV1_0(w[28], w[30], w[9], pars->GC_11, amp[36]); 
  FFS2_0(w[18], w[37], w[3], pars->GC_74, amp[37]); 
  FFV1_0(w[18], w[30], w[31], pars->GC_11, amp[38]); 
  FFS2_0(w[19], w[30], w[3], pars->GC_74, amp[39]); 
  FFV1_0(w[40], w[5], w[31], pars->GC_11, amp[40]); 
  VVV1_0(w[31], w[41], w[4], pars->GC_10, amp[41]); 
  FFV1_0(w[42], w[5], w[9], pars->GC_11, amp[42]); 
  VVS3_0(w[9], w[41], w[3], pars->GC_13, amp[43]); 
  FFV1_0(w[42], w[12], w[4], pars->GC_11, amp[44]); 
  FFS2_0(w[40], w[12], w[3], pars->GC_74, amp[45]); 
  FFV1_0(w[39], w[5], w[35], pars->GC_11, amp[46]); 
  FFV1_0(w[43], w[5], w[36], pars->GC_11, amp[47]); 
  FFV1_0(w[39], w[5], w[38], pars->GC_11, amp[48]); 
  FFV1_0(w[39], w[12], w[36], pars->GC_11, amp[49]); 
  FFV1_0(w[43], w[25], w[4], pars->GC_11, amp[50]); 
  FFV1_0(w[39], w[27], w[4], pars->GC_11, amp[51]); 
  FFV1_0(w[39], w[25], w[9], pars->GC_11, amp[52]); 
  FFS2_0(w[43], w[15], w[3], pars->GC_74, amp[53]); 
  FFV1_0(w[39], w[15], w[31], pars->GC_11, amp[54]); 
  FFS2_0(w[39], w[17], w[3], pars->GC_74, amp[55]); 
  FFS2_0(w[44], w[12], w[2], pars->GC_74, amp[56]); 
  FFS2_0(w[14], w[45], w[2], pars->GC_74, amp[57]); 
  FFV1_0(w[6], w[47], w[46], pars->GC_11, amp[58]); 
  VVV1_0(w[46], w[48], w[4], pars->GC_10, amp[59]); 
  FFV1_0(w[6], w[49], w[9], pars->GC_11, amp[60]); 
  VVS3_0(w[9], w[48], w[2], pars->GC_13, amp[61]); 
  FFV1_0(w[14], w[49], w[4], pars->GC_11, amp[62]); 
  FFS2_0(w[14], w[47], w[2], pars->GC_74, amp[63]); 
  FFV1_0(w[6], w[25], w[50], pars->GC_11, amp[64]); 
  FFV1_0(w[18], w[25], w[46], pars->GC_11, amp[65]); 
  FFS2_0(w[18], w[27], w[2], pars->GC_74, amp[66]); 
  FFS2_0(w[19], w[25], w[2], pars->GC_74, amp[67]); 
  FFV1_0(w[51], w[5], w[46], pars->GC_11, amp[68]); 
  VVV1_0(w[46], w[52], w[4], pars->GC_10, amp[69]); 
  FFV1_0(w[53], w[5], w[9], pars->GC_11, amp[70]); 
  VVS3_0(w[9], w[52], w[2], pars->GC_13, amp[71]); 
  FFV1_0(w[53], w[12], w[4], pars->GC_11, amp[72]); 
  FFS2_0(w[51], w[12], w[2], pars->GC_74, amp[73]); 
  FFV1_0(w[28], w[5], w[50], pars->GC_11, amp[74]); 
  FFV1_0(w[28], w[15], w[46], pars->GC_11, amp[75]); 
  FFS2_0(w[29], w[15], w[2], pars->GC_74, amp[76]); 
  FFS2_0(w[28], w[17], w[2], pars->GC_74, amp[77]); 
  FFV1_0(w[6], w[54], w[46], pars->GC_11, amp[78]); 
  FFV1_0(w[6], w[55], w[31], pars->GC_11, amp[79]); 
  FFS2_0(w[14], w[55], w[3], pars->GC_74, amp[80]); 
  FFS2_0(w[14], w[54], w[2], pars->GC_74, amp[81]); 
  FFV1_0(w[56], w[5], w[46], pars->GC_11, amp[82]); 
  FFV1_0(w[57], w[5], w[31], pars->GC_11, amp[83]); 
  FFS2_0(w[57], w[12], w[3], pars->GC_74, amp[84]); 
  FFS2_0(w[56], w[12], w[2], pars->GC_74, amp[85]); 
  FFS2_0(w[61], w[60], w[3], pars->GC_74, amp[86]); 
  FFS2_0(w[62], w[63], w[3], pars->GC_74, amp[87]); 
  FFV1_0(w[6], w[25], w[64], pars->GC_11, amp[88]); 
  FFV1_0(w[6], w[65], w[59], pars->GC_11, amp[89]); 
  FFV1_0(w[62], w[25], w[59], pars->GC_11, amp[90]); 
  FFV1_0(w[28], w[5], w[64], pars->GC_11, amp[91]); 
  FFV1_0(w[66], w[5], w[59], pars->GC_11, amp[92]); 
  FFV1_0(w[28], w[60], w[59], pars->GC_11, amp[93]); 
  FFV1_0(w[6], w[69], w[68], pars->GC_11, amp[94]); 
  FFV1_0(w[62], w[69], w[4], pars->GC_11, amp[95]); 
  FFS2_0(w[62], w[70], w[3], pars->GC_74, amp[96]); 
  FFV1_0(w[28], w[71], w[4], pars->GC_11, amp[97]); 
  FFV1_0(w[66], w[67], w[4], pars->GC_11, amp[98]); 
  FFV1_0(w[28], w[67], w[68], pars->GC_11, amp[99]); 
  FFS2_0(w[18], w[71], w[3], pars->GC_74, amp[100]); 
  FFS2_0(w[72], w[67], w[3], pars->GC_74, amp[101]); 
  FFV1_0(w[74], w[5], w[68], pars->GC_11, amp[102]); 
  FFV1_0(w[74], w[60], w[4], pars->GC_11, amp[103]); 
  FFS2_0(w[75], w[60], w[3], pars->GC_74, amp[104]); 
  FFV1_0(w[76], w[25], w[4], pars->GC_11, amp[105]); 
  FFV1_0(w[73], w[65], w[4], pars->GC_11, amp[106]); 
  FFV1_0(w[73], w[25], w[68], pars->GC_11, amp[107]); 
  FFS2_0(w[76], w[15], w[3], pars->GC_74, amp[108]); 
  FFS2_0(w[73], w[77], w[3], pars->GC_74, amp[109]); 
  FFV1_0(w[6], w[47], w[78], pars->GC_11, amp[110]); 
  VVV1_0(w[78], w[48], w[4], pars->GC_10, amp[111]); 
  FFV1_0(w[6], w[79], w[68], pars->GC_11, amp[112]); 
  VVV1_0(w[68], w[1], w[48], pars->GC_10, amp[113]); 
  FFV1_0(w[62], w[79], w[4], pars->GC_11, amp[114]); 
  FFV1_0(w[62], w[47], w[1], pars->GC_11, amp[115]); 
  FFV1_0(w[6], w[25], w[80], pars->GC_11, amp[116]); 
  FFV1_0(w[6], w[25], w[81], pars->GC_11, amp[117]); 
  FFV1_0(w[6], w[25], w[82], pars->GC_11, amp[118]); 
  FFV1_0(w[18], w[25], w[78], pars->GC_11, amp[119]); 
  FFV1_0(w[18], w[65], w[1], pars->GC_11, amp[120]); 
  FFV1_0(w[72], w[25], w[1], pars->GC_11, amp[121]); 
  FFV1_0(w[51], w[5], w[78], pars->GC_11, amp[122]); 
  VVV1_0(w[78], w[52], w[4], pars->GC_10, amp[123]); 
  FFV1_0(w[83], w[5], w[68], pars->GC_11, amp[124]); 
  VVV1_0(w[68], w[1], w[52], pars->GC_10, amp[125]); 
  FFV1_0(w[83], w[60], w[4], pars->GC_11, amp[126]); 
  FFV1_0(w[51], w[60], w[1], pars->GC_11, amp[127]); 
  FFV1_0(w[28], w[5], w[80], pars->GC_11, amp[128]); 
  FFV1_0(w[28], w[5], w[81], pars->GC_11, amp[129]); 
  FFV1_0(w[28], w[5], w[82], pars->GC_11, amp[130]); 
  FFV1_0(w[28], w[15], w[78], pars->GC_11, amp[131]); 
  FFV1_0(w[66], w[15], w[1], pars->GC_11, amp[132]); 
  FFV1_0(w[28], w[77], w[1], pars->GC_11, amp[133]); 
  FFV1_0(w[6], w[54], w[78], pars->GC_11, amp[134]); 
  FFS2_0(w[62], w[84], w[3], pars->GC_74, amp[135]); 
  FFV1_0(w[62], w[54], w[1], pars->GC_11, amp[136]); 
  FFV1_0(w[56], w[5], w[78], pars->GC_11, amp[137]); 
  FFS2_0(w[85], w[60], w[3], pars->GC_74, amp[138]); 
  FFV1_0(w[56], w[60], w[1], pars->GC_11, amp[139]); 
  FFS2_0(w[61], w[87], w[2], pars->GC_74, amp[140]); 
  FFS2_0(w[88], w[63], w[2], pars->GC_74, amp[141]); 
  FFV1_0(w[6], w[30], w[89], pars->GC_11, amp[142]); 
  FFV1_0(w[6], w[90], w[59], pars->GC_11, amp[143]); 
  FFV1_0(w[88], w[30], w[59], pars->GC_11, amp[144]); 
  FFV1_0(w[39], w[5], w[89], pars->GC_11, amp[145]); 
  FFV1_0(w[91], w[5], w[59], pars->GC_11, amp[146]); 
  FFV1_0(w[39], w[87], w[59], pars->GC_11, amp[147]); 
  FFV1_0(w[6], w[93], w[92], pars->GC_11, amp[148]); 
  FFV1_0(w[88], w[93], w[4], pars->GC_11, amp[149]); 
  FFS2_0(w[88], w[70], w[2], pars->GC_74, amp[150]); 
  FFV1_0(w[39], w[94], w[4], pars->GC_11, amp[151]); 
  FFV1_0(w[91], w[67], w[4], pars->GC_11, amp[152]); 
  FFV1_0(w[39], w[67], w[92], pars->GC_11, amp[153]); 
  FFS2_0(w[18], w[94], w[2], pars->GC_74, amp[154]); 
  FFS2_0(w[95], w[67], w[2], pars->GC_74, amp[155]); 
  FFV1_0(w[96], w[5], w[92], pars->GC_11, amp[156]); 
  FFV1_0(w[96], w[87], w[4], pars->GC_11, amp[157]); 
  FFS2_0(w[75], w[87], w[2], pars->GC_74, amp[158]); 
  FFV1_0(w[97], w[30], w[4], pars->GC_11, amp[159]); 
  FFV1_0(w[73], w[90], w[4], pars->GC_11, amp[160]); 
  FFV1_0(w[73], w[30], w[92], pars->GC_11, amp[161]); 
  FFS2_0(w[97], w[15], w[2], pars->GC_74, amp[162]); 
  FFS2_0(w[73], w[98], w[2], pars->GC_74, amp[163]); 
  FFV1_0(w[6], w[32], w[99], pars->GC_11, amp[164]); 
  VVV1_0(w[99], w[33], w[4], pars->GC_10, amp[165]); 
  FFV1_0(w[6], w[100], w[92], pars->GC_11, amp[166]); 
  VVV1_0(w[92], w[1], w[33], pars->GC_10, amp[167]); 
  FFV1_0(w[88], w[100], w[4], pars->GC_11, amp[168]); 
  FFV1_0(w[88], w[32], w[1], pars->GC_11, amp[169]); 
  FFV1_0(w[6], w[30], w[101], pars->GC_11, amp[170]); 
  FFV1_0(w[6], w[30], w[102], pars->GC_11, amp[171]); 
  FFV1_0(w[6], w[30], w[103], pars->GC_11, amp[172]); 
  FFV1_0(w[18], w[30], w[99], pars->GC_11, amp[173]); 
  FFV1_0(w[18], w[90], w[1], pars->GC_11, amp[174]); 
  FFV1_0(w[95], w[30], w[1], pars->GC_11, amp[175]); 
  FFV1_0(w[40], w[5], w[99], pars->GC_11, amp[176]); 
  VVV1_0(w[99], w[41], w[4], pars->GC_10, amp[177]); 
  FFV1_0(w[104], w[5], w[92], pars->GC_11, amp[178]); 
  VVV1_0(w[92], w[1], w[41], pars->GC_10, amp[179]); 
  FFV1_0(w[104], w[87], w[4], pars->GC_11, amp[180]); 
  FFV1_0(w[40], w[87], w[1], pars->GC_11, amp[181]); 
  FFV1_0(w[39], w[5], w[101], pars->GC_11, amp[182]); 
  FFV1_0(w[39], w[5], w[102], pars->GC_11, amp[183]); 
  FFV1_0(w[39], w[5], w[103], pars->GC_11, amp[184]); 
  FFV1_0(w[39], w[15], w[99], pars->GC_11, amp[185]); 
  FFV1_0(w[91], w[15], w[1], pars->GC_11, amp[186]); 
  FFV1_0(w[39], w[98], w[1], pars->GC_11, amp[187]); 
  FFV1_0(w[6], w[55], w[99], pars->GC_11, amp[188]); 
  FFS2_0(w[88], w[84], w[2], pars->GC_74, amp[189]); 
  FFV1_0(w[88], w[55], w[1], pars->GC_11, amp[190]); 
  FFV1_0(w[57], w[5], w[99], pars->GC_11, amp[191]); 
  FFS2_0(w[85], w[87], w[2], pars->GC_74, amp[192]); 
  FFV1_0(w[57], w[87], w[1], pars->GC_11, amp[193]); 
  FFS2_0(w[108], w[107], w[3], pars->GC_74, amp[194]); 
  FFS2_0(w[109], w[110], w[3], pars->GC_74, amp[195]); 
  FFV1_0(w[6], w[25], w[111], pars->GC_11, amp[196]); 
  FFV1_0(w[6], w[112], w[106], pars->GC_11, amp[197]); 
  FFV1_0(w[109], w[25], w[106], pars->GC_11, amp[198]); 
  FFV1_0(w[28], w[5], w[111], pars->GC_11, amp[199]); 
  FFV1_0(w[113], w[5], w[106], pars->GC_11, amp[200]); 
  FFV1_0(w[28], w[107], w[106], pars->GC_11, amp[201]); 
  FFS2_0(w[115], w[107], w[2], pars->GC_74, amp[202]); 
  FFS2_0(w[109], w[116], w[2], pars->GC_74, amp[203]); 
  FFV1_0(w[6], w[30], w[117], pars->GC_11, amp[204]); 
  FFV1_0(w[6], w[118], w[114], pars->GC_11, amp[205]); 
  FFV1_0(w[109], w[30], w[114], pars->GC_11, amp[206]); 
  FFV1_0(w[39], w[5], w[117], pars->GC_11, amp[207]); 
  FFV1_0(w[119], w[5], w[114], pars->GC_11, amp[208]); 
  FFV1_0(w[39], w[107], w[114], pars->GC_11, amp[209]); 
  FFV1_0(w[6], w[69], w[120], pars->GC_11, amp[210]); 
  FFV1_0(w[6], w[93], w[121], pars->GC_11, amp[211]); 
  FFS2_0(w[109], w[93], w[3], pars->GC_74, amp[212]); 
  FFS2_0(w[109], w[69], w[2], pars->GC_74, amp[213]); 
  FFS2_0(w[6], w[122], w[8], pars->GC_74, amp[214]); 
  FFV1_0(w[6], w[67], w[123], pars->GC_11, amp[215]); 
  FFS2_0(w[109], w[67], w[8], pars->GC_74, amp[216]); 
  FFS2_0(w[39], w[122], w[3], pars->GC_74, amp[217]); 
  FFS2_0(w[119], w[67], w[3], pars->GC_74, amp[218]); 
  FFV1_0(w[39], w[67], w[121], pars->GC_11, amp[219]); 
  FFS2_0(w[28], w[122], w[2], pars->GC_74, amp[220]); 
  FFV1_0(w[28], w[67], w[120], pars->GC_11, amp[221]); 
  FFS2_0(w[113], w[67], w[2], pars->GC_74, amp[222]); 
  FFV1_0(w[74], w[5], w[120], pars->GC_11, amp[223]); 
  FFV1_0(w[96], w[5], w[121], pars->GC_11, amp[224]); 
  FFS2_0(w[96], w[107], w[3], pars->GC_74, amp[225]); 
  FFS2_0(w[74], w[107], w[2], pars->GC_74, amp[226]); 
  FFS2_0(w[124], w[5], w[8], pars->GC_74, amp[227]); 
  FFV1_0(w[73], w[5], w[123], pars->GC_11, amp[228]); 
  FFS2_0(w[73], w[107], w[8], pars->GC_74, amp[229]); 
  FFS2_0(w[124], w[30], w[3], pars->GC_74, amp[230]); 
  FFS2_0(w[73], w[118], w[3], pars->GC_74, amp[231]); 
  FFV1_0(w[73], w[30], w[121], pars->GC_11, amp[232]); 
  FFS2_0(w[124], w[25], w[2], pars->GC_74, amp[233]); 
  FFV1_0(w[73], w[25], w[120], pars->GC_11, amp[234]); 
  FFS2_0(w[73], w[112], w[2], pars->GC_74, amp[235]); 
  FFV1_0(w[6], w[10], w[125], pars->GC_11, amp[236]); 
  FFV1_0(w[11], w[5], w[125], pars->GC_11, amp[237]); 
  FFV1_0(w[6], w[107], w[126], pars->GC_11, amp[238]); 
  FFV1_0(w[11], w[107], w[1], pars->GC_11, amp[239]); 
  FFV1_0(w[109], w[5], w[126], pars->GC_11, amp[240]); 
  FFV1_0(w[109], w[10], w[1], pars->GC_11, amp[241]); 
  VVVS2_0(w[105], w[1], w[20], w[8], pars->GC_14, amp[242]); 
  VVS3_0(w[20], w[125], w[8], pars->GC_13, amp[243]); 
  VVV1_0(w[1], w[20], w[123], pars->GC_10, amp[244]); 
  VVS3_0(w[1], w[127], w[8], pars->GC_13, amp[245]); 
  FFV1_0(w[6], w[34], w[125], pars->GC_11, amp[246]); 
  VVS3_0(w[125], w[33], w[3], pars->GC_13, amp[247]); 
  FFV1_0(w[6], w[100], w[121], pars->GC_11, amp[248]); 
  VVV1_0(w[121], w[1], w[33], pars->GC_10, amp[249]); 
  FFS2_0(w[109], w[100], w[3], pars->GC_74, amp[250]); 
  FFV1_0(w[109], w[34], w[1], pars->GC_11, amp[251]); 
  FFV1_0(w[6], w[30], w[128], pars->GC_11, amp[252]); 
  FFV1_0(w[28], w[30], w[125], pars->GC_11, amp[253]); 
  FFV1_0(w[28], w[118], w[1], pars->GC_11, amp[254]); 
  FFV1_0(w[113], w[30], w[1], pars->GC_11, amp[255]); 
  FFV1_0(w[42], w[5], w[125], pars->GC_11, amp[256]); 
  VVS3_0(w[125], w[41], w[3], pars->GC_13, amp[257]); 
  FFV1_0(w[104], w[5], w[121], pars->GC_11, amp[258]); 
  VVV1_0(w[121], w[1], w[41], pars->GC_10, amp[259]); 
  FFS2_0(w[104], w[107], w[3], pars->GC_74, amp[260]); 
  FFV1_0(w[42], w[107], w[1], pars->GC_11, amp[261]); 
  FFV1_0(w[39], w[5], w[128], pars->GC_11, amp[262]); 
  FFV1_0(w[39], w[25], w[125], pars->GC_11, amp[263]); 
  FFV1_0(w[119], w[25], w[1], pars->GC_11, amp[264]); 
  FFV1_0(w[39], w[112], w[1], pars->GC_11, amp[265]); 
  FFV1_0(w[6], w[49], w[125], pars->GC_11, amp[266]); 
  VVS3_0(w[125], w[48], w[2], pars->GC_13, amp[267]); 
  FFV1_0(w[6], w[79], w[120], pars->GC_11, amp[268]); 
  VVV1_0(w[120], w[1], w[48], pars->GC_10, amp[269]); 
  FFS2_0(w[109], w[79], w[2], pars->GC_74, amp[270]); 
  FFV1_0(w[109], w[49], w[1], pars->GC_11, amp[271]); 
  FFV1_0(w[6], w[25], w[129], pars->GC_11, amp[272]); 
  FFV1_0(w[53], w[5], w[125], pars->GC_11, amp[273]); 
  VVS3_0(w[125], w[52], w[2], pars->GC_13, amp[274]); 
  FFV1_0(w[83], w[5], w[120], pars->GC_11, amp[275]); 
  VVV1_0(w[120], w[1], w[52], pars->GC_10, amp[276]); 
  FFS2_0(w[83], w[107], w[2], pars->GC_74, amp[277]); 
  FFV1_0(w[53], w[107], w[1], pars->GC_11, amp[278]); 
  FFV1_0(w[28], w[5], w[129], pars->GC_11, amp[279]); 
  FFV1_0(w[6], w[131], w[132], pars->GC_11, amp[280]); 
  FFV1_0(w[108], w[131], w[4], pars->GC_11, amp[281]); 
  FFS2_0(w[108], w[133], w[3], pars->GC_74, amp[282]); 
  FFV1_0(w[28], w[134], w[4], pars->GC_11, amp[283]); 
  VVV1_0(w[106], w[4], w[135], pars->GC_10, amp[284]); 
  FFV1_0(w[28], w[133], w[106], pars->GC_11, amp[285]); 
  FFS2_0(w[18], w[134], w[3], pars->GC_74, amp[286]); 
  FFV1_0(w[18], w[131], w[106], pars->GC_11, amp[287]); 
  FFV1_0(w[6], w[136], w[137], pars->GC_11, amp[288]); 
  FFV1_0(w[115], w[136], w[4], pars->GC_11, amp[289]); 
  FFS2_0(w[115], w[133], w[2], pars->GC_74, amp[290]); 
  FFV1_0(w[39], w[138], w[4], pars->GC_11, amp[291]); 
  VVV1_0(w[114], w[4], w[139], pars->GC_10, amp[292]); 
  FFV1_0(w[39], w[133], w[114], pars->GC_11, amp[293]); 
  FFS2_0(w[18], w[138], w[2], pars->GC_74, amp[294]); 
  FFV1_0(w[18], w[136], w[114], pars->GC_11, amp[295]); 
  FFV1_0(w[6], w[136], w[140], pars->GC_11, amp[296]); 
  FFS2_0(w[61], w[136], w[3], pars->GC_74, amp[297]); 
  FFV1_0(w[6], w[131], w[141], pars->GC_11, amp[298]); 
  FFS2_0(w[61], w[131], w[2], pars->GC_74, amp[299]); 
  FFS2_0(w[6], w[142], w[8], pars->GC_74, amp[300]); 
  FFV1_0(w[6], w[143], w[59], pars->GC_11, amp[301]); 
  VVS3_0(w[59], w[144], w[8], pars->GC_13, amp[302]); 
  FFS2_0(w[39], w[142], w[3], pars->GC_74, amp[303]); 
  VVS3_0(w[59], w[139], w[3], pars->GC_13, amp[304]); 
  FFV1_0(w[39], w[131], w[59], pars->GC_11, amp[305]); 
  FFS2_0(w[28], w[142], w[2], pars->GC_74, amp[306]); 
  FFV1_0(w[28], w[136], w[59], pars->GC_11, amp[307]); 
  VVS3_0(w[59], w[135], w[2], pars->GC_13, amp[308]); 
  FFV1_0(w[74], w[136], w[4], pars->GC_11, amp[309]); 
  FFS2_0(w[75], w[136], w[3], pars->GC_74, amp[310]); 
  FFV1_0(w[96], w[131], w[4], pars->GC_11, amp[311]); 
  FFS2_0(w[75], w[131], w[2], pars->GC_74, amp[312]); 
  FFS2_0(w[96], w[133], w[3], pars->GC_74, amp[313]); 
  FFS2_0(w[74], w[133], w[2], pars->GC_74, amp[314]); 
  VVS3_0(w[4], w[145], w[8], pars->GC_13, amp[315]); 
  FFV1_0(w[73], w[143], w[4], pars->GC_11, amp[316]); 
  FFS2_0(w[73], w[133], w[8], pars->GC_74, amp[317]); 
  FFS2_0(w[73], w[146], w[3], pars->GC_74, amp[318]); 
  FFV1_0(w[73], w[131], w[22], pars->GC_11, amp[319]); 
  FFV1_0(w[73], w[136], w[36], pars->GC_11, amp[320]); 
  FFS2_0(w[73], w[147], w[2], pars->GC_74, amp[321]); 
  FFV1_0(w[6], w[148], w[13], pars->GC_11, amp[322]); 
  FFV1_0(w[11], w[148], w[4], pars->GC_11, amp[323]); 
  FFV1_0(w[6], w[133], w[126], pars->GC_11, amp[324]); 
  FFV1_0(w[11], w[133], w[1], pars->GC_11, amp[325]); 
  VVV1_0(w[144], w[126], w[4], pars->GC_10, amp[326]); 
  VVV1_0(w[144], w[1], w[13], pars->GC_10, amp[327]); 
  VVVS2_0(w[1], w[4], w[144], w[8], pars->GC_14, amp[328]); 
  FFS2_0(w[18], w[148], w[8], pars->GC_74, amp[329]); 
  FFV1_0(w[18], w[143], w[1], pars->GC_11, amp[330]); 
  VVS3_0(w[1], w[149], w[8], pars->GC_13, amp[331]); 
  FFS2_0(w[23], w[148], w[3], pars->GC_74, amp[332]); 
  FFV1_0(w[6], w[131], w[150], pars->GC_11, amp[333]); 
  FFV1_0(w[23], w[131], w[1], pars->GC_11, amp[334]); 
  FFV1_0(w[28], w[148], w[22], pars->GC_11, amp[335]); 
  FFV1_0(w[28], w[146], w[1], pars->GC_11, amp[336]); 
  VVV1_0(w[1], w[22], w[135], pars->GC_10, amp[337]); 
  FFV1_0(w[42], w[148], w[4], pars->GC_11, amp[338]); 
  FFS2_0(w[40], w[148], w[3], pars->GC_74, amp[339]); 
  FFV1_0(w[104], w[131], w[4], pars->GC_11, amp[340]); 
  FFV1_0(w[40], w[131], w[1], pars->GC_11, amp[341]); 
  FFS2_0(w[104], w[133], w[3], pars->GC_74, amp[342]); 
  FFV1_0(w[42], w[133], w[1], pars->GC_11, amp[343]); 
  FFV1_0(w[39], w[148], w[36], pars->GC_11, amp[344]); 
  VVV1_0(w[1], w[36], w[139], pars->GC_10, amp[345]); 
  FFV1_0(w[39], w[147], w[1], pars->GC_11, amp[346]); 
  FFS2_0(w[44], w[148], w[2], pars->GC_74, amp[347]); 
  FFV1_0(w[6], w[136], w[151], pars->GC_11, amp[348]); 
  FFV1_0(w[44], w[136], w[1], pars->GC_11, amp[349]); 
  FFV1_0(w[53], w[148], w[4], pars->GC_11, amp[350]); 
  FFS2_0(w[51], w[148], w[2], pars->GC_74, amp[351]); 
  FFV1_0(w[83], w[136], w[4], pars->GC_11, amp[352]); 
  FFV1_0(w[51], w[136], w[1], pars->GC_11, amp[353]); 
  FFS2_0(w[83], w[133], w[2], pars->GC_74, amp[354]); 
  FFV1_0(w[53], w[133], w[1], pars->GC_11, amp[355]); 
  FFS2_0(w[57], w[148], w[3], pars->GC_74, amp[356]); 
  FFS2_0(w[56], w[148], w[2], pars->GC_74, amp[357]); 
  FFS2_0(w[85], w[136], w[3], pars->GC_74, amp[358]); 
  FFV1_0(w[56], w[136], w[1], pars->GC_11, amp[359]); 
  FFS2_0(w[85], w[131], w[2], pars->GC_74, amp[360]); 
  FFV1_0(w[57], w[131], w[1], pars->GC_11, amp[361]); 
  FFV1_0(w[6], w[131], w[152], pars->GC_11, amp[362]); 
  FFV1_0(w[28], w[130], w[152], pars->GC_11, amp[363]); 
  FFV1_0(w[6], w[136], w[153], pars->GC_11, amp[364]); 
  FFV1_0(w[39], w[130], w[153], pars->GC_11, amp[365]); 
  FFV1_0(w[155], w[5], w[132], pars->GC_11, amp[366]); 
  FFV1_0(w[155], w[110], w[4], pars->GC_11, amp[367]); 
  FFS2_0(w[156], w[110], w[3], pars->GC_74, amp[368]); 
  FFV1_0(w[157], w[25], w[4], pars->GC_11, amp[369]); 
  VVV1_0(w[106], w[4], w[158], pars->GC_10, amp[370]); 
  FFV1_0(w[156], w[25], w[106], pars->GC_11, amp[371]); 
  FFS2_0(w[157], w[15], w[3], pars->GC_74, amp[372]); 
  FFV1_0(w[155], w[15], w[106], pars->GC_11, amp[373]); 
  FFV1_0(w[159], w[5], w[137], pars->GC_11, amp[374]); 
  FFV1_0(w[159], w[116], w[4], pars->GC_11, amp[375]); 
  FFS2_0(w[156], w[116], w[2], pars->GC_74, amp[376]); 
  FFV1_0(w[160], w[30], w[4], pars->GC_11, amp[377]); 
  VVV1_0(w[114], w[4], w[161], pars->GC_10, amp[378]); 
  FFV1_0(w[156], w[30], w[114], pars->GC_11, amp[379]); 
  FFS2_0(w[160], w[15], w[2], pars->GC_74, amp[380]); 
  FFV1_0(w[159], w[15], w[114], pars->GC_11, amp[381]); 
  FFV1_0(w[159], w[5], w[140], pars->GC_11, amp[382]); 
  FFS2_0(w[159], w[63], w[3], pars->GC_74, amp[383]); 
  FFV1_0(w[155], w[5], w[141], pars->GC_11, amp[384]); 
  FFS2_0(w[155], w[63], w[2], pars->GC_74, amp[385]); 
  FFS2_0(w[162], w[5], w[8], pars->GC_74, amp[386]); 
  FFV1_0(w[163], w[5], w[59], pars->GC_11, amp[387]); 
  VVS3_0(w[59], w[164], w[8], pars->GC_13, amp[388]); 
  FFS2_0(w[162], w[30], w[3], pars->GC_74, amp[389]); 
  VVS3_0(w[59], w[161], w[3], pars->GC_13, amp[390]); 
  FFV1_0(w[155], w[30], w[59], pars->GC_11, amp[391]); 
  FFS2_0(w[162], w[25], w[2], pars->GC_74, amp[392]); 
  FFV1_0(w[159], w[25], w[59], pars->GC_11, amp[393]); 
  VVS3_0(w[59], w[158], w[2], pars->GC_13, amp[394]); 
  FFV1_0(w[159], w[69], w[4], pars->GC_11, amp[395]); 
  FFS2_0(w[159], w[70], w[3], pars->GC_74, amp[396]); 
  FFV1_0(w[155], w[93], w[4], pars->GC_11, amp[397]); 
  FFS2_0(w[155], w[70], w[2], pars->GC_74, amp[398]); 
  FFS2_0(w[156], w[93], w[3], pars->GC_74, amp[399]); 
  FFS2_0(w[156], w[69], w[2], pars->GC_74, amp[400]); 
  VVS3_0(w[4], w[165], w[8], pars->GC_13, amp[401]); 
  FFV1_0(w[163], w[67], w[4], pars->GC_11, amp[402]); 
  FFS2_0(w[156], w[67], w[8], pars->GC_74, amp[403]); 
  FFS2_0(w[166], w[67], w[3], pars->GC_74, amp[404]); 
  FFV1_0(w[155], w[67], w[22], pars->GC_11, amp[405]); 
  FFV1_0(w[159], w[67], w[36], pars->GC_11, amp[406]); 
  FFS2_0(w[167], w[67], w[2], pars->GC_74, amp[407]); 
  FFV1_0(w[168], w[5], w[13], pars->GC_11, amp[408]); 
  FFV1_0(w[168], w[10], w[4], pars->GC_11, amp[409]); 
  FFV1_0(w[156], w[5], w[126], pars->GC_11, amp[410]); 
  FFV1_0(w[156], w[10], w[1], pars->GC_11, amp[411]); 
  VVV1_0(w[164], w[126], w[4], pars->GC_10, amp[412]); 
  VVV1_0(w[164], w[1], w[13], pars->GC_10, amp[413]); 
  VVVS2_0(w[1], w[4], w[164], w[8], pars->GC_14, amp[414]); 
  FFS2_0(w[168], w[15], w[8], pars->GC_74, amp[415]); 
  FFV1_0(w[163], w[15], w[1], pars->GC_11, amp[416]); 
  VVS3_0(w[1], w[169], w[8], pars->GC_13, amp[417]); 
  FFS2_0(w[168], w[24], w[3], pars->GC_74, amp[418]); 
  FFV1_0(w[155], w[5], w[150], pars->GC_11, amp[419]); 
  FFV1_0(w[155], w[24], w[1], pars->GC_11, amp[420]); 
  FFV1_0(w[168], w[25], w[22], pars->GC_11, amp[421]); 
  FFV1_0(w[166], w[25], w[1], pars->GC_11, amp[422]); 
  VVV1_0(w[1], w[22], w[158], pars->GC_10, amp[423]); 
  FFV1_0(w[168], w[34], w[4], pars->GC_11, amp[424]); 
  FFS2_0(w[168], w[32], w[3], pars->GC_74, amp[425]); 
  FFV1_0(w[155], w[100], w[4], pars->GC_11, amp[426]); 
  FFV1_0(w[155], w[32], w[1], pars->GC_11, amp[427]); 
  FFS2_0(w[156], w[100], w[3], pars->GC_74, amp[428]); 
  FFV1_0(w[156], w[34], w[1], pars->GC_11, amp[429]); 
  FFV1_0(w[168], w[30], w[36], pars->GC_11, amp[430]); 
  VVV1_0(w[1], w[36], w[161], pars->GC_10, amp[431]); 
  FFV1_0(w[167], w[30], w[1], pars->GC_11, amp[432]); 
  FFS2_0(w[168], w[45], w[2], pars->GC_74, amp[433]); 
  FFV1_0(w[159], w[5], w[151], pars->GC_11, amp[434]); 
  FFV1_0(w[159], w[45], w[1], pars->GC_11, amp[435]); 
  FFV1_0(w[168], w[49], w[4], pars->GC_11, amp[436]); 
  FFS2_0(w[168], w[47], w[2], pars->GC_74, amp[437]); 
  FFV1_0(w[159], w[79], w[4], pars->GC_11, amp[438]); 
  FFV1_0(w[159], w[47], w[1], pars->GC_11, amp[439]); 
  FFS2_0(w[156], w[79], w[2], pars->GC_74, amp[440]); 
  FFV1_0(w[156], w[49], w[1], pars->GC_11, amp[441]); 
  FFS2_0(w[168], w[55], w[3], pars->GC_74, amp[442]); 
  FFS2_0(w[168], w[54], w[2], pars->GC_74, amp[443]); 
  FFS2_0(w[159], w[84], w[3], pars->GC_74, amp[444]); 
  FFV1_0(w[159], w[54], w[1], pars->GC_11, amp[445]); 
  FFS2_0(w[155], w[84], w[2], pars->GC_74, amp[446]); 
  FFV1_0(w[155], w[55], w[1], pars->GC_11, amp[447]); 
  FFV1_0(w[155], w[5], w[152], pars->GC_11, amp[448]); 
  FFV1_0(w[154], w[25], w[152], pars->GC_11, amp[449]); 
  FFV1_0(w[159], w[5], w[153], pars->GC_11, amp[450]); 
  FFV1_0(w[154], w[30], w[153], pars->GC_11, amp[451]); 
  FFV1_0(w[6], w[47], w[170], pars->GC_11, amp[452]); 
  VVV1_0(w[170], w[48], w[4], pars->GC_10, amp[453]); 
  FFV1_0(w[6], w[171], w[132], pars->GC_11, amp[454]); 
  FFV1_0(w[108], w[171], w[4], pars->GC_11, amp[455]); 
  VVV1_0(w[0], w[132], w[48], pars->GC_10, amp[456]); 
  FFV1_0(w[108], w[47], w[0], pars->GC_11, amp[457]); 
  FFV1_0(w[6], w[25], w[172], pars->GC_11, amp[458]); 
  FFV1_0(w[6], w[25], w[173], pars->GC_11, amp[459]); 
  FFV1_0(w[6], w[25], w[174], pars->GC_11, amp[460]); 
  FFV1_0(w[18], w[25], w[170], pars->GC_11, amp[461]); 
  FFV1_0(w[18], w[171], w[106], pars->GC_11, amp[462]); 
  FFV1_0(w[175], w[25], w[106], pars->GC_11, amp[463]); 
  FFV1_0(w[51], w[5], w[170], pars->GC_11, amp[464]); 
  VVV1_0(w[170], w[52], w[4], pars->GC_10, amp[465]); 
  FFV1_0(w[176], w[5], w[132], pars->GC_11, amp[466]); 
  FFV1_0(w[176], w[110], w[4], pars->GC_11, amp[467]); 
  VVV1_0(w[0], w[132], w[52], pars->GC_10, amp[468]); 
  FFV1_0(w[51], w[110], w[0], pars->GC_11, amp[469]); 
  FFV1_0(w[28], w[5], w[172], pars->GC_11, amp[470]); 
  FFV1_0(w[28], w[5], w[173], pars->GC_11, amp[471]); 
  FFV1_0(w[28], w[5], w[174], pars->GC_11, amp[472]); 
  FFV1_0(w[28], w[15], w[170], pars->GC_11, amp[473]); 
  FFV1_0(w[176], w[15], w[106], pars->GC_11, amp[474]); 
  FFV1_0(w[28], w[177], w[106], pars->GC_11, amp[475]); 
  FFV1_0(w[6], w[54], w[170], pars->GC_11, amp[476]); 
  FFS2_0(w[108], w[177], w[3], pars->GC_74, amp[477]); 
  FFV1_0(w[108], w[54], w[0], pars->GC_11, amp[478]); 
  FFV1_0(w[56], w[5], w[170], pars->GC_11, amp[479]); 
  FFS2_0(w[175], w[110], w[3], pars->GC_74, amp[480]); 
  FFV1_0(w[56], w[110], w[0], pars->GC_11, amp[481]); 
  FFV1_0(w[6], w[32], w[178], pars->GC_11, amp[482]); 
  VVV1_0(w[178], w[33], w[4], pars->GC_10, amp[483]); 
  FFV1_0(w[6], w[179], w[137], pars->GC_11, amp[484]); 
  FFV1_0(w[115], w[179], w[4], pars->GC_11, amp[485]); 
  VVV1_0(w[0], w[137], w[33], pars->GC_10, amp[486]); 
  FFV1_0(w[115], w[32], w[0], pars->GC_11, amp[487]); 
  FFV1_0(w[6], w[30], w[180], pars->GC_11, amp[488]); 
  FFV1_0(w[6], w[30], w[181], pars->GC_11, amp[489]); 
  FFV1_0(w[6], w[30], w[182], pars->GC_11, amp[490]); 
  FFV1_0(w[18], w[30], w[178], pars->GC_11, amp[491]); 
  FFV1_0(w[18], w[179], w[114], pars->GC_11, amp[492]); 
  FFV1_0(w[175], w[30], w[114], pars->GC_11, amp[493]); 
  FFV1_0(w[40], w[5], w[178], pars->GC_11, amp[494]); 
  VVV1_0(w[178], w[41], w[4], pars->GC_10, amp[495]); 
  FFV1_0(w[183], w[5], w[137], pars->GC_11, amp[496]); 
  FFV1_0(w[183], w[116], w[4], pars->GC_11, amp[497]); 
  VVV1_0(w[0], w[137], w[41], pars->GC_10, amp[498]); 
  FFV1_0(w[40], w[116], w[0], pars->GC_11, amp[499]); 
  FFV1_0(w[39], w[5], w[180], pars->GC_11, amp[500]); 
  FFV1_0(w[39], w[5], w[181], pars->GC_11, amp[501]); 
  FFV1_0(w[39], w[5], w[182], pars->GC_11, amp[502]); 
  FFV1_0(w[39], w[15], w[178], pars->GC_11, amp[503]); 
  FFV1_0(w[183], w[15], w[114], pars->GC_11, amp[504]); 
  FFV1_0(w[39], w[177], w[114], pars->GC_11, amp[505]); 
  FFV1_0(w[6], w[55], w[178], pars->GC_11, amp[506]); 
  FFS2_0(w[115], w[177], w[2], pars->GC_74, amp[507]); 
  FFV1_0(w[115], w[55], w[0], pars->GC_11, amp[508]); 
  FFV1_0(w[57], w[5], w[178], pars->GC_11, amp[509]); 
  FFS2_0(w[175], w[116], w[2], pars->GC_74, amp[510]); 
  FFV1_0(w[57], w[116], w[0], pars->GC_11, amp[511]); 
  FFV1_0(w[6], w[10], w[184], pars->GC_11, amp[512]); 
  FFV1_0(w[11], w[5], w[184], pars->GC_11, amp[513]); 
  FFV1_0(w[6], w[63], w[185], pars->GC_11, amp[514]); 
  FFV1_0(w[61], w[5], w[185], pars->GC_11, amp[515]); 
  FFV1_0(w[11], w[63], w[0], pars->GC_11, amp[516]); 
  FFV1_0(w[61], w[10], w[0], pars->GC_11, amp[517]); 
  VVVS2_0(w[0], w[59], w[20], w[8], pars->GC_14, amp[518]); 
  VVS3_0(w[20], w[184], w[8], pars->GC_13, amp[519]); 
  VVV1_0(w[59], w[20], w[185], pars->GC_10, amp[520]); 
  VVS3_0(w[59], w[186], w[8], pars->GC_13, amp[521]); 
  FFV1_0(w[6], w[34], w[184], pars->GC_11, amp[522]); 
  VVS3_0(w[184], w[33], w[3], pars->GC_13, amp[523]); 
  FFV1_0(w[6], w[179], w[140], pars->GC_11, amp[524]); 
  FFS2_0(w[61], w[179], w[3], pars->GC_74, amp[525]); 
  VVV1_0(w[0], w[140], w[33], pars->GC_10, amp[526]); 
  FFV1_0(w[61], w[34], w[0], pars->GC_11, amp[527]); 
  FFV1_0(w[6], w[30], w[187], pars->GC_11, amp[528]); 
  FFV1_0(w[28], w[30], w[184], pars->GC_11, amp[529]); 
  FFV1_0(w[28], w[179], w[59], pars->GC_11, amp[530]); 
  FFV1_0(w[176], w[30], w[59], pars->GC_11, amp[531]); 
  FFV1_0(w[42], w[5], w[184], pars->GC_11, amp[532]); 
  VVS3_0(w[184], w[41], w[3], pars->GC_13, amp[533]); 
  FFV1_0(w[183], w[5], w[140], pars->GC_11, amp[534]); 
  FFS2_0(w[183], w[63], w[3], pars->GC_74, amp[535]); 
  VVV1_0(w[0], w[140], w[41], pars->GC_10, amp[536]); 
  FFV1_0(w[42], w[63], w[0], pars->GC_11, amp[537]); 
  FFV1_0(w[39], w[5], w[187], pars->GC_11, amp[538]); 
  FFV1_0(w[39], w[25], w[184], pars->GC_11, amp[539]); 
  FFV1_0(w[183], w[25], w[59], pars->GC_11, amp[540]); 
  FFV1_0(w[39], w[171], w[59], pars->GC_11, amp[541]); 
  FFV1_0(w[6], w[49], w[184], pars->GC_11, amp[542]); 
  VVS3_0(w[184], w[48], w[2], pars->GC_13, amp[543]); 
  FFV1_0(w[6], w[171], w[141], pars->GC_11, amp[544]); 
  FFS2_0(w[61], w[171], w[2], pars->GC_74, amp[545]); 
  VVV1_0(w[0], w[141], w[48], pars->GC_10, amp[546]); 
  FFV1_0(w[61], w[49], w[0], pars->GC_11, amp[547]); 
  FFV1_0(w[6], w[25], w[188], pars->GC_11, amp[548]); 
  FFV1_0(w[53], w[5], w[184], pars->GC_11, amp[549]); 
  VVS3_0(w[184], w[52], w[2], pars->GC_13, amp[550]); 
  FFV1_0(w[176], w[5], w[141], pars->GC_11, amp[551]); 
  FFS2_0(w[176], w[63], w[2], pars->GC_74, amp[552]); 
  VVV1_0(w[0], w[141], w[52], pars->GC_10, amp[553]); 
  FFV1_0(w[53], w[63], w[0], pars->GC_11, amp[554]); 
  FFV1_0(w[28], w[5], w[188], pars->GC_11, amp[555]); 
  FFV1_0(w[6], w[189], w[13], pars->GC_11, amp[556]); 
  FFV1_0(w[11], w[189], w[4], pars->GC_11, amp[557]); 
  FFV1_0(w[6], w[70], w[185], pars->GC_11, amp[558]); 
  VVV1_0(w[185], w[190], w[4], pars->GC_10, amp[559]); 
  FFV1_0(w[11], w[70], w[0], pars->GC_11, amp[560]); 
  VVV1_0(w[0], w[190], w[13], pars->GC_10, amp[561]); 
  FFV1_0(w[6], w[67], w[191], pars->GC_11, amp[562]); 
  FFS2_0(w[18], w[189], w[8], pars->GC_74, amp[563]); 
  FFV1_0(w[18], w[67], w[185], pars->GC_11, amp[564]); 
  FFS2_0(w[175], w[67], w[8], pars->GC_74, amp[565]); 
  FFS2_0(w[23], w[189], w[3], pars->GC_74, amp[566]); 
  FFV1_0(w[6], w[69], w[192], pars->GC_11, amp[567]); 
  FFV1_0(w[23], w[69], w[0], pars->GC_11, amp[568]); 
  FFV1_0(w[28], w[189], w[22], pars->GC_11, amp[569]); 
  FFV1_0(w[28], w[67], w[192], pars->GC_11, amp[570]); 
  FFV1_0(w[176], w[67], w[22], pars->GC_11, amp[571]); 
  FFV1_0(w[42], w[189], w[4], pars->GC_11, amp[572]); 
  FFS2_0(w[40], w[189], w[3], pars->GC_74, amp[573]); 
  FFV1_0(w[183], w[69], w[4], pars->GC_11, amp[574]); 
  FFS2_0(w[183], w[70], w[3], pars->GC_74, amp[575]); 
  FFV1_0(w[40], w[69], w[0], pars->GC_11, amp[576]); 
  FFV1_0(w[42], w[70], w[0], pars->GC_11, amp[577]); 
  FFV1_0(w[39], w[189], w[36], pars->GC_11, amp[578]); 
  FFV1_0(w[183], w[67], w[36], pars->GC_11, amp[579]); 
  FFV1_0(w[39], w[67], w[193], pars->GC_11, amp[580]); 
  FFS2_0(w[44], w[189], w[2], pars->GC_74, amp[581]); 
  FFV1_0(w[6], w[93], w[193], pars->GC_11, amp[582]); 
  FFV1_0(w[44], w[93], w[0], pars->GC_11, amp[583]); 
  FFV1_0(w[53], w[189], w[4], pars->GC_11, amp[584]); 
  FFS2_0(w[51], w[189], w[2], pars->GC_74, amp[585]); 
  FFV1_0(w[176], w[93], w[4], pars->GC_11, amp[586]); 
  FFS2_0(w[176], w[70], w[2], pars->GC_74, amp[587]); 
  FFV1_0(w[51], w[93], w[0], pars->GC_11, amp[588]); 
  FFV1_0(w[53], w[70], w[0], pars->GC_11, amp[589]); 
  FFS2_0(w[57], w[189], w[3], pars->GC_74, amp[590]); 
  FFS2_0(w[56], w[189], w[2], pars->GC_74, amp[591]); 
  FFS2_0(w[175], w[93], w[3], pars->GC_74, amp[592]); 
  FFS2_0(w[175], w[69], w[2], pars->GC_74, amp[593]); 
  FFV1_0(w[56], w[93], w[0], pars->GC_11, amp[594]); 
  FFV1_0(w[57], w[69], w[0], pars->GC_11, amp[595]); 
  FFV1_0(w[194], w[5], w[13], pars->GC_11, amp[596]); 
  FFV1_0(w[194], w[10], w[4], pars->GC_11, amp[597]); 
  FFV1_0(w[75], w[5], w[185], pars->GC_11, amp[598]); 
  VVV1_0(w[185], w[195], w[4], pars->GC_10, amp[599]); 
  FFV1_0(w[75], w[10], w[0], pars->GC_11, amp[600]); 
  VVV1_0(w[0], w[195], w[13], pars->GC_10, amp[601]); 
  FFV1_0(w[73], w[5], w[191], pars->GC_11, amp[602]); 
  FFS2_0(w[194], w[15], w[8], pars->GC_74, amp[603]); 
  FFV1_0(w[73], w[15], w[185], pars->GC_11, amp[604]); 
  FFS2_0(w[73], w[177], w[8], pars->GC_74, amp[605]); 
  FFS2_0(w[194], w[24], w[3], pars->GC_74, amp[606]); 
  FFV1_0(w[74], w[5], w[192], pars->GC_11, amp[607]); 
  FFV1_0(w[74], w[24], w[0], pars->GC_11, amp[608]); 
  FFV1_0(w[194], w[25], w[22], pars->GC_11, amp[609]); 
  FFV1_0(w[73], w[25], w[192], pars->GC_11, amp[610]); 
  FFV1_0(w[73], w[171], w[22], pars->GC_11, amp[611]); 
  FFV1_0(w[194], w[34], w[4], pars->GC_11, amp[612]); 
  FFS2_0(w[194], w[32], w[3], pars->GC_74, amp[613]); 
  FFV1_0(w[74], w[179], w[4], pars->GC_11, amp[614]); 
  FFS2_0(w[75], w[179], w[3], pars->GC_74, amp[615]); 
  FFV1_0(w[74], w[32], w[0], pars->GC_11, amp[616]); 
  FFV1_0(w[75], w[34], w[0], pars->GC_11, amp[617]); 
  FFV1_0(w[194], w[30], w[36], pars->GC_11, amp[618]); 
  FFV1_0(w[73], w[179], w[36], pars->GC_11, amp[619]); 
  FFV1_0(w[73], w[30], w[193], pars->GC_11, amp[620]); 
  FFS2_0(w[194], w[45], w[2], pars->GC_74, amp[621]); 
  FFV1_0(w[96], w[5], w[193], pars->GC_11, amp[622]); 
  FFV1_0(w[96], w[45], w[0], pars->GC_11, amp[623]); 
  FFV1_0(w[194], w[49], w[4], pars->GC_11, amp[624]); 
  FFS2_0(w[194], w[47], w[2], pars->GC_74, amp[625]); 
  FFV1_0(w[96], w[171], w[4], pars->GC_11, amp[626]); 
  FFS2_0(w[75], w[171], w[2], pars->GC_74, amp[627]); 
  FFV1_0(w[96], w[47], w[0], pars->GC_11, amp[628]); 
  FFV1_0(w[75], w[49], w[0], pars->GC_11, amp[629]); 
  FFS2_0(w[194], w[55], w[3], pars->GC_74, amp[630]); 
  FFS2_0(w[194], w[54], w[2], pars->GC_74, amp[631]); 
  FFS2_0(w[96], w[177], w[3], pars->GC_74, amp[632]); 
  FFS2_0(w[74], w[177], w[2], pars->GC_74, amp[633]); 
  FFV1_0(w[96], w[54], w[0], pars->GC_11, amp[634]); 
  FFV1_0(w[74], w[55], w[0], pars->GC_11, amp[635]); 
  FFV1_0(w[6], w[84], w[185], pars->GC_11, amp[636]); 
  VVV1_0(w[185], w[1], w[196], pars->GC_10, amp[637]); 
  FFV1_0(w[6], w[177], w[126], pars->GC_11, amp[638]); 
  FFV1_0(w[11], w[177], w[1], pars->GC_11, amp[639]); 
  VVV1_0(w[0], w[126], w[196], pars->GC_10, amp[640]); 
  FFV1_0(w[11], w[84], w[0], pars->GC_11, amp[641]); 
  FFV1_0(w[6], w[15], w[197], pars->GC_11, amp[642]); 
  FFV1_0(w[85], w[5], w[185], pars->GC_11, amp[643]); 
  VVV1_0(w[185], w[1], w[198], pars->GC_10, amp[644]); 
  FFV1_0(w[175], w[5], w[126], pars->GC_11, amp[645]); 
  FFV1_0(w[175], w[10], w[1], pars->GC_11, amp[646]); 
  VVV1_0(w[0], w[126], w[198], pars->GC_10, amp[647]); 
  FFV1_0(w[85], w[10], w[0], pars->GC_11, amp[648]); 
  FFV1_0(w[18], w[5], w[197], pars->GC_11, amp[649]); 
  VVVVS1_0(w[0], w[1], w[4], w[20], w[8], pars->GC_15, amp[650]); 
  VVVVS2_0(w[0], w[1], w[4], w[20], w[8], pars->GC_15, amp[651]); 
  VVVVS3_0(w[0], w[1], w[4], w[20], w[8], pars->GC_15, amp[652]); 
  VVV1_0(w[185], w[199], w[4], pars->GC_10, amp[653]); 
  VVV1_0(w[185], w[1], w[200], pars->GC_10, amp[654]); 
  VVVV1_0(w[1], w[4], w[20], w[185], pars->GC_12, amp[655]); 
  VVVV3_0(w[1], w[4], w[20], w[185], pars->GC_12, amp[656]); 
  VVVV4_0(w[1], w[4], w[20], w[185], pars->GC_12, amp[657]); 
  VVV1_0(w[186], w[126], w[4], pars->GC_10, amp[658]); 
  VVV1_0(w[186], w[1], w[13], pars->GC_10, amp[659]); 
  VVVS2_0(w[1], w[4], w[186], w[8], pars->GC_14, amp[660]); 
  VVV1_0(w[0], w[126], w[200], pars->GC_10, amp[661]); 
  VVV1_0(w[0], w[199], w[13], pars->GC_10, amp[662]); 
  VVV1_0(w[4], w[20], w[197], pars->GC_10, amp[663]); 
  VVS3_0(w[4], w[201], w[8], pars->GC_13, amp[664]); 
  VVS3_0(w[4], w[202], w[8], pars->GC_13, amp[665]); 
  VVS3_0(w[4], w[203], w[8], pars->GC_13, amp[666]); 
  VVV1_0(w[1], w[20], w[191], pars->GC_10, amp[667]); 
  VVS3_0(w[1], w[204], w[8], pars->GC_13, amp[668]); 
  VVS3_0(w[1], w[205], w[8], pars->GC_13, amp[669]); 
  VVS3_0(w[1], w[206], w[8], pars->GC_13, amp[670]); 
  FFV1_0(w[6], w[79], w[192], pars->GC_11, amp[671]); 
  VVV1_0(w[192], w[1], w[48], pars->GC_10, amp[672]); 
  FFV1_0(w[6], w[171], w[150], pars->GC_11, amp[673]); 
  FFV1_0(w[23], w[171], w[1], pars->GC_11, amp[674]); 
  VVV1_0(w[0], w[150], w[48], pars->GC_10, amp[675]); 
  FFV1_0(w[23], w[79], w[0], pars->GC_11, amp[676]); 
  FFV1_0(w[6], w[25], w[207], pars->GC_11, amp[677]); 
  FFV1_0(w[6], w[25], w[208], pars->GC_11, amp[678]); 
  FFV1_0(w[6], w[25], w[209], pars->GC_11, amp[679]); 
  FFV1_0(w[83], w[5], w[192], pars->GC_11, amp[680]); 
  VVV1_0(w[192], w[1], w[52], pars->GC_10, amp[681]); 
  FFV1_0(w[176], w[5], w[150], pars->GC_11, amp[682]); 
  FFV1_0(w[176], w[24], w[1], pars->GC_11, amp[683]); 
  VVV1_0(w[0], w[150], w[52], pars->GC_10, amp[684]); 
  FFV1_0(w[83], w[24], w[0], pars->GC_11, amp[685]); 
  FFV1_0(w[28], w[5], w[207], pars->GC_11, amp[686]); 
  FFV1_0(w[28], w[5], w[208], pars->GC_11, amp[687]); 
  FFV1_0(w[28], w[5], w[209], pars->GC_11, amp[688]); 
  FFV1_0(w[6], w[179], w[151], pars->GC_11, amp[689]); 
  FFV1_0(w[44], w[179], w[1], pars->GC_11, amp[690]); 
  FFV1_0(w[6], w[100], w[193], pars->GC_11, amp[691]); 
  VVV1_0(w[193], w[1], w[33], pars->GC_10, amp[692]); 
  FFV1_0(w[44], w[100], w[0], pars->GC_11, amp[693]); 
  VVV1_0(w[0], w[151], w[33], pars->GC_10, amp[694]); 
  FFV1_0(w[6], w[30], w[210], pars->GC_11, amp[695]); 
  FFV1_0(w[6], w[30], w[211], pars->GC_11, amp[696]); 
  FFV1_0(w[6], w[30], w[212], pars->GC_11, amp[697]); 
  FFV1_0(w[83], w[179], w[4], pars->GC_11, amp[698]); 
  FFV1_0(w[51], w[179], w[1], pars->GC_11, amp[699]); 
  FFV1_0(w[176], w[100], w[4], pars->GC_11, amp[700]); 
  FFV1_0(w[176], w[32], w[1], pars->GC_11, amp[701]); 
  FFV1_0(w[51], w[100], w[0], pars->GC_11, amp[702]); 
  FFV1_0(w[83], w[32], w[0], pars->GC_11, amp[703]); 
  FFS2_0(w[85], w[179], w[3], pars->GC_74, amp[704]); 
  FFV1_0(w[56], w[179], w[1], pars->GC_11, amp[705]); 
  FFS2_0(w[175], w[100], w[3], pars->GC_74, amp[706]); 
  FFV1_0(w[175], w[34], w[1], pars->GC_11, amp[707]); 
  FFV1_0(w[56], w[100], w[0], pars->GC_11, amp[708]); 
  FFV1_0(w[85], w[34], w[0], pars->GC_11, amp[709]); 
  FFV1_0(w[183], w[5], w[151], pars->GC_11, amp[710]); 
  FFV1_0(w[183], w[45], w[1], pars->GC_11, amp[711]); 
  FFV1_0(w[104], w[5], w[193], pars->GC_11, amp[712]); 
  VVV1_0(w[193], w[1], w[41], pars->GC_10, amp[713]); 
  FFV1_0(w[104], w[45], w[0], pars->GC_11, amp[714]); 
  VVV1_0(w[0], w[151], w[41], pars->GC_10, amp[715]); 
  FFV1_0(w[39], w[5], w[210], pars->GC_11, amp[716]); 
  FFV1_0(w[39], w[5], w[211], pars->GC_11, amp[717]); 
  FFV1_0(w[39], w[5], w[212], pars->GC_11, amp[718]); 
  FFV1_0(w[183], w[79], w[4], pars->GC_11, amp[719]); 
  FFV1_0(w[183], w[47], w[1], pars->GC_11, amp[720]); 
  FFV1_0(w[104], w[171], w[4], pars->GC_11, amp[721]); 
  FFV1_0(w[40], w[171], w[1], pars->GC_11, amp[722]); 
  FFV1_0(w[104], w[47], w[0], pars->GC_11, amp[723]); 
  FFV1_0(w[40], w[79], w[0], pars->GC_11, amp[724]); 
  FFS2_0(w[183], w[84], w[3], pars->GC_74, amp[725]); 
  FFV1_0(w[183], w[54], w[1], pars->GC_11, amp[726]); 
  FFS2_0(w[104], w[177], w[3], pars->GC_74, amp[727]); 
  FFV1_0(w[42], w[177], w[1], pars->GC_11, amp[728]); 
  FFV1_0(w[104], w[54], w[0], pars->GC_11, amp[729]); 
  FFV1_0(w[42], w[84], w[0], pars->GC_11, amp[730]); 
  FFS2_0(w[85], w[171], w[2], pars->GC_74, amp[731]); 
  FFV1_0(w[57], w[171], w[1], pars->GC_11, amp[732]); 
  FFS2_0(w[175], w[79], w[2], pars->GC_74, amp[733]); 
  FFV1_0(w[175], w[49], w[1], pars->GC_11, amp[734]); 
  FFV1_0(w[57], w[79], w[0], pars->GC_11, amp[735]); 
  FFV1_0(w[85], w[49], w[0], pars->GC_11, amp[736]); 
  FFS2_0(w[176], w[84], w[2], pars->GC_74, amp[737]); 
  FFV1_0(w[176], w[55], w[1], pars->GC_11, amp[738]); 
  FFS2_0(w[83], w[177], w[2], pars->GC_74, amp[739]); 
  FFV1_0(w[53], w[177], w[1], pars->GC_11, amp[740]); 
  FFV1_0(w[83], w[55], w[0], pars->GC_11, amp[741]); 
  FFV1_0(w[53], w[84], w[0], pars->GC_11, amp[742]); 
  FFV1_0(w[6], w[25], w[214], pars->GC_11, amp[743]); 
  FFV1_0(w[215], w[25], w[4], pars->GC_11, amp[744]); 
  FFV1_0(w[18], w[25], w[213], pars->GC_11, amp[745]); 
  FFV1_0(w[28], w[5], w[214], pars->GC_11, amp[746]); 
  FFV1_0(w[28], w[216], w[4], pars->GC_11, amp[747]); 
  FFV1_0(w[28], w[15], w[213], pars->GC_11, amp[748]); 
  FFS2_0(w[215], w[15], w[3], pars->GC_74, amp[749]); 
  FFS2_0(w[18], w[216], w[3], pars->GC_74, amp[750]); 
  FFV1_0(w[6], w[30], w[218], pars->GC_11, amp[751]); 
  FFV1_0(w[219], w[30], w[4], pars->GC_11, amp[752]); 
  FFV1_0(w[18], w[30], w[217], pars->GC_11, amp[753]); 
  FFV1_0(w[39], w[5], w[218], pars->GC_11, amp[754]); 
  FFV1_0(w[39], w[220], w[4], pars->GC_11, amp[755]); 
  FFV1_0(w[39], w[15], w[217], pars->GC_11, amp[756]); 
  FFS2_0(w[219], w[15], w[2], pars->GC_74, amp[757]); 
  FFS2_0(w[18], w[220], w[2], pars->GC_74, amp[758]); 
  FFS2_0(w[6], w[224], w[8], pars->GC_74, amp[759]); 
  FFS2_0(w[6], w[225], w[8], pars->GC_74, amp[760]); 
  FFS2_0(w[6], w[226], w[8], pars->GC_74, amp[761]); 
  FFS2_0(w[227], w[5], w[8], pars->GC_74, amp[762]); 
  FFS2_0(w[228], w[5], w[8], pars->GC_74, amp[763]); 
  FFS2_0(w[229], w[5], w[8], pars->GC_74, amp[764]); 
  VVS3_0(w[221], w[20], w[8], pars->GC_13, amp[765]); 
  VVS3_0(w[222], w[20], w[8], pars->GC_13, amp[766]); 
  VVS3_0(w[223], w[20], w[8], pars->GC_13, amp[767]); 
  FFV1_0(w[6], w[30], w[230], pars->GC_11, amp[768]); 
  FFV1_0(w[6], w[30], w[231], pars->GC_11, amp[769]); 
  FFV1_0(w[6], w[30], w[232], pars->GC_11, amp[770]); 
  FFS2_0(w[227], w[30], w[3], pars->GC_74, amp[771]); 
  FFS2_0(w[228], w[30], w[3], pars->GC_74, amp[772]); 
  FFS2_0(w[229], w[30], w[3], pars->GC_74, amp[773]); 
  FFV1_0(w[28], w[30], w[221], pars->GC_11, amp[774]); 
  FFV1_0(w[28], w[30], w[222], pars->GC_11, amp[775]); 
  FFV1_0(w[28], w[30], w[223], pars->GC_11, amp[776]); 
  FFV1_0(w[39], w[5], w[230], pars->GC_11, amp[777]); 
  FFV1_0(w[39], w[5], w[231], pars->GC_11, amp[778]); 
  FFV1_0(w[39], w[5], w[232], pars->GC_11, amp[779]); 
  FFS2_0(w[39], w[224], w[3], pars->GC_74, amp[780]); 
  FFS2_0(w[39], w[225], w[3], pars->GC_74, amp[781]); 
  FFS2_0(w[39], w[226], w[3], pars->GC_74, amp[782]); 
  FFV1_0(w[39], w[25], w[221], pars->GC_11, amp[783]); 
  FFV1_0(w[39], w[25], w[222], pars->GC_11, amp[784]); 
  FFV1_0(w[39], w[25], w[223], pars->GC_11, amp[785]); 
  FFV1_0(w[6], w[25], w[233], pars->GC_11, amp[786]); 
  FFV1_0(w[6], w[25], w[234], pars->GC_11, amp[787]); 
  FFV1_0(w[6], w[25], w[235], pars->GC_11, amp[788]); 
  FFS2_0(w[227], w[25], w[2], pars->GC_74, amp[789]); 
  FFS2_0(w[228], w[25], w[2], pars->GC_74, amp[790]); 
  FFS2_0(w[229], w[25], w[2], pars->GC_74, amp[791]); 
  FFV1_0(w[28], w[5], w[233], pars->GC_11, amp[792]); 
  FFV1_0(w[28], w[5], w[234], pars->GC_11, amp[793]); 
  FFV1_0(w[28], w[5], w[235], pars->GC_11, amp[794]); 
  FFS2_0(w[28], w[224], w[2], pars->GC_74, amp[795]); 
  FFS2_0(w[28], w[225], w[2], pars->GC_74, amp[796]); 
  FFS2_0(w[28], w[226], w[2], pars->GC_74, amp[797]); 
  FFS2_0(w[237], w[67], w[3], pars->GC_74, amp[798]); 
  FFV1_0(w[28], w[67], w[236], pars->GC_11, amp[799]); 
  FFS2_0(w[73], w[238], w[3], pars->GC_74, amp[800]); 
  FFV1_0(w[73], w[25], w[236], pars->GC_11, amp[801]); 
  FFV1_0(w[6], w[25], w[239], pars->GC_11, amp[802]); 
  FFV1_0(w[237], w[25], w[1], pars->GC_11, amp[803]); 
  FFV1_0(w[28], w[5], w[239], pars->GC_11, amp[804]); 
  FFV1_0(w[28], w[238], w[1], pars->GC_11, amp[805]); 
  FFS2_0(w[241], w[67], w[2], pars->GC_74, amp[806]); 
  FFV1_0(w[39], w[67], w[240], pars->GC_11, amp[807]); 
  FFS2_0(w[73], w[242], w[2], pars->GC_74, amp[808]); 
  FFV1_0(w[73], w[30], w[240], pars->GC_11, amp[809]); 
  FFV1_0(w[6], w[30], w[243], pars->GC_11, amp[810]); 
  FFV1_0(w[241], w[30], w[1], pars->GC_11, amp[811]); 
  FFV1_0(w[39], w[5], w[243], pars->GC_11, amp[812]); 
  FFV1_0(w[39], w[242], w[1], pars->GC_11, amp[813]); 
  FFV1_0(w[6], w[25], w[244], pars->GC_11, amp[814]); 
  FFV1_0(w[6], w[171], w[152], pars->GC_11, amp[815]); 
  FFV1_0(w[28], w[5], w[244], pars->GC_11, amp[816]); 
  FFV1_0(w[176], w[5], w[152], pars->GC_11, amp[817]); 
  FFV1_0(w[6], w[30], w[245], pars->GC_11, amp[818]); 
  FFV1_0(w[6], w[179], w[153], pars->GC_11, amp[819]); 
  FFV1_0(w[39], w[5], w[245], pars->GC_11, amp[820]); 
  FFV1_0(w[183], w[5], w[153], pars->GC_11, amp[821]); 
  FFV1_0(w[6], w[25], w[246], pars->GC_11, amp[822]); 
  FFV1_0(w[6], w[25], w[247], pars->GC_11, amp[823]); 
  FFV1_0(w[6], w[25], w[248], pars->GC_11, amp[824]); 
  FFV1_0(w[28], w[5], w[246], pars->GC_11, amp[825]); 
  FFV1_0(w[28], w[5], w[247], pars->GC_11, amp[826]); 
  FFV1_0(w[28], w[5], w[248], pars->GC_11, amp[827]); 
  FFV1_0(w[6], w[30], w[249], pars->GC_11, amp[828]); 
  FFV1_0(w[6], w[30], w[250], pars->GC_11, amp[829]); 
  FFV1_0(w[6], w[30], w[251], pars->GC_11, amp[830]); 
  FFV1_0(w[39], w[5], w[249], pars->GC_11, amp[831]); 
  FFV1_0(w[39], w[5], w[250], pars->GC_11, amp[832]); 
  FFV1_0(w[39], w[5], w[251], pars->GC_11, amp[833]); 


}
double PY8MEs_R8_P0_heft_gg_hhgbbx::matrix_8_gg_hhgbbx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 834;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = +amp[0] + amp[1] + Complex<double> (0, 1) * amp[2] +
      Complex<double> (0, 1) * amp[3] + Complex<double> (0, 1) * amp[9] +
      Complex<double> (0, 1) * amp[10] + Complex<double> (0, 1) * amp[11] +
      amp[12] + amp[13] + amp[14] - amp[15] + Complex<double> (0, 1) * amp[16]
      + amp[18] + Complex<double> (0, 1) * amp[19] + amp[21] + Complex<double>
      (0, 1) * amp[23] - amp[25] + amp[26] + amp[27] + amp[30] +
      Complex<double> (0, 1) * amp[31] + amp[32] + Complex<double> (0, 1) *
      amp[34] + amp[36] + Complex<double> (0, 1) * amp[37] + Complex<double>
      (0, 1) * amp[38] + Complex<double> (0, 1) * amp[39] + Complex<double> (0,
      1) * amp[40] - amp[41] + amp[42] + amp[43] + Complex<double> (0, 1) *
      amp[44] + Complex<double> (0, 1) * amp[45] + amp[46] + amp[48] +
      Complex<double> (0, 1) * amp[49] + Complex<double> (0, 1) * amp[51] +
      amp[52] + Complex<double> (0, 1) * amp[56] - amp[59] + amp[60] + amp[61]
      + amp[64] + Complex<double> (0, 1) * amp[65] + Complex<double> (0, 1) *
      amp[66] + Complex<double> (0, 1) * amp[67] + Complex<double> (0, 1) *
      amp[68] - amp[69] + amp[70] + amp[71] + Complex<double> (0, 1) * amp[72]
      + Complex<double> (0, 1) * amp[73] + amp[74] + Complex<double> (0, 1) *
      amp[82] + Complex<double> (0, 1) * amp[83] + Complex<double> (0, 1) *
      amp[84] + Complex<double> (0, 1) * amp[85] + Complex<double> (0, 1) *
      amp[86] + amp[88] + Complex<double> (0, 1) * amp[89] + amp[91] +
      Complex<double> (0, 1) * amp[93] - amp[111] - amp[118] + amp[116] +
      Complex<double> (0, 1) * amp[119] - amp[120] + Complex<double> (0, 1) *
      amp[122] - amp[123] - amp[127] - amp[130] + amp[128] + Complex<double>
      (0, 1) * amp[137] - amp[138] - amp[139] + Complex<double> (0, 1) *
      amp[140] + amp[142] + Complex<double> (0, 1) * amp[143] + amp[145] +
      Complex<double> (0, 1) * amp[147] - amp[165] - amp[172] + amp[170] +
      Complex<double> (0, 1) * amp[173] - amp[174] + Complex<double> (0, 1) *
      amp[176] - amp[177] - amp[181] - amp[184] + amp[182] + Complex<double>
      (0, 1) * amp[191] - amp[192] - amp[193] + Complex<double> (0, 1) *
      amp[280] - amp[283] + Complex<double> (0, 1) * amp[284] - amp[286] -
      amp[287] + Complex<double> (0, 1) * amp[288] - amp[291] + Complex<double>
      (0, 1) * amp[292] - amp[294] - amp[295] + Complex<double> (0, 1) *
      amp[296] + Complex<double> (0, 1) * amp[297] + Complex<double> (0, 1) *
      amp[298] + Complex<double> (0, 1) * amp[299] + Complex<double> (0, 1) *
      amp[300] + Complex<double> (0, 1) * amp[301] + Complex<double> (0, 1) *
      amp[302] + Complex<double> (0, 1) * amp[303] + Complex<double> (0, 1) *
      amp[304] + Complex<double> (0, 1) * amp[305] + Complex<double> (0, 1) *
      amp[306] + Complex<double> (0, 1) * amp[307] + Complex<double> (0, 1) *
      amp[308] - amp[322] - amp[323] + Complex<double> (0, 1) * amp[326] +
      Complex<double> (0, 1) * amp[327] + Complex<double> (0, 1) * amp[328] -
      amp[329] - amp[330] - amp[331] - amp[332] + Complex<double> (0, 1) *
      amp[333] - amp[334] - amp[335] + Complex<double> (0, 1) * amp[337] -
      amp[338] - amp[339] - amp[341] - amp[344] + Complex<double> (0, 1) *
      amp[345] - amp[347] + Complex<double> (0, 1) * amp[348] - amp[349] -
      amp[350] - amp[351] - amp[353] - amp[356] - amp[357] - amp[358] -
      amp[359] - amp[360] - amp[361] + Complex<double> (0, 1) * amp[362] +
      Complex<double> (0, 1) * amp[363] + Complex<double> (0, 1) * amp[364] +
      Complex<double> (0, 1) * amp[365] - amp[453] + Complex<double> (0, 1) *
      amp[454] + amp[456] - amp[460] + amp[458] + Complex<double> (0, 1) *
      amp[461] - amp[462] + Complex<double> (0, 1) * amp[464] - amp[465] +
      amp[468] - amp[472] + amp[470] + Complex<double> (0, 1) * amp[479] -
      amp[483] + Complex<double> (0, 1) * amp[484] + amp[486] - amp[490] +
      amp[488] + Complex<double> (0, 1) * amp[491] - amp[492] + Complex<double>
      (0, 1) * amp[494] - amp[495] + amp[498] - amp[502] + amp[500] +
      Complex<double> (0, 1) * amp[509] + amp[512] + amp[513] + Complex<double>
      (0, 1) * amp[515] + Complex<double> (0, 1) * amp[517] + amp[518] +
      amp[519] + amp[520] - amp[521] + amp[522] + amp[523] + Complex<double>
      (0, 1) * amp[524] + Complex<double> (0, 1) * amp[525] + amp[526] +
      Complex<double> (0, 1) * amp[527] + amp[528] + amp[529] + Complex<double>
      (0, 1) * amp[530] + amp[532] + amp[533] + amp[536] + amp[538] + amp[539]
      + Complex<double> (0, 1) * amp[541] + amp[542] + amp[543] +
      Complex<double> (0, 1) * amp[544] + Complex<double> (0, 1) * amp[545] +
      amp[546] + Complex<double> (0, 1) * amp[547] + amp[548] + amp[549] +
      amp[550] + amp[553] + amp[555] - amp[643] + Complex<double> (0, 1) *
      amp[644] + Complex<double> (0, 1) * amp[647] - amp[648] + Complex<double>
      (0, 1) * amp[649] + amp[650] - amp[652] + amp[654] - amp[657] + amp[655]
      - amp[658] - amp[659] - amp[660] + amp[661] + amp[663] + amp[666] +
      amp[665] - amp[668] - amp[669] + Complex<double> (0, 1) * amp[673] -
      amp[674] + amp[675] - amp[679] + amp[677] + amp[684] - amp[688] +
      amp[686] + Complex<double> (0, 1) * amp[689] - amp[690] + amp[694] -
      amp[697] + amp[695] - amp[699] - amp[704] - amp[705] - amp[709] +
      amp[715] - amp[718] + amp[716] - amp[722] - amp[731] - amp[732] -
      amp[736] + amp[743] + Complex<double> (0, 1) * amp[745] + amp[746] +
      Complex<double> (0, 1) * amp[747] + Complex<double> (0, 1) * amp[750] +
      amp[751] + Complex<double> (0, 1) * amp[753] + amp[754] + Complex<double>
      (0, 1) * amp[755] + Complex<double> (0, 1) * amp[758] + amp[759] -
      amp[761] + amp[762] - amp[764] + amp[765] - amp[767] - amp[770] +
      amp[768] + amp[771] - amp[773] - amp[776] + amp[774] - amp[779] +
      amp[777] - amp[782] + amp[780] - amp[785] + amp[783] - amp[788] +
      amp[786] + amp[789] - amp[791] - amp[794] + amp[792] - amp[797] +
      amp[795] + amp[814] + Complex<double> (0, 1) * amp[815] + amp[816] +
      amp[818] + Complex<double> (0, 1) * amp[819] + amp[820] - amp[824] +
      amp[822] - amp[827] + amp[825] - amp[830] + amp[828] - amp[833] +
      amp[831];
  jamp[1] = -Complex<double> (0, 1) * amp[86] - amp[88] - Complex<double> (0,
      1) * amp[89] - amp[91] - Complex<double> (0, 1) * amp[93] +
      Complex<double> (0, 1) * amp[102] - amp[103] - amp[104] - amp[106] +
      Complex<double> (0, 1) * amp[107] + amp[113] - amp[116] - amp[117] +
      Complex<double> (0, 1) * amp[124] + amp[125] - amp[126] - amp[128] -
      amp[129] - Complex<double> (0, 1) * amp[140] - amp[142] - Complex<double>
      (0, 1) * amp[143] - amp[145] - Complex<double> (0, 1) * amp[147] +
      Complex<double> (0, 1) * amp[156] - amp[157] - amp[158] - amp[160] +
      Complex<double> (0, 1) * amp[161] + amp[167] - amp[170] - amp[171] +
      Complex<double> (0, 1) * amp[178] + amp[179] - amp[180] - amp[182] -
      amp[183] + Complex<double> (0, 1) * amp[194] + amp[196] + Complex<double>
      (0, 1) * amp[197] + amp[199] + Complex<double> (0, 1) * amp[201] +
      Complex<double> (0, 1) * amp[202] + amp[204] + Complex<double> (0, 1) *
      amp[205] + amp[207] + Complex<double> (0, 1) * amp[209] + Complex<double>
      (0, 1) * amp[223] + Complex<double> (0, 1) * amp[224] + Complex<double>
      (0, 1) * amp[225] + Complex<double> (0, 1) * amp[226] + Complex<double>
      (0, 1) * amp[227] + Complex<double> (0, 1) * amp[228] + Complex<double>
      (0, 1) * amp[229] + Complex<double> (0, 1) * amp[230] + Complex<double>
      (0, 1) * amp[231] + Complex<double> (0, 1) * amp[232] + Complex<double>
      (0, 1) * amp[233] + Complex<double> (0, 1) * amp[234] + Complex<double>
      (0, 1) * amp[235] + amp[236] + amp[237] + Complex<double> (0, 1) *
      amp[238] + Complex<double> (0, 1) * amp[239] + amp[242] + amp[243] +
      amp[244] - amp[245] + amp[246] + amp[247] + amp[249] + amp[252] +
      amp[253] + Complex<double> (0, 1) * amp[254] + amp[256] + amp[257] +
      Complex<double> (0, 1) * amp[258] + amp[259] + Complex<double> (0, 1) *
      amp[260] + Complex<double> (0, 1) * amp[261] + amp[262] + amp[263] +
      Complex<double> (0, 1) * amp[265] + amp[266] + amp[267] + amp[269] +
      amp[272] + amp[273] + amp[274] + Complex<double> (0, 1) * amp[275] +
      amp[276] + Complex<double> (0, 1) * amp[277] + Complex<double> (0, 1) *
      amp[278] + amp[279] - Complex<double> (0, 1) * amp[280] - amp[281] -
      amp[282] - Complex<double> (0, 1) * amp[284] - amp[285] - Complex<double>
      (0, 1) * amp[288] - amp[289] - amp[290] - Complex<double> (0, 1) *
      amp[292] - amp[293] - Complex<double> (0, 1) * amp[296] - Complex<double>
      (0, 1) * amp[297] - Complex<double> (0, 1) * amp[298] - Complex<double>
      (0, 1) * amp[299] - Complex<double> (0, 1) * amp[300] - Complex<double>
      (0, 1) * amp[301] - Complex<double> (0, 1) * amp[302] - Complex<double>
      (0, 1) * amp[303] - Complex<double> (0, 1) * amp[304] - Complex<double>
      (0, 1) * amp[305] - Complex<double> (0, 1) * amp[306] - Complex<double>
      (0, 1) * amp[307] - Complex<double> (0, 1) * amp[308] - amp[309] -
      amp[310] - amp[311] - amp[312] - amp[313] - amp[314] - amp[315] -
      amp[316] - amp[317] - amp[318] - amp[319] - amp[320] - amp[321] -
      amp[324] - amp[325] - Complex<double> (0, 1) * amp[326] - Complex<double>
      (0, 1) * amp[327] - Complex<double> (0, 1) * amp[328] - Complex<double>
      (0, 1) * amp[333] - amp[336] - Complex<double> (0, 1) * amp[337] -
      amp[340] - amp[342] - amp[343] - Complex<double> (0, 1) * amp[345] -
      amp[346] - Complex<double> (0, 1) * amp[348] - amp[352] - amp[354] -
      amp[355] - Complex<double> (0, 1) * amp[362] - Complex<double> (0, 1) *
      amp[363] - Complex<double> (0, 1) * amp[364] - Complex<double> (0, 1) *
      amp[365] - Complex<double> (0, 1) * amp[454] - amp[455] - amp[456] -
      amp[458] - amp[459] - amp[468] - amp[470] - amp[471] - Complex<double>
      (0, 1) * amp[484] - amp[485] - amp[486] - amp[488] - amp[489] - amp[498]
      - amp[500] - amp[501] - amp[512] - amp[513] - Complex<double> (0, 1) *
      amp[515] - Complex<double> (0, 1) * amp[517] - amp[518] - amp[519] -
      amp[520] + amp[521] - amp[522] - amp[523] - Complex<double> (0, 1) *
      amp[524] - Complex<double> (0, 1) * amp[525] - amp[526] - Complex<double>
      (0, 1) * amp[527] - amp[528] - amp[529] - Complex<double> (0, 1) *
      amp[530] - amp[532] - amp[533] - amp[536] - amp[538] - amp[539] -
      Complex<double> (0, 1) * amp[541] - amp[542] - amp[543] - Complex<double>
      (0, 1) * amp[544] - Complex<double> (0, 1) * amp[545] - amp[546] -
      Complex<double> (0, 1) * amp[547] - amp[548] - amp[549] - amp[550] -
      amp[553] - amp[555] - amp[598] - Complex<double> (0, 1) * amp[599] -
      amp[600] - Complex<double> (0, 1) * amp[601] + Complex<double> (0, 1) *
      amp[602] + Complex<double> (0, 1) * amp[607] + Complex<double> (0, 1) *
      amp[610] - amp[611] - amp[614] - amp[615] - amp[617] - amp[619] +
      Complex<double> (0, 1) * amp[620] + Complex<double> (0, 1) * amp[622] -
      amp[626] - amp[627] - amp[629] + amp[651] + amp[652] - amp[653] -
      amp[656] - amp[655] + amp[658] + amp[659] + amp[660] - amp[662] -
      amp[664] - amp[665] + amp[667] + amp[670] + amp[669] + amp[672] -
      Complex<double> (0, 1) * amp[673] - amp[675] - amp[677] - amp[678] +
      Complex<double> (0, 1) * amp[680] + amp[681] - amp[684] - amp[686] -
      amp[687] - Complex<double> (0, 1) * amp[689] + amp[692] - amp[694] -
      amp[695] - amp[696] - amp[698] + Complex<double> (0, 1) * amp[712] +
      amp[713] - amp[715] - amp[716] - amp[717] - amp[721] - amp[759] -
      amp[760] - amp[762] - amp[763] - amp[765] - amp[766] - amp[768] -
      amp[769] - amp[771] - amp[772] - amp[774] - amp[775] - amp[777] -
      amp[778] - amp[780] - amp[781] - amp[783] - amp[784] - amp[786] -
      amp[787] - amp[789] - amp[790] - amp[792] - amp[793] - amp[795] -
      amp[796] + Complex<double> (0, 1) * amp[800] + Complex<double> (0, 1) *
      amp[801] + amp[802] + amp[804] + Complex<double> (0, 1) * amp[805] +
      Complex<double> (0, 1) * amp[808] + Complex<double> (0, 1) * amp[809] +
      amp[810] + amp[812] + Complex<double> (0, 1) * amp[813] - amp[814] -
      Complex<double> (0, 1) * amp[815] - amp[816] - amp[818] - Complex<double>
      (0, 1) * amp[819] - amp[820] - amp[823] - amp[822] - amp[826] - amp[825]
      - amp[829] - amp[828] - amp[832] - amp[831];
  jamp[2] = -amp[0] - amp[1] - Complex<double> (0, 1) * amp[2] -
      Complex<double> (0, 1) * amp[3] - Complex<double> (0, 1) * amp[9] -
      Complex<double> (0, 1) * amp[10] - Complex<double> (0, 1) * amp[11] -
      amp[12] - amp[13] - amp[14] + amp[15] - Complex<double> (0, 1) * amp[16]
      - amp[18] - Complex<double> (0, 1) * amp[19] - amp[21] - Complex<double>
      (0, 1) * amp[23] + amp[25] - amp[26] - amp[27] - amp[30] -
      Complex<double> (0, 1) * amp[31] - amp[32] - Complex<double> (0, 1) *
      amp[34] - amp[36] - Complex<double> (0, 1) * amp[37] - Complex<double>
      (0, 1) * amp[38] - Complex<double> (0, 1) * amp[39] - Complex<double> (0,
      1) * amp[40] + amp[41] - amp[42] - amp[43] - Complex<double> (0, 1) *
      amp[44] - Complex<double> (0, 1) * amp[45] - amp[46] - amp[48] -
      Complex<double> (0, 1) * amp[49] - Complex<double> (0, 1) * amp[51] -
      amp[52] - Complex<double> (0, 1) * amp[56] + amp[59] - amp[60] - amp[61]
      - amp[64] - Complex<double> (0, 1) * amp[65] - Complex<double> (0, 1) *
      amp[66] - Complex<double> (0, 1) * amp[67] - Complex<double> (0, 1) *
      amp[68] + amp[69] - amp[70] - amp[71] - Complex<double> (0, 1) * amp[72]
      - Complex<double> (0, 1) * amp[73] - amp[74] - Complex<double> (0, 1) *
      amp[82] - Complex<double> (0, 1) * amp[83] - Complex<double> (0, 1) *
      amp[84] - Complex<double> (0, 1) * amp[85] + Complex<double> (0, 1) *
      amp[94] - amp[97] + Complex<double> (0, 1) * amp[99] - amp[100] -
      amp[101] + amp[111] + Complex<double> (0, 1) * amp[112] - amp[113] +
      amp[118] + amp[117] - Complex<double> (0, 1) * amp[119] - amp[121] -
      Complex<double> (0, 1) * amp[122] + amp[123] - amp[125] + amp[130] +
      amp[129] - Complex<double> (0, 1) * amp[137] + Complex<double> (0, 1) *
      amp[148] - amp[151] + Complex<double> (0, 1) * amp[153] - amp[154] -
      amp[155] + amp[165] + Complex<double> (0, 1) * amp[166] - amp[167] +
      amp[172] + amp[171] - Complex<double> (0, 1) * amp[173] - amp[175] -
      Complex<double> (0, 1) * amp[176] + amp[177] - amp[179] + amp[184] +
      amp[183] - Complex<double> (0, 1) * amp[191] + Complex<double> (0, 1) *
      amp[195] - amp[196] + Complex<double> (0, 1) * amp[198] - amp[199] +
      Complex<double> (0, 1) * amp[200] + Complex<double> (0, 1) * amp[203] -
      amp[204] + Complex<double> (0, 1) * amp[206] - amp[207] + Complex<double>
      (0, 1) * amp[208] + Complex<double> (0, 1) * amp[210] + Complex<double>
      (0, 1) * amp[211] + Complex<double> (0, 1) * amp[212] + Complex<double>
      (0, 1) * amp[213] + Complex<double> (0, 1) * amp[214] + Complex<double>
      (0, 1) * amp[215] + Complex<double> (0, 1) * amp[216] + Complex<double>
      (0, 1) * amp[217] + Complex<double> (0, 1) * amp[218] + Complex<double>
      (0, 1) * amp[219] + Complex<double> (0, 1) * amp[220] + Complex<double>
      (0, 1) * amp[221] + Complex<double> (0, 1) * amp[222] - amp[236] -
      amp[237] + Complex<double> (0, 1) * amp[240] + Complex<double> (0, 1) *
      amp[241] - amp[242] - amp[243] - amp[244] + amp[245] - amp[246] -
      amp[247] + Complex<double> (0, 1) * amp[248] - amp[249] + Complex<double>
      (0, 1) * amp[250] + Complex<double> (0, 1) * amp[251] - amp[252] -
      amp[253] + Complex<double> (0, 1) * amp[255] - amp[256] - amp[257] -
      amp[259] - amp[262] - amp[263] + Complex<double> (0, 1) * amp[264] -
      amp[266] - amp[267] + Complex<double> (0, 1) * amp[268] - amp[269] +
      Complex<double> (0, 1) * amp[270] + Complex<double> (0, 1) * amp[271] -
      amp[272] - amp[273] - amp[274] - amp[276] - amp[279] + amp[453] +
      amp[460] + amp[459] - Complex<double> (0, 1) * amp[461] - amp[463] -
      Complex<double> (0, 1) * amp[464] + amp[465] - amp[469] + amp[472] +
      amp[471] - Complex<double> (0, 1) * amp[479] - amp[480] - amp[481] +
      amp[483] + amp[490] + amp[489] - Complex<double> (0, 1) * amp[491] -
      amp[493] - Complex<double> (0, 1) * amp[494] + amp[495] - amp[499] +
      amp[502] + amp[501] - Complex<double> (0, 1) * amp[509] - amp[510] -
      amp[511] - amp[556] - amp[557] - Complex<double> (0, 1) * amp[559] -
      Complex<double> (0, 1) * amp[561] + Complex<double> (0, 1) * amp[562] -
      amp[563] - amp[564] - amp[565] - amp[566] + Complex<double> (0, 1) *
      amp[567] - amp[568] - amp[569] + Complex<double> (0, 1) * amp[570] -
      amp[572] - amp[573] - amp[576] - amp[578] + Complex<double> (0, 1) *
      amp[580] - amp[581] + Complex<double> (0, 1) * amp[582] - amp[583] -
      amp[584] - amp[585] - amp[588] - amp[590] - amp[591] - amp[592] -
      amp[593] - amp[594] - amp[595] - Complex<double> (0, 1) * amp[644] -
      amp[645] - amp[646] - Complex<double> (0, 1) * amp[647] - Complex<double>
      (0, 1) * amp[649] - amp[650] - amp[651] + amp[653] - amp[654] + amp[657]
      + amp[656] - amp[661] + amp[662] - amp[663] - amp[666] + amp[664] -
      amp[667] - amp[670] + amp[668] + Complex<double> (0, 1) * amp[671] -
      amp[672] - amp[676] + amp[679] + amp[678] - amp[681] + amp[688] +
      amp[687] + Complex<double> (0, 1) * amp[691] - amp[692] - amp[693] +
      amp[697] + amp[696] - amp[702] - amp[706] - amp[707] - amp[708] -
      amp[713] + amp[718] + amp[717] - amp[724] - amp[733] - amp[734] -
      amp[735] - amp[743] - Complex<double> (0, 1) * amp[745] - amp[746] -
      Complex<double> (0, 1) * amp[747] - Complex<double> (0, 1) * amp[750] -
      amp[751] - Complex<double> (0, 1) * amp[753] - amp[754] - Complex<double>
      (0, 1) * amp[755] - Complex<double> (0, 1) * amp[758] + amp[760] +
      amp[761] + amp[763] + amp[764] + amp[766] + amp[767] + amp[770] +
      amp[769] + amp[772] + amp[773] + amp[776] + amp[775] + amp[779] +
      amp[778] + amp[782] + amp[781] + amp[785] + amp[784] + amp[788] +
      amp[787] + amp[790] + amp[791] + amp[794] + amp[793] + amp[797] +
      amp[796] + Complex<double> (0, 1) * amp[798] + Complex<double> (0, 1) *
      amp[799] - amp[802] + Complex<double> (0, 1) * amp[803] - amp[804] +
      Complex<double> (0, 1) * amp[806] + Complex<double> (0, 1) * amp[807] -
      amp[810] + Complex<double> (0, 1) * amp[811] - amp[812] + amp[824] +
      amp[823] + amp[827] + amp[826] + amp[830] + amp[829] + amp[833] +
      amp[832];
  jamp[3] = +Complex<double> (0, 1) * amp[87] - amp[88] + Complex<double> (0,
      1) * amp[90] - amp[91] + Complex<double> (0, 1) * amp[92] -
      Complex<double> (0, 1) * amp[94] - amp[95] - amp[96] - amp[98] -
      Complex<double> (0, 1) * amp[99] - Complex<double> (0, 1) * amp[112] +
      amp[113] - amp[114] - amp[116] - amp[117] + amp[125] - amp[128] -
      amp[129] + Complex<double> (0, 1) * amp[141] - amp[142] + Complex<double>
      (0, 1) * amp[144] - amp[145] + Complex<double> (0, 1) * amp[146] -
      Complex<double> (0, 1) * amp[148] - amp[149] - amp[150] - amp[152] -
      Complex<double> (0, 1) * amp[153] - Complex<double> (0, 1) * amp[166] +
      amp[167] - amp[168] - amp[170] - amp[171] + amp[179] - amp[182] -
      amp[183] - Complex<double> (0, 1) * amp[195] + amp[196] - Complex<double>
      (0, 1) * amp[198] + amp[199] - Complex<double> (0, 1) * amp[200] -
      Complex<double> (0, 1) * amp[203] + amp[204] - Complex<double> (0, 1) *
      amp[206] + amp[207] - Complex<double> (0, 1) * amp[208] - Complex<double>
      (0, 1) * amp[210] - Complex<double> (0, 1) * amp[211] - Complex<double>
      (0, 1) * amp[212] - Complex<double> (0, 1) * amp[213] - Complex<double>
      (0, 1) * amp[214] - Complex<double> (0, 1) * amp[215] - Complex<double>
      (0, 1) * amp[216] - Complex<double> (0, 1) * amp[217] - Complex<double>
      (0, 1) * amp[218] - Complex<double> (0, 1) * amp[219] - Complex<double>
      (0, 1) * amp[220] - Complex<double> (0, 1) * amp[221] - Complex<double>
      (0, 1) * amp[222] + amp[236] + amp[237] - Complex<double> (0, 1) *
      amp[240] - Complex<double> (0, 1) * amp[241] + amp[242] + amp[243] +
      amp[244] - amp[245] + amp[246] + amp[247] - Complex<double> (0, 1) *
      amp[248] + amp[249] - Complex<double> (0, 1) * amp[250] - Complex<double>
      (0, 1) * amp[251] + amp[252] + amp[253] - Complex<double> (0, 1) *
      amp[255] + amp[256] + amp[257] + amp[259] + amp[262] + amp[263] -
      Complex<double> (0, 1) * amp[264] + amp[266] + amp[267] - Complex<double>
      (0, 1) * amp[268] + amp[269] - Complex<double> (0, 1) * amp[270] -
      Complex<double> (0, 1) * amp[271] + amp[272] + amp[273] + amp[274] +
      amp[276] + amp[279] + Complex<double> (0, 1) * amp[366] - amp[367] -
      amp[368] + Complex<double> (0, 1) * amp[370] - amp[371] + Complex<double>
      (0, 1) * amp[374] - amp[375] - amp[376] + Complex<double> (0, 1) *
      amp[378] - amp[379] + Complex<double> (0, 1) * amp[382] + Complex<double>
      (0, 1) * amp[383] + Complex<double> (0, 1) * amp[384] + Complex<double>
      (0, 1) * amp[385] + Complex<double> (0, 1) * amp[386] + Complex<double>
      (0, 1) * amp[387] + Complex<double> (0, 1) * amp[388] + Complex<double>
      (0, 1) * amp[389] + Complex<double> (0, 1) * amp[390] + Complex<double>
      (0, 1) * amp[391] + Complex<double> (0, 1) * amp[392] + Complex<double>
      (0, 1) * amp[393] + Complex<double> (0, 1) * amp[394] - amp[395] -
      amp[396] - amp[397] - amp[398] - amp[399] - amp[400] - amp[401] -
      amp[402] - amp[403] - amp[404] - amp[405] - amp[406] - amp[407] -
      amp[410] - amp[411] + Complex<double> (0, 1) * amp[412] + Complex<double>
      (0, 1) * amp[413] + Complex<double> (0, 1) * amp[414] + Complex<double>
      (0, 1) * amp[419] - amp[422] + Complex<double> (0, 1) * amp[423] -
      amp[426] - amp[428] - amp[429] + Complex<double> (0, 1) * amp[431] -
      amp[432] + Complex<double> (0, 1) * amp[434] - amp[438] - amp[440] -
      amp[441] + Complex<double> (0, 1) * amp[448] + Complex<double> (0, 1) *
      amp[449] + Complex<double> (0, 1) * amp[450] + Complex<double> (0, 1) *
      amp[451] - amp[456] - amp[458] - amp[459] + Complex<double> (0, 1) *
      amp[466] - amp[467] - amp[468] - amp[470] - amp[471] - amp[486] -
      amp[488] - amp[489] + Complex<double> (0, 1) * amp[496] - amp[497] -
      amp[498] - amp[500] - amp[501] - amp[512] - amp[513] + Complex<double>
      (0, 1) * amp[514] + Complex<double> (0, 1) * amp[516] - amp[518] -
      amp[519] - amp[520] + amp[521] - amp[522] - amp[523] - amp[526] -
      amp[528] - amp[529] + Complex<double> (0, 1) * amp[531] - amp[532] -
      amp[533] + Complex<double> (0, 1) * amp[534] + Complex<double> (0, 1) *
      amp[535] - amp[536] + Complex<double> (0, 1) * amp[537] - amp[538] -
      amp[539] + Complex<double> (0, 1) * amp[540] - amp[542] - amp[543] -
      amp[546] - amp[548] - amp[549] - amp[550] + Complex<double> (0, 1) *
      amp[551] + Complex<double> (0, 1) * amp[552] - amp[553] + Complex<double>
      (0, 1) * amp[554] - amp[555] - amp[558] + Complex<double> (0, 1) *
      amp[559] - amp[560] + Complex<double> (0, 1) * amp[561] - Complex<double>
      (0, 1) * amp[562] - Complex<double> (0, 1) * amp[567] - Complex<double>
      (0, 1) * amp[570] - amp[571] - amp[574] - amp[575] - amp[577] - amp[579]
      - Complex<double> (0, 1) * amp[580] - Complex<double> (0, 1) * amp[582] -
      amp[586] - amp[587] - amp[589] + amp[651] + amp[652] - amp[653] -
      amp[656] - amp[655] + amp[658] + amp[659] + amp[660] - amp[662] -
      amp[664] - amp[665] + amp[667] + amp[670] + amp[669] - Complex<double>
      (0, 1) * amp[671] + amp[672] - amp[675] - amp[677] - amp[678] + amp[681]
      + Complex<double> (0, 1) * amp[682] - amp[684] - amp[686] - amp[687] -
      Complex<double> (0, 1) * amp[691] + amp[692] - amp[694] - amp[695] -
      amp[696] - amp[700] + Complex<double> (0, 1) * amp[710] + amp[713] -
      amp[715] - amp[716] - amp[717] - amp[719] - amp[759] - amp[760] -
      amp[762] - amp[763] - amp[765] - amp[766] - amp[768] - amp[769] -
      amp[771] - amp[772] - amp[774] - amp[775] - amp[777] - amp[778] -
      amp[780] - amp[781] - amp[783] - amp[784] - amp[786] - amp[787] -
      amp[789] - amp[790] - amp[792] - amp[793] - amp[795] - amp[796] -
      Complex<double> (0, 1) * amp[798] - Complex<double> (0, 1) * amp[799] +
      amp[802] - Complex<double> (0, 1) * amp[803] + amp[804] - Complex<double>
      (0, 1) * amp[806] - Complex<double> (0, 1) * amp[807] + amp[810] -
      Complex<double> (0, 1) * amp[811] + amp[812] - amp[814] - amp[816] +
      Complex<double> (0, 1) * amp[817] - amp[818] - amp[820] + Complex<double>
      (0, 1) * amp[821] - amp[823] - amp[822] - amp[826] - amp[825] - amp[829]
      - amp[828] - amp[832] - amp[831];
  jamp[4] = -amp[0] - amp[1] + Complex<double> (0, 1) * amp[4] +
      Complex<double> (0, 1) * amp[5] + Complex<double> (0, 1) * amp[6] +
      Complex<double> (0, 1) * amp[7] + Complex<double> (0, 1) * amp[8] -
      amp[12] - amp[13] - amp[14] + amp[15] + Complex<double> (0, 1) * amp[17]
      - amp[18] + Complex<double> (0, 1) * amp[20] - amp[21] + Complex<double>
      (0, 1) * amp[22] + Complex<double> (0, 1) * amp[24] + amp[25] - amp[26] -
      amp[27] + Complex<double> (0, 1) * amp[28] + Complex<double> (0, 1) *
      amp[29] - amp[30] - amp[32] + Complex<double> (0, 1) * amp[33] +
      Complex<double> (0, 1) * amp[35] - amp[36] + amp[41] - amp[42] - amp[43]
      - amp[46] + Complex<double> (0, 1) * amp[47] - amp[48] + Complex<double>
      (0, 1) * amp[50] - amp[52] + Complex<double> (0, 1) * amp[53] +
      Complex<double> (0, 1) * amp[54] + Complex<double> (0, 1) * amp[55] +
      Complex<double> (0, 1) * amp[57] + Complex<double> (0, 1) * amp[58] +
      amp[59] - amp[60] - amp[61] + Complex<double> (0, 1) * amp[62] +
      Complex<double> (0, 1) * amp[63] - amp[64] + amp[69] - amp[70] - amp[71]
      - amp[74] + Complex<double> (0, 1) * amp[75] + Complex<double> (0, 1) *
      amp[76] + Complex<double> (0, 1) * amp[77] + Complex<double> (0, 1) *
      amp[78] + Complex<double> (0, 1) * amp[79] + Complex<double> (0, 1) *
      amp[80] + Complex<double> (0, 1) * amp[81] - Complex<double> (0, 1) *
      amp[102] - amp[105] - Complex<double> (0, 1) * amp[107] - amp[108] -
      amp[109] + Complex<double> (0, 1) * amp[110] + amp[111] - amp[113] +
      amp[118] + amp[117] + amp[123] - Complex<double> (0, 1) * amp[124] -
      amp[125] + amp[130] + amp[129] + Complex<double> (0, 1) * amp[131] -
      amp[133] + Complex<double> (0, 1) * amp[134] - Complex<double> (0, 1) *
      amp[156] - amp[159] - Complex<double> (0, 1) * amp[161] - amp[162] -
      amp[163] + Complex<double> (0, 1) * amp[164] + amp[165] - amp[167] +
      amp[172] + amp[171] + amp[177] - Complex<double> (0, 1) * amp[178] -
      amp[179] + amp[184] + amp[183] + Complex<double> (0, 1) * amp[185] -
      amp[187] + Complex<double> (0, 1) * amp[188] - Complex<double> (0, 1) *
      amp[194] - amp[196] - Complex<double> (0, 1) * amp[197] - amp[199] -
      Complex<double> (0, 1) * amp[201] - Complex<double> (0, 1) * amp[202] -
      amp[204] - Complex<double> (0, 1) * amp[205] - amp[207] - Complex<double>
      (0, 1) * amp[209] - Complex<double> (0, 1) * amp[223] - Complex<double>
      (0, 1) * amp[224] - Complex<double> (0, 1) * amp[225] - Complex<double>
      (0, 1) * amp[226] - Complex<double> (0, 1) * amp[227] - Complex<double>
      (0, 1) * amp[228] - Complex<double> (0, 1) * amp[229] - Complex<double>
      (0, 1) * amp[230] - Complex<double> (0, 1) * amp[231] - Complex<double>
      (0, 1) * amp[232] - Complex<double> (0, 1) * amp[233] - Complex<double>
      (0, 1) * amp[234] - Complex<double> (0, 1) * amp[235] - amp[236] -
      amp[237] - Complex<double> (0, 1) * amp[238] - Complex<double> (0, 1) *
      amp[239] - amp[242] - amp[243] - amp[244] + amp[245] - amp[246] -
      amp[247] - amp[249] - amp[252] - amp[253] - Complex<double> (0, 1) *
      amp[254] - amp[256] - amp[257] - Complex<double> (0, 1) * amp[258] -
      amp[259] - Complex<double> (0, 1) * amp[260] - Complex<double> (0, 1) *
      amp[261] - amp[262] - amp[263] - Complex<double> (0, 1) * amp[265] -
      amp[266] - amp[267] - amp[269] - amp[272] - amp[273] - amp[274] -
      Complex<double> (0, 1) * amp[275] - amp[276] - Complex<double> (0, 1) *
      amp[277] - Complex<double> (0, 1) * amp[278] - amp[279] + Complex<double>
      (0, 1) * amp[452] + amp[453] - amp[457] + amp[460] + amp[459] + amp[465]
      + amp[472] + amp[471] + Complex<double> (0, 1) * amp[473] - amp[475] +
      Complex<double> (0, 1) * amp[476] - amp[477] - amp[478] + Complex<double>
      (0, 1) * amp[482] + amp[483] - amp[487] + amp[490] + amp[489] + amp[495]
      + amp[502] + amp[501] + Complex<double> (0, 1) * amp[503] - amp[505] +
      Complex<double> (0, 1) * amp[506] - amp[507] - amp[508] - amp[596] -
      amp[597] + Complex<double> (0, 1) * amp[599] + Complex<double> (0, 1) *
      amp[601] - Complex<double> (0, 1) * amp[602] - amp[603] - amp[604] -
      amp[605] - amp[606] - Complex<double> (0, 1) * amp[607] - amp[608] -
      amp[609] - Complex<double> (0, 1) * amp[610] - amp[612] - amp[613] -
      amp[616] - amp[618] - Complex<double> (0, 1) * amp[620] - amp[621] -
      Complex<double> (0, 1) * amp[622] - amp[623] - amp[624] - amp[625] -
      amp[628] - amp[630] - amp[631] - amp[632] - amp[633] - amp[634] -
      amp[635] + Complex<double> (0, 1) * amp[637] - amp[638] - amp[639] +
      Complex<double> (0, 1) * amp[640] + Complex<double> (0, 1) * amp[642] -
      amp[650] - amp[651] + amp[653] - amp[654] + amp[657] + amp[656] -
      amp[661] + amp[662] - amp[663] - amp[666] + amp[664] - amp[667] -
      amp[670] + amp[668] - amp[672] + amp[679] + amp[678] - Complex<double>
      (0, 1) * amp[680] - amp[681] - amp[685] + amp[688] + amp[687] - amp[692]
      + amp[697] + amp[696] - amp[703] - Complex<double> (0, 1) * amp[712] -
      amp[713] - amp[714] + amp[718] + amp[717] - amp[723] - amp[727] -
      amp[728] - amp[729] - amp[739] - amp[740] - amp[741] - amp[743] +
      Complex<double> (0, 1) * amp[744] - amp[746] + Complex<double> (0, 1) *
      amp[748] + Complex<double> (0, 1) * amp[749] - amp[751] + Complex<double>
      (0, 1) * amp[752] - amp[754] + Complex<double> (0, 1) * amp[756] +
      Complex<double> (0, 1) * amp[757] + amp[760] + amp[761] + amp[763] +
      amp[764] + amp[766] + amp[767] + amp[770] + amp[769] + amp[772] +
      amp[773] + amp[776] + amp[775] + amp[779] + amp[778] + amp[782] +
      amp[781] + amp[785] + amp[784] + amp[788] + amp[787] + amp[790] +
      amp[791] + amp[794] + amp[793] + amp[797] + amp[796] - Complex<double>
      (0, 1) * amp[800] - Complex<double> (0, 1) * amp[801] - amp[802] -
      amp[804] - Complex<double> (0, 1) * amp[805] - Complex<double> (0, 1) *
      amp[808] - Complex<double> (0, 1) * amp[809] - amp[810] - amp[812] -
      Complex<double> (0, 1) * amp[813] + amp[824] + amp[823] + amp[827] +
      amp[826] + amp[830] + amp[829] + amp[833] + amp[832];
  jamp[5] = +amp[0] + amp[1] - Complex<double> (0, 1) * amp[4] -
      Complex<double> (0, 1) * amp[5] - Complex<double> (0, 1) * amp[6] -
      Complex<double> (0, 1) * amp[7] - Complex<double> (0, 1) * amp[8] +
      amp[12] + amp[13] + amp[14] - amp[15] - Complex<double> (0, 1) * amp[17]
      + amp[18] - Complex<double> (0, 1) * amp[20] + amp[21] - Complex<double>
      (0, 1) * amp[22] - Complex<double> (0, 1) * amp[24] - amp[25] + amp[26] +
      amp[27] - Complex<double> (0, 1) * amp[28] - Complex<double> (0, 1) *
      amp[29] + amp[30] + amp[32] - Complex<double> (0, 1) * amp[33] -
      Complex<double> (0, 1) * amp[35] + amp[36] - amp[41] + amp[42] + amp[43]
      + amp[46] - Complex<double> (0, 1) * amp[47] + amp[48] - Complex<double>
      (0, 1) * amp[50] + amp[52] - Complex<double> (0, 1) * amp[53] -
      Complex<double> (0, 1) * amp[54] - Complex<double> (0, 1) * amp[55] -
      Complex<double> (0, 1) * amp[57] - Complex<double> (0, 1) * amp[58] -
      amp[59] + amp[60] + amp[61] - Complex<double> (0, 1) * amp[62] -
      Complex<double> (0, 1) * amp[63] + amp[64] - amp[69] + amp[70] + amp[71]
      + amp[74] - Complex<double> (0, 1) * amp[75] - Complex<double> (0, 1) *
      amp[76] - Complex<double> (0, 1) * amp[77] - Complex<double> (0, 1) *
      amp[78] - Complex<double> (0, 1) * amp[79] - Complex<double> (0, 1) *
      amp[80] - Complex<double> (0, 1) * amp[81] - Complex<double> (0, 1) *
      amp[87] + amp[88] - Complex<double> (0, 1) * amp[90] + amp[91] -
      Complex<double> (0, 1) * amp[92] - Complex<double> (0, 1) * amp[110] -
      amp[111] - amp[115] - amp[118] + amp[116] - amp[123] - amp[130] +
      amp[128] - Complex<double> (0, 1) * amp[131] - amp[132] - Complex<double>
      (0, 1) * amp[134] - amp[135] - amp[136] - Complex<double> (0, 1) *
      amp[141] + amp[142] - Complex<double> (0, 1) * amp[144] + amp[145] -
      Complex<double> (0, 1) * amp[146] - Complex<double> (0, 1) * amp[164] -
      amp[165] - amp[169] - amp[172] + amp[170] - amp[177] - amp[184] +
      amp[182] - Complex<double> (0, 1) * amp[185] - amp[186] - Complex<double>
      (0, 1) * amp[188] - amp[189] - amp[190] - Complex<double> (0, 1) *
      amp[366] - amp[369] - Complex<double> (0, 1) * amp[370] - amp[372] -
      amp[373] - Complex<double> (0, 1) * amp[374] - amp[377] - Complex<double>
      (0, 1) * amp[378] - amp[380] - amp[381] - Complex<double> (0, 1) *
      amp[382] - Complex<double> (0, 1) * amp[383] - Complex<double> (0, 1) *
      amp[384] - Complex<double> (0, 1) * amp[385] - Complex<double> (0, 1) *
      amp[386] - Complex<double> (0, 1) * amp[387] - Complex<double> (0, 1) *
      amp[388] - Complex<double> (0, 1) * amp[389] - Complex<double> (0, 1) *
      amp[390] - Complex<double> (0, 1) * amp[391] - Complex<double> (0, 1) *
      amp[392] - Complex<double> (0, 1) * amp[393] - Complex<double> (0, 1) *
      amp[394] - amp[408] - amp[409] - Complex<double> (0, 1) * amp[412] -
      Complex<double> (0, 1) * amp[413] - Complex<double> (0, 1) * amp[414] -
      amp[415] - amp[416] - amp[417] - amp[418] - Complex<double> (0, 1) *
      amp[419] - amp[420] - amp[421] - Complex<double> (0, 1) * amp[423] -
      amp[424] - amp[425] - amp[427] - amp[430] - Complex<double> (0, 1) *
      amp[431] - amp[433] - Complex<double> (0, 1) * amp[434] - amp[435] -
      amp[436] - amp[437] - amp[439] - amp[442] - amp[443] - amp[444] -
      amp[445] - amp[446] - amp[447] - Complex<double> (0, 1) * amp[448] -
      Complex<double> (0, 1) * amp[449] - Complex<double> (0, 1) * amp[450] -
      Complex<double> (0, 1) * amp[451] - Complex<double> (0, 1) * amp[452] -
      amp[453] + amp[456] - amp[460] + amp[458] - amp[465] - Complex<double>
      (0, 1) * amp[466] + amp[468] - amp[472] + amp[470] - Complex<double> (0,
      1) * amp[473] - amp[474] - Complex<double> (0, 1) * amp[476] -
      Complex<double> (0, 1) * amp[482] - amp[483] + amp[486] - amp[490] +
      amp[488] - amp[495] - Complex<double> (0, 1) * amp[496] + amp[498] -
      amp[502] + amp[500] - Complex<double> (0, 1) * amp[503] - amp[504] -
      Complex<double> (0, 1) * amp[506] + amp[512] + amp[513] - Complex<double>
      (0, 1) * amp[514] - Complex<double> (0, 1) * amp[516] + amp[518] +
      amp[519] + amp[520] - amp[521] + amp[522] + amp[523] + amp[526] +
      amp[528] + amp[529] - Complex<double> (0, 1) * amp[531] + amp[532] +
      amp[533] - Complex<double> (0, 1) * amp[534] - Complex<double> (0, 1) *
      amp[535] + amp[536] - Complex<double> (0, 1) * amp[537] + amp[538] +
      amp[539] - Complex<double> (0, 1) * amp[540] + amp[542] + amp[543] +
      amp[546] + amp[548] + amp[549] + amp[550] - Complex<double> (0, 1) *
      amp[551] - Complex<double> (0, 1) * amp[552] + amp[553] - Complex<double>
      (0, 1) * amp[554] + amp[555] - amp[636] - Complex<double> (0, 1) *
      amp[637] - Complex<double> (0, 1) * amp[640] - amp[641] - Complex<double>
      (0, 1) * amp[642] + amp[650] - amp[652] + amp[654] - amp[657] + amp[655]
      - amp[658] - amp[659] - amp[660] + amp[661] + amp[663] + amp[666] +
      amp[665] - amp[668] - amp[669] + amp[675] - amp[679] + amp[677] -
      Complex<double> (0, 1) * amp[682] - amp[683] + amp[684] - amp[688] +
      amp[686] + amp[694] - amp[697] + amp[695] - amp[701] - Complex<double>
      (0, 1) * amp[710] - amp[711] + amp[715] - amp[718] + amp[716] - amp[720]
      - amp[725] - amp[726] - amp[730] - amp[737] - amp[738] - amp[742] +
      amp[743] - Complex<double> (0, 1) * amp[744] + amp[746] - Complex<double>
      (0, 1) * amp[748] - Complex<double> (0, 1) * amp[749] + amp[751] -
      Complex<double> (0, 1) * amp[752] + amp[754] - Complex<double> (0, 1) *
      amp[756] - Complex<double> (0, 1) * amp[757] + amp[759] - amp[761] +
      amp[762] - amp[764] + amp[765] - amp[767] - amp[770] + amp[768] +
      amp[771] - amp[773] - amp[776] + amp[774] - amp[779] + amp[777] -
      amp[782] + amp[780] - amp[785] + amp[783] - amp[788] + amp[786] +
      amp[789] - amp[791] - amp[794] + amp[792] - amp[797] + amp[795] +
      amp[814] + amp[816] - Complex<double> (0, 1) * amp[817] + amp[818] +
      amp[820] - Complex<double> (0, 1) * amp[821] - amp[824] + amp[822] -
      amp[827] + amp[825] - amp[830] + amp[828] - amp[833] + amp[831];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[0][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}


}  // end namespace PY8MEs_namespace

