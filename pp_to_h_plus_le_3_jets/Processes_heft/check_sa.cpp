#include <iostream> 
#include <sstream> 
#include <iomanip> 
#include <vector> 
#include <set> 

#include "PY8ME.h"
#include "PY8MEs.h"
#include "rambo.h"

using namespace std; 

typedef vector<double> vec_double; 

// Nice string to display a process
string proc_string(vector<int> in_pdgs, vector<int> out_pdgs, set<int> 
req_s_channels = set<int> ())
{

  std::stringstream ss; 

  for (unsigned int i = 0; i < in_pdgs.size(); i++ )
    ss << in_pdgs[i] <<  " "; 
  if (req_s_channels.size() > 0)
  {
    ss <<  "> "; 
    for (set<int> ::iterator it = req_s_channels.begin(); it != 
      req_s_channels.end(); ++ it)
    ss << * it <<  " "; 
  }
  ss <<  "> "; 
  for (unsigned int i = 0; i < out_pdgs.size(); i++ )
  {
    ss << out_pdgs[i]; 
    if (i != (out_pdgs.size() - 1))
      ss <<  " "; 
  }

  return ss.str(); 
}

// Evaluate a given process with an accessor
void run_proc(PY8MEs_namespace::PY8MEs& accessor, vector<int> in_pdgs,
    vector<int> out_pdgs, set<int> req_s_channels = set<int> ())
{

  // This is not mandatory, we run it here only because we need an instance of
  // the process
  // to obtain the external masses to generate the PS point.
  PY8MEs_namespace::PY8ME * query = accessor.getProcess(in_pdgs, out_pdgs,
      req_s_channels);

  cout <<  " -> Process '"; 
  cout << proc_string(in_pdgs, out_pdgs, req_s_channels) <<  "'"; 
  if (query)
  {
    cout <<  " is available." << endl; 
  }
  else
  {
    cout <<  " is not available." << endl; 
    return; 
  }

  double energy = 1500; 
  double weight; 

  vector < vec_double > p_vec = vector < vec_double > (in_pdgs.size() +
      out_pdgs.size(), vec_double(4, 0.));

  //----
  // The line below 'get_momenta' that uses RAMBO has memory leaks. It is fine
  // for testing/debugging.
  // But replace with an hard-coded momentum configuration if you want to
  // memory-check the ME code.
  //----
  // Get phase space point
  vector < double * > p = get_momenta(in_pdgs.size(), energy,
      query->getMasses(), weight);
  // Cast to a sane type
  for (unsigned int i = 0; i < in_pdgs.size() + out_pdgs.size(); i++ )
  {
    for (unsigned int j = 0; j < 4; j++ )
    {
      p_vec[i][j] = p[i][j]; 
      // Copy-paste the printout below in this code to use an hard-coded
      // momentum configuration
      // cout<<setiosflags(ios::scientific) << setprecision(14) <<
      // setw(22)<<"p_vec["<<i<<"]["<<j<<"]"<<"="<<p_vec[i][j]<<";"<<endl;
    }
  }
  // Release arrays
  for(unsigned int i = 0; i < (in_pdgs.size() + out_pdgs.size()); i++ )
  {
    delete[] p[i]; 
    p[i] = NULL; 
  }
  //---
  // End of the segment that can cause memory leak.
  //---

  // You could require specific helicity and quantum numbers here
  // (these arguments, like req_s_channels are optional for calculateME and
  // considerd empty by default)
  // They are left empty here, meaning that these quantum numbers will be
  // summed/averaged over.
  // This vector's size would be twice the number of external legs (for color
  // and anti-color specification)
  vector<int> colors(0); 
  // This vector's size would be the number of external legs
  vector<int> helicities(0); 
  pair < double, bool > res = accessor.calculateME(in_pdgs, out_pdgs, p_vec,
      req_s_channels, colors, helicities);

  if ( !res.second)
  {
    cout <<  " | Its evaluation failed." << endl; 
    return; 
  }
  else
  {
    cout <<  " | Momenta:" << endl; 
    for(unsigned int i = 0; i < (in_pdgs.size() + out_pdgs.size()); i++ )
      cout <<  " | " << setw(4) << i + 1
     << setiosflags(ios::scientific) << setprecision(14) << setw(22) <<
        p_vec[i][0]
     << setiosflags(ios::scientific) << setprecision(14) << setw(22) <<
        p_vec[i][1]
     << setiosflags(ios::scientific) << setprecision(14) << setw(22) <<
        p_vec[i][2]
     << setiosflags(ios::scientific) << setprecision(14) << setw(22) <<
        p_vec[i][3] << endl;
    cout <<  " | Matrix element : " << setiosflags(ios::scientific) <<
        setprecision(17) << res.first;
    cout <<  " GeV^" << - (2 * int(in_pdgs.size() + out_pdgs.size()) - 8) <<
        endl;
  }
}

int main(int argc, char * * argv)
{
  // Prevent unused variable warning
  if (false)
    cout << argc; 
  if (false)
    cout << argv; 

  // Simplest way of creating a PY8MEs accessor
  PY8MEs_namespace::PY8MEs PY8MEs_accessor("param_card_heft.dat"); 

  //----------------------------------------------------------------------------
  // Here is an alternative way of creating a PY8MEs_accessor for which we
  // handle ourselves the instantiation, release and initialization of the
  // model. Notice that we need here the name of the model class because it
  // does not have a generic base class (I can add it if really necessary)
  // 
  // Parameters_heft* model = PY8MEs_namespace::PY8MEs::instantiateModel();
  // 
  // Or even directly with
  // 
  // Parameters_heft* model = new Parameters_heft();
  // 
  // With here an example of the initialization of the model using
  // Pythia8 objects.
  // 
  // model->setIndependentParameters(particleDataPtr,couplingsPtr,slhaPtr);
  // model->setIndependentCouplings();
  // model->printIndependentParameters();
  // model->printIndependentCouplings();
  // 
  // And then finally obtain the accessor with this particular model
  // 
  // PY8MEs_namespace::PY8MEs PY8MEs_accessor(model);
  //----------------------------------------------------------------------------

  //----------------------------------------------------------------------------
  // Finally one last way of creating a PY8MEs_accessor, which does the same
  // as above but this time doesn't require to define a local pointer to the
  // model (and hence to know its class name):
  // 
  // PY8MEs_namespace::PY8MEs PY8MEs_accessor();
  // 
  // We could now initialize the model from PY8 directly using the accessor
  // without having to manipulate a local pointer of the model
  // 
  // PY8MEs_namespace.initModelWithPY8(particleDataPtr, couplingsPtr, slhaPtr);
  // 
  // If needed, one can still access an instance of the model (for example
  // to be used for instantiating another PY8MEs_accessor) as follows. Be
  // wary however that as soon as you call this accessor, the PY8MEs_accessor
  // destructor will no longer take care of releasing the model instance and
  // it will be your responsability to do so.
  // 
  // Parameters_heft* model = PY8MEs_accessor.getModel();
  //----------------------------------------------------------------------------

  vector<int> vec_in_pdgs; 
  vector<int> vec_out_pdgs; 
  set<int> set_req_s_channels; 

  // Test the existence of a non-available process
  cout << endl <<  "Testing the non-existence of a non-available process:" <<
      endl;
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (33)(43)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (33)(2)(1)(5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> (2)(5)); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 

  // Testing available processes
  cout << endl <<  "Testing the evaluation of available processes:" << endl; 

  // Process Process: g g > h h g b b~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(21)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g b > h h g g b HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(21)(5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g b~ > h h g g b~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(21)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: b b~ > h h g g g HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (5)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(21)(21));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g b > h h b b b~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(5)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g b~ > h h b b~ b~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(5)(-5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: b b > h h g b b HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (5)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(5)(5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: b b~ > h h g b b~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (5)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: b~ b~ > h h g b~ b~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-5)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(-5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g g > h g g g HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(21)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(21)(21));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g g > h h g g g HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(21)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(21)(21));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g g > h g b b~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(21)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g b > h g g b HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(21)(5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g b~ > h g g b~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(21)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: b b~ > h g g g HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (5)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(21)(21));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g u > h h u b b~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(2)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g c > h h c b b~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(4)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g d > h h d b b~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(1)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g s > h h s b b~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(3)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g u~ > h h u~ b b~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(-2)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g c~ > h h c~ b b~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(-4)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g d~ > h h d~ b b~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(-1)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g s~ > h h s~ b b~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(-3)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g b > h h u u~ b HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(2)(-2)(5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g b > h h c c~ b HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(4)(-4)(5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g b > h h d d~ b HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(1)(-1)(5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g b > h h s s~ b HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(3)(-3)(5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g b~ > h h u u~ b~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(2)(-2)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g b~ > h h c c~ b~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(4)(-4)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g b~ > h h d d~ b~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(1)(-1)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g b~ > h h s s~ b~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(3)(-3)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u u~ > h h g b b~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c c~ > h h g b b~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d d~ > h h g b b~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s s~ > h h g b b~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u b > h h g u b HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(2)(5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c b > h h g c b HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(4)(5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d b > h h g d b HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(1)(5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s b > h h g s b HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(3)(5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u b~ > h h g u b~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(2)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c b~ > h h g c b~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(4)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d b~ > h h g d b~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(1)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s b~ > h h g s b~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(3)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u~ b > h h g u~ b HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-2)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(-2)(5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c~ b > h h g c~ b HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-4)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(-4)(5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d~ b > h h g d~ b HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-1)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(-1)(5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s~ b > h h g s~ b HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-3)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(-3)(5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u~ b~ > h h g u~ b~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-2)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(-2)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c~ b~ > h h g c~ b~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-4)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(-4)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d~ b~ > h h g d~ b~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-1)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(-1)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s~ b~ > h h g s~ b~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-3)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(-3)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: b b~ > h h g u u~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (5)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: b b~ > h h g c c~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (5)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: b b~ > h h g d d~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (5)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: b b~ > h h g s s~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (5)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g g > h h b b~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(21)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g b > h h g b HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g b~ > h h g b~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: b b~ > h h g g HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (5)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(21));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g g > h g u u~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(21)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g g > h g c c~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(21)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g g > h g d d~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(21)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g g > h g s s~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(21)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g u > h g g u HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(21)(2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g c > h g g c HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(21)(4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g d > h g g d HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(21)(1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g s > h g g s HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(21)(3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g u~ > h g g u~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(21)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g c~ > h g g c~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(21)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g d~ > h g g d~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(21)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g s~ > h g g s~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(21)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u u~ > h g g g HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(21)(21));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c c~ > h g g g HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(21)(21));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d d~ > h g g g HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(21)(21));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s s~ > h g g g HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(21)(21));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g g > h h g u u~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(21)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g g > h h g c c~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(21)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g g > h h g d d~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(21)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g g > h h g s s~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(21)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g u > h h g g u HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(21)(2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g c > h h g g c HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(21)(4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g d > h h g g d HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(21)(1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g s > h h g g s HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(21)(3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g u~ > h h g g u~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(21)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g c~ > h h g g c~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(21)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g d~ > h h g g d~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(21)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g s~ > h h g g s~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(21)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u u~ > h h g g g HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(21)(21));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c c~ > h h g g g HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(21)(21));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d d~ > h h g g g HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(21)(21));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s s~ > h h g g g HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(21)(21));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g b > h b b b~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(5)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g b~ > h b b~ b~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(5)(-5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: b b > h g b b HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (5)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(5)(5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: b b~ > h g b b~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (5)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: b~ b~ > h g b~ b~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-5)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(-5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: b b > h h b b HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (5)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(5)(5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: b b~ > h h b b~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (5)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: b~ b~ > h h b~ b~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-5)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(-5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g g > h g g HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(21)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(21));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g g > h h g g HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(21)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(21));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g u > h u u u~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(2)(2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g c > h c c c~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(4)(4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g d > h d d d~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(1)(1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g s > h s s s~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(3)(3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g u~ > h u u~ u~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(2)(-2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g c~ > h c c~ c~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(4)(-4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g d~ > h d d~ d~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(1)(-1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g s~ > h s s~ s~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(3)(-3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g u > h u c c~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(2)(4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g u > h u d d~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(2)(1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g u > h u s s~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(2)(3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g c > h c u u~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(4)(2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g c > h c d d~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(4)(1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g c > h c s s~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(4)(3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g d > h d u u~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(1)(2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g d > h d c c~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(1)(4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g d > h d s s~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(1)(3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g s > h s u u~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(3)(2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g s > h s c c~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(3)(4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g s > h s d d~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(3)(1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g u~ > h c u~ c~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(4)(-2)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g u~ > h d u~ d~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(1)(-2)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g u~ > h s u~ s~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(3)(-2)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g c~ > h u c~ u~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(2)(-4)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g c~ > h d c~ d~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(1)(-4)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g c~ > h s c~ s~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(3)(-4)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g d~ > h u d~ u~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(2)(-1)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g d~ > h c d~ c~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(4)(-1)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g d~ > h s d~ s~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(3)(-1)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g s~ > h u s~ u~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(2)(-3)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g s~ > h c s~ c~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(4)(-3)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g s~ > h d s~ d~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(1)(-3)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g u > h u b b~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(2)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g c > h c b b~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(4)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g d > h d b b~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(1)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g s > h s b b~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(3)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g u~ > h u~ b b~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(-2)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g c~ > h c~ b b~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(-4)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g d~ > h d~ b b~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(-1)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g s~ > h s~ b b~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(-3)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g b > h u u~ b HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(2)(-2)(5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g b > h c c~ b HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(4)(-4)(5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g b > h d d~ b HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(1)(-1)(5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g b > h s s~ b HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(3)(-3)(5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g b~ > h u u~ b~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(2)(-2)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g b~ > h c c~ b~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(4)(-4)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g b~ > h d d~ b~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(1)(-1)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g b~ > h s s~ b~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(3)(-3)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u u > h g u u HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(2)(2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c c > h g c c HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(4)(4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d d > h g d d HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(1)(1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s s > h g s s HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(3)(3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u u~ > h g u u~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c c~ > h g c c~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d d~ > h g d d~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s s~ > h g s s~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u~ u~ > h g u~ u~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-2)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(-2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c~ c~ > h g c~ c~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-4)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(-4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d~ d~ > h g d~ d~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-1)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(-1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s~ s~ > h g s~ s~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-3)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(-3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u c > h g u c HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(2)(4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u d > h g u d HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(2)(1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u s > h g u s HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(2)(3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c d > h g c d HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(4)(1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c s > h g c s HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(4)(3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d s > h g d s HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(1)(3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u u~ > h g c c~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u u~ > h g d d~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u u~ > h g s s~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c c~ > h g u u~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c c~ > h g d d~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c c~ > h g s s~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d d~ > h g u u~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d d~ > h g c c~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d d~ > h g s s~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s s~ > h g u u~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s s~ > h g c c~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s s~ > h g d d~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u c~ > h g u c~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(2)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u d~ > h g u d~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(2)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u s~ > h g u s~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(2)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c u~ > h g c u~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(4)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c d~ > h g c d~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(4)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c s~ > h g c s~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(4)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d u~ > h g d u~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(1)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d c~ > h g d c~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(1)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d s~ > h g d s~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(1)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s u~ > h g s u~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(3)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s c~ > h g s c~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(3)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s d~ > h g s d~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(3)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u~ c~ > h g u~ c~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-2)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(-2)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u~ d~ > h g u~ d~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-2)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(-2)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u~ s~ > h g u~ s~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-2)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(-2)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c~ d~ > h g c~ d~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-4)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(-4)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c~ s~ > h g c~ s~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-4)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(-4)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d~ s~ > h g d~ s~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-1)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(-1)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u u~ > h g b b~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c c~ > h g b b~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d d~ > h g b b~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s s~ > h g b b~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u b > h g u b HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(2)(5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c b > h g c b HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(4)(5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d b > h g d b HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(1)(5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s b > h g s b HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(3)(5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u b~ > h g u b~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(2)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c b~ > h g c b~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(4)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d b~ > h g d b~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(1)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s b~ > h g s b~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(3)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u~ b > h g u~ b HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-2)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(-2)(5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c~ b > h g c~ b HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-4)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(-4)(5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d~ b > h g d~ b HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-1)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(-1)(5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s~ b > h g s~ b HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-3)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(-3)(5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u~ b~ > h g u~ b~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-2)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(-2)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c~ b~ > h g c~ b~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-4)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(-4)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d~ b~ > h g d~ b~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-1)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(-1)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s~ b~ > h g s~ b~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-3)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(-3)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: b b~ > h g u u~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (5)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: b b~ > h g c c~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (5)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: b b~ > h g d d~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (5)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: b b~ > h g s s~ HIG<=1 HIW<=1 WEIGHTED<=5 @4
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (5)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g u > h h u u u~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(2)(2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g c > h h c c c~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(4)(4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g d > h h d d d~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(1)(1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g s > h h s s s~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(3)(3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g u~ > h h u u~ u~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(2)(-2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g c~ > h h c c~ c~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(4)(-4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g d~ > h h d d~ d~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(1)(-1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g s~ > h h s s~ s~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(3)(-3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g u > h h u c c~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(2)(4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g u > h h u d d~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(2)(1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g u > h h u s s~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(2)(3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g c > h h c u u~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(4)(2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g c > h h c d d~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(4)(1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g c > h h c s s~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(4)(3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g d > h h d u u~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(1)(2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g d > h h d c c~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(1)(4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g d > h h d s s~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(1)(3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g s > h h s u u~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(3)(2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g s > h h s c c~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(3)(4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g s > h h s d d~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(3)(1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g u~ > h h c u~ c~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(4)(-2)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g u~ > h h d u~ d~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(1)(-2)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g u~ > h h s u~ s~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(3)(-2)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g c~ > h h u c~ u~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(2)(-4)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g c~ > h h d c~ d~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(1)(-4)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g c~ > h h s c~ s~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(3)(-4)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g d~ > h h u d~ u~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(2)(-1)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g d~ > h h c d~ c~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(4)(-1)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g d~ > h h s d~ s~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(3)(-1)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g s~ > h h u s~ u~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(2)(-3)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g s~ > h h c s~ c~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(4)(-3)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g s~ > h h d s~ d~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(1)(-3)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u u > h h g u u HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(2)(2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c c > h h g c c HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(4)(4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d d > h h g d d HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(1)(1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s s > h h g s s HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(3)(3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u u~ > h h g u u~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c c~ > h h g c c~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d d~ > h h g d d~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s s~ > h h g s s~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u~ u~ > h h g u~ u~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-2)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(-2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c~ c~ > h h g c~ c~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-4)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(-4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d~ d~ > h h g d~ d~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-1)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(-1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s~ s~ > h h g s~ s~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-3)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(-3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u c > h h g u c HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(2)(4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u d > h h g u d HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(2)(1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u s > h h g u s HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(2)(3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c d > h h g c d HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(4)(1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c s > h h g c s HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(4)(3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d s > h h g d s HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(1)(3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u u~ > h h g c c~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u u~ > h h g d d~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u u~ > h h g s s~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c c~ > h h g u u~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c c~ > h h g d d~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c c~ > h h g s s~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d d~ > h h g u u~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d d~ > h h g c c~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d d~ > h h g s s~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s s~ > h h g u u~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s s~ > h h g c c~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s s~ > h h g d d~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u c~ > h h g u c~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(2)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u d~ > h h g u d~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(2)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u s~ > h h g u s~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(2)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c u~ > h h g c u~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(4)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c d~ > h h g c d~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(4)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c s~ > h h g c s~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(4)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d u~ > h h g d u~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(1)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d c~ > h h g d c~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(1)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d s~ > h h g d s~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(1)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s u~ > h h g s u~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(3)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s c~ > h h g s c~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(3)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s d~ > h h g s d~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(3)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u~ c~ > h h g u~ c~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-2)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(-2)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u~ d~ > h h g u~ d~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-2)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(-2)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u~ s~ > h h g u~ s~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-2)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(-2)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c~ d~ > h h g c~ d~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-4)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(-4)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c~ s~ > h h g c~ s~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-4)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(-4)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d~ s~ > h h g d~ s~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-1)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(-1)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g g > h b b~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(21)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(5)(-5)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g b > h g b HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(21)(5)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g b~ > h g b~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: b b~ > h g g HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (5)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(21));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g b > h h b HIG<=1 HIW<=1 WEIGHTED<=5 @6
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(25)(5)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g b~ > h h b~ HIG<=1 HIW<=1 WEIGHTED<=5 @6
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: b b~ > h h g HIG<=1 HIW<=1 WEIGHTED<=5 @6
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (5)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u u~ > h h b b~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c c~ > h h b b~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d d~ > h h b b~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s s~ > h h b b~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u b > h h u b HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(2)(5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c b > h h c b HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(4)(5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d b > h h d b HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(1)(5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s b > h h s b HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(3)(5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u b~ > h h u b~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(2)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c b~ > h h c b~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(4)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d b~ > h h d b~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(1)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s b~ > h h s b~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(3)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u~ b > h h u~ b HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-2)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(-2)(5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c~ b > h h c~ b HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-4)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(-4)(5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d~ b > h h d~ b HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-1)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(-1)(5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s~ b > h h s~ b HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-3)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(-3)(5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u~ b~ > h h u~ b~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-2)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(-2)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c~ b~ > h h c~ b~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-4)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(-4)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d~ b~ > h h d~ b~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-1)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(-1)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s~ b~ > h h s~ b~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-3)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(-3)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: b b~ > h h u u~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (5)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: b b~ > h h c c~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (5)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: b b~ > h h d d~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (5)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: b b~ > h h s s~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (5)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: b b > h b b HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (5)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(5)(5)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: b b~ > h b b~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (5)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(5)(-5)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: b~ b~ > h b~ b~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-5)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(-5)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g g > h u u~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(21)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(2)(-2)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g g > h c c~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(21)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(4)(-4)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g g > h d d~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(21)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(1)(-1)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g g > h s s~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(21)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(3)(-3)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g u > h g u HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(21)(2)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g c > h g c HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(21)(4)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g d > h g d HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(21)(1)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g s > h g s HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(21)(3)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g u~ > h g u~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g c~ > h g c~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g d~ > h g d~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g s~ > h g s~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u u~ > h g g HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(21));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c c~ > h g g HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(21));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d d~ > h g g HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(21));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s s~ > h g g HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(21)(21));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g g > h h u u~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(21)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g g > h h c c~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(21)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g g > h h d d~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(21)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g g > h h s s~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(21)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g u > h h g u HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g c > h h g c HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g d > h h g d HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g s > h h g s HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g u~ > h h g u~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g c~ > h h g c~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g d~ > h h g d~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g s~ > h h g s~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u u~ > h h g g HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(21));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c c~ > h h g g HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(21));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d d~ > h h g g HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(21));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s s~ > h h g g HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21)(21));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g g > h g HIG<=1 HIW<=1 WEIGHTED<=3 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(21)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(21)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g g > h h g HIG<=1 HIW<=1 WEIGHTED<=5 @6
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(21)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g b > h b HIG<=1 HIW<=1 WEIGHTED<=3 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(5)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g b~ > h b~ HIG<=1 HIW<=1 WEIGHTED<=3 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(-5)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: b b~ > h g HIG<=1 HIW<=1 WEIGHTED<=3 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (5)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(21)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u u~ > h b b~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(5)(-5)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c c~ > h b b~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(5)(-5)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d d~ > h b b~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(5)(-5)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s s~ > h b b~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(5)(-5)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u b > h u b HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(2)(5)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c b > h c b HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(4)(5)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d b > h d b HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(1)(5)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s b > h s b HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(3)(5)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u b~ > h u b~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(2)(-5)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c b~ > h c b~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(4)(-5)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d b~ > h d b~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(1)(-5)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s b~ > h s b~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(3)(-5)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u~ b > h u~ b HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-2)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(-2)(5)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c~ b > h c~ b HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-4)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(-4)(5)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d~ b > h d~ b HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-1)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(-1)(5)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s~ b > h s~ b HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-3)(5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(-3)(5)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u~ b~ > h u~ b~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-2)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(-2)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c~ b~ > h c~ b~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-4)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(-4)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d~ b~ > h d~ b~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-1)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(-1)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s~ b~ > h s~ b~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-3)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(-3)(-5));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: b b~ > h u u~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (5)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(2)(-2)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: b b~ > h c c~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (5)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(4)(-4)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: b b~ > h d d~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (5)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(1)(-1)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: b b~ > h s s~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (5)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(3)(-3)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: b b~ > h h HIG<=1 HIW<=1 WEIGHTED<=4 @5
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (5)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(25)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u u > h u u HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(2)(2)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c c > h c c HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(4)(4)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d d > h d d HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(1)(1)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s s > h s s HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(3)(3)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u u~ > h u u~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(2)(-2)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c c~ > h c c~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(4)(-4)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d d~ > h d d~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(1)(-1)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s s~ > h s s~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(3)(-3)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u~ u~ > h u~ u~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-2)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(-2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c~ c~ > h c~ c~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-4)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(-4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d~ d~ > h d~ d~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-1)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(-1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s~ s~ > h s~ s~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-3)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(-3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u c > h u c HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(2)(4)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u d > h u d HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(2)(1)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u s > h u s HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(2)(3)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c d > h c d HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(4)(1)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c s > h c s HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(4)(3)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d s > h d s HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(1)(3)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u u~ > h c c~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(4)(-4)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u u~ > h d d~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(1)(-1)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u u~ > h s s~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(3)(-3)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c c~ > h u u~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(2)(-2)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c c~ > h d d~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(1)(-1)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c c~ > h s s~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(3)(-3)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d d~ > h u u~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(2)(-2)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d d~ > h c c~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(4)(-4)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d d~ > h s s~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(3)(-3)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s s~ > h u u~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(2)(-2)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s s~ > h c c~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(4)(-4)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s s~ > h d d~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(1)(-1)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u c~ > h u c~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(2)(-4)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u d~ > h u d~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(2)(-1)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u s~ > h u s~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(2)(-3)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c u~ > h c u~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(4)(-2)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c d~ > h c d~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(4)(-1)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c s~ > h c s~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(4)(-3)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d u~ > h d u~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(1)(-2)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d c~ > h d c~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(1)(-4)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d s~ > h d s~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(1)(-3)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s u~ > h s u~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(3)(-2)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s c~ > h s c~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(3)(-4)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s d~ > h s d~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(3)(-1)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u~ c~ > h u~ c~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-2)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(-2)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u~ d~ > h u~ d~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-2)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(-2)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u~ s~ > h u~ s~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-2)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(-2)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c~ d~ > h c~ d~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-4)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(-4)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c~ s~ > h c~ s~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-4)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(-4)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d~ s~ > h d~ s~ HIG<=1 HIW<=1 WEIGHTED<=4 @3
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-1)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(-1)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u u > h h u u HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(2)(2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c c > h h c c HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(4)(4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d d > h h d d HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(1)(1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s s > h h s s HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(3)(3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u u~ > h h u u~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c c~ > h h c c~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d d~ > h h d d~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s s~ > h h s s~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u~ u~ > h h u~ u~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-2)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(-2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c~ c~ > h h c~ c~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-4)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(-4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d~ d~ > h h d~ d~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-1)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(-1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s~ s~ > h h s~ s~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-3)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(-3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u c > h h u c HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(2)(4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u d > h h u d HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(2)(1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u s > h h u s HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(2)(3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c d > h h c d HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(4)(1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c s > h h c s HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(4)(3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d s > h h d s HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(1)(3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u u~ > h h c c~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u u~ > h h d d~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u u~ > h h s s~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c c~ > h h u u~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c c~ > h h d d~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c c~ > h h s s~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d d~ > h h u u~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d d~ > h h c c~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d d~ > h h s s~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(3)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s s~ > h h u u~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(2)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s s~ > h h c c~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(4)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s s~ > h h d d~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(1)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u c~ > h h u c~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(2)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u d~ > h h u d~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(2)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u s~ > h h u s~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(2)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c u~ > h h c u~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(4)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c d~ > h h c d~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(4)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c s~ > h h c s~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(4)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d u~ > h h d u~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(1)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d c~ > h h d c~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(1)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d s~ > h h d s~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(1)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s u~ > h h s u~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(3)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s c~ > h h s c~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(3)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s d~ > h h s d~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(3)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u~ c~ > h h u~ c~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-2)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(-2)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u~ d~ > h h u~ d~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-2)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(-2)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u~ s~ > h h u~ s~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-2)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(-2)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c~ d~ > h h c~ d~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-4)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(-4)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c~ s~ > h h c~ s~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-4)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(-4)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d~ s~ > h h d~ s~ HIG<=1 HIW<=1 WEIGHTED<=6 @7
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (-1)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(-1)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g g > h HIG<=1 HIW<=1 WEIGHTED<=2 @1
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(21)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: b b~ > h HIG<=1 HIW<=1 WEIGHTED<=2 @1
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (5)(-5)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g u > h u HIG<=1 HIW<=1 WEIGHTED<=3 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(2)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g c > h c HIG<=1 HIW<=1 WEIGHTED<=3 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(4)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g d > h d HIG<=1 HIW<=1 WEIGHTED<=3 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(1)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g s > h s HIG<=1 HIW<=1 WEIGHTED<=3 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(3)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g u~ > h u~ HIG<=1 HIW<=1 WEIGHTED<=3 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(-2)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g c~ > h c~ HIG<=1 HIW<=1 WEIGHTED<=3 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(-4)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g d~ > h d~ HIG<=1 HIW<=1 WEIGHTED<=3 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(-1)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g s~ > h s~ HIG<=1 HIW<=1 WEIGHTED<=3 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(-3)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u u~ > h g HIG<=1 HIW<=1 WEIGHTED<=3 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(21)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c c~ > h g HIG<=1 HIW<=1 WEIGHTED<=3 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(21)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d d~ > h g HIG<=1 HIW<=1 WEIGHTED<=3 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(21)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s s~ > h g HIG<=1 HIW<=1 WEIGHTED<=3 @2
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(21)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g g > h h HIG<=1 HIW<=1 WEIGHTED<=4 @5
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(21)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(25)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g u > h h u HIG<=1 HIW<=1 WEIGHTED<=5 @6
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(25)(2)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g c > h h c HIG<=1 HIW<=1 WEIGHTED<=5 @6
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(25)(4)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g d > h h d HIG<=1 HIW<=1 WEIGHTED<=5 @6
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(25)(1)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g s > h h s HIG<=1 HIW<=1 WEIGHTED<=5 @6
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (25)(25)(3)); 
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g u~ > h h u~ HIG<=1 HIW<=1 WEIGHTED<=5 @6
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(-2));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g c~ > h h c~ HIG<=1 HIW<=1 WEIGHTED<=5 @6
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(-4));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g d~ > h h d~ HIG<=1 HIW<=1 WEIGHTED<=5 @6
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(-1));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: g s~ > h h s~ HIG<=1 HIW<=1 WEIGHTED<=5 @6
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (21)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(-3));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: u u~ > h h g HIG<=1 HIW<=1 WEIGHTED<=5 @6
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (2)(-2)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: c c~ > h h g HIG<=1 HIW<=1 WEIGHTED<=5 @6
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (4)(-4)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: d d~ > h h g HIG<=1 HIW<=1 WEIGHTED<=5 @6
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (1)(-1)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 
  // Process Process: s s~ > h h g HIG<=1 HIW<=1 WEIGHTED<=5 @6
  vec_in_pdgs = vector<int> (PY8MEs_namespace::createvector<int> (3)(-3)); 
  vec_out_pdgs = vector<int> (PY8MEs_namespace::createvector<int>
      (25)(25)(21));
  set_req_s_channels = set<int> (PY8MEs_namespace::createset<int> ()); 
  run_proc(PY8MEs_accessor, vec_in_pdgs, vec_out_pdgs, set_req_s_channels); 


  #include "check_sa_additional_runs.inc"

}
