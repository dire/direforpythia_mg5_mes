//==========================================================================
// This file has been automatically generated for Pythia 8 by
// MadGraph5_aMC@NLO v. 2.6.0, 2017-08-16
// By the MadGraph5_aMC@NLO Development Team
// Visit launchpad.net/madgraph5 and amcatnlo.web.cern.ch
//==========================================================================

#include "PY8MEs_R8_P4_heft_bb_hhgbb.h"
#include "HelAmps_heft.h"

using namespace Pythia8_heft; 

namespace PY8MEs_namespace 
{
//==========================================================================
// Class member functions for calculating the matrix elements for
// Process: b b > h h g b b HIG<=1 HIW<=1 WEIGHTED<=7 @8
// Process: b b~ > h h g b b~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
// Process: b~ b~ > h h g b~ b~ HIG<=1 HIW<=1 WEIGHTED<=7 @8

// Exception class
class PY8MEs_R8_P4_heft_bb_hhgbbException : public exception
{
  virtual const char * what() const throw()
  {
    return "Exception in class 'PY8MEs_R8_P4_heft_bb_hhgbb'."; 
  }
}
PY8MEs_R8_P4_heft_bb_hhgbb_exception; 

std::set<int> PY8MEs_R8_P4_heft_bb_hhgbb::s_channel_proc = std::set<int>
    (createset<int> ());

int PY8MEs_R8_P4_heft_bb_hhgbb::helicities[ncomb][nexternal] = {{-1, -1, 0, 0,
    -1, -1, -1}, {-1, -1, 0, 0, -1, -1, 1}, {-1, -1, 0, 0, -1, 1, -1}, {-1, -1,
    0, 0, -1, 1, 1}, {-1, -1, 0, 0, 1, -1, -1}, {-1, -1, 0, 0, 1, -1, 1}, {-1,
    -1, 0, 0, 1, 1, -1}, {-1, -1, 0, 0, 1, 1, 1}, {-1, 1, 0, 0, -1, -1, -1},
    {-1, 1, 0, 0, -1, -1, 1}, {-1, 1, 0, 0, -1, 1, -1}, {-1, 1, 0, 0, -1, 1,
    1}, {-1, 1, 0, 0, 1, -1, -1}, {-1, 1, 0, 0, 1, -1, 1}, {-1, 1, 0, 0, 1, 1,
    -1}, {-1, 1, 0, 0, 1, 1, 1}, {1, -1, 0, 0, -1, -1, -1}, {1, -1, 0, 0, -1,
    -1, 1}, {1, -1, 0, 0, -1, 1, -1}, {1, -1, 0, 0, -1, 1, 1}, {1, -1, 0, 0, 1,
    -1, -1}, {1, -1, 0, 0, 1, -1, 1}, {1, -1, 0, 0, 1, 1, -1}, {1, -1, 0, 0, 1,
    1, 1}, {1, 1, 0, 0, -1, -1, -1}, {1, 1, 0, 0, -1, -1, 1}, {1, 1, 0, 0, -1,
    1, -1}, {1, 1, 0, 0, -1, 1, 1}, {1, 1, 0, 0, 1, -1, -1}, {1, 1, 0, 0, 1,
    -1, 1}, {1, 1, 0, 0, 1, 1, -1}, {1, 1, 0, 0, 1, 1, 1}};

// Normalization factors the various processes
// Denominators: spins, colors and identical particles
int PY8MEs_R8_P4_heft_bb_hhgbb::denom_colors[nprocesses] = {9, 9, 9}; 
int PY8MEs_R8_P4_heft_bb_hhgbb::denom_hels[nprocesses] = {4, 4, 4}; 
int PY8MEs_R8_P4_heft_bb_hhgbb::denom_iden[nprocesses] = {4, 2, 4}; 

//--------------------------------------------------------------------------
// Color config initialization
void PY8MEs_R8_P4_heft_bb_hhgbb::initColorConfigs() 
{
  color_configs = vector < vec_vec_int > (); 
  jamp_nc_relative_power = vector < vec_int > (); 

  // Color flows of process Process: b b > h h g b b HIG<=1 HIW<=1 WEIGHTED<=7
  // @8
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(0)(2)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #1
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(0)(3)(0)(0)(0)(0)(0)(3)(1)(1)(0)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #2
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(0)(1)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #3
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(0)(3)(0)(0)(0)(0)(0)(3)(2)(1)(0)(2)(0)));
  jamp_nc_relative_power[0].push_back(0); 

  // Color flows of process Process: b b~ > h h g b b~ HIG<=1 HIW<=1
  // WEIGHTED<=7 @8
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(3)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #1
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(2)(2)(0)(0)(1)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #2
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(2)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #3
  color_configs[1].push_back(vec_int(createvector<int>
      (2)(0)(0)(1)(0)(0)(0)(0)(3)(1)(2)(0)(0)(3)));
  jamp_nc_relative_power[1].push_back(0); 

  // Color flows of process Process: b~ b~ > h h g b~ b~ HIG<=1 HIW<=1
  // WEIGHTED<=7 @8
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(1)(0)(3)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #1
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(2)(0)(3)(0)(1)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #2
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(2)(0)(3)));
  jamp_nc_relative_power[2].push_back(0); 
  // JAMP #3
  color_configs[2].push_back(vec_int(createvector<int>
      (0)(1)(0)(2)(0)(0)(0)(0)(3)(1)(0)(3)(0)(2)));
  jamp_nc_relative_power[2].push_back(0); 
}

//--------------------------------------------------------------------------
// Destructor.
PY8MEs_R8_P4_heft_bb_hhgbb::~PY8MEs_R8_P4_heft_bb_hhgbb() 
{
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    delete[] p[i]; 
    p[i] = NULL; 
  }
}

//--------------------------------------------------------------------------
// Invert the permutation mapping
vector<int> PY8MEs_R8_P4_heft_bb_hhgbb::invert_mapping(vector<int> mapping) 
{
  vector<int> inverted_mapping; 
  for (unsigned int i = 0; i < mapping.size(); i++ )
  {
    for (unsigned int j = 0; j < mapping.size(); j++ )
    {
      if (mapping[j] == ((int)i))
      {
        inverted_mapping.push_back(j); 
        break; 
      }
    }
  }
  return inverted_mapping; 
}

//--------------------------------------------------------------------------
// Return the list of possible helicity configurations
vector < vec_int > PY8MEs_R8_P4_heft_bb_hhgbb::getHelicityConfigs(vector<int>
    permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(ncomb, vector<int> (nexternal, 0)); 
  for (unsigned int ihel = 0; ihel < ncomb; ihel++ )
  {
    for(unsigned int j = 0; j < nexternal; j++ )
    {
      res[ihel][chosenPerm[j]] = helicities[ihel][j]; 
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Return the list of possible color configurations
vector < vec_int > PY8MEs_R8_P4_heft_bb_hhgbb::getColorConfigs(int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(color_configs[chosenProcID].size(), vector<int>
      (nexternal * 2, 0));
  for (unsigned int icol = 0; icol < color_configs[chosenProcID].size(); icol++
      )
  {
    for(unsigned int j = 0; j < (2 * nexternal); j++ )
    {
      res[icol][chosenPerm[j/2] * 2 + j%2] =
          color_configs[chosenProcID][icol][j];
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Get JAMP relative N_c power
int PY8MEs_R8_P4_heft_bb_hhgbb::getColorFlowRelativeNCPower(int color_flow_ID,
    int specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return jamp_nc_relative_power[chosenProcID][color_flow_ID]; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity ID -> Helicity Config
vector<int> PY8MEs_R8_P4_heft_bb_hhgbb::getHelicityConfigForID(int hel_ID,
    vector<int> permutation)
{
  if (hel_ID < 0 || hel_ID >= ncomb)
  {
    cerr <<  "Error in function 'getHelicityConfigForID' of class" << 
    " 'PY8MEs_R8_P4_heft_bb_hhgbb': Specified helicity ID '" << 
    hel_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R8_P4_heft_bb_hhgbb_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(nexternal, 0); 
  for (unsigned int j = 0; j < nexternal; j++ )
  {
    res[chosenPerm[j]] = helicities[hel_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity Config -> Helicity ID
int PY8MEs_R8_P4_heft_bb_hhgbb::getHelicityIDForConfig(vector<int> hel_config,
    vector<int> permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  int user_ihel = -1; 
  if (hel_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < ncomb; i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < nexternal; j++ )
      {
        if (helicities[i][chosenPerm[j]] != hel_config[j])
        {
          found = false; 
          break; 
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_ihel = i; 
        break; 
      }
    }
    if (user_ihel == -1)
    {
      cerr <<  "Error in function 'getHelicityIDForConfig' of class" << 
      " 'PY8MEs_R8_P4_heft_bb_hhgbb': Specified helicity" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R8_P4_heft_bb_hhgbb_exception; 
    }
  }
  return user_ihel; 
}


//--------------------------------------------------------------------------
// Implements the map Color ID -> Color Config
vector<int> PY8MEs_R8_P4_heft_bb_hhgbb::getColorConfigForID(int color_ID, int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < 0 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getColorConfigForID' of class" << 
    " 'PY8MEs_R8_P4_heft_bb_hhgbb': Specified color ID '" << 
    color_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R8_P4_heft_bb_hhgbb_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(color_configs[chosenProcID][color_ID].size(), 0); 
  for (unsigned int j = 0; j < (2 * nexternal); j++ )
  {
    res[chosenPerm[j/2] * 2 + j%2] = color_configs[chosenProcID][color_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Color Config -> Color ID
int PY8MEs_R8_P4_heft_bb_hhgbb::getColorIDForConfig(vector<int> color_config,
    int specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = -1; 
  if (color_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < color_configs[chosenProcID].size(); i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < (nexternal * 2); j++ )
      {

        // If colorless then make sure it matches
        // The little arithmetics in the color index is just
        // the permutation applies on the particle list which is
        // twice smaller since each particle can have two color indices.
        if (color_config[j] == 0)
        {
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] != 0)
          {
            found = false; 
            break; 
          }
          // Otherwise check that the color linked position matches
        }
        else
        {
          int color_linked_pos = -1; 
          // Find the other end of the line in the user color config
          for (unsigned int k = 0; k < (nexternal * 2); k++ )
          {
            if (k == j)
              continue; 
            if (color_config[j] == color_config[k])
            {
              color_linked_pos = k; 
              break; 
            }
          }
          if (color_linked_pos == -1)
          {
            cerr <<  "Error in function 'getColorIDForConfig' of class" << 
            " 'PY8MEs_R8_P4_heft_bb_hhgbb': A color line could " << 
            " not be closed." << endl; 
            throw PY8MEs_R8_P4_heft_bb_hhgbb_exception; 
          }
          // Now check whether the color line matches
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] !=
              color_configs[chosenProcID][i][chosenPerm[color_linked_pos/2] * 2
              + color_linked_pos%2])
          {
            found = false; 
            break; 
          }
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_icol = i; 
        break; 
      }
    }

    if (user_icol == -1)
    {
      cerr <<  "Error in function 'getColorIDForConfig' of class" << 
      " 'PY8MEs_R8_P4_heft_bb_hhgbb': Specified color" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R8_P4_heft_bb_hhgbb_exception; 
    }
  }
  return user_icol; 
}

//--------------------------------------------------------------------------
// Returns all result previously computed in SigmaKin
vector < vec_double > PY8MEs_R8_P4_heft_bb_hhgbb::getAllResults(int
    specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return all_results[chosenProcID]; 
}

//--------------------------------------------------------------------------
// Returns a result previously computed in SigmaKin for a specific helicity
// and color ID. -1 means avg and summed over that characteristic.
double PY8MEs_R8_P4_heft_bb_hhgbb::getResult(int helicity_ID, int color_ID, int
    specify_proc_ID)
{
  if (helicity_ID < - 1 || helicity_ID >= ncomb)
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R8_P4_heft_bb_hhgbb': Specified helicity ID '" << 
    helicity_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R8_P4_heft_bb_hhgbb_exception; 
  }
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < - 1 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R8_P4_heft_bb_hhgbb': Specified color ID '" << 
    color_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R8_P4_heft_bb_hhgbb_exception; 
  }
  return all_results[chosenProcID][helicity_ID + 1][color_ID + 1]; 
}

//--------------------------------------------------------------------------
// Check for the availability of the requested process and if available,
// If available, this returns the corresponding permutation and Proc_ID to use.
// If not available, this returns a negative Proc_ID.
pair < vector<int> , int > PY8MEs_R8_P4_heft_bb_hhgbb::static_getPY8ME(vector<int> initial_pdgs, vector<int> final_pdgs, set<int> schannels) 
{

  // Not available return value
  pair < vector<int> , int > NA(vector<int> (), -1); 

  // Check if s-channel requirements match
  if (nreq_s_channels > 0)
  {
    if (schannels != s_channel_proc)
      return NA; 
  }
  else
  {
    if (schannels.size() != 0)
      return NA; 
  }

  // Check number of final state particles
  if (final_pdgs.size() != (nexternal - ninitial))
    return NA; 

  // Check number of initial state particles
  if (initial_pdgs.size() != ninitial)
    return NA; 

  // List of processes available in this class
  const int nprocs = 4; 
  const int proc_IDS[nprocs] = {0, 1, 2, 1}; 
  const int in_pdgs[nprocs][ninitial] = {{5, 5}, {5, -5}, {-5, -5}, {-5, 5}}; 
  const int out_pdgs[nprocs][nexternal - ninitial] = {{25, 25, 21, 5, 5}, {25,
      25, 21, 5, -5}, {25, 25, 21, -5, -5}, {25, 25, 21, 5, -5}};

  bool in_pdgs_used[ninitial]; 
  bool out_pdgs_used[nexternal - ninitial]; 
  for(unsigned int i = 0; i < nprocs; i++ )
  {
    int permutations[nexternal]; 

    // Reinitialize initial state look-up variables
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      in_pdgs_used[j] = false; 
      permutations[j] = -1; 
    }
    // Look for initial state matches
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      for(unsigned int k = 0; k < ninitial; k++ )
      {
        // Make sure it has not been used already
        if (in_pdgs_used[k])
          continue; 
        if (initial_pdgs[k] == in_pdgs[i][j])
        {
          permutations[j] = k; 
          in_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[ninitial - 1] == -1)
      continue; 

    // Reinitialize final state look-up variables
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      out_pdgs_used[j] = false; 
      permutations[ninitial + j] = -1; 
    }
    // Look for final state matches
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      for(unsigned int k = 0; k < (nexternal - ninitial); k++ )
      {
        // Make sure it has not been used already
        if (out_pdgs_used[k])
          continue; 
        if (final_pdgs[k] == out_pdgs[i][j])
        {
          permutations[ninitial + j] = ninitial + k; 
          out_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[ninitial + j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[nexternal - 1] == -1)
      continue; 

    // Return process found
    return pair < vector<int> , int > (vector<int> (permutations, permutations
        + nexternal), proc_IDS[i]);
  }

  // No process found
  return NA; 
}

//--------------------------------------------------------------------------
// Set momenta
void PY8MEs_R8_P4_heft_bb_hhgbb::setMomenta(vector < vec_double >
    momenta_picked)
{
  if (momenta_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setMomenta' of class" << 
    " 'PY8MEs_R8_P4_heft_bb_hhgbb': Incorrect number of" << 
    " momenta specified." << endl; 
    throw PY8MEs_R8_P4_heft_bb_hhgbb_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    if (momenta_picked[i].size() != 4)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R8_P4_heft_bb_hhgbb': Incorrect number of" << 
      " momenta components specified." << endl; 
      throw PY8MEs_R8_P4_heft_bb_hhgbb_exception; 
    }
    if (momenta_picked[i][0] < 0.0)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R8_P4_heft_bb_hhgbb': A momentum was specified" << 
      " with negative energy. Check conventions." << endl; 
      throw PY8MEs_R8_P4_heft_bb_hhgbb_exception; 
    }
    for (unsigned int j = 0; j < 4; j++ )
    {
      p[i][j] = momenta_picked[i][j]; 
    }
  }
}

//--------------------------------------------------------------------------
// Set color configuration to use. An empty vector means sum over all.
void PY8MEs_R8_P4_heft_bb_hhgbb::setColors(vector<int> colors_picked)
{
  if (colors_picked.size() == 0)
  {
    user_colors = vector<int> (); 
    return; 
  }
  if (colors_picked.size() != (2 * nexternal))
  {
    cerr <<  "Error in function 'setColors' of class" << 
    " 'PY8MEs_R8_P4_heft_bb_hhgbb': Incorrect number" << 
    " of colors specified." << endl; 
    throw PY8MEs_R8_P4_heft_bb_hhgbb_exception; 
  }
  user_colors = vector<int> ((2 * nexternal), 0); 
  for(unsigned int i = 0; i < (2 * nexternal); i++ )
  {
    user_colors[i] = colors_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the helicity configuration to use. Am empty vector means sum over all.
void PY8MEs_R8_P4_heft_bb_hhgbb::setHelicities(vector<int> helicities_picked) 
{
  if (helicities_picked.size() != nexternal)
  {
    if (helicities_picked.size() == 0)
    {
      user_helicities = vector<int> (); 
      return; 
    }
    cerr <<  "Error in function 'setHelicities' of class" << 
    " 'PY8MEs_R8_P4_heft_bb_hhgbb': Incorrect number" << 
    " of helicities specified." << endl; 
    throw PY8MEs_R8_P4_heft_bb_hhgbb_exception; 
  }
  user_helicities = vector<int> (nexternal, 0); 
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    user_helicities[i] = helicities_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the permutation to use (will apply to momenta, colors and helicities)
void PY8MEs_R8_P4_heft_bb_hhgbb::setPermutation(vector<int> perm_picked) 
{
  if (perm_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setPermutations' of class" << 
    " 'PY8MEs_R8_P4_heft_bb_hhgbb': Incorrect number" << 
    " of permutations specified." << endl; 
    throw PY8MEs_R8_P4_heft_bb_hhgbb_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    perm[i] = perm_picked[i]; 
  }
}

// Set the proc_ID to use
void PY8MEs_R8_P4_heft_bb_hhgbb::setProcID(int procID_picked) 
{
  proc_ID = procID_picked; 
}

//--------------------------------------------------------------------------
// Initialize process.

void PY8MEs_R8_P4_heft_bb_hhgbb::initProc() 
{

  // Initialize flags
  include_symmetry_factors = true; 
  include_helicity_averaging_factors = true; 
  include_color_averaging_factors = true; 

  // Initialize vectors.
  perm = vector<int> (nexternal, 0); 
  user_colors = vector<int> (2 * nexternal, 0); 
  user_helicities = vector<int> (nexternal, 0); 
  p = vector < double * > (); 
  for (unsigned int i = 0; i < nexternal; i++ )
  {
    p.push_back(new double[4]); 
  }
  initColorConfigs(); 
  // Synchronize local variables dependent on the model with the active model.
  mME = vector<double> (nexternal, 0.); 
  syncProcModelParams(); 
  jamp2 = vector < vec_double > (3); 
  jamp2[0] = vector<double> (4, 0.); 
  jamp2[1] = vector<double> (4, 0.); 
  jamp2[2] = vector<double> (4, 0.); 
  all_results = vector < vec_vec_double > (3); 
  // The first entry is always the color or helicity avg/summed matrix element.
  all_results[0] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[1] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
  all_results[2] = vector < vec_double > (ncomb + 1, vector<double> (4 + 1,
      0.));
}

// Synchronize local variables of the process that depend on the model
// parameters
void PY8MEs_R8_P4_heft_bb_hhgbb::syncProcModelParams() 
{

  // Instantiate the model class and set parameters that stay fixed during run
  mME[0] = pars->mdl_MB; 
  mME[1] = pars->mdl_MB; 
  mME[2] = pars->mdl_MH; 
  mME[3] = pars->mdl_MH; 
  mME[4] = pars->ZERO; 
  mME[5] = pars->mdl_MB; 
  mME[6] = pars->mdl_MB; 
}

//--------------------------------------------------------------------------
// Setter allowing to force particular values for the external masses
void PY8MEs_R8_P4_heft_bb_hhgbb::setMasses(vec_double external_masses) 
{

  if (external_masses.size() != mME.size())
  {
    cerr <<  "Error in function 'setMasses' of class" << 
    " 'PY8MEs_R8_P4_heft_bb_hhgbb': Incorrect number of" << 
    " masses specified." << endl; 
    throw PY8MEs_R8_P4_heft_bb_hhgbb_exception; 
  }
  for (unsigned int j = 0; j < mME.size(); j++ )
  {
    mME[j] = external_masses[perm[j]]; 
  }
}

//--------------------------------------------------------------------------
// Getter accessing external masses with the correct ordering
vector<double> PY8MEs_R8_P4_heft_bb_hhgbb::getMasses() 
{

  vec_double external_masses; 
  vector<int> invertedPerm; 
  invertedPerm = invert_mapping(perm); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    external_masses.push_back(mME[invertedPerm[i]]); 
  }
  return external_masses; 

}


// Set all values of the external masses to float(-mode) where mode can be
// 0 : Mass taken from the model
// 1 : Mass taken from p_i^2 if not massless to begin with
// 2 : Mass always taken from p_i^2.
void PY8MEs_R8_P4_heft_bb_hhgbb::setExternalMassesMode(int mode) 
{
  if (mode != 0 && mode != 1 && mode != 2)
  {
    cerr <<  "Error in function 'setExternalMassesMode' of class" << 
    " 'PY8MEs_R8_P4_heft_bb_hhgbb': Incorrect mode selected :" << mode << 
    ". It must be either 0, 1 or 2" << endl; 
    throw PY8MEs_R8_P4_heft_bb_hhgbb_exception; 
  }
  if (mode == 0)
  {
    syncProcModelParams(); 
  }
  else if (mode == 1)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      if (mME[j] != pars->ZERO)
      {
        mME[j] = -1.0; 
      }
    }
  }
  else if (mode == 2)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      mME[j] = -1.0; 
    }
  }
}

//--------------------------------------------------------------------------
// Evaluate the squared matrix element.

double PY8MEs_R8_P4_heft_bb_hhgbb::sigmaKin() 
{
  // Set the parameters which change event by event
  pars->setDependentParameters(); 
  pars->setDependentCouplings(); 
  // Reset color flows
  for(int i = 0; i < 4; i++ )
    jamp2[0][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[1][i] = 0.; 
  for(int i = 0; i < 4; i++ )
    jamp2[2][i] = 0.; 

  // Save previous values of mME
  vector<double> saved_mME(mME.size(), 0.0); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (mME[i] < 0.0)
    {
      saved_mME[i] = mME[i]; 
      mME[i] = sqrt(abs(pow(p[perm[i]][0], 2) - 
      (pow(p[perm[i]][1], 2) + pow(p[perm[i]][2], 2) + pow(p[perm[i]][3],
          2))));
    }
  }

  // Local variables and constants
  const int max_tries = 10; 
  vector < vec_bool > goodhel(nprocesses, vec_bool(ncomb, false)); 
  vec_int ntry(nprocesses, 0); 
  double t = 0.; 
  double result = 0.; 

  if (ntry[proc_ID] <= max_tries)
    ntry[proc_ID] = ntry[proc_ID] + 1; 

  // Find which helicity configuration is asked for
  // -1 indicates one wants to sum over helicities
  int user_ihel = getHelicityIDForConfig(user_helicities); 

  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = getColorIDForConfig(user_colors); 

  // Reset the list of results that will be recomputed here
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      all_results[proc_ID][ihel + 1][icolor + 1] = 0.; 
    }
  }

  // Calculate the matrix element for all helicities
  // unless already detected as vanishing
  for(int ihel = 0; ihel < ncomb; ihel++ )
  {
    // Skip helicity if already detected as vanishing
    if ((ntry[proc_ID] >= max_tries) && !goodhel[proc_ID][ihel])
      continue; 

    // Also skip helicity if user asks for a specific one
    if ((ntry[proc_ID] >= max_tries) && user_ihel != -1 && user_ihel != ihel)
      continue; 

    calculate_wavefunctions(helicities[ihel]); 

    // Reset locally computed color flows
    for(int i = 0; i < 4; i++ )
      jamp2[0][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[1][i] = 0.; 
    for(int i = 0; i < 4; i++ )
      jamp2[2][i] = 0.; 

    if (proc_ID == 0)
      t = matrix_8_bb_hhgbb(); 
    if (proc_ID == 1)
      t = matrix_8_bbx_hhgbbx(); 
    if (proc_ID == 2)
      t = matrix_8_bxbx_hhgbxbx(); 

    // Store which helicities give non-zero result
    if ((ntry[proc_ID] < max_tries) && t != 0. && !goodhel[proc_ID][ihel])
      goodhel[proc_ID][ihel] = true; 

    // Aggregate results
    if (user_ihel == -1 || user_ihel == ihel)
    {
      if (user_icol == -1)
      {
        result = result + t; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][0] += t; 
          for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
          {
            all_results[proc_ID][0][i + 1] += jamp2[proc_ID][i]; 
          }
        }
        all_results[proc_ID][ihel + 1][0] += t; 
        for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
        {
          all_results[proc_ID][ihel + 1][i + 1] += jamp2[proc_ID][i]; 
        }
      }
      else
      {
        result = result + jamp2[proc_ID][user_icol]; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][user_icol + 1] += jamp2[proc_ID][user_icol]; 
        }
        all_results[proc_ID][ihel + 1][user_icol + 1] +=
            jamp2[proc_ID][user_icol];
      }
    }

  }

  // Normalize results with the identical particle factor
  if (include_symmetry_factors)
  {
    result = result/denom_iden[proc_ID]; 
  }
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      if (include_symmetry_factors)
      {
        all_results[proc_ID][ihel + 1][icolor + 1] /= denom_iden[proc_ID]; 
      }
    }
  }


  // Normalize when when summing+averaging over helicity configurations
  if (user_ihel == -1 && include_helicity_averaging_factors)
  {
    result /= denom_hels[proc_ID]; 
    if (user_icol == -1)
    {
      all_results[proc_ID][0][0] /= denom_hels[proc_ID]; 
      for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
      {
        all_results[proc_ID][0][i + 1] /= denom_hels[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][0][user_icol + 1] /= denom_hels[proc_ID]; 
    }
  }

  // Normalize when summing+averaging over color configurations
  if (user_icol == -1 && include_color_averaging_factors)
  {
    result /= denom_colors[proc_ID]; 
    if (user_ihel == -1)
    {
      all_results[proc_ID][0][0] /= denom_colors[proc_ID]; 
      for (unsigned int i = 0; i < ncomb; i++ )
      {
        all_results[proc_ID][i + 1][0] /= denom_colors[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][user_ihel + 1][0] /= denom_colors[proc_ID]; 
    }
  }

  // Reinstate previous values of mME
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (saved_mME[i] < 0.0)
    {
      mME[i] = saved_mME[i]; 
    }
  }

  // Finally return it
  return result; 
}

//==========================================================================
// Private class member functions

//--------------------------------------------------------------------------
// Evaluate |M|^2 for each subprocess

void PY8MEs_R8_P4_heft_bb_hhgbb::calculate_wavefunctions(const int hel[])
{
  // Calculate wavefunctions for all processes
  // Calculate all wavefunctions
  ixxxxx(p[perm[0]], mME[0], hel[0], +1, w[0]); 
  ixxxxx(p[perm[1]], mME[1], hel[1], +1, w[1]); 
  sxxxxx(p[perm[2]], +1, w[2]); 
  sxxxxx(p[perm[3]], +1, w[3]); 
  vxxxxx(p[perm[4]], mME[4], hel[4], +1, w[4]); 
  oxxxxx(p[perm[5]], mME[5], hel[5], +1, w[5]); 
  oxxxxx(p[perm[6]], mME[6], hel[6], +1, w[6]); 
  FFV1_2(w[0], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[7]); 
  SSS1_1(w[2], w[3], pars->GC_69, pars->mdl_MH, pars->mdl_WH, w[8]); 
  FFV1P0_3(w[7], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[9]); 
  FFS2_1(w[6], w[8], pars->GC_74, pars->mdl_MB, pars->ZERO, w[10]); 
  FFS2_2(w[1], w[8], pars->GC_74, pars->mdl_MB, pars->ZERO, w[11]); 
  FFV1P0_3(w[7], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[12]); 
  FFS2_1(w[5], w[8], pars->GC_74, pars->mdl_MB, pars->ZERO, w[13]); 
  FFV1P0_3(w[1], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[14]); 
  FFS2_2(w[7], w[8], pars->GC_74, pars->mdl_MB, pars->ZERO, w[15]); 
  FFV1_2(w[7], w[14], pars->GC_11, pars->mdl_MB, pars->ZERO, w[16]); 
  FFV1P0_3(w[1], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[17]); 
  FFV1_2(w[7], w[17], pars->GC_11, pars->mdl_MB, pars->ZERO, w[18]); 
  FFS2_1(w[5], w[2], pars->GC_74, pars->mdl_MB, pars->ZERO, w[19]); 
  FFS2_2(w[7], w[3], pars->GC_74, pars->mdl_MB, pars->ZERO, w[20]); 
  FFV1P0_3(w[1], w[19], pars->GC_11, pars->ZERO, pars->ZERO, w[21]); 
  FFS2_1(w[19], w[3], pars->GC_74, pars->mdl_MB, pars->ZERO, w[22]); 
  FFS2_1(w[6], w[3], pars->GC_74, pars->mdl_MB, pars->ZERO, w[23]); 
  FFV1P0_3(w[7], w[19], pars->GC_11, pars->ZERO, pars->ZERO, w[24]); 
  FFV1P0_3(w[7], w[23], pars->GC_11, pars->ZERO, pars->ZERO, w[25]); 
  FFS2_2(w[1], w[3], pars->GC_74, pars->mdl_MB, pars->ZERO, w[26]); 
  FFS2_1(w[6], w[2], pars->GC_74, pars->mdl_MB, pars->ZERO, w[27]); 
  FFV1P0_3(w[1], w[27], pars->GC_11, pars->ZERO, pars->ZERO, w[28]); 
  FFS2_1(w[27], w[3], pars->GC_74, pars->mdl_MB, pars->ZERO, w[29]); 
  FFS2_1(w[5], w[3], pars->GC_74, pars->mdl_MB, pars->ZERO, w[30]); 
  FFV1P0_3(w[7], w[27], pars->GC_11, pars->ZERO, pars->ZERO, w[31]); 
  FFV1P0_3(w[7], w[30], pars->GC_11, pars->ZERO, pars->ZERO, w[32]); 
  FFS2_2(w[1], w[2], pars->GC_74, pars->mdl_MB, pars->ZERO, w[33]); 
  FFV1P0_3(w[33], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[34]); 
  FFV1P0_3(w[33], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[35]); 
  FFS2_2(w[33], w[3], pars->GC_74, pars->mdl_MB, pars->ZERO, w[36]); 
  FFS2_2(w[7], w[2], pars->GC_74, pars->mdl_MB, pars->ZERO, w[37]); 
  FFV1P0_3(w[1], w[30], pars->GC_11, pars->ZERO, pars->ZERO, w[38]); 
  FFS2_1(w[30], w[2], pars->GC_74, pars->mdl_MB, pars->ZERO, w[39]); 
  FFV1P0_3(w[1], w[23], pars->GC_11, pars->ZERO, pars->ZERO, w[40]); 
  FFS2_1(w[23], w[2], pars->GC_74, pars->mdl_MB, pars->ZERO, w[41]); 
  FFV1P0_3(w[26], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[42]); 
  FFV1P0_3(w[26], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[43]); 
  FFS2_2(w[26], w[2], pars->GC_74, pars->mdl_MB, pars->ZERO, w[44]); 
  VVS3P0_1(w[14], w[3], pars->GC_13, pars->ZERO, pars->ZERO, w[45]); 
  FFV1_1(w[6], w[14], pars->GC_11, pars->mdl_MB, pars->ZERO, w[46]); 
  VVS3P0_1(w[14], w[2], pars->GC_13, pars->ZERO, pars->ZERO, w[47]); 
  VVS3P0_1(w[17], w[3], pars->GC_13, pars->ZERO, pars->ZERO, w[48]); 
  FFV1_1(w[5], w[17], pars->GC_11, pars->mdl_MB, pars->ZERO, w[49]); 
  VVS3P0_1(w[17], w[2], pars->GC_13, pars->ZERO, pars->ZERO, w[50]); 
  VVS3P0_1(w[4], w[2], pars->GC_13, pars->ZERO, pars->ZERO, w[51]); 
  FFS2_2(w[0], w[3], pars->GC_74, pars->mdl_MB, pars->ZERO, w[52]); 
  FFV1_1(w[5], w[51], pars->GC_11, pars->mdl_MB, pars->ZERO, w[53]); 
  FFV1P0_3(w[52], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[54]); 
  FFV1_1(w[6], w[51], pars->GC_11, pars->mdl_MB, pars->ZERO, w[55]); 
  FFV1P0_3(w[52], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[56]); 
  FFV1_2(w[1], w[51], pars->GC_11, pars->mdl_MB, pars->ZERO, w[57]); 
  FFV1_2(w[52], w[51], pars->GC_11, pars->mdl_MB, pars->ZERO, w[58]); 
  VVV1P0_1(w[51], w[14], pars->GC_10, pars->ZERO, pars->ZERO, w[59]); 
  VVV1P0_1(w[51], w[17], pars->GC_10, pars->ZERO, pars->ZERO, w[60]); 
  FFV1P0_3(w[0], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[61]); 
  FFV1_2(w[1], w[61], pars->GC_11, pars->mdl_MB, pars->ZERO, w[62]); 
  FFV1_1(w[6], w[61], pars->GC_11, pars->mdl_MB, pars->ZERO, w[63]); 
  VVV1P0_1(w[51], w[61], pars->GC_10, pars->ZERO, pars->ZERO, w[64]); 
  FFV1_1(w[23], w[51], pars->GC_11, pars->mdl_MB, pars->ZERO, w[65]); 
  FFV1_2(w[26], w[51], pars->GC_11, pars->mdl_MB, pars->ZERO, w[66]); 
  FFV1P0_3(w[0], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[67]); 
  FFV1_2(w[1], w[67], pars->GC_11, pars->mdl_MB, pars->ZERO, w[68]); 
  FFV1_1(w[5], w[67], pars->GC_11, pars->mdl_MB, pars->ZERO, w[69]); 
  VVV1P0_1(w[51], w[67], pars->GC_10, pars->ZERO, pars->ZERO, w[70]); 
  FFV1_1(w[30], w[51], pars->GC_11, pars->mdl_MB, pars->ZERO, w[71]); 
  FFV1_2(w[0], w[51], pars->GC_11, pars->mdl_MB, pars->ZERO, w[72]); 
  FFV1P0_3(w[0], w[30], pars->GC_11, pars->ZERO, pars->ZERO, w[73]); 
  FFV1P0_3(w[0], w[23], pars->GC_11, pars->ZERO, pars->ZERO, w[74]); 
  FFV1_2(w[0], w[14], pars->GC_11, pars->mdl_MB, pars->ZERO, w[75]); 
  FFV1_2(w[0], w[17], pars->GC_11, pars->mdl_MB, pars->ZERO, w[76]); 
  VVS3P0_1(w[4], w[3], pars->GC_13, pars->ZERO, pars->ZERO, w[77]); 
  FFS2_2(w[0], w[2], pars->GC_74, pars->mdl_MB, pars->ZERO, w[78]); 
  FFV1_1(w[5], w[77], pars->GC_11, pars->mdl_MB, pars->ZERO, w[79]); 
  FFV1P0_3(w[78], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[80]); 
  FFV1_1(w[6], w[77], pars->GC_11, pars->mdl_MB, pars->ZERO, w[81]); 
  FFV1P0_3(w[78], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[82]); 
  FFV1_2(w[1], w[77], pars->GC_11, pars->mdl_MB, pars->ZERO, w[83]); 
  FFV1_2(w[78], w[77], pars->GC_11, pars->mdl_MB, pars->ZERO, w[84]); 
  VVV1P0_1(w[77], w[14], pars->GC_10, pars->ZERO, pars->ZERO, w[85]); 
  VVV1P0_1(w[77], w[17], pars->GC_10, pars->ZERO, pars->ZERO, w[86]); 
  VVV1P0_1(w[77], w[61], pars->GC_10, pars->ZERO, pars->ZERO, w[87]); 
  FFV1_1(w[27], w[77], pars->GC_11, pars->mdl_MB, pars->ZERO, w[88]); 
  FFV1_2(w[33], w[77], pars->GC_11, pars->mdl_MB, pars->ZERO, w[89]); 
  VVV1P0_1(w[77], w[67], pars->GC_10, pars->ZERO, pars->ZERO, w[90]); 
  FFV1_1(w[19], w[77], pars->GC_11, pars->mdl_MB, pars->ZERO, w[91]); 
  FFV1_2(w[0], w[77], pars->GC_11, pars->mdl_MB, pars->ZERO, w[92]); 
  FFV1P0_3(w[0], w[19], pars->GC_11, pars->ZERO, pars->ZERO, w[93]); 
  FFV1P0_3(w[0], w[27], pars->GC_11, pars->ZERO, pars->ZERO, w[94]); 
  FFV1_1(w[5], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[95]); 
  FFS2_1(w[95], w[3], pars->GC_74, pars->mdl_MB, pars->ZERO, w[96]); 
  FFV1P0_3(w[1], w[95], pars->GC_11, pars->ZERO, pars->ZERO, w[97]); 
  FFS2_2(w[78], w[3], pars->GC_74, pars->mdl_MB, pars->ZERO, w[98]); 
  FFV1P0_3(w[78], w[95], pars->GC_11, pars->ZERO, pars->ZERO, w[99]); 
  FFV1P0_3(w[26], w[95], pars->GC_11, pars->ZERO, pars->ZERO, w[100]); 
  FFV1_1(w[95], w[17], pars->GC_11, pars->mdl_MB, pars->ZERO, w[101]); 
  FFS2_1(w[95], w[2], pars->GC_74, pars->mdl_MB, pars->ZERO, w[102]); 
  FFS2_2(w[52], w[2], pars->GC_74, pars->mdl_MB, pars->ZERO, w[103]); 
  FFV1P0_3(w[52], w[95], pars->GC_11, pars->ZERO, pars->ZERO, w[104]); 
  FFV1P0_3(w[33], w[95], pars->GC_11, pars->ZERO, pars->ZERO, w[105]); 
  VVS3P0_1(w[67], w[3], pars->GC_13, pars->ZERO, pars->ZERO, w[106]); 
  VVS3P0_1(w[67], w[2], pars->GC_13, pars->ZERO, pars->ZERO, w[107]); 
  FFV1_1(w[95], w[67], pars->GC_11, pars->mdl_MB, pars->ZERO, w[108]); 
  FFS2_1(w[95], w[8], pars->GC_74, pars->mdl_MB, pars->ZERO, w[109]); 
  FFV1P0_3(w[0], w[95], pars->GC_11, pars->ZERO, pars->ZERO, w[110]); 
  FFS2_2(w[0], w[8], pars->GC_74, pars->mdl_MB, pars->ZERO, w[111]); 
  FFV1_1(w[6], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[112]); 
  FFS2_1(w[112], w[3], pars->GC_74, pars->mdl_MB, pars->ZERO, w[113]); 
  FFV1P0_3(w[1], w[112], pars->GC_11, pars->ZERO, pars->ZERO, w[114]); 
  FFV1P0_3(w[78], w[112], pars->GC_11, pars->ZERO, pars->ZERO, w[115]); 
  FFV1P0_3(w[26], w[112], pars->GC_11, pars->ZERO, pars->ZERO, w[116]); 
  FFV1_1(w[112], w[14], pars->GC_11, pars->mdl_MB, pars->ZERO, w[117]); 
  FFS2_1(w[112], w[2], pars->GC_74, pars->mdl_MB, pars->ZERO, w[118]); 
  FFV1P0_3(w[52], w[112], pars->GC_11, pars->ZERO, pars->ZERO, w[119]); 
  FFV1P0_3(w[33], w[112], pars->GC_11, pars->ZERO, pars->ZERO, w[120]); 
  VVS3P0_1(w[61], w[3], pars->GC_13, pars->ZERO, pars->ZERO, w[121]); 
  VVS3P0_1(w[61], w[2], pars->GC_13, pars->ZERO, pars->ZERO, w[122]); 
  FFV1_1(w[112], w[61], pars->GC_11, pars->mdl_MB, pars->ZERO, w[123]); 
  FFS2_1(w[112], w[8], pars->GC_74, pars->mdl_MB, pars->ZERO, w[124]); 
  FFV1P0_3(w[0], w[112], pars->GC_11, pars->ZERO, pars->ZERO, w[125]); 
  FFV1_2(w[1], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[126]); 
  FFS2_2(w[126], w[3], pars->GC_74, pars->mdl_MB, pars->ZERO, w[127]); 
  FFV1P0_3(w[126], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[128]); 
  FFV1P0_3(w[126], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[129]); 
  FFV1P0_3(w[126], w[30], pars->GC_11, pars->ZERO, pars->ZERO, w[130]); 
  FFV1P0_3(w[126], w[23], pars->GC_11, pars->ZERO, pars->ZERO, w[131]); 
  FFS2_2(w[126], w[2], pars->GC_74, pars->mdl_MB, pars->ZERO, w[132]); 
  FFV1P0_3(w[126], w[19], pars->GC_11, pars->ZERO, pars->ZERO, w[133]); 
  FFV1P0_3(w[126], w[27], pars->GC_11, pars->ZERO, pars->ZERO, w[134]); 
  FFV1_2(w[126], w[61], pars->GC_11, pars->mdl_MB, pars->ZERO, w[135]); 
  FFS2_2(w[126], w[8], pars->GC_74, pars->mdl_MB, pars->ZERO, w[136]); 
  FFV1_2(w[126], w[67], pars->GC_11, pars->mdl_MB, pars->ZERO, w[137]); 
  FFV1_2(w[78], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[138]); 
  FFV1_1(w[30], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[139]); 
  VVV1P0_1(w[4], w[17], pars->GC_10, pars->ZERO, pars->ZERO, w[140]); 
  FFV1_1(w[23], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[141]); 
  VVV1P0_1(w[4], w[14], pars->GC_10, pars->ZERO, pars->ZERO, w[142]); 
  FFV1_2(w[26], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[143]); 
  VVVS2P0_1(w[4], w[14], w[3], pars->GC_14, pars->ZERO, pars->ZERO, w[144]); 
  VVVS2P0_1(w[4], w[17], w[3], pars->GC_14, pars->ZERO, pars->ZERO, w[145]); 
  FFV1_2(w[52], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[146]); 
  FFV1_1(w[19], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[147]); 
  FFV1_1(w[27], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[148]); 
  FFV1_2(w[33], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[149]); 
  VVVS2P0_1(w[4], w[14], w[2], pars->GC_14, pars->ZERO, pars->ZERO, w[150]); 
  VVVS2P0_1(w[4], w[17], w[2], pars->GC_14, pars->ZERO, pars->ZERO, w[151]); 
  VVV1P0_1(w[4], w[61], pars->GC_10, pars->ZERO, pars->ZERO, w[152]); 
  VVS3P0_1(w[4], w[8], pars->GC_13, pars->ZERO, pars->ZERO, w[153]); 
  VVVS2P0_1(w[4], w[61], w[3], pars->GC_14, pars->ZERO, pars->ZERO, w[154]); 
  VVVS2P0_1(w[4], w[61], w[2], pars->GC_14, pars->ZERO, pars->ZERO, w[155]); 
  VVV1P0_1(w[4], w[67], pars->GC_10, pars->ZERO, pars->ZERO, w[156]); 
  VVVS2P0_1(w[4], w[67], w[3], pars->GC_14, pars->ZERO, pars->ZERO, w[157]); 
  VVVS2P0_1(w[4], w[67], w[2], pars->GC_14, pars->ZERO, pars->ZERO, w[158]); 
  oxxxxx(p[perm[1]], mME[1], hel[1], -1, w[159]); 
  ixxxxx(p[perm[6]], mME[6], hel[6], -1, w[160]); 
  FFV1P0_3(w[7], w[159], pars->GC_11, pars->ZERO, pars->ZERO, w[161]); 
  FFS2_2(w[160], w[8], pars->GC_74, pars->mdl_MB, pars->ZERO, w[162]); 
  FFS2_1(w[159], w[8], pars->GC_74, pars->mdl_MB, pars->ZERO, w[163]); 
  FFV1P0_3(w[160], w[159], pars->GC_11, pars->ZERO, pars->ZERO, w[164]); 
  FFV1_2(w[7], w[164], pars->GC_11, pars->mdl_MB, pars->ZERO, w[165]); 
  FFV1P0_3(w[160], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[166]); 
  FFV1_2(w[7], w[166], pars->GC_11, pars->mdl_MB, pars->ZERO, w[167]); 
  FFS2_1(w[159], w[2], pars->GC_74, pars->mdl_MB, pars->ZERO, w[168]); 
  FFV1P0_3(w[160], w[168], pars->GC_11, pars->ZERO, pars->ZERO, w[169]); 
  FFS2_1(w[168], w[3], pars->GC_74, pars->mdl_MB, pars->ZERO, w[170]); 
  FFV1P0_3(w[7], w[168], pars->GC_11, pars->ZERO, pars->ZERO, w[171]); 
  FFS2_2(w[160], w[3], pars->GC_74, pars->mdl_MB, pars->ZERO, w[172]); 
  FFV1P0_3(w[160], w[19], pars->GC_11, pars->ZERO, pars->ZERO, w[173]); 
  FFS2_1(w[159], w[3], pars->GC_74, pars->mdl_MB, pars->ZERO, w[174]); 
  FFV1P0_3(w[7], w[174], pars->GC_11, pars->ZERO, pars->ZERO, w[175]); 
  FFS2_2(w[160], w[2], pars->GC_74, pars->mdl_MB, pars->ZERO, w[176]); 
  FFV1P0_3(w[176], w[159], pars->GC_11, pars->ZERO, pars->ZERO, w[177]); 
  FFV1P0_3(w[176], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[178]); 
  FFS2_2(w[176], w[3], pars->GC_74, pars->mdl_MB, pars->ZERO, w[179]); 
  FFV1P0_3(w[160], w[174], pars->GC_11, pars->ZERO, pars->ZERO, w[180]); 
  FFS2_1(w[174], w[2], pars->GC_74, pars->mdl_MB, pars->ZERO, w[181]); 
  FFV1P0_3(w[160], w[30], pars->GC_11, pars->ZERO, pars->ZERO, w[182]); 
  FFV1P0_3(w[172], w[159], pars->GC_11, pars->ZERO, pars->ZERO, w[183]); 
  FFV1P0_3(w[172], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[184]); 
  FFS2_2(w[172], w[2], pars->GC_74, pars->mdl_MB, pars->ZERO, w[185]); 
  VVS3P0_1(w[164], w[3], pars->GC_13, pars->ZERO, pars->ZERO, w[186]); 
  FFV1_1(w[5], w[164], pars->GC_11, pars->mdl_MB, pars->ZERO, w[187]); 
  VVS3P0_1(w[164], w[2], pars->GC_13, pars->ZERO, pars->ZERO, w[188]); 
  VVS3P0_1(w[166], w[3], pars->GC_13, pars->ZERO, pars->ZERO, w[189]); 
  FFV1_1(w[159], w[166], pars->GC_11, pars->mdl_MB, pars->ZERO, w[190]); 
  VVS3P0_1(w[166], w[2], pars->GC_13, pars->ZERO, pars->ZERO, w[191]); 
  FFV1_1(w[159], w[51], pars->GC_11, pars->mdl_MB, pars->ZERO, w[192]); 
  FFV1P0_3(w[52], w[159], pars->GC_11, pars->ZERO, pars->ZERO, w[193]); 
  FFV1_2(w[160], w[51], pars->GC_11, pars->mdl_MB, pars->ZERO, w[194]); 
  VVV1P0_1(w[51], w[164], pars->GC_10, pars->ZERO, pars->ZERO, w[195]); 
  VVV1P0_1(w[51], w[166], pars->GC_10, pars->ZERO, pars->ZERO, w[196]); 
  FFV1P0_3(w[0], w[159], pars->GC_11, pars->ZERO, pars->ZERO, w[197]); 
  FFV1_2(w[160], w[197], pars->GC_11, pars->mdl_MB, pars->ZERO, w[198]); 
  FFV1_1(w[5], w[197], pars->GC_11, pars->mdl_MB, pars->ZERO, w[199]); 
  VVV1P0_1(w[51], w[197], pars->GC_10, pars->ZERO, pars->ZERO, w[200]); 
  FFV1_2(w[172], w[51], pars->GC_11, pars->mdl_MB, pars->ZERO, w[201]); 
  FFV1_2(w[160], w[61], pars->GC_11, pars->mdl_MB, pars->ZERO, w[202]); 
  FFV1_1(w[159], w[61], pars->GC_11, pars->mdl_MB, pars->ZERO, w[203]); 
  FFV1_1(w[174], w[51], pars->GC_11, pars->mdl_MB, pars->ZERO, w[204]); 
  FFV1P0_3(w[0], w[174], pars->GC_11, pars->ZERO, pars->ZERO, w[205]); 
  FFV1_2(w[0], w[164], pars->GC_11, pars->mdl_MB, pars->ZERO, w[206]); 
  FFV1_2(w[0], w[166], pars->GC_11, pars->mdl_MB, pars->ZERO, w[207]); 
  FFV1_1(w[159], w[77], pars->GC_11, pars->mdl_MB, pars->ZERO, w[208]); 
  FFV1P0_3(w[78], w[159], pars->GC_11, pars->ZERO, pars->ZERO, w[209]); 
  FFV1_2(w[160], w[77], pars->GC_11, pars->mdl_MB, pars->ZERO, w[210]); 
  VVV1P0_1(w[77], w[164], pars->GC_10, pars->ZERO, pars->ZERO, w[211]); 
  VVV1P0_1(w[77], w[166], pars->GC_10, pars->ZERO, pars->ZERO, w[212]); 
  VVV1P0_1(w[77], w[197], pars->GC_10, pars->ZERO, pars->ZERO, w[213]); 
  FFV1_2(w[176], w[77], pars->GC_11, pars->mdl_MB, pars->ZERO, w[214]); 
  FFV1_1(w[168], w[77], pars->GC_11, pars->mdl_MB, pars->ZERO, w[215]); 
  FFV1P0_3(w[0], w[168], pars->GC_11, pars->ZERO, pars->ZERO, w[216]); 
  FFV1_1(w[159], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[217]); 
  FFS2_1(w[217], w[3], pars->GC_74, pars->mdl_MB, pars->ZERO, w[218]); 
  FFV1P0_3(w[160], w[217], pars->GC_11, pars->ZERO, pars->ZERO, w[219]); 
  FFV1P0_3(w[78], w[217], pars->GC_11, pars->ZERO, pars->ZERO, w[220]); 
  FFV1P0_3(w[172], w[217], pars->GC_11, pars->ZERO, pars->ZERO, w[221]); 
  FFV1_1(w[217], w[166], pars->GC_11, pars->mdl_MB, pars->ZERO, w[222]); 
  FFS2_1(w[217], w[2], pars->GC_74, pars->mdl_MB, pars->ZERO, w[223]); 
  FFV1P0_3(w[52], w[217], pars->GC_11, pars->ZERO, pars->ZERO, w[224]); 
  FFV1P0_3(w[176], w[217], pars->GC_11, pars->ZERO, pars->ZERO, w[225]); 
  FFV1_1(w[217], w[61], pars->GC_11, pars->mdl_MB, pars->ZERO, w[226]); 
  FFS2_1(w[217], w[8], pars->GC_74, pars->mdl_MB, pars->ZERO, w[227]); 
  FFV1P0_3(w[0], w[217], pars->GC_11, pars->ZERO, pars->ZERO, w[228]); 
  FFV1P0_3(w[160], w[95], pars->GC_11, pars->ZERO, pars->ZERO, w[229]); 
  FFV1P0_3(w[172], w[95], pars->GC_11, pars->ZERO, pars->ZERO, w[230]); 
  FFV1_1(w[95], w[164], pars->GC_11, pars->mdl_MB, pars->ZERO, w[231]); 
  FFV1P0_3(w[176], w[95], pars->GC_11, pars->ZERO, pars->ZERO, w[232]); 
  VVS3P0_1(w[197], w[3], pars->GC_13, pars->ZERO, pars->ZERO, w[233]); 
  VVS3P0_1(w[197], w[2], pars->GC_13, pars->ZERO, pars->ZERO, w[234]); 
  FFV1_1(w[95], w[197], pars->GC_11, pars->mdl_MB, pars->ZERO, w[235]); 
  FFV1_2(w[160], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[236]); 
  FFS2_2(w[236], w[3], pars->GC_74, pars->mdl_MB, pars->ZERO, w[237]); 
  FFV1P0_3(w[236], w[159], pars->GC_11, pars->ZERO, pars->ZERO, w[238]); 
  FFV1P0_3(w[236], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[239]); 
  FFV1P0_3(w[236], w[174], pars->GC_11, pars->ZERO, pars->ZERO, w[240]); 
  FFV1P0_3(w[236], w[30], pars->GC_11, pars->ZERO, pars->ZERO, w[241]); 
  FFS2_2(w[236], w[2], pars->GC_74, pars->mdl_MB, pars->ZERO, w[242]); 
  FFV1P0_3(w[236], w[168], pars->GC_11, pars->ZERO, pars->ZERO, w[243]); 
  FFV1P0_3(w[236], w[19], pars->GC_11, pars->ZERO, pars->ZERO, w[244]); 
  FFV1_2(w[236], w[197], pars->GC_11, pars->mdl_MB, pars->ZERO, w[245]); 
  FFS2_2(w[236], w[8], pars->GC_74, pars->mdl_MB, pars->ZERO, w[246]); 
  FFV1_2(w[236], w[61], pars->GC_11, pars->mdl_MB, pars->ZERO, w[247]); 
  FFV1_1(w[174], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[248]); 
  VVV1P0_1(w[4], w[166], pars->GC_10, pars->ZERO, pars->ZERO, w[249]); 
  VVV1P0_1(w[4], w[164], pars->GC_10, pars->ZERO, pars->ZERO, w[250]); 
  FFV1_2(w[172], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[251]); 
  VVVS2P0_1(w[4], w[164], w[3], pars->GC_14, pars->ZERO, pars->ZERO, w[252]); 
  VVVS2P0_1(w[4], w[166], w[3], pars->GC_14, pars->ZERO, pars->ZERO, w[253]); 
  FFV1_1(w[168], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[254]); 
  FFV1_2(w[176], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[255]); 
  VVVS2P0_1(w[4], w[164], w[2], pars->GC_14, pars->ZERO, pars->ZERO, w[256]); 
  VVVS2P0_1(w[4], w[166], w[2], pars->GC_14, pars->ZERO, pars->ZERO, w[257]); 
  VVV1P0_1(w[4], w[197], pars->GC_10, pars->ZERO, pars->ZERO, w[258]); 
  VVVS2P0_1(w[4], w[197], w[3], pars->GC_14, pars->ZERO, pars->ZERO, w[259]); 
  VVVS2P0_1(w[4], w[197], w[2], pars->GC_14, pars->ZERO, pars->ZERO, w[260]); 
  oxxxxx(p[perm[0]], mME[0], hel[0], -1, w[261]); 
  ixxxxx(p[perm[5]], mME[5], hel[5], -1, w[262]); 
  FFV1_2(w[262], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[263]); 
  FFV1P0_3(w[263], w[261], pars->GC_11, pars->ZERO, pars->ZERO, w[264]); 
  FFV1P0_3(w[263], w[159], pars->GC_11, pars->ZERO, pars->ZERO, w[265]); 
  FFS2_1(w[261], w[8], pars->GC_74, pars->mdl_MB, pars->ZERO, w[266]); 
  FFV1P0_3(w[160], w[261], pars->GC_11, pars->ZERO, pars->ZERO, w[267]); 
  FFS2_2(w[263], w[8], pars->GC_74, pars->mdl_MB, pars->ZERO, w[268]); 
  FFV1_2(w[263], w[267], pars->GC_11, pars->mdl_MB, pars->ZERO, w[269]); 
  FFV1_2(w[263], w[164], pars->GC_11, pars->mdl_MB, pars->ZERO, w[270]); 
  FFS2_1(w[261], w[2], pars->GC_74, pars->mdl_MB, pars->ZERO, w[271]); 
  FFS2_2(w[263], w[3], pars->GC_74, pars->mdl_MB, pars->ZERO, w[272]); 
  FFV1P0_3(w[160], w[271], pars->GC_11, pars->ZERO, pars->ZERO, w[273]); 
  FFS2_1(w[271], w[3], pars->GC_74, pars->mdl_MB, pars->ZERO, w[274]); 
  FFV1P0_3(w[263], w[271], pars->GC_11, pars->ZERO, pars->ZERO, w[275]); 
  FFV1P0_3(w[263], w[174], pars->GC_11, pars->ZERO, pars->ZERO, w[276]); 
  FFS2_1(w[261], w[3], pars->GC_74, pars->mdl_MB, pars->ZERO, w[277]); 
  FFV1P0_3(w[263], w[168], pars->GC_11, pars->ZERO, pars->ZERO, w[278]); 
  FFV1P0_3(w[263], w[277], pars->GC_11, pars->ZERO, pars->ZERO, w[279]); 
  FFV1P0_3(w[176], w[261], pars->GC_11, pars->ZERO, pars->ZERO, w[280]); 
  FFS2_2(w[263], w[2], pars->GC_74, pars->mdl_MB, pars->ZERO, w[281]); 
  FFV1P0_3(w[160], w[277], pars->GC_11, pars->ZERO, pars->ZERO, w[282]); 
  FFS2_1(w[277], w[2], pars->GC_74, pars->mdl_MB, pars->ZERO, w[283]); 
  FFV1P0_3(w[172], w[261], pars->GC_11, pars->ZERO, pars->ZERO, w[284]); 
  VVS3P0_1(w[267], w[3], pars->GC_13, pars->ZERO, pars->ZERO, w[285]); 
  FFV1_1(w[159], w[267], pars->GC_11, pars->mdl_MB, pars->ZERO, w[286]); 
  VVS3P0_1(w[267], w[2], pars->GC_13, pars->ZERO, pars->ZERO, w[287]); 
  FFV1_1(w[261], w[164], pars->GC_11, pars->mdl_MB, pars->ZERO, w[288]); 
  FFS2_2(w[262], w[3], pars->GC_74, pars->mdl_MB, pars->ZERO, w[289]); 
  FFV1_1(w[261], w[51], pars->GC_11, pars->mdl_MB, pars->ZERO, w[290]); 
  FFV1P0_3(w[289], w[159], pars->GC_11, pars->ZERO, pars->ZERO, w[291]); 
  FFV1P0_3(w[289], w[261], pars->GC_11, pars->ZERO, pars->ZERO, w[292]); 
  FFV1_2(w[289], w[51], pars->GC_11, pars->mdl_MB, pars->ZERO, w[293]); 
  VVV1P0_1(w[51], w[267], pars->GC_10, pars->ZERO, pars->ZERO, w[294]); 
  FFV1P0_3(w[262], w[261], pars->GC_11, pars->ZERO, pars->ZERO, w[295]); 
  FFV1_2(w[160], w[295], pars->GC_11, pars->mdl_MB, pars->ZERO, w[296]); 
  FFV1_1(w[159], w[295], pars->GC_11, pars->mdl_MB, pars->ZERO, w[297]); 
  VVV1P0_1(w[51], w[295], pars->GC_10, pars->ZERO, pars->ZERO, w[298]); 
  FFV1P0_3(w[262], w[159], pars->GC_11, pars->ZERO, pars->ZERO, w[299]); 
  FFV1_2(w[160], w[299], pars->GC_11, pars->mdl_MB, pars->ZERO, w[300]); 
  FFV1_1(w[261], w[299], pars->GC_11, pars->mdl_MB, pars->ZERO, w[301]); 
  VVV1P0_1(w[51], w[299], pars->GC_10, pars->ZERO, pars->ZERO, w[302]); 
  FFV1_1(w[277], w[51], pars->GC_11, pars->mdl_MB, pars->ZERO, w[303]); 
  FFV1_2(w[262], w[51], pars->GC_11, pars->mdl_MB, pars->ZERO, w[304]); 
  FFV1P0_3(w[262], w[277], pars->GC_11, pars->ZERO, pars->ZERO, w[305]); 
  FFV1P0_3(w[262], w[174], pars->GC_11, pars->ZERO, pars->ZERO, w[306]); 
  FFV1_2(w[262], w[267], pars->GC_11, pars->mdl_MB, pars->ZERO, w[307]); 
  FFV1_2(w[262], w[164], pars->GC_11, pars->mdl_MB, pars->ZERO, w[308]); 
  FFS2_2(w[262], w[2], pars->GC_74, pars->mdl_MB, pars->ZERO, w[309]); 
  FFV1_1(w[261], w[77], pars->GC_11, pars->mdl_MB, pars->ZERO, w[310]); 
  FFV1P0_3(w[309], w[159], pars->GC_11, pars->ZERO, pars->ZERO, w[311]); 
  FFV1P0_3(w[309], w[261], pars->GC_11, pars->ZERO, pars->ZERO, w[312]); 
  FFV1_2(w[309], w[77], pars->GC_11, pars->mdl_MB, pars->ZERO, w[313]); 
  VVV1P0_1(w[77], w[267], pars->GC_10, pars->ZERO, pars->ZERO, w[314]); 
  VVV1P0_1(w[77], w[295], pars->GC_10, pars->ZERO, pars->ZERO, w[315]); 
  VVV1P0_1(w[77], w[299], pars->GC_10, pars->ZERO, pars->ZERO, w[316]); 
  FFV1_1(w[271], w[77], pars->GC_11, pars->mdl_MB, pars->ZERO, w[317]); 
  FFV1_2(w[262], w[77], pars->GC_11, pars->mdl_MB, pars->ZERO, w[318]); 
  FFV1P0_3(w[262], w[271], pars->GC_11, pars->ZERO, pars->ZERO, w[319]); 
  FFV1P0_3(w[262], w[168], pars->GC_11, pars->ZERO, pars->ZERO, w[320]); 
  FFV1_1(w[261], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[321]); 
  FFS2_1(w[321], w[3], pars->GC_74, pars->mdl_MB, pars->ZERO, w[322]); 
  FFV1P0_3(w[160], w[321], pars->GC_11, pars->ZERO, pars->ZERO, w[323]); 
  FFS2_2(w[309], w[3], pars->GC_74, pars->mdl_MB, pars->ZERO, w[324]); 
  FFV1P0_3(w[309], w[321], pars->GC_11, pars->ZERO, pars->ZERO, w[325]); 
  FFV1P0_3(w[172], w[321], pars->GC_11, pars->ZERO, pars->ZERO, w[326]); 
  FFV1_1(w[321], w[164], pars->GC_11, pars->mdl_MB, pars->ZERO, w[327]); 
  FFS2_1(w[321], w[2], pars->GC_74, pars->mdl_MB, pars->ZERO, w[328]); 
  FFS2_2(w[289], w[2], pars->GC_74, pars->mdl_MB, pars->ZERO, w[329]); 
  FFV1P0_3(w[289], w[321], pars->GC_11, pars->ZERO, pars->ZERO, w[330]); 
  FFV1P0_3(w[176], w[321], pars->GC_11, pars->ZERO, pars->ZERO, w[331]); 
  VVS3P0_1(w[299], w[3], pars->GC_13, pars->ZERO, pars->ZERO, w[332]); 
  VVS3P0_1(w[299], w[2], pars->GC_13, pars->ZERO, pars->ZERO, w[333]); 
  FFV1_1(w[321], w[299], pars->GC_11, pars->mdl_MB, pars->ZERO, w[334]); 
  FFS2_1(w[321], w[8], pars->GC_74, pars->mdl_MB, pars->ZERO, w[335]); 
  FFV1P0_3(w[262], w[321], pars->GC_11, pars->ZERO, pars->ZERO, w[336]); 
  FFS2_2(w[262], w[8], pars->GC_74, pars->mdl_MB, pars->ZERO, w[337]); 
  FFV1P0_3(w[309], w[217], pars->GC_11, pars->ZERO, pars->ZERO, w[338]); 
  FFV1_1(w[217], w[267], pars->GC_11, pars->mdl_MB, pars->ZERO, w[339]); 
  FFV1P0_3(w[289], w[217], pars->GC_11, pars->ZERO, pars->ZERO, w[340]); 
  VVS3P0_1(w[295], w[3], pars->GC_13, pars->ZERO, pars->ZERO, w[341]); 
  VVS3P0_1(w[295], w[2], pars->GC_13, pars->ZERO, pars->ZERO, w[342]); 
  FFV1_1(w[217], w[295], pars->GC_11, pars->mdl_MB, pars->ZERO, w[343]); 
  FFV1P0_3(w[262], w[217], pars->GC_11, pars->ZERO, pars->ZERO, w[344]); 
  FFV1P0_3(w[236], w[261], pars->GC_11, pars->ZERO, pars->ZERO, w[345]); 
  FFV1P0_3(w[236], w[277], pars->GC_11, pars->ZERO, pars->ZERO, w[346]); 
  FFV1P0_3(w[236], w[271], pars->GC_11, pars->ZERO, pars->ZERO, w[347]); 
  FFV1_2(w[236], w[295], pars->GC_11, pars->mdl_MB, pars->ZERO, w[348]); 
  FFV1_2(w[236], w[299], pars->GC_11, pars->mdl_MB, pars->ZERO, w[349]); 
  FFV1_2(w[309], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[350]); 
  FFV1_1(w[277], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[351]); 
  VVV1P0_1(w[4], w[267], pars->GC_10, pars->ZERO, pars->ZERO, w[352]); 
  VVVS2P0_1(w[4], w[267], w[3], pars->GC_14, pars->ZERO, pars->ZERO, w[353]); 
  FFV1_2(w[289], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[354]); 
  FFV1_1(w[271], w[4], pars->GC_11, pars->mdl_MB, pars->ZERO, w[355]); 
  VVVS2P0_1(w[4], w[267], w[2], pars->GC_14, pars->ZERO, pars->ZERO, w[356]); 
  VVV1P0_1(w[4], w[295], pars->GC_10, pars->ZERO, pars->ZERO, w[357]); 
  VVVS2P0_1(w[4], w[295], w[3], pars->GC_14, pars->ZERO, pars->ZERO, w[358]); 
  VVVS2P0_1(w[4], w[295], w[2], pars->GC_14, pars->ZERO, pars->ZERO, w[359]); 
  VVV1P0_1(w[4], w[299], pars->GC_10, pars->ZERO, pars->ZERO, w[360]); 
  VVVS2P0_1(w[4], w[299], w[3], pars->GC_14, pars->ZERO, pars->ZERO, w[361]); 
  VVVS2P0_1(w[4], w[299], w[2], pars->GC_14, pars->ZERO, pars->ZERO, w[362]); 

  // Calculate all amplitudes
  // Amplitude(s) for diagram number 0
  FFV1_0(w[1], w[10], w[9], pars->GC_11, amp[0]); 
  FFV1_0(w[11], w[6], w[9], pars->GC_11, amp[1]); 
  FFV1_0(w[1], w[13], w[12], pars->GC_11, amp[2]); 
  FFV1_0(w[11], w[5], w[12], pars->GC_11, amp[3]); 
  FFV1_0(w[15], w[6], w[14], pars->GC_11, amp[4]); 
  FFS2_0(w[16], w[6], w[8], pars->GC_74, amp[5]); 
  VVS3_0(w[14], w[12], w[8], pars->GC_13, amp[6]); 
  FFV1_0(w[15], w[5], w[17], pars->GC_11, amp[7]); 
  VVS3_0(w[17], w[9], w[8], pars->GC_13, amp[8]); 
  FFS2_0(w[18], w[5], w[8], pars->GC_74, amp[9]); 
  FFV1_0(w[20], w[6], w[21], pars->GC_11, amp[10]); 
  FFV1_0(w[1], w[22], w[12], pars->GC_11, amp[11]); 
  VVS3_0(w[12], w[21], w[3], pars->GC_13, amp[12]); 
  FFV1_0(w[1], w[23], w[24], pars->GC_11, amp[13]); 
  FFV1_0(w[1], w[19], w[25], pars->GC_11, amp[14]); 
  FFV1_0(w[26], w[6], w[24], pars->GC_11, amp[15]); 
  FFV1_0(w[26], w[19], w[12], pars->GC_11, amp[16]); 
  VVS3_0(w[17], w[24], w[3], pars->GC_13, amp[17]); 
  FFV1_0(w[20], w[19], w[17], pars->GC_11, amp[18]); 
  FFS2_0(w[18], w[19], w[3], pars->GC_74, amp[19]); 
  FFV1_0(w[20], w[5], w[28], pars->GC_11, amp[20]); 
  FFV1_0(w[1], w[29], w[9], pars->GC_11, amp[21]); 
  VVS3_0(w[9], w[28], w[3], pars->GC_13, amp[22]); 
  FFV1_0(w[1], w[30], w[31], pars->GC_11, amp[23]); 
  FFV1_0(w[1], w[27], w[32], pars->GC_11, amp[24]); 
  FFV1_0(w[26], w[5], w[31], pars->GC_11, amp[25]); 
  FFV1_0(w[26], w[27], w[9], pars->GC_11, amp[26]); 
  VVS3_0(w[14], w[31], w[3], pars->GC_13, amp[27]); 
  FFV1_0(w[20], w[27], w[14], pars->GC_11, amp[28]); 
  FFS2_0(w[16], w[27], w[3], pars->GC_74, amp[29]); 
  FFV1_0(w[20], w[6], w[34], pars->GC_11, amp[30]); 
  FFV1_0(w[20], w[5], w[35], pars->GC_11, amp[31]); 
  FFV1_0(w[36], w[6], w[9], pars->GC_11, amp[32]); 
  VVS3_0(w[9], w[35], w[3], pars->GC_13, amp[33]); 
  FFV1_0(w[36], w[5], w[12], pars->GC_11, amp[34]); 
  VVS3_0(w[12], w[34], w[3], pars->GC_13, amp[35]); 
  FFV1_0(w[33], w[6], w[32], pars->GC_11, amp[36]); 
  FFV1_0(w[33], w[30], w[12], pars->GC_11, amp[37]); 
  FFV1_0(w[33], w[5], w[25], pars->GC_11, amp[38]); 
  FFV1_0(w[33], w[23], w[9], pars->GC_11, amp[39]); 
  FFV1_0(w[37], w[6], w[38], pars->GC_11, amp[40]); 
  FFV1_0(w[1], w[39], w[12], pars->GC_11, amp[41]); 
  VVS3_0(w[12], w[38], w[2], pars->GC_13, amp[42]); 
  FFV1_0(w[37], w[30], w[17], pars->GC_11, amp[43]); 
  VVS3_0(w[17], w[32], w[2], pars->GC_13, amp[44]); 
  FFS2_0(w[18], w[30], w[2], pars->GC_74, amp[45]); 
  FFV1_0(w[37], w[5], w[40], pars->GC_11, amp[46]); 
  FFV1_0(w[1], w[41], w[9], pars->GC_11, amp[47]); 
  VVS3_0(w[9], w[40], w[2], pars->GC_13, amp[48]); 
  FFV1_0(w[37], w[23], w[14], pars->GC_11, amp[49]); 
  VVS3_0(w[14], w[25], w[2], pars->GC_13, amp[50]); 
  FFS2_0(w[16], w[23], w[2], pars->GC_74, amp[51]); 
  FFV1_0(w[37], w[6], w[42], pars->GC_11, amp[52]); 
  FFV1_0(w[37], w[5], w[43], pars->GC_11, amp[53]); 
  FFV1_0(w[44], w[6], w[9], pars->GC_11, amp[54]); 
  VVS3_0(w[9], w[43], w[2], pars->GC_13, amp[55]); 
  FFV1_0(w[44], w[5], w[12], pars->GC_11, amp[56]); 
  VVS3_0(w[12], w[42], w[2], pars->GC_13, amp[57]); 
  FFV1_0(w[37], w[6], w[45], pars->GC_11, amp[58]); 
  FFS2_0(w[37], w[46], w[3], pars->GC_74, amp[59]); 
  FFV1_0(w[20], w[6], w[47], pars->GC_11, amp[60]); 
  FFS2_0(w[20], w[46], w[2], pars->GC_74, amp[61]); 
  FFV1_0(w[37], w[5], w[48], pars->GC_11, amp[62]); 
  FFS2_0(w[37], w[49], w[3], pars->GC_74, amp[63]); 
  FFV1_0(w[20], w[5], w[50], pars->GC_11, amp[64]); 
  FFS2_0(w[20], w[49], w[2], pars->GC_74, amp[65]); 
  FFV1_0(w[1], w[53], w[54], pars->GC_11, amp[66]); 
  FFV1_0(w[1], w[55], w[56], pars->GC_11, amp[67]); 
  FFV1_0(w[57], w[6], w[56], pars->GC_11, amp[68]); 
  FFV1_0(w[57], w[5], w[54], pars->GC_11, amp[69]); 
  FFV1_0(w[58], w[6], w[14], pars->GC_11, amp[70]); 
  FFV1_0(w[52], w[6], w[59], pars->GC_11, amp[71]); 
  FFV1_0(w[52], w[55], w[14], pars->GC_11, amp[72]); 
  FFV1_0(w[58], w[5], w[17], pars->GC_11, amp[73]); 
  FFV1_0(w[52], w[53], w[17], pars->GC_11, amp[74]); 
  FFV1_0(w[52], w[5], w[60], pars->GC_11, amp[75]); 
  FFS2_0(w[62], w[55], w[3], pars->GC_74, amp[76]); 
  FFS2_0(w[57], w[63], w[3], pars->GC_74, amp[77]); 
  FFV1_0(w[1], w[23], w[64], pars->GC_11, amp[78]); 
  FFV1_0(w[1], w[65], w[61], pars->GC_11, amp[79]); 
  FFV1_0(w[57], w[23], w[61], pars->GC_11, amp[80]); 
  FFV1_0(w[26], w[6], w[64], pars->GC_11, amp[81]); 
  FFV1_0(w[66], w[6], w[61], pars->GC_11, amp[82]); 
  FFV1_0(w[26], w[55], w[61], pars->GC_11, amp[83]); 
  FFS2_0(w[68], w[53], w[3], pars->GC_74, amp[84]); 
  FFS2_0(w[57], w[69], w[3], pars->GC_74, amp[85]); 
  FFV1_0(w[1], w[30], w[70], pars->GC_11, amp[86]); 
  FFV1_0(w[1], w[71], w[67], pars->GC_11, amp[87]); 
  FFV1_0(w[57], w[30], w[67], pars->GC_11, amp[88]); 
  FFV1_0(w[26], w[5], w[70], pars->GC_11, amp[89]); 
  FFV1_0(w[66], w[5], w[67], pars->GC_11, amp[90]); 
  FFV1_0(w[26], w[53], w[67], pars->GC_11, amp[91]); 
  FFV1_0(w[72], w[6], w[38], pars->GC_11, amp[92]); 
  FFV1_0(w[1], w[55], w[73], pars->GC_11, amp[93]); 
  FFV1_0(w[0], w[55], w[38], pars->GC_11, amp[94]); 
  FFV1_0(w[57], w[6], w[73], pars->GC_11, amp[95]); 
  FFV1_0(w[72], w[30], w[17], pars->GC_11, amp[96]); 
  FFV1_0(w[0], w[71], w[17], pars->GC_11, amp[97]); 
  FFV1_0(w[0], w[30], w[60], pars->GC_11, amp[98]); 
  FFV1_0(w[72], w[5], w[40], pars->GC_11, amp[99]); 
  FFV1_0(w[1], w[53], w[74], pars->GC_11, amp[100]); 
  FFV1_0(w[0], w[53], w[40], pars->GC_11, amp[101]); 
  FFV1_0(w[57], w[5], w[74], pars->GC_11, amp[102]); 
  FFV1_0(w[72], w[23], w[14], pars->GC_11, amp[103]); 
  FFV1_0(w[0], w[65], w[14], pars->GC_11, amp[104]); 
  FFV1_0(w[0], w[23], w[59], pars->GC_11, amp[105]); 
  FFV1_0(w[72], w[6], w[42], pars->GC_11, amp[106]); 
  FFV1_0(w[72], w[5], w[43], pars->GC_11, amp[107]); 
  FFV1_0(w[0], w[53], w[43], pars->GC_11, amp[108]); 
  FFV1_0(w[0], w[55], w[42], pars->GC_11, amp[109]); 
  FFS2_0(w[72], w[46], w[3], pars->GC_74, amp[110]); 
  FFS2_0(w[75], w[55], w[3], pars->GC_74, amp[111]); 
  FFS2_0(w[72], w[49], w[3], pars->GC_74, amp[112]); 
  FFS2_0(w[76], w[53], w[3], pars->GC_74, amp[113]); 
  FFV1_0(w[1], w[79], w[80], pars->GC_11, amp[114]); 
  FFV1_0(w[1], w[81], w[82], pars->GC_11, amp[115]); 
  FFV1_0(w[83], w[6], w[82], pars->GC_11, amp[116]); 
  FFV1_0(w[83], w[5], w[80], pars->GC_11, amp[117]); 
  FFV1_0(w[84], w[6], w[14], pars->GC_11, amp[118]); 
  FFV1_0(w[78], w[6], w[85], pars->GC_11, amp[119]); 
  FFV1_0(w[78], w[81], w[14], pars->GC_11, amp[120]); 
  FFV1_0(w[84], w[5], w[17], pars->GC_11, amp[121]); 
  FFV1_0(w[78], w[79], w[17], pars->GC_11, amp[122]); 
  FFV1_0(w[78], w[5], w[86], pars->GC_11, amp[123]); 
  FFS2_0(w[62], w[81], w[2], pars->GC_74, amp[124]); 
  FFS2_0(w[83], w[63], w[2], pars->GC_74, amp[125]); 
  FFV1_0(w[1], w[27], w[87], pars->GC_11, amp[126]); 
  FFV1_0(w[1], w[88], w[61], pars->GC_11, amp[127]); 
  FFV1_0(w[83], w[27], w[61], pars->GC_11, amp[128]); 
  FFV1_0(w[33], w[6], w[87], pars->GC_11, amp[129]); 
  FFV1_0(w[89], w[6], w[61], pars->GC_11, amp[130]); 
  FFV1_0(w[33], w[81], w[61], pars->GC_11, amp[131]); 
  FFS2_0(w[68], w[79], w[2], pars->GC_74, amp[132]); 
  FFS2_0(w[83], w[69], w[2], pars->GC_74, amp[133]); 
  FFV1_0(w[1], w[19], w[90], pars->GC_11, amp[134]); 
  FFV1_0(w[1], w[91], w[67], pars->GC_11, amp[135]); 
  FFV1_0(w[83], w[19], w[67], pars->GC_11, amp[136]); 
  FFV1_0(w[33], w[5], w[90], pars->GC_11, amp[137]); 
  FFV1_0(w[89], w[5], w[67], pars->GC_11, amp[138]); 
  FFV1_0(w[33], w[79], w[67], pars->GC_11, amp[139]); 
  FFV1_0(w[92], w[6], w[21], pars->GC_11, amp[140]); 
  FFV1_0(w[1], w[81], w[93], pars->GC_11, amp[141]); 
  FFV1_0(w[0], w[81], w[21], pars->GC_11, amp[142]); 
  FFV1_0(w[83], w[6], w[93], pars->GC_11, amp[143]); 
  FFV1_0(w[92], w[19], w[17], pars->GC_11, amp[144]); 
  FFV1_0(w[0], w[91], w[17], pars->GC_11, amp[145]); 
  FFV1_0(w[0], w[19], w[86], pars->GC_11, amp[146]); 
  FFV1_0(w[92], w[5], w[28], pars->GC_11, amp[147]); 
  FFV1_0(w[1], w[79], w[94], pars->GC_11, amp[148]); 
  FFV1_0(w[0], w[79], w[28], pars->GC_11, amp[149]); 
  FFV1_0(w[83], w[5], w[94], pars->GC_11, amp[150]); 
  FFV1_0(w[92], w[27], w[14], pars->GC_11, amp[151]); 
  FFV1_0(w[0], w[88], w[14], pars->GC_11, amp[152]); 
  FFV1_0(w[0], w[27], w[85], pars->GC_11, amp[153]); 
  FFV1_0(w[92], w[6], w[34], pars->GC_11, amp[154]); 
  FFV1_0(w[92], w[5], w[35], pars->GC_11, amp[155]); 
  FFV1_0(w[0], w[79], w[35], pars->GC_11, amp[156]); 
  FFV1_0(w[0], w[81], w[34], pars->GC_11, amp[157]); 
  FFS2_0(w[92], w[46], w[2], pars->GC_74, amp[158]); 
  FFS2_0(w[75], w[81], w[2], pars->GC_74, amp[159]); 
  FFS2_0(w[92], w[49], w[2], pars->GC_74, amp[160]); 
  FFS2_0(w[76], w[79], w[2], pars->GC_74, amp[161]); 
  FFV1_0(w[1], w[96], w[80], pars->GC_11, amp[162]); 
  FFV1_0(w[98], w[6], w[97], pars->GC_11, amp[163]); 
  VVS3_0(w[97], w[80], w[3], pars->GC_13, amp[164]); 
  FFV1_0(w[1], w[23], w[99], pars->GC_11, amp[165]); 
  FFV1_0(w[78], w[23], w[97], pars->GC_11, amp[166]); 
  FFV1_0(w[26], w[6], w[99], pars->GC_11, amp[167]); 
  FFV1_0(w[78], w[6], w[100], pars->GC_11, amp[168]); 
  VVS3_0(w[17], w[99], w[3], pars->GC_13, amp[169]); 
  FFV1_0(w[78], w[96], w[17], pars->GC_11, amp[170]); 
  FFS2_0(w[78], w[101], w[3], pars->GC_74, amp[171]); 
  FFV1_0(w[1], w[102], w[54], pars->GC_11, amp[172]); 
  FFV1_0(w[103], w[6], w[97], pars->GC_11, amp[173]); 
  VVS3_0(w[97], w[54], w[2], pars->GC_13, amp[174]); 
  FFV1_0(w[1], w[27], w[104], pars->GC_11, amp[175]); 
  FFV1_0(w[52], w[27], w[97], pars->GC_11, amp[176]); 
  FFV1_0(w[33], w[6], w[104], pars->GC_11, amp[177]); 
  FFV1_0(w[52], w[6], w[105], pars->GC_11, amp[178]); 
  VVS3_0(w[17], w[104], w[2], pars->GC_13, amp[179]); 
  FFV1_0(w[52], w[102], w[17], pars->GC_11, amp[180]); 
  FFS2_0(w[52], w[101], w[2], pars->GC_74, amp[181]); 
  FFV1_0(w[1], w[102], w[106], pars->GC_11, amp[182]); 
  FFS2_0(w[68], w[102], w[3], pars->GC_74, amp[183]); 
  FFV1_0(w[1], w[96], w[107], pars->GC_11, amp[184]); 
  FFS2_0(w[68], w[96], w[2], pars->GC_74, amp[185]); 
  FFS2_0(w[1], w[108], w[8], pars->GC_74, amp[186]); 
  FFV1_0(w[1], w[109], w[67], pars->GC_11, amp[187]); 
  VVS3_0(w[67], w[97], w[8], pars->GC_13, amp[188]); 
  FFS2_0(w[33], w[108], w[3], pars->GC_74, amp[189]); 
  VVS3_0(w[67], w[105], w[3], pars->GC_13, amp[190]); 
  FFV1_0(w[33], w[96], w[67], pars->GC_11, amp[191]); 
  FFS2_0(w[26], w[108], w[2], pars->GC_74, amp[192]); 
  FFV1_0(w[26], w[102], w[67], pars->GC_11, amp[193]); 
  VVS3_0(w[67], w[100], w[2], pars->GC_13, amp[194]); 
  FFV1_0(w[1], w[10], w[110], pars->GC_11, amp[195]); 
  FFV1_0(w[11], w[6], w[110], pars->GC_11, amp[196]); 
  FFV1_0(w[111], w[6], w[97], pars->GC_11, amp[197]); 
  FFV1_0(w[0], w[10], w[97], pars->GC_11, amp[198]); 
  VVS3_0(w[17], w[110], w[8], pars->GC_13, amp[199]); 
  FFV1_0(w[0], w[109], w[17], pars->GC_11, amp[200]); 
  FFS2_0(w[0], w[101], w[8], pars->GC_74, amp[201]); 
  FFV1_0(w[1], w[29], w[110], pars->GC_11, amp[202]); 
  VVS3_0(w[110], w[28], w[3], pars->GC_13, amp[203]); 
  FFV1_0(w[1], w[96], w[94], pars->GC_11, amp[204]); 
  FFV1_0(w[0], w[96], w[28], pars->GC_11, amp[205]); 
  VVS3_0(w[97], w[94], w[3], pars->GC_13, amp[206]); 
  FFV1_0(w[0], w[29], w[97], pars->GC_11, amp[207]); 
  FFV1_0(w[26], w[27], w[110], pars->GC_11, amp[208]); 
  FFV1_0(w[0], w[27], w[100], pars->GC_11, amp[209]); 
  FFV1_0(w[36], w[6], w[110], pars->GC_11, amp[210]); 
  VVS3_0(w[110], w[35], w[3], pars->GC_13, amp[211]); 
  FFV1_0(w[0], w[96], w[35], pars->GC_11, amp[212]); 
  FFV1_0(w[33], w[23], w[110], pars->GC_11, amp[213]); 
  FFV1_0(w[0], w[23], w[105], pars->GC_11, amp[214]); 
  FFV1_0(w[1], w[41], w[110], pars->GC_11, amp[215]); 
  VVS3_0(w[110], w[40], w[2], pars->GC_13, amp[216]); 
  FFV1_0(w[1], w[102], w[74], pars->GC_11, amp[217]); 
  FFV1_0(w[0], w[102], w[40], pars->GC_11, amp[218]); 
  VVS3_0(w[97], w[74], w[2], pars->GC_13, amp[219]); 
  FFV1_0(w[0], w[41], w[97], pars->GC_11, amp[220]); 
  FFV1_0(w[44], w[6], w[110], pars->GC_11, amp[221]); 
  VVS3_0(w[110], w[43], w[2], pars->GC_13, amp[222]); 
  FFV1_0(w[0], w[102], w[43], pars->GC_11, amp[223]); 
  FFS2_0(w[76], w[102], w[3], pars->GC_74, amp[224]); 
  FFV1_0(w[0], w[102], w[48], pars->GC_11, amp[225]); 
  FFS2_0(w[76], w[96], w[2], pars->GC_74, amp[226]); 
  FFV1_0(w[0], w[96], w[50], pars->GC_11, amp[227]); 
  FFV1_0(w[1], w[113], w[82], pars->GC_11, amp[228]); 
  FFV1_0(w[98], w[5], w[114], pars->GC_11, amp[229]); 
  VVS3_0(w[114], w[82], w[3], pars->GC_13, amp[230]); 
  FFV1_0(w[1], w[30], w[115], pars->GC_11, amp[231]); 
  FFV1_0(w[78], w[30], w[114], pars->GC_11, amp[232]); 
  FFV1_0(w[26], w[5], w[115], pars->GC_11, amp[233]); 
  FFV1_0(w[78], w[5], w[116], pars->GC_11, amp[234]); 
  VVS3_0(w[14], w[115], w[3], pars->GC_13, amp[235]); 
  FFV1_0(w[78], w[113], w[14], pars->GC_11, amp[236]); 
  FFS2_0(w[78], w[117], w[3], pars->GC_74, amp[237]); 
  FFV1_0(w[1], w[118], w[56], pars->GC_11, amp[238]); 
  FFV1_0(w[103], w[5], w[114], pars->GC_11, amp[239]); 
  VVS3_0(w[114], w[56], w[2], pars->GC_13, amp[240]); 
  FFV1_0(w[1], w[19], w[119], pars->GC_11, amp[241]); 
  FFV1_0(w[52], w[19], w[114], pars->GC_11, amp[242]); 
  FFV1_0(w[33], w[5], w[119], pars->GC_11, amp[243]); 
  FFV1_0(w[52], w[5], w[120], pars->GC_11, amp[244]); 
  VVS3_0(w[14], w[119], w[2], pars->GC_13, amp[245]); 
  FFV1_0(w[52], w[118], w[14], pars->GC_11, amp[246]); 
  FFS2_0(w[52], w[117], w[2], pars->GC_74, amp[247]); 
  FFV1_0(w[1], w[118], w[121], pars->GC_11, amp[248]); 
  FFS2_0(w[62], w[118], w[3], pars->GC_74, amp[249]); 
  FFV1_0(w[1], w[113], w[122], pars->GC_11, amp[250]); 
  FFS2_0(w[62], w[113], w[2], pars->GC_74, amp[251]); 
  FFS2_0(w[1], w[123], w[8], pars->GC_74, amp[252]); 
  FFV1_0(w[1], w[124], w[61], pars->GC_11, amp[253]); 
  VVS3_0(w[61], w[114], w[8], pars->GC_13, amp[254]); 
  FFS2_0(w[33], w[123], w[3], pars->GC_74, amp[255]); 
  VVS3_0(w[61], w[120], w[3], pars->GC_13, amp[256]); 
  FFV1_0(w[33], w[113], w[61], pars->GC_11, amp[257]); 
  FFS2_0(w[26], w[123], w[2], pars->GC_74, amp[258]); 
  FFV1_0(w[26], w[118], w[61], pars->GC_11, amp[259]); 
  VVS3_0(w[61], w[116], w[2], pars->GC_13, amp[260]); 
  FFV1_0(w[1], w[13], w[125], pars->GC_11, amp[261]); 
  FFV1_0(w[11], w[5], w[125], pars->GC_11, amp[262]); 
  FFV1_0(w[111], w[5], w[114], pars->GC_11, amp[263]); 
  FFV1_0(w[0], w[13], w[114], pars->GC_11, amp[264]); 
  VVS3_0(w[14], w[125], w[8], pars->GC_13, amp[265]); 
  FFV1_0(w[0], w[124], w[14], pars->GC_11, amp[266]); 
  FFS2_0(w[0], w[117], w[8], pars->GC_74, amp[267]); 
  FFV1_0(w[1], w[22], w[125], pars->GC_11, amp[268]); 
  VVS3_0(w[125], w[21], w[3], pars->GC_13, amp[269]); 
  FFV1_0(w[1], w[113], w[93], pars->GC_11, amp[270]); 
  FFV1_0(w[0], w[113], w[21], pars->GC_11, amp[271]); 
  VVS3_0(w[114], w[93], w[3], pars->GC_13, amp[272]); 
  FFV1_0(w[0], w[22], w[114], pars->GC_11, amp[273]); 
  FFV1_0(w[26], w[19], w[125], pars->GC_11, amp[274]); 
  FFV1_0(w[0], w[19], w[116], pars->GC_11, amp[275]); 
  FFV1_0(w[36], w[5], w[125], pars->GC_11, amp[276]); 
  VVS3_0(w[125], w[34], w[3], pars->GC_13, amp[277]); 
  FFV1_0(w[0], w[113], w[34], pars->GC_11, amp[278]); 
  FFV1_0(w[33], w[30], w[125], pars->GC_11, amp[279]); 
  FFV1_0(w[0], w[30], w[120], pars->GC_11, amp[280]); 
  FFV1_0(w[1], w[39], w[125], pars->GC_11, amp[281]); 
  VVS3_0(w[125], w[38], w[2], pars->GC_13, amp[282]); 
  FFV1_0(w[1], w[118], w[73], pars->GC_11, amp[283]); 
  FFV1_0(w[0], w[118], w[38], pars->GC_11, amp[284]); 
  VVS3_0(w[114], w[73], w[2], pars->GC_13, amp[285]); 
  FFV1_0(w[0], w[39], w[114], pars->GC_11, amp[286]); 
  FFV1_0(w[44], w[5], w[125], pars->GC_11, amp[287]); 
  VVS3_0(w[125], w[42], w[2], pars->GC_13, amp[288]); 
  FFV1_0(w[0], w[118], w[42], pars->GC_11, amp[289]); 
  FFS2_0(w[75], w[118], w[3], pars->GC_74, amp[290]); 
  FFV1_0(w[0], w[118], w[45], pars->GC_11, amp[291]); 
  FFS2_0(w[75], w[113], w[2], pars->GC_74, amp[292]); 
  FFV1_0(w[0], w[113], w[47], pars->GC_11, amp[293]); 
  FFV1_0(w[127], w[6], w[82], pars->GC_11, amp[294]); 
  FFV1_0(w[127], w[5], w[80], pars->GC_11, amp[295]); 
  FFV1_0(w[98], w[6], w[128], pars->GC_11, amp[296]); 
  VVS3_0(w[128], w[80], w[3], pars->GC_13, amp[297]); 
  FFV1_0(w[98], w[5], w[129], pars->GC_11, amp[298]); 
  VVS3_0(w[129], w[82], w[3], pars->GC_13, amp[299]); 
  FFV1_0(w[78], w[6], w[130], pars->GC_11, amp[300]); 
  FFV1_0(w[78], w[30], w[129], pars->GC_11, amp[301]); 
  FFV1_0(w[78], w[5], w[131], pars->GC_11, amp[302]); 
  FFV1_0(w[78], w[23], w[128], pars->GC_11, amp[303]); 
  FFV1_0(w[132], w[6], w[56], pars->GC_11, amp[304]); 
  FFV1_0(w[132], w[5], w[54], pars->GC_11, amp[305]); 
  FFV1_0(w[103], w[6], w[128], pars->GC_11, amp[306]); 
  VVS3_0(w[128], w[54], w[2], pars->GC_13, amp[307]); 
  FFV1_0(w[103], w[5], w[129], pars->GC_11, amp[308]); 
  VVS3_0(w[129], w[56], w[2], pars->GC_13, amp[309]); 
  FFV1_0(w[52], w[6], w[133], pars->GC_11, amp[310]); 
  FFV1_0(w[52], w[19], w[129], pars->GC_11, amp[311]); 
  FFV1_0(w[52], w[5], w[134], pars->GC_11, amp[312]); 
  FFV1_0(w[52], w[27], w[128], pars->GC_11, amp[313]); 
  FFV1_0(w[132], w[6], w[121], pars->GC_11, amp[314]); 
  FFS2_0(w[132], w[63], w[3], pars->GC_74, amp[315]); 
  FFV1_0(w[127], w[6], w[122], pars->GC_11, amp[316]); 
  FFS2_0(w[127], w[63], w[2], pars->GC_74, amp[317]); 
  FFS2_0(w[135], w[6], w[8], pars->GC_74, amp[318]); 
  FFV1_0(w[136], w[6], w[61], pars->GC_11, amp[319]); 
  VVS3_0(w[61], w[129], w[8], pars->GC_13, amp[320]); 
  FFS2_0(w[135], w[27], w[3], pars->GC_74, amp[321]); 
  VVS3_0(w[61], w[134], w[3], pars->GC_13, amp[322]); 
  FFV1_0(w[127], w[27], w[61], pars->GC_11, amp[323]); 
  FFS2_0(w[135], w[23], w[2], pars->GC_74, amp[324]); 
  FFV1_0(w[132], w[23], w[61], pars->GC_11, amp[325]); 
  VVS3_0(w[61], w[131], w[2], pars->GC_13, amp[326]); 
  FFV1_0(w[132], w[5], w[106], pars->GC_11, amp[327]); 
  FFS2_0(w[132], w[69], w[3], pars->GC_74, amp[328]); 
  FFV1_0(w[127], w[5], w[107], pars->GC_11, amp[329]); 
  FFS2_0(w[127], w[69], w[2], pars->GC_74, amp[330]); 
  FFS2_0(w[137], w[5], w[8], pars->GC_74, amp[331]); 
  FFV1_0(w[136], w[5], w[67], pars->GC_11, amp[332]); 
  VVS3_0(w[67], w[128], w[8], pars->GC_13, amp[333]); 
  FFS2_0(w[137], w[19], w[3], pars->GC_74, amp[334]); 
  VVS3_0(w[67], w[133], w[3], pars->GC_13, amp[335]); 
  FFV1_0(w[127], w[19], w[67], pars->GC_11, amp[336]); 
  FFS2_0(w[137], w[30], w[2], pars->GC_74, amp[337]); 
  FFV1_0(w[132], w[30], w[67], pars->GC_11, amp[338]); 
  VVS3_0(w[67], w[130], w[2], pars->GC_13, amp[339]); 
  FFV1_0(w[111], w[6], w[128], pars->GC_11, amp[340]); 
  FFV1_0(w[0], w[10], w[128], pars->GC_11, amp[341]); 
  FFV1_0(w[111], w[5], w[129], pars->GC_11, amp[342]); 
  FFV1_0(w[0], w[13], w[129], pars->GC_11, amp[343]); 
  FFV1_0(w[127], w[6], w[93], pars->GC_11, amp[344]); 
  VVS3_0(w[129], w[93], w[3], pars->GC_13, amp[345]); 
  FFV1_0(w[0], w[22], w[129], pars->GC_11, amp[346]); 
  FFV1_0(w[0], w[23], w[133], pars->GC_11, amp[347]); 
  FFV1_0(w[0], w[19], w[131], pars->GC_11, amp[348]); 
  FFV1_0(w[127], w[5], w[94], pars->GC_11, amp[349]); 
  VVS3_0(w[128], w[94], w[3], pars->GC_13, amp[350]); 
  FFV1_0(w[0], w[29], w[128], pars->GC_11, amp[351]); 
  FFV1_0(w[0], w[30], w[134], pars->GC_11, amp[352]); 
  FFV1_0(w[0], w[27], w[130], pars->GC_11, amp[353]); 
  FFV1_0(w[132], w[6], w[73], pars->GC_11, amp[354]); 
  VVS3_0(w[129], w[73], w[2], pars->GC_13, amp[355]); 
  FFV1_0(w[0], w[39], w[129], pars->GC_11, amp[356]); 
  FFV1_0(w[132], w[5], w[74], pars->GC_11, amp[357]); 
  VVS3_0(w[128], w[74], w[2], pars->GC_13, amp[358]); 
  FFV1_0(w[0], w[41], w[128], pars->GC_11, amp[359]); 
  FFV1_0(w[138], w[6], w[38], pars->GC_11, amp[360]); 
  FFV1_0(w[1], w[139], w[80], pars->GC_11, amp[361]); 
  VVV1_0(w[4], w[80], w[38], pars->GC_10, amp[362]); 
  FFV1_0(w[138], w[30], w[17], pars->GC_11, amp[363]); 
  FFV1_0(w[78], w[139], w[17], pars->GC_11, amp[364]); 
  FFV1_0(w[78], w[30], w[140], pars->GC_11, amp[365]); 
  FFV1_0(w[138], w[5], w[40], pars->GC_11, amp[366]); 
  FFV1_0(w[1], w[141], w[82], pars->GC_11, amp[367]); 
  VVV1_0(w[4], w[82], w[40], pars->GC_10, amp[368]); 
  FFV1_0(w[138], w[23], w[14], pars->GC_11, amp[369]); 
  FFV1_0(w[78], w[141], w[14], pars->GC_11, amp[370]); 
  FFV1_0(w[78], w[23], w[142], pars->GC_11, amp[371]); 
  FFV1_0(w[138], w[6], w[42], pars->GC_11, amp[372]); 
  FFV1_0(w[138], w[5], w[43], pars->GC_11, amp[373]); 
  FFV1_0(w[143], w[6], w[82], pars->GC_11, amp[374]); 
  FFV1_0(w[143], w[5], w[80], pars->GC_11, amp[375]); 
  VVV1_0(w[4], w[82], w[43], pars->GC_10, amp[376]); 
  VVV1_0(w[4], w[80], w[42], pars->GC_10, amp[377]); 
  FFV1_0(w[138], w[6], w[45], pars->GC_11, amp[378]); 
  FFS2_0(w[138], w[46], w[3], pars->GC_74, amp[379]); 
  FFV1_0(w[98], w[6], w[142], pars->GC_11, amp[380]); 
  VVS3_0(w[142], w[80], w[3], pars->GC_13, amp[381]); 
  FFV1_0(w[98], w[46], w[4], pars->GC_11, amp[382]); 
  VVV1_0(w[4], w[80], w[45], pars->GC_10, amp[383]); 
  FFV1_0(w[78], w[6], w[144], pars->GC_11, amp[384]); 
  FFV1_0(w[138], w[5], w[48], pars->GC_11, amp[385]); 
  FFS2_0(w[138], w[49], w[3], pars->GC_74, amp[386]); 
  FFV1_0(w[98], w[5], w[140], pars->GC_11, amp[387]); 
  VVS3_0(w[140], w[82], w[3], pars->GC_13, amp[388]); 
  FFV1_0(w[98], w[49], w[4], pars->GC_11, amp[389]); 
  VVV1_0(w[4], w[82], w[48], pars->GC_10, amp[390]); 
  FFV1_0(w[78], w[5], w[145], pars->GC_11, amp[391]); 
  FFV1_0(w[146], w[6], w[21], pars->GC_11, amp[392]); 
  FFV1_0(w[1], w[147], w[54], pars->GC_11, amp[393]); 
  VVV1_0(w[4], w[54], w[21], pars->GC_10, amp[394]); 
  FFV1_0(w[146], w[19], w[17], pars->GC_11, amp[395]); 
  FFV1_0(w[52], w[147], w[17], pars->GC_11, amp[396]); 
  FFV1_0(w[52], w[19], w[140], pars->GC_11, amp[397]); 
  FFV1_0(w[146], w[5], w[28], pars->GC_11, amp[398]); 
  FFV1_0(w[1], w[148], w[56], pars->GC_11, amp[399]); 
  VVV1_0(w[4], w[56], w[28], pars->GC_10, amp[400]); 
  FFV1_0(w[146], w[27], w[14], pars->GC_11, amp[401]); 
  FFV1_0(w[52], w[148], w[14], pars->GC_11, amp[402]); 
  FFV1_0(w[52], w[27], w[142], pars->GC_11, amp[403]); 
  FFV1_0(w[146], w[6], w[34], pars->GC_11, amp[404]); 
  FFV1_0(w[146], w[5], w[35], pars->GC_11, amp[405]); 
  FFV1_0(w[149], w[6], w[56], pars->GC_11, amp[406]); 
  FFV1_0(w[149], w[5], w[54], pars->GC_11, amp[407]); 
  VVV1_0(w[4], w[56], w[35], pars->GC_10, amp[408]); 
  VVV1_0(w[4], w[54], w[34], pars->GC_10, amp[409]); 
  FFV1_0(w[146], w[6], w[47], pars->GC_11, amp[410]); 
  FFS2_0(w[146], w[46], w[2], pars->GC_74, amp[411]); 
  FFV1_0(w[103], w[6], w[142], pars->GC_11, amp[412]); 
  VVS3_0(w[142], w[54], w[2], pars->GC_13, amp[413]); 
  FFV1_0(w[103], w[46], w[4], pars->GC_11, amp[414]); 
  VVV1_0(w[4], w[54], w[47], pars->GC_10, amp[415]); 
  FFV1_0(w[52], w[6], w[150], pars->GC_11, amp[416]); 
  FFV1_0(w[146], w[5], w[50], pars->GC_11, amp[417]); 
  FFS2_0(w[146], w[49], w[2], pars->GC_74, amp[418]); 
  FFV1_0(w[103], w[5], w[140], pars->GC_11, amp[419]); 
  VVS3_0(w[140], w[56], w[2], pars->GC_13, amp[420]); 
  FFV1_0(w[103], w[49], w[4], pars->GC_11, amp[421]); 
  VVV1_0(w[4], w[56], w[50], pars->GC_10, amp[422]); 
  FFV1_0(w[52], w[5], w[151], pars->GC_11, amp[423]); 
  FFV1_0(w[1], w[10], w[152], pars->GC_11, amp[424]); 
  FFV1_0(w[11], w[6], w[152], pars->GC_11, amp[425]); 
  FFV1_0(w[1], w[63], w[153], pars->GC_11, amp[426]); 
  FFV1_0(w[62], w[6], w[153], pars->GC_11, amp[427]); 
  FFV1_0(w[11], w[63], w[4], pars->GC_11, amp[428]); 
  FFV1_0(w[62], w[10], w[4], pars->GC_11, amp[429]); 
  VVVS2_0(w[4], w[61], w[17], w[8], pars->GC_14, amp[430]); 
  VVS3_0(w[17], w[152], w[8], pars->GC_13, amp[431]); 
  VVV1_0(w[61], w[17], w[153], pars->GC_10, amp[432]); 
  VVS3_0(w[61], w[140], w[8], pars->GC_13, amp[433]); 
  FFV1_0(w[1], w[29], w[152], pars->GC_11, amp[434]); 
  VVS3_0(w[152], w[28], w[3], pars->GC_13, amp[435]); 
  FFV1_0(w[1], w[148], w[121], pars->GC_11, amp[436]); 
  FFS2_0(w[62], w[148], w[3], pars->GC_74, amp[437]); 
  VVV1_0(w[4], w[121], w[28], pars->GC_10, amp[438]); 
  FFV1_0(w[62], w[29], w[4], pars->GC_11, amp[439]); 
  FFV1_0(w[1], w[27], w[154], pars->GC_11, amp[440]); 
  FFV1_0(w[26], w[27], w[152], pars->GC_11, amp[441]); 
  FFV1_0(w[26], w[148], w[61], pars->GC_11, amp[442]); 
  FFV1_0(w[143], w[27], w[61], pars->GC_11, amp[443]); 
  FFV1_0(w[36], w[6], w[152], pars->GC_11, amp[444]); 
  VVS3_0(w[152], w[35], w[3], pars->GC_13, amp[445]); 
  FFV1_0(w[149], w[6], w[121], pars->GC_11, amp[446]); 
  FFS2_0(w[149], w[63], w[3], pars->GC_74, amp[447]); 
  VVV1_0(w[4], w[121], w[35], pars->GC_10, amp[448]); 
  FFV1_0(w[36], w[63], w[4], pars->GC_11, amp[449]); 
  FFV1_0(w[33], w[6], w[154], pars->GC_11, amp[450]); 
  FFV1_0(w[33], w[23], w[152], pars->GC_11, amp[451]); 
  FFV1_0(w[149], w[23], w[61], pars->GC_11, amp[452]); 
  FFV1_0(w[33], w[141], w[61], pars->GC_11, amp[453]); 
  FFV1_0(w[1], w[41], w[152], pars->GC_11, amp[454]); 
  VVS3_0(w[152], w[40], w[2], pars->GC_13, amp[455]); 
  FFV1_0(w[1], w[141], w[122], pars->GC_11, amp[456]); 
  FFS2_0(w[62], w[141], w[2], pars->GC_74, amp[457]); 
  VVV1_0(w[4], w[122], w[40], pars->GC_10, amp[458]); 
  FFV1_0(w[62], w[41], w[4], pars->GC_11, amp[459]); 
  FFV1_0(w[1], w[23], w[155], pars->GC_11, amp[460]); 
  FFV1_0(w[44], w[6], w[152], pars->GC_11, amp[461]); 
  VVS3_0(w[152], w[43], w[2], pars->GC_13, amp[462]); 
  FFV1_0(w[143], w[6], w[122], pars->GC_11, amp[463]); 
  FFS2_0(w[143], w[63], w[2], pars->GC_74, amp[464]); 
  VVV1_0(w[4], w[122], w[43], pars->GC_10, amp[465]); 
  FFV1_0(w[44], w[63], w[4], pars->GC_11, amp[466]); 
  FFV1_0(w[26], w[6], w[155], pars->GC_11, amp[467]); 
  FFV1_0(w[1], w[13], w[156], pars->GC_11, amp[468]); 
  FFV1_0(w[11], w[5], w[156], pars->GC_11, amp[469]); 
  FFV1_0(w[1], w[69], w[153], pars->GC_11, amp[470]); 
  FFV1_0(w[68], w[5], w[153], pars->GC_11, amp[471]); 
  FFV1_0(w[11], w[69], w[4], pars->GC_11, amp[472]); 
  FFV1_0(w[68], w[13], w[4], pars->GC_11, amp[473]); 
  VVVS2_0(w[4], w[67], w[14], w[8], pars->GC_14, amp[474]); 
  VVS3_0(w[14], w[156], w[8], pars->GC_13, amp[475]); 
  VVV1_0(w[67], w[14], w[153], pars->GC_10, amp[476]); 
  VVS3_0(w[67], w[142], w[8], pars->GC_13, amp[477]); 
  FFV1_0(w[1], w[22], w[156], pars->GC_11, amp[478]); 
  VVS3_0(w[156], w[21], w[3], pars->GC_13, amp[479]); 
  FFV1_0(w[1], w[147], w[106], pars->GC_11, amp[480]); 
  FFS2_0(w[68], w[147], w[3], pars->GC_74, amp[481]); 
  VVV1_0(w[4], w[106], w[21], pars->GC_10, amp[482]); 
  FFV1_0(w[68], w[22], w[4], pars->GC_11, amp[483]); 
  FFV1_0(w[1], w[19], w[157], pars->GC_11, amp[484]); 
  FFV1_0(w[26], w[19], w[156], pars->GC_11, amp[485]); 
  FFV1_0(w[26], w[147], w[67], pars->GC_11, amp[486]); 
  FFV1_0(w[143], w[19], w[67], pars->GC_11, amp[487]); 
  FFV1_0(w[36], w[5], w[156], pars->GC_11, amp[488]); 
  VVS3_0(w[156], w[34], w[3], pars->GC_13, amp[489]); 
  FFV1_0(w[149], w[5], w[106], pars->GC_11, amp[490]); 
  FFS2_0(w[149], w[69], w[3], pars->GC_74, amp[491]); 
  VVV1_0(w[4], w[106], w[34], pars->GC_10, amp[492]); 
  FFV1_0(w[36], w[69], w[4], pars->GC_11, amp[493]); 
  FFV1_0(w[33], w[5], w[157], pars->GC_11, amp[494]); 
  FFV1_0(w[33], w[30], w[156], pars->GC_11, amp[495]); 
  FFV1_0(w[149], w[30], w[67], pars->GC_11, amp[496]); 
  FFV1_0(w[33], w[139], w[67], pars->GC_11, amp[497]); 
  FFV1_0(w[1], w[39], w[156], pars->GC_11, amp[498]); 
  VVS3_0(w[156], w[38], w[2], pars->GC_13, amp[499]); 
  FFV1_0(w[1], w[139], w[107], pars->GC_11, amp[500]); 
  FFS2_0(w[68], w[139], w[2], pars->GC_74, amp[501]); 
  VVV1_0(w[4], w[107], w[38], pars->GC_10, amp[502]); 
  FFV1_0(w[68], w[39], w[4], pars->GC_11, amp[503]); 
  FFV1_0(w[1], w[30], w[158], pars->GC_11, amp[504]); 
  FFV1_0(w[44], w[5], w[156], pars->GC_11, amp[505]); 
  VVS3_0(w[156], w[42], w[2], pars->GC_13, amp[506]); 
  FFV1_0(w[143], w[5], w[107], pars->GC_11, amp[507]); 
  FFS2_0(w[143], w[69], w[2], pars->GC_74, amp[508]); 
  VVV1_0(w[4], w[107], w[42], pars->GC_10, amp[509]); 
  FFV1_0(w[44], w[69], w[4], pars->GC_11, amp[510]); 
  FFV1_0(w[26], w[5], w[158], pars->GC_11, amp[511]); 
  FFV1_0(w[75], w[6], w[153], pars->GC_11, amp[512]); 
  FFV1_0(w[0], w[46], w[153], pars->GC_11, amp[513]); 
  FFV1_0(w[111], w[6], w[142], pars->GC_11, amp[514]); 
  FFV1_0(w[0], w[10], w[142], pars->GC_11, amp[515]); 
  FFV1_0(w[111], w[46], w[4], pars->GC_11, amp[516]); 
  FFV1_0(w[75], w[10], w[4], pars->GC_11, amp[517]); 
  FFV1_0(w[76], w[5], w[153], pars->GC_11, amp[518]); 
  FFV1_0(w[0], w[49], w[153], pars->GC_11, amp[519]); 
  FFV1_0(w[111], w[5], w[140], pars->GC_11, amp[520]); 
  FFV1_0(w[0], w[13], w[140], pars->GC_11, amp[521]); 
  FFV1_0(w[111], w[49], w[4], pars->GC_11, amp[522]); 
  FFV1_0(w[76], w[13], w[4], pars->GC_11, amp[523]); 
  FFV1_0(w[1], w[147], w[74], pars->GC_11, amp[524]); 
  FFV1_0(w[0], w[147], w[40], pars->GC_11, amp[525]); 
  FFV1_0(w[1], w[141], w[93], pars->GC_11, amp[526]); 
  FFV1_0(w[0], w[141], w[21], pars->GC_11, amp[527]); 
  VVV1_0(w[4], w[93], w[40], pars->GC_10, amp[528]); 
  VVV1_0(w[4], w[74], w[21], pars->GC_10, amp[529]); 
  FFV1_0(w[0], w[147], w[43], pars->GC_11, amp[530]); 
  FFV1_0(w[143], w[6], w[93], pars->GC_11, amp[531]); 
  VVV1_0(w[4], w[93], w[43], pars->GC_10, amp[532]); 
  FFS2_0(w[76], w[147], w[3], pars->GC_74, amp[533]); 
  FFV1_0(w[0], w[147], w[48], pars->GC_11, amp[534]); 
  VVS3_0(w[140], w[93], w[3], pars->GC_13, amp[535]); 
  FFV1_0(w[0], w[22], w[140], pars->GC_11, amp[536]); 
  VVV1_0(w[4], w[93], w[48], pars->GC_10, amp[537]); 
  FFV1_0(w[76], w[22], w[4], pars->GC_11, amp[538]); 
  FFV1_0(w[0], w[19], w[145], pars->GC_11, amp[539]); 
  FFV1_0(w[1], w[148], w[73], pars->GC_11, amp[540]); 
  FFV1_0(w[0], w[148], w[38], pars->GC_11, amp[541]); 
  FFV1_0(w[1], w[139], w[94], pars->GC_11, amp[542]); 
  FFV1_0(w[0], w[139], w[28], pars->GC_11, amp[543]); 
  VVV1_0(w[4], w[94], w[38], pars->GC_10, amp[544]); 
  VVV1_0(w[4], w[73], w[28], pars->GC_10, amp[545]); 
  FFV1_0(w[0], w[148], w[42], pars->GC_11, amp[546]); 
  FFV1_0(w[143], w[5], w[94], pars->GC_11, amp[547]); 
  VVV1_0(w[4], w[94], w[42], pars->GC_10, amp[548]); 
  FFS2_0(w[75], w[148], w[3], pars->GC_74, amp[549]); 
  FFV1_0(w[0], w[148], w[45], pars->GC_11, amp[550]); 
  VVS3_0(w[142], w[94], w[3], pars->GC_13, amp[551]); 
  FFV1_0(w[0], w[29], w[142], pars->GC_11, amp[552]); 
  VVV1_0(w[4], w[94], w[45], pars->GC_10, amp[553]); 
  FFV1_0(w[75], w[29], w[4], pars->GC_11, amp[554]); 
  FFV1_0(w[0], w[27], w[144], pars->GC_11, amp[555]); 
  FFV1_0(w[149], w[6], w[73], pars->GC_11, amp[556]); 
  FFV1_0(w[0], w[139], w[35], pars->GC_11, amp[557]); 
  VVV1_0(w[4], w[73], w[35], pars->GC_10, amp[558]); 
  FFV1_0(w[149], w[5], w[74], pars->GC_11, amp[559]); 
  FFV1_0(w[0], w[141], w[34], pars->GC_11, amp[560]); 
  VVV1_0(w[4], w[74], w[34], pars->GC_10, amp[561]); 
  FFS2_0(w[76], w[139], w[2], pars->GC_74, amp[562]); 
  FFV1_0(w[0], w[139], w[50], pars->GC_11, amp[563]); 
  VVS3_0(w[140], w[73], w[2], pars->GC_13, amp[564]); 
  FFV1_0(w[0], w[39], w[140], pars->GC_11, amp[565]); 
  VVV1_0(w[4], w[73], w[50], pars->GC_10, amp[566]); 
  FFV1_0(w[76], w[39], w[4], pars->GC_11, amp[567]); 
  FFV1_0(w[0], w[30], w[151], pars->GC_11, amp[568]); 
  FFS2_0(w[75], w[141], w[2], pars->GC_74, amp[569]); 
  FFV1_0(w[0], w[141], w[47], pars->GC_11, amp[570]); 
  VVS3_0(w[142], w[74], w[2], pars->GC_13, amp[571]); 
  FFV1_0(w[0], w[41], w[142], pars->GC_11, amp[572]); 
  VVV1_0(w[4], w[74], w[47], pars->GC_10, amp[573]); 
  FFV1_0(w[75], w[41], w[4], pars->GC_11, amp[574]); 
  FFV1_0(w[0], w[23], w[150], pars->GC_11, amp[575]); 
  FFV1_0(w[160], w[13], w[161], pars->GC_11, amp[576]); 
  FFV1_0(w[162], w[5], w[161], pars->GC_11, amp[577]); 
  FFV1_0(w[160], w[163], w[9], pars->GC_11, amp[578]); 
  FFV1_0(w[162], w[159], w[9], pars->GC_11, amp[579]); 
  FFV1_0(w[15], w[5], w[164], pars->GC_11, amp[580]); 
  FFS2_0(w[165], w[5], w[8], pars->GC_74, amp[581]); 
  VVS3_0(w[164], w[9], w[8], pars->GC_13, amp[582]); 
  FFV1_0(w[15], w[159], w[166], pars->GC_11, amp[583]); 
  VVS3_0(w[166], w[161], w[8], pars->GC_13, amp[584]); 
  FFS2_0(w[167], w[159], w[8], pars->GC_74, amp[585]); 
  FFV1_0(w[20], w[5], w[169], pars->GC_11, amp[586]); 
  FFV1_0(w[160], w[170], w[9], pars->GC_11, amp[587]); 
  VVS3_0(w[9], w[169], w[3], pars->GC_13, amp[588]); 
  FFV1_0(w[160], w[30], w[171], pars->GC_11, amp[589]); 
  FFV1_0(w[160], w[168], w[32], pars->GC_11, amp[590]); 
  FFV1_0(w[172], w[5], w[171], pars->GC_11, amp[591]); 
  FFV1_0(w[172], w[168], w[9], pars->GC_11, amp[592]); 
  VVS3_0(w[166], w[171], w[3], pars->GC_13, amp[593]); 
  FFV1_0(w[20], w[168], w[166], pars->GC_11, amp[594]); 
  FFS2_0(w[167], w[168], w[3], pars->GC_74, amp[595]); 
  FFV1_0(w[20], w[159], w[173], pars->GC_11, amp[596]); 
  FFV1_0(w[160], w[22], w[161], pars->GC_11, amp[597]); 
  VVS3_0(w[161], w[173], w[3], pars->GC_13, amp[598]); 
  FFV1_0(w[160], w[174], w[24], pars->GC_11, amp[599]); 
  FFV1_0(w[160], w[19], w[175], pars->GC_11, amp[600]); 
  FFV1_0(w[172], w[159], w[24], pars->GC_11, amp[601]); 
  FFV1_0(w[172], w[19], w[161], pars->GC_11, amp[602]); 
  VVS3_0(w[164], w[24], w[3], pars->GC_13, amp[603]); 
  FFV1_0(w[20], w[19], w[164], pars->GC_11, amp[604]); 
  FFS2_0(w[165], w[19], w[3], pars->GC_74, amp[605]); 
  FFV1_0(w[20], w[5], w[177], pars->GC_11, amp[606]); 
  FFV1_0(w[20], w[159], w[178], pars->GC_11, amp[607]); 
  FFV1_0(w[179], w[5], w[161], pars->GC_11, amp[608]); 
  VVS3_0(w[161], w[178], w[3], pars->GC_13, amp[609]); 
  FFV1_0(w[179], w[159], w[9], pars->GC_11, amp[610]); 
  VVS3_0(w[9], w[177], w[3], pars->GC_13, amp[611]); 
  FFV1_0(w[176], w[5], w[175], pars->GC_11, amp[612]); 
  FFV1_0(w[176], w[174], w[9], pars->GC_11, amp[613]); 
  FFV1_0(w[176], w[159], w[32], pars->GC_11, amp[614]); 
  FFV1_0(w[176], w[30], w[161], pars->GC_11, amp[615]); 
  FFV1_0(w[37], w[5], w[180], pars->GC_11, amp[616]); 
  FFV1_0(w[160], w[181], w[9], pars->GC_11, amp[617]); 
  VVS3_0(w[9], w[180], w[2], pars->GC_13, amp[618]); 
  FFV1_0(w[37], w[174], w[166], pars->GC_11, amp[619]); 
  VVS3_0(w[166], w[175], w[2], pars->GC_13, amp[620]); 
  FFS2_0(w[167], w[174], w[2], pars->GC_74, amp[621]); 
  FFV1_0(w[37], w[159], w[182], pars->GC_11, amp[622]); 
  FFV1_0(w[160], w[39], w[161], pars->GC_11, amp[623]); 
  VVS3_0(w[161], w[182], w[2], pars->GC_13, amp[624]); 
  FFV1_0(w[37], w[30], w[164], pars->GC_11, amp[625]); 
  VVS3_0(w[164], w[32], w[2], pars->GC_13, amp[626]); 
  FFS2_0(w[165], w[30], w[2], pars->GC_74, amp[627]); 
  FFV1_0(w[37], w[5], w[183], pars->GC_11, amp[628]); 
  FFV1_0(w[37], w[159], w[184], pars->GC_11, amp[629]); 
  FFV1_0(w[185], w[5], w[161], pars->GC_11, amp[630]); 
  VVS3_0(w[161], w[184], w[2], pars->GC_13, amp[631]); 
  FFV1_0(w[185], w[159], w[9], pars->GC_11, amp[632]); 
  VVS3_0(w[9], w[183], w[2], pars->GC_13, amp[633]); 
  FFV1_0(w[37], w[5], w[186], pars->GC_11, amp[634]); 
  FFS2_0(w[37], w[187], w[3], pars->GC_74, amp[635]); 
  FFV1_0(w[20], w[5], w[188], pars->GC_11, amp[636]); 
  FFS2_0(w[20], w[187], w[2], pars->GC_74, amp[637]); 
  FFV1_0(w[37], w[159], w[189], pars->GC_11, amp[638]); 
  FFS2_0(w[37], w[190], w[3], pars->GC_74, amp[639]); 
  FFV1_0(w[20], w[159], w[191], pars->GC_11, amp[640]); 
  FFS2_0(w[20], w[190], w[2], pars->GC_74, amp[641]); 
  FFV1_0(w[160], w[192], w[56], pars->GC_11, amp[642]); 
  FFV1_0(w[160], w[53], w[193], pars->GC_11, amp[643]); 
  FFV1_0(w[194], w[5], w[193], pars->GC_11, amp[644]); 
  FFV1_0(w[194], w[159], w[56], pars->GC_11, amp[645]); 
  FFV1_0(w[58], w[5], w[164], pars->GC_11, amp[646]); 
  FFV1_0(w[52], w[5], w[195], pars->GC_11, amp[647]); 
  FFV1_0(w[52], w[53], w[164], pars->GC_11, amp[648]); 
  FFV1_0(w[58], w[159], w[166], pars->GC_11, amp[649]); 
  FFV1_0(w[52], w[192], w[166], pars->GC_11, amp[650]); 
  FFV1_0(w[52], w[159], w[196], pars->GC_11, amp[651]); 
  FFS2_0(w[198], w[53], w[3], pars->GC_74, amp[652]); 
  FFS2_0(w[194], w[199], w[3], pars->GC_74, amp[653]); 
  FFV1_0(w[160], w[30], w[200], pars->GC_11, amp[654]); 
  FFV1_0(w[160], w[71], w[197], pars->GC_11, amp[655]); 
  FFV1_0(w[194], w[30], w[197], pars->GC_11, amp[656]); 
  FFV1_0(w[172], w[5], w[200], pars->GC_11, amp[657]); 
  FFV1_0(w[201], w[5], w[197], pars->GC_11, amp[658]); 
  FFV1_0(w[172], w[53], w[197], pars->GC_11, amp[659]); 
  FFS2_0(w[202], w[192], w[3], pars->GC_74, amp[660]); 
  FFS2_0(w[194], w[203], w[3], pars->GC_74, amp[661]); 
  FFV1_0(w[160], w[174], w[64], pars->GC_11, amp[662]); 
  FFV1_0(w[160], w[204], w[61], pars->GC_11, amp[663]); 
  FFV1_0(w[194], w[174], w[61], pars->GC_11, amp[664]); 
  FFV1_0(w[172], w[159], w[64], pars->GC_11, amp[665]); 
  FFV1_0(w[201], w[159], w[61], pars->GC_11, amp[666]); 
  FFV1_0(w[172], w[192], w[61], pars->GC_11, amp[667]); 
  FFV1_0(w[72], w[5], w[180], pars->GC_11, amp[668]); 
  FFV1_0(w[160], w[53], w[205], pars->GC_11, amp[669]); 
  FFV1_0(w[0], w[53], w[180], pars->GC_11, amp[670]); 
  FFV1_0(w[194], w[5], w[205], pars->GC_11, amp[671]); 
  FFV1_0(w[72], w[174], w[166], pars->GC_11, amp[672]); 
  FFV1_0(w[0], w[204], w[166], pars->GC_11, amp[673]); 
  FFV1_0(w[0], w[174], w[196], pars->GC_11, amp[674]); 
  FFV1_0(w[72], w[159], w[182], pars->GC_11, amp[675]); 
  FFV1_0(w[160], w[192], w[73], pars->GC_11, amp[676]); 
  FFV1_0(w[0], w[192], w[182], pars->GC_11, amp[677]); 
  FFV1_0(w[194], w[159], w[73], pars->GC_11, amp[678]); 
  FFV1_0(w[72], w[30], w[164], pars->GC_11, amp[679]); 
  FFV1_0(w[0], w[71], w[164], pars->GC_11, amp[680]); 
  FFV1_0(w[0], w[30], w[195], pars->GC_11, amp[681]); 
  FFV1_0(w[72], w[5], w[183], pars->GC_11, amp[682]); 
  FFV1_0(w[72], w[159], w[184], pars->GC_11, amp[683]); 
  FFV1_0(w[0], w[192], w[184], pars->GC_11, amp[684]); 
  FFV1_0(w[0], w[53], w[183], pars->GC_11, amp[685]); 
  FFS2_0(w[72], w[187], w[3], pars->GC_74, amp[686]); 
  FFS2_0(w[206], w[53], w[3], pars->GC_74, amp[687]); 
  FFS2_0(w[72], w[190], w[3], pars->GC_74, amp[688]); 
  FFS2_0(w[207], w[192], w[3], pars->GC_74, amp[689]); 
  FFV1_0(w[160], w[208], w[82], pars->GC_11, amp[690]); 
  FFV1_0(w[160], w[79], w[209], pars->GC_11, amp[691]); 
  FFV1_0(w[210], w[5], w[209], pars->GC_11, amp[692]); 
  FFV1_0(w[210], w[159], w[82], pars->GC_11, amp[693]); 
  FFV1_0(w[84], w[5], w[164], pars->GC_11, amp[694]); 
  FFV1_0(w[78], w[5], w[211], pars->GC_11, amp[695]); 
  FFV1_0(w[78], w[79], w[164], pars->GC_11, amp[696]); 
  FFV1_0(w[84], w[159], w[166], pars->GC_11, amp[697]); 
  FFV1_0(w[78], w[208], w[166], pars->GC_11, amp[698]); 
  FFV1_0(w[78], w[159], w[212], pars->GC_11, amp[699]); 
  FFS2_0(w[198], w[79], w[2], pars->GC_74, amp[700]); 
  FFS2_0(w[210], w[199], w[2], pars->GC_74, amp[701]); 
  FFV1_0(w[160], w[19], w[213], pars->GC_11, amp[702]); 
  FFV1_0(w[160], w[91], w[197], pars->GC_11, amp[703]); 
  FFV1_0(w[210], w[19], w[197], pars->GC_11, amp[704]); 
  FFV1_0(w[176], w[5], w[213], pars->GC_11, amp[705]); 
  FFV1_0(w[214], w[5], w[197], pars->GC_11, amp[706]); 
  FFV1_0(w[176], w[79], w[197], pars->GC_11, amp[707]); 
  FFS2_0(w[202], w[208], w[2], pars->GC_74, amp[708]); 
  FFS2_0(w[210], w[203], w[2], pars->GC_74, amp[709]); 
  FFV1_0(w[160], w[168], w[87], pars->GC_11, amp[710]); 
  FFV1_0(w[160], w[215], w[61], pars->GC_11, amp[711]); 
  FFV1_0(w[210], w[168], w[61], pars->GC_11, amp[712]); 
  FFV1_0(w[176], w[159], w[87], pars->GC_11, amp[713]); 
  FFV1_0(w[214], w[159], w[61], pars->GC_11, amp[714]); 
  FFV1_0(w[176], w[208], w[61], pars->GC_11, amp[715]); 
  FFV1_0(w[92], w[5], w[169], pars->GC_11, amp[716]); 
  FFV1_0(w[160], w[79], w[216], pars->GC_11, amp[717]); 
  FFV1_0(w[0], w[79], w[169], pars->GC_11, amp[718]); 
  FFV1_0(w[210], w[5], w[216], pars->GC_11, amp[719]); 
  FFV1_0(w[92], w[168], w[166], pars->GC_11, amp[720]); 
  FFV1_0(w[0], w[215], w[166], pars->GC_11, amp[721]); 
  FFV1_0(w[0], w[168], w[212], pars->GC_11, amp[722]); 
  FFV1_0(w[92], w[159], w[173], pars->GC_11, amp[723]); 
  FFV1_0(w[160], w[208], w[93], pars->GC_11, amp[724]); 
  FFV1_0(w[0], w[208], w[173], pars->GC_11, amp[725]); 
  FFV1_0(w[210], w[159], w[93], pars->GC_11, amp[726]); 
  FFV1_0(w[92], w[19], w[164], pars->GC_11, amp[727]); 
  FFV1_0(w[0], w[91], w[164], pars->GC_11, amp[728]); 
  FFV1_0(w[0], w[19], w[211], pars->GC_11, amp[729]); 
  FFV1_0(w[92], w[5], w[177], pars->GC_11, amp[730]); 
  FFV1_0(w[92], w[159], w[178], pars->GC_11, amp[731]); 
  FFV1_0(w[0], w[208], w[178], pars->GC_11, amp[732]); 
  FFV1_0(w[0], w[79], w[177], pars->GC_11, amp[733]); 
  FFS2_0(w[92], w[187], w[2], pars->GC_74, amp[734]); 
  FFS2_0(w[206], w[79], w[2], pars->GC_74, amp[735]); 
  FFS2_0(w[92], w[190], w[2], pars->GC_74, amp[736]); 
  FFS2_0(w[207], w[208], w[2], pars->GC_74, amp[737]); 
  FFV1_0(w[160], w[218], w[82], pars->GC_11, amp[738]); 
  FFV1_0(w[98], w[5], w[219], pars->GC_11, amp[739]); 
  VVS3_0(w[219], w[82], w[3], pars->GC_13, amp[740]); 
  FFV1_0(w[160], w[30], w[220], pars->GC_11, amp[741]); 
  FFV1_0(w[78], w[30], w[219], pars->GC_11, amp[742]); 
  FFV1_0(w[172], w[5], w[220], pars->GC_11, amp[743]); 
  FFV1_0(w[78], w[5], w[221], pars->GC_11, amp[744]); 
  VVS3_0(w[166], w[220], w[3], pars->GC_13, amp[745]); 
  FFV1_0(w[78], w[218], w[166], pars->GC_11, amp[746]); 
  FFS2_0(w[78], w[222], w[3], pars->GC_74, amp[747]); 
  FFV1_0(w[160], w[223], w[56], pars->GC_11, amp[748]); 
  FFV1_0(w[103], w[5], w[219], pars->GC_11, amp[749]); 
  VVS3_0(w[219], w[56], w[2], pars->GC_13, amp[750]); 
  FFV1_0(w[160], w[19], w[224], pars->GC_11, amp[751]); 
  FFV1_0(w[52], w[19], w[219], pars->GC_11, amp[752]); 
  FFV1_0(w[176], w[5], w[224], pars->GC_11, amp[753]); 
  FFV1_0(w[52], w[5], w[225], pars->GC_11, amp[754]); 
  VVS3_0(w[166], w[224], w[2], pars->GC_13, amp[755]); 
  FFV1_0(w[52], w[223], w[166], pars->GC_11, amp[756]); 
  FFS2_0(w[52], w[222], w[2], pars->GC_74, amp[757]); 
  FFV1_0(w[160], w[223], w[121], pars->GC_11, amp[758]); 
  FFS2_0(w[202], w[223], w[3], pars->GC_74, amp[759]); 
  FFV1_0(w[160], w[218], w[122], pars->GC_11, amp[760]); 
  FFS2_0(w[202], w[218], w[2], pars->GC_74, amp[761]); 
  FFS2_0(w[160], w[226], w[8], pars->GC_74, amp[762]); 
  FFV1_0(w[160], w[227], w[61], pars->GC_11, amp[763]); 
  VVS3_0(w[61], w[219], w[8], pars->GC_13, amp[764]); 
  FFS2_0(w[176], w[226], w[3], pars->GC_74, amp[765]); 
  VVS3_0(w[61], w[225], w[3], pars->GC_13, amp[766]); 
  FFV1_0(w[176], w[218], w[61], pars->GC_11, amp[767]); 
  FFS2_0(w[172], w[226], w[2], pars->GC_74, amp[768]); 
  FFV1_0(w[172], w[223], w[61], pars->GC_11, amp[769]); 
  VVS3_0(w[61], w[221], w[2], pars->GC_13, amp[770]); 
  FFV1_0(w[160], w[13], w[228], pars->GC_11, amp[771]); 
  FFV1_0(w[162], w[5], w[228], pars->GC_11, amp[772]); 
  FFV1_0(w[111], w[5], w[219], pars->GC_11, amp[773]); 
  FFV1_0(w[0], w[13], w[219], pars->GC_11, amp[774]); 
  VVS3_0(w[166], w[228], w[8], pars->GC_13, amp[775]); 
  FFV1_0(w[0], w[227], w[166], pars->GC_11, amp[776]); 
  FFS2_0(w[0], w[222], w[8], pars->GC_74, amp[777]); 
  FFV1_0(w[160], w[22], w[228], pars->GC_11, amp[778]); 
  VVS3_0(w[228], w[173], w[3], pars->GC_13, amp[779]); 
  FFV1_0(w[160], w[218], w[93], pars->GC_11, amp[780]); 
  FFV1_0(w[0], w[218], w[173], pars->GC_11, amp[781]); 
  VVS3_0(w[219], w[93], w[3], pars->GC_13, amp[782]); 
  FFV1_0(w[0], w[22], w[219], pars->GC_11, amp[783]); 
  FFV1_0(w[172], w[19], w[228], pars->GC_11, amp[784]); 
  FFV1_0(w[0], w[19], w[221], pars->GC_11, amp[785]); 
  FFV1_0(w[179], w[5], w[228], pars->GC_11, amp[786]); 
  VVS3_0(w[228], w[178], w[3], pars->GC_13, amp[787]); 
  FFV1_0(w[0], w[218], w[178], pars->GC_11, amp[788]); 
  FFV1_0(w[176], w[30], w[228], pars->GC_11, amp[789]); 
  FFV1_0(w[0], w[30], w[225], pars->GC_11, amp[790]); 
  FFV1_0(w[160], w[39], w[228], pars->GC_11, amp[791]); 
  VVS3_0(w[228], w[182], w[2], pars->GC_13, amp[792]); 
  FFV1_0(w[160], w[223], w[73], pars->GC_11, amp[793]); 
  FFV1_0(w[0], w[223], w[182], pars->GC_11, amp[794]); 
  VVS3_0(w[219], w[73], w[2], pars->GC_13, amp[795]); 
  FFV1_0(w[0], w[39], w[219], pars->GC_11, amp[796]); 
  FFV1_0(w[185], w[5], w[228], pars->GC_11, amp[797]); 
  VVS3_0(w[228], w[184], w[2], pars->GC_13, amp[798]); 
  FFV1_0(w[0], w[223], w[184], pars->GC_11, amp[799]); 
  FFS2_0(w[207], w[223], w[3], pars->GC_74, amp[800]); 
  FFV1_0(w[0], w[223], w[189], pars->GC_11, amp[801]); 
  FFS2_0(w[207], w[218], w[2], pars->GC_74, amp[802]); 
  FFV1_0(w[0], w[218], w[191], pars->GC_11, amp[803]); 
  FFV1_0(w[160], w[96], w[209], pars->GC_11, amp[804]); 
  FFV1_0(w[98], w[159], w[229], pars->GC_11, amp[805]); 
  VVS3_0(w[229], w[209], w[3], pars->GC_13, amp[806]); 
  FFV1_0(w[160], w[174], w[99], pars->GC_11, amp[807]); 
  FFV1_0(w[78], w[174], w[229], pars->GC_11, amp[808]); 
  FFV1_0(w[172], w[159], w[99], pars->GC_11, amp[809]); 
  FFV1_0(w[78], w[159], w[230], pars->GC_11, amp[810]); 
  VVS3_0(w[164], w[99], w[3], pars->GC_13, amp[811]); 
  FFV1_0(w[78], w[96], w[164], pars->GC_11, amp[812]); 
  FFS2_0(w[78], w[231], w[3], pars->GC_74, amp[813]); 
  FFV1_0(w[160], w[102], w[193], pars->GC_11, amp[814]); 
  FFV1_0(w[103], w[159], w[229], pars->GC_11, amp[815]); 
  VVS3_0(w[229], w[193], w[2], pars->GC_13, amp[816]); 
  FFV1_0(w[160], w[168], w[104], pars->GC_11, amp[817]); 
  FFV1_0(w[52], w[168], w[229], pars->GC_11, amp[818]); 
  FFV1_0(w[176], w[159], w[104], pars->GC_11, amp[819]); 
  FFV1_0(w[52], w[159], w[232], pars->GC_11, amp[820]); 
  VVS3_0(w[164], w[104], w[2], pars->GC_13, amp[821]); 
  FFV1_0(w[52], w[102], w[164], pars->GC_11, amp[822]); 
  FFS2_0(w[52], w[231], w[2], pars->GC_74, amp[823]); 
  FFV1_0(w[160], w[102], w[233], pars->GC_11, amp[824]); 
  FFS2_0(w[198], w[102], w[3], pars->GC_74, amp[825]); 
  FFV1_0(w[160], w[96], w[234], pars->GC_11, amp[826]); 
  FFS2_0(w[198], w[96], w[2], pars->GC_74, amp[827]); 
  FFS2_0(w[160], w[235], w[8], pars->GC_74, amp[828]); 
  FFV1_0(w[160], w[109], w[197], pars->GC_11, amp[829]); 
  VVS3_0(w[197], w[229], w[8], pars->GC_13, amp[830]); 
  FFS2_0(w[176], w[235], w[3], pars->GC_74, amp[831]); 
  VVS3_0(w[197], w[232], w[3], pars->GC_13, amp[832]); 
  FFV1_0(w[176], w[96], w[197], pars->GC_11, amp[833]); 
  FFS2_0(w[172], w[235], w[2], pars->GC_74, amp[834]); 
  FFV1_0(w[172], w[102], w[197], pars->GC_11, amp[835]); 
  VVS3_0(w[197], w[230], w[2], pars->GC_13, amp[836]); 
  FFV1_0(w[160], w[163], w[110], pars->GC_11, amp[837]); 
  FFV1_0(w[162], w[159], w[110], pars->GC_11, amp[838]); 
  FFV1_0(w[111], w[159], w[229], pars->GC_11, amp[839]); 
  FFV1_0(w[0], w[163], w[229], pars->GC_11, amp[840]); 
  VVS3_0(w[164], w[110], w[8], pars->GC_13, amp[841]); 
  FFV1_0(w[0], w[109], w[164], pars->GC_11, amp[842]); 
  FFS2_0(w[0], w[231], w[8], pars->GC_74, amp[843]); 
  FFV1_0(w[160], w[170], w[110], pars->GC_11, amp[844]); 
  VVS3_0(w[110], w[169], w[3], pars->GC_13, amp[845]); 
  FFV1_0(w[160], w[96], w[216], pars->GC_11, amp[846]); 
  FFV1_0(w[0], w[96], w[169], pars->GC_11, amp[847]); 
  VVS3_0(w[229], w[216], w[3], pars->GC_13, amp[848]); 
  FFV1_0(w[0], w[170], w[229], pars->GC_11, amp[849]); 
  FFV1_0(w[172], w[168], w[110], pars->GC_11, amp[850]); 
  FFV1_0(w[0], w[168], w[230], pars->GC_11, amp[851]); 
  FFV1_0(w[179], w[159], w[110], pars->GC_11, amp[852]); 
  VVS3_0(w[110], w[177], w[3], pars->GC_13, amp[853]); 
  FFV1_0(w[0], w[96], w[177], pars->GC_11, amp[854]); 
  FFV1_0(w[176], w[174], w[110], pars->GC_11, amp[855]); 
  FFV1_0(w[0], w[174], w[232], pars->GC_11, amp[856]); 
  FFV1_0(w[160], w[181], w[110], pars->GC_11, amp[857]); 
  VVS3_0(w[110], w[180], w[2], pars->GC_13, amp[858]); 
  FFV1_0(w[160], w[102], w[205], pars->GC_11, amp[859]); 
  FFV1_0(w[0], w[102], w[180], pars->GC_11, amp[860]); 
  VVS3_0(w[229], w[205], w[2], pars->GC_13, amp[861]); 
  FFV1_0(w[0], w[181], w[229], pars->GC_11, amp[862]); 
  FFV1_0(w[185], w[159], w[110], pars->GC_11, amp[863]); 
  VVS3_0(w[110], w[183], w[2], pars->GC_13, amp[864]); 
  FFV1_0(w[0], w[102], w[183], pars->GC_11, amp[865]); 
  FFS2_0(w[206], w[102], w[3], pars->GC_74, amp[866]); 
  FFV1_0(w[0], w[102], w[186], pars->GC_11, amp[867]); 
  FFS2_0(w[206], w[96], w[2], pars->GC_74, amp[868]); 
  FFV1_0(w[0], w[96], w[188], pars->GC_11, amp[869]); 
  FFV1_0(w[237], w[5], w[209], pars->GC_11, amp[870]); 
  FFV1_0(w[237], w[159], w[82], pars->GC_11, amp[871]); 
  FFV1_0(w[98], w[5], w[238], pars->GC_11, amp[872]); 
  VVS3_0(w[238], w[82], w[3], pars->GC_13, amp[873]); 
  FFV1_0(w[98], w[159], w[239], pars->GC_11, amp[874]); 
  VVS3_0(w[239], w[209], w[3], pars->GC_13, amp[875]); 
  FFV1_0(w[78], w[5], w[240], pars->GC_11, amp[876]); 
  FFV1_0(w[78], w[174], w[239], pars->GC_11, amp[877]); 
  FFV1_0(w[78], w[159], w[241], pars->GC_11, amp[878]); 
  FFV1_0(w[78], w[30], w[238], pars->GC_11, amp[879]); 
  FFV1_0(w[242], w[5], w[193], pars->GC_11, amp[880]); 
  FFV1_0(w[242], w[159], w[56], pars->GC_11, amp[881]); 
  FFV1_0(w[103], w[5], w[238], pars->GC_11, amp[882]); 
  VVS3_0(w[238], w[56], w[2], pars->GC_13, amp[883]); 
  FFV1_0(w[103], w[159], w[239], pars->GC_11, amp[884]); 
  VVS3_0(w[239], w[193], w[2], pars->GC_13, amp[885]); 
  FFV1_0(w[52], w[5], w[243], pars->GC_11, amp[886]); 
  FFV1_0(w[52], w[168], w[239], pars->GC_11, amp[887]); 
  FFV1_0(w[52], w[159], w[244], pars->GC_11, amp[888]); 
  FFV1_0(w[52], w[19], w[238], pars->GC_11, amp[889]); 
  FFV1_0(w[242], w[5], w[233], pars->GC_11, amp[890]); 
  FFS2_0(w[242], w[199], w[3], pars->GC_74, amp[891]); 
  FFV1_0(w[237], w[5], w[234], pars->GC_11, amp[892]); 
  FFS2_0(w[237], w[199], w[2], pars->GC_74, amp[893]); 
  FFS2_0(w[245], w[5], w[8], pars->GC_74, amp[894]); 
  FFV1_0(w[246], w[5], w[197], pars->GC_11, amp[895]); 
  VVS3_0(w[197], w[239], w[8], pars->GC_13, amp[896]); 
  FFS2_0(w[245], w[19], w[3], pars->GC_74, amp[897]); 
  VVS3_0(w[197], w[244], w[3], pars->GC_13, amp[898]); 
  FFV1_0(w[237], w[19], w[197], pars->GC_11, amp[899]); 
  FFS2_0(w[245], w[30], w[2], pars->GC_74, amp[900]); 
  FFV1_0(w[242], w[30], w[197], pars->GC_11, amp[901]); 
  VVS3_0(w[197], w[241], w[2], pars->GC_13, amp[902]); 
  FFV1_0(w[242], w[159], w[121], pars->GC_11, amp[903]); 
  FFS2_0(w[242], w[203], w[3], pars->GC_74, amp[904]); 
  FFV1_0(w[237], w[159], w[122], pars->GC_11, amp[905]); 
  FFS2_0(w[237], w[203], w[2], pars->GC_74, amp[906]); 
  FFS2_0(w[247], w[159], w[8], pars->GC_74, amp[907]); 
  FFV1_0(w[246], w[159], w[61], pars->GC_11, amp[908]); 
  VVS3_0(w[61], w[238], w[8], pars->GC_13, amp[909]); 
  FFS2_0(w[247], w[168], w[3], pars->GC_74, amp[910]); 
  VVS3_0(w[61], w[243], w[3], pars->GC_13, amp[911]); 
  FFV1_0(w[237], w[168], w[61], pars->GC_11, amp[912]); 
  FFS2_0(w[247], w[174], w[2], pars->GC_74, amp[913]); 
  FFV1_0(w[242], w[174], w[61], pars->GC_11, amp[914]); 
  VVS3_0(w[61], w[240], w[2], pars->GC_13, amp[915]); 
  FFV1_0(w[111], w[5], w[238], pars->GC_11, amp[916]); 
  FFV1_0(w[0], w[13], w[238], pars->GC_11, amp[917]); 
  FFV1_0(w[111], w[159], w[239], pars->GC_11, amp[918]); 
  FFV1_0(w[0], w[163], w[239], pars->GC_11, amp[919]); 
  FFV1_0(w[237], w[5], w[216], pars->GC_11, amp[920]); 
  VVS3_0(w[239], w[216], w[3], pars->GC_13, amp[921]); 
  FFV1_0(w[0], w[170], w[239], pars->GC_11, amp[922]); 
  FFV1_0(w[0], w[30], w[243], pars->GC_11, amp[923]); 
  FFV1_0(w[0], w[168], w[241], pars->GC_11, amp[924]); 
  FFV1_0(w[237], w[159], w[93], pars->GC_11, amp[925]); 
  VVS3_0(w[238], w[93], w[3], pars->GC_13, amp[926]); 
  FFV1_0(w[0], w[22], w[238], pars->GC_11, amp[927]); 
  FFV1_0(w[0], w[174], w[244], pars->GC_11, amp[928]); 
  FFV1_0(w[0], w[19], w[240], pars->GC_11, amp[929]); 
  FFV1_0(w[242], w[5], w[205], pars->GC_11, amp[930]); 
  VVS3_0(w[239], w[205], w[2], pars->GC_13, amp[931]); 
  FFV1_0(w[0], w[181], w[239], pars->GC_11, amp[932]); 
  FFV1_0(w[242], w[159], w[73], pars->GC_11, amp[933]); 
  VVS3_0(w[238], w[73], w[2], pars->GC_13, amp[934]); 
  FFV1_0(w[0], w[39], w[238], pars->GC_11, amp[935]); 
  FFV1_0(w[138], w[5], w[180], pars->GC_11, amp[936]); 
  FFV1_0(w[160], w[248], w[82], pars->GC_11, amp[937]); 
  VVV1_0(w[4], w[82], w[180], pars->GC_10, amp[938]); 
  FFV1_0(w[138], w[174], w[166], pars->GC_11, amp[939]); 
  FFV1_0(w[78], w[248], w[166], pars->GC_11, amp[940]); 
  FFV1_0(w[78], w[174], w[249], pars->GC_11, amp[941]); 
  FFV1_0(w[138], w[159], w[182], pars->GC_11, amp[942]); 
  FFV1_0(w[160], w[139], w[209], pars->GC_11, amp[943]); 
  VVV1_0(w[4], w[209], w[182], pars->GC_10, amp[944]); 
  FFV1_0(w[138], w[30], w[164], pars->GC_11, amp[945]); 
  FFV1_0(w[78], w[139], w[164], pars->GC_11, amp[946]); 
  FFV1_0(w[78], w[30], w[250], pars->GC_11, amp[947]); 
  FFV1_0(w[138], w[5], w[183], pars->GC_11, amp[948]); 
  FFV1_0(w[138], w[159], w[184], pars->GC_11, amp[949]); 
  FFV1_0(w[251], w[5], w[209], pars->GC_11, amp[950]); 
  FFV1_0(w[251], w[159], w[82], pars->GC_11, amp[951]); 
  VVV1_0(w[4], w[209], w[184], pars->GC_10, amp[952]); 
  VVV1_0(w[4], w[82], w[183], pars->GC_10, amp[953]); 
  FFV1_0(w[138], w[5], w[186], pars->GC_11, amp[954]); 
  FFS2_0(w[138], w[187], w[3], pars->GC_74, amp[955]); 
  FFV1_0(w[98], w[5], w[250], pars->GC_11, amp[956]); 
  VVS3_0(w[250], w[82], w[3], pars->GC_13, amp[957]); 
  FFV1_0(w[98], w[187], w[4], pars->GC_11, amp[958]); 
  VVV1_0(w[4], w[82], w[186], pars->GC_10, amp[959]); 
  FFV1_0(w[78], w[5], w[252], pars->GC_11, amp[960]); 
  FFV1_0(w[138], w[159], w[189], pars->GC_11, amp[961]); 
  FFS2_0(w[138], w[190], w[3], pars->GC_74, amp[962]); 
  FFV1_0(w[98], w[159], w[249], pars->GC_11, amp[963]); 
  VVS3_0(w[249], w[209], w[3], pars->GC_13, amp[964]); 
  FFV1_0(w[98], w[190], w[4], pars->GC_11, amp[965]); 
  VVV1_0(w[4], w[209], w[189], pars->GC_10, amp[966]); 
  FFV1_0(w[78], w[159], w[253], pars->GC_11, amp[967]); 
  FFV1_0(w[146], w[5], w[169], pars->GC_11, amp[968]); 
  FFV1_0(w[160], w[254], w[56], pars->GC_11, amp[969]); 
  VVV1_0(w[4], w[56], w[169], pars->GC_10, amp[970]); 
  FFV1_0(w[146], w[168], w[166], pars->GC_11, amp[971]); 
  FFV1_0(w[52], w[254], w[166], pars->GC_11, amp[972]); 
  FFV1_0(w[52], w[168], w[249], pars->GC_11, amp[973]); 
  FFV1_0(w[146], w[159], w[173], pars->GC_11, amp[974]); 
  FFV1_0(w[160], w[147], w[193], pars->GC_11, amp[975]); 
  VVV1_0(w[4], w[193], w[173], pars->GC_10, amp[976]); 
  FFV1_0(w[146], w[19], w[164], pars->GC_11, amp[977]); 
  FFV1_0(w[52], w[147], w[164], pars->GC_11, amp[978]); 
  FFV1_0(w[52], w[19], w[250], pars->GC_11, amp[979]); 
  FFV1_0(w[146], w[5], w[177], pars->GC_11, amp[980]); 
  FFV1_0(w[146], w[159], w[178], pars->GC_11, amp[981]); 
  FFV1_0(w[255], w[5], w[193], pars->GC_11, amp[982]); 
  FFV1_0(w[255], w[159], w[56], pars->GC_11, amp[983]); 
  VVV1_0(w[4], w[193], w[178], pars->GC_10, amp[984]); 
  VVV1_0(w[4], w[56], w[177], pars->GC_10, amp[985]); 
  FFV1_0(w[146], w[5], w[188], pars->GC_11, amp[986]); 
  FFS2_0(w[146], w[187], w[2], pars->GC_74, amp[987]); 
  FFV1_0(w[103], w[5], w[250], pars->GC_11, amp[988]); 
  VVS3_0(w[250], w[56], w[2], pars->GC_13, amp[989]); 
  FFV1_0(w[103], w[187], w[4], pars->GC_11, amp[990]); 
  VVV1_0(w[4], w[56], w[188], pars->GC_10, amp[991]); 
  FFV1_0(w[52], w[5], w[256], pars->GC_11, amp[992]); 
  FFV1_0(w[146], w[159], w[191], pars->GC_11, amp[993]); 
  FFS2_0(w[146], w[190], w[2], pars->GC_74, amp[994]); 
  FFV1_0(w[103], w[159], w[249], pars->GC_11, amp[995]); 
  VVS3_0(w[249], w[193], w[2], pars->GC_13, amp[996]); 
  FFV1_0(w[103], w[190], w[4], pars->GC_11, amp[997]); 
  VVV1_0(w[4], w[193], w[191], pars->GC_10, amp[998]); 
  FFV1_0(w[52], w[159], w[257], pars->GC_11, amp[999]); 
  FFV1_0(w[160], w[13], w[258], pars->GC_11, amp[1000]); 
  FFV1_0(w[162], w[5], w[258], pars->GC_11, amp[1001]); 
  FFV1_0(w[160], w[199], w[153], pars->GC_11, amp[1002]); 
  FFV1_0(w[198], w[5], w[153], pars->GC_11, amp[1003]); 
  FFV1_0(w[162], w[199], w[4], pars->GC_11, amp[1004]); 
  FFV1_0(w[198], w[13], w[4], pars->GC_11, amp[1005]); 
  VVVS2_0(w[4], w[197], w[166], w[8], pars->GC_14, amp[1006]); 
  VVS3_0(w[166], w[258], w[8], pars->GC_13, amp[1007]); 
  VVV1_0(w[197], w[166], w[153], pars->GC_10, amp[1008]); 
  VVS3_0(w[197], w[249], w[8], pars->GC_13, amp[1009]); 
  FFV1_0(w[160], w[22], w[258], pars->GC_11, amp[1010]); 
  VVS3_0(w[258], w[173], w[3], pars->GC_13, amp[1011]); 
  FFV1_0(w[160], w[147], w[233], pars->GC_11, amp[1012]); 
  FFS2_0(w[198], w[147], w[3], pars->GC_74, amp[1013]); 
  VVV1_0(w[4], w[233], w[173], pars->GC_10, amp[1014]); 
  FFV1_0(w[198], w[22], w[4], pars->GC_11, amp[1015]); 
  FFV1_0(w[160], w[19], w[259], pars->GC_11, amp[1016]); 
  FFV1_0(w[172], w[19], w[258], pars->GC_11, amp[1017]); 
  FFV1_0(w[172], w[147], w[197], pars->GC_11, amp[1018]); 
  FFV1_0(w[251], w[19], w[197], pars->GC_11, amp[1019]); 
  FFV1_0(w[179], w[5], w[258], pars->GC_11, amp[1020]); 
  VVS3_0(w[258], w[178], w[3], pars->GC_13, amp[1021]); 
  FFV1_0(w[255], w[5], w[233], pars->GC_11, amp[1022]); 
  FFS2_0(w[255], w[199], w[3], pars->GC_74, amp[1023]); 
  VVV1_0(w[4], w[233], w[178], pars->GC_10, amp[1024]); 
  FFV1_0(w[179], w[199], w[4], pars->GC_11, amp[1025]); 
  FFV1_0(w[176], w[5], w[259], pars->GC_11, amp[1026]); 
  FFV1_0(w[176], w[30], w[258], pars->GC_11, amp[1027]); 
  FFV1_0(w[255], w[30], w[197], pars->GC_11, amp[1028]); 
  FFV1_0(w[176], w[139], w[197], pars->GC_11, amp[1029]); 
  FFV1_0(w[160], w[39], w[258], pars->GC_11, amp[1030]); 
  VVS3_0(w[258], w[182], w[2], pars->GC_13, amp[1031]); 
  FFV1_0(w[160], w[139], w[234], pars->GC_11, amp[1032]); 
  FFS2_0(w[198], w[139], w[2], pars->GC_74, amp[1033]); 
  VVV1_0(w[4], w[234], w[182], pars->GC_10, amp[1034]); 
  FFV1_0(w[198], w[39], w[4], pars->GC_11, amp[1035]); 
  FFV1_0(w[160], w[30], w[260], pars->GC_11, amp[1036]); 
  FFV1_0(w[185], w[5], w[258], pars->GC_11, amp[1037]); 
  VVS3_0(w[258], w[184], w[2], pars->GC_13, amp[1038]); 
  FFV1_0(w[251], w[5], w[234], pars->GC_11, amp[1039]); 
  FFS2_0(w[251], w[199], w[2], pars->GC_74, amp[1040]); 
  VVV1_0(w[4], w[234], w[184], pars->GC_10, amp[1041]); 
  FFV1_0(w[185], w[199], w[4], pars->GC_11, amp[1042]); 
  FFV1_0(w[172], w[5], w[260], pars->GC_11, amp[1043]); 
  FFV1_0(w[160], w[163], w[152], pars->GC_11, amp[1044]); 
  FFV1_0(w[162], w[159], w[152], pars->GC_11, amp[1045]); 
  FFV1_0(w[160], w[203], w[153], pars->GC_11, amp[1046]); 
  FFV1_0(w[202], w[159], w[153], pars->GC_11, amp[1047]); 
  FFV1_0(w[162], w[203], w[4], pars->GC_11, amp[1048]); 
  FFV1_0(w[202], w[163], w[4], pars->GC_11, amp[1049]); 
  VVVS2_0(w[4], w[61], w[164], w[8], pars->GC_14, amp[1050]); 
  VVS3_0(w[164], w[152], w[8], pars->GC_13, amp[1051]); 
  VVV1_0(w[61], w[164], w[153], pars->GC_10, amp[1052]); 
  VVS3_0(w[61], w[250], w[8], pars->GC_13, amp[1053]); 
  FFV1_0(w[160], w[170], w[152], pars->GC_11, amp[1054]); 
  VVS3_0(w[152], w[169], w[3], pars->GC_13, amp[1055]); 
  FFV1_0(w[160], w[254], w[121], pars->GC_11, amp[1056]); 
  FFS2_0(w[202], w[254], w[3], pars->GC_74, amp[1057]); 
  VVV1_0(w[4], w[121], w[169], pars->GC_10, amp[1058]); 
  FFV1_0(w[202], w[170], w[4], pars->GC_11, amp[1059]); 
  FFV1_0(w[160], w[168], w[154], pars->GC_11, amp[1060]); 
  FFV1_0(w[172], w[168], w[152], pars->GC_11, amp[1061]); 
  FFV1_0(w[172], w[254], w[61], pars->GC_11, amp[1062]); 
  FFV1_0(w[251], w[168], w[61], pars->GC_11, amp[1063]); 
  FFV1_0(w[179], w[159], w[152], pars->GC_11, amp[1064]); 
  VVS3_0(w[152], w[177], w[3], pars->GC_13, amp[1065]); 
  FFV1_0(w[255], w[159], w[121], pars->GC_11, amp[1066]); 
  FFS2_0(w[255], w[203], w[3], pars->GC_74, amp[1067]); 
  VVV1_0(w[4], w[121], w[177], pars->GC_10, amp[1068]); 
  FFV1_0(w[179], w[203], w[4], pars->GC_11, amp[1069]); 
  FFV1_0(w[176], w[159], w[154], pars->GC_11, amp[1070]); 
  FFV1_0(w[176], w[174], w[152], pars->GC_11, amp[1071]); 
  FFV1_0(w[255], w[174], w[61], pars->GC_11, amp[1072]); 
  FFV1_0(w[176], w[248], w[61], pars->GC_11, amp[1073]); 
  FFV1_0(w[160], w[181], w[152], pars->GC_11, amp[1074]); 
  VVS3_0(w[152], w[180], w[2], pars->GC_13, amp[1075]); 
  FFV1_0(w[160], w[248], w[122], pars->GC_11, amp[1076]); 
  FFS2_0(w[202], w[248], w[2], pars->GC_74, amp[1077]); 
  VVV1_0(w[4], w[122], w[180], pars->GC_10, amp[1078]); 
  FFV1_0(w[202], w[181], w[4], pars->GC_11, amp[1079]); 
  FFV1_0(w[160], w[174], w[155], pars->GC_11, amp[1080]); 
  FFV1_0(w[185], w[159], w[152], pars->GC_11, amp[1081]); 
  VVS3_0(w[152], w[183], w[2], pars->GC_13, amp[1082]); 
  FFV1_0(w[251], w[159], w[122], pars->GC_11, amp[1083]); 
  FFS2_0(w[251], w[203], w[2], pars->GC_74, amp[1084]); 
  VVV1_0(w[4], w[122], w[183], pars->GC_10, amp[1085]); 
  FFV1_0(w[185], w[203], w[4], pars->GC_11, amp[1086]); 
  FFV1_0(w[172], w[159], w[155], pars->GC_11, amp[1087]); 
  FFV1_0(w[206], w[5], w[153], pars->GC_11, amp[1088]); 
  FFV1_0(w[0], w[187], w[153], pars->GC_11, amp[1089]); 
  FFV1_0(w[111], w[5], w[250], pars->GC_11, amp[1090]); 
  FFV1_0(w[0], w[13], w[250], pars->GC_11, amp[1091]); 
  FFV1_0(w[111], w[187], w[4], pars->GC_11, amp[1092]); 
  FFV1_0(w[206], w[13], w[4], pars->GC_11, amp[1093]); 
  FFV1_0(w[207], w[159], w[153], pars->GC_11, amp[1094]); 
  FFV1_0(w[0], w[190], w[153], pars->GC_11, amp[1095]); 
  FFV1_0(w[111], w[159], w[249], pars->GC_11, amp[1096]); 
  FFV1_0(w[0], w[163], w[249], pars->GC_11, amp[1097]); 
  FFV1_0(w[111], w[190], w[4], pars->GC_11, amp[1098]); 
  FFV1_0(w[207], w[163], w[4], pars->GC_11, amp[1099]); 
  FFV1_0(w[160], w[254], w[73], pars->GC_11, amp[1100]); 
  FFV1_0(w[0], w[254], w[182], pars->GC_11, amp[1101]); 
  FFV1_0(w[160], w[139], w[216], pars->GC_11, amp[1102]); 
  FFV1_0(w[0], w[139], w[169], pars->GC_11, amp[1103]); 
  VVV1_0(w[4], w[216], w[182], pars->GC_10, amp[1104]); 
  VVV1_0(w[4], w[73], w[169], pars->GC_10, amp[1105]); 
  FFV1_0(w[0], w[254], w[184], pars->GC_11, amp[1106]); 
  FFV1_0(w[251], w[5], w[216], pars->GC_11, amp[1107]); 
  VVV1_0(w[4], w[216], w[184], pars->GC_10, amp[1108]); 
  FFS2_0(w[207], w[254], w[3], pars->GC_74, amp[1109]); 
  FFV1_0(w[0], w[254], w[189], pars->GC_11, amp[1110]); 
  VVS3_0(w[249], w[216], w[3], pars->GC_13, amp[1111]); 
  FFV1_0(w[0], w[170], w[249], pars->GC_11, amp[1112]); 
  VVV1_0(w[4], w[216], w[189], pars->GC_10, amp[1113]); 
  FFV1_0(w[207], w[170], w[4], pars->GC_11, amp[1114]); 
  FFV1_0(w[0], w[168], w[253], pars->GC_11, amp[1115]); 
  FFV1_0(w[160], w[147], w[205], pars->GC_11, amp[1116]); 
  FFV1_0(w[0], w[147], w[180], pars->GC_11, amp[1117]); 
  FFV1_0(w[160], w[248], w[93], pars->GC_11, amp[1118]); 
  FFV1_0(w[0], w[248], w[173], pars->GC_11, amp[1119]); 
  VVV1_0(w[4], w[93], w[180], pars->GC_10, amp[1120]); 
  VVV1_0(w[4], w[205], w[173], pars->GC_10, amp[1121]); 
  FFV1_0(w[0], w[147], w[183], pars->GC_11, amp[1122]); 
  FFV1_0(w[251], w[159], w[93], pars->GC_11, amp[1123]); 
  VVV1_0(w[4], w[93], w[183], pars->GC_10, amp[1124]); 
  FFS2_0(w[206], w[147], w[3], pars->GC_74, amp[1125]); 
  FFV1_0(w[0], w[147], w[186], pars->GC_11, amp[1126]); 
  VVS3_0(w[250], w[93], w[3], pars->GC_13, amp[1127]); 
  FFV1_0(w[0], w[22], w[250], pars->GC_11, amp[1128]); 
  VVV1_0(w[4], w[93], w[186], pars->GC_10, amp[1129]); 
  FFV1_0(w[206], w[22], w[4], pars->GC_11, amp[1130]); 
  FFV1_0(w[0], w[19], w[252], pars->GC_11, amp[1131]); 
  FFV1_0(w[255], w[5], w[205], pars->GC_11, amp[1132]); 
  FFV1_0(w[0], w[248], w[178], pars->GC_11, amp[1133]); 
  VVV1_0(w[4], w[205], w[178], pars->GC_10, amp[1134]); 
  FFV1_0(w[255], w[159], w[73], pars->GC_11, amp[1135]); 
  FFV1_0(w[0], w[139], w[177], pars->GC_11, amp[1136]); 
  VVV1_0(w[4], w[73], w[177], pars->GC_10, amp[1137]); 
  FFS2_0(w[207], w[248], w[2], pars->GC_74, amp[1138]); 
  FFV1_0(w[0], w[248], w[191], pars->GC_11, amp[1139]); 
  VVS3_0(w[249], w[205], w[2], pars->GC_13, amp[1140]); 
  FFV1_0(w[0], w[181], w[249], pars->GC_11, amp[1141]); 
  VVV1_0(w[4], w[205], w[191], pars->GC_10, amp[1142]); 
  FFV1_0(w[207], w[181], w[4], pars->GC_11, amp[1143]); 
  FFV1_0(w[0], w[174], w[257], pars->GC_11, amp[1144]); 
  FFS2_0(w[206], w[139], w[2], pars->GC_74, amp[1145]); 
  FFV1_0(w[0], w[139], w[188], pars->GC_11, amp[1146]); 
  VVS3_0(w[250], w[73], w[2], pars->GC_13, amp[1147]); 
  FFV1_0(w[0], w[39], w[250], pars->GC_11, amp[1148]); 
  VVV1_0(w[4], w[73], w[188], pars->GC_10, amp[1149]); 
  FFV1_0(w[206], w[39], w[4], pars->GC_11, amp[1150]); 
  FFV1_0(w[0], w[30], w[256], pars->GC_11, amp[1151]); 
  FFV1_0(w[160], w[163], w[264], pars->GC_11, amp[1152]); 
  FFV1_0(w[162], w[159], w[264], pars->GC_11, amp[1153]); 
  FFV1_0(w[160], w[266], w[265], pars->GC_11, amp[1154]); 
  FFV1_0(w[162], w[261], w[265], pars->GC_11, amp[1155]); 
  FFV1_0(w[268], w[159], w[267], pars->GC_11, amp[1156]); 
  FFS2_0(w[269], w[159], w[8], pars->GC_74, amp[1157]); 
  VVS3_0(w[267], w[265], w[8], pars->GC_13, amp[1158]); 
  FFV1_0(w[268], w[261], w[164], pars->GC_11, amp[1159]); 
  VVS3_0(w[164], w[264], w[8], pars->GC_13, amp[1160]); 
  FFS2_0(w[270], w[261], w[8], pars->GC_74, amp[1161]); 
  FFV1_0(w[272], w[159], w[273], pars->GC_11, amp[1162]); 
  FFV1_0(w[160], w[274], w[265], pars->GC_11, amp[1163]); 
  VVS3_0(w[265], w[273], w[3], pars->GC_13, amp[1164]); 
  FFV1_0(w[160], w[174], w[275], pars->GC_11, amp[1165]); 
  FFV1_0(w[160], w[271], w[276], pars->GC_11, amp[1166]); 
  FFV1_0(w[172], w[159], w[275], pars->GC_11, amp[1167]); 
  FFV1_0(w[172], w[271], w[265], pars->GC_11, amp[1168]); 
  VVS3_0(w[164], w[275], w[3], pars->GC_13, amp[1169]); 
  FFV1_0(w[272], w[271], w[164], pars->GC_11, amp[1170]); 
  FFS2_0(w[270], w[271], w[3], pars->GC_74, amp[1171]); 
  FFV1_0(w[272], w[261], w[169], pars->GC_11, amp[1172]); 
  FFV1_0(w[160], w[170], w[264], pars->GC_11, amp[1173]); 
  VVS3_0(w[264], w[169], w[3], pars->GC_13, amp[1174]); 
  FFV1_0(w[160], w[277], w[278], pars->GC_11, amp[1175]); 
  FFV1_0(w[160], w[168], w[279], pars->GC_11, amp[1176]); 
  FFV1_0(w[172], w[261], w[278], pars->GC_11, amp[1177]); 
  FFV1_0(w[172], w[168], w[264], pars->GC_11, amp[1178]); 
  VVS3_0(w[267], w[278], w[3], pars->GC_13, amp[1179]); 
  FFV1_0(w[272], w[168], w[267], pars->GC_11, amp[1180]); 
  FFS2_0(w[269], w[168], w[3], pars->GC_74, amp[1181]); 
  FFV1_0(w[272], w[159], w[280], pars->GC_11, amp[1182]); 
  FFV1_0(w[272], w[261], w[177], pars->GC_11, amp[1183]); 
  FFV1_0(w[179], w[159], w[264], pars->GC_11, amp[1184]); 
  VVS3_0(w[264], w[177], w[3], pars->GC_13, amp[1185]); 
  FFV1_0(w[179], w[261], w[265], pars->GC_11, amp[1186]); 
  VVS3_0(w[265], w[280], w[3], pars->GC_13, amp[1187]); 
  FFV1_0(w[176], w[159], w[279], pars->GC_11, amp[1188]); 
  FFV1_0(w[176], w[277], w[265], pars->GC_11, amp[1189]); 
  FFV1_0(w[176], w[261], w[276], pars->GC_11, amp[1190]); 
  FFV1_0(w[176], w[174], w[264], pars->GC_11, amp[1191]); 
  FFV1_0(w[281], w[159], w[282], pars->GC_11, amp[1192]); 
  FFV1_0(w[160], w[283], w[265], pars->GC_11, amp[1193]); 
  VVS3_0(w[265], w[282], w[2], pars->GC_13, amp[1194]); 
  FFV1_0(w[281], w[277], w[164], pars->GC_11, amp[1195]); 
  VVS3_0(w[164], w[279], w[2], pars->GC_13, amp[1196]); 
  FFS2_0(w[270], w[277], w[2], pars->GC_74, amp[1197]); 
  FFV1_0(w[281], w[261], w[180], pars->GC_11, amp[1198]); 
  FFV1_0(w[160], w[181], w[264], pars->GC_11, amp[1199]); 
  VVS3_0(w[264], w[180], w[2], pars->GC_13, amp[1200]); 
  FFV1_0(w[281], w[174], w[267], pars->GC_11, amp[1201]); 
  VVS3_0(w[267], w[276], w[2], pars->GC_13, amp[1202]); 
  FFS2_0(w[269], w[174], w[2], pars->GC_74, amp[1203]); 
  FFV1_0(w[281], w[159], w[284], pars->GC_11, amp[1204]); 
  FFV1_0(w[281], w[261], w[183], pars->GC_11, amp[1205]); 
  FFV1_0(w[185], w[159], w[264], pars->GC_11, amp[1206]); 
  VVS3_0(w[264], w[183], w[2], pars->GC_13, amp[1207]); 
  FFV1_0(w[185], w[261], w[265], pars->GC_11, amp[1208]); 
  VVS3_0(w[265], w[284], w[2], pars->GC_13, amp[1209]); 
  FFV1_0(w[281], w[159], w[285], pars->GC_11, amp[1210]); 
  FFS2_0(w[281], w[286], w[3], pars->GC_74, amp[1211]); 
  FFV1_0(w[272], w[159], w[287], pars->GC_11, amp[1212]); 
  FFS2_0(w[272], w[286], w[2], pars->GC_74, amp[1213]); 
  FFV1_0(w[281], w[261], w[186], pars->GC_11, amp[1214]); 
  FFS2_0(w[281], w[288], w[3], pars->GC_74, amp[1215]); 
  FFV1_0(w[272], w[261], w[188], pars->GC_11, amp[1216]); 
  FFS2_0(w[272], w[288], w[2], pars->GC_74, amp[1217]); 
  FFV1_0(w[160], w[290], w[291], pars->GC_11, amp[1218]); 
  FFV1_0(w[160], w[192], w[292], pars->GC_11, amp[1219]); 
  FFV1_0(w[194], w[159], w[292], pars->GC_11, amp[1220]); 
  FFV1_0(w[194], w[261], w[291], pars->GC_11, amp[1221]); 
  FFV1_0(w[293], w[159], w[267], pars->GC_11, amp[1222]); 
  FFV1_0(w[289], w[159], w[294], pars->GC_11, amp[1223]); 
  FFV1_0(w[289], w[192], w[267], pars->GC_11, amp[1224]); 
  FFV1_0(w[293], w[261], w[164], pars->GC_11, amp[1225]); 
  FFV1_0(w[289], w[290], w[164], pars->GC_11, amp[1226]); 
  FFV1_0(w[289], w[261], w[195], pars->GC_11, amp[1227]); 
  FFS2_0(w[296], w[192], w[3], pars->GC_74, amp[1228]); 
  FFS2_0(w[194], w[297], w[3], pars->GC_74, amp[1229]); 
  FFV1_0(w[160], w[174], w[298], pars->GC_11, amp[1230]); 
  FFV1_0(w[160], w[204], w[295], pars->GC_11, amp[1231]); 
  FFV1_0(w[194], w[174], w[295], pars->GC_11, amp[1232]); 
  FFV1_0(w[172], w[159], w[298], pars->GC_11, amp[1233]); 
  FFV1_0(w[201], w[159], w[295], pars->GC_11, amp[1234]); 
  FFV1_0(w[172], w[192], w[295], pars->GC_11, amp[1235]); 
  FFS2_0(w[300], w[290], w[3], pars->GC_74, amp[1236]); 
  FFS2_0(w[194], w[301], w[3], pars->GC_74, amp[1237]); 
  FFV1_0(w[160], w[277], w[302], pars->GC_11, amp[1238]); 
  FFV1_0(w[160], w[303], w[299], pars->GC_11, amp[1239]); 
  FFV1_0(w[194], w[277], w[299], pars->GC_11, amp[1240]); 
  FFV1_0(w[172], w[261], w[302], pars->GC_11, amp[1241]); 
  FFV1_0(w[201], w[261], w[299], pars->GC_11, amp[1242]); 
  FFV1_0(w[172], w[290], w[299], pars->GC_11, amp[1243]); 
  FFV1_0(w[304], w[159], w[282], pars->GC_11, amp[1244]); 
  FFV1_0(w[160], w[192], w[305], pars->GC_11, amp[1245]); 
  FFV1_0(w[262], w[192], w[282], pars->GC_11, amp[1246]); 
  FFV1_0(w[194], w[159], w[305], pars->GC_11, amp[1247]); 
  FFV1_0(w[304], w[277], w[164], pars->GC_11, amp[1248]); 
  FFV1_0(w[262], w[303], w[164], pars->GC_11, amp[1249]); 
  FFV1_0(w[262], w[277], w[195], pars->GC_11, amp[1250]); 
  FFV1_0(w[304], w[261], w[180], pars->GC_11, amp[1251]); 
  FFV1_0(w[160], w[290], w[306], pars->GC_11, amp[1252]); 
  FFV1_0(w[262], w[290], w[180], pars->GC_11, amp[1253]); 
  FFV1_0(w[194], w[261], w[306], pars->GC_11, amp[1254]); 
  FFV1_0(w[304], w[174], w[267], pars->GC_11, amp[1255]); 
  FFV1_0(w[262], w[204], w[267], pars->GC_11, amp[1256]); 
  FFV1_0(w[262], w[174], w[294], pars->GC_11, amp[1257]); 
  FFV1_0(w[304], w[159], w[284], pars->GC_11, amp[1258]); 
  FFV1_0(w[304], w[261], w[183], pars->GC_11, amp[1259]); 
  FFV1_0(w[262], w[290], w[183], pars->GC_11, amp[1260]); 
  FFV1_0(w[262], w[192], w[284], pars->GC_11, amp[1261]); 
  FFS2_0(w[304], w[286], w[3], pars->GC_74, amp[1262]); 
  FFS2_0(w[307], w[192], w[3], pars->GC_74, amp[1263]); 
  FFS2_0(w[304], w[288], w[3], pars->GC_74, amp[1264]); 
  FFS2_0(w[308], w[290], w[3], pars->GC_74, amp[1265]); 
  FFV1_0(w[160], w[310], w[311], pars->GC_11, amp[1266]); 
  FFV1_0(w[160], w[208], w[312], pars->GC_11, amp[1267]); 
  FFV1_0(w[210], w[159], w[312], pars->GC_11, amp[1268]); 
  FFV1_0(w[210], w[261], w[311], pars->GC_11, amp[1269]); 
  FFV1_0(w[313], w[159], w[267], pars->GC_11, amp[1270]); 
  FFV1_0(w[309], w[159], w[314], pars->GC_11, amp[1271]); 
  FFV1_0(w[309], w[208], w[267], pars->GC_11, amp[1272]); 
  FFV1_0(w[313], w[261], w[164], pars->GC_11, amp[1273]); 
  FFV1_0(w[309], w[310], w[164], pars->GC_11, amp[1274]); 
  FFV1_0(w[309], w[261], w[211], pars->GC_11, amp[1275]); 
  FFS2_0(w[296], w[208], w[2], pars->GC_74, amp[1276]); 
  FFS2_0(w[210], w[297], w[2], pars->GC_74, amp[1277]); 
  FFV1_0(w[160], w[168], w[315], pars->GC_11, amp[1278]); 
  FFV1_0(w[160], w[215], w[295], pars->GC_11, amp[1279]); 
  FFV1_0(w[210], w[168], w[295], pars->GC_11, amp[1280]); 
  FFV1_0(w[176], w[159], w[315], pars->GC_11, amp[1281]); 
  FFV1_0(w[214], w[159], w[295], pars->GC_11, amp[1282]); 
  FFV1_0(w[176], w[208], w[295], pars->GC_11, amp[1283]); 
  FFS2_0(w[300], w[310], w[2], pars->GC_74, amp[1284]); 
  FFS2_0(w[210], w[301], w[2], pars->GC_74, amp[1285]); 
  FFV1_0(w[160], w[271], w[316], pars->GC_11, amp[1286]); 
  FFV1_0(w[160], w[317], w[299], pars->GC_11, amp[1287]); 
  FFV1_0(w[210], w[271], w[299], pars->GC_11, amp[1288]); 
  FFV1_0(w[176], w[261], w[316], pars->GC_11, amp[1289]); 
  FFV1_0(w[214], w[261], w[299], pars->GC_11, amp[1290]); 
  FFV1_0(w[176], w[310], w[299], pars->GC_11, amp[1291]); 
  FFV1_0(w[318], w[159], w[273], pars->GC_11, amp[1292]); 
  FFV1_0(w[160], w[208], w[319], pars->GC_11, amp[1293]); 
  FFV1_0(w[262], w[208], w[273], pars->GC_11, amp[1294]); 
  FFV1_0(w[210], w[159], w[319], pars->GC_11, amp[1295]); 
  FFV1_0(w[318], w[271], w[164], pars->GC_11, amp[1296]); 
  FFV1_0(w[262], w[317], w[164], pars->GC_11, amp[1297]); 
  FFV1_0(w[262], w[271], w[211], pars->GC_11, amp[1298]); 
  FFV1_0(w[318], w[261], w[169], pars->GC_11, amp[1299]); 
  FFV1_0(w[160], w[310], w[320], pars->GC_11, amp[1300]); 
  FFV1_0(w[262], w[310], w[169], pars->GC_11, amp[1301]); 
  FFV1_0(w[210], w[261], w[320], pars->GC_11, amp[1302]); 
  FFV1_0(w[318], w[168], w[267], pars->GC_11, amp[1303]); 
  FFV1_0(w[262], w[215], w[267], pars->GC_11, amp[1304]); 
  FFV1_0(w[262], w[168], w[314], pars->GC_11, amp[1305]); 
  FFV1_0(w[318], w[159], w[280], pars->GC_11, amp[1306]); 
  FFV1_0(w[318], w[261], w[177], pars->GC_11, amp[1307]); 
  FFV1_0(w[262], w[310], w[177], pars->GC_11, amp[1308]); 
  FFV1_0(w[262], w[208], w[280], pars->GC_11, amp[1309]); 
  FFS2_0(w[318], w[286], w[2], pars->GC_74, amp[1310]); 
  FFS2_0(w[307], w[208], w[2], pars->GC_74, amp[1311]); 
  FFS2_0(w[318], w[288], w[2], pars->GC_74, amp[1312]); 
  FFS2_0(w[308], w[310], w[2], pars->GC_74, amp[1313]); 
  FFV1_0(w[160], w[322], w[311], pars->GC_11, amp[1314]); 
  FFV1_0(w[324], w[159], w[323], pars->GC_11, amp[1315]); 
  VVS3_0(w[323], w[311], w[3], pars->GC_13, amp[1316]); 
  FFV1_0(w[160], w[174], w[325], pars->GC_11, amp[1317]); 
  FFV1_0(w[309], w[174], w[323], pars->GC_11, amp[1318]); 
  FFV1_0(w[172], w[159], w[325], pars->GC_11, amp[1319]); 
  FFV1_0(w[309], w[159], w[326], pars->GC_11, amp[1320]); 
  VVS3_0(w[164], w[325], w[3], pars->GC_13, amp[1321]); 
  FFV1_0(w[309], w[322], w[164], pars->GC_11, amp[1322]); 
  FFS2_0(w[309], w[327], w[3], pars->GC_74, amp[1323]); 
  FFV1_0(w[160], w[328], w[291], pars->GC_11, amp[1324]); 
  FFV1_0(w[329], w[159], w[323], pars->GC_11, amp[1325]); 
  VVS3_0(w[323], w[291], w[2], pars->GC_13, amp[1326]); 
  FFV1_0(w[160], w[168], w[330], pars->GC_11, amp[1327]); 
  FFV1_0(w[289], w[168], w[323], pars->GC_11, amp[1328]); 
  FFV1_0(w[176], w[159], w[330], pars->GC_11, amp[1329]); 
  FFV1_0(w[289], w[159], w[331], pars->GC_11, amp[1330]); 
  VVS3_0(w[164], w[330], w[2], pars->GC_13, amp[1331]); 
  FFV1_0(w[289], w[328], w[164], pars->GC_11, amp[1332]); 
  FFS2_0(w[289], w[327], w[2], pars->GC_74, amp[1333]); 
  FFV1_0(w[160], w[328], w[332], pars->GC_11, amp[1334]); 
  FFS2_0(w[300], w[328], w[3], pars->GC_74, amp[1335]); 
  FFV1_0(w[160], w[322], w[333], pars->GC_11, amp[1336]); 
  FFS2_0(w[300], w[322], w[2], pars->GC_74, amp[1337]); 
  FFS2_0(w[160], w[334], w[8], pars->GC_74, amp[1338]); 
  FFV1_0(w[160], w[335], w[299], pars->GC_11, amp[1339]); 
  VVS3_0(w[299], w[323], w[8], pars->GC_13, amp[1340]); 
  FFS2_0(w[176], w[334], w[3], pars->GC_74, amp[1341]); 
  VVS3_0(w[299], w[331], w[3], pars->GC_13, amp[1342]); 
  FFV1_0(w[176], w[322], w[299], pars->GC_11, amp[1343]); 
  FFS2_0(w[172], w[334], w[2], pars->GC_74, amp[1344]); 
  FFV1_0(w[172], w[328], w[299], pars->GC_11, amp[1345]); 
  VVS3_0(w[299], w[326], w[2], pars->GC_13, amp[1346]); 
  FFV1_0(w[160], w[163], w[336], pars->GC_11, amp[1347]); 
  FFV1_0(w[162], w[159], w[336], pars->GC_11, amp[1348]); 
  FFV1_0(w[337], w[159], w[323], pars->GC_11, amp[1349]); 
  FFV1_0(w[262], w[163], w[323], pars->GC_11, amp[1350]); 
  VVS3_0(w[164], w[336], w[8], pars->GC_13, amp[1351]); 
  FFV1_0(w[262], w[335], w[164], pars->GC_11, amp[1352]); 
  FFS2_0(w[262], w[327], w[8], pars->GC_74, amp[1353]); 
  FFV1_0(w[160], w[170], w[336], pars->GC_11, amp[1354]); 
  VVS3_0(w[336], w[169], w[3], pars->GC_13, amp[1355]); 
  FFV1_0(w[160], w[322], w[320], pars->GC_11, amp[1356]); 
  FFV1_0(w[262], w[322], w[169], pars->GC_11, amp[1357]); 
  VVS3_0(w[323], w[320], w[3], pars->GC_13, amp[1358]); 
  FFV1_0(w[262], w[170], w[323], pars->GC_11, amp[1359]); 
  FFV1_0(w[172], w[168], w[336], pars->GC_11, amp[1360]); 
  FFV1_0(w[262], w[168], w[326], pars->GC_11, amp[1361]); 
  FFV1_0(w[179], w[159], w[336], pars->GC_11, amp[1362]); 
  VVS3_0(w[336], w[177], w[3], pars->GC_13, amp[1363]); 
  FFV1_0(w[262], w[322], w[177], pars->GC_11, amp[1364]); 
  FFV1_0(w[176], w[174], w[336], pars->GC_11, amp[1365]); 
  FFV1_0(w[262], w[174], w[331], pars->GC_11, amp[1366]); 
  FFV1_0(w[160], w[181], w[336], pars->GC_11, amp[1367]); 
  VVS3_0(w[336], w[180], w[2], pars->GC_13, amp[1368]); 
  FFV1_0(w[160], w[328], w[306], pars->GC_11, amp[1369]); 
  FFV1_0(w[262], w[328], w[180], pars->GC_11, amp[1370]); 
  VVS3_0(w[323], w[306], w[2], pars->GC_13, amp[1371]); 
  FFV1_0(w[262], w[181], w[323], pars->GC_11, amp[1372]); 
  FFV1_0(w[185], w[159], w[336], pars->GC_11, amp[1373]); 
  VVS3_0(w[336], w[183], w[2], pars->GC_13, amp[1374]); 
  FFV1_0(w[262], w[328], w[183], pars->GC_11, amp[1375]); 
  FFS2_0(w[308], w[328], w[3], pars->GC_74, amp[1376]); 
  FFV1_0(w[262], w[328], w[186], pars->GC_11, amp[1377]); 
  FFS2_0(w[308], w[322], w[2], pars->GC_74, amp[1378]); 
  FFV1_0(w[262], w[322], w[188], pars->GC_11, amp[1379]); 
  FFV1_0(w[160], w[218], w[312], pars->GC_11, amp[1380]); 
  FFV1_0(w[324], w[261], w[219], pars->GC_11, amp[1381]); 
  VVS3_0(w[219], w[312], w[3], pars->GC_13, amp[1382]); 
  FFV1_0(w[160], w[277], w[338], pars->GC_11, amp[1383]); 
  FFV1_0(w[309], w[277], w[219], pars->GC_11, amp[1384]); 
  FFV1_0(w[172], w[261], w[338], pars->GC_11, amp[1385]); 
  FFV1_0(w[309], w[261], w[221], pars->GC_11, amp[1386]); 
  VVS3_0(w[267], w[338], w[3], pars->GC_13, amp[1387]); 
  FFV1_0(w[309], w[218], w[267], pars->GC_11, amp[1388]); 
  FFS2_0(w[309], w[339], w[3], pars->GC_74, amp[1389]); 
  FFV1_0(w[160], w[223], w[292], pars->GC_11, amp[1390]); 
  FFV1_0(w[329], w[261], w[219], pars->GC_11, amp[1391]); 
  VVS3_0(w[219], w[292], w[2], pars->GC_13, amp[1392]); 
  FFV1_0(w[160], w[271], w[340], pars->GC_11, amp[1393]); 
  FFV1_0(w[289], w[271], w[219], pars->GC_11, amp[1394]); 
  FFV1_0(w[176], w[261], w[340], pars->GC_11, amp[1395]); 
  FFV1_0(w[289], w[261], w[225], pars->GC_11, amp[1396]); 
  VVS3_0(w[267], w[340], w[2], pars->GC_13, amp[1397]); 
  FFV1_0(w[289], w[223], w[267], pars->GC_11, amp[1398]); 
  FFS2_0(w[289], w[339], w[2], pars->GC_74, amp[1399]); 
  FFV1_0(w[160], w[223], w[341], pars->GC_11, amp[1400]); 
  FFS2_0(w[296], w[223], w[3], pars->GC_74, amp[1401]); 
  FFV1_0(w[160], w[218], w[342], pars->GC_11, amp[1402]); 
  FFS2_0(w[296], w[218], w[2], pars->GC_74, amp[1403]); 
  FFS2_0(w[160], w[343], w[8], pars->GC_74, amp[1404]); 
  FFV1_0(w[160], w[227], w[295], pars->GC_11, amp[1405]); 
  VVS3_0(w[295], w[219], w[8], pars->GC_13, amp[1406]); 
  FFS2_0(w[176], w[343], w[3], pars->GC_74, amp[1407]); 
  VVS3_0(w[295], w[225], w[3], pars->GC_13, amp[1408]); 
  FFV1_0(w[176], w[218], w[295], pars->GC_11, amp[1409]); 
  FFS2_0(w[172], w[343], w[2], pars->GC_74, amp[1410]); 
  FFV1_0(w[172], w[223], w[295], pars->GC_11, amp[1411]); 
  VVS3_0(w[295], w[221], w[2], pars->GC_13, amp[1412]); 
  FFV1_0(w[160], w[266], w[344], pars->GC_11, amp[1413]); 
  FFV1_0(w[162], w[261], w[344], pars->GC_11, amp[1414]); 
  FFV1_0(w[337], w[261], w[219], pars->GC_11, amp[1415]); 
  FFV1_0(w[262], w[266], w[219], pars->GC_11, amp[1416]); 
  VVS3_0(w[267], w[344], w[8], pars->GC_13, amp[1417]); 
  FFV1_0(w[262], w[227], w[267], pars->GC_11, amp[1418]); 
  FFS2_0(w[262], w[339], w[8], pars->GC_74, amp[1419]); 
  FFV1_0(w[160], w[274], w[344], pars->GC_11, amp[1420]); 
  VVS3_0(w[344], w[273], w[3], pars->GC_13, amp[1421]); 
  FFV1_0(w[160], w[218], w[319], pars->GC_11, amp[1422]); 
  FFV1_0(w[262], w[218], w[273], pars->GC_11, amp[1423]); 
  VVS3_0(w[219], w[319], w[3], pars->GC_13, amp[1424]); 
  FFV1_0(w[262], w[274], w[219], pars->GC_11, amp[1425]); 
  FFV1_0(w[172], w[271], w[344], pars->GC_11, amp[1426]); 
  FFV1_0(w[262], w[271], w[221], pars->GC_11, amp[1427]); 
  FFV1_0(w[179], w[261], w[344], pars->GC_11, amp[1428]); 
  VVS3_0(w[344], w[280], w[3], pars->GC_13, amp[1429]); 
  FFV1_0(w[262], w[218], w[280], pars->GC_11, amp[1430]); 
  FFV1_0(w[176], w[277], w[344], pars->GC_11, amp[1431]); 
  FFV1_0(w[262], w[277], w[225], pars->GC_11, amp[1432]); 
  FFV1_0(w[160], w[283], w[344], pars->GC_11, amp[1433]); 
  VVS3_0(w[344], w[282], w[2], pars->GC_13, amp[1434]); 
  FFV1_0(w[160], w[223], w[305], pars->GC_11, amp[1435]); 
  FFV1_0(w[262], w[223], w[282], pars->GC_11, amp[1436]); 
  VVS3_0(w[219], w[305], w[2], pars->GC_13, amp[1437]); 
  FFV1_0(w[262], w[283], w[219], pars->GC_11, amp[1438]); 
  FFV1_0(w[185], w[261], w[344], pars->GC_11, amp[1439]); 
  VVS3_0(w[344], w[284], w[2], pars->GC_13, amp[1440]); 
  FFV1_0(w[262], w[223], w[284], pars->GC_11, amp[1441]); 
  FFS2_0(w[307], w[223], w[3], pars->GC_74, amp[1442]); 
  FFV1_0(w[262], w[223], w[285], pars->GC_11, amp[1443]); 
  FFS2_0(w[307], w[218], w[2], pars->GC_74, amp[1444]); 
  FFV1_0(w[262], w[218], w[287], pars->GC_11, amp[1445]); 
  FFV1_0(w[237], w[159], w[312], pars->GC_11, amp[1446]); 
  FFV1_0(w[237], w[261], w[311], pars->GC_11, amp[1447]); 
  FFV1_0(w[324], w[159], w[345], pars->GC_11, amp[1448]); 
  VVS3_0(w[345], w[311], w[3], pars->GC_13, amp[1449]); 
  FFV1_0(w[324], w[261], w[238], pars->GC_11, amp[1450]); 
  VVS3_0(w[238], w[312], w[3], pars->GC_13, amp[1451]); 
  FFV1_0(w[309], w[159], w[346], pars->GC_11, amp[1452]); 
  FFV1_0(w[309], w[277], w[238], pars->GC_11, amp[1453]); 
  FFV1_0(w[309], w[261], w[240], pars->GC_11, amp[1454]); 
  FFV1_0(w[309], w[174], w[345], pars->GC_11, amp[1455]); 
  FFV1_0(w[242], w[159], w[292], pars->GC_11, amp[1456]); 
  FFV1_0(w[242], w[261], w[291], pars->GC_11, amp[1457]); 
  FFV1_0(w[329], w[159], w[345], pars->GC_11, amp[1458]); 
  VVS3_0(w[345], w[291], w[2], pars->GC_13, amp[1459]); 
  FFV1_0(w[329], w[261], w[238], pars->GC_11, amp[1460]); 
  VVS3_0(w[238], w[292], w[2], pars->GC_13, amp[1461]); 
  FFV1_0(w[289], w[159], w[347], pars->GC_11, amp[1462]); 
  FFV1_0(w[289], w[271], w[238], pars->GC_11, amp[1463]); 
  FFV1_0(w[289], w[261], w[243], pars->GC_11, amp[1464]); 
  FFV1_0(w[289], w[168], w[345], pars->GC_11, amp[1465]); 
  FFV1_0(w[242], w[159], w[341], pars->GC_11, amp[1466]); 
  FFS2_0(w[242], w[297], w[3], pars->GC_74, amp[1467]); 
  FFV1_0(w[237], w[159], w[342], pars->GC_11, amp[1468]); 
  FFS2_0(w[237], w[297], w[2], pars->GC_74, amp[1469]); 
  FFS2_0(w[348], w[159], w[8], pars->GC_74, amp[1470]); 
  FFV1_0(w[246], w[159], w[295], pars->GC_11, amp[1471]); 
  VVS3_0(w[295], w[238], w[8], pars->GC_13, amp[1472]); 
  FFS2_0(w[348], w[168], w[3], pars->GC_74, amp[1473]); 
  VVS3_0(w[295], w[243], w[3], pars->GC_13, amp[1474]); 
  FFV1_0(w[237], w[168], w[295], pars->GC_11, amp[1475]); 
  FFS2_0(w[348], w[174], w[2], pars->GC_74, amp[1476]); 
  FFV1_0(w[242], w[174], w[295], pars->GC_11, amp[1477]); 
  VVS3_0(w[295], w[240], w[2], pars->GC_13, amp[1478]); 
  FFV1_0(w[242], w[261], w[332], pars->GC_11, amp[1479]); 
  FFS2_0(w[242], w[301], w[3], pars->GC_74, amp[1480]); 
  FFV1_0(w[237], w[261], w[333], pars->GC_11, amp[1481]); 
  FFS2_0(w[237], w[301], w[2], pars->GC_74, amp[1482]); 
  FFS2_0(w[349], w[261], w[8], pars->GC_74, amp[1483]); 
  FFV1_0(w[246], w[261], w[299], pars->GC_11, amp[1484]); 
  VVS3_0(w[299], w[345], w[8], pars->GC_13, amp[1485]); 
  FFS2_0(w[349], w[271], w[3], pars->GC_74, amp[1486]); 
  VVS3_0(w[299], w[347], w[3], pars->GC_13, amp[1487]); 
  FFV1_0(w[237], w[271], w[299], pars->GC_11, amp[1488]); 
  FFS2_0(w[349], w[277], w[2], pars->GC_74, amp[1489]); 
  FFV1_0(w[242], w[277], w[299], pars->GC_11, amp[1490]); 
  VVS3_0(w[299], w[346], w[2], pars->GC_13, amp[1491]); 
  FFV1_0(w[337], w[159], w[345], pars->GC_11, amp[1492]); 
  FFV1_0(w[262], w[163], w[345], pars->GC_11, amp[1493]); 
  FFV1_0(w[337], w[261], w[238], pars->GC_11, amp[1494]); 
  FFV1_0(w[262], w[266], w[238], pars->GC_11, amp[1495]); 
  FFV1_0(w[237], w[159], w[319], pars->GC_11, amp[1496]); 
  VVS3_0(w[238], w[319], w[3], pars->GC_13, amp[1497]); 
  FFV1_0(w[262], w[274], w[238], pars->GC_11, amp[1498]); 
  FFV1_0(w[262], w[174], w[347], pars->GC_11, amp[1499]); 
  FFV1_0(w[262], w[271], w[240], pars->GC_11, amp[1500]); 
  FFV1_0(w[237], w[261], w[320], pars->GC_11, amp[1501]); 
  VVS3_0(w[345], w[320], w[3], pars->GC_13, amp[1502]); 
  FFV1_0(w[262], w[170], w[345], pars->GC_11, amp[1503]); 
  FFV1_0(w[262], w[277], w[243], pars->GC_11, amp[1504]); 
  FFV1_0(w[262], w[168], w[346], pars->GC_11, amp[1505]); 
  FFV1_0(w[242], w[159], w[305], pars->GC_11, amp[1506]); 
  VVS3_0(w[238], w[305], w[2], pars->GC_13, amp[1507]); 
  FFV1_0(w[262], w[283], w[238], pars->GC_11, amp[1508]); 
  FFV1_0(w[242], w[261], w[306], pars->GC_11, amp[1509]); 
  VVS3_0(w[345], w[306], w[2], pars->GC_13, amp[1510]); 
  FFV1_0(w[262], w[181], w[345], pars->GC_11, amp[1511]); 
  FFV1_0(w[350], w[159], w[282], pars->GC_11, amp[1512]); 
  FFV1_0(w[160], w[351], w[311], pars->GC_11, amp[1513]); 
  VVV1_0(w[4], w[311], w[282], pars->GC_10, amp[1514]); 
  FFV1_0(w[350], w[277], w[164], pars->GC_11, amp[1515]); 
  FFV1_0(w[309], w[351], w[164], pars->GC_11, amp[1516]); 
  FFV1_0(w[309], w[277], w[250], pars->GC_11, amp[1517]); 
  FFV1_0(w[350], w[261], w[180], pars->GC_11, amp[1518]); 
  FFV1_0(w[160], w[248], w[312], pars->GC_11, amp[1519]); 
  VVV1_0(w[4], w[312], w[180], pars->GC_10, amp[1520]); 
  FFV1_0(w[350], w[174], w[267], pars->GC_11, amp[1521]); 
  FFV1_0(w[309], w[248], w[267], pars->GC_11, amp[1522]); 
  FFV1_0(w[309], w[174], w[352], pars->GC_11, amp[1523]); 
  FFV1_0(w[350], w[159], w[284], pars->GC_11, amp[1524]); 
  FFV1_0(w[350], w[261], w[183], pars->GC_11, amp[1525]); 
  FFV1_0(w[251], w[159], w[312], pars->GC_11, amp[1526]); 
  FFV1_0(w[251], w[261], w[311], pars->GC_11, amp[1527]); 
  VVV1_0(w[4], w[312], w[183], pars->GC_10, amp[1528]); 
  VVV1_0(w[4], w[311], w[284], pars->GC_10, amp[1529]); 
  FFV1_0(w[350], w[159], w[285], pars->GC_11, amp[1530]); 
  FFS2_0(w[350], w[286], w[3], pars->GC_74, amp[1531]); 
  FFV1_0(w[324], w[159], w[352], pars->GC_11, amp[1532]); 
  VVS3_0(w[352], w[311], w[3], pars->GC_13, amp[1533]); 
  FFV1_0(w[324], w[286], w[4], pars->GC_11, amp[1534]); 
  VVV1_0(w[4], w[311], w[285], pars->GC_10, amp[1535]); 
  FFV1_0(w[309], w[159], w[353], pars->GC_11, amp[1536]); 
  FFV1_0(w[350], w[261], w[186], pars->GC_11, amp[1537]); 
  FFS2_0(w[350], w[288], w[3], pars->GC_74, amp[1538]); 
  FFV1_0(w[324], w[261], w[250], pars->GC_11, amp[1539]); 
  VVS3_0(w[250], w[312], w[3], pars->GC_13, amp[1540]); 
  FFV1_0(w[324], w[288], w[4], pars->GC_11, amp[1541]); 
  VVV1_0(w[4], w[312], w[186], pars->GC_10, amp[1542]); 
  FFV1_0(w[309], w[261], w[252], pars->GC_11, amp[1543]); 
  FFV1_0(w[354], w[159], w[273], pars->GC_11, amp[1544]); 
  FFV1_0(w[160], w[355], w[291], pars->GC_11, amp[1545]); 
  VVV1_0(w[4], w[291], w[273], pars->GC_10, amp[1546]); 
  FFV1_0(w[354], w[271], w[164], pars->GC_11, amp[1547]); 
  FFV1_0(w[289], w[355], w[164], pars->GC_11, amp[1548]); 
  FFV1_0(w[289], w[271], w[250], pars->GC_11, amp[1549]); 
  FFV1_0(w[354], w[261], w[169], pars->GC_11, amp[1550]); 
  FFV1_0(w[160], w[254], w[292], pars->GC_11, amp[1551]); 
  VVV1_0(w[4], w[292], w[169], pars->GC_10, amp[1552]); 
  FFV1_0(w[354], w[168], w[267], pars->GC_11, amp[1553]); 
  FFV1_0(w[289], w[254], w[267], pars->GC_11, amp[1554]); 
  FFV1_0(w[289], w[168], w[352], pars->GC_11, amp[1555]); 
  FFV1_0(w[354], w[159], w[280], pars->GC_11, amp[1556]); 
  FFV1_0(w[354], w[261], w[177], pars->GC_11, amp[1557]); 
  FFV1_0(w[255], w[159], w[292], pars->GC_11, amp[1558]); 
  FFV1_0(w[255], w[261], w[291], pars->GC_11, amp[1559]); 
  VVV1_0(w[4], w[292], w[177], pars->GC_10, amp[1560]); 
  VVV1_0(w[4], w[291], w[280], pars->GC_10, amp[1561]); 
  FFV1_0(w[354], w[159], w[287], pars->GC_11, amp[1562]); 
  FFS2_0(w[354], w[286], w[2], pars->GC_74, amp[1563]); 
  FFV1_0(w[329], w[159], w[352], pars->GC_11, amp[1564]); 
  VVS3_0(w[352], w[291], w[2], pars->GC_13, amp[1565]); 
  FFV1_0(w[329], w[286], w[4], pars->GC_11, amp[1566]); 
  VVV1_0(w[4], w[291], w[287], pars->GC_10, amp[1567]); 
  FFV1_0(w[289], w[159], w[356], pars->GC_11, amp[1568]); 
  FFV1_0(w[354], w[261], w[188], pars->GC_11, amp[1569]); 
  FFS2_0(w[354], w[288], w[2], pars->GC_74, amp[1570]); 
  FFV1_0(w[329], w[261], w[250], pars->GC_11, amp[1571]); 
  VVS3_0(w[250], w[292], w[2], pars->GC_13, amp[1572]); 
  FFV1_0(w[329], w[288], w[4], pars->GC_11, amp[1573]); 
  VVV1_0(w[4], w[292], w[188], pars->GC_10, amp[1574]); 
  FFV1_0(w[289], w[261], w[256], pars->GC_11, amp[1575]); 
  FFV1_0(w[160], w[163], w[357], pars->GC_11, amp[1576]); 
  FFV1_0(w[162], w[159], w[357], pars->GC_11, amp[1577]); 
  FFV1_0(w[160], w[297], w[153], pars->GC_11, amp[1578]); 
  FFV1_0(w[296], w[159], w[153], pars->GC_11, amp[1579]); 
  FFV1_0(w[162], w[297], w[4], pars->GC_11, amp[1580]); 
  FFV1_0(w[296], w[163], w[4], pars->GC_11, amp[1581]); 
  VVVS2_0(w[4], w[295], w[164], w[8], pars->GC_14, amp[1582]); 
  VVS3_0(w[164], w[357], w[8], pars->GC_13, amp[1583]); 
  VVV1_0(w[295], w[164], w[153], pars->GC_10, amp[1584]); 
  VVS3_0(w[295], w[250], w[8], pars->GC_13, amp[1585]); 
  FFV1_0(w[160], w[170], w[357], pars->GC_11, amp[1586]); 
  VVS3_0(w[357], w[169], w[3], pars->GC_13, amp[1587]); 
  FFV1_0(w[160], w[254], w[341], pars->GC_11, amp[1588]); 
  FFS2_0(w[296], w[254], w[3], pars->GC_74, amp[1589]); 
  VVV1_0(w[4], w[341], w[169], pars->GC_10, amp[1590]); 
  FFV1_0(w[296], w[170], w[4], pars->GC_11, amp[1591]); 
  FFV1_0(w[160], w[168], w[358], pars->GC_11, amp[1592]); 
  FFV1_0(w[172], w[168], w[357], pars->GC_11, amp[1593]); 
  FFV1_0(w[172], w[254], w[295], pars->GC_11, amp[1594]); 
  FFV1_0(w[251], w[168], w[295], pars->GC_11, amp[1595]); 
  FFV1_0(w[179], w[159], w[357], pars->GC_11, amp[1596]); 
  VVS3_0(w[357], w[177], w[3], pars->GC_13, amp[1597]); 
  FFV1_0(w[255], w[159], w[341], pars->GC_11, amp[1598]); 
  FFS2_0(w[255], w[297], w[3], pars->GC_74, amp[1599]); 
  VVV1_0(w[4], w[341], w[177], pars->GC_10, amp[1600]); 
  FFV1_0(w[179], w[297], w[4], pars->GC_11, amp[1601]); 
  FFV1_0(w[176], w[159], w[358], pars->GC_11, amp[1602]); 
  FFV1_0(w[176], w[174], w[357], pars->GC_11, amp[1603]); 
  FFV1_0(w[255], w[174], w[295], pars->GC_11, amp[1604]); 
  FFV1_0(w[176], w[248], w[295], pars->GC_11, amp[1605]); 
  FFV1_0(w[160], w[181], w[357], pars->GC_11, amp[1606]); 
  VVS3_0(w[357], w[180], w[2], pars->GC_13, amp[1607]); 
  FFV1_0(w[160], w[248], w[342], pars->GC_11, amp[1608]); 
  FFS2_0(w[296], w[248], w[2], pars->GC_74, amp[1609]); 
  VVV1_0(w[4], w[342], w[180], pars->GC_10, amp[1610]); 
  FFV1_0(w[296], w[181], w[4], pars->GC_11, amp[1611]); 
  FFV1_0(w[160], w[174], w[359], pars->GC_11, amp[1612]); 
  FFV1_0(w[185], w[159], w[357], pars->GC_11, amp[1613]); 
  VVS3_0(w[357], w[183], w[2], pars->GC_13, amp[1614]); 
  FFV1_0(w[251], w[159], w[342], pars->GC_11, amp[1615]); 
  FFS2_0(w[251], w[297], w[2], pars->GC_74, amp[1616]); 
  VVV1_0(w[4], w[342], w[183], pars->GC_10, amp[1617]); 
  FFV1_0(w[185], w[297], w[4], pars->GC_11, amp[1618]); 
  FFV1_0(w[172], w[159], w[359], pars->GC_11, amp[1619]); 
  FFV1_0(w[160], w[266], w[360], pars->GC_11, amp[1620]); 
  FFV1_0(w[162], w[261], w[360], pars->GC_11, amp[1621]); 
  FFV1_0(w[160], w[301], w[153], pars->GC_11, amp[1622]); 
  FFV1_0(w[300], w[261], w[153], pars->GC_11, amp[1623]); 
  FFV1_0(w[162], w[301], w[4], pars->GC_11, amp[1624]); 
  FFV1_0(w[300], w[266], w[4], pars->GC_11, amp[1625]); 
  VVVS2_0(w[4], w[299], w[267], w[8], pars->GC_14, amp[1626]); 
  VVS3_0(w[267], w[360], w[8], pars->GC_13, amp[1627]); 
  VVV1_0(w[299], w[267], w[153], pars->GC_10, amp[1628]); 
  VVS3_0(w[299], w[352], w[8], pars->GC_13, amp[1629]); 
  FFV1_0(w[160], w[274], w[360], pars->GC_11, amp[1630]); 
  VVS3_0(w[360], w[273], w[3], pars->GC_13, amp[1631]); 
  FFV1_0(w[160], w[355], w[332], pars->GC_11, amp[1632]); 
  FFS2_0(w[300], w[355], w[3], pars->GC_74, amp[1633]); 
  VVV1_0(w[4], w[332], w[273], pars->GC_10, amp[1634]); 
  FFV1_0(w[300], w[274], w[4], pars->GC_11, amp[1635]); 
  FFV1_0(w[160], w[271], w[361], pars->GC_11, amp[1636]); 
  FFV1_0(w[172], w[271], w[360], pars->GC_11, amp[1637]); 
  FFV1_0(w[172], w[355], w[299], pars->GC_11, amp[1638]); 
  FFV1_0(w[251], w[271], w[299], pars->GC_11, amp[1639]); 
  FFV1_0(w[179], w[261], w[360], pars->GC_11, amp[1640]); 
  VVS3_0(w[360], w[280], w[3], pars->GC_13, amp[1641]); 
  FFV1_0(w[255], w[261], w[332], pars->GC_11, amp[1642]); 
  FFS2_0(w[255], w[301], w[3], pars->GC_74, amp[1643]); 
  VVV1_0(w[4], w[332], w[280], pars->GC_10, amp[1644]); 
  FFV1_0(w[179], w[301], w[4], pars->GC_11, amp[1645]); 
  FFV1_0(w[176], w[261], w[361], pars->GC_11, amp[1646]); 
  FFV1_0(w[176], w[277], w[360], pars->GC_11, amp[1647]); 
  FFV1_0(w[255], w[277], w[299], pars->GC_11, amp[1648]); 
  FFV1_0(w[176], w[351], w[299], pars->GC_11, amp[1649]); 
  FFV1_0(w[160], w[283], w[360], pars->GC_11, amp[1650]); 
  VVS3_0(w[360], w[282], w[2], pars->GC_13, amp[1651]); 
  FFV1_0(w[160], w[351], w[333], pars->GC_11, amp[1652]); 
  FFS2_0(w[300], w[351], w[2], pars->GC_74, amp[1653]); 
  VVV1_0(w[4], w[333], w[282], pars->GC_10, amp[1654]); 
  FFV1_0(w[300], w[283], w[4], pars->GC_11, amp[1655]); 
  FFV1_0(w[160], w[277], w[362], pars->GC_11, amp[1656]); 
  FFV1_0(w[185], w[261], w[360], pars->GC_11, amp[1657]); 
  VVS3_0(w[360], w[284], w[2], pars->GC_13, amp[1658]); 
  FFV1_0(w[251], w[261], w[333], pars->GC_11, amp[1659]); 
  FFS2_0(w[251], w[301], w[2], pars->GC_74, amp[1660]); 
  VVV1_0(w[4], w[333], w[284], pars->GC_10, amp[1661]); 
  FFV1_0(w[185], w[301], w[4], pars->GC_11, amp[1662]); 
  FFV1_0(w[172], w[261], w[362], pars->GC_11, amp[1663]); 
  FFV1_0(w[307], w[159], w[153], pars->GC_11, amp[1664]); 
  FFV1_0(w[262], w[286], w[153], pars->GC_11, amp[1665]); 
  FFV1_0(w[337], w[159], w[352], pars->GC_11, amp[1666]); 
  FFV1_0(w[262], w[163], w[352], pars->GC_11, amp[1667]); 
  FFV1_0(w[337], w[286], w[4], pars->GC_11, amp[1668]); 
  FFV1_0(w[307], w[163], w[4], pars->GC_11, amp[1669]); 
  FFV1_0(w[308], w[261], w[153], pars->GC_11, amp[1670]); 
  FFV1_0(w[262], w[288], w[153], pars->GC_11, amp[1671]); 
  FFV1_0(w[337], w[261], w[250], pars->GC_11, amp[1672]); 
  FFV1_0(w[262], w[266], w[250], pars->GC_11, amp[1673]); 
  FFV1_0(w[337], w[288], w[4], pars->GC_11, amp[1674]); 
  FFV1_0(w[308], w[266], w[4], pars->GC_11, amp[1675]); 
  FFV1_0(w[160], w[355], w[306], pars->GC_11, amp[1676]); 
  FFV1_0(w[262], w[355], w[180], pars->GC_11, amp[1677]); 
  FFV1_0(w[160], w[248], w[319], pars->GC_11, amp[1678]); 
  FFV1_0(w[262], w[248], w[273], pars->GC_11, amp[1679]); 
  VVV1_0(w[4], w[319], w[180], pars->GC_10, amp[1680]); 
  VVV1_0(w[4], w[306], w[273], pars->GC_10, amp[1681]); 
  FFV1_0(w[262], w[355], w[183], pars->GC_11, amp[1682]); 
  FFV1_0(w[251], w[159], w[319], pars->GC_11, amp[1683]); 
  VVV1_0(w[4], w[319], w[183], pars->GC_10, amp[1684]); 
  FFS2_0(w[308], w[355], w[3], pars->GC_74, amp[1685]); 
  FFV1_0(w[262], w[355], w[186], pars->GC_11, amp[1686]); 
  VVS3_0(w[250], w[319], w[3], pars->GC_13, amp[1687]); 
  FFV1_0(w[262], w[274], w[250], pars->GC_11, amp[1688]); 
  VVV1_0(w[4], w[319], w[186], pars->GC_10, amp[1689]); 
  FFV1_0(w[308], w[274], w[4], pars->GC_11, amp[1690]); 
  FFV1_0(w[262], w[271], w[252], pars->GC_11, amp[1691]); 
  FFV1_0(w[160], w[254], w[305], pars->GC_11, amp[1692]); 
  FFV1_0(w[262], w[254], w[282], pars->GC_11, amp[1693]); 
  FFV1_0(w[160], w[351], w[320], pars->GC_11, amp[1694]); 
  FFV1_0(w[262], w[351], w[169], pars->GC_11, amp[1695]); 
  VVV1_0(w[4], w[320], w[282], pars->GC_10, amp[1696]); 
  VVV1_0(w[4], w[305], w[169], pars->GC_10, amp[1697]); 
  FFV1_0(w[262], w[254], w[284], pars->GC_11, amp[1698]); 
  FFV1_0(w[251], w[261], w[320], pars->GC_11, amp[1699]); 
  VVV1_0(w[4], w[320], w[284], pars->GC_10, amp[1700]); 
  FFS2_0(w[307], w[254], w[3], pars->GC_74, amp[1701]); 
  FFV1_0(w[262], w[254], w[285], pars->GC_11, amp[1702]); 
  VVS3_0(w[352], w[320], w[3], pars->GC_13, amp[1703]); 
  FFV1_0(w[262], w[170], w[352], pars->GC_11, amp[1704]); 
  VVV1_0(w[4], w[320], w[285], pars->GC_10, amp[1705]); 
  FFV1_0(w[307], w[170], w[4], pars->GC_11, amp[1706]); 
  FFV1_0(w[262], w[168], w[353], pars->GC_11, amp[1707]); 
  FFV1_0(w[255], w[159], w[305], pars->GC_11, amp[1708]); 
  FFV1_0(w[262], w[351], w[177], pars->GC_11, amp[1709]); 
  VVV1_0(w[4], w[305], w[177], pars->GC_10, amp[1710]); 
  FFV1_0(w[255], w[261], w[306], pars->GC_11, amp[1711]); 
  FFV1_0(w[262], w[248], w[280], pars->GC_11, amp[1712]); 
  VVV1_0(w[4], w[306], w[280], pars->GC_10, amp[1713]); 
  FFS2_0(w[308], w[351], w[2], pars->GC_74, amp[1714]); 
  FFV1_0(w[262], w[351], w[188], pars->GC_11, amp[1715]); 
  VVS3_0(w[250], w[305], w[2], pars->GC_13, amp[1716]); 
  FFV1_0(w[262], w[283], w[250], pars->GC_11, amp[1717]); 
  VVV1_0(w[4], w[305], w[188], pars->GC_10, amp[1718]); 
  FFV1_0(w[308], w[283], w[4], pars->GC_11, amp[1719]); 
  FFV1_0(w[262], w[277], w[256], pars->GC_11, amp[1720]); 
  FFS2_0(w[307], w[248], w[2], pars->GC_74, amp[1721]); 
  FFV1_0(w[262], w[248], w[287], pars->GC_11, amp[1722]); 
  VVS3_0(w[352], w[306], w[2], pars->GC_13, amp[1723]); 
  FFV1_0(w[262], w[181], w[352], pars->GC_11, amp[1724]); 
  VVV1_0(w[4], w[306], w[287], pars->GC_10, amp[1725]); 
  FFV1_0(w[307], w[181], w[4], pars->GC_11, amp[1726]); 
  FFV1_0(w[262], w[174], w[356], pars->GC_11, amp[1727]); 


}
double PY8MEs_R8_P4_heft_bb_hhgbb::matrix_8_bb_hhgbb() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 576;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[0] + 1./3. * amp[1] + amp[2] + amp[3] +
      amp[4] + amp[5] + amp[6] + 1./3. * amp[7] + 1./3. * amp[8] + 1./3. *
      amp[9] + amp[10] + amp[11] + amp[12] + 1./3. * amp[13] + amp[14] + 1./3.
      * amp[15] + amp[16] + 1./3. * amp[17] + 1./3. * amp[18] + 1./3. * amp[19]
      + 1./3. * amp[20] + 1./3. * amp[21] + 1./3. * amp[22] + amp[23] + 1./3. *
      amp[24] + amp[25] + 1./3. * amp[26] + amp[27] + amp[28] + amp[29] +
      amp[30] + 1./3. * amp[31] + 1./3. * amp[32] + 1./3. * amp[33] + amp[34] +
      amp[35] + 1./3. * amp[36] + amp[37] + amp[38] + 1./3. * amp[39] + amp[40]
      + amp[41] + amp[42] + 1./3. * amp[43] + 1./3. * amp[44] + 1./3. * amp[45]
      + 1./3. * amp[46] + 1./3. * amp[47] + 1./3. * amp[48] + amp[49] + amp[50]
      + amp[51] + amp[52] + 1./3. * amp[53] + 1./3. * amp[54] + 1./3. * amp[55]
      + amp[56] + amp[57] + amp[58] + amp[59] + amp[60] + amp[61] + 1./3. *
      amp[62] + 1./3. * amp[63] + 1./3. * amp[64] + 1./3. * amp[65] + amp[66] +
      amp[70] + Complex<double> (0, 1) * amp[71] + 1./3. * amp[73] + 1./3. *
      amp[74] + amp[84] - Complex<double> (0, 1) * amp[86] + amp[87] -
      Complex<double> (0, 1) * amp[89] + amp[91] + amp[92] + 1./3. * amp[96] +
      1./3. * amp[97] + 1./3. * amp[99] + amp[100] + 1./3. * amp[101] +
      amp[103] + Complex<double> (0, 1) * amp[105] + amp[106] + 1./3. *
      amp[107] + 1./3. * amp[108] + amp[110] + 1./3. * amp[112] + 1./3. *
      amp[113] + amp[114] + amp[118] + Complex<double> (0, 1) * amp[119] +
      1./3. * amp[121] + 1./3. * amp[122] + amp[132] - Complex<double> (0, 1) *
      amp[134] + amp[135] - Complex<double> (0, 1) * amp[137] + amp[139] +
      amp[140] + 1./3. * amp[144] + 1./3. * amp[145] + 1./3. * amp[147] +
      amp[148] + 1./3. * amp[149] + amp[151] + Complex<double> (0, 1) *
      amp[153] + amp[154] + 1./3. * amp[155] + 1./3. * amp[156] + amp[158] +
      1./3. * amp[160] + 1./3. * amp[161] + amp[162] + amp[163] + amp[164] +
      1./3. * amp[165] + amp[166] + 1./3. * amp[167] + amp[168] + 1./3. *
      amp[169] + 1./3. * amp[170] + 1./3. * amp[171] + amp[172] + amp[173] +
      amp[174] + 1./3. * amp[175] + amp[176] + 1./3. * amp[177] + amp[178] +
      1./3. * amp[179] + 1./3. * amp[180] + 1./3. * amp[181] + amp[182] +
      amp[183] + amp[184] + amp[185] + amp[186] + amp[187] + amp[188] +
      amp[189] + amp[190] + amp[191] + amp[192] + amp[193] + amp[194] + 1./3. *
      amp[195] + 1./3. * amp[196] + amp[197] + amp[198] + 1./3. * amp[199] +
      1./3. * amp[200] + 1./3. * amp[201] + 1./3. * amp[202] + 1./3. * amp[203]
      + amp[204] + 1./3. * amp[205] + amp[206] + amp[207] + 1./3. * amp[208] +
      amp[209] + 1./3. * amp[210] + 1./3. * amp[211] + 1./3. * amp[212] + 1./3.
      * amp[213] + amp[214] + 1./3. * amp[215] + 1./3. * amp[216] + amp[217] +
      1./3. * amp[218] + amp[219] + amp[220] + 1./3. * amp[221] + 1./3. *
      amp[222] + 1./3. * amp[223] + 1./3. * amp[224] + 1./3. * amp[225] + 1./3.
      * amp[226] + 1./3. * amp[227] + amp[360] + amp[361] - Complex<double> (0,
      1) * amp[362] + 1./3. * amp[363] + 1./3. * amp[364] + 1./3. * amp[366] +
      amp[369] + Complex<double> (0, 1) * amp[371] + amp[372] + 1./3. *
      amp[373] - Complex<double> (0, 1) * amp[377] + amp[378] + amp[379] +
      Complex<double> (0, 1) * amp[380] + Complex<double> (0, 1) * amp[381] +
      amp[382] - Complex<double> (0, 1) * amp[383] + Complex<double> (0, 1) *
      amp[384] + 1./3. * amp[385] + 1./3. * amp[386] + 1./3. * amp[389] +
      amp[392] + amp[393] - Complex<double> (0, 1) * amp[394] + 1./3. *
      amp[395] + 1./3. * amp[396] + 1./3. * amp[398] + amp[401] +
      Complex<double> (0, 1) * amp[403] + amp[404] + 1./3. * amp[405] -
      Complex<double> (0, 1) * amp[409] + amp[410] + amp[411] + Complex<double>
      (0, 1) * amp[412] + Complex<double> (0, 1) * amp[413] + amp[414] -
      Complex<double> (0, 1) * amp[415] + Complex<double> (0, 1) * amp[416] +
      1./3. * amp[417] + 1./3. * amp[418] + 1./3. * amp[421] - Complex<double>
      (0, 1) * amp[468] - Complex<double> (0, 1) * amp[469] + amp[471] +
      amp[473] - Complex<double> (0, 1) * amp[474] - Complex<double> (0, 1) *
      amp[475] - Complex<double> (0, 1) * amp[476] + Complex<double> (0, 1) *
      amp[477] - Complex<double> (0, 1) * amp[478] - Complex<double> (0, 1) *
      amp[479] + amp[480] + amp[481] - Complex<double> (0, 1) * amp[482] +
      amp[483] - Complex<double> (0, 1) * amp[484] - Complex<double> (0, 1) *
      amp[485] + amp[486] - Complex<double> (0, 1) * amp[488] - Complex<double>
      (0, 1) * amp[489] - Complex<double> (0, 1) * amp[492] - Complex<double>
      (0, 1) * amp[494] - Complex<double> (0, 1) * amp[495] + amp[497] -
      Complex<double> (0, 1) * amp[498] - Complex<double> (0, 1) * amp[499] +
      amp[500] + amp[501] - Complex<double> (0, 1) * amp[502] + amp[503] -
      Complex<double> (0, 1) * amp[504] - Complex<double> (0, 1) * amp[505] -
      Complex<double> (0, 1) * amp[506] - Complex<double> (0, 1) * amp[509] -
      Complex<double> (0, 1) * amp[511] + amp[513] + Complex<double> (0, 1) *
      amp[514] + Complex<double> (0, 1) * amp[515] + amp[516] + 1./3. *
      amp[518] + 1./3. * amp[519] + 1./3. * amp[522] + 1./3. * amp[523] +
      amp[524] + 1./3. * amp[525] - Complex<double> (0, 1) * amp[529] + 1./3. *
      amp[530] + 1./3. * amp[533] + 1./3. * amp[534] + 1./3. * amp[538] +
      amp[542] + 1./3. * amp[543] - Complex<double> (0, 1) * amp[544] -
      Complex<double> (0, 1) * amp[548] + Complex<double> (0, 1) * amp[551] +
      Complex<double> (0, 1) * amp[552] - Complex<double> (0, 1) * amp[553] +
      Complex<double> (0, 1) * amp[555] + 1./3. * amp[557] - Complex<double>
      (0, 1) * amp[561] + 1./3. * amp[562] + 1./3. * amp[563] + 1./3. *
      amp[567] + Complex<double> (0, 1) * amp[571] + Complex<double> (0, 1) *
      amp[572] - Complex<double> (0, 1) * amp[573] + Complex<double> (0, 1) *
      amp[575]);
  jamp[1] = +1./2. * (-1./3. * amp[66] - amp[68] - 1./3. * amp[69] - amp[74] +
      Complex<double> (0, 1) * amp[75] - amp[77] - Complex<double> (0, 1) *
      amp[78] - amp[80] - Complex<double> (0, 1) * amp[81] - amp[82] - 1./3. *
      amp[84] - 1./3. * amp[85] - 1./3. * amp[87] - 1./3. * amp[88] - 1./3. *
      amp[90] - 1./3. * amp[91] - amp[95] - amp[97] + Complex<double> (0, 1) *
      amp[98] - 1./3. * amp[100] - amp[101] - 1./3. * amp[102] - amp[108] -
      amp[113] - 1./3. * amp[114] - amp[116] - 1./3. * amp[117] - amp[122] +
      Complex<double> (0, 1) * amp[123] - amp[125] - Complex<double> (0, 1) *
      amp[126] - amp[128] - Complex<double> (0, 1) * amp[129] - amp[130] -
      1./3. * amp[132] - 1./3. * amp[133] - 1./3. * amp[135] - 1./3. * amp[136]
      - 1./3. * amp[138] - 1./3. * amp[139] - amp[143] - amp[145] +
      Complex<double> (0, 1) * amp[146] - 1./3. * amp[148] - amp[149] - 1./3. *
      amp[150] - amp[156] - amp[161] - 1./3. * amp[162] - 1./3. * amp[163] -
      1./3. * amp[164] - amp[165] - 1./3. * amp[166] - amp[167] - 1./3. *
      amp[168] - amp[169] - amp[170] - amp[171] - 1./3. * amp[172] - 1./3. *
      amp[173] - 1./3. * amp[174] - amp[175] - 1./3. * amp[176] - amp[177] -
      1./3. * amp[178] - amp[179] - amp[180] - amp[181] - 1./3. * amp[182] -
      1./3. * amp[183] - 1./3. * amp[184] - 1./3. * amp[185] - 1./3. * amp[186]
      - 1./3. * amp[187] - 1./3. * amp[188] - 1./3. * amp[189] - 1./3. *
      amp[190] - 1./3. * amp[191] - 1./3. * amp[192] - 1./3. * amp[193] - 1./3.
      * amp[194] - amp[195] - amp[196] - 1./3. * amp[197] - 1./3. * amp[198] -
      amp[199] - amp[200] - amp[201] - amp[202] - amp[203] - 1./3. * amp[204] -
      amp[205] - 1./3. * amp[206] - 1./3. * amp[207] - amp[208] - 1./3. *
      amp[209] - amp[210] - amp[211] - amp[212] - amp[213] - 1./3. * amp[214] -
      amp[215] - amp[216] - 1./3. * amp[217] - amp[218] - 1./3. * amp[219] -
      1./3. * amp[220] - amp[221] - amp[222] - amp[223] - amp[224] - amp[225] -
      amp[226] - amp[227] - amp[294] - 1./3. * amp[295] - 1./3. * amp[296] -
      1./3. * amp[297] - amp[298] - amp[299] - 1./3. * amp[300] - amp[301] -
      amp[302] - 1./3. * amp[303] - amp[304] - 1./3. * amp[305] - 1./3. *
      amp[306] - 1./3. * amp[307] - amp[308] - amp[309] - 1./3. * amp[310] -
      amp[311] - amp[312] - 1./3. * amp[313] - amp[314] - amp[315] - amp[316] -
      amp[317] - amp[318] - amp[319] - amp[320] - amp[321] - amp[322] -
      amp[323] - amp[324] - amp[325] - amp[326] - 1./3. * amp[327] - 1./3. *
      amp[328] - 1./3. * amp[329] - 1./3. * amp[330] - 1./3. * amp[331] - 1./3.
      * amp[332] - 1./3. * amp[333] - 1./3. * amp[334] - 1./3. * amp[335] -
      1./3. * amp[336] - 1./3. * amp[337] - 1./3. * amp[338] - 1./3. * amp[339]
      - 1./3. * amp[340] - 1./3. * amp[341] - amp[342] - amp[343] - amp[344] -
      amp[345] - amp[346] - 1./3. * amp[347] - amp[348] - 1./3. * amp[349] -
      1./3. * amp[350] - 1./3. * amp[351] - amp[352] - 1./3. * amp[353] -
      amp[354] - amp[355] - amp[356] - 1./3. * amp[357] - 1./3. * amp[358] -
      1./3. * amp[359] - 1./3. * amp[361] - amp[364] + Complex<double> (0, 1) *
      amp[365] - Complex<double> (0, 1) * amp[368] - amp[374] - 1./3. *
      amp[375] - Complex<double> (0, 1) * amp[376] + Complex<double> (0, 1) *
      amp[387] + Complex<double> (0, 1) * amp[388] - Complex<double> (0, 1) *
      amp[390] + Complex<double> (0, 1) * amp[391] - 1./3. * amp[393] -
      amp[396] + Complex<double> (0, 1) * amp[397] - Complex<double> (0, 1) *
      amp[400] - amp[406] - 1./3. * amp[407] - Complex<double> (0, 1) *
      amp[408] + Complex<double> (0, 1) * amp[419] + Complex<double> (0, 1) *
      amp[420] - Complex<double> (0, 1) * amp[422] + Complex<double> (0, 1) *
      amp[423] - Complex<double> (0, 1) * amp[424] - Complex<double> (0, 1) *
      amp[425] - amp[426] - amp[428] - Complex<double> (0, 1) * amp[430] -
      Complex<double> (0, 1) * amp[431] - Complex<double> (0, 1) * amp[432] +
      Complex<double> (0, 1) * amp[433] - Complex<double> (0, 1) * amp[434] -
      Complex<double> (0, 1) * amp[435] - Complex<double> (0, 1) * amp[438] -
      Complex<double> (0, 1) * amp[440] - Complex<double> (0, 1) * amp[441] -
      amp[443] - Complex<double> (0, 1) * amp[444] - Complex<double> (0, 1) *
      amp[445] - amp[446] - amp[447] - Complex<double> (0, 1) * amp[448] -
      amp[449] - Complex<double> (0, 1) * amp[450] - Complex<double> (0, 1) *
      amp[451] - amp[452] - Complex<double> (0, 1) * amp[454] - Complex<double>
      (0, 1) * amp[455] - Complex<double> (0, 1) * amp[458] - Complex<double>
      (0, 1) * amp[460] - Complex<double> (0, 1) * amp[461] - Complex<double>
      (0, 1) * amp[462] - amp[463] - amp[464] - Complex<double> (0, 1) *
      amp[465] - amp[466] - Complex<double> (0, 1) * amp[467] - 1./3. *
      amp[470] - 1./3. * amp[471] - 1./3. * amp[472] - 1./3. * amp[473] - 1./3.
      * amp[480] - 1./3. * amp[481] - 1./3. * amp[483] - 1./3. * amp[486] -
      1./3. * amp[487] - 1./3. * amp[490] - 1./3. * amp[491] - 1./3. * amp[493]
      - 1./3. * amp[496] - 1./3. * amp[497] - 1./3. * amp[500] - 1./3. *
      amp[501] - 1./3. * amp[503] - 1./3. * amp[507] - 1./3. * amp[508] - 1./3.
      * amp[510] - amp[518] + Complex<double> (0, 1) * amp[520] +
      Complex<double> (0, 1) * amp[521] - amp[523] - 1./3. * amp[524] -
      amp[525] - Complex<double> (0, 1) * amp[528] - amp[530] - amp[531] -
      Complex<double> (0, 1) * amp[532] - amp[533] - amp[534] + Complex<double>
      (0, 1) * amp[535] + Complex<double> (0, 1) * amp[536] - Complex<double>
      (0, 1) * amp[537] - amp[538] + Complex<double> (0, 1) * amp[539] - 1./3.
      * amp[542] - amp[543] - Complex<double> (0, 1) * amp[545] - 1./3. *
      amp[547] - amp[556] - amp[557] - Complex<double> (0, 1) * amp[558] -
      1./3. * amp[559] - amp[562] - amp[563] + Complex<double> (0, 1) *
      amp[564] + Complex<double> (0, 1) * amp[565] - Complex<double> (0, 1) *
      amp[566] - amp[567] + Complex<double> (0, 1) * amp[568]);
  jamp[2] = +1./2. * (-amp[0] - amp[1] - 1./3. * amp[2] - 1./3. * amp[3] -
      1./3. * amp[4] - 1./3. * amp[5] - 1./3. * amp[6] - amp[7] - amp[8] -
      amp[9] - 1./3. * amp[10] - 1./3. * amp[11] - 1./3. * amp[12] - amp[13] -
      1./3. * amp[14] - amp[15] - 1./3. * amp[16] - amp[17] - amp[18] - amp[19]
      - amp[20] - amp[21] - amp[22] - 1./3. * amp[23] - amp[24] - 1./3. *
      amp[25] - amp[26] - 1./3. * amp[27] - 1./3. * amp[28] - 1./3. * amp[29] -
      1./3. * amp[30] - amp[31] - amp[32] - amp[33] - 1./3. * amp[34] - 1./3. *
      amp[35] - amp[36] - 1./3. * amp[37] - 1./3. * amp[38] - amp[39] - 1./3. *
      amp[40] - 1./3. * amp[41] - 1./3. * amp[42] - amp[43] - amp[44] - amp[45]
      - amp[46] - amp[47] - amp[48] - 1./3. * amp[49] - 1./3. * amp[50] - 1./3.
      * amp[51] - 1./3. * amp[52] - amp[53] - amp[54] - amp[55] - 1./3. *
      amp[56] - 1./3. * amp[57] - 1./3. * amp[58] - 1./3. * amp[59] - 1./3. *
      amp[60] - 1./3. * amp[61] - amp[62] - amp[63] - amp[64] - amp[65] -
      amp[67] - 1./3. * amp[70] - 1./3. * amp[72] - amp[73] - Complex<double>
      (0, 1) * amp[75] - amp[76] + Complex<double> (0, 1) * amp[78] - amp[79] +
      Complex<double> (0, 1) * amp[81] - amp[83] - 1./3. * amp[92] - amp[93] -
      1./3. * amp[94] - amp[96] - Complex<double> (0, 1) * amp[98] - amp[99] -
      1./3. * amp[103] - 1./3. * amp[104] - 1./3. * amp[106] - amp[107] - 1./3.
      * amp[109] - 1./3. * amp[110] - 1./3. * amp[111] - amp[112] - amp[115] -
      1./3. * amp[118] - 1./3. * amp[120] - amp[121] - Complex<double> (0, 1) *
      amp[123] - amp[124] + Complex<double> (0, 1) * amp[126] - amp[127] +
      Complex<double> (0, 1) * amp[129] - amp[131] - 1./3. * amp[140] -
      amp[141] - 1./3. * amp[142] - amp[144] - Complex<double> (0, 1) *
      amp[146] - amp[147] - 1./3. * amp[151] - 1./3. * amp[152] - 1./3. *
      amp[154] - amp[155] - 1./3. * amp[157] - 1./3. * amp[158] - 1./3. *
      amp[159] - amp[160] - amp[228] - amp[229] - amp[230] - 1./3. * amp[231] -
      amp[232] - 1./3. * amp[233] - amp[234] - 1./3. * amp[235] - 1./3. *
      amp[236] - 1./3. * amp[237] - amp[238] - amp[239] - amp[240] - 1./3. *
      amp[241] - amp[242] - 1./3. * amp[243] - amp[244] - 1./3. * amp[245] -
      1./3. * amp[246] - 1./3. * amp[247] - amp[248] - amp[249] - amp[250] -
      amp[251] - amp[252] - amp[253] - amp[254] - amp[255] - amp[256] -
      amp[257] - amp[258] - amp[259] - amp[260] - 1./3. * amp[261] - 1./3. *
      amp[262] - amp[263] - amp[264] - 1./3. * amp[265] - 1./3. * amp[266] -
      1./3. * amp[267] - 1./3. * amp[268] - 1./3. * amp[269] - amp[270] - 1./3.
      * amp[271] - amp[272] - amp[273] - 1./3. * amp[274] - amp[275] - 1./3. *
      amp[276] - 1./3. * amp[277] - 1./3. * amp[278] - 1./3. * amp[279] -
      amp[280] - 1./3. * amp[281] - 1./3. * amp[282] - amp[283] - 1./3. *
      amp[284] - amp[285] - amp[286] - 1./3. * amp[287] - 1./3. * amp[288] -
      1./3. * amp[289] - 1./3. * amp[290] - 1./3. * amp[291] - 1./3. * amp[292]
      - 1./3. * amp[293] - 1./3. * amp[360] - amp[363] - Complex<double> (0, 1)
      * amp[365] - amp[366] - amp[367] + Complex<double> (0, 1) * amp[368] -
      1./3. * amp[369] - 1./3. * amp[370] - 1./3. * amp[372] - amp[373] +
      Complex<double> (0, 1) * amp[376] - 1./3. * amp[378] - 1./3. * amp[379] -
      1./3. * amp[382] - amp[385] - amp[386] - Complex<double> (0, 1) *
      amp[387] - Complex<double> (0, 1) * amp[388] - amp[389] + Complex<double>
      (0, 1) * amp[390] - Complex<double> (0, 1) * amp[391] - 1./3. * amp[392]
      - amp[395] - Complex<double> (0, 1) * amp[397] - amp[398] - amp[399] +
      Complex<double> (0, 1) * amp[400] - 1./3. * amp[401] - 1./3. * amp[402] -
      1./3. * amp[404] - amp[405] + Complex<double> (0, 1) * amp[408] - 1./3. *
      amp[410] - 1./3. * amp[411] - 1./3. * amp[414] - amp[417] - amp[418] -
      Complex<double> (0, 1) * amp[419] - Complex<double> (0, 1) * amp[420] -
      amp[421] + Complex<double> (0, 1) * amp[422] - Complex<double> (0, 1) *
      amp[423] + Complex<double> (0, 1) * amp[424] + Complex<double> (0, 1) *
      amp[425] - amp[427] - amp[429] + Complex<double> (0, 1) * amp[430] +
      Complex<double> (0, 1) * amp[431] + Complex<double> (0, 1) * amp[432] -
      Complex<double> (0, 1) * amp[433] + Complex<double> (0, 1) * amp[434] +
      Complex<double> (0, 1) * amp[435] - amp[436] - amp[437] + Complex<double>
      (0, 1) * amp[438] - amp[439] + Complex<double> (0, 1) * amp[440] +
      Complex<double> (0, 1) * amp[441] - amp[442] + Complex<double> (0, 1) *
      amp[444] + Complex<double> (0, 1) * amp[445] + Complex<double> (0, 1) *
      amp[448] + Complex<double> (0, 1) * amp[450] + Complex<double> (0, 1) *
      amp[451] - amp[453] + Complex<double> (0, 1) * amp[454] + Complex<double>
      (0, 1) * amp[455] - amp[456] - amp[457] + Complex<double> (0, 1) *
      amp[458] - amp[459] + Complex<double> (0, 1) * amp[460] + Complex<double>
      (0, 1) * amp[461] + Complex<double> (0, 1) * amp[462] + Complex<double>
      (0, 1) * amp[465] + Complex<double> (0, 1) * amp[467] - 1./3. * amp[512]
      - 1./3. * amp[513] - 1./3. * amp[516] - 1./3. * amp[517] - amp[519] -
      Complex<double> (0, 1) * amp[520] - Complex<double> (0, 1) * amp[521] -
      amp[522] - amp[526] - 1./3. * amp[527] + Complex<double> (0, 1) *
      amp[528] + Complex<double> (0, 1) * amp[532] - Complex<double> (0, 1) *
      amp[535] - Complex<double> (0, 1) * amp[536] + Complex<double> (0, 1) *
      amp[537] - Complex<double> (0, 1) * amp[539] - amp[540] - 1./3. *
      amp[541] + Complex<double> (0, 1) * amp[545] - 1./3. * amp[546] - 1./3. *
      amp[549] - 1./3. * amp[550] - 1./3. * amp[554] + Complex<double> (0, 1) *
      amp[558] - 1./3. * amp[560] - Complex<double> (0, 1) * amp[564] -
      Complex<double> (0, 1) * amp[565] + Complex<double> (0, 1) * amp[566] -
      Complex<double> (0, 1) * amp[568] - 1./3. * amp[569] - 1./3. * amp[570] -
      1./3. * amp[574]);
  jamp[3] = +1./2. * (+1./3. * amp[67] + 1./3. * amp[68] + amp[69] -
      Complex<double> (0, 1) * amp[71] + amp[72] + 1./3. * amp[76] + 1./3. *
      amp[77] + 1./3. * amp[79] + 1./3. * amp[80] + 1./3. * amp[82] + 1./3. *
      amp[83] + amp[85] + Complex<double> (0, 1) * amp[86] + amp[88] +
      Complex<double> (0, 1) * amp[89] + amp[90] + 1./3. * amp[93] + amp[94] +
      1./3. * amp[95] + amp[102] + amp[104] - Complex<double> (0, 1) * amp[105]
      + amp[109] + amp[111] + 1./3. * amp[115] + 1./3. * amp[116] + amp[117] -
      Complex<double> (0, 1) * amp[119] + amp[120] + 1./3. * amp[124] + 1./3. *
      amp[125] + 1./3. * amp[127] + 1./3. * amp[128] + 1./3. * amp[130] + 1./3.
      * amp[131] + amp[133] + Complex<double> (0, 1) * amp[134] + amp[136] +
      Complex<double> (0, 1) * amp[137] + amp[138] + 1./3. * amp[141] +
      amp[142] + 1./3. * amp[143] + amp[150] + amp[152] - Complex<double> (0,
      1) * amp[153] + amp[157] + amp[159] + 1./3. * amp[228] + 1./3. * amp[229]
      + 1./3. * amp[230] + amp[231] + 1./3. * amp[232] + amp[233] + 1./3. *
      amp[234] + amp[235] + amp[236] + amp[237] + 1./3. * amp[238] + 1./3. *
      amp[239] + 1./3. * amp[240] + amp[241] + 1./3. * amp[242] + amp[243] +
      1./3. * amp[244] + amp[245] + amp[246] + amp[247] + 1./3. * amp[248] +
      1./3. * amp[249] + 1./3. * amp[250] + 1./3. * amp[251] + 1./3. * amp[252]
      + 1./3. * amp[253] + 1./3. * amp[254] + 1./3. * amp[255] + 1./3. *
      amp[256] + 1./3. * amp[257] + 1./3. * amp[258] + 1./3. * amp[259] + 1./3.
      * amp[260] + amp[261] + amp[262] + 1./3. * amp[263] + 1./3. * amp[264] +
      amp[265] + amp[266] + amp[267] + amp[268] + amp[269] + 1./3. * amp[270] +
      amp[271] + 1./3. * amp[272] + 1./3. * amp[273] + amp[274] + 1./3. *
      amp[275] + amp[276] + amp[277] + amp[278] + amp[279] + 1./3. * amp[280] +
      amp[281] + amp[282] + 1./3. * amp[283] + amp[284] + 1./3. * amp[285] +
      1./3. * amp[286] + amp[287] + amp[288] + amp[289] + amp[290] + amp[291] +
      amp[292] + amp[293] + 1./3. * amp[294] + amp[295] + amp[296] + amp[297] +
      1./3. * amp[298] + 1./3. * amp[299] + amp[300] + 1./3. * amp[301] + 1./3.
      * amp[302] + amp[303] + 1./3. * amp[304] + amp[305] + amp[306] + amp[307]
      + 1./3. * amp[308] + 1./3. * amp[309] + amp[310] + 1./3. * amp[311] +
      1./3. * amp[312] + amp[313] + 1./3. * amp[314] + 1./3. * amp[315] + 1./3.
      * amp[316] + 1./3. * amp[317] + 1./3. * amp[318] + 1./3. * amp[319] +
      1./3. * amp[320] + 1./3. * amp[321] + 1./3. * amp[322] + 1./3. * amp[323]
      + 1./3. * amp[324] + 1./3. * amp[325] + 1./3. * amp[326] + amp[327] +
      amp[328] + amp[329] + amp[330] + amp[331] + amp[332] + amp[333] +
      amp[334] + amp[335] + amp[336] + amp[337] + amp[338] + amp[339] +
      amp[340] + amp[341] + 1./3. * amp[342] + 1./3. * amp[343] + 1./3. *
      amp[344] + 1./3. * amp[345] + 1./3. * amp[346] + amp[347] + 1./3. *
      amp[348] + amp[349] + amp[350] + amp[351] + 1./3. * amp[352] + amp[353] +
      1./3. * amp[354] + 1./3. * amp[355] + 1./3. * amp[356] + amp[357] +
      amp[358] + amp[359] + Complex<double> (0, 1) * amp[362] + 1./3. *
      amp[367] + amp[370] - Complex<double> (0, 1) * amp[371] + 1./3. *
      amp[374] + amp[375] + Complex<double> (0, 1) * amp[377] - Complex<double>
      (0, 1) * amp[380] - Complex<double> (0, 1) * amp[381] + Complex<double>
      (0, 1) * amp[383] - Complex<double> (0, 1) * amp[384] + Complex<double>
      (0, 1) * amp[394] + 1./3. * amp[399] + amp[402] - Complex<double> (0, 1)
      * amp[403] + 1./3. * amp[406] + amp[407] + Complex<double> (0, 1) *
      amp[409] - Complex<double> (0, 1) * amp[412] - Complex<double> (0, 1) *
      amp[413] + Complex<double> (0, 1) * amp[415] - Complex<double> (0, 1) *
      amp[416] + 1./3. * amp[426] + 1./3. * amp[427] + 1./3. * amp[428] + 1./3.
      * amp[429] + 1./3. * amp[436] + 1./3. * amp[437] + 1./3. * amp[439] +
      1./3. * amp[442] + 1./3. * amp[443] + 1./3. * amp[446] + 1./3. * amp[447]
      + 1./3. * amp[449] + 1./3. * amp[452] + 1./3. * amp[453] + 1./3. *
      amp[456] + 1./3. * amp[457] + 1./3. * amp[459] + 1./3. * amp[463] + 1./3.
      * amp[464] + 1./3. * amp[466] + Complex<double> (0, 1) * amp[468] +
      Complex<double> (0, 1) * amp[469] + amp[470] + amp[472] + Complex<double>
      (0, 1) * amp[474] + Complex<double> (0, 1) * amp[475] + Complex<double>
      (0, 1) * amp[476] - Complex<double> (0, 1) * amp[477] + Complex<double>
      (0, 1) * amp[478] + Complex<double> (0, 1) * amp[479] + Complex<double>
      (0, 1) * amp[482] + Complex<double> (0, 1) * amp[484] + Complex<double>
      (0, 1) * amp[485] + amp[487] + Complex<double> (0, 1) * amp[488] +
      Complex<double> (0, 1) * amp[489] + amp[490] + amp[491] + Complex<double>
      (0, 1) * amp[492] + amp[493] + Complex<double> (0, 1) * amp[494] +
      Complex<double> (0, 1) * amp[495] + amp[496] + Complex<double> (0, 1) *
      amp[498] + Complex<double> (0, 1) * amp[499] + Complex<double> (0, 1) *
      amp[502] + Complex<double> (0, 1) * amp[504] + Complex<double> (0, 1) *
      amp[505] + Complex<double> (0, 1) * amp[506] + amp[507] + amp[508] +
      Complex<double> (0, 1) * amp[509] + amp[510] + Complex<double> (0, 1) *
      amp[511] + amp[512] - Complex<double> (0, 1) * amp[514] - Complex<double>
      (0, 1) * amp[515] + amp[517] + 1./3. * amp[526] + amp[527] +
      Complex<double> (0, 1) * amp[529] + 1./3. * amp[531] + 1./3. * amp[540] +
      amp[541] + Complex<double> (0, 1) * amp[544] + amp[546] + amp[547] +
      Complex<double> (0, 1) * amp[548] + amp[549] + amp[550] - Complex<double>
      (0, 1) * amp[551] - Complex<double> (0, 1) * amp[552] + Complex<double>
      (0, 1) * amp[553] + amp[554] - Complex<double> (0, 1) * amp[555] + 1./3.
      * amp[556] + amp[559] + amp[560] + Complex<double> (0, 1) * amp[561] +
      amp[569] + amp[570] - Complex<double> (0, 1) * amp[571] - Complex<double>
      (0, 1) * amp[572] + Complex<double> (0, 1) * amp[573] + amp[574] -
      Complex<double> (0, 1) * amp[575]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[0][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R8_P4_heft_bb_hhgbb::matrix_8_bbx_hhgbbx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 576;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 0, 4}, {4, 12, 4, 0}, {0,
      4, 12, 4}, {4, 0, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[643] + 1./3. * amp[644] + amp[645] -
      Complex<double> (0, 1) * amp[647] + amp[648] + 1./3. * amp[652] + 1./3. *
      amp[653] + 1./3. * amp[655] + 1./3. * amp[656] + 1./3. * amp[658] + 1./3.
      * amp[659] + amp[661] + Complex<double> (0, 1) * amp[662] + amp[664] +
      Complex<double> (0, 1) * amp[665] + amp[666] + 1./3. * amp[669] +
      amp[670] + 1./3. * amp[671] + amp[678] + amp[680] - Complex<double> (0,
      1) * amp[681] + amp[685] + amp[687] + 1./3. * amp[691] + 1./3. * amp[692]
      + amp[693] - Complex<double> (0, 1) * amp[695] + amp[696] + 1./3. *
      amp[700] + 1./3. * amp[701] + 1./3. * amp[703] + 1./3. * amp[704] + 1./3.
      * amp[706] + 1./3. * amp[707] + amp[709] + Complex<double> (0, 1) *
      amp[710] + amp[712] + Complex<double> (0, 1) * amp[713] + amp[714] +
      1./3. * amp[717] + amp[718] + 1./3. * amp[719] + amp[726] + amp[728] -
      Complex<double> (0, 1) * amp[729] + amp[733] + amp[735] + 1./3. *
      amp[804] + 1./3. * amp[805] + 1./3. * amp[806] + amp[807] + 1./3. *
      amp[808] + amp[809] + 1./3. * amp[810] + amp[811] + amp[812] + amp[813] +
      1./3. * amp[814] + 1./3. * amp[815] + 1./3. * amp[816] + amp[817] + 1./3.
      * amp[818] + amp[819] + 1./3. * amp[820] + amp[821] + amp[822] + amp[823]
      + 1./3. * amp[824] + 1./3. * amp[825] + 1./3. * amp[826] + 1./3. *
      amp[827] + 1./3. * amp[828] + 1./3. * amp[829] + 1./3. * amp[830] + 1./3.
      * amp[831] + 1./3. * amp[832] + 1./3. * amp[833] + 1./3. * amp[834] +
      1./3. * amp[835] + 1./3. * amp[836] + amp[837] + amp[838] + 1./3. *
      amp[839] + 1./3. * amp[840] + amp[841] + amp[842] + amp[843] + amp[844] +
      amp[845] + 1./3. * amp[846] + amp[847] + 1./3. * amp[848] + 1./3. *
      amp[849] + amp[850] + 1./3. * amp[851] + amp[852] + amp[853] + amp[854] +
      amp[855] + 1./3. * amp[856] + amp[857] + amp[858] + 1./3. * amp[859] +
      amp[860] + 1./3. * amp[861] + 1./3. * amp[862] + amp[863] + amp[864] +
      amp[865] + amp[866] + amp[867] + amp[868] + amp[869] + 1./3. * amp[870] +
      amp[871] + amp[872] + amp[873] + 1./3. * amp[874] + 1./3. * amp[875] +
      amp[876] + 1./3. * amp[877] + 1./3. * amp[878] + amp[879] + 1./3. *
      amp[880] + amp[881] + amp[882] + amp[883] + 1./3. * amp[884] + 1./3. *
      amp[885] + amp[886] + 1./3. * amp[887] + 1./3. * amp[888] + amp[889] +
      1./3. * amp[890] + 1./3. * amp[891] + 1./3. * amp[892] + 1./3. * amp[893]
      + 1./3. * amp[894] + 1./3. * amp[895] + 1./3. * amp[896] + 1./3. *
      amp[897] + 1./3. * amp[898] + 1./3. * amp[899] + 1./3. * amp[900] + 1./3.
      * amp[901] + 1./3. * amp[902] + amp[903] + amp[904] + amp[905] + amp[906]
      + amp[907] + amp[908] + amp[909] + amp[910] + amp[911] + amp[912] +
      amp[913] + amp[914] + amp[915] + amp[916] + amp[917] + 1./3. * amp[918] +
      1./3. * amp[919] + 1./3. * amp[920] + 1./3. * amp[921] + 1./3. * amp[922]
      + amp[923] + 1./3. * amp[924] + amp[925] + amp[926] + amp[927] + 1./3. *
      amp[928] + amp[929] + 1./3. * amp[930] + 1./3. * amp[931] + 1./3. *
      amp[932] + amp[933] + amp[934] + amp[935] + Complex<double> (0, 1) *
      amp[938] + 1./3. * amp[943] + amp[946] - Complex<double> (0, 1) *
      amp[947] + 1./3. * amp[950] + amp[951] + Complex<double> (0, 1) *
      amp[953] - Complex<double> (0, 1) * amp[956] - Complex<double> (0, 1) *
      amp[957] + Complex<double> (0, 1) * amp[959] - Complex<double> (0, 1) *
      amp[960] + Complex<double> (0, 1) * amp[970] + 1./3. * amp[975] +
      amp[978] - Complex<double> (0, 1) * amp[979] + 1./3. * amp[982] +
      amp[983] + Complex<double> (0, 1) * amp[985] - Complex<double> (0, 1) *
      amp[988] - Complex<double> (0, 1) * amp[989] + Complex<double> (0, 1) *
      amp[991] - Complex<double> (0, 1) * amp[992] + 1./3. * amp[1002] + 1./3.
      * amp[1003] + 1./3. * amp[1004] + 1./3. * amp[1005] + 1./3. * amp[1012] +
      1./3. * amp[1013] + 1./3. * amp[1015] + 1./3. * amp[1018] + 1./3. *
      amp[1019] + 1./3. * amp[1022] + 1./3. * amp[1023] + 1./3. * amp[1025] +
      1./3. * amp[1028] + 1./3. * amp[1029] + 1./3. * amp[1032] + 1./3. *
      amp[1033] + 1./3. * amp[1035] + 1./3. * amp[1039] + 1./3. * amp[1040] +
      1./3. * amp[1042] + Complex<double> (0, 1) * amp[1044] + Complex<double>
      (0, 1) * amp[1045] + amp[1046] + amp[1048] + Complex<double> (0, 1) *
      amp[1050] + Complex<double> (0, 1) * amp[1051] + Complex<double> (0, 1) *
      amp[1052] - Complex<double> (0, 1) * amp[1053] + Complex<double> (0, 1) *
      amp[1054] + Complex<double> (0, 1) * amp[1055] + Complex<double> (0, 1) *
      amp[1058] + Complex<double> (0, 1) * amp[1060] + Complex<double> (0, 1) *
      amp[1061] + amp[1063] + Complex<double> (0, 1) * amp[1064] +
      Complex<double> (0, 1) * amp[1065] + amp[1066] + amp[1067] +
      Complex<double> (0, 1) * amp[1068] + amp[1069] + Complex<double> (0, 1) *
      amp[1070] + Complex<double> (0, 1) * amp[1071] + amp[1072] +
      Complex<double> (0, 1) * amp[1074] + Complex<double> (0, 1) * amp[1075] +
      Complex<double> (0, 1) * amp[1078] + Complex<double> (0, 1) * amp[1080] +
      Complex<double> (0, 1) * amp[1081] + Complex<double> (0, 1) * amp[1082] +
      amp[1083] + amp[1084] + Complex<double> (0, 1) * amp[1085] + amp[1086] +
      Complex<double> (0, 1) * amp[1087] + amp[1088] - Complex<double> (0, 1) *
      amp[1090] - Complex<double> (0, 1) * amp[1091] + amp[1093] + 1./3. *
      amp[1102] + amp[1103] + Complex<double> (0, 1) * amp[1105] + 1./3. *
      amp[1107] + 1./3. * amp[1116] + amp[1117] + Complex<double> (0, 1) *
      amp[1120] + amp[1122] + amp[1123] + Complex<double> (0, 1) * amp[1124] +
      amp[1125] + amp[1126] - Complex<double> (0, 1) * amp[1127] -
      Complex<double> (0, 1) * amp[1128] + Complex<double> (0, 1) * amp[1129] +
      amp[1130] - Complex<double> (0, 1) * amp[1131] + 1./3. * amp[1132] +
      amp[1135] + amp[1136] + Complex<double> (0, 1) * amp[1137] + amp[1145] +
      amp[1146] - Complex<double> (0, 1) * amp[1147] - Complex<double> (0, 1) *
      amp[1148] + Complex<double> (0, 1) * amp[1149] + amp[1150] -
      Complex<double> (0, 1) * amp[1151]);
  jamp[1] = +1./2. * (-amp[576] - amp[577] - 1./3. * amp[578] - 1./3. *
      amp[579] - 1./3. * amp[580] - 1./3. * amp[581] - 1./3. * amp[582] -
      amp[583] - amp[584] - amp[585] - 1./3. * amp[586] - 1./3. * amp[587] -
      1./3. * amp[588] - amp[589] - 1./3. * amp[590] - amp[591] - 1./3. *
      amp[592] - amp[593] - amp[594] - amp[595] - amp[596] - amp[597] -
      amp[598] - 1./3. * amp[599] - amp[600] - 1./3. * amp[601] - amp[602] -
      1./3. * amp[603] - 1./3. * amp[604] - 1./3. * amp[605] - 1./3. * amp[606]
      - amp[607] - amp[608] - amp[609] - 1./3. * amp[610] - 1./3. * amp[611] -
      amp[612] - 1./3. * amp[613] - 1./3. * amp[614] - amp[615] - 1./3. *
      amp[616] - 1./3. * amp[617] - 1./3. * amp[618] - amp[619] - amp[620] -
      amp[621] - amp[622] - amp[623] - amp[624] - 1./3. * amp[625] - 1./3. *
      amp[626] - 1./3. * amp[627] - 1./3. * amp[628] - amp[629] - amp[630] -
      amp[631] - 1./3. * amp[632] - 1./3. * amp[633] - 1./3. * amp[634] - 1./3.
      * amp[635] - 1./3. * amp[636] - 1./3. * amp[637] - amp[638] - amp[639] -
      amp[640] - amp[641] - amp[643] - 1./3. * amp[646] - 1./3. * amp[648] -
      amp[649] - Complex<double> (0, 1) * amp[651] - amp[652] + Complex<double>
      (0, 1) * amp[654] - amp[655] + Complex<double> (0, 1) * amp[657] -
      amp[659] - 1./3. * amp[668] - amp[669] - 1./3. * amp[670] - amp[672] -
      Complex<double> (0, 1) * amp[674] - amp[675] - 1./3. * amp[679] - 1./3. *
      amp[680] - 1./3. * amp[682] - amp[683] - 1./3. * amp[685] - 1./3. *
      amp[686] - 1./3. * amp[687] - amp[688] - amp[691] - 1./3. * amp[694] -
      1./3. * amp[696] - amp[697] - Complex<double> (0, 1) * amp[699] -
      amp[700] + Complex<double> (0, 1) * amp[702] - amp[703] + Complex<double>
      (0, 1) * amp[705] - amp[707] - 1./3. * amp[716] - amp[717] - 1./3. *
      amp[718] - amp[720] - Complex<double> (0, 1) * amp[722] - amp[723] -
      1./3. * amp[727] - 1./3. * amp[728] - 1./3. * amp[730] - amp[731] - 1./3.
      * amp[733] - 1./3. * amp[734] - 1./3. * amp[735] - amp[736] - amp[804] -
      amp[805] - amp[806] - 1./3. * amp[807] - amp[808] - 1./3. * amp[809] -
      amp[810] - 1./3. * amp[811] - 1./3. * amp[812] - 1./3. * amp[813] -
      amp[814] - amp[815] - amp[816] - 1./3. * amp[817] - amp[818] - 1./3. *
      amp[819] - amp[820] - 1./3. * amp[821] - 1./3. * amp[822] - 1./3. *
      amp[823] - amp[824] - amp[825] - amp[826] - amp[827] - amp[828] -
      amp[829] - amp[830] - amp[831] - amp[832] - amp[833] - amp[834] -
      amp[835] - amp[836] - 1./3. * amp[837] - 1./3. * amp[838] - amp[839] -
      amp[840] - 1./3. * amp[841] - 1./3. * amp[842] - 1./3. * amp[843] - 1./3.
      * amp[844] - 1./3. * amp[845] - amp[846] - 1./3. * amp[847] - amp[848] -
      amp[849] - 1./3. * amp[850] - amp[851] - 1./3. * amp[852] - 1./3. *
      amp[853] - 1./3. * amp[854] - 1./3. * amp[855] - amp[856] - 1./3. *
      amp[857] - 1./3. * amp[858] - amp[859] - 1./3. * amp[860] - amp[861] -
      amp[862] - 1./3. * amp[863] - 1./3. * amp[864] - 1./3. * amp[865] - 1./3.
      * amp[866] - 1./3. * amp[867] - 1./3. * amp[868] - 1./3. * amp[869] -
      1./3. * amp[936] - amp[939] - Complex<double> (0, 1) * amp[941] -
      amp[942] - amp[943] + Complex<double> (0, 1) * amp[944] - 1./3. *
      amp[945] - 1./3. * amp[946] - 1./3. * amp[948] - amp[949] +
      Complex<double> (0, 1) * amp[952] - 1./3. * amp[954] - 1./3. * amp[955] -
      1./3. * amp[958] - amp[961] - amp[962] - Complex<double> (0, 1) *
      amp[963] - Complex<double> (0, 1) * amp[964] - amp[965] + Complex<double>
      (0, 1) * amp[966] - Complex<double> (0, 1) * amp[967] - 1./3. * amp[968]
      - amp[971] - Complex<double> (0, 1) * amp[973] - amp[974] - amp[975] +
      Complex<double> (0, 1) * amp[976] - 1./3. * amp[977] - 1./3. * amp[978] -
      1./3. * amp[980] - amp[981] + Complex<double> (0, 1) * amp[984] - 1./3. *
      amp[986] - 1./3. * amp[987] - 1./3. * amp[990] - amp[993] - amp[994] -
      Complex<double> (0, 1) * amp[995] - Complex<double> (0, 1) * amp[996] -
      amp[997] + Complex<double> (0, 1) * amp[998] - Complex<double> (0, 1) *
      amp[999] + Complex<double> (0, 1) * amp[1000] + Complex<double> (0, 1) *
      amp[1001] - amp[1003] - amp[1005] + Complex<double> (0, 1) * amp[1006] +
      Complex<double> (0, 1) * amp[1007] + Complex<double> (0, 1) * amp[1008] -
      Complex<double> (0, 1) * amp[1009] + Complex<double> (0, 1) * amp[1010] +
      Complex<double> (0, 1) * amp[1011] - amp[1012] - amp[1013] +
      Complex<double> (0, 1) * amp[1014] - amp[1015] + Complex<double> (0, 1) *
      amp[1016] + Complex<double> (0, 1) * amp[1017] - amp[1018] +
      Complex<double> (0, 1) * amp[1020] + Complex<double> (0, 1) * amp[1021] +
      Complex<double> (0, 1) * amp[1024] + Complex<double> (0, 1) * amp[1026] +
      Complex<double> (0, 1) * amp[1027] - amp[1029] + Complex<double> (0, 1) *
      amp[1030] + Complex<double> (0, 1) * amp[1031] - amp[1032] - amp[1033] +
      Complex<double> (0, 1) * amp[1034] - amp[1035] + Complex<double> (0, 1) *
      amp[1036] + Complex<double> (0, 1) * amp[1037] + Complex<double> (0, 1) *
      amp[1038] + Complex<double> (0, 1) * amp[1041] + Complex<double> (0, 1) *
      amp[1043] - 1./3. * amp[1088] - 1./3. * amp[1089] - 1./3. * amp[1092] -
      1./3. * amp[1093] - amp[1095] - Complex<double> (0, 1) * amp[1096] -
      Complex<double> (0, 1) * amp[1097] - amp[1098] - amp[1102] - 1./3. *
      amp[1103] + Complex<double> (0, 1) * amp[1104] + Complex<double> (0, 1) *
      amp[1108] - Complex<double> (0, 1) * amp[1111] - Complex<double> (0, 1) *
      amp[1112] + Complex<double> (0, 1) * amp[1113] - Complex<double> (0, 1) *
      amp[1115] - amp[1116] - 1./3. * amp[1117] + Complex<double> (0, 1) *
      amp[1121] - 1./3. * amp[1122] - 1./3. * amp[1125] - 1./3. * amp[1126] -
      1./3. * amp[1130] + Complex<double> (0, 1) * amp[1134] - 1./3. *
      amp[1136] - Complex<double> (0, 1) * amp[1140] - Complex<double> (0, 1) *
      amp[1141] + Complex<double> (0, 1) * amp[1142] - Complex<double> (0, 1) *
      amp[1144] - 1./3. * amp[1145] - 1./3. * amp[1146] - 1./3. * amp[1150]);
  jamp[2] = +1./2. * (+1./3. * amp[576] + 1./3. * amp[577] + amp[578] +
      amp[579] + amp[580] + amp[581] + amp[582] + 1./3. * amp[583] + 1./3. *
      amp[584] + 1./3. * amp[585] + amp[586] + amp[587] + amp[588] + 1./3. *
      amp[589] + amp[590] + 1./3. * amp[591] + amp[592] + 1./3. * amp[593] +
      1./3. * amp[594] + 1./3. * amp[595] + 1./3. * amp[596] + 1./3. * amp[597]
      + 1./3. * amp[598] + amp[599] + 1./3. * amp[600] + amp[601] + 1./3. *
      amp[602] + amp[603] + amp[604] + amp[605] + amp[606] + 1./3. * amp[607] +
      1./3. * amp[608] + 1./3. * amp[609] + amp[610] + amp[611] + 1./3. *
      amp[612] + amp[613] + amp[614] + 1./3. * amp[615] + amp[616] + amp[617] +
      amp[618] + 1./3. * amp[619] + 1./3. * amp[620] + 1./3. * amp[621] + 1./3.
      * amp[622] + 1./3. * amp[623] + 1./3. * amp[624] + amp[625] + amp[626] +
      amp[627] + amp[628] + 1./3. * amp[629] + 1./3. * amp[630] + 1./3. *
      amp[631] + amp[632] + amp[633] + amp[634] + amp[635] + amp[636] +
      amp[637] + 1./3. * amp[638] + 1./3. * amp[639] + 1./3. * amp[640] + 1./3.
      * amp[641] + amp[642] + amp[646] + Complex<double> (0, 1) * amp[647] +
      1./3. * amp[649] + 1./3. * amp[650] + amp[660] - Complex<double> (0, 1) *
      amp[662] + amp[663] - Complex<double> (0, 1) * amp[665] + amp[667] +
      amp[668] + 1./3. * amp[672] + 1./3. * amp[673] + 1./3. * amp[675] +
      amp[676] + 1./3. * amp[677] + amp[679] + Complex<double> (0, 1) *
      amp[681] + amp[682] + 1./3. * amp[683] + 1./3. * amp[684] + amp[686] +
      1./3. * amp[688] + 1./3. * amp[689] + amp[690] + amp[694] +
      Complex<double> (0, 1) * amp[695] + 1./3. * amp[697] + 1./3. * amp[698] +
      amp[708] - Complex<double> (0, 1) * amp[710] + amp[711] - Complex<double>
      (0, 1) * amp[713] + amp[715] + amp[716] + 1./3. * amp[720] + 1./3. *
      amp[721] + 1./3. * amp[723] + amp[724] + 1./3. * amp[725] + amp[727] +
      Complex<double> (0, 1) * amp[729] + amp[730] + 1./3. * amp[731] + 1./3. *
      amp[732] + amp[734] + 1./3. * amp[736] + 1./3. * amp[737] + amp[738] +
      amp[739] + amp[740] + 1./3. * amp[741] + amp[742] + 1./3. * amp[743] +
      amp[744] + 1./3. * amp[745] + 1./3. * amp[746] + 1./3. * amp[747] +
      amp[748] + amp[749] + amp[750] + 1./3. * amp[751] + amp[752] + 1./3. *
      amp[753] + amp[754] + 1./3. * amp[755] + 1./3. * amp[756] + 1./3. *
      amp[757] + amp[758] + amp[759] + amp[760] + amp[761] + amp[762] +
      amp[763] + amp[764] + amp[765] + amp[766] + amp[767] + amp[768] +
      amp[769] + amp[770] + 1./3. * amp[771] + 1./3. * amp[772] + amp[773] +
      amp[774] + 1./3. * amp[775] + 1./3. * amp[776] + 1./3. * amp[777] + 1./3.
      * amp[778] + 1./3. * amp[779] + amp[780] + 1./3. * amp[781] + amp[782] +
      amp[783] + 1./3. * amp[784] + amp[785] + 1./3. * amp[786] + 1./3. *
      amp[787] + 1./3. * amp[788] + 1./3. * amp[789] + amp[790] + 1./3. *
      amp[791] + 1./3. * amp[792] + amp[793] + 1./3. * amp[794] + amp[795] +
      amp[796] + 1./3. * amp[797] + 1./3. * amp[798] + 1./3. * amp[799] + 1./3.
      * amp[800] + 1./3. * amp[801] + 1./3. * amp[802] + 1./3. * amp[803] +
      amp[936] + amp[937] - Complex<double> (0, 1) * amp[938] + 1./3. *
      amp[939] + 1./3. * amp[940] + 1./3. * amp[942] + amp[945] +
      Complex<double> (0, 1) * amp[947] + amp[948] + 1./3. * amp[949] -
      Complex<double> (0, 1) * amp[953] + amp[954] + amp[955] + Complex<double>
      (0, 1) * amp[956] + Complex<double> (0, 1) * amp[957] + amp[958] -
      Complex<double> (0, 1) * amp[959] + Complex<double> (0, 1) * amp[960] +
      1./3. * amp[961] + 1./3. * amp[962] + 1./3. * amp[965] + amp[968] +
      amp[969] - Complex<double> (0, 1) * amp[970] + 1./3. * amp[971] + 1./3. *
      amp[972] + 1./3. * amp[974] + amp[977] + Complex<double> (0, 1) *
      amp[979] + amp[980] + 1./3. * amp[981] - Complex<double> (0, 1) *
      amp[985] + amp[986] + amp[987] + Complex<double> (0, 1) * amp[988] +
      Complex<double> (0, 1) * amp[989] + amp[990] - Complex<double> (0, 1) *
      amp[991] + Complex<double> (0, 1) * amp[992] + 1./3. * amp[993] + 1./3. *
      amp[994] + 1./3. * amp[997] - Complex<double> (0, 1) * amp[1044] -
      Complex<double> (0, 1) * amp[1045] + amp[1047] + amp[1049] -
      Complex<double> (0, 1) * amp[1050] - Complex<double> (0, 1) * amp[1051] -
      Complex<double> (0, 1) * amp[1052] + Complex<double> (0, 1) * amp[1053] -
      Complex<double> (0, 1) * amp[1054] - Complex<double> (0, 1) * amp[1055] +
      amp[1056] + amp[1057] - Complex<double> (0, 1) * amp[1058] + amp[1059] -
      Complex<double> (0, 1) * amp[1060] - Complex<double> (0, 1) * amp[1061] +
      amp[1062] - Complex<double> (0, 1) * amp[1064] - Complex<double> (0, 1) *
      amp[1065] - Complex<double> (0, 1) * amp[1068] - Complex<double> (0, 1) *
      amp[1070] - Complex<double> (0, 1) * amp[1071] + amp[1073] -
      Complex<double> (0, 1) * amp[1074] - Complex<double> (0, 1) * amp[1075] +
      amp[1076] + amp[1077] - Complex<double> (0, 1) * amp[1078] + amp[1079] -
      Complex<double> (0, 1) * amp[1080] - Complex<double> (0, 1) * amp[1081] -
      Complex<double> (0, 1) * amp[1082] - Complex<double> (0, 1) * amp[1085] -
      Complex<double> (0, 1) * amp[1087] + amp[1089] + Complex<double> (0, 1) *
      amp[1090] + Complex<double> (0, 1) * amp[1091] + amp[1092] + 1./3. *
      amp[1094] + 1./3. * amp[1095] + 1./3. * amp[1098] + 1./3. * amp[1099] +
      amp[1100] + 1./3. * amp[1101] - Complex<double> (0, 1) * amp[1105] +
      1./3. * amp[1106] + 1./3. * amp[1109] + 1./3. * amp[1110] + 1./3. *
      amp[1114] + amp[1118] + 1./3. * amp[1119] - Complex<double> (0, 1) *
      amp[1120] - Complex<double> (0, 1) * amp[1124] + Complex<double> (0, 1) *
      amp[1127] + Complex<double> (0, 1) * amp[1128] - Complex<double> (0, 1) *
      amp[1129] + Complex<double> (0, 1) * amp[1131] + 1./3. * amp[1133] -
      Complex<double> (0, 1) * amp[1137] + 1./3. * amp[1138] + 1./3. *
      amp[1139] + 1./3. * amp[1143] + Complex<double> (0, 1) * amp[1147] +
      Complex<double> (0, 1) * amp[1148] - Complex<double> (0, 1) * amp[1149] +
      Complex<double> (0, 1) * amp[1151]);
  jamp[3] = +1./2. * (-1./3. * amp[642] - amp[644] - 1./3. * amp[645] -
      amp[650] + Complex<double> (0, 1) * amp[651] - amp[653] - Complex<double>
      (0, 1) * amp[654] - amp[656] - Complex<double> (0, 1) * amp[657] -
      amp[658] - 1./3. * amp[660] - 1./3. * amp[661] - 1./3. * amp[663] - 1./3.
      * amp[664] - 1./3. * amp[666] - 1./3. * amp[667] - amp[671] - amp[673] +
      Complex<double> (0, 1) * amp[674] - 1./3. * amp[676] - amp[677] - 1./3. *
      amp[678] - amp[684] - amp[689] - 1./3. * amp[690] - amp[692] - 1./3. *
      amp[693] - amp[698] + Complex<double> (0, 1) * amp[699] - amp[701] -
      Complex<double> (0, 1) * amp[702] - amp[704] - Complex<double> (0, 1) *
      amp[705] - amp[706] - 1./3. * amp[708] - 1./3. * amp[709] - 1./3. *
      amp[711] - 1./3. * amp[712] - 1./3. * amp[714] - 1./3. * amp[715] -
      amp[719] - amp[721] + Complex<double> (0, 1) * amp[722] - 1./3. *
      amp[724] - amp[725] - 1./3. * amp[726] - amp[732] - amp[737] - 1./3. *
      amp[738] - 1./3. * amp[739] - 1./3. * amp[740] - amp[741] - 1./3. *
      amp[742] - amp[743] - 1./3. * amp[744] - amp[745] - amp[746] - amp[747] -
      1./3. * amp[748] - 1./3. * amp[749] - 1./3. * amp[750] - amp[751] - 1./3.
      * amp[752] - amp[753] - 1./3. * amp[754] - amp[755] - amp[756] - amp[757]
      - 1./3. * amp[758] - 1./3. * amp[759] - 1./3. * amp[760] - 1./3. *
      amp[761] - 1./3. * amp[762] - 1./3. * amp[763] - 1./3. * amp[764] - 1./3.
      * amp[765] - 1./3. * amp[766] - 1./3. * amp[767] - 1./3. * amp[768] -
      1./3. * amp[769] - 1./3. * amp[770] - amp[771] - amp[772] - 1./3. *
      amp[773] - 1./3. * amp[774] - amp[775] - amp[776] - amp[777] - amp[778] -
      amp[779] - 1./3. * amp[780] - amp[781] - 1./3. * amp[782] - 1./3. *
      amp[783] - amp[784] - 1./3. * amp[785] - amp[786] - amp[787] - amp[788] -
      amp[789] - 1./3. * amp[790] - amp[791] - amp[792] - 1./3. * amp[793] -
      amp[794] - 1./3. * amp[795] - 1./3. * amp[796] - amp[797] - amp[798] -
      amp[799] - amp[800] - amp[801] - amp[802] - amp[803] - amp[870] - 1./3. *
      amp[871] - 1./3. * amp[872] - 1./3. * amp[873] - amp[874] - amp[875] -
      1./3. * amp[876] - amp[877] - amp[878] - 1./3. * amp[879] - amp[880] -
      1./3. * amp[881] - 1./3. * amp[882] - 1./3. * amp[883] - amp[884] -
      amp[885] - 1./3. * amp[886] - amp[887] - amp[888] - 1./3. * amp[889] -
      amp[890] - amp[891] - amp[892] - amp[893] - amp[894] - amp[895] -
      amp[896] - amp[897] - amp[898] - amp[899] - amp[900] - amp[901] -
      amp[902] - 1./3. * amp[903] - 1./3. * amp[904] - 1./3. * amp[905] - 1./3.
      * amp[906] - 1./3. * amp[907] - 1./3. * amp[908] - 1./3. * amp[909] -
      1./3. * amp[910] - 1./3. * amp[911] - 1./3. * amp[912] - 1./3. * amp[913]
      - 1./3. * amp[914] - 1./3. * amp[915] - 1./3. * amp[916] - 1./3. *
      amp[917] - amp[918] - amp[919] - amp[920] - amp[921] - amp[922] - 1./3. *
      amp[923] - amp[924] - 1./3. * amp[925] - 1./3. * amp[926] - 1./3. *
      amp[927] - amp[928] - 1./3. * amp[929] - amp[930] - amp[931] - amp[932] -
      1./3. * amp[933] - 1./3. * amp[934] - 1./3. * amp[935] - 1./3. * amp[937]
      - amp[940] + Complex<double> (0, 1) * amp[941] - Complex<double> (0, 1) *
      amp[944] - amp[950] - 1./3. * amp[951] - Complex<double> (0, 1) *
      amp[952] + Complex<double> (0, 1) * amp[963] + Complex<double> (0, 1) *
      amp[964] - Complex<double> (0, 1) * amp[966] + Complex<double> (0, 1) *
      amp[967] - 1./3. * amp[969] - amp[972] + Complex<double> (0, 1) *
      amp[973] - Complex<double> (0, 1) * amp[976] - amp[982] - 1./3. *
      amp[983] - Complex<double> (0, 1) * amp[984] + Complex<double> (0, 1) *
      amp[995] + Complex<double> (0, 1) * amp[996] - Complex<double> (0, 1) *
      amp[998] + Complex<double> (0, 1) * amp[999] - Complex<double> (0, 1) *
      amp[1000] - Complex<double> (0, 1) * amp[1001] - amp[1002] - amp[1004] -
      Complex<double> (0, 1) * amp[1006] - Complex<double> (0, 1) * amp[1007] -
      Complex<double> (0, 1) * amp[1008] + Complex<double> (0, 1) * amp[1009] -
      Complex<double> (0, 1) * amp[1010] - Complex<double> (0, 1) * amp[1011] -
      Complex<double> (0, 1) * amp[1014] - Complex<double> (0, 1) * amp[1016] -
      Complex<double> (0, 1) * amp[1017] - amp[1019] - Complex<double> (0, 1) *
      amp[1020] - Complex<double> (0, 1) * amp[1021] - amp[1022] - amp[1023] -
      Complex<double> (0, 1) * amp[1024] - amp[1025] - Complex<double> (0, 1) *
      amp[1026] - Complex<double> (0, 1) * amp[1027] - amp[1028] -
      Complex<double> (0, 1) * amp[1030] - Complex<double> (0, 1) * amp[1031] -
      Complex<double> (0, 1) * amp[1034] - Complex<double> (0, 1) * amp[1036] -
      Complex<double> (0, 1) * amp[1037] - Complex<double> (0, 1) * amp[1038] -
      amp[1039] - amp[1040] - Complex<double> (0, 1) * amp[1041] - amp[1042] -
      Complex<double> (0, 1) * amp[1043] - 1./3. * amp[1046] - 1./3. *
      amp[1047] - 1./3. * amp[1048] - 1./3. * amp[1049] - 1./3. * amp[1056] -
      1./3. * amp[1057] - 1./3. * amp[1059] - 1./3. * amp[1062] - 1./3. *
      amp[1063] - 1./3. * amp[1066] - 1./3. * amp[1067] - 1./3. * amp[1069] -
      1./3. * amp[1072] - 1./3. * amp[1073] - 1./3. * amp[1076] - 1./3. *
      amp[1077] - 1./3. * amp[1079] - 1./3. * amp[1083] - 1./3. * amp[1084] -
      1./3. * amp[1086] - amp[1094] + Complex<double> (0, 1) * amp[1096] +
      Complex<double> (0, 1) * amp[1097] - amp[1099] - 1./3. * amp[1100] -
      amp[1101] - Complex<double> (0, 1) * amp[1104] - amp[1106] - amp[1107] -
      Complex<double> (0, 1) * amp[1108] - amp[1109] - amp[1110] +
      Complex<double> (0, 1) * amp[1111] + Complex<double> (0, 1) * amp[1112] -
      Complex<double> (0, 1) * amp[1113] - amp[1114] + Complex<double> (0, 1) *
      amp[1115] - 1./3. * amp[1118] - amp[1119] - Complex<double> (0, 1) *
      amp[1121] - 1./3. * amp[1123] - amp[1132] - amp[1133] - Complex<double>
      (0, 1) * amp[1134] - 1./3. * amp[1135] - amp[1138] - amp[1139] +
      Complex<double> (0, 1) * amp[1140] + Complex<double> (0, 1) * amp[1141] -
      Complex<double> (0, 1) * amp[1142] - amp[1143] + Complex<double> (0, 1) *
      amp[1144]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[1][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R8_P4_heft_bb_hhgbb::matrix_8_bxbx_hhgbxbx() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 576;
  const int ncolor = 4; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {1, 1, 1, 1}; 
  static const double cf[ncolor][ncolor] = {{12, 4, 4, 0}, {4, 12, 0, 4}, {4,
      0, 12, 4}, {0, 4, 4, 12}};

  // Calculate color flows
  jamp[0] = +1./2. * (+1./3. * amp[1219] + 1./3. * amp[1220] + amp[1221] -
      Complex<double> (0, 1) * amp[1223] + amp[1224] + 1./3. * amp[1228] +
      1./3. * amp[1229] + 1./3. * amp[1231] + 1./3. * amp[1232] + 1./3. *
      amp[1234] + 1./3. * amp[1235] + amp[1237] + Complex<double> (0, 1) *
      amp[1238] + amp[1240] + Complex<double> (0, 1) * amp[1241] + amp[1242] +
      1./3. * amp[1245] + amp[1246] + 1./3. * amp[1247] + amp[1254] + amp[1256]
      - Complex<double> (0, 1) * amp[1257] + amp[1261] + amp[1263] + 1./3. *
      amp[1267] + 1./3. * amp[1268] + amp[1269] - Complex<double> (0, 1) *
      amp[1271] + amp[1272] + 1./3. * amp[1276] + 1./3. * amp[1277] + 1./3. *
      amp[1279] + 1./3. * amp[1280] + 1./3. * amp[1282] + 1./3. * amp[1283] +
      amp[1285] + Complex<double> (0, 1) * amp[1286] + amp[1288] +
      Complex<double> (0, 1) * amp[1289] + amp[1290] + 1./3. * amp[1293] +
      amp[1294] + 1./3. * amp[1295] + amp[1302] + amp[1304] - Complex<double>
      (0, 1) * amp[1305] + amp[1309] + amp[1311] + 1./3. * amp[1380] + 1./3. *
      amp[1381] + 1./3. * amp[1382] + amp[1383] + 1./3. * amp[1384] + amp[1385]
      + 1./3. * amp[1386] + amp[1387] + amp[1388] + amp[1389] + 1./3. *
      amp[1390] + 1./3. * amp[1391] + 1./3. * amp[1392] + amp[1393] + 1./3. *
      amp[1394] + amp[1395] + 1./3. * amp[1396] + amp[1397] + amp[1398] +
      amp[1399] + 1./3. * amp[1400] + 1./3. * amp[1401] + 1./3. * amp[1402] +
      1./3. * amp[1403] + 1./3. * amp[1404] + 1./3. * amp[1405] + 1./3. *
      amp[1406] + 1./3. * amp[1407] + 1./3. * amp[1408] + 1./3. * amp[1409] +
      1./3. * amp[1410] + 1./3. * amp[1411] + 1./3. * amp[1412] + amp[1413] +
      amp[1414] + 1./3. * amp[1415] + 1./3. * amp[1416] + amp[1417] + amp[1418]
      + amp[1419] + amp[1420] + amp[1421] + 1./3. * amp[1422] + amp[1423] +
      1./3. * amp[1424] + 1./3. * amp[1425] + amp[1426] + 1./3. * amp[1427] +
      amp[1428] + amp[1429] + amp[1430] + amp[1431] + 1./3. * amp[1432] +
      amp[1433] + amp[1434] + 1./3. * amp[1435] + amp[1436] + 1./3. * amp[1437]
      + 1./3. * amp[1438] + amp[1439] + amp[1440] + amp[1441] + amp[1442] +
      amp[1443] + amp[1444] + amp[1445] + 1./3. * amp[1446] + amp[1447] +
      amp[1448] + amp[1449] + 1./3. * amp[1450] + 1./3. * amp[1451] + amp[1452]
      + 1./3. * amp[1453] + 1./3. * amp[1454] + amp[1455] + 1./3. * amp[1456] +
      amp[1457] + amp[1458] + amp[1459] + 1./3. * amp[1460] + 1./3. * amp[1461]
      + amp[1462] + 1./3. * amp[1463] + 1./3. * amp[1464] + amp[1465] + 1./3. *
      amp[1466] + 1./3. * amp[1467] + 1./3. * amp[1468] + 1./3. * amp[1469] +
      1./3. * amp[1470] + 1./3. * amp[1471] + 1./3. * amp[1472] + 1./3. *
      amp[1473] + 1./3. * amp[1474] + 1./3. * amp[1475] + 1./3. * amp[1476] +
      1./3. * amp[1477] + 1./3. * amp[1478] + amp[1479] + amp[1480] + amp[1481]
      + amp[1482] + amp[1483] + amp[1484] + amp[1485] + amp[1486] + amp[1487] +
      amp[1488] + amp[1489] + amp[1490] + amp[1491] + amp[1492] + amp[1493] +
      1./3. * amp[1494] + 1./3. * amp[1495] + 1./3. * amp[1496] + 1./3. *
      amp[1497] + 1./3. * amp[1498] + amp[1499] + 1./3. * amp[1500] + amp[1501]
      + amp[1502] + amp[1503] + 1./3. * amp[1504] + amp[1505] + 1./3. *
      amp[1506] + 1./3. * amp[1507] + 1./3. * amp[1508] + amp[1509] + amp[1510]
      + amp[1511] + Complex<double> (0, 1) * amp[1514] + 1./3. * amp[1519] +
      amp[1522] - Complex<double> (0, 1) * amp[1523] + 1./3. * amp[1526] +
      amp[1527] + Complex<double> (0, 1) * amp[1529] - Complex<double> (0, 1) *
      amp[1532] - Complex<double> (0, 1) * amp[1533] + Complex<double> (0, 1) *
      amp[1535] - Complex<double> (0, 1) * amp[1536] + Complex<double> (0, 1) *
      amp[1546] + 1./3. * amp[1551] + amp[1554] - Complex<double> (0, 1) *
      amp[1555] + 1./3. * amp[1558] + amp[1559] + Complex<double> (0, 1) *
      amp[1561] - Complex<double> (0, 1) * amp[1564] - Complex<double> (0, 1) *
      amp[1565] + Complex<double> (0, 1) * amp[1567] - Complex<double> (0, 1) *
      amp[1568] + 1./3. * amp[1578] + 1./3. * amp[1579] + 1./3. * amp[1580] +
      1./3. * amp[1581] + 1./3. * amp[1588] + 1./3. * amp[1589] + 1./3. *
      amp[1591] + 1./3. * amp[1594] + 1./3. * amp[1595] + 1./3. * amp[1598] +
      1./3. * amp[1599] + 1./3. * amp[1601] + 1./3. * amp[1604] + 1./3. *
      amp[1605] + 1./3. * amp[1608] + 1./3. * amp[1609] + 1./3. * amp[1611] +
      1./3. * amp[1615] + 1./3. * amp[1616] + 1./3. * amp[1618] +
      Complex<double> (0, 1) * amp[1620] + Complex<double> (0, 1) * amp[1621] +
      amp[1622] + amp[1624] + Complex<double> (0, 1) * amp[1626] +
      Complex<double> (0, 1) * amp[1627] + Complex<double> (0, 1) * amp[1628] -
      Complex<double> (0, 1) * amp[1629] + Complex<double> (0, 1) * amp[1630] +
      Complex<double> (0, 1) * amp[1631] + Complex<double> (0, 1) * amp[1634] +
      Complex<double> (0, 1) * amp[1636] + Complex<double> (0, 1) * amp[1637] +
      amp[1639] + Complex<double> (0, 1) * amp[1640] + Complex<double> (0, 1) *
      amp[1641] + amp[1642] + amp[1643] + Complex<double> (0, 1) * amp[1644] +
      amp[1645] + Complex<double> (0, 1) * amp[1646] + Complex<double> (0, 1) *
      amp[1647] + amp[1648] + Complex<double> (0, 1) * amp[1650] +
      Complex<double> (0, 1) * amp[1651] + Complex<double> (0, 1) * amp[1654] +
      Complex<double> (0, 1) * amp[1656] + Complex<double> (0, 1) * amp[1657] +
      Complex<double> (0, 1) * amp[1658] + amp[1659] + amp[1660] +
      Complex<double> (0, 1) * amp[1661] + amp[1662] + Complex<double> (0, 1) *
      amp[1663] + amp[1664] - Complex<double> (0, 1) * amp[1666] -
      Complex<double> (0, 1) * amp[1667] + amp[1669] + 1./3. * amp[1678] +
      amp[1679] + Complex<double> (0, 1) * amp[1681] + 1./3. * amp[1683] +
      1./3. * amp[1692] + amp[1693] + Complex<double> (0, 1) * amp[1696] +
      amp[1698] + amp[1699] + Complex<double> (0, 1) * amp[1700] + amp[1701] +
      amp[1702] - Complex<double> (0, 1) * amp[1703] - Complex<double> (0, 1) *
      amp[1704] + Complex<double> (0, 1) * amp[1705] + amp[1706] -
      Complex<double> (0, 1) * amp[1707] + 1./3. * amp[1708] + amp[1711] +
      amp[1712] + Complex<double> (0, 1) * amp[1713] + amp[1721] + amp[1722] -
      Complex<double> (0, 1) * amp[1723] - Complex<double> (0, 1) * amp[1724] +
      Complex<double> (0, 1) * amp[1725] + amp[1726] - Complex<double> (0, 1) *
      amp[1727]);
  jamp[1] = +1./2. * (-amp[1152] - amp[1153] - 1./3. * amp[1154] - 1./3. *
      amp[1155] - 1./3. * amp[1156] - 1./3. * amp[1157] - 1./3. * amp[1158] -
      amp[1159] - amp[1160] - amp[1161] - 1./3. * amp[1162] - 1./3. * amp[1163]
      - 1./3. * amp[1164] - amp[1165] - 1./3. * amp[1166] - amp[1167] - 1./3. *
      amp[1168] - amp[1169] - amp[1170] - amp[1171] - amp[1172] - amp[1173] -
      amp[1174] - 1./3. * amp[1175] - amp[1176] - 1./3. * amp[1177] - amp[1178]
      - 1./3. * amp[1179] - 1./3. * amp[1180] - 1./3. * amp[1181] - 1./3. *
      amp[1182] - amp[1183] - amp[1184] - amp[1185] - 1./3. * amp[1186] - 1./3.
      * amp[1187] - amp[1188] - 1./3. * amp[1189] - 1./3. * amp[1190] -
      amp[1191] - 1./3. * amp[1192] - 1./3. * amp[1193] - 1./3. * amp[1194] -
      amp[1195] - amp[1196] - amp[1197] - amp[1198] - amp[1199] - amp[1200] -
      1./3. * amp[1201] - 1./3. * amp[1202] - 1./3. * amp[1203] - 1./3. *
      amp[1204] - amp[1205] - amp[1206] - amp[1207] - 1./3. * amp[1208] - 1./3.
      * amp[1209] - 1./3. * amp[1210] - 1./3. * amp[1211] - 1./3. * amp[1212] -
      1./3. * amp[1213] - amp[1214] - amp[1215] - amp[1216] - amp[1217] -
      amp[1219] - 1./3. * amp[1222] - 1./3. * amp[1224] - amp[1225] -
      Complex<double> (0, 1) * amp[1227] - amp[1228] + Complex<double> (0, 1) *
      amp[1230] - amp[1231] + Complex<double> (0, 1) * amp[1233] - amp[1235] -
      1./3. * amp[1244] - amp[1245] - 1./3. * amp[1246] - amp[1248] -
      Complex<double> (0, 1) * amp[1250] - amp[1251] - 1./3. * amp[1255] -
      1./3. * amp[1256] - 1./3. * amp[1258] - amp[1259] - 1./3. * amp[1261] -
      1./3. * amp[1262] - 1./3. * amp[1263] - amp[1264] - amp[1267] - 1./3. *
      amp[1270] - 1./3. * amp[1272] - amp[1273] - Complex<double> (0, 1) *
      amp[1275] - amp[1276] + Complex<double> (0, 1) * amp[1278] - amp[1279] +
      Complex<double> (0, 1) * amp[1281] - amp[1283] - 1./3. * amp[1292] -
      amp[1293] - 1./3. * amp[1294] - amp[1296] - Complex<double> (0, 1) *
      amp[1298] - amp[1299] - 1./3. * amp[1303] - 1./3. * amp[1304] - 1./3. *
      amp[1306] - amp[1307] - 1./3. * amp[1309] - 1./3. * amp[1310] - 1./3. *
      amp[1311] - amp[1312] - amp[1380] - amp[1381] - amp[1382] - 1./3. *
      amp[1383] - amp[1384] - 1./3. * amp[1385] - amp[1386] - 1./3. * amp[1387]
      - 1./3. * amp[1388] - 1./3. * amp[1389] - amp[1390] - amp[1391] -
      amp[1392] - 1./3. * amp[1393] - amp[1394] - 1./3. * amp[1395] - amp[1396]
      - 1./3. * amp[1397] - 1./3. * amp[1398] - 1./3. * amp[1399] - amp[1400] -
      amp[1401] - amp[1402] - amp[1403] - amp[1404] - amp[1405] - amp[1406] -
      amp[1407] - amp[1408] - amp[1409] - amp[1410] - amp[1411] - amp[1412] -
      1./3. * amp[1413] - 1./3. * amp[1414] - amp[1415] - amp[1416] - 1./3. *
      amp[1417] - 1./3. * amp[1418] - 1./3. * amp[1419] - 1./3. * amp[1420] -
      1./3. * amp[1421] - amp[1422] - 1./3. * amp[1423] - amp[1424] - amp[1425]
      - 1./3. * amp[1426] - amp[1427] - 1./3. * amp[1428] - 1./3. * amp[1429] -
      1./3. * amp[1430] - 1./3. * amp[1431] - amp[1432] - 1./3. * amp[1433] -
      1./3. * amp[1434] - amp[1435] - 1./3. * amp[1436] - amp[1437] - amp[1438]
      - 1./3. * amp[1439] - 1./3. * amp[1440] - 1./3. * amp[1441] - 1./3. *
      amp[1442] - 1./3. * amp[1443] - 1./3. * amp[1444] - 1./3. * amp[1445] -
      1./3. * amp[1512] - amp[1515] - Complex<double> (0, 1) * amp[1517] -
      amp[1518] - amp[1519] + Complex<double> (0, 1) * amp[1520] - 1./3. *
      amp[1521] - 1./3. * amp[1522] - 1./3. * amp[1524] - amp[1525] +
      Complex<double> (0, 1) * amp[1528] - 1./3. * amp[1530] - 1./3. *
      amp[1531] - 1./3. * amp[1534] - amp[1537] - amp[1538] - Complex<double>
      (0, 1) * amp[1539] - Complex<double> (0, 1) * amp[1540] - amp[1541] +
      Complex<double> (0, 1) * amp[1542] - Complex<double> (0, 1) * amp[1543] -
      1./3. * amp[1544] - amp[1547] - Complex<double> (0, 1) * amp[1549] -
      amp[1550] - amp[1551] + Complex<double> (0, 1) * amp[1552] - 1./3. *
      amp[1553] - 1./3. * amp[1554] - 1./3. * amp[1556] - amp[1557] +
      Complex<double> (0, 1) * amp[1560] - 1./3. * amp[1562] - 1./3. *
      amp[1563] - 1./3. * amp[1566] - amp[1569] - amp[1570] - Complex<double>
      (0, 1) * amp[1571] - Complex<double> (0, 1) * amp[1572] - amp[1573] +
      Complex<double> (0, 1) * amp[1574] - Complex<double> (0, 1) * amp[1575] +
      Complex<double> (0, 1) * amp[1576] + Complex<double> (0, 1) * amp[1577] -
      amp[1579] - amp[1581] + Complex<double> (0, 1) * amp[1582] +
      Complex<double> (0, 1) * amp[1583] + Complex<double> (0, 1) * amp[1584] -
      Complex<double> (0, 1) * amp[1585] + Complex<double> (0, 1) * amp[1586] +
      Complex<double> (0, 1) * amp[1587] - amp[1588] - amp[1589] +
      Complex<double> (0, 1) * amp[1590] - amp[1591] + Complex<double> (0, 1) *
      amp[1592] + Complex<double> (0, 1) * amp[1593] - amp[1594] +
      Complex<double> (0, 1) * amp[1596] + Complex<double> (0, 1) * amp[1597] +
      Complex<double> (0, 1) * amp[1600] + Complex<double> (0, 1) * amp[1602] +
      Complex<double> (0, 1) * amp[1603] - amp[1605] + Complex<double> (0, 1) *
      amp[1606] + Complex<double> (0, 1) * amp[1607] - amp[1608] - amp[1609] +
      Complex<double> (0, 1) * amp[1610] - amp[1611] + Complex<double> (0, 1) *
      amp[1612] + Complex<double> (0, 1) * amp[1613] + Complex<double> (0, 1) *
      amp[1614] + Complex<double> (0, 1) * amp[1617] + Complex<double> (0, 1) *
      amp[1619] - 1./3. * amp[1664] - 1./3. * amp[1665] - 1./3. * amp[1668] -
      1./3. * amp[1669] - amp[1671] - Complex<double> (0, 1) * amp[1672] -
      Complex<double> (0, 1) * amp[1673] - amp[1674] - amp[1678] - 1./3. *
      amp[1679] + Complex<double> (0, 1) * amp[1680] + Complex<double> (0, 1) *
      amp[1684] - Complex<double> (0, 1) * amp[1687] - Complex<double> (0, 1) *
      amp[1688] + Complex<double> (0, 1) * amp[1689] - Complex<double> (0, 1) *
      amp[1691] - amp[1692] - 1./3. * amp[1693] + Complex<double> (0, 1) *
      amp[1697] - 1./3. * amp[1698] - 1./3. * amp[1701] - 1./3. * amp[1702] -
      1./3. * amp[1706] + Complex<double> (0, 1) * amp[1710] - 1./3. *
      amp[1712] - Complex<double> (0, 1) * amp[1716] - Complex<double> (0, 1) *
      amp[1717] + Complex<double> (0, 1) * amp[1718] - Complex<double> (0, 1) *
      amp[1720] - 1./3. * amp[1721] - 1./3. * amp[1722] - 1./3. * amp[1726]);
  jamp[2] = +1./2. * (-1./3. * amp[1218] - amp[1220] - 1./3. * amp[1221] -
      amp[1226] + Complex<double> (0, 1) * amp[1227] - amp[1229] -
      Complex<double> (0, 1) * amp[1230] - amp[1232] - Complex<double> (0, 1) *
      amp[1233] - amp[1234] - 1./3. * amp[1236] - 1./3. * amp[1237] - 1./3. *
      amp[1239] - 1./3. * amp[1240] - 1./3. * amp[1242] - 1./3. * amp[1243] -
      amp[1247] - amp[1249] + Complex<double> (0, 1) * amp[1250] - 1./3. *
      amp[1252] - amp[1253] - 1./3. * amp[1254] - amp[1260] - amp[1265] - 1./3.
      * amp[1266] - amp[1268] - 1./3. * amp[1269] - amp[1274] + Complex<double>
      (0, 1) * amp[1275] - amp[1277] - Complex<double> (0, 1) * amp[1278] -
      amp[1280] - Complex<double> (0, 1) * amp[1281] - amp[1282] - 1./3. *
      amp[1284] - 1./3. * amp[1285] - 1./3. * amp[1287] - 1./3. * amp[1288] -
      1./3. * amp[1290] - 1./3. * amp[1291] - amp[1295] - amp[1297] +
      Complex<double> (0, 1) * amp[1298] - 1./3. * amp[1300] - amp[1301] -
      1./3. * amp[1302] - amp[1308] - amp[1313] - 1./3. * amp[1314] - 1./3. *
      amp[1315] - 1./3. * amp[1316] - amp[1317] - 1./3. * amp[1318] - amp[1319]
      - 1./3. * amp[1320] - amp[1321] - amp[1322] - amp[1323] - 1./3. *
      amp[1324] - 1./3. * amp[1325] - 1./3. * amp[1326] - amp[1327] - 1./3. *
      amp[1328] - amp[1329] - 1./3. * amp[1330] - amp[1331] - amp[1332] -
      amp[1333] - 1./3. * amp[1334] - 1./3. * amp[1335] - 1./3. * amp[1336] -
      1./3. * amp[1337] - 1./3. * amp[1338] - 1./3. * amp[1339] - 1./3. *
      amp[1340] - 1./3. * amp[1341] - 1./3. * amp[1342] - 1./3. * amp[1343] -
      1./3. * amp[1344] - 1./3. * amp[1345] - 1./3. * amp[1346] - amp[1347] -
      amp[1348] - 1./3. * amp[1349] - 1./3. * amp[1350] - amp[1351] - amp[1352]
      - amp[1353] - amp[1354] - amp[1355] - 1./3. * amp[1356] - amp[1357] -
      1./3. * amp[1358] - 1./3. * amp[1359] - amp[1360] - 1./3. * amp[1361] -
      amp[1362] - amp[1363] - amp[1364] - amp[1365] - 1./3. * amp[1366] -
      amp[1367] - amp[1368] - 1./3. * amp[1369] - amp[1370] - 1./3. * amp[1371]
      - 1./3. * amp[1372] - amp[1373] - amp[1374] - amp[1375] - amp[1376] -
      amp[1377] - amp[1378] - amp[1379] - amp[1446] - 1./3. * amp[1447] - 1./3.
      * amp[1448] - 1./3. * amp[1449] - amp[1450] - amp[1451] - 1./3. *
      amp[1452] - amp[1453] - amp[1454] - 1./3. * amp[1455] - amp[1456] - 1./3.
      * amp[1457] - 1./3. * amp[1458] - 1./3. * amp[1459] - amp[1460] -
      amp[1461] - 1./3. * amp[1462] - amp[1463] - amp[1464] - 1./3. * amp[1465]
      - amp[1466] - amp[1467] - amp[1468] - amp[1469] - amp[1470] - amp[1471] -
      amp[1472] - amp[1473] - amp[1474] - amp[1475] - amp[1476] - amp[1477] -
      amp[1478] - 1./3. * amp[1479] - 1./3. * amp[1480] - 1./3. * amp[1481] -
      1./3. * amp[1482] - 1./3. * amp[1483] - 1./3. * amp[1484] - 1./3. *
      amp[1485] - 1./3. * amp[1486] - 1./3. * amp[1487] - 1./3. * amp[1488] -
      1./3. * amp[1489] - 1./3. * amp[1490] - 1./3. * amp[1491] - 1./3. *
      amp[1492] - 1./3. * amp[1493] - amp[1494] - amp[1495] - amp[1496] -
      amp[1497] - amp[1498] - 1./3. * amp[1499] - amp[1500] - 1./3. * amp[1501]
      - 1./3. * amp[1502] - 1./3. * amp[1503] - amp[1504] - 1./3. * amp[1505] -
      amp[1506] - amp[1507] - amp[1508] - 1./3. * amp[1509] - 1./3. * amp[1510]
      - 1./3. * amp[1511] - 1./3. * amp[1513] - amp[1516] + Complex<double> (0,
      1) * amp[1517] - Complex<double> (0, 1) * amp[1520] - amp[1526] - 1./3. *
      amp[1527] - Complex<double> (0, 1) * amp[1528] + Complex<double> (0, 1) *
      amp[1539] + Complex<double> (0, 1) * amp[1540] - Complex<double> (0, 1) *
      amp[1542] + Complex<double> (0, 1) * amp[1543] - 1./3. * amp[1545] -
      amp[1548] + Complex<double> (0, 1) * amp[1549] - Complex<double> (0, 1) *
      amp[1552] - amp[1558] - 1./3. * amp[1559] - Complex<double> (0, 1) *
      amp[1560] + Complex<double> (0, 1) * amp[1571] + Complex<double> (0, 1) *
      amp[1572] - Complex<double> (0, 1) * amp[1574] + Complex<double> (0, 1) *
      amp[1575] - Complex<double> (0, 1) * amp[1576] - Complex<double> (0, 1) *
      amp[1577] - amp[1578] - amp[1580] - Complex<double> (0, 1) * amp[1582] -
      Complex<double> (0, 1) * amp[1583] - Complex<double> (0, 1) * amp[1584] +
      Complex<double> (0, 1) * amp[1585] - Complex<double> (0, 1) * amp[1586] -
      Complex<double> (0, 1) * amp[1587] - Complex<double> (0, 1) * amp[1590] -
      Complex<double> (0, 1) * amp[1592] - Complex<double> (0, 1) * amp[1593] -
      amp[1595] - Complex<double> (0, 1) * amp[1596] - Complex<double> (0, 1) *
      amp[1597] - amp[1598] - amp[1599] - Complex<double> (0, 1) * amp[1600] -
      amp[1601] - Complex<double> (0, 1) * amp[1602] - Complex<double> (0, 1) *
      amp[1603] - amp[1604] - Complex<double> (0, 1) * amp[1606] -
      Complex<double> (0, 1) * amp[1607] - Complex<double> (0, 1) * amp[1610] -
      Complex<double> (0, 1) * amp[1612] - Complex<double> (0, 1) * amp[1613] -
      Complex<double> (0, 1) * amp[1614] - amp[1615] - amp[1616] -
      Complex<double> (0, 1) * amp[1617] - amp[1618] - Complex<double> (0, 1) *
      amp[1619] - 1./3. * amp[1622] - 1./3. * amp[1623] - 1./3. * amp[1624] -
      1./3. * amp[1625] - 1./3. * amp[1632] - 1./3. * amp[1633] - 1./3. *
      amp[1635] - 1./3. * amp[1638] - 1./3. * amp[1639] - 1./3. * amp[1642] -
      1./3. * amp[1643] - 1./3. * amp[1645] - 1./3. * amp[1648] - 1./3. *
      amp[1649] - 1./3. * amp[1652] - 1./3. * amp[1653] - 1./3. * amp[1655] -
      1./3. * amp[1659] - 1./3. * amp[1660] - 1./3. * amp[1662] - amp[1670] +
      Complex<double> (0, 1) * amp[1672] + Complex<double> (0, 1) * amp[1673] -
      amp[1675] - 1./3. * amp[1676] - amp[1677] - Complex<double> (0, 1) *
      amp[1680] - amp[1682] - amp[1683] - Complex<double> (0, 1) * amp[1684] -
      amp[1685] - amp[1686] + Complex<double> (0, 1) * amp[1687] +
      Complex<double> (0, 1) * amp[1688] - Complex<double> (0, 1) * amp[1689] -
      amp[1690] + Complex<double> (0, 1) * amp[1691] - 1./3. * amp[1694] -
      amp[1695] - Complex<double> (0, 1) * amp[1697] - 1./3. * amp[1699] -
      amp[1708] - amp[1709] - Complex<double> (0, 1) * amp[1710] - 1./3. *
      amp[1711] - amp[1714] - amp[1715] + Complex<double> (0, 1) * amp[1716] +
      Complex<double> (0, 1) * amp[1717] - Complex<double> (0, 1) * amp[1718] -
      amp[1719] + Complex<double> (0, 1) * amp[1720]);
  jamp[3] = +1./2. * (+1./3. * amp[1152] + 1./3. * amp[1153] + amp[1154] +
      amp[1155] + amp[1156] + amp[1157] + amp[1158] + 1./3. * amp[1159] + 1./3.
      * amp[1160] + 1./3. * amp[1161] + amp[1162] + amp[1163] + amp[1164] +
      1./3. * amp[1165] + amp[1166] + 1./3. * amp[1167] + amp[1168] + 1./3. *
      amp[1169] + 1./3. * amp[1170] + 1./3. * amp[1171] + 1./3. * amp[1172] +
      1./3. * amp[1173] + 1./3. * amp[1174] + amp[1175] + 1./3. * amp[1176] +
      amp[1177] + 1./3. * amp[1178] + amp[1179] + amp[1180] + amp[1181] +
      amp[1182] + 1./3. * amp[1183] + 1./3. * amp[1184] + 1./3. * amp[1185] +
      amp[1186] + amp[1187] + 1./3. * amp[1188] + amp[1189] + amp[1190] + 1./3.
      * amp[1191] + amp[1192] + amp[1193] + amp[1194] + 1./3. * amp[1195] +
      1./3. * amp[1196] + 1./3. * amp[1197] + 1./3. * amp[1198] + 1./3. *
      amp[1199] + 1./3. * amp[1200] + amp[1201] + amp[1202] + amp[1203] +
      amp[1204] + 1./3. * amp[1205] + 1./3. * amp[1206] + 1./3. * amp[1207] +
      amp[1208] + amp[1209] + amp[1210] + amp[1211] + amp[1212] + amp[1213] +
      1./3. * amp[1214] + 1./3. * amp[1215] + 1./3. * amp[1216] + 1./3. *
      amp[1217] + amp[1218] + amp[1222] + Complex<double> (0, 1) * amp[1223] +
      1./3. * amp[1225] + 1./3. * amp[1226] + amp[1236] - Complex<double> (0,
      1) * amp[1238] + amp[1239] - Complex<double> (0, 1) * amp[1241] +
      amp[1243] + amp[1244] + 1./3. * amp[1248] + 1./3. * amp[1249] + 1./3. *
      amp[1251] + amp[1252] + 1./3. * amp[1253] + amp[1255] + Complex<double>
      (0, 1) * amp[1257] + amp[1258] + 1./3. * amp[1259] + 1./3. * amp[1260] +
      amp[1262] + 1./3. * amp[1264] + 1./3. * amp[1265] + amp[1266] + amp[1270]
      + Complex<double> (0, 1) * amp[1271] + 1./3. * amp[1273] + 1./3. *
      amp[1274] + amp[1284] - Complex<double> (0, 1) * amp[1286] + amp[1287] -
      Complex<double> (0, 1) * amp[1289] + amp[1291] + amp[1292] + 1./3. *
      amp[1296] + 1./3. * amp[1297] + 1./3. * amp[1299] + amp[1300] + 1./3. *
      amp[1301] + amp[1303] + Complex<double> (0, 1) * amp[1305] + amp[1306] +
      1./3. * amp[1307] + 1./3. * amp[1308] + amp[1310] + 1./3. * amp[1312] +
      1./3. * amp[1313] + amp[1314] + amp[1315] + amp[1316] + 1./3. * amp[1317]
      + amp[1318] + 1./3. * amp[1319] + amp[1320] + 1./3. * amp[1321] + 1./3. *
      amp[1322] + 1./3. * amp[1323] + amp[1324] + amp[1325] + amp[1326] + 1./3.
      * amp[1327] + amp[1328] + 1./3. * amp[1329] + amp[1330] + 1./3. *
      amp[1331] + 1./3. * amp[1332] + 1./3. * amp[1333] + amp[1334] + amp[1335]
      + amp[1336] + amp[1337] + amp[1338] + amp[1339] + amp[1340] + amp[1341] +
      amp[1342] + amp[1343] + amp[1344] + amp[1345] + amp[1346] + 1./3. *
      amp[1347] + 1./3. * amp[1348] + amp[1349] + amp[1350] + 1./3. * amp[1351]
      + 1./3. * amp[1352] + 1./3. * amp[1353] + 1./3. * amp[1354] + 1./3. *
      amp[1355] + amp[1356] + 1./3. * amp[1357] + amp[1358] + amp[1359] + 1./3.
      * amp[1360] + amp[1361] + 1./3. * amp[1362] + 1./3. * amp[1363] + 1./3. *
      amp[1364] + 1./3. * amp[1365] + amp[1366] + 1./3. * amp[1367] + 1./3. *
      amp[1368] + amp[1369] + 1./3. * amp[1370] + amp[1371] + amp[1372] + 1./3.
      * amp[1373] + 1./3. * amp[1374] + 1./3. * amp[1375] + 1./3. * amp[1376] +
      1./3. * amp[1377] + 1./3. * amp[1378] + 1./3. * amp[1379] + amp[1512] +
      amp[1513] - Complex<double> (0, 1) * amp[1514] + 1./3. * amp[1515] +
      1./3. * amp[1516] + 1./3. * amp[1518] + amp[1521] + Complex<double> (0,
      1) * amp[1523] + amp[1524] + 1./3. * amp[1525] - Complex<double> (0, 1) *
      amp[1529] + amp[1530] + amp[1531] + Complex<double> (0, 1) * amp[1532] +
      Complex<double> (0, 1) * amp[1533] + amp[1534] - Complex<double> (0, 1) *
      amp[1535] + Complex<double> (0, 1) * amp[1536] + 1./3. * amp[1537] +
      1./3. * amp[1538] + 1./3. * amp[1541] + amp[1544] + amp[1545] -
      Complex<double> (0, 1) * amp[1546] + 1./3. * amp[1547] + 1./3. *
      amp[1548] + 1./3. * amp[1550] + amp[1553] + Complex<double> (0, 1) *
      amp[1555] + amp[1556] + 1./3. * amp[1557] - Complex<double> (0, 1) *
      amp[1561] + amp[1562] + amp[1563] + Complex<double> (0, 1) * amp[1564] +
      Complex<double> (0, 1) * amp[1565] + amp[1566] - Complex<double> (0, 1) *
      amp[1567] + Complex<double> (0, 1) * amp[1568] + 1./3. * amp[1569] +
      1./3. * amp[1570] + 1./3. * amp[1573] - Complex<double> (0, 1) *
      amp[1620] - Complex<double> (0, 1) * amp[1621] + amp[1623] + amp[1625] -
      Complex<double> (0, 1) * amp[1626] - Complex<double> (0, 1) * amp[1627] -
      Complex<double> (0, 1) * amp[1628] + Complex<double> (0, 1) * amp[1629] -
      Complex<double> (0, 1) * amp[1630] - Complex<double> (0, 1) * amp[1631] +
      amp[1632] + amp[1633] - Complex<double> (0, 1) * amp[1634] + amp[1635] -
      Complex<double> (0, 1) * amp[1636] - Complex<double> (0, 1) * amp[1637] +
      amp[1638] - Complex<double> (0, 1) * amp[1640] - Complex<double> (0, 1) *
      amp[1641] - Complex<double> (0, 1) * amp[1644] - Complex<double> (0, 1) *
      amp[1646] - Complex<double> (0, 1) * amp[1647] + amp[1649] -
      Complex<double> (0, 1) * amp[1650] - Complex<double> (0, 1) * amp[1651] +
      amp[1652] + amp[1653] - Complex<double> (0, 1) * amp[1654] + amp[1655] -
      Complex<double> (0, 1) * amp[1656] - Complex<double> (0, 1) * amp[1657] -
      Complex<double> (0, 1) * amp[1658] - Complex<double> (0, 1) * amp[1661] -
      Complex<double> (0, 1) * amp[1663] + amp[1665] + Complex<double> (0, 1) *
      amp[1666] + Complex<double> (0, 1) * amp[1667] + amp[1668] + 1./3. *
      amp[1670] + 1./3. * amp[1671] + 1./3. * amp[1674] + 1./3. * amp[1675] +
      amp[1676] + 1./3. * amp[1677] - Complex<double> (0, 1) * amp[1681] +
      1./3. * amp[1682] + 1./3. * amp[1685] + 1./3. * amp[1686] + 1./3. *
      amp[1690] + amp[1694] + 1./3. * amp[1695] - Complex<double> (0, 1) *
      amp[1696] - Complex<double> (0, 1) * amp[1700] + Complex<double> (0, 1) *
      amp[1703] + Complex<double> (0, 1) * amp[1704] - Complex<double> (0, 1) *
      amp[1705] + Complex<double> (0, 1) * amp[1707] + 1./3. * amp[1709] -
      Complex<double> (0, 1) * amp[1713] + 1./3. * amp[1714] + 1./3. *
      amp[1715] + 1./3. * amp[1719] + Complex<double> (0, 1) * amp[1723] +
      Complex<double> (0, 1) * amp[1724] - Complex<double> (0, 1) * amp[1725] +
      Complex<double> (0, 1) * amp[1727]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[2][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}


}  // end namespace PY8MEs_namespace

