//==========================================================================
// This file has been automatically generated for C++
// MadGraph5_aMC@NLO v. 2.6.0, 2017-08-16
// By the MadGraph5_aMC@NLO Development Team
// Visit launchpad.net/madgraph5 and amcatnlo.web.cern.ch
//==========================================================================

#ifndef Parameters_heft_H
#define Parameters_heft_H

#include "Complex.h"

#include "read_slha.h"
using namespace std; 

class Parameters_heft
{
  public:

    // Define "zero"
    double zero, ZERO; 
    // Model parameters independent of aS
    double mdl_WH1, mdl_WH, mdl_WW, mdl_WZ, mdl_WT, mdl_ymtau, mdl_ymt,
        mdl_ymb, aS, mdl_Gf, aEWM1, mdl_MP, mdl_MH, mdl_MZ, mdl_MTA, mdl_MT,
        mdl_MB, mdl_conjg__CKM3x3, mdl_CKM3x3, mdl_MZ__exp__2, mdl_MZ__exp__4,
        mdl_sqrt__2, mdl_MH__exp__4, mdl_MT__exp__4, mdl_MH__exp__2,
        mdl_MT__exp__2, mdl_MH__exp__12, mdl_MH__exp__10, mdl_MH__exp__8,
        mdl_MH__exp__6, mdl_MT__exp__6, mdl_aEW, mdl_MW, mdl_sqrt__aEW, mdl_ee,
        mdl_MW__exp__2, mdl_sw2, mdl_cw, mdl_sqrt__sw2, mdl_sw, mdl_g1, mdl_gw,
        mdl_v, mdl_ee__exp__2, mdl_MW__exp__12, mdl_MW__exp__10,
        mdl_MW__exp__8, mdl_MW__exp__6, mdl_MW__exp__4, mdl_AH, mdl_v__exp__2,
        mdl_lam, mdl_yb, mdl_yt, mdl_ytau, mdl_muH, mdl_gw__exp__2,
        mdl_cw__exp__2, mdl_sw__exp__2;
    Complex<double> mdl_complexi; 
    // Model parameters dependent on aS
    double mdl_sqrt__aS, G, mdl_G__exp__2, mdl_GH, mdl_Gphi; 
    // Model couplings independent of aS
    Complex<double> GC_1, GC_2, GC_3, GC_4, GC_6, GC_7, GC_8, GC_9, GC_19,
        GC_20, GC_21, GC_22, GC_23, GC_24, GC_25, GC_26, GC_27, GC_28, GC_29,
        GC_40, GC_41, GC_42, GC_43, GC_44, GC_45, GC_46, GC_47, GC_48, GC_49,
        GC_50, GC_51, GC_52, GC_53, GC_54, GC_55, GC_56, GC_57, GC_58, GC_59,
        GC_60, GC_61, GC_62, GC_63, GC_64, GC_65, GC_66, GC_67, GC_68, GC_69,
        GC_70, GC_73, GC_74, GC_75, GC_78, GC_79, GC_80, GC_83, GC_84, GC_85,
        GC_86, GC_87, GC_100, GC_101, GC_102;
    // Model couplings dependent on aS
    Complex<double> GC_17, GC_16, GC_15, GC_14, GC_13, GC_12, GC_11, GC_10; 

    // Set parameters that are unchanged during the run
    void setIndependentParameters(SLHAReader& slha); 
    // Set couplings that are unchanged during the run
    void setIndependentCouplings(); 
    // Set parameters that are changed event by event
    void setDependentParameters(); 
    // Set couplings that are changed event by event
    void setDependentCouplings(); 

    // Print parameters that are unchanged during the run
    void printIndependentParameters(); 
    // Print couplings that are unchanged during the run
    void printIndependentCouplings(); 
    // Print parameters that are changed event by event
    void printDependentParameters(); 
    // Print couplings that are changed event by event
    void printDependentCouplings(); 

}; 

#endif  // Parameters_heft_H

