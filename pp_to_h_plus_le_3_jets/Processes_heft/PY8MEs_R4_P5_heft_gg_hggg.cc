//==========================================================================
// This file has been automatically generated for Pythia 8 by
// MadGraph5_aMC@NLO v. 2.6.0, 2017-08-16
// By the MadGraph5_aMC@NLO Development Team
// Visit launchpad.net/madgraph5 and amcatnlo.web.cern.ch
//==========================================================================

#include "PY8MEs_R4_P5_heft_gg_hggg.h"
#include "HelAmps_heft.h"

using namespace Pythia8_heft; 

namespace PY8MEs_namespace 
{
//==========================================================================
// Class member functions for calculating the matrix elements for
// Process: g g > h g g g HIG<=1 HIW<=1 WEIGHTED<=5 @4

// Exception class
class PY8MEs_R4_P5_heft_gg_hgggException : public exception
{
  virtual const char * what() const throw()
  {
    return "Exception in class 'PY8MEs_R4_P5_heft_gg_hggg'."; 
  }
}
PY8MEs_R4_P5_heft_gg_hggg_exception; 

std::set<int> PY8MEs_R4_P5_heft_gg_hggg::s_channel_proc = std::set<int>
    (createset<int> ());

int PY8MEs_R4_P5_heft_gg_hggg::helicities[ncomb][nexternal] = {{-1, -1, 0, -1,
    -1, -1}, {-1, -1, 0, -1, -1, 1}, {-1, -1, 0, -1, 1, -1}, {-1, -1, 0, -1, 1,
    1}, {-1, -1, 0, 1, -1, -1}, {-1, -1, 0, 1, -1, 1}, {-1, -1, 0, 1, 1, -1},
    {-1, -1, 0, 1, 1, 1}, {-1, 1, 0, -1, -1, -1}, {-1, 1, 0, -1, -1, 1}, {-1,
    1, 0, -1, 1, -1}, {-1, 1, 0, -1, 1, 1}, {-1, 1, 0, 1, -1, -1}, {-1, 1, 0,
    1, -1, 1}, {-1, 1, 0, 1, 1, -1}, {-1, 1, 0, 1, 1, 1}, {1, -1, 0, -1, -1,
    -1}, {1, -1, 0, -1, -1, 1}, {1, -1, 0, -1, 1, -1}, {1, -1, 0, -1, 1, 1},
    {1, -1, 0, 1, -1, -1}, {1, -1, 0, 1, -1, 1}, {1, -1, 0, 1, 1, -1}, {1, -1,
    0, 1, 1, 1}, {1, 1, 0, -1, -1, -1}, {1, 1, 0, -1, -1, 1}, {1, 1, 0, -1, 1,
    -1}, {1, 1, 0, -1, 1, 1}, {1, 1, 0, 1, -1, -1}, {1, 1, 0, 1, -1, 1}, {1, 1,
    0, 1, 1, -1}, {1, 1, 0, 1, 1, 1}};

// Normalization factors the various processes
// Denominators: spins, colors and identical particles
int PY8MEs_R4_P5_heft_gg_hggg::denom_colors[nprocesses] = {64}; 
int PY8MEs_R4_P5_heft_gg_hggg::denom_hels[nprocesses] = {4}; 
int PY8MEs_R4_P5_heft_gg_hggg::denom_iden[nprocesses] = {6}; 

//--------------------------------------------------------------------------
// Color config initialization
void PY8MEs_R4_P5_heft_gg_hggg::initColorConfigs() 
{
  color_configs = vector < vec_vec_int > (); 
  jamp_nc_relative_power = vector < vec_int > (); 

  // Color flows of process Process: g g > h g g g HIG<=1 HIW<=1 WEIGHTED<=5 @4
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(1)(1)(2)(0)(0)(3)(2)(4)(3)(5)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #1
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(1)(1)(2)(0)(0)(3)(2)(4)(5)(5)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #2
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(1)(1)(2)(0)(0)(3)(4)(4)(2)(5)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #3
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(1)(1)(2)(0)(0)(3)(5)(4)(2)(5)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #4
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(1)(1)(2)(0)(0)(3)(5)(4)(3)(5)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #5
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(1)(1)(2)(0)(0)(3)(4)(4)(5)(5)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #6
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(1)(3)(2)(0)(0)(3)(1)(4)(2)(5)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #7
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(1)(3)(2)(0)(0)(3)(1)(4)(5)(5)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #8
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(1)(4)(2)(0)(0)(3)(1)(4)(3)(5)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #9
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(1)(5)(2)(0)(0)(3)(1)(4)(3)(5)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #10
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(1)(5)(2)(0)(0)(3)(1)(4)(2)(5)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #11
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(1)(4)(2)(0)(0)(3)(1)(4)(5)(5)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #12
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(1)(4)(2)(0)(0)(3)(2)(4)(1)(5)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #13
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(1)(4)(2)(0)(0)(3)(5)(4)(1)(5)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #14
  color_configs[0].push_back(vec_int(createvector<int>
      (5)(1)(3)(2)(0)(0)(3)(4)(4)(1)(5)(2)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #15
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(1)(5)(2)(0)(0)(3)(4)(4)(1)(5)(3)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #16
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(1)(5)(2)(0)(0)(3)(2)(4)(1)(5)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #17
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(1)(3)(2)(0)(0)(3)(5)(4)(1)(5)(4)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #18
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(1)(5)(2)(0)(0)(3)(2)(4)(3)(5)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #19
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(1)(5)(2)(0)(0)(3)(4)(4)(2)(5)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #20
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(1)(3)(2)(0)(0)(3)(5)(4)(2)(5)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #21
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(1)(4)(2)(0)(0)(3)(5)(4)(3)(5)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #22
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(1)(4)(2)(0)(0)(3)(2)(4)(5)(5)(1)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #23
  color_configs[0].push_back(vec_int(createvector<int>
      (2)(1)(3)(2)(0)(0)(3)(4)(4)(5)(5)(1)));
  jamp_nc_relative_power[0].push_back(0); 
}

//--------------------------------------------------------------------------
// Destructor.
PY8MEs_R4_P5_heft_gg_hggg::~PY8MEs_R4_P5_heft_gg_hggg() 
{
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    delete[] p[i]; 
    p[i] = NULL; 
  }
}

//--------------------------------------------------------------------------
// Invert the permutation mapping
vector<int> PY8MEs_R4_P5_heft_gg_hggg::invert_mapping(vector<int> mapping) 
{
  vector<int> inverted_mapping; 
  for (unsigned int i = 0; i < mapping.size(); i++ )
  {
    for (unsigned int j = 0; j < mapping.size(); j++ )
    {
      if (mapping[j] == ((int)i))
      {
        inverted_mapping.push_back(j); 
        break; 
      }
    }
  }
  return inverted_mapping; 
}

//--------------------------------------------------------------------------
// Return the list of possible helicity configurations
vector < vec_int > PY8MEs_R4_P5_heft_gg_hggg::getHelicityConfigs(vector<int>
    permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(ncomb, vector<int> (nexternal, 0)); 
  for (unsigned int ihel = 0; ihel < ncomb; ihel++ )
  {
    for(unsigned int j = 0; j < nexternal; j++ )
    {
      res[ihel][chosenPerm[j]] = helicities[ihel][j]; 
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Return the list of possible color configurations
vector < vec_int > PY8MEs_R4_P5_heft_gg_hggg::getColorConfigs(int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(color_configs[chosenProcID].size(), vector<int>
      (nexternal * 2, 0));
  for (unsigned int icol = 0; icol < color_configs[chosenProcID].size(); icol++
      )
  {
    for(unsigned int j = 0; j < (2 * nexternal); j++ )
    {
      res[icol][chosenPerm[j/2] * 2 + j%2] =
          color_configs[chosenProcID][icol][j];
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Get JAMP relative N_c power
int PY8MEs_R4_P5_heft_gg_hggg::getColorFlowRelativeNCPower(int color_flow_ID,
    int specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return jamp_nc_relative_power[chosenProcID][color_flow_ID]; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity ID -> Helicity Config
vector<int> PY8MEs_R4_P5_heft_gg_hggg::getHelicityConfigForID(int hel_ID,
    vector<int> permutation)
{
  if (hel_ID < 0 || hel_ID >= ncomb)
  {
    cerr <<  "Error in function 'getHelicityConfigForID' of class" << 
    " 'PY8MEs_R4_P5_heft_gg_hggg': Specified helicity ID '" << 
    hel_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R4_P5_heft_gg_hggg_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(nexternal, 0); 
  for (unsigned int j = 0; j < nexternal; j++ )
  {
    res[chosenPerm[j]] = helicities[hel_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity Config -> Helicity ID
int PY8MEs_R4_P5_heft_gg_hggg::getHelicityIDForConfig(vector<int> hel_config,
    vector<int> permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  int user_ihel = -1; 
  if (hel_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < ncomb; i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < nexternal; j++ )
      {
        if (helicities[i][chosenPerm[j]] != hel_config[j])
        {
          found = false; 
          break; 
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_ihel = i; 
        break; 
      }
    }
    if (user_ihel == -1)
    {
      cerr <<  "Error in function 'getHelicityIDForConfig' of class" << 
      " 'PY8MEs_R4_P5_heft_gg_hggg': Specified helicity" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R4_P5_heft_gg_hggg_exception; 
    }
  }
  return user_ihel; 
}


//--------------------------------------------------------------------------
// Implements the map Color ID -> Color Config
vector<int> PY8MEs_R4_P5_heft_gg_hggg::getColorConfigForID(int color_ID, int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < 0 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getColorConfigForID' of class" << 
    " 'PY8MEs_R4_P5_heft_gg_hggg': Specified color ID '" << 
    color_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R4_P5_heft_gg_hggg_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(color_configs[chosenProcID][color_ID].size(), 0); 
  for (unsigned int j = 0; j < (2 * nexternal); j++ )
  {
    res[chosenPerm[j/2] * 2 + j%2] = color_configs[chosenProcID][color_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Color Config -> Color ID
int PY8MEs_R4_P5_heft_gg_hggg::getColorIDForConfig(vector<int> color_config,
    int specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = -1; 
  if (color_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < color_configs[chosenProcID].size(); i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < (nexternal * 2); j++ )
      {

        // If colorless then make sure it matches
        // The little arithmetics in the color index is just
        // the permutation applies on the particle list which is
        // twice smaller since each particle can have two color indices.
        if (color_config[j] == 0)
        {
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] != 0)
          {
            found = false; 
            break; 
          }
          // Otherwise check that the color linked position matches
        }
        else
        {
          int color_linked_pos = -1; 
          // Find the other end of the line in the user color config
          for (unsigned int k = 0; k < (nexternal * 2); k++ )
          {
            if (k == j)
              continue; 
            if (color_config[j] == color_config[k])
            {
              color_linked_pos = k; 
              break; 
            }
          }
          if (color_linked_pos == -1)
          {
            cerr <<  "Error in function 'getColorIDForConfig' of class" << 
            " 'PY8MEs_R4_P5_heft_gg_hggg': A color line could " << 
            " not be closed." << endl; 
            throw PY8MEs_R4_P5_heft_gg_hggg_exception; 
          }
          // Now check whether the color line matches
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] !=
              color_configs[chosenProcID][i][chosenPerm[color_linked_pos/2] * 2
              + color_linked_pos%2])
          {
            found = false; 
            break; 
          }
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_icol = i; 
        break; 
      }
    }

    if (user_icol == -1)
    {
      cerr <<  "Error in function 'getColorIDForConfig' of class" << 
      " 'PY8MEs_R4_P5_heft_gg_hggg': Specified color" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R4_P5_heft_gg_hggg_exception; 
    }
  }
  return user_icol; 
}

//--------------------------------------------------------------------------
// Returns all result previously computed in SigmaKin
vector < vec_double > PY8MEs_R4_P5_heft_gg_hggg::getAllResults(int
    specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return all_results[chosenProcID]; 
}

//--------------------------------------------------------------------------
// Returns a result previously computed in SigmaKin for a specific helicity
// and color ID. -1 means avg and summed over that characteristic.
double PY8MEs_R4_P5_heft_gg_hggg::getResult(int helicity_ID, int color_ID, int
    specify_proc_ID)
{
  if (helicity_ID < - 1 || helicity_ID >= ncomb)
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R4_P5_heft_gg_hggg': Specified helicity ID '" << 
    helicity_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R4_P5_heft_gg_hggg_exception; 
  }
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < - 1 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R4_P5_heft_gg_hggg': Specified color ID '" << 
    color_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R4_P5_heft_gg_hggg_exception; 
  }
  return all_results[chosenProcID][helicity_ID + 1][color_ID + 1]; 
}

//--------------------------------------------------------------------------
// Check for the availability of the requested process and if available,
// If available, this returns the corresponding permutation and Proc_ID to use.
// If not available, this returns a negative Proc_ID.
pair < vector<int> , int > PY8MEs_R4_P5_heft_gg_hggg::static_getPY8ME(vector<int> initial_pdgs, vector<int> final_pdgs, set<int> schannels) 
{

  // Not available return value
  pair < vector<int> , int > NA(vector<int> (), -1); 

  // Check if s-channel requirements match
  if (nreq_s_channels > 0)
  {
    if (schannels != s_channel_proc)
      return NA; 
  }
  else
  {
    if (schannels.size() != 0)
      return NA; 
  }

  // Check number of final state particles
  if (final_pdgs.size() != (nexternal - ninitial))
    return NA; 

  // Check number of initial state particles
  if (initial_pdgs.size() != ninitial)
    return NA; 

  // List of processes available in this class
  const int nprocs = 1; 
  const int proc_IDS[nprocs] = {0}; 
  const int in_pdgs[nprocs][ninitial] = {{21, 21}}; 
  const int out_pdgs[nprocs][nexternal - ninitial] = {{25, 21, 21, 21}}; 

  bool in_pdgs_used[ninitial]; 
  bool out_pdgs_used[nexternal - ninitial]; 
  for(unsigned int i = 0; i < nprocs; i++ )
  {
    int permutations[nexternal]; 

    // Reinitialize initial state look-up variables
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      in_pdgs_used[j] = false; 
      permutations[j] = -1; 
    }
    // Look for initial state matches
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      for(unsigned int k = 0; k < ninitial; k++ )
      {
        // Make sure it has not been used already
        if (in_pdgs_used[k])
          continue; 
        if (initial_pdgs[k] == in_pdgs[i][j])
        {
          permutations[j] = k; 
          in_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[ninitial - 1] == -1)
      continue; 

    // Reinitialize final state look-up variables
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      out_pdgs_used[j] = false; 
      permutations[ninitial + j] = -1; 
    }
    // Look for final state matches
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      for(unsigned int k = 0; k < (nexternal - ninitial); k++ )
      {
        // Make sure it has not been used already
        if (out_pdgs_used[k])
          continue; 
        if (final_pdgs[k] == out_pdgs[i][j])
        {
          permutations[ninitial + j] = ninitial + k; 
          out_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[ninitial + j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[nexternal - 1] == -1)
      continue; 

    // Return process found
    return pair < vector<int> , int > (vector<int> (permutations, permutations
        + nexternal), proc_IDS[i]);
  }

  // No process found
  return NA; 
}

//--------------------------------------------------------------------------
// Set momenta
void PY8MEs_R4_P5_heft_gg_hggg::setMomenta(vector < vec_double > momenta_picked)
{
  if (momenta_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setMomenta' of class" << 
    " 'PY8MEs_R4_P5_heft_gg_hggg': Incorrect number of" << 
    " momenta specified." << endl; 
    throw PY8MEs_R4_P5_heft_gg_hggg_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    if (momenta_picked[i].size() != 4)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R4_P5_heft_gg_hggg': Incorrect number of" << 
      " momenta components specified." << endl; 
      throw PY8MEs_R4_P5_heft_gg_hggg_exception; 
    }
    if (momenta_picked[i][0] < 0.0)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R4_P5_heft_gg_hggg': A momentum was specified" << 
      " with negative energy. Check conventions." << endl; 
      throw PY8MEs_R4_P5_heft_gg_hggg_exception; 
    }
    for (unsigned int j = 0; j < 4; j++ )
    {
      p[i][j] = momenta_picked[i][j]; 
    }
  }
}

//--------------------------------------------------------------------------
// Set color configuration to use. An empty vector means sum over all.
void PY8MEs_R4_P5_heft_gg_hggg::setColors(vector<int> colors_picked)
{
  if (colors_picked.size() == 0)
  {
    user_colors = vector<int> (); 
    return; 
  }
  if (colors_picked.size() != (2 * nexternal))
  {
    cerr <<  "Error in function 'setColors' of class" << 
    " 'PY8MEs_R4_P5_heft_gg_hggg': Incorrect number" << 
    " of colors specified." << endl; 
    throw PY8MEs_R4_P5_heft_gg_hggg_exception; 
  }
  user_colors = vector<int> ((2 * nexternal), 0); 
  for(unsigned int i = 0; i < (2 * nexternal); i++ )
  {
    user_colors[i] = colors_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the helicity configuration to use. Am empty vector means sum over all.
void PY8MEs_R4_P5_heft_gg_hggg::setHelicities(vector<int> helicities_picked) 
{
  if (helicities_picked.size() != nexternal)
  {
    if (helicities_picked.size() == 0)
    {
      user_helicities = vector<int> (); 
      return; 
    }
    cerr <<  "Error in function 'setHelicities' of class" << 
    " 'PY8MEs_R4_P5_heft_gg_hggg': Incorrect number" << 
    " of helicities specified." << endl; 
    throw PY8MEs_R4_P5_heft_gg_hggg_exception; 
  }
  user_helicities = vector<int> (nexternal, 0); 
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    user_helicities[i] = helicities_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the permutation to use (will apply to momenta, colors and helicities)
void PY8MEs_R4_P5_heft_gg_hggg::setPermutation(vector<int> perm_picked) 
{
  if (perm_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setPermutations' of class" << 
    " 'PY8MEs_R4_P5_heft_gg_hggg': Incorrect number" << 
    " of permutations specified." << endl; 
    throw PY8MEs_R4_P5_heft_gg_hggg_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    perm[i] = perm_picked[i]; 
  }
}

// Set the proc_ID to use
void PY8MEs_R4_P5_heft_gg_hggg::setProcID(int procID_picked) 
{
  proc_ID = procID_picked; 
}

//--------------------------------------------------------------------------
// Initialize process.

void PY8MEs_R4_P5_heft_gg_hggg::initProc() 
{

  // Initialize flags
  include_symmetry_factors = true; 
  include_helicity_averaging_factors = true; 
  include_color_averaging_factors = true; 

  // Initialize vectors.
  perm = vector<int> (nexternal, 0); 
  user_colors = vector<int> (2 * nexternal, 0); 
  user_helicities = vector<int> (nexternal, 0); 
  p = vector < double * > (); 
  for (unsigned int i = 0; i < nexternal; i++ )
  {
    p.push_back(new double[4]); 
  }
  initColorConfigs(); 
  // Synchronize local variables dependent on the model with the active model.
  mME = vector<double> (nexternal, 0.); 
  syncProcModelParams(); 
  jamp2 = vector < vec_double > (1); 
  jamp2[0] = vector<double> (24, 0.); 
  all_results = vector < vec_vec_double > (1); 
  // The first entry is always the color or helicity avg/summed matrix element.
  all_results[0] = vector < vec_double > (ncomb + 1, vector<double> (24 + 1,
      0.));
}

// Synchronize local variables of the process that depend on the model
// parameters
void PY8MEs_R4_P5_heft_gg_hggg::syncProcModelParams() 
{

  // Instantiate the model class and set parameters that stay fixed during run
  mME[0] = pars->ZERO; 
  mME[1] = pars->ZERO; 
  mME[2] = pars->mdl_MH; 
  mME[3] = pars->ZERO; 
  mME[4] = pars->ZERO; 
  mME[5] = pars->ZERO; 
}

//--------------------------------------------------------------------------
// Setter allowing to force particular values for the external masses
void PY8MEs_R4_P5_heft_gg_hggg::setMasses(vec_double external_masses) 
{

  if (external_masses.size() != mME.size())
  {
    cerr <<  "Error in function 'setMasses' of class" << 
    " 'PY8MEs_R4_P5_heft_gg_hggg': Incorrect number of" << 
    " masses specified." << endl; 
    throw PY8MEs_R4_P5_heft_gg_hggg_exception; 
  }
  for (unsigned int j = 0; j < mME.size(); j++ )
  {
    mME[j] = external_masses[perm[j]]; 
  }
}

//--------------------------------------------------------------------------
// Getter accessing external masses with the correct ordering
vector<double> PY8MEs_R4_P5_heft_gg_hggg::getMasses() 
{

  vec_double external_masses; 
  vector<int> invertedPerm; 
  invertedPerm = invert_mapping(perm); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    external_masses.push_back(mME[invertedPerm[i]]); 
  }
  return external_masses; 

}


// Set all values of the external masses to float(-mode) where mode can be
// 0 : Mass taken from the model
// 1 : Mass taken from p_i^2 if not massless to begin with
// 2 : Mass always taken from p_i^2.
void PY8MEs_R4_P5_heft_gg_hggg::setExternalMassesMode(int mode) 
{
  if (mode != 0 && mode != 1 && mode != 2)
  {
    cerr <<  "Error in function 'setExternalMassesMode' of class" << 
    " 'PY8MEs_R4_P5_heft_gg_hggg': Incorrect mode selected :" << mode << 
    ". It must be either 0, 1 or 2" << endl; 
    throw PY8MEs_R4_P5_heft_gg_hggg_exception; 
  }
  if (mode == 0)
  {
    syncProcModelParams(); 
  }
  else if (mode == 1)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      if (mME[j] != pars->ZERO)
      {
        mME[j] = -1.0; 
      }
    }
  }
  else if (mode == 2)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      mME[j] = -1.0; 
    }
  }
}

//--------------------------------------------------------------------------
// Evaluate the squared matrix element.

double PY8MEs_R4_P5_heft_gg_hggg::sigmaKin() 
{
  // Set the parameters which change event by event
  pars->setDependentParameters(); 
  pars->setDependentCouplings(); 
  // Reset color flows
  for(int i = 0; i < 24; i++ )
    jamp2[0][i] = 0.; 

  // Save previous values of mME
  vector<double> saved_mME(mME.size(), 0.0); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (mME[i] < 0.0)
    {
      saved_mME[i] = mME[i]; 
      mME[i] = sqrt(abs(pow(p[perm[i]][0], 2) - 
      (pow(p[perm[i]][1], 2) + pow(p[perm[i]][2], 2) + pow(p[perm[i]][3],
          2))));
    }
  }

  // Local variables and constants
  const int max_tries = 10; 
  vector < vec_bool > goodhel(nprocesses, vec_bool(ncomb, false)); 
  vec_int ntry(nprocesses, 0); 
  double t = 0.; 
  double result = 0.; 

  if (ntry[proc_ID] <= max_tries)
    ntry[proc_ID] = ntry[proc_ID] + 1; 

  // Find which helicity configuration is asked for
  // -1 indicates one wants to sum over helicities
  int user_ihel = getHelicityIDForConfig(user_helicities); 

  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = getColorIDForConfig(user_colors); 

  // Reset the list of results that will be recomputed here
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      all_results[proc_ID][ihel + 1][icolor + 1] = 0.; 
    }
  }

  // Calculate the matrix element for all helicities
  // unless already detected as vanishing
  for(int ihel = 0; ihel < ncomb; ihel++ )
  {
    // Skip helicity if already detected as vanishing
    if ((ntry[proc_ID] >= max_tries) && !goodhel[proc_ID][ihel])
      continue; 

    // Also skip helicity if user asks for a specific one
    if ((ntry[proc_ID] >= max_tries) && user_ihel != -1 && user_ihel != ihel)
      continue; 

    calculate_wavefunctions(helicities[ihel]); 

    // Reset locally computed color flows
    for(int i = 0; i < 24; i++ )
      jamp2[0][i] = 0.; 

    if (proc_ID == 0)
      t = matrix_4_gg_hggg(); 

    // Store which helicities give non-zero result
    if ((ntry[proc_ID] < max_tries) && t != 0. && !goodhel[proc_ID][ihel])
      goodhel[proc_ID][ihel] = true; 

    // Aggregate results
    if (user_ihel == -1 || user_ihel == ihel)
    {
      if (user_icol == -1)
      {
        result = result + t; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][0] += t; 
          for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
          {
            all_results[proc_ID][0][i + 1] += jamp2[proc_ID][i]; 
          }
        }
        all_results[proc_ID][ihel + 1][0] += t; 
        for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
        {
          all_results[proc_ID][ihel + 1][i + 1] += jamp2[proc_ID][i]; 
        }
      }
      else
      {
        result = result + jamp2[proc_ID][user_icol]; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][user_icol + 1] += jamp2[proc_ID][user_icol]; 
        }
        all_results[proc_ID][ihel + 1][user_icol + 1] +=
            jamp2[proc_ID][user_icol];
      }
    }

  }

  // Normalize results with the identical particle factor
  if (include_symmetry_factors)
  {
    result = result/denom_iden[proc_ID]; 
  }
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      if (include_symmetry_factors)
      {
        all_results[proc_ID][ihel + 1][icolor + 1] /= denom_iden[proc_ID]; 
      }
    }
  }


  // Normalize when when summing+averaging over helicity configurations
  if (user_ihel == -1 && include_helicity_averaging_factors)
  {
    result /= denom_hels[proc_ID]; 
    if (user_icol == -1)
    {
      all_results[proc_ID][0][0] /= denom_hels[proc_ID]; 
      for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
      {
        all_results[proc_ID][0][i + 1] /= denom_hels[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][0][user_icol + 1] /= denom_hels[proc_ID]; 
    }
  }

  // Normalize when summing+averaging over color configurations
  if (user_icol == -1 && include_color_averaging_factors)
  {
    result /= denom_colors[proc_ID]; 
    if (user_ihel == -1)
    {
      all_results[proc_ID][0][0] /= denom_colors[proc_ID]; 
      for (unsigned int i = 0; i < ncomb; i++ )
      {
        all_results[proc_ID][i + 1][0] /= denom_colors[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][user_ihel + 1][0] /= denom_colors[proc_ID]; 
    }
  }

  // Reinstate previous values of mME
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (saved_mME[i] < 0.0)
    {
      mME[i] = saved_mME[i]; 
    }
  }

  // Finally return it
  return result; 
}

//==========================================================================
// Private class member functions

//--------------------------------------------------------------------------
// Evaluate |M|^2 for each subprocess

void PY8MEs_R4_P5_heft_gg_hggg::calculate_wavefunctions(const int hel[])
{
  // Calculate wavefunctions for all processes
  // Calculate all wavefunctions
  vxxxxx(p[perm[0]], mME[0], hel[0], -1, w[0]); 
  vxxxxx(p[perm[1]], mME[1], hel[1], -1, w[1]); 
  sxxxxx(p[perm[2]], +1, w[2]); 
  vxxxxx(p[perm[3]], mME[3], hel[3], +1, w[3]); 
  vxxxxx(p[perm[4]], mME[4], hel[4], +1, w[4]); 
  vxxxxx(p[perm[5]], mME[5], hel[5], +1, w[5]); 
  VVV1P0_1(w[0], w[1], pars->GC_10, pars->ZERO, pars->ZERO, w[6]); 
  VVS3P0_1(w[3], w[2], pars->GC_13, pars->ZERO, pars->ZERO, w[7]); 
  VVV1P0_1(w[6], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[8]); 
  VVV1P0_1(w[6], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[9]); 
  VVV1P0_1(w[4], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[10]); 
  VVS3P0_1(w[4], w[2], pars->GC_13, pars->ZERO, pars->ZERO, w[11]); 
  VVV1P0_1(w[6], w[3], pars->GC_10, pars->ZERO, pars->ZERO, w[12]); 
  VVV1P0_1(w[3], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[13]); 
  VVS3P0_1(w[5], w[2], pars->GC_13, pars->ZERO, pars->ZERO, w[14]); 
  VVV1P0_1(w[3], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[15]); 
  VVS3P0_1(w[6], w[2], pars->GC_13, pars->ZERO, pars->ZERO, w[16]); 
  VVVS2P0_1(w[3], w[4], w[2], pars->GC_14, pars->ZERO, pars->ZERO, w[17]); 
  VVVS2P0_1(w[3], w[5], w[2], pars->GC_14, pars->ZERO, pars->ZERO, w[18]); 
  VVVS2P0_1(w[4], w[5], w[2], pars->GC_14, pars->ZERO, pars->ZERO, w[19]); 
  VVVV1P0_1(w[3], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[20]); 
  VVVV3P0_1(w[3], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[21]); 
  VVVV4P0_1(w[3], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[22]); 
  VVS3P0_1(w[0], w[2], pars->GC_13, pars->ZERO, pars->ZERO, w[23]); 
  VVV1P0_1(w[1], w[3], pars->GC_10, pars->ZERO, pars->ZERO, w[24]); 
  VVV1P0_1(w[23], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[25]); 
  VVV1P0_1(w[23], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[26]); 
  VVV1P0_1(w[1], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[27]); 
  VVV1P0_1(w[23], w[3], pars->GC_10, pars->ZERO, pars->ZERO, w[28]); 
  VVV1P0_1(w[1], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[29]); 
  VVV1P0_1(w[23], w[1], pars->GC_10, pars->ZERO, pars->ZERO, w[30]); 
  VVVV1P0_1(w[1], w[3], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[31]); 
  VVVV3P0_1(w[1], w[3], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[32]); 
  VVVV4P0_1(w[1], w[3], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[33]); 
  VVVV1P0_1(w[1], w[3], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[34]); 
  VVVV3P0_1(w[1], w[3], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[35]); 
  VVVV4P0_1(w[1], w[3], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[36]); 
  VVVV1P0_1(w[1], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[37]); 
  VVVV3P0_1(w[1], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[38]); 
  VVVV4P0_1(w[1], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[39]); 
  VVV1P0_1(w[0], w[3], pars->GC_10, pars->ZERO, pars->ZERO, w[40]); 
  VVS3P0_1(w[1], w[2], pars->GC_13, pars->ZERO, pars->ZERO, w[41]); 
  VVV1P0_1(w[40], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[42]); 
  VVV1P0_1(w[40], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[43]); 
  VVS3P0_1(w[40], w[2], pars->GC_13, pars->ZERO, pars->ZERO, w[44]); 
  VVV1P0_1(w[40], w[1], pars->GC_10, pars->ZERO, pars->ZERO, w[45]); 
  VVVS2P0_1(w[1], w[4], w[2], pars->GC_14, pars->ZERO, pars->ZERO, w[46]); 
  VVVS2P0_1(w[1], w[5], w[2], pars->GC_14, pars->ZERO, pars->ZERO, w[47]); 
  VVV1P0_1(w[0], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[48]); 
  VVV1P0_1(w[48], w[3], pars->GC_10, pars->ZERO, pars->ZERO, w[49]); 
  VVV1P0_1(w[48], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[50]); 
  VVS3P0_1(w[48], w[2], pars->GC_13, pars->ZERO, pars->ZERO, w[51]); 
  VVV1P0_1(w[48], w[1], pars->GC_10, pars->ZERO, pars->ZERO, w[52]); 
  VVVS2P0_1(w[1], w[3], w[2], pars->GC_14, pars->ZERO, pars->ZERO, w[53]); 
  VVV1P0_1(w[0], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[54]); 
  VVV1P0_1(w[54], w[3], pars->GC_10, pars->ZERO, pars->ZERO, w[55]); 
  VVV1P0_1(w[54], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[56]); 
  VVS3P0_1(w[54], w[2], pars->GC_13, pars->ZERO, pars->ZERO, w[57]); 
  VVV1P0_1(w[54], w[1], pars->GC_10, pars->ZERO, pars->ZERO, w[58]); 
  VVV1P0_1(w[0], w[41], pars->GC_10, pars->ZERO, pars->ZERO, w[59]); 
  VVV1P0_1(w[0], w[15], pars->GC_10, pars->ZERO, pars->ZERO, w[60]); 
  VVV1P0_1(w[0], w[13], pars->GC_10, pars->ZERO, pars->ZERO, w[61]); 
  VVV1P0_1(w[0], w[10], pars->GC_10, pars->ZERO, pars->ZERO, w[62]); 
  VVV1P0_1(w[0], w[24], pars->GC_10, pars->ZERO, pars->ZERO, w[63]); 
  VVV1P0_1(w[0], w[11], pars->GC_10, pars->ZERO, pars->ZERO, w[64]); 
  VVV1P0_1(w[0], w[14], pars->GC_10, pars->ZERO, pars->ZERO, w[65]); 
  VVV1P0_1(w[0], w[27], pars->GC_10, pars->ZERO, pars->ZERO, w[66]); 
  VVV1P0_1(w[0], w[7], pars->GC_10, pars->ZERO, pars->ZERO, w[67]); 
  VVV1P0_1(w[0], w[29], pars->GC_10, pars->ZERO, pars->ZERO, w[68]); 
  VVVS2P0_1(w[0], w[1], w[2], pars->GC_14, pars->ZERO, pars->ZERO, w[69]); 
  VVVV1P0_1(w[0], w[1], w[3], pars->GC_12, pars->ZERO, pars->ZERO, w[70]); 
  VVVV3P0_1(w[0], w[1], w[3], pars->GC_12, pars->ZERO, pars->ZERO, w[71]); 
  VVVV4P0_1(w[0], w[1], w[3], pars->GC_12, pars->ZERO, pars->ZERO, w[72]); 
  VVVV1P0_1(w[0], w[1], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[73]); 
  VVVV3P0_1(w[0], w[1], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[74]); 
  VVVV4P0_1(w[0], w[1], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[75]); 
  VVVV1P0_1(w[0], w[1], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[76]); 
  VVVV3P0_1(w[0], w[1], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[77]); 
  VVVV4P0_1(w[0], w[1], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[78]); 
  VVVS2P0_1(w[0], w[3], w[2], pars->GC_14, pars->ZERO, pars->ZERO, w[79]); 
  VVVS2P0_1(w[0], w[4], w[2], pars->GC_14, pars->ZERO, pars->ZERO, w[80]); 
  VVVS2P0_1(w[0], w[5], w[2], pars->GC_14, pars->ZERO, pars->ZERO, w[81]); 
  VVVV1P0_1(w[0], w[3], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[82]); 
  VVVV3P0_1(w[0], w[3], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[83]); 
  VVVV4P0_1(w[0], w[3], w[4], pars->GC_12, pars->ZERO, pars->ZERO, w[84]); 
  VVVV1P0_1(w[0], w[3], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[85]); 
  VVVV3P0_1(w[0], w[3], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[86]); 
  VVVV4P0_1(w[0], w[3], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[87]); 
  VVVV1P0_1(w[0], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[88]); 
  VVVV3P0_1(w[0], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[89]); 
  VVVV4P0_1(w[0], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[90]); 
  VVVVS1P0_1(w[0], w[1], w[3], w[2], pars->GC_15, pars->ZERO, pars->ZERO,
      w[91]);
  VVVVS2P0_1(w[0], w[1], w[3], w[2], pars->GC_15, pars->ZERO, pars->ZERO,
      w[92]);
  VVVVS3P0_1(w[0], w[1], w[3], w[2], pars->GC_15, pars->ZERO, pars->ZERO,
      w[93]);
  VVVVS1P0_1(w[0], w[1], w[4], w[2], pars->GC_15, pars->ZERO, pars->ZERO,
      w[94]);
  VVVVS2P0_1(w[0], w[1], w[4], w[2], pars->GC_15, pars->ZERO, pars->ZERO,
      w[95]);
  VVVVS3P0_1(w[0], w[1], w[4], w[2], pars->GC_15, pars->ZERO, pars->ZERO,
      w[96]);
  VVVVS1P0_1(w[0], w[1], w[5], w[2], pars->GC_15, pars->ZERO, pars->ZERO,
      w[97]);
  VVVVS2P0_1(w[0], w[1], w[5], w[2], pars->GC_15, pars->ZERO, pars->ZERO,
      w[98]);
  VVVVS3P0_1(w[0], w[1], w[5], w[2], pars->GC_15, pars->ZERO, pars->ZERO,
      w[99]);
  VVVVS1P0_1(w[0], w[3], w[4], w[2], pars->GC_15, pars->ZERO, pars->ZERO,
      w[100]);
  VVVVS2P0_1(w[0], w[3], w[4], w[2], pars->GC_15, pars->ZERO, pars->ZERO,
      w[101]);
  VVVVS3P0_1(w[0], w[3], w[4], w[2], pars->GC_15, pars->ZERO, pars->ZERO,
      w[102]);
  VVVVS1P0_1(w[0], w[3], w[5], w[2], pars->GC_15, pars->ZERO, pars->ZERO,
      w[103]);
  VVVVS2P0_1(w[0], w[3], w[5], w[2], pars->GC_15, pars->ZERO, pars->ZERO,
      w[104]);
  VVVVS3P0_1(w[0], w[3], w[5], w[2], pars->GC_15, pars->ZERO, pars->ZERO,
      w[105]);
  VVVVS1P0_1(w[0], w[4], w[5], w[2], pars->GC_15, pars->ZERO, pars->ZERO,
      w[106]);
  VVVVS2P0_1(w[0], w[4], w[5], w[2], pars->GC_15, pars->ZERO, pars->ZERO,
      w[107]);
  VVVVS3P0_1(w[0], w[4], w[5], w[2], pars->GC_15, pars->ZERO, pars->ZERO,
      w[108]);

  // Calculate all amplitudes
  // Amplitude(s) for diagram number 0
  VVVV1_0(w[6], w[7], w[4], w[5], pars->GC_12, amp[0]); 
  VVVV3_0(w[6], w[7], w[4], w[5], pars->GC_12, amp[1]); 
  VVVV4_0(w[6], w[7], w[4], w[5], pars->GC_12, amp[2]); 
  VVV1_0(w[7], w[5], w[8], pars->GC_10, amp[3]); 
  VVV1_0(w[7], w[4], w[9], pars->GC_10, amp[4]); 
  VVV1_0(w[6], w[7], w[10], pars->GC_10, amp[5]); 
  VVVV1_0(w[6], w[11], w[3], w[5], pars->GC_12, amp[6]); 
  VVVV3_0(w[6], w[11], w[3], w[5], pars->GC_12, amp[7]); 
  VVVV4_0(w[6], w[11], w[3], w[5], pars->GC_12, amp[8]); 
  VVV1_0(w[11], w[5], w[12], pars->GC_10, amp[9]); 
  VVV1_0(w[11], w[3], w[9], pars->GC_10, amp[10]); 
  VVV1_0(w[6], w[11], w[13], pars->GC_10, amp[11]); 
  VVVV1_0(w[6], w[14], w[3], w[4], pars->GC_12, amp[12]); 
  VVVV3_0(w[6], w[14], w[3], w[4], pars->GC_12, amp[13]); 
  VVVV4_0(w[6], w[14], w[3], w[4], pars->GC_12, amp[14]); 
  VVV1_0(w[14], w[4], w[12], pars->GC_10, amp[15]); 
  VVV1_0(w[14], w[3], w[8], pars->GC_10, amp[16]); 
  VVV1_0(w[6], w[14], w[15], pars->GC_10, amp[17]); 
  VVVS2_0(w[6], w[15], w[5], w[2], pars->GC_14, amp[18]); 
  VVV1_0(w[15], w[5], w[16], pars->GC_10, amp[19]); 
  VVS3_0(w[15], w[9], w[2], pars->GC_13, amp[20]); 
  VVVS2_0(w[6], w[13], w[4], w[2], pars->GC_14, amp[21]); 
  VVV1_0(w[13], w[4], w[16], pars->GC_10, amp[22]); 
  VVS3_0(w[13], w[8], w[2], pars->GC_13, amp[23]); 
  VVVS2_0(w[6], w[3], w[10], w[2], pars->GC_14, amp[24]); 
  VVV1_0(w[3], w[10], w[16], pars->GC_10, amp[25]); 
  VVS3_0(w[10], w[12], w[2], pars->GC_13, amp[26]); 
  VVV1_0(w[6], w[17], w[5], pars->GC_10, amp[27]); 
  VVV1_0(w[6], w[18], w[4], pars->GC_10, amp[28]); 
  VVV1_0(w[6], w[19], w[3], pars->GC_10, amp[29]); 
  VVS3_0(w[6], w[20], w[2], pars->GC_13, amp[30]); 
  VVS3_0(w[6], w[21], w[2], pars->GC_13, amp[31]); 
  VVS3_0(w[6], w[22], w[2], pars->GC_13, amp[32]); 
  VVVVS1_0(w[3], w[4], w[5], w[6], w[2], pars->GC_15, amp[33]); 
  VVVVS2_0(w[3], w[4], w[5], w[6], w[2], pars->GC_15, amp[34]); 
  VVVVS3_0(w[3], w[4], w[5], w[6], w[2], pars->GC_15, amp[35]); 
  VVVV1_0(w[23], w[24], w[4], w[5], pars->GC_12, amp[36]); 
  VVVV3_0(w[23], w[24], w[4], w[5], pars->GC_12, amp[37]); 
  VVVV4_0(w[23], w[24], w[4], w[5], pars->GC_12, amp[38]); 
  VVV1_0(w[24], w[5], w[25], pars->GC_10, amp[39]); 
  VVV1_0(w[24], w[4], w[26], pars->GC_10, amp[40]); 
  VVV1_0(w[23], w[24], w[10], pars->GC_10, amp[41]); 
  VVVV1_0(w[23], w[27], w[3], w[5], pars->GC_12, amp[42]); 
  VVVV3_0(w[23], w[27], w[3], w[5], pars->GC_12, amp[43]); 
  VVVV4_0(w[23], w[27], w[3], w[5], pars->GC_12, amp[44]); 
  VVV1_0(w[27], w[5], w[28], pars->GC_10, amp[45]); 
  VVV1_0(w[27], w[3], w[26], pars->GC_10, amp[46]); 
  VVV1_0(w[23], w[27], w[13], pars->GC_10, amp[47]); 
  VVVV1_0(w[23], w[29], w[3], w[4], pars->GC_12, amp[48]); 
  VVVV3_0(w[23], w[29], w[3], w[4], pars->GC_12, amp[49]); 
  VVVV4_0(w[23], w[29], w[3], w[4], pars->GC_12, amp[50]); 
  VVV1_0(w[29], w[4], w[28], pars->GC_10, amp[51]); 
  VVV1_0(w[29], w[3], w[25], pars->GC_10, amp[52]); 
  VVV1_0(w[23], w[29], w[15], pars->GC_10, amp[53]); 
  VVVV1_0(w[23], w[1], w[15], w[5], pars->GC_12, amp[54]); 
  VVVV3_0(w[23], w[1], w[15], w[5], pars->GC_12, amp[55]); 
  VVVV4_0(w[23], w[1], w[15], w[5], pars->GC_12, amp[56]); 
  VVV1_0(w[15], w[5], w[30], pars->GC_10, amp[57]); 
  VVV1_0(w[1], w[15], w[26], pars->GC_10, amp[58]); 
  VVVV1_0(w[23], w[1], w[13], w[4], pars->GC_12, amp[59]); 
  VVVV3_0(w[23], w[1], w[13], w[4], pars->GC_12, amp[60]); 
  VVVV4_0(w[23], w[1], w[13], w[4], pars->GC_12, amp[61]); 
  VVV1_0(w[13], w[4], w[30], pars->GC_10, amp[62]); 
  VVV1_0(w[1], w[13], w[25], pars->GC_10, amp[63]); 
  VVVV1_0(w[23], w[1], w[3], w[10], pars->GC_12, amp[64]); 
  VVVV3_0(w[23], w[1], w[3], w[10], pars->GC_12, amp[65]); 
  VVVV4_0(w[23], w[1], w[3], w[10], pars->GC_12, amp[66]); 
  VVV1_0(w[3], w[10], w[30], pars->GC_10, amp[67]); 
  VVV1_0(w[1], w[10], w[28], pars->GC_10, amp[68]); 
  VVV1_0(w[23], w[31], w[5], pars->GC_10, amp[69]); 
  VVV1_0(w[23], w[32], w[5], pars->GC_10, amp[70]); 
  VVV1_0(w[23], w[33], w[5], pars->GC_10, amp[71]); 
  VVV1_0(w[23], w[34], w[4], pars->GC_10, amp[72]); 
  VVV1_0(w[23], w[35], w[4], pars->GC_10, amp[73]); 
  VVV1_0(w[23], w[36], w[4], pars->GC_10, amp[74]); 
  VVV1_0(w[23], w[37], w[3], pars->GC_10, amp[75]); 
  VVV1_0(w[23], w[38], w[3], pars->GC_10, amp[76]); 
  VVV1_0(w[23], w[39], w[3], pars->GC_10, amp[77]); 
  VVV1_0(w[23], w[1], w[20], pars->GC_10, amp[78]); 
  VVV1_0(w[23], w[1], w[21], pars->GC_10, amp[79]); 
  VVV1_0(w[23], w[1], w[22], pars->GC_10, amp[80]); 
  VVVV1_0(w[40], w[41], w[4], w[5], pars->GC_12, amp[81]); 
  VVVV3_0(w[40], w[41], w[4], w[5], pars->GC_12, amp[82]); 
  VVVV4_0(w[40], w[41], w[4], w[5], pars->GC_12, amp[83]); 
  VVV1_0(w[41], w[5], w[42], pars->GC_10, amp[84]); 
  VVV1_0(w[41], w[4], w[43], pars->GC_10, amp[85]); 
  VVV1_0(w[40], w[41], w[10], pars->GC_10, amp[86]); 
  VVVS2_0(w[40], w[27], w[5], w[2], pars->GC_14, amp[87]); 
  VVV1_0(w[27], w[5], w[44], pars->GC_10, amp[88]); 
  VVS3_0(w[27], w[43], w[2], pars->GC_13, amp[89]); 
  VVV1_0(w[40], w[27], w[14], pars->GC_10, amp[90]); 
  VVVS2_0(w[40], w[29], w[4], w[2], pars->GC_14, amp[91]); 
  VVV1_0(w[29], w[4], w[44], pars->GC_10, amp[92]); 
  VVS3_0(w[29], w[42], w[2], pars->GC_13, amp[93]); 
  VVV1_0(w[40], w[29], w[11], pars->GC_10, amp[94]); 
  VVVV1_0(w[40], w[1], w[11], w[5], pars->GC_12, amp[95]); 
  VVVV3_0(w[40], w[1], w[11], w[5], pars->GC_12, amp[96]); 
  VVVV4_0(w[40], w[1], w[11], w[5], pars->GC_12, amp[97]); 
  VVV1_0(w[11], w[5], w[45], pars->GC_10, amp[98]); 
  VVV1_0(w[1], w[11], w[43], pars->GC_10, amp[99]); 
  VVVV1_0(w[40], w[1], w[14], w[4], pars->GC_12, amp[100]); 
  VVVV3_0(w[40], w[1], w[14], w[4], pars->GC_12, amp[101]); 
  VVVV4_0(w[40], w[1], w[14], w[4], pars->GC_12, amp[102]); 
  VVV1_0(w[14], w[4], w[45], pars->GC_10, amp[103]); 
  VVV1_0(w[1], w[14], w[42], pars->GC_10, amp[104]); 
  VVVS2_0(w[40], w[1], w[10], w[2], pars->GC_14, amp[105]); 
  VVS3_0(w[10], w[45], w[2], pars->GC_13, amp[106]); 
  VVV1_0(w[1], w[10], w[44], pars->GC_10, amp[107]); 
  VVV1_0(w[40], w[46], w[5], pars->GC_10, amp[108]); 
  VVV1_0(w[40], w[47], w[4], pars->GC_10, amp[109]); 
  VVS3_0(w[40], w[37], w[2], pars->GC_13, amp[110]); 
  VVS3_0(w[40], w[38], w[2], pars->GC_13, amp[111]); 
  VVS3_0(w[40], w[39], w[2], pars->GC_13, amp[112]); 
  VVV1_0(w[40], w[1], w[19], pars->GC_10, amp[113]); 
  VVVVS1_0(w[1], w[4], w[5], w[40], w[2], pars->GC_15, amp[114]); 
  VVVVS2_0(w[1], w[4], w[5], w[40], w[2], pars->GC_15, amp[115]); 
  VVVVS3_0(w[1], w[4], w[5], w[40], w[2], pars->GC_15, amp[116]); 
  VVVV1_0(w[48], w[41], w[3], w[5], pars->GC_12, amp[117]); 
  VVVV3_0(w[48], w[41], w[3], w[5], pars->GC_12, amp[118]); 
  VVVV4_0(w[48], w[41], w[3], w[5], pars->GC_12, amp[119]); 
  VVV1_0(w[41], w[5], w[49], pars->GC_10, amp[120]); 
  VVV1_0(w[41], w[3], w[50], pars->GC_10, amp[121]); 
  VVV1_0(w[48], w[41], w[13], pars->GC_10, amp[122]); 
  VVVS2_0(w[48], w[24], w[5], w[2], pars->GC_14, amp[123]); 
  VVV1_0(w[24], w[5], w[51], pars->GC_10, amp[124]); 
  VVS3_0(w[24], w[50], w[2], pars->GC_13, amp[125]); 
  VVV1_0(w[48], w[24], w[14], pars->GC_10, amp[126]); 
  VVVS2_0(w[48], w[29], w[3], w[2], pars->GC_14, amp[127]); 
  VVV1_0(w[29], w[3], w[51], pars->GC_10, amp[128]); 
  VVS3_0(w[29], w[49], w[2], pars->GC_13, amp[129]); 
  VVV1_0(w[48], w[29], w[7], pars->GC_10, amp[130]); 
  VVVV1_0(w[48], w[1], w[7], w[5], pars->GC_12, amp[131]); 
  VVVV3_0(w[48], w[1], w[7], w[5], pars->GC_12, amp[132]); 
  VVVV4_0(w[48], w[1], w[7], w[5], pars->GC_12, amp[133]); 
  VVV1_0(w[7], w[5], w[52], pars->GC_10, amp[134]); 
  VVV1_0(w[1], w[7], w[50], pars->GC_10, amp[135]); 
  VVVV1_0(w[48], w[1], w[14], w[3], pars->GC_12, amp[136]); 
  VVVV3_0(w[48], w[1], w[14], w[3], pars->GC_12, amp[137]); 
  VVVV4_0(w[48], w[1], w[14], w[3], pars->GC_12, amp[138]); 
  VVV1_0(w[14], w[3], w[52], pars->GC_10, amp[139]); 
  VVV1_0(w[1], w[14], w[49], pars->GC_10, amp[140]); 
  VVVS2_0(w[48], w[1], w[13], w[2], pars->GC_14, amp[141]); 
  VVS3_0(w[13], w[52], w[2], pars->GC_13, amp[142]); 
  VVV1_0(w[1], w[13], w[51], pars->GC_10, amp[143]); 
  VVV1_0(w[48], w[53], w[5], pars->GC_10, amp[144]); 
  VVV1_0(w[48], w[47], w[3], pars->GC_10, amp[145]); 
  VVS3_0(w[48], w[34], w[2], pars->GC_13, amp[146]); 
  VVS3_0(w[48], w[35], w[2], pars->GC_13, amp[147]); 
  VVS3_0(w[48], w[36], w[2], pars->GC_13, amp[148]); 
  VVV1_0(w[48], w[1], w[18], pars->GC_10, amp[149]); 
  VVVVS1_0(w[1], w[3], w[5], w[48], w[2], pars->GC_15, amp[150]); 
  VVVVS2_0(w[1], w[3], w[5], w[48], w[2], pars->GC_15, amp[151]); 
  VVVVS3_0(w[1], w[3], w[5], w[48], w[2], pars->GC_15, amp[152]); 
  VVVV1_0(w[54], w[41], w[3], w[4], pars->GC_12, amp[153]); 
  VVVV3_0(w[54], w[41], w[3], w[4], pars->GC_12, amp[154]); 
  VVVV4_0(w[54], w[41], w[3], w[4], pars->GC_12, amp[155]); 
  VVV1_0(w[41], w[4], w[55], pars->GC_10, amp[156]); 
  VVV1_0(w[41], w[3], w[56], pars->GC_10, amp[157]); 
  VVV1_0(w[54], w[41], w[15], pars->GC_10, amp[158]); 
  VVVS2_0(w[54], w[24], w[4], w[2], pars->GC_14, amp[159]); 
  VVV1_0(w[24], w[4], w[57], pars->GC_10, amp[160]); 
  VVS3_0(w[24], w[56], w[2], pars->GC_13, amp[161]); 
  VVV1_0(w[54], w[24], w[11], pars->GC_10, amp[162]); 
  VVVS2_0(w[54], w[27], w[3], w[2], pars->GC_14, amp[163]); 
  VVV1_0(w[27], w[3], w[57], pars->GC_10, amp[164]); 
  VVS3_0(w[27], w[55], w[2], pars->GC_13, amp[165]); 
  VVV1_0(w[54], w[27], w[7], pars->GC_10, amp[166]); 
  VVVV1_0(w[54], w[1], w[7], w[4], pars->GC_12, amp[167]); 
  VVVV3_0(w[54], w[1], w[7], w[4], pars->GC_12, amp[168]); 
  VVVV4_0(w[54], w[1], w[7], w[4], pars->GC_12, amp[169]); 
  VVV1_0(w[7], w[4], w[58], pars->GC_10, amp[170]); 
  VVV1_0(w[1], w[7], w[56], pars->GC_10, amp[171]); 
  VVVV1_0(w[54], w[1], w[11], w[3], pars->GC_12, amp[172]); 
  VVVV3_0(w[54], w[1], w[11], w[3], pars->GC_12, amp[173]); 
  VVVV4_0(w[54], w[1], w[11], w[3], pars->GC_12, amp[174]); 
  VVV1_0(w[11], w[3], w[58], pars->GC_10, amp[175]); 
  VVV1_0(w[1], w[11], w[55], pars->GC_10, amp[176]); 
  VVVS2_0(w[54], w[1], w[15], w[2], pars->GC_14, amp[177]); 
  VVS3_0(w[15], w[58], w[2], pars->GC_13, amp[178]); 
  VVV1_0(w[1], w[15], w[57], pars->GC_10, amp[179]); 
  VVV1_0(w[54], w[53], w[4], pars->GC_10, amp[180]); 
  VVV1_0(w[54], w[46], w[3], pars->GC_10, amp[181]); 
  VVS3_0(w[54], w[31], w[2], pars->GC_13, amp[182]); 
  VVS3_0(w[54], w[32], w[2], pars->GC_13, amp[183]); 
  VVS3_0(w[54], w[33], w[2], pars->GC_13, amp[184]); 
  VVV1_0(w[54], w[1], w[17], pars->GC_10, amp[185]); 
  VVVVS1_0(w[1], w[3], w[4], w[54], w[2], pars->GC_15, amp[186]); 
  VVVVS2_0(w[1], w[3], w[4], w[54], w[2], pars->GC_15, amp[187]); 
  VVVVS3_0(w[1], w[3], w[4], w[54], w[2], pars->GC_15, amp[188]); 
  VVVV1_0(w[0], w[41], w[15], w[5], pars->GC_12, amp[189]); 
  VVVV3_0(w[0], w[41], w[15], w[5], pars->GC_12, amp[190]); 
  VVVV4_0(w[0], w[41], w[15], w[5], pars->GC_12, amp[191]); 
  VVV1_0(w[15], w[5], w[59], pars->GC_10, amp[192]); 
  VVV1_0(w[41], w[5], w[60], pars->GC_10, amp[193]); 
  VVVV1_0(w[0], w[41], w[13], w[4], pars->GC_12, amp[194]); 
  VVVV3_0(w[0], w[41], w[13], w[4], pars->GC_12, amp[195]); 
  VVVV4_0(w[0], w[41], w[13], w[4], pars->GC_12, amp[196]); 
  VVV1_0(w[13], w[4], w[59], pars->GC_10, amp[197]); 
  VVV1_0(w[41], w[4], w[61], pars->GC_10, amp[198]); 
  VVVV1_0(w[0], w[41], w[3], w[10], pars->GC_12, amp[199]); 
  VVVV3_0(w[0], w[41], w[3], w[10], pars->GC_12, amp[200]); 
  VVVV4_0(w[0], w[41], w[3], w[10], pars->GC_12, amp[201]); 
  VVV1_0(w[3], w[10], w[59], pars->GC_10, amp[202]); 
  VVV1_0(w[41], w[3], w[62], pars->GC_10, amp[203]); 
  VVV1_0(w[0], w[41], w[20], pars->GC_10, amp[204]); 
  VVV1_0(w[0], w[41], w[21], pars->GC_10, amp[205]); 
  VVV1_0(w[0], w[41], w[22], pars->GC_10, amp[206]); 
  VVVV1_0(w[0], w[24], w[11], w[5], pars->GC_12, amp[207]); 
  VVVV3_0(w[0], w[24], w[11], w[5], pars->GC_12, amp[208]); 
  VVVV4_0(w[0], w[24], w[11], w[5], pars->GC_12, amp[209]); 
  VVV1_0(w[11], w[5], w[63], pars->GC_10, amp[210]); 
  VVV1_0(w[24], w[5], w[64], pars->GC_10, amp[211]); 
  VVVV1_0(w[0], w[24], w[14], w[4], pars->GC_12, amp[212]); 
  VVVV3_0(w[0], w[24], w[14], w[4], pars->GC_12, amp[213]); 
  VVVV4_0(w[0], w[24], w[14], w[4], pars->GC_12, amp[214]); 
  VVV1_0(w[14], w[4], w[63], pars->GC_10, amp[215]); 
  VVV1_0(w[24], w[4], w[65], pars->GC_10, amp[216]); 
  VVVS2_0(w[0], w[24], w[10], w[2], pars->GC_14, amp[217]); 
  VVS3_0(w[10], w[63], w[2], pars->GC_13, amp[218]); 
  VVS3_0(w[24], w[62], w[2], pars->GC_13, amp[219]); 
  VVV1_0(w[0], w[24], w[19], pars->GC_10, amp[220]); 
  VVVV1_0(w[0], w[27], w[7], w[5], pars->GC_12, amp[221]); 
  VVVV3_0(w[0], w[27], w[7], w[5], pars->GC_12, amp[222]); 
  VVVV4_0(w[0], w[27], w[7], w[5], pars->GC_12, amp[223]); 
  VVV1_0(w[7], w[5], w[66], pars->GC_10, amp[224]); 
  VVV1_0(w[27], w[5], w[67], pars->GC_10, amp[225]); 
  VVVV1_0(w[0], w[27], w[14], w[3], pars->GC_12, amp[226]); 
  VVVV3_0(w[0], w[27], w[14], w[3], pars->GC_12, amp[227]); 
  VVVV4_0(w[0], w[27], w[14], w[3], pars->GC_12, amp[228]); 
  VVV1_0(w[14], w[3], w[66], pars->GC_10, amp[229]); 
  VVV1_0(w[27], w[3], w[65], pars->GC_10, amp[230]); 
  VVVS2_0(w[0], w[27], w[13], w[2], pars->GC_14, amp[231]); 
  VVS3_0(w[13], w[66], w[2], pars->GC_13, amp[232]); 
  VVS3_0(w[27], w[61], w[2], pars->GC_13, amp[233]); 
  VVV1_0(w[0], w[27], w[18], pars->GC_10, amp[234]); 
  VVVV1_0(w[0], w[29], w[7], w[4], pars->GC_12, amp[235]); 
  VVVV3_0(w[0], w[29], w[7], w[4], pars->GC_12, amp[236]); 
  VVVV4_0(w[0], w[29], w[7], w[4], pars->GC_12, amp[237]); 
  VVV1_0(w[7], w[4], w[68], pars->GC_10, amp[238]); 
  VVV1_0(w[29], w[4], w[67], pars->GC_10, amp[239]); 
  VVVV1_0(w[0], w[29], w[11], w[3], pars->GC_12, amp[240]); 
  VVVV3_0(w[0], w[29], w[11], w[3], pars->GC_12, amp[241]); 
  VVVV4_0(w[0], w[29], w[11], w[3], pars->GC_12, amp[242]); 
  VVV1_0(w[11], w[3], w[68], pars->GC_10, amp[243]); 
  VVV1_0(w[29], w[3], w[64], pars->GC_10, amp[244]); 
  VVVS2_0(w[0], w[29], w[15], w[2], pars->GC_14, amp[245]); 
  VVS3_0(w[15], w[68], w[2], pars->GC_13, amp[246]); 
  VVS3_0(w[29], w[60], w[2], pars->GC_13, amp[247]); 
  VVV1_0(w[0], w[29], w[17], pars->GC_10, amp[248]); 
  VVVV1_0(w[0], w[1], w[7], w[10], pars->GC_12, amp[249]); 
  VVVV3_0(w[0], w[1], w[7], w[10], pars->GC_12, amp[250]); 
  VVVV4_0(w[0], w[1], w[7], w[10], pars->GC_12, amp[251]); 
  VVV1_0(w[1], w[10], w[67], pars->GC_10, amp[252]); 
  VVV1_0(w[1], w[7], w[62], pars->GC_10, amp[253]); 
  VVVV1_0(w[0], w[1], w[11], w[13], pars->GC_12, amp[254]); 
  VVVV3_0(w[0], w[1], w[11], w[13], pars->GC_12, amp[255]); 
  VVVV4_0(w[0], w[1], w[11], w[13], pars->GC_12, amp[256]); 
  VVV1_0(w[1], w[13], w[64], pars->GC_10, amp[257]); 
  VVV1_0(w[1], w[11], w[61], pars->GC_10, amp[258]); 
  VVVV1_0(w[0], w[1], w[14], w[15], pars->GC_12, amp[259]); 
  VVVV3_0(w[0], w[1], w[14], w[15], pars->GC_12, amp[260]); 
  VVVV4_0(w[0], w[1], w[14], w[15], pars->GC_12, amp[261]); 
  VVV1_0(w[1], w[15], w[65], pars->GC_10, amp[262]); 
  VVV1_0(w[1], w[14], w[60], pars->GC_10, amp[263]); 
  VVV1_0(w[69], w[15], w[5], pars->GC_10, amp[264]); 
  VVV1_0(w[69], w[13], w[4], pars->GC_10, amp[265]); 
  VVV1_0(w[69], w[3], w[10], pars->GC_10, amp[266]); 
  VVVV1_0(w[3], w[4], w[5], w[69], pars->GC_12, amp[267]); 
  VVVV3_0(w[3], w[4], w[5], w[69], pars->GC_12, amp[268]); 
  VVVV4_0(w[3], w[4], w[5], w[69], pars->GC_12, amp[269]); 
  VVV1_0(w[70], w[11], w[5], pars->GC_10, amp[270]); 
  VVV1_0(w[71], w[11], w[5], pars->GC_10, amp[271]); 
  VVV1_0(w[72], w[11], w[5], pars->GC_10, amp[272]); 
  VVV1_0(w[70], w[14], w[4], pars->GC_10, amp[273]); 
  VVV1_0(w[71], w[14], w[4], pars->GC_10, amp[274]); 
  VVV1_0(w[72], w[14], w[4], pars->GC_10, amp[275]); 
  VVS3_0(w[70], w[10], w[2], pars->GC_13, amp[276]); 
  VVS3_0(w[71], w[10], w[2], pars->GC_13, amp[277]); 
  VVS3_0(w[72], w[10], w[2], pars->GC_13, amp[278]); 
  VVVS2_0(w[4], w[5], w[70], w[2], pars->GC_14, amp[279]); 
  VVVS2_0(w[4], w[5], w[71], w[2], pars->GC_14, amp[280]); 
  VVVS2_0(w[4], w[5], w[72], w[2], pars->GC_14, amp[281]); 
  VVV1_0(w[73], w[7], w[5], pars->GC_10, amp[282]); 
  VVV1_0(w[74], w[7], w[5], pars->GC_10, amp[283]); 
  VVV1_0(w[75], w[7], w[5], pars->GC_10, amp[284]); 
  VVV1_0(w[73], w[14], w[3], pars->GC_10, amp[285]); 
  VVV1_0(w[74], w[14], w[3], pars->GC_10, amp[286]); 
  VVV1_0(w[75], w[14], w[3], pars->GC_10, amp[287]); 
  VVS3_0(w[73], w[13], w[2], pars->GC_13, amp[288]); 
  VVS3_0(w[74], w[13], w[2], pars->GC_13, amp[289]); 
  VVS3_0(w[75], w[13], w[2], pars->GC_13, amp[290]); 
  VVVS2_0(w[3], w[5], w[73], w[2], pars->GC_14, amp[291]); 
  VVVS2_0(w[3], w[5], w[74], w[2], pars->GC_14, amp[292]); 
  VVVS2_0(w[3], w[5], w[75], w[2], pars->GC_14, amp[293]); 
  VVV1_0(w[76], w[7], w[4], pars->GC_10, amp[294]); 
  VVV1_0(w[77], w[7], w[4], pars->GC_10, amp[295]); 
  VVV1_0(w[78], w[7], w[4], pars->GC_10, amp[296]); 
  VVV1_0(w[76], w[11], w[3], pars->GC_10, amp[297]); 
  VVV1_0(w[77], w[11], w[3], pars->GC_10, amp[298]); 
  VVV1_0(w[78], w[11], w[3], pars->GC_10, amp[299]); 
  VVS3_0(w[76], w[15], w[2], pars->GC_13, amp[300]); 
  VVS3_0(w[77], w[15], w[2], pars->GC_13, amp[301]); 
  VVS3_0(w[78], w[15], w[2], pars->GC_13, amp[302]); 
  VVVS2_0(w[3], w[4], w[76], w[2], pars->GC_14, amp[303]); 
  VVVS2_0(w[3], w[4], w[77], w[2], pars->GC_14, amp[304]); 
  VVVS2_0(w[3], w[4], w[78], w[2], pars->GC_14, amp[305]); 
  VVV1_0(w[79], w[27], w[5], pars->GC_10, amp[306]); 
  VVV1_0(w[79], w[29], w[4], pars->GC_10, amp[307]); 
  VVV1_0(w[79], w[1], w[10], pars->GC_10, amp[308]); 
  VVVV1_0(w[1], w[4], w[5], w[79], pars->GC_12, amp[309]); 
  VVVV3_0(w[1], w[4], w[5], w[79], pars->GC_12, amp[310]); 
  VVVV4_0(w[1], w[4], w[5], w[79], pars->GC_12, amp[311]); 
  VVV1_0(w[80], w[24], w[5], pars->GC_10, amp[312]); 
  VVV1_0(w[80], w[29], w[3], pars->GC_10, amp[313]); 
  VVV1_0(w[80], w[1], w[13], pars->GC_10, amp[314]); 
  VVVV1_0(w[1], w[3], w[5], w[80], pars->GC_12, amp[315]); 
  VVVV3_0(w[1], w[3], w[5], w[80], pars->GC_12, amp[316]); 
  VVVV4_0(w[1], w[3], w[5], w[80], pars->GC_12, amp[317]); 
  VVV1_0(w[81], w[24], w[4], pars->GC_10, amp[318]); 
  VVV1_0(w[81], w[27], w[3], pars->GC_10, amp[319]); 
  VVV1_0(w[81], w[1], w[15], pars->GC_10, amp[320]); 
  VVVV1_0(w[1], w[3], w[4], w[81], pars->GC_12, amp[321]); 
  VVVV3_0(w[1], w[3], w[4], w[81], pars->GC_12, amp[322]); 
  VVVV4_0(w[1], w[3], w[4], w[81], pars->GC_12, amp[323]); 
  VVV1_0(w[82], w[41], w[5], pars->GC_10, amp[324]); 
  VVV1_0(w[83], w[41], w[5], pars->GC_10, amp[325]); 
  VVV1_0(w[84], w[41], w[5], pars->GC_10, amp[326]); 
  VVS3_0(w[82], w[29], w[2], pars->GC_13, amp[327]); 
  VVS3_0(w[83], w[29], w[2], pars->GC_13, amp[328]); 
  VVS3_0(w[84], w[29], w[2], pars->GC_13, amp[329]); 
  VVV1_0(w[82], w[1], w[14], pars->GC_10, amp[330]); 
  VVV1_0(w[83], w[1], w[14], pars->GC_10, amp[331]); 
  VVV1_0(w[84], w[1], w[14], pars->GC_10, amp[332]); 
  VVVS2_0(w[1], w[5], w[82], w[2], pars->GC_14, amp[333]); 
  VVVS2_0(w[1], w[5], w[83], w[2], pars->GC_14, amp[334]); 
  VVVS2_0(w[1], w[5], w[84], w[2], pars->GC_14, amp[335]); 
  VVV1_0(w[85], w[41], w[4], pars->GC_10, amp[336]); 
  VVV1_0(w[86], w[41], w[4], pars->GC_10, amp[337]); 
  VVV1_0(w[87], w[41], w[4], pars->GC_10, amp[338]); 
  VVS3_0(w[85], w[27], w[2], pars->GC_13, amp[339]); 
  VVS3_0(w[86], w[27], w[2], pars->GC_13, amp[340]); 
  VVS3_0(w[87], w[27], w[2], pars->GC_13, amp[341]); 
  VVV1_0(w[85], w[1], w[11], pars->GC_10, amp[342]); 
  VVV1_0(w[86], w[1], w[11], pars->GC_10, amp[343]); 
  VVV1_0(w[87], w[1], w[11], pars->GC_10, amp[344]); 
  VVVS2_0(w[1], w[4], w[85], w[2], pars->GC_14, amp[345]); 
  VVVS2_0(w[1], w[4], w[86], w[2], pars->GC_14, amp[346]); 
  VVVS2_0(w[1], w[4], w[87], w[2], pars->GC_14, amp[347]); 
  VVV1_0(w[88], w[41], w[3], pars->GC_10, amp[348]); 
  VVV1_0(w[89], w[41], w[3], pars->GC_10, amp[349]); 
  VVV1_0(w[90], w[41], w[3], pars->GC_10, amp[350]); 
  VVS3_0(w[88], w[24], w[2], pars->GC_13, amp[351]); 
  VVS3_0(w[89], w[24], w[2], pars->GC_13, amp[352]); 
  VVS3_0(w[90], w[24], w[2], pars->GC_13, amp[353]); 
  VVV1_0(w[88], w[1], w[7], pars->GC_10, amp[354]); 
  VVV1_0(w[89], w[1], w[7], pars->GC_10, amp[355]); 
  VVV1_0(w[90], w[1], w[7], pars->GC_10, amp[356]); 
  VVVS2_0(w[1], w[3], w[88], w[2], pars->GC_14, amp[357]); 
  VVVS2_0(w[1], w[3], w[89], w[2], pars->GC_14, amp[358]); 
  VVVS2_0(w[1], w[3], w[90], w[2], pars->GC_14, amp[359]); 
  VVV1_0(w[0], w[53], w[10], pars->GC_10, amp[360]); 
  VVV1_0(w[0], w[46], w[13], pars->GC_10, amp[361]); 
  VVV1_0(w[0], w[47], w[15], pars->GC_10, amp[362]); 
  VVV1_0(w[0], w[31], w[14], pars->GC_10, amp[363]); 
  VVV1_0(w[0], w[32], w[14], pars->GC_10, amp[364]); 
  VVV1_0(w[0], w[33], w[14], pars->GC_10, amp[365]); 
  VVV1_0(w[0], w[34], w[11], pars->GC_10, amp[366]); 
  VVV1_0(w[0], w[35], w[11], pars->GC_10, amp[367]); 
  VVV1_0(w[0], w[36], w[11], pars->GC_10, amp[368]); 
  VVV1_0(w[0], w[37], w[7], pars->GC_10, amp[369]); 
  VVV1_0(w[0], w[38], w[7], pars->GC_10, amp[370]); 
  VVV1_0(w[0], w[39], w[7], pars->GC_10, amp[371]); 
  VVV1_0(w[4], w[5], w[91], pars->GC_10, amp[372]); 
  VVV1_0(w[4], w[5], w[92], pars->GC_10, amp[373]); 
  VVV1_0(w[4], w[5], w[93], pars->GC_10, amp[374]); 
  VVV1_0(w[3], w[5], w[94], pars->GC_10, amp[375]); 
  VVV1_0(w[3], w[5], w[95], pars->GC_10, amp[376]); 
  VVV1_0(w[3], w[5], w[96], pars->GC_10, amp[377]); 
  VVV1_0(w[3], w[4], w[97], pars->GC_10, amp[378]); 
  VVV1_0(w[3], w[4], w[98], pars->GC_10, amp[379]); 
  VVV1_0(w[3], w[4], w[99], pars->GC_10, amp[380]); 
  VVV1_0(w[1], w[5], w[100], pars->GC_10, amp[381]); 
  VVV1_0(w[1], w[5], w[101], pars->GC_10, amp[382]); 
  VVV1_0(w[1], w[5], w[102], pars->GC_10, amp[383]); 
  VVV1_0(w[1], w[4], w[103], pars->GC_10, amp[384]); 
  VVV1_0(w[1], w[4], w[104], pars->GC_10, amp[385]); 
  VVV1_0(w[1], w[4], w[105], pars->GC_10, amp[386]); 
  VVV1_0(w[1], w[3], w[106], pars->GC_10, amp[387]); 
  VVV1_0(w[1], w[3], w[107], pars->GC_10, amp[388]); 
  VVV1_0(w[1], w[3], w[108], pars->GC_10, amp[389]); 


}
double PY8MEs_R4_P5_heft_gg_hggg::matrix_4_gg_hggg() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 390;
  const int ncolor = 24; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {108, 108, 108, 108, 108, 108, 108, 108,
      108, 108, 108, 108, 108, 108, 108, 108, 108, 108, 108, 108, 108, 108,
      108, 108};
  static const double cf[ncolor][ncolor] = {{455, -58, -58, 14, 14, 68, -58,
      -4, 14, -58, 5, -4, 14, 5, 68, -4, 14, 68, -58, -4, -4, 68, 68, -40},
      {-58, 455, 14, 68, -58, 14, -4, -58, 5, -4, 14, -58, -58, -4, -4, 68, 68,
      -40, 14, 5, 68, -4, 14, 68}, {-58, 14, 455, -58, 68, 14, 14, 5, 68, -4,
      14, 68, -58, -4, 14, -58, 5, -4, -4, -58, 68, -40, -4, 68}, {14, 68, -58,
      455, 14, -58, -58, -4, -4, 68, 68, -40, -4, -58, 5, -4, 14, -58, 5, 14,
      14, 68, 68, -4}, {14, -58, 68, 14, 455, -58, 5, 14, 14, 68, 68, -4, -4,
      -58, 68, -40, -4, 68, -58, -4, 14, -58, 5, -4}, {68, 14, 14, -58, -58,
      455, -4, -58, 68, -40, -4, 68, 5, 14, 14, 68, 68, -4, -4, -58, 5, -4, 14,
      -58}, {-58, -4, 14, -58, 5, -4, 455, -58, -58, 14, 14, 68, 68, -4, 14, 5,
      68, 14, -4, 68, -58, -4, -40, 68}, {-4, -58, 5, -4, 14, -58, -58, 455,
      14, 68, -58, 14, -4, 68, -58, -4, -40, 68, 68, -4, 14, 5, 68, 14}, {14,
      5, 68, -4, 14, 68, -58, 14, 455, -58, 68, 14, 14, -58, -58, -4, -4, 5,
      68, -40, -4, -58, 68, -4}, {-58, -4, -4, 68, 68, -40, 14, 68, -58, 455,
      14, -58, 5, -4, -4, -58, -58, 14, 14, 68, 5, 14, -4, 68}, {5, 14, 14, 68,
      68, -4, 14, -58, 68, 14, 455, -58, 68, -40, -4, -58, 68, -4, 14, -58,
      -58, -4, -4, 5}, {-4, -58, 68, -40, -4, 68, 68, 14, 14, -58, -58, 455,
      14, 68, 5, 14, -4, 68, 5, -4, -4, -58, -58, 14}, {14, -58, -58, -4, -4,
      5, 68, -4, 14, 5, 68, 14, 455, -58, -58, 14, 14, 68, 68, -4, -40, 68,
      -58, -4}, {5, -4, -4, -58, -58, 14, -4, 68, -58, -4, -40, 68, -58, 455,
      14, 68, -58, 14, -4, 68, 68, 14, 14, 5}, {68, -4, 14, 5, 68, 14, 14, -58,
      -58, -4, -4, 5, -58, 14, 455, -58, 68, 14, -40, 68, 68, -4, -4, -58},
      {-4, 68, -58, -4, -40, 68, 5, -4, -4, -58, -58, 14, 14, 68, -58, 455, 14,
      -58, 68, 14, -4, 68, 5, 14}, {14, 68, 5, 14, -4, 68, 68, -40, -4, -58,
      68, -4, 14, -58, 68, 14, 455, -58, -58, 14, -4, 5, -58, -4}, {68, -40,
      -4, -58, 68, -4, 14, 68, 5, 14, -4, 68, 68, 14, 14, -58, -58, 455, -4, 5,
      -58, 14, -4, -58}, {-58, 14, -4, 5, -58, -4, -4, 68, 68, 14, 14, 5, 68,
      -4, -40, 68, -58, -4, 455, -58, -58, 14, 14, 68}, {-4, 5, -58, 14, -4,
      -58, 68, -4, -40, 68, -58, -4, -4, 68, 68, 14, 14, 5, -58, 455, 14, 68,
      -58, 14}, {-4, 68, 68, 14, 14, 5, -58, 14, -4, 5, -58, -4, -40, 68, 68,
      -4, -4, -58, -58, 14, 455, -58, 68, 14}, {68, -4, -40, 68, -58, -4, -4,
      5, -58, 14, -4, -58, 68, 14, -4, 68, 5, 14, 14, 68, -58, 455, 14, -58},
      {68, 14, -4, 68, 5, 14, -40, 68, 68, -4, -4, -58, -58, 14, -4, 5, -58,
      -4, 14, -58, 68, 14, 455, -58}, {-40, 68, 68, -4, -4, -58, 68, 14, -4,
      68, 5, 14, -4, 5, -58, 14, -4, -58, 68, 14, 14, -58, -58, 455}};

  // Calculate color flows
  jamp[0] = +2. * (+Complex<double> (0, 1) * amp[0] - Complex<double> (0, 1) *
      amp[2] - Complex<double> (0, 1) * amp[4] + Complex<double> (0, 1) *
      amp[5] + Complex<double> (0, 1) * amp[7] + Complex<double> (0, 1) *
      amp[8] + Complex<double> (0, 1) * amp[9] + Complex<double> (0, 1) *
      amp[10] - Complex<double> (0, 1) * amp[12] - Complex<double> (0, 1) *
      amp[13] - Complex<double> (0, 1) * amp[15] - Complex<double> (0, 1) *
      amp[17] + Complex<double> (0, 1) * amp[18] + Complex<double> (0, 1) *
      amp[19] - Complex<double> (0, 1) * amp[20] + Complex<double> (0, 1) *
      amp[24] + Complex<double> (0, 1) * amp[25] + Complex<double> (0, 1) *
      amp[26] + Complex<double> (0, 1) * amp[27] - Complex<double> (0, 1) *
      amp[29] - Complex<double> (0, 1) * amp[32] + Complex<double> (0, 1) *
      amp[30] + Complex<double> (0, 1) * amp[33] - Complex<double> (0, 1) *
      amp[35] + Complex<double> (0, 1) * amp[36] - Complex<double> (0, 1) *
      amp[38] - Complex<double> (0, 1) * amp[40] + Complex<double> (0, 1) *
      amp[41] + Complex<double> (0, 1) * amp[54] - Complex<double> (0, 1) *
      amp[56] + Complex<double> (0, 1) * amp[57] - Complex<double> (0, 1) *
      amp[58] + Complex<double> (0, 1) * amp[64] - Complex<double> (0, 1) *
      amp[66] + Complex<double> (0, 1) * amp[67] - Complex<double> (0, 1) *
      amp[71] + Complex<double> (0, 1) * amp[69] - Complex<double> (0, 1) *
      amp[80] + Complex<double> (0, 1) * amp[78] - Complex<double> (0, 1) *
      amp[153] + Complex<double> (0, 1) * amp[155] + Complex<double> (0, 1) *
      amp[157] - Complex<double> (0, 1) * amp[158] - Complex<double> (0, 1) *
      amp[159] - Complex<double> (0, 1) * amp[160] + Complex<double> (0, 1) *
      amp[161] - Complex<double> (0, 1) * amp[162] - Complex<double> (0, 1) *
      amp[167] + Complex<double> (0, 1) * amp[169] - Complex<double> (0, 1) *
      amp[170] + Complex<double> (0, 1) * amp[171] + Complex<double> (0, 1) *
      amp[172] + Complex<double> (0, 1) * amp[173] + Complex<double> (0, 1) *
      amp[175] - Complex<double> (0, 1) * amp[177] - Complex<double> (0, 1) *
      amp[178] - Complex<double> (0, 1) * amp[179] - Complex<double> (0, 1) *
      amp[180] + Complex<double> (0, 1) * amp[184] - Complex<double> (0, 1) *
      amp[182] - Complex<double> (0, 1) * amp[185] - Complex<double> (0, 1) *
      amp[186] + Complex<double> (0, 1) * amp[188] + Complex<double> (0, 1) *
      amp[189] - Complex<double> (0, 1) * amp[191] + Complex<double> (0, 1) *
      amp[192] + Complex<double> (0, 1) * amp[199] - Complex<double> (0, 1) *
      amp[201] + Complex<double> (0, 1) * amp[202] - Complex<double> (0, 1) *
      amp[203] - Complex<double> (0, 1) * amp[206] + Complex<double> (0, 1) *
      amp[204] + Complex<double> (0, 1) * amp[207] - Complex<double> (0, 1) *
      amp[209] + Complex<double> (0, 1) * amp[210] - Complex<double> (0, 1) *
      amp[212] - Complex<double> (0, 1) * amp[213] - Complex<double> (0, 1) *
      amp[215] - Complex<double> (0, 1) * amp[216] + Complex<double> (0, 1) *
      amp[217] + Complex<double> (0, 1) * amp[218] - Complex<double> (0, 1) *
      amp[219] + Complex<double> (0, 1) * amp[220] + Complex<double> (0, 1) *
      amp[249] - Complex<double> (0, 1) * amp[251] - Complex<double> (0, 1) *
      amp[253] - Complex<double> (0, 1) * amp[259] - Complex<double> (0, 1) *
      amp[260] - Complex<double> (0, 1) * amp[262] + Complex<double> (0, 1) *
      amp[264] + Complex<double> (0, 1) * amp[266] + Complex<double> (0, 1) *
      amp[267] - Complex<double> (0, 1) * amp[269] + Complex<double> (0, 1) *
      amp[270] - Complex<double> (0, 1) * amp[272] - Complex<double> (0, 1) *
      amp[273] + Complex<double> (0, 1) * amp[275] + Complex<double> (0, 1) *
      amp[276] - Complex<double> (0, 1) * amp[278] - Complex<double> (0, 1) *
      amp[281] + Complex<double> (0, 1) * amp[279] + Complex<double> (0, 1) *
      amp[295] + Complex<double> (0, 1) * amp[296] - Complex<double> (0, 1) *
      amp[298] - Complex<double> (0, 1) * amp[299] + Complex<double> (0, 1) *
      amp[301] + Complex<double> (0, 1) * amp[302] + Complex<double> (0, 1) *
      amp[305] + Complex<double> (0, 1) * amp[304] - Complex<double> (0, 1) *
      amp[318] - Complex<double> (0, 1) * amp[320] - Complex<double> (0, 1) *
      amp[321] + Complex<double> (0, 1) * amp[323] - Complex<double> (0, 1) *
      amp[349] - Complex<double> (0, 1) * amp[348] - Complex<double> (0, 1) *
      amp[352] - Complex<double> (0, 1) * amp[351] - Complex<double> (0, 1) *
      amp[355] - Complex<double> (0, 1) * amp[354] - Complex<double> (0, 1) *
      amp[358] - Complex<double> (0, 1) * amp[357] + Complex<double> (0, 1) *
      amp[360] + Complex<double> (0, 1) * amp[363] - Complex<double> (0, 1) *
      amp[365] - Complex<double> (0, 1) * amp[374] + Complex<double> (0, 1) *
      amp[372] + Complex<double> (0, 1) * amp[380] + Complex<double> (0, 1) *
      amp[379] - Complex<double> (0, 1) * amp[388] - Complex<double> (0, 1) *
      amp[387]);
  jamp[1] = +2. * (-Complex<double> (0, 1) * amp[0] - Complex<double> (0, 1) *
      amp[1] - Complex<double> (0, 1) * amp[3] - Complex<double> (0, 1) *
      amp[5] - Complex<double> (0, 1) * amp[6] - Complex<double> (0, 1) *
      amp[7] - Complex<double> (0, 1) * amp[9] - Complex<double> (0, 1) *
      amp[11] + Complex<double> (0, 1) * amp[13] + Complex<double> (0, 1) *
      amp[14] + Complex<double> (0, 1) * amp[15] + Complex<double> (0, 1) *
      amp[16] + Complex<double> (0, 1) * amp[21] + Complex<double> (0, 1) *
      amp[22] - Complex<double> (0, 1) * amp[23] - Complex<double> (0, 1) *
      amp[24] - Complex<double> (0, 1) * amp[25] - Complex<double> (0, 1) *
      amp[26] + Complex<double> (0, 1) * amp[28] + Complex<double> (0, 1) *
      amp[29] - Complex<double> (0, 1) * amp[31] - Complex<double> (0, 1) *
      amp[30] + Complex<double> (0, 1) * amp[34] + Complex<double> (0, 1) *
      amp[35] - Complex<double> (0, 1) * amp[36] - Complex<double> (0, 1) *
      amp[37] - Complex<double> (0, 1) * amp[39] - Complex<double> (0, 1) *
      amp[41] + Complex<double> (0, 1) * amp[59] - Complex<double> (0, 1) *
      amp[61] + Complex<double> (0, 1) * amp[62] - Complex<double> (0, 1) *
      amp[63] - Complex<double> (0, 1) * amp[64] + Complex<double> (0, 1) *
      amp[66] - Complex<double> (0, 1) * amp[67] - Complex<double> (0, 1) *
      amp[74] + Complex<double> (0, 1) * amp[72] - Complex<double> (0, 1) *
      amp[79] - Complex<double> (0, 1) * amp[78] - Complex<double> (0, 1) *
      amp[117] + Complex<double> (0, 1) * amp[119] + Complex<double> (0, 1) *
      amp[121] - Complex<double> (0, 1) * amp[122] - Complex<double> (0, 1) *
      amp[123] - Complex<double> (0, 1) * amp[124] + Complex<double> (0, 1) *
      amp[125] - Complex<double> (0, 1) * amp[126] - Complex<double> (0, 1) *
      amp[131] + Complex<double> (0, 1) * amp[133] - Complex<double> (0, 1) *
      amp[134] + Complex<double> (0, 1) * amp[135] + Complex<double> (0, 1) *
      amp[136] + Complex<double> (0, 1) * amp[137] + Complex<double> (0, 1) *
      amp[139] - Complex<double> (0, 1) * amp[141] - Complex<double> (0, 1) *
      amp[142] - Complex<double> (0, 1) * amp[143] - Complex<double> (0, 1) *
      amp[144] + Complex<double> (0, 1) * amp[148] - Complex<double> (0, 1) *
      amp[146] - Complex<double> (0, 1) * amp[149] - Complex<double> (0, 1) *
      amp[150] + Complex<double> (0, 1) * amp[152] + Complex<double> (0, 1) *
      amp[194] - Complex<double> (0, 1) * amp[196] + Complex<double> (0, 1) *
      amp[197] - Complex<double> (0, 1) * amp[199] + Complex<double> (0, 1) *
      amp[201] - Complex<double> (0, 1) * amp[202] + Complex<double> (0, 1) *
      amp[203] - Complex<double> (0, 1) * amp[205] - Complex<double> (0, 1) *
      amp[204] - Complex<double> (0, 1) * amp[207] - Complex<double> (0, 1) *
      amp[208] - Complex<double> (0, 1) * amp[210] - Complex<double> (0, 1) *
      amp[211] + Complex<double> (0, 1) * amp[212] - Complex<double> (0, 1) *
      amp[214] + Complex<double> (0, 1) * amp[215] - Complex<double> (0, 1) *
      amp[217] - Complex<double> (0, 1) * amp[218] + Complex<double> (0, 1) *
      amp[219] - Complex<double> (0, 1) * amp[220] - Complex<double> (0, 1) *
      amp[249] + Complex<double> (0, 1) * amp[251] + Complex<double> (0, 1) *
      amp[253] - Complex<double> (0, 1) * amp[254] - Complex<double> (0, 1) *
      amp[255] - Complex<double> (0, 1) * amp[257] + Complex<double> (0, 1) *
      amp[265] - Complex<double> (0, 1) * amp[266] + Complex<double> (0, 1) *
      amp[268] + Complex<double> (0, 1) * amp[269] - Complex<double> (0, 1) *
      amp[270] + Complex<double> (0, 1) * amp[272] + Complex<double> (0, 1) *
      amp[273] - Complex<double> (0, 1) * amp[275] - Complex<double> (0, 1) *
      amp[276] + Complex<double> (0, 1) * amp[278] + Complex<double> (0, 1) *
      amp[281] - Complex<double> (0, 1) * amp[279] + Complex<double> (0, 1) *
      amp[283] + Complex<double> (0, 1) * amp[284] - Complex<double> (0, 1) *
      amp[286] - Complex<double> (0, 1) * amp[287] + Complex<double> (0, 1) *
      amp[289] + Complex<double> (0, 1) * amp[290] + Complex<double> (0, 1) *
      amp[293] + Complex<double> (0, 1) * amp[292] - Complex<double> (0, 1) *
      amp[312] - Complex<double> (0, 1) * amp[314] - Complex<double> (0, 1) *
      amp[315] + Complex<double> (0, 1) * amp[317] + Complex<double> (0, 1) *
      amp[348] - Complex<double> (0, 1) * amp[350] + Complex<double> (0, 1) *
      amp[351] - Complex<double> (0, 1) * amp[353] + Complex<double> (0, 1) *
      amp[354] - Complex<double> (0, 1) * amp[356] - Complex<double> (0, 1) *
      amp[359] + Complex<double> (0, 1) * amp[357] - Complex<double> (0, 1) *
      amp[360] + Complex<double> (0, 1) * amp[366] - Complex<double> (0, 1) *
      amp[368] + Complex<double> (0, 1) * amp[374] - Complex<double> (0, 1) *
      amp[372] + Complex<double> (0, 1) * amp[377] + Complex<double> (0, 1) *
      amp[376] - Complex<double> (0, 1) * amp[389] + Complex<double> (0, 1) *
      amp[387]);
  jamp[2] = +2. * (+Complex<double> (0, 1) * amp[1] + Complex<double> (0, 1) *
      amp[2] + Complex<double> (0, 1) * amp[3] + Complex<double> (0, 1) *
      amp[4] + Complex<double> (0, 1) * amp[6] - Complex<double> (0, 1) *
      amp[8] - Complex<double> (0, 1) * amp[10] + Complex<double> (0, 1) *
      amp[11] + Complex<double> (0, 1) * amp[12] - Complex<double> (0, 1) *
      amp[14] - Complex<double> (0, 1) * amp[16] + Complex<double> (0, 1) *
      amp[17] - Complex<double> (0, 1) * amp[18] - Complex<double> (0, 1) *
      amp[19] + Complex<double> (0, 1) * amp[20] - Complex<double> (0, 1) *
      amp[21] - Complex<double> (0, 1) * amp[22] + Complex<double> (0, 1) *
      amp[23] - Complex<double> (0, 1) * amp[27] - Complex<double> (0, 1) *
      amp[28] + Complex<double> (0, 1) * amp[32] + Complex<double> (0, 1) *
      amp[31] - Complex<double> (0, 1) * amp[34] - Complex<double> (0, 1) *
      amp[33] + Complex<double> (0, 1) * amp[42] - Complex<double> (0, 1) *
      amp[44] - Complex<double> (0, 1) * amp[46] + Complex<double> (0, 1) *
      amp[47] - Complex<double> (0, 1) * amp[54] + Complex<double> (0, 1) *
      amp[56] - Complex<double> (0, 1) * amp[57] + Complex<double> (0, 1) *
      amp[58] - Complex<double> (0, 1) * amp[59] - Complex<double> (0, 1) *
      amp[60] - Complex<double> (0, 1) * amp[62] - Complex<double> (0, 1) *
      amp[70] - Complex<double> (0, 1) * amp[69] + Complex<double> (0, 1) *
      amp[80] + Complex<double> (0, 1) * amp[79] + Complex<double> (0, 1) *
      amp[153] + Complex<double> (0, 1) * amp[154] + Complex<double> (0, 1) *
      amp[156] + Complex<double> (0, 1) * amp[158] - Complex<double> (0, 1) *
      amp[163] - Complex<double> (0, 1) * amp[164] + Complex<double> (0, 1) *
      amp[165] - Complex<double> (0, 1) * amp[166] + Complex<double> (0, 1) *
      amp[167] + Complex<double> (0, 1) * amp[168] + Complex<double> (0, 1) *
      amp[170] - Complex<double> (0, 1) * amp[172] + Complex<double> (0, 1) *
      amp[174] - Complex<double> (0, 1) * amp[175] + Complex<double> (0, 1) *
      amp[176] + Complex<double> (0, 1) * amp[177] + Complex<double> (0, 1) *
      amp[178] + Complex<double> (0, 1) * amp[179] - Complex<double> (0, 1) *
      amp[181] + Complex<double> (0, 1) * amp[183] + Complex<double> (0, 1) *
      amp[182] + Complex<double> (0, 1) * amp[185] - Complex<double> (0, 1) *
      amp[187] - Complex<double> (0, 1) * amp[188] - Complex<double> (0, 1) *
      amp[189] + Complex<double> (0, 1) * amp[191] - Complex<double> (0, 1) *
      amp[192] - Complex<double> (0, 1) * amp[194] - Complex<double> (0, 1) *
      amp[195] - Complex<double> (0, 1) * amp[197] - Complex<double> (0, 1) *
      amp[198] + Complex<double> (0, 1) * amp[206] + Complex<double> (0, 1) *
      amp[205] + Complex<double> (0, 1) * amp[221] - Complex<double> (0, 1) *
      amp[223] + Complex<double> (0, 1) * amp[224] - Complex<double> (0, 1) *
      amp[226] - Complex<double> (0, 1) * amp[227] - Complex<double> (0, 1) *
      amp[229] - Complex<double> (0, 1) * amp[230] + Complex<double> (0, 1) *
      amp[231] + Complex<double> (0, 1) * amp[232] - Complex<double> (0, 1) *
      amp[233] + Complex<double> (0, 1) * amp[234] + Complex<double> (0, 1) *
      amp[254] - Complex<double> (0, 1) * amp[256] - Complex<double> (0, 1) *
      amp[258] + Complex<double> (0, 1) * amp[259] + Complex<double> (0, 1) *
      amp[260] + Complex<double> (0, 1) * amp[262] - Complex<double> (0, 1) *
      amp[264] - Complex<double> (0, 1) * amp[265] - Complex<double> (0, 1) *
      amp[268] - Complex<double> (0, 1) * amp[267] + Complex<double> (0, 1) *
      amp[282] - Complex<double> (0, 1) * amp[284] - Complex<double> (0, 1) *
      amp[285] + Complex<double> (0, 1) * amp[287] + Complex<double> (0, 1) *
      amp[288] - Complex<double> (0, 1) * amp[290] - Complex<double> (0, 1) *
      amp[293] + Complex<double> (0, 1) * amp[291] - Complex<double> (0, 1) *
      amp[295] - Complex<double> (0, 1) * amp[296] + Complex<double> (0, 1) *
      amp[298] + Complex<double> (0, 1) * amp[299] - Complex<double> (0, 1) *
      amp[301] - Complex<double> (0, 1) * amp[302] - Complex<double> (0, 1) *
      amp[305] - Complex<double> (0, 1) * amp[304] - Complex<double> (0, 1) *
      amp[319] + Complex<double> (0, 1) * amp[320] - Complex<double> (0, 1) *
      amp[322] - Complex<double> (0, 1) * amp[323] - Complex<double> (0, 1) *
      amp[337] - Complex<double> (0, 1) * amp[336] - Complex<double> (0, 1) *
      amp[340] - Complex<double> (0, 1) * amp[339] - Complex<double> (0, 1) *
      amp[343] - Complex<double> (0, 1) * amp[342] - Complex<double> (0, 1) *
      amp[346] - Complex<double> (0, 1) * amp[345] + Complex<double> (0, 1) *
      amp[361] - Complex<double> (0, 1) * amp[364] - Complex<double> (0, 1) *
      amp[363] - Complex<double> (0, 1) * amp[377] + Complex<double> (0, 1) *
      amp[375] - Complex<double> (0, 1) * amp[380] - Complex<double> (0, 1) *
      amp[379] - Complex<double> (0, 1) * amp[385] - Complex<double> (0, 1) *
      amp[384]);
  jamp[3] = +2. * (-Complex<double> (0, 1) * amp[0] - Complex<double> (0, 1) *
      amp[1] - Complex<double> (0, 1) * amp[3] - Complex<double> (0, 1) *
      amp[5] - Complex<double> (0, 1) * amp[6] - Complex<double> (0, 1) *
      amp[7] - Complex<double> (0, 1) * amp[9] - Complex<double> (0, 1) *
      amp[11] + Complex<double> (0, 1) * amp[13] + Complex<double> (0, 1) *
      amp[14] + Complex<double> (0, 1) * amp[15] + Complex<double> (0, 1) *
      amp[16] + Complex<double> (0, 1) * amp[21] + Complex<double> (0, 1) *
      amp[22] - Complex<double> (0, 1) * amp[23] - Complex<double> (0, 1) *
      amp[24] - Complex<double> (0, 1) * amp[25] - Complex<double> (0, 1) *
      amp[26] + Complex<double> (0, 1) * amp[28] + Complex<double> (0, 1) *
      amp[29] - Complex<double> (0, 1) * amp[31] - Complex<double> (0, 1) *
      amp[30] + Complex<double> (0, 1) * amp[34] + Complex<double> (0, 1) *
      amp[35] - Complex<double> (0, 1) * amp[42] - Complex<double> (0, 1) *
      amp[43] - Complex<double> (0, 1) * amp[45] - Complex<double> (0, 1) *
      amp[47] + Complex<double> (0, 1) * amp[59] + Complex<double> (0, 1) *
      amp[60] + Complex<double> (0, 1) * amp[62] - Complex<double> (0, 1) *
      amp[64] - Complex<double> (0, 1) * amp[65] - Complex<double> (0, 1) *
      amp[67] - Complex<double> (0, 1) * amp[68] - Complex<double> (0, 1) *
      amp[77] + Complex<double> (0, 1) * amp[75] - Complex<double> (0, 1) *
      amp[79] - Complex<double> (0, 1) * amp[78] - Complex<double> (0, 1) *
      amp[81] + Complex<double> (0, 1) * amp[83] + Complex<double> (0, 1) *
      amp[85] - Complex<double> (0, 1) * amp[86] - Complex<double> (0, 1) *
      amp[87] - Complex<double> (0, 1) * amp[88] + Complex<double> (0, 1) *
      amp[89] - Complex<double> (0, 1) * amp[90] - Complex<double> (0, 1) *
      amp[95] + Complex<double> (0, 1) * amp[97] - Complex<double> (0, 1) *
      amp[98] + Complex<double> (0, 1) * amp[99] + Complex<double> (0, 1) *
      amp[100] + Complex<double> (0, 1) * amp[101] + Complex<double> (0, 1) *
      amp[103] - Complex<double> (0, 1) * amp[105] - Complex<double> (0, 1) *
      amp[106] - Complex<double> (0, 1) * amp[107] - Complex<double> (0, 1) *
      amp[108] + Complex<double> (0, 1) * amp[112] - Complex<double> (0, 1) *
      amp[110] - Complex<double> (0, 1) * amp[113] - Complex<double> (0, 1) *
      amp[114] + Complex<double> (0, 1) * amp[116] + Complex<double> (0, 1) *
      amp[194] + Complex<double> (0, 1) * amp[195] + Complex<double> (0, 1) *
      amp[197] + Complex<double> (0, 1) * amp[198] - Complex<double> (0, 1) *
      amp[199] - Complex<double> (0, 1) * amp[200] - Complex<double> (0, 1) *
      amp[202] - Complex<double> (0, 1) * amp[205] - Complex<double> (0, 1) *
      amp[204] - Complex<double> (0, 1) * amp[221] - Complex<double> (0, 1) *
      amp[222] - Complex<double> (0, 1) * amp[224] - Complex<double> (0, 1) *
      amp[225] + Complex<double> (0, 1) * amp[226] - Complex<double> (0, 1) *
      amp[228] + Complex<double> (0, 1) * amp[229] - Complex<double> (0, 1) *
      amp[231] - Complex<double> (0, 1) * amp[232] + Complex<double> (0, 1) *
      amp[233] - Complex<double> (0, 1) * amp[234] - Complex<double> (0, 1) *
      amp[249] - Complex<double> (0, 1) * amp[250] - Complex<double> (0, 1) *
      amp[252] - Complex<double> (0, 1) * amp[254] + Complex<double> (0, 1) *
      amp[256] + Complex<double> (0, 1) * amp[258] + Complex<double> (0, 1) *
      amp[265] - Complex<double> (0, 1) * amp[266] + Complex<double> (0, 1) *
      amp[268] + Complex<double> (0, 1) * amp[269] + Complex<double> (0, 1) *
      amp[271] + Complex<double> (0, 1) * amp[272] - Complex<double> (0, 1) *
      amp[274] - Complex<double> (0, 1) * amp[275] + Complex<double> (0, 1) *
      amp[277] + Complex<double> (0, 1) * amp[278] + Complex<double> (0, 1) *
      amp[281] + Complex<double> (0, 1) * amp[280] - Complex<double> (0, 1) *
      amp[282] + Complex<double> (0, 1) * amp[284] + Complex<double> (0, 1) *
      amp[285] - Complex<double> (0, 1) * amp[287] - Complex<double> (0, 1) *
      amp[288] + Complex<double> (0, 1) * amp[290] + Complex<double> (0, 1) *
      amp[293] - Complex<double> (0, 1) * amp[291] - Complex<double> (0, 1) *
      amp[306] - Complex<double> (0, 1) * amp[308] - Complex<double> (0, 1) *
      amp[309] + Complex<double> (0, 1) * amp[311] + Complex<double> (0, 1) *
      amp[336] - Complex<double> (0, 1) * amp[338] + Complex<double> (0, 1) *
      amp[339] - Complex<double> (0, 1) * amp[341] + Complex<double> (0, 1) *
      amp[342] - Complex<double> (0, 1) * amp[344] - Complex<double> (0, 1) *
      amp[347] + Complex<double> (0, 1) * amp[345] - Complex<double> (0, 1) *
      amp[361] + Complex<double> (0, 1) * amp[369] - Complex<double> (0, 1) *
      amp[371] + Complex<double> (0, 1) * amp[374] + Complex<double> (0, 1) *
      amp[373] + Complex<double> (0, 1) * amp[377] - Complex<double> (0, 1) *
      amp[375] - Complex<double> (0, 1) * amp[386] + Complex<double> (0, 1) *
      amp[384]);
  jamp[4] = +2. * (+Complex<double> (0, 1) * amp[1] + Complex<double> (0, 1) *
      amp[2] + Complex<double> (0, 1) * amp[3] + Complex<double> (0, 1) *
      amp[4] + Complex<double> (0, 1) * amp[6] - Complex<double> (0, 1) *
      amp[8] - Complex<double> (0, 1) * amp[10] + Complex<double> (0, 1) *
      amp[11] + Complex<double> (0, 1) * amp[12] - Complex<double> (0, 1) *
      amp[14] - Complex<double> (0, 1) * amp[16] + Complex<double> (0, 1) *
      amp[17] - Complex<double> (0, 1) * amp[18] - Complex<double> (0, 1) *
      amp[19] + Complex<double> (0, 1) * amp[20] - Complex<double> (0, 1) *
      amp[21] - Complex<double> (0, 1) * amp[22] + Complex<double> (0, 1) *
      amp[23] - Complex<double> (0, 1) * amp[27] - Complex<double> (0, 1) *
      amp[28] + Complex<double> (0, 1) * amp[32] + Complex<double> (0, 1) *
      amp[31] - Complex<double> (0, 1) * amp[34] - Complex<double> (0, 1) *
      amp[33] + Complex<double> (0, 1) * amp[48] - Complex<double> (0, 1) *
      amp[50] - Complex<double> (0, 1) * amp[52] + Complex<double> (0, 1) *
      amp[53] - Complex<double> (0, 1) * amp[54] - Complex<double> (0, 1) *
      amp[55] - Complex<double> (0, 1) * amp[57] - Complex<double> (0, 1) *
      amp[59] + Complex<double> (0, 1) * amp[61] - Complex<double> (0, 1) *
      amp[62] + Complex<double> (0, 1) * amp[63] - Complex<double> (0, 1) *
      amp[73] - Complex<double> (0, 1) * amp[72] + Complex<double> (0, 1) *
      amp[80] + Complex<double> (0, 1) * amp[79] + Complex<double> (0, 1) *
      amp[117] + Complex<double> (0, 1) * amp[118] + Complex<double> (0, 1) *
      amp[120] + Complex<double> (0, 1) * amp[122] - Complex<double> (0, 1) *
      amp[127] - Complex<double> (0, 1) * amp[128] + Complex<double> (0, 1) *
      amp[129] - Complex<double> (0, 1) * amp[130] + Complex<double> (0, 1) *
      amp[131] + Complex<double> (0, 1) * amp[132] + Complex<double> (0, 1) *
      amp[134] - Complex<double> (0, 1) * amp[136] + Complex<double> (0, 1) *
      amp[138] - Complex<double> (0, 1) * amp[139] + Complex<double> (0, 1) *
      amp[140] + Complex<double> (0, 1) * amp[141] + Complex<double> (0, 1) *
      amp[142] + Complex<double> (0, 1) * amp[143] - Complex<double> (0, 1) *
      amp[145] + Complex<double> (0, 1) * amp[147] + Complex<double> (0, 1) *
      amp[146] + Complex<double> (0, 1) * amp[149] - Complex<double> (0, 1) *
      amp[151] - Complex<double> (0, 1) * amp[152] - Complex<double> (0, 1) *
      amp[189] - Complex<double> (0, 1) * amp[190] - Complex<double> (0, 1) *
      amp[192] - Complex<double> (0, 1) * amp[193] - Complex<double> (0, 1) *
      amp[194] + Complex<double> (0, 1) * amp[196] - Complex<double> (0, 1) *
      amp[197] + Complex<double> (0, 1) * amp[206] + Complex<double> (0, 1) *
      amp[205] + Complex<double> (0, 1) * amp[235] - Complex<double> (0, 1) *
      amp[237] + Complex<double> (0, 1) * amp[238] - Complex<double> (0, 1) *
      amp[240] - Complex<double> (0, 1) * amp[241] - Complex<double> (0, 1) *
      amp[243] - Complex<double> (0, 1) * amp[244] + Complex<double> (0, 1) *
      amp[245] + Complex<double> (0, 1) * amp[246] - Complex<double> (0, 1) *
      amp[247] + Complex<double> (0, 1) * amp[248] + Complex<double> (0, 1) *
      amp[254] + Complex<double> (0, 1) * amp[255] + Complex<double> (0, 1) *
      amp[257] + Complex<double> (0, 1) * amp[259] - Complex<double> (0, 1) *
      amp[261] - Complex<double> (0, 1) * amp[263] - Complex<double> (0, 1) *
      amp[264] - Complex<double> (0, 1) * amp[265] - Complex<double> (0, 1) *
      amp[268] - Complex<double> (0, 1) * amp[267] - Complex<double> (0, 1) *
      amp[283] - Complex<double> (0, 1) * amp[284] + Complex<double> (0, 1) *
      amp[286] + Complex<double> (0, 1) * amp[287] - Complex<double> (0, 1) *
      amp[289] - Complex<double> (0, 1) * amp[290] - Complex<double> (0, 1) *
      amp[293] - Complex<double> (0, 1) * amp[292] + Complex<double> (0, 1) *
      amp[294] - Complex<double> (0, 1) * amp[296] - Complex<double> (0, 1) *
      amp[297] + Complex<double> (0, 1) * amp[299] + Complex<double> (0, 1) *
      amp[300] - Complex<double> (0, 1) * amp[302] - Complex<double> (0, 1) *
      amp[305] + Complex<double> (0, 1) * amp[303] - Complex<double> (0, 1) *
      amp[313] + Complex<double> (0, 1) * amp[314] - Complex<double> (0, 1) *
      amp[316] - Complex<double> (0, 1) * amp[317] - Complex<double> (0, 1) *
      amp[325] - Complex<double> (0, 1) * amp[324] - Complex<double> (0, 1) *
      amp[328] - Complex<double> (0, 1) * amp[327] - Complex<double> (0, 1) *
      amp[331] - Complex<double> (0, 1) * amp[330] - Complex<double> (0, 1) *
      amp[334] - Complex<double> (0, 1) * amp[333] + Complex<double> (0, 1) *
      amp[362] - Complex<double> (0, 1) * amp[367] - Complex<double> (0, 1) *
      amp[366] - Complex<double> (0, 1) * amp[377] - Complex<double> (0, 1) *
      amp[376] - Complex<double> (0, 1) * amp[380] + Complex<double> (0, 1) *
      amp[378] - Complex<double> (0, 1) * amp[382] - Complex<double> (0, 1) *
      amp[381]);
  jamp[5] = +2. * (+Complex<double> (0, 1) * amp[0] - Complex<double> (0, 1) *
      amp[2] - Complex<double> (0, 1) * amp[4] + Complex<double> (0, 1) *
      amp[5] + Complex<double> (0, 1) * amp[7] + Complex<double> (0, 1) *
      amp[8] + Complex<double> (0, 1) * amp[9] + Complex<double> (0, 1) *
      amp[10] - Complex<double> (0, 1) * amp[12] - Complex<double> (0, 1) *
      amp[13] - Complex<double> (0, 1) * amp[15] - Complex<double> (0, 1) *
      amp[17] + Complex<double> (0, 1) * amp[18] + Complex<double> (0, 1) *
      amp[19] - Complex<double> (0, 1) * amp[20] + Complex<double> (0, 1) *
      amp[24] + Complex<double> (0, 1) * amp[25] + Complex<double> (0, 1) *
      amp[26] + Complex<double> (0, 1) * amp[27] - Complex<double> (0, 1) *
      amp[29] - Complex<double> (0, 1) * amp[32] + Complex<double> (0, 1) *
      amp[30] + Complex<double> (0, 1) * amp[33] - Complex<double> (0, 1) *
      amp[35] - Complex<double> (0, 1) * amp[48] - Complex<double> (0, 1) *
      amp[49] - Complex<double> (0, 1) * amp[51] - Complex<double> (0, 1) *
      amp[53] + Complex<double> (0, 1) * amp[54] + Complex<double> (0, 1) *
      amp[55] + Complex<double> (0, 1) * amp[57] + Complex<double> (0, 1) *
      amp[64] + Complex<double> (0, 1) * amp[65] + Complex<double> (0, 1) *
      amp[67] + Complex<double> (0, 1) * amp[68] - Complex<double> (0, 1) *
      amp[76] - Complex<double> (0, 1) * amp[75] - Complex<double> (0, 1) *
      amp[80] + Complex<double> (0, 1) * amp[78] + Complex<double> (0, 1) *
      amp[81] + Complex<double> (0, 1) * amp[82] + Complex<double> (0, 1) *
      amp[84] + Complex<double> (0, 1) * amp[86] - Complex<double> (0, 1) *
      amp[91] - Complex<double> (0, 1) * amp[92] + Complex<double> (0, 1) *
      amp[93] - Complex<double> (0, 1) * amp[94] + Complex<double> (0, 1) *
      amp[95] + Complex<double> (0, 1) * amp[96] + Complex<double> (0, 1) *
      amp[98] - Complex<double> (0, 1) * amp[100] + Complex<double> (0, 1) *
      amp[102] - Complex<double> (0, 1) * amp[103] + Complex<double> (0, 1) *
      amp[104] + Complex<double> (0, 1) * amp[105] + Complex<double> (0, 1) *
      amp[106] + Complex<double> (0, 1) * amp[107] - Complex<double> (0, 1) *
      amp[109] + Complex<double> (0, 1) * amp[111] + Complex<double> (0, 1) *
      amp[110] + Complex<double> (0, 1) * amp[113] - Complex<double> (0, 1) *
      amp[115] - Complex<double> (0, 1) * amp[116] + Complex<double> (0, 1) *
      amp[189] + Complex<double> (0, 1) * amp[190] + Complex<double> (0, 1) *
      amp[192] + Complex<double> (0, 1) * amp[193] + Complex<double> (0, 1) *
      amp[199] + Complex<double> (0, 1) * amp[200] + Complex<double> (0, 1) *
      amp[202] - Complex<double> (0, 1) * amp[206] + Complex<double> (0, 1) *
      amp[204] - Complex<double> (0, 1) * amp[235] - Complex<double> (0, 1) *
      amp[236] - Complex<double> (0, 1) * amp[238] - Complex<double> (0, 1) *
      amp[239] + Complex<double> (0, 1) * amp[240] - Complex<double> (0, 1) *
      amp[242] + Complex<double> (0, 1) * amp[243] - Complex<double> (0, 1) *
      amp[245] - Complex<double> (0, 1) * amp[246] + Complex<double> (0, 1) *
      amp[247] - Complex<double> (0, 1) * amp[248] + Complex<double> (0, 1) *
      amp[249] + Complex<double> (0, 1) * amp[250] + Complex<double> (0, 1) *
      amp[252] - Complex<double> (0, 1) * amp[259] + Complex<double> (0, 1) *
      amp[261] + Complex<double> (0, 1) * amp[263] + Complex<double> (0, 1) *
      amp[264] + Complex<double> (0, 1) * amp[266] + Complex<double> (0, 1) *
      amp[267] - Complex<double> (0, 1) * amp[269] - Complex<double> (0, 1) *
      amp[271] - Complex<double> (0, 1) * amp[272] + Complex<double> (0, 1) *
      amp[274] + Complex<double> (0, 1) * amp[275] - Complex<double> (0, 1) *
      amp[277] - Complex<double> (0, 1) * amp[278] - Complex<double> (0, 1) *
      amp[281] - Complex<double> (0, 1) * amp[280] - Complex<double> (0, 1) *
      amp[294] + Complex<double> (0, 1) * amp[296] + Complex<double> (0, 1) *
      amp[297] - Complex<double> (0, 1) * amp[299] - Complex<double> (0, 1) *
      amp[300] + Complex<double> (0, 1) * amp[302] + Complex<double> (0, 1) *
      amp[305] - Complex<double> (0, 1) * amp[303] - Complex<double> (0, 1) *
      amp[307] + Complex<double> (0, 1) * amp[308] - Complex<double> (0, 1) *
      amp[310] - Complex<double> (0, 1) * amp[311] + Complex<double> (0, 1) *
      amp[324] - Complex<double> (0, 1) * amp[326] + Complex<double> (0, 1) *
      amp[327] - Complex<double> (0, 1) * amp[329] + Complex<double> (0, 1) *
      amp[330] - Complex<double> (0, 1) * amp[332] - Complex<double> (0, 1) *
      amp[335] + Complex<double> (0, 1) * amp[333] - Complex<double> (0, 1) *
      amp[362] - Complex<double> (0, 1) * amp[370] - Complex<double> (0, 1) *
      amp[369] - Complex<double> (0, 1) * amp[374] - Complex<double> (0, 1) *
      amp[373] + Complex<double> (0, 1) * amp[380] - Complex<double> (0, 1) *
      amp[378] - Complex<double> (0, 1) * amp[383] + Complex<double> (0, 1) *
      amp[381]);
  jamp[6] = +2. * (-Complex<double> (0, 1) * amp[36] + Complex<double> (0, 1) *
      amp[38] + Complex<double> (0, 1) * amp[40] - Complex<double> (0, 1) *
      amp[41] + Complex<double> (0, 1) * amp[43] + Complex<double> (0, 1) *
      amp[44] + Complex<double> (0, 1) * amp[45] + Complex<double> (0, 1) *
      amp[46] + Complex<double> (0, 1) * amp[65] + Complex<double> (0, 1) *
      amp[66] + Complex<double> (0, 1) * amp[68] + Complex<double> (0, 1) *
      amp[71] + Complex<double> (0, 1) * amp[70] + Complex<double> (0, 1) *
      amp[77] - Complex<double> (0, 1) * amp[75] + Complex<double> (0, 1) *
      amp[81] - Complex<double> (0, 1) * amp[83] - Complex<double> (0, 1) *
      amp[85] + Complex<double> (0, 1) * amp[86] + Complex<double> (0, 1) *
      amp[87] + Complex<double> (0, 1) * amp[88] - Complex<double> (0, 1) *
      amp[89] + Complex<double> (0, 1) * amp[90] + Complex<double> (0, 1) *
      amp[95] - Complex<double> (0, 1) * amp[97] + Complex<double> (0, 1) *
      amp[98] - Complex<double> (0, 1) * amp[99] - Complex<double> (0, 1) *
      amp[100] - Complex<double> (0, 1) * amp[101] - Complex<double> (0, 1) *
      amp[103] + Complex<double> (0, 1) * amp[105] + Complex<double> (0, 1) *
      amp[106] + Complex<double> (0, 1) * amp[107] + Complex<double> (0, 1) *
      amp[108] - Complex<double> (0, 1) * amp[112] + Complex<double> (0, 1) *
      amp[110] + Complex<double> (0, 1) * amp[113] + Complex<double> (0, 1) *
      amp[114] - Complex<double> (0, 1) * amp[116] - Complex<double> (0, 1) *
      amp[154] - Complex<double> (0, 1) * amp[155] - Complex<double> (0, 1) *
      amp[156] - Complex<double> (0, 1) * amp[157] + Complex<double> (0, 1) *
      amp[159] + Complex<double> (0, 1) * amp[160] - Complex<double> (0, 1) *
      amp[161] + Complex<double> (0, 1) * amp[162] + Complex<double> (0, 1) *
      amp[163] + Complex<double> (0, 1) * amp[164] - Complex<double> (0, 1) *
      amp[165] + Complex<double> (0, 1) * amp[166] - Complex<double> (0, 1) *
      amp[168] - Complex<double> (0, 1) * amp[169] - Complex<double> (0, 1) *
      amp[171] - Complex<double> (0, 1) * amp[173] - Complex<double> (0, 1) *
      amp[174] - Complex<double> (0, 1) * amp[176] + Complex<double> (0, 1) *
      amp[180] + Complex<double> (0, 1) * amp[181] - Complex<double> (0, 1) *
      amp[184] - Complex<double> (0, 1) * amp[183] + Complex<double> (0, 1) *
      amp[187] + Complex<double> (0, 1) * amp[186] + Complex<double> (0, 1) *
      amp[200] + Complex<double> (0, 1) * amp[201] + Complex<double> (0, 1) *
      amp[203] - Complex<double> (0, 1) * amp[207] + Complex<double> (0, 1) *
      amp[209] - Complex<double> (0, 1) * amp[210] + Complex<double> (0, 1) *
      amp[212] + Complex<double> (0, 1) * amp[213] + Complex<double> (0, 1) *
      amp[215] + Complex<double> (0, 1) * amp[216] - Complex<double> (0, 1) *
      amp[217] - Complex<double> (0, 1) * amp[218] + Complex<double> (0, 1) *
      amp[219] - Complex<double> (0, 1) * amp[220] + Complex<double> (0, 1) *
      amp[222] + Complex<double> (0, 1) * amp[223] + Complex<double> (0, 1) *
      amp[225] + Complex<double> (0, 1) * amp[227] + Complex<double> (0, 1) *
      amp[228] + Complex<double> (0, 1) * amp[230] + Complex<double> (0, 1) *
      amp[250] + Complex<double> (0, 1) * amp[251] + Complex<double> (0, 1) *
      amp[252] + Complex<double> (0, 1) * amp[253] - Complex<double> (0, 1) *
      amp[271] - Complex<double> (0, 1) * amp[270] + Complex<double> (0, 1) *
      amp[274] + Complex<double> (0, 1) * amp[273] - Complex<double> (0, 1) *
      amp[277] - Complex<double> (0, 1) * amp[276] - Complex<double> (0, 1) *
      amp[280] - Complex<double> (0, 1) * amp[279] + Complex<double> (0, 1) *
      amp[306] + Complex<double> (0, 1) * amp[308] + Complex<double> (0, 1) *
      amp[309] - Complex<double> (0, 1) * amp[311] + Complex<double> (0, 1) *
      amp[318] + Complex<double> (0, 1) * amp[319] + Complex<double> (0, 1) *
      amp[322] + Complex<double> (0, 1) * amp[321] + Complex<double> (0, 1) *
      amp[337] + Complex<double> (0, 1) * amp[338] + Complex<double> (0, 1) *
      amp[340] + Complex<double> (0, 1) * amp[341] + Complex<double> (0, 1) *
      amp[343] + Complex<double> (0, 1) * amp[344] + Complex<double> (0, 1) *
      amp[347] + Complex<double> (0, 1) * amp[346] + Complex<double> (0, 1) *
      amp[349] + Complex<double> (0, 1) * amp[348] + Complex<double> (0, 1) *
      amp[352] + Complex<double> (0, 1) * amp[351] + Complex<double> (0, 1) *
      amp[355] + Complex<double> (0, 1) * amp[354] + Complex<double> (0, 1) *
      amp[358] + Complex<double> (0, 1) * amp[357] - Complex<double> (0, 1) *
      amp[360] + Complex<double> (0, 1) * amp[364] + Complex<double> (0, 1) *
      amp[365] - Complex<double> (0, 1) * amp[369] + Complex<double> (0, 1) *
      amp[371] - Complex<double> (0, 1) * amp[373] - Complex<double> (0, 1) *
      amp[372] + Complex<double> (0, 1) * amp[386] + Complex<double> (0, 1) *
      amp[385] + Complex<double> (0, 1) * amp[388] + Complex<double> (0, 1) *
      amp[387]);
  jamp[7] = +2. * (+Complex<double> (0, 1) * amp[36] + Complex<double> (0, 1) *
      amp[37] + Complex<double> (0, 1) * amp[39] + Complex<double> (0, 1) *
      amp[41] + Complex<double> (0, 1) * amp[49] + Complex<double> (0, 1) *
      amp[50] + Complex<double> (0, 1) * amp[51] + Complex<double> (0, 1) *
      amp[52] - Complex<double> (0, 1) * amp[65] - Complex<double> (0, 1) *
      amp[66] - Complex<double> (0, 1) * amp[68] + Complex<double> (0, 1) *
      amp[74] + Complex<double> (0, 1) * amp[73] + Complex<double> (0, 1) *
      amp[76] + Complex<double> (0, 1) * amp[75] - Complex<double> (0, 1) *
      amp[81] - Complex<double> (0, 1) * amp[82] - Complex<double> (0, 1) *
      amp[84] - Complex<double> (0, 1) * amp[86] + Complex<double> (0, 1) *
      amp[91] + Complex<double> (0, 1) * amp[92] - Complex<double> (0, 1) *
      amp[93] + Complex<double> (0, 1) * amp[94] - Complex<double> (0, 1) *
      amp[95] - Complex<double> (0, 1) * amp[96] - Complex<double> (0, 1) *
      amp[98] + Complex<double> (0, 1) * amp[100] - Complex<double> (0, 1) *
      amp[102] + Complex<double> (0, 1) * amp[103] - Complex<double> (0, 1) *
      amp[104] - Complex<double> (0, 1) * amp[105] - Complex<double> (0, 1) *
      amp[106] - Complex<double> (0, 1) * amp[107] + Complex<double> (0, 1) *
      amp[109] - Complex<double> (0, 1) * amp[111] - Complex<double> (0, 1) *
      amp[110] - Complex<double> (0, 1) * amp[113] + Complex<double> (0, 1) *
      amp[115] + Complex<double> (0, 1) * amp[116] - Complex<double> (0, 1) *
      amp[118] - Complex<double> (0, 1) * amp[119] - Complex<double> (0, 1) *
      amp[120] - Complex<double> (0, 1) * amp[121] + Complex<double> (0, 1) *
      amp[123] + Complex<double> (0, 1) * amp[124] - Complex<double> (0, 1) *
      amp[125] + Complex<double> (0, 1) * amp[126] + Complex<double> (0, 1) *
      amp[127] + Complex<double> (0, 1) * amp[128] - Complex<double> (0, 1) *
      amp[129] + Complex<double> (0, 1) * amp[130] - Complex<double> (0, 1) *
      amp[132] - Complex<double> (0, 1) * amp[133] - Complex<double> (0, 1) *
      amp[135] - Complex<double> (0, 1) * amp[137] - Complex<double> (0, 1) *
      amp[138] - Complex<double> (0, 1) * amp[140] + Complex<double> (0, 1) *
      amp[144] + Complex<double> (0, 1) * amp[145] - Complex<double> (0, 1) *
      amp[148] - Complex<double> (0, 1) * amp[147] + Complex<double> (0, 1) *
      amp[151] + Complex<double> (0, 1) * amp[150] - Complex<double> (0, 1) *
      amp[200] - Complex<double> (0, 1) * amp[201] - Complex<double> (0, 1) *
      amp[203] + Complex<double> (0, 1) * amp[207] + Complex<double> (0, 1) *
      amp[208] + Complex<double> (0, 1) * amp[210] + Complex<double> (0, 1) *
      amp[211] - Complex<double> (0, 1) * amp[212] + Complex<double> (0, 1) *
      amp[214] - Complex<double> (0, 1) * amp[215] + Complex<double> (0, 1) *
      amp[217] + Complex<double> (0, 1) * amp[218] - Complex<double> (0, 1) *
      amp[219] + Complex<double> (0, 1) * amp[220] + Complex<double> (0, 1) *
      amp[236] + Complex<double> (0, 1) * amp[237] + Complex<double> (0, 1) *
      amp[239] + Complex<double> (0, 1) * amp[241] + Complex<double> (0, 1) *
      amp[242] + Complex<double> (0, 1) * amp[244] - Complex<double> (0, 1) *
      amp[250] - Complex<double> (0, 1) * amp[251] - Complex<double> (0, 1) *
      amp[252] - Complex<double> (0, 1) * amp[253] + Complex<double> (0, 1) *
      amp[271] + Complex<double> (0, 1) * amp[270] - Complex<double> (0, 1) *
      amp[274] - Complex<double> (0, 1) * amp[273] + Complex<double> (0, 1) *
      amp[277] + Complex<double> (0, 1) * amp[276] + Complex<double> (0, 1) *
      amp[280] + Complex<double> (0, 1) * amp[279] + Complex<double> (0, 1) *
      amp[307] - Complex<double> (0, 1) * amp[308] + Complex<double> (0, 1) *
      amp[310] + Complex<double> (0, 1) * amp[311] + Complex<double> (0, 1) *
      amp[312] + Complex<double> (0, 1) * amp[313] + Complex<double> (0, 1) *
      amp[316] + Complex<double> (0, 1) * amp[315] + Complex<double> (0, 1) *
      amp[325] + Complex<double> (0, 1) * amp[326] + Complex<double> (0, 1) *
      amp[328] + Complex<double> (0, 1) * amp[329] + Complex<double> (0, 1) *
      amp[331] + Complex<double> (0, 1) * amp[332] + Complex<double> (0, 1) *
      amp[335] + Complex<double> (0, 1) * amp[334] - Complex<double> (0, 1) *
      amp[348] + Complex<double> (0, 1) * amp[350] - Complex<double> (0, 1) *
      amp[351] + Complex<double> (0, 1) * amp[353] - Complex<double> (0, 1) *
      amp[354] + Complex<double> (0, 1) * amp[356] + Complex<double> (0, 1) *
      amp[359] - Complex<double> (0, 1) * amp[357] + Complex<double> (0, 1) *
      amp[360] + Complex<double> (0, 1) * amp[367] + Complex<double> (0, 1) *
      amp[368] + Complex<double> (0, 1) * amp[370] + Complex<double> (0, 1) *
      amp[369] + Complex<double> (0, 1) * amp[373] + Complex<double> (0, 1) *
      amp[372] + Complex<double> (0, 1) * amp[383] + Complex<double> (0, 1) *
      amp[382] + Complex<double> (0, 1) * amp[389] - Complex<double> (0, 1) *
      amp[387]);
  jamp[8] = +2. * (-Complex<double> (0, 1) * amp[43] - Complex<double> (0, 1) *
      amp[44] - Complex<double> (0, 1) * amp[45] - Complex<double> (0, 1) *
      amp[46] - Complex<double> (0, 1) * amp[48] - Complex<double> (0, 1) *
      amp[49] - Complex<double> (0, 1) * amp[51] - Complex<double> (0, 1) *
      amp[53] + Complex<double> (0, 1) * amp[55] + Complex<double> (0, 1) *
      amp[56] + Complex<double> (0, 1) * amp[58] - Complex<double> (0, 1) *
      amp[70] - Complex<double> (0, 1) * amp[69] - Complex<double> (0, 1) *
      amp[77] - Complex<double> (0, 1) * amp[76] + Complex<double> (0, 1) *
      amp[82] + Complex<double> (0, 1) * amp[83] + Complex<double> (0, 1) *
      amp[84] + Complex<double> (0, 1) * amp[85] - Complex<double> (0, 1) *
      amp[87] - Complex<double> (0, 1) * amp[88] + Complex<double> (0, 1) *
      amp[89] - Complex<double> (0, 1) * amp[90] - Complex<double> (0, 1) *
      amp[91] - Complex<double> (0, 1) * amp[92] + Complex<double> (0, 1) *
      amp[93] - Complex<double> (0, 1) * amp[94] + Complex<double> (0, 1) *
      amp[96] + Complex<double> (0, 1) * amp[97] + Complex<double> (0, 1) *
      amp[99] + Complex<double> (0, 1) * amp[101] + Complex<double> (0, 1) *
      amp[102] + Complex<double> (0, 1) * amp[104] - Complex<double> (0, 1) *
      amp[108] - Complex<double> (0, 1) * amp[109] + Complex<double> (0, 1) *
      amp[112] + Complex<double> (0, 1) * amp[111] - Complex<double> (0, 1) *
      amp[115] - Complex<double> (0, 1) * amp[114] + Complex<double> (0, 1) *
      amp[153] + Complex<double> (0, 1) * amp[154] + Complex<double> (0, 1) *
      amp[156] + Complex<double> (0, 1) * amp[158] - Complex<double> (0, 1) *
      amp[163] - Complex<double> (0, 1) * amp[164] + Complex<double> (0, 1) *
      amp[165] - Complex<double> (0, 1) * amp[166] + Complex<double> (0, 1) *
      amp[167] + Complex<double> (0, 1) * amp[168] + Complex<double> (0, 1) *
      amp[170] - Complex<double> (0, 1) * amp[172] + Complex<double> (0, 1) *
      amp[174] - Complex<double> (0, 1) * amp[175] + Complex<double> (0, 1) *
      amp[176] + Complex<double> (0, 1) * amp[177] + Complex<double> (0, 1) *
      amp[178] + Complex<double> (0, 1) * amp[179] - Complex<double> (0, 1) *
      amp[181] + Complex<double> (0, 1) * amp[183] + Complex<double> (0, 1) *
      amp[182] + Complex<double> (0, 1) * amp[185] - Complex<double> (0, 1) *
      amp[187] - Complex<double> (0, 1) * amp[188] + Complex<double> (0, 1) *
      amp[190] + Complex<double> (0, 1) * amp[191] + Complex<double> (0, 1) *
      amp[193] - Complex<double> (0, 1) * amp[222] - Complex<double> (0, 1) *
      amp[223] - Complex<double> (0, 1) * amp[225] - Complex<double> (0, 1) *
      amp[227] - Complex<double> (0, 1) * amp[228] - Complex<double> (0, 1) *
      amp[230] - Complex<double> (0, 1) * amp[235] - Complex<double> (0, 1) *
      amp[236] - Complex<double> (0, 1) * amp[238] - Complex<double> (0, 1) *
      amp[239] + Complex<double> (0, 1) * amp[240] - Complex<double> (0, 1) *
      amp[242] + Complex<double> (0, 1) * amp[243] - Complex<double> (0, 1) *
      amp[245] - Complex<double> (0, 1) * amp[246] + Complex<double> (0, 1) *
      amp[247] - Complex<double> (0, 1) * amp[248] + Complex<double> (0, 1) *
      amp[260] + Complex<double> (0, 1) * amp[261] + Complex<double> (0, 1) *
      amp[262] + Complex<double> (0, 1) * amp[263] - Complex<double> (0, 1) *
      amp[295] - Complex<double> (0, 1) * amp[294] + Complex<double> (0, 1) *
      amp[298] + Complex<double> (0, 1) * amp[297] - Complex<double> (0, 1) *
      amp[301] - Complex<double> (0, 1) * amp[300] - Complex<double> (0, 1) *
      amp[304] - Complex<double> (0, 1) * amp[303] - Complex<double> (0, 1) *
      amp[306] - Complex<double> (0, 1) * amp[307] - Complex<double> (0, 1) *
      amp[310] - Complex<double> (0, 1) * amp[309] - Complex<double> (0, 1) *
      amp[319] + Complex<double> (0, 1) * amp[320] - Complex<double> (0, 1) *
      amp[322] - Complex<double> (0, 1) * amp[323] + Complex<double> (0, 1) *
      amp[324] - Complex<double> (0, 1) * amp[326] + Complex<double> (0, 1) *
      amp[327] - Complex<double> (0, 1) * amp[329] + Complex<double> (0, 1) *
      amp[330] - Complex<double> (0, 1) * amp[332] - Complex<double> (0, 1) *
      amp[335] + Complex<double> (0, 1) * amp[333] - Complex<double> (0, 1) *
      amp[337] - Complex<double> (0, 1) * amp[338] - Complex<double> (0, 1) *
      amp[340] - Complex<double> (0, 1) * amp[341] - Complex<double> (0, 1) *
      amp[343] - Complex<double> (0, 1) * amp[344] - Complex<double> (0, 1) *
      amp[347] - Complex<double> (0, 1) * amp[346] - Complex<double> (0, 1) *
      amp[362] - Complex<double> (0, 1) * amp[364] - Complex<double> (0, 1) *
      amp[363] - Complex<double> (0, 1) * amp[370] - Complex<double> (0, 1) *
      amp[371] - Complex<double> (0, 1) * amp[379] - Complex<double> (0, 1) *
      amp[378] - Complex<double> (0, 1) * amp[383] + Complex<double> (0, 1) *
      amp[381] - Complex<double> (0, 1) * amp[386] - Complex<double> (0, 1) *
      amp[385]);
  jamp[9] = +2. * (-Complex<double> (0, 1) * amp[0] + Complex<double> (0, 1) *
      amp[2] + Complex<double> (0, 1) * amp[4] - Complex<double> (0, 1) *
      amp[5] - Complex<double> (0, 1) * amp[7] - Complex<double> (0, 1) *
      amp[8] - Complex<double> (0, 1) * amp[9] - Complex<double> (0, 1) *
      amp[10] + Complex<double> (0, 1) * amp[12] + Complex<double> (0, 1) *
      amp[13] + Complex<double> (0, 1) * amp[15] + Complex<double> (0, 1) *
      amp[17] - Complex<double> (0, 1) * amp[18] - Complex<double> (0, 1) *
      amp[19] + Complex<double> (0, 1) * amp[20] - Complex<double> (0, 1) *
      amp[24] - Complex<double> (0, 1) * amp[25] - Complex<double> (0, 1) *
      amp[26] - Complex<double> (0, 1) * amp[27] + Complex<double> (0, 1) *
      amp[29] + Complex<double> (0, 1) * amp[32] - Complex<double> (0, 1) *
      amp[30] - Complex<double> (0, 1) * amp[33] + Complex<double> (0, 1) *
      amp[35] + Complex<double> (0, 1) * amp[48] + Complex<double> (0, 1) *
      amp[49] + Complex<double> (0, 1) * amp[51] + Complex<double> (0, 1) *
      amp[53] - Complex<double> (0, 1) * amp[54] - Complex<double> (0, 1) *
      amp[55] - Complex<double> (0, 1) * amp[57] - Complex<double> (0, 1) *
      amp[64] - Complex<double> (0, 1) * amp[65] - Complex<double> (0, 1) *
      amp[67] - Complex<double> (0, 1) * amp[68] + Complex<double> (0, 1) *
      amp[76] + Complex<double> (0, 1) * amp[75] + Complex<double> (0, 1) *
      amp[80] - Complex<double> (0, 1) * amp[78] - Complex<double> (0, 1) *
      amp[81] - Complex<double> (0, 1) * amp[82] - Complex<double> (0, 1) *
      amp[84] - Complex<double> (0, 1) * amp[86] + Complex<double> (0, 1) *
      amp[91] + Complex<double> (0, 1) * amp[92] - Complex<double> (0, 1) *
      amp[93] + Complex<double> (0, 1) * amp[94] - Complex<double> (0, 1) *
      amp[95] - Complex<double> (0, 1) * amp[96] - Complex<double> (0, 1) *
      amp[98] + Complex<double> (0, 1) * amp[100] - Complex<double> (0, 1) *
      amp[102] + Complex<double> (0, 1) * amp[103] - Complex<double> (0, 1) *
      amp[104] - Complex<double> (0, 1) * amp[105] - Complex<double> (0, 1) *
      amp[106] - Complex<double> (0, 1) * amp[107] + Complex<double> (0, 1) *
      amp[109] - Complex<double> (0, 1) * amp[111] - Complex<double> (0, 1) *
      amp[110] - Complex<double> (0, 1) * amp[113] + Complex<double> (0, 1) *
      amp[115] + Complex<double> (0, 1) * amp[116] - Complex<double> (0, 1) *
      amp[189] - Complex<double> (0, 1) * amp[190] - Complex<double> (0, 1) *
      amp[192] - Complex<double> (0, 1) * amp[193] - Complex<double> (0, 1) *
      amp[199] - Complex<double> (0, 1) * amp[200] - Complex<double> (0, 1) *
      amp[202] + Complex<double> (0, 1) * amp[206] - Complex<double> (0, 1) *
      amp[204] + Complex<double> (0, 1) * amp[235] + Complex<double> (0, 1) *
      amp[236] + Complex<double> (0, 1) * amp[238] + Complex<double> (0, 1) *
      amp[239] - Complex<double> (0, 1) * amp[240] + Complex<double> (0, 1) *
      amp[242] - Complex<double> (0, 1) * amp[243] + Complex<double> (0, 1) *
      amp[245] + Complex<double> (0, 1) * amp[246] - Complex<double> (0, 1) *
      amp[247] + Complex<double> (0, 1) * amp[248] - Complex<double> (0, 1) *
      amp[249] - Complex<double> (0, 1) * amp[250] - Complex<double> (0, 1) *
      amp[252] + Complex<double> (0, 1) * amp[259] - Complex<double> (0, 1) *
      amp[261] - Complex<double> (0, 1) * amp[263] - Complex<double> (0, 1) *
      amp[264] - Complex<double> (0, 1) * amp[266] - Complex<double> (0, 1) *
      amp[267] + Complex<double> (0, 1) * amp[269] + Complex<double> (0, 1) *
      amp[271] + Complex<double> (0, 1) * amp[272] - Complex<double> (0, 1) *
      amp[274] - Complex<double> (0, 1) * amp[275] + Complex<double> (0, 1) *
      amp[277] + Complex<double> (0, 1) * amp[278] + Complex<double> (0, 1) *
      amp[281] + Complex<double> (0, 1) * amp[280] + Complex<double> (0, 1) *
      amp[294] - Complex<double> (0, 1) * amp[296] - Complex<double> (0, 1) *
      amp[297] + Complex<double> (0, 1) * amp[299] + Complex<double> (0, 1) *
      amp[300] - Complex<double> (0, 1) * amp[302] - Complex<double> (0, 1) *
      amp[305] + Complex<double> (0, 1) * amp[303] + Complex<double> (0, 1) *
      amp[307] - Complex<double> (0, 1) * amp[308] + Complex<double> (0, 1) *
      amp[310] + Complex<double> (0, 1) * amp[311] - Complex<double> (0, 1) *
      amp[324] + Complex<double> (0, 1) * amp[326] - Complex<double> (0, 1) *
      amp[327] + Complex<double> (0, 1) * amp[329] - Complex<double> (0, 1) *
      amp[330] + Complex<double> (0, 1) * amp[332] + Complex<double> (0, 1) *
      amp[335] - Complex<double> (0, 1) * amp[333] + Complex<double> (0, 1) *
      amp[362] + Complex<double> (0, 1) * amp[370] + Complex<double> (0, 1) *
      amp[369] + Complex<double> (0, 1) * amp[374] + Complex<double> (0, 1) *
      amp[373] - Complex<double> (0, 1) * amp[380] + Complex<double> (0, 1) *
      amp[378] + Complex<double> (0, 1) * amp[383] - Complex<double> (0, 1) *
      amp[381]);
  jamp[10] = +2. * (-Complex<double> (0, 1) * amp[42] - Complex<double> (0, 1)
      * amp[43] - Complex<double> (0, 1) * amp[45] - Complex<double> (0, 1) *
      amp[47] - Complex<double> (0, 1) * amp[49] - Complex<double> (0, 1) *
      amp[50] - Complex<double> (0, 1) * amp[51] - Complex<double> (0, 1) *
      amp[52] + Complex<double> (0, 1) * amp[60] + Complex<double> (0, 1) *
      amp[61] + Complex<double> (0, 1) * amp[63] - Complex<double> (0, 1) *
      amp[73] - Complex<double> (0, 1) * amp[72] - Complex<double> (0, 1) *
      amp[77] - Complex<double> (0, 1) * amp[76] + Complex<double> (0, 1) *
      amp[82] + Complex<double> (0, 1) * amp[83] + Complex<double> (0, 1) *
      amp[84] + Complex<double> (0, 1) * amp[85] - Complex<double> (0, 1) *
      amp[87] - Complex<double> (0, 1) * amp[88] + Complex<double> (0, 1) *
      amp[89] - Complex<double> (0, 1) * amp[90] - Complex<double> (0, 1) *
      amp[91] - Complex<double> (0, 1) * amp[92] + Complex<double> (0, 1) *
      amp[93] - Complex<double> (0, 1) * amp[94] + Complex<double> (0, 1) *
      amp[96] + Complex<double> (0, 1) * amp[97] + Complex<double> (0, 1) *
      amp[99] + Complex<double> (0, 1) * amp[101] + Complex<double> (0, 1) *
      amp[102] + Complex<double> (0, 1) * amp[104] - Complex<double> (0, 1) *
      amp[108] - Complex<double> (0, 1) * amp[109] + Complex<double> (0, 1) *
      amp[112] + Complex<double> (0, 1) * amp[111] - Complex<double> (0, 1) *
      amp[115] - Complex<double> (0, 1) * amp[114] + Complex<double> (0, 1) *
      amp[117] + Complex<double> (0, 1) * amp[118] + Complex<double> (0, 1) *
      amp[120] + Complex<double> (0, 1) * amp[122] - Complex<double> (0, 1) *
      amp[127] - Complex<double> (0, 1) * amp[128] + Complex<double> (0, 1) *
      amp[129] - Complex<double> (0, 1) * amp[130] + Complex<double> (0, 1) *
      amp[131] + Complex<double> (0, 1) * amp[132] + Complex<double> (0, 1) *
      amp[134] - Complex<double> (0, 1) * amp[136] + Complex<double> (0, 1) *
      amp[138] - Complex<double> (0, 1) * amp[139] + Complex<double> (0, 1) *
      amp[140] + Complex<double> (0, 1) * amp[141] + Complex<double> (0, 1) *
      amp[142] + Complex<double> (0, 1) * amp[143] - Complex<double> (0, 1) *
      amp[145] + Complex<double> (0, 1) * amp[147] + Complex<double> (0, 1) *
      amp[146] + Complex<double> (0, 1) * amp[149] - Complex<double> (0, 1) *
      amp[151] - Complex<double> (0, 1) * amp[152] + Complex<double> (0, 1) *
      amp[195] + Complex<double> (0, 1) * amp[196] + Complex<double> (0, 1) *
      amp[198] - Complex<double> (0, 1) * amp[221] - Complex<double> (0, 1) *
      amp[222] - Complex<double> (0, 1) * amp[224] - Complex<double> (0, 1) *
      amp[225] + Complex<double> (0, 1) * amp[226] - Complex<double> (0, 1) *
      amp[228] + Complex<double> (0, 1) * amp[229] - Complex<double> (0, 1) *
      amp[231] - Complex<double> (0, 1) * amp[232] + Complex<double> (0, 1) *
      amp[233] - Complex<double> (0, 1) * amp[234] - Complex<double> (0, 1) *
      amp[236] - Complex<double> (0, 1) * amp[237] - Complex<double> (0, 1) *
      amp[239] - Complex<double> (0, 1) * amp[241] - Complex<double> (0, 1) *
      amp[242] - Complex<double> (0, 1) * amp[244] + Complex<double> (0, 1) *
      amp[255] + Complex<double> (0, 1) * amp[256] + Complex<double> (0, 1) *
      amp[257] + Complex<double> (0, 1) * amp[258] - Complex<double> (0, 1) *
      amp[283] - Complex<double> (0, 1) * amp[282] + Complex<double> (0, 1) *
      amp[286] + Complex<double> (0, 1) * amp[285] - Complex<double> (0, 1) *
      amp[289] - Complex<double> (0, 1) * amp[288] - Complex<double> (0, 1) *
      amp[292] - Complex<double> (0, 1) * amp[291] - Complex<double> (0, 1) *
      amp[306] - Complex<double> (0, 1) * amp[307] - Complex<double> (0, 1) *
      amp[310] - Complex<double> (0, 1) * amp[309] - Complex<double> (0, 1) *
      amp[313] + Complex<double> (0, 1) * amp[314] - Complex<double> (0, 1) *
      amp[316] - Complex<double> (0, 1) * amp[317] - Complex<double> (0, 1) *
      amp[325] - Complex<double> (0, 1) * amp[326] - Complex<double> (0, 1) *
      amp[328] - Complex<double> (0, 1) * amp[329] - Complex<double> (0, 1) *
      amp[331] - Complex<double> (0, 1) * amp[332] - Complex<double> (0, 1) *
      amp[335] - Complex<double> (0, 1) * amp[334] + Complex<double> (0, 1) *
      amp[336] - Complex<double> (0, 1) * amp[338] + Complex<double> (0, 1) *
      amp[339] - Complex<double> (0, 1) * amp[341] + Complex<double> (0, 1) *
      amp[342] - Complex<double> (0, 1) * amp[344] - Complex<double> (0, 1) *
      amp[347] + Complex<double> (0, 1) * amp[345] - Complex<double> (0, 1) *
      amp[361] - Complex<double> (0, 1) * amp[367] - Complex<double> (0, 1) *
      amp[366] - Complex<double> (0, 1) * amp[370] - Complex<double> (0, 1) *
      amp[371] - Complex<double> (0, 1) * amp[376] - Complex<double> (0, 1) *
      amp[375] - Complex<double> (0, 1) * amp[383] - Complex<double> (0, 1) *
      amp[382] - Complex<double> (0, 1) * amp[386] + Complex<double> (0, 1) *
      amp[384]);
  jamp[11] = +2. * (+Complex<double> (0, 1) * amp[0] + Complex<double> (0, 1) *
      amp[1] + Complex<double> (0, 1) * amp[3] + Complex<double> (0, 1) *
      amp[5] + Complex<double> (0, 1) * amp[6] + Complex<double> (0, 1) *
      amp[7] + Complex<double> (0, 1) * amp[9] + Complex<double> (0, 1) *
      amp[11] - Complex<double> (0, 1) * amp[13] - Complex<double> (0, 1) *
      amp[14] - Complex<double> (0, 1) * amp[15] - Complex<double> (0, 1) *
      amp[16] - Complex<double> (0, 1) * amp[21] - Complex<double> (0, 1) *
      amp[22] + Complex<double> (0, 1) * amp[23] + Complex<double> (0, 1) *
      amp[24] + Complex<double> (0, 1) * amp[25] + Complex<double> (0, 1) *
      amp[26] - Complex<double> (0, 1) * amp[28] - Complex<double> (0, 1) *
      amp[29] + Complex<double> (0, 1) * amp[31] + Complex<double> (0, 1) *
      amp[30] - Complex<double> (0, 1) * amp[34] - Complex<double> (0, 1) *
      amp[35] + Complex<double> (0, 1) * amp[42] + Complex<double> (0, 1) *
      amp[43] + Complex<double> (0, 1) * amp[45] + Complex<double> (0, 1) *
      amp[47] - Complex<double> (0, 1) * amp[59] - Complex<double> (0, 1) *
      amp[60] - Complex<double> (0, 1) * amp[62] + Complex<double> (0, 1) *
      amp[64] + Complex<double> (0, 1) * amp[65] + Complex<double> (0, 1) *
      amp[67] + Complex<double> (0, 1) * amp[68] + Complex<double> (0, 1) *
      amp[77] - Complex<double> (0, 1) * amp[75] + Complex<double> (0, 1) *
      amp[79] + Complex<double> (0, 1) * amp[78] + Complex<double> (0, 1) *
      amp[81] - Complex<double> (0, 1) * amp[83] - Complex<double> (0, 1) *
      amp[85] + Complex<double> (0, 1) * amp[86] + Complex<double> (0, 1) *
      amp[87] + Complex<double> (0, 1) * amp[88] - Complex<double> (0, 1) *
      amp[89] + Complex<double> (0, 1) * amp[90] + Complex<double> (0, 1) *
      amp[95] - Complex<double> (0, 1) * amp[97] + Complex<double> (0, 1) *
      amp[98] - Complex<double> (0, 1) * amp[99] - Complex<double> (0, 1) *
      amp[100] - Complex<double> (0, 1) * amp[101] - Complex<double> (0, 1) *
      amp[103] + Complex<double> (0, 1) * amp[105] + Complex<double> (0, 1) *
      amp[106] + Complex<double> (0, 1) * amp[107] + Complex<double> (0, 1) *
      amp[108] - Complex<double> (0, 1) * amp[112] + Complex<double> (0, 1) *
      amp[110] + Complex<double> (0, 1) * amp[113] + Complex<double> (0, 1) *
      amp[114] - Complex<double> (0, 1) * amp[116] - Complex<double> (0, 1) *
      amp[194] - Complex<double> (0, 1) * amp[195] - Complex<double> (0, 1) *
      amp[197] - Complex<double> (0, 1) * amp[198] + Complex<double> (0, 1) *
      amp[199] + Complex<double> (0, 1) * amp[200] + Complex<double> (0, 1) *
      amp[202] + Complex<double> (0, 1) * amp[205] + Complex<double> (0, 1) *
      amp[204] + Complex<double> (0, 1) * amp[221] + Complex<double> (0, 1) *
      amp[222] + Complex<double> (0, 1) * amp[224] + Complex<double> (0, 1) *
      amp[225] - Complex<double> (0, 1) * amp[226] + Complex<double> (0, 1) *
      amp[228] - Complex<double> (0, 1) * amp[229] + Complex<double> (0, 1) *
      amp[231] + Complex<double> (0, 1) * amp[232] - Complex<double> (0, 1) *
      amp[233] + Complex<double> (0, 1) * amp[234] + Complex<double> (0, 1) *
      amp[249] + Complex<double> (0, 1) * amp[250] + Complex<double> (0, 1) *
      amp[252] + Complex<double> (0, 1) * amp[254] - Complex<double> (0, 1) *
      amp[256] - Complex<double> (0, 1) * amp[258] - Complex<double> (0, 1) *
      amp[265] + Complex<double> (0, 1) * amp[266] - Complex<double> (0, 1) *
      amp[268] - Complex<double> (0, 1) * amp[269] - Complex<double> (0, 1) *
      amp[271] - Complex<double> (0, 1) * amp[272] + Complex<double> (0, 1) *
      amp[274] + Complex<double> (0, 1) * amp[275] - Complex<double> (0, 1) *
      amp[277] - Complex<double> (0, 1) * amp[278] - Complex<double> (0, 1) *
      amp[281] - Complex<double> (0, 1) * amp[280] + Complex<double> (0, 1) *
      amp[282] - Complex<double> (0, 1) * amp[284] - Complex<double> (0, 1) *
      amp[285] + Complex<double> (0, 1) * amp[287] + Complex<double> (0, 1) *
      amp[288] - Complex<double> (0, 1) * amp[290] - Complex<double> (0, 1) *
      amp[293] + Complex<double> (0, 1) * amp[291] + Complex<double> (0, 1) *
      amp[306] + Complex<double> (0, 1) * amp[308] + Complex<double> (0, 1) *
      amp[309] - Complex<double> (0, 1) * amp[311] - Complex<double> (0, 1) *
      amp[336] + Complex<double> (0, 1) * amp[338] - Complex<double> (0, 1) *
      amp[339] + Complex<double> (0, 1) * amp[341] - Complex<double> (0, 1) *
      amp[342] + Complex<double> (0, 1) * amp[344] + Complex<double> (0, 1) *
      amp[347] - Complex<double> (0, 1) * amp[345] + Complex<double> (0, 1) *
      amp[361] - Complex<double> (0, 1) * amp[369] + Complex<double> (0, 1) *
      amp[371] - Complex<double> (0, 1) * amp[374] - Complex<double> (0, 1) *
      amp[373] - Complex<double> (0, 1) * amp[377] + Complex<double> (0, 1) *
      amp[375] + Complex<double> (0, 1) * amp[386] - Complex<double> (0, 1) *
      amp[384]);
  jamp[12] = +2. * (+Complex<double> (0, 1) * amp[37] + Complex<double> (0, 1)
      * amp[38] + Complex<double> (0, 1) * amp[39] + Complex<double> (0, 1) *
      amp[40] - Complex<double> (0, 1) * amp[42] + Complex<double> (0, 1) *
      amp[44] + Complex<double> (0, 1) * amp[46] - Complex<double> (0, 1) *
      amp[47] + Complex<double> (0, 1) * amp[60] + Complex<double> (0, 1) *
      amp[61] + Complex<double> (0, 1) * amp[63] + Complex<double> (0, 1) *
      amp[71] + Complex<double> (0, 1) * amp[70] + Complex<double> (0, 1) *
      amp[74] - Complex<double> (0, 1) * amp[72] + Complex<double> (0, 1) *
      amp[117] - Complex<double> (0, 1) * amp[119] - Complex<double> (0, 1) *
      amp[121] + Complex<double> (0, 1) * amp[122] + Complex<double> (0, 1) *
      amp[123] + Complex<double> (0, 1) * amp[124] - Complex<double> (0, 1) *
      amp[125] + Complex<double> (0, 1) * amp[126] + Complex<double> (0, 1) *
      amp[131] - Complex<double> (0, 1) * amp[133] + Complex<double> (0, 1) *
      amp[134] - Complex<double> (0, 1) * amp[135] - Complex<double> (0, 1) *
      amp[136] - Complex<double> (0, 1) * amp[137] - Complex<double> (0, 1) *
      amp[139] + Complex<double> (0, 1) * amp[141] + Complex<double> (0, 1) *
      amp[142] + Complex<double> (0, 1) * amp[143] + Complex<double> (0, 1) *
      amp[144] - Complex<double> (0, 1) * amp[148] + Complex<double> (0, 1) *
      amp[146] + Complex<double> (0, 1) * amp[149] + Complex<double> (0, 1) *
      amp[150] - Complex<double> (0, 1) * amp[152] - Complex<double> (0, 1) *
      amp[154] - Complex<double> (0, 1) * amp[155] - Complex<double> (0, 1) *
      amp[156] - Complex<double> (0, 1) * amp[157] + Complex<double> (0, 1) *
      amp[159] + Complex<double> (0, 1) * amp[160] - Complex<double> (0, 1) *
      amp[161] + Complex<double> (0, 1) * amp[162] + Complex<double> (0, 1) *
      amp[163] + Complex<double> (0, 1) * amp[164] - Complex<double> (0, 1) *
      amp[165] + Complex<double> (0, 1) * amp[166] - Complex<double> (0, 1) *
      amp[168] - Complex<double> (0, 1) * amp[169] - Complex<double> (0, 1) *
      amp[171] - Complex<double> (0, 1) * amp[173] - Complex<double> (0, 1) *
      amp[174] - Complex<double> (0, 1) * amp[176] + Complex<double> (0, 1) *
      amp[180] + Complex<double> (0, 1) * amp[181] - Complex<double> (0, 1) *
      amp[184] - Complex<double> (0, 1) * amp[183] + Complex<double> (0, 1) *
      amp[187] + Complex<double> (0, 1) * amp[186] + Complex<double> (0, 1) *
      amp[195] + Complex<double> (0, 1) * amp[196] + Complex<double> (0, 1) *
      amp[198] + Complex<double> (0, 1) * amp[208] + Complex<double> (0, 1) *
      amp[209] + Complex<double> (0, 1) * amp[211] + Complex<double> (0, 1) *
      amp[213] + Complex<double> (0, 1) * amp[214] + Complex<double> (0, 1) *
      amp[216] - Complex<double> (0, 1) * amp[221] + Complex<double> (0, 1) *
      amp[223] - Complex<double> (0, 1) * amp[224] + Complex<double> (0, 1) *
      amp[226] + Complex<double> (0, 1) * amp[227] + Complex<double> (0, 1) *
      amp[229] + Complex<double> (0, 1) * amp[230] - Complex<double> (0, 1) *
      amp[231] - Complex<double> (0, 1) * amp[232] + Complex<double> (0, 1) *
      amp[233] - Complex<double> (0, 1) * amp[234] + Complex<double> (0, 1) *
      amp[255] + Complex<double> (0, 1) * amp[256] + Complex<double> (0, 1) *
      amp[257] + Complex<double> (0, 1) * amp[258] - Complex<double> (0, 1) *
      amp[283] - Complex<double> (0, 1) * amp[282] + Complex<double> (0, 1) *
      amp[286] + Complex<double> (0, 1) * amp[285] - Complex<double> (0, 1) *
      amp[289] - Complex<double> (0, 1) * amp[288] - Complex<double> (0, 1) *
      amp[292] - Complex<double> (0, 1) * amp[291] + Complex<double> (0, 1) *
      amp[312] + Complex<double> (0, 1) * amp[314] + Complex<double> (0, 1) *
      amp[315] - Complex<double> (0, 1) * amp[317] + Complex<double> (0, 1) *
      amp[318] + Complex<double> (0, 1) * amp[319] + Complex<double> (0, 1) *
      amp[322] + Complex<double> (0, 1) * amp[321] + Complex<double> (0, 1) *
      amp[337] + Complex<double> (0, 1) * amp[336] + Complex<double> (0, 1) *
      amp[340] + Complex<double> (0, 1) * amp[339] + Complex<double> (0, 1) *
      amp[343] + Complex<double> (0, 1) * amp[342] + Complex<double> (0, 1) *
      amp[346] + Complex<double> (0, 1) * amp[345] + Complex<double> (0, 1) *
      amp[349] + Complex<double> (0, 1) * amp[350] + Complex<double> (0, 1) *
      amp[352] + Complex<double> (0, 1) * amp[353] + Complex<double> (0, 1) *
      amp[355] + Complex<double> (0, 1) * amp[356] + Complex<double> (0, 1) *
      amp[359] + Complex<double> (0, 1) * amp[358] - Complex<double> (0, 1) *
      amp[361] + Complex<double> (0, 1) * amp[364] + Complex<double> (0, 1) *
      amp[365] - Complex<double> (0, 1) * amp[366] + Complex<double> (0, 1) *
      amp[368] - Complex<double> (0, 1) * amp[376] - Complex<double> (0, 1) *
      amp[375] + Complex<double> (0, 1) * amp[385] + Complex<double> (0, 1) *
      amp[384] + Complex<double> (0, 1) * amp[389] + Complex<double> (0, 1) *
      amp[388]);
  jamp[13] = +2. * (+Complex<double> (0, 1) * amp[42] + Complex<double> (0, 1)
      * amp[43] + Complex<double> (0, 1) * amp[45] + Complex<double> (0, 1) *
      amp[47] + Complex<double> (0, 1) * amp[49] + Complex<double> (0, 1) *
      amp[50] + Complex<double> (0, 1) * amp[51] + Complex<double> (0, 1) *
      amp[52] - Complex<double> (0, 1) * amp[60] - Complex<double> (0, 1) *
      amp[61] - Complex<double> (0, 1) * amp[63] + Complex<double> (0, 1) *
      amp[73] + Complex<double> (0, 1) * amp[72] + Complex<double> (0, 1) *
      amp[77] + Complex<double> (0, 1) * amp[76] - Complex<double> (0, 1) *
      amp[82] - Complex<double> (0, 1) * amp[83] - Complex<double> (0, 1) *
      amp[84] - Complex<double> (0, 1) * amp[85] + Complex<double> (0, 1) *
      amp[87] + Complex<double> (0, 1) * amp[88] - Complex<double> (0, 1) *
      amp[89] + Complex<double> (0, 1) * amp[90] + Complex<double> (0, 1) *
      amp[91] + Complex<double> (0, 1) * amp[92] - Complex<double> (0, 1) *
      amp[93] + Complex<double> (0, 1) * amp[94] - Complex<double> (0, 1) *
      amp[96] - Complex<double> (0, 1) * amp[97] - Complex<double> (0, 1) *
      amp[99] - Complex<double> (0, 1) * amp[101] - Complex<double> (0, 1) *
      amp[102] - Complex<double> (0, 1) * amp[104] + Complex<double> (0, 1) *
      amp[108] + Complex<double> (0, 1) * amp[109] - Complex<double> (0, 1) *
      amp[112] - Complex<double> (0, 1) * amp[111] + Complex<double> (0, 1) *
      amp[115] + Complex<double> (0, 1) * amp[114] - Complex<double> (0, 1) *
      amp[117] - Complex<double> (0, 1) * amp[118] - Complex<double> (0, 1) *
      amp[120] - Complex<double> (0, 1) * amp[122] + Complex<double> (0, 1) *
      amp[127] + Complex<double> (0, 1) * amp[128] - Complex<double> (0, 1) *
      amp[129] + Complex<double> (0, 1) * amp[130] - Complex<double> (0, 1) *
      amp[131] - Complex<double> (0, 1) * amp[132] - Complex<double> (0, 1) *
      amp[134] + Complex<double> (0, 1) * amp[136] - Complex<double> (0, 1) *
      amp[138] + Complex<double> (0, 1) * amp[139] - Complex<double> (0, 1) *
      amp[140] - Complex<double> (0, 1) * amp[141] - Complex<double> (0, 1) *
      amp[142] - Complex<double> (0, 1) * amp[143] + Complex<double> (0, 1) *
      amp[145] - Complex<double> (0, 1) * amp[147] - Complex<double> (0, 1) *
      amp[146] - Complex<double> (0, 1) * amp[149] + Complex<double> (0, 1) *
      amp[151] + Complex<double> (0, 1) * amp[152] - Complex<double> (0, 1) *
      amp[195] - Complex<double> (0, 1) * amp[196] - Complex<double> (0, 1) *
      amp[198] + Complex<double> (0, 1) * amp[221] + Complex<double> (0, 1) *
      amp[222] + Complex<double> (0, 1) * amp[224] + Complex<double> (0, 1) *
      amp[225] - Complex<double> (0, 1) * amp[226] + Complex<double> (0, 1) *
      amp[228] - Complex<double> (0, 1) * amp[229] + Complex<double> (0, 1) *
      amp[231] + Complex<double> (0, 1) * amp[232] - Complex<double> (0, 1) *
      amp[233] + Complex<double> (0, 1) * amp[234] + Complex<double> (0, 1) *
      amp[236] + Complex<double> (0, 1) * amp[237] + Complex<double> (0, 1) *
      amp[239] + Complex<double> (0, 1) * amp[241] + Complex<double> (0, 1) *
      amp[242] + Complex<double> (0, 1) * amp[244] - Complex<double> (0, 1) *
      amp[255] - Complex<double> (0, 1) * amp[256] - Complex<double> (0, 1) *
      amp[257] - Complex<double> (0, 1) * amp[258] + Complex<double> (0, 1) *
      amp[283] + Complex<double> (0, 1) * amp[282] - Complex<double> (0, 1) *
      amp[286] - Complex<double> (0, 1) * amp[285] + Complex<double> (0, 1) *
      amp[289] + Complex<double> (0, 1) * amp[288] + Complex<double> (0, 1) *
      amp[292] + Complex<double> (0, 1) * amp[291] + Complex<double> (0, 1) *
      amp[306] + Complex<double> (0, 1) * amp[307] + Complex<double> (0, 1) *
      amp[310] + Complex<double> (0, 1) * amp[309] + Complex<double> (0, 1) *
      amp[313] - Complex<double> (0, 1) * amp[314] + Complex<double> (0, 1) *
      amp[316] + Complex<double> (0, 1) * amp[317] + Complex<double> (0, 1) *
      amp[325] + Complex<double> (0, 1) * amp[326] + Complex<double> (0, 1) *
      amp[328] + Complex<double> (0, 1) * amp[329] + Complex<double> (0, 1) *
      amp[331] + Complex<double> (0, 1) * amp[332] + Complex<double> (0, 1) *
      amp[335] + Complex<double> (0, 1) * amp[334] - Complex<double> (0, 1) *
      amp[336] + Complex<double> (0, 1) * amp[338] - Complex<double> (0, 1) *
      amp[339] + Complex<double> (0, 1) * amp[341] - Complex<double> (0, 1) *
      amp[342] + Complex<double> (0, 1) * amp[344] + Complex<double> (0, 1) *
      amp[347] - Complex<double> (0, 1) * amp[345] + Complex<double> (0, 1) *
      amp[361] + Complex<double> (0, 1) * amp[367] + Complex<double> (0, 1) *
      amp[366] + Complex<double> (0, 1) * amp[370] + Complex<double> (0, 1) *
      amp[371] + Complex<double> (0, 1) * amp[376] + Complex<double> (0, 1) *
      amp[375] + Complex<double> (0, 1) * amp[383] + Complex<double> (0, 1) *
      amp[382] + Complex<double> (0, 1) * amp[386] - Complex<double> (0, 1) *
      amp[384]);
  jamp[14] = +2. * (-Complex<double> (0, 1) * amp[37] - Complex<double> (0, 1)
      * amp[38] - Complex<double> (0, 1) * amp[39] - Complex<double> (0, 1) *
      amp[40] + Complex<double> (0, 1) * amp[48] - Complex<double> (0, 1) *
      amp[50] - Complex<double> (0, 1) * amp[52] + Complex<double> (0, 1) *
      amp[53] - Complex<double> (0, 1) * amp[55] - Complex<double> (0, 1) *
      amp[56] - Complex<double> (0, 1) * amp[58] - Complex<double> (0, 1) *
      amp[71] + Complex<double> (0, 1) * amp[69] - Complex<double> (0, 1) *
      amp[74] - Complex<double> (0, 1) * amp[73] + Complex<double> (0, 1) *
      amp[118] + Complex<double> (0, 1) * amp[119] + Complex<double> (0, 1) *
      amp[120] + Complex<double> (0, 1) * amp[121] - Complex<double> (0, 1) *
      amp[123] - Complex<double> (0, 1) * amp[124] + Complex<double> (0, 1) *
      amp[125] - Complex<double> (0, 1) * amp[126] - Complex<double> (0, 1) *
      amp[127] - Complex<double> (0, 1) * amp[128] + Complex<double> (0, 1) *
      amp[129] - Complex<double> (0, 1) * amp[130] + Complex<double> (0, 1) *
      amp[132] + Complex<double> (0, 1) * amp[133] + Complex<double> (0, 1) *
      amp[135] + Complex<double> (0, 1) * amp[137] + Complex<double> (0, 1) *
      amp[138] + Complex<double> (0, 1) * amp[140] - Complex<double> (0, 1) *
      amp[144] - Complex<double> (0, 1) * amp[145] + Complex<double> (0, 1) *
      amp[148] + Complex<double> (0, 1) * amp[147] - Complex<double> (0, 1) *
      amp[151] - Complex<double> (0, 1) * amp[150] - Complex<double> (0, 1) *
      amp[153] + Complex<double> (0, 1) * amp[155] + Complex<double> (0, 1) *
      amp[157] - Complex<double> (0, 1) * amp[158] - Complex<double> (0, 1) *
      amp[159] - Complex<double> (0, 1) * amp[160] + Complex<double> (0, 1) *
      amp[161] - Complex<double> (0, 1) * amp[162] - Complex<double> (0, 1) *
      amp[167] + Complex<double> (0, 1) * amp[169] - Complex<double> (0, 1) *
      amp[170] + Complex<double> (0, 1) * amp[171] + Complex<double> (0, 1) *
      amp[172] + Complex<double> (0, 1) * amp[173] + Complex<double> (0, 1) *
      amp[175] - Complex<double> (0, 1) * amp[177] - Complex<double> (0, 1) *
      amp[178] - Complex<double> (0, 1) * amp[179] - Complex<double> (0, 1) *
      amp[180] + Complex<double> (0, 1) * amp[184] - Complex<double> (0, 1) *
      amp[182] - Complex<double> (0, 1) * amp[185] - Complex<double> (0, 1) *
      amp[186] + Complex<double> (0, 1) * amp[188] - Complex<double> (0, 1) *
      amp[190] - Complex<double> (0, 1) * amp[191] - Complex<double> (0, 1) *
      amp[193] - Complex<double> (0, 1) * amp[208] - Complex<double> (0, 1) *
      amp[209] - Complex<double> (0, 1) * amp[211] - Complex<double> (0, 1) *
      amp[213] - Complex<double> (0, 1) * amp[214] - Complex<double> (0, 1) *
      amp[216] + Complex<double> (0, 1) * amp[235] - Complex<double> (0, 1) *
      amp[237] + Complex<double> (0, 1) * amp[238] - Complex<double> (0, 1) *
      amp[240] - Complex<double> (0, 1) * amp[241] - Complex<double> (0, 1) *
      amp[243] - Complex<double> (0, 1) * amp[244] + Complex<double> (0, 1) *
      amp[245] + Complex<double> (0, 1) * amp[246] - Complex<double> (0, 1) *
      amp[247] + Complex<double> (0, 1) * amp[248] - Complex<double> (0, 1) *
      amp[260] - Complex<double> (0, 1) * amp[261] - Complex<double> (0, 1) *
      amp[262] - Complex<double> (0, 1) * amp[263] + Complex<double> (0, 1) *
      amp[295] + Complex<double> (0, 1) * amp[294] - Complex<double> (0, 1) *
      amp[298] - Complex<double> (0, 1) * amp[297] + Complex<double> (0, 1) *
      amp[301] + Complex<double> (0, 1) * amp[300] + Complex<double> (0, 1) *
      amp[304] + Complex<double> (0, 1) * amp[303] - Complex<double> (0, 1) *
      amp[312] - Complex<double> (0, 1) * amp[313] - Complex<double> (0, 1) *
      amp[316] - Complex<double> (0, 1) * amp[315] - Complex<double> (0, 1) *
      amp[318] - Complex<double> (0, 1) * amp[320] - Complex<double> (0, 1) *
      amp[321] + Complex<double> (0, 1) * amp[323] - Complex<double> (0, 1) *
      amp[325] - Complex<double> (0, 1) * amp[324] - Complex<double> (0, 1) *
      amp[328] - Complex<double> (0, 1) * amp[327] - Complex<double> (0, 1) *
      amp[331] - Complex<double> (0, 1) * amp[330] - Complex<double> (0, 1) *
      amp[334] - Complex<double> (0, 1) * amp[333] - Complex<double> (0, 1) *
      amp[349] - Complex<double> (0, 1) * amp[350] - Complex<double> (0, 1) *
      amp[352] - Complex<double> (0, 1) * amp[353] - Complex<double> (0, 1) *
      amp[355] - Complex<double> (0, 1) * amp[356] - Complex<double> (0, 1) *
      amp[359] - Complex<double> (0, 1) * amp[358] + Complex<double> (0, 1) *
      amp[362] + Complex<double> (0, 1) * amp[363] - Complex<double> (0, 1) *
      amp[365] - Complex<double> (0, 1) * amp[367] - Complex<double> (0, 1) *
      amp[368] + Complex<double> (0, 1) * amp[379] + Complex<double> (0, 1) *
      amp[378] - Complex<double> (0, 1) * amp[382] - Complex<double> (0, 1) *
      amp[381] - Complex<double> (0, 1) * amp[389] - Complex<double> (0, 1) *
      amp[388]);
  jamp[15] = +2. * (-Complex<double> (0, 1) * amp[1] - Complex<double> (0, 1) *
      amp[2] - Complex<double> (0, 1) * amp[3] - Complex<double> (0, 1) *
      amp[4] - Complex<double> (0, 1) * amp[6] + Complex<double> (0, 1) *
      amp[8] + Complex<double> (0, 1) * amp[10] - Complex<double> (0, 1) *
      amp[11] - Complex<double> (0, 1) * amp[12] + Complex<double> (0, 1) *
      amp[14] + Complex<double> (0, 1) * amp[16] - Complex<double> (0, 1) *
      amp[17] + Complex<double> (0, 1) * amp[18] + Complex<double> (0, 1) *
      amp[19] - Complex<double> (0, 1) * amp[20] + Complex<double> (0, 1) *
      amp[21] + Complex<double> (0, 1) * amp[22] - Complex<double> (0, 1) *
      amp[23] + Complex<double> (0, 1) * amp[27] + Complex<double> (0, 1) *
      amp[28] - Complex<double> (0, 1) * amp[32] - Complex<double> (0, 1) *
      amp[31] + Complex<double> (0, 1) * amp[34] + Complex<double> (0, 1) *
      amp[33] - Complex<double> (0, 1) * amp[48] + Complex<double> (0, 1) *
      amp[50] + Complex<double> (0, 1) * amp[52] - Complex<double> (0, 1) *
      amp[53] + Complex<double> (0, 1) * amp[54] + Complex<double> (0, 1) *
      amp[55] + Complex<double> (0, 1) * amp[57] + Complex<double> (0, 1) *
      amp[59] - Complex<double> (0, 1) * amp[61] + Complex<double> (0, 1) *
      amp[62] - Complex<double> (0, 1) * amp[63] + Complex<double> (0, 1) *
      amp[73] + Complex<double> (0, 1) * amp[72] - Complex<double> (0, 1) *
      amp[80] - Complex<double> (0, 1) * amp[79] - Complex<double> (0, 1) *
      amp[117] - Complex<double> (0, 1) * amp[118] - Complex<double> (0, 1) *
      amp[120] - Complex<double> (0, 1) * amp[122] + Complex<double> (0, 1) *
      amp[127] + Complex<double> (0, 1) * amp[128] - Complex<double> (0, 1) *
      amp[129] + Complex<double> (0, 1) * amp[130] - Complex<double> (0, 1) *
      amp[131] - Complex<double> (0, 1) * amp[132] - Complex<double> (0, 1) *
      amp[134] + Complex<double> (0, 1) * amp[136] - Complex<double> (0, 1) *
      amp[138] + Complex<double> (0, 1) * amp[139] - Complex<double> (0, 1) *
      amp[140] - Complex<double> (0, 1) * amp[141] - Complex<double> (0, 1) *
      amp[142] - Complex<double> (0, 1) * amp[143] + Complex<double> (0, 1) *
      amp[145] - Complex<double> (0, 1) * amp[147] - Complex<double> (0, 1) *
      amp[146] - Complex<double> (0, 1) * amp[149] + Complex<double> (0, 1) *
      amp[151] + Complex<double> (0, 1) * amp[152] + Complex<double> (0, 1) *
      amp[189] + Complex<double> (0, 1) * amp[190] + Complex<double> (0, 1) *
      amp[192] + Complex<double> (0, 1) * amp[193] + Complex<double> (0, 1) *
      amp[194] - Complex<double> (0, 1) * amp[196] + Complex<double> (0, 1) *
      amp[197] - Complex<double> (0, 1) * amp[206] - Complex<double> (0, 1) *
      amp[205] - Complex<double> (0, 1) * amp[235] + Complex<double> (0, 1) *
      amp[237] - Complex<double> (0, 1) * amp[238] + Complex<double> (0, 1) *
      amp[240] + Complex<double> (0, 1) * amp[241] + Complex<double> (0, 1) *
      amp[243] + Complex<double> (0, 1) * amp[244] - Complex<double> (0, 1) *
      amp[245] - Complex<double> (0, 1) * amp[246] + Complex<double> (0, 1) *
      amp[247] - Complex<double> (0, 1) * amp[248] - Complex<double> (0, 1) *
      amp[254] - Complex<double> (0, 1) * amp[255] - Complex<double> (0, 1) *
      amp[257] - Complex<double> (0, 1) * amp[259] + Complex<double> (0, 1) *
      amp[261] + Complex<double> (0, 1) * amp[263] + Complex<double> (0, 1) *
      amp[264] + Complex<double> (0, 1) * amp[265] + Complex<double> (0, 1) *
      amp[268] + Complex<double> (0, 1) * amp[267] + Complex<double> (0, 1) *
      amp[283] + Complex<double> (0, 1) * amp[284] - Complex<double> (0, 1) *
      amp[286] - Complex<double> (0, 1) * amp[287] + Complex<double> (0, 1) *
      amp[289] + Complex<double> (0, 1) * amp[290] + Complex<double> (0, 1) *
      amp[293] + Complex<double> (0, 1) * amp[292] - Complex<double> (0, 1) *
      amp[294] + Complex<double> (0, 1) * amp[296] + Complex<double> (0, 1) *
      amp[297] - Complex<double> (0, 1) * amp[299] - Complex<double> (0, 1) *
      amp[300] + Complex<double> (0, 1) * amp[302] + Complex<double> (0, 1) *
      amp[305] - Complex<double> (0, 1) * amp[303] + Complex<double> (0, 1) *
      amp[313] - Complex<double> (0, 1) * amp[314] + Complex<double> (0, 1) *
      amp[316] + Complex<double> (0, 1) * amp[317] + Complex<double> (0, 1) *
      amp[325] + Complex<double> (0, 1) * amp[324] + Complex<double> (0, 1) *
      amp[328] + Complex<double> (0, 1) * amp[327] + Complex<double> (0, 1) *
      amp[331] + Complex<double> (0, 1) * amp[330] + Complex<double> (0, 1) *
      amp[334] + Complex<double> (0, 1) * amp[333] - Complex<double> (0, 1) *
      amp[362] + Complex<double> (0, 1) * amp[367] + Complex<double> (0, 1) *
      amp[366] + Complex<double> (0, 1) * amp[377] + Complex<double> (0, 1) *
      amp[376] + Complex<double> (0, 1) * amp[380] - Complex<double> (0, 1) *
      amp[378] + Complex<double> (0, 1) * amp[382] + Complex<double> (0, 1) *
      amp[381]);
  jamp[16] = +2. * (-Complex<double> (0, 1) * amp[36] - Complex<double> (0, 1)
      * amp[37] - Complex<double> (0, 1) * amp[39] - Complex<double> (0, 1) *
      amp[41] - Complex<double> (0, 1) * amp[49] - Complex<double> (0, 1) *
      amp[50] - Complex<double> (0, 1) * amp[51] - Complex<double> (0, 1) *
      amp[52] + Complex<double> (0, 1) * amp[65] + Complex<double> (0, 1) *
      amp[66] + Complex<double> (0, 1) * amp[68] - Complex<double> (0, 1) *
      amp[74] - Complex<double> (0, 1) * amp[73] - Complex<double> (0, 1) *
      amp[76] - Complex<double> (0, 1) * amp[75] + Complex<double> (0, 1) *
      amp[81] + Complex<double> (0, 1) * amp[82] + Complex<double> (0, 1) *
      amp[84] + Complex<double> (0, 1) * amp[86] - Complex<double> (0, 1) *
      amp[91] - Complex<double> (0, 1) * amp[92] + Complex<double> (0, 1) *
      amp[93] - Complex<double> (0, 1) * amp[94] + Complex<double> (0, 1) *
      amp[95] + Complex<double> (0, 1) * amp[96] + Complex<double> (0, 1) *
      amp[98] - Complex<double> (0, 1) * amp[100] + Complex<double> (0, 1) *
      amp[102] - Complex<double> (0, 1) * amp[103] + Complex<double> (0, 1) *
      amp[104] + Complex<double> (0, 1) * amp[105] + Complex<double> (0, 1) *
      amp[106] + Complex<double> (0, 1) * amp[107] - Complex<double> (0, 1) *
      amp[109] + Complex<double> (0, 1) * amp[111] + Complex<double> (0, 1) *
      amp[110] + Complex<double> (0, 1) * amp[113] - Complex<double> (0, 1) *
      amp[115] - Complex<double> (0, 1) * amp[116] + Complex<double> (0, 1) *
      amp[118] + Complex<double> (0, 1) * amp[119] + Complex<double> (0, 1) *
      amp[120] + Complex<double> (0, 1) * amp[121] - Complex<double> (0, 1) *
      amp[123] - Complex<double> (0, 1) * amp[124] + Complex<double> (0, 1) *
      amp[125] - Complex<double> (0, 1) * amp[126] - Complex<double> (0, 1) *
      amp[127] - Complex<double> (0, 1) * amp[128] + Complex<double> (0, 1) *
      amp[129] - Complex<double> (0, 1) * amp[130] + Complex<double> (0, 1) *
      amp[132] + Complex<double> (0, 1) * amp[133] + Complex<double> (0, 1) *
      amp[135] + Complex<double> (0, 1) * amp[137] + Complex<double> (0, 1) *
      amp[138] + Complex<double> (0, 1) * amp[140] - Complex<double> (0, 1) *
      amp[144] - Complex<double> (0, 1) * amp[145] + Complex<double> (0, 1) *
      amp[148] + Complex<double> (0, 1) * amp[147] - Complex<double> (0, 1) *
      amp[151] - Complex<double> (0, 1) * amp[150] + Complex<double> (0, 1) *
      amp[200] + Complex<double> (0, 1) * amp[201] + Complex<double> (0, 1) *
      amp[203] - Complex<double> (0, 1) * amp[207] - Complex<double> (0, 1) *
      amp[208] - Complex<double> (0, 1) * amp[210] - Complex<double> (0, 1) *
      amp[211] + Complex<double> (0, 1) * amp[212] - Complex<double> (0, 1) *
      amp[214] + Complex<double> (0, 1) * amp[215] - Complex<double> (0, 1) *
      amp[217] - Complex<double> (0, 1) * amp[218] + Complex<double> (0, 1) *
      amp[219] - Complex<double> (0, 1) * amp[220] - Complex<double> (0, 1) *
      amp[236] - Complex<double> (0, 1) * amp[237] - Complex<double> (0, 1) *
      amp[239] - Complex<double> (0, 1) * amp[241] - Complex<double> (0, 1) *
      amp[242] - Complex<double> (0, 1) * amp[244] + Complex<double> (0, 1) *
      amp[250] + Complex<double> (0, 1) * amp[251] + Complex<double> (0, 1) *
      amp[252] + Complex<double> (0, 1) * amp[253] - Complex<double> (0, 1) *
      amp[271] - Complex<double> (0, 1) * amp[270] + Complex<double> (0, 1) *
      amp[274] + Complex<double> (0, 1) * amp[273] - Complex<double> (0, 1) *
      amp[277] - Complex<double> (0, 1) * amp[276] - Complex<double> (0, 1) *
      amp[280] - Complex<double> (0, 1) * amp[279] - Complex<double> (0, 1) *
      amp[307] + Complex<double> (0, 1) * amp[308] - Complex<double> (0, 1) *
      amp[310] - Complex<double> (0, 1) * amp[311] - Complex<double> (0, 1) *
      amp[312] - Complex<double> (0, 1) * amp[313] - Complex<double> (0, 1) *
      amp[316] - Complex<double> (0, 1) * amp[315] - Complex<double> (0, 1) *
      amp[325] - Complex<double> (0, 1) * amp[326] - Complex<double> (0, 1) *
      amp[328] - Complex<double> (0, 1) * amp[329] - Complex<double> (0, 1) *
      amp[331] - Complex<double> (0, 1) * amp[332] - Complex<double> (0, 1) *
      amp[335] - Complex<double> (0, 1) * amp[334] + Complex<double> (0, 1) *
      amp[348] - Complex<double> (0, 1) * amp[350] + Complex<double> (0, 1) *
      amp[351] - Complex<double> (0, 1) * amp[353] + Complex<double> (0, 1) *
      amp[354] - Complex<double> (0, 1) * amp[356] - Complex<double> (0, 1) *
      amp[359] + Complex<double> (0, 1) * amp[357] - Complex<double> (0, 1) *
      amp[360] - Complex<double> (0, 1) * amp[367] - Complex<double> (0, 1) *
      amp[368] - Complex<double> (0, 1) * amp[370] - Complex<double> (0, 1) *
      amp[369] - Complex<double> (0, 1) * amp[373] - Complex<double> (0, 1) *
      amp[372] - Complex<double> (0, 1) * amp[383] - Complex<double> (0, 1) *
      amp[382] - Complex<double> (0, 1) * amp[389] + Complex<double> (0, 1) *
      amp[387]);
  jamp[17] = +2. * (+Complex<double> (0, 1) * amp[0] + Complex<double> (0, 1) *
      amp[1] + Complex<double> (0, 1) * amp[3] + Complex<double> (0, 1) *
      amp[5] + Complex<double> (0, 1) * amp[6] + Complex<double> (0, 1) *
      amp[7] + Complex<double> (0, 1) * amp[9] + Complex<double> (0, 1) *
      amp[11] - Complex<double> (0, 1) * amp[13] - Complex<double> (0, 1) *
      amp[14] - Complex<double> (0, 1) * amp[15] - Complex<double> (0, 1) *
      amp[16] - Complex<double> (0, 1) * amp[21] - Complex<double> (0, 1) *
      amp[22] + Complex<double> (0, 1) * amp[23] + Complex<double> (0, 1) *
      amp[24] + Complex<double> (0, 1) * amp[25] + Complex<double> (0, 1) *
      amp[26] - Complex<double> (0, 1) * amp[28] - Complex<double> (0, 1) *
      amp[29] + Complex<double> (0, 1) * amp[31] + Complex<double> (0, 1) *
      amp[30] - Complex<double> (0, 1) * amp[34] - Complex<double> (0, 1) *
      amp[35] + Complex<double> (0, 1) * amp[36] + Complex<double> (0, 1) *
      amp[37] + Complex<double> (0, 1) * amp[39] + Complex<double> (0, 1) *
      amp[41] - Complex<double> (0, 1) * amp[59] + Complex<double> (0, 1) *
      amp[61] - Complex<double> (0, 1) * amp[62] + Complex<double> (0, 1) *
      amp[63] + Complex<double> (0, 1) * amp[64] - Complex<double> (0, 1) *
      amp[66] + Complex<double> (0, 1) * amp[67] + Complex<double> (0, 1) *
      amp[74] - Complex<double> (0, 1) * amp[72] + Complex<double> (0, 1) *
      amp[79] + Complex<double> (0, 1) * amp[78] + Complex<double> (0, 1) *
      amp[117] - Complex<double> (0, 1) * amp[119] - Complex<double> (0, 1) *
      amp[121] + Complex<double> (0, 1) * amp[122] + Complex<double> (0, 1) *
      amp[123] + Complex<double> (0, 1) * amp[124] - Complex<double> (0, 1) *
      amp[125] + Complex<double> (0, 1) * amp[126] + Complex<double> (0, 1) *
      amp[131] - Complex<double> (0, 1) * amp[133] + Complex<double> (0, 1) *
      amp[134] - Complex<double> (0, 1) * amp[135] - Complex<double> (0, 1) *
      amp[136] - Complex<double> (0, 1) * amp[137] - Complex<double> (0, 1) *
      amp[139] + Complex<double> (0, 1) * amp[141] + Complex<double> (0, 1) *
      amp[142] + Complex<double> (0, 1) * amp[143] + Complex<double> (0, 1) *
      amp[144] - Complex<double> (0, 1) * amp[148] + Complex<double> (0, 1) *
      amp[146] + Complex<double> (0, 1) * amp[149] + Complex<double> (0, 1) *
      amp[150] - Complex<double> (0, 1) * amp[152] - Complex<double> (0, 1) *
      amp[194] + Complex<double> (0, 1) * amp[196] - Complex<double> (0, 1) *
      amp[197] + Complex<double> (0, 1) * amp[199] - Complex<double> (0, 1) *
      amp[201] + Complex<double> (0, 1) * amp[202] - Complex<double> (0, 1) *
      amp[203] + Complex<double> (0, 1) * amp[205] + Complex<double> (0, 1) *
      amp[204] + Complex<double> (0, 1) * amp[207] + Complex<double> (0, 1) *
      amp[208] + Complex<double> (0, 1) * amp[210] + Complex<double> (0, 1) *
      amp[211] - Complex<double> (0, 1) * amp[212] + Complex<double> (0, 1) *
      amp[214] - Complex<double> (0, 1) * amp[215] + Complex<double> (0, 1) *
      amp[217] + Complex<double> (0, 1) * amp[218] - Complex<double> (0, 1) *
      amp[219] + Complex<double> (0, 1) * amp[220] + Complex<double> (0, 1) *
      amp[249] - Complex<double> (0, 1) * amp[251] - Complex<double> (0, 1) *
      amp[253] + Complex<double> (0, 1) * amp[254] + Complex<double> (0, 1) *
      amp[255] + Complex<double> (0, 1) * amp[257] - Complex<double> (0, 1) *
      amp[265] + Complex<double> (0, 1) * amp[266] - Complex<double> (0, 1) *
      amp[268] - Complex<double> (0, 1) * amp[269] + Complex<double> (0, 1) *
      amp[270] - Complex<double> (0, 1) * amp[272] - Complex<double> (0, 1) *
      amp[273] + Complex<double> (0, 1) * amp[275] + Complex<double> (0, 1) *
      amp[276] - Complex<double> (0, 1) * amp[278] - Complex<double> (0, 1) *
      amp[281] + Complex<double> (0, 1) * amp[279] - Complex<double> (0, 1) *
      amp[283] - Complex<double> (0, 1) * amp[284] + Complex<double> (0, 1) *
      amp[286] + Complex<double> (0, 1) * amp[287] - Complex<double> (0, 1) *
      amp[289] - Complex<double> (0, 1) * amp[290] - Complex<double> (0, 1) *
      amp[293] - Complex<double> (0, 1) * amp[292] + Complex<double> (0, 1) *
      amp[312] + Complex<double> (0, 1) * amp[314] + Complex<double> (0, 1) *
      amp[315] - Complex<double> (0, 1) * amp[317] - Complex<double> (0, 1) *
      amp[348] + Complex<double> (0, 1) * amp[350] - Complex<double> (0, 1) *
      amp[351] + Complex<double> (0, 1) * amp[353] - Complex<double> (0, 1) *
      amp[354] + Complex<double> (0, 1) * amp[356] + Complex<double> (0, 1) *
      amp[359] - Complex<double> (0, 1) * amp[357] + Complex<double> (0, 1) *
      amp[360] - Complex<double> (0, 1) * amp[366] + Complex<double> (0, 1) *
      amp[368] - Complex<double> (0, 1) * amp[374] + Complex<double> (0, 1) *
      amp[372] - Complex<double> (0, 1) * amp[377] - Complex<double> (0, 1) *
      amp[376] + Complex<double> (0, 1) * amp[389] - Complex<double> (0, 1) *
      amp[387]);
  jamp[18] = +2. * (+Complex<double> (0, 1) * amp[37] + Complex<double> (0, 1)
      * amp[38] + Complex<double> (0, 1) * amp[39] + Complex<double> (0, 1) *
      amp[40] - Complex<double> (0, 1) * amp[48] + Complex<double> (0, 1) *
      amp[50] + Complex<double> (0, 1) * amp[52] - Complex<double> (0, 1) *
      amp[53] + Complex<double> (0, 1) * amp[55] + Complex<double> (0, 1) *
      amp[56] + Complex<double> (0, 1) * amp[58] + Complex<double> (0, 1) *
      amp[71] - Complex<double> (0, 1) * amp[69] + Complex<double> (0, 1) *
      amp[74] + Complex<double> (0, 1) * amp[73] - Complex<double> (0, 1) *
      amp[118] - Complex<double> (0, 1) * amp[119] - Complex<double> (0, 1) *
      amp[120] - Complex<double> (0, 1) * amp[121] + Complex<double> (0, 1) *
      amp[123] + Complex<double> (0, 1) * amp[124] - Complex<double> (0, 1) *
      amp[125] + Complex<double> (0, 1) * amp[126] + Complex<double> (0, 1) *
      amp[127] + Complex<double> (0, 1) * amp[128] - Complex<double> (0, 1) *
      amp[129] + Complex<double> (0, 1) * amp[130] - Complex<double> (0, 1) *
      amp[132] - Complex<double> (0, 1) * amp[133] - Complex<double> (0, 1) *
      amp[135] - Complex<double> (0, 1) * amp[137] - Complex<double> (0, 1) *
      amp[138] - Complex<double> (0, 1) * amp[140] + Complex<double> (0, 1) *
      amp[144] + Complex<double> (0, 1) * amp[145] - Complex<double> (0, 1) *
      amp[148] - Complex<double> (0, 1) * amp[147] + Complex<double> (0, 1) *
      amp[151] + Complex<double> (0, 1) * amp[150] + Complex<double> (0, 1) *
      amp[153] - Complex<double> (0, 1) * amp[155] - Complex<double> (0, 1) *
      amp[157] + Complex<double> (0, 1) * amp[158] + Complex<double> (0, 1) *
      amp[159] + Complex<double> (0, 1) * amp[160] - Complex<double> (0, 1) *
      amp[161] + Complex<double> (0, 1) * amp[162] + Complex<double> (0, 1) *
      amp[167] - Complex<double> (0, 1) * amp[169] + Complex<double> (0, 1) *
      amp[170] - Complex<double> (0, 1) * amp[171] - Complex<double> (0, 1) *
      amp[172] - Complex<double> (0, 1) * amp[173] - Complex<double> (0, 1) *
      amp[175] + Complex<double> (0, 1) * amp[177] + Complex<double> (0, 1) *
      amp[178] + Complex<double> (0, 1) * amp[179] + Complex<double> (0, 1) *
      amp[180] - Complex<double> (0, 1) * amp[184] + Complex<double> (0, 1) *
      amp[182] + Complex<double> (0, 1) * amp[185] + Complex<double> (0, 1) *
      amp[186] - Complex<double> (0, 1) * amp[188] + Complex<double> (0, 1) *
      amp[190] + Complex<double> (0, 1) * amp[191] + Complex<double> (0, 1) *
      amp[193] + Complex<double> (0, 1) * amp[208] + Complex<double> (0, 1) *
      amp[209] + Complex<double> (0, 1) * amp[211] + Complex<double> (0, 1) *
      amp[213] + Complex<double> (0, 1) * amp[214] + Complex<double> (0, 1) *
      amp[216] - Complex<double> (0, 1) * amp[235] + Complex<double> (0, 1) *
      amp[237] - Complex<double> (0, 1) * amp[238] + Complex<double> (0, 1) *
      amp[240] + Complex<double> (0, 1) * amp[241] + Complex<double> (0, 1) *
      amp[243] + Complex<double> (0, 1) * amp[244] - Complex<double> (0, 1) *
      amp[245] - Complex<double> (0, 1) * amp[246] + Complex<double> (0, 1) *
      amp[247] - Complex<double> (0, 1) * amp[248] + Complex<double> (0, 1) *
      amp[260] + Complex<double> (0, 1) * amp[261] + Complex<double> (0, 1) *
      amp[262] + Complex<double> (0, 1) * amp[263] - Complex<double> (0, 1) *
      amp[295] - Complex<double> (0, 1) * amp[294] + Complex<double> (0, 1) *
      amp[298] + Complex<double> (0, 1) * amp[297] - Complex<double> (0, 1) *
      amp[301] - Complex<double> (0, 1) * amp[300] - Complex<double> (0, 1) *
      amp[304] - Complex<double> (0, 1) * amp[303] + Complex<double> (0, 1) *
      amp[312] + Complex<double> (0, 1) * amp[313] + Complex<double> (0, 1) *
      amp[316] + Complex<double> (0, 1) * amp[315] + Complex<double> (0, 1) *
      amp[318] + Complex<double> (0, 1) * amp[320] + Complex<double> (0, 1) *
      amp[321] - Complex<double> (0, 1) * amp[323] + Complex<double> (0, 1) *
      amp[325] + Complex<double> (0, 1) * amp[324] + Complex<double> (0, 1) *
      amp[328] + Complex<double> (0, 1) * amp[327] + Complex<double> (0, 1) *
      amp[331] + Complex<double> (0, 1) * amp[330] + Complex<double> (0, 1) *
      amp[334] + Complex<double> (0, 1) * amp[333] + Complex<double> (0, 1) *
      amp[349] + Complex<double> (0, 1) * amp[350] + Complex<double> (0, 1) *
      amp[352] + Complex<double> (0, 1) * amp[353] + Complex<double> (0, 1) *
      amp[355] + Complex<double> (0, 1) * amp[356] + Complex<double> (0, 1) *
      amp[359] + Complex<double> (0, 1) * amp[358] - Complex<double> (0, 1) *
      amp[362] - Complex<double> (0, 1) * amp[363] + Complex<double> (0, 1) *
      amp[365] + Complex<double> (0, 1) * amp[367] + Complex<double> (0, 1) *
      amp[368] - Complex<double> (0, 1) * amp[379] - Complex<double> (0, 1) *
      amp[378] + Complex<double> (0, 1) * amp[382] + Complex<double> (0, 1) *
      amp[381] + Complex<double> (0, 1) * amp[389] + Complex<double> (0, 1) *
      amp[388]);
  jamp[19] = +2. * (+Complex<double> (0, 1) * amp[43] + Complex<double> (0, 1)
      * amp[44] + Complex<double> (0, 1) * amp[45] + Complex<double> (0, 1) *
      amp[46] + Complex<double> (0, 1) * amp[48] + Complex<double> (0, 1) *
      amp[49] + Complex<double> (0, 1) * amp[51] + Complex<double> (0, 1) *
      amp[53] - Complex<double> (0, 1) * amp[55] - Complex<double> (0, 1) *
      amp[56] - Complex<double> (0, 1) * amp[58] + Complex<double> (0, 1) *
      amp[70] + Complex<double> (0, 1) * amp[69] + Complex<double> (0, 1) *
      amp[77] + Complex<double> (0, 1) * amp[76] - Complex<double> (0, 1) *
      amp[82] - Complex<double> (0, 1) * amp[83] - Complex<double> (0, 1) *
      amp[84] - Complex<double> (0, 1) * amp[85] + Complex<double> (0, 1) *
      amp[87] + Complex<double> (0, 1) * amp[88] - Complex<double> (0, 1) *
      amp[89] + Complex<double> (0, 1) * amp[90] + Complex<double> (0, 1) *
      amp[91] + Complex<double> (0, 1) * amp[92] - Complex<double> (0, 1) *
      amp[93] + Complex<double> (0, 1) * amp[94] - Complex<double> (0, 1) *
      amp[96] - Complex<double> (0, 1) * amp[97] - Complex<double> (0, 1) *
      amp[99] - Complex<double> (0, 1) * amp[101] - Complex<double> (0, 1) *
      amp[102] - Complex<double> (0, 1) * amp[104] + Complex<double> (0, 1) *
      amp[108] + Complex<double> (0, 1) * amp[109] - Complex<double> (0, 1) *
      amp[112] - Complex<double> (0, 1) * amp[111] + Complex<double> (0, 1) *
      amp[115] + Complex<double> (0, 1) * amp[114] - Complex<double> (0, 1) *
      amp[153] - Complex<double> (0, 1) * amp[154] - Complex<double> (0, 1) *
      amp[156] - Complex<double> (0, 1) * amp[158] + Complex<double> (0, 1) *
      amp[163] + Complex<double> (0, 1) * amp[164] - Complex<double> (0, 1) *
      amp[165] + Complex<double> (0, 1) * amp[166] - Complex<double> (0, 1) *
      amp[167] - Complex<double> (0, 1) * amp[168] - Complex<double> (0, 1) *
      amp[170] + Complex<double> (0, 1) * amp[172] - Complex<double> (0, 1) *
      amp[174] + Complex<double> (0, 1) * amp[175] - Complex<double> (0, 1) *
      amp[176] - Complex<double> (0, 1) * amp[177] - Complex<double> (0, 1) *
      amp[178] - Complex<double> (0, 1) * amp[179] + Complex<double> (0, 1) *
      amp[181] - Complex<double> (0, 1) * amp[183] - Complex<double> (0, 1) *
      amp[182] - Complex<double> (0, 1) * amp[185] + Complex<double> (0, 1) *
      amp[187] + Complex<double> (0, 1) * amp[188] - Complex<double> (0, 1) *
      amp[190] - Complex<double> (0, 1) * amp[191] - Complex<double> (0, 1) *
      amp[193] + Complex<double> (0, 1) * amp[222] + Complex<double> (0, 1) *
      amp[223] + Complex<double> (0, 1) * amp[225] + Complex<double> (0, 1) *
      amp[227] + Complex<double> (0, 1) * amp[228] + Complex<double> (0, 1) *
      amp[230] + Complex<double> (0, 1) * amp[235] + Complex<double> (0, 1) *
      amp[236] + Complex<double> (0, 1) * amp[238] + Complex<double> (0, 1) *
      amp[239] - Complex<double> (0, 1) * amp[240] + Complex<double> (0, 1) *
      amp[242] - Complex<double> (0, 1) * amp[243] + Complex<double> (0, 1) *
      amp[245] + Complex<double> (0, 1) * amp[246] - Complex<double> (0, 1) *
      amp[247] + Complex<double> (0, 1) * amp[248] - Complex<double> (0, 1) *
      amp[260] - Complex<double> (0, 1) * amp[261] - Complex<double> (0, 1) *
      amp[262] - Complex<double> (0, 1) * amp[263] + Complex<double> (0, 1) *
      amp[295] + Complex<double> (0, 1) * amp[294] - Complex<double> (0, 1) *
      amp[298] - Complex<double> (0, 1) * amp[297] + Complex<double> (0, 1) *
      amp[301] + Complex<double> (0, 1) * amp[300] + Complex<double> (0, 1) *
      amp[304] + Complex<double> (0, 1) * amp[303] + Complex<double> (0, 1) *
      amp[306] + Complex<double> (0, 1) * amp[307] + Complex<double> (0, 1) *
      amp[310] + Complex<double> (0, 1) * amp[309] + Complex<double> (0, 1) *
      amp[319] - Complex<double> (0, 1) * amp[320] + Complex<double> (0, 1) *
      amp[322] + Complex<double> (0, 1) * amp[323] - Complex<double> (0, 1) *
      amp[324] + Complex<double> (0, 1) * amp[326] - Complex<double> (0, 1) *
      amp[327] + Complex<double> (0, 1) * amp[329] - Complex<double> (0, 1) *
      amp[330] + Complex<double> (0, 1) * amp[332] + Complex<double> (0, 1) *
      amp[335] - Complex<double> (0, 1) * amp[333] + Complex<double> (0, 1) *
      amp[337] + Complex<double> (0, 1) * amp[338] + Complex<double> (0, 1) *
      amp[340] + Complex<double> (0, 1) * amp[341] + Complex<double> (0, 1) *
      amp[343] + Complex<double> (0, 1) * amp[344] + Complex<double> (0, 1) *
      amp[347] + Complex<double> (0, 1) * amp[346] + Complex<double> (0, 1) *
      amp[362] + Complex<double> (0, 1) * amp[364] + Complex<double> (0, 1) *
      amp[363] + Complex<double> (0, 1) * amp[370] + Complex<double> (0, 1) *
      amp[371] + Complex<double> (0, 1) * amp[379] + Complex<double> (0, 1) *
      amp[378] + Complex<double> (0, 1) * amp[383] - Complex<double> (0, 1) *
      amp[381] + Complex<double> (0, 1) * amp[386] + Complex<double> (0, 1) *
      amp[385]);
  jamp[20] = +2. * (-Complex<double> (0, 1) * amp[37] - Complex<double> (0, 1)
      * amp[38] - Complex<double> (0, 1) * amp[39] - Complex<double> (0, 1) *
      amp[40] + Complex<double> (0, 1) * amp[42] - Complex<double> (0, 1) *
      amp[44] - Complex<double> (0, 1) * amp[46] + Complex<double> (0, 1) *
      amp[47] - Complex<double> (0, 1) * amp[60] - Complex<double> (0, 1) *
      amp[61] - Complex<double> (0, 1) * amp[63] - Complex<double> (0, 1) *
      amp[71] - Complex<double> (0, 1) * amp[70] - Complex<double> (0, 1) *
      amp[74] + Complex<double> (0, 1) * amp[72] - Complex<double> (0, 1) *
      amp[117] + Complex<double> (0, 1) * amp[119] + Complex<double> (0, 1) *
      amp[121] - Complex<double> (0, 1) * amp[122] - Complex<double> (0, 1) *
      amp[123] - Complex<double> (0, 1) * amp[124] + Complex<double> (0, 1) *
      amp[125] - Complex<double> (0, 1) * amp[126] - Complex<double> (0, 1) *
      amp[131] + Complex<double> (0, 1) * amp[133] - Complex<double> (0, 1) *
      amp[134] + Complex<double> (0, 1) * amp[135] + Complex<double> (0, 1) *
      amp[136] + Complex<double> (0, 1) * amp[137] + Complex<double> (0, 1) *
      amp[139] - Complex<double> (0, 1) * amp[141] - Complex<double> (0, 1) *
      amp[142] - Complex<double> (0, 1) * amp[143] - Complex<double> (0, 1) *
      amp[144] + Complex<double> (0, 1) * amp[148] - Complex<double> (0, 1) *
      amp[146] - Complex<double> (0, 1) * amp[149] - Complex<double> (0, 1) *
      amp[150] + Complex<double> (0, 1) * amp[152] + Complex<double> (0, 1) *
      amp[154] + Complex<double> (0, 1) * amp[155] + Complex<double> (0, 1) *
      amp[156] + Complex<double> (0, 1) * amp[157] - Complex<double> (0, 1) *
      amp[159] - Complex<double> (0, 1) * amp[160] + Complex<double> (0, 1) *
      amp[161] - Complex<double> (0, 1) * amp[162] - Complex<double> (0, 1) *
      amp[163] - Complex<double> (0, 1) * amp[164] + Complex<double> (0, 1) *
      amp[165] - Complex<double> (0, 1) * amp[166] + Complex<double> (0, 1) *
      amp[168] + Complex<double> (0, 1) * amp[169] + Complex<double> (0, 1) *
      amp[171] + Complex<double> (0, 1) * amp[173] + Complex<double> (0, 1) *
      amp[174] + Complex<double> (0, 1) * amp[176] - Complex<double> (0, 1) *
      amp[180] - Complex<double> (0, 1) * amp[181] + Complex<double> (0, 1) *
      amp[184] + Complex<double> (0, 1) * amp[183] - Complex<double> (0, 1) *
      amp[187] - Complex<double> (0, 1) * amp[186] - Complex<double> (0, 1) *
      amp[195] - Complex<double> (0, 1) * amp[196] - Complex<double> (0, 1) *
      amp[198] - Complex<double> (0, 1) * amp[208] - Complex<double> (0, 1) *
      amp[209] - Complex<double> (0, 1) * amp[211] - Complex<double> (0, 1) *
      amp[213] - Complex<double> (0, 1) * amp[214] - Complex<double> (0, 1) *
      amp[216] + Complex<double> (0, 1) * amp[221] - Complex<double> (0, 1) *
      amp[223] + Complex<double> (0, 1) * amp[224] - Complex<double> (0, 1) *
      amp[226] - Complex<double> (0, 1) * amp[227] - Complex<double> (0, 1) *
      amp[229] - Complex<double> (0, 1) * amp[230] + Complex<double> (0, 1) *
      amp[231] + Complex<double> (0, 1) * amp[232] - Complex<double> (0, 1) *
      amp[233] + Complex<double> (0, 1) * amp[234] - Complex<double> (0, 1) *
      amp[255] - Complex<double> (0, 1) * amp[256] - Complex<double> (0, 1) *
      amp[257] - Complex<double> (0, 1) * amp[258] + Complex<double> (0, 1) *
      amp[283] + Complex<double> (0, 1) * amp[282] - Complex<double> (0, 1) *
      amp[286] - Complex<double> (0, 1) * amp[285] + Complex<double> (0, 1) *
      amp[289] + Complex<double> (0, 1) * amp[288] + Complex<double> (0, 1) *
      amp[292] + Complex<double> (0, 1) * amp[291] - Complex<double> (0, 1) *
      amp[312] - Complex<double> (0, 1) * amp[314] - Complex<double> (0, 1) *
      amp[315] + Complex<double> (0, 1) * amp[317] - Complex<double> (0, 1) *
      amp[318] - Complex<double> (0, 1) * amp[319] - Complex<double> (0, 1) *
      amp[322] - Complex<double> (0, 1) * amp[321] - Complex<double> (0, 1) *
      amp[337] - Complex<double> (0, 1) * amp[336] - Complex<double> (0, 1) *
      amp[340] - Complex<double> (0, 1) * amp[339] - Complex<double> (0, 1) *
      amp[343] - Complex<double> (0, 1) * amp[342] - Complex<double> (0, 1) *
      amp[346] - Complex<double> (0, 1) * amp[345] - Complex<double> (0, 1) *
      amp[349] - Complex<double> (0, 1) * amp[350] - Complex<double> (0, 1) *
      amp[352] - Complex<double> (0, 1) * amp[353] - Complex<double> (0, 1) *
      amp[355] - Complex<double> (0, 1) * amp[356] - Complex<double> (0, 1) *
      amp[359] - Complex<double> (0, 1) * amp[358] + Complex<double> (0, 1) *
      amp[361] - Complex<double> (0, 1) * amp[364] - Complex<double> (0, 1) *
      amp[365] + Complex<double> (0, 1) * amp[366] - Complex<double> (0, 1) *
      amp[368] + Complex<double> (0, 1) * amp[376] + Complex<double> (0, 1) *
      amp[375] - Complex<double> (0, 1) * amp[385] - Complex<double> (0, 1) *
      amp[384] - Complex<double> (0, 1) * amp[389] - Complex<double> (0, 1) *
      amp[388]);
  jamp[21] = +2. * (-Complex<double> (0, 1) * amp[1] - Complex<double> (0, 1) *
      amp[2] - Complex<double> (0, 1) * amp[3] - Complex<double> (0, 1) *
      amp[4] - Complex<double> (0, 1) * amp[6] + Complex<double> (0, 1) *
      amp[8] + Complex<double> (0, 1) * amp[10] - Complex<double> (0, 1) *
      amp[11] - Complex<double> (0, 1) * amp[12] + Complex<double> (0, 1) *
      amp[14] + Complex<double> (0, 1) * amp[16] - Complex<double> (0, 1) *
      amp[17] + Complex<double> (0, 1) * amp[18] + Complex<double> (0, 1) *
      amp[19] - Complex<double> (0, 1) * amp[20] + Complex<double> (0, 1) *
      amp[21] + Complex<double> (0, 1) * amp[22] - Complex<double> (0, 1) *
      amp[23] + Complex<double> (0, 1) * amp[27] + Complex<double> (0, 1) *
      amp[28] - Complex<double> (0, 1) * amp[32] - Complex<double> (0, 1) *
      amp[31] + Complex<double> (0, 1) * amp[34] + Complex<double> (0, 1) *
      amp[33] - Complex<double> (0, 1) * amp[42] + Complex<double> (0, 1) *
      amp[44] + Complex<double> (0, 1) * amp[46] - Complex<double> (0, 1) *
      amp[47] + Complex<double> (0, 1) * amp[54] - Complex<double> (0, 1) *
      amp[56] + Complex<double> (0, 1) * amp[57] - Complex<double> (0, 1) *
      amp[58] + Complex<double> (0, 1) * amp[59] + Complex<double> (0, 1) *
      amp[60] + Complex<double> (0, 1) * amp[62] + Complex<double> (0, 1) *
      amp[70] + Complex<double> (0, 1) * amp[69] - Complex<double> (0, 1) *
      amp[80] - Complex<double> (0, 1) * amp[79] - Complex<double> (0, 1) *
      amp[153] - Complex<double> (0, 1) * amp[154] - Complex<double> (0, 1) *
      amp[156] - Complex<double> (0, 1) * amp[158] + Complex<double> (0, 1) *
      amp[163] + Complex<double> (0, 1) * amp[164] - Complex<double> (0, 1) *
      amp[165] + Complex<double> (0, 1) * amp[166] - Complex<double> (0, 1) *
      amp[167] - Complex<double> (0, 1) * amp[168] - Complex<double> (0, 1) *
      amp[170] + Complex<double> (0, 1) * amp[172] - Complex<double> (0, 1) *
      amp[174] + Complex<double> (0, 1) * amp[175] - Complex<double> (0, 1) *
      amp[176] - Complex<double> (0, 1) * amp[177] - Complex<double> (0, 1) *
      amp[178] - Complex<double> (0, 1) * amp[179] + Complex<double> (0, 1) *
      amp[181] - Complex<double> (0, 1) * amp[183] - Complex<double> (0, 1) *
      amp[182] - Complex<double> (0, 1) * amp[185] + Complex<double> (0, 1) *
      amp[187] + Complex<double> (0, 1) * amp[188] + Complex<double> (0, 1) *
      amp[189] - Complex<double> (0, 1) * amp[191] + Complex<double> (0, 1) *
      amp[192] + Complex<double> (0, 1) * amp[194] + Complex<double> (0, 1) *
      amp[195] + Complex<double> (0, 1) * amp[197] + Complex<double> (0, 1) *
      amp[198] - Complex<double> (0, 1) * amp[206] - Complex<double> (0, 1) *
      amp[205] - Complex<double> (0, 1) * amp[221] + Complex<double> (0, 1) *
      amp[223] - Complex<double> (0, 1) * amp[224] + Complex<double> (0, 1) *
      amp[226] + Complex<double> (0, 1) * amp[227] + Complex<double> (0, 1) *
      amp[229] + Complex<double> (0, 1) * amp[230] - Complex<double> (0, 1) *
      amp[231] - Complex<double> (0, 1) * amp[232] + Complex<double> (0, 1) *
      amp[233] - Complex<double> (0, 1) * amp[234] - Complex<double> (0, 1) *
      amp[254] + Complex<double> (0, 1) * amp[256] + Complex<double> (0, 1) *
      amp[258] - Complex<double> (0, 1) * amp[259] - Complex<double> (0, 1) *
      amp[260] - Complex<double> (0, 1) * amp[262] + Complex<double> (0, 1) *
      amp[264] + Complex<double> (0, 1) * amp[265] + Complex<double> (0, 1) *
      amp[268] + Complex<double> (0, 1) * amp[267] - Complex<double> (0, 1) *
      amp[282] + Complex<double> (0, 1) * amp[284] + Complex<double> (0, 1) *
      amp[285] - Complex<double> (0, 1) * amp[287] - Complex<double> (0, 1) *
      amp[288] + Complex<double> (0, 1) * amp[290] + Complex<double> (0, 1) *
      amp[293] - Complex<double> (0, 1) * amp[291] + Complex<double> (0, 1) *
      amp[295] + Complex<double> (0, 1) * amp[296] - Complex<double> (0, 1) *
      amp[298] - Complex<double> (0, 1) * amp[299] + Complex<double> (0, 1) *
      amp[301] + Complex<double> (0, 1) * amp[302] + Complex<double> (0, 1) *
      amp[305] + Complex<double> (0, 1) * amp[304] + Complex<double> (0, 1) *
      amp[319] - Complex<double> (0, 1) * amp[320] + Complex<double> (0, 1) *
      amp[322] + Complex<double> (0, 1) * amp[323] + Complex<double> (0, 1) *
      amp[337] + Complex<double> (0, 1) * amp[336] + Complex<double> (0, 1) *
      amp[340] + Complex<double> (0, 1) * amp[339] + Complex<double> (0, 1) *
      amp[343] + Complex<double> (0, 1) * amp[342] + Complex<double> (0, 1) *
      amp[346] + Complex<double> (0, 1) * amp[345] - Complex<double> (0, 1) *
      amp[361] + Complex<double> (0, 1) * amp[364] + Complex<double> (0, 1) *
      amp[363] + Complex<double> (0, 1) * amp[377] - Complex<double> (0, 1) *
      amp[375] + Complex<double> (0, 1) * amp[380] + Complex<double> (0, 1) *
      amp[379] + Complex<double> (0, 1) * amp[385] + Complex<double> (0, 1) *
      amp[384]);
  jamp[22] = +2. * (+Complex<double> (0, 1) * amp[36] - Complex<double> (0, 1)
      * amp[38] - Complex<double> (0, 1) * amp[40] + Complex<double> (0, 1) *
      amp[41] - Complex<double> (0, 1) * amp[43] - Complex<double> (0, 1) *
      amp[44] - Complex<double> (0, 1) * amp[45] - Complex<double> (0, 1) *
      amp[46] - Complex<double> (0, 1) * amp[65] - Complex<double> (0, 1) *
      amp[66] - Complex<double> (0, 1) * amp[68] - Complex<double> (0, 1) *
      amp[71] - Complex<double> (0, 1) * amp[70] - Complex<double> (0, 1) *
      amp[77] + Complex<double> (0, 1) * amp[75] - Complex<double> (0, 1) *
      amp[81] + Complex<double> (0, 1) * amp[83] + Complex<double> (0, 1) *
      amp[85] - Complex<double> (0, 1) * amp[86] - Complex<double> (0, 1) *
      amp[87] - Complex<double> (0, 1) * amp[88] + Complex<double> (0, 1) *
      amp[89] - Complex<double> (0, 1) * amp[90] - Complex<double> (0, 1) *
      amp[95] + Complex<double> (0, 1) * amp[97] - Complex<double> (0, 1) *
      amp[98] + Complex<double> (0, 1) * amp[99] + Complex<double> (0, 1) *
      amp[100] + Complex<double> (0, 1) * amp[101] + Complex<double> (0, 1) *
      amp[103] - Complex<double> (0, 1) * amp[105] - Complex<double> (0, 1) *
      amp[106] - Complex<double> (0, 1) * amp[107] - Complex<double> (0, 1) *
      amp[108] + Complex<double> (0, 1) * amp[112] - Complex<double> (0, 1) *
      amp[110] - Complex<double> (0, 1) * amp[113] - Complex<double> (0, 1) *
      amp[114] + Complex<double> (0, 1) * amp[116] + Complex<double> (0, 1) *
      amp[154] + Complex<double> (0, 1) * amp[155] + Complex<double> (0, 1) *
      amp[156] + Complex<double> (0, 1) * amp[157] - Complex<double> (0, 1) *
      amp[159] - Complex<double> (0, 1) * amp[160] + Complex<double> (0, 1) *
      amp[161] - Complex<double> (0, 1) * amp[162] - Complex<double> (0, 1) *
      amp[163] - Complex<double> (0, 1) * amp[164] + Complex<double> (0, 1) *
      amp[165] - Complex<double> (0, 1) * amp[166] + Complex<double> (0, 1) *
      amp[168] + Complex<double> (0, 1) * amp[169] + Complex<double> (0, 1) *
      amp[171] + Complex<double> (0, 1) * amp[173] + Complex<double> (0, 1) *
      amp[174] + Complex<double> (0, 1) * amp[176] - Complex<double> (0, 1) *
      amp[180] - Complex<double> (0, 1) * amp[181] + Complex<double> (0, 1) *
      amp[184] + Complex<double> (0, 1) * amp[183] - Complex<double> (0, 1) *
      amp[187] - Complex<double> (0, 1) * amp[186] - Complex<double> (0, 1) *
      amp[200] - Complex<double> (0, 1) * amp[201] - Complex<double> (0, 1) *
      amp[203] + Complex<double> (0, 1) * amp[207] - Complex<double> (0, 1) *
      amp[209] + Complex<double> (0, 1) * amp[210] - Complex<double> (0, 1) *
      amp[212] - Complex<double> (0, 1) * amp[213] - Complex<double> (0, 1) *
      amp[215] - Complex<double> (0, 1) * amp[216] + Complex<double> (0, 1) *
      amp[217] + Complex<double> (0, 1) * amp[218] - Complex<double> (0, 1) *
      amp[219] + Complex<double> (0, 1) * amp[220] - Complex<double> (0, 1) *
      amp[222] - Complex<double> (0, 1) * amp[223] - Complex<double> (0, 1) *
      amp[225] - Complex<double> (0, 1) * amp[227] - Complex<double> (0, 1) *
      amp[228] - Complex<double> (0, 1) * amp[230] - Complex<double> (0, 1) *
      amp[250] - Complex<double> (0, 1) * amp[251] - Complex<double> (0, 1) *
      amp[252] - Complex<double> (0, 1) * amp[253] + Complex<double> (0, 1) *
      amp[271] + Complex<double> (0, 1) * amp[270] - Complex<double> (0, 1) *
      amp[274] - Complex<double> (0, 1) * amp[273] + Complex<double> (0, 1) *
      amp[277] + Complex<double> (0, 1) * amp[276] + Complex<double> (0, 1) *
      amp[280] + Complex<double> (0, 1) * amp[279] - Complex<double> (0, 1) *
      amp[306] - Complex<double> (0, 1) * amp[308] - Complex<double> (0, 1) *
      amp[309] + Complex<double> (0, 1) * amp[311] - Complex<double> (0, 1) *
      amp[318] - Complex<double> (0, 1) * amp[319] - Complex<double> (0, 1) *
      amp[322] - Complex<double> (0, 1) * amp[321] - Complex<double> (0, 1) *
      amp[337] - Complex<double> (0, 1) * amp[338] - Complex<double> (0, 1) *
      amp[340] - Complex<double> (0, 1) * amp[341] - Complex<double> (0, 1) *
      amp[343] - Complex<double> (0, 1) * amp[344] - Complex<double> (0, 1) *
      amp[347] - Complex<double> (0, 1) * amp[346] - Complex<double> (0, 1) *
      amp[349] - Complex<double> (0, 1) * amp[348] - Complex<double> (0, 1) *
      amp[352] - Complex<double> (0, 1) * amp[351] - Complex<double> (0, 1) *
      amp[355] - Complex<double> (0, 1) * amp[354] - Complex<double> (0, 1) *
      amp[358] - Complex<double> (0, 1) * amp[357] + Complex<double> (0, 1) *
      amp[360] - Complex<double> (0, 1) * amp[364] - Complex<double> (0, 1) *
      amp[365] + Complex<double> (0, 1) * amp[369] - Complex<double> (0, 1) *
      amp[371] + Complex<double> (0, 1) * amp[373] + Complex<double> (0, 1) *
      amp[372] - Complex<double> (0, 1) * amp[386] - Complex<double> (0, 1) *
      amp[385] - Complex<double> (0, 1) * amp[388] - Complex<double> (0, 1) *
      amp[387]);
  jamp[23] = +2. * (-Complex<double> (0, 1) * amp[0] + Complex<double> (0, 1) *
      amp[2] + Complex<double> (0, 1) * amp[4] - Complex<double> (0, 1) *
      amp[5] - Complex<double> (0, 1) * amp[7] - Complex<double> (0, 1) *
      amp[8] - Complex<double> (0, 1) * amp[9] - Complex<double> (0, 1) *
      amp[10] + Complex<double> (0, 1) * amp[12] + Complex<double> (0, 1) *
      amp[13] + Complex<double> (0, 1) * amp[15] + Complex<double> (0, 1) *
      amp[17] - Complex<double> (0, 1) * amp[18] - Complex<double> (0, 1) *
      amp[19] + Complex<double> (0, 1) * amp[20] - Complex<double> (0, 1) *
      amp[24] - Complex<double> (0, 1) * amp[25] - Complex<double> (0, 1) *
      amp[26] - Complex<double> (0, 1) * amp[27] + Complex<double> (0, 1) *
      amp[29] + Complex<double> (0, 1) * amp[32] - Complex<double> (0, 1) *
      amp[30] - Complex<double> (0, 1) * amp[33] + Complex<double> (0, 1) *
      amp[35] - Complex<double> (0, 1) * amp[36] + Complex<double> (0, 1) *
      amp[38] + Complex<double> (0, 1) * amp[40] - Complex<double> (0, 1) *
      amp[41] - Complex<double> (0, 1) * amp[54] + Complex<double> (0, 1) *
      amp[56] - Complex<double> (0, 1) * amp[57] + Complex<double> (0, 1) *
      amp[58] - Complex<double> (0, 1) * amp[64] + Complex<double> (0, 1) *
      amp[66] - Complex<double> (0, 1) * amp[67] + Complex<double> (0, 1) *
      amp[71] - Complex<double> (0, 1) * amp[69] + Complex<double> (0, 1) *
      amp[80] - Complex<double> (0, 1) * amp[78] + Complex<double> (0, 1) *
      amp[153] - Complex<double> (0, 1) * amp[155] - Complex<double> (0, 1) *
      amp[157] + Complex<double> (0, 1) * amp[158] + Complex<double> (0, 1) *
      amp[159] + Complex<double> (0, 1) * amp[160] - Complex<double> (0, 1) *
      amp[161] + Complex<double> (0, 1) * amp[162] + Complex<double> (0, 1) *
      amp[167] - Complex<double> (0, 1) * amp[169] + Complex<double> (0, 1) *
      amp[170] - Complex<double> (0, 1) * amp[171] - Complex<double> (0, 1) *
      amp[172] - Complex<double> (0, 1) * amp[173] - Complex<double> (0, 1) *
      amp[175] + Complex<double> (0, 1) * amp[177] + Complex<double> (0, 1) *
      amp[178] + Complex<double> (0, 1) * amp[179] + Complex<double> (0, 1) *
      amp[180] - Complex<double> (0, 1) * amp[184] + Complex<double> (0, 1) *
      amp[182] + Complex<double> (0, 1) * amp[185] + Complex<double> (0, 1) *
      amp[186] - Complex<double> (0, 1) * amp[188] - Complex<double> (0, 1) *
      amp[189] + Complex<double> (0, 1) * amp[191] - Complex<double> (0, 1) *
      amp[192] - Complex<double> (0, 1) * amp[199] + Complex<double> (0, 1) *
      amp[201] - Complex<double> (0, 1) * amp[202] + Complex<double> (0, 1) *
      amp[203] + Complex<double> (0, 1) * amp[206] - Complex<double> (0, 1) *
      amp[204] - Complex<double> (0, 1) * amp[207] + Complex<double> (0, 1) *
      amp[209] - Complex<double> (0, 1) * amp[210] + Complex<double> (0, 1) *
      amp[212] + Complex<double> (0, 1) * amp[213] + Complex<double> (0, 1) *
      amp[215] + Complex<double> (0, 1) * amp[216] - Complex<double> (0, 1) *
      amp[217] - Complex<double> (0, 1) * amp[218] + Complex<double> (0, 1) *
      amp[219] - Complex<double> (0, 1) * amp[220] - Complex<double> (0, 1) *
      amp[249] + Complex<double> (0, 1) * amp[251] + Complex<double> (0, 1) *
      amp[253] + Complex<double> (0, 1) * amp[259] + Complex<double> (0, 1) *
      amp[260] + Complex<double> (0, 1) * amp[262] - Complex<double> (0, 1) *
      amp[264] - Complex<double> (0, 1) * amp[266] - Complex<double> (0, 1) *
      amp[267] + Complex<double> (0, 1) * amp[269] - Complex<double> (0, 1) *
      amp[270] + Complex<double> (0, 1) * amp[272] + Complex<double> (0, 1) *
      amp[273] - Complex<double> (0, 1) * amp[275] - Complex<double> (0, 1) *
      amp[276] + Complex<double> (0, 1) * amp[278] + Complex<double> (0, 1) *
      amp[281] - Complex<double> (0, 1) * amp[279] - Complex<double> (0, 1) *
      amp[295] - Complex<double> (0, 1) * amp[296] + Complex<double> (0, 1) *
      amp[298] + Complex<double> (0, 1) * amp[299] - Complex<double> (0, 1) *
      amp[301] - Complex<double> (0, 1) * amp[302] - Complex<double> (0, 1) *
      amp[305] - Complex<double> (0, 1) * amp[304] + Complex<double> (0, 1) *
      amp[318] + Complex<double> (0, 1) * amp[320] + Complex<double> (0, 1) *
      amp[321] - Complex<double> (0, 1) * amp[323] + Complex<double> (0, 1) *
      amp[349] + Complex<double> (0, 1) * amp[348] + Complex<double> (0, 1) *
      amp[352] + Complex<double> (0, 1) * amp[351] + Complex<double> (0, 1) *
      amp[355] + Complex<double> (0, 1) * amp[354] + Complex<double> (0, 1) *
      amp[358] + Complex<double> (0, 1) * amp[357] - Complex<double> (0, 1) *
      amp[360] - Complex<double> (0, 1) * amp[363] + Complex<double> (0, 1) *
      amp[365] + Complex<double> (0, 1) * amp[374] - Complex<double> (0, 1) *
      amp[372] - Complex<double> (0, 1) * amp[380] - Complex<double> (0, 1) *
      amp[379] + Complex<double> (0, 1) * amp[388] + Complex<double> (0, 1) *
      amp[387]);

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[0][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}


}  // end namespace PY8MEs_namespace

