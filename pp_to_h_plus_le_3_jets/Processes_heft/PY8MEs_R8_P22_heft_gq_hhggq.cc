//==========================================================================
// This file has been automatically generated for Pythia 8 by
// MadGraph5_aMC@NLO v. 2.6.0, 2017-08-16
// By the MadGraph5_aMC@NLO Development Team
// Visit launchpad.net/madgraph5 and amcatnlo.web.cern.ch
//==========================================================================

#include "PY8MEs_R8_P22_heft_gq_hhggq.h"
#include "HelAmps_heft.h"

using namespace Pythia8_heft; 

namespace PY8MEs_namespace 
{
//==========================================================================
// Class member functions for calculating the matrix elements for
// Process: g u > h h g g u HIG<=1 HIW<=1 WEIGHTED<=7 @8
// Process: g c > h h g g c HIG<=1 HIW<=1 WEIGHTED<=7 @8
// Process: g d > h h g g d HIG<=1 HIW<=1 WEIGHTED<=7 @8
// Process: g s > h h g g s HIG<=1 HIW<=1 WEIGHTED<=7 @8
// Process: g u~ > h h g g u~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
// Process: g c~ > h h g g c~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
// Process: g d~ > h h g g d~ HIG<=1 HIW<=1 WEIGHTED<=7 @8
// Process: g s~ > h h g g s~ HIG<=1 HIW<=1 WEIGHTED<=7 @8

// Exception class
class PY8MEs_R8_P22_heft_gq_hhggqException : public exception
{
  virtual const char * what() const throw()
  {
    return "Exception in class 'PY8MEs_R8_P22_heft_gq_hhggq'."; 
  }
}
PY8MEs_R8_P22_heft_gq_hhggq_exception; 

std::set<int> PY8MEs_R8_P22_heft_gq_hhggq::s_channel_proc = std::set<int>
    (createset<int> ());

int PY8MEs_R8_P22_heft_gq_hhggq::helicities[ncomb][nexternal] = {{-1, -1, 0, 0,
    -1, -1, -1}, {-1, -1, 0, 0, -1, -1, 1}, {-1, -1, 0, 0, -1, 1, -1}, {-1, -1,
    0, 0, -1, 1, 1}, {-1, -1, 0, 0, 1, -1, -1}, {-1, -1, 0, 0, 1, -1, 1}, {-1,
    -1, 0, 0, 1, 1, -1}, {-1, -1, 0, 0, 1, 1, 1}, {-1, 1, 0, 0, -1, -1, -1},
    {-1, 1, 0, 0, -1, -1, 1}, {-1, 1, 0, 0, -1, 1, -1}, {-1, 1, 0, 0, -1, 1,
    1}, {-1, 1, 0, 0, 1, -1, -1}, {-1, 1, 0, 0, 1, -1, 1}, {-1, 1, 0, 0, 1, 1,
    -1}, {-1, 1, 0, 0, 1, 1, 1}, {1, -1, 0, 0, -1, -1, -1}, {1, -1, 0, 0, -1,
    -1, 1}, {1, -1, 0, 0, -1, 1, -1}, {1, -1, 0, 0, -1, 1, 1}, {1, -1, 0, 0, 1,
    -1, -1}, {1, -1, 0, 0, 1, -1, 1}, {1, -1, 0, 0, 1, 1, -1}, {1, -1, 0, 0, 1,
    1, 1}, {1, 1, 0, 0, -1, -1, -1}, {1, 1, 0, 0, -1, -1, 1}, {1, 1, 0, 0, -1,
    1, -1}, {1, 1, 0, 0, -1, 1, 1}, {1, 1, 0, 0, 1, -1, -1}, {1, 1, 0, 0, 1,
    -1, 1}, {1, 1, 0, 0, 1, 1, -1}, {1, 1, 0, 0, 1, 1, 1}};

// Normalization factors the various processes
// Denominators: spins, colors and identical particles
int PY8MEs_R8_P22_heft_gq_hhggq::denom_colors[nprocesses] = {24, 24}; 
int PY8MEs_R8_P22_heft_gq_hhggq::denom_hels[nprocesses] = {4, 4}; 
int PY8MEs_R8_P22_heft_gq_hhggq::denom_iden[nprocesses] = {4, 4}; 

//--------------------------------------------------------------------------
// Color config initialization
void PY8MEs_R8_P22_heft_gq_hhggq::initColorConfigs() 
{
  color_configs = vector < vec_vec_int > (); 
  jamp_nc_relative_power = vector < vec_int > (); 

  // Color flows of process Process: g u > h h g g u HIG<=1 HIW<=1 WEIGHTED<=7
  // @8
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(2)(4)(0)(0)(0)(0)(0)(3)(2)(4)(3)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #1
  color_configs[0].push_back(vec_int(createvector<int>
      (1)(2)(3)(0)(0)(0)(0)(0)(3)(4)(4)(2)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #2
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(2)(4)(0)(0)(0)(0)(0)(3)(1)(4)(2)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #3
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(2)(2)(0)(0)(0)(0)(0)(3)(1)(4)(3)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #4
  color_configs[0].push_back(vec_int(createvector<int>
      (4)(2)(3)(0)(0)(0)(0)(0)(3)(2)(4)(1)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 
  // JAMP #5
  color_configs[0].push_back(vec_int(createvector<int>
      (3)(2)(2)(0)(0)(0)(0)(0)(3)(4)(4)(1)(1)(0)));
  jamp_nc_relative_power[0].push_back(0); 

  // Color flows of process Process: g u~ > h h g g u~ HIG<=1 HIW<=1
  // WEIGHTED<=7 @8
  color_configs.push_back(vec_vec_int()); 
  jamp_nc_relative_power.push_back(vec_int()); 
  // JAMP #0
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(2)(0)(1)(0)(0)(0)(0)(3)(2)(4)(3)(0)(4)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #1
  color_configs[1].push_back(vec_int(createvector<int>
      (1)(2)(0)(1)(0)(0)(0)(0)(3)(4)(4)(2)(0)(3)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #2
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(2)(0)(1)(0)(0)(0)(0)(3)(1)(4)(2)(0)(4)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #3
  color_configs[1].push_back(vec_int(createvector<int>
      (4)(2)(0)(1)(0)(0)(0)(0)(3)(1)(4)(3)(0)(2)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #4
  color_configs[1].push_back(vec_int(createvector<int>
      (4)(2)(0)(1)(0)(0)(0)(0)(3)(2)(4)(1)(0)(3)));
  jamp_nc_relative_power[1].push_back(0); 
  // JAMP #5
  color_configs[1].push_back(vec_int(createvector<int>
      (3)(2)(0)(1)(0)(0)(0)(0)(3)(4)(4)(1)(0)(2)));
  jamp_nc_relative_power[1].push_back(0); 
}

//--------------------------------------------------------------------------
// Destructor.
PY8MEs_R8_P22_heft_gq_hhggq::~PY8MEs_R8_P22_heft_gq_hhggq() 
{
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    delete[] p[i]; 
    p[i] = NULL; 
  }
}

//--------------------------------------------------------------------------
// Invert the permutation mapping
vector<int> PY8MEs_R8_P22_heft_gq_hhggq::invert_mapping(vector<int> mapping) 
{
  vector<int> inverted_mapping; 
  for (unsigned int i = 0; i < mapping.size(); i++ )
  {
    for (unsigned int j = 0; j < mapping.size(); j++ )
    {
      if (mapping[j] == ((int)i))
      {
        inverted_mapping.push_back(j); 
        break; 
      }
    }
  }
  return inverted_mapping; 
}

//--------------------------------------------------------------------------
// Return the list of possible helicity configurations
vector < vec_int > PY8MEs_R8_P22_heft_gq_hhggq::getHelicityConfigs(vector<int>
    permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(ncomb, vector<int> (nexternal, 0)); 
  for (unsigned int ihel = 0; ihel < ncomb; ihel++ )
  {
    for(unsigned int j = 0; j < nexternal; j++ )
    {
      res[ihel][chosenPerm[j]] = helicities[ihel][j]; 
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Return the list of possible color configurations
vector < vec_int > PY8MEs_R8_P22_heft_gq_hhggq::getColorConfigs(int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector < vec_int > res(color_configs[chosenProcID].size(), vector<int>
      (nexternal * 2, 0));
  for (unsigned int icol = 0; icol < color_configs[chosenProcID].size(); icol++
      )
  {
    for(unsigned int j = 0; j < (2 * nexternal); j++ )
    {
      res[icol][chosenPerm[j/2] * 2 + j%2] =
          color_configs[chosenProcID][icol][j];
    }
  }
  return res; 
}

//--------------------------------------------------------------------------
// Get JAMP relative N_c power
int PY8MEs_R8_P22_heft_gq_hhggq::getColorFlowRelativeNCPower(int color_flow_ID,
    int specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return jamp_nc_relative_power[chosenProcID][color_flow_ID]; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity ID -> Helicity Config
vector<int> PY8MEs_R8_P22_heft_gq_hhggq::getHelicityConfigForID(int hel_ID,
    vector<int> permutation)
{
  if (hel_ID < 0 || hel_ID >= ncomb)
  {
    cerr <<  "Error in function 'getHelicityConfigForID' of class" << 
    " 'PY8MEs_R8_P22_heft_gq_hhggq': Specified helicity ID '" << 
    hel_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R8_P22_heft_gq_hhggq_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(nexternal, 0); 
  for (unsigned int j = 0; j < nexternal; j++ )
  {
    res[chosenPerm[j]] = helicities[hel_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Helicity Config -> Helicity ID
int PY8MEs_R8_P22_heft_gq_hhggq::getHelicityIDForConfig(vector<int> hel_config,
    vector<int> permutation)
{
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  int user_ihel = -1; 
  if (hel_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < ncomb; i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < nexternal; j++ )
      {
        if (helicities[i][chosenPerm[j]] != hel_config[j])
        {
          found = false; 
          break; 
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_ihel = i; 
        break; 
      }
    }
    if (user_ihel == -1)
    {
      cerr <<  "Error in function 'getHelicityIDForConfig' of class" << 
      " 'PY8MEs_R8_P22_heft_gq_hhggq': Specified helicity" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R8_P22_heft_gq_hhggq_exception; 
    }
  }
  return user_ihel; 
}


//--------------------------------------------------------------------------
// Implements the map Color ID -> Color Config
vector<int> PY8MEs_R8_P22_heft_gq_hhggq::getColorConfigForID(int color_ID, int
    specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < 0 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getColorConfigForID' of class" << 
    " 'PY8MEs_R8_P22_heft_gq_hhggq': Specified color ID '" << 
    color_ID <<  "' cannot be found." << endl; 
    throw PY8MEs_R8_P22_heft_gq_hhggq_exception; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = perm; 
  }
  else
  {
    chosenPerm = permutation; 
  }
  vector<int> res(color_configs[chosenProcID][color_ID].size(), 0); 
  for (unsigned int j = 0; j < (2 * nexternal); j++ )
  {
    res[chosenPerm[j/2] * 2 + j%2] = color_configs[chosenProcID][color_ID][j]; 
  }
  return res; 
}

//--------------------------------------------------------------------------
// Implements the map Color Config -> Color ID
int PY8MEs_R8_P22_heft_gq_hhggq::getColorIDForConfig(vector<int> color_config,
    int specify_proc_ID, vector<int> permutation)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  vector<int> chosenPerm; 
  if (permutation.size() == 0)
  {
    chosenPerm = invert_mapping(perm); 
  }
  else
  {
    chosenPerm = invert_mapping(permutation); 
  }
  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = -1; 
  if (color_config.size() > 0)
  {
    bool found = false; 
    for(unsigned int i = 0; i < color_configs[chosenProcID].size(); i++ )
    {
      found = true; 
      for (unsigned int j = 0; j < (nexternal * 2); j++ )
      {

        // If colorless then make sure it matches
        // The little arithmetics in the color index is just
        // the permutation applies on the particle list which is
        // twice smaller since each particle can have two color indices.
        if (color_config[j] == 0)
        {
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] != 0)
          {
            found = false; 
            break; 
          }
          // Otherwise check that the color linked position matches
        }
        else
        {
          int color_linked_pos = -1; 
          // Find the other end of the line in the user color config
          for (unsigned int k = 0; k < (nexternal * 2); k++ )
          {
            if (k == j)
              continue; 
            if (color_config[j] == color_config[k])
            {
              color_linked_pos = k; 
              break; 
            }
          }
          if (color_linked_pos == -1)
          {
            cerr <<  "Error in function 'getColorIDForConfig' of class" << 
            " 'PY8MEs_R8_P22_heft_gq_hhggq': A color line could " << 
            " not be closed." << endl; 
            throw PY8MEs_R8_P22_heft_gq_hhggq_exception; 
          }
          // Now check whether the color line matches
          if (color_configs[chosenProcID][i][chosenPerm[j/2] * 2 + j%2] !=
              color_configs[chosenProcID][i][chosenPerm[color_linked_pos/2] * 2
              + color_linked_pos%2])
          {
            found = false; 
            break; 
          }
        }
      }
      if ( !found)
        continue; 
      else
      {
        user_icol = i; 
        break; 
      }
    }

    if (user_icol == -1)
    {
      cerr <<  "Error in function 'getColorIDForConfig' of class" << 
      " 'PY8MEs_R8_P22_heft_gq_hhggq': Specified color" << 
      " configuration cannot be found." << endl; 
      throw PY8MEs_R8_P22_heft_gq_hhggq_exception; 
    }
  }
  return user_icol; 
}

//--------------------------------------------------------------------------
// Returns all result previously computed in SigmaKin
vector < vec_double > PY8MEs_R8_P22_heft_gq_hhggq::getAllResults(int
    specify_proc_ID)
{
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  return all_results[chosenProcID]; 
}

//--------------------------------------------------------------------------
// Returns a result previously computed in SigmaKin for a specific helicity
// and color ID. -1 means avg and summed over that characteristic.
double PY8MEs_R8_P22_heft_gq_hhggq::getResult(int helicity_ID, int color_ID,
    int specify_proc_ID)
{
  if (helicity_ID < - 1 || helicity_ID >= ncomb)
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R8_P22_heft_gq_hhggq': Specified helicity ID '" << 
    helicity_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R8_P22_heft_gq_hhggq_exception; 
  }
  int chosenProcID = -1; 
  if (specify_proc_ID == -1)
  {
    chosenProcID = proc_ID; 
  }
  else
  {
    chosenProcID = specify_proc_ID; 
  }
  if (color_ID < - 1 || color_ID >= int(color_configs[chosenProcID].size()))
  {
    cerr <<  "Error in function 'getResult' of class" << 
    " 'PY8MEs_R8_P22_heft_gq_hhggq': Specified color ID '" << 
    color_ID <<  "' configuration cannot be found." << endl; 
    throw PY8MEs_R8_P22_heft_gq_hhggq_exception; 
  }
  return all_results[chosenProcID][helicity_ID + 1][color_ID + 1]; 
}

//--------------------------------------------------------------------------
// Check for the availability of the requested process and if available,
// If available, this returns the corresponding permutation and Proc_ID to use.
// If not available, this returns a negative Proc_ID.
pair < vector<int> , int > PY8MEs_R8_P22_heft_gq_hhggq::static_getPY8ME(vector<int> initial_pdgs, vector<int> final_pdgs, set<int> schannels) 
{

  // Not available return value
  pair < vector<int> , int > NA(vector<int> (), -1); 

  // Check if s-channel requirements match
  if (nreq_s_channels > 0)
  {
    if (schannels != s_channel_proc)
      return NA; 
  }
  else
  {
    if (schannels.size() != 0)
      return NA; 
  }

  // Check number of final state particles
  if (final_pdgs.size() != (nexternal - ninitial))
    return NA; 

  // Check number of initial state particles
  if (initial_pdgs.size() != ninitial)
    return NA; 

  // List of processes available in this class
  const int nprocs = 16; 
  const int proc_IDS[nprocs] = {0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1,
      1};
  const int in_pdgs[nprocs][ninitial] = {{21, 2}, {21, 4}, {21, 1}, {21, 3},
      {21, -2}, {21, -4}, {21, -1}, {21, -3}, {2, 21}, {4, 21}, {1, 21}, {3,
      21}, {-2, 21}, {-4, 21}, {-1, 21}, {-3, 21}};
  const int out_pdgs[nprocs][nexternal - ninitial] = {{25, 25, 21, 21, 2}, {25,
      25, 21, 21, 4}, {25, 25, 21, 21, 1}, {25, 25, 21, 21, 3}, {25, 25, 21,
      21, -2}, {25, 25, 21, 21, -4}, {25, 25, 21, 21, -1}, {25, 25, 21, 21,
      -3}, {25, 25, 21, 21, 2}, {25, 25, 21, 21, 4}, {25, 25, 21, 21, 1}, {25,
      25, 21, 21, 3}, {25, 25, 21, 21, -2}, {25, 25, 21, 21, -4}, {25, 25, 21,
      21, -1}, {25, 25, 21, 21, -3}};

  bool in_pdgs_used[ninitial]; 
  bool out_pdgs_used[nexternal - ninitial]; 
  for(unsigned int i = 0; i < nprocs; i++ )
  {
    int permutations[nexternal]; 

    // Reinitialize initial state look-up variables
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      in_pdgs_used[j] = false; 
      permutations[j] = -1; 
    }
    // Look for initial state matches
    for(unsigned int j = 0; j < ninitial; j++ )
    {
      for(unsigned int k = 0; k < ninitial; k++ )
      {
        // Make sure it has not been used already
        if (in_pdgs_used[k])
          continue; 
        if (initial_pdgs[k] == in_pdgs[i][j])
        {
          permutations[j] = k; 
          in_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[ninitial - 1] == -1)
      continue; 

    // Reinitialize final state look-up variables
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      out_pdgs_used[j] = false; 
      permutations[ninitial + j] = -1; 
    }
    // Look for final state matches
    for(unsigned int j = 0; j < (nexternal - ninitial); j++ )
    {
      for(unsigned int k = 0; k < (nexternal - ninitial); k++ )
      {
        // Make sure it has not been used already
        if (out_pdgs_used[k])
          continue; 
        if (final_pdgs[k] == out_pdgs[i][j])
        {
          permutations[ninitial + j] = ninitial + k; 
          out_pdgs_used[k] = true; 
          break; 
        }
      }
      // If no match found for this particular initial state,
      // proceed with the next process
      if (permutations[ninitial + j] == -1)
        break; 
    }
    // Proceed with next process if not match found
    if (permutations[nexternal - 1] == -1)
      continue; 

    // Return process found
    return pair < vector<int> , int > (vector<int> (permutations, permutations
        + nexternal), proc_IDS[i]);
  }

  // No process found
  return NA; 
}

//--------------------------------------------------------------------------
// Set momenta
void PY8MEs_R8_P22_heft_gq_hhggq::setMomenta(vector < vec_double >
    momenta_picked)
{
  if (momenta_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setMomenta' of class" << 
    " 'PY8MEs_R8_P22_heft_gq_hhggq': Incorrect number of" << 
    " momenta specified." << endl; 
    throw PY8MEs_R8_P22_heft_gq_hhggq_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    if (momenta_picked[i].size() != 4)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R8_P22_heft_gq_hhggq': Incorrect number of" << 
      " momenta components specified." << endl; 
      throw PY8MEs_R8_P22_heft_gq_hhggq_exception; 
    }
    if (momenta_picked[i][0] < 0.0)
    {
      cerr <<  "Error in function 'setMomenta' of class" << 
      " 'PY8MEs_R8_P22_heft_gq_hhggq': A momentum was specified" << 
      " with negative energy. Check conventions." << endl; 
      throw PY8MEs_R8_P22_heft_gq_hhggq_exception; 
    }
    for (unsigned int j = 0; j < 4; j++ )
    {
      p[i][j] = momenta_picked[i][j]; 
    }
  }
}

//--------------------------------------------------------------------------
// Set color configuration to use. An empty vector means sum over all.
void PY8MEs_R8_P22_heft_gq_hhggq::setColors(vector<int> colors_picked)
{
  if (colors_picked.size() == 0)
  {
    user_colors = vector<int> (); 
    return; 
  }
  if (colors_picked.size() != (2 * nexternal))
  {
    cerr <<  "Error in function 'setColors' of class" << 
    " 'PY8MEs_R8_P22_heft_gq_hhggq': Incorrect number" << 
    " of colors specified." << endl; 
    throw PY8MEs_R8_P22_heft_gq_hhggq_exception; 
  }
  user_colors = vector<int> ((2 * nexternal), 0); 
  for(unsigned int i = 0; i < (2 * nexternal); i++ )
  {
    user_colors[i] = colors_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the helicity configuration to use. Am empty vector means sum over all.
void PY8MEs_R8_P22_heft_gq_hhggq::setHelicities(vector<int> helicities_picked) 
{
  if (helicities_picked.size() != nexternal)
  {
    if (helicities_picked.size() == 0)
    {
      user_helicities = vector<int> (); 
      return; 
    }
    cerr <<  "Error in function 'setHelicities' of class" << 
    " 'PY8MEs_R8_P22_heft_gq_hhggq': Incorrect number" << 
    " of helicities specified." << endl; 
    throw PY8MEs_R8_P22_heft_gq_hhggq_exception; 
  }
  user_helicities = vector<int> (nexternal, 0); 
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    user_helicities[i] = helicities_picked[i]; 
  }
}

//--------------------------------------------------------------------------
// Set the permutation to use (will apply to momenta, colors and helicities)
void PY8MEs_R8_P22_heft_gq_hhggq::setPermutation(vector<int> perm_picked) 
{
  if (perm_picked.size() != nexternal)
  {
    cerr <<  "Error in function 'setPermutations' of class" << 
    " 'PY8MEs_R8_P22_heft_gq_hhggq': Incorrect number" << 
    " of permutations specified." << endl; 
    throw PY8MEs_R8_P22_heft_gq_hhggq_exception; 
  }
  for(unsigned int i = 0; i < nexternal; i++ )
  {
    perm[i] = perm_picked[i]; 
  }
}

// Set the proc_ID to use
void PY8MEs_R8_P22_heft_gq_hhggq::setProcID(int procID_picked) 
{
  proc_ID = procID_picked; 
}

//--------------------------------------------------------------------------
// Initialize process.

void PY8MEs_R8_P22_heft_gq_hhggq::initProc() 
{

  // Initialize flags
  include_symmetry_factors = true; 
  include_helicity_averaging_factors = true; 
  include_color_averaging_factors = true; 

  // Initialize vectors.
  perm = vector<int> (nexternal, 0); 
  user_colors = vector<int> (2 * nexternal, 0); 
  user_helicities = vector<int> (nexternal, 0); 
  p = vector < double * > (); 
  for (unsigned int i = 0; i < nexternal; i++ )
  {
    p.push_back(new double[4]); 
  }
  initColorConfigs(); 
  // Synchronize local variables dependent on the model with the active model.
  mME = vector<double> (nexternal, 0.); 
  syncProcModelParams(); 
  jamp2 = vector < vec_double > (2); 
  jamp2[0] = vector<double> (6, 0.); 
  jamp2[1] = vector<double> (6, 0.); 
  all_results = vector < vec_vec_double > (2); 
  // The first entry is always the color or helicity avg/summed matrix element.
  all_results[0] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
  all_results[1] = vector < vec_double > (ncomb + 1, vector<double> (6 + 1,
      0.));
}

// Synchronize local variables of the process that depend on the model
// parameters
void PY8MEs_R8_P22_heft_gq_hhggq::syncProcModelParams() 
{

  // Instantiate the model class and set parameters that stay fixed during run
  mME[0] = pars->ZERO; 
  mME[1] = pars->ZERO; 
  mME[2] = pars->mdl_MH; 
  mME[3] = pars->mdl_MH; 
  mME[4] = pars->ZERO; 
  mME[5] = pars->ZERO; 
  mME[6] = pars->ZERO; 
}

//--------------------------------------------------------------------------
// Setter allowing to force particular values for the external masses
void PY8MEs_R8_P22_heft_gq_hhggq::setMasses(vec_double external_masses) 
{

  if (external_masses.size() != mME.size())
  {
    cerr <<  "Error in function 'setMasses' of class" << 
    " 'PY8MEs_R8_P22_heft_gq_hhggq': Incorrect number of" << 
    " masses specified." << endl; 
    throw PY8MEs_R8_P22_heft_gq_hhggq_exception; 
  }
  for (unsigned int j = 0; j < mME.size(); j++ )
  {
    mME[j] = external_masses[perm[j]]; 
  }
}

//--------------------------------------------------------------------------
// Getter accessing external masses with the correct ordering
vector<double> PY8MEs_R8_P22_heft_gq_hhggq::getMasses() 
{

  vec_double external_masses; 
  vector<int> invertedPerm; 
  invertedPerm = invert_mapping(perm); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    external_masses.push_back(mME[invertedPerm[i]]); 
  }
  return external_masses; 

}


// Set all values of the external masses to float(-mode) where mode can be
// 0 : Mass taken from the model
// 1 : Mass taken from p_i^2 if not massless to begin with
// 2 : Mass always taken from p_i^2.
void PY8MEs_R8_P22_heft_gq_hhggq::setExternalMassesMode(int mode) 
{
  if (mode != 0 && mode != 1 && mode != 2)
  {
    cerr <<  "Error in function 'setExternalMassesMode' of class" << 
    " 'PY8MEs_R8_P22_heft_gq_hhggq': Incorrect mode selected :" << mode << 
    ". It must be either 0, 1 or 2" << endl; 
    throw PY8MEs_R8_P22_heft_gq_hhggq_exception; 
  }
  if (mode == 0)
  {
    syncProcModelParams(); 
  }
  else if (mode == 1)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      if (mME[j] != pars->ZERO)
      {
        mME[j] = -1.0; 
      }
    }
  }
  else if (mode == 2)
  {
    for (unsigned int j = 0; j < mME.size(); j++ )
    {
      mME[j] = -1.0; 
    }
  }
}

//--------------------------------------------------------------------------
// Evaluate the squared matrix element.

double PY8MEs_R8_P22_heft_gq_hhggq::sigmaKin() 
{
  // Set the parameters which change event by event
  pars->setDependentParameters(); 
  pars->setDependentCouplings(); 
  // Reset color flows
  for(int i = 0; i < 6; i++ )
    jamp2[0][i] = 0.; 
  for(int i = 0; i < 6; i++ )
    jamp2[1][i] = 0.; 

  // Save previous values of mME
  vector<double> saved_mME(mME.size(), 0.0); 
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (mME[i] < 0.0)
    {
      saved_mME[i] = mME[i]; 
      mME[i] = sqrt(abs(pow(p[perm[i]][0], 2) - 
      (pow(p[perm[i]][1], 2) + pow(p[perm[i]][2], 2) + pow(p[perm[i]][3],
          2))));
    }
  }

  // Local variables and constants
  const int max_tries = 10; 
  vector < vec_bool > goodhel(nprocesses, vec_bool(ncomb, false)); 
  vec_int ntry(nprocesses, 0); 
  double t = 0.; 
  double result = 0.; 

  if (ntry[proc_ID] <= max_tries)
    ntry[proc_ID] = ntry[proc_ID] + 1; 

  // Find which helicity configuration is asked for
  // -1 indicates one wants to sum over helicities
  int user_ihel = getHelicityIDForConfig(user_helicities); 

  // Find which color configuration is asked for
  // -1 indicates one wants to sum over all color configurations
  int user_icol = getColorIDForConfig(user_colors); 

  // Reset the list of results that will be recomputed here
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      all_results[proc_ID][ihel + 1][icolor + 1] = 0.; 
    }
  }

  // Calculate the matrix element for all helicities
  // unless already detected as vanishing
  for(int ihel = 0; ihel < ncomb; ihel++ )
  {
    // Skip helicity if already detected as vanishing
    if ((ntry[proc_ID] >= max_tries) && !goodhel[proc_ID][ihel])
      continue; 

    // Also skip helicity if user asks for a specific one
    if ((ntry[proc_ID] >= max_tries) && user_ihel != -1 && user_ihel != ihel)
      continue; 

    calculate_wavefunctions(helicities[ihel]); 

    // Reset locally computed color flows
    for(int i = 0; i < 6; i++ )
      jamp2[0][i] = 0.; 
    for(int i = 0; i < 6; i++ )
      jamp2[1][i] = 0.; 

    if (proc_ID == 0)
      t = matrix_8_gu_hhggu(); 
    if (proc_ID == 1)
      t = matrix_8_gux_hhggux(); 

    // Store which helicities give non-zero result
    if ((ntry[proc_ID] < max_tries) && t != 0. && !goodhel[proc_ID][ihel])
      goodhel[proc_ID][ihel] = true; 

    // Aggregate results
    if (user_ihel == -1 || user_ihel == ihel)
    {
      if (user_icol == -1)
      {
        result = result + t; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][0] += t; 
          for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
          {
            all_results[proc_ID][0][i + 1] += jamp2[proc_ID][i]; 
          }
        }
        all_results[proc_ID][ihel + 1][0] += t; 
        for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
        {
          all_results[proc_ID][ihel + 1][i + 1] += jamp2[proc_ID][i]; 
        }
      }
      else
      {
        result = result + jamp2[proc_ID][user_icol]; 
        if (user_ihel == -1)
        {
          all_results[proc_ID][0][user_icol + 1] += jamp2[proc_ID][user_icol]; 
        }
        all_results[proc_ID][ihel + 1][user_icol + 1] +=
            jamp2[proc_ID][user_icol];
      }
    }

  }

  // Normalize results with the identical particle factor
  if (include_symmetry_factors)
  {
    result = result/denom_iden[proc_ID]; 
  }
  // Starts with -1 which are the summed results
  for (int ihel = -1; ihel + 1 < ((int)all_results[proc_ID].size()); ihel++ )
  {
    // Only if it is the helicity picked
    if (user_ihel != -1 && ihel != user_ihel)
      continue; 
    for (int icolor = -1; icolor + 1 < ((int)all_results[proc_ID][ihel +
        1].size()); icolor++ )
    {
      // Only if color picked
      if (user_icol != -1 && icolor != user_icol)
        continue; 
      if (include_symmetry_factors)
      {
        all_results[proc_ID][ihel + 1][icolor + 1] /= denom_iden[proc_ID]; 
      }
    }
  }


  // Normalize when when summing+averaging over helicity configurations
  if (user_ihel == -1 && include_helicity_averaging_factors)
  {
    result /= denom_hels[proc_ID]; 
    if (user_icol == -1)
    {
      all_results[proc_ID][0][0] /= denom_hels[proc_ID]; 
      for (unsigned int i = 0; i < jamp2[proc_ID].size(); i++ )
      {
        all_results[proc_ID][0][i + 1] /= denom_hels[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][0][user_icol + 1] /= denom_hels[proc_ID]; 
    }
  }

  // Normalize when summing+averaging over color configurations
  if (user_icol == -1 && include_color_averaging_factors)
  {
    result /= denom_colors[proc_ID]; 
    if (user_ihel == -1)
    {
      all_results[proc_ID][0][0] /= denom_colors[proc_ID]; 
      for (unsigned int i = 0; i < ncomb; i++ )
      {
        all_results[proc_ID][i + 1][0] /= denom_colors[proc_ID]; 
      }
    }
    else
    {
      all_results[proc_ID][user_ihel + 1][0] /= denom_colors[proc_ID]; 
    }
  }

  // Reinstate previous values of mME
  for (unsigned int i = 0; i < mME.size(); i++ )
  {
    if (saved_mME[i] < 0.0)
    {
      mME[i] = saved_mME[i]; 
    }
  }

  // Finally return it
  return result; 
}

//==========================================================================
// Private class member functions

//--------------------------------------------------------------------------
// Evaluate |M|^2 for each subprocess

void PY8MEs_R8_P22_heft_gq_hhggq::calculate_wavefunctions(const int hel[])
{
  // Calculate wavefunctions for all processes
  // Calculate all wavefunctions
  vxxxxx(p[perm[0]], mME[0], hel[0], -1, w[0]); 
  ixxxxx(p[perm[1]], mME[1], hel[1], +1, w[1]); 
  sxxxxx(p[perm[2]], +1, w[2]); 
  sxxxxx(p[perm[3]], +1, w[3]); 
  vxxxxx(p[perm[4]], mME[4], hel[4], +1, w[4]); 
  vxxxxx(p[perm[5]], mME[5], hel[5], +1, w[5]); 
  oxxxxx(p[perm[6]], mME[6], hel[6], +1, w[6]); 
  VVV1P0_1(w[0], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[7]); 
  SSS1_1(w[2], w[3], pars->GC_69, pars->mdl_MH, pars->mdl_WH, w[8]); 
  FFV1_1(w[6], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[9]); 
  VVS3P0_1(w[5], w[8], pars->GC_13, pars->ZERO, pars->ZERO, w[10]); 
  FFV1_2(w[1], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[11]); 
  FFV1_1(w[6], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[12]); 
  VVS3P0_1(w[7], w[8], pars->GC_13, pars->ZERO, pars->ZERO, w[13]); 
  FFV1_2(w[1], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[14]); 
  FFV1P0_3(w[1], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[15]); 
  VVV1P0_1(w[7], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[16]); 
  VVV1P0_1(w[7], w[15], pars->GC_10, pars->ZERO, pars->ZERO, w[17]); 
  VVV1P0_1(w[0], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[18]); 
  FFV1_1(w[6], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[19]); 
  VVS3P0_1(w[18], w[8], pars->GC_13, pars->ZERO, pars->ZERO, w[20]); 
  FFV1_2(w[1], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[21]); 
  FFV1_1(w[6], w[18], pars->GC_11, pars->ZERO, pars->ZERO, w[22]); 
  VVS3P0_1(w[4], w[8], pars->GC_13, pars->ZERO, pars->ZERO, w[23]); 
  FFV1_2(w[1], w[18], pars->GC_11, pars->ZERO, pars->ZERO, w[24]); 
  VVV1P0_1(w[18], w[4], pars->GC_10, pars->ZERO, pars->ZERO, w[25]); 
  VVV1P0_1(w[18], w[15], pars->GC_10, pars->ZERO, pars->ZERO, w[26]); 
  FFV1_1(w[6], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[27]); 
  VVV1P0_1(w[4], w[5], pars->GC_10, pars->ZERO, pars->ZERO, w[28]); 
  FFV1P0_3(w[1], w[27], pars->GC_11, pars->ZERO, pars->ZERO, w[29]); 
  FFV1P0_3(w[21], w[27], pars->GC_11, pars->ZERO, pars->ZERO, w[30]); 
  FFV1_1(w[27], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[31]); 
  FFV1_1(w[27], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[32]); 
  FFV1P0_3(w[14], w[27], pars->GC_11, pars->ZERO, pars->ZERO, w[33]); 
  FFV1_2(w[1], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[34]); 
  FFV1P0_3(w[34], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[35]); 
  FFV1P0_3(w[34], w[19], pars->GC_11, pars->ZERO, pars->ZERO, w[36]); 
  FFV1_2(w[34], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[37]); 
  FFV1_2(w[34], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[38]); 
  FFV1P0_3(w[34], w[12], pars->GC_11, pars->ZERO, pars->ZERO, w[39]); 
  VVS3P0_1(w[0], w[8], pars->GC_13, pars->ZERO, pars->ZERO, w[40]); 
  FFV1_1(w[6], w[28], pars->GC_11, pars->ZERO, pars->ZERO, w[41]); 
  FFV1_2(w[1], w[28], pars->GC_11, pars->ZERO, pars->ZERO, w[42]); 
  VVV1P0_1(w[0], w[28], pars->GC_10, pars->ZERO, pars->ZERO, w[43]); 
  VVV1P0_1(w[0], w[15], pars->GC_10, pars->ZERO, pars->ZERO, w[44]); 
  FFV1_1(w[19], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[45]); 
  FFV1_1(w[19], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[46]); 
  FFV1P0_3(w[1], w[19], pars->GC_11, pars->ZERO, pars->ZERO, w[47]); 
  VVVS2P0_1(w[0], w[5], w[8], pars->GC_14, pars->ZERO, pars->ZERO, w[48]); 
  FFV1_2(w[21], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[49]); 
  FFV1_2(w[21], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[50]); 
  FFV1P0_3(w[21], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[51]); 
  FFV1_1(w[12], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[52]); 
  FFV1P0_3(w[1], w[12], pars->GC_11, pars->ZERO, pars->ZERO, w[53]); 
  FFV1_1(w[12], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[54]); 
  VVVS2P0_1(w[0], w[4], w[8], pars->GC_14, pars->ZERO, pars->ZERO, w[55]); 
  FFV1_2(w[14], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[56]); 
  FFV1P0_3(w[14], w[6], pars->GC_11, pars->ZERO, pars->ZERO, w[57]); 
  FFV1_2(w[14], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[58]); 
  VVV1P0_1(w[4], w[15], pars->GC_10, pars->ZERO, pars->ZERO, w[59]); 
  VVV1P0_1(w[5], w[15], pars->GC_10, pars->ZERO, pars->ZERO, w[60]); 
  VVVV1P0_1(w[0], w[4], w[15], pars->GC_12, pars->ZERO, pars->ZERO, w[61]); 
  VVVV3P0_1(w[0], w[4], w[15], pars->GC_12, pars->ZERO, pars->ZERO, w[62]); 
  VVVV4P0_1(w[0], w[4], w[15], pars->GC_12, pars->ZERO, pars->ZERO, w[63]); 
  VVVV1P0_1(w[0], w[5], w[15], pars->GC_12, pars->ZERO, pars->ZERO, w[64]); 
  VVVV3P0_1(w[0], w[5], w[15], pars->GC_12, pars->ZERO, pars->ZERO, w[65]); 
  VVVV4P0_1(w[0], w[5], w[15], pars->GC_12, pars->ZERO, pars->ZERO, w[66]); 
  VVVV1P0_1(w[0], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[67]); 
  VVVV3P0_1(w[0], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[68]); 
  VVVV4P0_1(w[0], w[4], w[5], pars->GC_12, pars->ZERO, pars->ZERO, w[69]); 
  oxxxxx(p[perm[1]], mME[1], hel[1], -1, w[70]); 
  ixxxxx(p[perm[6]], mME[6], hel[6], -1, w[71]); 
  FFV1_1(w[70], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[72]); 
  FFV1_2(w[71], w[7], pars->GC_11, pars->ZERO, pars->ZERO, w[73]); 
  FFV1_1(w[70], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[74]); 
  FFV1_2(w[71], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[75]); 
  FFV1P0_3(w[71], w[70], pars->GC_11, pars->ZERO, pars->ZERO, w[76]); 
  VVV1P0_1(w[7], w[76], pars->GC_10, pars->ZERO, pars->ZERO, w[77]); 
  FFV1_1(w[70], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[78]); 
  FFV1_2(w[71], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[79]); 
  FFV1_1(w[70], w[18], pars->GC_11, pars->ZERO, pars->ZERO, w[80]); 
  FFV1_2(w[71], w[18], pars->GC_11, pars->ZERO, pars->ZERO, w[81]); 
  VVV1P0_1(w[18], w[76], pars->GC_10, pars->ZERO, pars->ZERO, w[82]); 
  FFV1_1(w[70], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[83]); 
  FFV1P0_3(w[71], w[83], pars->GC_11, pars->ZERO, pars->ZERO, w[84]); 
  FFV1P0_3(w[79], w[83], pars->GC_11, pars->ZERO, pars->ZERO, w[85]); 
  FFV1_1(w[83], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[86]); 
  FFV1_1(w[83], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[87]); 
  FFV1P0_3(w[75], w[83], pars->GC_11, pars->ZERO, pars->ZERO, w[88]); 
  FFV1_2(w[71], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[89]); 
  FFV1P0_3(w[89], w[70], pars->GC_11, pars->ZERO, pars->ZERO, w[90]); 
  FFV1P0_3(w[89], w[78], pars->GC_11, pars->ZERO, pars->ZERO, w[91]); 
  FFV1_2(w[89], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[92]); 
  FFV1_2(w[89], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[93]); 
  FFV1P0_3(w[89], w[74], pars->GC_11, pars->ZERO, pars->ZERO, w[94]); 
  FFV1_1(w[70], w[28], pars->GC_11, pars->ZERO, pars->ZERO, w[95]); 
  FFV1_2(w[71], w[28], pars->GC_11, pars->ZERO, pars->ZERO, w[96]); 
  VVV1P0_1(w[0], w[76], pars->GC_10, pars->ZERO, pars->ZERO, w[97]); 
  FFV1_1(w[78], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[98]); 
  FFV1_1(w[78], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[99]); 
  FFV1P0_3(w[71], w[78], pars->GC_11, pars->ZERO, pars->ZERO, w[100]); 
  FFV1_2(w[79], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[101]); 
  FFV1_2(w[79], w[5], pars->GC_11, pars->ZERO, pars->ZERO, w[102]); 
  FFV1P0_3(w[79], w[70], pars->GC_11, pars->ZERO, pars->ZERO, w[103]); 
  FFV1_1(w[74], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[104]); 
  FFV1P0_3(w[71], w[74], pars->GC_11, pars->ZERO, pars->ZERO, w[105]); 
  FFV1_1(w[74], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[106]); 
  FFV1_2(w[75], w[4], pars->GC_11, pars->ZERO, pars->ZERO, w[107]); 
  FFV1P0_3(w[75], w[70], pars->GC_11, pars->ZERO, pars->ZERO, w[108]); 
  FFV1_2(w[75], w[0], pars->GC_11, pars->ZERO, pars->ZERO, w[109]); 
  VVV1P0_1(w[4], w[76], pars->GC_10, pars->ZERO, pars->ZERO, w[110]); 
  VVV1P0_1(w[5], w[76], pars->GC_10, pars->ZERO, pars->ZERO, w[111]); 
  VVVV1P0_1(w[0], w[4], w[76], pars->GC_12, pars->ZERO, pars->ZERO, w[112]); 
  VVVV3P0_1(w[0], w[4], w[76], pars->GC_12, pars->ZERO, pars->ZERO, w[113]); 
  VVVV4P0_1(w[0], w[4], w[76], pars->GC_12, pars->ZERO, pars->ZERO, w[114]); 
  VVVV1P0_1(w[0], w[5], w[76], pars->GC_12, pars->ZERO, pars->ZERO, w[115]); 
  VVVV3P0_1(w[0], w[5], w[76], pars->GC_12, pars->ZERO, pars->ZERO, w[116]); 
  VVVV4P0_1(w[0], w[5], w[76], pars->GC_12, pars->ZERO, pars->ZERO, w[117]); 

  // Calculate all amplitudes
  // Amplitude(s) for diagram number 0
  FFV1_0(w[1], w[9], w[10], pars->GC_11, amp[0]); 
  FFV1_0(w[11], w[6], w[10], pars->GC_11, amp[1]); 
  FFV1_0(w[1], w[12], w[13], pars->GC_11, amp[2]); 
  FFV1_0(w[14], w[6], w[13], pars->GC_11, amp[3]); 
  VVVS2_0(w[7], w[5], w[15], w[8], pars->GC_14, amp[4]); 
  VVV1_0(w[5], w[15], w[13], pars->GC_10, amp[5]); 
  VVS3_0(w[15], w[16], w[8], pars->GC_13, amp[6]); 
  VVS3_0(w[5], w[17], w[8], pars->GC_13, amp[7]); 
  FFV1_0(w[1], w[19], w[20], pars->GC_11, amp[8]); 
  FFV1_0(w[21], w[6], w[20], pars->GC_11, amp[9]); 
  FFV1_0(w[1], w[22], w[23], pars->GC_11, amp[10]); 
  FFV1_0(w[24], w[6], w[23], pars->GC_11, amp[11]); 
  VVVS2_0(w[18], w[4], w[15], w[8], pars->GC_14, amp[12]); 
  VVS3_0(w[15], w[25], w[8], pars->GC_13, amp[13]); 
  VVV1_0(w[4], w[15], w[20], pars->GC_10, amp[14]); 
  VVS3_0(w[4], w[26], w[8], pars->GC_13, amp[15]); 
  VVS3_0(w[28], w[29], w[8], pars->GC_13, amp[16]); 
  VVS3_0(w[5], w[30], w[8], pars->GC_13, amp[17]); 
  FFV1_0(w[1], w[31], w[10], pars->GC_11, amp[18]); 
  FFV1_0(w[1], w[32], w[23], pars->GC_11, amp[19]); 
  VVV1_0(w[29], w[23], w[5], pars->GC_10, amp[20]); 
  VVV1_0(w[29], w[4], w[10], pars->GC_10, amp[21]); 
  VVVS2_0(w[4], w[5], w[29], w[8], pars->GC_14, amp[22]); 
  VVS3_0(w[4], w[33], w[8], pars->GC_13, amp[23]); 
  VVS3_0(w[28], w[35], w[8], pars->GC_13, amp[24]); 
  VVS3_0(w[5], w[36], w[8], pars->GC_13, amp[25]); 
  FFV1_0(w[37], w[6], w[10], pars->GC_11, amp[26]); 
  FFV1_0(w[38], w[6], w[23], pars->GC_11, amp[27]); 
  VVV1_0(w[35], w[23], w[5], pars->GC_10, amp[28]); 
  VVV1_0(w[35], w[4], w[10], pars->GC_10, amp[29]); 
  VVVS2_0(w[4], w[5], w[35], w[8], pars->GC_14, amp[30]); 
  VVS3_0(w[4], w[39], w[8], pars->GC_13, amp[31]); 
  FFV1_0(w[1], w[41], w[40], pars->GC_11, amp[32]); 
  FFV1_0(w[42], w[6], w[40], pars->GC_11, amp[33]); 
  VVVS2_0(w[0], w[28], w[15], w[8], pars->GC_14, amp[34]); 
  VVS3_0(w[15], w[43], w[8], pars->GC_13, amp[35]); 
  VVV1_0(w[28], w[15], w[40], pars->GC_10, amp[36]); 
  VVS3_0(w[28], w[44], w[8], pars->GC_13, amp[37]); 
  FFV1_0(w[1], w[45], w[10], pars->GC_11, amp[38]); 
  FFV1_0(w[1], w[46], w[40], pars->GC_11, amp[39]); 
  VVV1_0(w[40], w[47], w[5], pars->GC_10, amp[40]); 
  VVV1_0(w[0], w[47], w[10], pars->GC_10, amp[41]); 
  FFV1_0(w[1], w[19], w[48], pars->GC_11, amp[42]); 
  FFV1_0(w[14], w[19], w[40], pars->GC_11, amp[43]); 
  FFV1_0(w[49], w[6], w[10], pars->GC_11, amp[44]); 
  FFV1_0(w[50], w[6], w[40], pars->GC_11, amp[45]); 
  VVV1_0(w[40], w[51], w[5], pars->GC_10, amp[46]); 
  VVV1_0(w[0], w[51], w[10], pars->GC_10, amp[47]); 
  FFV1_0(w[21], w[6], w[48], pars->GC_11, amp[48]); 
  FFV1_0(w[21], w[12], w[40], pars->GC_11, amp[49]); 
  FFV1_0(w[1], w[52], w[40], pars->GC_11, amp[50]); 
  VVV1_0(w[40], w[4], w[53], pars->GC_10, amp[51]); 
  FFV1_0(w[1], w[54], w[23], pars->GC_11, amp[52]); 
  VVV1_0(w[0], w[23], w[53], pars->GC_10, amp[53]); 
  FFV1_0(w[1], w[12], w[55], pars->GC_11, amp[54]); 
  FFV1_0(w[56], w[6], w[40], pars->GC_11, amp[55]); 
  VVV1_0(w[40], w[4], w[57], pars->GC_10, amp[56]); 
  FFV1_0(w[58], w[6], w[23], pars->GC_11, amp[57]); 
  VVV1_0(w[0], w[23], w[57], pars->GC_10, amp[58]); 
  FFV1_0(w[14], w[6], w[55], pars->GC_11, amp[59]); 
  VVVVS1_0(w[0], w[4], w[5], w[15], w[8], pars->GC_15, amp[60]); 
  VVVVS2_0(w[0], w[4], w[5], w[15], w[8], pars->GC_15, amp[61]); 
  VVVVS3_0(w[0], w[4], w[5], w[15], w[8], pars->GC_15, amp[62]); 
  VVV1_0(w[40], w[59], w[5], pars->GC_10, amp[63]); 
  VVV1_0(w[40], w[4], w[60], pars->GC_10, amp[64]); 
  VVVV1_0(w[4], w[5], w[15], w[40], pars->GC_12, amp[65]); 
  VVVV3_0(w[4], w[5], w[15], w[40], pars->GC_12, amp[66]); 
  VVVV4_0(w[4], w[5], w[15], w[40], pars->GC_12, amp[67]); 
  VVV1_0(w[44], w[23], w[5], pars->GC_10, amp[68]); 
  VVV1_0(w[44], w[4], w[10], pars->GC_10, amp[69]); 
  VVVS2_0(w[4], w[5], w[44], w[8], pars->GC_14, amp[70]); 
  VVV1_0(w[0], w[23], w[60], pars->GC_10, amp[71]); 
  VVV1_0(w[0], w[59], w[10], pars->GC_10, amp[72]); 
  VVV1_0(w[5], w[15], w[55], pars->GC_10, amp[73]); 
  VVS3_0(w[5], w[61], w[8], pars->GC_13, amp[74]); 
  VVS3_0(w[5], w[62], w[8], pars->GC_13, amp[75]); 
  VVS3_0(w[5], w[63], w[8], pars->GC_13, amp[76]); 
  VVV1_0(w[4], w[15], w[48], pars->GC_10, amp[77]); 
  VVS3_0(w[4], w[64], w[8], pars->GC_13, amp[78]); 
  VVS3_0(w[4], w[65], w[8], pars->GC_13, amp[79]); 
  VVS3_0(w[4], w[66], w[8], pars->GC_13, amp[80]); 
  VVS3_0(w[67], w[15], w[8], pars->GC_13, amp[81]); 
  VVS3_0(w[68], w[15], w[8], pars->GC_13, amp[82]); 
  VVS3_0(w[69], w[15], w[8], pars->GC_13, amp[83]); 
  FFV1_0(w[71], w[72], w[10], pars->GC_11, amp[84]); 
  FFV1_0(w[73], w[70], w[10], pars->GC_11, amp[85]); 
  FFV1_0(w[71], w[74], w[13], pars->GC_11, amp[86]); 
  FFV1_0(w[75], w[70], w[13], pars->GC_11, amp[87]); 
  VVVS2_0(w[7], w[5], w[76], w[8], pars->GC_14, amp[88]); 
  VVV1_0(w[5], w[76], w[13], pars->GC_10, amp[89]); 
  VVS3_0(w[76], w[16], w[8], pars->GC_13, amp[90]); 
  VVS3_0(w[5], w[77], w[8], pars->GC_13, amp[91]); 
  FFV1_0(w[71], w[78], w[20], pars->GC_11, amp[92]); 
  FFV1_0(w[79], w[70], w[20], pars->GC_11, amp[93]); 
  FFV1_0(w[71], w[80], w[23], pars->GC_11, amp[94]); 
  FFV1_0(w[81], w[70], w[23], pars->GC_11, amp[95]); 
  VVVS2_0(w[18], w[4], w[76], w[8], pars->GC_14, amp[96]); 
  VVS3_0(w[76], w[25], w[8], pars->GC_13, amp[97]); 
  VVV1_0(w[4], w[76], w[20], pars->GC_10, amp[98]); 
  VVS3_0(w[4], w[82], w[8], pars->GC_13, amp[99]); 
  VVS3_0(w[28], w[84], w[8], pars->GC_13, amp[100]); 
  VVS3_0(w[5], w[85], w[8], pars->GC_13, amp[101]); 
  FFV1_0(w[71], w[86], w[10], pars->GC_11, amp[102]); 
  FFV1_0(w[71], w[87], w[23], pars->GC_11, amp[103]); 
  VVV1_0(w[84], w[23], w[5], pars->GC_10, amp[104]); 
  VVV1_0(w[84], w[4], w[10], pars->GC_10, amp[105]); 
  VVVS2_0(w[4], w[5], w[84], w[8], pars->GC_14, amp[106]); 
  VVS3_0(w[4], w[88], w[8], pars->GC_13, amp[107]); 
  VVS3_0(w[28], w[90], w[8], pars->GC_13, amp[108]); 
  VVS3_0(w[5], w[91], w[8], pars->GC_13, amp[109]); 
  FFV1_0(w[92], w[70], w[10], pars->GC_11, amp[110]); 
  FFV1_0(w[93], w[70], w[23], pars->GC_11, amp[111]); 
  VVV1_0(w[90], w[23], w[5], pars->GC_10, amp[112]); 
  VVV1_0(w[90], w[4], w[10], pars->GC_10, amp[113]); 
  VVVS2_0(w[4], w[5], w[90], w[8], pars->GC_14, amp[114]); 
  VVS3_0(w[4], w[94], w[8], pars->GC_13, amp[115]); 
  FFV1_0(w[71], w[95], w[40], pars->GC_11, amp[116]); 
  FFV1_0(w[96], w[70], w[40], pars->GC_11, amp[117]); 
  VVVS2_0(w[0], w[28], w[76], w[8], pars->GC_14, amp[118]); 
  VVS3_0(w[76], w[43], w[8], pars->GC_13, amp[119]); 
  VVV1_0(w[28], w[76], w[40], pars->GC_10, amp[120]); 
  VVS3_0(w[28], w[97], w[8], pars->GC_13, amp[121]); 
  FFV1_0(w[71], w[98], w[10], pars->GC_11, amp[122]); 
  FFV1_0(w[71], w[99], w[40], pars->GC_11, amp[123]); 
  VVV1_0(w[40], w[100], w[5], pars->GC_10, amp[124]); 
  VVV1_0(w[0], w[100], w[10], pars->GC_10, amp[125]); 
  FFV1_0(w[71], w[78], w[48], pars->GC_11, amp[126]); 
  FFV1_0(w[75], w[78], w[40], pars->GC_11, amp[127]); 
  FFV1_0(w[101], w[70], w[10], pars->GC_11, amp[128]); 
  FFV1_0(w[102], w[70], w[40], pars->GC_11, amp[129]); 
  VVV1_0(w[40], w[103], w[5], pars->GC_10, amp[130]); 
  VVV1_0(w[0], w[103], w[10], pars->GC_10, amp[131]); 
  FFV1_0(w[79], w[70], w[48], pars->GC_11, amp[132]); 
  FFV1_0(w[79], w[74], w[40], pars->GC_11, amp[133]); 
  FFV1_0(w[71], w[104], w[40], pars->GC_11, amp[134]); 
  VVV1_0(w[40], w[4], w[105], pars->GC_10, amp[135]); 
  FFV1_0(w[71], w[106], w[23], pars->GC_11, amp[136]); 
  VVV1_0(w[0], w[23], w[105], pars->GC_10, amp[137]); 
  FFV1_0(w[71], w[74], w[55], pars->GC_11, amp[138]); 
  FFV1_0(w[107], w[70], w[40], pars->GC_11, amp[139]); 
  VVV1_0(w[40], w[4], w[108], pars->GC_10, amp[140]); 
  FFV1_0(w[109], w[70], w[23], pars->GC_11, amp[141]); 
  VVV1_0(w[0], w[23], w[108], pars->GC_10, amp[142]); 
  FFV1_0(w[75], w[70], w[55], pars->GC_11, amp[143]); 
  VVVVS1_0(w[0], w[4], w[5], w[76], w[8], pars->GC_15, amp[144]); 
  VVVVS2_0(w[0], w[4], w[5], w[76], w[8], pars->GC_15, amp[145]); 
  VVVVS3_0(w[0], w[4], w[5], w[76], w[8], pars->GC_15, amp[146]); 
  VVV1_0(w[40], w[110], w[5], pars->GC_10, amp[147]); 
  VVV1_0(w[40], w[4], w[111], pars->GC_10, amp[148]); 
  VVVV1_0(w[4], w[5], w[76], w[40], pars->GC_12, amp[149]); 
  VVVV3_0(w[4], w[5], w[76], w[40], pars->GC_12, amp[150]); 
  VVVV4_0(w[4], w[5], w[76], w[40], pars->GC_12, amp[151]); 
  VVV1_0(w[97], w[23], w[5], pars->GC_10, amp[152]); 
  VVV1_0(w[97], w[4], w[10], pars->GC_10, amp[153]); 
  VVVS2_0(w[4], w[5], w[97], w[8], pars->GC_14, amp[154]); 
  VVV1_0(w[0], w[23], w[111], pars->GC_10, amp[155]); 
  VVV1_0(w[0], w[110], w[10], pars->GC_10, amp[156]); 
  VVV1_0(w[5], w[76], w[55], pars->GC_10, amp[157]); 
  VVS3_0(w[5], w[112], w[8], pars->GC_13, amp[158]); 
  VVS3_0(w[5], w[113], w[8], pars->GC_13, amp[159]); 
  VVS3_0(w[5], w[114], w[8], pars->GC_13, amp[160]); 
  VVV1_0(w[4], w[76], w[48], pars->GC_10, amp[161]); 
  VVS3_0(w[4], w[115], w[8], pars->GC_13, amp[162]); 
  VVS3_0(w[4], w[116], w[8], pars->GC_13, amp[163]); 
  VVS3_0(w[4], w[117], w[8], pars->GC_13, amp[164]); 
  VVS3_0(w[67], w[76], w[8], pars->GC_13, amp[165]); 
  VVS3_0(w[68], w[76], w[8], pars->GC_13, amp[166]); 
  VVS3_0(w[69], w[76], w[8], pars->GC_13, amp[167]); 


}
double PY8MEs_R8_P22_heft_gq_hhggq::matrix_8_gu_hhggu() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 84;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = -Complex<double> (0, 1) * amp[0] - Complex<double> (0, 1) * amp[3]
      - amp[4] - amp[5] - amp[6] + amp[7] - Complex<double> (0, 1) * amp[16] +
      amp[18] - Complex<double> (0, 1) * amp[20] - Complex<double> (0, 1) *
      amp[21] - Complex<double> (0, 1) * amp[22] + amp[23] - Complex<double>
      (0, 1) * amp[33] - amp[34] - amp[35] - amp[36] + amp[37] + amp[55] -
      Complex<double> (0, 1) * amp[56] - Complex<double> (0, 1) * amp[58] -
      Complex<double> (0, 1) * amp[59] - amp[60] + amp[62] - amp[64] + amp[67]
      - amp[65] + amp[68] + amp[69] + amp[70] - amp[71] - amp[73] - amp[76] -
      amp[75] + amp[78] + amp[79] - amp[81] + amp[83];
  jamp[1] = -Complex<double> (0, 1) * amp[9] - Complex<double> (0, 1) * amp[10]
      - amp[12] - amp[13] - amp[14] + amp[15] + Complex<double> (0, 1) *
      amp[16] + amp[17] + amp[19] + Complex<double> (0, 1) * amp[20] +
      Complex<double> (0, 1) * amp[21] + Complex<double> (0, 1) * amp[22] +
      Complex<double> (0, 1) * amp[33] + amp[34] + amp[35] + amp[36] - amp[37]
      + amp[45] + Complex<double> (0, 1) * amp[46] + Complex<double> (0, 1) *
      amp[47] - Complex<double> (0, 1) * amp[48] - amp[61] - amp[62] + amp[63]
      + amp[66] + amp[65] - amp[68] - amp[69] - amp[70] + amp[72] + amp[74] +
      amp[75] - amp[77] - amp[80] - amp[79] + amp[81] + amp[82];
  jamp[2] = +Complex<double> (0, 1) * amp[0] + Complex<double> (0, 1) * amp[3]
      + amp[4] + amp[5] + amp[6] - amp[7] - Complex<double> (0, 1) * amp[8] -
      Complex<double> (0, 1) * amp[11] + amp[12] + amp[13] + amp[14] - amp[15]
      + amp[38] + Complex<double> (0, 1) * amp[40] + Complex<double> (0, 1) *
      amp[41] - Complex<double> (0, 1) * amp[42] + amp[43] + Complex<double>
      (0, 1) * amp[56] + amp[57] + Complex<double> (0, 1) * amp[58] +
      Complex<double> (0, 1) * amp[59] + amp[60] + amp[61] - amp[63] + amp[64]
      - amp[67] - amp[66] + amp[71] - amp[72] + amp[73] + amp[76] - amp[74] +
      amp[77] + amp[80] - amp[78] - amp[82] - amp[83];
  jamp[3] = +Complex<double> (0, 1) * amp[8] + Complex<double> (0, 1) * amp[11]
      - amp[12] - amp[13] - amp[14] + amp[15] - Complex<double> (0, 1) *
      amp[24] + amp[25] + amp[27] - Complex<double> (0, 1) * amp[28] -
      Complex<double> (0, 1) * amp[29] - Complex<double> (0, 1) * amp[30] -
      Complex<double> (0, 1) * amp[32] + amp[34] + amp[35] + amp[36] - amp[37]
      + amp[39] - Complex<double> (0, 1) * amp[40] - Complex<double> (0, 1) *
      amp[41] + Complex<double> (0, 1) * amp[42] - amp[61] - amp[62] + amp[63]
      + amp[66] + amp[65] - amp[68] - amp[69] - amp[70] + amp[72] + amp[74] +
      amp[75] - amp[77] - amp[80] - amp[79] + amp[81] + amp[82];
  jamp[4] = -Complex<double> (0, 1) * amp[1] - Complex<double> (0, 1) * amp[2]
      + amp[4] + amp[5] + amp[6] - amp[7] + Complex<double> (0, 1) * amp[9] +
      Complex<double> (0, 1) * amp[10] + amp[12] + amp[13] + amp[14] - amp[15]
      + amp[44] - Complex<double> (0, 1) * amp[46] - Complex<double> (0, 1) *
      amp[47] + Complex<double> (0, 1) * amp[48] + amp[49] - Complex<double>
      (0, 1) * amp[51] + amp[52] - Complex<double> (0, 1) * amp[53] -
      Complex<double> (0, 1) * amp[54] + amp[60] + amp[61] - amp[63] + amp[64]
      - amp[67] - amp[66] + amp[71] - amp[72] + amp[73] + amp[76] - amp[74] +
      amp[77] + amp[80] - amp[78] - amp[82] - amp[83];
  jamp[5] = +Complex<double> (0, 1) * amp[1] + Complex<double> (0, 1) * amp[2]
      - amp[4] - amp[5] - amp[6] + amp[7] + Complex<double> (0, 1) * amp[24] +
      amp[26] + Complex<double> (0, 1) * amp[28] + Complex<double> (0, 1) *
      amp[29] + Complex<double> (0, 1) * amp[30] + amp[31] + Complex<double>
      (0, 1) * amp[32] - amp[34] - amp[35] - amp[36] + amp[37] + amp[50] +
      Complex<double> (0, 1) * amp[51] + Complex<double> (0, 1) * amp[53] +
      Complex<double> (0, 1) * amp[54] - amp[60] + amp[62] - amp[64] + amp[67]
      - amp[65] + amp[68] + amp[69] + amp[70] - amp[71] - amp[73] - amp[76] -
      amp[75] + amp[78] + amp[79] - amp[81] + amp[83];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[0][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}

double PY8MEs_R8_P22_heft_gq_hhggq::matrix_8_gux_hhggux() 
{
  int i, j; 
  // Local variables
  // const int ngraphs = 84;
  const int ncolor = 6; 
  Complex<double> ztemp; 
  Complex<double> jamp[ncolor]; 
  // The color matrix;
  static const double denom[ncolor] = {9, 9, 9, 9, 9, 9}; 
  static const double cf[ncolor][ncolor] = {{64, -8, -8, 1, 1, 10}, {-8, 64, 1,
      10, -8, 1}, {-8, 1, 64, -8, 10, 1}, {1, 10, -8, 64, 1, -8}, {1, -8, 10,
      1, 64, -8}, {10, 1, 1, -8, -8, 64}};

  // Calculate color flows
  jamp[0] = +Complex<double> (0, 1) * amp[84] + Complex<double> (0, 1) *
      amp[87] + amp[88] + amp[89] + amp[90] - amp[91] + Complex<double> (0, 1)
      * amp[100] - amp[102] + Complex<double> (0, 1) * amp[104] +
      Complex<double> (0, 1) * amp[105] + Complex<double> (0, 1) * amp[106] -
      amp[107] + Complex<double> (0, 1) * amp[117] + amp[118] + amp[119] +
      amp[120] - amp[121] - amp[139] + Complex<double> (0, 1) * amp[140] +
      Complex<double> (0, 1) * amp[142] + Complex<double> (0, 1) * amp[143] +
      amp[144] - amp[146] + amp[148] - amp[151] + amp[149] - amp[152] -
      amp[153] - amp[154] + amp[155] + amp[157] + amp[160] + amp[159] -
      amp[162] - amp[163] + amp[165] - amp[167];
  jamp[1] = +Complex<double> (0, 1) * amp[93] + Complex<double> (0, 1) *
      amp[94] + amp[96] + amp[97] + amp[98] - amp[99] - Complex<double> (0, 1)
      * amp[100] - amp[101] - amp[103] - Complex<double> (0, 1) * amp[104] -
      Complex<double> (0, 1) * amp[105] - Complex<double> (0, 1) * amp[106] -
      Complex<double> (0, 1) * amp[117] - amp[118] - amp[119] - amp[120] +
      amp[121] - amp[129] - Complex<double> (0, 1) * amp[130] - Complex<double>
      (0, 1) * amp[131] + Complex<double> (0, 1) * amp[132] + amp[145] +
      amp[146] - amp[147] - amp[150] - amp[149] + amp[152] + amp[153] +
      amp[154] - amp[156] - amp[158] - amp[159] + amp[161] + amp[164] +
      amp[163] - amp[165] - amp[166];
  jamp[2] = -Complex<double> (0, 1) * amp[84] - Complex<double> (0, 1) *
      amp[87] - amp[88] - amp[89] - amp[90] + amp[91] + Complex<double> (0, 1)
      * amp[92] + Complex<double> (0, 1) * amp[95] - amp[96] - amp[97] -
      amp[98] + amp[99] - amp[122] - Complex<double> (0, 1) * amp[124] -
      Complex<double> (0, 1) * amp[125] + Complex<double> (0, 1) * amp[126] -
      amp[127] - Complex<double> (0, 1) * amp[140] - amp[141] - Complex<double>
      (0, 1) * amp[142] - Complex<double> (0, 1) * amp[143] - amp[144] -
      amp[145] + amp[147] - amp[148] + amp[151] + amp[150] - amp[155] +
      amp[156] - amp[157] - amp[160] + amp[158] - amp[161] - amp[164] +
      amp[162] + amp[166] + amp[167];
  jamp[3] = -Complex<double> (0, 1) * amp[92] - Complex<double> (0, 1) *
      amp[95] + amp[96] + amp[97] + amp[98] - amp[99] + Complex<double> (0, 1)
      * amp[108] - amp[109] - amp[111] + Complex<double> (0, 1) * amp[112] +
      Complex<double> (0, 1) * amp[113] + Complex<double> (0, 1) * amp[114] +
      Complex<double> (0, 1) * amp[116] - amp[118] - amp[119] - amp[120] +
      amp[121] - amp[123] + Complex<double> (0, 1) * amp[124] + Complex<double>
      (0, 1) * amp[125] - Complex<double> (0, 1) * amp[126] + amp[145] +
      amp[146] - amp[147] - amp[150] - amp[149] + amp[152] + amp[153] +
      amp[154] - amp[156] - amp[158] - amp[159] + amp[161] + amp[164] +
      amp[163] - amp[165] - amp[166];
  jamp[4] = +Complex<double> (0, 1) * amp[85] + Complex<double> (0, 1) *
      amp[86] - amp[88] - amp[89] - amp[90] + amp[91] - Complex<double> (0, 1)
      * amp[93] - Complex<double> (0, 1) * amp[94] - amp[96] - amp[97] -
      amp[98] + amp[99] - amp[128] + Complex<double> (0, 1) * amp[130] +
      Complex<double> (0, 1) * amp[131] - Complex<double> (0, 1) * amp[132] -
      amp[133] + Complex<double> (0, 1) * amp[135] - amp[136] + Complex<double>
      (0, 1) * amp[137] + Complex<double> (0, 1) * amp[138] - amp[144] -
      amp[145] + amp[147] - amp[148] + amp[151] + amp[150] - amp[155] +
      amp[156] - amp[157] - amp[160] + amp[158] - amp[161] - amp[164] +
      amp[162] + amp[166] + amp[167];
  jamp[5] = -Complex<double> (0, 1) * amp[85] - Complex<double> (0, 1) *
      amp[86] + amp[88] + amp[89] + amp[90] - amp[91] - Complex<double> (0, 1)
      * amp[108] - amp[110] - Complex<double> (0, 1) * amp[112] -
      Complex<double> (0, 1) * amp[113] - Complex<double> (0, 1) * amp[114] -
      amp[115] - Complex<double> (0, 1) * amp[116] + amp[118] + amp[119] +
      amp[120] - amp[121] - amp[134] - Complex<double> (0, 1) * amp[135] -
      Complex<double> (0, 1) * amp[137] - Complex<double> (0, 1) * amp[138] +
      amp[144] - amp[146] + amp[148] - amp[151] + amp[149] - amp[152] -
      amp[153] - amp[154] + amp[155] + amp[157] + amp[160] + amp[159] -
      amp[162] - amp[163] + amp[165] - amp[167];

  // Sum and square the color flows to get the matrix element
  double matrix = 0; 
  for(i = 0; i < ncolor; i++ )
  {
    ztemp = 0.; 
    for(j = 0; j < ncolor; j++ )
      ztemp = ztemp + cf[i][j] * jamp[j]; 
    matrix = matrix + real(ztemp * conj(jamp[i]))/denom[i]; 
  }

  // Store the leading color flows for choice of color
  for(i = 0; i < ncolor; i++ )
    jamp2[1][i] += real(jamp[i] * conj(jamp[i])) * (cf[i][i]/denom[i]); 

  return matrix; 
}


}  // end namespace PY8MEs_namespace

