#ifndef COMPLEX_H_INCLUDED
#define COMPLEX_H_INCLUDED

#include <cmath>
#include <iostream>
#include <fstream>
#include <iomanip>

template <class T>
class Complex {
public:
	enum { D = 2 };

	//Complex(const Complex<T> &S) : vals._a(S.vals._a), vals._b(S.vals._b) { }
	Complex(const Complex<T> &S)
          { vals._a = S.vals._a; vals._b = S.vals._b; }
	//Complex(const T U[D]) : a(U[0]), b(U[1]) { }
	Complex(const T U[D])
          { vals._a = U[0]; vals._b =U[1]; }
	//Complex(const T &sa = (T)0, const T &sb = (T)0) : vals._a(sa), vals._b(sb) { }
	Complex(const T &sa = (T)0, const T &sb = (T)0)
          { vals._a = sa; vals._b = sb; }

	const Complex<T> & set(const T &, const T &);

	const Complex<T> operator + (const Complex<T> &) const;
	const Complex<T> operator - (const Complex<T> &) const;
	const Complex<T> operator * (const Complex<T> &) const;
	const Complex<T> operator / (const Complex<T> &) const;
	const Complex<T> operator + (const T &) const;
	const Complex<T> operator - (const T &) const;
	const Complex<T> operator * (const T &) const;
	const Complex<T> operator / (const T &) const;

	const Complex<T> & operator = (const Complex<T> &);
	const Complex<T> & operator += (const Complex<T> &);
	const Complex<T> & operator -= (const Complex<T> &);
	const Complex<T> & operator *= (const Complex<T> &);
	const Complex<T> & operator /= (const Complex<T> &);
	const Complex<T> & operator = (const T &);
	const Complex<T> & operator += (const T &);
	const Complex<T> & operator -= (const T &);
	const Complex<T> & operator *= (const T &);
	const Complex<T> & operator /= (const T &);

	const Complex<T> operator - () const;
	const Complex<T> operator + () const;
	const T & operator [] (const int) const;
	T & operator [] (const int);

        T re() { return vals._a;}
        T im() { return vals._b;}
        T real() { return vals._a;}
        T imag() { return vals._b;}

	std::ofstream & saveBin(std::ofstream &) const;
	std::ifstream & loadBin(std::ifstream &);

	T normalize(void);

	~Complex(void) { }

	union {
          T V[D];
          struct {
            T _a, _b;
          } vals;
        };
};

template <class T>
inline const T abs(const Complex<T> &A)
{
    return sqrt(A.vals._a*A.vals._a + A.vals._b*A.vals._b);
}

template <class T>
inline const T absSq(const Complex<T> &A)
{
    return A.vals._a*A.vals._a + A.vals._b*A.vals._b;
}

template <class T>
inline const T arg(const Complex<T> &A)
{
    return atan2(A.vals._b, A.vals._a);
}

template <class T>
const Complex<T> conj(const Complex<T> &A)
{
    return Complex<T>(A.vals._a, -A.vals._b);
}

template <class T>
inline const T re(const Complex<T> &A)
{
    return A.vals._a;
}

template <class T>
inline const T real(const Complex<T> &A)
{
    return A.vals._a;
}

template <class T>
inline const T im(const Complex<T> &A)
{
    return A.vals._b;
}

template <class T>
inline const T imag(const Complex<T> &A)
{
    return A.vals._b;
}

template <class T>
inline Complex<T> sqrt(const Complex<T> &C)
{
	T l = abs(C);
	T p = 0.5*arg(C);

	return Complex<T>(l*cos(p), l*sin(p));
}

template <class T>
inline Complex<T> exp(const Complex<T> &C)
{
	T k = exp(C.vals._a);
	return Complex<T>(cos(C.vals._b)*k, sin(C.vals._b)*k);
}

template <class T>
inline Complex<T> pow(const Complex<T> &C, const T &m)
{
	T l = pow(abs(C), m);
	T p = m*arg(C);

	return Complex<T>(l*cos(p), l*sin(p));
}

template <class T>
inline Complex<T> pow(const T &a, const Complex<T> &C)
{
	T p = pow(a, C.vals._a);
	T l = log(C.vals._a);

	return Complex<T>(p*cos(C.vals._b*l), p*sin(C.vals._b*l));
}

template <class T>
std::ostream & operator << (std::ostream &vout, const Complex<T> &Q)
{
    return vout << "" << std::setw(14) << Q.vals._a << " + " << std::setw(14) << Q.vals._b << "i";
}

template <class T>
inline const Complex<T> operator + (const T &l, const Complex<T> &R)
{
    return Complex<T>(l + R.vals._a, R.vals._b);
}

template <class T>
inline const Complex<T> operator - (const T &l, const Complex<T> &R)
{
    return Complex<T>(l - R.vals._a, -R.vals._b);
}

template <class T>
inline const Complex<T> operator * (const T &l, const Complex<T> &R)
{
    return Complex<T>(l*R.vals._a, l*R.vals._b);
}

template <class T>
inline const Complex<T> operator / (const T &l, const Complex<T> &R)
{
    T z = absSq(R);
    return Complex<T>(l*R.vals._a/z, -l*R.vals._b/z);
}

template <class T>
inline const Complex<T> & Complex<T>::set(const T &sa, const T &sb)
{
	vals._a = sa; vals._b = sb;
	return *this;
}

template <class T>
inline const Complex<T> Complex<T>::operator + (const Complex<T> &R) const
{
    return Complex<T>(vals._a + R.vals._a, vals._b + R.vals._b);
}

template <class T>
inline const Complex<T> Complex<T>::operator - (const Complex<T> &R) const
{
    return Complex<T>(vals._a - R.vals._a, vals._b - R.vals._b);
}

template <class T>
inline const Complex<T> Complex<T>::operator * (const Complex<T> &R) const
{
    return Complex<T>(vals._a*R.vals._a - vals._b*R.vals._b, vals._a*R.vals._b + vals._b*R.vals._a);
}

template <class T>
inline const Complex<T> Complex<T>::operator / (const Complex<T> &R) const
{
    T z = abs(R);
    return Complex<T>((vals._a*R.vals._a + vals._b*R.vals._b)/z, (vals._b*R.vals._a - vals._a*R.vals._b)/z);
}

template <class T>
inline const Complex<T> Complex<T>::operator + (const T &r) const
{
    return Complex<T>(vals._a + r, vals._b);
}

template <class T>
inline const Complex<T> Complex<T>::operator - (const T &r) const
{
    return Complex<T>(vals._a - r, vals._b);
}

template <class T>
inline const Complex<T> Complex<T>::operator * (const T &r) const
{
    return Complex<T>(vals._a*r, vals._b*r);
}

template <class T>
inline const Complex<T> Complex<T>::operator / (const T &r) const
{
    return Complex<T>(vals._a/r, vals._b/r);
}

template <class T>
inline const Complex<T> & Complex<T>::operator = (const Complex<T> &R)
{
    return set(R.vals._a, R.vals._b);
}

template <class T>
inline const Complex<T> & Complex<T>::operator += (const Complex<T> &R)
{
    return set(vals._a + R.vals._a, vals._b + R.vals._b);
}

template <class T>
inline const Complex<T> & Complex<T>::operator -= (const Complex<T> &R)
{
    return set(vals._a - R.vals._a, vals._b - R.vals._b);
}

template <class T>
inline const Complex<T> & Complex<T>::operator *= (const Complex<T> &R)
{
    return set(vals._a*R.vals._a - vals._a*R.vals._b, vals._a*R.vals._b + vals._b*R.vals._a);
}

template <class T>
inline const Complex<T> & Complex<T>::operator /= (const Complex<T> &R)
{
    T z = abs(R);
    return set((vals._a*R.vals._a + vals._a*R.vals._b)/z, (vals._b*R.vals._a - vals,vals._a*R.vals._b)/z);
}

template <class T>
inline const Complex<T> & Complex<T>::operator = (const T &r)
{
    return set(r, static_cast<T>(0));
}

template <class T>
inline const Complex<T> & Complex<T>::operator += (const T &r)
{
    return set(vals._a + r, vals._b);
}

template <class T>
inline const Complex<T> & Complex<T>::operator -= (const T &r)
{
    return set(vals._a - r, vals._b);
}

template <class T>
inline const Complex<T> & Complex<T>::operator *= (const T &r)
{
    return set(vals._a*r, vals._b*r);
}

template <class T>
inline const Complex<T> & Complex<T>::operator /= (const T &r)
{
    return set(vals._a/r, vals._b/r);
}

template <class T>
inline const Complex <T> Complex<T>::operator - () const
{
	return Complex(-vals._a, -vals._b);
}

template <class T>
inline const Complex <T> Complex<T>::operator + () const
{
	return Complex(vals._a,vals._b);
}

template <class T>
inline const T & Complex<T>::operator [] (const int en) const
{
	return V[en];
}

template <class T>
inline T & Complex<T>::operator [] (const int en)
{
	return V[en];
}

template <class T>
inline std::ofstream & Complex<T>::saveBin(std::ofstream &savf) const
{
	savf.write((char *)V, D*sizeof(T));
	return savf;
}

template <class T>
inline std::ifstream & Complex<T>::loadBin(std::ifstream &loaf)
{
	loaf.read((char *)V, D*sizeof(T));
	return loaf;
}


template <class T>
inline T Complex<T>::normalize(void)
{
	T l = sqrt(vals._a*vals._a + vals._b*vals._b);

    if(fabs(l) > (T)0)
    {
        vals._a /= l;
        vals._b /= l;
    }

	return l;
}

#endif 
